local UITabBar = import "L10.UI.UITabBar"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"
local UIProgressBar = import "UIProgressBar"
local CButton = import "L10.UI.CButton"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local PlayerSettings = import "L10.Game.PlayerSettings"
local MessageWndManager = import "L10.UI.MessageWndManager"
local BoxCollider = import "UnityEngine.BoxCollider"
local NGUIMath = import "NGUIMath"
local CUICommonDef = import "L10.UI.CUICommonDef"
local PathMode = import "DG.Tweening.PathMode"
local PathType = import "DG.Tweening.PathType"
local Ease = import "DG.Tweening.Ease"
local Vector4 = import "UnityEngine.Vector4"

LuaHaoYiXingCardsWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaHaoYiXingCardsWnd, "TopInfoLabel", "TopInfoLabel", UILabel)
RegistChildComponent(LuaHaoYiXingCardsWnd, "TopHintLabel", "TopHintLabel", UILabel)
RegistChildComponent(LuaHaoYiXingCardsWnd, "ScoreLabel", "ScoreLabel", UILabel)
RegistChildComponent(LuaHaoYiXingCardsWnd, "ScoreProgressLabel", "ScoreProgressLabel", UILabel)
RegistChildComponent(LuaHaoYiXingCardsWnd, "UITabBar", "UITabBar", UITabBar)
RegistChildComponent(LuaHaoYiXingCardsWnd, "ShareButton", "ShareButton", GameObject)
RegistChildComponent(LuaHaoYiXingCardsWnd, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaHaoYiXingCardsWnd, "Template", "Template", GameObject)
RegistChildComponent(LuaHaoYiXingCardsWnd, "BatchBreakButton", "BatchBreakButton", CButton)
RegistChildComponent(LuaHaoYiXingCardsWnd, "DuplicateCradsNumLabel", "DuplicateCradsNumLabel", UILabel)
RegistChildComponent(LuaHaoYiXingCardsWnd, "ComposeButton", "ComposeButton", GameObject)
RegistChildComponent(LuaHaoYiXingCardsWnd, "ReadMeButton", "ReadMeButton", GameObject)
RegistChildComponent(LuaHaoYiXingCardsWnd, "ShowBigPictButton", "ShowBigPictButton", GameObject)
RegistChildComponent(LuaHaoYiXingCardsWnd, "Pool", "Pool", GameObject)
RegistChildComponent(LuaHaoYiXingCardsWnd, "ButtonsBg", "ButtonsBg", GameObject)
RegistChildComponent(LuaHaoYiXingCardsWnd, "BreakButton", "BreakButton", GameObject)
RegistChildComponent(LuaHaoYiXingCardsWnd, "BlankWidget", "BlankWidget", GameObject)
RegistChildComponent(LuaHaoYiXingCardsWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaHaoYiXingCardsWnd, "ScrollView", "ScrollView", UIScrollView)
RegistChildComponent(LuaHaoYiXingCardsWnd, "Type1CardNumLabel", "Type1CardNumLabel", UILabel)
RegistChildComponent(LuaHaoYiXingCardsWnd, "Type2CardNumLabel", "Type2CardNumLabel", UILabel)
RegistChildComponent(LuaHaoYiXingCardsWnd, "Type3CardNumLabel", "Type3CardNumLabel", UILabel)
RegistChildComponent(LuaHaoYiXingCardsWnd, "Type4CardNumLabel", "Type4CardNumLabel", UILabel)
RegistChildComponent(LuaHaoYiXingCardsWnd, "ScoreProgress", "ScoreProgress", UIProgressBar)
RegistChildComponent(LuaHaoYiXingCardsWnd, "AcceptTaskButton", "AcceptTaskButton", CButton)
RegistChildComponent(LuaHaoYiXingCardsWnd, "AcceptNextTaskScoreLabel", "AcceptNextTaskScoreLabel", UILabel)
RegistChildComponent(LuaHaoYiXingCardsWnd, "NextPageButton", "NextPageButton", GameObject)
RegistChildComponent(LuaHaoYiXingCardsWnd, "LastPageButton", "LastPageButton", GameObject)
RegistChildComponent(LuaHaoYiXingCardsWnd, "PageLabel", "PageLabel", UILabel)
RegistChildComponent(LuaHaoYiXingCardsWnd, "AcceptTaskButtonFx", "AcceptTaskButtonFx", CUIFx)
RegistChildComponent(LuaHaoYiXingCardsWnd, "HuaFx", "HuaFx", CUIFx)
RegistChildComponent(LuaHaoYiXingCardsWnd, "ExperienceAllTasksButton", "ExperienceAllTasksButton", GameObject)
RegistChildComponent(LuaHaoYiXingCardsWnd, "DiscussionButton", "DiscussionButton", GameObject)
--@endregion RegistChildComponent end
RegistClassMember(LuaHaoYiXingCardsWnd, "m_UITabBarCUITextures")
RegistClassMember(LuaHaoYiXingCardsWnd, "m_CardsList")
RegistClassMember(LuaHaoYiXingCardsWnd, "m_LastSelectTabIndex")
RegistClassMember(LuaHaoYiXingCardsWnd, "m_LastClickCardID")
RegistClassMember(LuaHaoYiXingCardsWnd, "m_IsGetAllCards")
RegistClassMember(LuaHaoYiXingCardsWnd, "m_SelectNum")
RegistClassMember(LuaHaoYiXingCardsWnd, "m_TemplateScripts")
RegistClassMember(LuaHaoYiXingCardsWnd, "m_CurShowCardIds")
RegistClassMember(LuaHaoYiXingCardsWnd, "m_FirstCardIndex")
RegistClassMember(LuaHaoYiXingCardsWnd, "m_AllCardIds")
RegistClassMember(LuaHaoYiXingCardsWnd, "m_AimScoreToAcceptNextTask")
-- RegistClassMember(LuaHaoYiXingCardsWnd, "m_Sound")
RegistClassMember(LuaHaoYiXingCardsWnd, "m_SpecialExtraCards")

function LuaHaoYiXingCardsWnd:Awake()
	--@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.ShareButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareButtonClick()
	end)

	UIEventListener.Get(self.BatchBreakButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBatchBreakButtonClick()
	end)

	UIEventListener.Get(self.ComposeButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnComposeButtonClick()
	end)

	UIEventListener.Get(self.ReadMeButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnReadMeButtonClick()
	end)

	UIEventListener.Get(self.ShowBigPictButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShowBigPictButtonClick()
	end)

	UIEventListener.Get(self.BreakButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBreakButtonClick()
	end)

	UIEventListener.Get(self.BlankWidget.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBlankWidgetClick()
    end)
    
    UIEventListener.Get(self.AcceptTaskButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAcceptTaskButtonClick()
	end)

    UIEventListener.Get(self.NextPageButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:ChangeToNextPage()
    end)
    
    UIEventListener.Get(self.LastPageButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:ChangeToLastPage()
	end)
    
    UIEventListener.Get(self.ExperienceAllTasksButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnExperienceAllTasksButtonClick()
	end)

    UIEventListener.Get(self.DiscussionButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDiscussionButtonClick()
	end)
    self.DiscussionButton.gameObject:SetActive(false)

    --@endregion EventBind end
end

function LuaHaoYiXingCardsWnd:Init()
    self.m_SelectNum = 0
    self.DiscussionButton.transform:Find("AlertSprite").gameObject:SetActive(false)
    local extraCards = g_LuaUtil:StrSplit(SpokesmanTCG_ErHaSetting.GetData("SpecialExtraCard").Value,";")
    self.m_SpecialExtraCards = {}
    for _,id in pairs(extraCards) do
        self.m_SpecialExtraCards[tonumber(id)] = true
    end
	self.TopInfoLabel.text = CUICommonDef.TranslateToNGUIText(g_MessageMgr:FormatMessage("HaoYiXingCard_Share_Exchange"))
	--self.TopHintLabel.text = CUICommonDef.TranslateToNGUIText(g_MessageMgr:FormatMessage("HaoYiXingCardsWnd_TopHintText"))
	self.Template:SetActive(false)
    self.Pool:SetActive(false)
	self.ButtonsBg:SetActive(false)
	self.AcceptTaskButtonFx:LoadFx("Fx/UI/Prefab/UI_kuang_blue02.prefab")
	self.m_UITabBarCUITextures = {}
    self.UITabBar:Start()
    for i =0,self.UITabBar.tabButtons.Length - 1 do
        local tab = self.UITabBar:GetTabGoByIndex(i)
        self.m_UITabBarCUITextures[i] = tab.transform:GetComponent(typeof(CUITexture))
    end
    self.m_UITabBarCUITextures.normalArray = {"HaoYiXingCardsWnd_tab_fusheng_02","HaoYiXingCardsWnd_tab_yike_02","HaoYiXingCardsWnd_tab_jinghong_02","HaoYiXingCardsWnd_tab_chenyuan_02"}
    self.m_UITabBarCUITextures.hightlightArray = {"HaoYiXingCardsWnd_tab_fusheng_01","HaoYiXingCardsWnd_tab_yike_01","HaoYiXingCardsWnd_tab_jinghong_01","HaoYiXingCardsWnd_tab_chenyuan_01"}
	self.UITabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
	    self:OnUITabBarTabChange(index)
	end)
    Extensions.RemoveAllChildren(self.Pool.transform)
    Extensions.RemoveAllChildren(self.Grid.transform)
    self.m_TemplateScripts = {}
    for i = 1, 10 do
        local obj = NGUITools.AddChild(self.Pool.gameObject,self.Template)
        obj:SetActive(true)
        local script = obj:GetComponent(typeof(CCommonLuaScript))
        script:Awake()
        script = script.m_LuaSelf
        table.insert(self.m_TemplateScripts, {script = script, obj = obj})
    end
    self.m_CurShowCardIds = {}
    self.m_LastSelectTabIndex = 0
    self.HuaFx:LoadFx("fx/ui/prefab/UI_haoyixing_hua.prefab")
    Gac2Gas.RequestErHaTCGData()
end

function LuaHaoYiXingCardsWnd:OnEnable()
    g_ScriptEvent:AddListener("OnPlotDiscussionEnable",self,"OnPlotDiscussionEnable")
    g_ScriptEvent:AddListener("OnPlotCommentShowRedAlert",self,"OnPlotCommentShowRedAlert")
    g_ScriptEvent:AddListener("SyncErHaTCGData",self,"OnSyncErHaTCGData")
    g_ScriptEvent:AddListener("ShareFinished", self, "OnShareFinished")
    g_ScriptEvent:AddListener("PersonalSpaceShareFinished", self, "OnPersonalSpaceShareFinished")
    g_ScriptEvent:AddListener("OnErHaTCGAcceptCardTaskSucc", self, "OnErHaTCGAcceptCardTaskSucc")
    LuaPlotDiscussionMgr:CheckIsEnable()
end

function LuaHaoYiXingCardsWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnPlotDiscussionEnable",self,"OnPlotDiscussionEnable")
    g_ScriptEvent:RemoveListener("OnPlotCommentShowRedAlert",self,"OnPlotCommentShowRedAlert")
    g_ScriptEvent:RemoveListener("SyncErHaTCGData",self,"OnSyncErHaTCGData")
    g_ScriptEvent:RemoveListener("ShareFinished", self, "OnShareFinished")
    g_ScriptEvent:RemoveListener("PersonalSpaceShareFinished", self, "OnPersonalSpaceShareFinished")
    g_ScriptEvent:RemoveListener("OnErHaTCGAcceptCardTaskSucc", self, "OnErHaTCGAcceptCardTaskSucc")
    -- if self.m_Sound then
    --     SoundManager.Inst:StopSound(self.m_Sound)
    --     self.m_Sound = nil
    -- end
    SoundManager.Inst:StopDialogSound()
    LuaHaoYiXingMgr.m_NeedSelectHiddenCardTab = false
end

function LuaHaoYiXingCardsWnd:OnPlotDiscussionEnable()
    local isEnable = LuaPlotDiscussionMgr:IsOpen()
    self.DiscussionButton.gameObject:SetActive(isEnable)
    if isEnable then
        LuaPlotDiscussionMgr:RequestRedDotInfo()
        CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.HaoYiXingCardsWndDiscussionButton)
    end
end

function LuaHaoYiXingCardsWnd:OnPlotCommentShowRedAlert(haoYiXingCardId, yaoGuiHuCardId, isClear)
    --print(haoYiXingCardId, yaoGuiHuCardId)
    local alertSprite = self.DiscussionButton.transform:Find("AlertSprite")
    if isClear then
        alertSprite.gameObject:SetActive(false)
    end
    if haoYiXingCardId ~= 0 then
        alertSprite.gameObject:SetActive(haoYiXingCardId ~= 0)
    end
end

function LuaHaoYiXingCardsWnd:OnShareFinished(argv)
    local code, result = argv[0], argv[1]
    if result then
        Gac2Gas.ErHaTCGShareDone(self.m_LastClickCardID)
    end
end

function LuaHaoYiXingCardsWnd:OnPersonalSpaceShareFinished()
    Gac2Gas.ErHaTCGShareDone(self.m_LastClickCardID)
end

function LuaHaoYiXingCardsWnd:OnSyncErHaTCGData(data)
    local list = data.list
    local totoalScore = data.totalScore
    local idsList = {}
    if LuaHaoYiXingMgr.m_NeedSelectHiddenCardTab then
        for i = 1,#list do
            local d = list[i]
            local id = d.id
            table.insert(idsList,id)
            local tcgdata = SpokesmanTCG_ErHaTCG.GetData(id)
            if tcgdata.IsHidden == 1 and not d.owner and not self.m_SpecialExtraCards[id] and (tcgdata.Type - 1) > self.m_LastSelectTabIndex then
                self.m_LastSelectTabIndex = tcgdata.Type - 1
            end
        end
    end
    self:InitCardsData(list)
    self:InitScore(totoalScore)

    self.UITabBar:ChangeTab(self.m_LastSelectTabIndex, false)
    if LuaHaoYiXingMgr.m_NeedSelectHiddenCardTab then
        for index, id in pairs(self.m_AllCardIds) do
           local t = self.m_CardsList[id]
            if t.tcgdata.IsHidden == 1 and not t.owner and not self.m_SpecialExtraCards[id] then 
                self.m_FirstCardIndex = math.floor(index / 10) * 10 + 1
                self:RecycleCards()
                self:ReloadCards()
                self:ShowPage()
                self:OnCardClick(id)
                break
            end
        end
    end
end

function LuaHaoYiXingCardsWnd:RecycleCards()
    for i , t in pairs(self.m_TemplateScripts) do
        t.obj.transform.parent = self.Pool.transform
    end
    for i , id in pairs(self.m_CurShowCardIds) do
        local t = self.m_CardsList[id]
        t.obj = nil
        t.script = nil
    end
end

function LuaHaoYiXingCardsWnd:ReloadCards()
    if not self.m_FirstCardIndex then return end
    self.m_CurShowCardIds = {}
    local maxNum = 10
    for index, id in pairs(self.m_AllCardIds) do
        if index == self.m_FirstCardIndex or (maxNum > 0 and maxNum < 10) then
            maxNum = maxNum - 1
            local key = id
            table.insert(self.m_CurShowCardIds, key)
        end
    end
    local arr = {0,0,0,0}
    for id, t in pairs(self.m_CardsList) do
        arr[t.tcgdata.Type] = arr[t.tcgdata.Type] + t.num
    end
    for i , id in pairs(self.m_CurShowCardIds) do
        local t = self.m_CardsList[id]
        local scriptData = self.m_TemplateScripts[i]
        if not scriptData then return end
        t.obj = scriptData.obj
        t.obj.transform.parent = self.Grid.transform
        t.script = scriptData.script
        t.script:InitCard(t.id,t.owner,t.isnew,t.num,t.pieceCount,t.taskStatus)
        t.script.Fx:DestroyFx()
        LuaTweenUtils.DOKill(t.script.Fx.transform, false)
        if t.tcgdata.IsHidden == 1 and not t.owner and not self.m_SpecialExtraCards[t.id] and LuaHaoYiXingMgr.m_ComposeHiddenCardTimes > 0 and arr[t.tcgdata.Type] >= (t.tcgdata.Type == 1 and 10 or 2) then
            --print(t.script.Fx)
            local fx = t.script.Fx
            local b = NGUIMath.CalculateRelativeWidgetBounds(t.script.transform)
            local waypoints = CUICommonDef.GetWayPoints(Vector4(b.min.x, b.max.y, b.size.x, b.size.y), 1, true)
			fx.transform.localPosition = waypoints[0]
			fx:LoadFx("fx/ui/prefab/UI_kuang_yellow02.prefab")
			LuaTweenUtils.DOLuaLocalPath(fx.transform, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, - 1, Ease.Linear)
        end
        UIEventListener.Get(t.obj).onClick = DelegateFactory.VoidDelegate(function (p)
            self:OnCardClick(t.id)
        end)
    end
    self.Grid:Reposition()
    self.ScrollView:ResetPosition()
end

function LuaHaoYiXingCardsWnd:ChangeToLastPage()
    if not self.m_FirstCardIndex or not self.m_AllCardIds or self.m_FirstCardIndex <= 10 then return end
    self.m_FirstCardIndex = self.m_FirstCardIndex - 10
    self:RecycleCards()
    self:ReloadCards()
    self:ShowPage()
end


function LuaHaoYiXingCardsWnd:ChangeToNextPage()
    if not self.m_FirstCardIndex or not self.m_AllCardIds or self.m_FirstCardIndex > (#self.m_AllCardIds - 10) then return end
    self.m_FirstCardIndex = self.m_FirstCardIndex + 10
    self:RecycleCards()
    self:ReloadCards()
    self:ShowPage()
end

function LuaHaoYiXingCardsWnd:ShowPage()
    if not self.m_FirstCardIndex or not self.m_AllCardIds then return end
    local curIndex = math.ceil(self.m_FirstCardIndex / 10)
    local pageNum = math.ceil(#self.m_AllCardIds / 10)
    self.PageLabel.text = SafeStringFormat3(LocalString.GetString("第%d/%d页"),curIndex,pageNum)
    self.LastPageButton.gameObject:SetActive(self.m_FirstCardIndex > 10)
    self.NextPageButton.gameObject:SetActive(self.m_FirstCardIndex <= (#self.m_AllCardIds - 10))
end

function LuaHaoYiXingCardsWnd:OnErHaTCGAcceptCardTaskSucc(cardId)
    if self.m_CardsList[tonumber(cardId)] then
        self.m_CardsList[tonumber(cardId)].taskStatus = 1
        self:RecycleCards()
        self:ReloadCards()
    end
end

function LuaHaoYiXingCardsWnd:InitCardsData(list)
    self.m_CardsList = {}
    local ownerNumList = {}
    local cardsNumList = {}
    local repateCardsNum = 0
    self.m_AllCardIds = {}
    self.m_FirstCardIndex = 1
    for i = 1,#list do

        local d = list[i]
        local id = d.id
        local isnew = d.isNew
        local num = d.num
        local pieceCount = d.pieceCount
        local taskStatus = d.taskStatus

        local t = {}
        t.tcgdata = SpokesmanTCG_ErHaTCG.GetData(id)
        t.num = num
        t.isnew = isnew
        t.id = id
        t.pieceCount = pieceCount
        t.owner = d.owner
        t.isCollected = d.isCollected
        t.taskStatus = taskStatus
        
        if t.tcgdata.Type <= 2 then
            repateCardsNum = repateCardsNum + num
        end
        cardsNumList[t.tcgdata.Type] = (not cardsNumList[t.tcgdata.Type]) and 1 or cardsNumList[t.tcgdata.Type] + 1
        ownerNumList[t.tcgdata.Type] = (ownerNumList[t.tcgdata.Type] and ownerNumList[t.tcgdata.Type] or 0) + (t.owner and 1 or 0)

        self.m_CardsList[id] = t
        if t.tcgdata.Type == (self.m_LastSelectTabIndex + 1) then
            table.insert(self.m_AllCardIds, id)
        end
    end
    table.sort(self.m_AllCardIds, function(a,b) 
        return a < b
    end)

    local ownCardsLabels = {self.Type1CardNumLabel, self.Type2CardNumLabel, self.Type3CardNumLabel, self.Type4CardNumLabel}
    for i = 1, 4 do 
        local label = ownCardsLabels[i]
        local ownerNum = ownerNumList[i] and ownerNumList[i] or 0
        local cardsNum = cardsNumList[i] and cardsNumList[i] or 0
        label.text = SafeStringFormat3("%d/%d",ownerNum, cardsNum)
    end

    self.DuplicateCradsNumLabel.text = CUICommonDef.TranslateToNGUIText(SafeStringFormat3(repateCardsNum == 0 and LocalString.GetString("重复卡片x[#ff0000]%d") or LocalString.GetString("重复卡片x%d"), repateCardsNum))
    Extensions.SetLocalPositionZ(self.BatchBreakButton.transform, repateCardsNum == 0 and -1 or 0)
    self.BatchBreakButton.enabled =  repateCardsNum > 0
    self.BatchBreakButton:GetComponent(typeof(BoxCollider)).enabled =  repateCardsNum > 0
    self:ShowPage()
end

function LuaHaoYiXingCardsWnd:InitScore(totoalScore)
    local score = 0
    local fullScoreSum = 0
    local idx = LuaHaoYiXingMgr.m_CurMainTaskIdx
    local lastSection = 1
    SpokesmanTCG_ErHaTCGScore.Foreach(function(key, data)
        if idx >= key then
            score = data.Score
        end
        lastSection = lastSection > key and lastSection or key
    end)
    SpokesmanTCG_ErHaTCG.Foreach(function(key, data)
        fullScoreSum = fullScoreSum + data.Score
    end)
    self.ScoreLabel.text = LocalString.GetString("积分")
    self.ScoreProgress.value = totoalScore / fullScoreSum
    self.ScoreProgressLabel.text = SafeStringFormat3("%d/%d",totoalScore , fullScoreSum)
    local data = SpokesmanTCG_ErHaTCGScore.GetData(idx)
    self.AcceptNextTaskScoreLabel.text = SafeStringFormat3(LocalString.GetString("%d积分"), score)
    if data then
        self.AcceptTaskButton.Text = SafeStringFormat3(LocalString.GetString("领取%s任务"), data.Desc)
    end
    
    self.m_AimScoreToAcceptNextTask = score 
    if score <= totoalScore and lastSection >= idx and LuaHaoYiXingMgr.m_canAcceptMainTask then
        local btnWith = self.AcceptTaskButton.width/2
        local btnHeight = self.AcceptTaskButton.height/2
        local path={ Vector3(btnWith,-btnHeight,0),Vector3(-btnWith,-btnHeight,0),Vector3(-btnWith,btnHeight,0),Vector3(btnWith,btnHeight,0),Vector3(btnWith,-btnHeight,0)}
        local array = Table2Array(path, MakeArrayClass(Vector3))
        LuaUtils.SetLocalPosition(self.AcceptTaskButtonFx.transform,btnWith,-btnHeight,0)
        local tween = LuaTweenUtils.DODefaultLocalPath(self.AcceptTaskButtonFx.transform,array,2)
        LuaTweenUtils.SetTarget(tween,self.AcceptTaskButtonFx.transform)
    else
        LuaTweenUtils.DOKill(self.AcceptTaskButtonFx.transform, false)
    end

    if lastSection < idx then
        self.AcceptTaskButton.Text = LocalString.GetString("已领取全部任务")
    end
    self.AcceptTaskButton.Enabled = lastSection >= idx
    self.AcceptNextTaskScoreLabel.gameObject:SetActive(lastSection >= idx)
end

function LuaHaoYiXingCardsWnd:OnDuiHuanClick(t)
    local arr = {0,0,0,0}
    for id, t in pairs(self.m_CardsList) do
        arr[t.tcgdata.Type] = arr[t.tcgdata.Type] + t.num
    end
    if LuaHaoYiXingMgr.m_ComposeHiddenCardTimes == 0 then
        g_MessageMgr:ShowMessage("HaoYiXingCard_Chance_NotEnoughToExchange")
        return
    elseif arr[t.tcgdata.Type] < (t.tcgdata.Type == 1 and 10 or 2) then
        g_MessageMgr:ShowMessage("HaoYiXingCard_Number_NotEnoughToExchange")
        return
    end
    local shareComposeGetHiddenCardRate = g_LuaUtil:StrSplit(SpokesmanTCG_ErHaSetting.GetData("ShareComposeNeedCardCountByType").Value,";")
    local costStr = shareComposeGetHiddenCardRate[t.tcgdata.Type]
    if costStr then
        local costinfo = g_LuaUtil:StrSplit(costStr,",")
        if costinfo then
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("HaoYiXingCard_ChanceToExchangeConfirm", costinfo[2], LuaHaoYiXingMgr.m_ComposeHiddenCardTimes), DelegateFactory.Action(function ()
                Gac2Gas.ErHaTCGComposeHiddenCard(t.id)
            end), nil, nil, nil, false)
        end
    end
end

function LuaHaoYiXingCardsWnd:OnCardClick(clickID)
    if not self.m_CardsList then return end
    local t = self.m_CardsList[clickID]
    if not t and not t.obj or not t.script then return end
    if self.m_LastClickCardID then
        local t = self.m_CardsList[self.m_LastClickCardID]
        if t and t.script then
            t.script:SetSelect(false)
        end
    end
    if L10.Game.Guide.CGuideMgr.Inst:IsInPhase(EnumGuideKey.HaoYiXingCard) then
        if tonumber(SpokesmanTCG_ErHaSetting.GetData("FreeCard").Value) == clickID then
            L10.Game.Guide.CGuideMgr.Inst:TriggerGuide(6)
            return
        end
    end
    t.script:OnCardClick()
    self.m_LastClickCardID = clickID
    --self.ButtonsBg:SetActive(t.owner)
    local popupList={}
    if t.tcgdata.IsHidden == 1 and not t.owner then
        if not self.m_SpecialExtraCards[t.id] then
            table.insert(popupList,PopupMenuItemData(LocalString.GetString("兑换"), DelegateFactory.Action_int(function(index) 
                self:OnDuiHuanClick(t)
            end),false, nil, EnumPopupMenuItemStyle.Default))
            local array=Table2Array(popupList,MakeArrayClass(PopupMenuItemData))
            local side = CPopupMenuInfoMgr.AlignType.Right
            CPopupMenuInfoMgr.ShowPopupMenu(array,t.obj.transform, side, 1, nil, 600, true, 220)
        else
            g_MessageMgr:ShowMessage("HaoYiXingCard_Task_Card_Cannot_Compose") 
        end
        return
    end
    if not t.isCollected then
        g_MessageMgr:ShowMessage("HaoYiXingCard_NeedMoreFragments")
        return 
    elseif t.isCollected and not t.owner then
        g_MessageMgr:ShowMessage("HaoYiXingCard_Card_Locked")
        return 
    end
    if t.isnew then
        Gac2Gas.ErHaTCGReadCard(clickID)
        t.isnew = false
        t.script:InitCard(t.id, t.owner, t.isnew, t.num, t.pieceCount,t.taskStatus)
    end
    self:ShowDefaultPopupMenu(t)
end

function LuaHaoYiXingCardsWnd:ShowDefaultPopupMenu(t)
    local popupList={}
    if t.tcgdata.Type == 4 and t.tcgdata.AcceptTask ~= 0 then
        if t.taskStatus == 0  then
            table.insert( popupList,PopupMenuItemData(LocalString.GetString("领取任务"), DelegateFactory.Action_int(function(index) 
                Gac2Gas.ErHaTCGAcceptCardTask(t.id)
            end),false, nil, EnumPopupMenuItemStyle.Orange))
        else
            local cardId = t.id
            table.insert( popupList,PopupMenuItemData(LocalString.GetString("重温剧情"), DelegateFactory.Action_int(function(index) 
                if self.m_CardsList[cardId] then
                    if self.m_CardsList[cardId].taskStatus == 1 then
                        g_MessageMgr:ShowMessage("HaoYiXing_ReviewTask_CanNotAccept_CanNotReview")
                        return
                    end
                end
                Gac2Gas.ErHaTCGReviewCardTask(cardId)
            end),false, nil, EnumPopupMenuItemStyle.Default))
        end
    end
    table.insert( popupList,PopupMenuItemData(t.tcgdata.Type == 1 and LocalString.GetString("故事背景") or (t.tcgdata.Type == 2 and LocalString.GetString("播放语音") or LocalString.GetString("查看大图")), DelegateFactory.Action_int(function(index) 
        self:OnShowBigPictButtonClick()
    end),false, nil, EnumPopupMenuItemStyle.Default))
    if t.num > 0 then
        table.insert( popupList,PopupMenuItemData(LocalString.GetString("分解卡牌"), DelegateFactory.Action_int(function(index) 
            self:OnBreakButtonClick()
        end),false, nil, EnumPopupMenuItemStyle.Default))
    end
    if t.owner and t.pieceCount > 0 then
        table.insert( popupList,PopupMenuItemData(LocalString.GetString("分解碎片"), DelegateFactory.Action_int(function(index) 
            self:OnBreakPieceButtonClick()
        end),false, nil, EnumPopupMenuItemStyle.Default))
    end
    table.insert( popupList,PopupMenuItemData(LocalString.GetString("截图分享"), DelegateFactory.Action_int(function(index) 
        self:OnShareButtonClick()
    end),false, nil, EnumPopupMenuItemStyle.Default))
    local array=Table2Array(popupList,MakeArrayClass(PopupMenuItemData))
    local side = CPopupMenuInfoMgr.AlignType.Right
    CPopupMenuInfoMgr.ShowPopupMenu(array,t.obj.transform, side, 1, nil, 600, true, 220)
end
--@region UIEvent

function LuaHaoYiXingCardsWnd:OnUITabBarTabChange(index)
	if not self.m_CardsList then return end
    if self.m_LastSelectTabIndex then
        self:OnTabChangeNoraml(self.m_LastSelectTabIndex,true)
    end
    self:OnTabChangeNoraml(index,false)
    self.m_LastSelectTabIndex = index
    self.m_AllCardIds = {}
    for id, t in pairs(self.m_CardsList) do
        if t.tcgdata.Type == (self.m_LastSelectTabIndex + 1) then
            table.insert(self.m_AllCardIds, id)
        end
    end
    table.sort(self.m_AllCardIds, function(a,b) 
        return a < b
    end)
    self.m_FirstCardIndex = 1
    self:RecycleCards()
    self:ReloadCards()
    self:ShowPage()
    self.ButtonsBg:SetActive(false)
end

function LuaHaoYiXingCardsWnd:OnTabChangeNoraml(index,noraml)
    if not self.m_UITabBarCUITextures then return end
    local tab =  self.m_UITabBarCUITextures[index]
    local array = noraml and self.m_UITabBarCUITextures.normalArray or self.m_UITabBarCUITextures.hightlightArray
    if tab then
        local path = SafeStringFormat3("UI/Texture/Transparent/Material/%s.mat",array[index + 1])
        tab:LoadMaterial(path)
    end
    local kuangFx = tab.transform:Find("KuangFx"):GetComponent(typeof(CUIFx))
    if kuangFx then
        kuangFx:DestroyFx()
        if not noraml then
            kuangFx:LoadFx("fx/ui/prefab/UI_haoyixing_kuang.prefab")
        end
    end
end

function LuaHaoYiXingCardsWnd:OnShareButtonClick()
	self.ButtonsBg:SetActive(false)
    if not self.m_LastClickCardID then return end
    if not CClientMainPlayer.Inst then return end
    CUICommonDef.CaptureScreen(
            "screenshot",
            true,
            false,
            self.ShareButton,
            DelegateFactory.Action_string_bytes(
                    function(fullPath, jpgBytes)
                        if  CLuaShareMgr.IsOpenShare() then
                            ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
                        else
                            CLuaShareMgr.SimpleShareLocalImage2PersonalSpace(fullPath)
                        end
                    end
            ),
            false
    )
end

function LuaHaoYiXingCardsWnd:OnBatchBreakButtonClick()
	if not self.m_CardsList then return end
    local arr = {0,0,0,0}
    local reward1 = 0
    for id, t in pairs(self.m_CardsList) do
        arr[t.tcgdata.Type] = arr[t.tcgdata.Type] + t.num
        if t.tcgdata.Type <= 2 then
            reward1 = reward1 + t.tcgdata.DecomposeGetHaiTangPieceCount * t.num
        end
    end
    local reward2 = math.floor(reward1 / tonumber(SpokesmanTCG_ErHaSetting.GetData("ComposeHaiTangNeedPieceCount").Value))
    local msg = g_MessageMgr:FormatMessage("HaoYiXingCard_Decompose_Fast", arr[1],arr[2],reward1)
    MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
        Gac2Gas.ErHaTCGBatchDecomposeCard()
    end), nil, nil, nil, false)
end

function LuaHaoYiXingCardsWnd:OnComposeButtonClick()
    CUIManager.ShowUI(CLuaUIResources.HaoYiXingCardsComposeWnd)
end

function LuaHaoYiXingCardsWnd:OnReadMeButtonClick()
	g_MessageMgr:ShowMessage("HaoYiXingCard_RuleMesssage")
end

function LuaHaoYiXingCardsWnd:OnShowBigPictButtonClick()
    if not self.m_LastClickCardID then return end
    LuaHaoYiXingMgr.m_PreviewWndPreviewCardID = self.m_LastClickCardID
    local tcgdata = SpokesmanTCG_ErHaTCG.GetData(self.m_LastClickCardID)
    if tcgdata.Type == 2 then
        if PlayerSettings.VolumeSetting == 0 then
            PlayerSettings.VolumeSetting = 1
        end
        PlayerSettings.SoundEnabled = true
        -- if self.m_Sound~=nil then
        --     SoundManager.Inst:StopSound(self.m_Sound)
        --     self.m_Sound = nil
        -- end
        SoundManager.Inst:StartDialogSound(tcgdata.Audio)
        --self.m_Sound = SoundManager.Inst:PlayOneShot(tcgdata.Audio, Vector3.zero, nil, 0, -1)
        return
    end
    CUIManager.ShowUI(tcgdata.Type == 1 and CLuaUIResources.HaoYiXingCardStoryWnd or CLuaUIResources.HaoYiXingCardPreviewWnd)
end

function LuaHaoYiXingCardsWnd:OnBreakButtonClick()
	if not self.m_LastClickCardID then return end
    if not self.m_CardsList then return end
    local t = self.m_CardsList[self.m_LastClickCardID]
    if not t then return end
    local msg = g_MessageMgr:FormatMessage("HaoYiXingCardsWnd_BreakCard",t.tcgdata.Name,t.tcgdata.DecomposeGetHaiTangPieceCount)
    CLuaNumberInputMgr.ShowNumInputBox(1, t.num, 1, function (val)
        Gac2Gas.ErHaTCGDecomposeCard(t.id, val)
    end, msg, -1)
end

function LuaHaoYiXingCardsWnd:OnBreakPieceButtonClick()
    if not self.m_LastClickCardID then return end
    if not self.m_CardsList then return end
    local t = self.m_CardsList[self.m_LastClickCardID]
    if not t then return end
    local msg = g_MessageMgr:FormatMessage("HaoYiXingCardsWnd_BreakPeice",t.tcgdata.Name,t.tcgdata.DecomposeGetHaiTangPieceCount/t.tcgdata.PieceCount)
    CLuaNumberInputMgr.ShowNumInputBox(1, t.pieceCount, t.pieceCount, function (val)
        Gac2Gas.ErHaTCGDecomposeCardPiece(t.id, val)
    end, msg, -1)
end

function LuaHaoYiXingCardsWnd:OnBlankWidgetClick()
	self.ButtonsBg:SetActive(false)
end

function LuaHaoYiXingCardsWnd:OnAcceptTaskButtonClick()
    if self.m_AimScoreToAcceptNextTask > LuaHaoYiXingMgr.m_TotalScore then
        g_MessageMgr:ShowMessage("HaoYiXingCard_AcceptTask_Limit_Score",self.m_AimScoreToAcceptNextTask)
        return 
    end
	Gac2Gas.ErHaTCGAcceptScoreTask()
end

function LuaHaoYiXingCardsWnd:OnExperienceAllTasksButtonClick()
    CUIManager.ShowUI(CLuaUIResources.HaoYiXingExperienceTasksWnd)
end

function LuaHaoYiXingCardsWnd:OnDiscussionButtonClick()
    self.DiscussionButton.transform:Find("AlertSprite").gameObject:SetActive(false)
    LuaPlotDiscussionMgr.m_ShowCardWndType = 1
    CUIManager.ShowUI(CLuaUIResources.PlotCardsWnd)
end

function LuaHaoYiXingCardsWnd:GetGuideGo(methodName)
    if methodName == "GetComposeButton" then
        return self.ComposeButton
    elseif methodName == "GetScoreLabel" then
        return self.ScoreLabel.gameObject
    elseif methodName == "GetAcceptTaskButton" then
        return self.AcceptTaskButton.gameObject
    elseif methodName == "GetNewGetButton" then
        local id = tonumber(SpokesmanTCG_ErHaSetting.GetData("FreeCard").Value)
        if self.m_CardsList then
            local t = self.m_CardsList[id]
            return t and t.obj or nil
        end
    elseif methodName == "GetDiscussionButton" then
        return self.DiscussionButton.gameObject
    end
    return nil
end

--@endregion UIEvent

