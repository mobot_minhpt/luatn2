local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UITabBar = import "L10.UI.UITabBar"
local GameObject = import "UnityEngine.GameObject"
local CUITexture = import "L10.UI.CUITexture"

LuaRuYiDongSignWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaRuYiDongSignWnd, "TopTabBar", "TopTabBar", UITabBar)
RegistChildComponent(LuaRuYiDongSignWnd, "TipBtn", "TipBtn", GameObject)
RegistChildComponent(LuaRuYiDongSignWnd, "RightTabBar", "RightTabBar", UITabBar)
RegistChildComponent(LuaRuYiDongSignWnd, "SignBtn", "SignBtn", GameObject)
RegistChildComponent(LuaRuYiDongSignWnd, "BossView", "BossView", GameObject)
RegistChildComponent(LuaRuYiDongSignWnd, "BossIcon1", "BossIcon1", CUITexture)
RegistChildComponent(LuaRuYiDongSignWnd, "BossIcon2", "BossIcon2", CUITexture)
RegistChildComponent(LuaRuYiDongSignWnd, "BossIcon3", "BossIcon3", CUITexture)
RegistChildComponent(LuaRuYiDongSignWnd, "BossIcon4", "BossIcon4", CUITexture)
RegistChildComponent(LuaRuYiDongSignWnd, "SignView", "SignView", GameObject)
RegistChildComponent(LuaRuYiDongSignWnd, "RuYiDongRankView", "RuYiDongRankView", CCommonLuaScript)

--@endregion RegistChildComponent end
RegistClassMember(LuaRuYiDongSignWnd,"m_HasRankData")
RegistClassMember(LuaRuYiDongSignWnd,"m_DifficultyIndex")

function LuaRuYiDongSignWnd:Awake()
	self.m_HasRankData = true-------false
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.TipBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipBtnClick()
	end)


	
	UIEventListener.Get(self.SignBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSignBtnClick()
	end)


    --@endregion EventBind end
	self.RuYiDongRankView.gameObject:SetActive(true)
	self.TopTabBar.OnTabChange = CommonDefs.CombineListner_Action_GameObject_int(self.TopTabBar.OnTabChange, DelegateFactory.Action_GameObject_int(function(go, index)
        self:OnTopTabChange(go, index)
    end), true)

	self.RightTabBar.OnTabChange = CommonDefs.CombineListner_Action_GameObject_int(self.RightTabBar.OnTabChange, DelegateFactory.Action_GameObject_int(function(go, index)
        self:OnRightTabChange(go, index)
    end), true)
end
--#region Event Listenner
function LuaRuYiDongSignWnd:OnEnable()
end

--#endregion

function LuaRuYiDongSignWnd:Init()
	self.m_DifficultyIndex = 0
	self.RightTabBar:ChangeTab(0,false)
	self.TopTabBar:ChangeTab(LuaHuLuWa2022Mgr.m_RuYiDongWndTabIndex,false)
end

function LuaRuYiDongSignWnd:InitSignView()
end

function LuaRuYiDongSignWnd:InitRankWnd()
end

function LuaRuYiDongSignWnd:OnTopTabChange(go,index)
	if index == 1 then
		self.SignView:SetActive(false)
		self.RuYiDongRankView.gameObject:SetActive(true)

		--self.RuYiDongRankView:InitRankData(self.m_DifficultyIndex)
		self.RuYiDongRankView:InitRankData()

	else
		self.SignView:SetActive(true)
		self.RuYiDongRankView.gameObject:SetActive(false)
	end
end

function LuaRuYiDongSignWnd:OnRightTabChange(go,index)
	self.m_DifficultyIndex = index
	--Refresh Boss Icon
	local setting = HuluBrothers_Setting.GetData()
	local boss = setting.RuYiDongBoss
	for i=1,4,1 do
		local npcName = boss[index*4+(i-1)]
		local path = SafeStringFormat3("UI/Texture/Portrait/Material/%s.mat",npcName)
		local texName = SafeStringFormat3("BossIcon%d",i)
		self[texName]:LoadMaterial(path)
	end
end
--@region UIEvent

function LuaRuYiDongSignWnd:OnTipBtnClick()
	g_MessageMgr:ShowMessage("YongChuangRuYiDong_Sign_Tip")
end

function LuaRuYiDongSignWnd:OnSignBtnClick()
	--請求进入如意洞副本
	Gac2Gas.RequestEnterHuLuWa(self.m_DifficultyIndex)
end

--@endregion UIEvent

