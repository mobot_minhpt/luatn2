local UILabel = import "UILabel"
local QnRadioBox = import "L10.UI.QnRadioBox"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local CItemMgr = import "L10.Game.CItemMgr"
local QnButton = import "L10.UI.QnButton"
local QnTabView = import "L10.UI.QnTabView"
local PopupMenuItemData=import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr=import "L10.UI.CPopupMenuInfoMgr"
local AlignType2=import "L10.UI.CPopupMenuInfoMgr+AlignType"
local CFreightEquipSubmitMgr = import "L10.UI.CFreightEquipSubmitMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CChatLinkMgr = import "CChatLinkMgr"
local GestureRecognizer = import "L10.Engine.GestureRecognizer"
local GestureType = import "L10.Engine.GestureType"
local Input = import "UnityEngine.Input"
local Screen = import "UnityEngine.Screen"
local EnumGender = import "L10.Game.EnumGender"

CLuaRanFaMainWnd = class()
CLuaRanFaMainWnd.Path = "ui/ranfa/LuaRanFaMainWnd"
RegistClassMember(CLuaRanFaMainWnd, "m_RecipeSelectedSpriteTable")
RegistClassMember(CLuaRanFaMainWnd, "m_RecipeIndex")
RegistClassMember(CLuaRanFaMainWnd, "m_OwnRecipeTable")
RegistClassMember(CLuaRanFaMainWnd, "m_CurrentRecipeNameLabel")
RegistClassMember(CLuaRanFaMainWnd, "m_CurrentRecipeDescLabel")
RegistClassMember(CLuaRanFaMainWnd, "m_RecipeOperateLabel")
RegistClassMember(CLuaRanFaMainWnd, "m_RecipeOperateQnButton")

RegistClassMember(CLuaRanFaMainWnd, "m_RecipeModelCameraName")
RegistClassMember(CLuaRanFaMainWnd, "m_RecipeModelTexture")
RegistClassMember(CLuaRanFaMainWnd, "m_RecipeModelRO")
RegistClassMember(CLuaRanFaMainWnd, "m_RecipeModelAppear")
RegistClassMember(CLuaRanFaMainWnd, "m_RecipeModelRotation")
RegistClassMember(CLuaRanFaMainWnd, "m_RecipeInited")

RegistClassMember(CLuaRanFaMainWnd, "m_HairModelCameraName")
RegistClassMember(CLuaRanFaMainWnd, "m_HairModelTexture")
RegistClassMember(CLuaRanFaMainWnd, "m_HairModelRO")
RegistClassMember(CLuaRanFaMainWnd, "m_HairModelAppear")
RegistClassMember(CLuaRanFaMainWnd, "m_HairModelRotation")
RegistClassMember(CLuaRanFaMainWnd, "m_HairGridView")
RegistClassMember(CLuaRanFaMainWnd, "m_HairInited")

RegistClassMember(CLuaRanFaMainWnd, "m_RanFaJiTemplateIdTable")

RegistClassMember(CLuaRanFaMainWnd, "m_QnTabView")

RegistClassMember(CLuaRanFaMainWnd, "m_OnPinchInDelegate")
RegistClassMember(CLuaRanFaMainWnd, "m_OnPinchOutDelegate")
RegistClassMember(CLuaRanFaMainWnd, "m_CurrentPinchDelta")
RegistClassMember(CLuaRanFaMainWnd, "m_HeadPreviewPos")
RegistClassMember(CLuaRanFaMainWnd, "m_FullPreviewPos")
RegistClassMember(CLuaRanFaMainWnd, "m_XianFanRadioBox")

function CLuaRanFaMainWnd:Awake()
    --Gac2Gas.RequestSyncRanFaJiPlayData()
    self.m_QnTabView = self.transform:Find("QnTabView"):GetComponent(typeof(QnTabView))
    self.m_QnTabView.OnSelect = DelegateFactory.Action_QnTabButton_int(function (go, index)
		if index == 1 then
            self:InitLibrary()
            self:RefreshLibrary()
            self:SyncAppearance()
            CLuaRanFaMgr:CheckHeadFashion()
        end
    end)
end

function CLuaRanFaMainWnd:SyncAppearance()
    self.m_HairModelAppear = CClientMainPlayer.Inst.AppearanceProp:Clone()
    if self.m_HairModelAppear.Gender == EnumGender.Monster then
        self.m_HairModelAppear.YingLingState = 0
    end
    if self.m_XianFanRadioBox then
        self.m_HairModelAppear.FeiShengAppearanceXianFanStatus = self.m_XianFanRadioBox.CurrentSelectIndex == 0 and 1 or 0
    end
    CClientMainPlayer.LoadResource(self.m_HairModelRO, self.m_HairModelAppear, true, 1, 0, false, 0, true, 0, false, nil)
end

function CLuaRanFaMainWnd:InitRecipe()
    if self.m_RecipeInited then return end
    self.m_RecipeInited = true
    self.m_CurrentRecipeNameLabel = self.transform:Find("QnTabView/Recipe/Right/RecipeLabel"):GetComponent(typeof(UILabel))
    self.m_CurrentRecipeDescLabel = self.transform:Find("QnTabView/Recipe/Right/DescLabel"):GetComponent(typeof(UILabel))
    local operateBtn = self.transform:Find("QnTabView/Recipe/Right/OperateBtn")
    self.m_RecipeOperateQnButton = operateBtn:GetComponent(typeof(QnButton))
    self.m_RecipeOperateLabel = operateBtn:Find("Label"):GetComponent(typeof(UILabel))
    UIEventListener.Get(operateBtn.gameObject).onClick = LuaUtils.VoidDelegate(function ()
        self:RecipeOperate()
    end)

    -- Init preview recipe
    local interface = LuaDefaultModelTextureLoader.Create(function (ro)
        if not CClientMainPlayer.Inst then return end
        local appear = CClientMainPlayer.Inst.AppearanceProp:Clone()
        self.m_RecipeModelRO = ro
        -- 不是拓本
        if appear.HeadFashionId > 0 and not EquipmentTemplate_Equip.GetData(appear.HeadFashionId) then
            appear.HeadFashionId = 0
        end
        if appear.Gender == EnumGender.Monster then
            appear.YingLingState = 0
        end
        self.m_RecipeModelAppear = appear
        CClientMainPlayer.LoadResource(ro, appear, true, 1, 0, false, 0, true, 0, false, nil)
    end)
    self.m_RecipeModelCameraName = "__RanFaMainWndRecipe__"
    self.m_RecipeModelTexture = self.transform:Find("QnTabView/Recipe/Right/ModelTexture"):GetComponent(typeof(UITexture))
    self.m_RecipeModelRotation = 180
    self.m_HeadPreviewPos = Vector3(0, -1.69, 1)
    if CClientMainPlayer.Inst then
        local gender = CClientMainPlayer.Inst.AppearanceGender
        if gender == EnumGender.Monster then
            gender = CClientMainPlayer.Inst.Gender
        end
        local data = RanFaJi_Preview.GetData(EnumToInt(CClientMainPlayer.Inst.Class) * 100 + EnumToInt(gender))
        if data then
            local y, z = data.PreviewOffset:match("([^,]+),([^,]+)")
            self.m_HeadPreviewPos = Vector3(0, tonumber(y), tonumber(z))
        end
    end
    self.m_FullPreviewPos = Vector3(0, -1, 4.66)
    self.m_RecipeModelTexture.mainTexture = CUIManager.CreateModelTexture(self.m_RecipeModelCameraName, interface, self.m_RecipeModelRotation, self.m_HeadPreviewPos.x, self.m_HeadPreviewPos.y, self.m_HeadPreviewPos.z, false, false, 1.0, false)
    local leftBtn = self.m_RecipeModelTexture.transform:Find("LeftBtn").gameObject
    local rightBtn = self.m_RecipeModelTexture.transform:Find("RightBtn").gameObject
    UIEventListener.Get(leftBtn).onClick = LuaUtils.VoidDelegate(function ()
        self.m_RecipeModelRotation = self.m_RecipeModelRotation - 10
        CUIManager.SetModelRotation(self.m_RecipeModelCameraName, self.m_RecipeModelRotation)
    end)
    UIEventListener.Get(rightBtn).onClick = LuaUtils.VoidDelegate(function ()
        self.m_RecipeModelRotation = self.m_RecipeModelRotation + 10
        CUIManager.SetModelRotation(self.m_RecipeModelCameraName, self.m_RecipeModelRotation)
    end)
    UIEventListener.Get(self.m_RecipeModelTexture.gameObject).onDrag = LuaUtils.VectorDelegate(function(go, delta)
        local dd = -delta.x / Screen.width * 360
        self.m_RecipeModelRotation = self.m_RecipeModelRotation + dd
        CUIManager.SetModelRotation(self.m_RecipeModelCameraName, self.m_RecipeModelRotation)
    end)

    local gridView = self.transform:Find("QnTabView/Recipe/AdvView"):GetComponent(typeof(QnAdvanceGridView))
    gridView.m_DataSource = DefaultTableViewDataSource.Create(function()
            return RanFaJi_RanFaJi.GetDataCount() / 4
        end, function(item, index)
            self:InitItem(item.gameObject, index)
        end)
    self.m_RecipeSelectedSpriteTable = {}
    self.m_OwnRecipeTable = {}
    self.m_RecipeIndex = 0
    gridView:ReloadData(true, false)
    local recipeId = 1
    if CLuaRanFaMgr.m_NeedShowRecipeId > 0 then
        recipeId = CLuaRanFaMgr.m_NeedShowRecipeId
        CLuaRanFaMgr.m_NeedShowRecipeId = 0
        gridView:ScrollToRow(RanFaJi_RanFaJi.GetData(recipeId).ColorSystem - 1)
    end
    self:OnRecipeClick(recipeId)
    --self.m_RecipeSelectedSpriteTable[1]:SetSelected(true, false)
end

function CLuaRanFaMainWnd:InitLibrary()
    if self.m_HairInited then return end
    self.m_HairInited = true
    local xianFanObj = self.transform:Find("QnTabView/Library/ModelTexturePanel/ModelTexture/ChangeXianFanRadioBox").gameObject
    xianFanObj:SetActive(CClientMainPlayer.Inst.HasFeiSheng and CClientMainPlayer.IsFashionSupportFeiSheng(CClientMainPlayer.Inst.AppearanceProp.HeadFashionId, CClientMainPlayer.Inst.AppearanceProp.BodyFashionId, CClientMainPlayer.Inst.AppearanceProp.HideHeadFashionEffect, CClientMainPlayer.Inst.AppearanceProp.HideBodyFashionEffect))
    if CClientMainPlayer.Inst.HasFeiSheng then
        self.m_XianFanRadioBox = xianFanObj:GetComponent(typeof(QnRadioBox))
        self.m_XianFanRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function()
            if self.m_XianFanRadioBox then
                self.m_HairModelAppear.FeiShengAppearanceXianFanStatus = self.m_XianFanRadioBox.CurrentSelectIndex == 0 and 1 or 0
            end
            CClientMainPlayer.LoadResource(self.m_HairModelRO, self.m_HairModelAppear, true, 1, 0, false, 0, true, 0, false, nil)
        end)
    end
    -- Init preview hair
    local interface2 = LuaDefaultModelTextureLoader.Create(function (ro)
        if not CClientMainPlayer.Inst then return end
        self.m_HairModelRO = ro
    	self.m_HairModelAppear = CClientMainPlayer.Inst.AppearanceProp:Clone()
        if self.m_HairModelAppear.Gender == EnumGender.Monster then
            self.m_HairModelAppear.YingLingState = 0
        end
        if self.m_XianFanRadioBox then
            self.m_XianFanRadioBox:ChangeTo(self.m_HairModelAppear.FeiShengAppearanceXianFanStatus > 0 and 0 or 1, false)
        end
        CClientMainPlayer.LoadResource(ro, self.m_HairModelAppear, true, 1, 0, false, 0, true, 0, false, nil)
	end)
    self.m_HairModelCameraName = "__RanFaMainWndHair__"
    self.m_HairModelTexture = self.transform:Find("QnTabView/Library/ModelTexturePanel/ModelTexture"):GetComponent(typeof(UITexture))
    self.m_HairModelRotation = 180
    self.m_HairModelTexture.mainTexture = CUIManager.CreateModelTexture(self.m_HairModelCameraName, interface2, self.m_HairModelRotation, 0, -1, 4.66, false, false, 1.0, false)
    local leftBtn2 = self.m_HairModelTexture.transform:Find("LeftBtn").gameObject
    local rightBtn2 = self.m_HairModelTexture.transform:Find("RightBtn").gameObject
    UIEventListener.Get(leftBtn2).onClick = LuaUtils.VoidDelegate(function ()
        self.m_HairModelRotation = self.m_HairModelRotation - 10
    	CUIManager.SetModelRotation(self.m_HairModelCameraName, self.m_HairModelRotation)
    end)
    UIEventListener.Get(rightBtn2).onClick = LuaUtils.VoidDelegate(function ()
        self.m_HairModelRotation = self.m_HairModelRotation + 10
    	CUIManager.SetModelRotation(self.m_HairModelCameraName, self.m_HairModelRotation)
    end)
    UIEventListener.Get(self.m_HairModelTexture.gameObject).onDrag = LuaUtils.VectorDelegate(function(go, delta)
        local dd = -delta.x / Screen.width * 360
        self.m_HairModelRotation = self.m_HairModelRotation + dd
        CUIManager.SetModelRotation(self.m_HairModelCameraName, self.m_HairModelRotation)
    end)

    UIEventListener.Get(self.transform:Find("QnTabView/Library/LibBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ()
        CUIManager.ShowUI(CLuaUIResources.RanFaJiBookWnd)
    end)

    self.m_HairGridView = self.transform:Find("QnTabView/Library/AdvView"):GetComponent(typeof(QnAdvanceGridView))
    self.m_HairGridView.m_DataSource = DefaultTableViewDataSource.Create(function()
            return #CLuaRanFaMgr.m_RanFaJiTable + 1
        end, function(item, index)
            self:InitRanFaJiItem(item.transform, index)
        end)
    self.m_HairGridView.OnSelectAtRow=DelegateFactory.Action_int(function (row)
    	if row >= #CLuaRanFaMgr.m_RanFaJiTable then
            self:AddRanFaJi()
            return
        end
        self:OnRanFaJiClick(row)
    end)
end

function CLuaRanFaMainWnd:RefreshLibrary()
    self.m_HairGridView:ReloadData(true, false)
end

function CLuaRanFaMainWnd:AddRanFaJi()
    if not self.m_RanFaJiTemplateIdTable then
        self.m_RanFaJiTemplateIdTable = {}
        RanFaJi_RanFaJi.Foreach(function(k, v)
            self.m_RanFaJiTemplateIdTable[v.RanFaJiItemID] = 1
        end)
    end
    local dic = CreateFromClass(MakeGenericClass(Dictionary, Int32, MakeGenericClass(List, System.String)))
    local equipList = CreateFromClass(MakeGenericClass(List, System.String))
    local bagSize = CClientMainPlayer.Inst.ItemProp:GetPlaceSize(EnumItemPlace.Bag)
    for i = 1, bagSize do
        local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
        if (not System.String.IsNullOrEmpty(id)) then
            local equip = CItemMgr.Inst:GetById(id)
            if equip and self.m_RanFaJiTemplateIdTable[equip.TemplateId] then
                 CommonDefs.ListAdd(equipList, typeof(System.String), equip.Id)
            end
        end
    end
    CommonDefs.DictAdd(dic, typeof(Int32), 0, typeof(MakeGenericClass(List, System.String)), equipList)
    CFreightEquipSubmitMgr.OpenEquipmentChooseWnd("RanFaMainWnd", dic, LocalString.GetString("包裹中的高级染发剂"), 0, false, LocalString.GetString("添加"), LocalString.GetString("获取更多染发剂"), DelegateFactory.Action(function ()
        CUIManager.CloseUI(CUIResources.FreightEquipSubmitWnd)
        self.m_QnTabView:ChangeTo(0)
    end), LocalString.GetString("包裹中暂无高级染发剂"))
end

function CLuaRanFaMainWnd:OnRanFaJiClick(row)
    local tbl = {}
    local id = CLuaRanFaMgr.m_RanFaJiTable[row + 1][1]
    local expiredTime = CLuaRanFaMgr.m_RanFaJiTable[row + 1][2]
    if id == CLuaRanFaMgr.m_MyRanFaJiId then
        table.insert(tbl, PopupMenuItemData(LocalString.GetString("褪色"), DelegateFactory.Action_int(function(index)
            CLuaRanFaMgr:TakeOffAdvancedHairColor(id)
        end), false, nil))
    --[[
    elseif expiredTime <=CServerTimeMgr.Inst.timeStamp then
        table.insert(tbl, PopupMenuItemData(LocalString.GetString("制作"), DelegateFactory.Action_int(function(index)
            CLuaRanFaJiMakeWnd.RecipeIndex = id
            CUIManager.ShowUI(CLuaUIResources.RanFaJiMakeWnd)
        end), false, nil))
        ]]
    else
        table.insert(tbl, PopupMenuItemData(LocalString.GetString("染色"), DelegateFactory.Action_int(function(index)
            CLuaRanFaMgr:TakeOnAdvancedHairColor(id)
        end), false, nil))
    end
    --[[
    table.insert(tbl, PopupMenuItemData(LocalString.GetString("丢弃"), DelegateFactory.Action_int(function(index)
        Gac2Gas.DiscardAdvancedHairColor(id)
    end), false, nil))]]
    CPopupMenuInfoMgr.ShowPopupMenu(Table2Array(tbl, MakeArrayClass(PopupMenuItemData)), self.m_HairGridView:GetItemAtRow(row).transform, AlignType2.Bottom, 1, nil, 600, true, 266)

    local data = RanFaJi_RanFaJi.GetData(id)
    if data then
        self.m_HairModelAppear.AdvancedHairColor = data.ColorRGB
        self.m_HairModelAppear.AdvancedHairColorId = id
        CClientMainPlayer.LoadResource(self.m_HairModelRO, self.m_HairModelAppear, true, 1, 0, false, 0, true, 0, false, nil)
    end
end

function CLuaRanFaMainWnd:InitRanFaJiItem(trans, index)
    -- last one
    local texObj = trans:Find("Tex").gameObject
    local emptyObj = trans:Find("Empty").gameObject
    local nameLabel = trans:Find("NameLabel"):GetComponent(typeof(UILabel))
    local outtimeLabel = trans:Find("OutTimeLabel"):GetComponent(typeof(UILabel))
    local takeonFlag = trans:Find("TakeOnFlag").gameObject
    local timeLimitLabel = trans:Find("TimeLimitLabel"):GetComponent(typeof(UILabel))
    local bLast = index == #CLuaRanFaMgr.m_RanFaJiTable
    texObj:SetActive(not bLast)
    emptyObj:SetActive(bLast)
    nameLabel.text = ""
    outtimeLabel.gameObject:SetActive(false)
    timeLimitLabel.text = ""
    takeonFlag.gameObject:SetActive(false)
    if bLast then return end
    local id = CLuaRanFaMgr.m_RanFaJiTable[index + 1][1]
    local designData = RanFaJi_RanFaJi.GetData(CLuaRanFaMgr.m_RanFaJiTable[index + 1][1])
    nameLabel.text = SafeStringFormat3(LocalString.GetString("%s染发剂"), designData.ColorName)
    --[[
    if CLuaRanFaMgr.m_RanFaJiTable[index + 1][2] > CServerTimeMgr.Inst.timeStamp then
        local dt = CServerTimeMgr.ConvertTimeStampToZone8Time(CLuaRanFaMgr.m_RanFaJiTable[index + 1][2])
        timeLimitLabel.text = SafeStringFormat3(LocalString.GetString("有效期至  %d-%d-%d,%02d:%02d"), dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute)
    else
        outtimeLabel.gameObject:SetActive(true)
    end
    ]]
    takeonFlag:SetActive(id == CLuaRanFaMgr.m_MyRanFaJiId)
    --texObj:GetComponent(typeof(CUITexture)):LoadMaterial(Item_Item.GetData(designData.RanFaJiItemID).Icon)
    texObj:GetComponent(typeof(UITexture)).color = NGUIText.ParseColor24(designData.ColorRGB, 0)
end

function CLuaRanFaMainWnd:OnDestroy()
	CUIManager.DestroyModelTexture(self.m_RecipeModelCameraName)
    CUIManager.DestroyModelTexture(self.m_HairModelCameraName)
end

function CLuaRanFaMainWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncRanFaJiPlayData", self, "SyncRanFaJiPlayData")

    self.m_OnPinchInDelegate = DelegateFactory.Action_GenericGesture(function(gesture) self:OnPinchIn(gesture) end)
    self.m_OnPinchOutDelegate = DelegateFactory.Action_GenericGesture(function(gesture) self:OnPinchOut(gesture) end)
    self.m_CurrentPinchDelta = 0
    GestureRecognizer.AddGestureListener(GestureType.OnUIPinchIn,  self.m_OnPinchInDelegate)
    GestureRecognizer.AddGestureListener(GestureType.OnUIPinchOut, self.m_OnPinchOutDelegate)
    GestureRecognizer.Inst:AddNeedUIPinchWnd(CLuaUIResources.RanFaMainWnd)
    g_ScriptEvent:AddListener("MouseScrollWheel",self,"OnMouseScrollWheel")
    g_ScriptEvent:AddListener("OnSyncMainPlayerAppearanceProp", self, "SyncAppearance")
    g_ScriptEvent:AddListener("OnFreightSubmitEquipSelected", self, "UseRanFaJi")
end

function CLuaRanFaMainWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncRanFaJiPlayData", self, "SyncRanFaJiPlayData")

    GestureRecognizer.RemoveGestureListener(GestureType.OnUIPinchIn, self.m_OnPinchInDelegate)
    GestureRecognizer.RemoveGestureListener(GestureType.OnUIPinchOut, self.m_OnPinchOutDelegate)
    g_ScriptEvent:RemoveListener("MouseScrollWheel",self,"OnMouseScrollWheel")
    g_ScriptEvent:RemoveListener("OnSyncMainPlayerAppearanceProp", self, "SyncAppearance")
    g_ScriptEvent:RemoveListener("OnFreightSubmitEquipSelected", self, "UseRanFaJi")
end

function CLuaRanFaMainWnd:UseRanFaJi(args)
    local itemId = args[0]
	local key = args[1]
    if key ~= "RanFaMainWnd" then return end
    local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, itemId)
    Gac2Gas.RequestUseItem(EnumItemPlace_lua.Bag, pos, itemId, "")
end

function CLuaRanFaMainWnd:OnMouseScrollWheel()
    self:OnPinch(Input.GetAxis("Mouse ScrollWheel"))
end

function CLuaRanFaMainWnd:OnPinchIn(gesture)
    local deltaPinch = gesture.deltaPinch
    local pinchScale = math.min(math.max(deltaPinch / 400, 0),1)
    self:OnPinch(pinchScale)
end

function CLuaRanFaMainWnd:OnPinchOut(gesture)
    local deltaPinch = gesture.deltaPinch
    local pinchScale = math.min(math.max(deltaPinch / 400, 0),1)
    self:OnPinch(-pinchScale)
end

function CLuaRanFaMainWnd:OnPinch(delta)
    self.m_CurrentPinchDelta = math.max(math.min(1, self.m_CurrentPinchDelta + delta), 0)
    CUIManager.SetModelPosition(self.m_HairModelCameraName, Vector3.Lerp(self.m_HeadPreviewPos, Vector3(0, -1, 4.66), self.m_CurrentPinchDelta))
end

function CLuaRanFaMainWnd:Init()
    self:InitRecipe()
    self:SyncRanFaJiPlayData()
    if CLuaRanFaMgr.m_OpenWndType == EnumSyncRanFaJiType.eOpenMainWndLibraryTab then
        self.m_QnTabView:ChangeTo(1)
    end
end

function CLuaRanFaMainWnd:InitItem(obj, index)
    local start = index * 4 + 1
    for i = start, start + 4 - 1 do
        local designData = RanFaJi_RanFaJi.GetData(i)
        if i == start then
            obj.transform:Find("TypeLabel"):GetComponent(typeof(UILabel)).text = designData.ColorSystemName
        end
        local rootTrans = obj.transform:Find(tostring(i - start + 1))
        rootTrans:Find("NameLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("%s染发配方"), designData.ColorName)
        rootTrans:Find("Color"):GetComponent(typeof(UITexture)).color = NGUIText.ParseColor24(designData.ColorRGB, 0)
        self.m_RecipeSelectedSpriteTable[i] = rootTrans:Find("Selected").gameObject
        self.m_RecipeSelectedSpriteTable[i]:SetActive(self.m_RecipeIndex == i)
        UIEventListener.Get(rootTrans:Find("Texture").gameObject).onClick = LuaUtils.VoidDelegate(function ()
            self:OnRecipeClick(i)
        end)
        self:UpdateRecipeGotFlag(rootTrans, self.m_OwnRecipeTable[i] ~= nil)
    end
end

function CLuaRanFaMainWnd:UpdateRecipeGotFlag(obj, bGot)
    obj.transform:Find("GotFlag").gameObject:SetActive(bGot)
end

function CLuaRanFaMainWnd:OnRecipeClick(index)
    if self.m_RecipeIndex > 0 and self.m_RecipeSelectedSpriteTable[self.m_RecipeIndex] then
        self.m_RecipeSelectedSpriteTable[self.m_RecipeIndex]:SetActive(false)
    end
    self.m_RecipeIndex = index
    self.m_RecipeSelectedSpriteTable[self.m_RecipeIndex]:SetActive(true)
    local designData = RanFaJi_RanFaJi.GetData(index)
    self.m_CurrentRecipeNameLabel.text = SafeStringFormat3(LocalString.GetString("%s染发配方"), designData.ColorName)
    self.m_CurrentRecipeDescLabel.text = CChatLinkMgr.TranslateToNGUIText(designData.FormulaDescription, false)
    self:RefreshRecipe()
    self.m_RecipeModelAppear.AdvancedHairColor = designData.ColorRGB
    self.m_RecipeModelAppear.AdvancedHairColorId = index
    CClientMainPlayer.LoadResource(self.m_RecipeModelRO, self.m_RecipeModelAppear, true, 1, 0, false, 0, true, 0, false, nil, false)
end

function CLuaRanFaMainWnd:RefreshRecipe()
    if CLuaRanFaMgr.m_OwnRanFaJiTable[self.m_RecipeIndex] then
        self.m_RecipeOperateLabel.text = LocalString.GetString("已制作")
        --self.m_RecipeOperateQnButton.Enabled = false
    elseif self.m_OwnRecipeTable[self.m_RecipeIndex] then
        self.m_RecipeOperateLabel.text = LocalString.GetString("制作染发剂")
        --self.m_RecipeOperateQnButton.Enabled = true
    else
        local ownCnt = CItemMgr.Inst:GetItemCount(RanFaJi_Setting.GetData().RanFaJiSuiPianItemID)
        local needCnt = RanFaJi_Setting.GetData().HeChengPeiFangNeedSuiPianNum
        self.m_RecipeOperateLabel.text = SafeStringFormat3(LocalString.GetString("兑换（%d/%d）"), ownCnt, needCnt)
        --self.m_RecipeOperateQnButton.Enabled = ownCnt >= needCnt
    end
end

function CLuaRanFaMainWnd:SyncRanFaJiPlayData()
    self.m_OwnRecipeTable = CLuaRanFaMgr.m_RecipeTable
    for k, v in pairs(CLuaRanFaMgr.m_RecipeTable) do
        if self.m_RecipeSelectedSpriteTable[k] then
            self:UpdateRecipeGotFlag(self.m_RecipeSelectedSpriteTable[k].transform.parent, true)
        end
    end
    self:RefreshRecipe()
    if self.m_QnTabView.CurrentSelectTab == 1 then
        self:RefreshLibrary()
    end
end

function CLuaRanFaMainWnd:RecipeOperate()
    if CLuaRanFaMgr.m_OwnRanFaJiTable[self.m_RecipeIndex] then
        g_MessageMgr:ShowMessage("RanFaJi_Have_Already_Owned")
    elseif self.m_OwnRecipeTable[self.m_RecipeIndex] then
        CLuaRanFaJiMakeWnd.RecipeIndex = self.m_RecipeIndex
        CUIManager.ShowUI(CLuaUIResources.RanFaJiMakeWnd)
    else
        local ownCnt = CItemMgr.Inst:GetItemCount(RanFaJi_Setting.GetData().RanFaJiSuiPianItemID)
        local needCnt = RanFaJi_Setting.GetData().HeChengPeiFangNeedSuiPianNum
        if ownCnt < needCnt then
            g_MessageMgr:ShowMessage("RanFaJi_RecipeFragment_NotEnough")
        else
            g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("RanFaJi_Recipe_Exchange_Confirm", RanFaJi_RanFaJi.GetData(self.m_RecipeIndex).ColorName), function ()
                Gac2Gas.ExchangeRanFaJiPeiFang(self.m_RecipeIndex)
            end, nil, nil, nil, false)
        end
    end
end
