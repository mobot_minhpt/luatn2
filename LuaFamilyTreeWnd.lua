local Vector3 = import "UnityEngine.Vector3"
local UISprite = import "UISprite"
local UInt64 = import "System.UInt64"
local UIEventListener = import "UIEventListener"
local String = import "System.String"
local FamilyTree_Setting = import "L10.Game.FamilyTree_Setting"
local FamilyRelationShip = import "L10.Game.FamilyRelationShip"
local FamilyMember = import "L10.Game.FamilyMember"
local DelegateFactory = import "DelegateFactory"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CFamilyTreeMgr = import "L10.Game.CFamilyTreeMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local LuaUtils = import "LuaUtils"
local Ease = import "DG.Tweening.Ease"
local UIWidget = import "UIWidget"
local TweenPosition = import "TweenPosition"
local TweenAlpha = import "TweenAlpha"
local TweenScale = import "TweenScale"
local UILabel = import "UILabel"
local NGUIText = import "NGUIText"
local UICommonDef = import "L10.UI.CUICommonDef"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"
local CFamilyItem=import "L10.UI.CFamilyItem"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CIMMgr = import "L10.Game.CIMMgr"
require("ui/familytree/LuaFamilyTagItem")

CLuaFamilyTreeWnd = class()
RegistChildComponent(CLuaFamilyTreeWnd , "ShiMenLevelButton", "ShiMenLevelButton", CUITexture)
RegistClassMember(CLuaFamilyTreeWnd,"HintBtn")
RegistClassMember(CLuaFamilyTreeWnd,"Stars")
RegistClassMember(CLuaFamilyTreeWnd,"Shifu_heart")
RegistClassMember(CLuaFamilyTreeWnd,"ItemPrefab")
RegistClassMember(CLuaFamilyTreeWnd,"ShifuRoot")
RegistClassMember(CLuaFamilyTreeWnd,"SelfRoot")
RegistClassMember(CLuaFamilyTreeWnd,"BrothersRoot")
RegistClassMember(CLuaFamilyTreeWnd,"ApprenticesRoot")
RegistClassMember(CLuaFamilyTreeWnd,"ClassMatesRoot")
RegistClassMember(CLuaFamilyTreeWnd,"GuestRoot")
RegistClassMember(CLuaFamilyTreeWnd,"BrotherLine")
RegistClassMember(CLuaFamilyTreeWnd,"ApprenticeLine")
RegistClassMember(CLuaFamilyTreeWnd,"ClassMateLine")
RegistClassMember(CLuaFamilyTreeWnd,"HorizonalGuestLine")
RegistClassMember(CLuaFamilyTreeWnd,"VerticalGuestLine")
RegistClassMember(CLuaFamilyTreeWnd,"TitleTexture")
RegistClassMember(CLuaFamilyTreeWnd,"SettingBtn")
RegistClassMember(CLuaFamilyTreeWnd,"SettingPanel")
RegistClassMember(CLuaFamilyTreeWnd,"self_Item")
RegistClassMember(CLuaFamilyTreeWnd,"wife_Item")
RegistClassMember(CLuaFamilyTreeWnd,"zhiji_Item")
RegistClassMember(CLuaFamilyTreeWnd,"shifu_Item")
RegistClassMember(CLuaFamilyTreeWnd,"shifu_Wife_Item")
RegistClassMember(CLuaFamilyTreeWnd,"brother_Items")
RegistClassMember(CLuaFamilyTreeWnd,"guest_Items")
RegistClassMember(CLuaFamilyTreeWnd,"apprentice_Items")
RegistClassMember(CLuaFamilyTreeWnd,"classmate_Items")

RegistClassMember(CLuaFamilyTreeWnd,"TreeRoot") --家谱节点
RegistClassMember(CLuaFamilyTreeWnd,"TagRoot")  --标签节点
RegistClassMember(CLuaFamilyTreeWnd,"TagItemRoot")  --标签Item节点
RegistClassMember(CLuaFamilyTreeWnd,"ItemRoot1")  --标签分节点1
RegistClassMember(CLuaFamilyTreeWnd,"ItemRoot2")  --标签分节点2
RegistClassMember(CLuaFamilyTreeWnd,"ItemRoot3")  --标签分节点3
RegistClassMember(CLuaFamilyTreeWnd,"TagItems")   --标签合集
RegistClassMember(CLuaFamilyTreeWnd,"TagClassTable")   --标签实例的合集，new出来的class的table

RegistClassMember(CLuaFamilyTreeWnd,"OnOpenTag")
RegistClassMember(CLuaFamilyTreeWnd,"OnOpenTree")

RegistClassMember(CLuaFamilyTreeWnd,"ReturnBtn")  --返回按钮
RegistClassMember(CLuaFamilyTreeWnd,"ShareBtn")  --分享按钮
RegistClassMember(CLuaFamilyTreeWnd,"DeLTagBtn")  --删除标签按钮
RegistClassMember(CLuaFamilyTreeWnd,"AddTagBtn")  --添加标签按钮
RegistClassMember(CLuaFamilyTreeWnd,"PopTag")   --家谱界面不断弹出的小标签
RegistClassMember(CLuaFamilyTreeWnd,"PopTagLable")   --小标签内容
RegistClassMember(CLuaFamilyTreeWnd,"MemberIcon")   --标签页人物头像
RegistClassMember(CLuaFamilyTreeWnd,"NameLabel")   --标签页人物名称
RegistClassMember(CLuaFamilyTreeWnd,"TagBtnRoot")  
RegistClassMember(CLuaFamilyTreeWnd,"ConfirmBtn")    --确认并返回按钮

RegistClassMember(CLuaFamilyTreeWnd,"RandomAngle")
RegistClassMember(CLuaFamilyTreeWnd,"m_Counter")    --popTag计数器
RegistClassMember(CLuaFamilyTreeWnd,"Radius")       --popTag漂浮半径
RegistClassMember(CLuaFamilyTreeWnd,"TagZanRank")   
RegistClassMember(CLuaFamilyTreeWnd,"ShowNum")      --显示popTag数

-- Baby
RegistClassMember(CLuaFamilyTreeWnd, "m_BabyItemObj")
RegistClassMember(CLuaFamilyTreeWnd, "m_BabyCount")

RegistClassMember(CLuaFamilyTreeWnd, "m_ZhiJiNotExpired")
RegistClassMember(CLuaFamilyTreeWnd, "m_ZhiJiSetting")

CLuaFamilyTreeWnd.Path = "ui/familytree/LuaFamilyTreeWnd"

CLuaFamilyTreeWnd.NormalHorizonalInterval = 224
CLuaFamilyTreeWnd.NormalVertialInterval = 220
CLuaFamilyTreeWnd.MateInterval = 370
CLuaFamilyTreeWnd.YellowStarSpriteName = "UI/Texture/Transparent/Material/familytreewnd_star01.mat"
CLuaFamilyTreeWnd.GreyStarSpriteName = "UI/Texture/Transparent/Material/familytreewnd_star02.mat"

function CLuaFamilyTreeWnd:Awake()
    self.TreeRoot = self.transform:Find("BG/FamilyTreeArea")    
    self.TagRoot = self.transform:Find("BG/FamilyTagArea")
    self.TagItemRoot = self.transform:Find("BG/FamilyTagArea/TagItemRoot")   
    self.TagItems = self.transform:Find("BG/FamilyTagArea/TagItems")
    self.TagItems.gameObject:SetActive(false)
    self.PopTag = self.transform:Find("BG/FamilyTreeArea/SelfRoot/PopTag")
    self.PopTagLable = self.PopTag.transform:Find("TagLable"):GetComponent(typeof(UILabel))
    self.MemberIcon = self.transform:Find("BG/FamilyTagArea/IconTexture"):GetComponent(typeof(CUITexture))
    self.NameLabel = self.transform:Find("BG/FamilyTagArea/NameLabel"):GetComponent(typeof(UILabel))
    self.PopTag.gameObject:SetActive(false)
    self.PopTag:GetComponent(typeof(TweenPosition)).enabled = false
    self.PopTag:GetComponent(typeof(TweenAlpha)).enabled = false
    self.PopTag:GetComponent(typeof(TweenScale)).enabled = false
    self.TagRoot.transform:GetComponent(typeof(UIWidget)).alpha = 0
    self.TagRoot.gameObject:SetActive(false)
    self.ReturnBtn = self.transform:Find("BG/FamilyTagArea/ReturnBtn").gameObject
    self.TagBtnRoot = self.transform:Find("BG/FamilyTagArea/TagBtnRoot").gameObject
    self.ShareBtn = self.transform:Find("BG/FamilyTagArea/TagBtnRoot/ShareBtn").gameObject
    self.DeLTagBtn = self.transform:Find("BG/FamilyTagArea/TagBtnRoot/DeLTagBtn").gameObject
    self.AddTagBtn = self.transform:Find("BG/FamilyTagArea/TagBtnRoot/AddTagBtn").gameObject
    self.ConfirmBtn = self.transform:Find("BG/FamilyTagArea/ConfirmButton").gameObject
    self.TagBtnRoot:SetActive(true)
    self.ConfirmBtn:SetActive(false)

    self.HintBtn = self.transform:Find("BG/FamilyTreeArea/HintButton").gameObject
    self.Stars = {}
    for i = 1, 5 do
        table.insert(self.Stars, self.transform:Find("BG/FamilyTreeArea/Star"..i).gameObject)
    end
    self.Shifu_heart = self.transform:Find("BG/FamilyTreeArea/ShifuRoot/Sprite").gameObject
    self.ItemPrefab = self.transform:Find("BG/FamilyTreeArea/FamilyItem").gameObject
    self.ItemPrefab:SetActive(false)
    self.m_BabyItemObj = self.transform:Find("BG/FamilyTreeArea/BabyItem").gameObject
    self.m_BabyItemObj:SetActive(false)

    self.ShifuRoot = self.transform:Find("BG/FamilyTreeArea/ShifuRoot").gameObject
    self.SelfRoot = self.transform:Find("BG/FamilyTreeArea/SelfRoot").gameObject
    self.BrothersRoot = self.transform:Find("BG/FamilyTreeArea/BrothersRoot").gameObject
    self.ApprenticesRoot = self.transform:Find("BG/FamilyTreeArea/ApprenticeRoot").gameObject
    self.ClassMatesRoot = self.transform:Find("BG/FamilyTreeArea/ClassmatesRoot").gameObject
    self.GuestRoot = self.transform:Find("BG/FamilyTreeArea/GuestsRoot").gameObject
    self.BrotherLine = self.transform:Find("BG/FamilyTreeArea/BrothersRoot/Line (3)"):GetComponent(typeof(UISprite))
    self.ApprenticeLine = self.transform:Find("BG/FamilyTreeArea/ApprenticeRoot/Line (5)"):GetComponent(typeof(UISprite))
    self.ClassMateLine = self.transform:Find("BG/FamilyTreeArea/ClassmatesRoot/Line"):GetComponent(typeof(UISprite))
    self.HorizonalGuestLine = self.transform:Find("BG/FamilyTreeArea/GuestsRoot/Line"):GetComponent(typeof(UISprite))
    self.VerticalGuestLine = self.transform:Find("BG/FamilyTreeArea/GuestsRoot/Line (1)"):GetComponent(typeof(UISprite))
    self.TitleTexture = self.transform:Find("BG/FamilyTreeArea/Texture"):GetComponent(typeof(CUITexture))
    self.SettingBtn = self.transform:Find("BG/FamilyTreeArea/SettingButton").gameObject
    self.SettingPanel = self.transform:Find("SettingPanel").gameObject
    self.self_Item = self.transform:Find("BG/FamilyTreeArea/SelfRoot/SelfItem"):GetComponent(typeof(CFamilyItem))
    self.wife_Item = nil
    self.shifu_Item = nil
    self.shifu_Wife_Item = nil
    self.brother_Items = CreateFromClass(MakeGenericClass(List, CFamilyItem))
    self.guest_Items = CreateFromClass(MakeGenericClass(List, CFamilyItem))
    self.apprentice_Items = CreateFromClass(MakeGenericClass(List, CFamilyItem))
    self.classmate_Items = CreateFromClass(MakeGenericClass(List, CFamilyItem))

    self.OnOpenTag = false
    self.OnOpenTree = false
    UIEventListener.Get(self.self_Item.gameObject).onClick = DelegateFactory.VoidDelegate(function(p)
        if luaFamilyMgr.TagRpcBack and not self.OnOpenTag and not self.OnOpenTree then
            self:OpenTagArea()
        end
    end)
    UIEventListener.Get(self.ReturnBtn).onClick = DelegateFactory.VoidDelegate(function(p)
        if not self.OnOpenTag and not self.OnOpenTree then
            self:OpenTreeArea()
        end
    end)
    self.TagZanRank = FamilyTree_Setting.GetData().TagZanRank
    self.RandomAngle = 0
    self.m_Counter = 0
    self.Radius = 100
    CommonDefs.AddEventDelegate(self.PopTag:GetComponent(typeof(TweenPosition)).onFinished, DelegateFactory.Action(function ()  --  循环poptag，重设tag数据
        self:ReSetPopTag()
    end))
    UIEventListener.Get(self.AddTagBtn).onClick = DelegateFactory.VoidDelegate(function(p)
        -- LuaAddTagWnd
        -- local playerInfo = CFamilyTreeMgr.Instance:GetMemberLocationInfo()
        -- if CommonDefs.ListContains(playerInfo, typeof(UInt64), CClientMainPlayer.Inst.Id) then
        --     if luaFamilyMgr.TagRpcBack then
        --         CUIManager.ShowUI(CLuaUIResources.LuaAddTagWnd)
        --     end
        -- else            
        --     g_MessageMgr:ShowMessage("TAG_FAMILY_TREE_NOT_MEMBER")
        -- end
        Gac2Gas.RequestOpenAddPlayerFamilyTreeTagWnd(CFamilyTreeMgr.Instance.CenterPlayerID)
    end)

    UIEventListener.Get(self.DeLTagBtn).onClick = DelegateFactory.VoidDelegate(function(p)
        -- LuaDelTagWnd
        if luaFamilyMgr.TagRpcBack then
            if CFamilyTreeMgr.Instance.CenterPlayerID ~= CClientMainPlayer.Inst.Id then
                luaFamilyMgr.DelTagId = luaFamilyMgr.m_selfTagId
                CUIManager.ShowUI(CLuaUIResources.LuaDelTagWnd)
            else
                self.TagBtnRoot:SetActive(false)
                self.ConfirmBtn:SetActive(true)
                self:ShowItemsDel(true)
            end
        end
    end)
    
    UIEventListener.Get(self.ConfirmBtn).onClick = DelegateFactory.VoidDelegate(function(p)
        self.TagBtnRoot:SetActive(true)
        self.ConfirmBtn:SetActive(false)
        self:ShowItemsDel(false)
    end)
    UIEventListener.Get(self.ShareBtn).onClick = DelegateFactory.VoidDelegate(function(p)
        -- 截图分享
        UICommonDef.CaptureScreen(
            "screenshot",
            true,
            false,
            nil,
            DelegateFactory.Action_string_bytes(
                function(fullPath, jpgBytes)
                    ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
                end
            ),
            false
        )
    end)
    self.ShiMenLevelButton.gameObject:SetActive(false)
    UIEventListener.Get(self.ShiMenLevelButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(p)
        CUIManager.ShowUI(CLuaUIResources.ShiTuRatingTipWnd)
    end)

    self.m_ZhiJiNotExpired = CServerTimeMgr.Inst.timeStamp >= CServerTimeMgr.Inst:GetTimeStampByStr("2023-5-6 0:0") and CServerTimeMgr.Inst.timeStamp < CServerTimeMgr.Inst:GetTimeStampByStr(ZhouNianQing2023_PSZJSetting.GetData().PlayCloseTime)
    local zhiJi = CFamilyTreeMgr.Instance:GetSingleMember(FamilyRelationShip.ZhiJi)
    self.m_ZhiJiSetting = zhiJi and zhiJi.Index 
    if not self.m_ZhiJiSetting then
        if CClientMainPlayer.Inst and CFamilyTreeMgr.Instance.CenterPlayerID == CClientMainPlayer.Inst.Id then
            if CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.PlayTimes, EnumPlayTimesKey_lua.eAnniv2023FamilyTreeShowConfidantFlag) then
                self.m_ZhiJiSetting = CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eAnniv2023FamilyTreeShowConfidantFlag)
            else
                self.m_ZhiJiSetting = 2
            end
        else
            self.m_ZhiJiSetting = 2
        end
    end
end

-- Auto Generated!!
function CLuaFamilyTreeWnd:Init( )
    self:RefreshTagData()
    self.m_BabyCount = 0

    UIEventListener.Get(self.HintBtn).onClick = LuaUtils.VoidDelegate(function ( ... )
        g_MessageMgr:ShowMessage("FAMILY_TREE_TIP")
    end)
    local flag = CFamilyTreeMgr.Instance.CenterPlayerID == CClientMainPlayer.Inst.Id
    self.HintBtn:SetActive(flag)
    if CPersonalSpaceMgr.OpenPersonalSpace then
        self.SettingBtn:SetActive(flag)
    else
        self.SettingBtn:SetActive(false)
    end
    --CPersonalSpaceMgr.Inst.GetPlayerLocations(
    CommonDefs.DictClear(CFamilyTreeMgr.Instance.playerLocationDic)
    local playerInfo = CFamilyTreeMgr.Instance:GetMemberLocationInfo()
    if playerInfo ~= nil and playerInfo.Length > 0 and CPersonalSpaceMgr.OpenPersonalSpace then
        CPersonalSpaceMgr.Inst:GetPlayerLocations(playerInfo, DelegateFactory.Action_CPersonalSpace_LocationsRet(function (ret)
            if not self.HintBtn then
                return
            end
            if ret.code == 0 then
                if ret.data.Length > 0 then
                    CommonDefs.EnumerableIterate(ret.data, DelegateFactory.Action_object(function (___value)
                        local info = ___value
                        CommonDefs.DictSet(CFamilyTreeMgr.Instance.playerLocationDic, typeof(UInt64), info.RoleId, typeof(String), info.Location)
                    end))

                    self:showAll()
                end
            end
        end))
    else
        self:showAll()
    end
    UIEventListener.Get(self.SettingBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        self:InitSettingPanel()
    end)

    CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.FamilyTreeWndTags)
end

function CLuaFamilyTreeWnd:OnEnable()
    g_ScriptEvent:AddListener("SendPlayerFamilyTreeTags", self, "SendPlayerFamilyTreeTags")
    g_ScriptEvent:AddListener("TagPlayerFamilyTreeSuccess", self, "TagPlayerFamilyTreeSuccess")
    g_ScriptEvent:AddListener("FavorPlayerFamilyTreeTagSuccess", self, "FavorPlayerFamilyTreeTagSuccess")
    g_ScriptEvent:AddListener("OnSendFamilyTreeShiMenInfo", self, "OnSendFamilyTreeShiMenInfo")
    g_ScriptEvent:AddListener("UpdateTempPlayTimesWithKey", self, "OnUpdateTempPlayTimesWithKey")
    self:OnSendFamilyTreeShiMenInfo()
end

function CLuaFamilyTreeWnd:OnDisable()
    LuaShiTuMgr.m_FamilyTreeShiMenInfo = {}
    luaFamilyMgr:ClearData()
    g_ScriptEvent:RemoveListener("SendPlayerFamilyTreeTags", self, "SendPlayerFamilyTreeTags")
    g_ScriptEvent:RemoveListener("TagPlayerFamilyTreeSuccess", self, "TagPlayerFamilyTreeSuccess")    
    g_ScriptEvent:RemoveListener("FavorPlayerFamilyTreeTagSuccess", self, "FavorPlayerFamilyTreeTagSuccess")
    g_ScriptEvent:RemoveListener("OnSendFamilyTreeShiMenInfo", self, "OnSendFamilyTreeShiMenInfo")
    g_ScriptEvent:RemoveListener("UpdateTempPlayTimesWithKey", self, "OnUpdateTempPlayTimesWithKey")
end

function CLuaFamilyTreeWnd:OnUpdateTempPlayTimesWithKey(args)
    if CFamilyTreeMgr.Instance.CenterPlayerID == CClientMainPlayer.Inst.Id and args[0] == EnumPlayTimesKey_lua.eAnniv2023FamilyTreeShowConfidantFlag then
        self.m_ZhiJiSetting = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eAnniv2023FamilyTreeShowConfidantFlag) or 2
        if self.SettingPanel.activeSelf then
            self:InitSettingPanel()
        end
        self:showAll()
    end
end

function CLuaFamilyTreeWnd:OnSendFamilyTreeShiMenInfo(targetPlayerId, isBestTudi, shimenLevel, bestTudiIds)
    self.ShiMenLevelButton.gameObject:SetActive(false)
    if not LuaShiTuMgr.m_FamilyTreeShiMenInfo.shimenLevel then
        return
    end
    local data = ShiTu_MainLevel.GetData(LuaShiTuMgr.m_FamilyTreeShiMenInfo.shimenLevel)
    if data then
        self.ShiMenLevelButton.gameObject:SetActive(LuaShiTuMgr.m_OpenNewSystem)
        self.ShiMenLevelButton.transform:Find("Texture"):GetComponent(typeof(CUITexture)):LoadMaterial(data.LevelIcon)
        self:showAll()
    end
end

function CLuaFamilyTreeWnd:showAll( )
    self:showSelf()
    self:showShifu()
    self:showClassMates()
    self:showBrothers()
    self:showApprentices()
    self:showGuestsOrMaster()
    self:ShowBaby()
    self:ShowStars()
end

-- FamilyRelationShip是一个C# enum，原来没有Baby这个字段，如果ios热更新，要把这个方法patch掉
function CLuaFamilyTreeWnd:ShowBaby( ... )
    local babyRoot = self.SelfRoot.transform:Find("BabyRoot").gameObject
    local babyPosY = -60
    if self.m_ZhiJiNotExpired then
        if self.m_ZhiJiSetting == 0 then
            babyRoot:SetActive(true)
        elseif self.m_ZhiJiSetting == 1 then
            babyRoot:SetActive(false)
        else
            babyRoot:SetActive(true)
            babyPosY = 50
        end
    else
        babyRoot:SetActive(true)
    end

    Extensions.RemoveAllChildren(babyRoot.transform)
    local babyList = CFamilyTreeMgr.Instance:GetMemberList(FamilyRelationShip.Baby)
    local babyCnt = babyList.Count
    self.m_BabyCount = babyCnt
    for i = 0, babyCnt - 1 do
        local go = NGUITools.AddChild(babyRoot, self.m_BabyItemObj)
        go:SetActive(true)
        go.transform.localPosition = babyCnt == 1 and Vector3(-185, babyPosY, 0) or (i == 0 and Vector3(-235, babyPosY, 0) or Vector3(-135, babyPosY, 0))
        if babyList[i].Gender == 1 then
            go:GetComponent(typeof(CUITexture)):LoadMaterial("UI/Texture/Transparent/Material/otherbaby_girl.mat", false)
        end
        UIEventListener.Get(go).onClick = LuaUtils.VoidDelegate(function ( ... )
            if babyList[i].Index > 0 then
                CLuaOtherBabyInfoWnd.m_PlayerId = CFamilyTreeMgr.Instance.CenterPlayerID
                CLuaOtherBabyInfoWnd.m_BabyId = babyList[i].Name
                CUIManager.ShowUI(CLuaUIResources.OtherBabyInfoWnd)
            else
                g_MessageMgr:ShowMessage("FamilyTreeBabyNoHome")
            end
        end)
    end
end

function CLuaFamilyTreeWnd:showSelf( )
    local selfMember = CFamilyTreeMgr.Instance:GetSingleMember(FamilyRelationShip.Self)
    if selfMember ~= nil then
        self.self_Item.gameObject:SetActive(true)
        self.self_Item:SetMemberInfo(selfMember, false, 0, 0)
    end

    local xinIcon = self.SelfRoot.transform:Find("XinIcon").gameObject
    local zhiJiIcon = self.SelfRoot.transform:Find("ZhiJiIcon").gameObject
    local wifePosY = self.m_ZhiJiNotExpired and self.m_ZhiJiSetting == 2 and 110 or 0

    local wife = CFamilyTreeMgr.Instance:GetSingleMember(FamilyRelationShip.Wife)
    if self.wife_Item == nil then
        self.wife_Item = self:addItemToRoot(self.SelfRoot, - CLuaFamilyTreeWnd.MateInterval, wifePosY)
        self.wife_Item:GetComponent(typeof(UITexture)).material = nil
        self.wife_Item.transform:Find("CPTexture").gameObject:SetActive(true)
    end
    if wife == nil then
        wife = CFamilyTreeMgr.Instance:GetSingleMember(FamilyRelationShip.Engaged)
    end
    if wife == nil then
        wife = CreateFromClass(FamilyMember)
        wife.Relationship = FamilyRelationShip.Wife
    else
    end
    self.wife_Item:SetMemberInfo(wife, false, 0, 0)

    if self.m_ZhiJiNotExpired then
        if self.m_ZhiJiSetting == 0 then
            zhiJiIcon:SetActive(false)
            xinIcon:SetActive(true)
            LuaUtils.SetLocalPositionY(xinIcon.transform, 0)

            if self.zhiji_Item then
                self.zhiji_Item.gameObject:SetActive(false)
            end
            self.wife_Item.transform.localPosition = Vector3(- CLuaFamilyTreeWnd.MateInterval, 0, 0)
            self.wife_Item.gameObject:SetActive(true)
        elseif self.m_ZhiJiSetting == 1 then
            zhiJiIcon:SetActive(true)
            LuaUtils.SetLocalPositionY(self.SelfRoot.transform:Find("ZhiJiIcon"), 0)
            xinIcon:SetActive(false)

            self:ShowZhiJi()
            self.zhiji_Item.gameObject:SetActive(true)
            self.wife_Item.gameObject:SetActive(false)
        else
            zhiJiIcon:SetActive(true)
            LuaUtils.SetLocalPositionY(self.SelfRoot.transform:Find("ZhiJiIcon"), -70)
            xinIcon:SetActive(true)
            LuaUtils.SetLocalPositionY(xinIcon.transform, 110)

            self:ShowZhiJi()
            self.zhiji_Item.gameObject:SetActive(true)
            self.wife_Item.transform.localPosition = Vector3(- CLuaFamilyTreeWnd.MateInterval, 110, 0)
            self.wife_Item.gameObject:SetActive(true)
        end
    else
        zhiJiIcon:SetActive(false)
        xinIcon:SetActive(true)
        LuaUtils.SetLocalPositionY(xinIcon.transform, 0)
        if self.zhiji_Item then
            self.zhiji_Item.gameObject:SetActive(false)
        end
        self.wife_Item.transform.localPosition = Vector3(- CLuaFamilyTreeWnd.MateInterval, 0, 0)
        self.wife_Item.gameObject:SetActive(true)
    end
end

function CLuaFamilyTreeWnd:ShowZhiJi()
    if not CClientMainPlayer.Inst then return end
    
    if self.zhiji_Item == nil then
        self.zhiji_Item = self:addItemToRoot(self.SelfRoot, - CLuaFamilyTreeWnd.MateInterval, 0)
        self.zhiji_Item:GetComponent(typeof(UITexture)).material = nil
        self.zhiji_Item.transform:Find("ZhiJiTexture").gameObject:SetActive(true)
    end
    if self.m_ZhiJiSetting == 0 then
    elseif self.m_ZhiJiSetting == 1 then
        self.zhiji_Item.transform.localPosition = Vector3(- CLuaFamilyTreeWnd.MateInterval, 0, 0)
    else
        self.zhiji_Item.transform.localPosition = Vector3(- CLuaFamilyTreeWnd.MateInterval, -70, 0)
    end

    local zhiJi = CFamilyTreeMgr.Instance:GetSingleMember(FamilyRelationShip.ZhiJi)
    if zhiJi == nil then
        zhiJi = CreateFromClass(FamilyMember)
        zhiJi.Relationship = FamilyRelationShip.ZhiJi
    end
    self.zhiji_Item:SetMemberInfo(zhiJi, false, 0, 0)
end

function CLuaFamilyTreeWnd:showShifu( )
    local shifu = CFamilyTreeMgr.Instance:GetSingleMember(FamilyRelationShip.Shifu)
    if self.shifu_Item == nil then
        self.shifu_Item = self:addItemToRoot(self.ShifuRoot, 0, 0)
    end
    if shifu == nil then
        shifu = CFamilyTreeMgr.Instance:GetSingleMember(FamilyRelationShip.LastMaster)
        if shifu == nil then
            shifu = CreateFromClass(FamilyMember)
            shifu.Relationship = FamilyRelationShip.Shifu
        end
    end
    self.shifu_Item:SetMemberInfo(shifu, false, 0, 0)
    local shifu_wife = CFamilyTreeMgr.Instance:GetSingleMember(FamilyRelationShip.Shifu_Wife)
    if self.shifu_Wife_Item == nil then
        self.shifu_Wife_Item = self:addItemToRoot(self.ShifuRoot, 278, 0)
    end
    if shifu_wife ~= nil then
        self.shifu_Wife_Item:SetMemberInfo(shifu_wife, false, 0, 0)
        self.shifu_Wife_Item.gameObject:SetActive(true)
        self.Shifu_heart:SetActive(true)
    else
        self.shifu_Wife_Item.gameObject:SetActive(false)
        self.Shifu_heart:SetActive(false)
    end
end
function CLuaFamilyTreeWnd:showClassMates( )
    local list = CFamilyTreeMgr.Instance:GetMemberList(FamilyRelationShip.ClassMate)
    self.ClassMatesRoot:SetActive(list.Count > 0)
    CommonDefs.ListSort1(list, typeof(FamilyMember), DelegateFactory.Comparison_FamilyMember(function (x, y)
        return NumberCompareTo(x.Index, y.Index)
    end))
    local selfIndex = list:FindIndex(DelegateFactory.Predicate_FamilyMember(function (x)
        return x.PlayerID == CFamilyTreeMgr.Instance.CenterPlayerID
    end))
    if selfIndex >= 0 then
        CommonDefs.ListRemoveAt(list, selfIndex)
    end
    if list.Count > 3 then
        CommonDefs.ListRemoveRange(list, 3, list.Count - 3)
    end
    do
        local i = self.classmate_Items.Count
        while i < list.Count do
            local item = self:addItemToRoot(self.ClassMatesRoot, - i * CLuaFamilyTreeWnd.NormalHorizonalInterval, 0)
            CommonDefs.ListAdd(self.classmate_Items, typeof(CFamilyItem), item)
            i = i + 1
        end
    end

    do
        local i = 0
        while i < list.Count do
            self.classmate_Items[i]:SetMemberInfo(list[i], i < selfIndex, 0, 0)
            i = i + 1
        end
    end
    do
        local i = 0
        while i < self.classmate_Items.Count do
            self.classmate_Items[i].gameObject:SetActive(i < list.Count)
            i = i + 1
        end
    end
    self.ClassMateLine.leftAnchor.absolute = - CLuaFamilyTreeWnd.NormalHorizonalInterval * list.Count
    self.ClassMateLine:ResetAndUpdateAnchors()
end
function CLuaFamilyTreeWnd:showApprentices( )
    local list = CFamilyTreeMgr.Instance:GetMemberList(FamilyRelationShip.Apprentice)
    local apprenticeCnt = list.Count
    CommonDefs.ListSort1(list, typeof(FamilyMember), DelegateFactory.Comparison_FamilyMember(function (x, y)
        return NumberCompareTo(x.Index, y.Index)
    end))
    local lastApprenticeList = CFamilyTreeMgr.Instance:GetMemberList(FamilyRelationShip.LastApprentice)
    local lastApprenticeCnt = 0
    if lastApprenticeList ~= nil then
        if lastApprenticeList.Count >= 1 then
            CommonDefs.ListInsert(list, 0, typeof(FamilyMember), lastApprenticeList[0])
            lastApprenticeCnt = 1
        end
        if lastApprenticeList.Count >= 2 then
            CommonDefs.ListInsert(list, 0, typeof(FamilyMember), lastApprenticeList[1])
            lastApprenticeCnt = 2
        end
    end
    if list.Count == 0 then
        local mem = CreateFromClass(FamilyMember)
        mem.Relationship = FamilyRelationShip.Apprentice
        CommonDefs.ListAdd(list, typeof(FamilyMember), mem)
    end
    do
        local i = self.apprentice_Items.Count
        while i < list.Count do
            local item = self:addItemToRoot(self.ApprenticesRoot, 0, 0)
            CommonDefs.ListAdd(self.apprentice_Items, typeof(CFamilyItem), item)
            i = i + 1
        end
    end

    do
        local i = 0 local cnt = list.Count
        while i < cnt do
            self.apprentice_Items[i]:SetMemberInfo(list[i], false, i - lastApprenticeCnt, cnt - lastApprenticeCnt)
            if cnt == 1 then
                self.apprentice_Items[i].transform.localPosition = Vector3.zero
            elseif apprenticeCnt == 1 then
                self.apprentice_Items[i].transform.localPosition = Vector3((cnt - 1 - i) * (- CLuaFamilyTreeWnd.NormalHorizonalInterval), 0, 0)
            else
                self.apprentice_Items[i].transform.localPosition = Vector3((cnt - 2 - i) * (- CLuaFamilyTreeWnd.NormalHorizonalInterval), 0, 0)
            end
            i = i + 1
        end
    end
    do
        local i = 0
        while i < self.apprentice_Items.Count do
            self.apprentice_Items[i].gameObject:SetActive(i < list.Count)
            i = i + 1
        end
    end
    if list.Count == 1 then
        self.ApprenticeLine.leftAnchor.absolute = 448 --[[NormalHorizonalInterval * 2]]
        self.ApprenticeLine.rightAnchor.absolute = 448 --[[NormalHorizonalInterval * 2]]
    elseif apprenticeCnt == 1 then
        self.ApprenticeLine.leftAnchor.absolute = CLuaFamilyTreeWnd.NormalHorizonalInterval * (3 - list.Count)
        self.ApprenticeLine.rightAnchor.absolute = 448 --[[NormalHorizonalInterval * 2]]
    else
        self.ApprenticeLine.leftAnchor.absolute = CLuaFamilyTreeWnd.NormalHorizonalInterval * (4 - list.Count)
        self.ApprenticeLine.rightAnchor.absolute = 672 --[[NormalHorizonalInterval * 3]]
    end
    --switch (list.Count)
    --{
    --    case 1:
    --        ApprenticeLine.leftAnchor.absolute = NormalInterval * 2;
    --        ApprenticeLine.rightAnchor.absolute = NormalInterval * 2;
    --        break;
    --    case 2:
    --        ApprenticeLine.leftAnchor.absolute = NormalInterval * 2;
    --        ApprenticeLine.rightAnchor.absolute = NormalInterval * 3;
    --        break;
    --    case 3:
    --        ApprenticeLine.leftAnchor.absolute = NormalInterval;
    --        ApprenticeLine.rightAnchor.absolute = NormalInterval * 3;
    --        break;
    --    case 4:
    --        ApprenticeLine.leftAnchor.absolute = 0;
    --        ApprenticeLine.rightAnchor.absolute = NormalInterval * 3;
    --        break;
    --    case 5:
    --        ApprenticeLine.leftAnchor.absolute = -NormalInterval;
    --        ApprenticeLine.rightAnchor.absolute = NormalInterval*3;
    --        break;
    --    case 6:
    --        ApprenticeLine.leftAnchor.absolute = -NormalInterval*2;
    --        ApprenticeLine.rightAnchor.absolute = NormalInterval*3;
    --        break;
    --}
    self.ApprenticeLine:ResetAndUpdateAnchors()
end
function CLuaFamilyTreeWnd:showBrothers( )
    local list = CFamilyTreeMgr.Instance:GetMemberList(FamilyRelationShip.Brother)


    if list.Count == 0 then
        local mem = CreateFromClass(FamilyMember)
        mem.Relationship = FamilyRelationShip.Brother
        CommonDefs.ListAdd(list, typeof(FamilyMember), mem)
    end
    CommonDefs.ListSort1(list, typeof(FamilyMember), DelegateFactory.Comparison_FamilyMember(function (x, y)
        return NumberCompareTo(x.Index, y.Index)
    end))
    local selfIndex = list:FindIndex(DelegateFactory.Predicate_FamilyMember(function (x)
        return x.PlayerID == CFamilyTreeMgr.Instance.CenterPlayerID
    end))
    if selfIndex >= 0 then
        CommonDefs.ListRemoveAt(list, selfIndex)
    end
    do
        local i = self.brother_Items.Count
        while i < list.Count do
            local item = self:addItemToRoot(self.BrothersRoot, - i * CLuaFamilyTreeWnd.NormalHorizonalInterval, 0)
            CommonDefs.ListAdd(self.brother_Items, typeof(CFamilyItem), item)
            i = i + 1
        end
    end
    do
        local i = 0
        while i < list.Count do
            self.brother_Items[i]:SetMemberInfo(list[i], i < selfIndex, 0, 0)
            i = i + 1
        end
    end
    do
        local i = 0
        while i < self.brother_Items.Count do
            self.brother_Items[i].gameObject:SetActive(i < list.Count)
            i = i + 1
        end
    end
    self.BrotherLine.leftAnchor.absolute = - (list.Count - 1) * CLuaFamilyTreeWnd.NormalHorizonalInterval
    self.BrotherLine:ResetAndUpdateAnchors()
end
function CLuaFamilyTreeWnd:showGuestsOrMaster( )
    local list = CFamilyTreeMgr.Instance:GetMemberList(FamilyRelationShip.Guest)
    if list.Count == 0 then
        list = CFamilyTreeMgr.Instance:GetMemberList(FamilyRelationShip.HouseMaster)
    end
    local selfIndex = list:FindIndex(DelegateFactory.Predicate_FamilyMember(function (x)
        return x.PlayerID == CFamilyTreeMgr.Instance.CenterPlayerID
    end))

    if selfIndex >= 0 then
        CommonDefs.ListRemoveAt(list, selfIndex)
    end


    if list.Count == 0 then
        self.GuestRoot:SetActive(false)
        return
    else
        self.GuestRoot:SetActive(true)
    end
    CommonDefs.ListSort1(list, typeof(FamilyMember), DelegateFactory.Comparison_FamilyMember(function (x, y)
        return NumberCompareTo(x.Index, y.Index)
    end))

    do
        local i = self.guest_Items.Count
        while i < list.Count do
            local item = nil
            if i < 3 then
                item = self:addItemToRoot(self.GuestRoot, i * CLuaFamilyTreeWnd.NormalHorizonalInterval, 0)
            else
                item = self:addItemToRoot(self.GuestRoot, 448 --[[2 * NormalHorizonalInterval]], - (i - 2) * CLuaFamilyTreeWnd.NormalVertialInterval)
            end
            CommonDefs.ListAdd(self.guest_Items, typeof(CFamilyItem), item)
            i = i + 1
        end
    end
    do
        local i = 0
        while i < list.Count do
            self.guest_Items[i]:SetMemberInfo(list[i], i < selfIndex, 0, 0)
            i = i + 1
        end
    end
    do
        local i = 0
        while i < self.guest_Items.Count do
            self.guest_Items[i].gameObject:SetActive(i < list.Count)
            i = i + 1
        end
    end
    if list.Count <= 3 then
        self.HorizonalGuestLine.rightAnchor.absolute = (list.Count) * CLuaFamilyTreeWnd.NormalHorizonalInterval
        self.VerticalGuestLine.gameObject:SetActive(false)
    elseif list.Count == 4 then
        self.HorizonalGuestLine.rightAnchor.absolute = 672 --[[3 * NormalHorizonalInterval]]
        self.VerticalGuestLine.gameObject:SetActive(true)
    end
    self.HorizonalGuestLine:ResetAndUpdateAnchors()
end
function CLuaFamilyTreeWnd:ShowStars( )
    local starList = FamilyTree_Setting.GetData().StarRelationship
    local memberCount = CFamilyTreeMgr.Instance:GetCurrentFamilyCount()
    --孩子不算人数
    memberCount = memberCount - self.m_BabyCount

    local s = 0
    do
        local i = 0
        while i < starList.Length do
            if memberCount > starList[i] then
                s = s + 1
            else
                break
            end
            i = i + 1
        end
    end
    do
        local i = 1
        while i <= #self.Stars do
            if i <= s + 1 then
                CommonDefs.GetComponent_GameObject_Type(self.Stars[i], typeof(CUITexture)):LoadMaterial(CLuaFamilyTreeWnd.YellowStarSpriteName)
            else
                CommonDefs.GetComponent_GameObject_Type(self.Stars[i], typeof(CUITexture)):LoadMaterial(CLuaFamilyTreeWnd.GreyStarSpriteName)
            end
            i = i + 1
        end
    end
    self.TitleTexture:LoadMaterial(CFamilyTreeMgr.Instance:GetTitleTexturePath(s))
end
function CLuaFamilyTreeWnd:SendPrivacySetting( )
    --GameObject option1Sign = SettingPanel.transform.FindChild("node/Option1/click/node").gameObject;
    --if (option1Sign.activeSelf)
    --{
    --    Gac2Gas.RequestChangeShareLBSLocationFlag(1);
    --    CFamilyTreeMgr.Instance.setPlayerLocationShare = false;
    --}
    --else
    --{
    --    Gac2Gas.RequestChangeShareLBSLocationFlag(0);
    --    CFamilyTreeMgr.Instance.setPlayerLocationShare = true;
    --}
end
function CLuaFamilyTreeWnd:SetOptionNode( node)
    if node.activeSelf then
        node:SetActive(false)
    else
        node:SetActive(true)
    end

    self:SendPrivacySetting()
end
function CLuaFamilyTreeWnd:InitSettingPanel()
    self.SettingPanel:SetActive(true)
    local backBtn = self.SettingPanel.transform:Find("darkbg").gameObject
    UIEventListener.Get(backBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        self.SettingPanel:SetActive(false)
    end)

    --GameObject option1Btn = SettingPanel.transform.FindChild("node/Option1/click").gameObject;
    --GameObject option1Sign = SettingPanel.transform.FindChild("node/Option1/click/node").gameObject;
    --if (CFamilyTreeMgr.Instance.setPlayerLocationShare)
    --{
    --    option1Sign.SetActive(false);
    --}
    --else
    --{
    --    option1Sign.SetActive(true);
    --}
    --UIEventListener.Get(option1Btn).onClick = p =>
    --{
    --    SetOptionNode(option1Sign);
    --};
    -- CClientMainPlayer.Inst.PlayProp.GameSetting.FamilyTreeHideLastApprentice;
    local option2Btn = self.SettingPanel.transform:Find("node/Grid/Option2/click").gameObject
    local option2Sign = self.SettingPanel.transform:Find("node/Grid/Option2/click/node").gameObject
    option2Sign:SetActive(CClientMainPlayer.Inst.PlayProp.GameSetting.FamilyTreeHideLastMaster == 1)
    UIEventListener.Get(option2Btn).onClick = DelegateFactory.VoidDelegate(function (p)
        if option2Sign.activeSelf then
            option2Sign:SetActive(false)
            CClientMainPlayer.Inst.PlayProp.GameSetting.FamilyTreeHideLastMaster = 0
            Gac2Gas.RequestSetFamilyTreeSetting(8, 0)
        else
            option2Sign:SetActive(true)
            CClientMainPlayer.Inst.PlayProp.GameSetting.FamilyTreeHideLastMaster = 1
            Gac2Gas.RequestSetFamilyTreeSetting(8, 1)
        end
        self:showAll()
    end)

    local option3Btn = self.SettingPanel.transform:Find("node/Grid/Option3/click").gameObject
    local option3Sign = self.SettingPanel.transform:Find("node/Grid/Option3/click/node").gameObject
    option3Sign:SetActive(CClientMainPlayer.Inst.PlayProp.GameSetting.FamilyTreeHideLastApprentice == 1)
    UIEventListener.Get(option3Btn).onClick = DelegateFactory.VoidDelegate(function (p)
        if option3Sign.activeSelf then
            option3Sign:SetActive(false)
            CClientMainPlayer.Inst.PlayProp.GameSetting.FamilyTreeHideLastApprentice = 0
            Gac2Gas.RequestSetFamilyTreeSetting(9, 0)
        else
            option3Sign:SetActive(true)
            CClientMainPlayer.Inst.PlayProp.GameSetting.FamilyTreeHideLastApprentice = 1
            Gac2Gas.RequestSetFamilyTreeSetting(9, 1)
        end
        self:showAll()
    end)

    if self.m_ZhiJiNotExpired then
        --0, 1, 2, 默认=2
        local sign = {
            self.SettingPanel.transform:Find("node/Grid/Option4/click/node").gameObject,
            self.SettingPanel.transform:Find("node/Grid/Option5/click/node").gameObject,
            self.SettingPanel.transform:Find("node/Grid/Option6/click/node").gameObject,
        }
        for i = 0, 2 do
            local Btn = self.SettingPanel.transform:Find("node/Grid/Option"..(i + 4).."/click").gameObject
            Btn.transform.parent.gameObject:SetActive(true)
            local Sign = sign[i + 1]
            Sign:SetActive(self.m_ZhiJiSetting == i)
            UIEventListener.Get(Btn).onClick = DelegateFactory.VoidDelegate(function (p)
                if not Sign.activeSelf then
                    Gac2Gas.Anniv2023PSZJ_SetFamilyTreeFlag(i)
                end
            end)
        end

        self.SettingPanel.transform:Find("node"):GetComponent(typeof(UISprite)).height = 500
        LuaUtils.SetLocalPositionY(self.SettingPanel.transform:Find("node/title"), 200)
        LuaUtils.SetLocalPositionY(self.SettingPanel.transform:Find("node/Grid"), 125)
        LuaUtils.SetLocalPositionY(self.SettingPanel.transform:Find("node"), -125)
        self.SettingPanel.transform:Find("node/Grid"):GetComponent(typeof(UIGrid)):Reposition()
    end
end

function CLuaFamilyTreeWnd:addItemToRoot(root, xOffset, yOffset)
    local go = NGUITools.AddChild(root, self.ItemPrefab)
    go.transform.localPosition = Vector3(xOffset, yOffset, 0)
    go:SetActive(true)
    return go:GetComponent(typeof(CFamilyItem))
end

function CLuaFamilyTreeWnd:GetGuideGo(methodName)
    if(methodName == "GetSelfButton")then
        return self.self_Item.gameObject
    end
end

--标签界面
function CLuaFamilyTreeWnd:OpenTagArea()
    self.OnOpenTag = true
    LuaTweenUtils.SetEase(LuaTweenUtils.TweenAlpha(self.TreeRoot.transform:GetComponent(typeof(UIWidget)), 1,0, 0.3,function()
        self.TagRoot.gameObject:SetActive(true)
        LuaTweenUtils.SetEase(LuaTweenUtils.TweenAlpha(self.TagRoot.transform:GetComponent(typeof(UIWidget)), 0,1, 0.5,function()
            self.OnOpenTag = false
        end), Ease.OutQuint)
        LuaTweenUtils.SetEase(LuaTweenUtils.TweenScale(self.TagItemRoot.transform,Vector3(0.3,0.3,0), Vector3.one, 0.5), Ease.OutQuint)  
    end), Ease.OutQuint)   
end

function CLuaFamilyTreeWnd:OpenTreeArea()
    self.OnOpenTree = true
    LuaTweenUtils.SetEase(LuaTweenUtils.TweenAlpha(self.TagRoot.transform:GetComponent(typeof(UIWidget)), 1,0, 0.3,function()
        self.TagRoot.gameObject:SetActive(false)
        self:RefreshTagInfo()
        LuaTweenUtils.SetEase(LuaTweenUtils.TweenAlpha(self.TreeRoot.transform:GetComponent(typeof(UIWidget)), 0,1, 0.5,function()
            self.OnOpenTree = false
        end), Ease.OutQuint)
    end), Ease.OutQuint)  
    LuaTweenUtils.SetEase(LuaTweenUtils.TweenScale(self.TagItemRoot.transform,Vector3.one,Vector3(0,0,0), 0.5), Ease.OutQuint)   
end

function CLuaFamilyTreeWnd:RefreshTagData()
    luaFamilyMgr:ClearData()
    luaFamilyMgr.TagRpcBack =false
    Gac2Gas.QueryPlayerFamilyTreeTags(CFamilyTreeMgr.Instance.CenterPlayerID,"")
end

--标签修改信息
function CLuaFamilyTreeWnd:TagPlayerFamilyTreeSuccess()
    LuaTweenUtils.SetEase(LuaTweenUtils.TweenAlpha(self.TagRoot.transform:GetComponent(typeof(UIWidget)), 0,1, 0.5), Ease.OutQuint)
    LuaTweenUtils.SetEase(LuaTweenUtils.TweenScale(self.TagItemRoot.transform,Vector3(0.3,0.3,0), Vector3.one, 0.5), Ease.OutQuint)  
    self:RefreshTagItemRoot()   --刷新TagItem
    self:RefreshPop()           --刷新pop泡泡状态
    self:RefreshBtnState()      --刷新删除按钮状态
end

--查询标签信息
function CLuaFamilyTreeWnd:SendPlayerFamilyTreeTags()
    self:RefreshTagInfo()
end

function CLuaFamilyTreeWnd:RefreshTagInfo()
    if luaFamilyMgr.TagCenterPlayerID ~= CFamilyTreeMgr.Instance.CenterPlayerID then 
        self:RefreshTagData()
    end
    self:RefreshPop()           --刷新pop泡泡状态
    self:RefreshBtnState()      --刷新删除按钮状态
    self:RefreshMemberIcon()    --刷新人物头像
    self:RefreshTagItemRoot()   --刷新TagItem
end

function CLuaFamilyTreeWnd:RefreshTagItemRoot()
    self.TagClassTable = {}
    self.ItemRoot1 = {}
    self.ItemRoot2 = {}
    self.ItemRoot3 = {}
    for i=0,self.TagItemRoot:Find("Root1").childCount-1 do                     --获取TagRoot下节点
        local child = self.TagItemRoot:Find("Root1"):GetChild(i)
        Extensions.RemoveAllChildren(child)
        table.insert( self.ItemRoot1, child)
    end
    for i=0,self.TagItemRoot:Find("Root2").childCount-1 do
        local child = self.TagItemRoot:Find("Root2"):GetChild(i)
        Extensions.RemoveAllChildren(child)
        table.insert( self.ItemRoot2, child)
    end
    for i=0,self.TagItemRoot:Find("Root3").childCount-1 do
        local child = self.TagItemRoot:Find("Root3"):GetChild(i)
        Extensions.RemoveAllChildren(child)
        table.insert( self.ItemRoot3, child)
    end
    local root1Len = #self.ItemRoot1
    local root2Len = #self.ItemRoot2
    -- LuaFamilyTagItem
    for i = 1,#luaFamilyMgr.m_TagData do
        if i <= root1Len then
            self:RandomSet(self.ItemRoot1,i)
        elseif i >root1Len and i <= root1Len+root2Len then
            self:RandomSet(self.ItemRoot2,i)
        else
            self:RandomSet(self.ItemRoot3,i)
        end
    end
end

function CLuaFamilyTreeWnd:RandomSet(root,i)
    local index = math.random(#root)
    local trans = root[index]
    table.remove(root,index)

    local LuaFamilyTagItem = LuaFamilyTagItem:new()
    LuaFamilyTagItem:Init(trans,i,self.TagItems,function()
        luaFamilyMgr.DelTagId = luaFamilyMgr.m_TagData[i].tagId
    end)
    self.TagClassTable[luaFamilyMgr.m_TagData[i].tagId] = LuaFamilyTagItem
end

function CLuaFamilyTreeWnd:ShowItemsDel(show)
    if self.TagClassTable then
        for k,v in pairs(self.TagClassTable) do
            v:ShowDel(show)
        end
    end
end

function CLuaFamilyTreeWnd:FavorPlayerFamilyTreeTagSuccess(playerId, tagId, favor, favorNum)
    if playerId == CFamilyTreeMgr.Instance.CenterPlayerID then
        self.TagClassTable[tagId]:RefreshTag(favor)
    end
end

function CLuaFamilyTreeWnd:RefreshMemberIcon()
    local selfMember = CFamilyTreeMgr.Instance:GetSingleMember(FamilyRelationShip.Self)
    local pname = CUICommonDef.GetPortraitName(selfMember.Clazz, selfMember.Gender, selfMember.Expression)
    self.MemberIcon:LoadNPCPortrait(pname);
    self.NameLabel.text = selfMember.Name
end

function CLuaFamilyTreeWnd:RefreshBtnState()
    if CFamilyTreeMgr.Instance.CenterPlayerID == CClientMainPlayer.Inst.Id then
        self.DeLTagBtn:SetActive(#luaFamilyMgr.m_TagData ~= 0)
    else
        self.DeLTagBtn:SetActive(luaFamilyMgr.m_selfTagId ~= "")
    end
    self.TagBtnRoot:SetActive(true)
    self.ConfirmBtn:SetActive(false)
end

function CLuaFamilyTreeWnd:RefreshPop()
    self.PopTag.gameObject:SetActive(true)
    self.ShowNum = 0    --用来记录应该显示的次数
    self.m_Counter = 0  --用来记录poptag已播放的次数
    if self.TagZanRank > #luaFamilyMgr.m_TagData then
        self.ShowNum = #luaFamilyMgr.m_TagData
    else
        self.ShowNum = self.TagZanRank
    end
    self:ReSetPopTag()
    if  #luaFamilyMgr.m_TagData ~= 0 and luaFamilyMgr.TagRpcBack then
        self.PopTag.gameObject:SetActive(true)
        self.PopTag:GetComponent(typeof(TweenPosition)).enabled = true
        self.PopTag:GetComponent(typeof(TweenAlpha)).enabled = true
        self.PopTag:GetComponent(typeof(TweenScale)).enabled = true
    else
        self.PopTag:GetComponent(typeof(TweenPosition)).enabled = false
        self.PopTag:GetComponent(typeof(TweenAlpha)).enabled = false
        self.PopTag:GetComponent(typeof(TweenScale)).enabled = false
        self.PopTag.gameObject:SetActive(false)
    end
end

--循环播放标签泡泡
function CLuaFamilyTreeWnd:ReSetPopTag()
    if self.ShowNum ~= 0 and #luaFamilyMgr.m_TagData ~= 0 and luaFamilyMgr.TagRpcBack then
        local Num = self.m_Counter % self.ShowNum + 1
        self.RandomAngle = (self.RandomAngle + math.random(60, 180))%360
        local x = math.cos( self.RandomAngle ) * self.Radius
        local y = math.sin( self.RandomAngle ) * self.Radius
        self.PopTag:GetComponent(typeof(TweenPosition)).to = Vector3(x, y)
        self.PopTagLable.text = luaFamilyMgr.m_TagData[Num].tagName
        local HashCode = CommonDefs.GetHashCode(self.PopTagLable.text)
        self.PopTagLable.color = NGUIText.ParseColor24(luaFamilyMgr.ColorTable[HashCode%#luaFamilyMgr.ColorTable + 1], 0)
        self.m_Counter = self.m_Counter + 1
        if self.m_Counter == self.ShowNum then      --  这里本来是要循环播放的，现在是播放一遍就停止
            self.ShowNum = 0
        end
    else
        self.PopTag:GetComponent(typeof(TweenPosition)):ResetToBeginning()
        self.PopTag:GetComponent(typeof(TweenAlpha)):ResetToBeginning()
        self.PopTag:GetComponent(typeof(TweenScale)):ResetToBeginning()
        self.PopTag.gameObject:SetActive(false)
    end
end
