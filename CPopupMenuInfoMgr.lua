-- Auto Generated!!
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CUIManager = import "L10.UI.CUIManager"


CPopupMenuInfoMgr.m_ShowPopupMenu__Vector3_AlignType_Int32_CS2LuaHook = function (items, nguiWorldPos, type, columns) 
    CPopupMenuInfoMgr.items = items
    CPopupMenuInfoMgr.defaultSelectedIndex = - 1
    CPopupMenuInfoMgr.nguiWorldPos = nguiWorldPos
    CPopupMenuInfoMgr.alignType = type
    CPopupMenuInfoMgr.columnCount = columns
    CPopupMenuInfoMgr.itemColors = nil
    CPopupMenuInfoMgr.CloseCallBack = nil
    CPopupMenuInfoMgr.closeWndWhenClicked = true
    CPopupMenuInfoMgr.buttonWidth = 296
    CUIManager.ShowUI(CIndirectUIResources.PopupMenu)
end
