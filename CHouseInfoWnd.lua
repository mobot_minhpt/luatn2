-- Auto Generated!!
local Boolean = import "System.Boolean"
local Byte = import "System.Byte"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CGarden = import "L10.Game.CGarden"
local CHouseInfoWnd = import "L10.UI.CHouseInfoWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local CProgressItemMgr = import "L10.UI.CProgressItemMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CStealProp = import "L10.Game.CStealProp"
local CTickMgr = import "L10.Engine.CTickMgr"
local CTooltip = import "L10.UI.CTooltip"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DateTime = import "System.DateTime"
local DelegateFactory = import "DelegateFactory"
local Double = import "System.Double"
local EHouseProgress = import "L10.Game.EHouseProgress"
local EnumChineseDigits = import "L10.Game.EnumChineseDigits"
local EnumEventType = import "EnumEventType"
local EProgressItemType = import "L10.UI.EProgressItemType"
local ETickType = import "L10.Engine.ETickType"
local EventManager = import "EventManager"
local Gac2Gas = import "L10.Game.Gac2Gas"
local House_CangkuUpgrade = import "L10.Game.House_CangkuUpgrade"
local House_Crop = import "L10.Game.House_Crop"
local Int32 = import "System.Int32"
local LoadPicMgr = import "L10.Game.LoadPicMgr"
local LocalString = import "LocalString"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local UInt64 = import "System.UInt64"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local Zhuangshiwu_Setting = import "L10.Game.Zhuangshiwu_Setting"
CHouseInfoWnd.m_OnEnable_CS2LuaHook = function (this) 
    this.BtnRename:SetActive(false)
    this.OpenShineiBtn:SetSelected(false, true)

    --if(CClientHouseMgr.Inst.mBasicProp == null || CClientHouseMgr.Inst.mCommunityId == -1)
    Gac2Gas.QueryHouseData()

    this:ShowHouseInfo()

    if LoadPicMgr.EnableHousePicShare then
        this.HouseShareBtn:SetActive(true)
        UIEventListener.Get(this.HouseShareBtn).onClick = MakeDelegateFromCSFunction(this.OnHouseShareBtnClick, VoidDelegate, this)
    else
        this.HouseShareBtn:SetActive(false)
    end

    UIEventListener.Get(this.BtnAddProgress).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.BtnAddProgress).onClick, MakeDelegateFromCSFunction(this.OnAddProgressClicked, VoidDelegate, this), true)
    UIEventListener.Get(this.BtnGoHome).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.BtnGoHome).onClick, MakeDelegateFromCSFunction(this.OnGoHomeClicked, VoidDelegate, this), true)
    UIEventListener.Get(this.BtnConstructHouse).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.BtnConstructHouse).onClick, MakeDelegateFromCSFunction(this.OnConstructHouseClicked, VoidDelegate, this), true)
    UIEventListener.Get(this.BtnRename).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.BtnRename).onClick, MakeDelegateFromCSFunction(this.OnRenameClick, VoidDelegate, this), true)
    UIEventListener.Get(this.BtnXiangfang).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.BtnXiangfang).onClick, MakeDelegateFromCSFunction(this.OnBtnXiangfangClick, VoidDelegate, this), true)
    UIEventListener.Get(this.LabelQishu.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.LabelQishu.gameObject).onClick, MakeDelegateFromCSFunction(this.OnLabelClick, VoidDelegate, this), true)
    UIEventListener.Get(this.LabelLingqi.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.LabelLingqi.gameObject).onClick, MakeDelegateFromCSFunction(this.OnLabelClick, VoidDelegate, this), true)
    UIEventListener.Get(this.LabelLingqiHuifu.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.LabelLingqiHuifu.gameObject).onClick, MakeDelegateFromCSFunction(this.OnLabelClick, VoidDelegate, this), true)
    UIEventListener.Get(this.ExtraProductBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.ExtraProductBtn).onClick, MakeDelegateFromCSFunction(this.OnExtraProductBtnClick, VoidDelegate, this), true)
    this.OpenShineiBtn.OnValueChanged = MakeDelegateFromCSFunction(this.OnOpenShineiValueChanged, MakeGenericClass(Action1, Boolean), this)

    EventManager.AddListener(EnumEventType.OnSendHouseData, MakeDelegateFromCSFunction(this.OnSendHouseData, Action0, this))
    EventManager.AddListener(EnumEventType.OnUpdateHouseProgress, MakeDelegateFromCSFunction(this.OnUpdateHouseProgress, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.OnUpdateHouseResource, MakeDelegateFromCSFunction(this.OnUpdateHouseResource, MakeGenericClass(Action3, Double, Double, Double), this))
    EventManager.AddListenerInternal(EnumEventType.OnQiShuFurnitureChanged, MakeDelegateFromCSFunction(this.OnQiShuFurnitureChanged, MakeGenericClass(Action2, UInt32, String), this))
    EventManager.AddListener(EnumEventType.OnUpdateHouseName, MakeDelegateFromCSFunction(this.OnUpdateHouseName, Action0, this))
    EventManager.AddListener(EnumEventType.OnUpdateExtraProductRate, MakeDelegateFromCSFunction(this.OnUpdateExtraProductRate, Action0, this))
    EventManager.AddListener(EnumEventType.OnUpdateFreeMaintainFeeDay, MakeDelegateFromCSFunction(this.ShowWeihufei, Action0, this))
    EventManager.AddListener(EnumEventType.OnChangeShineiOpenSuccess, MakeDelegateFromCSFunction(this.OnChangeShineiOpenSuccess, Action0, this))
end
CHouseInfoWnd.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.BtnAddProgress).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.BtnAddProgress).onClick, MakeDelegateFromCSFunction(this.OnAddProgressClicked, VoidDelegate, this), false)
    UIEventListener.Get(this.BtnGoHome).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.BtnGoHome).onClick, MakeDelegateFromCSFunction(this.OnGoHomeClicked, VoidDelegate, this), false)
    UIEventListener.Get(this.BtnConstructHouse).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.BtnConstructHouse).onClick, MakeDelegateFromCSFunction(this.OnConstructHouseClicked, VoidDelegate, this), false)
    UIEventListener.Get(this.BtnRename).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.BtnRename).onClick, MakeDelegateFromCSFunction(this.OnRenameClick, VoidDelegate, this), false)
    UIEventListener.Get(this.BtnXiangfang).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.BtnXiangfang).onClick, MakeDelegateFromCSFunction(this.OnBtnXiangfangClick, VoidDelegate, this), false)
    UIEventListener.Get(this.LabelQishu.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.LabelQishu.gameObject).onClick, MakeDelegateFromCSFunction(this.OnLabelClick, VoidDelegate, this), false)
    UIEventListener.Get(this.LabelLingqi.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.LabelLingqi.gameObject).onClick, MakeDelegateFromCSFunction(this.OnLabelClick, VoidDelegate, this), false)
    UIEventListener.Get(this.LabelLingqiHuifu.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.LabelLingqiHuifu.gameObject).onClick, MakeDelegateFromCSFunction(this.OnLabelClick, VoidDelegate, this), false)
    UIEventListener.Get(this.ExtraProductBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.ExtraProductBtn).onClick, MakeDelegateFromCSFunction(this.OnExtraProductBtnClick, VoidDelegate, this), false)
    this.OpenShineiBtn.OnValueChanged = nil

    EventManager.RemoveListener(EnumEventType.OnSendHouseData, MakeDelegateFromCSFunction(this.OnSendHouseData, Action0, this))
    EventManager.RemoveListener(EnumEventType.OnUpdateHouseProgress, MakeDelegateFromCSFunction(this.OnUpdateHouseProgress, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.OnUpdateHouseResource, MakeDelegateFromCSFunction(this.OnUpdateHouseResource, MakeGenericClass(Action3, Double, Double, Double), this))
    EventManager.RemoveListenerInternal(EnumEventType.OnQiShuFurnitureChanged, MakeDelegateFromCSFunction(this.OnQiShuFurnitureChanged, MakeGenericClass(Action2, UInt32, String), this))
    EventManager.RemoveListener(EnumEventType.OnUpdateHouseName, MakeDelegateFromCSFunction(this.OnUpdateHouseName, Action0, this))
    EventManager.RemoveListener(EnumEventType.OnUpdateExtraProductRate, MakeDelegateFromCSFunction(this.OnUpdateExtraProductRate, Action0, this))
    EventManager.RemoveListener(EnumEventType.OnUpdateFreeMaintainFeeDay, MakeDelegateFromCSFunction(this.ShowWeihufei, Action0, this))
    EventManager.RemoveListener(EnumEventType.OnChangeShineiOpenSuccess, MakeDelegateFromCSFunction(this.OnChangeShineiOpenSuccess, Action0, this))

    this:UnRegisterTick()
    this:UnRegisterExtraProgressTick()
end
CHouseInfoWnd.m_OnOpenShineiValueChanged_CS2LuaHook = function (this, bOpen) 
    if not CClientHouseMgr.Inst:IsInOwnHouse() then
        g_MessageMgr:ShowMessage("OPEN_HOUSE_NOT_AT_HOME")
        this.OpenShineiBtn:SetSelected(not bOpen, true)
        return
    end
    if not CClientHouseMgr.Inst:IsHouseOwner() then
        g_MessageMgr:ShowMessage("OPEN_HOUSE_NOT_OWNER")
        this.OpenShineiBtn:SetSelected(not bOpen, true)
        return
    end

    Gac2Gas.RequestChangeShineiOpen(bOpen)
end
CHouseInfoWnd.m_OnChangeShineiOpenSuccess_CS2LuaHook = function (this) 
    local mgr = CClientHouseMgr.Inst
    if mgr.mExtraProp == nil then
        return
    end
    this.OpenShineiBtn:SetSelected(mgr.mExtraProp.IsOpenShinei > 0, true)
end
CHouseInfoWnd.m_OnExtraProductBtnClick_CS2LuaHook = function (this, go) 
    CUIManager.ShowUI(CUIResources.ExtraProductWnd)
end
CHouseInfoWnd.m_OnLabelClick_CS2LuaHook = function (this, go) 
    local msg = nil
    if go == this.LabelQishu.gameObject then
        msg = "QiShu_Info"
    elseif go == this.LabelLingqi.gameObject then
        msg = "LingQi_Info"
    elseif go == this.LabelLingqiHuifu.gameObject then
        msg = "LingQi_Restore_Info"
    end

    if msg ~= nil then
        CTooltip.Show(g_MessageMgr:FormatMessage(msg), go.transform, CTooltip.AlignType.Top)
    end
end
CHouseInfoWnd.m_OnUpdateExtraProductRate_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst == nil or CClientHouseMgr.Inst.mGardenProp == nil then
        return
    end

    local time = math.floor(CServerTimeMgr.Inst.timeStamp)
    local endTime = CClientHouseMgr.Inst.mGardenProp.ExtraProductEndTime
    local totalTime = math.max(CClientHouseMgr.Inst.mGardenProp.ExtraProductTotalTime, 1)
    if endTime > time then
        local percent = (endTime - time) / totalTime
        percent = math.max(0, math.min(1, percent))
        this.ExtraProductSprite.fillAmount = percent
        this.ExtraProductSprite.gameObject:SetActive(true)

        this.ExtraProductFgSprite.spriteName = "extraproduct_02"

        this.ExtraProductFx.gameObject:SetActive(true)
        this.ExtraProductFx:LoadFx(CUIFxPaths.ExtraProductFx)
    else
        this.ExtraProductFgSprite.spriteName = "extraproduct_01"
        this.ExtraProductSprite.gameObject:SetActive(false)

        this.ExtraProductFx.gameObject:SetActive(false)
    end
end
CHouseInfoWnd.m_OnUpdateHouseName_CS2LuaHook = function (this) 
    this.LabelMingcheng.text = CClientHouseMgr.Inst:GetHouseName()
end
CHouseInfoWnd.m_OnBtnXiangfangClick_CS2LuaHook = function (this, go) 
    local zhengwuGrade = 0
    if CClientHouseMgr.Inst.mConstructProp ~= nil then
        zhengwuGrade = CClientHouseMgr.Inst.mConstructProp.ZhengwuGrade
    end
    if zhengwuGrade < Zhuangshiwu_Setting.GetData().XiangfangNeedZhengwuGrade then
        g_MessageMgr:ShowMessage("Xiangfang_Not_Open")
        return
    end
    CUIManager.ShowUI(CUIResources.CXiangfangManageWnd)
end
CHouseInfoWnd.m_OnRenameClick_CS2LuaHook = function (this, go) 
    --CUIManager.ShowUI(CUIResources.CMingyuanNameWnd);
    Gac2Gas.RequestOpenNameMyHouse()
end
CHouseInfoWnd.m_OnQiShuFurnitureChanged_CS2LuaHook = function (this, templateId, itemid) 
    this:ShowHouseInfo()
end
CHouseInfoWnd.m_OnUpdateHouseResource_CS2LuaHook = function (this, food, wood, stone) 
    local cangkuGrade = CClientHouseMgr.Inst.mConstructProp.CangkuGrade
    local cangkudata = House_CangkuUpgrade.GetData(cangkuGrade)
    local maxWood = 1000 local maxStone = 1000 local maxFood = 1000
    if cangkudata ~= nil then
        maxWood = cangkudata.MaxWood
        maxFood = cangkudata.MaxFood
        maxStone = cangkudata.MaxStone
    end
    this.LabelShiliao.text = System.String.Format(LocalString.GetString("{0}/{1}"), math.floor(stone), maxStone)
    this.LabelLiangshi.text = System.String.Format(LocalString.GetString("{0}/{1}"), math.floor(food), maxFood)
    this.LabelMucai.text = System.String.Format(LocalString.GetString("{0}/{1}"), math.floor(wood), maxWood)
end
CHouseInfoWnd.m_OnGoHomeClicked_CS2LuaHook = function (this, go) 

    CClientHouseMgr.Inst:GoBackHome()
end
CHouseInfoWnd.m_OnConstructHouseClicked_CS2LuaHook = function (this, go) 
    CUIManager.ShowUI(CUIResources.ConstructHouseWnd)
end
CHouseInfoWnd.m_OnAddProgressClicked_CS2LuaHook = function (this, go) 
    CProgressItemMgr.Inst.CurType = EProgressItemType.AddProgressItem

    CProgressItemMgr.Inst.CurProgressPos = 0
    CUIManager.ShowUI(CUIResources.ProgressItemList)
end
CHouseInfoWnd.m_RegisterTick_CS2LuaHook = function (this) 
    this:UnRegisterTick()

    this.mProgressTick = CTickMgr.Register(DelegateFactory.Action(function () 
        this:OnProgressTick()
    end), 1000, ETickType.Loop)
end
CHouseInfoWnd.m_RegisterExtraProgressTick_CS2LuaHook = function (this) 
    this:UnRegisterExtraProgressTick()

    this.mExtraProgressTick = CTickMgr.Register(DelegateFactory.Action(function () 
        this:OnExtraProgressTick()
    end), 1000, ETickType.Loop)
end
CHouseInfoWnd.m_UnRegisterExtraProgressTick_CS2LuaHook = function (this) 
    if this.mExtraProgressTick ~= nil then
        invoke(this.mExtraProgressTick)
        this.mExtraProgressTick = nil
    end
end
CHouseInfoWnd.m_GetProgressName_CS2LuaHook = function (this, progressPos, progress) 
    if progressPos < EHouseProgress_lua.eGardenProgressBase then
        if progressPos == EnumToInt(EHouseProgress.eZhengwuProgressId) then
            return LocalString.GetString(" 正屋 ")
        elseif progressPos == EnumToInt(EHouseProgress.eCangkuProgressId) then
            return LocalString.GetString(" 仓库 ")
        elseif progressPos == EnumToInt(EHouseProgress.eXiangfangProgressId) then
            return LocalString.GetString(" 厢房 ")
        elseif progressPos == EHouseProgress_lua.eShuiyuProgressId then
            return LocalString.GetString(" 海梦泽 ")
        end
    elseif progressPos >= EHouseProgress_lua.eGardenProgressBase and progressPos < EHouseProgress_lua.eCropProgressBase then
        local index = (progressPos - EHouseProgress_lua.eGardenProgressBase + 1)
        return System.String.Format(LocalString.GetString("苗圃 {0}"), EnumChineseDigits.GetDigit(index))
    elseif progressPos >= EHouseProgress_lua.eCropProgressBase then
        local gardenProp = CClientHouseMgr.Inst.mGardenProp
        if gardenProp ~= nil then
            local garden
            if (function () 
                local __try_get_result
                __try_get_result, garden = CommonDefs.DictTryGet(gardenProp.GardenList, typeof(Byte), (progressPos - EHouseProgress_lua.eCropProgressBase), typeof(CGarden))
                return __try_get_result
            end)() then
                local cropId = garden.CropId
                local cropData = House_Crop.GetData(cropId)
                if cropData ~= nil then
                    return cropData.Name
                end
            end
        end
    end
    return LocalString.GetString("进度")
end
CHouseInfoWnd.m_ShowProgressInfo_CS2LuaHook = function (this) 
    local mgr = CClientHouseMgr.Inst
    if mgr.mProgressProp == nil then
        return
    end

    local index = 0
    CommonDefs.DictIterate(mgr.mProgressProp.ProgressList, DelegateFactory.Action_object_object(function (___key, ___value) 
        local progress = {}
        progress.Key = ___key
        progress.Value = ___value
        if index < CHouseInfoWnd.MaxProgressCount then
            this.ProgressBar[index].gameObject:SetActive(true)
            local nameText = this:GetProgressName(progress.Key, progress.Value)
            this.ProgressBar[index]:UpdateProgressInfo(progress.Key, true, nameText)

            index = index + 1
        end
    end))

    for i = index, 5 do
        if i >= mgr.mProgressProp.ProgressList.Count and i < mgr:GetMaxProgressCount() then
            this.ProgressBar[index].gameObject:SetActive(true)
            this.ProgressBar[i]:UpdateProgressInfo(0, true, System.String.Format(LocalString.GetString("进度{0}"), i + 1))
            index = index + 1
        else
            this.ProgressBar[i].gameObject:SetActive(false)
        end
    end

    if index > 0 then
        local pos = this.AddProgressRow.transform.localPosition
        local posY = this.ProgressBar[index - 1].gameObject.transform.localPosition.y - 102
        this.AddProgressRow.transform.localPosition = Vector3(pos.x, posY, pos.z)
    end

    this.AddProgressRow:SetActive(index ~= CHouseInfoWnd.MaxProgressCount)
    this.ScrollIndicator:SetActive(index == CHouseInfoWnd.MaxProgressCount)

    local bNeedTick = this:UpdateProgressBarRestTime()
    if bNeedTick then
        this:RegisterTick()
    else
        this:UnRegisterTick()
    end
end
CHouseInfoWnd.m_UpdateProgressBarRestTime_CS2LuaHook = function (this) 
    if not (CClientHouseMgr.Inst ~= nil and CClientHouseMgr.Inst.mProgressProp ~= nil and CClientHouseMgr.Inst.mGardenProp ~= nil) then
        return false
    end

    local bNeedTick = false
    local index = 0
    CommonDefs.DictIterate(CClientHouseMgr.Inst.mProgressProp.ProgressList, DelegateFactory.Action_object_object(function (___key, ___value) 
        local progress = {}
        progress.Key = ___key
        progress.Value = ___value
        if index < CHouseInfoWnd.MaxProgressCount then
            local beginTime = progress.Value.BeginTime
            local totalTime = 0
            local default
            default, totalTime = CClientHouseMgr.Inst:GetProgressEndTime(progress.Key, progress.Value, CClientHouseMgr.Inst.mGardenProp, CClientHouseMgr.Inst.mConstructProp)
            local endTime = default
            local curTime = math.floor(CServerTimeMgr.Inst.timeStamp)
            local restTime = math.max(0, endTime - curTime)

            if restTime > 0 then
                bNeedTick = true

                local percent = math.max(math.min((totalTime - restTime) / (totalTime), 1), 0)
                local restTimeText = System.String.Format(LocalString.GetString("{0}:{1}:{2}"), (math.floor(restTime / 3600)), (math.floor((restTime % 3600) / 60)), restTime % 60)

                this.ProgressBar[index]:UpdateProgressPercent(restTimeText, percent)

                index = index + 1
            end
        end
    end))

    for i = index, 5 do
        this.ProgressBar[i]:UpdateProgressInfo(0, true, System.String.Format(LocalString.GetString("进度{0}"), i + 1))
        this.ProgressBar[i]:UpdateProgressPercent(LocalString.GetString("00:00:00"), 0)
    end
    return bNeedTick
end
CHouseInfoWnd.m_GetToucaiText_CS2LuaHook = function (this) 
    local stealCount = CClientHouseMgr.Inst:GetCurrentDayStealCropCount()
    local cangkuGrade = CClientHouseMgr.Inst.mConstructProp.CangkuGrade
    local data = House_CangkuUpgrade.GetData(cangkuGrade)
    local maxStealCount = 100
    if data ~= nil then
        maxStealCount = data.MaxStealCrop
    end

    local toucaiTxt = System.String.Format(LocalString.GetString("{0}/{1}"), stealCount, maxStealCount)
    return toucaiTxt
end
CHouseInfoWnd.m_ShowHouseInfo_CS2LuaHook = function (this) 
    local mgr = CClientHouseMgr.Inst

    if mgr.mBasicProp == nil or mgr.mConstructProp == nil or mgr.mGardenProp == nil or mgr.mProgressProp == nil or CClientMainPlayer.Inst == nil then
        return
    end

    if mgr.mBasicProp.IsMingyuan == 1 then
        this.BtnRename:SetActive(true)
    end
    this.OpenShineiBtn:SetSelected(mgr.mExtraProp.IsOpenShinei > 0, true)

    this.LabelToucai.text = this:GetToucaiText()

    this.LabelShushi.text = LocalString.GetString(tostring(mgr.mShushi))
    this.LabelHaohua.text = LocalString.GetString(tostring(mgr.mHaohua))
    this.LabelFengshui.text = LocalString.GetString(tostring(mgr.mFengshui))

    local maxLingqi = math.floor(mgr.mLingqiMax)
    if not CClientHouseMgr.Inst:IsHouseOwner() then
        maxLingqi = math.floor(mgr.mRoomerLingqiMax)
    end
    local lingqi = 0
    (function () 
        local __try_get_result
        __try_get_result, lingqi = CommonDefs.DictTryGet(mgr.mConstructProp.Lingqi, typeof(UInt64), CClientMainPlayer.Inst.Id, typeof(Double))
        return __try_get_result
    end)()
    local lingqiInt = math.floor(lingqi or 0)
    this.LabelLingqi.text = System.String.Format(LocalString.GetString("{0}/{1}"), lingqiInt, maxLingqi)
    this.LabelLingqiHuifu.text = System.String.Format(LocalString.GetString("{0:N5}/分"), mgr.mLingqiRecover)

    this.LabelQishu.text = LocalString.GetString(tostring(CClientHouseMgr.Inst.mQishu))

    this.LabelGeju.text = CClientHouseMgr.GetGejuName(mgr.mConstructProp.Wuxing)
    this.LabelDengji.text = LocalString.GetString(tostring(mgr.mConstructProp.ZhengwuGrade))

    local menpai = System.String.Empty
    if mgr.mCommunityName ~= nil then
        menpai = System.String.Format(LocalString.GetString("{0}{1}{2}号"), mgr.mCommunityName, CommonDefs.IS_VN_CLIENT and "-" or "", mgr.mCommunityNO)
    end
    this.LabelMenpai.text = menpai
    this.LabelMingcheng.text = CClientHouseMgr.Inst:GetHouseName()
    if mgr.mBasicProp.IsMingyuan == 1 then
        this.LabelMingchengDesc.text = LocalString.GetString("名园名称")
    else
        this.LabelMingchengDesc.text = LocalString.GetString("家园名称")
    end

    local cangkuGrade = CClientHouseMgr.Inst.mConstructProp.CangkuGrade
    local cangkudata = House_CangkuUpgrade.GetData(cangkuGrade)
    local maxWood = 1000 local maxStone = 1000 local maxFood = 1000
    if cangkudata ~= nil then
        maxWood = cangkudata.MaxWood
        maxFood = cangkudata.MaxFood
        maxStone = cangkudata.MaxStone
    end
    this.LabelShiliao.text = System.String.Format(LocalString.GetString("{0}/{1}"), mgr.mConstructProp.Stone, maxStone)
    this.LabelLiangshi.text = System.String.Format(LocalString.GetString("{0}/{1}"), mgr.mConstructProp.Food, maxFood)
    this.LabelMucai.text = System.String.Format(LocalString.GetString("{0}/{1}"), mgr.mConstructProp.Wood, maxWood)


    this.LabelChanquan.text = mgr.ownerName1
    this.LabelTongzhu.text = mgr.ownerName2

    --if(mgr.mBasicProp.OwnerId == CClientMainPlayer.Inst.Id)
    --{
    --    LabelChanquan.text = LocalString.GetString(CClientMainPlayer.Inst.BasicProp.Name);
    --    LabelTongzhu.text = mgr.mPartnerName;
    --}
    --else
    --{
    --    LabelChanquan.text = mgr.mPartnerName;
    --    LabelTongzhu.text = LocalString.GetString(CClientMainPlayer.Inst.BasicProp.Name);
    --}

    this:ShowWeihufei()

    this.LabelZuoling.text = LocalString.GetString(mgr.mZuolingName)
    this.LabelYoushe.text = LocalString.GetString(mgr.mYousheName)
    this:ShowTotalToucai(cangkudata)
    this:ShowProgressInfo()
    this:ShowExtraProgressTime()
    this:OnUpdateExtraProductRate()
    this:ShowShengwang()
end
CHouseInfoWnd.m_ShowWeihufei_CS2LuaHook = function (this) 
    local mgr = CClientHouseMgr.Inst
    if mgr.mBasicProp == nil or mgr.mConstructProp == nil or mgr.mGardenProp == nil or mgr.mProgressProp == nil or CClientMainPlayer.Inst == nil then
        return
    end

    local fee
    fee = mgr:GetWeihufei()
    this.LabelWeihufei.text = System.String.Format(LocalString.GetString("{0}/天"), fee)
    if mgr.mConstructProp.FreeMaintainFeeDay > 0 then
        this.LabelWeihufei.text = System.String.Format(LocalString.GetString("[c][00FF00]已减免[-][/c]({0}天后恢复)"), mgr.mConstructProp.FreeMaintainFeeDay)
    end
end
CHouseInfoWnd.m_ShowTotalToucai_CS2LuaHook = function (this, data) 
    local totalToucai = data.MaxStealCrop * 2

    local time = CServerTimeMgr.Inst:GetZone8Time()
    local time0Hour = CreateFromClass(DateTime, time.Year, time.Month, time.Day, 0, 0, 0)

    local sharedTocai = 0
    if CClientHouseMgr.Inst.mGardenProp ~= nil then
        CommonDefs.DictIterate(CClientHouseMgr.Inst.mGardenProp.PlayerStealInfo, DelegateFactory.Action_object_object(function (___key, ___value) 
            local v = {}
            v.Key = ___key
            v.Value = ___value
            if v.Key ~= 0 then
                local stealCount = v.Value.StealCount
                local lastUpdateTime = v.Value.StealUpdateTime
                local lastUpdateDate = CServerTimeMgr.UtcStart:AddSeconds(lastUpdateTime + 28800 --[[8 * 3600]])
                if DateTime.Compare(lastUpdateDate, time0Hour) < 0 then
                    stealCount = 0
                end

                sharedTocai = sharedTocai + stealCount
            end
        end))

        local sharedStealProp = nil
        if (function () 
            local __try_get_result
            __try_get_result, sharedStealProp = CommonDefs.DictTryGet(CClientHouseMgr.Inst.mGardenProp.PlayerStealInfo, typeof(Int32), 0, typeof(CStealProp))
            return __try_get_result
        end)() then
            local stealCount = sharedStealProp.StealCount
            local lastUpdateTime = sharedStealProp.StealUpdateTime
            local lastUpdateDate = CServerTimeMgr.UtcStart:AddSeconds(lastUpdateTime + 28800 --[[8 * 3600]])
            if DateTime.Compare(lastUpdateDate, time0Hour) < 0 then
                stealCount = 0
            end

            sharedTocai = math.max(stealCount, sharedTocai)
        end
    end
    sharedTocai = math.min(sharedTocai, totalToucai)

    this.LabelTotalToucai.text = System.String.Format("{0}/{1}", sharedTocai, totalToucai)
end
CHouseInfoWnd.m_ShowExtraProgressTime_CS2LuaHook = function (this) 
    local mgr = CClientHouseMgr.Inst
    if mgr.mProgressProp == nil then
        return
    end

    local restTimeInSecond = mgr.mProgressProp.ExtraProgressEndTime - math.floor(CServerTimeMgr.Inst.timeStamp)
    restTimeInSecond = math.max(restTimeInSecond, 0)

    local txt = ""
    if restTimeInSecond > 0 then
        txt = txt .. (math.floor(restTimeInSecond / 3600) .. ":")
        txt = txt .. (math.floor((restTimeInSecond % 3600) / 60) .. ":")
        txt = txt .. (restTimeInSecond % 60)
    end

    if mgr:GetMaxProgressCount() > 2 then
        this.LabelProgressInfo.text = System.String.Format(LocalString.GetString("共{0}个进度  {1}"), mgr:GetMaxProgressCount(), txt)
    else
        this.LabelProgressInfo.text = LocalString.GetString("当前进度")
    end

    if restTimeInSecond > 0 and this.mExtraProgressTick == nil then
        this:RegisterExtraProgressTick()
    elseif restTimeInSecond <= 0 and this.mExtraProgressTick ~= nil then
        this:UnRegisterExtraProgressTick()
        this:ShowProgressInfo()
    end
end
CHouseInfoWnd.m_ShowShengwang_CS2LuaHook = function (this) 
    if CClientHouseMgr.sbEnableJiayuanJiyu then
        this.LabelShengwang.transform.parent.gameObject:SetActive(true)
    else
        this.LabelShengwang.transform.parent.gameObject:SetActive(false)
        return
    end

    if not CClientHouseMgr.sbEnableJiayuanJiyu then
        return
    end
    local mgr = CClientHouseMgr.Inst
    if mgr.mExtraProp == nil then
        return
    end
    this.LabelShengwang.text = System.String.Format(LocalString.GetString("{0}/{1}"), mgr.mExtraProp.CurrentCommunityShengwang, mgr.mExtraProp.TotalCommunityShengwang)
end
