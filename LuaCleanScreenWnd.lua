local Object=import "System.Object"
local CPaintTexture     =import "L10.UI.CPaintTexture"
local CUIFx             = import "L10.UI.CUIFx"
local NewPostEffectMgr  = import "L10.Engine.NewPostEffectMgr"

LuaCleanScreenWnd = class()      --CleanScreenWnd

--------RegistChildComponent-------
RegistChildComponent(LuaCleanScreenWnd,         "Clean",            CPaintTexture)
RegistChildComponent(LuaCleanScreenWnd,         "Piont",            GameObject)
RegistChildComponent(LuaCleanScreenWnd,         "Piont1",           GameObject)
RegistChildComponent(LuaCleanScreenWnd,         "Piont2",           GameObject)
RegistChildComponent(LuaCleanScreenWnd,         "Piont3",           GameObject)
RegistChildComponent(LuaCleanScreenWnd,         "Piont4",           GameObject)
RegistChildComponent(LuaCleanScreenWnd,         "Piont5",           GameObject)
RegistChildComponent(LuaCleanScreenWnd,         "Piont6",           GameObject)
RegistChildComponent(LuaCleanScreenWnd,         "Piont7",           GameObject)
RegistChildComponent(LuaCleanScreenWnd,         "Piont8",           GameObject)
RegistChildComponent(LuaCleanScreenWnd,         "Piont9",           GameObject)
RegistChildComponent(LuaCleanScreenWnd,         "Label",            GameObject)
RegistChildComponent(LuaCleanScreenWnd,         "CleanEffect",      CUIFx)
RegistChildComponent(LuaCleanScreenWnd,         "BG_1",             UIWidget)     --夜晚
RegistChildComponent(LuaCleanScreenWnd,         "BG_2",             UIWidget)     --朝阳
RegistChildComponent(LuaCleanScreenWnd,         "BG_3",             UIWidget)     --白天
RegistChildComponent(LuaCleanScreenWnd,         "mengban",          GameObject)     --白天

---------RegistClassMember-------
RegistClassMember(LuaCleanScreenWnd,            "PiontTable")
-- RegistClassMember(LuaCleanScreenWnd,            "PicReplaceTick")
RegistClassMember(LuaCleanScreenWnd,            "EyeCloseEffect")
RegistClassMember(LuaCleanScreenWnd,            "m_TimeTick")

LuaCleanScreenWnd.taskId = 0
LuaCleanScreenWnd.wipeId = 1        --1 白天版 2 夜晚版

function LuaCleanScreenWnd:Init()
    self.CleanEffect.gameObject:SetActive(false)
    self:InitPiontTable()
    for i,v in ipairs(self.PiontTable) do
        self.Clean:RegisterTrigger(v)
    end

    if LuaCleanScreenWnd.wipeId==1 then
        self.BG_1.alpha = 0
        self.BG_2.alpha = 0
        self.BG_3.alpha = 1
    else
        self.BG_1.alpha = 1
        self.BG_2.alpha = 0
        self.BG_3.alpha = 0
    end

    self.Clean.OnPaintOver = DelegateFactory.Action(function()
        -- 擦亮屏幕
        self.Clean:FinishPaint()
        self.mengban:SetActive(false)

        if LuaCleanScreenWnd.wipeId == 1 then
            self:ShouComplete()
        else
            LuaTweenUtils.TweenAlpha(self.BG_1, 1,0, 6,function()end)
            LuaTweenUtils.TweenAlpha(self.BG_2, 0,1, 6,function()
                LuaTweenUtils.TweenAlpha(self.BG_2, 1,0, 6,function()end)
                LuaTweenUtils.TweenAlpha(self.BG_3, 0,1, 6,function()
                    self:ShouComplete()
                end)
            end)
        end

    end)
    self.Clean.OnStartPaint = DelegateFactory.Action(function()
        self.Label:SetActive(false)
    end)
end

function LuaCleanScreenWnd:OnDisable()
    UnRegisterTick(self.m_TimeTick)
    if self.EyeCloseEffect then
        self.EyeCloseEffect:Stop()
    end
end

function LuaCleanScreenWnd:InitPiontTable()
    self.PiontTable = {self.Piont,self.Piont1,self.Piont2,self.Piont3,self.Piont4,
    self.Piont5,self.Piont6,self.Piont7,self.Piont8,self.Piont9}
end

function LuaCleanScreenWnd:ShouComplete()
    self.EyeCloseEffect = NewPostEffectMgr.SetEyeCloseOnCamera(CUIManager.instance.MainCamera.gameObject,2,3,4,true)
    self.m_TimeTick = RegisterTickOnce(function ()
        local empty = CreateFromClass(MakeGenericClass(List, Object))
        empty = MsgPackImpl.pack(empty)
        Gac2Gas.FinishClientTaskEventWithConditionId(LuaCleanScreenWnd.taskId, "WipeScreenLight",empty, LuaCleanScreenWnd.wipeId)
        CUIManager.CloseUI(CLuaUIResources.CleanScreenWnd)
    end, 5 * 1000)
end
