local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local Profession = import "L10.Game.Profession"
local EnumClass = import "L10.Game.EnumClass"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"

LuaGSNZResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGSNZResultWnd, "WinStyle1", "WinStyle1", GameObject)
RegistChildComponent(LuaGSNZResultWnd, "WinStyle2", "WinStyle2", GameObject)
RegistChildComponent(LuaGSNZResultWnd, "FootStyle1", "FootStyle1", GameObject)
RegistChildComponent(LuaGSNZResultWnd, "FootStyle2", "FootStyle2", GameObject)
RegistChildComponent(LuaGSNZResultWnd, "Left", "Left", GameObject)
RegistChildComponent(LuaGSNZResultWnd, "Right", "Right", GameObject)
RegistChildComponent(LuaGSNZResultWnd, "ShareButton", "ShareButton", GameObject)
RegistChildComponent(LuaGSNZResultWnd, "CountLabel", "CountLabel", UILabel)
RegistChildComponent(LuaGSNZResultWnd, "TypeLabel", "TypeLabel", UILabel)
RegistChildComponent(LuaGSNZResultWnd, "IconTexture", "IconTexture", CUITexture)
RegistChildComponent(LuaGSNZResultWnd, "Side1", "Side1", GameObject)
RegistChildComponent(LuaGSNZResultWnd, "Side2", "Side2", GameObject)
RegistChildComponent(LuaGSNZResultWnd, "ExRoot", "ExRoot", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaGSNZResultWnd,"m_roles")

function LuaGSNZResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    UIEventListener.Get(self.ShareButton.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:OnShareButtonClick()
        end
    )

    --@endregion EventBind end
end

function LuaGSNZResultWnd:Init()
    local data = ZhongYuanJie2022Mgr.GSNZResultData

    if data == nil then
        return
    end

    local iswin = data.IsWin
    self.WinStyle1:SetActive(data.WinType == 1)
    self.WinStyle2:SetActive(data.WinType ~= 1)

    self.Side1:SetActive(data.Force == 1)
    self.Side2:SetActive(data.Force ~= 1)

    local go = data.Force == 1 and self.Side1 or self.Side2
    local wingo = FindChild(go.transform, "win").gameObject
    local lostgo = FindChild(go.transform, "lost").gameObject
    wingo:SetActive(iswin)
    lostgo:SetActive(not iswin)

    local setting = ZhongYuanJie_Setting2022.GetData()
    self.m_roles = setting.Role2Name

    local ct = data.RewardTimes
    local maxct = setting.GSNZ_MaxCount
    local ltct = maxct - ct
    self.FootStyle1:SetActive(ltct > 0)
    self.FootStyle2:SetActive(ltct <= 0)

    if ltct > 0 then
        local fmt = LocalString.GetString("今日剩余奖励次数[ffffff]%s/%s[-]")
        self.CountLabel.text = g_MessageMgr:Format(fmt, ltct, maxct)

        if iswin then
            self.TypeLabel.text = LocalString.GetString("获得获胜礼包")
            local item = Item_Item.GetData(setting.GSNZ_WinReward)
            self.IconTexture:LoadMaterial(item.Icon)
        else
            self.TypeLabel.text = LocalString.GetString("获得参与礼包")
            local item = Item_Item.GetData(setting.GSNZ_LostReward)
            self.IconTexture:LoadMaterial(item.Icon)
        end
    end

    local pdatas = data.PlayerDatas
    local ctrl = nil
    local lindex = 1
    local rindex = 1
    for i = 1, #pdatas do
        local data = pdatas[i]
        if data.Side == 1 then --游魂
            ctrl = FindChild(self.Left.transform, "InfoRow" .. lindex)
            lindex = lindex + 1
        else
            ctrl = FindChild(self.Right.transform, "InfoRow" .. rindex)
            rindex = rindex + 1
        end
        self:InitItem(ctrl, data)
    end
    for i = lindex, 5 do
        ctrl = FindChild(self.Left.transform, "InfoRow" .. i)
        ctrl.gameObject:SetActive(false)
    end

    for i = rindex, 5 do
        ctrl = FindChild(self.Right.transform, "InfoRow" .. i)
        ctrl.gameObject:SetActive(false)
    end
end

function LuaGSNZResultWnd:InitItem(ctrl, pdata)
    local namelb = FindChildWithType(ctrl, "NameLabel", typeof(UILabel))
    local rolelb = FindChildWithType(ctrl, "RoleLabel", typeof(UILabel))
    local killlb = FindChildWithType(ctrl, "KillLabel", typeof(UILabel))
    local picklb = FindChildWithType(ctrl, "PickLabel", typeof(UILabel))
    local jobsp = FindChildWithType(ctrl, "JobSprite", typeof(UISprite))

    namelb.text = pdata.Name
    namelb.color = pdata.PlayerId == CClientMainPlayer.Inst.Id and Color(0, 1, 96 / 255, 1) or Color(1, 1, 1, 1)
    rolelb.text = self.m_roles[pdata.Role - 1]
    killlb.text = pdata.Killnum
    picklb.text = pdata.Picknum
    jobsp.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), pdata.Job))
end

--@region UIEvent

function LuaGSNZResultWnd:OnShareButtonClick()
    CUICommonDef.CaptureScreen(
        "screenshot",
        true,
        false,
        self.ExRoot,
        DelegateFactory.Action_string_bytes(
            function(fullPath, jpgBytes)
                ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
            end
        ),
        false
    )
end

--@endregion UIEvent
