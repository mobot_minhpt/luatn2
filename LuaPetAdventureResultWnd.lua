local CButton = import "L10.UI.CButton"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local CClientMonster = import "L10.Game.CClientMonster"
local CRankData = import "L10.UI.CRankData"
local RankPlayerBaseInfo = import "L10.UI.RankPlayerBaseInfo"
local EnumRankSheet = import "L10.UI.EnumRankSheet"
local EnumClass = import "L10.Game.EnumClass"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"
local Monster_Monster = import "L10.Game.Monster_Monster"

LuaPetAdventureResultWnd = class()

RegistChildComponent(LuaPetAdventureResultWnd, "RankButton", CButton)
RegistChildComponent(LuaPetAdventureResultWnd, "AgainButton", CButton)
RegistChildComponent(LuaPetAdventureResultWnd, "ShareButton", CButton)

RegistChildComponent(LuaPetAdventureResultWnd, "SurviveTimeLabel", UILabel)
RegistChildComponent(LuaPetAdventureResultWnd, "PetTexture", UITexture)
RegistChildComponent(LuaPetAdventureResultWnd, "RankLabel", UILabel)
RegistChildComponent(LuaPetAdventureResultWnd, "Win", GameObject)
RegistChildComponent(LuaPetAdventureResultWnd, "Fail", GameObject)
RegistChildComponent(LuaPetAdventureResultWnd, "Buttons", GameObject)

RegistChildComponent(LuaPetAdventureResultWnd, "PlayerInfoNode", GameObject)

RegistClassMember(LuaPetAdventureResultWnd, "ModelTextureLoader")
RegistClassMember(LuaPetAdventureResultWnd, "ModelName")
RegistClassMember(LuaPetAdventureResultWnd, "MonsterModelScale")

function LuaPetAdventureResultWnd:Init()
    CommonDefs.AddOnClickListener(self.RankButton.gameObject, DelegateFactory.Action_GameObject(function (go)
        self:OnRankButtonClicked(go)       
    end), false)

    CommonDefs.AddOnClickListener(self.AgainButton.gameObject, DelegateFactory.Action_GameObject(function (go)
        self:OnAgainButtonClicked(go)
    end), false)

    CommonDefs.AddOnClickListener(self.ShareButton.gameObject, DelegateFactory.Action_GameObject(function (go)
        self:OnShareButtonClicked(go)
    end), false)
    
    self.MonsterModelScale = {}
    self.MonsterModelScale[18005668] = 1.2 -- 小狐狸适当放大
    self.MonsterModelScale[18005670] = 0.7 -- 雪团子适当缩小

    self.ModelTextureLoader = nil
    self.ModelName = "__petadventuremodel__"
    self:InitPlayerInfoNode(self.PlayerInfoNode)
    self:UpdatePlayerInfo()

    self.ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        local monster = Monster_Monster.GetData(LuaHanJiaMgr.PlayerMonsterID)
        if not monster then return end

        ro:LoadMain(CClientMonster.GetMonsterPrefabPath(monster), CClientMonster.DEFAULT_MONSTER, false, false, false)
    end)

    local scale = 1
    if self.MonsterModelScale[LuaHanJiaMgr.PlayerMonsterID] then
        scale = self.MonsterModelScale[LuaHanJiaMgr.PlayerMonsterID]
    end
    self.PetTexture.mainTexture = CUIManager.CreateModelTexture(self.ModelName, self.ModelTextureLoader, -150, 0.05, -1, 4.66, false, true, scale, true, false)
end

function LuaPetAdventureResultWnd:UpdatePlayerInfo()
    self.RankLabel.text = tostring(LuaHanJiaMgr.PlayerFinalRank)
    local isWin = LuaHanJiaMgr.PlayerFinalRank <= 4
    self.Win:SetActive(isWin)
    self.Fail:SetActive(not isWin)
    self.SurviveTimeLabel.text = SafeStringFormat3(LocalString.GetString("存活时间 %s"), CUICommonDef.SecondsToTimeString(LuaHanJiaMgr.PlayerAliveTime, true)) 
end

--[[ local i = {
        rank = rank,
        playerId = playerId,
        hpPercent = hpPercent,
        aliveTime = aliveTime,
        playerName = playerName,
        isDead = isDead,
        cls = cls,
    }]]
--@desc 打开排行榜
function LuaPetAdventureResultWnd:OnRankButtonClicked(go)
    if not LuaHanJiaMgr.PlayerFinalInfo then return end
    
    -- 数据已有，直接展示即可
    CRankData.Inst.MainPlayerRankInfo = RankPlayerBaseInfo(0, 0, CClientMainPlayer.Inst.Name, "", 0, 0, CClientMainPlayer.Inst.Class, 0, 0, EnumRankSheet.eGuild, "", "", "", "", "", "", 0, 0, "", "", nil, nil, 0, "")
    CommonDefs.ListClear(CRankData.Inst.RankList)
    if CClientMainPlayer.Inst then
        CRankData.Inst.MainPlayerRankInfo.PlayerIndex = CClientMainPlayer.Inst.Id
    end

    for i = 1, #LuaHanJiaMgr.PlayerFinalInfo do
        local info = LuaHanJiaMgr.PlayerFinalInfo[i]
        if info.playerId == CClientMainPlayer.Inst.Id then
            CRankData.Inst.MainPlayerRankInfo.Rank = info.rank
            CRankData.Inst.MainPlayerRankInfo.Name = info.playName
            CRankData.Inst.MainPlayerRankInfo.Value = info.aliveTime
        end
        CommonDefs.ListAdd(CRankData.Inst.RankList, typeof(RankPlayerBaseInfo), RankPlayerBaseInfo(
            0, info.playerId, info.playerName, "", 0, info.rank, CommonDefs.ConvertIntToEnum(typeof(EnumClass), info.cls), 0, info.aliveTime, EnumRankSheet.eGuild, "", "", "", "", "", "", 0, 0, "", "", nil, nil, 0, ""))
    end
    LuaCommonRankWndMgr.m_BottomText = nil
    LuaCommonRankWndMgr.m_TitleText = LocalString.GetString("排名")
    LuaCommonRankWndMgr.m_ThirdHeadText = LocalString.GetString("存活时间")
    LuaCommonRankWndMgr.m_ValueProcessFunc = function (info)
        return CUICommonDef.SecondsToTimeString(info.Value, true)
    end
    CUIManager.ShowUI(CLuaUIResources.CommonRankWnd)
end

--@desc 再来一局
function LuaPetAdventureResultWnd:OnAgainButtonClicked(go)
    Gac2Gas.OpenHanJiaAdventureSignUpWnd()
    CUIManager.CloseUI(CLuaUIResources.PetAdventureResultWnd)
end

--@desc 分享表现
function LuaPetAdventureResultWnd:OnShareButtonClicked(go)
    CUICommonDef.CaptureScreen(
        "screenshot",
        true,
        false,
        self.Buttons,
        DelegateFactory.Action_string_bytes(
            function(fullPath, jpgBytes)
                ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
            end
        ),
        false
    )
end

function LuaPetAdventureResultWnd:InitPlayerInfoNode(node)
    local portrait = node.transform:Find("Portrait"):GetComponent(typeof(CUITexture))
    local name = node.transform:Find("Name"):GetComponent(typeof(UILabel))
    local id = node.transform:Find("ID"):GetComponent(typeof(UILabel))
    local server = node.transform:Find("Server"):GetComponent(typeof(UILabel))

    portrait:LoadNPCPortrait(LuaHanJiaMgr.PlayerPortraitName)
    name.text = LuaHanJiaMgr.PlayerName
    server.text = LuaHanJiaMgr.PlayerServerName
    id.text = LuaHanJiaMgr.PlayerId
end

function LuaPetAdventureResultWnd:OnEnable()
    
end

function LuaPetAdventureResultWnd:OnDisable()
    CUIManager.DestroyModelTexture(self.ModelName)
end