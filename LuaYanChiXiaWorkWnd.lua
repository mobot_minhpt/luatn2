local CUIFx = import "L10.UI.CUIFx"
local UISlider = import "UISlider"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local DelegateFactory = import "DelegateFactory"
local CButton = import "L10.UI.CButton"
local UIEventListener = import "UIEventListener"
local Extensions = import "Extensions"
local TweenAlpha = import "TweenAlpha"
local Money = import "L10.Game.Money"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local Collider = import "UnityEngine.Collider"
local QnRadioBox = import "L10.UI.QnRadioBox"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"

LuaYanChiXiaWorkWnd = class()
RegistChildComponent(LuaYanChiXiaWorkWnd,"ActivityTimeLabel", UILabel)
RegistChildComponent(LuaYanChiXiaWorkWnd,"RateOfTaskProgressLabel", UILabel)
RegistChildComponent(LuaYanChiXiaWorkWnd,"FlagonOpenProgressLabel1", UILabel)
RegistChildComponent(LuaYanChiXiaWorkWnd,"FlagonOpenProgressLabel2", UILabel)
RegistChildComponent(LuaYanChiXiaWorkWnd,"FlagonIcon", CButton)
RegistChildComponent(LuaYanChiXiaWorkWnd,"FlagonFx", CUIFx)
RegistChildComponent(LuaYanChiXiaWorkWnd,"TaskTemplate", GameObject)
RegistChildComponent(LuaYanChiXiaWorkWnd,"TasksGrid", UIGrid)
RegistChildComponent(LuaYanChiXiaWorkWnd,"TabButtonTemplate", GameObject)
RegistChildComponent(LuaYanChiXiaWorkWnd,"TabButtonsGrid", UIGrid)
RegistChildComponent(LuaYanChiXiaWorkWnd,"ItemTemplate", GameObject)
RegistChildComponent(LuaYanChiXiaWorkWnd,"ItemsGrid", UIGrid)
RegistChildComponent(LuaYanChiXiaWorkWnd,"JiuQiSpecialIcon", CUITexture)
RegistChildComponent(LuaYanChiXiaWorkWnd,"CurNameLabel", UILabel)
RegistChildComponent(LuaYanChiXiaWorkWnd,"RestDesLabel", UILabel)
RegistChildComponent(LuaYanChiXiaWorkWnd,"CurNumOfPilesSlider", UISlider)
RegistChildComponent(LuaYanChiXiaWorkWnd,"ExplanationIcon", CButton)
RegistChildComponent(LuaYanChiXiaWorkWnd,"BuyButton", CButton)
RegistChildComponent(LuaYanChiXiaWorkWnd,"CurNumOfPilesSliderFx", CUIFx)
RegistChildComponent(LuaYanChiXiaWorkWnd,"LevelUpFx", CUIFx)
RegistChildComponent(LuaYanChiXiaWorkWnd,"NumOfPliesLabel", UILabel)
RegistChildComponent(LuaYanChiXiaWorkWnd,"NumIncreaseFx", CUIFx)
RegistChildComponent(LuaYanChiXiaWorkWnd,"ProgressLabel", UILabel)
RegistChildComponent(LuaYanChiXiaWorkWnd,"JiuQiArea", GameObject)
RegistChildComponent(LuaYanChiXiaWorkWnd,"TabArea", GameObject)
RegistChildComponent(LuaYanChiXiaWorkWnd,"TaskArea", GameObject)
RegistChildComponent(LuaYanChiXiaWorkWnd,"FlagonArea", GameObject)

RegistClassMember(LuaYanChiXiaWorkWnd,"curManuals")
RegistClassMember(LuaYanChiXiaWorkWnd,"curTasks")
RegistClassMember(LuaYanChiXiaWorkWnd,"curManualIndex")
RegistClassMember(LuaYanChiXiaWorkWnd,"curSelectIndex")
RegistClassMember(LuaYanChiXiaWorkWnd,"curManualsState")
RegistClassMember(LuaYanChiXiaWorkWnd,"curWinePoint")
RegistClassMember(LuaYanChiXiaWorkWnd,"curLevel")
RegistClassMember(LuaYanChiXiaWorkWnd,"curOpenBottleTimes")
RegistClassMember(LuaYanChiXiaWorkWnd,"ownManualInfo")
RegistClassMember(LuaYanChiXiaWorkWnd,"canReceiveJiuQi")
RegistClassMember(LuaYanChiXiaWorkWnd,"curFinishedTaskNum")
RegistClassMember(LuaYanChiXiaWorkWnd,"radioBox")
RegistClassMember(LuaYanChiXiaWorkWnd, "tweenList")
function LuaYanChiXiaWorkWnd:Init()

    if HanJia2020Mgr.isFirstOpen then
        self:ShowExplanation()
        HanJia2020Mgr.isFirstOpen = false
    end

    self.transform:Find("Anchor").gameObject:SetActive(false)

    --Title
    self:InitTitleRoot()

    --Tasks
    self:InitTasks()

    --Manuals
    self:InitManuals()
    self:InitTabButtons()

    --Bottom
    self:InitBottomRoot()

    Gac2Gas.QueryWorkManualOverview()

    CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.YanChiXiaWorkWnd)
end

function LuaYanChiXiaWorkWnd:OnEnable()
    g_ScriptEvent:AddListener("HanJia2020_OnReplyWorkManualOverview", self, "OnReplyWorkManualOverview")
    g_ScriptEvent:AddListener("HanJia2020_OnGetTaskReward",self, "OnGetTaskReward")
    g_ScriptEvent:AddListener("HanJia2020_OnBottleOpen", self, "OnBottleOpen")
    g_ScriptEvent:AddListener("HanJia2020_OnBuyManualSuccess", self, "OnBuyManualSuccess")
    g_ScriptEvent:AddListener("HanJia2020_GetManualReward", self, "OnGetManualReward")
end

function LuaYanChiXiaWorkWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HanJia2020_OnReplyWorkManualOverview", self, "OnReplyWorkManualOverview")
    g_ScriptEvent:RemoveListener("HanJia2020_OnGetTaskReward",self, "OnGetTaskReward")
    g_ScriptEvent:RemoveListener("HanJia2020_OnBottleOpen", self, "OnBottleOpen")
    g_ScriptEvent:RemoveListener("HanJia2020_OnBuyManualSuccess", self, "OnBuyManualSuccess")
    g_ScriptEvent:RemoveListener("HanJia2020_GetManualReward", self, "OnGetManualReward")
    self:KillTweenList()
end

----------------------------------------
---开启酒壶
----------------------------------------
function LuaYanChiXiaWorkWnd:OnNumOfOpenBottleTimesIncrease(num)
    if self.curOpenBottleTimes and num > self.curOpenBottleTimes then
        self.NumIncreaseFx:DestroyFx()
        self.NumIncreaseFx:LoadFx("fx/ui/prefab/UI_dagongshouce_kekaiqicishu.prefab")
    end
    self.curOpenBottleTimes = num
    self.ProgressLabel.text = num

    if self.curOpenBottleTimes > 0 then
        self:EnableFlagonOpen()
    else
        self:DisableFlagonOpen()
    end
end

function LuaYanChiXiaWorkWnd:OnFlagonIconClick()
    if self.curOpenBottleTimes > 0 then
        HanJia2020Mgr:ShowFlagonWnd()
    end
end

function LuaYanChiXiaWorkWnd:InitTitleRoot()
    self.ActivityTimeLabel.text = HanJia2020_Setting.GetData().ActivityTime
    UIEventListener.Get(self.FlagonIcon.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        self:OnFlagonIconClick()
    end)
    self:DisableFlagonOpen()
end

function LuaYanChiXiaWorkWnd:DisableFlagonOpen()
    self.FlagonOpenProgressLabel1.gameObject:SetActive(true)
    self.FlagonOpenProgressLabel2.gameObject:SetActive(false)
    self.FlagonIcon.Enabled = false
    self.FlagonFx:DestroyFx()
end

function LuaYanChiXiaWorkWnd:EnableFlagonOpen()
    self.FlagonOpenProgressLabel1.gameObject:SetActive(false)
    self.FlagonOpenProgressLabel2.gameObject:SetActive(true)
    self.FlagonIcon.Enabled = true
    self.FlagonFx:DestroyFx()
    self.FlagonFx:LoadFx("fx/ui/prefab/UI_dagongshouce_changzhu01.prefab")
end
----------------------------------------
---酒魄千叠
----------------------------------------
function LuaYanChiXiaWorkWnd:InitTasks()
    Extensions.RemoveAllChildren(self.TasksGrid.transform)
    self.curTasks = {}
    self.TaskTemplate:SetActive(false)
    for i = 1, 8 do
        local go = NGUITools.AddChild(self.TasksGrid.gameObject, self.TaskTemplate)
        go:SetActive(true)
        local task = self:InitTask(go)
        table.insert(self.curTasks, task)
    end
    self.TasksGrid:Reposition()
    self.canReceiveJiuQi = true
end

function LuaYanChiXiaWorkWnd:InitTask(go)
    local task = {}
    task.Widget = go.transform:Find("Widget"):GetComponent(typeof(UIWidget))
    task.Slider = task.Widget.transform:Find("Slider"):GetComponent(typeof(UISlider))
    task.DescriptionLabel = task.Widget.transform:Find("LeftContainer/DescriptionLabel"):GetComponent(typeof(UILabel))
    task.GetRewardLabel = task.Widget.transform:Find("RightContainer/GetRewardLabel"):GetComponent(typeof(UILabel))
    task.JiuQiIcon = task.Widget.transform:Find("RightContainer/JiuQiIcon"):GetComponent(typeof(CUITexture))
    task.YinPiaoIcon = task.Widget.transform:Find("RightContainer/YinPiaoIcon"):GetComponent(typeof(UISprite))
    task.JiuQiLabel = task.JiuQiIcon.transform:Find("JiuQiLabel"):GetComponent(typeof(UILabel))
    task.YinPiaoLabel = task.YinPiaoIcon.transform:Find("YinPiaoLabel"):GetComponent(typeof(UILabel))
    task.Fx = task.Slider.transform:Find("Fx"):GetComponent(typeof(CUIFx))
    task.GetRewardSprite = go.transform:Find("GetRewardSprite"):GetComponent(typeof(UISprite))

    task.GetRewardLabelTweenAlpha = task.GetRewardLabel:GetComponent(typeof(TweenAlpha))
    self:InitTaskState(task)
    task.YinPiaoLabel.text = 0
    UIEventListener.Get(task.GetRewardLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        self:ReceiveTaskReward(task)
    end)
    return task
end

function LuaYanChiXiaWorkWnd:InitTaskState(task)
    task.GetRewardLabel.gameObject:SetActive(false)
    task.JiuQiIcon.gameObject:SetActive(false)
    task.YinPiaoIcon.gameObject:SetActive(false)
    task.GetRewardSprite.gameObject:SetActive(false)
    task.Widget.alpha = 1
end

function LuaYanChiXiaWorkWnd:ReloadTaskData(task, data)
    task.ID = tonumber(data.subTaskId)
    local taskData = HanJia2020_Task.GetData(task.ID)
    if taskData == nil then
        return
    end
    task.Slider.value = data.progress / taskData.Count
    task.DescriptionLabel.text = taskData.Description
    task.JiuQiLabel.text = taskData.JiuQi

    local isTaskReadyGetReward = data.status == 1 and data.progress == taskData.Count
    local isGetReward = data.status == 2

    self:InitTaskState(task)

    if taskData.YinPiao and taskData.YinPiao > 0 then
        task.YinPiaoLabel.text = taskData.YinPiao
        task.YinPiaoIcon.spriteName = Money.GetIconName(EnumMoneyType.YinPiao, EnumPlayScoreKey.NONE)
    elseif taskData.YinLiang and taskData.YinLiang > 0 then
        task.YinPiaoLabel.text = taskData.YinLiang
        task.YinPiaoIcon.spriteName = Money.GetIconName(EnumMoneyType.YinLiang, EnumPlayScoreKey.NONE)
    else
        if data.expReward then
            task.YinPiaoLabel.text = data.expReward
        end
        task.YinPiaoIcon.spriteName = Money.GetIconName(EnumMoneyType.Exp, EnumPlayScoreKey.NONE)
    end

    if isGetReward then
        --完成状态
        task.JiuQiIcon.gameObject:SetActive(true)
        task.YinPiaoIcon.gameObject:SetActive(true)
        task.GetRewardSprite.gameObject:SetActive(true)
        task.Widget.alpha = 0.3
    elseif not isTaskReadyGetReward then
        --未完成状态
        task.JiuQiIcon.gameObject:SetActive(true)
        task.YinPiaoIcon.gameObject:SetActive(true)
    else
        --未领取奖励状态
        task.GetRewardLabel.gameObject:SetActive(true)
    end
end

function LuaYanChiXiaWorkWnd:ReceiveTaskReward(task)
    if task.ID and self.canReceiveJiuQi then
        Gac2Gas.RequestGetHanJiaSubTaskReward(task.ID)
    end
end
----------------------------------------
---手册奖励
----------------------------------------
function LuaYanChiXiaWorkWnd:InitTabButtons()

    self.TabButtonTemplate:SetActive(false)

    for _,index in ipairs(HanJia2020Mgr.sortedYanChiXiaWorkManualList) do
        local go = self.TabButtonsGrid.transform:GetChild(index - 1).gameObject
        go:SetActive(true)
        self:InitTabButton(go,  HanJia2020Mgr:GetYanChiXiaWorkManualName(index), index)
    end
    self.TabButtonsGrid:Reposition()

    self:InitRadioBox()
end

function LuaYanChiXiaWorkWnd:InitRadioBox()
    self.radioBox = CommonDefs.GetComponent_GameObject_Type(self.TabButtonsGrid.gameObject, typeof(QnRadioBox))
    self.radioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnTabButtonSelected(btn, index)
    end)
end

function LuaYanChiXiaWorkWnd:InitTabButton(go, name, index)
    local btn = go:GetComponent(typeof(QnSelectableButton))
    btn.Text = name
    btn:SetSelected(false, false)
    return btn
end

function LuaYanChiXiaWorkWnd:OnTabButtonSelected(btn, index)

    index = index + 1
    self.curManualIndex = index
    self:ReloadManualsData()

    self:OnTabButtonChange(index)
end

function LuaYanChiXiaWorkWnd:InitManuals()
    self.ItemTemplate:SetActive(false)
    self.curManuals = {}
    Extensions.RemoveAllChildren(self.ItemsGrid.transform)
    for i = 1, 6 do
        local go = NGUITools.AddChild(self.ItemsGrid.gameObject, self.ItemTemplate)
        go:SetActive(true)
        local item = self:InitItem(go)
        table.insert(self.curManuals, item)
    end
    self.ItemsGrid:Reposition()

    self.curManualIndex = EnumYanChiXiaWorkManual.TianYun
    self.curSelectIndex = 0
end

function LuaYanChiXiaWorkWnd:InitItem(go)
    local item = {}
    item.ChoiceBox = go.transform:Find("ChoiceBox"):GetComponent(typeof(UITexture))
    item.NumOfPliesLabel = go.transform:Find("Top/NumOfPliesLabel"):GetComponent(typeof(UILabel))
    item.Icon = go.transform:Find("Bottom/Bg/Icon"):GetComponent(typeof(CUITexture))
    item.NumLabel =  item.Icon.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
    item.GetRewardSprite = item.Icon.transform:Find("GetRewardSprite"):GetComponent(typeof(UISprite))
    item.LockSprite = item.Icon.transform:Find("LockSprite"):GetComponent(typeof(UISprite))
    item.DisableSprite = item.Icon.transform:Find("DisableSprite"):GetComponent(typeof(UISprite))
    item.Fx = item.Icon.transform:Find("Fx"):GetComponent(typeof(CUIFx))
    self:InitItemState(item)
    item.Fx:LoadFx("fx/ui/prefab/UI_kuang_blue02.prefab")
    local path = {
        Vector3(item.Fx.transform.localPosition.x      ,item.Fx.transform.localPosition.y      ,item.Fx.transform.localPosition.z),
        Vector3(item.Fx.transform.localPosition.x + 100,item.Fx.transform.localPosition.y      ,item.Fx.transform.localPosition.z),
        Vector3(item.Fx.transform.localPosition.x + 100,item.Fx.transform.localPosition.y - 100,item.Fx.transform.localPosition.z),
        Vector3(item.Fx.transform.localPosition.x      ,item.Fx.transform.localPosition.y - 100,item.Fx.transform.localPosition.z),
        Vector3(item.Fx.transform.localPosition.x      ,item.Fx.transform.localPosition.y      ,item.Fx.transform.localPosition.z),
    }
    local m_FxPath = Table2Array(path, MakeArrayClass(Vector3))
    LuaTweenUtils.DODefaultLocalPath(item.Fx.transform, m_FxPath , 2)

    UIEventListener.Get(item.Icon.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        self:GetManualReward(item)
    end)
    UIEventListener.Get(item.LockSprite.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        if item.itemData then
            CItemInfoMgr.ShowLinkItemTemplateInfo(item.itemData.ItemID, false, nil, AlignType.Default, 0, 0, 0, 0)
        end
    end)

    return item
end

function LuaYanChiXiaWorkWnd:InitItemState(item)
    item.ChoiceBox.gameObject:SetActive(false)
    item.GetRewardSprite.enabled = false
    item.LockSprite.gameObject:SetActive(false)
    item.DisableSprite.enabled = false
    item.Fx.gameObject:SetActive(false)
    item.Icon:GetComponent(typeof(Collider)).enabled = false
end

function LuaYanChiXiaWorkWnd:ReloadItemData(item, level)
    local itemData = HanJia2020_Manual.GetData(level)
    item.itemData = itemData
    item.NumLabel.text = itemData.RewardCount
    item.Icon:LoadMaterial(Item_Item.GetData(itemData.ItemID).Icon)
    item.NumOfPliesLabel.text = String.Format(LocalString.GetString("{0}层"), level)
    self:InitItemState(item)

    item.ChoiceBox.gameObject:SetActive(level == self.curSelectIndex)
    --未解锁状态
    if self.curSelectIndex < level then
        item.LockSprite.gameObject:SetActive(true)
        item.DisableSprite.enabled = true
    elseif self.curSelectIndex == level and self.curManualsState[level] == 0 then
        if self.ownManualInfo[self.curManualIndex - 1] == 0 or level > self.curLevel then
            --未解锁状态
            item.LockSprite.gameObject:SetActive(true)
            item.DisableSprite.enabled = true
        else
            --可领取状态
            item.Icon:GetComponent(typeof(Collider)).enabled = true
            item.Fx.gameObject:SetActive(true)
        end
    elseif self.curManualsState[level] ~= 0 then
    --已领取状态
        item.DisableSprite.enabled = true
        item.GetRewardSprite.enabled = true
    end
end

function LuaYanChiXiaWorkWnd:ReloadManualsData()
    if self.curManuals == nil or #self.curManuals ~= 6 then return end
    for i = 1, 6 do
        local item = self.curManuals[i]
        self:ReloadItemData(item, self.curManualIndex * 6 + i - 6)
    end
end

function LuaYanChiXiaWorkWnd:OnBuyButtonClick()
    if self.ownManualInfo[self.curManualIndex - 1] == 1 then
        return
    end

    local txt = g_MessageMgr:FormatMessage("YanChiXiaWorkWnd_BuyManual_ShowPrice",HanJia2020_ManualPrice.GetData(self.curManualIndex).Price,
            HanJia2020_ManualPrice.GetData(self.curManualIndex).Name)
    MessageWndManager.ShowOKCancelMessage(txt, DelegateFactory.Action(function ()
        Gac2Gas.RequestBuyHanJiaManual(self.curManualIndex, HanJia2020Mgr:GetYanChiXiaWorkManualName(self.curManualIndex))
    end), nil, LocalString.GetString("确认"), LocalString.GetString("取消"), false)

end

function LuaYanChiXiaWorkWnd:GetManualReward()
    Gac2Gas.RequestGetManualReward(self.curSelectIndex)
end

function LuaYanChiXiaWorkWnd:OnOwnManualInfoUpdate(data)
    if data and data.Count and data.Count == 3 then
        self.ownManualInfo = data
    end

    self:UpdateManualsState()
end

function LuaYanChiXiaWorkWnd:SelectManual(index)
    self.radioBox:ChangeTo(index - 1, true)
end

function LuaYanChiXiaWorkWnd:UpdateManualsState()
    if self.ownManualInfo[0] == 0 and self.ownManualInfo[1] == 0 and self.ownManualInfo[2] == 0 then
        self:SelectManual(1)
        return
    end

    local isSelected = false
    local selectManualIndex = 0
    for i,enum in pairs(HanJia2020Mgr.sortedYanChiXiaWorkManualList) do
        if isSelected then
            break
        end
        --已购买
        if self.ownManualInfo[i - 1] ~= 0 then
            selectManualIndex = i
            self.curSelectIndex = i * 6 - 5
            for j = 1, 6 do
                local index = i * 6 + j - 6
                self.curSelectIndex = index
                --奖励未领取
                if self.curManualsState and not isSelected and self.curManualsState[index] == 0 then
                    isSelected = true
                    break
                end
            end
        else
            selectManualIndex = i
            self.curSelectIndex = i * 6 - 5
            isSelected = true
        end
    end

    if self.curSelectIndex > self.curLevel then
        self.curSelectIndex = self.curLevel
    end
    local newIndex = (math.modf(self.curLevel / 6 ) + 1)
    newIndex = math.min(math.max(0, newIndex), 3)
    self:SelectManual(isSelected and selectManualIndex or newIndex)
    self:ReloadManualsData()
end
----------------------------------------
---玩家当前状态及称谓
----------------------------------------
function LuaYanChiXiaWorkWnd:InitBottomRoot()
    UIEventListener.Get(self.BuyButton.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        self:OnBuyButtonClick()
    end)
    UIEventListener.Get(self.ExplanationIcon.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        self:ShowExplanation()
    end)
    self.curLevel = 1
end

function LuaYanChiXiaWorkWnd:ShowExplanation()
    g_MessageMgr:ShowMessage("message_yanchixiawork_readme")
end

function LuaYanChiXiaWorkWnd:OnWinePointUpdate(num)
    local rest = self.curWinePoint and self.curWinePoint or num
    local needPointToLevelUp = 0
    local curStateAndTitle = nil
    for i = 1, 18 do
        local data = HanJia2020_StateAndTitle.GetData(i)
        if rest >= data.JiuQi then
            rest = rest - data.JiuQi
        else
            needPointToLevelUp = data.JiuQi - rest
            curStateAndTitle = data
            break
        end
    end
    if curStateAndTitle == nil then
        curStateAndTitle = HanJia2020_StateAndTitle.GetData(18)
        needPointToLevelUp = 0
    end

    needPointToLevelUp, curStateAndTitle = self:OpenPilesSliderAnimation(rest, needPointToLevelUp, curStateAndTitle, num)

    self.curWinePoint = num
    self.curLevel = curStateAndTitle.Level
    self.NumOfPliesLabel.text = self.curLevel
    self.CurNameLabel.text = curStateAndTitle.Title
    if curStateAndTitle.Level > 12 then
        self.JiuQiSpecialIcon:LoadMaterial(HanJia2020_ManualPrice.GetData(EnumYanChiXiaWorkManual.TianMing).Icon)
    elseif curStateAndTitle.Level > 6 then
        self.JiuQiSpecialIcon:LoadMaterial(HanJia2020_ManualPrice.GetData(EnumYanChiXiaWorkManual.TianYi).Icon)
    else
        self.JiuQiSpecialIcon:LoadMaterial(HanJia2020_ManualPrice.GetData(EnumYanChiXiaWorkManual.TianYun).Icon)
    end
end

function LuaYanChiXiaWorkWnd:ClosePilesSliderAnimation(needPointToLevelUp, curStateAndTitle)
    self.canReceiveJiuQi = true
    self.CurNumOfPilesSliderFx:DestroyFx()
    self.LevelUpFx:DestroyFx()
    self:UpdateRestDesLabel(needPointToLevelUp, curStateAndTitle)
end

function LuaYanChiXiaWorkWnd:UpdateRestDesLabel(needPointToLevelUp, curStateAndTitle)
    if curStateAndTitle.Level == 18 then
        self.RestDesLabel.text = LocalString.GetString("当前已达到最高层数")
    else
        self.RestDesLabel.text = String.Format(LocalString.GetString("叠至下层还需{0}"), needPointToLevelUp)
    end
end

function LuaYanChiXiaWorkWnd:KillTweenList()
    if self.tweenList then
        for i = 1, #self.tweenList do
            LuaTweenUtils.Kill(self.tweenList[i], false)
        end
    end
    self.tweenList = {}
end

function LuaYanChiXiaWorkWnd:OpenPilesSliderAnimation(rest, needPointToLevelUp, curStateAndTitle, winePointNum)
    self:KillTweenList()
    self:UpdateRestDesLabel(needPointToLevelUp, curStateAndTitle)
    if curStateAndTitle.Level == 18 then
        rest = 0
        needPointToLevelUp = 10
    end

    self.CurNumOfPilesSlider.value = rest / curStateAndTitle.JiuQi
    if self.curWinePoint and winePointNum > self.curWinePoint and curStateAndTitle.Level ~= 18 then
        local increase = winePointNum - self.curWinePoint
        self.CurNumOfPilesSliderFx:DestroyFx()
        self.CurNumOfPilesSliderFx:LoadFx("fx/ui/prefab/UI_dagongshouce_jinduxing.prefab")
        self.canReceiveJiuQi = false
        if increase < needPointToLevelUp then
            needPointToLevelUp = needPointToLevelUp - increase
            local tween = LuaTweenUtils.TweenFloat(rest / curStateAndTitle.JiuQi, 1 - needPointToLevelUp / curStateAndTitle.JiuQi,  1, function (x)
                self.CurNumOfPilesSlider.value = x
            end)
            table.insert(self.tweenList,tween)
            LuaTweenUtils.OnComplete(tween, function ()
                self:ClosePilesSliderAnimation(needPointToLevelUp, curStateAndTitle)
            end)
        else
            self.LevelUpFx:LoadFx("fx/ui/prefab/UI_dagongshouce_shengji.prefab", 1, false)
            local tween1 = LuaTweenUtils.TweenFloat(rest / curStateAndTitle.JiuQi, 1, 1, function (x)
                self.CurNumOfPilesSlider.value = x
            end)
            table.insert(self.tweenList,tween1)
            curStateAndTitle = HanJia2020_StateAndTitle.GetData(curStateAndTitle.Level + 1)
            local extraJiuQi = increase - needPointToLevelUp
            for i = curStateAndTitle.Level,18 do
                curStateAndTitle = HanJia2020_StateAndTitle.GetData(i)
                if extraJiuQi >= curStateAndTitle.JiuQi then
                    extraJiuQi = extraJiuQi - curStateAndTitle.JiuQi
                else
                    needPointToLevelUp = curStateAndTitle.JiuQi - extraJiuQi
                    break
                end
            end
            if curStateAndTitle.Level == 18 then
                LuaTweenUtils.OnComplete(tween1, function ()
                    needPointToLevelUp = 10
                    self.CurNumOfPilesSlider.value = 0
                    self:ClosePilesSliderAnimation(needPointToLevelUp, curStateAndTitle)
                end)
            else
                LuaTweenUtils.OnComplete(tween1, function ()
                    self.CurNumOfPilesSlider.value = 0
                    local tween2 = LuaTweenUtils.TweenFloat(0, (extraJiuQi)/curStateAndTitle.JiuQi,  1, function (x)
                        self.CurNumOfPilesSlider.value = x
                    end)
                    table.insert(self.tweenList,tween2)
                    LuaTweenUtils.OnComplete(tween2, function ()
                        needPointToLevelUp = curStateAndTitle.JiuQi - extraJiuQi
                        self:ClosePilesSliderAnimation(needPointToLevelUp, curStateAndTitle)
                    end)
                end)
            end
        end
    end
    return needPointToLevelUp, curStateAndTitle
end
----------------------------------------
---公用事件
----------------------------------------
function LuaYanChiXiaWorkWnd:OnTabButtonChange(index)
    self.BuyButton.Enabled = true
    self.BuyButton.Text = HanJia2020Mgr:GetYanChiXiaWorkManualBuyInfo(index)
    if self.ownManualInfo then
        if self.ownManualInfo[index - 1] ~= 0 then
            self.BuyButton.Text = String.Format(LocalString.GetString("已购买{0}"), HanJia2020Mgr:GetYanChiXiaWorkManualName(index))
            self.BuyButton.Enabled = false
        end
    end
end

function LuaYanChiXiaWorkWnd:OnReplyWorkManualOverview(data)

    local tasksTable = data[1]
    local manualRewardList = data[2]
    local openBottleTimes = data[3]
    local winePoint = data[4]
    local ownManualDataList = data[5]
    self:ReloadTasksData(tasksTable)
    self:OnWinePointUpdate(winePoint)
    self:ReloadManualsStateData(manualRewardList)
    self:OnOwnManualInfoUpdate(ownManualDataList)
    self:OnNumOfOpenBottleTimesIncrease(openBottleTimes)

    self.transform:Find("Anchor").gameObject:SetActive(true)
end

function LuaYanChiXiaWorkWnd:ReloadTasksData(data)
    self.curFinishedTaskNum = 0
    for i = 1,8 do
        if data[i] then
            self:ReloadTaskData(self.curTasks[i], data[i])
            --已领取
            if data[i].status == 2 then
                self.curFinishedTaskNum  = self.curFinishedTaskNum + 1
            end
        end
    end
    self.RateOfTaskProgressLabel.text = self.curFinishedTaskNum .. "/8"
end

function LuaYanChiXiaWorkWnd:ReloadManualsStateData(data)
    self.curManualsState = {}

    for i = 1, 18 do
        table.insert(self.curManualsState, data[i - 1])
    end

    self:ReloadManualsData()
end

function LuaYanChiXiaWorkWnd:OnGetTaskReward(data)
    local taskID = data[1]
    local taskStatus = data[2]
    local winePoint = data[3]
    local rewardedTaskCount = data[4]
    local leftOpenBottleTimes = data[5]
    for i = 1, 8 do
        if self.curTasks[i].ID == taskID then
            local taskData = HanJia2020_Task.GetData(taskID)
            if taskData ~= nil then
                self:ReloadTaskData(self.curTasks[i],{subTaskId = taskID, progress = taskData.Count, status = taskStatus})
            end
            break
        end
    end

    self:OnNumOfOpenBottleTimesIncrease(leftOpenBottleTimes)
    self.curFinishedTaskNum = rewardedTaskCount
    self.RateOfTaskProgressLabel.text = rewardedTaskCount .. "/8"

    self:OnWinePointUpdate(winePoint)
    self:UpdateManualsState()
    self:ReloadManualsData()
end

function LuaYanChiXiaWorkWnd:OnBottleOpen(data)
    local OpenBottleTimes = data[1]
    self:OnNumOfOpenBottleTimesIncrease(OpenBottleTimes)
end

function LuaYanChiXiaWorkWnd:OnBuyManualSuccess(data)

    --local ownManualInfo = data[2]
    --self:OnOwnManualInfoUpdate(ownManualInfo)
end

function LuaYanChiXiaWorkWnd:OnGetManualReward(data)
    local level = data[1]
    self.curManualsState[level] = 1
    if level ~= 18 then
        local nextManualIndex = math.modf(  level / 6 ) + 1
        if self.ownManualInfo[nextManualIndex - 1] ~= 0 then
            self:SelectManual(nextManualIndex)
        end
    end
    if self.curSelectIndex < self.curLevel then
        local manualIndex = math.modf(  level/ 6 )
        if self.ownManualInfo[manualIndex] ~= 0 then
            self.curSelectIndex = level + 1
        end
    end
    self:ReloadManualsData()
end