local CClientObjInteractionWnd = import "L10.UI.CClientObjInteractionWnd"
local CClientObjInteractionMgr = import "L10.UI.CClientObjInteractionMgr"

CClientObjInteractionWnd.m_hookInit = function(this)
    this.swipeTimes = 0
    this.swipeProgressSprite.gameObject:SetActive(false)
    this.longpressProgressSprite.gameObject:SetActive(false)
    this.fx:DestroyFx()
    
    this.info = CClientObjInteractionMgr.Inst.Info
    if this.info.type == CClientObjInteractionMgr.EnumGestureType.LongPress then
        this.respanseWidget.transform:Find("CUIFx").gameObject:SetActive(true)
        this.titleLabel.text = SafeStringFormat3(LocalString.GetString("请在下面选框中长按%d秒"), math.floor(this.info.longPressThreshold))
    elseif this.info.type == CClientObjInteractionMgr.EnumGestureType.Swipe then
        this.respanseWidget.transform:Find("CUIFx").gameObject:SetActive(false)
        this.titleLabel.text = SafeStringFormat3(LocalString.GetString("请在下面选框中划动%d次"), this.info.swipeTimesThreshold)
    else
        this.respanseWidget.transform:Find("CUIFx").gameObject:SetActive(false)
        this.titleLabel.text = LocalString.GetString("不知所以")
    end
    
    this.respanseWidget.gameObject:SetActive(true)
    this:UpdateResponseArea()
end

CClientObjInteractionWnd.m_hookOnResponseGoPress = function(this, go, pressed)
    if this.info.type ~= CClientObjInteractionMgr.EnumGestureType.LongPress then
        return
    end
    if pressed then
        this.pressBegan = true
        this.dragHappened = false
        this.lastPressStartTime = Time.realtimeSinceStartup
        this.longpressProgressSprite.gameObject:SetActive(true)
        this.longpressProgressSprite.fillAmount = (Time.realtimeSinceStartup - this.lastPressStartTime) / this.info.longPressThreshold
    else
        this.pressBegan = false
        this.longpressProgressSprite.gameObject:SetActive(false)
        if Time.realtimeSinceStartup - this.lastPressStartTime > this.info.longPressThreshold and not this.dragHappened then
            this:OnGestureRecognize()
        end
    end
end
