local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local QnTableView = import "L10.UI.QnTableView"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CRankData = import "L10.UI.CRankData"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local AlignType = import "CPlayerInfoMgr+AlignType"
local Profession = import "L10.Game.Profession"
local CUITexture = import "L10.UI.CUITexture"
local CChatLinkMgr = import "CChatLinkMgr"
local UICamera = import "UICamera"

LuaLiuYi2023DaoDanXiaoGuiRankWnd = class()

-- @region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiRankWnd, "Rank", "Rank", GameObject)
RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiRankWnd, "NoTip", "NoTip", GameObject)
RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiRankWnd, "TopTeamTimeLab", "TopTeamTimeLab", UILabel)
RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiRankWnd, "MemberTable", "MemberTable", QnTableView)
RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiRankWnd, "MainPlayerInfo", "MainPlayerInfo", GameObject)
RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiRankWnd, "TableView", "TableView", QnAdvanceGridView)
RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiRankWnd, "RewardLab", "RewardLab", UILabel)

-- @endregion RegistChildComponent end
RegistClassMember(LuaLiuYi2023DaoDanXiaoGuiRankWnd, "m_RankList")
RegistClassMember(LuaLiuYi2023DaoDanXiaoGuiRankWnd, "m_MemberList")
RegistClassMember(LuaLiuYi2023DaoDanXiaoGuiRankWnd, "m_Tick")
RegistClassMember(LuaLiuYi2023DaoDanXiaoGuiRankWnd, "m_Tweener")
RegistClassMember(LuaLiuYi2023DaoDanXiaoGuiRankWnd, "m_CurBubble")
RegistClassMember(LuaLiuYi2023DaoDanXiaoGuiRankWnd, "m_RankId")
RegistClassMember(LuaLiuYi2023DaoDanXiaoGuiRankWnd, "m_BubbleStrs")

function LuaLiuYi2023DaoDanXiaoGuiRankWnd:Awake()
    -- @region EventBind: Dont Modify Manually!
    -- @endregion EventBind end
    self.m_RankId = tonumber(LiuYi2023_DaoDanXiaoGui.GetData("RankId").Value)
    self.m_BubbleStrs = {}
    local strs = g_LuaUtil:StrSplit(((LiuYi2023_DaoDanXiaoGui.GetData("RankBubbles") or {}).Value or ""), ";")
    for i = 1, #strs do
        if strs[i] ~= "" then
            table.insert(self.m_BubbleStrs, strs[i])
        end 
    end

    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(function()
        return #self.m_RankList
        end, function(item, index)
        item:SetBackgroundTexture(index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)

        self:InitItem(item, self.m_RankList[index + 1])
    end)
    self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        local data = self.m_RankList[row + 1]
        if data then
            CPlayerInfoMgr.ShowPlayerPopupMenu(data.PlayerIndex, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined,
                "", nil, Vector3.zero, AlignType.Default)
        end
    end)

    self.MemberTable.m_DataSource = DefaultTableViewDataSource.Create(function()
        return #self.m_MemberList
        end, function(item, index)
        self:InitMemberItem(item, self.m_MemberList[index + 1])
    end)
    self.MemberTable.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        local data = self.m_MemberList[row + 1]
        if data then
            CPlayerInfoMgr.ShowPlayerPopupMenu(data.PlayerIndex, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined,
                "", nil, Vector3.zero, AlignType.Default)
        end
    end)
    UIEventListener.Get(self.RewardLab.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        local url = p:GetComponent(typeof(UILabel)):GetUrlAtPosition(UICamera.lastWorldPosition)
        if url ~= nil then
            CChatLinkMgr.ProcessLinkClick(url, nil)
        end
    end)
end

function LuaLiuYi2023DaoDanXiaoGuiRankWnd:Init()
    self.RewardLab.text = CChatLinkMgr.TranslateToNGUIText(g_MessageMgr:FormatMessage("LIUYI2023_DAODANXIAOGUI_REWARDTIP"))

    self.Rank:SetActive(false)
    self.NoTip:SetActive(true)

    Gac2Gas.QueryRank(self.m_RankId)
end

-- @region UIEvent

-- @endregion UIEvent

function LuaLiuYi2023DaoDanXiaoGuiRankWnd:OnEnable()
    g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaLiuYi2023DaoDanXiaoGuiRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaLiuYi2023DaoDanXiaoGuiRankWnd:OnRankDataReady()
    local myInfo = CRankData.Inst.MainPlayerRankInfo
    if myInfo == nil or myInfo.rankId ~= self.m_RankId then
        return
    end
    if CRankData.Inst.RankList.Count > 0 then
        self.Rank:SetActive(true)
        self.NoTip:SetActive(false)

        if CClientMainPlayer.Inst then
            myInfo.Name = CClientMainPlayer.Inst.Name
        end
        
        self:InitItem(self.MainPlayerInfo, myInfo)

        self.m_RankList = {}
        for i= 1, CRankData.Inst.RankList.Count do
            table.insert(self.m_RankList, CRankData.Inst.RankList[i-1])
        end
        self.TableView:ReloadData(true,false)

        self:InitTopTeam()
    end
end

function LuaLiuYi2023DaoDanXiaoGuiRankWnd:InitItem(item, info)
    local tf = item.transform
    local timeLabel = tf:Find("TimeLabel"):GetComponent(typeof(UILabel))
    timeLabel.text = ""
    local iconSprite = tf:Find("CareerIcon"):GetComponent(typeof(UISprite))
    iconSprite.spriteName = ""
    local nameLabel = tf:Find("NameLabel"):GetComponent(typeof(UILabel))
    nameLabel.text = ""
    local rankLabel = tf:Find("RankLabel"):GetComponent(typeof(UILabel))
    rankLabel.text = ""
    local rankSpriteTf = tf:Find("RankLabel/RankImage")
    if rankSpriteTf then
        local rankSprite = rankSpriteTf:GetComponent(typeof(UISprite))
        rankSprite.spriteName = ""
        local rank = info.Rank
        if rank == 1 then
            rankSprite.spriteName = "Rank_No.1"
        elseif rank == 2 then
            rankSprite.spriteName = "Rank_No.2png"
        elseif rank == 3 then
            rankSprite.spriteName = "Rank_No.3png"
        else
            rankLabel.text = tostring(rank)
        end
    else 
        rankLabel.text = info.Rank > 0 and info.Rank or LocalString.GetString("未上榜")
    end

    iconSprite.spriteName = Profession.GetIcon(info.Job)
    nameLabel.text = info.Name
    timeLabel.text = self:GetRemainTimeText(info.Value)
end

function LuaLiuYi2023DaoDanXiaoGuiRankWnd:InitTopTeam()
    self.m_MemberList = {}
    for i = 1, 5 do
        if i <= CRankData.Inst.RankList.Count then
            table.insert(self.m_MemberList, CRankData.Inst.RankList[i-1])
        end
    end
    self.TopTeamTimeLab.text = #self.m_MemberList > 0 and self:GetRemainTimeText(self.m_MemberList[1].Value or 0) or "00:00"
    self.MemberTable:ReloadData(true, false)

    self.m_Tick = RegisterTick(function()
        self:OnTick()
    end, 10 * 1000)
    self:OnTick()
end

function LuaLiuYi2023DaoDanXiaoGuiRankWnd:InitMemberItem(item, info)
    local tf = item.transform
    local portrait      = tf:GetComponent(typeof(CUITexture))
    local playerNameLab = tf:Find("PlayerName"):GetComponent(typeof(UILabel))
    local bubbleLab     = tf:Find("BubbleLab"):GetComponent(typeof(UILabel))

    local extraUD = info.extraData
	local extra = extraUD and MsgPackImpl.unpack(extraUD)
	local gender = extra[1]

    local portraitName = CUICommonDef.GetPortraitName(EnumToInt(info.Job) or 1, gender, -1)
	portrait:LoadNPCPortrait(portraitName,false)
    playerNameLab.text = info.Name
    bubbleLab.gameObject:SetActive(false)
end

function LuaLiuYi2023DaoDanXiaoGuiRankWnd:GetRemainTimeText(totalSeconds)
    if totalSeconds <= 1 then
        return ""
    end
    local time
    if totalSeconds >= 3600 then
        time = SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        time = SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
    return time
end

function LuaLiuYi2023DaoDanXiaoGuiRankWnd:OnTick()
    if self.m_CurBubble then
        LuaTweenUtils.Kill(self.m_Tweener, false)
        self.m_CurBubble.gameObject:SetActive(false)
        self.m_CurBubble = nil
    end
    if self.m_MemberList and #self.m_MemberList > 0 then
        local curBubbleIdx = math.random(1, #self.m_MemberList) - 1
        local bubbleStr = #self.m_BubbleStrs > 0 and self.m_BubbleStrs[math.random(1, #self.m_BubbleStrs)] or ""

        local item = self.MemberTable.m_Grid.transform:GetChild(curBubbleIdx)
        if item then
            self.m_CurBubble = item:Find("BubbleLab"):GetComponent(typeof(UILabel))
            self.m_CurBubble.alpha = 1
            self.m_CurBubble.text = bubbleStr
            self.m_CurBubble.gameObject:SetActive(true)
            self.m_Tweener = LuaTweenUtils.TweenFloat(1, 0, 1, function ( val )
                if self.m_CurBubble then
                    self.m_CurBubble.alpha = val
                end
            end)
            LuaTweenUtils.SetDelay(self.m_Tweener, 3)
            CommonDefs.OnComplete_Tweener(self.m_Tweener, DelegateFactory.TweenCallback(function ()
                self.m_CurBubble.gameObject:SetActive(false)
                self.m_CurBubble = nil
            end))
        end
    end
end

function LuaLiuYi2023DaoDanXiaoGuiRankWnd:OnDestroy()
    UnRegisterTick(self.m_Tick)
    LuaTweenUtils.Kill(self.m_Tweener, false)
end