-- Auto Generated!!
local CGuildTrainWnd = import "L10.UI.CGuildTrainWnd"
local Int32 = import "System.Int32"
CGuildTrainWnd.m_OnTrainInfoUpdate_CS2LuaHook = function (this) 
    this.transform:Find("Anchor").gameObject:SetActive(true)

    this.topTabs:Init()
    this.trainWindows:Init(this.curSelectTab)
    this.topTabs.OnTabChanged = MakeDelegateFromCSFunction(this.OnTabChanged, MakeGenericClass(Action1, Int32), this)
end
