local GameObject = import "UnityEngine.GameObject"
local CUIManager = import "L10.UI.CUIManager"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local QnButton = import "L10.UI.QnButton"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local QnNewSlider = import "L10.UI.QnNewSlider"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType	= import "L10.UI.CItemInfoMgr+AlignType"
local DelegateFactory = import "DelegateFactory"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local LocalString = import "LocalString"
local LuaGameObject = import "LuaGameObject"

LuaCommonTongXingZhengBuyLevelMgr = {}
LuaCommonTongXingZhengBuyLevelMgr.data = nil

-- CurLv 当前等级
-- MaxLv 最大等级
-- CurExp 当前等级已有经验
-- RewardList Key(等级)--{needExp,ItemList{ID,Count,IsBind,weight}}
-- OneJadeAddValue 1灵玉增加的进度值
-- IconPath 图标路径
-- buyCallBack 点击购买按钮的回调，参数(BuyCost,BuyExp,BuyLevel)(购买花费，购买经验，购买等级)
function LuaCommonTongXingZhengBuyLevelMgr:ShowBuyLevelWnd(curLv,maxLv,curExp,rewardList,oneJadeAddValue,iconPath,buyCallBack)
    LuaCommonTongXingZhengBuyLevelMgr.data = {}
    LuaCommonTongXingZhengBuyLevelMgr.data.CurLv = curLv
    LuaCommonTongXingZhengBuyLevelMgr.data.MaxLv = maxLv
    LuaCommonTongXingZhengBuyLevelMgr.data.CurExp = curExp
    LuaCommonTongXingZhengBuyLevelMgr.data.RewardList = rewardList
    LuaCommonTongXingZhengBuyLevelMgr.data.OneJadeAddValue = oneJadeAddValue
    LuaCommonTongXingZhengBuyLevelMgr.data.IconPath = iconPath
    LuaCommonTongXingZhengBuyLevelMgr.data.BuyCallBack = buyCallBack
    CUIManager.ShowUI(CLuaUIResources.CommonTongXingZhengBuyLevelWnd)
end

LuaCommonTongXingZhengBuyLevelWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaCommonTongXingZhengBuyLevelWnd, "BuyBtn", "BuyBtn", GameObject)
RegistChildComponent(LuaCommonTongXingZhengBuyLevelWnd, "QnCostAndOwnMoney", "QnCostAndOwnMoney", CCurentMoneyCtrl)
RegistChildComponent(LuaCommonTongXingZhengBuyLevelWnd, "Slider2", "Slider2", QnNewSlider)
RegistChildComponent(LuaCommonTongXingZhengBuyLevelWnd, "DelBtn", "DelBtn", GameObject)
RegistChildComponent(LuaCommonTongXingZhengBuyLevelWnd, "AddBtn", "AddBtn", GameObject)
RegistChildComponent(LuaCommonTongXingZhengBuyLevelWnd, "Icon", "Icon", GameObject)
RegistChildComponent(LuaCommonTongXingZhengBuyLevelWnd, "DesLabel", "DesLabel", UILabel)
RegistChildComponent(LuaCommonTongXingZhengBuyLevelWnd, "TableView", "TableView", QnAdvanceGridView)
RegistChildComponent(LuaCommonTongXingZhengBuyLevelWnd, "TitleLabel", "TitleLabel", UILabel)
--@endregion RegistChildComponent end
RegistClassMember(LuaCommonTongXingZhengBuyLevelWnd, "m_BuyLevel")
RegistClassMember(LuaCommonTongXingZhengBuyLevelWnd, "m_BuyExp")
RegistClassMember(LuaCommonTongXingZhengBuyLevelWnd, "m_BuyCost")
RegistClassMember(LuaCommonTongXingZhengBuyLevelWnd, "m_IgnoreModify")
RegistClassMember(LuaCommonTongXingZhengBuyLevelWnd, "m_SelectedItem")
RegistClassMember(LuaCommonTongXingZhengBuyLevelWnd, "m_ModifyLevelTick")
function LuaCommonTongXingZhengBuyLevelWnd:Awake()
    self.m_BuyLevel = 1
    self.m_BuyExp = 0
    self.m_BuyCost = 0
    self.m_IgnoreModify = false
    self.m_SelectedItem = nil
    self.m_ModifyLevelTick = nil
    UIEventListener.Get(self.BuyBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnBuyBtnClick()
    end)

    UIEventListener.Get(self.DelBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnModifyLevel(-1)
    end)

    UIEventListener.Get(self.AddBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnModifyLevel(1)
    end)
    -- 按钮长按
    UIEventListener.Get(self.DelBtn).onPress = DelegateFactory.BoolDelegate(function (go, isPressed)
        UnRegisterTick(self.m_ModifyLevelTick)
        if isPressed then
            self.m_ModifyLevelTick = RegisterTick(function ()
                self:OnModifyLevel(-1)
            end, 200)
        end
    end)
    UIEventListener.Get(self.AddBtn).onPress = DelegateFactory.BoolDelegate(function (go, isPressed)
        UnRegisterTick(self.m_ModifyLevelTick)
        if isPressed then
            self.m_ModifyLevelTick = RegisterTick(function ()
                self:OnModifyLevel(1)
            end, 200)
        end
    end)
    self.Slider2.OnValueChanged = DelegateFactory.Action_float(function(value)

        if self.IgnorModify then 
            self.IgnorModify = false
            return 
        end
        local data = LuaCommonTongXingZhengBuyLevelMgr.data
        local buymax = data.MaxLv - data.CurLv
        self.m_BuyLevel = math.min(math.max(math.floor((data.MaxLv - data.CurLv)*value),1),buymax)
        self:InitBuyInfo()
    end)
end

function LuaCommonTongXingZhengBuyLevelWnd:Init()
    local data = LuaCommonTongXingZhengBuyLevelMgr.data
    if data.IconPath then
        self.Icon:SetActive(true)
        self.Icon:GetComponent(typeof(CUITexture)):LoadMaterial(data.IconPath)
    else
        self.Icon:SetActive(false)
    end

    local lv = self.m_BuyLevel + data.CurLv
    if lv <= data.MaxLv then
        self:OnModifyLevel(0)
    end
end
function LuaCommonTongXingZhengBuyLevelWnd:OnModifyLevel(value)
    self.IgnorModify = true
    local data = LuaCommonTongXingZhengBuyLevelMgr.data
    local buymax = data.MaxLv - data.CurLv
    self.m_BuyLevel = math.min(math.max(self.m_BuyLevel + value,1),buymax)
    self.Slider2.m_Value = self.m_BuyLevel / buymax
    self:InitBuyInfo()
end

function LuaCommonTongXingZhengBuyLevelWnd:AddItem(item,items,itemids)
    if item == nil then return end
    if itemids[item.ItemID] == nil then
        itemids[item.ItemID] = item.Count
        items[#items+1] = item
    else
        itemids[item.ItemID] = itemids[item.ItemID] + item.Count
    end
end

function LuaCommonTongXingZhengBuyLevelWnd:InitBuyInfo()
    local data = LuaCommonTongXingZhengBuyLevelMgr.data
    local lv = self.m_BuyLevel + data.CurLv
    local exp = 0
    local itemdatas = {}            --itemarray:    index   ->  item={ItemID,Count,IsBind,weight}
    local itemids = {}              --itemdic:      itemid  ->  count
    for i = data.CurLv+1,lv do         --从下一级开始
        local RewardInfo = data.RewardList[i]     
        if RewardInfo ~= nil then 
            exp = exp + RewardInfo.needExp
            if i == data.CurLv + 1 then exp = exp - data.CurExp end
            if RewardInfo.ItemList ~= nil then
                for j = 1,#RewardInfo.ItemList do
                    self:AddItem(RewardInfo.ItemList[j],itemdatas,itemids)
                end
            end

        end
    end


    local cost = math.ceil(exp / data.OneJadeAddValue) 
    
    self.DesLabel.text = SafeStringFormat3(LocalString.GetString("升至[FFFF00]%d级[-],购买[FFFF00]%d[-]"),lv,exp)
    self.Icon:GetComponent(typeof(UIWidget)):ResetAndUpdateAnchors()
    self.TitleLabel.text = g_MessageMgr:FormatMessage("Tcjh_BuyLevelTitle",lv)
    
    self.QnCostAndOwnMoney:SetType(4, 0, false)
    self.QnCostAndOwnMoney:SetCost(cost)

    self.m_BuyExp = exp
    self.m_BuyCost = cost
    -- 排序，weight越小越靠前
    table.sort(itemdatas,function(a,b)
        if a.weight and b.weight and a.weight ~= b.weight then
            return a.weight < b.weight
        else
            return a.ItemID < b.ItemID
        end
    end)
    local len = #itemdatas
    local initfunc = function(item,index)
        local data = itemdatas[index+1]
        local count = itemids[data.ItemID]
        self:FillItemCell(item,data,count)
    end
    self.TableView.m_DataSource = DefaultTableViewDataSource.CreateByCount(len,initfunc)
    self.TableView:Clear()
    self.TableView:ReloadData(true, true)

    local dbtn = self.DelBtn:GetComponent(typeof(QnButton))
    local abtn = self.AddBtn:GetComponent(typeof(QnButton))

    local buymax = data.MaxLv - data.CurLv
    dbtn.Enabled = self.m_BuyLevel > 1
    abtn.Enabled = self.m_BuyLevel < buymax
    if self.m_BuyLevel <= 1 or self.m_BuyLevel >= buymax then
        UnRegisterTick(self.m_ModifyLevelTick)
        self.m_ModifyLevelTick = nil
    end
end

function LuaCommonTongXingZhengBuyLevelWnd:FillItemCell(item,data,count)

    local cell = item.transform
    local icon = LuaGameObject.GetChildNoGC(cell,"IconTexture").cTexture
    local bindgo = LuaGameObject.GetChildNoGC(cell,"BindSprite").gameObject
    local countlb = LuaGameObject.GetChildNoGC(cell,"AmountLabel").label

    local itemid = data.ItemID
    local isBind = data.IsBind
    
    local itemcfg = Item_Item.GetData(itemid)

    if itemcfg then
        icon:LoadMaterial(itemcfg.Icon)
    end

    bindgo:SetActive(tonumber(isBind) == 1)

    if tonumber(count) <= 1 then
        countlb.text = ""
    else
        countlb.text = count
    end

    UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemid, false, nil, AlignType.Default, 0, 0, 0, 0) 
        if self.SelectedItem ~= go then
            if self.SelectedItem then
                self:SelectItem(self.SelectedItem,false)
            end
            self.SelectedItem = go
            self:SelectItem(self.SelectedItem,true)
        end
    end)

    item.gameObject:SetActive(true)
end
function LuaCommonTongXingZhengBuyLevelWnd:SelectItem(itemgo,selected)
    local selectgo = LuaGameObject.GetChildNoGC(itemgo.transform,"SelectedSprite").gameObject
    selectgo:SetActive(selected)
end
function LuaCommonTongXingZhengBuyLevelWnd:OnBuyBtnClick()
    local data = LuaCommonTongXingZhengBuyLevelMgr.data
    if data.BuyCallBack then
        data.BuyCallBack(self.m_BuyCost,self.m_BuyExp,self.m_BuyLevel)
    end
end
function LuaCommonTongXingZhengBuyLevelWnd:OnDisable()
    UnRegisterTick(self.m_ModifyLevelTick)
    self.m_ModifyLevelTick = nil
    LuaCommonTongXingZhengBuyLevelMgr.data = nil
end
--@region UIEvent

--@endregion UIEvent

