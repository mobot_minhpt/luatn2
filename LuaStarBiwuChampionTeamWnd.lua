local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local Constants = import "L10.Game.Constants"
local UISprite = import "UISprite"
local Profession = import "L10.Game.Profession"
local NGUIText = import "NGUIText"

CLuaStarBiwuChampionTeamWnd = class()
CLuaStarBiwuChampionTeamWnd.Path = "ui/starbiwu/LuaStarBiwuChampionTeamWnd"

RegistClassMember(CLuaStarBiwuChampionTeamWnd, "m_GridView")
RegistClassMember(CLuaStarBiwuChampionTeamWnd, "m_SelectSprite")
RegistClassMember(CLuaStarBiwuChampionTeamWnd, "m_SelectIndex")

function CLuaStarBiwuChampionTeamWnd:Init()
  self.m_GridView = self.transform:Find("Anchor/AdvView"):GetComponent(typeof(QnAdvanceGridView))
  self.m_GridView.m_DataSource = DefaultTableViewDataSource.Create(function()
			return #CLuaStarBiwuMgr.m_HistoryDataTable[CLuaStarBiwuMgr.m_HistoryTeamIndex]["TeamTable"]
		end, function(item, index)
			self:InitItem(item.gameObject, index + 1)
		end)
  self.m_GridView:ReloadData(true, false)
end

function CLuaStarBiwuChampionTeamWnd:InitItem(obj, index)
  local bgSprite = obj.transform:GetComponent(typeof(UISprite))
  bgSprite.spriteName = index % 2 == 0 and Constants.NewOddBgSprite or Constants.NewEvenBgSprite
  UIEventListener.Get(bgSprite.gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
  	if self.m_SelectSprite then 
      self.m_SelectSprite.spriteName = self.m_SelectIndex % 2 == 0 and Constants.NewOddBgSprite or Constants.NewEvenBgSprite 
    end
  	self.m_SelectIndex = index
  	self.m_SelectSprite = bgSprite
  	bgSprite.spriteName = Constants.ChosenItemBgSpriteName
  end)

  local memberData = CLuaStarBiwuMgr.m_HistoryDataTable[CLuaStarBiwuMgr.m_HistoryTeamIndex]["TeamTable"][index]
  obj.transform:Find("LeaderFlag").gameObject:SetActive(memberData.Id == CLuaStarBiwuMgr.m_HistoryDataTable[CLuaStarBiwuMgr.m_HistoryTeamIndex]["LeaderId"])
  obj.transform:Find("ServerNameLabel"):GetComponent(typeof(UILabel)).text = memberData.ServerName
  obj.transform:Find("PlayerNameLabel"):GetComponent(typeof(UILabel)).text = memberData.Name
  local levelLabel = obj.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
  levelLabel.text = "lv."..(memberData.FeiShengGrade > 0 and memberData.FeiShengGrade or memberData.Grade)
  levelLabel.color = memberData.FeiShengGrade > 0 and NGUIText.ParseColor24(Constants.ColorOfFeiSheng, 0) or Color.white
  obj.transform:Find("ClassSprite"):GetComponent(typeof(UISprite)).spriteName = Profession.GetIconByNumber(memberData.Class)
end
