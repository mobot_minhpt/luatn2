local GameObject = import "UnityEngine.GameObject"
local CGamePlayMgr = import "L10.Game.CGamePlayMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaWuLiangCompanionBtnWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaWuLiangCompanionBtnWnd, "Btn", "Btn", GameObject)
RegistChildComponent(LuaWuLiangCompanionBtnWnd, "LeaveButton", "LeaveButton", GameObject)

--@endregion RegistChildComponent end

function LuaWuLiangCompanionBtnWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.Btn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBtnClick()
	end)


	
	UIEventListener.Get(self.LeaveButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeaveButtonClick()
	end)


    --@endregion EventBind end
end

function LuaWuLiangCompanionBtnWnd:Init()

end

function LuaWuLiangCompanionBtnWnd:GetGuideGo(methodName)
    if methodName == "GetBtn" then
        return self.Btn
	end
	return nil
end

--@region UIEvent

function LuaWuLiangCompanionBtnWnd:OnBtnClick()
	CUIManager.ShowUI(CLuaUIResources.WuLiangCompanionInfoWnd)
end


function LuaWuLiangCompanionBtnWnd:OnLeaveButtonClick()
	CGamePlayMgr.Inst:LeavePlay()
end


--@endregion UIEvent

