local CBaseWnd = import "L10.UI.CBaseWnd"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local Money = import "L10.Game.Money"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"

LuaJingLingQuickPurchaseWnd = class()
--道具显示
RegistClassMember(LuaJingLingQuickPurchaseWnd,"m_ItemIconTexture")
RegistClassMember(LuaJingLingQuickPurchaseWnd,"m_ItemQualitySprite")
RegistClassMember(LuaJingLingQuickPurchaseWnd,"m_ItemNameLabel")
--数量选择
RegistClassMember(LuaJingLingQuickPurchaseWnd,"m_AddSubAndInputButton")
--支付手段选择
RegistClassMember(LuaJingLingQuickPurchaseWnd,"m_PurchaseIconSprite")
RegistClassMember(LuaJingLingQuickPurchaseWnd,"m_PurchaseLabel")
RegistClassMember(LuaJingLingQuickPurchaseWnd,"m_PurchaseSwitchButton")
RegistClassMember(LuaJingLingQuickPurchaseWnd,"m_OwnNumLabel")

--操作按钮
RegistClassMember(LuaJingLingQuickPurchaseWnd,"m_OkButton")
RegistClassMember(LuaJingLingQuickPurchaseWnd,"m_CancelButton")

RegistClassMember(LuaJingLingQuickPurchaseWnd,"m_TemplateId")
RegistClassMember(LuaJingLingQuickPurchaseWnd,"m_PurchaseType")
RegistClassMember(LuaJingLingQuickPurchaseWnd,"m_CurPurchaseIdx") --当前支付方式 1. 元宝 2. 灵玉，仅限于可以同时用灵玉和元宝购买的商品

function LuaJingLingQuickPurchaseWnd:Awake()
    
end

function LuaJingLingQuickPurchaseWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaJingLingQuickPurchaseWnd:Init()
	
	self.m_ItemIconTexture = self.transform:Find("Anchor/ItemCell/IconTexture"):GetComponent(typeof(CUITexture))
	self.m_ItemQualitySprite = self.transform:Find("Anchor/ItemCell/QualitySprite"):GetComponent(typeof(UISprite))
	self.m_ItemNameLabel = self.transform:Find("Anchor/ItemCell/NameLabel"):GetComponent(typeof(UILabel))

	self.m_AddSubAndInputButton = self.transform:Find("Anchor/QnIncreseAndDecreaseButton"):GetComponent(typeof(QnAddSubAndInputButton))
	
	self.m_PurchaseIconSprite = self.transform:Find("Anchor/Price/Icon"):GetComponent(typeof(UISprite))
	self.m_PurchaseLabel = self.transform:Find("Anchor/Price/TotalPriceLabel"):GetComponent(typeof(UILabel))
	self.m_PurchaseSwitchButton = self.transform:Find("Anchor/Price/SwitchButton").gameObject
	self.m_OwnNumLabel = self.transform:Find("Anchor/OwnNumLabel"):GetComponent(typeof(UILabel))

	self.m_OkButton = self.transform:Find("Anchor/OkButton").gameObject
	self.m_CancelButton = self.transform:Find("Anchor/CancelButton").gameObject

	CommonDefs.AddOnClickListener(self.m_PurchaseSwitchButton, DelegateFactory.Action_GameObject(function(go) self:OnPurchaseSwitchButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_OkButton, DelegateFactory.Action_GameObject(function(go) self:OnOKButtonClick() end), false)
	CommonDefs.AddOnClickListener(self.m_CancelButton, DelegateFactory.Action_GameObject(function(go) self:OnCancelButtonClick() end), false)
	
	self.m_AddSubAndInputButton.onValueChanged = DelegateFactory.Action_uint(function(val)
		self:OnPurchaseNumChanged(val)
		end)

	self:InitContent()
end

function LuaJingLingQuickPurchaseWnd:InitContent()
	local purchaseInfo = LuaJingLingMgr:GetQuickPurchaseInfo()
	local itemTemplateId = purchaseInfo and purchaseInfo.templateId
	local purchaseType = purchaseInfo and purchaseInfo.purchaseType
	if Item_Item.Exists(itemTemplateId) then
		local data = Item_Item.GetData(itemTemplateId)
		self.m_ItemIconTexture:LoadMaterial(data.Icon)
		self.m_ItemNameLabel.text = data.Name
		if purchaseType == EnumJingLingQuickPurchaseType.eRMB then
			self.m_AddSubAndInputButton:SetMinMax(1, 1, 1) -- RMB礼包限制一次只能买一个
		else
			self.m_AddSubAndInputButton:SetMinMax(1, 999, 1)
		end
		self.m_AddSubAndInputButton:SetValue(1, true)

		self.m_PurchaseSwitchButton:SetActive(purchaseType == EnumJingLingQuickPurchaseType.eLingYuAndYuanBao)

	else
		self:Close()
	end

	self:ShowOwnNum()
end

function LuaJingLingQuickPurchaseWnd:OnPurchaseNumChanged(val)
	self:DisplayCostInfo()
end

function LuaJingLingQuickPurchaseWnd:ShowOwnNum()
	local purchaseInfo = LuaJingLingMgr:GetQuickPurchaseInfo()
	local itemTemplateId = purchaseInfo and purchaseInfo.templateId or 0
	self.m_OwnNumLabel.text = SafeStringFormat3(LocalString.GetString("已拥有%d个"), CItemMgr.Inst:GetItemCount(itemTemplateId))
end

function LuaJingLingQuickPurchaseWnd:DisplayCostInfo()
	local purchaseInfo = LuaJingLingMgr:GetQuickPurchaseInfo()
	local itemTemplateId = purchaseInfo and purchaseInfo.templateId
	local purchaseType = purchaseInfo and purchaseInfo.purchaseType
	if Item_Item.Exists(itemTemplateId) then
		if purchaseType == EnumJingLingQuickPurchaseType.eRMB then
			self:ShowRMBCostInfo(itemTemplateId)
	
		elseif purchaseType == EnumJingLingQuickPurchaseType.eLingYu then
			self:ShowLingYuCostInfo(itemTemplateId)
	
		elseif purchaseType == EnumJingLingQuickPurchaseType.eYuanBao then
			self:ShowYuanBaoCostInfo(itemTemplateId)
	
		elseif purchaseType == EnumJingLingQuickPurchaseType.eLingYuAndYuanBao then
			if self.m_CurPurchaseIdx and self.m_CurPurchaseIdx == 1 then
				self:ShowYuanBaoCostInfo(itemTemplateId)
			else
				self:ShowLingYuCostInfo(itemTemplateId)
			end
		else
			self.m_PurchaseIconSprite.spriteName = ""
			self.m_PurchaseLabel.text = ""
		end
	else
		self.m_PurchaseIconSprite.spriteName = ""
		self.m_PurchaseLabel.text = ""
	end
end

function LuaJingLingQuickPurchaseWnd:ShowLingYuCostInfo(templateId)
	self.m_PurchaseIconSprite.spriteName = Money.GetIconName(EnumMoneyType.LingYu)
	if Mall_LingYuMall.Exists(templateId) then
		local data = Mall_LingYuMall.GetData(templateId)
		self.m_PurchaseLabel.text = tostring(data.Jade*self.m_AddSubAndInputButton:GetValue())
	elseif Mall_LingYuMallLimit.Exists(templateId) then
		local data = Mall_LingYuMallLimit.GetData(templateId)
		self.m_PurchaseLabel.text = tostring(data.Jade*self.m_AddSubAndInputButton:GetValue())
	else
		self.m_PurchaseIconSprite.spriteName = ""
		self.m_PurchaseLabel.text = ""
	end
end

function LuaJingLingQuickPurchaseWnd:ShowYuanBaoCostInfo(templateId)
	self.m_PurchaseIconSprite.spriteName = Money.GetIconName(EnumMoneyType.YuanBao)
	local data = Mall_YuanBaoMall.GetData(templateId)
	if data then
		self.m_PurchaseLabel.text = tostring(data.YuanBao*self.m_AddSubAndInputButton:GetValue())
	end
end

function LuaJingLingQuickPurchaseWnd:ShowRMBCostInfo(templateId)
	self.m_PurchaseIconSprite.spriteName = ""
	local data = Mall_LingYuMallLimit.GetData(templateId)
	if data then
		self.m_PurchaseLabel.text = SafeStringFormat3(LocalString.GetString("%d元"), data.Jade*self.m_AddSubAndInputButton:GetValue())
	end
end

function LuaJingLingQuickPurchaseWnd:OnEnable()
	g_ScriptEvent:AddListener("MainPlayerUpdateMoney", self, "OnMainPlayerUpdateMoney")
	g_ScriptEvent:AddListener("SendItem", self, "OnSendItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "OnSetItemAt")
end

function LuaJingLingQuickPurchaseWnd:OnDisable()
	g_ScriptEvent:RemoveListener("MainPlayerUpdateMoney", self, "OnMainPlayerUpdateMoney")
	g_ScriptEvent:RemoveListener("SendItem", self, "OnSendItem")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "OnSetItemAt")
end

function LuaJingLingQuickPurchaseWnd:OnMainPlayerUpdateMoney(args)
	--考虑到绑定灵玉的特殊情况，这里暂时先不处理
end

function LuaJingLingQuickPurchaseWnd:OnSendItem(args)
	local itemId = args[0]
	local item = CItemMgr.Inst:GetById(itemId)
	if not item then return end
	local purchaseInfo = LuaJingLingMgr:GetQuickPurchaseInfo()
	local itemTemplateId = (purchaseInfo and purchaseInfo.templateId or 0)
	if itemTemplateId~=item.TemplateId then return end

	self:ShowOwnNum()
end

function LuaJingLingQuickPurchaseWnd:OnSetItemAt(args)
	local place = args[0]
	local pos = args[1]
	local oldItemId = args[2]
	local newItemId = args[3]

	if place ~= EnumItemPlace.Bag then return end

	self:ShowOwnNum()
end

function LuaJingLingQuickPurchaseWnd:OnPurchaseSwitchButtonClick()

	local purchaseInfo = LuaJingLingMgr:GetQuickPurchaseInfo()
	local itemTemplateId = purchaseInfo and purchaseInfo.templateId
	local purchaseType = purchaseInfo and purchaseInfo.purchaseType
	if purchaseType == EnumJingLingQuickPurchaseType.eLingYuAndYuanBao then

		if self.m_CurPurchaseIdx and self.m_CurPurchaseIdx == 1 then
			--当前是元宝，切换到灵玉
			self.m_CurPurchaseIdx = 2
		else
			--默认或者已选择灵玉，切换到元宝
			self.m_CurPurchaseIdx = 1
		end
		self:DisplayCostInfo()
	end
end

function LuaJingLingQuickPurchaseWnd:OnOKButtonClick()
	local purchaseInfo = LuaJingLingMgr:GetQuickPurchaseInfo()
	local itemTemplateId = purchaseInfo and purchaseInfo.templateId
	local purchaseType = purchaseInfo and purchaseInfo.purchaseType
	if Item_Item.Exists(itemTemplateId) then
		if purchaseType == EnumJingLingQuickPurchaseType.eRMB then
			LuaJingLingMgr:QuickPurchaseRMBItem(itemTemplateId, self.m_AddSubAndInputButton:GetValue())	
		elseif purchaseType == EnumJingLingQuickPurchaseType.eLingYu then
			LuaJingLingMgr:QuickPurchaseLingYuMallItem(itemTemplateId, self.m_AddSubAndInputButton:GetValue())
		elseif purchaseType == EnumJingLingQuickPurchaseType.eYuanBao then
			LuaJingLingMgr:QuickPurchaseYuanBaoMallItem(itemTemplateId, self.m_AddSubAndInputButton:GetValue())
		elseif purchaseType == EnumJingLingQuickPurchaseType.eLingYuAndYuanBao then
			if self.m_CurPurchaseIdx and self.m_CurPurchaseIdx == 1 then
				LuaJingLingMgr:QuickPurchaseYuanBaoMallItem(itemTemplateId, self.m_AddSubAndInputButton:GetValue())
			else
				LuaJingLingMgr:QuickPurchaseLingYuMallItem(itemTemplateId, self.m_AddSubAndInputButton:GetValue())
			end
		end
	end
end

function LuaJingLingQuickPurchaseWnd:OnCancelButtonClick()
	self:Close()
end
