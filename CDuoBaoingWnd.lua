-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CDuoBaoingWnd = import "L10.UI.CDuoBaoingWnd"
local CDuoBaoMgr = import "L10.UI.CDuoBaoMgr"
local CDuobaoTableItem = import "L10.UI.CDuobaoTableItem"
local CommonDefs = import "L10.Game.CommonDefs"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local DelegateFactory = import "DelegateFactory"
local EGlobalDuoBaoStatus = import "L10.UI.EGlobalDuoBaoStatus"
local EnumEventType = import "EnumEventType"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local EventManager = import "EventManager"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local QnButton = import "L10.UI.QnButton"
local UInt32 = import "System.UInt32"
CDuoBaoingWnd.m_Init_CS2LuaHook = function (this) 
    this.m_Table.m_DataSource = this
    this.m_BuyCountButton:SetMinMax(1, 999, 1)

    this.m_RadioBox:ChangeTo(0, true)
    if this.m_CurrentItems.Count > 0 then
        this.CurrentSelectedItem = 0
        this.m_Table:SetSelectRow(this.CurrentSelectedItem, true)
    else
        this.m_ModelPreviewer:UpdateData(EGlobalDuoBaoStatus.NotStarted, nil)
    end
    CDuoBaoMgr.Inst:QueryCurrentDuoBaoStatus()
    CDuoBaoMgr.Inst:QueryDuoBaoInfoOverView()
end
CDuoBaoingWnd.m_OnEnable_CS2LuaHook = function (this) 
    this.m_Table.OnSelectAtRow = CommonDefs.CombineListner_Action_int(this.m_Table.OnSelectAtRow, MakeDelegateFromCSFunction(this.OnItemClick, MakeGenericClass(Action1, Int32), this), true)
    this.m_BuyCountButton.onValueChanged = CommonDefs.CombineListner_Action_uint(this.m_BuyCountButton.onValueChanged, MakeDelegateFromCSFunction(this.OnBuyCountChanged, MakeGenericClass(Action1, UInt32), this), true)
    this.m_RadioBox.OnSelect = CommonDefs.CombineListner_Action_QnButton_int(this.m_RadioBox.OnSelect, MakeDelegateFromCSFunction(this.OnRadioBoxChanged, MakeGenericClass(Action2, QnButton, Int32), this), true)
    this.m_BuyButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_BuyButton.OnClick, MakeDelegateFromCSFunction(this.OnBuyButtonClick, MakeGenericClass(Action1, QnButton), this), true)
    this.m_TipButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_TipButton.OnClick, MakeDelegateFromCSFunction(this.OnTipButtonClick, MakeGenericClass(Action1, QnButton), this), true)
    EventManager.AddListener(EnumEventType.ReplyDuoBaoOverviewEnd, MakeDelegateFromCSFunction(this.OnReplyDuoBaoOverviewEnd, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.ReplyDuoBaoPlayStatus, MakeDelegateFromCSFunction(this.OnReplyDuoBaoPlayStatus, MakeGenericClass(Action1, UInt32), this))
end
CDuoBaoingWnd.m_OnDisable_CS2LuaHook = function (this) 
    this.m_Table.OnSelectAtRow = CommonDefs.CombineListner_Action_int(this.m_Table.OnSelectAtRow, MakeDelegateFromCSFunction(this.OnItemClick, MakeGenericClass(Action1, Int32), this), false)
    this.m_BuyCountButton.onValueChanged = CommonDefs.CombineListner_Action_uint(this.m_BuyCountButton.onValueChanged, MakeDelegateFromCSFunction(this.OnBuyCountChanged, MakeGenericClass(Action1, UInt32), this), false)
    this.m_RadioBox.OnSelect = CommonDefs.CombineListner_Action_QnButton_int(this.m_RadioBox.OnSelect, MakeDelegateFromCSFunction(this.OnRadioBoxChanged, MakeGenericClass(Action2, QnButton, Int32), this), false)
    this.m_BuyButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_BuyButton.OnClick, MakeDelegateFromCSFunction(this.OnBuyButtonClick, MakeGenericClass(Action1, QnButton), this), false)
    this.m_TipButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_TipButton.OnClick, MakeDelegateFromCSFunction(this.OnTipButtonClick, MakeGenericClass(Action1, QnButton), this), false)
    EventManager.RemoveListener(EnumEventType.ReplyDuoBaoOverviewEnd, MakeDelegateFromCSFunction(this.OnReplyDuoBaoOverviewEnd, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.ReplyDuoBaoPlayStatus, MakeDelegateFromCSFunction(this.OnReplyDuoBaoPlayStatus, MakeGenericClass(Action1, UInt32), this))
end
CDuoBaoingWnd.m_ItemAt_CS2LuaHook = function (this, view, row) 
    local item = TypeAs(view:GetFromPool(0), typeof(CDuobaoTableItem))
    item:UpdateData(CDuoBaoMgr.Inst.GlobalDuoBaoStatus, this.m_CurrentItems[row])
    item.OnClickItemTexture = (DelegateFactory.Action(function () 
        view:SetSelectRow(row, true)
    end))
    return item
end
CDuoBaoingWnd.m_OnItemClick_CS2LuaHook = function (this, row) 
    this.CurrentSelectedItem = row
    local maxvalue = 999
    if this.m_CurrentItems ~= nil and this.m_CurrentItems.Count > this.CurrentSelectedItem then
        local info = this.m_CurrentItems[this.CurrentSelectedItem]
        maxvalue = math.min(math.floor(info.Buy_Max) - info.MyInvestInCurrentGroup, info.Need_FenShu - info.CurrentGroupProgress)
        maxvalue = math.floor(math.max(1, maxvalue))
    end
    this.m_BuyCountButton:SetMinMax(1, maxvalue, 1)
    this.m_BuyCountButton:SetValue(1, true)
    this:UpdateTotalPrice()
    this:UpdatePreview(row)
end
CDuoBaoingWnd.m_OnRadioBoxChanged_CS2LuaHook = function (this, button, index) 
    this.m_CurrentCategory = index + 1
    this.CurrentSelectedItem = - 1
    if CDuoBaoMgr.Inst:IsLingYuDuoBao(this.m_CurrentCategory) then
        this.m_MoneyCtrl:SetType(EnumMoneyType.LingYu, EnumPlayScoreKey.NONE, true)
    else
        this.m_MoneyCtrl:SetType(EnumMoneyType.YinLiang, EnumPlayScoreKey.NONE, true)
    end
    this:UpdateView(false)
end
CDuoBaoingWnd.m_OnBuyButtonClick_CS2LuaHook = function (this, button) 

    if this.m_CurrentItems ~= nil and this.m_CurrentItems.Count > this.CurrentSelectedItem and this.CurrentSelectedItem >= 0 then
        local count = this.m_BuyCountButton:GetValue()
        local info = this.m_CurrentItems[this.CurrentSelectedItem]
        local maxFenShu = math.floor(info.Buy_Max)
        if this.CurrentSelectedItem >= 0 and count > 0 then
            if CDuoBaoMgr.Inst:IsLingYuDuoBao(info) then
                local cost = count * CDuoBaoMgr.Inst:GetYinLiang_PerFenShu(info)
                if not CShopMallMgr.TryCheckEnoughJade(cost, 0) then
                    return
                end
            else
                if CClientMainPlayer.Inst.Silver < count * CDuoBaoMgr.Inst:GetYinLiang_PerFenShu(info) then
                    g_MessageMgr:ShowMessage("SILVER_NOT_ENOUGH")
                    return
                end
            end
            if CClientMainPlayer.Inst.MaxLevel < CDuoBaoMgr.Inst.LevelLimit then
                g_MessageMgr:ShowMessage("message_yinliangduobao_Lv_too_Low", CDuoBaoMgr.Inst.LevelLimit)
            elseif count + info.MyInvestInCurrentGroup > info.Need_FenShu then
                CDuoBaoMgr.Inst:QueryDuoBaoInfoOverView()
                g_MessageMgr:ShowMessage("message_yinliangduobao_touru_is_too_much", maxFenShu)
            elseif info.MyInvestInCurrentGroup + count > maxFenShu then
                CDuoBaoMgr.Inst:QueryDuoBaoInfoOverView()
                g_MessageMgr:ShowMessage("message_yinliangduobao_touru_is_too_much", maxFenShu)
            else
                if info.Need_FenShu == 1 then
                    local format = g_MessageMgr:FormatMessage("PLAYERSHOP_BUY_CONFIRM", count * CDuoBaoMgr.Inst:GetYinLiang_PerFenShu(info), 1, info.ItemName)
                    MessageWndManager.ShowOKCancelMessage(format, DelegateFactory.Action(function () 
                        CDuoBaoMgr.Inst:RequestBuyDuoBao(this.m_CurrentItems[this.CurrentSelectedItem].ID, count)
                    end), nil, nil, nil, false)
                else
                    CDuoBaoMgr.Inst:RequestBuyDuoBao(this.m_CurrentItems[this.CurrentSelectedItem].ID, count)
                end
            end
        end
    end
end
CDuoBaoingWnd.m_UpdateView_CS2LuaHook = function (this, updatePosition) 

    --m_ModelPreviewer.UpdateData(null);
    this.m_CurrentItems = CDuoBaoMgr.Inst:GetDuoBaoInfosByCategory(this.m_CurrentCategory)
    this.m_Table:ReloadData(true, updatePosition)
    if this.m_CurrentItems ~= nil and this.m_CurrentItems.Count > 0 then
        this.m_BuyCountButton:SetMinMax(1, 999, 1)
        if this.m_BuyCountButton:GetValue() > 999 or this.m_BuyCountButton:GetValue() < 1 then
            this.m_BuyCountButton:SetValue(1, true)
        end
        if this.CurrentSelectedItem >= 0 and this.CurrentSelectedItem < this.m_CurrentItems.Count then
            this.m_Table:SetSelectRow(this.CurrentSelectedItem, true)
        else
            this.m_Table:SetSelectRow(0, true)
        end
    else
        this.m_BuyCountButton:SetMinMax(0, 0, 1)
        this.m_BuyCountButton:SetValue(0, true)
        this.CurrentSelectedItem = - 1
        this.m_ModelPreviewer:UpdateData(EGlobalDuoBaoStatus.NotStarted, nil)
    end
    this:UpdateTotalPrice()
end
CDuoBaoingWnd.m_UpdateTotalPrice_CS2LuaHook = function (this) 

    local totalprice = 0
    if this.m_CurrentItems ~= nil and this.m_CurrentItems.Count > this.CurrentSelectedItem and this.CurrentSelectedItem >= 0 then
        totalprice = CDuoBaoMgr.Inst:GetYinLiang_PerFenShu(this.m_CurrentItems[this.CurrentSelectedItem]) * this.m_BuyCountButton:GetValue()
    end
    this.m_MoneyCtrl:SetCost(totalprice)
end
CDuoBaoingWnd.m_UpdatePreview_CS2LuaHook = function (this, index) 
    local template = nil
    if index >= 0 and index <= this.m_CurrentItems.Count then
        template = this.m_CurrentItems[index]
    end
    local default
    if (template.Need_FenShu == 1) then
        default = LocalString.GetString("一口价")
    else
        default = LocalString.GetString("投入")
    end
    this.m_BuyButton.Text = default
    this.m_ModelPreviewer:UpdateData(CDuoBaoMgr.Inst.GlobalDuoBaoStatus, template)
end
CDuoBaoingWnd.m_OnReplyDuoBaoOverviewEnd_CS2LuaHook = function (this) 
    local linyuItems = CDuoBaoMgr.Inst:GetDuoBaoInfosByCategory(4)
    if linyuItems == nil or linyuItems.Count == 0 then
        this.m_RadioBox:SetVisibleGroup(CDuoBaoingWnd.noLingYuGroup, false)
    else
        this.m_RadioBox:SetVisibleGroup(CDuoBaoingWnd.hasLingYuGroup, false)
    end
    this:UpdateView(true)
end
CDuoBaoingWnd.m_OnReplyDuoBaoPlayStatus_CS2LuaHook = function (this, mytotalDuobao) 
    this:UpdateTimeTick()
    if CDuoBaoMgr.Inst.GlobalDuoBaoStatus == EGlobalDuoBaoStatus.DuoBaoView or CDuoBaoMgr.Inst.GlobalDuoBaoStatus == EGlobalDuoBaoStatus.DuoBaoing then
        this:StartTimeCountTick()
    else
        this:DestroyTimeCountTick()
    end
    this.m_BuyButton.Enabled = (CDuoBaoMgr.Inst.GlobalDuoBaoStatus == EGlobalDuoBaoStatus.DuoBaoing)
    this.m_DuoBaoStatusLabel.text = System.String.Format(LocalString.GetString("[acf8ff]夺宝空间[-] {0}/{1}"), mytotalDuobao, CDuoBaoMgr.Inst.InvestLimit)
    this.m_Table:ReloadData(false, true)
end
CDuoBaoingWnd.m_DestroyTimeCountTick_CS2LuaHook = function (this) 
    if this.m_TimeTick ~= nil then
        invoke(this.m_TimeTick)
        this.m_TimeTick = nil
    end
end
