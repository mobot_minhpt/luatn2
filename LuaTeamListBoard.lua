local CTeamListBoard = import "L10.UI.CTeamListBoard"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CTeamFollowMgr = import "L10.Game.CTeamFollowMgr"
local COpenEntryMgr = import "L10.Game.COpenEntryMgr"

CTeamListBoard.m_OnMemberFollowButtonClick_CS2LuaHook = function (this,go)
    if CClientMainPlayer.Inst == nil or not CTeamMgr.Inst:TeamExists() or CTeamMgr.Inst:MainPlayerIsTeamLeader() then
        return
    end
    if not CTeamFollowMgr.Inst:IsTeamFollowing(CClientMainPlayer.Inst.Id) then
        if LuaGuildTerritorialWarsMgr:IsChallengePlayOpen() then
            Gac2Gas.GTWChallengeTeleportToTeamLeader()
            return
        elseif LuaGuildTerritorialWarsMgr:IsInPlay() or LuaGuildTerritorialWarsMgr:IsPeripheryPlay() then
            Gac2Gas.TerritoryWarTeleportToGuildLeader()
            return
        elseif LuaJuDianBattleMgr:IsInJuDian() or LuaJuDianBattleMgr:IsInGameplay() or LuaJuDianBattleMgr:IsInDailyGameplay() then
            Gac2Gas.GuildJuDianTeleportToLeaderScene()
            return
        end
        CTeamFollowMgr.Inst:RequestTeamFollow()
    else
        CTeamFollowMgr.Inst:RequestCancelTeamFollow()
    end
end

CTeamListBoard.m_hookOnLeaderFollowButtonClick = function (this,go)
    if LuaGuildTerritorialWarsMgr:IsChallengePlayOpen() then
        Gac2Gas.GTWChallengeAskMemberTeleport()
        return
    elseif LuaGuildTerritorialWarsMgr:IsInPlay() or LuaGuildTerritorialWarsMgr:IsPeripheryPlay() then
        Gac2Gas.GuildTerritoryWarRequestTeamMemberTeleport()
        return
    end
    CTeamFollowMgr.Inst:RequestSummonTeammate()
end

CTeamListBoard.m_hookOpenTeamRecruitWnd = function (this, go)
    LuaTeamRecruitMgr:OpenTeamRecruitWnd()
end

CTeamListBoard.m_TeamRecruitGuideHook = function (this)
    if not COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.TeamRecruitInTaskAndTeamGuide) then
        CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.TeamRecruitInTaskAndTeamGuide)
    end
end
