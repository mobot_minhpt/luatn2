local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local Animation = import "UnityEngine.Animation"
local DelegateFactory  = import "DelegateFactory"

LuaPengDaoResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaPengDaoResultWnd, "RankBtn", "RankBtn", GameObject)
RegistChildComponent(LuaPengDaoResultWnd, "ShopBtn", "ShopBtn", GameObject)
RegistChildComponent(LuaPengDaoResultWnd, "NextBtn", "NextBtn", GameObject)
RegistChildComponent(LuaPengDaoResultWnd, "Failed", "Failed", GameObject)
RegistChildComponent(LuaPengDaoResultWnd, "Success", "Success", GameObject)
RegistChildComponent(LuaPengDaoResultWnd, "ScoreLabel", "ScoreLabel", UILabel)
RegistChildComponent(LuaPengDaoResultWnd, "GotScoreLabel", "GotScoreLabel", UILabel)
RegistChildComponent(LuaPengDaoResultWnd, "NumLabel", "NumLabel", UILabel)
RegistChildComponent(LuaPengDaoResultWnd, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaPengDaoResultWnd, "ValLabel1", "ValLabel1", UILabel)
RegistChildComponent(LuaPengDaoResultWnd, "ValLabel2", "ValLabel2", UILabel)
RegistChildComponent(LuaPengDaoResultWnd, "ValLabel3", "ValLabel3", UILabel)
RegistChildComponent(LuaPengDaoResultWnd, "BreakRecord", "BreakRecord", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaPengDaoResultWnd,"m_Ani")

function LuaPengDaoResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.RankBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRankBtnClick()
	end)

	UIEventListener.Get(self.ShopBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShopBtnClick()
	end)

	UIEventListener.Get(self.NextBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnNextBtnClick()
	end)

    --@endregion EventBind end

	if CommonDefs.IS_VN_CLIENT then
		self.Success.transform:Find("pengdaoresultwnd_tiao (1)").transform.localPosition = Vector3(-238, 4, 0)
		self.Success.transform:Find("pengdaoresultwnd_zhan (1)").transform.localPosition = Vector3(-68, -10, 0)
		self.Success.transform:Find("pengdaoresultwnd_cheng").transform.localPosition = Vector3(87, 2.7, 0)
		self.Success.transform:Find("pengdaoresultwnd_gong").transform.localPosition = Vector3(244, -10, 0)

		self.Failed.transform:Find("pengdaoresultwnd_tiao").transform.localPosition = Vector3(-261, 6, 0)
		self.Failed.transform:Find("pengdaoresultwnd_zhan").transform.localPosition = Vector3(-68, -8, 0)
		self.Failed.transform:Find("pengdaoresultwnd_jie").transform.localPosition = Vector3(112, 13, 0)
		self.Failed.transform:Find("pengdaoresultwnd_zhan").transform.localPosition = Vector3(294, -3, 0)
	end
end

function LuaPengDaoResultWnd:Init()
	self.m_Ani = self.gameObject:GetComponent(typeof(Animation))
	local t = LuaPengDaoMgr.m_PlayResultInfo
	local bSuccess, difficulty1, difficulty2, difficulty3, passedLevel, usedTime, score, breakRecord, rewardScore = t[1],t[2],t[3],t[4],t[5],t[6],t[7],t[8],t[9]
	self.Success.gameObject:SetActive(bSuccess)
	self.Failed.gameObject:SetActive(not bSuccess)
	self.ValLabel1.text = SafeStringFormat3(LocalString.GetString("生存 %d级"),difficulty1)
	self.ValLabel2.text = SafeStringFormat3(LocalString.GetString("攻击 %d级"), difficulty2)
	self.ValLabel3.text = SafeStringFormat3(LocalString.GetString("腐蚀 %d级"), difficulty3)
	self.NumLabel.text = passedLevel
	self.TimeLabel.text = SafeStringFormat3(LocalString.GetString("%02d:%02d"), math.floor((usedTime % 3600) / 60), usedTime % 60)
	self.ScoreLabel.text = score
	self.BreakRecord.gameObject:SetActive(breakRecord)
	self.GotScoreLabel.text = SafeStringFormat3(LocalString.GetString("获得积分 %d"),rewardScore)
	self.m_Ani:Play(bSuccess and "pengdaoresultwnd_win" or "pengdaoresultwnd_end")
end

--@region UIEvent

function LuaPengDaoResultWnd:OnRankBtnClick()
	CUIManager.ShowUI(CLuaUIResources.PengDaoRankWnd)
end

function LuaPengDaoResultWnd:OnShopBtnClick()
	CLuaNPCShopInfoMgr.ShowScoreShopById(PengDaoFuYao_Setting.GetData().PengDaoFuYaoShopId)
end

function LuaPengDaoResultWnd:OnNextBtnClick()
	CUIManager.CloseUI(CLuaUIResources.PengDaoResultWnd)
	CUIManager.ShowUI(CLuaUIResources.PengDaoSelectDifficultyWnd)
end


--@endregion UIEvent

