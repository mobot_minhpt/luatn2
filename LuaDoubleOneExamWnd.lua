local CKeJuOptionTemplate = import "L10.UI.CKeJuOptionTemplate"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UIGrid = import "UIGrid"
local BoxCollider = import "UnityEngine.BoxCollider"
local Vector3 = import "UnityEngine.Vector3"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaDoubleOneExamWnd = class()

RegistClassMember(LuaDoubleOneExamWnd, "Question")
RegistClassMember(LuaDoubleOneExamWnd, "NumberLabel")
RegistClassMember(LuaDoubleOneExamWnd, "CountDownLabel")
RegistClassMember(LuaDoubleOneExamWnd, "TopicLabel")
RegistClassMember(LuaDoubleOneExamWnd, "TopicScrollview")
RegistClassMember(LuaDoubleOneExamWnd, "OptionGrid")
RegistClassMember(LuaDoubleOneExamWnd, "OptionTemplate")
RegistClassMember(LuaDoubleOneExamWnd, "InstructionLabel")
RegistClassMember(LuaDoubleOneExamWnd, "CloseButton")

RegistClassMember(LuaDoubleOneExamWnd, "AnswerList")
RegistClassMember(LuaDoubleOneExamWnd, "SelectedIndex")
RegistClassMember(LuaDoubleOneExamWnd, "QuestionEndTime")
RegistClassMember(LuaDoubleOneExamWnd, "CurrentTime")
RegistClassMember(LuaDoubleOneExamWnd, "CountDownTick")

function LuaDoubleOneExamWnd:Init()
	self:InitClassMembers()
	self:InitValues()
end

function LuaDoubleOneExamWnd:InitClassMembers()
	self.Question = self.transform:Find("Anchor/Question").gameObject
	self.NumberLabel = self.transform:Find("Anchor/Question/NumberLabel"):GetComponent(typeof(UILabel))
	self.CountDownLabel = self.transform:Find("Anchor/Question/CountDownLabel"):GetComponent(typeof(UILabel))

	self.TopicLabel = self.transform:Find("Anchor/Question/TopicScrollview/TopicLabel"):GetComponent(typeof(UILabel))
	self.TopicScrollview = self.transform:Find("Anchor/Question/TopicScrollview"):GetComponent(typeof(CUIRestrictScrollView))
	self.OptionGrid = self.transform:Find("Anchor/Question/OptionGrid"):GetComponent(typeof(UIGrid))
	self.OptionTemplate = self.transform:Find("Anchor/Question/OptionTemplate").gameObject
	self.OptionTemplate:SetActive(false)

	self.InstructionLabel = self.transform:Find("Anchor/InstructionLabel"):GetComponent(typeof(UILabel))
	self.CloseButton = self.transform:Find("Wnd_Bg_Secondary_3/CloseButton").gameObject
end

function LuaDoubleOneExamWnd:InitValues()
	self.AnswerList = {}
	self.SelectedIndex = 0

	self.QuestionEndTime = CServerTimeMgr.Inst.timeStamp + LuaAuthenData.m_QuestionCountDown
	if self.CountDownTick ~= nil then
      UnRegisterTick(self.CountDownTick)
      self.CountDownTick=nil
    end

    local onCloseButtonClick = function (go)
    	self:TryToStopTest(go)
    end
    CommonDefs.AddOnClickListener(self.CloseButton,DelegateFactory.Action_GameObject(onCloseButtonClick),false)

	self:InitQuestion()
end

function LuaDoubleOneExamWnd:InitQuestion()
	CUICommonDef.ClearTransform(self.OptionGrid.transform)

	for i = 1, #LuaAuthenData.m_AnswerList, 1 do
		local go = NGUITools.AddChild(self.OptionGrid.gameObject, self.OptionTemplate)
		self:InitOption(go, i, LuaAuthenData.m_AnswerList[i])
		go:SetActive(true)
		table.insert(self.AnswerList, go)
	end

	self.OptionGrid:Reposition()

	self.TopicLabel.text = LuaAuthenData.m_Question
	self.TopicScrollview:ResetPosition()

	self.NumberLabel.text = ""
	local totalQuestCount = LuaAuthenData.m_TotalQuestionCount
	if LuaAuthenData.m_CurrentQuestionIndex > 0 and LuaAuthenData.m_CurrentQuestionIndex <= totalQuestCount then
		self.NumberLabel.text = SafeStringFormat3(LocalString.GetString("第%d/%d题"), LuaAuthenData.m_CurrentQuestionIndex, totalQuestCount)
	end

	self.InstructionLabel.text = SafeStringFormat3(LocalString.GetString("[ACF8FF]得分超过%d分通过测试。[-]当前得分[FFFF00]%d[-]分。"), LuaAuthenData.m_PassScore, LuaAuthenData.m_TotalScore)

	local currentTime = self.QuestionEndTime - CServerTimeMgr.Inst.timeStamp - 1
	self.CountDownLabel.text = SafeStringFormat3(LocalString.GetString("%d秒"), math.ceil(currentTime))

	self.CountDownTick = RegisterTick(function ()
		local currentTime = self.QuestionEndTime - CServerTimeMgr.Inst.timeStamp - 1
		if currentTime >= 0 then
			self.CountDownLabel.text = SafeStringFormat3(LocalString.GetString("%d秒"), math.ceil(currentTime))
		end
	end, 300)
end

function LuaDoubleOneExamWnd:InitOption(go, index, answer)
	local optionLabel = ""
	if index == 1 then
		optionLabel = "A."
	elseif index == 2 then
		optionLabel = "B."
	elseif index == 3 then
		optionLabel = "C."
	elseif index == 4 then
		optionLabel = "D."
	end

	local answerOption = SafeStringFormat3("%s%s", optionLabel, answer)
	local optionScrpit = go.transform:GetComponent(typeof(CKeJuOptionTemplate))
	optionScrpit:Init(answerOption)

	local box = go.transform:GetComponent(typeof(BoxCollider))
	local sprite = go.transform:GetComponent(typeof(UISprite))

	box.size = Vector3(sprite.width, sprite.height, 1)

	local onAnswerClick = function (go)
		self:OnAnswerClick(go, index)
	end
	CommonDefs.AddOnClickListener(go,DelegateFactory.Action_GameObject(onAnswerClick), false)
end

function LuaDoubleOneExamWnd:OnAnswerClick(go, index)
	for i = 1, #self.AnswerList, 1 do
		local box = self.AnswerList[i]:GetComponent(typeof(BoxCollider))
		box.size = Vector3.one

		if go == self.AnswerList[i] then
			self.SelectedIndex = i
			Gac2Gas.AnswerAuthenticationQuestion(self.SelectedIndex)
		end
	end
end

function LuaDoubleOneExamWnd:OnEnable()
	g_ScriptEvent:AddListener("AuthenAnswerUpdate", self, "OnQuestionAnswerUpdate")
	g_ScriptEvent:AddListener("AuthenInfoUpdate", self, "InitValues")
end

function LuaDoubleOneExamWnd:OnDisable()
	g_ScriptEvent:RemoveListener("AuthenAnswerUpdate", self, "OnQuestionAnswerUpdate")
	g_ScriptEvent:RemoveListener("AuthenInfoUpdate", self, "InitValues")
end

function LuaDoubleOneExamWnd:OnDestroy()
	if self.CountDownTick ~= nil then
      UnRegisterTick(self.CountDownTick)
      self.CountDownTick=nil
    end
end

function LuaDoubleOneExamWnd:OnQuestionAnswerUpdate()
	for  i = 1, #self.AnswerList, 1 do
		if i ~= LuaAuthenData.m_RightAnswerIndex and i == self.SelectedIndex then -- 玩家选择了
			local option = self.AnswerList[i]:GetComponent(typeof(CKeJuOptionTemplate))
			if option then
				option:SetResult(false)
			end
		elseif i ~= LuaAuthenData.m_RightAnswerIndex and i ~= self.SelectedIndex then
			local box = self.AnswerList[i]:GetComponent(typeof(BoxCollider))
			box.size = Vector3.one
		else
			local box = self.AnswerList[i]:GetComponent(typeof(BoxCollider))
			box.size = Vector3.one
			local option = self.AnswerList[i]:GetComponent(typeof(CKeJuOptionTemplate))
			if option then
				option:SetResult(true)
			end
		end
	end
end

function LuaDoubleOneExamWnd:TryToStopTest(go)
	local msg = g_MessageMgr:FormatMessage("Rearing_Test_Stop_Test_Confirm")
	MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
		Gac2Gas.EndAuthentication()
		CUIManager.CloseUI("DoubleOneExamWnd")
	end), nil, nil, nil, false)
end

return LuaDoubleOneExamWnd
