local QnButton = import "L10.UI.QnButton"
local UITabBar = import "L10.UI.UITabBar"
local QnCheckBox = import "L10.UI.QnCheckBox"
local EnumVoicePlatform = import "L10.Game.EnumVoicePlatform"
local EnumRecordContext = import "L10.Game.EnumRecordContext"
local EChatPanel = import "L10.Game.EChatPanel"
local VoiceRecordInfo = import "L10.Game.VoiceRecordInfo"
local CInputBoxMgr = import "L10.UI.CInputBoxMgr"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"

CLuaMusicBoxRecordWnd = class()
CLuaMusicBoxRecordWnd.Path = "ui/musicbox/LuaMusicBoxRecordWnd"

RegistClassMember(CLuaMusicBoxRecordWnd, "m_MakeButton")
RegistClassMember(CLuaMusicBoxRecordWnd, "m_TabBar")
RegistClassMember(CLuaMusicBoxRecordWnd, "m_RecordRootObj")
RegistClassMember(CLuaMusicBoxRecordWnd, "m_BgRootObj")
RegistClassMember(CLuaMusicBoxRecordWnd, "m_RecordStatusObjTable")

RegistClassMember(CLuaMusicBoxRecordWnd, "m_BgListViewRootObj")
RegistClassMember(CLuaMusicBoxRecordWnd, "m_BgListeningObj")

RegistClassMember(CLuaMusicBoxRecordWnd, "m_CheckBoxTable")
RegistClassMember(CLuaMusicBoxRecordWnd, "m_BgMusicId")

RegistClassMember(CLuaMusicBoxRecordWnd, "m_CountdownLabel")
RegistClassMember(CLuaMusicBoxRecordWnd, "m_Count")

RegistClassMember(CLuaMusicBoxRecordWnd, "m_RecordTimeLabel")

RegistClassMember(CLuaMusicBoxRecordWnd, "m_RecordingTick")
RegistClassMember(CLuaMusicBoxRecordWnd, "m_CountdownTick")
RegistClassMember(CLuaMusicBoxRecordWnd, "m_MaxRecordTime")
RegistClassMember(CLuaMusicBoxRecordWnd, "m_CurRecordTime")

RegistClassMember(CLuaMusicBoxRecordWnd, "m_HaveVoice")

RegistClassMember(CLuaMusicBoxRecordWnd, "m_CurrentTabIndex")
RegistClassMember(CLuaMusicBoxRecordWnd, "m_VoiceUrl")
function CLuaMusicBoxRecordWnd:Init()
  self.m_MaxRecordTime = 30
  self.m_HaveVoice = false
  UIEventListener.Get(self.transform:Find("Anchor/TipBtn").gameObject).onClick = LuaUtils.VoidDelegate(function()
    g_MessageMgr:ShowMessage("MusicBox_Record_Tip")
  end)
  UIEventListener.Get(self.transform:Find("Wnd_Bg/CloseButton").gameObject).onClick = LuaUtils.VoidDelegate(function()
    if self.m_HaveVoice then
      g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("YINYUEHE_CLOSE"), function()
        CUIManager.CloseUI(CLuaUIResources.MusicBoxRecordWnd)
      end, nil, nil, nil, false)
    else
      CUIManager.CloseUI(CLuaUIResources.MusicBoxRecordWnd)
    end
  end)
  self.m_MakeButton = self.transform:Find("Anchor/MakeBtn"):GetComponent(typeof(QnButton))
  self.m_MakeButton.Enabled = false
  UIEventListener.Get(self.m_MakeButton.gameObject).onClick = LuaUtils.VoidDelegate(function()
    self:DoMake()
  end)

  self.m_RecordRootObj = self.transform:Find("Anchor/RecordRoot").gameObject
  self.m_BgRootObj = self.transform:Find("Anchor/BgRoot").gameObject

  self.m_RecordStatusObjTable = {}
  local nameTbl = {"Begin", "Recording", "Countdown", "Listening", "End"}
  for i = 1, #nameTbl do
    self.m_RecordStatusObjTable[nameTbl[i]] = self.m_RecordRootObj.transform:Find(nameTbl[i]).gameObject
    self.m_RecordStatusObjTable[nameTbl[i]]:SetActive(false)
  end
  self.m_RecordStatusObjTable["Begin"]:SetActive(true)

  self:InitRecordTab()
  self:InitBgMusicTab()

  self.m_TabBar = self.transform:Find("Anchor/TabBar"):GetComponent(typeof(UITabBar))
  self.m_TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function(go, index)
    self:OnTabChange(go, index)
  end)
  self.m_TabBar:ChangeTab(0, false)
end

function CLuaMusicBoxRecordWnd:DoMake()
  if self.m_CurrentTabIndex == 0 then
    CInputBoxMgr.ShowInputBox(g_MessageMgr:FormatMessage("MusicBox_XHZS_Name"), DelegateFactory.Action_string(function (val)
      local ret = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(val, nil, LocalString.GetString("星痕之砂名字"), true)
      if (not ret.msg) or ret.shouldBeIgnore then return end
      Gac2Gas.DiskPlayerRecordDiskItem(true, CLuaMusicBoxMgr.RecordItemPlace, CLuaMusicBoxMgr.RecordItemPos, CLuaMusicBoxMgr.RecordItemId, self.m_VoiceUrl, ret.msg)
      CUIManager.CloseUI(CLuaUIResources.MusicBoxRecordWnd)
    end), 12, true, nil, LocalString.GetString("点击此处输入（不超过五个字）"))
  else
    local name = StarBiWuShow_SceneMusic.GetData(self.m_BgMusicId).Name
    g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("MusicBox_InsertBg_Confim", name), function()
      Gac2Gas.DiskPlayerRecordDiskItem(false, CLuaMusicBoxMgr.RecordItemPlace, CLuaMusicBoxMgr.RecordItemPos, CLuaMusicBoxMgr.RecordItemId, tostring(self.m_BgMusicId), name)
      CUIManager.CloseUI(CLuaUIResources.MusicBoxRecordWnd)
    end, nil, nil, nil, false)
  end
end

function CLuaMusicBoxRecordWnd:InitRecordTab()
    UIEventListener.Get(self.m_RecordStatusObjTable["Begin"].transform:Find("Sprite").gameObject).onClick = LuaUtils.VoidDelegate(function()
      self:StartRecord()
    end)
    self.m_CountdownLabel = self.m_RecordStatusObjTable["Countdown"]:GetComponent(typeof(UILabel))
    UIEventListener.Get(self.m_RecordStatusObjTable["Recording"].transform:Find("Sprite").gameObject).onClick = LuaUtils.VoidDelegate(function()
      self:StopRecord()
    end)
    self.m_RecordTimeLabel = self.m_RecordStatusObjTable["Recording"].transform:Find("Label"):GetComponent(typeof(UILabel))
    UIEventListener.Get(self.m_RecordStatusObjTable["End"].transform:Find("Redo").gameObject).onClick = LuaUtils.VoidDelegate(function()
      self:StartRecord()
      self.m_MakeButton.Enabled = false
    end)
    UIEventListener.Get(self.m_RecordStatusObjTable["End"].transform:Find("Play").gameObject).onClick = LuaUtils.VoidDelegate(function()
      self:PlayVoice()
    end)
end

function CLuaMusicBoxRecordWnd:PlayVoice()
  if not self.m_VoiceUrl then
    g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("录制失败，请重新录制"))
    return
  end
  --self:ShowRecordStatusObj("Listening")
  CLuaMusicBoxMgr:PlayVoice(self.m_VoiceUrl, CLuaMusicBoxMgr:GetFullUrl(self.m_VoiceUrl), true)
end

function CLuaMusicBoxRecordWnd:StopRecord()
  CUIManager.StopRecord(false)
end

function CLuaMusicBoxRecordWnd:ShowRecordStatusObj(status)
  for k, v in pairs(self.m_RecordStatusObjTable) do
    v:SetActive(k == status)
  end
end

function CLuaMusicBoxRecordWnd:StartRecord()
  self:ShowRecordStatusObj("Countdown")
  self.m_Count = 3
  self.m_CountdownLabel.text = self.m_Count
  if self.m_CountdownTick then
    UnRegisterTick(self.m_CountdownTick)
    self.m_CountdownTick = nil
  end
  self.m_CountdownTick = RegisterTick(function()
    self:CountDown()
  end, 1000)
end

function CLuaMusicBoxRecordWnd:CountDown()
  self.m_Count = self.m_Count - 1
  self.m_CountdownLabel.text = self.m_Count
  if self.m_Count <= 0 then
    self:ShowRecordStatusObj("Recording")
    self:DoRecord()
    if self.m_CountdownTick then
      UnRegisterTick(self.m_CountdownTick)
      self.m_CountdownTick = nil
    end
  end
end

function CLuaMusicBoxRecordWnd:DoRecord()
  local voiceInfo = VoiceRecordInfo(EnumVoicePlatform.WebBrowser, EnumRecordContext.Undefined, EChatPanel.Undefined, 0 , 0 ,0)
  CUIManager.StartRecord(voiceInfo, self.m_MaxRecordTime)
  self.m_CurRecordTime = 0
  self:UpdateRecordTime()
  if self.m_RecordingTick then
    UnRegisterTick(self.m_RecordingTick)
    self.m_RecordingTick = nil
  end
  self.m_RecordingTick = RegisterTickWithDuration(function()
    self.m_CurRecordTime = self.m_CurRecordTime + 1
    self:UpdateRecordTime()
    end, 1000, 30000)
end

function CLuaMusicBoxRecordWnd:UpdateRecordTime()
  self.m_RecordTimeLabel.text = SafeStringFormat3("00:%02d/00:"..self.m_MaxRecordTime, self.m_CurRecordTime)
  if self.m_CurRecordTime >= self.m_MaxRecordTime then
    UnRegisterTick(self.m_RecordingTick)
    self.m_RecordingTick = nil
    self:ShowRecordStatusObj("End")
  end
end

function CLuaMusicBoxRecordWnd:InitBgMusicTab()
  self.m_BgListViewRootObj = self.m_BgRootObj.transform:Find("AdvView").gameObject
  self.m_BgListeningObj = self.m_BgRootObj.transform:Find("Listening").gameObject

  local templateObj = self.m_BgListViewRootObj.transform:Find("Pool/Template").gameObject
  local tableObj = self.m_BgListViewRootObj.transform:Find("ScrollView/Table").gameObject
  templateObj:SetActive(false)

  self.m_CheckBoxTable = {}
  StarBiWuShow_SceneMusic.Foreach(function(key, value)
    local go = NGUITools.AddChild(tableObj, templateObj)
    go:SetActive(true)
    go.transform:Find("QnCheckbox/Label"):GetComponent(typeof(UILabel)).text = value.Name
    local checkbox = go.transform:Find("QnCheckbox"):GetComponent(typeof(QnCheckBox))
    checkbox:SetSelected(false, true)
    checkbox.OnValueChanged = DelegateFactory.Action_bool(function(val)
      if val then
        self:OnCheckboxSelected(checkbox)
      else
        checkbox:SetSelected(true, true)
      end
    end)
    self.m_CheckBoxTable[key] = checkbox
    UIEventListener.Get(go.transform:Find("ListenBtn").gameObject).onClick = LuaUtils.VoidDelegate(function()
      self:TryListenBgMusic(key)
    end)
  end)
  tableObj:GetComponent(typeof(UIGrid)):Reposition()

  self.m_BgListViewRootObj:SetActive(true)
  self.m_BgListeningObj:SetActive(false)

  UIEventListener.Get(self.m_BgListeningObj.transform:Find("Sprite").gameObject).onClick = LuaUtils.VoidDelegate(function()
    self:StopListenBgMusic()
  end)
end

function CLuaMusicBoxRecordWnd:TryListenBgMusic(id)
  self.m_BgListViewRootObj:SetActive(false)
  self.m_BgListeningObj:SetActive(true)

  local designdata = StarBiWuShow_SceneMusic.GetData(id)
  self.m_BgListeningObj.transform:Find("Label"):GetComponent(typeof(UILabel)).text = designdata.Name

  CLuaMusicBoxMgr:PlayBgMusic(id, true)
end

function CLuaMusicBoxRecordWnd:StopListenBgMusic()
  self.m_BgListViewRootObj:SetActive(true)
  self.m_BgListeningObj:SetActive(false)

  CLuaMusicBoxMgr:StopBgMusic()
end

function CLuaMusicBoxRecordWnd:OnCheckboxSelected(checkbox)
  for key, val in pairs(self.m_CheckBoxTable) do
    if val == checkbox then
      self.m_BgMusicId = key
      self.m_MakeButton.Enabled = true
    else
      val:SetSelected(false, true)
    end
  end
end

function CLuaMusicBoxRecordWnd:OnTabChange(go, index)
  self.m_CurrentTabIndex = index
  self.m_RecordRootObj:SetActive(index == 0)
  self.m_BgRootObj:SetActive(index == 1)

  if index == 0 then self.m_MakeButton.Enabled = self.m_HaveVoice
  else self.m_MakeButton.Enabled = self.m_BgMusicId ~= nil end
end

function CLuaMusicBoxRecordWnd:OnDestroy()
  CLuaMusicBoxMgr:StopBgMusic()

  if self.m_RecordingTick then
    UnRegisterTick(self.m_RecordingTick)
    self.m_RecordingTick = nil
  end
  if self.m_CountdownTick then
    UnRegisterTick(self.m_CountdownTick)
    self.m_CountdownTick = nil
  end
end

function CLuaMusicBoxRecordWnd:OnEnable( ... )
	g_ScriptEvent:AddListener("UploadWebVoiceViaPersonalSpaceFinished", self, "UploadWebVoiceViaPersonalSpaceFinished")
  g_ScriptEvent:AddListener("OnVoiceRecordFinish", self, "OnVoiceRecordFinish")
end
function CLuaMusicBoxRecordWnd:OnDisable( ... )
	g_ScriptEvent:RemoveListener("UploadWebVoiceViaPersonalSpaceFinished", self, "UploadWebVoiceViaPersonalSpaceFinished")
  g_ScriptEvent:RemoveListener("OnVoiceRecordFinish", self, "OnVoiceRecordFinish")
end

function CLuaMusicBoxRecordWnd:OnVoiceRecordFinish(argv)
  local bSuccess = argv[0]
  self:ShowRecordStatusObj(bSuccess and "End" or "Begin")
  if self.m_RecordingTick then
    UnRegisterTick(self.m_RecordingTick)
    self.m_RecordingTick = nil
  end
end
function CLuaMusicBoxRecordWnd:UploadWebVoiceViaPersonalSpaceFinished(argv)
  local url = argv[0]
  self.m_VoiceUrl = CLuaMusicBoxMgr:GetPartialUrl(url)
  self.m_HaveVoice = self.m_VoiceUrl ~= nil
  self.m_MakeButton.Enabled = self.m_HaveVoice
end
