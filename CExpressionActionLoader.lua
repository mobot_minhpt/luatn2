-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CExpressionActionLoader = import "L10.UI.CExpressionActionLoader"
local CExpressionActionMgr = import "L10.Game.CExpressionActionMgr"
local CExpressionAppearanceMgr = import "L10.Game.CExpressionAppearanceMgr"
local Expression_Define = import "L10.Game.Expression_Define"
local Expression_Show = import "L10.Game.Expression_Show"
local Time = import "UnityEngine.Time"
CExpressionActionLoader.m_Init_CS2LuaHook = function (this) 
    this.deltaTime = Time.deltaTime / 2
    this.identifier = System.String.Format("__{0}__", this:GetInstanceID())
    this.rotation = 180
    local expression = Expression_Show.GetData(CExpressionActionMgr.ExpressionActionID)
    if expression ~= nil then
        this.actionName.text = expression.ExpName
    end
    this:LoadModel()
end
CExpressionActionLoader.m_Load_CS2LuaHook = function (this, renderObj) 
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer ~= nil then
        local show = Expression_Show.GetData(CExpressionActionMgr.ExpressionActionID)
        local define = Expression_Define.GetData(CExpressionActionMgr.ExpressionActionID)

        local selectedAppearance = CExpressionAppearanceMgr.Inst:GetMainPlayerSelectedAppearance(show.Group)
        local appearancedActionId = CExpressionAppearanceMgr.Inst:GetSelectedAppearanceAction(show.ID, selectedAppearance)

        if show ~= nil and define ~= nil then
            local fakeAppearance = mainPlayer.AppearanceProp
            CClientMainPlayer.LoadResource(renderObj, fakeAppearance, true, 1, 0, false, mainPlayer.AppearanceProp.ResourceId, false, appearancedActionId, false, nil, false)
        end
    end
end
