LuaShuangshiyi2023Mgr = {}

local CCenterCountdownWnd = import "L10.UI.CCenterCountdownWnd"
local CSkillButtonBoardWnd = import "L10.UI.CSkillButtonBoardWnd"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local CScene = import "L10.Game.CScene"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local DelegateFactory = import "DelegateFactory"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CPos = import "L10.Engine.CPos"
local CForcesMgr = import "L10.Game.CForcesMgr"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local EnumEventType = import "EnumEventType"
local EnumQualityType = import "L10.Game.EnumQualityType"

LuaShuangshiyi2023Mgr.KeyTbl = {ePrice = 1, eCurLotteryTimes = 3, eCurReplacementTimes = 4, eCurMustGetTimes = 5, eRewardStatus = 6,
                                eReplacementItems = 4, eMustGetItem = 5}

LuaShuangshiyi2023Mgr.DiscountQualityColor = {
    NGUIText.ParseColor24("949494",0),
    NGUIText.ParseColor24("A89674",0),
    NGUIText.ParseColor24("77A0B3",0),
    NGUIText.ParseColor24("AE707C",0),
    NGUIText.ParseColor24("895F8D",0),
    NGUIText.ParseColor24("AD642A",0),
}

LuaShuangshiyi2023Mgr.MQXHEnterPhase2Check = false
LuaShuangshiyi2023Mgr.MQHXEnterPhaseCrazyCheck = false
LuaShuangshiyi2023Mgr.IsInMQLDPlay = function()
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        if gamePlayId == Double11_MQLD.GetData().GamePlayId then
            return true
        end
    end
    return false
end

LuaShuangshiyi2023Mgr.IsInBaoYuDuoYuPlay = function()
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        if gamePlayId == Double11_BYDLY.GetData().GamePlayId or gamePlayId == Double11_BYDLY.GetData().HeroGamePlayId then
            return true
        end
    end
    return false
end

LuaShuangshiyi2023Mgr.m_ShowLotteryRemainTimes = true
LuaShuangshiyi2023Mgr.AddUnLoadSceneFallBackListener = false

function LuaShuangshiyi2023Mgr:OnPlayerLogin()
    if self.m_delayShowRewardTick then
        UnRegisterTick(self.m_delayShowRewardTick)
        self.m_delayShowRewardTick = nil
    end

    LuaShuangshiyi2023Mgr.m_BaoYuDuoYuPlayInfo = nil
    LuaShuangshiyi2023Mgr.m_BaoYuDuoYuTreasureInfo = {}

    LuaShuangshiyi2023Mgr.m_PvePickBuffLevel = 0

    LuaShuangshiyi2023Mgr.m_ShowLotteryRemainTimes = true
    
    if not LuaShuangshiyi2023Mgr.AddUnLoadSceneFallBackListener then
        g_ScriptEvent:AddListener("StartUnloadScene", self, "OnStartUnloadScene")
        LuaShuangshiyi2023Mgr.AddUnLoadSceneFallBackListener = true
    end


    g_ScriptEvent:RemoveListener("OnAddBuffFX", self, "OnAddBuffFX")
    g_ScriptEvent:AddListener("OnAddBuffFX", self, "OnAddBuffFX")

    self.m_RemovePVEBuffFunc = DelegateFactory.Action_uint_uint(function(engineId, buffId)
        self:OnRemoveBuffFx(engineId, buffId)
    end)
    EventManager.RemoveListenerInternal(EnumEventType.OnRemoveDouble112023PVEBuff, LuaShuangshiyi2023Mgr.m_RemovePVEBuffFunc)
    EventManager.AddListenerInternal(EnumEventType.OnRemoveDouble112023PVEBuff, LuaShuangshiyi2023Mgr.m_RemovePVEBuffFunc)
end

function LuaShuangshiyi2023Mgr:OnStartUnloadScene(args)
    local sceneName = args[0] or ""
    if sceneName == "rouge46" then
        LuaShuangshiyi2023Mgr.m_BaoYuDuoYuTreasureInfo.formatTeamTreasureInfo = nil
    end
end

function LuaShuangshiyi2023Mgr:OnMainPlayerCreated(playId)
    if playId == Double11_BYDLY.GetData().GamePlayId or playId == Double11_BYDLY.GetData().HeroGamePlayId then
        LuaCommonGamePlayTaskViewMgr:AppendGameplayInfo(playId,
                false, nil,
                true,
                true, function() return "" end,
                true, function() return "" end,
                true, nil, nil,
                false, nil, function() return self:ShowCommonImageRuleWnd() end)
    end
end

function LuaShuangshiyi2023Mgr:OnMainPlayerDestroyed()
end

function LuaShuangshiyi2023Mgr:OnAddBuffFX(args)
    if not LuaShuangshiyi2023Mgr.IsInBaoYuDuoYuPlay() then
        return
    end
    local treasureBuffId = Double11_BYDLY.GetData().TreasureBuffCls
    local treasureBuffList = {}
    for i = 1, Double11_BYDLY.GetData().TreasureMaxNum do
        local realBuffId = treasureBuffId * 100 + i
        treasureBuffList[realBuffId] = true
    end
    
    local enginedId = args[0]
    local buffId = args[1]
    local endTime = args[2]
    local clientObj = CClientObjectMgr.Inst:GetObject(enginedId)
    
    if clientObj and (treasureBuffList[buffId] and treasureBuffList[buffId] or false) then
        if CClientMainPlayer.Inst and enginedId == CClientMainPlayer.Inst.EngineId then
            LuaShuangshiyi2023Mgr.m_PvePickBuffLevel = buffId % 10
        end
        
        local format = "UI/Texture/FestivalActivity/Festival_Double11/Shuangshiyi2023/Material/baoyuduoyu2023toprightwnd_baoshi.mat"
        local args = {}
        args[0] = enginedId
        args[1] = {}
        args[1].texturePath = SafeStringFormat3(format, buffId%10)
        args[1].textureSize = {120, 120}
        args[2] = {}
        args[2].content = buffId % 10
        args[2].LabelSize = 34
        args[2].FontType = "defaultFont"
        args[2].Color = NGUIText.ParseColor24("00594a",0)
        args[3] = false
        args[4] = true
        args[5] = "DoubleOne2023PVE"
        g_ScriptEvent:BroadcastInLua("UpdatePlayerHeadInfoCountDown",args)
    end
end

function LuaShuangshiyi2023Mgr:OnRemoveBuffFx(enginedId, buffId)
    if not LuaShuangshiyi2023Mgr.IsInBaoYuDuoYuPlay() then
        return
    end
    
    local clientObj = CClientObjectMgr.Inst:GetObject(enginedId)
    local treasureBuffId = Double11_BYDLY.GetData().TreasureBuffCls
    local treasureBuffList = {}
    for i = 1, Double11_BYDLY.GetData().TreasureMaxNum do
        local realBuffId = treasureBuffId * 100 + i
        treasureBuffList[realBuffId] = true
    end

    if clientObj and (treasureBuffList[buffId] and treasureBuffList[buffId] or false) then
        if CClientMainPlayer.Inst and enginedId == CClientMainPlayer.Inst.EngineId then
            LuaShuangshiyi2023Mgr.m_PvePickBuffLevel = 0
        end
        local args = {}
        args[0] = enginedId
        args[4] = false
        args[5] = "DoubleOne2023PVE"
        g_ScriptEvent:BroadcastInLua("UpdatePlayerHeadInfoCountDown",args) 
    end
end

function LuaShuangshiyi2023Mgr:IsDiscountFreeOneDraw()
    if not CClientMainPlayer.Inst then
        return false
    end
    local oneDrawTimes = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eDouble11CouponsSingleDrawTimes) or 0
    local freeDrawTimes = 0
    if CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11ExteriorDiscountCouponsData) then
        local playDataU = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11ExteriorDiscountCouponsData)
        local list = g_MessagePack.unpack(playDataU.Data.Data)
        freeDrawTimes = list[3] and list[3] or 0
    end
    
    return oneDrawTimes == 0 or freeDrawTimes > 0
end

function LuaShuangshiyi2023Mgr:IsLotteryDrawTimesRemain()
    if not CClientMainPlayer.Inst then
        return false
    end
    
    local rechargeData = {}
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer and CommonDefs.DictContains_LuaCall(mainPlayer.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11RechargeData) then
        local playDataU = CommonDefs.DictGetValue_LuaCall(mainPlayer.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11RechargeData)
        local list = g_MessagePack.unpack(playDataU.Data.Data)
        rechargeData = list
    end

    local remainDraw = (rechargeData[LuaShuangshiyi2023Mgr.KeyTbl.eCurLotteryTimes] and rechargeData[LuaShuangshiyi2023Mgr.KeyTbl.eCurLotteryTimes] or 0)
    return remainDraw > 0 and (LuaShuangshiyi2023Mgr.m_ShowLotteryRemainTimes)
end

function LuaShuangshiyi2023Mgr:IsRechargeRewardToGet()
    if not CClientMainPlayer.Inst then
        return false
    end

    local rechargeData = {}
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer and CommonDefs.DictContains_LuaCall(mainPlayer.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11RechargeData) then
        local playDataU = CommonDefs.DictGetValue_LuaCall(mainPlayer.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11RechargeData)
        local list = g_MessagePack.unpack(playDataU.Data.Data)
        rechargeData = list
    end
    local rewardGotTbl = rechargeData[LuaShuangshiyi2023Mgr.KeyTbl.eRewardStatus] and rechargeData[LuaShuangshiyi2023Mgr.KeyTbl.eRewardStatus] or {}
    local chargeNumber = rechargeData[LuaShuangshiyi2023Mgr.KeyTbl.ePrice] and rechargeData[LuaShuangshiyi2023Mgr.KeyTbl.ePrice] or 0

    local rawData = Double11_RechargeLottery.GetData().PriceRewardItems
    local chargeList = g_LuaUtil:StrSplit(rawData, ";")
    for i = 1, #chargeList do
        if chargeList[i] ~= "" then
            local itemList = g_LuaUtil:StrSplit(chargeList[i], ",")
            local chargeRequireNumber = tonumber(itemList[1])
            local isGot = rewardGotTbl[i] and rewardGotTbl[i] or false
            if chargeNumber >= chargeRequireNumber and (not isGot) then
                return true
            end
        end
    end
    return false
end

function LuaShuangshiyi2023Mgr:Double11MQLD_SyncPlayState(playerId, playState, startTime, endTime, RankTbl)
    --rank数据
    local srcRankTbl = LuaShuangshiyi2023Mgr.MQLDPlayInfo and LuaShuangshiyi2023Mgr.MQLDPlayInfo.RankTbl or {}
    LuaShuangshiyi2023Mgr.MQLDPlayInfo = {
        playState = playState,
        startTime = startTime,
        endTime = endTime,
        RankTbl = srcRankTbl,
        gameFinishTime = 0,
    }
    
    if #RankTbl == 0 then
        --读取缓存的老数据
        RankTbl = LuaShuangshiyi2023Mgr.MQLDPlayInfo.RankTbl
    else
        --缓存新数据
        LuaShuangshiyi2023Mgr.MQLDPlayInfo.RankTbl = RankTbl
    end
    g_ScriptEvent:BroadcastInLua("SyncMengQuanHengXingRankInfo", playState, RankTbl, playerId)
    --playState流转
    if playState == EnumDouble11MQLDPlayState.ePrepare then
        LuaShuangshiyi2023Mgr.MQXHEnterPhase2Check = true
        self:TryAddMQLDWorldFx()
    elseif playState == EnumDouble11MQLDPlayState.eFirstStage then
        if LuaShuangshiyi2023Mgr.MQXHEnterPhase2Check then
            if not CUIManager.IsLoaded(CLuaUIResources.MengQuan2023WarningWnd) then
                LuaMengQuanHengXingWarningWnd.playState = playState
                CUIManager.ShowUI(CLuaUIResources.MengQuan2023WarningWnd)
            else
                g_ScriptEvent:BroadcastInLua("UpdateMengQuanHengXingWarning", playState)
            end
            LuaShuangshiyi2023Mgr.MQXHEnterPhase2Check = false
        end
        LuaShuangshiyi2023Mgr.MQHXEnterPhaseCrazyCheck = true

        self:TryAddMQLDWorldFx()
        local extraTime = Double11_MQLD.GetData().StageTimes
        LuaShuangshiyi2023Mgr.MQLDPlayInfo.gameFinishTime = CServerTimeMgr.Inst.timeStamp+endTime+extraTime[1]-startTime
        g_ScriptEvent:BroadcastInLua("SyncMengQuanHengXingCountDown", CServerTimeMgr.Inst.timeStamp+endTime+extraTime[1]-startTime)
    elseif playState == EnumDouble11MQLDPlayState.eSecondStage then
        if LuaShuangshiyi2023Mgr.MQHXEnterPhaseCrazyCheck then
            if not CUIManager.IsLoaded(CLuaUIResources.MengQuan2023WarningWnd) then
                LuaMengQuanHengXingWarningWnd.playState = playState
                CUIManager.ShowUI(CLuaUIResources.MengQuan2023WarningWnd)
            else
                g_ScriptEvent:BroadcastInLua("UpdateMengQuanHengXingWarning", playState)
            end
            LuaShuangshiyi2023Mgr.MQHXEnterPhaseCrazyCheck = false
        end
        self:TryAddMQLDWorldFx()
        LuaShuangshiyi2023Mgr.MQLDPlayInfo.gameFinishTime = CServerTimeMgr.Inst.timeStamp+endTime-startTime
        g_ScriptEvent:BroadcastInLua("SyncMengQuanHengXingCountDown", CServerTimeMgr.Inst.timeStamp+endTime-startTime)
    end
end

function LuaShuangshiyi2023Mgr:TryAddMQLDWorldFx()
    if CommonDefs.IS_VN_CLIENT then
        return
    end
    
    if LuaShuangshiyi2023Mgr.m_MQLDWorldFxList then
        for i = 1, #LuaShuangshiyi2023Mgr.m_MQLDWorldFxList do
            LuaShuangshiyi2023Mgr.m_MQLDWorldFxList[i]:Destroy()
        end
    end
    LuaShuangshiyi2023Mgr.m_MQLDWorldFxList = {}
    local fxId = Double11_MQLD.GetData().ScorePlaceFxId
    local fxPosRawData = Double11_MQLD.GetData().ScorePlacePostionList
    local fxRawDataList = g_LuaUtil:StrSplit(fxPosRawData, ";")
    local rotationList = {90, 0, 180}
    for i = 1, #fxRawDataList do
        if fxRawDataList[i] ~= "" then
            local subData = g_LuaUtil:StrSplit(fxRawDataList[i], ",")
            local fx = CEffectMgr.Inst:AddWorldPositionFX(fxId, Vector3(tonumber(subData[1]), tonumber(subData[2]), tonumber(subData[3])), rotationList[i], 0, 1, -1, EnumWarnFXType.None, nil, 0, 0, nil)
            table.insert(LuaShuangshiyi2023Mgr.m_MQLDWorldFxList, fx)
        end
    end
end

function LuaShuangshiyi2023Mgr:Double11MQLDBroadFlyAction(playerId, x, y, z)
    g_ScriptEvent:BroadcastInLua("Double11MQLDBroadFlyAction", playerId, x, z, y)
end

function LuaShuangshiyi2023Mgr:GetMQLDHeadTexturePath(playerId)
    local bFind, teamId = CommonDefs.DictTryGet_LuaCall(CForcesMgr.Inst.m_PlayForceInfo, playerId)
    if not bFind then
        return
    end

    local leadForce = 0
    local rankData = LuaShuangshiyi2023Mgr.MQLDPlayInfo.RankTbl and LuaShuangshiyi2023Mgr.MQLDPlayInfo.RankTbl or {}
    for i = 1, #rankData do
        if i == 1 then
            leadForce = rankData[i][1]
            break
        end
    end

    if teamId == leadForce then
        return "UI/Texture/Transparent/Material/grabchristmasgiftrankwnd_huangguang_01.mat"
    else
        return
    end
end

function LuaShuangshiyi2023Mgr:SyncDouble11MQLDResult(isGetDailyAward, isGetEliteAward, isNewRecord, rankCache, fightData)
    --delay个两秒弹结算数据
    if not CUIManager.IsLoaded(CLuaUIResources.MengQuan2023WarningWnd) then
        LuaMengQuanHengXingWarningWnd.playState = EnumDouble11MQLDPlayState.eEnd
        CUIManager.ShowUI(CLuaUIResources.MengQuan2023WarningWnd)
    else
        g_ScriptEvent:BroadcastInLua("UpdateMengQuanHengXingWarning", EnumDouble11MQLDPlayState.eEnd)
    end
    --fightData格式跟预期的不一致, 在客户端这转成预期格式
    local formatFightData = {}
    local steps = 8
    for i = 1, #fightData, steps do
        local curIndex = i
        local curData = {}
        local finishIndex = i + steps
        for j = curIndex, finishIndex do
            table.insert(curData, fightData[j])
        end
        table.insert(formatFightData, curData)
    end

    self.m_delayShowRewardTick = RegisterTickOnce(function ()
        --弹结算
        LuaMengQuan2023ResultWnd.m_newRecord = isNewRecord
        LuaMengQuan2023ResultWnd.m_isGetDailyAward = isGetDailyAward
        LuaMengQuan2023ResultWnd.m_isGetEliteAward = isGetEliteAward
        LuaMengQuan2023ResultWnd.m_rankCache = rankCache
        LuaMengQuan2023ResultWnd.m_fightData = formatFightData
        CUIManager.ShowUI(CLuaUIResources.MengQuan2023ResultWnd)
    end, 2000)
end

LuaShuangshiyi2023Mgr.m_BaoYuDuoYuTreasureInfo = {}
function LuaShuangshiyi2023Mgr:SyncDouble11BYDLYFirstStageInfo(totalTreasure, teamTreasureInfo)
    LuaShuangshiyi2023Mgr.m_BaoYuDuoYuTreasureInfo.totalTreasure = totalTreasure
    if LuaShuangshiyi2023Mgr.m_BaoYuDuoYuTreasureInfo.formatTeamTreasureInfo == nil then
        LuaShuangshiyi2023Mgr.m_BaoYuDuoYuTreasureInfo.formatTeamTreasureInfo = {}
    end
    for i = 1, #teamTreasureInfo, 3 do
        local playerInfo = {}
        playerInfo.playerId = teamTreasureInfo[i]
        playerInfo.playerName = teamTreasureInfo[i+1]
        playerInfo.treasure = teamTreasureInfo[i+2]

        local searchTbl = LuaShuangshiyi2023Mgr.m_BaoYuDuoYuTreasureInfo.formatTeamTreasureInfo
        local isFind = false
        for playerIndex = 1, #searchTbl do
            if searchTbl[playerIndex].playerId == playerInfo.playerId then
                searchTbl[playerIndex].treasure = playerInfo.treasure
                isFind = true 
                break
            end
        end
        if not isFind then 
            table.insert(searchTbl, playerInfo)
        end
    end
    g_ScriptEvent:BroadcastInLua("SyncDouble11BYDLYFirstStageInfo")

end 

function LuaShuangshiyi2023Mgr:SyncDouble11BYDLYPlayInfo(isHero, stage, curtime, timelimit, isChangeStage)
    if isChangeStage then
        LuaBaoYuDuoYu2023ChangePhaseWnd.m_stage = stage
        CUIManager.ShowUI(CLuaUIResources.BaoYuDuoYu2023ChangePhaseWnd)

        LuaShuangshiyi2023Mgr.m_BaoYuDuoYuTreasureInfo.formatTeamTreasureInfo = nil
    end
    if stage <= 2 then
        LuaShuangshiyi2023Mgr.m_heroReliveTimes = 0
    end
    LuaShuangshiyi2023Mgr.m_BaoYuDuoYuPlayInfo = {
        isHero = isHero,
        stage = stage,
        curtime = curtime,
        timelimit = timelimit,
    }
    g_ScriptEvent:BroadcastInLua("SyncDouble11BYDLYPlayInfo")
end

function LuaShuangshiyi2023Mgr:SyncDouble11BYDLYHeroReliveTimes(heroReliveTimes)
    LuaShuangshiyi2023Mgr.m_heroReliveTimes = heroReliveTimes
    g_ScriptEvent:BroadcastInLua("SyncDouble11BYDLYHeroReliveTimes", heroReliveTimes)
end

function LuaShuangshiyi2023Mgr:SyncDouble11BYDLYResult(isHero, isWin, isGetDailyAward, isGetHeroAward, isGetEliteAward, usedTime, extraInfo_U)
    LuaBaoYuDuoYu2023ResultWnd.m_IsHero = isHero
    LuaBaoYuDuoYu2023ResultWnd.m_IsWin = isWin
    LuaBaoYuDuoYu2023ResultWnd.m_IsGetDailyAward = isGetDailyAward
    LuaBaoYuDuoYu2023ResultWnd.m_IsGetHeroAward = isGetHeroAward
    LuaBaoYuDuoYu2023ResultWnd.m_IsGetEliteAward = isGetEliteAward
    LuaBaoYuDuoYu2023ResultWnd.m_UsedTime = usedTime
    LuaBaoYuDuoYu2023ResultWnd.m_ExtraInfo = g_MessagePack.unpack(extraInfo_U)
    CUIManager.ShowUI(CLuaUIResources.BaoYuDuoYu2023ResultWnd)
end

function LuaShuangshiyi2023Mgr:Double11ShowRechargeLotteryRewards(isBagFull, rewards_U)
    local rawItemList = g_MessagePack.unpack(rewards_U)
    local itemList = {}
    local drawResult = 0
    local isSingleDraw = false

    for k, v in pairs(rawItemList) do
        local count = 0
        
        if v[0] then
            count = count + v[0]
        end

        if v[1] then
            count = count + v[1]
        end

        table.insert(itemList, {ItemID=k, 
                                Count=count})
        drawResult = drawResult + count
    end
    isSingleDraw = drawResult == 1
    
    if (not CUIManager.IsLoaded(CLuaUIResources.Shuangshiyi2023LotteryWnd)) and not (CUIManager.IsLoading(CLuaUIResources.Shuangshiyi2023LotteryWnd))  then
        LuaCommonGetRewardWnd.m_materialName = "gongxihuode"
        LuaCommonGetRewardWnd.m_Reward1List = itemList
        LuaCommonGetRewardWnd.m_Reward2Label = ""
        LuaCommonGetRewardWnd.m_Reward2List = {  }
        LuaCommonGetRewardWnd.m_hint = ""
        LuaCommonGetRewardWnd.m_button = {
            {spriteName="blue", buttonLabel = LocalString.GetString("确定"), clickCB = function() CUIManager.CloseUI(CLuaUIResources.CommonGetRewardWnd) end},
        }
        CUIManager.ShowUI(CLuaUIResources.CommonGetRewardWnd)
    else
        local delayTime = 300
        if isSingleDraw then
            delayTime = 300
        else    
            delayTime = 600
        end
        
        self.showRewardTick = RegisterTickOnce(function ()
            LuaCommonGetRewardWnd.m_materialName = "gongxihuode"
            LuaCommonGetRewardWnd.m_Reward1List = itemList
            LuaCommonGetRewardWnd.m_Reward2Label = ""
            LuaCommonGetRewardWnd.m_Reward2List = {  }
            LuaCommonGetRewardWnd.m_hint = ""
            LuaCommonGetRewardWnd.m_button = {
                {spriteName="blue", buttonLabel = LocalString.GetString("确定"), clickCB = function() CUIManager.CloseUI(CLuaUIResources.CommonGetRewardWnd) end},
            }
            CUIManager.ShowUI(CLuaUIResources.CommonGetRewardWnd)
            g_ScriptEvent:BroadcastInLua("OnDelayShowCommonGetRewardWnd")
        end,  delayTime)
    end
end

function LuaShuangshiyi2023Mgr:Show2023Double11ExteriorDiscountCoupons_Result(coupons_U)
    local itemList = g_MessagePack.unpack(coupons_U)
    g_ScriptEvent:BroadcastInLua("Show2023Double11DiscountCouponsResult", itemList)
end

function LuaShuangshiyi2023Mgr:CanCastSkillInMengQuanLuanDou(skillId)
    local playState = LuaShuangshiyi2023Mgr.MQLDPlayInfo and LuaShuangshiyi2023Mgr.MQLDPlayInfo.playState or nil
    if playState then
        if playState < EnumDouble11MQLDPlayState.eFirstStage or playState >= EnumDouble11MQLDPlayState.eEnd then return false end
        
        --获得buffId
        self.tempSkillList = {}
        local dogBuffId = Double11_MQLD.GetData().DogBuffId
        local dogTempSkillIdRawData = Double11_MQLD.GetData().DogTempSkillIds
        local rawTempSkillList = g_LuaUtil:StrSplit(dogTempSkillIdRawData, ";")
        for i = 1, #rawTempSkillList do
            if rawTempSkillList[i] ~= "" then
                self.tempSkillList[ tonumber(rawTempSkillList[i]) ] = true
            end
        end
        
        local exist = false
        CommonDefs.DictIterate(CClientMainPlayer.Inst.BuffProp.Buffs, DelegateFactory.Action_object_object(function (___key, ___value)
            if ___key == dogBuffId then
                exist = true
            end
        end))
        if exist then
            return self.tempSkillList[skillId] ~= nil
        else
            return true
        end
    else
        return false
    end
end

function LuaShuangshiyi2023Mgr:GetItemQuality(itemId)
    local itemData = Item_Item.GetData(itemId)
    local nameColor = itemData.NameColor
    if nameColor == "COLOR_WHITE" then
        return EnumToInt(EnumQualityType.White)
    elseif nameColor == "COLOR_GOLD" then
        return EnumToInt(EnumQualityType.Yellow)
    elseif nameColor == "COLOR_ORANGE" then
        return EnumToInt(EnumQualityType.Orange)
    elseif nameColor == "COLOR_GREEN" then
        return EnumToInt(EnumQualityType.White)
    elseif nameColor == "COLOR_BLUE" then
        return EnumToInt(EnumQualityType.Blue)
    elseif nameColor == "COLOR_RED" then
        return EnumToInt(EnumQualityType.Red)
    elseif nameColor == "COLOR_PURPLE" then
        return EnumToInt(EnumQualityType.Purple)
    end
    return EnumToInt(EnumQualityType.White)
end
