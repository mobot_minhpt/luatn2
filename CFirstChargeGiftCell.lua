-- Auto Generated!!
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CFirstChargeGiftCell = import "L10.UI.CFirstChargeGiftCell"
local CItem = import "L10.Game.CItem"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CUICommonDef = import "L10.UI.CUICommonDef"
local DelegateFactory = import "DelegateFactory"
local UIEventListener = import "UIEventListener"
CFirstChargeGiftCell.m_Init_CS2LuaHook = function (this, id, amount) 
    this.iconTexture.material = nil
    if this.amountLabel ~= nil then
        this.amountLabel.text = ""
    end
    if this.nameLabel ~= nil then
        this.nameLabel.text = ""
    end
    this.templateId = id
    local item = CItemMgr.Inst:GetItemTemplate(this.templateId)
    if item == nil then
        return
    end
    this.iconTexture:LoadMaterial(item.Icon)
    this.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(item, nil, true)
    if this.amountLabel ~= nil then
        this.amountLabel.text = amount > 1 and tostring(amount) or ""
    end
    if this.nameLabel ~= nil then
        this.nameLabel.text = item.Name
        this.nameLabel.color = CItem.GetColor(id)
    end

    UIEventListener.Get(this.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.gameObject).onClick, DelegateFactory.VoidDelegate(function (go) 
        CItemInfoMgr.ShowLinkItemTemplateInfo(id, false, nil, AlignType.Default, 0, 0, 0, 0)
    end), true)
end
