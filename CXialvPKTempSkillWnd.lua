-- Auto Generated!!
local CommonDefs = import "L10.Game.CommonDefs"
local CXialvPKTempSkillWnd = import "L10.UI.CXialvPKTempSkillWnd"
local GameObject = import "UnityEngine.GameObject"
local UInt32 = import "System.UInt32"
CXialvPKTempSkillWnd.m_OnEnable_CS2LuaHook = function (this) 
    if this.m_LeftView ~= nil then
        this.m_LeftView.OnClassSelected = CommonDefs.CombineListner_Action_uint(this.m_LeftView.OnClassSelected, MakeDelegateFromCSFunction(this.OnSelectedClass, MakeGenericClass(Action1, UInt32), this), true)
    end
    if this.m_MiddleView ~= nil then
        this.m_MiddleView.OnDragComplete = CommonDefs.CombineListner_Action_uint_GameObject(this.m_MiddleView.OnDragComplete, MakeDelegateFromCSFunction(this.OnDragComplete, MakeGenericClass(Action2, UInt32, GameObject), this), true)
    end
end
CXialvPKTempSkillWnd.m_OnDisable_CS2LuaHook = function (this) 
    if this.m_LeftView ~= nil then
        this.m_LeftView.OnClassSelected = CommonDefs.CombineListner_Action_uint(this.m_LeftView.OnClassSelected, MakeDelegateFromCSFunction(this.OnSelectedClass, MakeGenericClass(Action1, UInt32), this), false)
    end
    if this.m_MiddleView ~= nil then
        this.m_MiddleView.OnDragComplete = CommonDefs.CombineListner_Action_uint_GameObject(this.m_MiddleView.OnDragComplete, MakeDelegateFromCSFunction(this.OnDragComplete, MakeGenericClass(Action2, UInt32, GameObject), this), false)
    end
end
CXialvPKTempSkillWnd.m_OnSelectedClass_CS2LuaHook = function (this, classId) 

    if this.m_MiddleView ~= nil then
        this.m_MiddleView:Init(classId)
    end
end
CXialvPKTempSkillWnd.m_OnDragComplete_CS2LuaHook = function (this, skillId, destGo) 

    if this.m_RightView ~= nil then
        this.m_RightView:ToLearnSkill(skillId, destGo, 0)
    end
end
