local UILabel = import "UILabel"
local CUIFx = import "L10.UI.CUIFx"
local CCommonUIFxWnd = import "L10.UI.CCommonUIFxWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local EnumHongBaoRainType = import "L10.UI.EnumHongBaoRainType"
local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local ETickType = import "L10.Engine.ETickType"
local CTickMgr = import "L10.Engine.CTickMgr"

LuaFightingSpiritGambleResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaFightingSpiritGambleResultWnd, "MoneyCount", "MoneyCount", UILabel)
RegistChildComponent(LuaFightingSpiritGambleResultWnd, "MultipleCount", "MultipleCount", UILabel)
RegistChildComponent(LuaFightingSpiritGambleResultWnd, "ComformBtn", "ComformBtn", GameObject)
RegistChildComponent(LuaFightingSpiritGambleResultWnd, "Title", "Title", UILabel)
RegistChildComponent(LuaFightingSpiritGambleResultWnd, "ItemFx", "ItemFx", CUIFx)

--@endregion RegistChildComponent end
RegistClassMember(LuaFightingSpiritGambleResultWnd, "m_Tick")

function LuaFightingSpiritGambleResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ComformBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnComformBtnClick()
	end)


    --@endregion EventBind end
end

function LuaFightingSpiritGambleResultWnd:Init()
	self.MultipleCount.text = ""
	self.MoneyCount.text = CLuaFightingSpiritMgr.m_GambleRewardSilver
	self.Title.text = CLuaFightingSpiritMgr.m_Des
	CCommonUIFxWnd.Instance:PlayHongBaoFx(EnumHongBaoRainType.YinLiang)

	self:PlayCountShow(CLuaFightingSpiritMgr.m_GambleRewardSilver)
end

-- 刷新数字
function LuaFightingSpiritGambleResultWnd:OnReshow(silver, des, multi)
	self.MultipleCount.text = ""
	self.MoneyCount.text = silver
	self.Title.text = des
	self:PlayCountShow(silver)
end

-- 刷新数字
function LuaFightingSpiritGambleResultWnd:PlayCountShow(silver)
	local result = silver
	local interval = math.ceil(result / 500 + 1)
	local temp = 1

	if self.m_Tick then
		invoke(self.m_Tick)
	end

	self.m_Tick = CTickMgr.Register(DelegateFactory.Action(function () 
		if temp >= result then
			temp = result
			if self.m_Tick ~= nil then
				invoke(self.m_Tick)
			end
		end
		self.MoneyCount.text = tostring(temp)
		temp = temp + interval
	end), 5, ETickType.Loop)
end

function LuaFightingSpiritGambleResultWnd:OnEnable()
	g_ScriptEvent:AddListener("OpenGambleRewardWnd", self, "OnReshow")
end

function LuaFightingSpiritGambleResultWnd:OnDisable()
	if self.m_Tick then
		invoke(self.m_Tick)
	end
	g_ScriptEvent:RemoveListener("OpenGambleRewardWnd", self, "OnReshow")
end

--@region UIEvent

function LuaFightingSpiritGambleResultWnd:OnComformBtnClick()
	CUIManager.CloseUI(CLuaUIResources.FightingSpiritGambleResultWnd)
end


--@endregion UIEvent

