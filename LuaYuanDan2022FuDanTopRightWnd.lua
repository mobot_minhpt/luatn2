local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CTopAndRightTipWnd=import "L10.UI.CTopAndRightTipWnd"

LuaYuanDan2022FuDanTopRightWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaYuanDan2022FuDanTopRightWnd, "RightBtn", "RightBtn", GameObject)
RegistChildComponent(LuaYuanDan2022FuDanTopRightWnd, "NumLabel1", "NumLabel1", UILabel)
RegistChildComponent(LuaYuanDan2022FuDanTopRightWnd, "NumLabel2", "NumLabel2", UILabel)

--@endregion RegistChildComponent end

function LuaYuanDan2022FuDanTopRightWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.RightBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRightBtnClick()
	end)


    --@endregion EventBind end
end

function LuaYuanDan2022FuDanTopRightWnd:Init()
	self.NumLabel1.text = 0
	self.NumLabel2.text = 0
	CUIManager.ShowUI(CLuaUIResources.YuanDan2022FuDanReadMeWnd)
end


function LuaYuanDan2022FuDanTopRightWnd:OnEnable()
	g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
	g_ScriptEvent:AddListener("OnSyncYuanDan2022PlayInfo", self, "OnSyncYuanDan2022PlayInfo")
end


function LuaYuanDan2022FuDanTopRightWnd:OnDisable()
	g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
	g_ScriptEvent:RemoveListener("OnSyncYuanDan2022PlayInfo", self, "OnSyncYuanDan2022PlayInfo")
end

function LuaYuanDan2022FuDanTopRightWnd:OnHideTopAndRightTipWnd()
	LuaUtils.SetLocalRotation(self.RightBtn.transform,0,0,180)
end

function LuaYuanDan2022FuDanTopRightWnd:OnSyncYuanDan2022PlayInfo()
	self.NumLabel1.text = LuaYuanDan2022Mgr.m_EggsDataInfo[0]
	self.NumLabel2.text = LuaYuanDan2022Mgr.m_EggsDataInfo[1]
end

--@region UIEvent

function LuaYuanDan2022FuDanTopRightWnd:OnRightBtnClick()
	LuaUtils.SetLocalRotation(self.RightBtn.transform,0,0,0)
	CTopAndRightTipWnd.showPackage = false
	CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

--@endregion UIEvent

