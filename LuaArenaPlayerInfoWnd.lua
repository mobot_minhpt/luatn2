local Profession            = import "L10.Game.Profession"
local QnModelPreviewer      = import "L10.UI.QnModelPreviewer"
local CPropertyAppearance   = import "L10.Game.CPropertyAppearance"
local EnumClass             = import "L10.Game.EnumClass"
local EnumGender            = import "L10.Game.EnumGender"
local CServerTimeMgr        = import "L10.Game.CServerTimeMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel            = import "L10.Game.EChatPanel"
local AlignType             = import "CPlayerInfoMgr+AlignType"
local CPlayerInfoMgr        = import "CPlayerInfoMgr"
local EnumChineseDigits     = import "L10.Game.EnumChineseDigits"

LuaArenaPlayerInfoWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaArenaPlayerInfoWnd, "name")
RegistClassMember(LuaArenaPlayerInfoWnd, "class")
RegistClassMember(LuaArenaPlayerInfoWnd, "level")
RegistClassMember(LuaArenaPlayerInfoWnd, "modelPreview")

RegistClassMember(LuaArenaPlayerInfoWnd, "currentRank")
RegistClassMember(LuaArenaPlayerInfoWnd, "maxRank")
RegistClassMember(LuaArenaPlayerInfoWnd, "totalWinNum")
RegistClassMember(LuaArenaPlayerInfoWnd, "totalFavorNum")
RegistClassMember(LuaArenaPlayerInfoWnd, "killNum")
RegistClassMember(LuaArenaPlayerInfoWnd, "assistNum")
RegistClassMember(LuaArenaPlayerInfoWnd, "repeatedWinNum")
RegistClassMember(LuaArenaPlayerInfoWnd, "lastWinTime")
RegistClassMember(LuaArenaPlayerInfoWnd, "recentPartnerTable")
RegistClassMember(LuaArenaPlayerInfoWnd, "recentEnemyTable")
RegistClassMember(LuaArenaPlayerInfoWnd, "danLarge")
RegistClassMember(LuaArenaPlayerInfoWnd, "danSmall")

function LuaArenaPlayerInfoWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    local left = self.transform:Find("Anchor/Left")
    self.name = left:Find("Name"):GetComponent(typeof(UILabel))
    self.class = left:Find("Class"):GetComponent(typeof(UISprite))
    self.level = left:Find("Level"):GetComponent(typeof(UILabel))
    self.modelPreview = left:Find("ModelPreview"):GetComponent(typeof(QnModelPreviewer))

    local right = self.transform:Find("Anchor/Right")
    local rightTable = right:Find("Table")
    self.currentRank = rightTable:Find("CurrentRank/Label"):GetComponent(typeof(UILabel))
    self.maxRank = rightTable:Find("MaxRank/Label"):GetComponent(typeof(UILabel))
    self.totalWinNum = rightTable:Find("TotalWinNum/Label"):GetComponent(typeof(UILabel))
    self.totalFavorNum = rightTable:Find("TotalFavorNum/Label"):GetComponent(typeof(UILabel))
    self.killNum = rightTable:Find("KillNum/Label"):GetComponent(typeof(UILabel))
    self.assistNum = rightTable:Find("AssistNum/Label"):GetComponent(typeof(UILabel))
    self.repeatedWinNum = rightTable:Find("RepeatedWinNum/Label"):GetComponent(typeof(UILabel))
    self.lastWinTime = rightTable:Find("LastWinTime/Label"):GetComponent(typeof(UILabel))
    self.recentPartnerTable = rightTable:Find("RecentPartner/Table")
    self.recentEnemyTable = rightTable:Find("RecentEnemy/Table")
    self.danLarge = right:Find("Dan/Large"):GetComponent(typeof(CUITexture))
    self.danSmall = right:Find("Dan/Small"):GetComponent(typeof(CUITexture))
end

function LuaArenaPlayerInfoWnd:Init()
    local info = LuaArenaMgr.playerInfo
    self.name.text = info.name
    self.class.spriteName = Profession.GetIconByNumber(info.class)
    self.level.text = SafeStringFormat3("[%s]lv.%d[-]", info.isFeiSheng and Constants.ColorOfFeiSheng or "FFFFFF", info.level)
    local appearance = CPropertyAppearance()
    appearance:LoadFromString(info.appearanceProp, CommonDefs.ConvertIntToEnum(typeof(EnumClass), info.class), CommonDefs.ConvertIntToEnum(typeof(EnumGender), info.gender))
    appearance.YingLingState = EnumYingLingState.eDefault
    self.modelPreview:PreviewFakeAppear(appearance)

    local largeDan, smallDan = LuaArenaMgr:GetLargeAndSmallDan(info.arenaDetails.rankScore)
    local currentDanData = Arena_DanInfo.GetData(largeDan)
    self.danLarge:LoadMaterial(currentDanData.Icon)
    self.danSmall:LoadMaterial(LuaArenaMgr.smallDanMatPaths[smallDan])
    self.currentRank.text = SafeStringFormat3(LocalString.GetString("[%s]%s·%s段[-]"), currentDanData.DanColor, currentDanData.DanName, EnumChineseDigits.GetDigit(smallDan))
    local maxLargeDan, maxSmallDan = LuaArenaMgr:GetLargeAndSmallDan(info.arenaDetails.maxScore)
    local maxDanData = Arena_DanInfo.GetData(maxLargeDan)
    self.maxRank.text = SafeStringFormat3(LocalString.GetString("[%s]%s·%s段[-]"), maxDanData.DanColor, maxDanData.DanName, EnumChineseDigits.GetDigit(maxSmallDan))

    self.totalWinNum.text = info.arenaDetails.winCount
    self.totalFavorNum.text = info.arenaDetails.favCount
    self.killNum.text = info.arenaDetails.kill
    self.assistNum.text = info.arenaDetails.assist
    self.repeatedWinNum.text = info.arenaDetails.winCombo
    if info.arenaDetails.lastWinTime > 0 then
        local dateTime = CServerTimeMgr.ConvertTimeStampToZone8Time(info.arenaDetails.lastWinTime)
        self.lastWinTime.text = SafeStringFormat3(LocalString.GetString("%d年%d月%d日"), dateTime.Year, dateTime.Month, dateTime.Day)
    else
        self.lastWinTime.text = LocalString.GetString("暂无")
    end

    self:InitMate()
end

function LuaArenaPlayerInfoWnd:InitMate()
    local mateData = LuaArenaMgr.playerInfo.mateData
    local partnerTbl = {}
    local enemyTbl = {}
    table.sort(mateData, function (a, b)
        return a.playTime > b.playTime
    end)

    for id, data in pairs(mateData) do
        data.playerId = id
        if data.type == 1 then
            table.insert(partnerTbl, data)
        elseif data.type == 2 then
            table.insert(enemyTbl, data)
        end
    end

    self:InitMateSub(self.recentEnemyTable, enemyTbl)
    self:InitMateSub(self.recentPartnerTable, partnerTbl)
end

function LuaArenaPlayerInfoWnd:InitMateSub(parent, tbl)
    for i = 1, 5 do
        local enemyChild = parent.transform:GetChild(i - 1)
        local portrait = enemyChild:Find("Portrait"):GetComponent(typeof(CUITexture))
        local level = enemyChild:Find("Level"):GetComponent(typeof(UILabel))
        if tbl[i] then
            portrait.alpha = 1
            portrait:LoadPortrait(CUICommonDef.GetPortraitName(tbl[i].class, tbl[i].gender, -1), true)
            level.text = CUICommonDef.GetColoredLevelString(tbl[i].isFeiSheng, tbl[i].level, Color.white)
            UIEventListener.Get(portrait.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
                CPlayerInfoMgr.ShowPlayerPopupMenu(tbl[i].playerId, EnumPlayerInfoContext.Arena, EChatPanel.Undefined, "", nil, Vector3.zero, AlignType.Default)
            end)
        else
            portrait.alpha = 0.5
            portrait:LoadMaterial("")
            level.text = ""
            UIEventListener.Get(portrait.gameObject).onClick = nil
        end
    end
end

function LuaArenaPlayerInfoWnd:OnDestroy()
    LuaArenaMgr.playerInfo = {}
end

--@region UIEvent
--@endregion UIEvent
