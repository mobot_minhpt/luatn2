local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local PathFindDefs = import "L10.Engine.PathFinding.PathFindDefs"
local EMoveType = import "L10.Engine.EMoveType"
local EMoveToResult = import "L10.Engine.EMoveToResult"
local Setting = import "L10.Engine.Setting"
local Vector3 = import "UnityEngine.Vector3"
local CPos = import "L10.Engine.CPos"

CLuaTargetAutoMoveMgr = class()
CLuaTargetAutoMoveMgr.m_BaseVerticalSpeed = 5.0
CLuaTargetAutoMoveMgr.m_BaseHorizontalSpeed = 2.0
CLuaTargetAutoMoveMgr.m_Threshold = 0.0001
CLuaTargetAutoMoveMgr.m_TargetPos = nil
CLuaTargetAutoMoveMgr.m_TargetDist = 2.0
CLuaTargetAutoMoveMgr.m_MoveDir = nil

CLuaTargetAutoMoveMgr.m_DragDir = nil
CLuaTargetAutoMoveMgr.m_DragDot = 0
CLuaTargetAutoMoveMgr.m_Threshold = 0.0001
CLuaTargetAutoMoveMgr.m_ReverseDrag = false

function CLuaTargetAutoMoveMgr:AddListener()
    g_ScriptEvent:RemoveListener("MainPlayerMoveEnded", self, "OnMainPlayerMoveEnded")
	g_ScriptEvent:RemoveListener("PlayerDragJoyStick", self, "OnPlayerDragJoyStick")
    g_ScriptEvent:RemoveListener("PlayerDragJoyStickComplete", self, "OnPlayerDragJoyStickComplete")
	g_ScriptEvent:AddListener("MainPlayerMoveEnded", self, "OnMainPlayerMoveEnded")
    g_ScriptEvent:AddListener("PlayerDragJoyStick", self, "OnPlayerDragJoyStick")
    g_ScriptEvent:AddListener("PlayerDragJoyStickComplete", self, "OnPlayerDragJoyStickComplete")
end
CLuaTargetAutoMoveMgr:AddListener()

function CLuaTargetAutoMoveMgr:IsTargetArrived()
	if not (CClientMainPlayer.Inst and CClientMainPlayer.Inst.IsInTargetAutoMove) then
		return true
	end

	local targetVec = CommonDefs.op_Subtraction_Vector3_Vector3(self.m_TargetPos, CClientMainPlayer.Inst.WorldPos)
	targetVec.y = 0
	return targetVec.sqrMagnitude < self.m_TargetDist * self.m_TargetDist
end

function CLuaTargetAutoMoveMgr:DoMove(bMoveEnd)
	if not (CClientMainPlayer.Inst and CClientMainPlayer.Inst.IsInTargetAutoMove) then return end

	if self:IsTargetArrived() then
		if self.m_RetryMoveTick ~= nil then
			UnRegisterTick(self.m_RetryMoveTick)
			self.m_RetryMoveTick = nil
		end
		self.m_RetryMoveTick = RegisterTickOnce(function()
			self:DoMove()
		end, 500)
		return
	end

	self:UpdateMoveDir()

	local speed = CClientMainPlayer.Inst.MaxSpeed
	local pixelSpeed = CClientMainPlayer.Inst.MaxPixelSpeed

	local deltaVec = CommonDefs.op_Multiply_Single_Vector3(2 * speed, self.m_MoveDir)

	local posX = CClientMainPlayer.Inst.WorldPos.x + deltaVec.x
	local posY = CClientMainPlayer.Inst.WorldPos.z + deltaVec.z
	local targetVec = CommonDefs.op_Subtraction_Vector3_Vector3(self.m_TargetPos, CClientMainPlayer.Inst.WorldPos)
	targetVec.y = 0
	if targetVec.magnitude < deltaVec.magnitude and self.m_DragDot == 0 then
		posX = self.m_TargetPos.x
		posY = self.m_TargetPos.z
	end
	posX = math.max(math.min(posX, CRenderScene.Inst.MapWidth - 1), 1)
	posY = math.max(math.min(posY, CRenderScene.Inst.MapHeight - 1), 1)

	local target = CPos(posX * Setting.eGridSpan, posY * Setting.eGridSpan)
	local ret = CClientMainPlayer.Inst:MoveTo(target, pixelSpeed, 0,0,0, PathFindDefs.EFindPathType.eFPT_HypoLine, CClientMainPlayer.Inst.BarrierType, false, EMoveType.Normal, PathFindDefs.MAX_REGION_LIMIT)

	if ret ~= EMoveToResult.Success then
		if ret ~= EMoveToResult.NotAllowed and ret ~= EMoveToResult.SuperPosition and self.m_DragDot ~= 0 then
			ret = CClientMainPlayer.Inst:MoveTo(target, pixelSpeed, 0,0,0, PathFindDefs.EFindPathType.eFPT_HypoLineHelper, CClientMainPlayer.Inst.BarrierType, false, EMoveType.Normal, PathFindDefs.MAX_REGION_LIMIT)
		end
		if ret ~= EMoveToResult.Success then
			if self.m_RetryMoveTick ~= nil then
				UnRegisterTick(self.m_RetryMoveTick)
				self.m_RetryMoveTick = nil
			end
			self.m_RetryMoveTick = RegisterTickOnce(function()
				self:DoMove()
			end, 100)
		end
	end
end

function CLuaTargetAutoMoveMgr:UpdateMoveDir()
	if not (CClientMainPlayer.Inst and CClientMainPlayer.Inst.IsInTargetAutoMove) then return end

	local dragDir = self.m_DragDir
	local targetVec = CommonDefs.op_Subtraction_Vector3_Vector3(self.m_TargetPos, CClientMainPlayer.Inst.WorldPos)
	targetVec.y = 0
	local targetDir = targetVec.normalized

	local horizontal = Vector3(targetDir.z, 0, -targetDir.x)
	local horizontalDot = Vector3.Dot(dragDir, horizontal)
	local dragDot = 0
	local flag = self.m_ReverseDrag and -1 or 1
	if horizontalDot < -0.01 or horizontalDot > 0.01 then
		dragDot = flag * horizontalDot
	end
	horizontal = CommonDefs.op_Multiply_Single_Vector3(dragDot * self.m_BaseHorizontalSpeed, horizontal)
	local vertical = CommonDefs.op_Multiply_Single_Vector3(self.m_BaseVerticalSpeed, targetDir)
	self.m_MoveDir = CommonDefs.op_Addition_Vector3_Vector3(horizontal, vertical).normalized
	self.m_DragDot = dragDot
end

function CLuaTargetAutoMoveMgr:ChangeMoveDir(dir, bForce)
	if not (CClientMainPlayer.Inst and CClientMainPlayer.Inst.IsInTargetAutoMove) then return end

	local dragDir = self.m_DragDir
	if bForce or math.abs(dragDir.x - dir.x) > self.m_Threshold or math.abs(dragDir.z - dir.z) > self.m_Threshold then
		self.m_DragDir = Vector3(dir.x, 0, dir.z)
		self:DoMove()
	end
end

function CLuaTargetAutoMoveMgr:OnMainPlayerMoveEnded()
	self:DoMove(true)
end

function CLuaTargetAutoMoveMgr:OnPlayerDragJoyStick(args)
	self:ChangeMoveDir(args[0])
end

function CLuaTargetAutoMoveMgr:OnPlayerDragJoyStickComplete()
	self:ChangeMoveDir(Vector3(0, 0, 0), true)
end

function Gas2Gac.StartTargetAutoMove(vSpeed, hSpeed, targetX, targetY, targetDist)
	if not CClientMainPlayer.Inst then return end

	CLuaTargetAutoMoveMgr.m_BaseVerticalSpeed = vSpeed
	CLuaTargetAutoMoveMgr.m_BaseHorizontalSpeed = hSpeed

	local targetPos = Vector3(targetX, 0, targetY)
	CLuaTargetAutoMoveMgr.m_TargetPos = targetPos
	CLuaTargetAutoMoveMgr.m_TargetDist = targetDist

	local targetVec = CommonDefs.op_Subtraction_Vector3_Vector3(targetPos, CClientMainPlayer.Inst.WorldPos)
	targetVec.y = 0
	CLuaTargetAutoMoveMgr.m_MoveDir = targetVec.normalized
	CLuaTargetAutoMoveMgr.m_DragDir = Vector3(0, 0, 0)
	CLuaTargetAutoMoveMgr.m_DragDot = 0

	CClientMainPlayer.Inst.IsInTargetAutoMove = true
	CLuaTargetAutoMoveMgr:DoMove()
end

function Gas2Gac.EndTargetAutoMove()
	if not CClientMainPlayer.Inst then return end

	CClientMainPlayer.Inst.IsInTargetAutoMove = false
    CClientMainPlayer.Inst:StopMove()
end

function Gas2Gac.SetTargetAutoMoveReverseDrag(bReverse)
	if not CClientMainPlayer.Inst then return end

	CLuaTargetAutoMoveMgr.m_ReverseDrag = bReverse
	CLuaTargetAutoMoveMgr:DoMove()
end
