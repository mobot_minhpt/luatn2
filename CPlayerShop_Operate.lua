-- Auto Generated!!
local CPlayerShop_Operate = import "L10.UI.CPlayerShop_Operate"
local CPlayerShopData = import "L10.UI.CPlayerShopData"
local CPlayerShopItemData = import "L10.UI.CPlayerShopItemData"
local RequestReason = import "L10.UI.CPlayerShopData+RequestReason"
local UInt32 = import "System.UInt32"
CPlayerShop_Operate.m_Init_CS2LuaHook = function (this) 
    if CPlayerShopData.Main.HasPlayerShop then
        CPlayerShopData.Main:RequestPlayerShopInfo(MakeDelegateFromCSFunction(this.OnRecievePlayerShopData, MakeGenericClass(Action2, CPlayerShopData, MakeGenericClass(Dictionary, UInt32, CPlayerShopItemData)), this), RequestReason.GetData)
    else
        this:ShowPlayerShopOpenView()
    end
end
CPlayerShop_Operate.m_ShowPlayerShopView_CS2LuaHook = function (this, data, itemdata) 
    if CPlayerShopData.Main ~= nil and this ~= nil then
        if CPlayerShopData.Main.BankruptTime ~= 0 then
            this.m_PlayerShopBankrupt.gameObject:SetActive(true)
            this.m_PlayerShopOpen.gameObject:SetActive(false)
            this.m_PlayerShopSell.gameObject:SetActive(false)
            this.m_PlayerShopBankrupt:Init()
        else
            this.m_PlayerShopBankrupt.gameObject:SetActive(false)
            this.m_PlayerShopOpen.gameObject:SetActive(false)
            this.m_PlayerShopSell.gameObject:SetActive(true)
            this.m_PlayerShopSell:Init(data, itemdata)
        end
    end
end
CPlayerShop_Operate.m_OnRequestAddPlayerShopCounterPosSuccess_CS2LuaHook = function (this, shopId, place, silver) 
    if shopId == CPlayerShopData.Main.PlayerShopId then
        g_MessageMgr:ShowMessage("PLAYERSHOP_ADD_COUNTER_SUCCEED", silver)
        CPlayerShopData.Main:RequestPlayerShopInfo(MakeDelegateFromCSFunction(this.OnRecievePlayerShopData, MakeGenericClass(Action2, CPlayerShopData, MakeGenericClass(Dictionary, UInt32, CPlayerShopItemData)), this), CPlayerShopData.RequestReason.OpenNewCounter)
    end
end
