CLuaZhuShaBiStarWnd = class()
-- CLuaZhuShaBiStarWnd.m_ItemId = nil
CLuaZhuShaBiStarWnd.m_WashedStar = 0
CLuaZhuShaBiStarWnd.m_Star = 0


function CLuaZhuShaBiStarWnd:Init()
    -- local node = self.transform:Find("Anchor/StarNode")
    -- node.gameObject:SetActive(true)
    local starItem = self.transform:Find("Anchor/Star").gameObject
    starItem:SetActive(false)
    local grid1 = self.transform:Find("Anchor/StarGrid").gameObject
    local grid2 = self.transform:Find("Anchor/StarGrid2").gameObject

    local star = CLuaZhuShaBiStarWnd.m_Star
    local washedStar = CLuaZhuShaBiStarWnd.m_WashedStar
    local c2 = ChuanjiabaoModel_Setting.GetData().Chuanjiabao_Total_Score
    for i=1,c2 do
        local g = NGUITools.AddChild(grid1,starItem)
        g:SetActive(true)

        local sprite = g:GetComponent(typeof(UISprite))
        if i<=star then
            sprite.spriteName = "playerAchievementView_star_1"
        else
            sprite.spriteName = "playerAchievementView_star_2"
        end
        ---------------------------------------------------------
        local g = NGUITools.AddChild(grid2,starItem)
        g:SetActive(true)
        local sprite = g:GetComponent(typeof(UISprite))
        if i<=washedStar then
            sprite.spriteName = "playerAchievementView_star_1"
        else
            sprite.spriteName = "playerAchievementView_star_2"
        end
    end
    grid1:GetComponent(typeof(UIGrid)):Reposition()
    grid2:GetComponent(typeof(UIGrid)):Reposition()
end

function CLuaZhuShaBiStarWnd:OnDestroy()
    CLuaZhuShaBiStarWnd.m_WashedStar = 0
    CLuaZhuShaBiStarWnd.m_Star = 0
end