-- Auto Generated!!
local CGuanNingMgr = import "L10.Game.CGuanNingMgr"
local BoolDelegate = import "UIEventListener+BoolDelegate"
local Boolean = import "System.Boolean"
local CClientFurnitureMgr = import "L10.Game.CClientFurnitureMgr"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientNpc = import "L10.Game.CClientNpc"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CForcesMgr = import "L10.Game.CForcesMgr"
local CGuildLeagueMgr = import "L10.Game.CGuildLeagueMgr"
local CHouseCompetitionMgr = import "L10.Game.CHouseCompetitionMgr"
local CItem = import "L10.Game.CItem"
local CJXYSBossInfo = import "L10.Game.CJXYSBossInfo"
local CJXYSMgr = import "L10.Game.CJXYSMgr"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CMiniMap = import "L10.UI.CMiniMap"
local CMiniMapGuildLeagueBossResource = import "L10.UI.CMiniMapPopWnd+CMiniMapGuildLeagueBossResource"
local CMiniMapGuildLeagueBossTemplate = import "L10.UI.CMiniMapGuildLeagueBossTemplate"
local CMiniMapGuildLeagueBuildResource = import "L10.UI.CMiniMapPopWnd+CMiniMapGuildLeagueBuildResource"
local CMiniMapGuildLeagueBuildTemplate = import "L10.UI.CMiniMapGuildLeagueBuildTemplate"
local CMiniMapGuildLeagueFlagResource = import "L10.UI.CMiniMapPopWnd+CMiniMapGuildLeagueFlagResource"
local CMiniMapGuildLeagueFlagTemplate = import "L10.UI.CMiniMapGuildLeagueFlagTemplate"
local CMiniMapGuildQueenHugResource = import "L10.UI.CMiniMapPopWnd+CMiniMapGuildQueenHugResource"
local CMiniMapMarkTemplate = import "L10.UI.CMiniMapMarkTemplate"
local CMiniMapNpcItem = import "L10.UI.CMiniMapNpcItem"
local CMiniMapPopWnd = import "L10.UI.CMiniMapPopWnd"
local CMiniMapPopWndHouseJianzhuMarkResource = import "L10.UI.CMiniMapPopWnd+CMiniMapPopWndHouseJianzhuMarkResource"
local CMiniMapPopWndNpcMarkResource = import "L10.UI.CMiniMapPopWnd+CMiniMapPopWndNpcMarkResource"
local CMiniMapPopWndTeleportResource = import "L10.UI.CMiniMapPopWnd+CMiniMapPopWndTeleportResource"
local CMiniMapRoutingWnd = import "L10.UI.CMiniMapRoutingWnd"
local CMiniMapWeekendFightItemResource = import "L10.UI.CMiniMapPopWnd+CMiniMapWeekendFightItemResource"
local CMiniMapWeekendFightMonsterItemResource = import "L10.UI.CMiniMapPopWnd+CMiniMapWeekendFightMonsterItemResource"
local CMiniMapCommonItemNodeResource = import "L10.UI.CMiniMapPopWnd+CMiniMapCommonItemNodeResource"
local CommonDefs = import "L10.Game.CommonDefs"
local CPos = import "L10.Engine.CPos"
local CQingQiuMgr = import "L10.UI.CQingQiuMgr"
local CQingQiuMonster = import "L10.UI.CQingQiuMonster"
local CRandomMapMgr = import "L10.Engine.Scene.CRandomMapMgr"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local CResourceMgr = import "L10.Engine.CResourceMgr"
local CScene = import "L10.Game.CScene"
local CSnowBallMgr = import "L10.Game.CSnowBallMgr"
local CStatusMgr = import "L10.Game.CStatusMgr"
local CTooltip = import "L10.UI.CTooltip"
local CTreasureMapMgr = import "L10.Game.CTreasureMapMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIFx = import "L10.UI.CUIFx"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local CUIManager = import "L10.UI.CUIManager"
local CUIMaterialPaths = import "L10.UI.CUIMaterialPaths"
local CUIResources = import "L10.UI.CUIResources"
local CUITexture = import "L10.UI.CUITexture"
local CWeekendFightMgr = import "L10.Game.CWeekendFightMgr"
local CWorldEventMgr = import "L10.Game.CWorldEventMgr"
local CWorldMapInfo = import "L10.UI.CWorldMapInfo"
local DelegateFactory = import "DelegateFactory"
local Ease = import "DG.Tweening.Ease"
local EMinimapCategory = import "L10.UI.EMinimapCategory"
local EMinimapMarkType = import "L10.UI.EMinimapMarkType"
local EnumEvent = import "L10.Game.EnumEvent"
local EnumEventType = import "EnumEventType"
local EnumGuildLeagueFlagType = import "L10.Game.EnumGuildLeagueFlagType"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local GuildLeagueBossInfo = import "L10.UI.CMiniMapPopWnd+GuildLeagueBossInfo"
local GuildLeagueBuildInfo = import "L10.Game.GuildLeagueBuildInfo"
local GuildQueenHugInfo = import "L10.UI.CMiniMapPopWnd+GuildQueenHugInfo"
local Input = import "UnityEngine.Input"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local LuaTweenUtils = import "LuaTweenUtils"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Monster_Monster = import "L10.Game.Monster_Monster"
local NGUIMath = import "NGUIMath"
local NGUITools = import "NGUITools"
local NPC_NPC = import "L10.Game.NPC_NPC"
local NPC_ShowHide = import "L10.Game.NPC_ShowHide"
local NpcInfo = import "L10.UI.CMiniMapPopWnd+NpcInfo"
local Object = import "System.Object"
local PathMode = import "DG.Tweening.PathMode"
local PathType = import "DG.Tweening.PathType"
local PlayerInfo = import "L10.UI.CMiniMapPopWnd+PlayerInfo"
local PublicMap_PublicMap = import "L10.Game.PublicMap_PublicMap"
local QnButton = import "L10.UI.QnButton"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local Quaternion = import "UnityEngine.Quaternion"
local ShiJieShiJian_CronMessage2019 = import "L10.Game.ShiJieShiJian_CronMessage2019"
local String = import "System.String"
local Time = import "UnityEngine.Time"
local UIDrawCall = import "UIDrawCall"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UInt32 = import "System.UInt32"
local UInt64 = import "System.UInt64"
local UIPanel = import "UIPanel"
local UISprite = import "UISprite"
local Utility = import "L10.Engine.Utility"
local Vector2 = import "UnityEngine.Vector2"
local Vector3 = import "UnityEngine.Vector3"
local Vector4 = import "UnityEngine.Vector4"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local WaBao_Setting = import "L10.Game.WaBao_Setting"
local WndType = import "L10.UI.WndType"
local XianZongShan_Crystal = import "L10.Game.XianZongShan_Crystal"
local Zhuangshiwu_Zhuangshiwu = import "L10.Game.Zhuangshiwu_Zhuangshiwu"
local LuaUtils = import "LuaUtils"
local CBabyNPCPosInfo = import "L10.Game.CBabyNPCPosInfo"
local CTeamMgr = import "L10.Game.CTeamMgr"
local UICamera = import "UICamera"
local NGUIText = import "NGUIText"
local PlayerSettings = import "L10.Game.PlayerSettings"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CPUBGMgr = import "L10.Game.CPUBGMgr"
local CTrackMgr = import "L10.Game.CTrackMgr"
local BoxCollider = import "UnityEngine.BoxCollider"
local CTaskMgr = import "L10.Game.CTaskMgr"
local UISlider = import "UISlider"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"

CMiniMapPopWnd.m_OnDestroy_CS2LuaHook = function (this)
    if this.m_QueryPlayerTick ~= nil then
        invoke(this.m_QueryPlayerTick)
    end
    this.m_QueryPlayerTick = nil

    if this.m_QueryBabyNPCTick ~= nil then
        invoke(this.m_QueryBabyNPCTick)
    end
    this.m_QueryBabyNPCTick = nil
    this.m_MapTexture.mainTexture = nil
    CMiniMapPopWndNpcMarkResource.Inst.Resource = nil
    CMiniMapPopWndTeleportResource.Inst.Resource = nil
    CMiniMapGuildLeagueBossResource.Inst.Resource = nil
    CMiniMapGuildLeagueBuildResource.Inst.Resource = nil
    CMiniMapGuildLeagueFlagResource.Inst.Resource = nil
    CMiniMapGuildQueenHugResource.Inst.Resource = nil
    CMiniMapWeekendFightItemResource.Inst.Resource = nil
    CMiniMapWeekendFightMonsterItemResource.Inst.Resource = nil
    CMiniMapCommonItemNodeResource.Inst.Resource = nil
    CMiniMapPopWnd.m_ZhuangshiwuTemplateId = 0
    CLuaMiniMapPopWnd:OnDestroy(this)
end
CMiniMapPopWnd.m_Init_CS2LuaHook = function (this)
    this.m_FxPlayer:LoadFx("fx/ui/prefab/UI_zhiyin.prefab")

    --#region UI相关
    if CRandomMapMgr.Inst:IsInRandomMap() then
        if CScene.MainScene.IsHideMinimap then
            this.m_MapTexture.mainTexture = CMiniMap.Instance.mHiddenMiniMapTexture
            this.m_NoMinimapTipGO:SetActive(true)
        else
            this.m_MapTexture.mainTexture = CMiniMap.Instance.mapTexture.mainTexture
            this.m_NoMinimapTipGO:SetActive(false)
        end
        this.m_HelpCamera = nil
    else
        this.m_NoMinimapTipGO:SetActive(false)

        this.m_HelpCamera = CMiniMap.Instance.helpCamera
        if CMiniMap.Instance.m_MinimapData ~= nil then
            this.m_MapTexture.mainTexture = CMiniMap.Instance.m_MinimapData.m_MapTexture
        elseif not CommonDefs.IS_PUB_RELEASE then
            --内部版本为未设置mapData的场景,用随机地图的方式生成小地图,处理小地图逻辑以供测试
            this.m_MapTexture.mainTexture = CMiniMap.Instance.mapTexture.mainTexture
            this.m_NoMinimapTipGO:SetActive(false)
            --用m_HelpeCamera是否为nil来判断是否用随机地图逻辑处理
            this.m_HelpCamera = nil
        end
    end


    this.m_NpcTableView.m_DataSource = this
    this.m_BackToGuildButton.OnClick = MakeDelegateFromCSFunction(this.OnClickBackToGuildButton, MakeGenericClass(Action1, QnButton), this)
    this.m_BackToHouseButton.OnClick = MakeDelegateFromCSFunction(this.OnClickBackToHouseButton, MakeGenericClass(Action1, QnButton), this)
    this.m_NpcButton.OnClick = MakeDelegateFromCSFunction(this.OnNpcListButtonClick, MakeGenericClass(Action1, QnButton), this)
    this.m_SwitchButton.OnClick = MakeDelegateFromCSFunction(this.OnSwitchButtonClick, MakeGenericClass(Action1, QnButton), this)
    this.m_RoutingButton.OnClick = MakeDelegateFromCSFunction(this.OnRoutingButtonClick, MakeGenericClass(Action1, QnButton), this)
    this.m_NpcTableView.OnSelectAtRow = MakeDelegateFromCSFunction(this.OnSelectAtRow, MakeGenericClass(Action1, Int32), this)
    UIEventListener.Get(this.m_MapTexture.gameObject).onClick = MakeDelegateFromCSFunction(this.OnClickMap, VoidDelegate, this)
    UIEventListener.Get(this.m_MapTexture.gameObject).onPress = MakeDelegateFromCSFunction(this.OnItemPress, BoolDelegate, this)
    UIEventListener.Get(this.m_MapTexture.gameObject).onDragStart = MakeDelegateFromCSFunction(this.OnDragItemStart, VoidDelegate, this)
    UIEventListener.Get(this.m_CloseButton).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)


    --初始化NPC 显示设置
    local showLevel = CScene.MainScene.MinimapShowLevel

    if showLevel == 0 then
        this.m_ShowNpcLevel = 7
    elseif showLevel == 1 then
        this.m_ShowNpcLevel = 6
    else
        this.m_ShowNpcLevel = 4
    end

    --看看是否有存储
    local savedLevel = PlayerSettings.GetNpcShowInfo(CScene.MainScene.SceneTemplateId)
    if savedLevel > 0 then this.m_ShowNpcLevel = savedLevel end

    UIEventListener.Get(this.m_NpcInfoButton).onClick = DelegateFactory.VoidDelegate(
        function(go)
            this.m_bShowNpcList = false
            this.m_NpcTableView.gameObject:SetActive(this.m_bShowNpcList)
            if this.m_NpcCheckGroup.activeSelf then
                this.m_NpcCheckGroup:SetActive(false)
            else
                this.m_NpcCheckGroup:SetActive(true)
                this.m_ImportantNpcCheckBox:SetSelected(bit.band(this.m_ShowNpcLevel, 4) > 0, true)
                this.m_NormalNpcCheckBox:SetSelected(bit.band(this.m_ShowNpcLevel, 2) > 0, true)
                this.m_OtherNpcCheckBox:SetSelected(bit.band(this.m_ShowNpcLevel, 1) > 0, true)
            end
        end
    )

    this.m_ImportantNpcCheckBox.OnValueChanged = DelegateFactory.Action_bool(
        function(select)
            this.m_ShowNpcLevel = select and bit.bor(this.m_ShowNpcLevel, 4) or bit.band(this.m_ShowNpcLevel, 3)

            if CScene.MainScene.MinimapShowLevel > 0 then
                --尝试存储
                PlayerSettings.TrySaveNpcShowInfo(CScene.MainScene.SceneTemplateId, this.m_ShowNpcLevel)
            end

            if CClientMainPlayer.Inst then
                Gac2Gas.QuerySceneInfo(CClientMainPlayer.Inst.EngineId)
            end
        end
    )
    this.m_NormalNpcCheckBox.OnValueChanged = DelegateFactory.Action_bool(
        function(select)
            this.m_ShowNpcLevel = select and bit.bor(this.m_ShowNpcLevel, 2) or bit.band(this.m_ShowNpcLevel, 5)

            if CScene.MainScene.MinimapShowLevel > 0 then
                --尝试存储
                PlayerSettings.TrySaveNpcShowInfo(CScene.MainScene.SceneTemplateId, this.m_ShowNpcLevel)
            end

            if CClientMainPlayer.Inst then
                Gac2Gas.QuerySceneInfo(CClientMainPlayer.Inst.EngineId)
            end
        end
    )
    this.m_OtherNpcCheckBox.OnValueChanged = DelegateFactory.Action_bool(
        function(select)
            this.m_ShowNpcLevel = select and bit.bor(this.m_ShowNpcLevel, 1) or bit.band(this.m_ShowNpcLevel, 6)

            if CScene.MainScene.MinimapShowLevel > 0 then
                --尝试存储
                PlayerSettings.TrySaveNpcShowInfo(CScene.MainScene.SceneTemplateId, this.m_ShowNpcLevel)
            end

            if CClientMainPlayer.Inst then
                Gac2Gas.QuerySceneInfo(CClientMainPlayer.Inst.EngineId)
            end
        end
    )
    --#endregion
    if CClientMainPlayer.Inst then
        Gac2Gas.QuerySceneInfo(CClientMainPlayer.Inst.EngineId)
    end

    this:UpdateMainPlayerPos()

    if CGuildLeagueMgr.Inst.inGuildLeagueMainScene then
        if CGuildLeagueMgr.Inst.buildInfos.Count > 0 then
            this:UpdateGLBuildingPosData(CGuildLeagueMgr.Inst.buildInfos)
        end
    end

    if CQingQiuMgr.Inst:IsInQingQiuScene() or CQingQiuMgr.Inst:IsInQingQiuCopyScene() then
        Gac2Gas.QingQiuQueryNpcAndMonsterLocation()
    end

    if CSnowBallMgr.Inst:IsInSnowBallFight() then
        Gac2Gas.QueryXueQiuPlayLocationInfo()
    end

    if CJXYSMgr.Inst.InJXYS then
        Gac2Gas.RequestQueryJXYSBossPos()
        -- 请求boss位置
        if CRandomMapMgr.Inst:IsInRandomMap() then --策划设定有一些绛雪地图不是随机地图，这种情况无需请求是否显示地图，节省点rpc开销
            Gac2Gas.QueryJXYSPlayerDieCount(CScene.MainScene.SceneTemplateId)
            --请求是否显示地图
        end
    end

    if CWeekendFightMgr.Inst.inFight then
        this:UpdateWeekendFightItem()
    end

    this:UpdateWorldEventShow()

    this:InitTeleport()

    this:SetCircleClip()

    this:InitHouseFurniture()

    this:InitGuildLeagueForce()

    CLuaMiniMapPopWnd:Init(this)

    if LuaSanxingGamePlayMgr.IsInPlay() then
      CLuaMiniMapPopWnd:UpdateSanxingFlagPos()
      CLuaMiniMapPopWnd:UpdateSanxingBossPos()
    end

    if CLuaMiniMapPopWnd.m_ShowWorldMap then
        CLuaMiniMapPopWnd.m_ShowWorldMap = false
        this:OnSwitchButtonClick(this.m_SwitchButton)
    end

    -- pubg
    local regionView = this.transform:Find("Contents/BigMap/CurrentMap/Map/Regions").gameObject
    regionView.gameObject:SetActive(CPUBGMgr.Inst:IsInChiJi())
    if regionView then
        local regionViewScript = regionView:GetComponent(typeof(CCommonLuaScript))
        regionViewScript:Init({})
    end

    if LuaJuDianBattleMgr:IsInGameplay() or LuaJuDianBattleMgr:IsInDailyGameplay() then
        this.m_JuDianCurrentMapRoot.gameObject:SetActive(true)
        this.m_BackToCityButton.gameObject:SetActive(false)
        this.m_RoutingButton.gameObject:SetActive(false)
        this.m_BackToGuildButton.gameObject:SetActive(false)
        this.m_BackToHouseButton.gameObject:SetActive(false)
        this.m_NpcButton.gameObject:SetActive(false)
    else
        this.m_JuDianCurrentMapRoot.gameObject:SetActive(false)
    end

    if LuaJuDianBattleMgr:IsInJuDian() then
        this.m_BackToCityButton.gameObject:SetActive(false)
        this.m_BackToGuildButton.gameObject:SetActive(false)
        this.m_BackToHouseButton.gameObject:SetActive(false)
    end
end

CMiniMapPopWnd.m_InitGuildLeagueForce_CS2LuaHook = function (this)
    if CGuildLeagueMgr.Inst.inGuildLeagueViceScene then
        this.m_GuildLeagueForce:SetActive(true)
        local force = CommonDefs.GetComponent_Component_Type(this.m_GuildLeagueForce.transform:Find("ForceTexture"), typeof(CUITexture))
        if CGuildLeagueMgr.Inst:GetMyForce() == 1 then
            force:LoadMaterial(CUIMaterialPaths.JiangJunWeiText)
        else
            force:LoadMaterial(CUIMaterialPaths.XuanXiaoNuText)
        end
    else
        this.m_GuildLeagueForce:SetActive(false)
    end
end
CMiniMapPopWnd.m_OnItemPress_CS2LuaHook = function (this, go, pressed)
    if not this.enableLongPress then
        return
    end
    if pressed then
        this.pressBegan = true
        this.dragged = false
        this.lastPressStartTime = Time.realtimeSinceStartup
        this.mousePosition = Input.mousePosition
    else
        this.pressBegan = false
        if not this.dragged then
            if Time.realtimeSinceStartup - this.lastPressStartTime > this.longPressDelta then
                --long press end
            else
                --if (OnItemClickDelegate != null)
                --    OnItemClickDelegate(this);
            end
        end
    end
end
CMiniMapPopWnd.m_OnDragItemStart_CS2LuaHook = function (this, go)
    if not this.enableLongPress then
        return
    end
    this.dragged = true
end
CMiniMapPopWnd.m_SendGuildLeagueFlag_CS2LuaHook = function (this, flagType, x, y)
    Gac2Gas.SetGuildLeagueFlag(EnumToInt(flagType), x, y)
end
CMiniMapPopWnd.m_OnUpdateGuildLeagueFlagPos_CS2LuaHook = function (this, flagType, x, y)
    if not CGuildLeagueMgr.Inst.inGuildLeagueBattle then
        return
    end
    local obj = CResourceMgr.Inst:Instantiate(CMiniMapGuildLeagueFlagResource.Inst, false, 10)
    if obj ~= nil then
        local Pos3d = Utility.PixelPos2WorldPos(CreateFromClass(CPos, x, y))
        -- Utility.GridPos2WorldPos((int)x, (int)y);
        obj.transform.parent = this.m_MapTexture.transform
        obj.transform.localPosition = this:CalculateMapPos(Pos3d)
        obj.transform.localRotation = Quaternion.identity
        obj.transform.localScale = Vector3.one
        obj.layer = this.gameObject.layer
        obj:SetActive(true)
        local template = CommonDefs.GetComponent_GameObject_Type(obj, typeof(CMiniMapGuildLeagueFlagTemplate))
        template:Init(CommonDefs.ConvertIntToEnum(typeof(EnumGuildLeagueFlagType), flagType))
    end
end
CMiniMapPopWnd.m_InitHouseFurniture_CS2LuaHook = function (this)
    CommonDefs.ListIterate(this.m_HouseJianzhuList, DelegateFactory.Action_object(function (___value)
        local kv = ___value
        if kv ~= nil then
            CResourceMgr.Inst:Destroy(kv)
        end
    end))
    CommonDefs.ListClear(this.m_HouseJianzhuList)

    local sceneType = CClientHouseMgr.Inst:GetCurSceneType()
    --处理认床逻辑
    local showBed = false
    local wakeupBedX,wakeupBedY,wakeupBedRoom,wakeupBedTemplateId=0,0,0,0
    if CClientMainPlayer.Inst then
        local basicProp = CClientMainPlayer.Inst.BasicProp
        wakeupBedX = basicProp.WakeupBedX
        wakeupBedY = basicProp.WakeupBedY
        wakeupBedRoom = basicProp.WakeupBedRoom
        wakeupBedTemplateId = basicProp.WakeupBedTemplateId
        showBed=wakeupBedTemplateId>0 and wakeupBedRoom==sceneType
    end

    if CScene.MainScene ~= nil and ((CClientHouseMgr.Inst:IsPlayerInHouse() and sceneType == EnumHouseSceneType_lua.eYard) or CHouseCompetitionMgr.Inst:IsInFusaiPlay()) then
        CommonDefs.EnumerableIterate(CClientFurnitureMgr.Inst:GetAllFurnitures(), DelegateFactory.Action_object(function (fur)
            local zswdata = Zhuangshiwu_Zhuangshiwu.GetData(fur.TemplateId)
            local needShow = zswdata.Type == EnumFurnitureType_lua.eJianzhu and zswdata.SubType ~= EnumFurnitureSubType_lua.eWeiqiang
            local name = nil
            if not needShow and showBed then
                if fur.TemplateId==wakeupBedTemplateId and math.floor(fur.ServerPos.x)==wakeupBedX and math.floor(fur.ServerPos.z)==wakeupBedY then
                    needShow = true
                    name=LocalString.GetString("床")--zswdata.Name
                end
            else
                name = CClientFurnitureMgr.GetJianzhuName(zswdata)
            end
            if needShow then
                local Pos3d = Utility.GridPos2WorldPos(math.floor(fur.ServerPos.x), math.floor(fur.ServerPos.z))
                local obj = CResourceMgr.Inst:Instantiate(CMiniMapPopWndHouseJianzhuMarkResource.Inst, false, 10)
                if obj ~= nil then
                    obj.transform.parent = this.m_MapTexture.transform
                    obj.transform.localPosition = this:CalculateMapPos(Pos3d)
                    obj.transform.localRotation = Quaternion.identity
                    obj.transform.localScale = CommonDefs.op_Multiply_Vector3_Single(Vector3.one, 1.5)
                    obj.layer = this.gameObject.layer
                    obj:SetActive(true)
                    local template = CommonDefs.GetComponent_GameObject_Type(obj, typeof(CMiniMapMarkTemplate))
                    template.EngineId = 1
                    -- 不是1，要不然默认就是跑商的图标了

                    local type = EMinimapMarkType.Npc_NotConcerned
                    template:UpdateView(EMinimapCategory.Normal, type, name, "")

                    CommonDefs.ListAdd(this.m_HouseJianzhuList, typeof(GameObject), obj)
                end
            end

        end))
    end
    -- 一键收回家具时显示
    if CScene.MainScene ~= nil and CClientHouseMgr.Inst:IsPlayerInHouse() and CMiniMapPopWnd.m_ZhuangshiwuTemplateId ~= 0 then
        CommonDefs.EnumerableIterate(CClientFurnitureMgr.Inst:GetAllFurnitures(), DelegateFactory.Action_object(function (___value)
            local fur = ___value
            if fur.TemplateId == CMiniMapPopWnd.m_ZhuangshiwuTemplateId then
                local zswdata = Zhuangshiwu_Zhuangshiwu.GetData(fur.TemplateId)
                local x = fur.ServerPos.x
                local z = fur.ServerPos.z
                if zswdata.SubType == EnumFurnitureSubType_lua.eQinggong then
                    local parentFur = CClientFurnitureMgr.Inst:GetFurniture(fur.ParentId)
                    if parentFur ~= nil then
                        x = parentFur.ServerPos.x
                        z = parentFur.ServerPos.z
                    end
                end

                local Pos3d = Utility.GridPos2WorldPos(math.floor(x), math.floor(z))

                local obj = NGUITools.AddChild(this.m_MapTexture.gameObject, this.m_ZhuangshiwuTemplate)
                if obj ~= nil then
                    obj.transform.localPosition = this:CalculateMapPos(Pos3d)
                    obj.layer = this.gameObject.layer
                    obj:SetActive(true)
                    local template = CommonDefs.GetComponent_GameObject_Type(obj, typeof(CMiniMapMarkTemplate))
                    template.EngineId = 1
                    -- 不是1，要不然默认就是跑商的图标了

                    local type = EMinimapMarkType.Jiayuan_Zhuangshiwu
                    --特殊处理查找装饰物时的图标
                    template:UpdateView(EMinimapCategory.Normal, type, "minimap_teammember","", "")
                    local sprite = obj.transform:GetComponent(typeof(UISprite))
                    sprite.width = 18
                end
            end
        end))
    end
end
CMiniMapPopWnd.m_OnEnable_CS2LuaHook = function (this)
    EventManager.AddListener(EnumEventType.RenderSceneInit, MakeDelegateFromCSFunction(this.InitMainPlayerTexturePos, Action0, this))
    EventManager.AddListener(EnumEventType.RenderSceneInit, MakeDelegateFromCSFunction(this.InitTeleport, Action0, this))
    EventManager.AddListener(EnumEventType.MainPlayerMoveStepped, MakeDelegateFromCSFunction(this.UpdateMainPlayerPos, Action0, this))
    EventManager.AddListener(EnumEventType.MainPlayerMoveEnded, MakeDelegateFromCSFunction(this.ClearMapPath, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.UpdateNpcInfo, MakeDelegateFromCSFunction(this.LoadNpcInfos, MakeGenericClass(Action1, MakeGenericClass(List, Object)), this))
    EventManager.AddListenerInternal(EnumEventType.UpdateObjPosInfo, MakeDelegateFromCSFunction(this.UpdatePlayerInfos, MakeGenericClass(Action1, MakeGenericClass(List, Object)), this))
    EventManager.AddListenerInternal(EnumEventType.UpdateHuoCheMapPosInfo, MakeDelegateFromCSFunction(this.UpdateHuocheInfo, MakeGenericClass(Action1, MakeGenericClass(List, Object)), this))
    EventManager.AddListener(EnumEventType.OnGumuOpened, MakeDelegateFromCSFunction(this.OnGumuOpened, Action0, this))

    EventManager.AddListenerInternal(EnumEventType.UpdateGuildLeagueInfo, MakeDelegateFromCSFunction(this.UpdateGuildLeagueInfo, MakeGenericClass(Action1, MakeGenericClass(List, GuildLeagueBossInfo)), this))
    EventManager.AddListenerInternal(EnumEventType.UpdateGLBuildingPosData, MakeDelegateFromCSFunction(this.UpdateGLBuildingPosData, MakeGenericClass(Action1, MakeGenericClass(List, GuildLeagueBuildInfo)), this))
    EventManager.AddListenerInternal(EnumEventType.QueryHugPlayerPosResult, MakeDelegateFromCSFunction(this.UpdateGuildQueenHugPosData, MakeGenericClass(Action1, MakeGenericClass(List, GuildQueenHugInfo)), this))

    EventManager.AddListener(EnumEventType.MiniMapLoadFinished, MakeDelegateFromCSFunction(this.OnMiniMapLoadFinished, Action0, this))

    EventManager.AddListenerInternal(EnumEventType.UpdateGuildLeagueFlagPos, MakeDelegateFromCSFunction(this.OnUpdateGuildLeagueFlagPos, MakeGenericClass(Action3, UInt32, UInt32, UInt32), this))

    EventManager.AddListenerInternal(EnumEventType.SetQingQiuMonsterInMap, MakeDelegateFromCSFunction(this.UpdateQingQiuMonster, MakeGenericClass(Action1, MakeGenericClass(List, CQingQiuMonster)), this))
    EventManager.AddListenerInternal(EnumEventType.OnUpdateJXYSBossLocations, MakeDelegateFromCSFunction(this.UpdateJXYSBoss, MakeGenericClass(Action1, CJXYSBossInfo), this))
    EventManager.AddListenerInternal(EnumEventType.OnUpdateJXYSMap, MakeDelegateFromCSFunction(this.UpdateJXYSMapTexture, MakeGenericClass(Action1, Boolean), this))
    EventManager.AddListenerInternal(EnumEventType.UpdateNPCMiniMapInfo, MakeDelegateFromCSFunction(this.UpdateNPCPosInfo, MakeGenericClass(Action3, UInt32, Int32, Int32), this))

    EventManager.AddListener(EnumEventType.UpdateWorldEvent, MakeDelegateFromCSFunction(this.UpdateWorldEventShow, Action0, this))

    EventManager.AddListenerInternal(EnumEventType.ReplyCityWarPlayOpenStatus, MakeDelegateFromCSFunction(this.UpdateCityWarPlayOpenStatus, MakeGenericClass(Action1, Boolean), this))
    EventManager.AddListenerInternal(EnumEventType.UpdateBabyNPCPos, MakeDelegateFromCSFunction(this.UpdateBabyNPCPos, MakeGenericClass(Action1, MakeGenericClass(List, CBabyNPCPosInfo)), this))

    --if CWeekendFightMgr.Inst.inFight then
        EventManager.AddListener(EnumEventType.UpdateWeekendFightForceInfo, MakeDelegateFromCSFunction(this.UpdateWeekendFightItem, Action0, this))
    --end

    --转lua时请保留
    this.m_BackToCityButton.gameObject:SetActive(false)
    Gac2Gas.QueryCityWarPlayOpenStatus()
    UIEventListener.Get(this.m_BackToCityButton.gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
        Gac2Gas.RequestTeleportToTerritoryByBigMap()
    end)

    g_ScriptEvent:AddListener("SyncYarkPick", CLuaMiniMapPopWnd, "OnSyncYarkPick")
    g_ScriptEvent:AddListener("UpdateQLDBossState", CLuaMiniMapPopWnd, "UpdateQLDBossState")
    g_ScriptEvent:AddListener("SanxingFlagUpdate", CLuaMiniMapPopWnd, "UpdateSanxingFlagPos")
    g_ScriptEvent:AddListener("SanxingBossUpdate", CLuaMiniMapPopWnd, "UpdateSanxingBossPos")
    g_ScriptEvent:AddListener("SyncHalloween2020ArrestPlayInfo", CLuaMiniMapPopWnd, "OnSyncHalloween2020ArrestPlayInfo")
    g_ScriptEvent:AddListener("AnQiIslandSyncCannonBallAndBossPos", CLuaMiniMapPopWnd, "OnAnQiIslandSyncCannonBallAndBossPos")
    g_ScriptEvent:AddListener("MainPlayerSoulCoreCreate", CLuaMiniMapPopWnd, "TryShowMainPlayerSoulCorePos")
    g_ScriptEvent:AddListener("OnSyncGnjcBossHpInfo", CLuaMiniMapPopWnd, "OnSyncGnjcBossHpInfo")
    g_ScriptEvent:AddListener("OnUpdateHuaCheMapPos", CLuaMiniMapPopWnd, "OnUpdateHuaCheMapPos")
    g_ScriptEvent:AddListener("OnQueryGuildLeaguePlayerPosResult", CLuaMiniMapPopWnd, "OnQueryGuildLeaguePlayerPosResult")
    g_ScriptEvent:AddListener("OnQueryPlayerPos_JXYSEnemy", CLuaMiniMapPopWnd, "OnQueryPlayerPos_JXYSEnemy")
    g_ScriptEvent:AddListener("UpdateMiniMapMark", CLuaMiniMapPopWnd, "UpdateMiniMapMark")
    g_ScriptEvent:AddListener("RefreshMiniMapMarks", CLuaMiniMapPopWnd, "RefreshMiniMapMarks")
    g_ScriptEvent:AddListener("RefreshMiniMapPopMarks", CLuaMiniMapPopWnd, "RefreshMiniMapMarks")
	UIEventListener.Get(this.WorldEventBtn).onClick = MakeDelegateFromCSFunction(this.WorldEevntIconClick, VoidDelegate, this)

end
CMiniMapPopWnd.m_OnDisable_CS2LuaHook = function (this)
    EventManager.RemoveListener(EnumEventType.RenderSceneInit, MakeDelegateFromCSFunction(this.InitMainPlayerTexturePos, Action0, this))
    EventManager.RemoveListener(EnumEventType.RenderSceneInit, MakeDelegateFromCSFunction(this.InitTeleport, Action0, this))
    EventManager.RemoveListener(EnumEventType.MainPlayerMoveStepped, MakeDelegateFromCSFunction(this.UpdateMainPlayerPos, Action0, this))
    EventManager.RemoveListener(EnumEventType.MainPlayerMoveEnded, MakeDelegateFromCSFunction(this.ClearMapPath, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.UpdateNpcInfo, MakeDelegateFromCSFunction(this.LoadNpcInfos, MakeGenericClass(Action1, MakeGenericClass(List, Object)), this))
    EventManager.RemoveListenerInternal(EnumEventType.UpdateObjPosInfo, MakeDelegateFromCSFunction(this.UpdatePlayerInfos, MakeGenericClass(Action1, MakeGenericClass(List, Object)), this))
    EventManager.RemoveListenerInternal(EnumEventType.UpdateHuoCheMapPosInfo, MakeDelegateFromCSFunction(this.UpdateHuocheInfo, MakeGenericClass(Action1, MakeGenericClass(List, Object)), this))
    EventManager.RemoveListener(EnumEventType.OnGumuOpened, MakeDelegateFromCSFunction(this.OnGumuOpened, Action0, this))

    EventManager.RemoveListenerInternal(EnumEventType.UpdateGuildLeagueInfo, MakeDelegateFromCSFunction(this.UpdateGuildLeagueInfo, MakeGenericClass(Action1, MakeGenericClass(List, GuildLeagueBossInfo)), this))
    EventManager.RemoveListenerInternal(EnumEventType.UpdateGLBuildingPosData, MakeDelegateFromCSFunction(this.UpdateGLBuildingPosData, MakeGenericClass(Action1, MakeGenericClass(List, GuildLeagueBuildInfo)), this))
    EventManager.RemoveListenerInternal(EnumEventType.QueryHugPlayerPosResult, MakeDelegateFromCSFunction(this.UpdateGuildQueenHugPosData, MakeGenericClass(Action1, MakeGenericClass(List, GuildQueenHugInfo)), this))

    EventManager.RemoveListener(EnumEventType.MiniMapLoadFinished, MakeDelegateFromCSFunction(this.OnMiniMapLoadFinished, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.UpdateGuildLeagueFlagPos, MakeDelegateFromCSFunction(this.OnUpdateGuildLeagueFlagPos, MakeGenericClass(Action3, UInt32, UInt32, UInt32), this))

    EventManager.RemoveListenerInternal(EnumEventType.SetQingQiuMonsterInMap, MakeDelegateFromCSFunction(this.UpdateQingQiuMonster, MakeGenericClass(Action1, MakeGenericClass(List, CQingQiuMonster)), this))
    EventManager.RemoveListenerInternal(EnumEventType.OnUpdateJXYSBossLocations, MakeDelegateFromCSFunction(this.UpdateJXYSBoss, MakeGenericClass(Action1, CJXYSBossInfo), this))
    EventManager.RemoveListenerInternal(EnumEventType.OnUpdateJXYSMap, MakeDelegateFromCSFunction(this.UpdateJXYSMapTexture, MakeGenericClass(Action1, Boolean), this))

    EventManager.RemoveListenerInternal(EnumEventType.UpdateNPCMiniMapInfo, MakeDelegateFromCSFunction(this.UpdateNPCPosInfo, MakeGenericClass(Action3, UInt32, Int32, Int32), this))

    EventManager.RemoveListener(EnumEventType.UpdateWorldEvent, MakeDelegateFromCSFunction(this.UpdateWorldEventShow, Action0, this))

    EventManager.RemoveListenerInternal(EnumEventType.ReplyCityWarPlayOpenStatus, MakeDelegateFromCSFunction(this.UpdateCityWarPlayOpenStatus, MakeGenericClass(Action1, Boolean), this))

    --if CWeekendFightMgr.Inst.inFight then
        EventManager.RemoveListener(EnumEventType.UpdateWeekendFightForceInfo, MakeDelegateFromCSFunction(this.UpdateWeekendFightItem, Action0, this))
    --end

    EventManager.RemoveListenerInternal(EnumEventType.UpdateBabyNPCPos, MakeDelegateFromCSFunction(this.UpdateBabyNPCPos, MakeGenericClass(Action1, MakeGenericClass(List, CBabyNPCPosInfo)), this))
    g_ScriptEvent:RemoveListener("SyncYarkPick", CLuaMiniMapPopWnd, "OnSyncYarkPick")
    g_ScriptEvent:RemoveListener("UpdateQLDBossState", CLuaMiniMapPopWnd, "UpdateQLDBossState")
    g_ScriptEvent:RemoveListener("SanxingFlagUpdate", CLuaMiniMapPopWnd, "UpdateSanxingFlagPos")
    g_ScriptEvent:RemoveListener("SanxingBossUpdate", CLuaMiniMapPopWnd, "UpdateSanxingBossPos")
    g_ScriptEvent:RemoveListener("SyncHalloween2020ArrestPlayInfo", CLuaMiniMapPopWnd, "OnSyncHalloween2020ArrestPlayInfo")
    g_ScriptEvent:RemoveListener("AnQiIslandSyncCannonBallAndBossPos", CLuaMiniMapPopWnd, "OnAnQiIslandSyncCannonBallAndBossPos")
    g_ScriptEvent:RemoveListener("MainPlayerSoulCoreCreate", CLuaMiniMapPopWnd, "TryShowMainPlayerSoulCorePos")
    g_ScriptEvent:RemoveListener("OnSyncGnjcBossHpInfo", CLuaMiniMapPopWnd, "OnSyncGnjcBossHpInfo")
    g_ScriptEvent:RemoveListener("OnUpdateHuaCheMapPos", CLuaMiniMapPopWnd, "OnUpdateHuaCheMapPos")
    g_ScriptEvent:RemoveListener("OnQueryGuildLeaguePlayerPosResult", CLuaMiniMapPopWnd, "OnQueryGuildLeaguePlayerPosResult")
    g_ScriptEvent:RemoveListener("OnQueryPlayerPos_JXYSEnemy", CLuaMiniMapPopWnd, "OnQueryPlayerPos_JXYSEnemy")
    g_ScriptEvent:RemoveListener("UpdateMiniMapMark", CLuaMiniMapPopWnd, "UpdateMiniMapMark")
    g_ScriptEvent:RemoveListener("RefreshMiniMapMarks", CLuaMiniMapPopWnd, "RefreshMiniMapMarks")
    g_ScriptEvent:RemoveListener("RefreshMiniMapPopMarks", CLuaMiniMapPopWnd, "RefreshMiniMapMarks")
end
CMiniMapPopWnd.m_OnMiniMapLoadFinished_CS2LuaHook = function (this)
    this:Init()
end

CMiniMapPopWnd.m_UpdateGuildLeagueInfo_CS2LuaHook = function (this, infos)
    local towerInfoTbl = {}
    for i=0, infos.Count-1 do
        local info = infos[i]
        if CLuaMiniMapPopWnd:IsGuildLeagueTower(info.templateId) then
            table.insert(towerInfoTbl, {templateId = info.templateId, hp = info.hp, hpFull = info.hpFull, posX = info.posX, posY = info.posY, force = info.force})
        else
            local find = CommonDefs.ListFind(this.m_GuildLeagueBossMapMarkList, typeof(CMiniMapGuildLeagueBossTemplate), DelegateFactory.Predicate_CMiniMapGuildLeagueBossTemplate(function (p)
                return p.TemplateId == info.templateId
            end))
            if find ~= nil then
                local obj = find.gameObject
                local Pos3d = Utility.GridPos2WorldPos(info.posX, info.posY)
                obj.transform.localPosition = this:CalculateMapPos(Pos3d)
            else
                local obj = CResourceMgr.Inst:Instantiate(CMiniMapGuildLeagueBossResource.Inst, false, 10)
                if obj ~= nil then
                    local Pos3d = Utility.GridPos2WorldPos(info.posX, info.posY)
                    obj.transform.parent = this.m_MapTexture.transform
                    obj.transform.localPosition = this:CalculateMapPos(Pos3d)
                    obj.transform.localRotation = Quaternion.identity
                    obj.transform.localScale = Vector3.one
                    obj.layer = this.gameObject.layer
                    obj:SetActive(true)
                    local template = CommonDefs.GetComponent_GameObject_Type(obj, typeof(CMiniMapGuildLeagueBossTemplate))
                    template:Init(info)
                    CommonDefs.ListAdd(this.m_GuildLeagueBossMapMarkList, typeof(CMiniMapGuildLeagueBossTemplate), template)
                end
            end
        end
    end
    CLuaMiniMapPopWnd:OnQueryGuildLeagueTowerPosResult(towerInfoTbl)
end
CMiniMapPopWnd.m_UpdateQingQiuMonster_CS2LuaHook = function (this, locations)
    if not CQingQiuMgr.Inst:IsInQingQiuScene() and not CQingQiuMgr.Inst:IsInQingQiuCopyScene() then
        return
    end
    CommonDefs.ListIterate(this.m_QingQiuMonsterLocationList, DelegateFactory.Action_object(function (___value)
        local v = ___value
        if v ~= nil then
            CResourceMgr.Inst:Destroy(v)
        end
    end))
    CommonDefs.ListClear(this.m_QingQiuMonsterLocationList)
    CommonDefs.DictClear(this.m_QingQiuMonsterDict)
    CommonDefs.DictClear(this.m_QingQiuMarkDict)

    do
        local i = 0 local cnt = locations.Count
        while i < cnt do
            local obj = CResourceMgr.Inst:Instantiate(CMiniMapPopWndNpcMarkResource.Inst, false, 10)
            if obj ~= nil then
                local Pos3d = Utility.GridPos2WorldPos(locations[i].m_PosX, locations[i].m_PosY)
                obj.transform.parent = this.m_MapTexture.transform
                obj.transform.localPosition = this:CalculateMapPos(Pos3d)
                obj.transform.localRotation = Quaternion.identity
                obj.transform.localScale = Vector3.one
                obj.layer = this.gameObject.layer
                obj:SetActive(true)
                local template = CommonDefs.GetComponent_GameObject_Type(obj, typeof(CMiniMapMarkTemplate))
                template.EngineId = locations[i].m_EngineId
                template.OnButtonSelected_02 = CommonDefs.CombineListner_Action_QnSelectableButton_bool(template.OnButtonSelected_02, MakeDelegateFromCSFunction(this.OnSelectMapMark, MakeGenericClass(Action2, QnSelectableButton, Boolean), this), true)
                if (locations[i].m_Type == "Npc") then
                    template:UpdateView(EMinimapCategory.Normal, EMinimapMarkType.Npc_NotConcerned, locations[i].m_Name, "")
                elseif (locations[i].m_Type == "Monster") then
                    template:UpdateView(EMinimapCategory.Normal, EMinimapMarkType.Monster_Enemy, locations[i].m_Name, "")
                end
                CommonDefs.DictSet(this.m_QingQiuMonsterDict, typeof(UInt32), locations[i].m_EngineId, typeof(CQingQiuMonster), locations[i])
                CommonDefs.ListAdd(this.m_QingQiuMonsterLocationList, typeof(GameObject), obj)
                CommonDefs.DictSet(this.m_QingQiuMarkDict, typeof(UInt32), locations[i].m_EngineId, typeof(CMiniMapMarkTemplate), template)
            end
            i = i + 1
        end
    end
end
CMiniMapPopWnd.m_UpdateJXYSBoss_CS2LuaHook = function (this, bossInfo)
    if not CJXYSMgr.Inst.InJXYS then
        return
    end
    CommonDefs.ListIterate(this.m_JXYSBossLocationList, DelegateFactory.Action_object(function (___value)
        local v = ___value
        if v ~= nil then
            CResourceMgr.Inst:Destroy(v)
        end
    end))
    CommonDefs.ListClear(this.m_JXYSBossLocationList)

    local obj = CResourceMgr.Inst:Instantiate(CMiniMapPopWndNpcMarkResource.Inst, false, 10)
    if obj ~= nil then
        local Pos3d = Utility.GridPos2WorldPos(bossInfo.posX, bossInfo.posY)
        obj.transform.parent = this.m_MapTexture.transform
        obj.transform.localPosition = this:CalculateMapPos(Pos3d)
        obj.transform.localRotation = Quaternion.identity
        obj.transform.localScale = Vector3.one
        obj.layer = this.gameObject.layer
        obj:SetActive(true)
        local monster = Monster_Monster.GetData(bossInfo.templateId)
        local template = CommonDefs.GetComponent_GameObject_Type(obj, typeof(CMiniMapMarkTemplate))
        if monster ~= nil and template ~= nil then
            template.EngineId = bossInfo.engineId
            template:UpdateView(EMinimapCategory.Normal, EMinimapMarkType.Monster_Enemy, monster.Name, "")
            CommonDefs.ListAdd(this.m_JXYSBossLocationList, typeof(GameObject), obj)
        end
    end
end
CMiniMapPopWnd.m_UpdateJXYSMapTexture_CS2LuaHook = function (this, bShow)
    if CJXYSMgr.Inst.InJXYS and CRandomMapMgr.Inst:IsInRandomMap() then --绛雪部分地图改为了非随机地图，这里做个判断区分，避免非随机地图显示随机地图信息
        if bShow then
            this.m_MapTexture.mainTexture = CMiniMap.Instance.mapTexture.mainTexture
            this.m_NoMinimapTipGO:SetActive(false)
        else
            this.m_MapTexture.mainTexture = CMiniMap.Instance.mHiddenMiniMapTexture
            this.m_NoMinimapTipGO:SetActive(true)
        end
    end
end
CMiniMapPopWnd.m_UpdateGLBuildingPosData_CS2LuaHook = function (this, list)
    --只有主战场会处理箭塔
    if not CGuildLeagueMgr.Inst.inGuildLeagueMainScene then
        return
    end

    CommonDefs.ListIterate(this.m_GuildLeagueBuildMapMarkList, DelegateFactory.Action_object(function (___value)
        local v = ___value
        CResourceMgr.Inst:Destroy(v.gameObject)
    end))
    CommonDefs.ListClear(this.m_GuildLeagueBuildMapMarkList)
    do
        local i = 0
        while i < list.Count do
            local info = list[i]
            local obj = CResourceMgr.Inst:Instantiate(CMiniMapGuildLeagueBuildResource.Inst, false, 10)
            if obj ~= nil then
                local Pos3d = Utility.GridPos2WorldPos(info.x, info.y)
                obj.transform.parent = this.m_MapTexture.transform
                obj.transform.localPosition = this:CalculateMapPos(Pos3d)
                obj.transform.localRotation = Quaternion.identity
                obj.transform.localScale = Vector3.one
                obj.layer = this.gameObject.layer
                obj:SetActive(true)
                local template = CommonDefs.GetComponent_GameObject_Type(obj, typeof(CMiniMapGuildLeagueBuildTemplate))
                template:Init(info)
                CommonDefs.ListAdd(this.m_GuildLeagueBuildMapMarkList, typeof(CMiniMapGuildLeagueBuildTemplate), template)
            end
            i = i + 1
        end
    end
end
CMiniMapPopWnd.m_UpdateGuildQueenHugPosData_CS2LuaHook = function (this, list)
    --只有在帮花第三关才同步这个信息
    if CScene.MainScene == nil or CScene.MainScene.SceneTemplateId ~= CMiniMapPopWnd.GUILD_QUEEN_HUG_SCENE_ID then
        return
    end
    CommonDefs.ListIterate(this.m_GuildQueenHugMarkList, DelegateFactory.Action_object(function (___value)
        local v = ___value
        CResourceMgr.Inst:Destroy(v)
    end))
    CommonDefs.ListClear(this.m_GuildQueenHugMarkList)
    do
        local i = 0
        while i < list.Count do
            local info = list[i]
            local obj = CResourceMgr.Inst:Instantiate(CMiniMapGuildQueenHugResource.Inst, false, 4)
            if obj ~= nil then
                local Pos3d = Utility.GridPos2WorldPos(info.Pos.x, info.Pos.y)
                obj.transform.parent = this.m_MapTexture.transform
                obj.transform.localPosition = this:CalculateMapPos(Pos3d)
                obj.transform.localRotation = Quaternion.identity
                obj.transform.localScale = Vector3.one
                obj.layer = this.gameObject.layer
                local template = CommonDefs.GetComponent_GameObject_Type(obj, typeof(CMiniMapMarkTemplate))
                template.Text = info.PlayerName
                obj:SetActive(true)
                CommonDefs.ListAdd(this.m_GuildQueenHugMarkList, typeof(GameObject), obj)
            end
            i = i + 1
        end
    end
end
CMiniMapPopWnd.m_OnNpcListButtonClick_CS2LuaHook = function (this, button)
    this.m_bShowNpcList = not this.m_bShowNpcList
    this.m_NpcTableView.gameObject:SetActive(this.m_bShowNpcList)
    if this.m_bShowNpcList then
        this.m_NpcTableView:ReloadData(false, false)
    end
end
CMiniMapPopWnd.m_OnClickBackToGuildButton_CS2LuaHook = function (this, button)
    if CClientMainPlayer.Inst ~= nil then
        Gac2Gas.RequestOperationInGuild(CClientMainPlayer.Inst.BasicProp.GuildId, "BackToGuildCity", "", "")
    end
end
CMiniMapPopWnd.m_OnClickBackToHouseButton_CS2LuaHook = function (this, button)

    CClientHouseMgr.Inst:GoBackHome()
end
CMiniMapPopWnd.m_OnClickInfoButton_CS2LuaHook = function (this, button)
    local contentColor = 4294967295
    --白色
    if CScene.MainScene ~= nil then
        local data = PublicMap_PublicMap.GetData(CScene.MainScene.SceneTemplateId)
        local tips = CreateFromClass(MakeGenericClass(List, String))
        if data ~= nil then
            if data.AllowPK then
                if CScene.MainScene:IsInSafeRegion(CClientMainPlayer.Inst) then
                    contentColor = 4294967295
                    CommonDefs.ListAdd(tips, typeof(String), LocalString.GetString("不可进行PK战斗"))
                else
                    CommonDefs.ListAdd(tips, typeof(String), LocalString.GetString("可进行PK战斗"))
                    contentColor = 4278190335
                    --红色
                    if data.EnablePK then
                        CommonDefs.ListAdd(tips, typeof(String), LocalString.GetString("恶意杀人增加PK值"))
                        local default
                        if data.IsDeathPunish then
                            default = LocalString.GetString("死亡有惩罚")
                        else
                            default = LocalString.GetString("死亡无惩罚")
                        end
                        CommonDefs.ListAdd(tips, typeof(String), default)
                    else
                        CommonDefs.ListAdd(tips, typeof(String), LocalString.GetString("恶意杀人不增加PK值"))
                        local extern
                        if data.IsDeathPunish then
                            extern = LocalString.GetString("死亡有惩罚")
                        else
                            extern = LocalString.GetString("死亡无惩罚")
                        end
                        CommonDefs.ListAdd(tips, typeof(String), extern)
                    end
                    local ref
                    if data.CanDuoHun then
                        ref = LocalString.GetString("可夺魂")
                    else
                        ref = LocalString.GetString("不可夺魂")
                    end
                    CommonDefs.ListAdd(tips, typeof(String), ref)
                end
            else
                contentColor = 4294967295
                --白色
                CommonDefs.ListAdd(tips, typeof(String), LocalString.GetString("不可进行PK战斗"))
            end
            if (data.DecPK and not CScene.MainScene:IsInSafeRegion(CClientMainPlayer.Inst)) or CScene.MainScene.SceneTemplateId == 16100021 then
                CommonDefs.ListAdd(tips, typeof(String), LocalString.GetString("在此区域活动可减少PK值"))
            else
                CommonDefs.ListAdd(tips, typeof(String), LocalString.GetString("此区域活动无法减少PK值"))
            end
        end
        
        CommonDefs.GetComponent_Component_Type(this.m_InfoButton.transform:GetChild(0), typeof(UISprite)).color = NGUIMath.HexToColor(contentColor)
        --只有点击真正点击调用的才弹出窗口
        if button ~= nil then
            CMessageTipMgr.Inst:Display(WndType.Tooltip, true, LocalString.GetString("当前场景须知"), tips, 400, button.transform, CTooltip.AlignType.Top, contentColor)
        end
    end
end

-- 切换世界/当前地图
CMiniMapPopWnd.m_OnSwitchButtonClick_CS2LuaHook = function (this, button)    
    this.m_bShowNpcList = false
    this.m_NpcTableView.gameObject:SetActive(false)
    CLuaMiniMapPopWnd.m_ShowWorldMap = not CLuaMiniMapPopWnd.m_ShowWorldMap
    this.m_WorldMapRoot:SetActive(CLuaMiniMapPopWnd.m_ShowWorldMap)
    this.m_CurrentMapRoot:SetActive(not CLuaMiniMapPopWnd.m_ShowWorldMap)
    this.m_NpcButton.gameObject:SetActive(not CLuaMiniMapPopWnd.m_ShowWorldMap)
    this.m_RoutingButton.gameObject:SetActive(not CLuaMiniMapPopWnd.m_ShowWorldMap)
    this.m_ButtonTable.repositionNow = true
    this.m_InfoButton.Visible = not CLuaMiniMapPopWnd.m_ShowWorldMap
    CLuaMiniMapPopWnd:SetShowPinSystem(not CLuaMiniMapPopWnd.m_ShowWorldMap)
    if CLuaMiniMapPopWnd.m_ShowWorldMap then
        this.m_NpcInfoButton:SetActive(false)
        this.m_NpcCheckGroup:SetActive(false)
        this.m_ClipBackground.gameObject:SetActive(false)

        -- 据点战世界地图特殊处理
        if LuaJuDianBattleMgr:IsInGameplay() or LuaJuDianBattleMgr:IsInJuDian() or LuaJuDianBattleMgr:IsInDailyGameplay() or LuaJuDianBattleMgr:IsInTaskGameplay() then
            this.m_JuDianWorldMapRoot.gameObject:SetActive(true)
            -- 隐藏按钮
            this.m_BackToGuildButton.gameObject:SetActive(false)
            this.m_BackToHouseButton.gameObject:SetActive(false)
            this.m_BackToCityButton.gameObject:SetActive(false)
            this.m_WorldMapRoot:SetActive(false)
            this.m_RoutingButton.gameObject:SetActive(false)
            this.m_NpcButton.gameObject:SetActive(false)
        else
            this:InitWorldMapInfos()
            Gac2Gas.QueryOpenedGumuInfo()
        end
    else
        if LuaJuDianBattleMgr:IsInGameplay() or LuaJuDianBattleMgr:IsInDailyGameplay() or LuaJuDianBattleMgr:IsInTaskGameplay() then
            this.m_JuDianWorldMapRoot.gameObject:SetActive(false)
            this.m_SwitchButton.gameObject:SetActive(true)
            this.m_NpcButton.gameObject:SetActive(false)
            this.m_BackToGuildButton.gameObject:SetActive(false)
            this.m_BackToHouseButton.gameObject:SetActive(false)
            this.m_BackToCityButton.gameObject:SetActive(false)
            this.m_InfoButton.Visible = false
            this.m_RoutingButton.gameObject:SetActive(false)
        elseif LuaJuDianBattleMgr:IsInJuDian() then
            this.m_BackToGuildButton.gameObject:SetActive(false)
            this.m_BackToHouseButton.gameObject:SetActive(false)
            this.m_BackToCityButton.gameObject:SetActive(false)
        else
            this.m_NpcInfoButton:SetActive(true)
        end
        this.m_ClipBackground.gameObject:SetActive(CMiniMapPopWnd.m_isCircleClipping)
    end
end

CMiniMapPopWnd.m_OnRoutingButtonClick_CS2LuaHook = function (this, button)
    CUIManager.ShowUI(CUIResources.MiniMapRoutingWnd)
    CMiniMapRoutingWnd.RouteTo = MakeDelegateFromCSFunction(this.RouteTo, MakeGenericClass(Func3, Int32, Int32, Boolean), this)
end

CMiniMapPopWnd.m_LoadNpcInfos_CS2LuaHook = function (this, npcData)
    CommonDefs.EnumerableIterate(this.m_NpcMapMarkList.Values, DelegateFactory.Action_object(function (___value)
        local v = ___value
        CResourceMgr.Inst:Destroy(v.gameObject)
    end))
    CommonDefs.DictClear(this.m_NpcMapMarkList)
    CommonDefs.DictClear(this.m_NpcInfos)
    
    CLuaMiniMapPopWnd.m_NpcData = npcData

    local npcList = {}
    if npcData ~= nil and npcData.Count > 0 then
        local category = CMiniMap.GetMinimapCategory()
        do
            local i = 0
            while i < npcData.Count do
                local continue
                repeat
                    local engineId = math.floor(tonumber(npcData[i] or 0))
                    local templateId = math.floor(tonumber(npcData[i + 1] or 0))
                    local x = math.floor(tonumber(npcData[i + 2] or 0))
                    local z = math.floor(tonumber(npcData[i + 3] or 0))
                    local pos = CreateFromClass(CPos, x, z)

                    local info = CreateFromClass(NpcInfo)
                    info.TemplateId = templateId
                    info.Pos = pos
                    info.EngineId = engineId

                    --是否可见
                    local visible = true
                    local npcShow = NPC_ShowHide.GetData(templateId)
                    if npcShow ~= nil then
                        if npcShow.Status == 0 then
                            local showIndex = math.floor(tonumber(npcShow.ID or 0))
                            local hideIndex = math.floor(tonumber(npcShow.HideID or 0))
                            if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.PlayProp ~= nil then
                                visible = CClientMainPlayer.Inst.PlayProp:IsShowNPC(showIndex, hideIndex)
                            end
                        else
                            visible = npcShow.AlwaysShow ~= 0
                        end
                    end
                    if not visible then
                        continue = true
                        break
                    end

                    local data = NPC_NPC.GetData(templateId)
                    if data ~= nil then
                        if data.HideInMap > 0 then
                            continue = true
                            break
                        end
                        --是否显示在大地图上
                        info.Name = data.Name
                        info.PortaitName = data.Portrait
                        info.TitleIcon = data.TitleIcon
                        info.IsConcerned = data.IsConcerned

                        if data.Selectable > 0 and info then
                            CommonDefs.DictAdd(this.m_NpcInfos, typeof(UInt32), info.EngineId, typeof(NpcInfo), info)
                            table.insert(npcList, info)
                        end

                        if (bit.band(this.m_ShowNpcLevel, 4) == 0 and data.IsConcerned == 2) or
                        (bit.band(this.m_ShowNpcLevel, 2) == 0 and data.IsConcerned == 1) or
                        (bit.band(this.m_ShowNpcLevel, 1) == 0 and data.IsConcerned == 0) then
                            continue = true
                            break
                        end

                        local obj = CResourceMgr.Inst:Instantiate(CMiniMapPopWndNpcMarkResource.Inst, false, 10)
                        if obj ~= nil then
                            local Pos3d = Utility.GridPos2WorldPos(info.Pos.x, info.Pos.y)
                            obj.transform.parent = this.m_MapTexture.transform
                            obj.transform.localPosition = this:CalculateMapPos(Pos3d)
                            obj.transform.localRotation = Quaternion.identity
                            obj.transform.localScale = Vector3.one
                            obj.layer = this.gameObject.layer
                            obj:SetActive(true)
                            local template = CommonDefs.GetComponent_GameObject_Type(obj, typeof(CMiniMapMarkTemplate))
                            template.EngineId = engineId
                            template.OnButtonSelected_02 = CommonDefs.CombineListner_Action_QnSelectableButton_bool(template.OnButtonSelected_02, MakeDelegateFromCSFunction(this.OnSelectMapMark, MakeGenericClass(Action2, QnSelectableButton, Boolean), this), true)
                            --判断名字在上方还是下方
                            local pos = template.m_Label.transform.parent.localPosition
                            pos.y = data.NameUnderPointInMap and -math.abs(pos.y) or math.abs(pos.y)
                            template.m_Label.transform.parent.localPosition = pos

                            local type = EMinimapMarkType.Npc_NotConcerned
                            if info.IsConcerned > 0 then
                                type = (data.IsConcerned == 1) and EMinimapMarkType.Npc_Concerned or EMinimapMarkType.Npc_Concerned_Important
                            end

                            -- 这个标记位是一次性的 处理打开地图前的寻路
                            if CLuaMiniMapPopWnd.m_CurrentTrackingNpcTemplateId == templateId then
                                type = EMinimapMarkType.Npc_Current_Finding
                                CLuaMiniMapPopWnd.m_CurrentTrackingNpcTemplateId = 0
                            end

                            if CLuaMiniMapPopWnd.m_CurrentFindNpc and CLuaMiniMapPopWnd.m_CurrentFindNpc == engineId then
                                type = EMinimapMarkType.Npc_Current_Finding
                            end

                            CLuaMiniMapPopWnd:ProcessNPCTemplateStatus(template, templateId)
                            template:UpdateView(category, type, info.Name, info.TitleIcon)

                            CommonDefs.DictAdd(this.m_NpcMapMarkList, typeof(UInt32), info.EngineId, typeof(CMiniMapMarkTemplate), template)
                        end
                    end
                    continue = true
                until 1
                if not continue then
                    break
                end
                i = i + 4
            end
        end

        table.sort(npcList, function(a,b)
            if a.IsConcerned == b.IsConcerned then
                return a.EngineId > b.EngineId
            else
                return a.IsConcerned > b.IsConcerned
            end
        end)

        this.m_NpcList = CreateFromClass(MakeGenericClass(List, NpcInfo))

        for _, info in ipairs(npcList) do
            CommonDefs.ListAdd(this.m_NpcList, typeof(NpcInfo), info)
        end
    end
    if LuaHalloween2021Mgr:IsInYouHuaChePlay() then
        CLuaMiniMapPopWnd:OnUpdateHuaCheMapPos()
    end

    if CGuanNingMgr.Inst.inGnjc then
        CLuaMiniMapPopWnd:OnSendGnjcCounterattackNpcInfo(this)
    end
end

CMiniMapPopWnd.m_UpdateNPCPosInfo_CS2LuaHook = function (this, engineId, pos_x, pos_y)
    if CommonDefs.DictContains(this.m_NpcMapMarkList, typeof(UInt32), engineId) then
        local Pos3d = Utility.GridPos2WorldPos(pos_x, pos_y)
        CommonDefs.DictGetValue(this.m_NpcMapMarkList, typeof(UInt32), engineId).transform.localPosition = this:CalculateMapPos(Pos3d)
    end
end

CMiniMapPopWnd.m_UpdatePlayerInfos_CS2LuaHook = function (this, playerData)
    local team_list = nil
    local isWuJianDiYu = (CScene.MainScene.GamePlayDesignId == LuaWuJianDiYuMgr.gamePlayId)
    if(isWuJianDiYu)then
        team_list = CTeamMgr.Inst.Members
    end

    CommonDefs.DictIterate(this.m_PlayerMapMarkDict, DelegateFactory.Action_object_object(function (___key, ___value)
        local kv = {}
        kv.Key = ___key
        kv.Value = ___value
        CResourceMgr.Inst:Destroy(kv.Value.gameObject)
    end))
    CommonDefs.DictClear(this.m_PlayerMapMarkDict)
    CommonDefs.DictClear(this.m_PlayerInfos)

    if playerData ~= nil and playerData.Count > 0 then
        local category = CMiniMap.GetMinimapCategory()
        do
            local i = 0
            while i < playerData.Count do
                local continue
                repeat
                    local playerId = math.floor(tonumber(playerData[i] or 0))
                    local engineId = math.floor(tonumber(playerData[i + 1] or 0))
                    --跳过主角
                    if engineId == CClientMainPlayer.Inst.EngineId then
                        continue = true
                        break
                    end
                    local x = math.floor(tonumber(playerData[i + 2] or 0))
                    local z = math.floor(tonumber(playerData[i + 3] or 0))

                    local isTeamMember = CommonDefs.Convert_ToBoolean(playerData[i + 4])
                    local IsTeamLeader = CommonDefs.Convert_ToBoolean(playerData[i + 5])

                    local pos = CreateFromClass(CPos, x, z)
                    local info = CreateFromClass(PlayerInfo)
                    info.EngineId = engineId
                    info.PlayerId = playerId
                    info.Pos = pos
                    CommonDefs.DictAdd(this.m_PlayerInfos, typeof(UInt32), engineId, typeof(PlayerInfo), info)

                    local obj = CResourceMgr.Inst:Instantiate(CMiniMapPopWndNpcMarkResource.Inst, false, 10)
                    if obj ~= nil then
                        local Pos3d = Utility.GridPos2WorldPos(pos.x, pos.y)
                        obj.transform.parent = this.m_MapTexture.transform
                        obj.transform.localPosition = this:CalculateMapPos(Pos3d)
                        obj.transform.localRotation = Quaternion.identity
                        obj.transform.localScale = Vector3.one
                        obj.layer = this.gameObject.layer
                        --
                        if LuaSanxingGamePlayMgr.IsInPlay() then
                          local uiTexture = obj.transform:GetComponent(typeof(UISprite))
                          uiTexture.width = 24
                          uiTexture.height = 24
                        end
                        obj:SetActive(true)
                        local template = CommonDefs.GetComponent_GameObject_Type(obj, typeof(CMiniMapMarkTemplate))
                        template.EngineId = engineId
                        template.OnButtonSelected_02 = CommonDefs.CombineListner_Action_QnSelectableButton_bool(template.OnButtonSelected_02, MakeDelegateFromCSFunction(this.OnSelectMapMark, MakeGenericClass(Action2, QnSelectableButton, Boolean), this), true)
                        --根据势力进行划分
                        if CForcesMgr.Inst.m_PlayForceInfo == nil or not CommonDefs.DictContains(CForcesMgr.Inst.m_PlayForceInfo, typeof(UInt64), CClientMainPlayer.Inst.Id) then
                            template:UpdateView(category, EMinimapMarkType.OtherPlayer_Normal, "", "")
                            --在非势力情况下, 给队员,队长上个mark
                            if isTeamMember then
                                if IsTeamLeader then
                                    template:UpdateView(category, EMinimapMarkType.OtherPlayer_TeamLeader, "", "")
                                else
                                    template:UpdateView(category, EMinimapMarkType.OtherPlayer_TeamMember, "","")
                                end
                            end
                            if(isWuJianDiYu and team_list)then
                                for i = 0, team_list.Count - 1 do
                                    if(playerId == team_list[i].m_MemberId)then
                                        local teammate_label = template.transform:Find("NumberLabel"):GetComponent(typeof(UILabel))
                                        local teammate_bg = template.transform:Find("NumberBG"):GetComponent(typeof(UISprite))
                                        if(CLuaMiniMapPopWnd.NumberColors)then
                                            teammate_bg.color = NGUIText.ParseColor24(CLuaMiniMapPopWnd.NumberColors[i + 1], 0)--CLuaMiniMapPopWnd.NumberColors[i + 1]
                                        end
                                        teammate_label.text = tostring(i + 1)
                                        teammate_label.gameObject:SetActive(true)
                                        teammate_bg.gameObject:SetActive(true)
                                        template.gameObject:GetComponent(typeof(UISprite)).enabled = false
                                        break

                                    end
                                end
                            end
                        else
                            local mainPlayerForce = CommonDefs.DictGetValue(CForcesMgr.Inst.m_PlayForceInfo, typeof(UInt64), CClientMainPlayer.Inst.Id)
                            local curPlayerForce
                            if (function ()
                                local __try_get_result
                                __try_get_result, curPlayerForce = CommonDefs.DictTryGet(CForcesMgr.Inst.m_PlayForceInfo, typeof(UInt64), info.PlayerId, typeof(UInt32))
                                return __try_get_result
                            end)() then
                                if curPlayerForce == mainPlayerForce then
                                    if isTeamMember then
                                        if IsTeamLeader then
                                            template:UpdateView(category, EMinimapMarkType.OtherPlayer_TeamLeader, "", "")
                                        else
                                            template:UpdateView(category, EMinimapMarkType.OtherPlayer_TeamMember, "","")
                                        end
                                    else
                                        template:UpdateView(category, EMinimapMarkType.OtherPlayer_Friend, "", "")
                                    end
                                else
                                    template:UpdateView(category, EMinimapMarkType.OtherPlayer_Enemy, "", "")
                                end
                            else
                                template:UpdateView(category, EMinimapMarkType.OtherPlayer_Normal, "", "")
                            end
                        end
                        CommonDefs.DictAdd(this.m_PlayerMapMarkDict, typeof(UInt32), engineId, typeof(CMiniMapMarkTemplate), template)
                    end
                    continue = true
                until 1
                if not continue then
                    break
                end
                i = i + 6
            end
        end
    end
end
CMiniMapPopWnd.m_UpdateHuocheInfo_CS2LuaHook = function (this, playerData)
    if this.m_HuocheTemplate ~= nil then
        CResourceMgr.Inst:Destroy(this.m_HuocheTemplate)
    end

    if playerData ~= nil and playerData.Count > 0 then
        local category = CMiniMap.GetMinimapCategory()
        local engineId = math.floor(tonumber(playerData[0] or 0))
        local x = math.floor(tonumber(playerData[1] or 0))
        local z = math.floor(tonumber(playerData[2] or 0))
        local pos = CreateFromClass(CPos, x, z)
        this.m_HuocheTemplate = CResourceMgr.Inst:Instantiate(CMiniMapPopWndNpcMarkResource.Inst, false, 10)
        if this.m_HuocheTemplate ~= nil then
            local Pos3d = Utility.GridPos2WorldPos(pos.x, pos.y)
            this.m_HuocheTemplate.transform.parent = this.m_MapTexture.transform
            this.m_HuocheTemplate.transform.localPosition = this:CalculateMapPos(Pos3d)
            this.m_HuocheTemplate.transform.localRotation = Quaternion.identity
            this.m_HuocheTemplate.transform.localScale = Vector3.one
            this.m_HuocheTemplate.layer = this.gameObject.layer
            this.m_HuocheTemplate:SetActive(true)
            local template = CommonDefs.GetComponent_GameObject_Type(this.m_HuocheTemplate, typeof(CMiniMapMarkTemplate))
            template.EngineId = engineId
            template:UpdateView(category, EMinimapMarkType.Monster_Friend, LocalString.GetString("货车"), "")
        end
    end
end
CMiniMapPopWnd.m_UpdateWeekendFightItem_CS2LuaHook = function (this)
    if not CWeekendFightMgr.Inst.inFight then
      return
    end
    do
        local i = 0
        while i < this.saveWeekendFightNode.Count do
            if this.saveWeekendFightNode[i] ~= nil then
                CResourceMgr.Inst:Destroy(this.saveWeekendFightNode[i])
            end
            i = i + 1
        end
    end
    CommonDefs.ListClear(this.saveWeekendFightNode)
    local selfForceInfo = CWeekendFightMgr.Inst:GetSelfForceInfo()
    if CWeekendFightMgr.Inst.monsterState then
        local node = CResourceMgr.Inst:Instantiate(CMiniMapWeekendFightMonsterItemResource.Inst, false, 1)
        if node ~= nil then
            local Pos3d = Utility.GridPos2WorldPos(CWeekendFightMgr.Inst.monsterPos.x, CWeekendFightMgr.Inst.monsterPos.y)
            node.transform.parent = this.m_MapTexture.transform
            node.transform.localPosition = this:CalculateMapPos(Pos3d)
            node.transform.localRotation = Quaternion.identity
            node.transform.localScale = Vector3.one
            node.layer = this.gameObject.layer
            node:SetActive(true)
            --node.transform.FindChild("Label").GetComponent<UILabel>().text = CWeekendFightMgr.Inst.monsterName;
            CommonDefs.ListAdd(this.saveWeekendFightNode, typeof(GameObject), node)
        end
    end

    do
        local i = 0
        while i < CWeekendFightMgr.Inst.MiniMapShowItemList.Count do
            local info = CWeekendFightMgr.Inst.MiniMapShowItemList[i]
            local node = CResourceMgr.Inst:Instantiate(CMiniMapWeekendFightItemResource.Inst, false, 1)
            if node ~= nil then
                local Pos3d = Utility.GridPos2WorldPos(info.x, info.y)
                node.transform.parent = this.m_MapTexture.transform
                node.transform.localPosition = this:CalculateMapPos(Pos3d)
                node.transform.localRotation = Quaternion.identity
                node.transform.localScale = Vector3.one
                node.layer = this.gameObject.layer
                node:SetActive(true)

                local uiLabel = CommonDefs.GetComponentInChildren_GameObject_Type(node, typeof(UILabel))
                local uiFx = CommonDefs.GetComponentInChildren_GameObject_Type(node, typeof(CUIFx))

                if i < CWeekendFightMgr.MiniMapMustShowCount then
                    if uiLabel ~= nil then
                        uiLabel.gameObject:SetActive(true)
                        uiLabel.text = tostring(info.score)
                    end
                    if uiFx ~= nil then
                        uiFx:LoadFx("fx/ui/prefab/UI_shuijingguanghuan.prefab")
                    end
                else
                    if uiLabel ~= nil then
                        uiLabel.gameObject:SetActive(false)
                    end
                    --node.GetComponentInChildren<UILabel>().text = "";
                end

                --string[] picName = new string[] { "xzl_red", "xzl_blue" , "xzl_yellow" };

                if CommonDefs.DictContains(CWeekendFightMgr.Inst.SaveWeekendFightItemTypeIDDic, typeof(UInt32), info.engineId) then
                    local id = CommonDefs.DictGetValue(CWeekendFightMgr.Inst.SaveWeekendFightItemTypeIDDic, typeof(UInt32), info.engineId)
                    local xcInfo = XianZongShan_Crystal.GetData(id)
                    --if(xcInfo != null && picName.Length >= xcInfo.Type)
                    --{
                    --node.GetComponentInChildren<UISprite>().spriteName = picName[xcInfo.Type - 1];

                    CWeekendFightMgr.Inst:SetForceIcon(node.transform:Find("Sprite").gameObject, info.force)
                    --if (selfForceInfo != null && info.force != selfForceInfo.force)
                    --{
                    --    Color oldColor = node.GetComponentInChildren<UISprite>().color;
                    --    node.GetComponentInChildren<UISprite>().color = new Color(oldColor.r, oldColor.g, oldColor.b, 0.5f);
                    --}
                    --}
                end
                CommonDefs.ListAdd(this.saveWeekendFightNode, typeof(GameObject), node)
            end
            i = i + 1
        end
    end
end
CMiniMapPopWnd.m_NumberOfRows_CS2LuaHook = function (this, view)
    return this.m_NpcList.Count
end
CMiniMapPopWnd.m_ItemAt_CS2LuaHook = function (this, view, row)
    local item = TypeAs(view:GetFromPool(0), typeof(CMiniMapNpcItem))
    item:UpdateData(this.m_NpcList[row].Name, this.m_NpcList[row].PortaitName, this.m_NpcList[row].TitleIcon)
    return item
end
CMiniMapPopWnd.m_OnSelectAtRow_CS2LuaHook = function (this, row)
    local info = this.m_NpcList[row]
    CLuaMiniMapPopWnd:OnSelectNpc(info.EngineId)
end
CMiniMapPopWnd.m_UpdateMainPlayerPos_CS2LuaHook = function (this)

    if CClientMainPlayer.Inst == nil then
        return
    end
    local pos = Utility.PixelPos2GridPos(CClientMainPlayer.Inst.Pos)
    local worldPos = Utility.PixelPos2WorldPos(CClientMainPlayer.Inst.Pos)
    local playerMapPos = this:CalculateMapPos(worldPos)
    local isWuJianDiYu = (CScene.MainScene.GamePlayDesignId == LuaWuJianDiYuMgr.gamePlayId)
    this.m_MainPlayerMark.transform.localPosition = playerMapPos
    if(isWuJianDiYu and CLuaMiniMapPopWnd.m_WuJianDiYuMainPlayerNumberRoot)then
        --local extend_sprite = this.m_MainPlayerMark.transform:Find("extendedSprite").gameObject
        --extend_sprite:SetActive(true)
        CLuaMiniMapPopWnd.m_WuJianDiYuMainPlayerNumberRoot.transform.localPosition = playerMapPos
        local team_list = CTeamMgr.Inst.Members
        for i = 0, (team_list.Count - 1), 1 do
            if(team_list[i].m_MemberId == CClientMainPlayer.Inst.Id)then
                CLuaMiniMapPopWnd.m_WuJianDiYuMainPlayerBG.color = NGUIText.ParseColor24(CLuaMiniMapPopWnd.NumberColors[i + 1], 0)
                CLuaMiniMapPopWnd.m_WuJianDiYuMainPlayerNumber.text = tostring(i + 1)
                break
            end
        end

    end
    local dirPos = this:CalculateMapPos(CommonDefs.op_Addition_Vector3_Vector3(worldPos, CommonDefs.op_Multiply_Vector3_Single(CClientMainPlayer.Inst.RO.transform.forward, 100)))

    local viewdir = CommonDefs.op_Subtraction_Vector3_Vector3(dirPos, playerMapPos)
    if viewdir.magnitude > 0.1 then
        local angle = math.asin(viewdir.y / viewdir.magnitude) / math.pi * 180
        if viewdir.x < 0 then
            angle = 180 - angle
        end
        this.m_MainPlayerMark.transform.localEulerAngles = Vector3(0, 0, angle)
    end
    this:UpdateMapPath(CClientMainPlayer.Inst.Pos, CClientMainPlayer.Inst.EngineObject.EndPos, playerMapPos)
end


CMiniMapPopWnd.m_OnSelectMapMark_CS2LuaHook = function (this, button, selected)
    if selected then
        local template = TypeAs(button, typeof(CMiniMapMarkTemplate))
        local engineId = template.EngineId
        CLuaMiniMapPopWnd:OnSelectNpc(engineId)
        local lastTemplate
        if (function ()
            local __try_get_result
            __try_get_result, lastTemplate = CommonDefs.DictTryGet(this.m_NpcMapMarkList, typeof(UInt32), this.LastEngineId, typeof(CMiniMapMarkTemplate))
            return __try_get_result
        end)() then
            lastTemplate:SetSelected(false, false)
        end
        if (function ()
            local __try_get_result
            __try_get_result, lastTemplate = CommonDefs.DictTryGet(this.m_PlayerMapMarkDict, typeof(UInt32), this.LastEngineId, typeof(CMiniMapMarkTemplate))
            return __try_get_result
        end)() then
            lastTemplate:SetSelected(false, false)
        end
        if (function ()
            local __try_get_result
            __try_get_result, lastTemplate = CommonDefs.DictTryGet(this.m_QingQiuMarkDict, typeof(UInt32), this.LastEngineId, typeof(CMiniMapMarkTemplate))
            return __try_get_result
        end)() then
            lastTemplate:SetSelected(false, false)
        end
        this.LastEngineId = template.EngineId
    end
end

CMiniMapPopWnd.m_PlayClickFx_CS2LuaHook = function (this, pos)
    this.m_FxDianji.transform.localPosition = pos
    this.m_FxDianji:LoadFx("fx/ui/prefab/UI_ditudianji.prefab")

    if CLuaMiniMapPopWnd.m_EndPos == nil then
        CLuaMiniMapPopWnd.m_EndPos = pos
    elseif not CommonDefs.op_Equality_Vector3_Vector3(CLuaMiniMapPopWnd.m_EndPos, pos) then
        CLuaMiniMapPopWnd.m_EndPos = pos
        CLuaMiniMapPopWnd:ResetCurrentFindingNpc(nil)
    end
end

CMiniMapPopWnd.m_UpdateMapPath_CS2LuaHook = function (this, startPos, endPos, playerMapPos)

    local movePath = CClientMainPlayer.Inst.EngineObject.CurrentMovePath
    local wayPoint = CClientMainPlayer.Inst.EngineObject.WayPoint
    if movePath ~= nil then
        local listPos = CreateFromClass(MakeGenericClass(List, Vector3))
        do
            local i = wayPoint
            while i < movePath.PathSize do
                local tmp1 = i
                local tmp2 = 0
                local default
                default, tmp1, tmp2 = movePath:GetPixelPosAt(tmp1, tmp2)
                local pixelPos = default
                local worldPos = Utility.PixelPos2WorldPos(pixelPos)
                local mapPos = this:CalculateMapPos(worldPos)
                CommonDefs.ListAdd(listPos, typeof(Vector3), mapPos)
                i = i + 1
            end
        end
        if CPUBGMgr.Inst:IsInChiJi() then
            local endPos1 = Utility.PixelPos2WorldPos(CTrackMgr.Inst:GetCurSceneEndPos())
            local mapPos1 = this:CalculateMapPos(endPos1)
            CommonDefs.ListAdd(listPos, typeof(Vector3), mapPos1)

            this.m_PathDrawer:UpdatePlayerPath(listPos, playerMapPos)
            this:PlayClickFx(mapPos1)
        else
            this.m_PathDrawer:UpdatePlayerPath(listPos, playerMapPos)

            local endWorldPos = Utility.PixelPos2WorldPos(endPos)
            local endMapPos = this:CalculateMapPos(endWorldPos)
            this:PlayClickFx(endMapPos)
        end
    end
end
CMiniMapPopWnd.m_ClearMapPath_CS2LuaHook = function (this)
    this.m_FxDianji.gameObject:SetActive(false)
    this.m_PathDrawer:ClearCurrentPath()
end

CMiniMapPopWnd.m_UpdateWorldEventShow_CS2LuaHook = function (this)
    if CWorldEventMgr.Inst.worldMessageIdx > 0 then
        this.WorldEventBtn:SetActive(true)
		local trans = this.WorldEventBtn.transform

		local fx = trans:Find("FxAlert"):GetComponent(typeof(CUIFx));
		if CLuaWorldEventMgr.isShowRedPoint then
			LuaTweenUtils.DOKill(fx.transform, false)
			fx.transform.localPosition=Vector3(0,0,0)
			local b = NGUIMath.CalculateRelativeWidgetBounds(trans)
			local waypoints = CUICommonDef.GetWayPoints(Vector4(b.min.x, b.max.y, b.size.x, b.size.y), 1, true)
			fx.transform.localPosition = waypoints[0]
			fx:LoadFx("fx/ui/prefab/UI_kuang_yellow02.prefab")
			LuaTweenUtils.DOLuaLocalPath(fx.transform, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, - 1, Ease.Linear)
		else
			fx:DestroyFx()
			LuaTweenUtils.DOKill(fx.transform, false)
		end
		local iconfx = trans:Find("FxNode"):GetComponent(typeof(CUIFx))
		local icontx = CommonDefs.GetComponent_GameObject_Type(this.WorldEventBtn, typeof(CUITexture))
        local data = ShiJieShiJian_CronMessage2019.GetData(CWorldEventMgr.Inst.worldMessageIdx)
        if data ~= nil then
			iconfx:LoadFx(data.IconEffect)
			icontx:LoadMaterial(data.Icon)
		else
			iconfx:DestroyFx()
			icontx:Clear()
			this.WorldEventBtn:SetActive(false)
        end
    else
        this.WorldEventBtn:SetActive(false)
    end
end

CMiniMapPopWnd.m_WorldEevntIconClick_CS2LuaHook = function (this, go)
	CLuaWorldEventMgr.OpenWorldEventHisWnd()
end

CMiniMapPopWnd.m_InitWorldMapInfos_CS2LuaHook = function (this)
    if this.m_WorldMapInfoInited then
        return
    end
    this.m_WorldMapInfoInited = true
    CommonDefs.ListClear(this.m_WorldMapInfos)

    do
        local i = 0
        this.transform:Find("Contents/BigMap/MenPaiRoot").gameObject:SetActive(false)
        while i < this.m_WorldMapRoot.transform.childCount do
            local child = this.m_WorldMapRoot.transform:GetChild(i)
            local MenPaiRoot = child.transform:Find("MenPaiRoot")
            if MenPaiRoot then
                MenPaiRoot.gameObject:SetActive(false)
            end
            local info = CommonDefs.GetComponent_Component_Type(child, typeof(CWorldMapInfo))
            if info ~= nil then
                CommonDefs.ListAdd(this.m_WorldMapInfos, typeof(CWorldMapInfo), info)
                local data = PublicMap_PublicMap.GetData(info.TemplateId)
                if data ~= nil then
                    if data.LevelCanTeleport > 0 then
                        info.MinLevel = data.MinLevel
                        info.RecommandLevel = data.RecommandLevel
                    end
                end
            end
            i = i + 1
        end
    end
    local level = CClientMainPlayer.Inst.MaxLevel

    do
        local i = 0
        while i < this.m_WorldMapInfos.Count do
            if this.m_WorldMapInfos[i].MinLevel > level or this.m_WorldMapInfos[i].MinLevel < 0 then
                this.m_WorldMapInfos[i].Visible = false
            else
                local initSign = true
                if this.m_WorldMapInfos[i].TemplateId == 16000005 then
                    if CWorldEventMgr.Inst.showDestroyLanruosi then
                        if this.m_WorldMapInfos[i].gameObject.name == "lanruosi" then
                            this.m_WorldMapInfos[i].Visible = false
                            initSign = false
                        end
                    else
                        if this.m_WorldMapInfos[i].gameObject.name == "lanruosihot" then
                            this.m_WorldMapInfos[i].Visible = false
                            initSign = false
                        end
                    end
                end
                if this.m_WorldMapInfos[i].TemplateId == 16000499 then
                    local isShow = LuaYiRenSiMgr:CheckWorldMapEntrance()
                    this.m_WorldMapInfos[i].Visible = isShow
                    initSign = isShow
                end
                
                if initSign then
                    this.m_WorldMapInfos[i].Visible = true
                    local index = i
                    this.m_WorldMapInfos[index].m_WorldButton.OnClick = DelegateFactory.Action_QnButton(function (go)
                        local templateId = this.m_WorldMapInfos[index].TemplateId

                        if CClientMainPlayer.Inst.IsCasting and this.lastSelectMapTemplateId == templateId then
                            return
                        end

                        this.lastSelectMapTemplateId = templateId
                        CStatusMgr.Inst:ApplyInterrupt(CClientMainPlayer.Inst, EnumEvent.GetId("LevelTeleport"))
                        Gac2Gas.LevelTeleport(templateId)
                        this:Close()
                    end)
                end
            end
            i = i + 1
        end
    end

    this:InitMainPlayerTexturePos()

    UIEventListener.Get(this.GumuBtn.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.GumuBtn.gameObject).onClick, DelegateFactory.VoidDelegate(function (go)
        if CScene.MainScene.SceneTemplateId ~= WaBao_Setting.GetData().PublicCopyId then
            Gac2Gas.RequestEnterGumu(CTreasureMapMgr.Inst.gamePlayId)
        end
    end), true)
    this.GumuBtn.gameObject:SetActive(false)
    this.m_GumuFx:DestroyFx()
    this:UpdateWorldEventShow()

    if LuaZongMenMgr.m_SeverMapMapInfo then
        CLuaMiniMapPopWnd:OnSyncSectInWorldMap(LuaZongMenMgr.m_SeverMapMapInfo)
    end
end
CMiniMapPopWnd.m_OnGumuOpened_CS2LuaHook = function (this)

    this.GumuBtn.gameObject:SetActive(true)
    local b = NGUIMath.CalculateRelativeWidgetBounds(this.GumuBtn.transform)
    this.m_GumuFx:DestroyFx()
    this.m_GumuFx:LoadFx(CUIFxPaths.CommonYellowLineFxPath)
    LuaTweenUtils.DOKill(this.m_GumuFx.transform, false)
    local waypoints = CUICommonDef.GetWayPoints(Vector4(b.min.x, b.max.y, b.size.x, b.size.y), 1, false)
    this.m_GumuFx.transform.localPosition = waypoints[0]
    LuaTweenUtils.DOLuaLocalPath(this.m_GumuFx.transform, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, - 1, Ease.Linear)

    local info = CommonDefs.GetComponent_Component_Type(this.GumuBtn, typeof(CWorldMapInfo))
    info.TemplateId = WaBao_Setting.GetData().PublicCopyId
    info.RecommandLevel = 0
    CommonDefs.ListAdd(this.m_WorldMapInfos, typeof(CWorldMapInfo), info)
    this:InitMainPlayerTexturePos()
    if LuaZongMenMgr.m_SeverMapMapInfo then
        CLuaMiniMapPopWnd:OnSyncSectInWorldMap(LuaZongMenMgr.m_SeverMapMapInfo)
    end
end
CMiniMapPopWnd.m_InitMainPlayerTexturePos_CS2LuaHook = function (this)

    this.m_PlayerTextureInWorld.gameObject:SetActive(false)
    do
        local i = 0
        while i < this.m_WorldMapInfos.Count do
            if this.m_WorldMapInfos[i].gameObject.activeSelf and this.m_WorldMapInfos[i].TemplateId == CScene.MainScene.SceneTemplateId then
                this.m_PlayerTextureInWorld.transform.parent = this.m_WorldMapInfos[i].m_WorldButton.transform
                this.m_PlayerTextureInWorld.transform.localPosition = CMiniMapPopWnd.PLAYER_POS
                this.m_PlayerTextureInWorld.gameObject:SetActive(true)
                --初始化主角贴图
                this.m_PlayerTextureInWorld:LoadNPCPortrait(CClientMainPlayer.Inst.PortraitName, false)
                break
            end
            i = i + 1
        end
    end
end
CMiniMapPopWnd.m_InitTeleport_CS2LuaHook = function (this)
    CommonDefs.ListIterate(this.m_TeleportList, DelegateFactory.Action_object(function (___value)
        local kv = ___value
        CResourceMgr.Inst:Destroy(kv)
    end))
    CommonDefs.ListClear(this.m_TeleportList)
    local sectView = this.transform:Find('Contents/BigMap/CurrentMap/Map/SectView').gameObject
    sectView:SetActive(false)
    if CScene.MainScene ~= nil and CScene.MainScene.ShowTeleportInMiniMapPopWnd > 0 then
        local trapList = CRenderScene.Inst:GetExtraTrapPos()
        if trapList ~= nil then
            do
                local i = 0
                while i < trapList.Count do
                    local p = trapList[i]
                    local Pos3d = Utility.GridPos2WorldPos(p.x, p.y)
                    local obj = CResourceMgr.Inst:Instantiate(CMiniMapPopWndTeleportResource.Inst, false, 2)
                    obj.transform.parent = this.m_MapTexture.transform
                    obj.transform.localPosition = this:CalculateMapPos(Pos3d)
                    obj.transform.localRotation = Quaternion.identity
                    obj.transform.localScale = Vector3.one
                    obj.layer = this.gameObject.layer
                    obj:SetActive(true)
                    local label = CommonDefs.GetComponent_GameObject_Type(obj, typeof(UILabel))
                    if CScene.MainScene.ShowTeleportInMiniMapPopWnd == 1 then
                        label.text = ""
                    end
                    if CScene.MainScene.ShowTeleportInMiniMapPopWnd == 2 then
                        label.text = LocalString.GetString("出口")
                    end
                    CommonDefs.ListAdd(this.m_TeleportList, typeof(GameObject), obj)
                    i = i + 1
                end
            end
        end

        CLuaMiniMapPopWnd:InitSectTrap(this)

        if CScene.MainScene.Teleport ~= nil then
            do
                local i = 0
                while i < CScene.MainScene.Teleport.Length do
                    local Pos3d = Utility.GridPos2WorldPos(CScene.MainScene.Teleport[i].SrcX, CScene.MainScene.Teleport[i].SrcY)
                    local obj = CResourceMgr.Inst:Instantiate(CMiniMapPopWndTeleportResource.Inst, false, 2)
                    obj.transform.parent = this.m_MapTexture.transform
                    obj.transform.localPosition = this:CalculateMapPos(Pos3d)
                    obj.transform.localRotation = Quaternion.identity
                    obj.transform.localScale = Vector3.one
                    obj.layer = this.gameObject.layer
                    obj:SetActive(true)

                    local label = CommonDefs.GetComponent_GameObject_Type(obj, typeof(UILabel))
                    if CScene.MainScene.ShowTeleportInMiniMapPopWnd == 1 then
                        label.text = ""
                    end
                    if CScene.MainScene.ShowTeleportInMiniMapPopWnd == 2 then
                        label.text = LocalString.GetString("出口")
                    elseif CScene.MainScene.ShowTeleportInMiniMapPopWnd == 3 then
                        local destSceneName = ""
                        local destScene = PublicMap_PublicMap.GetData(CScene.MainScene.Teleport[i].DestSceneId)
                        if destScene ~= nil then
                            destSceneName = destScene.Name
                        end
                        label.text = destSceneName
                    end
                    CommonDefs.ListAdd(this.m_TeleportList, typeof(GameObject), obj)
                    i = i + 1
                end
            end
        end
    end
end
CMiniMapPopWnd.m_EnableCircleClip_CS2LuaHook = function (ClipCenterX, ClipCenterZ, ClipRadius)
    CMiniMapPopWnd.m_isCircleClipping = true
    CMiniMapPopWnd.m_ClipCenterX = ClipCenterX
    CMiniMapPopWnd.m_ClipCenterZ = ClipCenterZ
    CMiniMapPopWnd.m_ClipRadius = ClipRadius
end
CMiniMapPopWnd.m_DisableCircleClip_CS2LuaHook = function ()
    CMiniMapPopWnd.m_isCircleClipping = false
    CMiniMapPopWnd.m_ClipCenterZ = 0 CMiniMapPopWnd.m_ClipCenterX = CMiniMapPopWnd.m_ClipCenterZ
    CMiniMapPopWnd.m_ClipRadius = 0
end
CMiniMapPopWnd.m_DoCircleClip_CS2LuaHook = function (this, ClipCenterX, ClipCenterZ, ClipRadius)
    local CenterInWorld = Utility.GridPos2WorldPos(ClipCenterX, ClipCenterZ)
    local PointOnCircleInWorld = Vector3(CenterInWorld.x + ClipRadius, CenterInWorld.y, CenterInWorld.z)

    local CenterInMap = this:CalculateMapPos(CenterInWorld)
    local PointOnCircleInMap = this:CalculateMapPos(PointOnCircleInWorld)
    local RadiusInMap = (CommonDefs.op_Subtraction_Vector3_Vector3(PointOnCircleInMap, CenterInMap)).magnitude

    local center = Vector2(CenterInMap.x, CenterInMap.y)
    local panel = CommonDefs.GetComponent_GameObject_Type(this.m_CurrentMapRoot, typeof(UIPanel))
    panel.clipping = UIDrawCall.Clipping.TextureMask
    panel.clipTexture = this.m_MaskTexture
    panel.baseClipRegion = Vector4(center.x, center.y, RadiusInMap * 2, RadiusInMap * 2)
    local mapTexturePos = this.m_MapTexture.transform.localPosition
    panel.clipOffset = Vector2(mapTexturePos.x,mapTexturePos.y)
    --this.m_MapTexture.material = this.m_CircleClipMat
    this.m_ClipBackground.gameObject:SetActive(true)
end
CMiniMapPopWnd.m_CancleCircleClip_CS2LuaHook = function (this)
    local panel = CommonDefs.GetComponent_GameObject_Type(this.m_CurrentMapRoot, typeof(UIPanel))
    panel.clipping = UIDrawCall.Clipping.None
     --this.m_MapTexture.material = nil
    this.m_ClipBackground.gameObject:SetActive(false)
end
CMiniMapPopWnd.m_SetCircleClip_CS2LuaHook = function (this)
    if CMiniMapPopWnd.m_isCircleClipping then
        this:DoCircleClip(CMiniMapPopWnd.m_ClipCenterX, CMiniMapPopWnd.m_ClipCenterZ, CMiniMapPopWnd.m_ClipRadius)
    else
        this:CancleCircleClip()
    end
end
CMiniMapPopWnd.m_RandomMapCalculateMapPos_CS2LuaHook = function (this, worldPos)
    local res = Utility.AffineTransform(CMiniMap.Instance.mGridPos2PixelPosMat, Vector2(worldPos.x, worldPos.z))
    local ret = Vector3((res.x / CMiniMap.sRandomMapMinimapResolutionX - 0.5) * this.m_MapTexture.width, (res.y / CMiniMap.sRandomMapMinimapResolutionZ - 0.5) * this.m_MapTexture.height, 0)
    return ret
end
CMiniMapPopWnd.m_InitZhuangshiwuInfo_CS2LuaHook = function (this)

  if 0 == CMiniMapPopWnd.m_ZhuangshiwuTemplateId then
    this.m_ZhuangshiwuInfoRoot:SetActive(false)
    return
  end
  this.m_ZhuangshiwuInfoRoot:SetActive(true)
  this.WorldEventBtn:SetActive(false)--在家园显示装饰物的时候不显示世界事件图标
  UIEventListener.Get(this.m_PackAllZhuangshiwuBtn).onClick = MakeDelegateFromCSFunction(this.OnPackZhuangshiwuBtnClick, VoidDelegate, this)
  local data = Zhuangshiwu_Zhuangshiwu.GetData(CMiniMapPopWnd.m_ZhuangshiwuTemplateId)
  if data ~= nil then
    this.m_ZhuangshiwuName.text = data.Name
  end
  local placedCnt = CLuaClientFurnitureMgr.GetPlacedFurnitureCountByTemplateId(CMiniMapPopWnd.m_ZhuangshiwuTemplateId)
  local count = CClientFurnitureMgr.Inst:GetRepositoryFurnitureCount(CMiniMapPopWnd.m_ZhuangshiwuTemplateId)
  this.m_ZhuangshiwuCount.text = System.String.Format(LocalString.GetString("已摆放 {0}/{1}"), placedCnt, placedCnt + count)
  local delta = CommonDefs.StringLength(data.Name) > 4 and (CommonDefs.StringLength(data.Name) - 4) * 35 or 0
  Extensions.SetLocalPositionX(this.m_LeftInfoRoot.transform, 20 - delta)
  this.m_ZhuangshiwuName.color = CItem.GetColor(data.ItemId)
end
CMiniMapPopWnd.m_OnPackZhuangshiwuBtnClick_CS2LuaHook = function (this, go)
    local data = Zhuangshiwu_Zhuangshiwu.GetData(CMiniMapPopWnd.m_ZhuangshiwuTemplateId)
    if nil == data then
        return
    end
    local text = g_MessageMgr:FormatMessage("Pack_All_Furniture_Msg", data.Name)
    MessageWndManager.ShowOKCancelMessage(text, DelegateFactory.Action(function ()
        CLuaClientFurnitureMgr.PackAllFurnitureByTemplateId(CMiniMapPopWnd.m_ZhuangshiwuTemplateId)
        this:Close()
    end), nil, nil, nil, false)
end
CMiniMapPopWnd.m_UpdateBabyNPCPos_CS2LuaHook = function (this, list)
    if not CClientHouseMgr.Inst:IsPlayerInHouse() then
        return
    end
    CommonDefs.DictIterate(this.m_BabyNPCMapMarkDict, DelegateFactory.Action_object_object(function (___key, ___value)
        local kv = {}
        kv.Key = ___key
        kv.Value = ___value
        if kv.Value ~= nil then
            CResourceMgr.Inst:Destroy(kv.Value.gameObject)
        end
    end))
    CommonDefs.DictClear(this.m_BabyNPCMapMarkDict)

    do
        local i = 0
        while i < list.Count do
            local info = list[i]
            local obj = CResourceMgr.Inst:Instantiate(CMiniMapPopWndNpcMarkResource.Inst, false, 10)
            if obj ~= nil then
                local pos3d = Utility.GridPos2WorldPos(info.posX, info.posY)
                obj.transform.parent = this.m_MapTexture.transform
                obj.transform.localPosition = this:CalculateMapPos(pos3d)
                obj.transform.localRotation = Quaternion.identity
                obj.transform.localScale = Vector3.one
                obj.layer = this.gameObject.layer
                obj:SetActive(true)
                local template = CommonDefs.GetComponent_GameObject_Type(obj, typeof(CMiniMapMarkTemplate))
                template.EngineId = info.engineId
                template:UpdateView(EMinimapCategory.Normal, EMinimapMarkType.Npc_Concerned, info.name, "")
                CommonDefs.DictAdd(this.m_BabyNPCMapMarkDict, typeof(UInt32), info.engineId, typeof(CMiniMapMarkTemplate), template)
            end
            i = i + 1
        end
    end
end

CMiniMapPopWnd.m_hookCheckQuickTransfer = function (this, nguiPos, pixelPos)
    if not this:ContainsQuickTransferBuff() then return false end
    local quickTransferRoot = this.m_CurrentMapRoot.transform:Find("Map/QuickTransfer")
    if quickTransferRoot == nil then return false end
    quickTransferRoot.localPosition = Vector3(nguiPos.x, nguiPos.y, 0)
    quickTransferRoot.gameObject:SetActive(true)
    local flashButton = quickTransferRoot.transform:Find("FlashButton").gameObject
    local gridPos = Utility.PixelPos2GridPos(pixelPos)
    UIEventListener.Get(flashButton).onClick = DelegateFactory.VoidDelegate(function()

        Gac2Gas.Watch_FlashInScene(gridPos.x, gridPos.y)
        quickTransferRoot.gameObject:SetActive(false)
        CUIManager.CloseUI(CUIResources.MiniMapPopWnd) --传送成功关闭大地图，考虑到传送会就近选择可行走区域，基本都会成功，根据策划交互的沟通这里直接关闭
    end)
    return true
end

----------------------------------------------------------------------------------------------------
--todo:转lua，下面是暂时的解决方案
----------------------------------------------------------------------------------------------------

local Pick_Pick=import "L10.Game.Pick_Pick"
CLuaMiniMapPopWnd = {}
CLuaMiniMapPopWnd.m_YarkObjs = {}
CLuaMiniMapPopWnd.m_MGJPlayerInfos = {}
CLuaMiniMapPopWnd.m_AnQiDaoObjInfos = {}
CLuaMiniMapPopWnd.m_CommonMarks = {}
CLuaMiniMapPopWnd.m_SoulCoreObjInfo = nil
CLuaMiniMapPopWnd.m_Wnd = nil
CLuaMiniMapPopWnd.m_WuJianDiYuBossIcons = nil
CLuaMiniMapPopWnd.m_WuJianDiYuBossIconTemplates = nil
CLuaMiniMapPopWnd.m_WuJianDiYuMainPlayerNumberRoot = nil
CLuaMiniMapPopWnd.m_WuJianDiYuMainPlayerNumber = nil
CLuaMiniMapPopWnd.m_WuJianDiYuMainPlayerBG = nil
CLuaMiniMapPopWnd.ExtIcons = {}
CLuaMiniMapPopWnd.NumberColors = nil
CLuaMiniMapPopWnd.m_AddPinBtn = nil
CLuaMiniMapPopWnd.m_RemovePinBtn = nil
CLuaMiniMapPopWnd.m_BackPinBtn = nil
CLuaMiniMapPopWnd.m_PinCollider = nil
CLuaMiniMapPopWnd.m_PinIcons = nil
CLuaMiniMapPopWnd.m_MainPlayerPinIcon = nil
CLuaMiniMapPopWnd.m_TablePanel = nil
CLuaMiniMapPopWnd.m_PinSystemEnabled = false

CLuaMiniMapPopWnd.m_ShowWorldMap =false

CLuaMiniMapPopWnd.m_AnQiDaoPickName = nil
CLuaMiniMapPopWnd.m_AnQiDaoBossName = nil
CLuaMiniMapPopWnd.m_BackToMenPaiButton = nil
CLuaMiniMapPopWnd.m_EnemyMenPaiButton = nil
CLuaMiniMapPopWnd.m_ZongMenButtonInited = false
CLuaMiniMapPopWnd.m_IsShowGuanNingSundayView = false
CLuaMiniMapPopWnd.m_GuanNingSundayView = nil

CLuaMiniMapPopWnd.m_GuildLeaguePlayerPosMapMarkTbl = nil
CLuaMiniMapPopWnd.m_GuildLeagueTowerPosMapMarkTbl = nil
CLuaMiniMapPopWnd.m_GuildLeagueTowerIds = nil
CLuaMiniMapPopWnd.m_JXYSPosMapMarkTbl = nil
CLuaMiniMapPopWnd.m_CurrentFindNpc = nil
CLuaMiniMapPopWnd.m_NpcData = nil
CLuaMiniMapPopWnd.m_EndPos = nil
CLuaMiniMapPopWnd.m_CurrentTrackingNpcTemplateId = nil

function CLuaMiniMapPopWnd:Init(this)
    self.m_Wnd = this
    self.m_YarkObjs = {}
    self.m_MGJPlayerInfos = {}
    self.m_AnQiDaoObjInfos = {}
    self.m_SoulCoreObjInfo = nil
    self.m_CommonMarks = {}
    self.m_EndPos = nil
    self.m_TablePanel = self.m_Wnd.transform:Find("Contents/BigMap/TablePanel").gameObject

    self:OnSyncYarkPick(g_YarkPick)
    self:InitHellImprismIcons()
    self.m_EnemyMenPaiButton = nil
    self.m_CurrentTrackingNpcTemplateId = CTrackMgr.Inst.m_TrackingNpcTemplateId

    CLuaMiniMapPopWnd.ExtIcons = {}
    g_ScriptEvent:AddListener("SyncExtPos", self, "OnSyncExtPos")
    g_ScriptEvent:AddListener("OnSyncSectInWorldMap", self, "OnSyncSectInWorldMap")
    g_ScriptEvent:AddListener("OnLeaveZongMen", self, "OnLeaveZongMen")
    g_ScriptEvent:AddListener("SyncGnjcZhiHuiFaZhenPosition", self, "OnSyncGnjcZhiHuiFaZhenPosition")
    g_ScriptEvent:AddListener("SFEQ_XW_SyncTargetPos", self, "OnSyncQiXi2022TargetPos")
    g_ScriptEvent:AddListener("QueryMonsterPosResultNew", self, "OnSyncMonsterPosResultNew")

    local sanxingNode = this.transform:Find('Contents/BigMap/TablePanel/SanxingGamePanel').gameObject
    if LuaSanxingGamePlayMgr.IsInPlay() then
      sanxingNode:SetActive(true)
      local sanxingTipBtn = sanxingNode.transform:Find('TipButton').gameObject
      local onTipClick = function(go)
        CUIManager.ShowUI(CLuaUIResources.SanxingTipWnd)
      end
      CommonDefs.AddOnClickListener(sanxingTipBtn,DelegateFactory.Action_GameObject(onTipClick),false)
        if (LuaSanxingGamePlayMgr.InGuidePha1 and not LuaSanxingGamePlayMgr.InGuidePha1_2) or (LuaSanxingGamePlayMgr.InGuidePha2 and not LuaSanxingGamePlayMgr.InGuidePha2_2) then
          CUIManager.ShowUI(CLuaUIResources.SanxingGuideWnd)
        end
    else
      sanxingNode:SetActive(false)
      --触发npc过滤引导
      CLuaGuideMgr.TryTriggerNpcFilterGuide()
    end

    -- 提前请求宗派数据
    LuaZongMenMgr.m_SeverMapMapInfo = nil
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.HasZongMen then
        Gac2Gas.QuerySectInWorldMap(CClientMainPlayer.Inst.BasicProp.SectId)
    end

    self:TryShowMainPlayerSoulCorePos()

    self.m_GuanNingSundayView = this.transform:Find('Contents/BigMap/CurrentMap/Map/GuanNingSundayView').gameObject
    self.m_GuanNingSundayView:SetActive(false)
    self:InitHuluwaBoss()
    self:InitGuildLeagueTowerTemplate()
    self:InitQiXi2022Map(this)
    self:InitGuildCustomPreTask1()

    LuaGamePlayMgr:RefreshMiniMapPopMarks()
end

function CLuaMiniMapPopWnd:InitQiXi2022Map(this)
    local root = this.transform:Find('Contents/BigMap/CurrentMap/Map/QiXi2022Target')
    if root then
        root.gameObject:SetActive(false)
    end
end

function CLuaMiniMapPopWnd:OnSyncQiXi2022TargetPos(targetX, targetY,show)
    local this = self.m_Wnd
    local worldPos = Utility.GridPos2WorldPos(targetX, targetY)
    local playerMapPos = this:CalculateMapPos(worldPos)
    local root = this.transform:Find('Contents/BigMap/CurrentMap/Map/QiXi2022Target')
    if root then
        root.gameObject:SetActive(show)
        root.transform.localPosition = playerMapPos
    end
end

function CLuaMiniMapPopWnd:OnLeaveZongMen()
    local this =  self.m_Wnd
    this.transform:Find("Contents/BigMap/MenPaiRoot").gameObject:SetActive(false)
    for i = 0, this.m_WorldMapRoot.transform.childCount - 1 do
        local child = this.m_WorldMapRoot.transform:GetChild(i)
        local MenPaiRoot = child.transform:Find("MenPaiRoot").gameObject
        if MenPaiRoot then
            MenPaiRoot:SetActive(false)
        end
    end
end

function CLuaMiniMapPopWnd:OnSyncGnjcZhiHuiFaZhenPosition(x1, y1, x2, y2) -- 分别是指挥和法阵的位置
    if x1 and y1 then
        if not CLuaGuanNingMgr.m_CommanderPopMapIcon[1] then
            local icon = { GuanNing_Setting.GetData().CommandIconResourceBlue, GuanNing_Setting.GetData().CommandIconResourceRed }
            local go = CResourceMgr.Inst:Instantiate(CMiniMapCommonItemNodeResource.Inst, false, 1)
            go.transform.parent = self.m_Wnd.m_MapTexture.transform
            go.transform.localRotation = Quaternion.identity
            go.transform.localScale = Vector3.one
            go.layer = self.m_Wnd.gameObject.layer
            local i = CLuaGuanNingMgr:GetMyForce() + 1
            if icon[i] then
                go.transform:Find("icon"):GetComponent(typeof(CUITexture)):LoadMaterial(icon[i])
                local uiTexture = go.transform:Find("icon"):GetComponent(typeof(UITexture))
                uiTexture.width = 64
                uiTexture.height = 64
            end
            go:SetActive(false)
            CLuaGuanNingMgr.m_CommanderPopMapIcon[1] = go
        end
        if x1 == 0 and y1 == 0 then
            CLuaGuanNingMgr.m_CommanderPopMapIcon[1]:SetActive(false)
        else
            local Pos3d = Utility.GridPos2WorldPos(x1, y1)
            local lcPos = self.m_Wnd:CalculateMapPos(Pos3d)
            CLuaGuanNingMgr.m_CommanderPopMapIcon[1].transform.localPosition = lcPos
            CLuaGuanNingMgr.m_CommanderPopMapIcon[1].transform.localRotation = Quaternion.identity
            CLuaGuanNingMgr.m_CommanderPopMapIcon[1].transform.localScale = Vector3.one
            CLuaGuanNingMgr.m_CommanderPopMapIcon[1]:SetActive(true)
        end
    end
    if x2 and y2 then
        if not CLuaGuanNingMgr.m_CommanderPopMapIcon[2] then
            local go = CResourceMgr.Inst:Instantiate(CMiniMapCommonItemNodeResource.Inst, false, 1)
            go.transform.parent = self.m_Wnd.m_MapTexture.transform
            go.transform.localRotation = Quaternion.identity
            go.transform.localScale = Vector3.one
            go.layer = self.m_Wnd.gameObject.layer
            go.transform:Find("icon"):GetComponent(typeof(CUITexture)):LoadMaterial("UI/Texture/Transparent/Material/guanningcommandwnd_zengyifazhen.mat")
            local uiTexture = go.transform:Find("icon"):GetComponent(typeof(UITexture))
            uiTexture.width = 64
            uiTexture.height = 64
            go:SetActive(false)
            CLuaGuanNingMgr.m_CommanderPopMapIcon[2] = go
        end
        if x2 == 0 and y2 == 0 then
            CLuaGuanNingMgr.m_CommanderPopMapIcon[2]:SetActive(false)
        else
            local Pos3d = Utility.GridPos2WorldPos(x2, y2)
            local lcPos = self.m_Wnd:CalculateMapPos(Pos3d)
            CLuaGuanNingMgr.m_CommanderPopMapIcon[2].transform.localPosition = lcPos
            CLuaGuanNingMgr.m_CommanderPopMapIcon[2].transform.localRotation = Quaternion.identity
            CLuaGuanNingMgr.m_CommanderPopMapIcon[2].transform.localScale = Vector3.one
            CLuaGuanNingMgr.m_CommanderPopMapIcon[2]:SetActive(true)
        end
    end
end

function CLuaMiniMapPopWnd:OnSendGnjcCounterattackNpcInfo(this)
    if not this or type(this) == "number" then return end
    local func = function (idx, x, y)
        if CLuaGuanNingMgr.m_CounterattackPopMapIcon[idx] then 
            CResourceMgr.Inst:Destroy(CLuaGuanNingMgr.m_CounterattackPopMapIcon[idx])
            CLuaGuanNingMgr.m_CounterattackPopMapIcon[idx] = nil
        end
        local go = CResourceMgr.Inst:Instantiate(CMiniMapCommonItemNodeResource.Inst, false, 1)
        go:SetActive(true)
        go.transform.parent = self.m_Wnd.m_MapTexture.transform
        go.transform.localRotation = Quaternion.identity
        go.transform.localScale = Vector3.one
        go.layer = self.m_Wnd.gameObject.layer

        if CGuideMgr.Inst:IsInPhase(EnumGuideKey.GuanNingCounterattackWeak) or CGuideMgr.Inst:IsInPhase(EnumGuideKey.GuanNingCounterattackStrong) then
            if idx == 1 or idx == 2 and not CLuaGuanNingMgr.m_CounterattackPopMapIcon[1] then
                local box = go:AddComponent(typeof(BoxCollider))
                box.size = Vector3(48, 48, 0)
                UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function (p)
                    if not CGuideMgr.Inst:IsInPhase(EnumGuideKey.GuanNingCounterattackWeak) and not CGuideMgr.Inst:IsInPhase(EnumGuideKey.GuanNingCounterattackStrong) then
                        box.enabled = false
                        CLuaGuanNingMgr.m_InCounterattackGuide = false
                    end
                end)
            end
        end
        
        local icon = { "UI/Texture/Transparent/Material/minimap_haojiao.mat", "UI/Texture/Transparent/Material/minimap_zhangu.mat" }
        go.transform:Find("icon"):GetComponent(typeof(CUITexture)):LoadMaterial(icon[idx])
        local uiTexture = go.transform:Find("icon"):GetComponent(typeof(UITexture))
        uiTexture.width = 48
        uiTexture.height = 48
        
        local Pos3d = Utility.GridPos2WorldPos(x, y)
        local lcPos = self.m_Wnd:CalculateMapPos(Pos3d)
        go.transform.localPosition = lcPos
        go.transform.localRotation = Quaternion.identity
        go.transform.localScale = Vector3.one

        CLuaGuanNingMgr.m_CounterattackPopMapIcon[idx] = go
    end

    local val1 = false
    local val2 = false
    CommonDefs.DictIterate(this.m_NpcInfos, DelegateFactory.Action_object_object(function(key,val)
        if val.TemplateId == 20007138 then -- 号角
            val1 = val
        elseif val.TemplateId == 20007139 then -- 战鼓
            val2 = val
        end
    end))
    if val1 then
        func(1, val1.Pos.x, val1.Pos.y)
    end
    if val2 then
        func(2, val2.Pos.x, val2.Pos.y) 
    end
end

function CLuaMiniMapPopWnd:OnSyncSectInWorldMap(mapMap)
    local this =  self.m_Wnd
    if not this.m_WorldMapRoot or not this.m_WorldMapRoot.activeSelf or mapMap == nil then
        return
    end
    CLuaMiniMapPopWnd.m_EnemyMenPaiButton = nil
    CLuaMiniMapPopWnd.m_ZongMenButtonInited = true

    local tempMenPaiRoot = this.transform:Find("Contents/BigMap/MenPaiRoot").gameObject
    local MenPaiContainer = this.transform:Find("Contents/BigMap/MenPaiContainer").gameObject
    CUICommonDef.ClearTransform(MenPaiContainer.transform)

    local mapData = Menpai_Setting.GetData("ZongMenJointlyMapId").Value
    local except = {}
    local exceptMap = {}

    local mapList = g_LuaUtil:StrSplit(mapData,";")
    for i=1, #mapList do
        local m2m = g_LuaUtil:StrSplit(mapList[i],",")
        local m1 = tonumber(m2m[1])
        local m2 = tonumber(m2m[2])

        if m1 and m2 then
            except[m1] = 1
            exceptMap[m2] = m1
        end
    end

    local tempMap = {}
    local info = CommonDefs.GetComponent_Component_Type(this.GumuBtn, typeof(CWorldMapInfo))
    info.TemplateId = WaBao_Setting.GetData().PublicCopyId
    for i = 0, this.m_WorldMapRoot.transform.childCount - 1 do
        local child = this.m_WorldMapRoot.transform:GetChild(i)
        local info = CommonDefs.GetComponent_Component_Type(child, typeof(CWorldMapInfo))
        local mapid = info and info.TemplateId or 0
        
        local otherMapid = exceptMap[mapid]
        if info and ((mapMap[mapid] ~=  nil and except[mapid] == nil) or (otherMapid~=nil and mapMap[otherMapid]~=nil)) and not tempMap[mapid] then

            local MenPaiRoot = child.transform:Find("MenPaiRoot").gameObject
            MenPaiRoot:SetActive(false)
            MenPaiContainer:SetActive(true)
            local temp = CommonDefs.Object_Instantiate(tempMenPaiRoot)
            temp.transform.parent = MenPaiContainer.transform
            temp.transform.localScale = Vector3.one
            temp.transform.position = MenPaiRoot.transform.position
            temp.transform:Find("SelfBtn").localPosition = MenPaiRoot.transform:Find("SelfBtn").localPosition
            temp.transform:Find("EnemyBtn").localPosition = MenPaiRoot.transform:Find("EnemyBtn").localPosition
            temp.transform:Find("SelfMixedBtn").localPosition = MenPaiRoot.transform:Find("SelfMixedBtn").localPosition
            temp.transform:Find("EnemyMixedBtn").localPosition = MenPaiRoot.transform:Find("EnemyMixedBtn").localPosition
            MenPaiRoot = temp
            
            if MenPaiRoot then
                tempMap[mapid] = true
                MenPaiRoot:SetActive(true)
                CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.GoToZongMen)
                -- 状态 (1只有1个自己宗门,2只有1个敌对宗门，3多个且含自己，4多个不含自己)
                local selfBtnType = mapMap[mapid] or 0
                local otherBtnType = mapMap[otherMapid] or 0
                local doubleMark = mapMap[mapid] and mapMap[otherMapid]

                local btnType = selfBtnType + otherBtnType

                local selfBtn = MenPaiRoot.transform:Find("SelfBtn").gameObject
                local enemyBtn = MenPaiRoot.transform:Find("EnemyBtn").gameObject
                local selfMixedBtn = MenPaiRoot.transform:Find("SelfMixedBtn").gameObject
                local enemyMixedBtn = MenPaiRoot.transform:Find("EnemyMixedBtn").gameObject

                selfBtn:SetActive(btnType == 1)
                enemyBtn:SetActive(btnType == 2)
                selfMixedBtn:SetActive(btnType == 3 or btnType == 7 or btnType == 5)
                enemyMixedBtn:SetActive(btnType == 4 or btnType == 6 or btnType == 8)

                if btnType == 1 then
                    UIEventListener.Get(selfBtn).onClick = DelegateFactory.VoidDelegate(function (p)
                        Gac2Gas.RequestTrackToSectEntrance()
                        CUIManager.CloseUI("MiniMapPopWnd")
                    end)
                    CLuaMiniMapPopWnd.m_BackToMenPaiButton = selfBtn
                elseif btnType == 2 then
                    UIEventListener.Get(enemyBtn).onClick = DelegateFactory.VoidDelegate(function (p)
                        LuaZongMenMgr:QuerySectWithMapId(mapid, otherMapid)
                    end)
                    CLuaMiniMapPopWnd.m_EnemyMenPaiButton = enemyBtn
                elseif btnType == 3 or btnType == 7 or btnType == 5 then
                    UIEventListener.Get(selfMixedBtn).onClick = DelegateFactory.VoidDelegate(function (p)
                        LuaZongMenMgr:QuerySectWithMapId(mapid, otherMapid)
                    end)
                    CLuaMiniMapPopWnd.m_BackToMenPaiButton = selfMixedBtn
                    CLuaMiniMapPopWnd.m_EnemyMenPaiButton = selfMixedBtn
                elseif btnType == 4 or btnType == 6 or btnType == 8 then
                    UIEventListener.Get(enemyMixedBtn).onClick = DelegateFactory.VoidDelegate(function (p)
                        LuaZongMenMgr:QuerySectWithMapId(mapid, otherMapid)
                    end)
                    CLuaMiniMapPopWnd.m_EnemyMenPaiButton = enemyMixedBtn
                end
            end
        end
    end
end

function CLuaMiniMapPopWnd:OnSelectNpc(engineId)
    local this = self.m_Wnd
    if CommonDefs.DictContains(this.m_NpcInfos, typeof(UInt32), engineId) then
        local pixelPos = Utility.GridPos2PixelPos(CommonDefs.DictGetValue(this.m_NpcInfos, typeof(UInt32), engineId).Pos)
        local onSuccess = DelegateFactory.Action(function ()
            local npc = TypeAs(CClientObjectMgr.Inst:GetObject(engineId), typeof(CClientNpc))
            if npc ~= nil then
                npc:OnSelected()
            end
            CLuaMiniMapPopWnd.m_EndPos = nil
            self:ResetCurrentFindingNpc(nil)
        end)

        CLuaMiniMapPopWnd.m_EndPos = nil
        self:ResetCurrentFindingNpc(engineId)
        EventManager.BroadcastInternalForLua(EnumEventType.PlayerClickMiniMap, {pixelPos, onSuccess, nil})
    end
    if CommonDefs.DictContains(this.m_PlayerInfos, typeof(UInt32), engineId) then
        local pixelPos = Utility.GridPos2PixelPos(CommonDefs.DictGetValue(this.m_PlayerInfos, typeof(UInt32), engineId).Pos)
        EventManager.BroadcastInternalForLua(EnumEventType.PlayerClickMiniMap, {pixelPos, nil, nil})
    end

    if CommonDefs.DictContains(this.m_QingQiuMonsterDict, typeof(UInt32), engineId) then
        local monster = CommonDefs.DictGetValue(this.m_QingQiuMonsterDict, typeof(UInt32), engineId)
        local onSuccess = DelegateFactory.Action(function ()
            local npc = CClientObjectMgr.Inst:GetObject(engineId)
            if npc ~= nil then
                npc:OnSelected()
            end
        end)

        local pixelPos = Utility.GridPos2PixelPos(CreateFromClass(CPos, monster.m_PosX, monster.m_PosY))
        EventManager.BroadcastInternalForLua(EnumEventType.PlayerClickMiniMap, {pixelPos, onSuccess, nil})
    end
end

function CLuaMiniMapPopWnd:ResetCurrentFindingNpc(engineId)
    self.m_CurrentFindNpc = engineId
    if self.m_NpcData then
        self.m_Wnd:LoadNpcInfos(self.m_NpcData)
    end
end

function CLuaMiniMapPopWnd:OnDestroy(this)
    self.m_YarkObjs = {}
    self.m_MGJPlayerInfos = {}
    self.m_AnQiDaoObjInfos = {}
    self.m_CommonMarks = {}
    self.m_TablePanel = nil
    self.m_Wnd = nil
    self.m_CurrentFindNpc = nil
    self.m_NpcData = nil
    self.m_EndPos = nil
    self.m_CurrentTrackingNpcTemplateId = 0
    self:FreeHellImprismIcons()

    g_ScriptEvent:RemoveListener("SyncExtPos", self, "OnSyncExtPos")
    g_ScriptEvent:RemoveListener("OnSyncSectInWorldMap", self, "OnSyncSectInWorldMap")
    g_ScriptEvent:RemoveListener("OnLeaveZongMen", self, "OnLeaveZongMen")
    g_ScriptEvent:RemoveListener("SyncGnjcZhiHuiFaZhenPosition", self, "OnSyncGnjcZhiHuiFaZhenPosition")
    g_ScriptEvent:RemoveListener("SFEQ_XW_SyncTargetPos", self, "OnSyncQiXi2022TargetPos")
    g_ScriptEvent:RemoveListener("QueryMonsterPosResultNew", self, "OnSyncMonsterPosResultNew")

    self.ExtIcons = nil
    self.m_WuJianDiYuBossIcons = nil
    self.m_WuJianDiYuBossIconTemplates = nil
    self.m_WuJianDiYuMainPlayerNumberRoot = nil
    self.m_WuJianDiYuMainPlayerNumber = nil
    self.m_WuJianDiYuMainPlayerBG = nil
    self.NumberColors = nil
    self.m_AddPinBtn = nil
    self.m_RemovePinBtn = nil
    self.m_BackPinBtn = nil
    self.m_PinCollider = nil
    self.m_PinIcons = nil
    self.m_MainPlayerPinIcon = nil
    self.m_TablePanel = nil
    self.m_PinSystemEnabled = false
    self.m_IsShowGuanNingSundayView = false
    self.m_ShowWorldMap = false
    self.m_GuanNingSundayView = nil
    CLuaMiniMapPopWnd.m_ZongMenButtonInited = false
    self.m_GuildLeaguePlayerPosMapMarkTbl = nil
    self.m_GuildLeagueTowerPosMapMarkTbl = nil
    self.m_GuildLeagueTowerIds = nil
    self.m_JXYSPosMapMarkTbl = nil
    CLuaGuanNingMgr:ClearCommanderPopMapIcon()
    CLuaGuanNingMgr:ClearCounterattackPopMapIcon()

    self.monsterRedPointList = nil
    self.monsterOrangePointList = nil
    self.monsterBigIconNoHpList = nil
    self.monsterBigIconList = nil
    
    CUIManager.CloseUI(CUIResources.MiniMapRoutingWnd)
end

function CLuaMiniMapPopWnd:OnSyncExtPos(playerinfos,bossinfos)
    if self.ExtIcons.PlayerIcons == nil then
        self.ExtIcons.PlayerIcons = {}
    end

    for i=1,#playerinfos do
        local key = playerinfos[i].playerId
        if self.ExtIcons.PlayerIcons[key] == nil then
            local hellicon_root = self.m_Wnd.transform:Find("Contents/BigMap/CurrentMap/Map/HellIcons")
            local pTempCon = hellicon_root:Find("Lock").gameObject
            self.ExtIcons.PlayerIcons[key] = self:AddExtIcon(pTempCon)
        end
        if playerinfos[i].isHaveBuff then
            self:SyncPos(self.ExtIcons.PlayerIcons[key],playerinfos[i].x,playerinfos[i].y)
        else
            self.ExtIcons.PlayerIcons[key]:SetActive(false)
        end
    end

    if self.ExtIcons.BossIcons == nil then
        self.ExtIcons.BossIcons = {}
    end
    for i=1,#bossinfos do
        local key = bossinfos[i].bossTemplateId
        if self.ExtIcons.BossIcons[key] == nil then
            local hellicon_root = self.m_Wnd.transform:Find("Contents/BigMap/CurrentMap/Map/HellIcons")
            if i ==1 then
                local boss1 = hellicon_root.transform:Find("BossIcon0").gameObject
                self.ExtIcons.BossIcons[key] = self:AddExtIcon(boss1)
            else
                local boss2 = hellicon_root.transform:Find("BossIcon1").gameObject
                self.ExtIcons.BossIcons[key] = self:AddExtIcon(boss2)
            end
        end
        self:SyncPos(self.ExtIcons.BossIcons[key],bossinfos[i].x,bossinfos[i].y)
    end
end

function CLuaMiniMapPopWnd:SyncPos(ctrl,x,y)
    local Pos3d = Utility.GridPos2WorldPos(x, y)
    ctrl.transform.localPosition = self.m_Wnd:CalculateMapPos(Pos3d)
end

function CLuaMiniMapPopWnd:AddExtIcon(temp)
    local go = NGUITools.AddChild(self.m_Wnd.m_MapTexture.gameObject,temp)
    go:SetActive(true)
    return go
end

function CLuaMiniMapPopWnd:OnSyncYarkPick(t)
    --判断一下场景
    if not CClientHouseMgr.Inst:IsPlayerInYard() then
        for k,v in pairs(self.m_YarkObjs) do
            GameObject.Destroy(v)
        end
        self.m_YarkObjs = {}
        return
    end

    local lookup = {}
    for i,v in ipairs(t) do
        local engineId = v.engineId
        lookup[engineId] = true
    end
    --先删掉移除的
    for k,v in pairs(self.m_YarkObjs) do
        if not lookup[k] then
            if v then GameObject.Destroy(v) end
            self.m_YarkObjs[k] = nil
        end
    end

    for i,v in ipairs(t) do
        local engineId = v.engineId
        local pickTempId = v.pickTempId
        local obj = self.m_YarkObjs[engineId]
        if obj then
            local Pos3d = Utility.GridPos2WorldPos(v.x,v.y)
            obj.transform.localPosition = self.m_Wnd:CalculateMapPos(Pos3d)
        else
            local go = NGUITools.AddChild(self.m_Wnd.m_MapTexture.gameObject, self.m_Wnd.m_ZhuangshiwuTemplate)
            go:SetActive(true)
            go.transform:GetChild(0):GetComponent(typeof(UILabel)).text=Pick_Pick.GetData(pickTempId).Name
            local Pos3d = Utility.GridPos2WorldPos(v.x,v.y)
            go.transform.localPosition = self.m_Wnd:CalculateMapPos(Pos3d)

            self.m_YarkObjs[engineId] = go
        end
    end
end

function CLuaMiniMapPopWnd:UpdateQLDBossState()
  local node = CResourceMgr.Inst:Instantiate(CMiniMapWeekendFightMonsterItemResource.Inst, false, 1)
  if node ~= nil then
    local Pos3d = Utility.GridPos2WorldPos(LuaDoubleOne2019Mgr.QLDBossPosX, LuaDoubleOne2019Mgr.QLDBossPosY)
    node.transform.parent = self.m_Wnd.m_MapTexture.transform
    node.transform.localPosition = self.m_Wnd:CalculateMapPos(Pos3d)
    node.transform.localRotation = Quaternion.identity
    node.transform.localScale = Vector3.one
    node.layer = self.m_Wnd.gameObject.layer
    node:SetActive(true)
    if LuaDoubleOne2019Mgr.QLDBossAlive then
      node.transform:Find('Sprite'):GetComponent(typeof(UISprite)).spriteName = 'minimap_qilindong_boss_1'
    else
      node.transform:Find('Sprite'):GetComponent(typeof(UISprite)).spriteName = 'minimap_qilindong_boss_2'
    end
    --node.transform.FindChild("Label").GetComponent<UILabel>().text = CWeekendFightMgr.Inst.monsterName;
    CommonDefs.ListAdd(self.m_Wnd.saveWeekendFightNode, typeof(GameObject), node)
  end
end

function CLuaMiniMapPopWnd:UpdateSanxingFlagPos()
  do
    local i = 0
    while i < self.m_Wnd.saveWeekendFightNode.Count do
      if self.m_Wnd.saveWeekendFightNode[i] ~= nil then
        CResourceMgr.Inst:Destroy(self.m_Wnd.saveWeekendFightNode[i])
      end
      i = i + 1
    end
  end
  CommonDefs.ListClear(self.m_Wnd.saveWeekendFightNode)

  if not LuaSanxingGamePlayMgr.FlagPos then
    return
  end

  local textureTable = {}
  textureTable[37000165] = 'UI/Texture/Transparent/Material/sanxing_qizhi_02.mat'
  textureTable[37000166] = 'UI/Texture/Transparent/Material/sanxing_qizhi_01.mat'
  textureTable[37000167] = 'UI/Texture/Transparent/Material/sanxing_qizhi_01.mat'

  for i,v in pairs(LuaSanxingGamePlayMgr.FlagPos) do
    local node = CResourceMgr.Inst:Instantiate(CMiniMapCommonItemNodeResource.Inst, false, 1)
    if node ~= nil then
      local Pos3d = Utility.GridPos2WorldPos(v[1], v[2])
      node.transform.parent = self.m_Wnd.m_MapTexture.transform
      node.transform.localPosition = self.m_Wnd:CalculateMapPos(Pos3d)
      node.transform.localRotation = Quaternion.identity
      node.transform.localScale = Vector3.one
      node.layer = self.m_Wnd.gameObject.layer
      node:SetActive(true)
      node.transform:Find('icon'):GetComponent(typeof(CUITexture)):LoadMaterial(textureTable[i])
      local uiTexture = node.transform:Find('icon'):GetComponent(typeof(UITexture))
      uiTexture.width = 64
      uiTexture.height = 64
      --node.transform.FindChild("Label").GetComponent<UILabel>().text = CWeekendFightMgr.Inst.monsterName;
      CommonDefs.ListAdd(self.m_Wnd.saveWeekendFightNode, typeof(GameObject), node)
    end
  end

  --self:UpdateSanxingBossPos()
end

function CLuaMiniMapPopWnd:UpdateSanxingBossPos()
  if not LuaSanxingGamePlayMgr.BossPos then
    return
  end

  if LuaSanxingGamePlayMgr.sanxingStage and LuaSanxingGamePlayMgr.sanxingStage > 2 then
    return
  end

	--LuaSanxingGamePlayMgr.sanxingStage = stage
  local texturePath = 'UI/Texture/Portrait/Material/eguidao.mat'

  local node = CResourceMgr.Inst:Instantiate(CMiniMapCommonItemNodeResource.Inst, false, 1)
  if node ~= nil then
    local Pos3d = Utility.GridPos2WorldPos(LuaSanxingGamePlayMgr.BossPos[1], LuaSanxingGamePlayMgr.BossPos[2])
    node.transform.parent = self.m_Wnd.m_MapTexture.transform
    node.transform.localPosition = self.m_Wnd:CalculateMapPos(Pos3d)
    node.transform.localRotation = Quaternion.identity
    node.transform.localScale = Vector3.one
    node.layer = self.m_Wnd.gameObject.layer
    node:SetActive(true)
    node.transform:Find('icon'):GetComponent(typeof(CUITexture)):LoadMaterial(texturePath)
    local uiTexture = node.transform:Find('icon'):GetComponent(typeof(UITexture))
    uiTexture.width = 64
    uiTexture.height = 64
    --node.transform.FindChild("Label").GetComponent<UILabel>().text = CWeekendFightMgr.Inst.monsterName;
    CommonDefs.ListAdd(self.m_Wnd.saveWeekendFightNode, typeof(GameObject), node)
  end
end
function CLuaMiniMapPopWnd:InitHuluwaBoss()
    if not LuaHuLuWa2022Mgr.IsInRuYiDongPlay() then
        return
    end
    if not LuaHuLuWa2022Mgr.BossPosInfo then
        return
    end
    local info = LuaHuLuWa2022Mgr.BossPosInfo
    local templateId = info.templateId
    local x = info.x 
    local y = info.y 
    
    local data = Monster_Monster.GetData(templateId)
    local Pos3d = Utility.GridPos2WorldPos(x,y)
    if data then
        local texturePath = SafeStringFormat3("UI/Texture/Portrait/Material/%s.mat",data.HeadIcon)
        local node = CResourceMgr.Inst:Instantiate(CMiniMapCommonItemNodeResource.Inst, false, 1)
        node.transform.parent = self.m_Wnd.m_MapTexture.transform
        node.transform.localPosition = self.m_Wnd:CalculateMapPos(Pos3d)
        node.transform.localRotation = Quaternion.identity
        node.transform.localScale = Vector3.one
        node.layer = self.m_Wnd.gameObject.layer
        node:SetActive(true)
        node.transform:Find('icon'):GetComponent(typeof(CUITexture)):LoadMaterial(texturePath)
        local uiTexture = node.transform:Find('icon'):GetComponent(typeof(UITexture))
        uiTexture.width = 100
        uiTexture.height = 100
    end
end

function CLuaMiniMapPopWnd:InitGuildCustomPreTask1()
    if not LuaGuildCustomBuildMgr.s_SwitchOn then
        return
    end
    if not LuaGuildCustomBuildMgr.mPreTaskStage or LuaGuildCustomBuildMgr.mPreTaskStage~=1 then
        return
    end
    
    local setting = GuildCustomBuildPreTask_GameSetting.GetData()
    local config = setting.Task1MonsterConfig

    if config then
        for i=0,config.Length-5,5 do
            local templateId = config[i]
            local x = config[i+1]
            local y = config[i+2]
            local data = Monster_Monster.GetData(templateId)

            if data then
                local Pos3d = Utility.GridPos2WorldPos(x,y)
                local texturePath = SafeStringFormat3("UI/Texture/Portrait/Material/%s.mat",data.HeadIcon)
                local node = CResourceMgr.Inst:Instantiate(CMiniMapCommonItemNodeResource.Inst, false, 1)
                node.transform.parent = self.m_Wnd.m_MapTexture.transform
                node.transform.localPosition = self.m_Wnd:CalculateMapPos(Pos3d)
                node.transform.localRotation = Quaternion.identity
                node.transform.localScale = Vector3.one
                node.layer = self.m_Wnd.gameObject.layer
                node:SetActive(true)
                node.transform:Find('icon'):GetComponent(typeof(CUITexture)):LoadMaterial(texturePath)
                local uiTexture = node.transform:Find('icon'):GetComponent(typeof(UITexture))
                uiTexture.width = 100
                uiTexture.height = 100
            end
        end
    end
end

function CLuaMiniMapPopWnd:InitHellImprismIcons()
    local gamePlayId=CScene.MainScene.GamePlayDesignId
    if (gamePlayId == LuaWuJianDiYuMgr.gamePlayId) then --无间地狱， 初始化束缚位置
        self.NumberColors = {"e9cb3c", "f97e33", "4dccf7", "94d43f", "9f79ec"}
        local hellicon_root = self.m_Wnd.transform:Find("Contents/BigMap/CurrentMap/Map/HellIcons")
        self.m_WuJianDiYuMainPlayerNumberRoot = self.m_Wnd.transform:Find("Contents/BigMap/CurrentMap/Map/MainPlayerNumber").gameObject
        self.m_WuJianDiYuMainPlayerNumberRoot:SetActive(true)
        self.m_WuJianDiYuMainPlayerNumberRoot.transform.localPosition = self.m_Wnd.m_MainPlayerMark.transform.localPosition
        local extend_sprite = self.m_Wnd.m_MainPlayerMark.transform:Find("extendedSprite").gameObject
        extend_sprite:SetActive(true)
        self.HellImprismIcon = hellicon_root.transform:Find("Lock").gameObject
        self.HellImprismIcon:SetActive(false)
        self.m_WuJianDiYuMainPlayerNumber = self.m_WuJianDiYuMainPlayerNumberRoot.transform:Find("NumberLabel"):GetComponent(typeof(UILabel))
        self.m_WuJianDiYuMainPlayerBG = self.m_WuJianDiYuMainPlayerNumberRoot.transform:Find("NumberBG"):GetComponent(typeof(UISprite))
        self:EnablePinSystem()
        self:InitWuJianDiYuPinSystem()
        do
            local team_list = CTeamMgr.Inst.Members
            for i = 0, (team_list.Count - 1), 1 do
                if(team_list[i].m_MemberId == CClientMainPlayer.Inst.Id)then
                    self.m_WuJianDiYuMainPlayerBG.color = NGUIText.ParseColor24(self.NumberColors[i + 1], 0)
                    self.m_WuJianDiYuMainPlayerNumber.text = tostring(i + 1)
                    break
                end
            end
        end
        do
            LuaWuJianDiYuMgr:InitSettingData()
            self.m_WuJianDiYuBossIconTemplates = {}
            local icon = hellicon_root.transform:Find("BossIcon0").gameObject
            icon:SetActive(false)
            self.m_WuJianDiYuBossIconTemplates[LuaWuJianDiYuMgr.m_BossIds[1]] = icon
            icon = hellicon_root.transform:Find("BossIcon1").gameObject
            icon:SetActive(false)
            self.m_WuJianDiYuBossIconTemplates[LuaWuJianDiYuMgr.m_BossIds[2]] = icon
        end

        if(self.m_HellImprismIcons)then
            for k, v in ipairs(self.m_HellImprismIcons)do
                GameObject.Destroy(v)
            end
        end
        self.m_HellImprismIcons = {}
        if(self.HellImprismIcon)then
            for i = 1, 5, 1 do
                local go = NGUITools.AddChild(self.m_Wnd.m_MapTexture.gameObject, self.HellImprismIcon)
                go:SetActive(false)
                table.insert(self.m_HellImprismIcons, go)
            end
            if(LuaWuJianDiYuMgr.m_PlayerImprismInfoCache)then
                local cache = LuaWuJianDiYuMgr.m_PlayerImprismInfoCache
                local player_info_num = #cache
                for i = 1, player_info_num, 1 do
                    local Pos3d = Utility.GridPos2WorldPos(cache[i].x, cache[i].y)
                    self.m_HellImprismIcons[i].transform.localPosition = self.m_Wnd:CalculateMapPos(Pos3d)
                    self.m_HellImprismIcons[i]:SetActive(cache[i].isHaveBuff)
                end
            end
        end

        if(self.m_WuJianDiYuBossIconTemplates)then
            self:InitHellBossIcons()
        end
        self:NotifyUpdateHellInfos()
        self:NotifyUpdatePinInfos()
    end
end

function CLuaMiniMapPopWnd:InitHellBossIcons()
    if(self.m_Wnd)then
        if(self.m_WuJianDiYuBossIcons)then
            for k, v in pairs(self.m_WuJianDiYuBossIcons)do --类型列表
                for k1, v1 in ipairs(v) do --实例列表
                    GameObject.Destroy(v1)
                end
            end
        end

        self.m_WuJianDiYuBossIcons = {}
        for k, v in pairs(self.m_WuJianDiYuBossIconTemplates)do
            local go = NGUITools.AddChild(self.m_Wnd.m_MapTexture.gameObject, v)
            go:SetActive(false)
            self.m_WuJianDiYuBossIcons[k] = {}
            table.insert(self.m_WuJianDiYuBossIcons[k], go)
        end
    end
end

function CLuaMiniMapPopWnd:FreeHellImprismIcons()
    if(self.m_HellImprismIcons)then
        for k, v in ipairs(self.m_HellImprismIcons)do
            GameObject.Destroy(v)
        end
        self.m_HellImprismIcons = nil
    end
    if(self.m_WuJianDiYuBossIcons)then
        for k, v in pairs(self.m_WuJianDiYuBossIcons)do --类型列表
            for k1, v1 in ipairs(v) do --实例列表
                GameObject.Destroy(v1)
            end
        end
    end
    self.NumberColors = nil
    self.m_WuJianDiYuBossIcons = nil
    self.m_WuJianDiYuBossIconTemplates = nil
    self.HellImprismIcon = nil

    self.m_WuJianDiYuMainPlayerNumberRoot = nil
    self.m_WuJianDiYuMainPlayerNumber = nil
    self.m_WuJianDiYuMainPlayerBG = nil

    self:FreePinSystem()
end

function CLuaMiniMapPopWnd:NotifyUpdateHellInfos()
    if(self.m_HellImprismIcons)then
        for k, v in ipairs(self.m_HellImprismIcons) do
            v:SetActive(false)
        end
        if(LuaWuJianDiYuMgr.m_PlayerImprismInfoCache)then
            local player_table = LuaWuJianDiYuMgr.m_PlayerImprismInfoCache
            local player_info_num = #player_table
            for i = 1, player_info_num, 1 do
                local Pos3d = Utility.GridPos2WorldPos(player_table[i].x, player_table[i].y)
                self.m_HellImprismIcons[i].transform.localPosition = self.m_Wnd:CalculateMapPos(Pos3d)
                self.m_HellImprismIcons[i]:SetActive(player_table[i].isHaveBuff)
            end
        end
    end

    if(self.m_WuJianDiYuBossIcons)then

        for k, v in pairs(LuaWuJianDiYuMgr.m_BossInfoCache)do
            local thisnum = #self.m_WuJianDiYuBossIcons[k]
            local theirsnum = #v

            if(thisnum < theirsnum)then
                for i = 1, (theirsnum - thisnum), 1 do
                    local go = NGUITools.AddChild(self.m_Wnd.m_MapTexture.gameObject, self.m_WuJianDiYuBossIconTemplates[k])
                    --go:SetActive(true)
                    table.insert(self.m_WuJianDiYuBossIcons[k], go)
                end
            elseif(thisnum > theirsnum)then
                for i = (theirsnum + 1), thisnum, 1 do
                    self.m_WuJianDiYuBossIcons[k][i]:SetActive(false)
                end
            end
            for k1, v1 in ipairs(v) do
                local Pos3d = Utility.GridPos2WorldPos(v1.x, v1.y)
                self.m_WuJianDiYuBossIcons[k][k1].transform.localPosition = self.m_Wnd:CalculateMapPos(Pos3d)
                self.m_WuJianDiYuBossIcons[k][k1]:SetActive(true)
            end
        end
    end
end

function CLuaMiniMapPopWnd:EnablePinSystem()
    local pin_root = self.m_Wnd.transform:Find("Contents/BigMap/PinPanel/AddPinRoot").gameObject
    pin_root:SetActive(true)
    self.m_AddPinBtn = pin_root.transform:Find("AddPinButton").gameObject
    self.m_RemovePinBtn = pin_root.transform:Find("RemovePinButton").gameObject
    self.m_PinCollider = pin_root.transform:Find("PinCollider").gameObject
    self.m_PinCollider:SetActive(false)
    self.m_BackPinBtn = pin_root.transform:Find("PinCollider/BackBtn").gameObject

    UIEventListener.Get(self.m_AddPinBtn).onClick = DelegateFactory.VoidDelegate(
        function(go)
            self.m_PinCollider:SetActive(true)
            self.m_AddPinBtn:SetActive(false)
            self.m_RemovePinBtn:SetActive(false)
            self.m_TablePanel:SetActive(false)
            local pin_lable = pin_root.transform:Find("PinCollider/TopLabel/LabelText").gameObject
            LuaTweenUtils.DOKill(pin_lable.transform)
            LuaTweenUtils.SetLoops(LuaTweenUtils.TweenAlpha(pin_lable.transform, 0.0, 1.0, 1.0), -1, true)
        end
    )
    UIEventListener.Get(self.m_BackPinBtn).onClick = DelegateFactory.VoidDelegate(
        function(go)
            self.m_PinCollider:SetActive(false)
            self.m_AddPinBtn:SetActive(true)
            self.m_RemovePinBtn:SetActive(true)
            self.m_TablePanel:SetActive(true)
        end
    )
    self.m_PinSystemEnabled = true
end

function CLuaMiniMapPopWnd:SetShowPinSystem(_show)
    if(self.m_PinSystemEnabled)then
        local pin_root = self.m_Wnd.transform:Find("Contents/BigMap/PinPanel/AddPinRoot").gameObject
        pin_root:SetActive(_show and true or false)
    end
end

function CLuaMiniMapPopWnd:FreePinSystem()
    self.m_AddPinBtn = nil
    self.m_RemovePinBtn = nil
    self.m_PinCollider = nil
    self.m_BackPinBtn = nil
    if(self.m_PinIcons)then
        for k, v in ipairs(self.m_PinIcons) do
            GameObject.Destroy(v)
        end
        self.m_PinIcons = nil
    end
    if(self.m_MainPlayerPinIcon)then
        GameObject.Destroy(self.m_MainPlayerPinIcon)
        self.m_MainPlayerPinIcon = nil
    end
    self.m_PinSystemEnabled = false
end

function CLuaMiniMapPopWnd:InitWuJianDiYuPinSystem()
    local mainplayer_pin_template = self.m_Wnd.transform:Find("Contents/BigMap/CurrentMap/Map/HellIcons/PinPoint").gameObject
    if(self.m_MainPlayerPinIcon)then
        GameObject.Destroy(self.m_MainPlayerPinIcon)
        self.m_MainPlayerPinIcon = nil
    end
    self.m_MainPlayerPinIcon = NGUITools.AddChild(self.m_Wnd.m_MapTexture.gameObject, mainplayer_pin_template)
    self.m_MainPlayerPinIcon:SetActive(false)

    local pin_icon_template = self.m_Wnd.transform:Find("Contents/BigMap/CurrentMap/Map/HellIcons/TeamMatePinPoint").gameObject
    pin_icon_template:SetActive(false)
    if(self.m_PinIcons)then
        for k, v in ipairs(self.m_PinIcons) do
            GameObject.Destroy(v)
        end
    end
    self.m_PinIcons = {}
    for i = 1, 5, 1 do
        local go = NGUITools.AddChild(self.m_Wnd.m_MapTexture.gameObject, pin_icon_template)
        go:SetActive(false)
        table.insert(self.m_PinIcons, go)
    end
    UIEventListener.Get(self.m_RemovePinBtn).onClick = DelegateFactory.VoidDelegate(
        function(go)
            Gac2Gas.MarkTreasureBoxInUnintermittentHell(0, 0)
        end
    )
    UIEventListener.Get(self.m_PinCollider).onClick = DelegateFactory.VoidDelegate(
        function(go)
            self:WuJianDiYuOnAddPin()
        end
    )
end

function CLuaMiniMapPopWnd:WuJianDiYuOnAddPin()
    local pos = self.m_Wnd.m_MapTexture.transform:InverseTransformPoint(UICamera.lastWorldPosition)
    local _w = self.m_Wnd.m_MapTexture.width
    local _h = self.m_Wnd.m_MapTexture.height
    --将坐标系映射至0-1000
    local _x = math.floor(pos.x * 1000 / _w + 500)
    if(_x <= 0)then
        _x = 1
    end
    local _y = math.floor(pos.y * 1000 / _h + 500)
    if(_y <= 0)then
        _y = 1
    end
    Gac2Gas.MarkTreasureBoxInUnintermittentHell(_x, _y)
end

function CLuaMiniMapPopWnd:NotifyUpdatePinInfos()
    if(self.m_Wnd and LuaWuJianDiYuMgr.m_PinDataCache)then
        local _w = self.m_Wnd.m_MapTexture.width
        local _h = self.m_Wnd.m_MapTexture.height
        local teammate_pin_id = 1
        local team_list = CTeamMgr.Inst.Members
        self.m_MainPlayerPinIcon:SetActive(false)
        for k, v in ipairs(self.m_PinIcons)do
            v:SetActive(false)
        end
        for k, v in ipairs(LuaWuJianDiYuMgr.m_PinDataCache)do
            local _sprite = nil
            if(CClientMainPlayer.Inst.Id == tonumber(v.playerId))then
                self.m_MainPlayerPinIcon.transform.localPosition = Vector3((v.x - 500) * _w / 1000, (v.y - 500) * _h / 1000, 0)
                self.m_MainPlayerPinIcon:SetActive(true)
                _sprite = self.m_MainPlayerPinIcon.transform:Find("PinPointSprite"):GetComponent(typeof(UISprite))
            else
                self.m_PinIcons[teammate_pin_id].transform.localPosition = Vector3((v.x - 500) * _w / 1000, (v.y - 500) * _h / 1000, 0)
                self.m_PinIcons[teammate_pin_id]:SetActive(true)
                _sprite = self.m_PinIcons[teammate_pin_id].transform:Find("PinPointSprite"):GetComponent(typeof(UISprite))
                teammate_pin_id = teammate_pin_id + 1
            end
            do
                for i = 0, (team_list.Count - 1), 1 do
                    if(team_list[i].m_MemberId == v.playerId)then
                        _sprite.color = NGUIText.ParseColor24(self.NumberColors[i + 1], 0)
                        break
                    end
                end
            end
        end
    end
end

function CLuaMiniMapPopWnd:OnSyncHalloween2020ArrestPlayInfo(restCount, totalCount, playData)
    --判断一下场景
    if not CLuaHalloween2020Mgr.IsInPlay() then
        for k,v in pairs(self.m_MGJPlayerInfos) do
            GameObject.Destroy(v)
        end
        self.m_MGJPlayerInfos = {}
        return
    end

    local lookup = {}
    for i,v in ipairs(playData) do
        local playerId = v.playerId
        lookup[playerId] = true
    end
    --先删掉移除的
    for k,v in pairs(self.m_MGJPlayerInfos) do
        if not lookup[k] then
            if v then GameObject.Destroy(v) end
            self.m_MGJPlayerInfos[k] = nil
        end
    end

    for i,v in ipairs(playData) do
        local playerId = v.playerId

        if (CClientMainPlayer.Inst or {}).Id ~= playerId then
            local obj = self.m_MGJPlayerInfos[playerId]
            if not obj then
                local mengGuiJieTemplate = self.m_Wnd.transform:Find("Contents/BigMap/CurrentMap/Map/MengGuiJieTemplate").gameObject
                local go = NGUITools.AddChild(self.m_Wnd.m_MapTexture.gameObject, mengGuiJieTemplate)
                go:SetActive(true)
                self.m_MGJPlayerInfos[playerId] = go
                obj = go
            end

            local Pos3d = Utility.GridPos2WorldPos(v.x, v.y)
            obj.transform.localPosition = self.m_Wnd:CalculateMapPos(Pos3d)
            local sprite = obj.transform:Find("Sprite"):GetComponent(typeof(UISprite))
            local lockGo = obj.transform:Find("Lock").gameObject
            if v.flag == 1 then
                lockGo:SetActive(true)
                sprite.gameObject:SetActive(false)
            else
                lockGo:SetActive(false)
                sprite.gameObject:SetActive(true)
                if CLuaHalloween2020Mgr.playerForce == v.force then
                    sprite.spriteName = Minimap_Normal.GetData(EnumToInt(EMinimapMarkType.OtherPlayer_Friend)).Icon
                else
                    sprite.spriteName = Minimap_Normal.GetData(EnumToInt(EMinimapMarkType.OtherPlayer_Enemy)).Icon
                end
            end
        end
    end
end

function CLuaMiniMapPopWnd:OnAnQiIslandSyncCannonBallAndBossPos(infos)
    --判断一下场景
    if not CLuaAnQiDaoMgr.IsInPlay() then
        for k,v in pairs(self.m_AnQiDaoObjInfos) do
            GameObject.Destroy(v)
        end
        self.m_AnQiDaoObjInfos = {}
        return
    end

    local lookup = {}
    for i,v in ipairs(infos) do
        local eId = v.eId
        lookup[eId] = true
    end
    --先删掉移除的
    for k,v in pairs(self.m_AnQiDaoObjInfos) do
        if not lookup[k] then
            if v then GameObject.Destroy(v) end
            self.m_AnQiDaoObjInfos[k] = nil
        end
    end

    if not self.m_AnQiDaoPickName then
        self.m_AnQiDaoPickName = Pick_Pick.GetData(AnQiIsland_Setting.GetData().CannonBallPickId).Name
    end

    if not self.m_AnQiDaoBossName then
        self.m_AnQiDaoBossName = Monster_Monster.GetData(AnQiIsland_Setting.GetData().BossTemplateId).Name
    end

    for i,v in ipairs(infos) do
        local eId = v.eId
        local obj = self.m_AnQiDaoObjInfos[eId]
        if not obj then
            local markTemplate = self.m_Wnd.transform:Find("Contents/BigMap/CurrentMap/Map/markTemplate").gameObject
            local go = NGUITools.AddChild(self.m_Wnd.m_MapTexture.gameObject, markTemplate)
            local collider = go:GetComponent(typeof(BoxCollider))
            local miniMapMark = go:GetComponent(typeof(CMiniMapMarkTemplate))
            collider.enabled = false
            miniMapMark.EngineId = eId
            miniMapMark:UpdateView(
                EMinimapCategory.Normal, 
                v.isPick == true and EMinimapMarkType.Pick or EMinimapMarkType.Monster_Boss, 
                v.isPick == true and self.m_AnQiDaoPickName or self.m_AnQiDaoBossName, 
                "")
            go:SetActive(true)
            self.m_AnQiDaoObjInfos[eId] = go
            obj = go
        end

        local Pos3d = Utility.GridPos2WorldPos(v.x, v.y)
        obj.transform.localPosition = self.m_Wnd:CalculateMapPos(Pos3d)
    end
end

function CLuaMiniMapPopWnd:TryShowMainPlayerSoulCorePos()
    --判断一下场景
    if not LuaZongMenMgr:IsMySectScene() or LuaZongMenMgr.m_SelfSoulCoreGo == nil then
        if self.m_SoulCoreObjInfo ~= nil then
            GameObject.Destroy(self.m_SoulCoreObjInfo)
            self.m_SoulCoreObjInfo = nil
        end
        return
    end
    local soulcoreName = SafeStringFormat3(LocalString.GetString("我的灵核"))

    local obj = self.m_SoulCoreObjInfo
    if not obj then
        local markTemplate = self.m_Wnd.transform:Find("Contents/BigMap/CurrentMap/Map/markTemplate").gameObject
        local go = NGUITools.AddChild(self.m_Wnd.m_MapTexture.gameObject, markTemplate)
        local miniMapMark = go:GetComponent(typeof(CMiniMapMarkTemplate))
        miniMapMark.EngineId = 1
        miniMapMark:UpdateView(
            EMinimapCategory.Normal, 
            EMinimapMarkType.Npc_Concerned, 
            soulcoreName, 
            "")
        go:SetActive(true)
        self.m_SoulCoreObjInfo = go
        obj = go
    end

    obj.transform.localPosition = self.m_Wnd:CalculateMapPos(LuaZongMenMgr.m_SelfSoulCoreGo.transform.position)
end

function CLuaMiniMapPopWnd:ReplacePopWndWorldMap(this)
    ShiMenShouWei_Ruin.ForeachKey(function (key)
        LuaMiniMapMgr:ChangePopWndWorldMapInfo(key)
    end)
    for key, t in pairs(LuaMiniMapMgr.m_PopWndWorldMapReplaceInfoDict) do
        local root = this.m_WorldMapRoot.transform:Find(key)
        if root then
            local worldMapInfo = root:GetComponent(typeof(CWorldMapInfo))
            if worldMapInfo then
                worldMapInfo.TemplateId = t.MapId
            end
            local uitex = root:GetComponent(typeof(UISprite))
            local nameUitex = root.transform:Find("Texture"):GetComponent(typeof(UISprite))
            if uitex then
                uitex.spriteName = t.Icon
                nameUitex.spriteName = t.NameIcon
            end
        end
    end
end

function CLuaMiniMapPopWnd:OnSyncGnjcBossHpInfo()
    if self.m_Wnd and not self.m_IsShowGuanNingSundayView and CGuanNingMgr.Inst.inGnjc and CLuaGuanNingMgr.m_IsSundayPlayOpen and self.m_GuanNingSundayView then
        self.m_IsShowGuanNingSundayView = true
        self.m_GuanNingSundayView:SetActive(true)
    end
end

function CLuaMiniMapPopWnd:InitSectTrap(this)
    local sectTrapInfoList = CRenderScene.Inst:GetExtraSectTrapInfo()
    if sectTrapInfoList then
        for i  = 0, sectTrapInfoList.Count - 1 do
            local p = sectTrapInfoList[i].pos
            local Pos3d = Utility.GridPos2WorldPos(p.x, p.y)
            local obj = CResourceMgr.Inst:Instantiate(CMiniMapPopWndTeleportResource.Inst, false, 2)
            obj.transform:Find("Sprite").gameObject:SetActive(false)
            obj.name = sectTrapInfoList[i].isXiePai and "SectXiePaiChuansong" or "SectChuansong";
            local tex = obj.transform:Find("Texture")
            if tex then
                tex:GetComponent(typeof(CUITexture)):LoadMaterial(sectTrapInfoList[i].isXiePai and "UI/Texture/Transparent/Material/minimap_sectchuansongzhen_evil.mat" or "UI/Texture/Transparent/Material/minimap_sectchuansongzhen.mat")
                tex.gameObject:SetActive(true)
            end
            obj.transform.parent = this.m_MapTexture.transform
            obj.transform.localPosition = this:CalculateMapPos(Pos3d)
            obj.transform.localRotation = Quaternion.identity
            obj.transform.localScale = Vector3.one
            obj.layer = this.gameObject.layer
            obj:SetActive(true)
            local label = CommonDefs.GetComponent_GameObject_Type(obj, typeof(UILabel))
            if CScene.MainScene.ShowTeleportInMiniMapPopWnd == 1 then
                label.text = ""
            end
            if CScene.MainScene.ShowTeleportInMiniMapPopWnd == 2 then
                label.text = LocalString.GetString("出口")
            end
            CommonDefs.ListAdd(this.m_TeleportList, typeof(GameObject), obj)
        end
        if CScene.MainScene then
            local sceneName = CScene.MainScene.Res
            local sectView = this.transform:Find('Contents/BigMap/CurrentMap/Map/SectView').gameObject
            sectView:SetActive(sectTrapInfoList.Count > 0 and string.find(sceneName,"zilishimen") ~= nil)
        end
    end
end

function CLuaMiniMapPopWnd:ProcessNPCTemplateStatus(obj, NPCId)
    local status = obj.transform:Find("Status")
    if status ~= nil then
        status.gameObject:SetActive(CTaskMgr.Inst:IsImportantTaskReleasableNpc(NPCId))
    end
end

function CLuaMiniMapPopWnd:OnUpdateHuaCheMapPos()
    if self.m_Wnd then
        local set = {}
        CommonDefs.DictIterate(self.m_Wnd.m_NpcMapMarkList, DelegateFactory.Action_object_object(function(engineId,val)
            set[engineId] = true
        end))
        for engineId,_ in pairs(set) do
            local obj = CClientObjectMgr.Inst:GetObject(engineId)
            if obj and obj.RO then
                local Pos3d = obj.RO:GetPosition()
                CommonDefs.DictGetValue(self.m_Wnd.m_NpcMapMarkList, typeof(UInt32), engineId).transform.localPosition = self.m_Wnd:CalculateMapPos(Pos3d)
            end
        end 
    end
end

function CLuaMiniMapPopWnd:OnQueryGuildLeaguePlayerPosResult(posInfoTbl)
    local this = self.m_Wnd

    if self.m_GuildLeaguePlayerPosMapMarkTbl then
        for __, mark in pairs(self.m_GuildLeaguePlayerPosMapMarkTbl) do
            CResourceMgr.Inst:Destroy(mark.gameObject)
        end
        
    end
    self.m_GuildLeaguePlayerPosMapMarkTbl = {}
    for __, posInfo in pairs(posInfoTbl) do
        local obj = CResourceMgr.Inst:Instantiate(CMiniMapPopWndNpcMarkResource.Inst, false, 10)
        local pos3d = Utility.GridPos2WorldPos(posInfo.posX, posInfo.posY)
        obj.transform.parent = this.m_MapTexture.transform
        obj.transform.localPosition = this:CalculateMapPos(pos3d)
        obj.transform.localRotation = Quaternion.identity
        obj.transform.localScale = Vector3.one
        obj.layer = this.gameObject.layer
        obj:SetActive(true)
        local template = CommonDefs.GetComponent_GameObject_Type(obj, typeof(CMiniMapMarkTemplate))
        template.Selectable = false
        template.EngineId = posInfo.engineId
        --根据势力进行划分 将军威阵营为蓝色小点，玄霄怒阵营为红色小点
        if posInfo.force == EnumCommonForce_lua.eAttack then --将军威
            template:UpdateView(EMinimapCategory.Normal, EMinimapMarkType.Default, "minimap_monster_blue", "", "")
        else
            template:UpdateView(EMinimapCategory.Normal, EMinimapMarkType.Default, "minimap_otherplayer", "", "")
        end
        table.insert(self.m_GuildLeaguePlayerPosMapMarkTbl, template)
    end
end

function CLuaMiniMapPopWnd:OnQueryGuildLeagueTowerPosResult(posInfoTbl)
    local this = self.m_Wnd

    if self.m_GuildLeagueTowerPosMapMarkTbl then
        for __, mark in pairs(self.m_GuildLeagueTowerPosMapMarkTbl) do
            mark:SetActive(false)
        end
    else
        self.m_GuildLeagueTowerPosMapMarkTbl = {}
    end
    local templateTrans = this.m_CurrentMapRoot.transform:Find("Map/GuildLeagueTowerTemplate")
    if templateTrans == nil then return end
    local template = templateTrans.gameObject
    for i, posInfo in pairs(posInfoTbl) do
        if posInfo.hp>0 then
            local pos3d = Utility.GridPos2WorldPos(posInfo.posX, posInfo.posY)
            local obj = i<=#self.m_GuildLeagueTowerPosMapMarkTbl and self.m_GuildLeagueTowerPosMapMarkTbl[i] or CommonDefs.Object_Instantiate(template)
            obj.transform.parent = this.m_MapTexture.transform
            obj.transform.localPosition = this:CalculateMapPos(pos3d)
            obj.transform.localRotation = Quaternion.identity
            obj.transform.localScale = Vector3.one
            obj.layer = this.gameObject.layer
            obj:SetActive(true)
            local normalIcon = obj.transform:Find("Normal").gameObject
            local damageIcon = obj.transform:Find("Damage").gameObject
            local slider = obj.transform:Find("Slider"):GetComponent(typeof(UISlider))
            normalIcon:SetActive(posInfo.hp==posInfo.hpFull)
            damageIcon:SetActive(posInfo.hp<posInfo.hpFull)
            slider.value = posInfo.hp / posInfo.hpFull
            if posInfo.force == EnumCommonForce_lua.eAttack then --蓝色将军威
                (TypeAs(slider.foregroundWidget, typeof(UISprite))).spriteName = g_sprites.CommonMPSprite
            else --玄霄怒
                (TypeAs(slider.foregroundWidget, typeof(UISprite))).spriteName = g_sprites.CommonHPSprite
            end
            slider.gameObject:SetActive(posInfo.hp<posInfo.hpFull) --根据策划要求，气血不满的时候才显示血条
            self.m_GuildLeagueTowerPosMapMarkTbl[i] = obj
        end
    end
end

function CLuaMiniMapPopWnd:IsGuildLeagueTower(templateId)
    if self.m_GuildLeagueTowerIds == nil then
        self.m_GuildLeagueTowerIds = {}
        local ids = GuildLeague_Setting.GetData().TowerMonsterIds
        for i=0, ids.Length-1 do
            self.m_GuildLeagueTowerIds[ids[i]] = 1
        end
    end
    return self.m_GuildLeagueTowerIds[templateId]~=nil
end

function CLuaMiniMapPopWnd:InitGuildLeagueTowerTemplate()
    local this = self.m_Wnd
    local template = this.m_CurrentMapRoot.transform:Find("GuildLeagueTowerTemplate")
    if template then template:SetActive(false) end --隐藏一下跨服联赛观战中的箭塔图标模板,避免制作资源时没有隐藏导致错误显示
end

function CLuaMiniMapPopWnd:OnQueryPlayerPos_JXYSEnemy(posInfoTbl)
    local this = self.m_Wnd

    if self.m_JXYSPosMapMarkTbl then
        for __, mark in pairs(self.m_JXYSPosMapMarkTbl) do
            CResourceMgr.Inst:Destroy(mark.gameObject)
        end
    end
    self.m_JXYSPosMapMarkTbl = {}
    local category = CMiniMap.GetMinimapCategory()
    for __, posInfo in pairs(posInfoTbl) do
        local obj = CResourceMgr.Inst:Instantiate(CMiniMapPopWndNpcMarkResource.Inst, false, 10)
        local pos3d = Utility.GridPos2WorldPos(posInfo.posX, posInfo.posY)
        obj.transform.parent = this.m_MapTexture.transform
        obj.transform.localPosition = this:CalculateMapPos(pos3d)
        obj.transform.localRotation = Quaternion.identity
        obj.transform.localScale = Vector3.one
        obj.layer = this.gameObject.layer
        obj:SetActive(true)
        local template = CommonDefs.GetComponent_GameObject_Type(obj, typeof(CMiniMapMarkTemplate))
        template.Selectable = false
        template.EngineId = posInfo.engineId
        --特殊规则 非队伍成员显示红色
        if posInfo.isTeamMember then
            template:UpdateView(category, posInfo.isTeamLeader and EMinimapMarkType.OtherPlayer_TeamLeader or EMinimapMarkType.OtherPlayer_TeamMember, "", "")
        else
            template:UpdateView(category, EMinimapMarkType.OtherPlayer_Enemy, "", "")
        end
        table.insert(self.m_JXYSPosMapMarkTbl, template)
    end
end

function CLuaMiniMapPopWnd:UpdateMiniMapMark(engineId, info) 
    local go = self.m_CommonMarks[engineId]
    if info == nil then 
        if not CommonDefs.IsNull(go) then CResourceMgr.Inst:Destroy(go) end
        self.m_CommonMarks[engineId] = nil
    else
        self:SetMapMark(engineId, info)
    end
end

function CLuaMiniMapPopWnd:RefreshMiniMapMarks(markInfos)
    if not markInfos then return end

    for id, obj in pairs(self.m_CommonMarks) do
        if not markInfos[id] then
            if not CommonDefs.IsNull(obj) then CResourceMgr.Inst:Destroy(obj) end
            self.m_CommonMarks[id] = nil
        end
    end

    for id, info in pairs(markInfos) do
        self:SetMapMark(id, info)
    end
end

function CLuaMiniMapPopWnd:SetMapMark(id, info)
    local go = self.m_CommonMarks[id]

    if CommonDefs.IsNull(go) then
        go = CResourceMgr.Inst:Instantiate(CMiniMapCommonItemNodeResource.Inst, false, 1)
        go.transform.parent = self.m_Wnd.m_MapTexture.transform
        go.layer = self.m_Wnd.gameObject.layer
        self.m_CommonMarks[id] = go
    end

    go.transform.localPosition = self.m_Wnd:CalculateMapPos(Utility.GridPos2WorldPos(info.x or 0, info.y or 0))
    go.transform.localRotation = Quaternion.Euler(0, 0, info.rotZ or 0)
    go.transform.localScale = CommonDefs.op_Multiply_Vector3_Single(Vector3.one, info.scale or 1)

    local icon = go.transform:Find("icon"):GetComponent(typeof(CUITexture))
    icon.transform.localPosition = Vector3(info.offset and info.offset[2] and info.offset[2].x or 0, info.offset and info.offset[2] and info.offset[2].y or 0, 0)
    icon:LoadMaterial(info.res or "")
    if info.color then icon.color = info.color end
    if info.alpha then icon.alpha = info.alpha end
    if info.depth then icon.texture.depth = info.depth end

    local uiTexture = go.transform:Find("icon"):GetComponent(typeof(UITexture))
    uiTexture.width = info.size and info.size[2] and info.size[2].width or 64
    uiTexture.height = info.size and info.size[2] and info.size[2].height or 64
    
    go:SetActive(info.show == nil and true or info.show)
end

function CLuaMiniMapPopWnd:OnSyncMonsterPosResultNew(monsterInfoData)
    local monsterList = {self.monsterRedPointList, self.monsterOrangePointList, self.monsterBigIconNoHpList, self.monsterBigIconList}
    for i = 1, 4 do
        local tbl = monsterList[i]
        if tbl then
            for _, v in pairs(tbl) do
                v.gameObject:SetActive(false)
            end
        end
    end
    local monsterRedIndex = 1
    local monsterOrangeIndex = 1
    local monsterBigIconNoHpIndex = 1
    local monsterBigIconIndex = 1
    
    for _, info in pairs(monsterInfoData) do
        local monsterTemplateId = info.templateId
        local monsterCfgData = Monster_Monster.GetData(monsterTemplateId)
        local go = nil
        if monsterCfgData.IconTypeInMap == 1 then
            if self.monsterRedPointList == nil then
                self.monsterRedPointList = {}
            end
            go = self.monsterRedPointList[monsterRedIndex]
            if CommonDefs.IsNull(go) then
                local markTemplate = self.m_Wnd.transform:Find("Contents/BigMap/CurrentMap/Map/markTemplate").gameObject
                go = CommonDefs.Object_Instantiate(markTemplate)
                go.transform:Find("Table").gameObject:SetActive(false)
                local collider = go.transform:GetComponent(typeof(BoxCollider))
                collider.enabled = false
                self.monsterRedPointList[monsterRedIndex] = go
            end
            local icon = go.transform:GetComponent(typeof(UISprite))
            icon.spriteName = "minimap_monster"
            monsterRedIndex = monsterRedIndex + 1
        elseif monsterCfgData.IconTypeInMap == 2 then
            if self.monsterOrangePointList == nil then
                self.monsterOrangePointList = {}
            end
            go = self.monsterOrangePointList[monsterOrangeIndex]
            if CommonDefs.IsNull(go) then
                local markTemplate = self.m_Wnd.transform:Find("Contents/BigMap/CurrentMap/Map/markTemplate").gameObject
                go = CommonDefs.Object_Instantiate(markTemplate)
                go.transform:Find("Table").gameObject:SetActive(false)
                local collider = go.transform:GetComponent(typeof(BoxCollider))
                collider.enabled = false
                self.monsterOrangePointList[monsterOrangeIndex] = go
            end
            local icon = go.transform:GetComponent(typeof(UISprite))
            icon.spriteName = "minimap_boss"
            monsterOrangeIndex = monsterOrangeIndex + 1
        elseif monsterCfgData.IconTypeInMap == 3 then
            if self.monsterBigIconNoHpList == nil then
                self.monsterBigIconNoHpList = {}
            end
            go = self.monsterBigIconNoHpList[monsterBigIconNoHpIndex]
            if CommonDefs.IsNull(go) then
                go = CResourceMgr.Inst:Instantiate(CMiniMapGuildLeagueBossResource.Inst, false, 10)
                go.transform:Find("Slider").gameObject:SetActive(false)
                go.transform:Find("Texture").localScale = Vector3(0.4,0.4,1)
                self.monsterBigIconNoHpList[monsterBigIconNoHpIndex] = go
            end
            go.transform:Find("Texture"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(monsterCfgData.HeadIcon, false)
            monsterBigIconNoHpIndex = monsterBigIconNoHpIndex + 1
        elseif monsterCfgData.IconTypeInMap == 4 then
            if self.monsterBigIconList == nil then
                self.monsterBigIconList = {}
            end
            go = self.monsterBigIconList[monsterBigIconIndex]
            if CommonDefs.IsNull(go) then
                go = CResourceMgr.Inst:Instantiate(CMiniMapGuildLeagueBossResource.Inst, false, 10)
                go.transform:Find("Slider").gameObject:SetActive(true)
                go.transform:Find("Texture").localScale = Vector3(1,1,1)
                self.monsterBigIconList[monsterBigIconIndex] = go
            end
            go.transform:Find("Texture"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(monsterCfgData.HeadIcon, false)
            go.transform:Find("Slider"):GetComponent(typeof(UISlider)).value = info.hp / info.hpFull
            monsterBigIconIndex = monsterBigIconIndex + 1
        end
        if go then
            go.transform.parent = self.m_Wnd.m_MapTexture.transform
            go.transform.localPosition = self.m_Wnd:CalculateMapPos(Utility.GridPos2WorldPos(info.posX or 0, info.posY or 0))
            go.transform.localRotation = Quaternion.Euler(0, 0, 0)
            go.transform.localScale = Vector3.one
            go.layer = self.m_Wnd.gameObject.layer
            go:SetActive(true)
        end
    end
end 