-- Auto Generated!!
local CChargeWnd = import "L10.UI.CChargeWnd"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local CLinyuShopWnd = import "L10.UI.CLinyuShopWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DateTime = import "System.DateTime"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local Mall_LingYuMallLimit = import "L10.Game.Mall_LingYuMallLimit"
local Mall_YuanBaoMall = import "L10.Game.Mall_YuanBaoMall"
local CItemMgr = import "L10.Game.CItemMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Object = import "System.Object"
local ShopMallTemlate = import "L10.UI.ShopMallTemlate"
local String = import "System.String"
local System = import "System"
local UInt32 = import "System.UInt32"
local EnumItemPlace = import "L10.Game.EnumItemPlace"



--将根据窗口的显示动态初始化和释放资源
CShopMallMgr.m_Init_CS2LuaHook = function ()
    if CShopMallMgr.Inst ~= nil then
        CShopMallMgr.ClearData()
        CShopMallMgr.Inst:InitLingYuMall()
        CShopMallMgr.Inst:InitYuanBaoMall()
    end
end
CShopMallMgr.m_ClearData_CS2LuaHook = function ()
    if CShopMallMgr.Inst ~= nil then
        CommonDefs.DictClear(CShopMallMgr.Inst.m_LingyuMallInfos)
        CommonDefs.DictClear(CShopMallMgr.Inst.m_YuanBaoMallInfos)
    end
end

CShopMallMgr.m_hookInitLingYuMall = function(this)
	Mall_LingYuMall.Foreach(function (k, v)
		local template = ShopMallTemlate()
		template.ItemId = v.ID
		template.Price = v.Jade
		template.Category = v.Category
		template.SubCategory = v.SubCategory
		template.PreSell = v.PreSell
		template.PreSellTime = v.PreSellTime
		template.Status = v.Status
		template.Index = v.Index
        template.FeiShengIndex = v.FeiShengIndex
        template.OwnerIndex = v.OwnerIndex
		template.Discount = v.Discount
		template.AvalibleCount = -1
		template.CanZengSong = v.CanZengSong
		template.ConfirmMessage = v.ConfirmMessage
		template.DeleteTime = v.DeleteTime
		template.CanUseBindJade = v.CanUseBindJade
		if not System.String.IsNullOrEmpty(v.DeleteTime) then
            local deleteTime = DateTime.Parse(v.DeleteTime)
			local currentTime = CServerTimeMgr.Inst:GetZone8Time()
			if DateTime.Compare(currentTime, deleteTime) > 0 then
				return
			end
		end
		local isNew = true
		if not System.String.IsNullOrEmpty(v.NewDeleteTime) then
			local newdeleteTime = DateTime.Parse(v.NewDeleteTime)
			local currentTime = CServerTimeMgr.Inst:GetZone8Time()
			if DateTime.Compare(currentTime, newdeleteTime) > 0 then isNew = false end
		end
		if template.Status < 3 or template.PreSell then
			for i = 0, template.Category.Length -1 do
                local category = template.Category[i]
				if not CommonDefs.DictContains(this.m_LingyuMallInfos, typeof(int), category) then
					CommonDefs.DictAdd(this.m_LingyuMallInfos, typeof(int), category, typeof(MakeGenericClass(List, ShopMallTemlate)), CreateFromClass(MakeGenericClass(List, ShopMallTemlate)))
				end
				if isNew or category ~= -1 then
					CommonDefs.ListAdd(this.m_LingyuMallInfos[category], typeof(ShopMallTemlate), template)
				end
			end
		end
	end)

	local vipLevel = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.Vip.Level or 0
	local limitCategory = Table2Array({5}, MakeArrayClass(int))
	Mall_LingYuMallLimit.Foreach(DelegateFactory.Action_object_object(function (k, v)
		local inchannal = CShopMallMgr.IsInChannal(v)
		if v.ShowInWelfare == 0 and v.ServerOnly == 0 and inchannal and v.VipLimit <= vipLevel then
			local template = ShopMallTemlate()
			template.ItemId = v.ID
            template.Price = v.Jade
            if v.Category then
                template.Category = v.Category
		        template.SubCategory = v.SubCategory
            else
                template.Category = limitCategory
            end
            template.ConfirmMessage = v.ConfirmMessage
		    template.SubCategory = v.SubCategory
			template.PreSell = v.PreSell
			template.PreSellTime = v.PreSellTime
			template.Status = v.Status
			template.Discount = v.Discount
			template.AvalibleCount = -1
			template.Index = v.Index
            template.FeiShengIndex = v.FeiShengIndex
			template.IsGiftPackage = v.IsGiftPackage
			template.RmbPID = v.RmbPID
			template.IsAllLifeLimit = v.IsAllLifeLimit
			template.IsMonthLimit = v.MonthLimit > 0
			template.DeleteTime = v.DeleteTime
			template.IsGlobalLimitRmbPackage = v.IsGlobalLimitRmbPackage
			template.CanUseBindJade = v.CanUseBindJade
			template.LimitGroup = v.LimitGroup
			template.LimitGroupDesc = v.LimitGroupDesc
            template.CanZengSong = v.CanZengSong
            local isNew = true
            if not System.String.IsNullOrEmpty(v.NewDeleteTime) then
                local newdeleteTime = DateTime.Parse(v.NewDeleteTime)
                local currentTime = CServerTimeMgr.Inst:GetZone8Time()
                if DateTime.Compare(currentTime, newdeleteTime) > 0 then isNew = false end
            end
			if template.Status < 3 or template.PreSell then
				for i = 0, template.Category.Length - 1 do
					local category = template.Category[i]
					if template.IsGiftPackage then
						category = 6
                    end
					if not CommonDefs.DictContains(this.m_LingyuMallInfos, typeof(int), category) then
                        
                        CommonDefs.DictAdd(this.m_LingyuMallInfos, typeof(int), category, typeof(MakeGenericClass(List, ShopMallTemlate)), CreateFromClass(MakeGenericClass(List, ShopMallTemlate)))
                    end
                    if isNew or category ~= -1 then
                         CommonDefs.ListAdd(this.m_LingyuMallInfos[category], typeof(ShopMallTemlate), template)
                    end
                    

				end
			end
		end
    end))
    LuaShopMallMgr.m_HasOwnIndexItemIds = {}
    if CClientMainPlayer.Inst then
        local bagSize = CClientMainPlayer.Inst.ItemProp:GetPlaceSize(EnumItemPlace.Bag)
        for i = 1, bagSize, 1 do
            local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
            local item = CItemMgr.Inst:GetById(id)
            if item and item.IsItem then
                local data = Mall_LingYuMall.GetData(item.TemplateId)
                if data then
                    LuaShopMallMgr.m_HasOwnIndexItemIds[item.TemplateId] = true
                end
            end
        end
    end
    local hasFeiSheng = CClientMainPlayer.Inst and CClientMainPlayer.Inst.HasFeiSheng or false
    CommonDefs.DictIterate(this.m_LingyuMallInfos, DelegateFactory.Action_object_object(function (___key, ___value)
        CommonDefs.ListSort1(___value, typeof(ShopMallTemlate), DelegateFactory.Comparison_ShopMallTemlate(function(a, b)
            local indexA = LuaShopMallMgr.m_HasOwnIndexItemIds[a.ItemId] and (a.OwnerIndex ~= 0 and a.OwnerIndex) or (hasFeiSheng and a.FeiShengIndex or a.Index) 
            local indexB = LuaShopMallMgr.m_HasOwnIndexItemIds[b.ItemId] and (b.OwnerIndex ~= 0 and b.OwnerIndex) or (hasFeiSheng and b.FeiShengIndex or b.Index) 
            return indexA - indexB
        end))
    end))
end

CShopMallMgr.m_hookHandleLingyuMallLimitAutoShangJia = function (itemInfos)
	if CShopMallMgr.Inst == nil then
		return
	end

	local vipLevel = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.Vip.Level or 0
	local limitCategory = Table2Array({5}, MakeArrayClass(int))
	for i = 0, itemInfos.Count - 1 do
		local itemId = itemInfos[i]

		local v = Mall_LingYuMallLimit.GetData(itemId)
		local inchannal = CShopMallMgr.IsInChannal(v)
		if v.ShowInWelfare <= 0 and v.ServerOnly <= 0 and inchannal and v.VipLimit <= vipLevel then
			local template = ShopMallTemlate()
			template.ItemId = v.ID
			template.Price = v.Jade
			template.Category = limitCategory
			template.PreSell = false
			template.Status = v.Status
			template.Discount = v.Discount
			template.AvalibleCount = -1
			template.Index = v.Index
            template.FeiShengIndex = v.FeiShengIndex
			template.IsGiftPackage = v.IsGiftPackage
			template.RmbPID = v.RmbPID
			template.IsAllLifeLimit = v.IsAllLifeLimit
            template.IsMonthLimit = v.MonthLimit > 0
			template.DeleteTime = v.DeleteTime
			template.IsGlobalLimitRmbPackage = v.IsGlobalLimitRmbPackage
			template.CanUseBindJade = v.CanUseBindJade
			template.LimitGroup = v.LimitGroup
			template.LimitGroupDesc = v.LimitGroupDesc
            template.CanZengSong = v.CanZengSong
            template.ConfirmMessage = v.ConfirmMessage
            if v.Category and v.Category.Length > 1 then
                template.Category = v.Category
                template.SubCategory = v.SubCategory
            end
			if template.Status == 3 then
				for j = 0, template.Category.Length - 1 do
                    local category = template.Category[j]
                    -- 如果是礼包，直接到礼包分类里面找
                    if v.IsGiftPackage then category = 6 end
                    if not CommonDefs.DictContains(CShopMallMgr.Inst.m_LingyuMallInfos, typeof(int), category) then
                        CommonDefs.DictAdd(CShopMallMgr.Inst.m_LingyuMallInfos, typeof(int), category, typeof(MakeGenericClass(List, ShopMallTemlate)), CreateFromClass(MakeGenericClass(List, ShopMallTemlate)))
                    end
                    local isFind = false
                    for k = 0, CShopMallMgr.Inst.m_LingyuMallInfos[category].Count - 1 do
                        if CShopMallMgr.Inst.m_LingyuMallInfos[category][k].ItemId == template.ItemId then
                            isFind = true
                            CShopMallMgr.Inst.m_LingyuMallInfos[category][k].PreSell = false -- 自动上架后取消预售
                            break
                        end
                    end
                    if not isFind then
                        CommonDefs.ListInsert(CShopMallMgr.Inst.m_LingyuMallInfos[category], 0, typeof(ShopMallTemlate), template)
                    end
				end
			end
		end
	end

	if CommonDefs.DictContains(CShopMallMgr.Inst.m_LingyuMallInfos, typeof(int), 5) then
		-- 最好也判断下再排序
		if CClientMainPlayer.Inst and CClientMainPlayer.Inst.HasFeiSheng then
			CommonDefs.ListSort1(CShopMallMgr.Inst.m_LingyuMallInfos[5], typeof(ShopMallTemlate), DelegateFactory.Comparison_ShopMallTemlate(function(a, b)
                return a.FeiShengIndex - b.FeiShengIndex
			end))
		else
			CommonDefs.ListSort1(CShopMallMgr.Inst.m_LingyuMallInfos[5], typeof(ShopMallTemlate), DelegateFactory.Comparison_ShopMallTemlate(function(a, b)
                return a.Index - b.Index
			end))
		end
	end
	if CommonDefs.DictContains(CShopMallMgr.Inst.m_LingyuMallInfos, typeof(int), 6) then
		 -- 可能没有礼包
		if CClientMainPlayer.Inst and CClientMainPlayer.Inst.HasFeiSheng then
			CommonDefs.ListSort1(CShopMallMgr.Inst.m_LingyuMallInfos[6], typeof(ShopMallTemlate), DelegateFactory.Comparison_ShopMallTemlate(function(a, b)
                return a.FeiShengIndex - b.FeiShengIndex
			end))
		else
			CommonDefs.ListSort1(CShopMallMgr.Inst.m_LingyuMallInfos[6], typeof(ShopMallTemlate), DelegateFactory.Comparison_ShopMallTemlate(function(a, b)
                return a.Index - b.Index
			end))
		end
	end

	if CShopMallMgr.OnLingyuMallAutoShangJiaUpdated ~= nil then
        GenericDelegateInvoke(CShopMallMgr.OnLingyuMallAutoShangJiaUpdated, Table2ArrayWithCount({true}, 1, MakeArrayClass(Object)))
	end
end

CShopMallMgr.m_hookHandleLingyuMallAutoShangJia = function (itemInfos)
	if CShopMallMgr.Inst == nil then
		return
	end

	for i = 0, itemInfos.Count - 1 do
		local itemId = itemInfos[i]
		local template = ShopMallTemlate()
		local v = Mall_LingYuMall.GetData(itemId)
		template.ItemId = v.ID
		template.Price = v.Jade
		template.Category = v.Category
		template.SubCategory = v.SubCategory
		template.PreSell = false
		template.Status = v.Status
		template.Index = v.Index
        template.FeiShengIndex = v.FeiShengIndex
        template.OwnerIndex = v.OwnerIndex
		template.Discount = v.Discount
		template.AvalibleCount = -1
		template.CanZengSong = v.CanZengSong
		template.DeleteTime = v.DeleteTime
		template.CanUseBindJade = v.CanUseBindJade
        template.ConfirmMessage = v.ConfirmMessage
		if template.Status == 3 then
			local toContinue = false
			if not System.String.IsNullOrEmpty(v.DeleteTime) then
				local deleteTime = DateTime.Parse(v.DeleteTime)
				local currentTime = CServerTimeMgr.Inst:GetZone8Time()
				if DateTime.Compare(currentTime, deleteTime) > 0 then
					toContinue = true
				end
			end
			if not toContinue then
				local isNew = true
				if not System.String.IsNullOrEmpty(v.NewDeleteTime) then
					local newdeleteTime = DateTime.Parse(v.NewDeleteTime)
					local currentTime = CServerTimeMgr.Inst:GetZone8Time()
					if DateTime.Compare(currentTime, newdeleteTime) > 0 then isNew = false end
				end

				for j = 0, template.Category.Length - 1 do
					local category = template.Category[j]
					if not CommonDefs.DictContains(CShopMallMgr.Inst.m_LingyuMallInfos, typeof(int), category) then
						CommonDefs.DictAdd(CShopMallMgr.Inst.m_LingyuMallInfos, typeof(int), category, typeof(MakeGenericClass(List, ShopMallTemlate)), CreateFromClass(MakeGenericClass(List, ShopMallTemlate)))
					end
					local isFind = false
					for k = 0, CShopMallMgr.Inst.m_LingyuMallInfos[category].Count - 1 do
						if CShopMallMgr.Inst.m_LingyuMallInfos[category][k].ItemId == template.ItemId then
							isFind = true
							CShopMallMgr.Inst.m_LingyuMallInfos[category][k].PreSell = false -- 自动上架后取消预售
							break
						end
					end
					if not isFind and (isNew or category ~= -1) then
						CommonDefs.ListInsert(CShopMallMgr.Inst.m_LingyuMallInfos[category], 0, typeof(ShopMallTemlate), template)
					end
				end
			end
		end
	end

	if CClientMainPlayer.Inst and CClientMainPlayer.Inst.HasFeiSheng then
		CommonDefs.DictIterate(CShopMallMgr.Inst.m_LingyuMallInfos, DelegateFactory.Action_object_object(function (___key, ___value)
			CommonDefs.ListSort1(___value, typeof(ShopMallTemlate), DelegateFactory.Comparison_ShopMallTemlate(function(a, b)
				return a.FeiShengIndex - b.FeiShengIndex
			end))

		end))
	else
		CommonDefs.DictIterate(CShopMallMgr.Inst.m_LingyuMallInfos, DelegateFactory.Action_object_object(function (___key, ___value)
			CommonDefs.ListSort1(___value, typeof(ShopMallTemlate), DelegateFactory.Comparison_ShopMallTemlate(function(a, b)
				return a.Index - b.Index
			end))

		end))
	end

	if CShopMallMgr.OnLingyuMallAutoShangJiaUpdated ~= nil then
        GenericDelegateInvoke(CShopMallMgr.OnLingyuMallAutoShangJiaUpdated, Table2ArrayWithCount({true}, 1, MakeArrayClass(Object)))
	end
end


CShopMallMgr.m_UpdateLingYuLimit_CS2LuaHook = function (limitInfos)
    if CShopMallMgr.Inst ~= nil then
        local lingyuLimits = CShopMallMgr.GetLingYuMallInfo(5, 0)
        local giftLimits = CShopMallMgr.GetLingYuMallInfo(6, 0)
        local lingyuLimits2 = CShopMallMgr.GetLingYuMallInfo(3, 0)
        --限购信息可能在礼包里也要更新
        do
            local i = 0
            while i < limitInfos.Count do
                local key = math.floor(tonumber(limitInfos[i] or 0))
                local val = math.floor(tonumber(limitInfos[i + 1] or 0))
                local find = false
                do
                    local j = 0
                    while j < lingyuLimits.Count do
                        if lingyuLimits[j].ItemId == key then
                            lingyuLimits[j].AvalibleCount = val
                            find = true
                            break
                        end
                        j = j + 1
                    end
                end
                if not find then
                    do
                        local j = 0
                        while j < giftLimits.Count do
                            if giftLimits[j].ItemId == key then
                                giftLimits[j].AvalibleCount = val
                                find = true
                                break
                            end
                            j = j + 1
                        end
                    end
                end
                for k = 0, lingyuLimits2.Count - 1 do
                    if lingyuLimits2[k].ItemId == key then
                        lingyuLimits2[k].AvalibleCount = val
                        break
                    end
                end
                i = i + 2
            end
        end
    end
    if CShopMallMgr.OnLingyuLimitUpdated ~= nil then
        GenericDelegateInvoke(CShopMallMgr.OnLingyuLimitUpdated, Table2ArrayWithCount({true}, 1, MakeArrayClass(Object)))
    end
	g_ScriptEvent:BroadcastInLua("OnLingyuLimitUpdatedForSearchTemplateId")
end
CShopMallMgr.m_UpdateYuanBaoLimit_CS2LuaHook = function (limitInfos)
    if CShopMallMgr.Inst ~= nil then
        do
            local i = 0
            while i < limitInfos.Count do
                local key = math.floor(tonumber(limitInfos[i] or 0))
                local val = math.floor(tonumber(limitInfos[i + 1] or 0))

                local find = false
                CommonDefs.EnumerableIterate(CShopMallMgr.Inst.m_YuanBaoMallInfos.Values, DelegateFactory.Action_object(function (___value)
                    local v = ___value
                    do
                        local j = 0
                        while j < v.Count do
                            if v[j].ItemId == key then
                                v[j].AvalibleCount = val
                                find = true
                                break
                            end
                            j = j + 1
                        end
                    end
                    if find then
                        return
                    end
                end))
                i = i + 2
            end
        end
        if CShopMallMgr.OnYuanbaoLimitUpdated ~= nil then
            GenericDelegateInvoke(CShopMallMgr.OnYuanbaoLimitUpdated, Table2ArrayWithCount({true}, 1, MakeArrayClass(Object)))
        end
    end
end
CShopMallMgr.m_GetLingYuMallInfo_CS2LuaHook = function (category, subCategory)
    local giftList = CLinyuShopWndHolder:GetGiftList(category)
    if giftList then return giftList end

    local ret = nil
    if CShopMallMgr.Inst ~= nil then
        (function ()
            local __try_get_result
            __try_get_result, ret = CommonDefs.DictTryGet(CShopMallMgr.Inst.m_LingyuMallInfos, typeof(Int32), category, typeof(MakeGenericClass(List, ShopMallTemlate)))
            return __try_get_result
        end)()
        if ret ~= nil and ret.Count > 0 and subCategory > 0 then
            ret = CommonDefs.ListFindAll(ret, DelegateFactory.Predicate_ShopMallTemlate(function (t)
                return t.SubCategory == subCategory
            end))
        end
    end
    local default
    if ret ~= nil then
        default = ret
    else
        default = CreateFromClass(MakeGenericClass(List, ShopMallTemlate))
    end
	if category == CLinyuShopWnd.LINGYU_GIFT_CATEGORY then
		CommonDefs.ListSort1(default, typeof(ShopMallTemlate), DelegateFactory.Comparison_ShopMallTemlate(function(a, b)
			if b.AvalibleCount == 0 and a.AvalibleCount == 0 then
				return a.Index - b.Index
			elseif b.AvalibleCount == 0 then
				return -1
			elseif a.AvalibleCount == 0 then
				return 1
			else
				return a.Index - b.Index
			end
        end))
    else
        local hasFeiSheng = CClientMainPlayer.Inst and CClientMainPlayer.Inst.HasFeiSheng or false
        CommonDefs.ListSort1(default, typeof(ShopMallTemlate), DelegateFactory.Comparison_ShopMallTemlate(function(a, b)
            local indexA = LuaShopMallMgr.m_HasOwnIndexItemIds[a.ItemId] and (a.OwnerIndex ~= 0 and a.OwnerIndex) or (hasFeiSheng and a.FeiShengIndex or a.Index) 
            local indexB = LuaShopMallMgr.m_HasOwnIndexItemIds[b.ItemId] and (b.OwnerIndex ~= 0 and b.OwnerIndex) or (hasFeiSheng and b.FeiShengIndex or b.Index) 
            return indexA - indexB
        end))
	end
    return default
end
CShopMallMgr.m_GetLingYuMallSubCategory_CS2LuaHook = function (category)
    local subcategorys = {}
    if CShopMallMgr.Inst then
        local __try_get_result, tmp = CommonDefs.DictTryGet(CShopMallMgr.Inst.m_LingyuMallInfos, typeof(Int32), category, typeof(MakeGenericClass(List, ShopMallTemlate)))
        if tmp then
            for i = 0, tmp.Count - 1 do
                local subCategory = tmp[i].SubCategory
                if subCategory > 0 then
                    subcategorys[subCategory] = true
                end
            end
        end
    end
    local ret = {0}
    for subCategory,_ in pairs(subcategorys) do
        table.insert(ret, subCategory)
    end
    return (#ret > 1) and Table2List(ret, MakeGenericClass(List, Int32)) or InitializeList(CreateFromClass(MakeGenericClass(List, Int32)), Int32, 0)
end
CShopMallMgr.m_ShowLinyuShoppingMall_CS2LuaHook = function (templateId)
    CShopMallMgr.SelectMallIndex = 0
    CShopMallMgr.SelectCategory = CShopMallMgr.GetItemCategoryOfLinyuShop(templateId)
    CShopMallMgr.SearchTemplateId = templateId

	if (CShopMallMgr.SelectCategory == 3) or (CShopMallMgr.SelectCategory == 2) then
		local mall = Mall_LingYuMall.GetData(templateId)
		if mall then
			CShopMallMgr.SelectSubCategory = mall.SubCategory
		end
	end

    CUIManager.ShowUI(CUIResources.ShoppingMallWnd)
end
CShopMallMgr.m_ShowYuanbaoShoppingMall_CS2LuaHook = function (templateId)
    CShopMallMgr.SelectMallIndex = 1
    CShopMallMgr.SelectCategory = CShopMallMgr.GetItemCategoryOfYuanbaoShop(templateId)
    CShopMallMgr.SearchTemplateId = templateId
    CUIManager.ShowUI(CUIResources.ShoppingMallWnd)
end
CShopMallMgr.m_ShowChargePrivilegeWnd_CS2LuaHook = function (vipLevel)
    if vipLevel <= 0 then
        vipLevel = 1
    end
    if vipLevel >= CChargeWnd.MAX_VIP_LEVEL then
        vipLevel = CChargeWnd.MAX_VIP_LEVEL
    end
    CShopMallMgr.VipPrivilegeLevel = vipLevel
    CUIManager.ShowUI(CIndirectUIResources.ChargePrivilegeWnd)
end
CShopMallMgr.m_ShowExchangeQnPointWnd_CS2LuaHook = function (fullExchange, cancelAction)
    if fullExchange then
        local qnPoint = 0
        if CClientMainPlayer.Inst ~= nil then
            qnPoint = CClientMainPlayer.Inst.QnPoint
        end
        local msg = g_MessageMgr:FormatMessage("MSG_EXCHANGE_DIANSHU", qnPoint)
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
            Gac2Gas.RequestExchangeQnPoint2Jade(qnPoint)
        end), cancelAction, nil, nil, false)
    else
        CUIManager.ShowUI(CIndirectUIResources.QnPointExchangeWnd)
    end
end
CShopMallMgr.m_GetItemCategoryOfLinyuShop_CS2LuaHook = function (templateId)
    local ret = 0
    local data = Mall_LingYuMall.GetData(templateId)
    if data ~= nil then
        do
            local i = 0
            while i < data.Category.Length do
                if data.Category[i] > 0 then
                    return data.Category[i]
                end
                i = i + 1
            end
        end
    else
        local data2 = Mall_LingYuMallLimit.GetData(templateId)
        if data2 ~= nil then
			if data2.Category then
				for i = 0, data2.Category.Length - 1 do
					if data2.Category[i] > 0 then
						return data2.Category[i]
					end
				end
			end
            ret = data2.IsGiftPackage and CLinyuShopWnd.LINGYU_GIFT_CATEGORY or CLinyuShopWnd.LINGYU_LIMIT_CATEGORY
        end
    end
    return ret
end
CShopMallMgr.m_GetItemCategoryOfYuanbaoShop_CS2LuaHook = function (templateId)
    local ret = 0
    local data = Mall_YuanBaoMall.GetData(templateId)
    if data ~= nil then
        do
            local i = 0
            while i < data.Category.Length do
                if data.Category[i] > 0 then
                    return data.Category[i]
                end
                i = i + 1
            end
        end
    end
    return ret
end
CShopMallMgr.m_TryCheckEnoughJade_CS2LuaHook = function (needJade, templateId)
    local jade = 0
    local qnPoint = 0
    if CClientMainPlayer.Inst ~= nil then
        if templateId == System.UInt32.MaxValue or (templateId > 0 and CShopMallMgr.CanUseBindJadeBuy(templateId)) then
            jade = CClientMainPlayer.Inst.Jade + CClientMainPlayer.Inst.BindJade
        else
            jade = CClientMainPlayer.Inst.Jade
        end
        qnPoint = CClientMainPlayer.Inst.QnPoint
    end
    if needJade > jade or needJade > qnPoint + jade then
        if needJade > qnPoint + jade then
            local txt = g_MessageMgr:FormatMessage("NotEnough_Jade", qnPoint)
            MessageWndManager.ShowOKCancelMessage(txt, DelegateFactory.Action(function ()
                CShopMallMgr.ShowChargeWnd()
            end), nil, LocalString.GetString("充值"), LocalString.GetString("取消"), false)
        else
            CUIManager.ShowUI(CIndirectUIResources.QnPointExchangeWnd)
        end
        return false
    end
    return true
end
CShopMallMgr.m_CanUseBindJadeBuy_CS2LuaHook = function (templateId)
    local data = Mall_LingYuMall.GetData(templateId)
    if data ~= nil then
        return data.CanUseBindJade > 0
    end
    local data2 = Mall_LingYuMallLimit.GetData(templateId)
    if data2 ~= nil then
        return data2.CanUseBindJade > 0
    end
    return false
end



CShopMallMgr.m_GetLimitCount_CS2LuaHook = function (this, templateId)
    if CommonDefs.DictContains(this.LimitInfoLookup, typeof(UInt32), templateId) then
        return CommonDefs.DictGetValue(this.LimitInfoLookup, typeof(UInt32), templateId)
    end
    return 0
end
CShopMallMgr.m_GetDiscountItemIds_CS2LuaHook = function (this)
    local ids = CreateFromClass(MakeGenericClass(List, UInt32))

    local currentTime = CServerTimeMgr.Inst:GetZone8Time()

    Mall_LingYuMallLimit.Foreach(DelegateFactory.Action_object_object(function (key, val)
        if val.ShowInWelfare > 0 and val.ServerOnly == 0 and val.Status == 0 then
            if System.String.IsNullOrEmpty(val.DeleteTime) then
                CommonDefs.ListAdd(ids, typeof(UInt32), key)
            else
                local deleteTime = DateTime.Parse(val.DeleteTime)
                if CommonDefs.op_LessThanOrEqual_DateTime_DateTime(currentTime, deleteTime) then
                    CommonDefs.ListAdd(ids, typeof(UInt32), key)
                end
            end
        end
    end))
    return ids
end
CShopMallMgr.m_EnableEvent_CS2LuaHook = function (this)
    EventManager.AddListenerInternal(EnumEventType.SendMallMarketItemLimitUpdate, MakeDelegateFromCSFunction(this.UpdateMarketLimit, MakeGenericClass(Action2, Int32, MakeGenericClass(List, Object)), this))
    EventManager.AddListenerInternal(EnumEventType.AutoShangJiaItemUpdate, MakeDelegateFromCSFunction(this.OnAutoShangJiaItemUpdate, MakeGenericClass(Action2, Int32, MakeGenericClass(List, UInt32)), this))
    EventManager.AddListenerInternal(EnumEventType.NotifyChargeDone, MakeDelegateFromCSFunction(this.OnNotifyChargeDone, MakeGenericClass(Action2, String, String), this))
end
CShopMallMgr.m_UpdateMarketLimit_CS2LuaHook = function (this, regionId, limitInfo)
    do
        local i = 0
        while i < limitInfo.Count do
            local key = math.floor(tonumber(limitInfo[i] or 0))
            local val = math.floor(tonumber(limitInfo[i + 1] or 0))
            if CommonDefs.DictContains(CShopMallMgr.Inst.LimitInfoLookup, typeof(UInt32), key) then
                CommonDefs.DictSet(CShopMallMgr.Inst.LimitInfoLookup, typeof(UInt32), key, typeof(Int32), val)
            else
                CommonDefs.DictAdd(CShopMallMgr.Inst.LimitInfoLookup, typeof(UInt32), key, typeof(Int32), val)
            end
            i = i + 2
        end
    end
    EventManager.Broadcast(EnumEventType.UpdateMallLimit)
end
CShopMallMgr.m_OnAutoShangJiaItemUpdate_CS2LuaHook = function (this, regionId, itemInfos)
    if regionId == EShopMallRegion_lua.ELingyuMallLimit then
        local dynamicItems = CreateFromClass(MakeGenericClass(List, UInt32))
        do
            local i = 0
            while i < itemInfos.Count do
                local itemId = itemInfos[i]
                local v = Mall_LingYuMallLimit.GetData(itemId)
                if v ~= nil then
                    if v.ShowInWelfare > 0 then
                        CommonDefs.ListAdd(dynamicItems, typeof(UInt32), itemId)
                    end
                end
                i = i + 1
            end
        end
        if dynamicItems.Count > 0 then
            EventManager.BroadcastInternalForLua(EnumEventType.UpdateShangXiaJiaZheKouLiBao, {dynamicItems})
        end
    end
end

LuaShopMallMgr = {}
LuaShopMallMgr.m_HasOwnIndexItemIds = {}
LuaShopMallMgr.TryCheckEnoughJade = function (needJade, useBind)
    if CClientMainPlayer.Inst then
        local jade = CClientMainPlayer.Inst.Jade
        local qnPoint = CClientMainPlayer.Inst.QnPoint
        if useBind then
            jade = jade + CClientMainPlayer.Inst.BindJade
        end
        if needJade > jade or needJade > qnPoint + jade then
            if needJade > qnPoint + jade then
                local txt = g_MessageMgr:FormatMessage("NotEnough_Jade", qnPoint)
                MessageWndManager.ShowOKCancelMessage(txt, DelegateFactory.Action(function ()
                    CShopMallMgr.ShowChargeWnd()
                end), nil, LocalString.GetString("充值"), LocalString.GetString("取消"), false)
            else
                CUIManager.ShowUI(CIndirectUIResources.QnPointExchangeWnd)
            end
            return false
        end
        return true
    end
    return false
end
