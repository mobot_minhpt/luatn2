-- Auto Generated!!
local CAuctionMgr = import "L10.Game.CAuctionMgr"
local CAuctionShelfDetailWnd = import "L10.UI.CAuctionShelfDetailWnd"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CAuctionShelfDetailWnd.m_Init_CS2LuaHook = function (this) 
    UIEventListener.Get(this.CancelButton).onClick = MakeDelegateFromCSFunction(this.onCancelClick, VoidDelegate, this)
    UIEventListener.Get(this.OffShelfButton).onClick = MakeDelegateFromCSFunction(this.onOffShelfClick, VoidDelegate, this)
    this.info = CAuctionMgr.Inst:GetCurrentItemInfo(CAuctionMgr.Inst.FocusInstanceID)
    if this.info == nil then
        return
    end

    local item = CAuctionMgr.Inst:GetItem(this.info.InstanceID)
    this.NameLabel.text = item.Name
    if item.IsItem then
        this.NameLabel.color = item.Item.Color
    elseif item.IsEquip then
        this.NameLabel.color = item.Equip.DisplayColor
    end
    this.IconTexture:LoadMaterial(item.Icon)
    this.PriceLabel.text = tostring(this.info.FixPrice)

    local priceRoot = this.transform:Find("Container/PriceRoot")
    local fixPriceRoot = this.transform:Find("Container/FixPriceRoot")

    -- 不同情况的layout
    if LuaAuctionMgr.NeedShowStartPrice(item.TemplateId) then
        priceRoot.gameObject:SetActive(true)
        local priceLabel = priceRoot:Find("PriceBGSprite/Label"):GetComponent(typeof(UILabel))
        priceLabel.text = this.info.Price
        LuaUtils.SetLocalPosition(priceRoot.transform, 0, -19, 0)
        LuaUtils.SetLocalPosition(fixPriceRoot.transform, 0, 28, 0)
    else
        priceRoot.gameObject:SetActive(false)
        LuaUtils.SetLocalPosition(fixPriceRoot.transform, 0, -19, 0)
    end
        
end
CAuctionShelfDetailWnd.m_onPriceChange_CS2LuaHook = function (this, id, newPrice, ownPrice) 
    if id == this.info.AuctionID then
        this.PriceLabel.text = tostring(this.info.Price)
    end
end
