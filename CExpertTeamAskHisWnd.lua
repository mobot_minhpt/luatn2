-- Auto Generated!!
local Boolean = import "System.Boolean"
local CExpertTeam_Basic_Ret = import "L10.Game.CExpertTeam_Basic_Ret"
local CExpertTeamAskHisWnd = import "L10.UI.CExpertTeamAskHisWnd"
local CExpertTeamMgr = import "L10.Game.CExpertTeamMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local L10 = import "L10"
local NGUITools = import "NGUITools"
local Object = import "UnityEngine.Object"
local Object1 = import "System.Object"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UInt64 = import "System.UInt64"
CExpertTeamAskHisWnd.m_Init_CS2LuaHook = function (this) 
    this.templateNode:SetActive(false)
    Extensions.RemoveAllChildren(this.table.transform)

    UIEventListener.Get(this.backBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
        this:Close()
    end)

    CExpertTeamMgr.Inst:GetMyQuestion(MakeDelegateFromCSFunction(this.GetMyQuestion, MakeGenericClass(Action1, String), this), nil)
end
CExpertTeamAskHisWnd.m_DeleteQuestionBack_CS2LuaHook = function (this, qid) 
    if this.saveNode ~= nil then
        this.saveNode.transform.parent = nil
        Object.Destroy(this.saveNode)
        this.table:Reposition()
        this.scrollView:ResetPosition()
    end
end
CExpertTeamAskHisWnd.m_GetMyQuestion_CS2LuaHook = function (this, ret) 
    if not this then
        return
    end

    local retDic = TypeAs(L10.Game.Utils.Json.Deserialize(ret), typeof(MakeGenericClass(Dictionary, String, Object1)))
    local result = CommonDefs.DictGetValue(retDic, typeof(String), "result")
    local data = TypeAs(CommonDefs.DictGetValue(retDic, typeof(String), "data"), typeof(MakeGenericClass(List, Object1)))
    if result then
        if data ~= nil and data.Count > 0 then
            do
                local i = 0
                while i < data.Count do
                    local info = TypeAs(data[i], typeof(MakeGenericClass(Dictionary, String, Object1)))

                    local node = NGUITools.AddChild(this.table.gameObject, this.templateNode)
                    node:SetActive(true)

                    local acceptNode = node.transform:Find("accept").gameObject
                    local adopt = System.Int32.Parse(ToStringWrap(CommonDefs.DictGetValue(info, typeof(String), "adopt")))
                    if adopt == 1 then
                        acceptNode:SetActive(true)
                    else
                        acceptNode:SetActive(false)
                    end

                    local showContent = ToStringWrap(CommonDefs.DictGetValue(info, typeof(String), "content"))
                    local detailLabel = CommonDefs.GetComponent_Component_Type(node.transform:Find("detail"), typeof(UILabel))
                    CExpertTeamMgr.SetUILabelText(detailLabel, showContent)

                    local qid = System.UInt64.Parse(ToStringWrap(CommonDefs.DictGetValue(info, typeof(String), "id")))

                    CommonDefs.GetComponent_Component_Type(node.transform:Find("answer"), typeof(UILabel)).text = g_MessageMgr:FormatMessage("JINGLINGEXPERT_REPLY_AMOUNT", ToStringWrap(CommonDefs.DictGetValue(info, typeof(String), "answer_number")))
                    local redpoint = node.transform:Find("RedPoint").gameObject
                    if CommonDefs.DictContains(CExpertTeamMgr.Inst.NewAnswerArriveQuestion, typeof(UInt64), qid) and CommonDefs.DictGetValue(CExpertTeamMgr.Inst.NewAnswerArriveQuestion, typeof(UInt64), qid) then
                        redpoint:SetActive(true)
                    else
                        redpoint:SetActive(false)
                    end

                    local resendBtn = node.transform:Find("AnotherBtn").gameObject
                    local invalid = System.Int32.Parse(ToStringWrap(CommonDefs.DictGetValue(info, typeof(String), "invalid")))
                    if invalid == 1 then
                        resendBtn:SetActive(true)
                        UIEventListener.Get(resendBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
                            this:ResendQuestion(go, qid, System.Int32.Parse(ToStringWrap(CommonDefs.DictGetValue(info, typeof(String), "reward_score"))))
                        end)
                    else
                        resendBtn:SetActive(false)
                    end

                    local bgBtn = node.transform:Find("bgbutton").gameObject
                    UIEventListener.Get(bgBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
                        this:OpenDetailWnd(qid)
                        this.saveNode = node
                        if CommonDefs.DictContains(CExpertTeamMgr.Inst.NewAnswerArriveQuestion, typeof(UInt64), qid) and CommonDefs.DictGetValue(CExpertTeamMgr.Inst.NewAnswerArriveQuestion, typeof(UInt64), qid) then
                            redpoint:SetActive(false)
                            CommonDefs.DictSet(CExpertTeamMgr.Inst.NewAnswerArriveQuestion, typeof(UInt64), qid, typeof(Boolean), false)
                            EventManager.Broadcast(EnumEventType.OnJingLingIMReadStatusChanged)
                        end
                    end)
                    i = i + 1
                end
            end

            this.table:Reposition()
            this.scrollView:ResetPosition()
        end
    end
end
CExpertTeamAskHisWnd.m_ResendQuestionBack_CS2LuaHook = function (this, ret) 
    if not this then
        return
    end
    local objRet = CPersonalSpaceMgr.Inst:DeserializeJson2Object(typeof(CExpertTeam_Basic_Ret), ret, 0)
    local retData = objRet
    if retData.result then
        if not System.String.IsNullOrEmpty(retData.data.msg) then
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", retData.data.msg)
        end
        if this.saveGo ~= nil then
            this.saveGo:SetActive(false)
        end
    end
end
