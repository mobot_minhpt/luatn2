local DelegateFactory = import "DelegateFactory"
local CButton         = import "L10.UI.CButton"
local CServerTimeMgr  = import "L10.Game.CServerTimeMgr"

LuaWorldCup2022GJZLSelectWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaWorldCup2022GJZLSelectWnd, "topLabel")
RegistClassMember(LuaWorldCup2022GJZLSelectWnd, "table")
RegistClassMember(LuaWorldCup2022GJZLSelectWnd, "skillIcon")
RegistClassMember(LuaWorldCup2022GJZLSelectWnd, "skillName")
RegistClassMember(LuaWorldCup2022GJZLSelectWnd, "skillDesc")
RegistClassMember(LuaWorldCup2022GJZLSelectWnd, "skillButton")

RegistClassMember(LuaWorldCup2022GJZLSelectWnd, "curSelect")
RegistClassMember(LuaWorldCup2022GJZLSelectWnd, "tick")

function LuaWorldCup2022GJZLSelectWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()

    UIEventListener.Get(self.skillButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSkillButtonClick()
	end)
end

-- 初始化UI组件
function LuaWorldCup2022GJZLSelectWnd:InitUIComponents()
    self.topLabel = self.transform:Find("Top"):GetComponent(typeof(UILabel))
    self.table = self.transform:Find("WorldCup2022SelectCommon/Table")
    self.skillIcon = self.transform:Find("Skill/Mask/Icon"):GetComponent(typeof(CUITexture))
    self.skillName = self.transform:Find("Skill/Name"):GetComponent(typeof(UILabel))
    self.skillDesc = self.transform:Find("Skill/Desc"):GetComponent(typeof(UILabel))
    self.skillButton = self.transform:Find("Skill/Button").gameObject
end

function LuaWorldCup2022GJZLSelectWnd:OnEnable()
    g_ScriptEvent:AddListener("GJZL_SyncSelectPosition", self, "OnSyncSelectPosition")
    g_ScriptEvent:AddListener("GJZL_SelectPositionSuccess", self, "OnSelectPositionSuccess")
end

function LuaWorldCup2022GJZLSelectWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GJZL_SyncSelectPosition", self, "OnSyncSelectPosition")
    g_ScriptEvent:RemoveListener("GJZL_SelectPositionSuccess", self, "OnSelectPositionSuccess")
end

function LuaWorldCup2022GJZLSelectWnd:OnSyncSelectPosition()
    self:UpdatePos()
    self:UpdateSkillButtonActive()
end

function LuaWorldCup2022GJZLSelectWnd:OnSelectPositionSuccess()
    self:UpdatePos()
end


function LuaWorldCup2022GJZLSelectWnd:Init()
    self:InitTable()
    self:UpdatePos()

    local posInfo = LuaWorldCup2022Mgr.GJZLInfo.posInfo or {}
    local myPos = LuaWorldCup2022Mgr.GJZLInfo.myPos
    local default
    if myPos then
        default = myPos
    else
        local selectableIds = {}
        for i = 1, 5 do
            if not posInfo[i] then
                table.insert(selectableIds, i)
            end
        end
        if #selectableIds > 0 then
            default = selectableIds[math.random(#selectableIds)]
        else
            default = math.random(5)
        end
    end

    self:OnPlaceClick(default)

    self:ClearTick()
    self:UpdateTopLabel()
    self.tick = RegisterTick(function()
        self:UpdateTopLabel()
    end, 1000)
end

-- 初始化所有位置
function LuaWorldCup2022GJZLSelectWnd:InitTable()
    for i = 1, 5 do
        local child = self.table:GetChild(i - 1)
        local place = child:Find("Place"):GetComponent(typeof(UILabel))
        place.text = WorldCup2022_GJZLPlace.GetData(i).Name
        child:Find("Highlight").gameObject:SetActive(false)
        child:Find("Select").gameObject:SetActive(false)

        UIEventListener.Get(child.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnPlaceClick(i)
        end)
    end
end

-- 更新位置信息
function LuaWorldCup2022GJZLSelectWnd:UpdatePos()
    local posInfo = LuaWorldCup2022Mgr.GJZLInfo.posInfo or {}
    local myPos = LuaWorldCup2022Mgr.GJZLInfo.myPos

    for i = 1, 5 do
        local child = self.table:GetChild(i - 1)
        local name = child:Find("Name"):GetComponent(typeof(UILabel))
        local outline = child:Find("Outline").gameObject
        local portrait = child:GetComponent(typeof(CUITexture))
        local select = child:Find("Select").gameObject

        name.color = NGUIText.ParseColor24("FFFFFF", 0)
        if posInfo[i] then
            name.text = posInfo[i][1]
            name.alpha = 1
            if myPos and myPos == i then
                name.color = NGUIText.ParseColor24("00FF60", 0)
                select:SetActive(true)
            else
                select:SetActive(false)
            end
            outline.gameObject:SetActive(false)
            portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(posInfo[i][2], posInfo[i][3], -1), false)
        else
            select:SetActive(false)
            name.text = LocalString.GetString("待就位")
            name.alpha = 0.6
            outline.gameObject:SetActive(true)
            portrait:LoadMaterial("")
        end
    end
end

-- 更新是否显示按钮
function LuaWorldCup2022GJZLSelectWnd:UpdateSkillButtonActive()
    local posInfo = LuaWorldCup2022Mgr.GJZLInfo.posInfo or {}

    if posInfo[self.curSelect] then
        self.skillButton:SetActive(false)
    else
        self.skillButton:SetActive(true)
    end
end

function LuaWorldCup2022GJZLSelectWnd:UpdateTopLabel()
    local leftTime = math.floor(LuaWorldCup2022Mgr.GJZLInfo.selectEndTimeStamp - CServerTimeMgr.Inst.timeStamp)

    if leftTime <= 0 then
        CUIManager.CloseUI(CLuaUIResources.WorldCup2022GJZLSelectWnd)
        return
    end

    self.topLabel.text = SafeStringFormat3(LocalString.GetString("选择位置获得额外技能(%d秒)"), leftTime)
end

function LuaWorldCup2022GJZLSelectWnd:ClearTick()
    if self.tick then
        UnRegisterTick(self.tick)
    end
end

function LuaWorldCup2022GJZLSelectWnd:OnDestroy()
    self:ClearTick()
end

--@region UIEvent

function LuaWorldCup2022GJZLSelectWnd:OnSkillButtonClick()
    Gac2Gas.GJZL_SelectPosition(self.curSelect)
end

function LuaWorldCup2022GJZLSelectWnd:OnPlaceClick(i)
    if self.curSelect and self.curSelect == i then return end

    if self.curSelect then
        local before = self.table:GetChild(self.curSelect - 1)
        before:Find("Highlight").gameObject:SetActive(false)
    end

    self.curSelect = i
    local now = self.table:GetChild(self.curSelect - 1)
    now:Find("Highlight").gameObject:SetActive(true)

    local data = WorldCup2022_GJZLPlace.GetData(i)
    self.skillDesc.text = data.SkillDesc

    local skillData = Skill_AllSkills.GetData(data.SkillId)
    self.skillName.text = SafeStringFormat3(LocalString.GetString("%s技能 %s"), data.Name, skillData.Name)
    self.skillIcon:LoadSkillIcon(skillData.SkillIcon)
    self:UpdateSkillButtonActive()
    self.skillButton.transform:GetComponent(typeof(CButton)).Text = SafeStringFormat3(LocalString.GetString("成为%s"), data.Name)
end

--@endregion UIEvent
