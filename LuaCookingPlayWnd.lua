local EventDelegate=import "EventDelegate"

local CUIFx = import "L10.UI.CUIFx"
local CUITexture = import "L10.UI.CUITexture"
local UISlider = import "UISlider"
local Animation = import "UnityEngine.Animation"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local Extensions = import "Extensions"
local TweenAlpha = import "TweenAlpha"
local TweenPosition = import "TweenPosition"
local TweenScale = import "TweenScale"
local CCenterCountdownWnd = import "L10.UI.CCenterCountdownWnd"
local ZhuJueJuQing_Cooking = import "L10.Game.ZhuJueJuQing_Cooking"
local CUICommonDef = import "L10.UI.CUICommonDef"
local COpenEntryMgr = import "L10.Game.COpenEntryMgr"

CLuaCookingPlayWnd = class()
RegistClassMember(CLuaCookingPlayWnd, "m_EnablePlayerShake")
RegistClassMember(CLuaCookingPlayWnd, "m_ShakeCount")
RegistClassMember(CLuaCookingPlayWnd, "m_Table")
RegistClassMember(CLuaCookingPlayWnd, "m_MaterialTemplate")
RegistClassMember(CLuaCookingPlayWnd, "m_Fx")
RegistClassMember(CLuaCookingPlayWnd, "m_Dish")
RegistClassMember(CLuaCookingPlayWnd, "m_Silder")
RegistClassMember(CLuaCookingPlayWnd, "m_SilderText")
RegistClassMember(CLuaCookingPlayWnd, "m_Anchor")
RegistClassMember(CLuaCookingPlayWnd, "m_Background")
RegistClassMember(CLuaCookingPlayWnd, "m_Shake")
RegistClassMember(CLuaCookingPlayWnd, "m_ShakeAni")
RegistClassMember(CLuaCookingPlayWnd, "m_MaterialTemplateTbl")
RegistClassMember(CLuaCookingPlayWnd, "m_TempCookIcon")
RegistClassMember(CLuaCookingPlayWnd, "m_DelayStartTick")
RegistClassMember(CLuaCookingPlayWnd, "m_PlayTweenTickTbl")
RegistClassMember(CLuaCookingPlayWnd, "m_FinishShakeFunc")
RegistClassMember(CLuaCookingPlayWnd, "m_PlayerShakeFunc")
RegistClassMember(CLuaCookingPlayWnd, "m_GuideTemplate")

function CLuaCookingPlayWnd:Init()
	self.m_Table = self.transform:Find("Anchor/Table"):GetComponent(typeof(UITable))
	self.m_MaterialTemplate = self.transform:Find("Anchor/MaterialTemplate").gameObject
	self.m_Fx = self.transform:Find("Anchor/Fx"):GetComponent(typeof(CUIFx))
	self.m_Dish = self.transform:Find("ShakeAnchor/Shake"):GetComponent(typeof(CUITexture))
	self.m_Silder = self.transform:Find("Anchor/0/Progress"):GetComponent(typeof(UISlider))
	self.m_SilderText = self.transform:Find("Anchor/0/100"):GetComponent(typeof(UILabel))
	self.m_Anchor = self.transform:Find("Anchor").gameObject
	self.m_Background = self.transform:Find("Texture").gameObject
	self.m_Shake = self.transform:Find("ShakeAnchor/Shake").gameObject
	self.m_ShakeAni = self.transform:Find("ShakeAnchor/Shake"):GetComponent(typeof(Animation))

	self.m_EnablePlayerShake = false
	self.m_MaterialTemplate:SetActive(false)
	self.m_Background:SetActive(true)
	self.m_Shake:SetActive(true)
	self.m_Anchor:SetActive(true)
	self.m_Silder.value = 0
	self.m_SilderText.text = "0%"
	self:SamleAni(self.m_ShakeAni, LuaCookingPlayMgr.m_ShakeAniName, 0)
	LuaUtils.SetLocalPosition(self.m_Shake.transform.parent, 11, 36, 0)
	
	if LuaCookingPlayMgr.m_Type == EnumCookingPlayType.eCooking or LuaCookingPlayMgr.m_Type == EnumCookingPlayType.eRandomCooking then
		self.m_Dish:LoadMaterial(LuaCookingPlayMgr.m_UIPath)
		self.m_TempCookIcon = LuaCookingPlayMgr.m_UIPath
		self.m_Dish.texture.width = 615
		self.m_Dish.texture.height = 434
		self.m_Fx:LoadFx(CUIFxPaths.CJBMakeBack)
		LuaUtils.SetLocalPosition(self.m_Anchor.transform:Find("Pan"), 425.9, -94.3, 0)
		self.transform:Find("ShakeAnchor/Shaozi").gameObject:SetActive(true)
		self:DelayStartCook(LuaCookingPlayMgr.m_Type == EnumCookingPlayType.eRandomCooking)
	elseif LuaCookingPlayMgr.m_Type == EnumCookingPlayType.eYinMengXiang then
		self.m_Dish:LoadMaterial(LuaCookingPlayMgr.m_JiuTanPath)
		self.m_TempCookIcon = LuaCookingPlayMgr.m_JiuTanPath
		self.m_Dish.texture.width = 729
		self.m_Dish.texture.height = 530
		LuaUtils.SetLocalPosition(self.m_Anchor.transform:Find("Pan"), 195, -181.5, 0)
		self.transform:Find("ShakeAnchor/Shaozi").gameObject:SetActive(false)
		self.m_Fx:DestroyFx()
		self:StartCook()
	end

	local closeButton = self.transform:Find("Texture/Sprite").gameObject
	CommonDefs.AddOnClickListener(closeButton, DelegateFactory.Action_GameObject(function()
		self:OnCloseButtonClick()
	end), false)
end

function CLuaCookingPlayWnd:DelayStartCook(bRandom)
	if self.m_DelayStartTick then
		UnRegisterTick(self.m_DelayStartTick)
	end
	self.m_DelayStartTick = RegisterTickOnce(function()
		if bRandom and not COpenEntryMgr.Inst:GetGuideRecord(70) then
			self:StartRandomCook()
		else
			self:StartCook() 
		end
	end, 3000)

	self:StartCountDown()
end

function CLuaCookingPlayWnd:SamleAni(anim, clipName, time)
	anim:get_Item(clipName).time = time
	anim:get_Item(clipName).enabled = true
	anim:get_Item(clipName).weight = 1
	anim:Sample()
	anim:get_Item(clipName).enabled = false
end

function CLuaCookingPlayWnd:StartCountDown()
	CCenterCountdownWnd.count = 3
	CCenterCountdownWnd.InitStartTime()
	CUIManager.ShowUI(CUIResources.CenterCountdownWnd)
end

function CLuaCookingPlayWnd:StartCook()
	self:InitCookMaterials()

	Gac2Gas.TaskBeginCooking(LuaCookingPlayMgr.m_TaskId)
end

function CLuaCookingPlayWnd:StartRandomCook()
	self:InitCookMaterials()

	local cookId = LuaCookingPlayMgr.m_CookIdList[1]
	if not cookId then
		return
	end

	for template, cookId in pairs(self.m_MaterialTemplateTbl) do
		local go = template.transform:Find("Texture").gameObject
		if LuaCookingPlayMgr.m_CookIdList[1] == cookId then
			self.m_GuideTemplate = template
			self:PlayTween(template, true)
			CUICommonDef.SetGrey(go, false)
			CLuaGuideMgr.TriggerCookingPlayGuide()
			return
		else
			CUICommonDef.SetGrey(go, true)
		end
	end
end

function CLuaCookingPlayWnd:InitCookMaterials()
	Extensions.RemoveAllChildren(self.m_Table.transform)

	self.m_MaterialTemplateTbl = {}
	self.m_PlayTweenTickTbl = {}

	for _, cookId in pairs(LuaCookingPlayMgr.m_CookIdList) do
		self:InitCookMaterial(cookId)
	end
	self.m_Table:Reposition()

	if LuaCookingPlayMgr.m_Type == EnumCookingPlayType.eYinMengXiang then
		LuaUtils.SetLocalPositionY(self.m_Table.transform, 492)
		self:OnSyncCookId()
	else
		LuaUtils.SetLocalPositionY(self.m_Table.transform, 405)
	end
end

function CLuaCookingPlayWnd:InitCookMaterial(cookId)
	local designData = ZhuJueJuQing_Cooking.GetData(cookId)
	if not designData then
		return
	end

	local template = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_MaterialTemplate)
	local icon = template.transform:Find("Texture"):GetComponent(typeof(CUITexture))
	icon:LoadMaterial(designData.SmallIcon)

	local sprite = template.transform:Find("Blin"):GetComponent(typeof(UISprite))
	sprite.alpha = 0

	local nameLabel = template.transform:Find("Name"):GetComponent(typeof(UILabel))
	nameLabel.text = designData.DisplayName

	template:SetActive(true)

	CommonDefs.AddOnClickListener(template, DelegateFactory.Action_GameObject(function()
		self:OnMaterialClick(template)
	end), false)

	self.m_MaterialTemplateTbl[template] = cookId
end

function CLuaCookingPlayWnd:OnMaterialClick(template)
	local cookId = self.m_MaterialTemplateTbl[template]
	if not cookId then
		return
	end

	local icon = template.transform:Find("Texture"):GetComponent(typeof(CUITexture))
	if not icon.material then
		return
	end
	
	if LuaCookingPlayMgr.m_Type == EnumCookingPlayType.eYinMengXiang and LuaCookingPlayMgr.m_CurrentCookId ~= cookId then
		g_MessageMgr:ShowMessage("YinMengXiangAddWrongMeterial")
	else
		if LuaCookingPlayMgr.m_Type ~= EnumCookingPlayType.eRandomCooking then
			icon.material = nil
		end

		self:StopPlayTween(template)
		local designData = ZhuJueJuQing_Cooking.GetData(cookId)
		if designData then
			g_MessageMgr:ShowMessage("CUSTOM_STRING2", designData.DisplayMessage)
		end

		if template == self.m_GuideTemplate then
			Gac2Gas.TaskBeginCooking(LuaCookingPlayMgr.m_TaskId)
			self.m_GuideTemplate = nil
			return
		end

		if LuaCookingPlayMgr.m_Type == EnumCookingPlayType.eCooking or LuaCookingPlayMgr.m_Type == EnumCookingPlayType.eRandomCooking then
			Gac2Gas.CookingPlayAddMeterial(LuaCookingPlayMgr.m_TaskId, cookId)
		elseif LuaCookingPlayMgr.m_Type == EnumCookingPlayType.eYinMengXiang then
			Gac2Gas.YinMengXiangAddMeterial(LuaCookingPlayMgr.m_TaskId, cookId)
		end
		self.m_Dish:LoadMaterial(self.m_TempCookIcon)
	end
end

function CLuaCookingPlayWnd:PlayTween(template, permanent)
	local cookId = self.m_MaterialTemplateTbl[template]
	if not cookId then
		return
	end

	if self.m_PlayTweenTickTbl[template] then
		UnRegisterTick(self.m_PlayTweenTickTbl[template])
		self.m_PlayTweenTickTbl[template] = nil
	end

	local tween = template.transform:Find("Blin"):GetComponent(typeof(TweenAlpha))
	tween:PlayForward()

	if not permanent then
		local designData = ZhuJueJuQing_Cooking.GetData(cookId)
		local delayTime = designData.DelayTime
		if delayTime <= 0 then
			delayTime = 5
		end
		self.m_PlayTweenTickTbl[template] = RegisterTickOnce(function() 
			self:StopPlayTween(template)
		end, delayTime * 1000)
	end
end

function CLuaCookingPlayWnd:StopPlayTween(template)
	if self.m_PlayTweenTickTbl[template] then
		UnRegisterTick(self.m_PlayTweenTickTbl[template])
		self.m_PlayTweenTickTbl[template] = nil
	end

	local tween = template.transform:Find("Blin"):GetComponent(typeof(TweenAlpha))
	tween.enabled = false

	local sprite = template.transform:Find("Blin"):GetComponent(typeof(UISprite))
	sprite.alpha = 0
end

function CLuaCookingPlayWnd:OnFinished()
	self.m_EnablePlayerShake = true
end

function CLuaCookingPlayWnd:DoShake()
	self.m_ShakeCount = 0
	self.m_EnablePlayerShake = false
	self.m_Anchor:SetActive(false)
	self.m_Background:SetActive(false)
	self.m_Shake:SetActive(true)
		
	local tweenPosition = self.m_Shake:GetComponent(typeof(TweenPosition))
	if tweenPosition ~= nil then
		tweenPosition:PlayForward()
	end

	local tweenScale = self.m_Shake:GetComponent(typeof(TweenScale))
	if tweenScale ~= nil then
		tweenScale:PlayForward()
		EventDelegate.Add(tweenScale.onFinished, DelegateFactory.Callback(function() self:OnFinished() end))
	end
end

function CLuaCookingPlayWnd:OnSyncCookId()
	self.m_Silder.value = LuaCookingPlayMgr.m_CurrentProgress / LuaCookingPlayMgr.m_TotalProgress
	self.m_SilderText.text = tostring(math.floor(self.m_Silder.value * 100)) .. "%"

	for template, cookId in pairs(self.m_MaterialTemplateTbl) do
		local go = template.transform:Find("Texture").gameObject
		if LuaCookingPlayMgr.m_CurrentCookId == cookId then
			self:PlayTween(template, LuaCookingPlayMgr.m_Type == EnumCookingPlayType.eYinMengXiang)
			CUICommonDef.SetGrey(go, false)
		else
			self:StopPlayTween(template)
			CUICommonDef.SetGrey(go, true)
		end
	end

	local designData = ZhuJueJuQing_Cooking.GetData(LuaCookingPlayMgr.m_CurrentCookId)
	if designData ~= nil and not System.String.IsNullOrEmpty(designData.BigIcon) and self.m_TempCookIcon then
		self.m_TempCookIcon = designData.BigIcon
	end
end

function CLuaCookingPlayWnd:OnPlayFinished()
	self.m_Silder.value = 1
	self.m_SilderText.text = "100%"

	if LuaCookingPlayMgr.m_Type == EnumCookingPlayType.eYinMengXiang then
		self:DoShake()
	end
end

function CLuaCookingPlayWnd:OnPlayerShake()
	if LuaCookingPlayMgr.m_AllowShake and self.m_EnablePlayerShake and self.m_ShakeCount < 3 then
		LuaUtils.SetLocalPositionY(self.m_Shake.transform.parent, 148)
		self.m_ShakeCount = self.m_ShakeCount + 1
		self.m_ShakeAni:get_Item(LuaCookingPlayMgr.m_ShakeAniName).time = 0
		self.m_ShakeAni:Play()
		self.m_EnablePlayerShake = false
	end
end

function CLuaCookingPlayWnd:OnFinishShake()
	self.m_EnablePlayerShake = true
	if self.m_ShakeCount == 1 then
		g_MessageMgr:ShowMessage("YINMENGXAING_YAOYIYAO_TWO")
	elseif self.m_ShakeCount == 2 then
		g_MessageMgr:ShowMessage("YINMENGXAING_YAOYIYAO_THREE")
	elseif self.m_ShakeCount == 3 then
		self.m_EnablePlayerShake = false
		Gac2Gas.FinishShakeYinMengXiang(LuaCookingPlayMgr.m_TaskId)
	end
end

function CLuaCookingPlayWnd:OnCloseButtonClick()
	if CLuaCookingPlayWnd.m_PlayFinish then
		CUIManager.CloseUI(CLuaUIResources.CookingPlayWnd)
	else
		g_MessageMgr:ShowMessage("cooking_close_window")
	end
end

function CLuaCookingPlayWnd:OnEnable()
	g_ScriptEvent:AddListener("OnSyncCurrentCookId", self, "OnSyncCookId")
	g_ScriptEvent:AddListener("OnCookingPlayFinished", self, "OnPlayFinished")

	if not self.m_PlayerShakeFunc then
		self.m_PlayerShakeFunc = DelegateFactory.Action(function() self:OnPlayerShake() end)
	end
	EventManager.AddListener(EnumEventType.PlayerShakeDevice, self.m_PlayerShakeFunc)

	if not self.m_FinishShakeFunc then
		self.m_FinishShakeFunc = DelegateFactory.Action(function() self:OnFinishShake() end)
	end
	EventManager.AddListener(EnumEventType.CookingPlayShakeFinished, self.m_FinishShakeFunc)
end

function CLuaCookingPlayWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnSyncCurrentCookId", self, "OnSyncCookId")
	g_ScriptEvent:RemoveListener("OnCookingPlayFinished", self, "OnPlayFinished")

	EventManager.RemoveListener(EnumEventType.CookingPlayShakeFinished, self.m_PlayerShakeFunc)
	EventManager.RemoveListener(EnumEventType.CookingPlayShakeFinished, self.m_FinishShakeFunc)

	if self.m_DelayStartTick then
		UnRegisterTick(self.m_DelayStartTick)
		self.m_DelayStartTick = nil
	end

	for _, tick in pairs(self.m_PlayTweenTickTbl or {}) do
		UnRegisterTick(tick)
	end
	self.m_PlayTweenTickTbl = nil
end

function CLuaCookingPlayWnd:GetGuideGo(method)
	if method == "GetGuideButton" then
		return self.m_GuideTemplate
	end	
end
