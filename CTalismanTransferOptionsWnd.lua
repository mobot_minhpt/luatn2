-- Auto Generated!!
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CTalismanTransferOptionsItem = import "L10.UI.CTalismanTransferOptionsItem"
local CTalismanTransferOptionsWnd = import "L10.UI.CTalismanTransferOptionsWnd"
local CUICommonDef = import "L10.UI.CUICommonDef"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local Gac2Gas = import "L10.Game.Gac2Gas"
local NGUIMath = import "NGUIMath"
local String = import "System.String"
CTalismanTransferOptionsWnd.m_Init_CS2LuaHook = function (this) 

    Extensions.RemoveAllChildren(this.grid.transform)
    CommonDefs.ListClear(this.items)
    this.chosenNumLabel.text = System.String.Format("{0}{1}/{2}", this.labelTxt, 0, 0)
    Gac2Gas.QueryCanTransferXianJiaFaBaoList()
end
CTalismanTransferOptionsWnd.m_LoadData_CS2LuaHook = function (this, itemIds) 

    Extensions.RemoveAllChildren(this.grid.transform)
    CommonDefs.ListClear(this.items)
    do
        local i = 0
        while i < itemIds.Count do
            local obj = CUICommonDef.AddChild(this.grid.gameObject, this.itemTpl)
            obj:SetActive(true)
            local item = CommonDefs.GetComponent_GameObject_Type(obj, typeof(CTalismanTransferOptionsItem))
            item:Init(itemIds[i])
            item.Selected = true
            item.OnItemClickDelegate = MakeDelegateFromCSFunction(this.OnItemClickDelegate, Action0, this)
            item.OnItemLongPressDelegate = MakeDelegateFromCSFunction(this.OnItemLongPressDelegate, MakeGenericClass(Action1, String), this)
            CommonDefs.ListAdd(this.items, typeof(CTalismanTransferOptionsItem), item)
            i = i + 1
        end
    end
    this.grid:Reposition()
    this.scrollView:ResetPosition()
    this.chosenNumLabel.text = System.String.Format("{0}{1}/{2}", this.labelTxt, itemIds.Count, itemIds.Count)
end
CTalismanTransferOptionsWnd.m_OnItemClickDelegate_CS2LuaHook = function (this) 
    local n = this.items.Count
    local selCount = 0
    do
        local i = 0
        while i < n do
            local itemId = this.items[i].ItemId
            if this.items[i].Selected and not System.String.IsNullOrEmpty(itemId) then
                selCount = selCount + 1
            end
            i = i + 1
        end
    end
    this.chosenNumLabel.text = System.String.Format("{0}{1}/{2}", this.labelTxt, selCount, n)
end
CTalismanTransferOptionsWnd.m_OnItemLongPressDelegate_CS2LuaHook = function (this, itemId) 

    local b1 = NGUIMath.CalculateRelativeWidgetBounds(this.background.transform)
    local height = b1.size.y
    local worldCenterY = this.background.transform:TransformPoint(b1.center).y
    local width = b1.size.x
    local worldCenterX = this.background.transform:TransformPoint(b1.center).x

    CItemInfoMgr.ShowLinkItemInfo(itemId, false, nil, CItemInfoMgr.AlignType.Right, worldCenterX, worldCenterY, width, height)
end
CTalismanTransferOptionsWnd.m_OnAddButtonClick_CS2LuaHook = function (this, go) 
    local candidates = CreateFromClass(MakeGenericClass(List, String))
    do
        local i = 0
        while i < this.items.Count do
            local itemId = this.items[i].ItemId
            if this.items[i].Selected and not System.String.IsNullOrEmpty(itemId) then
                CommonDefs.ListAdd(candidates, typeof(String), itemId)
            end
            i = i + 1
        end
    end
    EventManager.BroadcastInternalForLua(EnumEventType.OnProfessionTransferTalismanSelected, {candidates})
    this:Close()
end
