local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CClientFurnitureMgr=import "L10.Game.CClientFurnitureMgr"
local CUIFx = import "L10.UI.CUIFx"
local CMainCamera = import "L10.Engine.CMainCamera"

LuaFurnitureLianPuWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaFurnitureLianPuWnd, "LeftBtn", "LeftBtn", GameObject)
RegistChildComponent(LuaFurnitureLianPuWnd, "RightBtn", "RightBtn", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaFurnitureLianPuWnd,"m_TargetFur")
RegistClassMember(LuaFurnitureLianPuWnd,"m_LeftFur")--最左边的fur
RegistClassMember(LuaFurnitureLianPuWnd,"m_RightFur")
RegistClassMember(LuaFurnitureLianPuWnd,"m_LeftPressed")
RegistClassMember(LuaFurnitureLianPuWnd,"m_RightPressed")
function LuaFurnitureLianPuWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    if not CLuaHouseMgr.mCurLianPuFur then
        CUIManager.CloseUI(CLuaUIResources.FurnitureLianPuWnd)
        return
    end
    self.m_TargetFur = CLuaHouseMgr.mCurLianPuFur
    self.mUintRotateYContinued = self.m_TargetFur.ServerRotateY
    

    self:InitWalls()
    self.m_LeftFur = self:GetTheMostLeftFur(self.m_TargetFur)
    self.m_RightFur = self:GetTheMostRightFur(self.m_TargetFur)

    self:InitButtons()
    
end

function LuaFurnitureLianPuWnd:OnEnable( )
     g_ScriptEvent:AddListener("ClientFurnitureCreate",self,"OnClientObjCreate")
end

function LuaFurnitureLianPuWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("ClientFurnitureCreate",self,"OnClientObjCreate")
end

function LuaFurnitureLianPuWnd:Init()
    
end

function LuaFurnitureLianPuWnd:Update()
    self:ClickThroughToClose()
    self:InitButtons()
end

function LuaFurnitureLianPuWnd:InitWalls()
    self.m_SameFurPlaced = {}
    if not self.m_TargetFur then
        return
    end
    local dict = CClientFurnitureMgr.Inst.FurnitureList
    CommonDefs.DictIterate(dict,DelegateFactory.Action_object_object(function (___key, ___value) 
        local fid = tonumber(___key)
        local fur = ___value
        if fur.TemplateId ==  self.m_TargetFur.TemplateId then
            local x = math.floor(fur.RO.Position.x)
            local z = math.floor(fur.RO.Position.z)
            if not self.m_SameFurPlaced[x] then
                self.m_SameFurPlaced[x] = {}
            end
            self.m_SameFurPlaced[x][z] = fur
        end
    end))
end

function LuaFurnitureLianPuWnd:OnClientObjCreate()
    --print("OnClientObjCreate")
    self:InitWalls()
    self.m_LeftFur = self:GetTheMostLeftFur(self.m_TargetFur)
    self.m_RightFur = self:GetTheMostRightFur(self.m_TargetFur)
    self:InitButtons()
end

function LuaFurnitureLianPuWnd:InitButtons()
    local fur = CLuaHouseMgr.mCurLianPuFur
    local pos = self.m_TargetFur.RO.Position
    local width = CClientFurnitureMgr.Inst:getBarrierWidth(fur.TemplateId, fur:GetGridHeight(), fur:GetGridWidth())
    local height = CClientFurnitureMgr.Inst:getBarrierHeight(fur.TemplateId, fur:GetGridHeight(), fur:GetGridWidth())
    local mUintRotateYContinued = self.m_TargetFur.ServerRotateY
    local lpos = self.m_LeftFur.RO.Position
    local rpos = self.m_RightFur.RO.Position

    local leftbtnPos,rightBtnPos
    if (mUintRotateYContinued%180==90) then
        leftbtnPos = Vector3(lpos.x,lpos.y,lpos.z+width/2)
        rightBtnPos = Vector3(rpos.x,rpos.y,rpos.z-width/2)  
    else
        leftbtnPos = Vector3(lpos.x-width/2,lpos.y,lpos.z)
        rightBtnPos = Vector3(rpos.x+width/2,rpos.y,rpos.z)
    end
    leftbtnPos = CMainCamera.Main:WorldToScreenPoint(leftbtnPos)
    leftbtnPos.z = 0
    rightBtnPos = CMainCamera.Main:WorldToScreenPoint(rightBtnPos)
    rightBtnPos.z = 0
    leftbtnPos = CUIManager.UIMainCamera:ScreenToWorldPoint(leftbtnPos)
    rightBtnPos = CUIManager.UIMainCamera:ScreenToWorldPoint(rightBtnPos)
    self.LeftBtn.transform.position = leftbtnPos
    self.RightBtn.transform.position = rightBtnPos

    UIEventListener.Get(self.LeftBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickLeftBtn(go)
    end)
    UIEventListener.Get(self.RightBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickRightBtn(go)
    end)    
end

function LuaFurnitureLianPuWnd:GetTheMostLeftFur(fur)
    local mUintRotateYContinued = self.m_TargetFur.ServerRotateY
    local width = CClientFurnitureMgr.Inst:getBarrierWidth(fur.TemplateId, fur:GetGridHeight(), fur:GetGridWidth())
    local height = CClientFurnitureMgr.Inst:getBarrierHeight(fur.TemplateId, fur:GetGridHeight(), fur:GetGridWidth())
    local pos = fur.RO.Position
    local leftPos = self:GetLeftPos(fur)
    
    local leftFur = nil
    if self.m_SameFurPlaced[leftPos.x] then
        leftFur = self.m_SameFurPlaced[leftPos.x][leftPos.z]
    end

    if leftFur then
        return self:GetTheMostLeftFur(leftFur)
    else
        return fur
    end

end

function LuaFurnitureLianPuWnd:GetTheMostRightFur(fur)
    local mUintRotateYContinued = self.m_TargetFur.ServerRotateY
    local width = CClientFurnitureMgr.Inst:getBarrierWidth(fur.TemplateId, fur:GetGridHeight(), fur:GetGridWidth())
    local height = CClientFurnitureMgr.Inst:getBarrierHeight(fur.TemplateId, fur:GetGridHeight(), fur:GetGridWidth())
    local pos = fur.RO.Position
    local rightPos = self:GetRightPos(fur)

    local rightFur = nil
    if self.m_SameFurPlaced[rightPos.x] then
        rightFur = self.m_SameFurPlaced[rightPos.x][rightPos.z]
    end

    if rightFur then
        return self:GetTheMostRightFur(rightFur)
    end
    return fur
end

function LuaFurnitureLianPuWnd:GetLeftPos(fur)
    local pos = fur.RO.Position
    local mUintRotateYContinued = self.m_TargetFur.ServerRotateY
    local width = CClientFurnitureMgr.Inst:getBarrierWidth(fur.TemplateId, fur:GetGridHeight(), fur:GetGridWidth())
    local leftPos
    if (mUintRotateYContinued%180==90) then
        leftPos = Vector3(math.floor(pos.x),pos.y,math.floor(pos.z) + width)
    else
        leftPos = Vector3(math.floor(pos.x)- width,pos.y,math.floor(pos.z))
    end
    return leftPos
end

function LuaFurnitureLianPuWnd:GetRightPos(fur)
    local pos = fur.RO.Position
    local mUintRotateYContinued = self.m_TargetFur.ServerRotateY
    local width = CClientFurnitureMgr.Inst:getBarrierWidth(fur.TemplateId, fur:GetGridHeight(), fur:GetGridWidth())
    local rightPos
    if (mUintRotateYContinued%180==90) then
        rightPos = Vector3(math.floor(pos.x),pos.y,math.floor(pos.z) - width)
    else
        rightPos = Vector3(math.floor(pos.x)+width,pos.y,math.floor(pos.z))
    end
    return rightPos
end

function LuaFurnitureLianPuWnd:OnClickLeftBtn()
    if not self:CheckCountInRepository() then
        g_MessageMgr:ShowMessage("Cannot_LianPu_FurnitureNotEnough")
        return
    end
    local eulerAngles = self.m_LeftFur.RO.transform.localEulerAngles
                
    local zdata = Zhuangshiwu_Zhuangshiwu.GetData(self.m_LeftFur.TemplateId)
    local rx = eulerAngles.x
    local ry = eulerAngles.y
    local rz = eulerAngles.z
    local rot = math.floor(ry+0.5)
    if zdata.RevolveSwitch == 1 then
        rx = math.max(0,math.floor(rx/360*256))
        ry = math.max(0,math.floor(ry/360*256))
        rz = math.max(0,math.floor(rz/360*256))
    elseif zdata.Revolve90 == 1 then
        rx = 0
        ry = math.max(0,math.floor(ry/360*256))
        rz = 0
    else
        rx = 0
        ry = 0
        rz = 0
    end

    local ui8Scale = CClientFurnitureMgr.GetScaleFloatToUI8(self.m_LeftFur.RO.Scale)
    local pos = self:GetLeftPos(self.m_LeftFur)
    self:OnCreateContinueFurniture(math.floor(pos.x),0,math.floor(pos.z),rot,rx,ry,rz,ui8Scale)
end

function LuaFurnitureLianPuWnd:OnClickRightBtn()
    if not self:CheckCountInRepository() then
        g_MessageMgr:ShowMessage("Cannot_LianPu_FurnitureNotEnough")
        return
    end
    local eulerAngles = self.m_RightFur.RO.transform.localEulerAngles
                
    local zdata = Zhuangshiwu_Zhuangshiwu.GetData(self.m_RightFur.TemplateId)

    local rx = eulerAngles.x
    local ry = eulerAngles.y
    local rz = eulerAngles.z
    local rot = math.floor(ry+0.5)
    if zdata.RevolveSwitch == 1 then
        rx = math.max(0,math.floor(rx/360*256))
        ry = math.max(0,math.floor(ry/360*256))
        rz = math.max(0,math.floor(rz/360*256))
    elseif zdata.Revolve90 == 1 then
        rx = 0
        ry = math.max(0,math.floor(ry/360*256))
        rz = 0
    else
        rx = 0
        ry = 0
        rz = 0
    end

    local ui8Scale = CClientFurnitureMgr.GetScaleFloatToUI8(self.m_RightFur.RO.Scale)
    local pos = self:GetRightPos(self.m_RightFur)
    self:OnCreateContinueFurniture(math.floor(pos.x),0,math.floor(pos.z),rot,rx,ry,rz,ui8Scale)
end

function LuaFurnitureLianPuWnd:OnCreateContinueFurniture(x,y,z,rot,rx,ry,rz,ui8Scale)
    Gac2Gas.CreateAndAddFurniture(self.m_TargetFur.TemplateId,
                                    x,
                                    y,
                                    z,
                                    rot,
                                    false,
                                    "",
                                    CommonDefs.EmptyDictionaryMsgPack,
                                    rx,
                                    ry,
                                    rz,
                                    ui8Scale
                                )
end

function LuaFurnitureLianPuWnd:CheckCountInRepository()
    if not self.m_TargetFur then
        return false
    end
    return CClientFurnitureMgr.Inst:GetRepositoryFurnitureCount(self.m_TargetFur.TemplateId) > 0
end

function LuaFurnitureLianPuWnd:ClickThroughToClose()
    if Input.GetMouseButtonDown(0) then    
        if (not CUICommonDef.IsOverUI or (UICamera.lastHit.collider and
           not CUICommonDef.IsChild(self.LeftBtn.transform, UICamera.lastHit.collider.transform) and 
           not CUICommonDef.IsChild(self.RightBtn.transform, UICamera.lastHit.collider.transform)
            )) then
            
            CUIManager.CloseUI(CLuaUIResources.FurnitureLianPuWnd)
        end
    end
end

--@region UIEvent

--@endregion UIEvent

