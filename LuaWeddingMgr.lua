local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CCommonItem=import "L10.Game.CCommonItem"

CLuaWeddingMgr = {}

function CLuaWeddingMgr.ShowWeddingRingTabenWnd(id)
    CLuaWeddingRingTabenWnd.weddingRingId = id
    CUIManager.ShowUI(CUIResources.WeddingRingTabenWnd)
end

function CLuaWeddingMgr.IsWeddingRing(templateId)
    return templateId == Wedding_Setting.GetData().MarryRingItemId or templateId == Valentine2024_Setting.GetData().RingItemTempId
end

function Gas2Gac.TaBenWeddingRingSuccess(equioInfo) 
    local item = CCommonItem(false, equioInfo)
    if item == nil then return end

    CItemMgr.Inst:AddItem(item)
    CUIManager.CloseUI(CUIResources.WeddingRingTabenWnd)
    CItemInfoMgr.ShowLinkItemInfo(item, false, nil, AlignType.Default, 0, 0, 0, 0, 1)
    EventManager.BroadcastInternalForLua(EnumEventType.WeddingRingTabenSuccess, {item.Id})
end