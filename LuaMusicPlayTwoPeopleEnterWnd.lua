local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"

LuaMusicPlayTwoPeopleEnterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaMusicPlayTwoPeopleEnterWnd, "HuaCaiPlayer", "HuaCaiPlayer", GameObject)
RegistChildComponent(LuaMusicPlayTwoPeopleEnterWnd, "HeMingPlayer", "HeMingPlayer", GameObject)
RegistChildComponent(LuaMusicPlayTwoPeopleEnterWnd, "CountDownLabel", "CountDownLabel", GameObject)
RegistChildComponent(LuaMusicPlayTwoPeopleEnterWnd, "ButtonColorTip", "ButtonColorTip", GameObject)

--@endregion RegistChildComponent end

function LuaMusicPlayTwoPeopleEnterWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaMusicPlayTwoPeopleEnterWnd:Init()

end

--@region UIEvent

--@endregion UIEvent

