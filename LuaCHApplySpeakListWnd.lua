local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UISimpleTableView = import "L10.UI.UISimpleTableView"
local DefaultUISimpleTableViewDataSource = import "L10.UI.DefaultUISimpleTableViewDataSource"
local CButton = import "L10.UI.CButton"
local Constants = import "L10.Game.Constants"
local NGUIText = import "NGUIText"
local UITabBar = import "L10.UI.UITabBar"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local Clipboard = import "L10.Engine.Clipboard"

LuaCHApplySpeakListWnd = class()
RegistClassMember(LuaCHApplySpeakListWnd, "m_PasswordSetBtn")

RegistClassMember(LuaCHApplySpeakListWnd, "m_TabBar")
RegistClassMember(LuaCHApplySpeakListWnd, "m_PasswordSetView")
RegistClassMember(LuaCHApplySpeakListWnd, "m_CloseApplyButton")
RegistClassMember(LuaCHApplySpeakListWnd, "m_ClearApplyButton")
RegistClassMember(LuaCHApplySpeakListWnd, "m_SimpleTableView")
RegistClassMember(LuaCHApplySpeakListWnd, "m_ItemTemplate")
RegistClassMember(LuaCHApplySpeakListWnd, "m_DefaultSimpleTableViewDataSource")
RegistClassMember(LuaCHApplySpeakListWnd, "m_TipLabel")

RegistClassMember(LuaCHApplySpeakListWnd, "m_ApplySpeakListView")
RegistClassMember(LuaCHApplySpeakListWnd, "m_CurPasswordLabel")
RegistClassMember(LuaCHApplySpeakListWnd, "m_NewPasswordInput")
RegistClassMember(LuaCHApplySpeakListWnd, "m_NoPasswardTip")
RegistClassMember(LuaCHApplySpeakListWnd, "m_PasswardConfirmButton")
RegistClassMember(LuaCHApplySpeakListWnd, "m_PasswardRuleButton")
RegistClassMember(LuaCHApplySpeakListWnd, "m_PasswardCopyButton")

RegistClassMember(LuaCHApplySpeakListWnd, "m_AllData")

function LuaCHApplySpeakListWnd:Awake()
    self.m_PasswordSetBtn = self.transform:Find("Anchor/TabBar/PasswordSetBtn").gameObject

    self.m_TabBar = self.transform:Find("Anchor/TabBar"):GetComponent(typeof(UITabBar))
    self.m_ApplySpeakListView = self.transform:Find("Anchor/ApplySpeakListView").gameObject
    self.m_CloseApplyButton = self.transform:Find("Anchor/ApplySpeakListView/CloseApplyButton").gameObject
    self.m_ClearApplyButton = self.transform:Find("Anchor/ApplySpeakListView/ClearApplyButton").gameObject
    self.m_SimpleTableView = self.transform:GetComponent(typeof(UISimpleTableView))
    self.m_ItemTemplate = self.transform:Find("Anchor/ApplySpeakListView/ScrollView/Pool/Item").gameObject
    self.m_TipLabel = self.transform:Find("Anchor/ApplySpeakListView/TipLabel").gameObject

    self.m_PasswordSetView = self.transform:Find("Anchor/PasswordSetView").gameObject
    self.m_CurPasswordLabel = self.transform:Find("Anchor/PasswordSetView/CurPassword"):GetComponent(typeof(UILabel))
    self.m_NewPasswordInput = self.transform:Find("Anchor/PasswordSetView/NewPassword/PasswordInput"):GetComponent(typeof(UIInput))
    self.m_NoPasswardTip = self.transform:Find("Anchor/PasswordSetView/NewPassword/NoPasswardTip").gameObject
    self.m_PasswardConfirmButton = self.transform:Find("Anchor/PasswordSetView/ConfirmButton").gameObject
    self.m_PasswardRuleButton = self.transform:Find("Anchor/PasswordSetView/RuleButton").gameObject
    self.m_PasswardCopyButton = self.transform:Find("Anchor/PasswordSetView/CopyButton").gameObject

    self.m_AllData = {}
    self:InitTableView()
    self.m_ItemTemplate:SetActive(false)
    CommonDefs.AddOnClickListener(self.m_CloseApplyButton, DelegateFactory.Action_GameObject(function(go) self:OnCloseApplyButtonClick() end), false)
    CommonDefs.AddOnClickListener(self.m_ClearApplyButton, DelegateFactory.Action_GameObject(function(go) self:OnClearApplyButtonClick() end), false)
    CommonDefs.AddOnClickListener(self.m_PasswardConfirmButton, DelegateFactory.Action_GameObject(function(go) self:OnPasswardConfirmButtonClick() end), false)
    CommonDefs.AddOnClickListener(self.m_PasswardRuleButton, DelegateFactory.Action_GameObject(function(go) self:OnPasswardRuleButtonClick() end), false)
    CommonDefs.AddOnClickListener(self.m_PasswardCopyButton, DelegateFactory.Action_GameObject(function(go) self:OnPasswardCopyButtonClick() end), false)

    CommonDefs.AddEventDelegate(self.m_NewPasswordInput.onChange, DelegateFactory.Action(function ()
        self:OnPasswordInputValueChanged()
    end))
end

function LuaCHApplySpeakListWnd:Init()
    self.m_PasswardRuleButton:SetActive(false)

    self:OnPasswordInputValueChanged()
    self.m_PasswordSetBtn:SetActive(LuaClubHouseMgr.m_CurRoomInfo.RoomType == EnumCHRoomType.private)
    if LuaClubHouseMgr.m_CurRoomInfo.RoomType == EnumCHRoomType.private then
        Gac2Gas.ClubHouse_QueryPassWord("PasswordSet")
    end
	self.m_TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index) 
        self:OnTabChange(go, index)
    end)
	self.m_TabBar:ChangeTab(0, false)
end

function LuaCHApplySpeakListWnd:OnTabChange(go, index)
    if index == 0 then
        self:InitApplySpeakListView()
    elseif index == 1 then
        self:InitPasswordSetView()
    end
end

function LuaCHApplySpeakListWnd:OnEnable()
    g_ScriptEvent:AddListener("ClubHouse_QueryMemberByStatus_Result", self, "OnClubHouseQueryMemberByStatusResult")
    g_ScriptEvent:AddListener("ClubHouse_Member_Character_Changed", self, "OnClubHouseMemberCharacterChanged")
    g_ScriptEvent:AddListener("ClubHouse_Notify_ClearStatusAll", self, "OnClubHouseNotifyClearStatusAll")
    g_ScriptEvent:AddListener("ClubHouse_Notify_ForbidHandsUpChange", self, "OnClubHouseNotifyForbidHandsUpChange")
    g_ScriptEvent:AddListener("ClubHouse_QueryPassWord_Result", self, "OnClubHouseQueryPassWordResult")
    g_ScriptEvent:AddListener("ClubHouse_ChangePassWord_Result", self, "OnClubHouseChangePassWordResult")
end

function LuaCHApplySpeakListWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ClubHouse_QueryMemberByStatus_Result", self, "OnClubHouseQueryMemberByStatusResult")
    g_ScriptEvent:RemoveListener("ClubHouse_Member_Character_Changed", self, "OnClubHouseMemberCharacterChanged")
    g_ScriptEvent:RemoveListener("ClubHouse_Notify_ClearStatusAll", self, "OnClubHouseNotifyClearStatusAll")
    g_ScriptEvent:RemoveListener("ClubHouse_Notify_ForbidHandsUpChange", self, "OnClubHouseNotifyForbidHandsUpChange")
    g_ScriptEvent:RemoveListener("ClubHouse_QueryPassWord_Result", self, "OnClubHouseQueryPassWordResult")
    g_ScriptEvent:RemoveListener("ClubHouse_ChangePassWord_Result", self, "OnClubHouseChangePassWordResult")
end

function LuaCHApplySpeakListWnd:InitApplySpeakListView()
    self.m_ApplySpeakListView:SetActive(true)
    self.m_PasswordSetView:SetActive(false)
    self:InitCloseApplyButtonText()
    LuaClubHouseMgr:QueryApplySpeakList() -- 查询
end

function LuaCHApplySpeakListWnd:InitPasswordSetView()
    if LuaClubHouseMgr.m_CurRoomInfo.RoomType == EnumCHRoomType.private then
        self.m_ApplySpeakListView:SetActive(false)
        self.m_PasswordSetView:SetActive(true)
    else
        g_MessageMgr:ShowMessage("CLUBHOUSE_PASSWORD_NOT_PRIVATEROOM")
        self.m_TabBar:ChangeTab(0, false)
    end
end

function LuaCHApplySpeakListWnd:OnClubHouseQueryMemberByStatusResult(bSuccess, members)
    self.m_AllData = members
    self:Refresh()
end

function LuaCHApplySpeakListWnd:OnClubHouseQueryPassWordResult(playerId, bSuccess, data_U)
    local data = g_MessagePack.unpack(data_U)
    if bSuccess and data.roRoomId ~= LuaClubHouseMgr.m_CurRoomInfo.RoomId then
        self.m_CurPasswordLabel.text = data.PassWord
        return
    end
    self.m_TabBar:ChangeTab(0, false)
end

function LuaCHApplySpeakListWnd:OnClubHouseMemberCharacterChanged(member)
    local tbl = {}
    local shouldRemove = false
    for __, m in pairs(self.m_AllData) do
        if m.AccId ~= member.AccId or member.Status[EnumCHStatusType.HandsUp] then
            table.insert(tbl, m)
        else
            shouldRemove = true
        end
    end
    if shouldRemove then
        self.m_AllData = tbl
        self:Refresh()
    end
end

function LuaCHApplySpeakListWnd:OnClubHouseNotifyClearStatusAll(status)
    if status == EnumCHStatusType.HandsUp then
        LuaClubHouseMgr:QueryApplySpeakList()
    end
end

function LuaCHApplySpeakListWnd:InitCloseApplyButtonText()
    self.m_CloseApplyButton:GetComponent(typeof(CButton)).Text = LuaClubHouseMgr:IsRoomForbiddenHandsUp() and LocalString.GetString("开启申请") or LocalString.GetString("关闭申请")
end

function LuaCHApplySpeakListWnd:InitTableView()
    self.m_AllData = {}
    if not self.m_DefaultSimpleTableViewDataSource then

		self.m_DefaultSimpleTableViewDataSource = DefaultUISimpleTableViewDataSource.Create(function ()
			return self:NumberOfRows()
		end,
		function ( index )
			return self:CellForRowAtIndex(index)
		end)
    end
    self.m_SimpleTableView:Clear()
    self.m_SimpleTableView.dataSource = self.m_DefaultSimpleTableViewDataSource
    self.m_SimpleTableView:LoadData(0, false)
    self.m_TipLabel:SetActive(true)
end

function LuaCHApplySpeakListWnd:Refresh()
    self.m_SimpleTableView:Clear()
    self.m_SimpleTableView:LoadData(0, false)
    self.m_TipLabel:SetActive(self.m_AllData==nil or #self.m_AllData==0)
end

function LuaCHApplySpeakListWnd:NumberOfRows()
	return #self.m_AllData
end

function LuaCHApplySpeakListWnd:CellForRowAtIndex(index)
	if index < 0 and index >= #self.m_AllData then
        return nil
    end

    local data = self.m_AllData[index + 1]
    local cellIdentifier = "PlayerTemplate"
    local template = self.m_ItemTemplate

    local cell = self.m_SimpleTableView:DequeueReusableCellWithIdentifier(cellIdentifier)
    if cell == nil then
        cell = self.m_SimpleTableView:AllocNewCellWithIdentifier(template, cellIdentifier)
    end
    self:InitPlayerItem(cell, data)
    return cell
end

function LuaCHApplySpeakListWnd:InitPlayerItem(playerItem, member)
    local nameLabel = playerItem.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local fromLabel = playerItem.transform:Find("FromLabel"):GetComponent(typeof(UILabel))
    local levelLabel = playerItem.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
    local portrait = playerItem.transform:Find("Border/Portrait"):GetComponent(typeof(CUITexture))
    local approveButton = playerItem.transform:Find("ApproveButton").gameObject
    nameLabel.text = member.PlayerName
    if member.IsInSameApp and member.Server and member.Server~="" then
        fromLabel.text = member.Server        
    else
        fromLabel.text = SafeStringFormat3(LocalString.GetString("来自: %s"), LuaClubHouseMgr:GetAppDisplayName(member.AppName))
    end
    if member.IsInSameApp then
        levelLabel.text = SafeStringFormat3("[c][%s]%s[-][/c]", member.QnFeiSheng and Constants.ColorOfFeiSheng or NGUIText.EncodeColor24(levelLabel.color), member.QnLevel)
    else
        levelLabel.text = tostring(member.Level)
    end
    portrait:LoadNPCPortrait(member.Portrait, false)
    LuaClubHouseMgr:SetMengDaoProfile(portrait, member.PlayerId, "CHApplySpeakListWnd")
    CommonDefs.AddOnClickListener(approveButton, DelegateFactory.Action_GameObject(function(go) self:OnApproveButtonClick(member.AccId, member.Name) end), false)
end

function LuaCHApplySpeakListWnd:GetGuideGo(methodName)
    if methodName == "GetPasswordSetTab" then
        return self.m_PasswordSetBtn.gameObject
	end
	return nil
end

function LuaCHApplySpeakListWnd:OnClubHouseNotifyForbidHandsUpChange(bForbid)
    self:InitCloseApplyButtonText()
end

function LuaCHApplySpeakListWnd:OnApproveButtonClick(accid, name)
    LuaClubHouseMgr:DealWithApplySpeaking(accid, true)
end

function LuaCHApplySpeakListWnd:OnCloseApplyButtonClick()
    LuaClubHouseMgr:ChangeForbidHandsUpStatus()
end

function LuaCHApplySpeakListWnd:OnClearApplyButtonClick()
    LuaClubHouseMgr:ClearAllHandsUp()
end

function LuaCHApplySpeakListWnd:OnPasswardConfirmButtonClick()
    Gac2Gas.ClubHouse_ChangePassWord(self.m_NewPasswordInput.value)
end

function LuaCHApplySpeakListWnd:OnPasswardRuleButtonClick()
    g_MessageMgr:ShowMessage("CLUBHOUSE_PRIVATEROOM_PASSWORD_SET_RULE")
end

function LuaCHApplySpeakListWnd:OnPasswardCopyButtonClick()
	Clipboard.SetClipboradText(self.m_CurPasswordLabel.text)
    g_MessageMgr:ShowMessage("Copy_Success_Tips")
end

function LuaCHApplySpeakListWnd:OnPasswordInputValueChanged()
    local str = self.m_NewPasswordInput.value
    self.m_NoPasswardTip:SetActive(System.String.IsNullOrEmpty(str))
    CUICommonDef.SetActive(self.m_PasswardConfirmButton.gameObject, CommonDefs.StringLength(str) >= 6, true)
end

function LuaCHApplySpeakListWnd:OnClubHouseChangePassWordResult(playerId, bSuccess, password)
    if bSuccess then
        self.m_CurPasswordLabel.text = password
        self.m_NewPasswordInput.value = ""
    end
end