local Money = import "L10.Game.Money"
local IdPartition = import "L10.Game.IdPartition"
local EquipmentTemplate_SubType = import "L10.Game.EquipmentTemplate_SubType"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local CItemMgr = import "L10.Game.CItemMgr"
local CItem = import "L10.Game.CItem"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CChatLinkMgr = import "CChatLinkMgr"

local CTaskMgr = import "L10.Game.CTaskMgr"
local CEquipment = import "L10.Game.CEquipment"
local QnTableView=import "L10.UI.QnTableView"
local QnAddSubAndInputButton=import "L10.UI.QnAddSubAndInputButton"
local CUITexture=import "L10.UI.CUITexture"
local CButton=import "L10.UI.CButton"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local MessageWndManager = import "L10.UI.MessageWndManager"
local DateTime = import "System.DateTime"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CItemCountUpdate = import "L10.UI.CItemCountUpdate"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CTooltip = import "L10.UI.CTooltip"
local CCommonLuaWnd = import "L10.UI.CCommonLuaWnd"
local BoxCollider = import "UnityEngine.BoxCollider"

CLuaNPCShopWnd = class()
RegistClassMember(CLuaNPCShopWnd,"titleLabel")
RegistClassMember(CLuaNPCShopWnd,"goodsTable")
RegistClassMember(CLuaNPCShopWnd,"nameLabel")
RegistClassMember(CLuaNPCShopWnd,"levelLabel")
RegistClassMember(CLuaNPCShopWnd,"typeLabel")
RegistClassMember(CLuaNPCShopWnd,"descLabel")
RegistClassMember(CLuaNPCShopWnd,"ownedNumLabel")
RegistClassMember(CLuaNPCShopWnd,"descScrollView")
RegistClassMember(CLuaNPCShopWnd,"numberInput")
RegistClassMember(CLuaNPCShopWnd,"icon")
RegistClassMember(CLuaNPCShopWnd,"buyBtn")
RegistClassMember(CLuaNPCShopWnd,"selectedItemIndex")
RegistClassMember(CLuaNPCShopWnd,"moneyCtrl")
RegistClassMember(CLuaNPCShopWnd,"m_ItemLookup")

RegistClassMember(CLuaNPCShopWnd,"m_IsGlobalLimit")
RegistClassMember(CLuaNPCShopWnd,"m_GlobalLimitInfo")
RegistClassMember(CLuaNPCShopWnd,"m_IsOpenGlobalLimit")

RegistClassMember(CLuaNPCShopWnd,"CostItemCtrl")
RegistClassMember(CLuaNPCShopWnd,"CostItemIcon")
RegistClassMember(CLuaNPCShopWnd,"CostItemMask")


function CLuaNPCShopWnd:BindCostCtrl()
    self.CostItemCtrl = FindChildWithType(self.transform,"Detail/Desc/XiaoHaoPing",typeof(CItemCountUpdate))
    self.CostItemIcon = FindChildWithType(self.transform,"Detail/Desc/XiaoHaoPing",typeof(CUITexture))
    self.CostItemMask = FindChildWithType(self.transform,"Detail/Desc/XiaoHaoPing/Mask",typeof(Transform))
end

function CLuaNPCShopWnd:Awake()
    self.titleLabel = self.transform:Find("Wnd_Bg_Primary_Normal/TitleLabel"):GetComponent(typeof(UILabel))
    self.goodsTable = self.transform:Find("GoodsGrid"):GetComponent(typeof(QnTableView))
    self.nameLabel = self.transform:Find("Detail/Desc/NameLabel"):GetComponent(typeof(UILabel))
    self.levelLabel = self.transform:Find("Detail/Desc/LevelLabel"):GetComponent(typeof(UILabel))
    self.typeLabel = self.transform:Find("Detail/Desc/Type/TypeLabel"):GetComponent(typeof(UILabel))
    self.ownedNumLabel = self.transform:Find("Detail/Desc/OwnedNumLabel"):GetComponent(typeof(UILabel))
    self.descLabel = self.transform:Find("Detail/Desc/DescRegion/DescScrollView/DescLabel"):GetComponent(typeof(UILabel))
    self.descScrollView = self.transform:Find("Detail/Desc/DescRegion/DescScrollView"):GetComponent(typeof(UIScrollView))
    self.numberInput = self.transform:Find("Detail/Desc/QnIncreseAndDecreaseButton"):GetComponent(typeof(QnAddSubAndInputButton))
    self.icon = self.transform:Find("Detail/Desc/Icon (1)"):GetComponent(typeof(CUITexture))
    self.buyBtn = self.transform:Find("Detail/BuyBtn"):GetComponent(typeof(CButton))

    self:BindCostCtrl()

    self.selectedItemIndex = 0

    self.m_ItemLookup={}
    self.m_GlobalLimitInfo = {}
    self.m_IsGlobalLimit = false
    self.m_IsOpenGlobalLimit = true


    UIEventListener.Get(self.buyBtn.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
        --最大数量 弹窗提示
        local templateId = CLuaNPCShopInfoMgr.RowAt(self.selectedItemIndex)
        local type = CLuaNPCShopInfoMgr.m_npcShopInfo.MoneyType
        local isYinLiang = type == EnumMoneyType.YinLiang or EnumMoneyType.YinPiao
        if isYinLiang and self.numberInput.m_MaxValue == self.numberInput.m_CurrentValue then
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("NpcShop_Buy_Max_Confirm",self.numberInput.m_CurrentValue,self.nameLabel.text), DelegateFactory.Action(function () 
                Gac2Gas.BuyNpcShopItem(CLuaNPCShopInfoMgr.m_npcShopInfo.NpcEngineId, CLuaNPCShopInfoMgr.m_npcShopInfo.ShopId, templateId, self.numberInput:GetValue())
            end), nil, nil, nil, false)
            return
        end

        if not self:IsScoreMilestoneUnlocked(templateId) then
            g_MessageMgr:ShowMessage("NPC_SHOP_MILESTONE_LOCKED")
            return
        end
        Gac2Gas.BuyNpcShopItem(CLuaNPCShopInfoMgr.m_npcShopInfo.NpcEngineId, CLuaNPCShopInfoMgr.m_npcShopInfo.ShopId, templateId, self.numberInput:GetValue())
    end)
end

function CLuaNPCShopWnd:Init()
    self.moneyCtrl = self.transform:Find("Detail/Desc/GameObject/QnCostAndOwnMoney"):GetComponent(typeof(CCommonLuaScript)).m_LuaSelf

    local isTempPlayScore = false
    local scoreShopData = Shop_PlayScore.GetData(CLuaNPCShopInfoMgr.m_npcShopInfo.ShopId)
    if scoreShopData then
        local globalLimit = scoreShopData.GlobalLimit
        if CLuaNPCShopInfoMgr.m_npcShopInfo.ShopId == 34000075 then
            local now = CServerTimeMgr.Inst:GetZone8Time()
            local timestr = Spokesman_Setting.GetData().ZhouBianMallOpenTime
            local year, month, day, hour, min= string.match(timestr, "(%d+)/(%d+)/(%d+)  (%d+):(%d+):(%d+)")  
            local startTime = CreateFromClass(DateTime, year, month, day, hour, min, 0)
            if DateTime.Compare(now, startTime) >= 0 then      
                self.m_IsOpenGlobalLimit = true
            else
                self.m_IsOpenGlobalLimit = false
            end
        end
        if globalLimit and globalLimit ~= "" then
            self.m_IsGlobalLimit = true
            for cronStr in string.gmatch(globalLimit, "([^;]+);?") do
                local t = {}
                local itemId,limitCount = string.match(cronStr, "(%d+),(%d+)")
                t.itemId = tonumber(itemId)
                t.limitCount = tonumber(limitCount)
                self.m_GlobalLimitInfo[t.itemId] = t
            end
        end
        isTempPlayScore = scoreShopData.ScoreType == 2--临时玩法积分
    end

    self.numberInput.onValueChanged = DelegateFactory.Action_uint(function(val)
        local templateId = CLuaNPCShopInfoMgr.RowAt(self.selectedItemIndex)
        self.moneyCtrl:SetCost(val * CLuaNPCShopInfoMgr.GetPrice(templateId))
    end)

    self.titleLabel.text = CLuaNPCShopInfoMgr.LastOpenShopName
    local type = CLuaNPCShopInfoMgr.m_npcShopInfo.MoneyType
    local scoreKey = CLuaNPCShopInfoMgr.m_npcShopInfo.PlayScoreKey

    self.buyBtn.Text = type == EnumMoneyType.Score and LocalString.GetString("兑换") or LocalString.GetString("购买")

    if isTempPlayScore then
        self.moneyCtrl:InitTempPlayScore(EnumTempPlayScoreKey_lua[scoreShopData.Key])
    else
        if type == EnumMoneyType.Score then
            self.moneyCtrl:SetType(EnumMoneyType.Score, scoreKey, true)
        else
            self.moneyCtrl:SetType(type, EnumPlayScoreKey.NONE, true)
        end
    end

    --数量变化的时候
    self.moneyCtrl.updateAction = function () 
        self:RefreshInputMinMax(self.goodsTable.currentSelectRow)--更新最大数量
    end

    self.goodsTable.m_DataSource=DefaultTableViewDataSource.Create(
        function() return CLuaNPCShopInfoMgr.Count() end,
        function(item,index) self:InitItem(item.transform,index) end)

    self.goodsTable.OnSelectAtRow=DelegateFactory.Action_int(function(row)
        self.selectedItemIndex = row
        self:UpdateDescription()
        self:RefreshInputMinMax(row)
    end)

    self.goodsTable:ReloadData(false, false)
    self.goodsTable:SetSelectRow(0, true)
end

function CLuaNPCShopWnd:RefreshInputMinMax( row )
    --刷新最大最小值
    local templateId = CLuaNPCShopInfoMgr.RowAt(row)
    local limit = CLuaNPCShopInfoMgr.GetRemainCount(templateId)
    local price = CLuaNPCShopInfoMgr.GetPrice(templateId)
    if price == 0 then
        price = 1
    end
    --以防出现价格为0的情况
    local maxNum = (math.floor(self.moneyCtrl:GetOwn() / price))
    maxNum = math.min(maxNum, 999)
    if limit == 0 then
        self.numberInput:SetMinMax(1, 1, 1)
    elseif maxNum == 0 then
        self.numberInput:SetMinMax(1, 1, 1)
    else
        if maxNum >= limit then
            self.numberInput:SetMinMax(1, limit, 1)
        else
            self.numberInput:SetMinMax(1, maxNum, 1)
        end
    end
    -- --刷新
    local templateId = CLuaNPCShopInfoMgr.RowAt(self.selectedItemIndex)
    --不能用SetCost，会递归调用，引起崩溃
    local cost = self.numberInput:GetValue() * CLuaNPCShopInfoMgr.GetPrice(templateId)
    self.moneyCtrl.m_CostLabel.text = tostring(cost)
    --全服限购的物品每次只允许购买一个
    if self.m_IsGlobalLimit and self.m_GlobalLimitInfo[templateId] then
        self.numberInput:SetMinMax(1, 1, 1)
        self.numberInput:SetMinMax(1, 1, 1)
    end
    
end

function CLuaNPCShopWnd:UpdateDescription( )
    local name = ""
    local level = 0
    local desc = ""
    local itemType = ""
    local price = ""

    local templateId = CLuaNPCShopInfoMgr.RowAt(self.selectedItemIndex)
    if IdPartition.IdIsItem(templateId) then
        local item = CItemMgr.Inst:GetItemTemplate(templateId)
        name = item.Name
        level = CLuaDesignMgr.Item_Item_GetAdjustedGradeCheck(item)
        desc = CItem.GetItemDescription(item.ID,true)
        self.icon:LoadMaterial(item.Icon)
        local data = Item_Type.GetData(item.Type)
        if data ~= nil then
            itemType = data.Name
        end
    elseif IdPartition.IdIsEquip(templateId) then
        local equip = EquipmentTemplate_Equip.GetData(templateId)
        local equipment = CLuaNPCShopInfoMgr.GetEquipment(templateId)
        name = equip.Name
        level = equipment.Grade
        desc = equipment.WordDescription
        self.icon:LoadMaterial(equip.Icon)
        local subTypeTemplate = EquipmentTemplate_SubType.GetData(equip.Type * 100 + equip.SubType)
        if subTypeTemplate ~= nil then
            itemType = subTypeTemplate.Name
        end
    end

    self:RefreshCost(templateId)

    self.numberInput:SetValue(1, true)

    self.nameLabel.text = name
    local levelStr = level > 0 and "Lv." .. tostring(level) or nil
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Level < level then
        levelStr = SafeStringFormat3("[c][ff0000]%s[-][/c]", levelStr)
    end

    if self:IsScoreMilestoneUnlocked(templateId) then
        self.levelLabel.text = levelStr
    else
        self.levelLabel.text = LocalString.GetString("[FF0000]未解锁[-]")
    end

    self.typeLabel.text = itemType
    self.descLabel.text = CChatLinkMgr.TranslateToNGUIText(desc, false)
    self.descScrollView:ResetPosition()
    self.moneyCtrl:SetCost(CLuaNPCShopInfoMgr.GetPrice(templateId))
    self:UpdateOwnedNumLabel()


    --全服限购
    if self.m_IsGlobalLimit and self.m_GlobalLimitInfo[templateId] then
        Gac2Gas.QueryScoreShopGoodsRestStock(CLuaNPCShopInfoMgr.m_npcShopInfo.ShopId,templateId)
    else
        if not self.buyBtn.Enabled then
            self.buyBtn.Enabled = true
        end
    end
end

function CLuaNPCShopWnd:ShowModelPreviewWnd(desireItemId)
    local itemData = Item_Item.GetData(desireItemId)
    if itemData.Type == EnumItemType_lua.Mount then
        CLuaNPCShopInfoMgr.m_ModelSet.ItemData = itemData
        CLuaNPCShopInfoMgr.m_ModelSet.ZuoQi = ZuoQi_ZuoQi.GetDataBySubKey("ItemID", desireItemId).ID
    else
        return
    end
    CUIManager.ShowUI(CLuaUIResources.NPCShopModelPreviewWnd)
end

function CLuaNPCShopWnd:UpdateOwnedNumLabel()
    local templateId = CLuaNPCShopInfoMgr.RowAt(self.selectedItemIndex)
    if IdPartition.IdIsItem(templateId) and not CItemMgr.Inst.IsTaskItem then
        self.ownedNumLabel.text = SafeStringFormat3(LocalString.GetString("已拥有:%d"),CItemMgr.Inst:GetItemCount(templateId))
    else
        self.ownedNumLabel.text = ""
    end
end

function CLuaNPCShopWnd:RefreshCost(templateId)
    local costItem = CLuaNPCShopInfoMgr.GetCostItem(templateId)
    local costgo = self.CostItemCtrl.gameObject
    if costItem then
        costgo:SetActive(true)

        local costItemID = costItem[1]
        local costItemcount = costItem[2]
        local itemcfg = CItemMgr.Inst:GetItemTemplate(costItemID)

        self.CostItemCtrl.templateId = costItemID
        self.CostItemCtrl.format = "{0}/"..costItem[2]
        self.CostItemCtrl:UpdateCount()

        self.CostItemIcon:LoadMaterial(itemcfg.Icon)

        local havecount = CItemMgr.Inst:GetItemCount(costItemID)
        local enough = havecount >= costItem[2]

        self.CostItemMask.gameObject:SetActive(not enough)

        self.CostItemCtrl.onChange = DelegateFactory.Action_int(function(count)
            print("onChange")
            self.CostItemMask.gameObject:SetActive(count<costItemcount)
        end)

        UIEventListener.Get(costgo).onClick=DelegateFactory.VoidDelegate(function(go)
            if enough then
                CItemInfoMgr.ShowLinkItemTemplateInfo(costItemID, false, nil, AlignType.ScreenRight, 0, 0, 0, 0)
            else
                CItemAccessListMgr.Inst:ShowItemAccessInfo(costItemID, false, nil, CTooltip.AlignType.Right)
            end
        end)
    else
        costgo:SetActive(false)
    end
end

function CLuaNPCShopWnd:InitItem(transform,row)
    local templateId = CLuaNPCShopInfoMgr.RowAt(row)
    self.m_ItemLookup[templateId]=transform

    local icon = transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local nameLabel = transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local priceLabel = transform:Find("PriceLabel"):GetComponent(typeof(UILabel))
    local moneySprite = transform:Find("MoneySprite"):GetComponent(typeof(UISprite))
    local needSprite = transform:Find("NeedSprite"):GetComponent(typeof(UISprite))
    local disableSprite = transform:Find("Icon/DisableSprite"):GetComponent(typeof(UISprite))
    local levelLabel = transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
    local numLimitLabel = transform:Find("NumLimitLabel"):GetComponent(typeof(UILabel))
    local globalSellOutTag = transform:Find("GlobalSellOutTag").gameObject
    local lockSprite = transform:Find("LockSprite"):GetComponent(typeof(UISprite))
    local unlock = transform:Find("Unlock").gameObject
    local milestoneLabelLabel = transform:Find("Unlock/MilestoneLabelLabel"):GetComponent(typeof(UILabel))

    nameLabel.text = nil
    levelLabel.text = nil
    icon:Clear()
    moneySprite.spriteName = Money.GetIconName(CLuaNPCShopInfoMgr.m_npcShopInfo.MoneyType, CLuaNPCShopInfoMgr.m_npcShopInfo.PlayScoreKey)
    globalSellOutTag:SetActive(false)
    lockSprite.enabled = false
    unlock:SetActive(false)
    icon.alpha = 1

    if IdPartition.IdIsEquip(templateId) then
        if CTaskMgr.IsEquipSubmitItem(templateId) then
            needSprite.alpha = 1
        else
            needSprite.alpha = 0
        end
    else
        if CLuaNPCShopInfoMgr.taskNeededItems[templateId] then
            needSprite.alpha = 1
        else
            needSprite.alpha = 0
        end
    end
 
    disableSprite.enabled = false
    local gradeRequire = 0
    if IdPartition.IdIsItem(templateId) then
        local item = CItemMgr.Inst:GetItemTemplate(templateId)
        if item ~= nil then
            nameLabel.text = item.Name
            gradeRequire = CLuaDesignMgr.Item_Item_GetAdjustedGradeCheck(item)
            icon:LoadMaterial(item.Icon)
            disableSprite.enabled = not CItem.GetMainPlayerIsFit(templateId, true)
        end
    elseif IdPartition.IdIsEquip(templateId) then
        --装备
        local equip = EquipmentTemplate_Equip.GetData(templateId)
        nameLabel.text = equip.Name
        gradeRequire = equip.Grade

        icon:LoadMaterial(equip.Icon)
        disableSprite.enabled = not CEquipment.GetMainPlayerIsFit(templateId, true)
    end

    local gradeStr = gradeRequire > 0 and "Lv." .. tostring(gradeRequire) or nil
    --等级不够 标红
    if gradeStr and CClientMainPlayer.Inst and CClientMainPlayer.Inst.Level < gradeRequire then
        gradeStr = SafeStringFormat3("[c][ff0000]%s[-][/c]", gradeStr)
    end
    levelLabel.text = gradeStr

    priceLabel.text = tostring(CLuaNPCShopInfoMgr.GetPrice(templateId))

    self:UpdateRemainCountInfo(templateId)

    if not self:IsScoreMilestoneUnlocked(templateId) then
        local scoreShopData = Shop_PlayScore.GetData(CLuaNPCShopInfoMgr.m_npcShopInfo.ShopId)
        local needScore = CommonDefs.DictGetValue_LuaCall(scoreShopData.ScoreMilestone, templateId)
        levelLabel.text = nil
        moneySprite.spriteName = nil
        lockSprite.enabled = true
        unlock:SetActive(true)
        priceLabel.text = nil
        icon.alpha = 0.5
        numLimitLabel.text = nil
        milestoneLabelLabel.text = SafeStringFormat3(LocalString.GetString("[9BC9FF]历史累计[-]%d[9BC9FF]积分解锁[-]"), needScore)
    end

    if self.m_IsGlobalLimit and self.m_GlobalLimitInfo[templateId] then
        Gac2Gas.QueryScoreShopGoodsRestStock(CLuaNPCShopInfoMgr.m_npcShopInfo.ShopId,templateId)
    end

    local desireItemId = CLuaNPCShopInfoMgr.GetExchangeItemIdToDesireItemIdTbl()[templateId]
    local boxCollider = icon:GetComponent(typeof(BoxCollider))
    if boxCollider then boxCollider.enabled = desireItemId end
    if desireItemId then
        UIEventListener.Get(icon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self:ShowModelPreviewWnd(desireItemId)
            self.goodsTable:SetSelectRow(row, true)
        end)
    end
end

--@desc 购买里程碑是否解锁
function CLuaNPCShopWnd:IsScoreMilestoneUnlocked(templateId)
    local scoreShopData = Shop_PlayScore.GetData(CLuaNPCShopInfoMgr.m_npcShopInfo.ShopId)
    if scoreShopData and scoreShopData.ScoreMilestone and CommonDefs.DictContains_LuaCall(scoreShopData.ScoreMilestone, templateId) then
        local scoreKey = CLuaNPCShopInfoMgr.m_npcShopInfo.PlayScoreKey
        local totalEnum = CommonDefs.ConvertIntToEnum(typeof(EnumPlayScoreKey), EnumToInt(scoreKey)+1)
        local total = CClientMainPlayer.Inst.PlayProp:GetScoreByKey(totalEnum)
        local needScore = CommonDefs.DictGetValue_LuaCall(scoreShopData.ScoreMilestone, templateId)
        return total >= needScore
    end
    return true
end

function CLuaNPCShopWnd:OnNPCShopItemRemainCountChange( shopId, templateId, remainCount) 
    if CLuaNPCShopInfoMgr.m_npcShopInfo.ShopId == shopId then
        CLuaNPCShopInfoMgr.SetRemainCount(templateId,remainCount)
        self:RefreshInputMinMax(self.goodsTable.currentSelectRow)

        self:UpdateRemainCountInfo(templateId)
    end
end

function CLuaNPCShopWnd:UpdateRemainCountInfo(templateId)
    if not self.m_ItemLookup[templateId] then
        return 
    end
    local numLimitLabel = self.m_ItemLookup[templateId]:Find("NumLimitLabel"):GetComponent(typeof(UILabel))

    local limit = CLuaNPCShopInfoMgr.GetRemainCount(templateId)
    local limitType = CLuaNPCShopInfoMgr.GetLimitType(templateId)

    if CLuaNPCShopInfoMgr.m_npcShopInfo.ShopId == 34000105 then
        numLimitLabel.text = SafeStringFormat3(LocalString.GetString("剩余%d"), limit)
        return
    end

    if limitType == 1 or limitType == 5 then
        numLimitLabel.text = SafeStringFormat3(LocalString.GetString("剩余%d"), limit)
    elseif limitType == 2 then
        numLimitLabel.text = SafeStringFormat3(LocalString.GetString("本月剩余%d"), limit)
    elseif limitType == 3 then
        numLimitLabel.text = SafeStringFormat3(LocalString.GetString("本周剩余%d"), limit)
    else
        numLimitLabel.text = nil
        local globalLimitLabel = self.m_ItemLookup[templateId]:Find("GlobalLimitLabel"):GetComponent(typeof(UILabel))
        if self.m_IsGlobalLimit and self.m_GlobalLimitInfo[templateId] then
            local limitCount = self.m_GlobalLimitInfo[templateId].limitCount
            globalLimitLabel.text = SafeStringFormat3(LocalString.GetString("全服限购%d"),limitCount)
        else
            globalLimitLabel.text = nil
        end
    end
end

function CLuaNPCShopWnd:OnEnable()
    g_ScriptEvent:AddListener("SendItem", self, "SendItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "SetItemAt")
	g_ScriptEvent:AddListener("UpdateNpcShopLimitItemCount", self, "OnNPCShopItemRemainCountChange")
    g_ScriptEvent:AddListener("MainPlayerLevelChange", self, "UpdateDisableStatus")
    g_ScriptEvent:AddListener("OnReplyScoreShopGoodsRestStock", self, "OnReplyScoreShopGoodsRestStock")   
end
function CLuaNPCShopWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendItem", self, "SendItem")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "SetItemAt")
	g_ScriptEvent:RemoveListener("UpdateNpcShopLimitItemCount", self, "OnNPCShopItemRemainCountChange")
    g_ScriptEvent:RemoveListener("MainPlayerLevelChange", self, "UpdateDisableStatus")
    g_ScriptEvent:RemoveListener("OnReplyScoreShopGoodsRestStock", self, "OnReplyScoreShopGoodsRestStock") 
end

function CLuaNPCShopWnd:SendItem()
    self:UpdateOwnedNumLabel()
end

function CLuaNPCShopWnd:SetItemAt()
    self:UpdateOwnedNumLabel()
end

function CLuaNPCShopWnd:UpdateDisableStatus()
    for k,transform in pairs(self.m_ItemLookup) do
        local templateId=k
        local disableSprite = transform:Find("Icon/DisableSprite"):GetComponent(typeof(UISprite))
        if IdPartition.IdIsItem(templateId) then
            disableSprite.enabled = not CItem.GetMainPlayerIsFit(templateId, false)
        elseif IdPartition.IdIsEquip(templateId) then
            disableSprite.enabled = not CEquipment.GetMainPlayerIsFit(templateId, false)
        end
    end
end

function CLuaNPCShopWnd:OnReplyScoreShopGoodsRestStock(shopId, itemTemplateId, stock)
    local transform = self.m_ItemLookup[itemTemplateId]
    local globalSellOutTag = transform:Find("GlobalSellOutTag").gameObject
    globalSellOutTag:SetActive(stock <= 0 and self.m_IsOpenGlobalLimit)
    local selectedId = CLuaNPCShopInfoMgr.RowAt(self.selectedItemIndex)
    if itemTemplateId == selectedId then
        self.buyBtn.Enabled = (stock > 0)
    end
end

--引导，首次添加给跨服帮赛培养积分商店用
function CLuaNPCShopWnd:GetGuideGo(methodName)
    if CUIManager.IsLoaded("GuildLeagueCrossTrainWnd") then
        if methodName == "GetFirstShopItem" then
            local item = self.goodsTable:GetItemAtRow(0)
            return item and item.gameObject or nil
        elseif methodName == "GetCloseBtn" then
            return self.transform:GetComponent(typeof(CCommonLuaWnd)).m_CloseButton
        end
    end
	return nil
end