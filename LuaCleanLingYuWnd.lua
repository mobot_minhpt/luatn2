
local CPaintTexture     =import "L10.UI.CPaintTexture"
local UILabel           = import "UILabel"
local CUIFx             = import "L10.UI.CUIFx"

LuaCleanLingYuWnd = class()      --CleanLingYuWnd 

--------RegistChildComponent-------
RegistChildComponent(LuaCleanLingYuWnd,         "Clean",            CPaintTexture)
RegistChildComponent(LuaCleanLingYuWnd,         "Piont",            GameObject)
RegistChildComponent(LuaCleanLingYuWnd,         "Piont1",           GameObject)
RegistChildComponent(LuaCleanLingYuWnd,         "Piont2",           GameObject)
RegistChildComponent(LuaCleanLingYuWnd,         "Piont3",           GameObject)
RegistChildComponent(LuaCleanLingYuWnd,         "Piont4",           GameObject)
RegistChildComponent(LuaCleanLingYuWnd,         "Piont5",           GameObject)
RegistChildComponent(LuaCleanLingYuWnd,         "Piont6",           GameObject)
RegistChildComponent(LuaCleanLingYuWnd,         "Piont7",           GameObject)
RegistChildComponent(LuaCleanLingYuWnd,         "Piont8",           GameObject)
RegistChildComponent(LuaCleanLingYuWnd,         "Piont9",           GameObject)
RegistChildComponent(LuaCleanLingYuWnd,         "Label",            UILabel)
RegistChildComponent(LuaCleanLingYuWnd,         "CleanEffect",      CUIFx)

---------RegistClassMember-------   
RegistClassMember(LuaCleanLingYuWnd,            "PiontTable")

RegistClassMember(LuaCleanLingYuWnd,            "CountDownTick")

function LuaCleanLingYuWnd:Init()
    self.CleanEffect.gameObject:SetActive(false)
    self:InitPiontTable()
    for i,v in ipairs(self.PiontTable) do
        self.Clean:RegisterTrigger(v)
    end
    self.Clean.OnPaintOver = DelegateFactory.Action(function()
        self.Clean:FinishPaint()
        self.Label.text = LocalString.GetString("修复成功！")
        Gac2Gas.RequestFinishTouchJade("FixJadeWnd", 100)
        self.CleanEffect.gameObject:SetActive(true)

        if self.CountDownTick ~= nil then
            invoke(self.CountDownTick)
        end

        self.CountDownTick = RegisterTickOnce(function()
            CUIManager.CloseUI("CleanLingYuWnd")
        end, 10000)
    end)
end

function LuaCleanLingYuWnd:OnDisable()   
    UnRegisterTick(self.CountDownTick)
end

function LuaCleanLingYuWnd:InitPiontTable()
    self.PiontTable = {self.Piont,self.Piont1,self.Piont2,self.Piont3,self.Piont4,
    self.Piont5,self.Piont6,self.Piont7,self.Piont8,self.Piont9}
end
