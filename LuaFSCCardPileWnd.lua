local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local Animation = import "UnityEngine.Animation"
local UITabBar = import "L10.UI.UITabBar"
local UITableTween = import "L10.UI.UITableTween"
local CGuideMgr=import "L10.Game.Guide.CGuideMgr"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"


LuaFSCCardPileWnd = class()
LuaFSCCardPileWnd.s_Side = 1
LuaFSCCardPileWnd.s_Panel = nil

RegistClassMember(LuaFSCCardPileWnd,"m_Anim")
RegistClassMember(LuaFSCCardPileWnd,"m_CurSide")
RegistClassMember(LuaFSCCardPileWnd,"m_TabBar1")
RegistClassMember(LuaFSCCardPileWnd,"m_TabBar2")
RegistClassMember(LuaFSCCardPileWnd,"m_Page1")
RegistClassMember(LuaFSCCardPileWnd,"m_Page2")
RegistClassMember(LuaFSCCardPileWnd,"m_AddSubBtn")

RegistClassMember(LuaFSCCardPileWnd,"m_CloseTick")
RegistClassMember(LuaFSCCardPileWnd,"m_CollectedCards")
RegistClassMember(LuaFSCCardPileWnd,"m_CollectedCombos")
RegistClassMember(LuaFSCCardPileWnd,"m_SelectedIdx")

function LuaFSCCardPileWnd:Awake()
    self.m_CurSide = LuaFSCCardPileWnd.s_Side
    self.m_Anim = self.transform:GetComponent(typeof(Animation))
    self.m_TabBar1 = self.transform:Find("Anchor/Self/UITabBar"):GetComponent(typeof(UITabBar))
    self.m_TabBar2 = self.transform:Find("Anchor/Enemy/UITabBar"):GetComponent(typeof(UITabBar))
    self.m_Page1 = self.transform:Find("Anchor/Page1")
    self.m_Page2 = self.transform:Find("Anchor/Page2")
    self.m_AddSubBtn = self.m_Page2:Find("QnIncreseAndDecreaseButton"):GetComponent(typeof(QnAddSubAndInputButton))

    self.m_CollectedCards = {}
    self.m_CollectedCombos = {}

    self.m_SelectedIdx = -1

    self.m_TabBar1.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
        self:OnTabChange(index)
    end)

    self.m_TabBar2.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
        self:OnTabChange(index)
    end)

    UIEventListener.Get(self.transform:Find("Bg/CloseButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnClose()
    end)

    UIEventListener.Get(self.transform:Find("Anchor/Self/SwitchBtn").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        if next(LuaFourSeasonCardMgr.CollectedCards[LuaFourSeasonCardMgr.OpponentInfo.gameId]) then 
            self.m_CurSide = 2
            if self.m_SelectedIdx == 2 then self.m_SelectedIdx = 1 end
            local tabBar = self["m_TabBar"..self.m_CurSide]
            tabBar:ChangeTab(self.m_SelectedIdx, true)
            self:InitTab(self.m_SelectedIdx)
            self.m_Anim:Play("fsccardpilewnd_lanqiehuanhong")
        else
            g_MessageMgr:ShowMessage("FSC_NO_CARD_PILE_HINT")
        end
    end)

    UIEventListener.Get(self.transform:Find("Anchor/Enemy/SwitchBtn").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        if next(LuaFourSeasonCardMgr.CollectedCards[LuaFourSeasonCardMgr.SelfInfo.gameId]) then
            self.m_CurSide = 1 
            local tabBar = self["m_TabBar"..self.m_CurSide]
            tabBar:ChangeTab(self.m_SelectedIdx, true)
            self:InitTab(self.m_SelectedIdx)
            self.m_Anim:Play("fsccardpilewnd_hongqielan")
        else
            g_MessageMgr:ShowMessage("FSC_NO_CARD_PILE_HINT")
        end
    end)
end

function LuaFSCCardPileWnd:OnEnable()
    LuaFSCCardPileWnd.s_Panel = self.transform:GetComponent(typeof(UIPanel))
    g_ScriptEvent:AddListener("FSC_SendCollectedCardsData" ,self, "SendCollectedCardsData")
end

function LuaFSCCardPileWnd:OnDisable()
    LuaFSCCardPileWnd.s_Panel = nil
    g_ScriptEvent:RemoveListener("FSC_SendCollectedCardsData" ,self, "SendCollectedCardsData")
end

function LuaFSCCardPileWnd:OnClose()
    if not self.m_CloseTick then
        self.m_SelectedIdx = -1
        self.m_Anim:Play("fsccardpilewnd_close")
        self.m_CloseTick = RegisterTickOnce(function()
            CUIManager.CloseUI(CLuaUIResources.FSCCardPileWnd)
        end, 0.1 * 1000)
    end
end

function LuaFSCCardPileWnd:OnDestroy()
    CUIManager.CloseUI(CLuaUIResources.FSCCardDetailWnd)

    if LuaFourSeasonCardMgr.WaitForNxtGuide and LuaFourSeasonCardMgr.GuidingStage == 3 or CGuideMgr.Inst:IsInPhase(EnumGuideKey.FSC_Guide4) then  
        CGuideMgr.Inst:EndCurrentPhase()
        LuaFourSeasonCardMgr.WaitForNxtGuide = false
        Gac2Gas.Anniv2023FSC_IncGuidingStage()
        LuaFourSeasonCardMgr.GuidingStage = LuaFourSeasonCardMgr.GuidingStage + 1
        LuaFourSeasonCardMgr:TryTriggerGuide()
    elseif LuaFourSeasonCardMgr.GuidingStage == 4 then
        CGuideMgr.Inst:StartGuide(EnumGuideKey.FSC_Guide6, 0)
        self.WaitForNxtGuide = true
    end
end


function LuaFSCCardPileWnd:Init()
    --UnRegisterTick(self.m_CloseTick)
    --self.m_CloseTick = nil

    self.m_CurSide = LuaFSCCardPileWnd.s_Side
    self.m_SelectedIdx = 0

    self.m_CollectedCards[1] = {}
    for cardId, order in pairs(LuaFourSeasonCardMgr.CollectedCards[LuaFourSeasonCardMgr.SelfInfo.gameId]) do
        self.m_CollectedCards[1][order] = cardId
    end
    self.m_CollectedCombos[1] = LuaFourSeasonCardMgr.CollectedCombos[LuaFourSeasonCardMgr.SelfInfo.gameId]

    self.m_CollectedCards[2] = {}
    for cardId, order in pairs(LuaFourSeasonCardMgr.CollectedCards[LuaFourSeasonCardMgr.OpponentInfo.gameId]) do
        self.m_CollectedCards[2][order] = cardId
    end
    self.m_CollectedCombos[2] = LuaFourSeasonCardMgr.CollectedCombos[LuaFourSeasonCardMgr.OpponentInfo.gameId]

    local tabBar = self["m_TabBar"..self.m_CurSide]
    tabBar:ChangeTab(0, true)
    self:InitTab(0)

    if self.m_CurSide == 1 then
        self.m_Anim:Play("fsccardpilewnd_showlan")
    else
        self.m_Anim:Play("fsccardpilewnd_showhong")
    end
end

function LuaFSCCardPileWnd:SendCollectedCardsData(queryIndex, collectedCards, collectedCombos)
    local side = queryIndex == LuaFourSeasonCardMgr.SelfInfo.gameId and 1 or 2
    self.m_CollectedCards[side] = {}
    for cardId, order in pairs(collectedCards) do
        self.m_CollectedCards[side][order] = cardId
    end
     
    self.m_CollectedCombos[side] = collectedCombos

    if self.m_CurSide == side then
        self:InitTab(self.m_SelectedIdx)
    end
end

function LuaFSCCardPileWnd:OnTabChange(index)
    if index == self.m_SelectedIdx then return end
    if index == 0 then -- 已收取卡牌
        self:InitTab(0)
        self.m_Anim:Play("fsccardpilewnd_pageqiehuan2")
    elseif index == 1 then -- 已完成卡牌
        self:InitTab(1)
        self.m_Anim:Play("fsccardpilewnd_pageqiehuan")
    else -- 推荐牌组
        self:InitTab(2)
        self.m_Anim:Play("fsccardpilewnd_pageqiehuan")
    end
    self.m_SelectedIdx = index
end

function LuaFSCCardPileWnd:InitTab(index)
    if index < 0 then return end
    self.m_Page1.gameObject:SetActive(index == 0)
    self.m_Page2.gameObject:SetActive(index ~= 0)
    if index == 0 then -- 已收取卡牌
        local grid = self.m_Page1:Find("Grid"):GetComponent(typeof(UIGrid))
        Extensions.RemoveAllChildren(grid.transform)
        local template = self.m_Page1:Find("FSCCard").gameObject
        template:SetActive(false)
        local scale = template.transform.localScale
        if self.m_CollectedCards[self.m_CurSide] then
            local cards = self.m_CollectedCards[self.m_CurSide]
            for i = #cards, 1, -1 do
                local card = NGUITools.AddChild(grid.gameObject, template)
                card.transform.localScale = scale
                card:SetActive(true)
                local script = card:GetComponent(typeof(CCommonLuaScript))
                if self.m_CurSide == 1 then
                    script.m_LuaSelf:Init(cards[i], LuaFourSeasonCardMgr.SelfSpecialCards[cards[i]] and true or false)
                else
                    script.m_LuaSelf:Init(cards[i])
                end
            end
        end
        grid:Reposition()
    else -- 已完成卡牌=1, 推荐牌组=2
        --self.m_AddSubBtn:UpdateButtonStatus()

        local cnt = 0
        local total = ZhouNianQing2023_CardCombination.GetDataCount()

        local normalGrid = self.m_Page2:Find("ScrollView/Grid"):GetComponent(typeof(UIGrid))
        local extraGrid = self.m_Page2:Find("ScrollView2/ExtraGrid"):GetComponent(typeof(UIGrid))
        normalGrid.transform.parent.gameObject:SetActive(false)
        extraGrid.transform.parent.gameObject:SetActive(false)
        local grid = index == 1 and normalGrid or extraGrid
        Extensions.RemoveAllChildren(grid.transform)
        local template = self.m_Page2:Find("Template").gameObject
        template:SetActive(false)
        local odd = true
        if self.m_CollectedCombos[self.m_CurSide] then
            ZhouNianQing2023_CardCombination.Foreach(function(comboId, data)
                if self.m_CollectedCombos[self.m_CurSide][comboId] and index == 1 or not self.m_CollectedCombos[self.m_CurSide][comboId] and index == 2 then
                    local combo = NGUITools.AddChild(grid.gameObject, template)
                    combo:SetActive(true)
        
                    combo.transform:Find("Name"):GetComponent(typeof(UILabel)).text = data.Name
                    combo.transform:Find("Score"):GetComponent(typeof(UILabel)).text = data.Value
                    combo.transform:Find("OddTexture").gameObject:SetActive(odd)
                    combo.transform:Find("EvenTexture").gameObject:SetActive(not odd)
        
                    local headGrid = combo.transform:Find("Grid"):GetComponent(typeof(UIGrid))
                    local headTemplate = combo.transform:Find("Template").gameObject
                    headTemplate:SetActive(false)
                    for i = 0, data.CardId.Length - 1 do
                        local cardId = data.CardId[i]
                        local head = NGUITools.AddChild(headGrid.gameObject, headTemplate)
                        head:SetActive(true)
                        head:GetComponent(typeof(UILabel)).text = LuaFourSeasonCardMgr:GetCardData(cardId).Title

                        local script = head.transform:Find("FSCCardHeadshot"):GetComponent(typeof(CCommonLuaScript))
                        script:Awake()
                        if self.m_CurSide == 1 then
                            --local isRare = LuaFourSeasonCardMgr.SelfSpecialCards[cardId]
                            --local borrowed = isRare and isRare[1] == EnumFSC_SpCardSrcType.Borrowed
                            --local isBanned = isRare and isRare[2] == EnumFSC_SpCardStatus.Interrupted
                            if index == 2 then
                                local inHand = LuaFourSeasonCardMgr.CollectedCards[LuaFourSeasonCardMgr.SelfInfo.gameId][cardId]
                                script.m_LuaSelf:Init(cardId, false, not inHand and EnumFSC_CardStatus.NotInHand)
                            else
                                script.m_LuaSelf:Init(cardId)
                            end
                        else
                            script.m_LuaSelf:Init(cardId)
                        end
                    end
                    headGrid:Reposition()
                    
                    cnt = cnt + 1
                    odd = not odd
                end
            end)
        end
        --local tableTween = grid:GetComponent(typeof(UITableTween))
        grid:Reposition()
        --tableTween:Reset()
        --tableTween:PlayAnim()
        grid.transform.parent.gameObject:SetActive(true)

        local progress = index == 1 and LocalString.GetString("已完成") or LocalString.GetString("未完成")
        progress = progress.." "..cnt.."/"..total
        self.m_Page2:Find("ProgressLabel"):GetComponent(typeof(UILabel)).text = progress
        --local scrollView = grid.transform.parent:GetComponent(typeof(CUIRestrictScrollView))
        --scrollView:RestrictWithinBounds(true)
        --scrollView:MoveRelative(Vector3(0, -math.max(0, scrollView.transform.localPosition.y - 91), 0))
    end
end

function LuaFSCCardPileWnd:GetGuideGo(methodName)
    if methodName == "GetCard1" then
        local card = self.m_Page1:Find("Grid"):GetChild(1) 
        return card and card.gameObject
    end
    return nil
end