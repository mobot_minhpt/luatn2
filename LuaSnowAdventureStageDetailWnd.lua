local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CUITexture = import "L10.UI.CUITexture"
local UISprite = import "UISprite"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local Profession = import "L10.Game.Profession"
local CChatLinkMgr = import "CChatLinkMgr"

LuaSnowAdventureStageDetailWnd = class()

function LuaSnowAdventureStageDetailWnd:Ctor()
    g_ScriptEvent:AddListener("HanJia2023_SyncXuePoLiXianData", self, "OnRefreshData")
end

function LuaSnowAdventureStageDetailWnd:OnDestroy()
    g_ScriptEvent:RemoveListener("HanJia2023_SyncXuePoLiXianData", self, "OnRefreshData")
end

function LuaSnowAdventureStageDetailWnd:OnRefreshData()
    if self.stageId and (not CommonDefs.IsNull( self.transform) )then
        self:RefreshData(self.stageId)
    end
end

function LuaSnowAdventureStageDetailWnd:Init(obj)
    self.transform = obj.transform
    
    --Find Child
    self.Difficulty = self.transform:Find("Difficulty"):GetComponent(typeof(UITexture))
    self.DifficultyTextureLoader = self.transform:Find("Difficulty"):GetComponent(typeof(CUITexture))
    self.StageName = self.transform:Find("StageName"):GetComponent(typeof(UILabel))
    self.StageIntroduciton = self.transform:Find("StageIntroduciton"):GetComponent(typeof(UILabel))
    self.MonsterInfo = self.transform:Find("MonsterInfo")
    self.Monster1 = self.MonsterInfo:Find("Monster1")
    self.Monster2 = self.MonsterInfo:Find("Monster2")
    self.Monster3 = self.MonsterInfo:Find("Monster3")
    self.Monster1Tex = self.Monster1:Find("Monster1Tex"):GetComponent(typeof(CUITexture))
    self.Monster2Tex = self.Monster2:Find("Monster2Tex"):GetComponent(typeof(CUITexture))
    self.Monster3Tex = self.Monster3:Find("Monster3Tex"):GetComponent(typeof(CUITexture))
    self.RewardInfo = self.transform:Find("RewardInfo")
    self.RewardRemainLabel = self.RewardInfo:Find("RewardRemainLabel"):GetComponent(typeof(UILabel))
    self.RewardItem1 = self.RewardInfo:Find("RewardItem1")
    self.RewardItem2 = self.RewardInfo:Find("RewardItem2")
    self.RewardItem3 = self.RewardInfo:Find("RewardItem3")
    self.RewardItem1Tex = self.RewardItem1:Find("RewardItem1Tex"):GetComponent(typeof(CUITexture))
    self.RewardItem2Tex = self.RewardItem2:Find("RewardItem2Tex"):GetComponent(typeof(CUITexture))
    self.RewardItem3Tex = self.RewardItem3:Find("RewardItem3Tex"):GetComponent(typeof(CUITexture))
    self.ChallengeButton = self.transform:Find("ChallengeButton").gameObject
    self.MultipleTips = self.transform:Find("MultipleTips"):GetComponent(typeof(UILabel))
    self.SingleSnowManButton = self.transform:Find("SingleSnowManButton").gameObject
    self.SnowManTexture = self.SingleSnowManButton.transform:Find("SnowManTexture"):GetComponent(typeof(CUITexture))
    self.SnowManProIcon = self.SingleSnowManButton.transform:Find("SnowManProIcon"):GetComponent(typeof(UISprite))
    self.SnowManLevel = self.SingleSnowManButton.transform:Find("SnowManLevel"):GetComponent(typeof(UILabel))
    self.CloseButton = self.transform:Find("CloseButton").gameObject
    
    UIEventListener.Get(self.CloseButton).onClick = DelegateFactory.VoidDelegate(function (_)
        if self.transform then
            g_ScriptEvent:BroadcastInLua("CloseSnowAdventureStageDetailWnd")
            self.transform.gameObject:SetActive(false)
        end
    end)
end

function LuaSnowAdventureStageDetailWnd:RefreshData(stageId)
    self.stageId = stageId
    local stageType = math.floor(stageId/100)
    local difficultyColor = LuaHanJia2023Mgr.easyColor
    if stageType == 1 then
        --简单
        difficultyColor = LuaHanJia2023Mgr.easyColor
        self.DifficultyTextureLoader:LoadMaterial("UI/Texture/FestivalActivity/Festival_HanJia/HanJia2023/Material/snowadventuresinglestagemainwnd_right_chuji.mat")
    elseif stageType == 2 then
        --普通
        difficultyColor = LuaHanJia2023Mgr.normalColor
        self.DifficultyTextureLoader:LoadMaterial("UI/Texture/FestivalActivity/Festival_HanJia/HanJia2023/Material/snowadventuresinglestagemainwnd_right_zhongji.mat")
    elseif stageType == 3 then
        --困难
        difficultyColor = LuaHanJia2023Mgr.hardColor
        self.DifficultyTextureLoader:LoadMaterial("UI/Texture/FestivalActivity/Festival_HanJia/HanJia2023/Material/snowadventuresinglestagemainwnd_right_kunnan.mat")
    elseif stageType == 4 then
        --多人
        difficultyColor = LuaHanJia2023Mgr.multipleColor
        self.DifficultyTextureLoader:LoadMaterial("UI/Texture/FestivalActivity/Festival_HanJia/HanJia2023/Material/snowadventuresinglestagemainwnd_right_tongxing.mat")
    end
    self.StageName.transform:Find("Texture"):GetComponent(typeof(UITexture)).color = difficultyColor
    
    local stageConfigData = HanJia2023_XuePoLiXianLevel.GetData(stageId)
    self.StageName.text = stageConfigData.Title
    self.StageIntroduciton.text = CChatLinkMgr.TranslateToNGUIText(stageConfigData.GameplayDes) 
    local monsterDesList = g_LuaUtil:StrSplit(stageConfigData.MonsterDes,";")

    local monsterGameobjectList = {self.Monster1, self.Monster2, self.Monster3}
    local monsterTexList = {self.Monster1Tex, self.Monster2Tex, self.Monster3Tex}
    for i = 1, #monsterGameobjectList do
        local monsterId = stageConfigData.MonsterInfo[i-1]
        if monsterId then
            monsterGameobjectList[i].gameObject:SetActive(true)
            self:InitOneMonster(monsterGameobjectList[i].gameObject, monsterTexList[i], monsterId, monsterDesList[i])
        else
            monsterGameobjectList[i].gameObject:SetActive(false)
        end
    end
    
    local rewardItemGameobjectList = {self.RewardItem1, self.RewardItem2, self.RewardItem3}
    local rewardTexList = {self.RewardItem1Tex, self.RewardItem2Tex, self.RewardItem3Tex}
    for i = 1, #rewardItemGameobjectList do
        local itemId = stageConfigData.RewardItem[i-1]
        if itemId then
            rewardItemGameobjectList[i].gameObject:SetActive(true)
            self:InitOneItem(rewardItemGameobjectList[i].gameObject, rewardTexList[i], itemId)
        else
            rewardItemGameobjectList[i].gameObject:SetActive(false)
        end
    end
    
    local rewardLimit = stageConfigData.RewardTimes
    local getRewardTimes = LuaHanJia2023Mgr.snowAdventureData.rewardInfo[stageId] or 0
    if getRewardTimes >= rewardLimit then
        self.RewardRemainLabel.text = CUICommonDef.TranslateToNGUIText( SafeStringFormat3(LocalString.GetString("[c][f2c202]剩余[c][5ef88d]%d[c][f2c202]/%d"), (rewardLimit-getRewardTimes), rewardLimit))
    else    
        self.RewardRemainLabel.text = CUICommonDef.TranslateToNGUIText( SafeStringFormat3(LocalString.GetString("[c][b0f1ff]剩余[c][5ef88d]%d[c][b0f1ff]/%d"), (rewardLimit-getRewardTimes), rewardLimit))
    end

    UIEventListener.Get(self.SingleSnowManButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        LuaSnowAdventureSnowManWnd.ShowCountdownPattern = false
        CUIManager.ShowUI(CLuaUIResources.SnowAdventureSnowManWnd)
    end)
    --刷新雪人
    local snowmanType = LuaHanJia2023Mgr.snowAdventureData.lastSnowmanType
    local snowmanLevel = LuaHanJia2023Mgr.snowAdventureData.snowmanInfo[snowmanType]
    local snowmanConfig = HanJia2023_XuePoLiXianSnowman.GetData(snowmanType)
    self.SnowManTexture:LoadMaterial(snowmanConfig.Pic)
    self.SnowManLevel.text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(snowmanLevel))
    self.SnowManProIcon.spriteName = Profession.GetIconByNumber(snowmanConfig.Career)

    UIEventListener.Get(self.ChallengeButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        if stageType == 4 then
            Gac2Gas.XuePoLiXianRequestTeamPrepare(stageId)
        else
            Gac2Gas.XuePoLiXianEnterSinglePlay(stageId)
        end
    end)
end

function LuaSnowAdventureStageDetailWnd:InitOneMonster(curGameobject, texScript, monsterId, monsterDes)
    local monsterData = Monster_Monster.GetData(monsterId)
    if monsterData then texScript:LoadNPCPortrait(monsterData.HeadIcon) end
    UIEventListener.Get(curGameobject).onClick = DelegateFactory.VoidDelegate(function(_)
        g_MessageMgr:ShowMessage(monsterDes)
    end)
end

function LuaSnowAdventureStageDetailWnd:InitOneItem(curGameobject, texScript, itemID)
    local ItemData = Item_Item.GetData(itemID)
    if ItemData then texScript:LoadMaterial(ItemData.Icon) end
    UIEventListener.Get(curGameobject).onClick = DelegateFactory.VoidDelegate(function (_)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemID, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
    end)
end