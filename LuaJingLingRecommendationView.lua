local UICamera = import "UICamera"
local System = import "System"
local String = import "System.String"
local EnumQueryJingLingContext = import "L10.Game.EnumQueryJingLingContext"
local CQuickAskItem = import "L10.UI.CQuickAskItem"
local CommonDefs = import "L10.Game.CommonDefs"
local CJingLingMgr = import "L10.Game.CJingLingMgr"
local CChatLinkMgr = import "CChatLinkMgr"
local UISimpleTableView = import "L10.UI.UISimpleTableView"
local DefaultUISimpleTableViewDataSource = import "L10.UI.DefaultUISimpleTableViewDataSource"
local EnumJingLingParamReplaceRule = import "L10.Game.EnumJingLingParamReplaceRule"
local DelegateFactory = import "DelegateFactory"
local LinkSource = import "CChatLinkMgr.LinkSource"
local Object = import "System.Object"
local JingLingMessageExtraInfoKey = import "L10.Game.JingLingMessageExtraInfoKey"

CLuaJingLingRecommendationView = class()
RegistClassMember(CLuaJingLingRecommendationView,"tableView")
RegistClassMember(CLuaJingLingRecommendationView,"itemTemplate")
RegistClassMember(CLuaJingLingRecommendationView,"answerScrollView")
RegistClassMember(CLuaJingLingRecommendationView,"answerLabel")
RegistClassMember(CLuaJingLingRecommendationView,"currentQuestion")
RegistClassMember(CLuaJingLingRecommendationView,"items")
RegistClassMember(CLuaJingLingRecommendationView,"m_DefaultSimpleTableViewDataSource")

function CLuaJingLingRecommendationView:Awake()
    self.tableView = self.transform:GetComponent(typeof(UISimpleTableView))
    self.itemTemplate = self.transform:Find("ItemList/Scroll View/Item").gameObject
    self.answerScrollView = self.transform:Find("ItemContent/Scroll View"):GetComponent(typeof(UIScrollView))
    self.answerLabel = self.transform:Find("ItemContent/Scroll View/Label"):GetComponent(typeof(UILabel))
    self.currentQuestion = nil
    self.items = {}
    self.itemTemplate:SetActive(false)
    self.answerLabel.supportTabSymbol = true
    self.answerLabel.supportImgSymbol = true
    self.answerLabel:SetImgContext("imgsource", "jingling")
    self.answerLabel:SetImgContext("subsource", "recommendation")
    self.answerLabel:SetImgContext("keyword", "jingling_recommendation")
end

function CLuaJingLingRecommendationView:Start()
    CommonDefs.AddOnClickListener(self.answerLabel.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnLabelClick(go) end), false)
end

function CLuaJingLingRecommendationView:InitInfo( )
    
    if not self.m_DefaultSimpleTableViewDataSource then

        self.m_DefaultSimpleTableViewDataSource = DefaultUISimpleTableViewDataSource.Create(function ()
            return self:NumberOfRows()
        end,
        function ( index )
            return self:CellForRowAtIndex(index)
        end)
    end

    self.tableView.dataSource = self.m_DefaultSimpleTableViewDataSource
    self.tableView:Clear()
    self.answerLabel.text = nil
    CJingLingMgr.Inst:QueryRecommendations()
end

function CLuaJingLingRecommendationView:LoadData(hotspots) 

    if System.String.IsNullOrEmpty(hotspots) then
        return
    end
    self.items = {}
    local args = CommonDefs.StringSplit_ArrayChar_StringSplitOptions(hotspots, Table2ArrayWithCount({"|"}, 1, MakeArrayClass(String)), System.StringSplitOptions.RemoveEmptyEntries)
    for i=0,args.Length-1 do
        local isHot = StringStartWith(args[i], "hot")
        local text = args[i]
        if isHot then
            text = string.sub(args[i],4) -- 跳过hot
        end
        local recommendationdata = {}
        recommendationdata.displayText = CJingLingMgr.Inst:ReplaceParams(text, EnumJingLingParamReplaceRule.ReplaceByEmpty)
        recommendationdata.askText = CJingLingMgr.Inst:ReplaceParams(text, EnumJingLingParamReplaceRule.ReplaceByKeyValue)
        recommendationdata.isHot = isHot

        table.insert(self.items, i+1, recommendationdata)
    end
    self.tableView:LoadData(0, true)
end

function CLuaJingLingRecommendationView:OnEnable()
    g_ScriptEvent:AddListener("OnQueryJingLingFinished", self, "OnQueryJingLingFinished")
    self:InitInfo()
end

function CLuaJingLingRecommendationView:OnDisable()
    g_ScriptEvent:RemoveListener("OnQueryJingLingFinished", self, "OnQueryJingLingFinished")
end

function CLuaJingLingRecommendationView:OnLabelClick(go) 
    local url = self.answerLabel:GetUrlAtPosition(UICamera.lastWorldPosition)
    local dict = CreateFromClass(MakeGenericClass(Dictionary, cs_string, Object))
    CommonDefs.DictAdd_LuaCall(dict, "keyword", "jingling_recommendation")
    CChatLinkMgr.ProcessLinkClick(url,LinkSource.JingLing, dict)
end

function CLuaJingLingRecommendationView:OnQueryJingLingFinished(args) 
    
    local context = args[0]
    local question = args[1]
    local answer = args[2]
    if context == EnumQueryJingLingContext.Recommendation then
        self:LoadData(answer)
    elseif context == EnumQueryJingLingContext.RecommendationDetail then
        if question == self.currentQuestion then
            self.answerLabel.text = answer
            self.answerScrollView:ResetPosition()
        end
    end
end

function CLuaJingLingRecommendationView:NumberOfRows()
    return #self.items
end

function CLuaJingLingRecommendationView:CellForRowAtIndex(index) 

    local cellIdentifier = "RecommendationItemIdentifier"
    local cell = self.tableView:DequeueReusableCellWithIdentifier(cellIdentifier)
    if cell == nil then
        cell = self.tableView:AllocNewCellWithIdentifier(self.itemTemplate, cellIdentifier)
    end

    if index >= 0 and index < #self.items then
        local data = self.items[index+1]
        local quickAskItem = cell:GetComponent(typeof(CQuickAskItem))
        quickAskItem:Init(data.displayText, data.isHot, data.askText)
        quickAskItem.OnSelected = DelegateFactory.Action_string(function (text)
            self:OnQuestionSelected(text)
        end)
    end
   
    return cell
end
function CLuaJingLingRecommendationView:OnQuestionSelected( text) 

    self.currentQuestion = text
    self.answerLabel.text = nil
    local extraInfo = CreateFromClass(MakeGenericClass(Dictionary, cs_string, Object))
    CommonDefs.DictAdd_LuaCall(extraInfo, JingLingMessageExtraInfoKey.QueryMethod, "link")
    CJingLingMgr.Inst:QueryJingLing(EnumQueryJingLingContext.RecommendationDetail, text, true, false, false, false, false, true, extraInfo)
end

