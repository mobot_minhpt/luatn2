local CMainCamera = import "L10.Engine.CMainCamera"
local Gac2Gas = import "L10.Game.Gac2Gas"
local CUIRes = import "L10.UI.CUIResources"
local UITexture = import "UITexture"
local MessageWndManager = import "L10.UI.MessageWndManager"
local RenderTexture = import "UnityEngine.RenderTexture"
local RenderTextureFormat = import "UnityEngine.RenderTextureFormat"
local RenderTextureReadWrite = import "UnityEngine.RenderTextureReadWrite"
local TextureWrapMode = import "UnityEngine.TextureWrapMode"
local FilterMode = import "UnityEngine.FilterMode"
local Camera = import "UnityEngine.Camera"
local GameObject = import "UnityEngine.GameObject"
local CameraClearFlags = import "UnityEngine.CameraClearFlags"
local LayerDefine = import "L10.Engine.LayerDefine"
local Quaternion = import "UnityEngine.Quaternion"
local Vector3 = import "UnityEngine.Vector3"
local CPreDrawMgr = import "L10.Engine.CPreDrawMgr"
local TweenPosition = import "TweenPosition"
local Utility = import "L10.Engine.Utility"
local CEffectMgr = import "L10.Game.CEffectMgr"
local CClientFurnitureMgr = import "L10.Game.CClientFurnitureMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
local PathType = import "DG.Tweening.PathType"
local PathMode = import "DG.Tweening.PathMode"
local Ease = import "DG.Tweening.Ease"
local Animator = import "UnityEngine.Animator"

CLuaTaoQuanWnd=class()
RegistChildComponent(CLuaTaoQuanWnd,"closeBtn", GameObject)
RegistChildComponent(CLuaTaoQuanWnd,"showTexture", UITexture)
RegistChildComponent(CLuaTaoQuanWnd,"quanNode", GameObject)
RegistChildComponent(CLuaTaoQuanWnd,"restQuanNode", GameObject)
RegistChildComponent(CLuaTaoQuanWnd,"leftSide", GameObject)
RegistChildComponent(CLuaTaoQuanWnd,"rightSide", GameObject)
RegistChildComponent(CLuaTaoQuanWnd,"throwBtn", GameObject)
RegistChildComponent(CLuaTaoQuanWnd,"scoreLabel", UILabel)
RegistChildComponent(CLuaTaoQuanWnd,"timeLabel", UILabel)
RegistChildComponent(CLuaTaoQuanWnd,"liduSlider", UISlider)
RegistChildComponent(CLuaTaoQuanWnd,"thumb", GameObject)
RegistChildComponent(CLuaTaoQuanWnd,"FingerRoot1", GameObject)
RegistChildComponent(CLuaTaoQuanWnd,"Finger1", GameObject)
RegistChildComponent(CLuaTaoQuanWnd,"Finger2", GameObject)

RegistClassMember(CLuaTaoQuanWnd, "cameraNode")
RegistClassMember(CLuaTaoQuanWnd, "moveDir")
RegistClassMember(CLuaTaoQuanWnd, "m_Tick")
RegistClassMember(CLuaTaoQuanWnd, "m_MoveTick")
RegistClassMember(CLuaTaoQuanWnd, "m_ThrowSign")
RegistClassMember(CLuaTaoQuanWnd, "m_ThrowCount")
RegistClassMember(CLuaTaoQuanWnd, "m_ThrowCountTick")
RegistClassMember(CLuaTaoQuanWnd, "cameraScript")
RegistClassMember(CLuaTaoQuanWnd, "m_GuideTick")
RegistClassMember(CLuaTaoQuanWnd, "game2CameraPos")
RegistClassMember(CLuaTaoQuanWnd, "game2CameraRotation")

function CLuaTaoQuanWnd:Awake()
end

function CLuaTaoQuanWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateTaoQuanInfo", self, "UpdateInfo")
	g_ScriptEvent:AddListener("OnScreenChange", self, "UpdateScreen")
	g_ScriptEvent:AddListener("OpenOrCloseWinSocialWnd", self, "UpdateScreen")
end

function CLuaTaoQuanWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateTaoQuanInfo", self, "UpdateInfo")
	g_ScriptEvent:RemoveListener("OnScreenChange", self, "UpdateScreen")
	g_ScriptEvent:RemoveListener("OpenOrCloseWinSocialWnd", self, "UpdateScreen")
	self:TryEndGuide()
end

function CLuaTaoQuanWnd:InitPosData()
	self.game2CameraPos = Vector3(37.96,45.4,100.47)
	self.game2CameraRotation = Quaternion.Euler(55.18,-179,0)
end

function CLuaTaoQuanWnd:UpdateScreen()
	if self.cameraNode then
		GameObject.Destroy(self.cameraNode)
	end
	self.cameraNode = nil
	local screenWidth = CommonDefs.GameScreenWidth
  local screenHeight = CommonDefs.GameScreenHeight
	if 1920 / screenWidth > 1080 / screenHeight then
		self.showTexture.width = 1920--Screen.width
		self.showTexture.height = screenHeight * 1920 / screenWidth--Screen.height
	else
		self.showTexture.width = screenWidth * 1080 / screenHeight--Screen.width
		self.showTexture.height = 1080--Screen.height
	end

	local renderTex = RenderTexture(self.showTexture.width,self.showTexture.height,32,RenderTextureFormat.ARGB32,RenderTextureReadWrite.Default)
	renderTex.wrapMode = TextureWrapMode.Clamp
	renderTex.filterMode = FilterMode.Bilinear
	renderTex.anisoLevel = 2
	self.showTexture.mainTexture = renderTex

	self.cameraNode = GameObject("__TaoQuanCamera__")
	local cameraScript = CommonDefs.AddCameraComponent(self.cameraNode)
	cameraScript.clearFlags = CameraClearFlags.Skybox
  --cameraScript.backgroundColor = Color(49 / 255, 77 / 255, 121 / 255, 5 / 255)
  cameraScript.cullingMask = 2^LayerDefine.ModelForNGUI_3D + 2^LayerDefine.Effect_3D + 2^LayerDefine.Default + 2^LayerDefine.Water + 2^LayerDefine.WaterReflection + 2^LayerDefine.Terrain + 2^LayerDefine.NPC
  cameraScript.orthographic = false
  cameraScript.fieldOfView = 60
  cameraScript.farClipPlane = 40
  cameraScript.nearClipPlane = 0.1
  cameraScript.depth = -10
  cameraScript.transform.parent = CUIManager.instance.transform

	if LuaTaoQuanMgr.GameType == 1 then
		self.cameraNode.transform.position = Vector3(37.5,39.9,57)
		self.cameraNode.transform.localRotation = Quaternion.Euler(52,90,0)
	elseif LuaTaoQuanMgr.GameType == 2 then
		self.cameraNode.transform.position = self.game2CameraPos
		self.cameraNode.transform.localRotation = self.game2CameraRotation
	end
  --cameraScript.transform.localScale = Vector3.one
	cameraScript.targetTexture = renderTex
	self.cameraScript = cameraScript
end

function CLuaTaoQuanWnd:ShowAddTime(score,pos)
	if score and score > 0 then

	end
end

function CLuaTaoQuanWnd:SetThrowTime(throwTime)
	local nodeTable = {self.restQuanNode.transform:Find("node1"),self.restQuanNode.transform:Find("node2"),self.restQuanNode.transform:Find("node3"),self.restQuanNode.transform:Find("node4"),self.restQuanNode.transform:Find("node5")}
	for i,v in ipairs(nodeTable) do
		if i <= throwTime then
			v.transform:Find("show").gameObject:SetActive(true)
		else
			v.transform:Find("show").gameObject:SetActive(false)
		end
	end
end

function CLuaTaoQuanWnd:SetThrowCount()
	self.m_ThrowSign = true
	self.m_ThrowCount = LuaTaoQuanMgr.TimeCount
	if self.m_ThrowCountTick then
			UnRegisterTick(self.m_ThrowCountTick)
	end
	self.m_ThrowCountTick = RegisterTickWithDuration(function ()
		self.m_ThrowCount = self.m_ThrowCount - 1
		if self.m_ThrowCount <= 0 then
			self.timeLabel.text = '00:00'
			if self.m_ThrowCountTick then
					UnRegisterTick(self.m_ThrowCountTick)
			end
			self:SetTouchPoint()
		else
			if self.m_ThrowCount < 10 then
				self.timeLabel.text = '00:0' .. self.m_ThrowCount
			else
				self.timeLabel.text = '00:' .. self.m_ThrowCount
			end
		end
	end,1000,LuaTaoQuanMgr.TimeCount * 1000)
end

function CLuaTaoQuanWnd:UpdateInfo()
	self.scoreLabel.text = LuaTaoQuanMgr.Score
	--LuaTaoQuanMgr.ScoreDetails = extraDataDic.ScoreDetails
	--LuaTaoQuanMgr.RingPos = extraDataDic.RingPos--x,y
	self:SetThrowTime(LuaTaoQuanMgr.ThrowTimes)

	if LuaTaoQuanMgr.PlayState == 1 then -- waitingforplayer

	elseif LuaTaoQuanMgr.PlayState == 3 then -- finish

	elseif LuaTaoQuanMgr.PlayState == 4 then -- reward

	end
end

function CLuaTaoQuanWnd:StartQuanMove()
	local tweenPos = self:DoQuanMove()

	local tweenFinish = function()
		if self.quanNode and self.quanNode.activeSelf then
			self:DoQuanMove()
		end
	end

	CommonDefs.AddEventDelegate(tweenPos.onFinished, DelegateFactory.Action(tweenFinish))
end

function CLuaTaoQuanWnd:LeaveWnd()
	CUIManager.CloseUI(CUIRes.TaoQuanWnd)
	Gac2Gas.RequestLeavePlay()
end

function CLuaTaoQuanWnd:CheckState()
	if LuaTaoQuanMgr.PlayState == 1 then -- waitingforplayer
		if self.quanNode and not self.quanNode.activeSelf then
			self.quanNode:SetActive(true)
		end
		self:SetThrowCount()
	elseif LuaTaoQuanMgr.PlayState == 3 then -- finish
		--self:LeaveWnd()
	elseif LuaTaoQuanMgr.PlayState == 4 then -- reward

	end
end

function CLuaTaoQuanWnd:DoQuanMove()
	local moveSpeed = 1000
	local pos = 0
	self.quanNode.transform.localPosition = self.rightSide.transform.localPosition
	if self.moveDir == 1 then
		self.moveDir = 0
		self.quanNode.transform.localPosition = self.leftSide.transform.localPosition
		pos = self.rightSide.transform.localPosition
	else
		self.moveDir = 1
		self.quanNode.transform.localPosition = self.rightSide.transform.localPosition
		pos = self.leftSide.transform.localPosition
	end
	--local tweener = LuaTweenUtils.TweenPositionX(self.quanNode.transform, pos, (self.rightSide.transform.localPosition.x - self.leftSide.transform.localPosition.x) / moveSpeed)
	local tweenPos = TweenPosition.Begin(self.quanNode,(self.rightSide.transform.localPosition.x - self.leftSide.transform.localPosition.x) / moveSpeed, pos)

	return tweenPos
end

function CLuaTaoQuanWnd:PlayQuanFx(fxId,gridPos,dir)
	local fx = CEffectMgr.Inst:AddWorldPositionFX(fxId, Utility.GridPos2WorldPos(gridPos.x, gridPos.y), dir, 0, 1, -1, EnumWarnFXType.None, nil, 0, 0, DelegateFactory.Action_GameObject(function(go)
			local ani = CommonDefs.GetComponentInChildren_GameObject_Type(go, typeof(Animator))
			if ani then
				ani:Play("lnpc493_feixingguiji", 0, 0)
				ani:Update(0)
			end
	end))
	
	local fxTime = 2000
	if self.m_Tick ~= nil then
		UnRegisterTick(self.m_Tick)
	end

	self.m_Tick = RegisterTickWithDuration(function ()
		if fx then
			fx:Destroy()
		end
		self:CheckState()
	end,fxTime,fxTime)
end

function CLuaTaoQuanWnd:SetTouchPoint()
	if LuaTaoQuanMgr.PlayState == 1 and self.m_ThrowSign then
		if self.m_ThrowCountTick then
				UnRegisterTick(self.m_ThrowCountTick)
		end
		self.timeLabel.text = ''

		--local screenPointX = self.quanNode.transform.localPosition.x * Screen.width / self.showTexture.width + Screen.width / 2
		--local screenPointY = self.liduSlider.value * Screen.height

		local screenPointX = self.quanNode.transform.localPosition.x + self.showTexture.width / 2
		local screenPointY = (self.liduSlider.value * 0.7 + 0.18) * self.showTexture.height

		local gridPos = Utility.ScreenPoint2GridPos(Vector3(screenPointX,screenPointY,0),self.cameraScript)
		--local gridPos = Utility.ScreenPoint2GridPos(Vector3(1920/2,1199/2,0),self.cameraScript)
		--local calScreenPos = Utility.GridPos2ScreenPoint(gridPos,self.cameraScript)
		if LuaTaoQuanMgr.GameType == 1 then
			Gac2Gas.SHTYSetRingPos(gridPos.x,gridPos.y)
		elseif LuaTaoQuanMgr.GameType == 2 then
			Gac2Gas.SHTYSetRingPos(gridPos.x,gridPos.y)
		end
		if LuaTaoQuanMgr.GameType == 1 then
			self:PlayQuanFx(88800827,gridPos,540) --wait for fx complete
		elseif LuaTaoQuanMgr.GameType == 2 then
			self:PlayQuanFx(88801257,gridPos,90) --wait for fx complete
		end

		self.m_ThrowSign = false
		self.quanNode:SetActive(false)
	end
end

function CLuaTaoQuanWnd:Init()
	local onCloseClick = function(go)
		MessageWndManager.ShowOKCancelMessage(LocalString.GetString("确定要退出吗？"), DelegateFactory.Action(function ()
				self:LeaveWnd()
		end), nil, nil, nil, false)
	end

	self:InitPosData()
	CClientFurnitureMgr.Inst:HideSceneModels(6)

	self.quanNode:SetActive(false)

	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)
	self.m_ThrowSign = true
	self:SetThrowCount()

	local touchBtn = function(go)
		self:TryEndGuide()
		self:SetTouchPoint()
	end

	CommonDefs.AddOnClickListener(self.throwBtn,DelegateFactory.Action_GameObject(touchBtn),false)

	CPreDrawMgr.m_bEnableRectPreDraw = false

	local screenWidth = CommonDefs.GameScreenWidth
  local screenHeight = CommonDefs.GameScreenHeight
	if 1920 / screenWidth > 1080 / screenHeight then
		self.showTexture.width = 1920--Screen.width
		self.showTexture.height = screenHeight * 1920 / screenWidth--Screen.height
	else
		self.showTexture.width = screenWidth * 1080 / screenHeight--Screen.width
		self.showTexture.height = 1080--Screen.height
	end

	local renderTex = RenderTexture(self.showTexture.width,self.showTexture.height,32,RenderTextureFormat.ARGB32,RenderTextureReadWrite.Default)
	renderTex.wrapMode = TextureWrapMode.Clamp
	renderTex.filterMode = FilterMode.Bilinear
	renderTex.anisoLevel = 2
	self.showTexture.mainTexture = renderTex

	self.cameraNode = GameObject("__TaoQuanCamera__")
	local cameraScript = CommonDefs.AddCameraComponent(self.cameraNode)
    CMainCamera.Inst:SetCameraEnableStatus(false,"use_another_camera", false)

	cameraScript.clearFlags = CameraClearFlags.Skybox
  --cameraScript.backgroundColor = Color(49 / 255, 77 / 255, 121 / 255, 5 / 255)
  cameraScript.cullingMask = 2^LayerDefine.ModelForNGUI_3D + 2^LayerDefine.Effect_3D + 2^LayerDefine.Default + 2^LayerDefine.Water + 2^LayerDefine.WaterReflection + 2^LayerDefine.Terrain + 2^LayerDefine.NPC
  cameraScript.orthographic = false
  cameraScript.fieldOfView = 60
  cameraScript.farClipPlane = 40
  cameraScript.nearClipPlane = 0.1
  cameraScript.depth = -10
  cameraScript.transform.parent = CUIManager.instance.transform

	if LuaTaoQuanMgr.GameType == 1 then
		self.cameraNode.transform.position = Vector3(37.5,39.9,57)
		self.cameraNode.transform.localRotation = Quaternion.Euler(52,90,0)
	elseif LuaTaoQuanMgr.GameType == 2 then
		self.cameraNode.transform.position = self.game2CameraPos
		self.cameraNode.transform.localRotation = self.game2CameraRotation
	end
  --cameraScript.transform.localScale = Vector3.one
	cameraScript.targetTexture = renderTex
	self.cameraScript = cameraScript
	--lineNode = GameObject("__KitePlayLine__")
	--local lineRenderScript = lineNode:AddComponent(typeof(LineRenderer))
	--lineRenderScript.material:SetColor("_Color", Color.white)
	--lineRenderScript:SetWidth(1,1)
	--lineRenderScript:SetPosition(0,cameraNode.transform.position)
	--lineRenderScript:SetPosition(1,Vector3(selfData.kitePos[1],selfData.kitePos[2],selfData.kitePos[3]))
	self.moveDir = 1
	self.liduSlider.value = 0.5
	self.quanNode:SetActive(true)

	if self.m_MoveTick then
			UnRegisterTick(self.m_MoveTick)
	end

	self.m_MoveTick = RegisterTickWithDuration(function ()
		self:StartQuanMove()
	end,100,100)

	self:UpdateInfo()
	self:TryStartGuide()
end

function CLuaTaoQuanWnd:OnDestroy()
    CMainCamera.Inst:SetCameraEnableStatus(true,"use_another_camera", false)
	CPreDrawMgr.m_bEnableRectPreDraw = true
	if self.cameraNode then
		GameObject.Destroy(self.cameraNode)
	end
	self.cameraNode = nil
	if self.m_ThrowCountTick then
			UnRegisterTick(self.m_ThrowCountTick)
	end
	if self.m_Tick then
			UnRegisterTick(self.m_Tick)
	end
	if self.m_MoveTick then
			UnRegisterTick(self.m_MoveTick)
	end
end

function CLuaTaoQuanWnd:TryStartGuide()
	local should_guide = CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.TaoQuanWnd)
	if(should_guide)then
		--滑动条手指动画
		self.Finger1:SetActive(true)
		self.Finger2:SetActive(false)
		local waypoints = {Vector3(0,0,0), Vector3(0,50,0), Vector3(0,0,0), Vector3(0,-150,0), Vector3(0,0,0)}
		local wp_array = Table2Array(waypoints, MakeArrayClass(Vector3))
		LuaTweenUtils.DOLuaLocalPath(self.Finger1.transform, wp_array, 2, PathType.Linear, PathMode.Ignore, 2, - 1, Ease.Linear)

		local next_subphase_delegate =  DelegateFactory.VoidDelegate(function(g)
			if(CGuideMgr.Inst:IsInPhase(EnumGuideKey.TaoQuanWnd) and CGuideMgr.Inst:IsInSubPhase(0))then
				LuaTweenUtils.DOKill(self.Finger1.transform, false)
				self.Finger1:SetActive(false)
				self.Finger2:SetActive(true)
				if(self.m_GuideTick)then
					UnRegisterTick(self.m_GuideTick)
					self.m_GuideTick = nil
				end

				self.Finger2.transform.localPosition = Vector3(30,-20,0)
				self.Finger2.transform.localScale = Vector3(1.3,1.3,1.0)
				LuaTweenUtils.TweenPosition(self.Finger2.transform,0,0,0,0.5)
				LuaTweenUtils.TweenScaleTo(self.Finger2.transform,Vector3(1.0,1.0,1.0),0.5)
				LuaTweenUtils.SetDelay(LuaTweenUtils.TweenPosition(self.Finger2.transform,30,-20,0,0.5),0.5)
				LuaTweenUtils.SetDelay(LuaTweenUtils.TweenScaleTo(self.Finger2.transform,Vector3(1.3,1.3,1.0),0.5),0.5)
				self.m_GuideTick = RegisterTick(function()
					LuaTweenUtils.TweenPosition(self.Finger2.transform,0,0,0,0.5)
					LuaTweenUtils.TweenScaleTo(self.Finger2.transform,Vector3(1.0,1.0,1.0),0.5)
					LuaTweenUtils.SetDelay(LuaTweenUtils.TweenPosition(self.Finger2.transform,30,-20,0,0.5),0.5)
					LuaTweenUtils.SetDelay(LuaTweenUtils.TweenScaleTo(self.Finger2.transform,Vector3(1.3,1.3,1.0),0.5),0.5)
				end,1300)
				CGuideMgr.Inst:NextSubPhase()
			end
		end)

		UIEventListener.Get(self.thumb).onDragEnd = next_subphase_delegate
		UIEventListener.Get(self.thumb).onClick = next_subphase_delegate
	else
		self.Finger1:SetActive(false)
		self.Finger2:SetActive(false)
	end
end

function CLuaTaoQuanWnd:TryEndGuide()
	if(self.m_GuideTick)then
		UnRegisterTick(self.m_GuideTick)
		self.m_GuideTick = nil
	end
	LuaTweenUtils.DOKill(self.Finger1.transform, false)
	LuaTweenUtils.DOKill(self.Finger2.transform, false)
	self.Finger1:SetActive(false)
	self.Finger2:SetActive(false)
	if(L10.Game.Guide.CGuideMgr.Inst:IsInPhase(EnumGuideKey.TaoQuanWnd))then
		L10.Game.Guide.CGuideMgr.Inst:EndCurrentPhase()
	end
end

function CLuaTaoQuanWnd:GetGuideGo(methodName)
	if(methodName == "GetThrowBtn")then
		return self.throwBtn.transform:Find("Mask/Texture").gameObject
	elseif(methodName == "GetForceThumb")then
		return self.thumb
	end
	return nil
end

return CLuaTaoQuanWnd
