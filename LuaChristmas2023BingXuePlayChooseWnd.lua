local CCommonUITween = import "L10.UI.CCommonUITween"
local GameObject = import "UnityEngine.GameObject"

local QnRadioBox = import "L10.UI.QnRadioBox"
local UILabel = import "UILabel"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaChristmas2023BingXuePlayChooseWnd = class()
RegistClassMember(LuaChristmas2023BingXuePlayChooseWnd, "m_SelectIndex")       -- 选择的区域
RegistClassMember(LuaChristmas2023BingXuePlayChooseWnd, "m_CountDownTickTask") -- 倒计时任务
RegistClassMember(LuaChristmas2023BingXuePlayChooseWnd, "m_LeftTime")          -- 剩余秒数

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChristmas2023BingXuePlayChooseWnd, "TabGrid", "TabGrid", QnRadioBox)
RegistChildComponent(LuaChristmas2023BingXuePlayChooseWnd, "CountDownLabel", "CountDownLabel", UILabel)
RegistChildComponent(LuaChristmas2023BingXuePlayChooseWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaChristmas2023BingXuePlayChooseWnd, "tween_inout", "tween_inout", CCommonUITween)

--@endregion RegistChildComponent end

function LuaChristmas2023BingXuePlayChooseWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

    self.m_CountDownTickTask = nil
    UIEventListener.Get(self.CloseButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self.tween_inout:PlayDisapperAnimation(DelegateFactory.Action(function()
            CUIManager.CloseUI(CLuaUIResources.Christmas2023BingXuePlayChooseWnd)
        end))
    end)
end
-- 初始化选择Tab
function LuaChristmas2023BingXuePlayChooseWnd:InitTab()
    self.TabGrid.OnSelect = DelegateFactory.Action_QnButton_int(function(go, index)
        self.m_SelectIndex = index
        Gac2Gas.BingXuePlay2023ChoseCircleId(index + 1) -- 发送选择
    end)
    self.TabGrid:ChangeTo(2, true) -- 默认选择中间
    self.m_SelectIndex = 2
end
-- 开启倒计时
function LuaChristmas2023BingXuePlayChooseWnd:StartCountDown()
    self.m_LeftTime = Christmas2023_BingXuePlay.GetData().InteractiveDuration
    self.CountDownLabel.text = SafeStringFormat3(LocalString.GetString("凛冬之王将在[fffe91]%d[-]秒后进攻"), self.m_LeftTime)
    self.m_CountDownTickTask = RegisterTick(function()
        self.m_LeftTime = self.m_LeftTime - 1
        if self.m_LeftTime <= 0 then -- 倒计时归零隐藏
            if self.m_CountDownTickTask then
                UnRegisterTick(self.m_CountDownTickTask)
                self.m_CountDownTickTask = nil
            end
            self.tween_inout:PlayDisapperAnimation(DelegateFactory.Action(function()
                CUIManager.CloseUI(CLuaUIResources.Christmas2023BingXuePlayChooseWnd)
            end))
            return
        end
        self.CountDownLabel.text = SafeStringFormat3(LocalString.GetString("凛冬之王将在[fffe91]%d[-]秒后进攻"), self.m_LeftTime)
    end, 1000)
end
function LuaChristmas2023BingXuePlayChooseWnd:Init()
    self:InitTab()
    self:StartCountDown()
end
function LuaChristmas2023BingXuePlayChooseWnd:OnEnable()
    self.tween_inout:PlayAppearAnimation()
end
function LuaChristmas2023BingXuePlayChooseWnd:OnDisable()
    if self.m_CountDownTickTask then
        UnRegisterTick(self.m_CountDownTickTask)
        self.m_CountDownTickTask = nil
    end
end

--@region UIEvent

--@endregion UIEvent

