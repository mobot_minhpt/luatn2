local GameObject = import "UnityEngine.GameObject"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CUITexture = import "L10.UI.CUITexture"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CIMMgr = import "L10.Game.CIMMgr"

LuaHuiLiuFriendWnd = class()
RegistChildComponent(LuaHuiLiuFriendWnd,"closeBtn", "CloseButton", GameObject)
RegistChildComponent(LuaHuiLiuFriendWnd,"dayLabel", UILabel)
RegistChildComponent(LuaHuiLiuFriendWnd,"p1", GameObject)
RegistChildComponent(LuaHuiLiuFriendWnd,"p2", GameObject)
RegistChildComponent(LuaHuiLiuFriendWnd,"cancelBtn", GameObject)

--RegistClassMember(LuaHuiLiuFriendWnd, "maxChooseNum")

function LuaHuiLiuFriendWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.HuiLiuFriendWnd)
end

function LuaHuiLiuFriendWnd:OnEnable()
	--g_ScriptEvent:AddListener("", self, "")
end

function LuaHuiLiuFriendWnd:OnDisable()
	--g_ScriptEvent:RemoveListener("", self, "")
end

function LuaHuiLiuFriendWnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	local onCancelClick = function(go)
		MessageWndManager.ShowOKCancelMessage(LocalString.GetString('确认解除伙伴关系'), DelegateFactory.Action(function ()
			Gac2Gas.RequestRemoveJieBanPlayer()
			self:Close()
		end), nil, nil, nil, false)
	end
	CommonDefs.AddOnClickListener(self.cancelBtn,DelegateFactory.Action_GameObject(onCancelClick),false)
	self:InitTime()

  local huiLiuData = CClientMainPlayer.Inst.PlayProp.HuiGuiJieBanData
	local selfData = {CClientMainPlayer.Inst.Id,CClientMainPlayer.Inst.Name,CClientMainPlayer.Inst.Level,EnumToInt(CClientMainPlayer.Inst.Class),EnumToInt(CClientMainPlayer.Inst.Gender)}
	local friendData = CIMMgr.Inst:GetBasicInfo(huiLiuData.JieBanPlayerId)
	local playerData = {friendData.ID,friendData.Name,friendData.Level,friendData.Class,friendData.Gender}

	self:InitPlayerNode(self.p1,selfData)
	self:InitPlayerNode(self.p2,playerData)
end

function LuaHuiLiuFriendWnd:InitTime()
  local huiLiuData = CClientMainPlayer.Inst.PlayProp.HuiGuiJieBanData
	local serverTime = CServerTimeMgr.Inst.timeStamp - huiLiuData.JieBanTime
	local dayTime = math.ceil(serverTime / 3600 / 24)
	if dayTime < 1 then
		dayTime = 1
	end

	self.dayLabel.text = dayTime
end

function LuaHuiLiuFriendWnd:InitPlayerNode(node,data)
	node.transform:Find('IconTexture'):GetComponent(typeof(CUITexture)):LoadPortrait(CUICommonDef.GetPortraitName(data[4],data[5]),true)
	node.transform:Find('name'):GetComponent(typeof(UILabel)).text = data[2]
	node.transform:Find('lv'):GetComponent(typeof(UILabel)).text = 'lv.'..data[3]
end

return LuaHuiLiuFriendWnd
