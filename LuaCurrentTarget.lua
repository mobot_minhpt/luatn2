local CCurrentTargetWnd = import "L10.UI.CCurrentTargetWnd"
local CClientMonster = import "L10.Game.CClientMonster"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CClientLingShou = import "L10.Game.CClientLingShou"
local CClientPet = import "L10.Game.CClientPet"
local CClientOtherPlayer = import "L10.Game.CClientOtherPlayer"
local CClientNpc = import "L10.Game.CClientNpc"
local CClientObjectMgr=import "L10.Game.CClientObjectMgr"
local CPetInfoMgr = import "L10.UI.CPetInfoMgr"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local BabyMgr = import "L10.Game.BabyMgr"
local EnumBabyInteractPlace = import "L10.Game.EnumBabyInteractPlace"

LuaCurrentTarget = class()

RegistChildComponent(LuaCurrentTarget, "m_ZhuoYaoSuoLian", "ZhuoYaoSuoLian", GameObject)
RegistChildComponent(LuaCurrentTarget, "m_ZhuoYaoButton", "ZhuoYaoButton", GameObject)
RegistChildComponent(LuaCurrentTarget, "m_ZhuoYaoPromptFx", "ZhuoYaoPromptFx", CUIFx)
RegistChildComponent(LuaCurrentTarget, "m_ZhuoYaoRateLabel", "ZhuoYaoRateLabel", UILabel)

RegistClassMember(LuaCurrentTarget,  "m_Wnd")
RegistClassMember(LuaCurrentTarget,  "m_LastZhuoYaoBtnStatus")
RegistClassMember(LuaCurrentTarget,  "m_ReadyCatchMonsterID")
RegistClassMember(LuaCurrentTarget,  "m_CurrentTargetCanCatch")

function LuaCurrentTarget:Awake()
    self.m_ZhuoYaoPromptFx:LoadFx("Fx/UI/Prefab/UI_kuang_yellow02.prefab")
    self.m_ZhuoYaoSuoLian:SetActive(false)
    self.m_Wnd = self.gameObject:GetComponent(typeof(CCurrentTargetWnd))
    self:InitZhuoYaoBtn()
end

function LuaCurrentTarget:OnTargetChange()
    if LuaZhuoYaoMgr:IsFeatureOpen() and not LuaZongMenMgr.m_OpenNewSystem then
        self.m_LastZhuoYaoBtnStatus = false
        self:ShowZhuoYaoPromptFx()
        self:CheckCanCatch()
        self:UpdateZhuoYaoRateLabel()
    end 
end

function LuaCurrentTarget:OnHpOrCurrentHpFullUpdate(args)
    local engineId = args[0]
    local target = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Target

    if target then
        if LuaZhuoYaoMgr:IsFeatureOpen() and not LuaZongMenMgr.m_OpenNewSystem then
            self:ShowZhuoYaoPromptFx()
            if target.EngineId == engineId then
                self:CheckCanCatch()
            end
        end
    end
end

function LuaCurrentTarget:ShowZhuoYaoPromptFx()
    self.m_ZhuoYaoSuoLian:SetActive(LuaZhuoYaoMgr:CheckIsCatched(self.m_Wnd:RealEngineId()))
end

function LuaCurrentTarget:OnTryCatchYaoGuai()
    if LuaZhuoYaoMgr:CheckIsCatched(self.m_Wnd:RealEngineId()) then
        self.m_ZhuoYaoPromptFx.transform.transform.localPosition = Vector3(130,9,0)
        local tween = LuaTweenUtils.TweenPosition(self.m_ZhuoYaoPromptFx.transform,-239,489,0,1)
        LuaTweenUtils.SetTarget(tween,self.transform)
    end
end

function LuaCurrentTarget:OnDisable()
    LuaTweenUtils.DOKill(self.transform, false)
end

function LuaCurrentTarget:InitZhuoYaoBtn()
    UIEventListener.Get(self.m_ZhuoYaoButton).onClick = DelegateFactory.VoidDelegate(function(go) 
        if not (LuaZhuoYaoMgr:IsFeatureOpen() and not LuaZongMenMgr.m_OpenNewSystem) then return end
        LuaZhuoYaoMgr:CatchYaoGuai(self.m_ReadyCatchMonsterID)
        --self:CheckCanCatch()
    end)
    self.m_ZhuoYaoButton:SetActive(false)
    self.m_LastZhuoYaoBtnStatus = false
end

function LuaCurrentTarget:CheckCanCatch()
    if not (LuaZhuoYaoMgr:IsFeatureOpen() and not LuaZongMenMgr.m_OpenNewSystem) then return end
    local res = false
    local monster = self:GetTargetMonster()
    if monster then
        if not LuaZhuoYaoMgr:CheckIsCatched(monster.EngineId) then 
            local tujianID = LuaZhuoYaoMgr:GetTuJianId(monster.TemplateId)
            if tujianID then
                if monster.HpFull > 0 then
                    if monster.Hp > 0 and monster.Hp <= LuaZhuoYaoMgr:GetHpThreshold(monster.HpFull) then  
                        local data = ZhuoYao_TuJian.GetData(tujianID)
                        if data.Grade < (LuaZhuoYaoMgr.m_Level + 3) then  
                            if (monster.Level - CClientMainPlayer.Inst.MaxLevel) <21 then
                                self.m_ReadyCatchMonsterID = monster.EngineId
                                res = true
                            end                     
                        end
                    end
                end
            end
        end
    end

    if self.m_LastZhuoYaoBtnStatus ~= res then
        self.m_LastZhuoYaoBtnStatus = res
        if res == false then
            self.m_ReadyCatchMonsterID = 0
        end
        self.m_ZhuoYaoButton:SetActive(res)
    end
end

function LuaCurrentTarget:OnEnable()
    g_ScriptEvent:AddListener("OnTryCatchYaoGuai", self, "OnTryCatchYaoGuai")
    g_ScriptEvent:AddListener("SetItemAt", self, "OnSetItemAt")
end

function LuaCurrentTarget:OnDisable()
    g_ScriptEvent:RemoveListener("OnTryCatchYaoGuai", self, "OnTryCatchYaoGuai")
    g_ScriptEvent:RemoveListener("SetItemAt", self, "OnSetItemAt")
end

function LuaCurrentTarget:UpdateZhuoYaoRateLabel()
    if not (LuaZhuoYaoMgr:IsFeatureOpen() and not LuaZongMenMgr.m_OpenNewSystem) then return end
    local monster = self:GetTargetMonster()
    if monster then
        local tujianID = LuaZhuoYaoMgr:GetTuJianId(monster.TemplateId)
        if tujianID then
            local data = ZhuoYao_TuJian.GetData(tujianID)
            if data then
                local hasCatchItem, hasFuLongSuoItem, hasSheHunXiangItem = self:CheckHasZhuoYaoItem()
                local rate = hasFuLongSuoItem and 100 or math.min(100, math.floor(LuaZhuoYaoMgr:GetCatchRate(data,monster.Level) * 100) + (hasSheHunXiangItem and 20 or 0))
                self.m_ZhuoYaoRateLabel.text = String.Format(LocalString.GetString("{0}%成功"),rate) 
                self.m_ZhuoYaoRateLabel.gameObject:SetActive(hasCatchItem)
            end
        end
    end
end

function LuaCurrentTarget:GetTargetMonster()
    if CClientMainPlayer.Inst then
        local target = CClientMainPlayer.Inst.Target
        if target then
            local monster = TypeAs(target, typeof(CClientMonster)) 
            if monster then
                return monster
            end
        end
    end
    return nil
end

function LuaCurrentTarget:OnSetItemAt( args ) 
    if not (LuaZhuoYaoMgr:IsFeatureOpen() and not LuaZongMenMgr.m_OpenNewSystem) then return nil end
    local  place, position, oldItemId, newItemId = args[0],args[1],args[2],args[3]
    local item = CItemMgr.Inst:GetById(newItemId)
    local needcheck = oldItemId and not newItemId
    if item then
        local templateId = item.TemplateId
        local fuLongPieceItemId, catchItemId = LuaZhuoYaoMgr:GetFuLongPieceItemAndCatchItem()
        local shehunxiangItemId = 21021027
        needcheck = needcheck or (fuLongPieceItemId == templateId) or (catchItemId == templateId) or (shehunxiangItemId == templateId)
    end
    if needcheck then
        self:UpdateZhuoYaoRateLabel()
    end
end

function LuaCurrentTarget:CheckHasZhuoYaoItem()
    local bagSize = CClientMainPlayer.Inst.ItemProp:GetPlaceSize(EnumItemPlace.Bag)
    local fuLongPieceItemId, catchItemId = LuaZhuoYaoMgr:GetFuLongPieceItemAndCatchItem()
    local shehunxiangItemId = 21021027
    local hasCatchItem, hasFuLongSuoItem, hasSheHunXiangItem = false,false, false
    for i = 1, bagSize, 1 do
        local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
        local item = CItemMgr.Inst:GetById(id)
        if item then
            local templateId = item.TemplateId
            hasFuLongSuoItem =  hasFuLongSuoItem or (templateId == fuLongPieceItemId)
            hasCatchItem = hasCatchItem or (templateId == fuLongPieceItemId) or (templateId == catchItemId)
            hasSheHunXiangItem = hasSheHunXiangItem or (templateId == shehunxiangItemId)
        end
    end
    return hasCatchItem, hasFuLongSuoItem, hasSheHunXiangItem
end

CCurrentTargetWnd.m_UpdateNameAction = function(this, obj)
    local info, ownerInfo

    if TypeIs(obj, typeof(CClientLingShou)) then
        local ownerId = obj.m_OwnerEngineId
        ownerInfo = LuaGamePlayMgr:GetNameBoardInfo(ownerId)
        info = LuaGamePlayMgr:GetNameBoardInfo(obj.EngineId) or (ownerInfo and ownerInfo.lingShouInfo)
        
    elseif TypeIs(obj, typeof(CClientPet)) then
        local ownerId = obj.m_OwnerEngineId
        ownerInfo = LuaGamePlayMgr:GetNameBoardInfo(ownerId)
        info = LuaGamePlayMgr:GetNameBoardInfo(obj.EngineId) or (ownerInfo and ownerInfo.petInfo)
    elseif TypeIs(obj, typeof(CClientOtherPlayer)) or TypeIs(obj, typeof(CClientMainPlayer)) then
        local playerId = obj.BasicProp and obj.BasicProp.Id
        info =  LuaGamePlayMgr:GetNameBoardInfo(obj.EngineId) or LuaGamePlayMgr:GetPlayerNameBoardInfo(playerId)
    end

    if info and info.syncTargetWnd ~= false then
        this.nameLabel.text = info.displayName or ""
        this.nameLabel.width = this.maxNameLabelWidth
        this.nameLabel:UpdateNGUIText()
        NGUIText.regionWidth = 1000000
        local size = NGUIText.CalculatePrintedSize(info.displayName or "")
        if size.x < this.maxNameLabelWidth then this.nameLabel.width = math.ceil(size.x) end
    end
    --分身跟随主角的名字
    if TypeIs(obj, typeof(CClientPet)) and obj.IsShadow and ownerInfo and ownerInfo.displayName then
        this.nameLabel.text = ownerInfo.displayName
    end
end

CCurrentTargetWnd.m_OnPortraitButtonClick_CS2LuaHook = function(this, go)
    local obj = CClientObjectMgr.Inst:GetObject(this.targetEngineId) --这里使用真实的engineId，和端游类似，刀客分身不做特殊处理
    if TypeIs(obj, typeof(CClientLingShou)) then
        local ownerId = obj.m_OwnerEngineId
        local ownerInfo = LuaGamePlayMgr:GetNameBoardInfo(ownerId)
        local info = LuaGamePlayMgr:GetNameBoardInfo(obj.EngineId) or (ownerInfo and ownerInfo.lingShouInfo)
        if info and info.onClickTargetWnd then
            info.onClickTargetWnd()
        end
    elseif TypeIs(obj, typeof(CClientPet)) then
        local ownerId = obj.m_OwnerEngineId
        local ownerInfo = LuaGamePlayMgr:GetNameBoardInfo(ownerId)
        local info = LuaGamePlayMgr:GetNameBoardInfo(obj.EngineId) or (ownerInfo and ownerInfo.petInfo)
        if info and info.onClickTargetWnd then
            info.onClickTargetWnd()
        else
            CPetInfoMgr.ShowPetInfoWnd(this.targetEngineId, go.transform.position)
        end
    elseif TypeIs(obj, typeof(CClientOtherPlayer)) then
        local playerId = obj.BasicProp and obj.BasicProp.Id
        local info =  LuaGamePlayMgr:GetNameBoardInfo(obj.EngineId) or LuaGamePlayMgr:GetPlayerNameBoardInfo(playerId)
        if info and info.onClickTargetWnd then
            info.onClickTargetWnd()
        else
            local mainPlayer = CClientMainPlayer.Inst
            if mainPlayer and TypeIs(mainPlayer.Target, typeof(CClientOtherPlayer)) then
                local b = NGUIMath.CalculateAbsoluteWidgetBounds(go.transform)
                CPlayerInfoMgr.ShowPlayerPopupMenu(TypeAs(mainPlayer.Target, typeof(CClientOtherPlayer)).PlayerId, EnumPlayerInfoContext.CurrentTarget, EChatPanel.Undefined, nil, nil, Vector3(b.center.x, b.min.y, 0), CPlayerInfoMgr.AlignType.Bottom)
            end
        end
    elseif TypeIs(obj, typeof(CClientNpc)) then
        local npc = TypeAs(obj, typeof(CClientNpc))
        if BabyMgr.Inst:IsBaby(npc.TemplateId) then
            local b = NGUIMath.CalculateAbsoluteWidgetBounds(go.transform)
            BabyMgr.Inst.InteractPos = Vector3(b.center.x, b.min.y, 0)
            Gac2Gas.QueryBabyNpcInteract(this.targetEngineId, EnumToInt(EnumBabyInteractPlace.eTarget))
        end
    elseif TypeIs(obj, (typeof(CClientMainPlayer))) then
        local playerId = obj.BasicProp and obj.BasicProp.Id
        local info =  LuaGamePlayMgr:GetNameBoardInfo(obj.EngineId) or LuaGamePlayMgr:GetPlayerNameBoardInfo(playerId)
        if info and info.onClickTargetWnd then
            info.onClickTargetWnd()
        else
            -- ...
        end
    end
end
