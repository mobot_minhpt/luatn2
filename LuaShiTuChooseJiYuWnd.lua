local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UITabBar = import "L10.UI.UITabBar"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local QnTableView = import "L10.UI.QnTableView"

LuaShiTuChooseJiYuWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaShiTuChooseJiYuWnd, "TabBar", "TabBar", UITabBar)
RegistChildComponent(LuaShiTuChooseJiYuWnd, "ReadMeLabel", "ReadMeLabel", UILabel)
RegistChildComponent(LuaShiTuChooseJiYuWnd, "Button", "Button", GameObject)
RegistChildComponent(LuaShiTuChooseJiYuWnd, "TableView", "TableView", QnTableView)

--@endregion RegistChildComponent end
RegistClassMember(LuaShiTuChooseJiYuWnd, "m_Data")
RegistClassMember(LuaShiTuChooseJiYuWnd, "m_SelectRow")

function LuaShiTuChooseJiYuWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.Button.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButtonClick()
	end)


    --@endregion EventBind end
end

function LuaShiTuChooseJiYuWnd:Init()
	self.ReadMeLabel.text = g_MessageMgr:FormatMessage("ShiTuChooseJiYuWnd_ReadMe")
	self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return self.m_Data and #self.m_Data or 0
        end,
        function(item,index)
            self:InitItem(item,index)
        end)
	self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
		self:OnSelectAtRow(row)
	end)
	self:InitTabBar()
end

function LuaShiTuChooseJiYuWnd:InitTabBar()
	self.TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
	    self:OnTabChange(index)
	end)
	self.TabBar:ChangeTab(0, false)
	local jiYuTypeName = ShiTu_Setting.GetData().JiYuTypeName
	for i = 0, self.TabBar.tabButtons.Length - 1 do
		self.TabBar.tabButtons[i].Text = jiYuTypeName[i]
	end
end

function LuaShiTuChooseJiYuWnd:InitItem(item, index)
	local data = self.m_Data[index + 1]
	local label = item.transform:Find("Label"):GetComponent(typeof(UILabel))
	label.text = data.Content
end

--@region UIEvent

function LuaShiTuChooseJiYuWnd:OnButtonClick()
	if not self.m_SelectRow then
		g_MessageMgr:ShowMessage("ShiTuChooseJiYuWnd_Select_Alert")
		return
	end
	local data = self.m_Data[self.m_SelectRow + 1]
	if LuaShiTuMgr.m_ChuShiGiftInfo.jiyuId then
		LuaShiTuMgr:ModifyChuShiJiYuSucc(LuaShiTuMgr.m_ChuShiGiftInfo.tudiId, data.ID)
	else
		Gac2Gas.ModifyChuShiJiYu(LuaShiTuMgr.m_ShiTuChooseJiYuWnd_TuDiId, data.ID)
	end
	CUIManager.CloseUI(CLuaUIResources.ShiTuChooseJiYuWnd)
end

function LuaShiTuChooseJiYuWnd:OnSelectAtRow(row)
	self.m_SelectRow = row
end

function LuaShiTuChooseJiYuWnd:OnTabChange(index)
	self.m_SelectRow = nil
	self.m_Data = {}
	ShiTu_ChuShiJiYu.Foreach(function (k,v)
		if v.Class == (index + 1) then
			table.insert(self.m_Data,ShiTu_ChuShiJiYu.GetData(k))
		end
	end)
	self.TableView:ReloadData(true, true)
end
--@endregion UIEvent

