local UILabel = import "UILabel"
local CServerTimeMgr= import "L10.Game.CServerTimeMgr"
local UInt32 = import "System.UInt32"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CWorldMapInfo = import "L10.UI.CWorldMapInfo"
local DelegateFactory  = import "DelegateFactory"
local CScene = import "L10.Game.CScene"
local CTrackMgr = import "L10.Game.CTrackMgr"
local UICamera = import "UICamera"
local CSharpResourceLoader= import "L10.Game.CSharpResourceLoader"
local Plane = import "UnityEngine.Plane"

LuaJuDianBattleWorldMapInfoRoot = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaJuDianBattleWorldMapInfoRoot, "WorldMap", "WorldMap", GameObject)
RegistChildComponent(LuaJuDianBattleWorldMapInfoRoot, "SelfServerBtn", "SelfServerBtn", GameObject)
RegistChildComponent(LuaJuDianBattleWorldMapInfoRoot, "ZhuDianBtn", "ZhuDianBtn", GameObject)
RegistChildComponent(LuaJuDianBattleWorldMapInfoRoot, "TitleLabel", "TitleLabel", UILabel)

--@endregion RegistChildComponent end

function LuaJuDianBattleWorldMapInfoRoot:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.SelfServerBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSelfServerBtnClick()
	end)


	
	UIEventListener.Get(self.ZhuDianBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnZhuDianBtnClick()
	end)


    --@endregion EventBind end
end

function LuaJuDianBattleWorldMapInfoRoot:InitMap()
    -- 初始化MapList
    if not self.m_MapId2Item then
        self.m_MapId2Item = {}
        local mapInfoList = CommonDefs.GetComponentsInChildren_Component_Type(self.WorldMap.transform, typeof(CWorldMapInfo))
        print(mapInfoList, mapInfoList.Length)
        for i=0, mapInfoList.Length -1 do
            local mapInfo = mapInfoList[i]
            print(mapInfo, mapInfo.TemplateId, mapInfo.gameObject)
            self.m_MapId2Item[mapInfo.TemplateId] = mapInfo.gameObject
        end
    end
end

function LuaJuDianBattleWorldMapInfoRoot:OnWorldMapData(battleFieldId, currentStage, statusEndTime, mapList)
    self.m_Stage = currentStage
    self.m_StageEndTime = statusEndTime
    self.m_MapDataList = mapList
    self:InitMap()

    for id, item in pairs(self.m_MapId2Item) do
        local data = mapList[id]
        print("OnWorldMapData", item, data, id)
        self:InitMapItem(item, data, id)
    end

    self.m_TimeStamp = self.m_StageEndTime - CServerTimeMgr.Inst.timeStamp
    self:UpdataTimeTick()

    self.m_TimeTick = RegisterTick(function()
        self:UpdataTimeTick()
    end, 1000)
end

function LuaJuDianBattleWorldMapInfoRoot:UpdataTimeTick()
    if self.m_TimeStamp == nil or self.m_TimeStamp < 0 then
        self.m_TimeStamp = 0
    end

    local mins = math.floor(self.m_TimeStamp/60)
    local secs = math.floor((self.m_TimeStamp% 60))

    if self.m_Stage == EnumJuDianPlayStage.ePrepare then
        self.TitleLabel.text = SafeStringFormat3(LocalString.GetString("[ACF8FF]入场准备中，将于%02d:%02d后开始"), mins, secs)
    elseif self.m_Stage == EnumJuDianPlayStage.eInFight then
        self.TitleLabel.text = SafeStringFormat3(LocalString.GetString("[FFFE91]人间棋局进行中%02d:%02d"), mins, secs)
    else
        self.TitleLabel.text = LocalString.GetString("")
    end

    self.m_TimeStamp = self.m_TimeStamp - 1
end

function LuaJuDianBattleWorldMapInfoRoot:InitMapItem(item, mapData, mapid)
    local state = EnumJuDianBattleMapState.NotChosen

    local hasTag = false
    if mapData then
        state = mapData.state
        hasTag = mapData.tag
    end

    -- 测试
    -- state = EnumJuDianBattleMapState.Taken
    -- mapData = {}
    -- mapData.guildId = 123
    -- mapData.guildName = "123"
    -- mapData.supplyEndTime = 123

    local selfGuildId = LuaJuDianBattleMgr:GetSelfGuildId()

    local leaderMark = item.transform:Find("BattleInfo/LeaderMark").gameObject
    local mainplayerTexture = item.transform:Find("BattleInfo/MainplayerTexture"):GetComponent(typeof(CUITexture))

    -- 状态
    local battleInfo = item.transform:Find("BattleInfo").gameObject
    battleInfo:SetActive(state ~= EnumJuDianBattleMapState.NotChosen)

    local battleState = item.transform:Find("BattleInfo/StateRoot/BattleState").gameObject
    local selfOccupyState = item.transform:Find("BattleInfo/StateRoot/SelfOccupyState").gameObject
    local otherOccupyState = item.transform:Find("BattleInfo/StateRoot/OtherOccupyState").gameObject
    local namelessOccupyState = item.transform:Find("BattleInfo/StateRoot/NamelessOccupyState").gameObject
    local monsterState = item.transform:Find("BattleInfo/StateRoot/MonsterState").gameObject
    local mapTexture = item.transform:Find("Texture"):GetComponent(typeof(UISprite))

    leaderMark:SetActive(false)
    mainplayerTexture.gameObject:SetActive(false)
    battleState:SetActive(false)
    selfOccupyState:SetActive(false)
    otherOccupyState:SetActive(false)
    namelessOccupyState:SetActive(false)
    monsterState:SetActive(false)

    if hasTag then
        leaderMark:SetActive(true)
    end

    -- 玩家头像
    if CScene.MainScene and CScene.MainScene.SceneTemplateId == mapid then
		mainplayerTexture.gameObject:SetActive(true)
        if CClientMainPlayer.Inst then
            local path = CUICommonDef.GetPortraitName(CClientMainPlayer.Inst.Class, CClientMainPlayer.Inst.Gender, -1)
            mainplayerTexture:LoadNPCPortrait(path, false)
        end
    end

    if state == EnumJuDianBattleMapState.NotTaken then
        battleState:SetActive(true)
    elseif state == EnumJuDianBattleMapState.Taken then
        -- 本帮占领
        if mapData.guildId  == selfGuildId then
            selfOccupyState:SetActive(true)
            local nameLabel = selfOccupyState.transform:Find("Label"):GetComponent(typeof(UILabel))
            nameLabel.text = mapData.guildName
        else
            -- 外帮占领
            otherOccupyState:SetActive(true)
            local nameLabel = otherOccupyState.transform:Find("Label"):GetComponent(typeof(UILabel))
            nameLabel.text = mapData.guildName
        end
    elseif state == EnumJuDianBattleMapState.Supply then
        UnRegisterTick(self.m_NpcTimeTick)

        namelessOccupyState:SetActive(true)

        self.m_NpcStateLabel = namelessOccupyState.transform:Find("Label"):GetComponent(typeof(UILabel))
        self.m_NpcStateLabel.gameObject:SetActive(true)
        self.m_NpcTimeStamp = mapData.supplyEndTime - CServerTimeMgr.Inst.timeStamp
        self.m_NpcShow = mapData.bShow
        self:UpdataNpcTimeTick()

        self.m_NpcTimeTick = RegisterTick(function()
            self:UpdataNpcTimeTick()
        end, 1000)
    elseif state == EnumJuDianBattleMapState.Monster then
        local countLabel = monsterState.transform:Find("Label"):GetComponent(typeof(UILabel))
        countLabel.text = "×" .. mapData.monsterCount
    end

    if state == EnumJuDianBattleMapState.NotChosen then
        CUICommonDef.SetActive(item, false, true)
        mapTexture.color = Color(1,1,1,0.5)
    else
        mapTexture.color = Color(1,1,1,1)
        CUICommonDef.SetActive(item, true, true)
    end
end

function LuaJuDianBattleWorldMapInfoRoot:UpdataNpcTimeTick()
    if self.m_NpcTimeStamp < 0 then
        self.m_NpcTimeStamp = 0
    end

    local mins = math.floor(self.m_NpcTimeStamp/60)
    local secs = math.floor((self.m_NpcTimeStamp% 60))

    if self.m_NpcTimeStamp == 0 then
        self.m_NpcStateLabel.text = LocalString.GetString("无名神像")
    elseif self.m_NpcShow then
        self.m_NpcStateLabel.text = SafeStringFormat3(LocalString.GetString("无名神像[ACF8FF]%02d:%02d[-]后消失"), mins, secs)
    else
        self.m_NpcStateLabel.text = SafeStringFormat3(LocalString.GetString("无名神像[ACF8FF]%02d:%02d[-]后出现"), mins, secs)
    end
    
    self.m_NpcTimeStamp = self.m_NpcTimeStamp - 1
end

-- 外围玩法
function LuaJuDianBattleWorldMapInfoRoot:OnDailyData(data)
    self:InitMap()
    self.TitleLabel.gameObject:SetActive(false)
    self.m_DailyData = LuaJuDianBattleMgr.SceneMonsterData

    for id, item in pairs(self.m_MapId2Item) do
        local monsterList = self.m_DailyData[id]

        local data = {}
        
        if monsterList then
            data.state = EnumJuDianBattleMapState.Monster
            data.monsterCount = #monsterList
        else
            data.state = EnumJuDianBattleMapState.NotChosen
        end

        self:InitMapItem(item, data, id)
    end
end

function LuaJuDianBattleWorldMapInfoRoot:OnEnable()
    self:InitMap()
    -- 请求据点世界地图数据
    Gac2Gas.GuildJuDianQuerySceneOverview()
    g_ScriptEvent:AddListener("GuildJuDianReplySceneOverview", self, "OnWorldMapData")
    g_ScriptEvent:AddListener("JuDianBattle_SyncDailySceneData", self, "OnDailyData")
end

function LuaJuDianBattleWorldMapInfoRoot:OnDisable()
    UnRegisterTick(self.m_NpcTimeTick)
    UnRegisterTick(self.m_TimeTick)
    g_ScriptEvent:RemoveListener("GuildJuDianReplySceneOverview", self, "OnWorldMapData")
    g_ScriptEvent:RemoveListener("JuDianBattle_SyncDailySceneData", self, "OnDailyData")
end



--@region UIEvent

function LuaJuDianBattleWorldMapInfoRoot:OnSelfServerBtnClick()
    LuaJuDianBattleMgr:RequestBackSelfSever()
end

function LuaJuDianBattleWorldMapInfoRoot:OnZhuDianBtnClick()
    -- 回到驻点
    Gac2Gas.GuildJuDianRequestGoToOccupation()
end


--@endregion UIEvent

