local CButton = import "L10.UI.CButton"
local CUITexture = import "L10.UI.CUITexture"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local EnumSkillInfoContext = import "L10.UI.CSkillInfoMgr+EnumSkillInfoContext"

LuaPetAdventureStartWnd = class()

RegistChildComponent(LuaPetAdventureStartWnd, "TimeLabel", UILabel)
RegistChildComponent(LuaPetAdventureStartWnd, "AwardLeftLabel", UILabel)

RegistChildComponent(LuaPetAdventureStartWnd, "RuleLabel", UILabel)
RegistChildComponent(LuaPetAdventureStartWnd, "Skill1", GameObject)
RegistChildComponent(LuaPetAdventureStartWnd, "Skill2", GameObject)

RegistChildComponent(LuaPetAdventureStartWnd, "ApplyBtn", CButton)
RegistChildComponent(LuaPetAdventureStartWnd, "DismissApplyBtn", CButton)
RegistChildComponent(LuaPetAdventureStartWnd, "MatchingHint", UILabel)

function LuaPetAdventureStartWnd:Init()
    self.MatchingHint.gameObject:SetActive(false)

    local activityTimeMsg = g_MessageMgr:FormatMessage("Pet_Adventure_Time")
    local activityRule = g_MessageMgr:FormatMessage("Pet_Adventure_Rule")

    self.TimeLabel.text = activityTimeMsg
    self.RuleLabel.text = activityRule
    self.DismissApplyBtn.gameObject:SetActive(false)

    CommonDefs.AddOnClickListener(self.ApplyBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
        self:OnSingleApplyBtnClicked(go)
    end), false)

    CommonDefs.AddOnClickListener(self.DismissApplyBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
        self:OnDismissApplyBtnClicked(go)
    end), false)

    local skills = HanJia2021_AdventureSetting.GetData().Skills
    self:InitSkillGO(self.Skill1, skills[0])
    self:InitSkillGO(self.Skill2, skills[1])

    local color = LuaColorUtils.RedColorTx
    if LuaHanJiaMgr.PetAdventureLeftRewards > 0 then
        color = LuaColorUtils.WhiteColorTx
    end
    self.AwardLeftLabel.text = SafeStringFormat3(LocalString.GetString("[ACF8FF]本周剩余奖励次数[-] [%s]%d[-]"), color, LuaHanJiaMgr.PetAdventureLeftRewards)

    self:UpdateButtonStatus()
end

--@desc 请求报名
function LuaPetAdventureStartWnd:OnSingleApplyBtnClicked(go)
    Gac2Gas.RequestSignUpHanJiaAdventure()
end

--@desc 取消报名
function LuaPetAdventureStartWnd:OnDismissApplyBtnClicked(go)
    Gac2Gas.RequestCancelSignUpHanJiaAdventure()
end

function LuaPetAdventureStartWnd:UpdateButtonStatus()
    self.DismissApplyBtn.gameObject:SetActive(LuaHanJiaMgr.PetAdventureSignUp)
    self.ApplyBtn.gameObject:SetActive(not LuaHanJiaMgr.PetAdventureSignUp)
    self.MatchingHint.gameObject:SetActive(LuaHanJiaMgr.PetAdventureSignUp)
end


function LuaPetAdventureStartWnd:InitSkillGO(go, skillTId)
    if not go then return end
    local icon = go.transform:Find("Mask/Icon"):GetComponent(typeof(CUITexture))
    local skillName = go.transform:Find("SkillName"):GetComponent(typeof(UILabel))
    local skillDisplay = go.transform:Find("SkillDisplay"):GetComponent(typeof(UILabel))

    local skillId = Skill_AllSkills.GetSkillId(skillTId, 1)
    local skill = Skill_AllSkills.GetData(skillId)

    icon:LoadSkillIcon(skill.SkillIcon)
    skillName.text = skill.Name
    skillDisplay.text = skill.Display_jian

    CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(function (go)
        CSkillInfoMgr.ShowSkillInfoWnd(skillId, go.transform.position, EnumSkillInfoContext.Default, true, 0, 0, nil)
    end), false)
end

function LuaPetAdventureStartWnd:UpdateSignUpStatus()
    self:UpdateButtonStatus()
end

function LuaPetAdventureStartWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncHanJiaAdventureSignUpResult", self, "UpdateSignUpStatus")
end

function LuaPetAdventureStartWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncHanJiaAdventureSignUpResult", self, "UpdateSignUpStatus")
end