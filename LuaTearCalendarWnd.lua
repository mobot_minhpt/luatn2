local UIPanel = import "UIPanel"

local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

local Animation = import "UnityEngine.Animation"

LuaTearCalendarWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaTearCalendarWnd, "SlideHelp", "SlideHelp", GameObject)
RegistChildComponent(LuaTearCalendarWnd, "FrontPage", "FrontPage", GameObject)
RegistChildComponent(LuaTearCalendarWnd, "BackPage", "BackPage", GameObject)
RegistChildComponent(LuaTearCalendarWnd, "FrontLabel", "FrontLabel", GameObject)
RegistChildComponent(LuaTearCalendarWnd, "BackLabel", "BackLabel", GameObject)
RegistChildComponent(LuaTearCalendarWnd, "CalendarPanel", "CalendarPanel", GameObject)
RegistChildComponent(LuaTearCalendarWnd, "SlideTip", "SlideTip", GameObject)
RegistChildComponent(LuaTearCalendarWnd, "Spring", "Spring", GameObject)
RegistChildComponent(LuaTearCalendarWnd, "Summer", "Summer", GameObject)
RegistChildComponent(LuaTearCalendarWnd, "Autumn", "Autumn", GameObject)
RegistChildComponent(LuaTearCalendarWnd, "Winter", "Winter", GameObject)
RegistChildComponent(LuaTearCalendarWnd, "CUIFx", "CUIFx", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaTearCalendarWnd, "m_TaskId")
RegistClassMember(LuaTearCalendarWnd, "m_CalendarId")
RegistClassMember(LuaTearCalendarWnd, "m_DragStartX")
RegistClassMember(LuaTearCalendarWnd, "m_FinishAniTick")
RegistClassMember(LuaTearCalendarWnd, "m_CloseTick")
RegistClassMember(LuaTearCalendarWnd, "m_IsFinish")
RegistClassMember(LuaTearCalendarWnd, "mainAnim")

function LuaTearCalendarWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.CUIFx.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)	    self:OnCalendarLightClick()	end)

    --@endregion EventBind end
end

function LuaTearCalendarWnd:InitDrag()
	local dragGo = self.FrontPage.gameObject
    UIEventListener.Get(dragGo).onDragStart = DelegateFactory.VoidDelegate(function(go)
		self:OnCalendarPanelDragStart(go)
    end)

    UIEventListener.Get(dragGo).onDragEnd = DelegateFactory.VoidDelegate(function(go)
		self:OnCalendarPanelDragEnd(go)
    end)
end

function LuaTearCalendarWnd:Init()
	self.m_CalendarId = LuaLiuRuShiMgr.m_TearCalendarInfo.calendarId or 1
	self.m_TaskId = LuaLiuRuShiMgr.m_TearCalendarInfo.taskId or 0

	self.Spring:SetActive(false)
	self.Summer:SetActive(false)
	self.Autumn:SetActive(false)
	self.Winter:SetActive(false)

	local design = ZhuJueJuQing_TearCalendar.GetData(self.m_CalendarId) or ZhuJueJuQing_TearCalendar.GetData(1)
	FindChild(self.FrontLabel.transform, "year"):GetComponent(typeof(UILabel)).text		= design.Year
	FindChild(self.FrontLabel.transform, "month"):GetComponent(typeof(UILabel)).text	= design.Month
	FindChild(self.FrontLabel.transform, "day"):GetComponent(typeof(UILabel)).text		= design.Day
	FindChild(self.FrontLabel.transform, "yiLabel"):GetComponent(typeof(UILabel)).text	= design.Fit
	FindChild(self.FrontLabel.transform, "jiLabel"):GetComponent(typeof(UILabel)).text	= design.Avoid
	self[design.Season]:SetActive(true)

	local nextDesign = ZhuJueJuQing_TearCalendar.GetData(self.m_CalendarId + 1) or ZhuJueJuQing_TearCalendar.GetData(1)
	FindChild(self.BackLabel.transform, "year"):GetComponent(typeof(UILabel)).text		= nextDesign.Year
	FindChild(self.BackLabel.transform, "month"):GetComponent(typeof(UILabel)).text		= nextDesign.Month
	FindChild(self.BackLabel.transform, "day"):GetComponent(typeof(UILabel)).text		= nextDesign.Day
	FindChild(self.BackLabel.transform, "yiLabel"):GetComponent(typeof(UILabel)).text	= nextDesign.Fit
	FindChild(self.BackLabel.transform, "jiLabel"):GetComponent(typeof(UILabel)).text	= nextDesign.Avoid


	self.mainAnim = self.transform:GetComponent(typeof(Animation))


	self.m_IsFinish = false
	self:DestroyAllTick()
end

function LuaTearCalendarWnd:OnDisable()
	self:DestroyAllTick()
end

function LuaTearCalendarWnd:DestroyAllTick()
	if self.m_FinishAniTick then
		UnRegisterTick(self.m_FinishAniTick)
	end
	if self.m_CloseTick then
		UnRegisterTick(self.m_CloseTick)
	end
end

function LuaTearCalendarWnd:OnCalendarPanelDragStart(go)
	self.m_DragStartX = UICamera.currentTouch.pos.x
	self.SlideHelp:SetActive(false)
	self.SlideTip:SetActive(false)
end

function LuaTearCalendarWnd:OnCalendarPanelDragEnd(go)
	if self.m_IsFinish then return end

	local diff = UICamera.currentTouch.pos.x - self.m_DragStartX
	if diff > 100 then
		self:FinishTask()
		self:OnFinishAni()
		self:StartCloseTick()
	else
		self.SlideHelp:SetActive(true)
		self.SlideTip:SetActive(true)
	end
end

function LuaTearCalendarWnd:FinishTask()
	self.m_IsFinish = true
	local empty = CreateFromClass(MakeGenericClass(List, Object))
	empty = MsgPackImpl.pack(empty)
	Gac2Gas.FinishClientTaskEventWithConditionId(self.m_TaskId, "TearCalendar", empty, self.m_CalendarId)
end

function LuaTearCalendarWnd:OnFinishAni()
	-- self.FrontPage:SetActive(false)
	self.BackPage:SetActive(true)

	self.mainAnim:Play("tearcalendarwnd_zhi")
	if self.m_FinishAniTick then
		UnRegisterTick(self.m_FinishAniTick)
	end
	local design = ZhuJueJuQing_TearCalendar.GetData(self.m_CalendarId) or ZhuJueJuQing_TearCalendar.GetData(1)
	local nextDesign = ZhuJueJuQing_TearCalendar.GetData(self.m_CalendarId + 1) or ZhuJueJuQing_TearCalendar.GetData(1)
	if nextDesign.Season ~= design.Season then
		self.m_FinishAniTick = RegisterTickOnce(function()
			self[nextDesign.Season]:SetActive(true)
			LuaTweenUtils.TweenAlpha(self[nextDesign.Season].transform, 0.5, 1, 1, function() end)
			LuaTweenUtils.TweenAlpha(self[design.Season].transform, 1, 0, 1, function() end)
		end, 400)
	end
end

function LuaTearCalendarWnd:StartCloseTick()
	if self.m_CloseTick then
		UnRegisterTick(self.m_CloseTick)
	end

	self.m_CloseTick = RegisterTickOnce(function()
        CUIManager.CloseUI(CLuaUIResources.TearCalendarWnd)
	end, 1000*4)
end

--@region UIEvent

function LuaTearCalendarWnd:OnCalendarLightClick()
	self.mainAnim:Play("tearcalendarwnd_tishi")
	self:InitDrag()
end


--@endregion UIEvent

