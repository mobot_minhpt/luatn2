local DelegateFactory  = import "DelegateFactory"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local QnCheckBoxGroup  = import "L10.UI.QnCheckBoxGroup"
local EnumMoneyType    = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"

LuaWeddingOpenGameplayWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaWeddingOpenGameplayWnd, "tip")
RegistClassMember(LuaWeddingOpenGameplayWnd, "desc")
RegistClassMember(LuaWeddingOpenGameplayWnd, "moneyCtrl")
RegistClassMember(LuaWeddingOpenGameplayWnd, "checkBoxGroup")

function LuaWeddingOpenGameplayWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
    self:InitEventListener()
end

-- 初始化UI组件
function LuaWeddingOpenGameplayWnd:InitUIComponents()
    local anchor = self.transform:Find("Anchor")
    self.tip = anchor:Find("Tip"):GetComponent(typeof(UILabel))
    self.desc = anchor:Find("Desc"):GetComponent(typeof(UILabel))
    self.moneyCtrl = anchor:Find("MoneyCtrl"):GetComponent(typeof(CCurentMoneyCtrl))
    self.okButton = anchor:Find("OKButton").gameObject

    self.checkBoxGroup = anchor:Find("CheckBoxGroup"):GetComponent(typeof(QnCheckBoxGroup))
end

function LuaWeddingOpenGameplayWnd:InitEventListener()
    UIEventListener.Get(self.okButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOKButtonClick()
	end)
end


function LuaWeddingOpenGameplayWnd:Init()
    self:InitTip()
    self:InitCheckBox()
    self:InitCost()
end

function LuaWeddingOpenGameplayWnd:InitTip()
    local gameplay = LuaWeddingIterationMgr.openedGameplay
    if gameplay == "YiXianQian" then
        self.tip.text = LocalString.GetString("请选择消耗银两或灵玉开启【一线牵】玩法:")
    elseif gameplay == "NaoDongFang" then
        self.tip.text = LocalString.GetString("请选择消耗银两或灵玉开启【同庆贺】玩法:")
    end
end

function LuaWeddingOpenGameplayWnd:InitCheckBox()
    self.checkBoxGroup:SetSelect(0, true)
    self.checkBoxGroup.OnSelect = DelegateFactory.Action_QnCheckBox_int(function(checkbox, index)
        self:OnCheckBoxSelect(index)
    end)
    self:OnCheckBoxSelect(0)
end

function LuaWeddingOpenGameplayWnd:InitCost()
    local gameplay = LuaWeddingIterationMgr.openedGameplay
    local setting = WeddingIteration_Setting.GetData()

    for i = 0, 1 do
        local costLabel = self.checkBoxGroup[i].transform:Find("Cost"):GetComponent(typeof(UILabel))
        if gameplay == "YiXianQian" then
            costLabel.text = (i == 0) and setting.YiXianQianCost or setting.YiXianQianCost2
        elseif gameplay == "NaoDongFang" then
            costLabel.text = (i == 0) and setting.NaoDongFangCost or setting.NaoDongFangCost2
        end
    end
end

--@region UIEvent

function LuaWeddingOpenGameplayWnd:OnOKButtonClick()
    if not self.moneyCtrl.moneyEnough then
		g_MessageMgr:ShowMessage("INVITE_NOT_ENOUGH_MONEY")
		return
	end

    local bLuxury = self.moneyCtrl.m_Type == EnumMoneyType.LingYu
    local cost = self.moneyCtrl:GetCost()
    local gameplay = LuaWeddingIterationMgr.openedGameplay
    if gameplay == "YiXianQian" then
        Gac2Gas.NewWeddingRequestStartYiXianQian(cost, bLuxury)
    elseif gameplay == "NaoDongFang" then
        Gac2Gas.NewWeddingRequestStartNaoDongFang(cost, bLuxury)
    end
    CUIManager.CloseUI(CLuaUIResources.WeddingOpenGameplayWnd)
end

function LuaWeddingOpenGameplayWnd:OnCheckBoxSelect(index)
    local gameplay = LuaWeddingIterationMgr.openedGameplay

    local cost
    local setting = WeddingIteration_Setting.GetData()
    if index == 0 then
        if gameplay == "YiXianQian" then
            cost = setting.YiXianQianCost
            self.desc.text = g_MessageMgr:FormatMessage("YIXIANQIAN_YINLIANG_TIPS")
        elseif gameplay == "NaoDongFang" then
            cost = setting.NaoDongFangCost
            self.desc.text = g_MessageMgr:FormatMessage("NAODONGFANG_YINLIANG_TIPS")
        end
        self.moneyCtrl:SetType(EnumMoneyType.YinLiang, EnumPlayScoreKey.NONE, true)
    elseif index == 1 then
        if gameplay == "YiXianQian" then
            cost = setting.YiXianQianCost2
            self.desc.text = g_MessageMgr:FormatMessage("YIXIANQIAN_LINGYU_TIPS")
        elseif gameplay == "NaoDongFang" then
            cost = setting.NaoDongFangCost2
            self.desc.text = g_MessageMgr:FormatMessage("NAODONGFANG_LINGYU_TIPS")
        end
        self.moneyCtrl:SetType(EnumMoneyType.LingYu, EnumPlayScoreKey.NONE, true)
    end

    self.moneyCtrl:SetCost(cost)
end

--@endregion UIEvent
