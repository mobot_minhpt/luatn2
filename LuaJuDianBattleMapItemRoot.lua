local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CServerTimeMgr= import "L10.Game.CServerTimeMgr"
local Vector3 = import "UnityEngine.Vector3"
local BoxCollider = import "UnityEngine.BoxCollider"

LuaJuDianBattleMapItemRoot = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaJuDianBattleMapItemRoot, "FlagItem", "FlagItem", GameObject)
RegistChildComponent(LuaJuDianBattleMapItemRoot, "PointItem", "PointItem", GameObject)
RegistChildComponent(LuaJuDianBattleMapItemRoot, "NamelessItem", "NamelessItem", GameObject)
RegistChildComponent(LuaJuDianBattleMapItemRoot, "MarkRoot", "MarkRoot", GameObject)
RegistChildComponent(LuaJuDianBattleMapItemRoot, "MonsterItem", "MonsterItem", GameObject)
RegistChildComponent(LuaJuDianBattleMapItemRoot, "LingQuanItem", "LingQuanItem", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaJuDianBattleMapItemRoot, "m_Root")

function LuaJuDianBattleMapItemRoot:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaJuDianBattleMapItemRoot:InitDailyMap(mapid, monsterList)
	-- 清除item
	Extensions.RemoveAllChildren(self.MarkRoot.transform)

	self.FlagItem:SetActive(false)
	self.PointItem:SetActive(false)
	self.NamelessItem:SetActive(false)
	self.MonsterItem:SetActive(false)
	self.LingQuanItem:SetActive(false)
    self.m_Root = self.transform.parent.parent:GetComponent(typeof(CCommonLuaScript))

	for i, monster in ipairs(monsterList) do
		local g = NGUITools.AddChild(self.MarkRoot, self.MonsterItem)
		g:SetActive(true)
		
		local data = {}
		data.x = monster[1]
		data.y = monster[2]

		self:SetLocation(g, data)
	end
end

function LuaJuDianBattleMapItemRoot:InitMap(mapId, index, isTagMode)
	-- 清除item
	Extensions.RemoveAllChildren(self.MarkRoot.transform)

	self.FlagItem:SetActive(false)
	self.PointItem:SetActive(false)
	self.NamelessItem:SetActive(false)
	self.MonsterItem:SetActive(false)
	self.LingQuanItem:SetActive(false)

	local setting = GuildOccupationWar_Setting.GetData()
	self.m_PointRadius = setting.StrongholdCaptureShowRadius
	self.m_NoTransportRadius = setting.ForbidTeleportShowRadius
	self.m_NoMarkRadius = setting.ForbidLabelShowRadius
	self.m_NpcRadius = setting.StatueNPCAlertShowRadius
	self.m_PointRadius_JLHZ = setting.StrongholdCaptureShowRadiusJLHZ
	self.m_NoTransportRadius_JLHZ = setting.ForbidTeleportShowRadiusJLHZ
	self.m_NoMarkRadius_JLHZ = setting.ForbidLabelShowRadiusJLHZ
	self.m_LingQuanNoTransportRadius = setting.LingQuanForbidTeleportShowRadius
	-- 请求据点地图数据
	Gac2Gas.GuildJuDianQueryMapSceneInfo(mapId, index)
	-- 请求灵泉数据
	Gac2Gas.GuildJuDianQueryQueryMapLingQuanPos(mapId)
    self.m_Root = self.transform.parent.parent:GetComponent(typeof(CCommonLuaScript))
end

function LuaJuDianBattleMapItemRoot:OnMapData(mapId, sceneIdx, sceneId, tag, flagList, juDianList, npcList)
	self.m_SelectMapId = mapId
	UnRegisterTick(self.m_NpcTimeTick)
	-- 清除item
	Extensions.RemoveAllChildren(self.MarkRoot.transform)
	
	-- 设置旗帜
	for i=1, #flagList do
		local g = NGUITools.AddChild(self.MarkRoot, self.FlagItem)
		g:SetActive(true)

		local data = flagList[i]
		self:SetLocation(g, data)
		self:InitFlag(g, data)
	end

	-- 设置据点
	for i=1, #juDianList do
		local g = NGUITools.AddChild(self.MarkRoot, self.PointItem)
		g:SetActive(true)

		local data = juDianList[i]
		self:SetLocation(g, data)
		self:InitJuDian(g, data)
	end

	-- 设置npc
	for i=1, #npcList do
		local g = NGUITools.AddChild(self.MarkRoot, self.NamelessItem)
		g:SetActive(true)

		local data = npcList[i]
		self:SetLocation(g, data)
		self:InitNpc(g, data)
	end

	self:SetShowModeItem(false, false)
end

function LuaJuDianBattleMapItemRoot:OnLingQuanData(nextRefreshTime, lingQuanPosList)
	if #lingQuanPosList == 0 then return end
	self.m_Root.m_LuaSelf:OnLingQuanRefreshTime(nextRefreshTime)
	for i = 1, #lingQuanPosList, 2 do
		local g = NGUITools.AddChild(self.MarkRoot, self.LingQuanItem)
		g:SetActive(true)
		local data = {}
		data.x = lingQuanPosList[i]
		data.y = lingQuanPosList[i+1]
		self:SetLocation(g, data)
		local noTransport = g.transform:Find("NoTransport"):GetComponent(typeof(UITexture))
		noTransport.height = self.m_LingQuanNoTransportRadius
		noTransport.width = self.m_LingQuanNoTransportRadius
		UIEventListener.Get(noTransport.gameObject).onClick = DelegateFactory.VoidDelegate(function ()
			g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("灵泉附近不可进行传送"))
		end)
	end
end

-- 设置地图模式
function LuaJuDianBattleMapItemRoot:SetShowModeItem(isLocalMap, isTagMode)
	for i =0, self.MarkRoot.transform.childCount-1 do
		local item = self.MarkRoot.transform:GetChild(i)
		local noMark = item:Find("NoMark")
		local noTransport = item:Find("NoTransport")

		if noMark then
			noMark.gameObject:SetActive(isTagMode)
		end

		if noTransport then
			noTransport.gameObject:SetActive(not isTagMode)
		end
	end
end

function LuaJuDianBattleMapItemRoot:InitFlag(item, flagData)
	local flagTexture = item.transform:Find("FlagTexture"):GetComponent(typeof(CUITexture))
	local battleIcon = item.transform:Find("BattleIcon").gameObject
	local playerLabel = item.transform:Find("PlayerLabel"):GetComponent(typeof(UILabel))
	local stateLabel = item.transform:Find("StateLabel"):GetComponent(typeof(UILabel))
	local bg = item.transform:Find("Bg"):GetComponent(typeof(UISprite))
	local noMark = item.transform:Find("NoMark"):GetComponent(typeof(UITexture))
	local noTransport = item.transform:Find("NoTransport"):GetComponent(typeof(UITexture))

	if self.m_SelectMapId == 16103048 or self.m_SelectMapId == 16103049 then
		noTransport.height = self.m_NoTransportRadius_JLHZ
		noTransport.width = self.m_NoTransportRadius_JLHZ
		noMark.height = self.m_NoMarkRadius_JLHZ
		noMark.width = self.m_NoMarkRadius_JLHZ
	else
		noTransport.height = self.m_NoTransportRadius
		noTransport.width = self.m_NoTransportRadius
		noMark.height = self.m_NoMarkRadius
		noMark.width = self.m_NoMarkRadius
	end
	local noTransportBoxCollider = noTransport.transform:GetComponent(typeof(BoxCollider))
	noTransportBoxCollider.size = Vector3(noTransport.height, noTransport.width, 0)
	local noMarkBoxCollider = noMark.transform:GetComponent(typeof(BoxCollider))
	noMarkBoxCollider.size = Vector3(noMark.height, noMark.width, 0)

	if flagData.hasTaken then
        playerLabel.text = flagData.playerName
        stateLabel.text = flagData.guildName
		local selfGuildId = 0
    	if CClientMainPlayer.Inst then
			selfGuildId = CClientMainPlayer.Inst.BasicProp.GuildId
		end

		if selfGuildId == flagData.guildId then
			-- 自己帮派
            bg.color = JuDianColor.Self
            flagTexture:LoadMaterial("UI/Texture/Transparent/Material/minimappopwnd_icon_qi_wobang.mat")
		else
			-- 敌人帮派
            bg.color = JuDianColor.Enemy
            flagTexture:LoadMaterial("UI/Texture/Transparent/Material/minimappopwnd_icon_qi_tabang.mat")
		end
	else
		-- 争夺中
        bg.color = JuDianColor.Battle
        flagTexture:LoadMaterial("UI/Texture/Transparent/Material/minimappopwnd_icon_qi_zhengduo.mat")
		playerLabel.text = ""
		stateLabel.text = LocalString.GetString("争夺中")
	end

	UIEventListener.Get(noMark.gameObject).onClick = DelegateFactory.VoidDelegate(function ()
		g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("据点、据点旗、无名神像附近不可进行标记"))
	end)

	UIEventListener.Get(noTransport.gameObject).onClick = DelegateFactory.VoidDelegate(function ()
		g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("据点、据点旗、无名神像附近不可进行传送"))
	end)
end

function LuaJuDianBattleMapItemRoot:InitJuDian(item, pointData)
	local pointTexture = item.transform:Find("PointTexture"):GetComponent(typeof(UITexture))
	local battleIcon = item.transform:Find("BattleIcon").gameObject
	local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
	local stateLabel = item.transform:Find("StateLabel"):GetComponent(typeof(UILabel))
    local bg = item.transform:Find("Bg"):GetComponent(typeof(UISprite))
	local areaTexture = item.transform:Find("AreaTexture"):GetComponent(typeof(UITexture))
	local areaSprite = item.transform:Find("AreaTexture/Sprite"):GetComponent(typeof(UITexture))
	local noMark = item.transform:Find("NoMark"):GetComponent(typeof(UITexture))
	local noTransport = item.transform:Find("NoTransport"):GetComponent(typeof(UITexture))

    battleIcon:SetActive(not pointData.hasTaken)
    nameLabel.text = LocalString.GetString("据点").. tostring(pointData.judianId)

	-- 设置半径
	if self.m_SelectMapId == 16103048 or self.m_SelectMapId == 16103049 then
		areaTexture.height = self.m_PointRadius_JLHZ
		areaTexture.width = self.m_PointRadius_JLHZ
		noTransport.height = self.m_NoTransportRadius_JLHZ
		noTransport.width = self.m_NoTransportRadius_JLHZ
		noMark.height = self.m_NoMarkRadius_JLHZ
		noMark.width = self.m_NoMarkRadius_JLHZ
	else
		areaTexture.height = self.m_PointRadius
		areaTexture.width = self.m_PointRadius
		noTransport.height = self.m_NoTransportRadius
		noTransport.width = self.m_NoTransportRadius
		noMark.height = self.m_NoMarkRadius
		noMark.width = self.m_NoMarkRadius
	end
	local noTransportBoxCollider = noTransport.transform:GetComponent(typeof(BoxCollider))
	noTransportBoxCollider.size = Vector3(noTransport.height, noTransport.width, 0)
	local noMarkBoxCollider = noMark.transform:GetComponent(typeof(BoxCollider))
	noMarkBoxCollider.size = Vector3(noMark.height, noMark.width, 0)

	if pointData.hasTaken then
        stateLabel.text = pointData.guildName

		local selfGuildId = 0
    	if CClientMainPlayer.Inst then
			selfGuildId = CClientMainPlayer.Inst.BasicProp.GuildId
		end

		if selfGuildId == pointData.guildId then
			-- 自己帮派
			bg.color = JuDianColor.Self
			areaTexture.color = JuDianColor.Self
			pointTexture.color = JuDianColor.Self
			areaSprite.color = JuDianTransparentColor.Self
		else
			-- 敌人帮派
            bg.color = JuDianColor.Enemy
			pointTexture.color = JuDianColor.Enemy
			areaTexture.color = JuDianColor.Enemy
			areaSprite.color = JuDianTransparentColor.Enemy
		end
	else
		-- 争夺中
        bg.color = JuDianColor.Battle
        pointTexture.color = JuDianColor.Battle
        areaTexture.color = JuDianColor.Battle
        areaSprite.color = JuDianTransparentColor.Battle

		if pointData.guildId == 0 then
			stateLabel.text = LocalString.GetString("争夺中")
		else
			stateLabel.text = pointData.guildName
		end
	end

	UIEventListener.Get(noMark.gameObject).onClick = DelegateFactory.VoidDelegate(function ()
		g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("据点、据点旗、无名神像附近不可进行标记"))
	end)

	UIEventListener.Get(noTransport.gameObject).onClick = DelegateFactory.VoidDelegate(function ()
		g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("据点、据点旗、无名神像附近不可进行传送"))
	end)
end

function LuaJuDianBattleMapItemRoot:InitNpc(item, npcData)
    self.m_NpcStateLabel = item.transform:Find("StateLabel"):GetComponent(typeof(UILabel))
	self.m_NpcStateLabel.gameObject:SetActive(true)
	self.m_NpcTimeStamp = npcData.nextRefreshTime - CServerTimeMgr.Inst.timeStamp
	self.m_NpcShow = npcData.bShow
	self:UpdataNpcTimeTick()


	local noMark = item.transform:Find("NoMark"):GetComponent(typeof(UITexture))
	local noTransport = item.transform:Find("NoTransport"):GetComponent(typeof(UITexture))
	local warning = item.transform:Find("WarningTexture"):GetComponent(typeof(UITexture))

	if self.m_SelectMapId == 16103048 or self.m_SelectMapId == 16103049 then
		noTransport.height = self.m_NoTransportRadius_JLHZ
		noTransport.width = self.m_NoTransportRadius_JLHZ
		noMark.height = self.m_NoMarkRadius_JLHZ
		noMark.width = self.m_NoMarkRadius_JLHZ
	else
		noTransport.height = self.m_NoTransportRadius
		noTransport.width = self.m_NoTransportRadius
		noMark.height = self.m_NoMarkRadius
		noMark.width = self.m_NoMarkRadius
	end
	warning.height = self.m_NpcRadius
	warning.width = self.m_NpcRadius
	local noTransportBoxCollider = noTransport.transform:GetComponent(typeof(BoxCollider))
	noTransportBoxCollider.size = Vector3(noTransport.height, noTransport.width, 0)
	local noMarkBoxCollider = noMark.transform:GetComponent(typeof(BoxCollider))
	noMarkBoxCollider.size = Vector3(noMark.height, noMark.width, 0)

	UIEventListener.Get(noMark.gameObject).onClick = DelegateFactory.VoidDelegate(function ()
		g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("据点、据点旗、无名神像附近不可进行标记"))
	end)

	UIEventListener.Get(noTransport.gameObject).onClick = DelegateFactory.VoidDelegate(function ()
		g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("据点、据点旗、无名神像附近不可进行传送"))
	end)

	self.m_NpcTimeTick = RegisterTick(function()
		self:UpdataNpcTimeTick()
	end, 1000)
end

function LuaJuDianBattleMapItemRoot:UpdataNpcTimeTick()
    if self.m_NpcTimeStamp < 0 then
        self.m_NpcTimeStamp = 0
		self.m_NpcStateLabel.gameObject:SetActive(false)
    end

    local mins = math.floor(self.m_NpcTimeStamp/60)
    local secs = math.floor((self.m_NpcTimeStamp% 60))

	if self.m_NpcShow then
		self.m_NpcStateLabel.text = SafeStringFormat3(LocalString.GetString("[ACF8FF]%02d:%02d后消失"), mins, secs)
	else
		self.m_NpcStateLabel.text = SafeStringFormat3(LocalString.GetString("[ACF8FF]%02d:%02d后出现"), mins, secs)
	end
    
    self.m_NpcTimeStamp = self.m_NpcTimeStamp - 1
end

-- 从父节点设置位置
function LuaJuDianBattleMapItemRoot:SetLocation(item, data)
	self.m_Root = self.transform.parent.parent:GetComponent(typeof(CCommonLuaScript))
    self.m_Root.m_LuaSelf:SetLocation(item, data)
end

function LuaJuDianBattleMapItemRoot:OnEnable()
    g_ScriptEvent:AddListener("GuildJuDianSyncMapSceneInfo", self, "OnMapData")
    g_ScriptEvent:AddListener("GuildJuDianQueryMapLingQuanPosResult", self, "OnLingQuanData")
end

function LuaJuDianBattleMapItemRoot:OnDisable()
	UnRegisterTick(self.m_NpcTimeTick)
    g_ScriptEvent:RemoveListener("GuildJuDianSyncMapSceneInfo", self, "OnMapData")
    g_ScriptEvent:RemoveListener("GuildJuDianQueryMapLingQuanPosResult", self, "OnLingQuanData")
end

--@region UIEvent

--@endregion UIEvent

