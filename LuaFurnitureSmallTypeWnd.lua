local COpenEntryMgr=import "L10.Game.COpenEntryMgr"
local TweenPosition = import "TweenPosition"
local EventDelegate = import "EventDelegate"
local CItemMgr = import "L10.Game.CItemMgr"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CClientFurnitureMgr = import "L10.Game.CClientFurnitureMgr"
local Input = import "UnityEngine.Input"

CLuaFurnitureSmallTypeWnd = class()
RegistClassMember(CLuaFurnitureSmallTypeWnd,"SingleRowPartGO")
RegistClassMember(CLuaFurnitureSmallTypeWnd,"scrollView")
RegistClassMember(CLuaFurnitureSmallTypeWnd,"table")
RegistClassMember(CLuaFurnitureSmallTypeWnd,"expandBtn")
RegistClassMember(CLuaFurnitureSmallTypeWnd,"showAllButton")
RegistClassMember(CLuaFurnitureSmallTypeWnd,"showAllButtonSelectedSprite")
RegistClassMember(CLuaFurnitureSmallTypeWnd,"showAllButtonLabel")

RegistClassMember(CLuaFurnitureSmallTypeWnd,"AllPartGO")
RegistClassMember(CLuaFurnitureSmallTypeWnd,"allScrollView")
RegistClassMember(CLuaFurnitureSmallTypeWnd,"allTable")
RegistClassMember(CLuaFurnitureSmallTypeWnd,"TweenGO")
RegistClassMember(CLuaFurnitureSmallTypeWnd,"FurnitureTypeId")

RegistClassMember(CLuaFurnitureSmallTypeWnd,"m_SingleRowTableView")
RegistClassMember(CLuaFurnitureSmallTypeWnd,"m_AllRowTableView")
RegistClassMember(CLuaFurnitureSmallTypeWnd,"m_FurnitureTemplateIds")
RegistClassMember(CLuaFurnitureSmallTypeWnd,"m_FurnitureFilterInfos")
RegistClassMember(CLuaFurnitureSmallTypeWnd,"m_ChuanjiabaoIds")
RegistClassMember(CLuaFurnitureSmallTypeWnd,"m_ChuanjiabaoFilterInfos")

RegistClassMember(CLuaFurnitureSmallTypeWnd,"m_Categorys")
RegistClassMember(CLuaFurnitureSmallTypeWnd,"m_CategoryGo")
RegistClassMember(CLuaFurnitureSmallTypeWnd,"m_CategoryTableView")
RegistClassMember(CLuaFurnitureSmallTypeWnd,"m_CategorySelectedId")

RegistClassMember(CLuaFurnitureSmallTypeWnd,"getData")

local s_initThemeName = nil
function CLuaFurnitureSmallTypeWnd.SetInitTheme(name)
    s_initThemeName = name
end

function CLuaFurnitureSmallTypeWnd:Awake()
    self.SingleRowPartGO = self.transform:Find("Anchor/Tab/SingleRowRO").gameObject
    self.m_SingleRowTableView = self.SingleRowPartGO:GetComponent(typeof(QnTableView))

    self.expandBtn = self.transform:Find("Anchor/Tab/ExpandButton").gameObject

    UIEventListener.Get(self.expandBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnExpandBtnClicked(go)
    end)

    self.showAllButton = self.transform:Find("Anchor/Tab/TweenGO/ShowAllButton").gameObject
    UIEventListener.Get(self.showAllButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnShowAllButtonClicked(go)
    end)

    UIEventListener.Get(self.transform:Find("Anchor/Tab/TweenGO/ExchangeButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        CLuaClientFurnitureMgr.ShowTableType = 1 --装修仓库
        CUIManager.ShowUI(CUIResources.FurnitureExchangeWnd)
    end)

    self.showAllButtonSelectedSprite = self.transform:Find("Anchor/Tab/TweenGO/ShowAllButton/Sprite").gameObject
    self.showAllButtonLabel = self.transform:Find("Anchor/Tab/TweenGO/ShowAllButton/Label"):GetComponent(typeof(UILabel))
    self.AllPartGO = self.transform:Find("Anchor/Tab/TweenGO/ShowAllGO").gameObject
    self.m_AllRowTableView = self.AllPartGO:GetComponent(typeof(QnTableView))

    self.TweenGO = self.transform:Find("Anchor/Tab/TweenGO").gameObject
    self.FurnitureTypeId = 0

    self.m_CategoryGo = self.transform:Find("Anchor/Tab/Filters").gameObject
    self.m_CategoryTableView = self.transform:Find("Anchor/Tab/Filters/QnRadioBox"):GetComponent(typeof(QnTableView))

    local tweener = CommonDefs.GetComponent_GameObject_Type(self.TweenGO, typeof(TweenPosition))
    if tweener ~= nil then
        EventDelegate.Add(tweener.onFinished, DelegateFactory.Callback(function() self:onTweenFinished() end))
        tweener.enabled = false
    end

    self.m_ChuanjiabaoIds = {}
    self.m_FurnitureTemplateIds = {}

    self.m_SingleRowTableView.m_DataSource = DefaultTableViewDataSource.Create2(
        function() return #self.m_FurnitureTemplateIds+#self.m_ChuanjiabaoIds end,
        function(view,row)
            local item=view:GetFromPool(0)
            if row<#self.m_ChuanjiabaoIds then
                local id = self.m_ChuanjiabaoIds[row+1]
                item:GetComponent(typeof(CCommonLuaScript)):Awake()
                local script = item:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
                script:Init4Chuanjiabao(id)
            else
                local id = self.m_FurnitureTemplateIds[row+1-#self.m_ChuanjiabaoIds]
                item:GetComponent(typeof(CCommonLuaScript)):Awake()
                local script = item:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
                script:Init0(id)
            end
            return item
        end
    )
    self.m_SingleRowTableView.OnSelectAtRow = DelegateFactory.Action_int(function(idx)

        local item = self.m_SingleRowTableView:GetItemAtRow(idx)
        if item then
            local t = item.gameObject:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
            t:SetSelected(true)
        end
        CClientFurnitureMgr.Inst:ClearCurFurniture()
    end)

    self.m_AllRowTableView.m_DataSource = DefaultTableViewDataSource.Create2(
        function() return #self.m_FurnitureTemplateIds+#self.m_ChuanjiabaoIds end,
        function(view,row)
            local item=view:GetFromPool(1)
            if row<#self.m_ChuanjiabaoIds then
                local id = self.m_ChuanjiabaoIds[row+1]
                item:GetComponent(typeof(CCommonLuaScript)):Awake()
                local script = item:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
                script:Init4Chuanjiabao(id)
            else
                local id = self.m_FurnitureTemplateIds[row+1-#self.m_ChuanjiabaoIds]
                item:GetComponent(typeof(CCommonLuaScript)):Awake()
                local script = item:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
                script:Init0(id)
            end
            return item
        end
    )
    self.m_AllRowTableView.OnSelectAtRow = DelegateFactory.Action_int(function(idx)
        local item = self.m_AllRowTableView:GetItemAtRow(idx)
        if item then
            local t = item.gameObject:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
            t:SetSelected(true)
        end
        CClientFurnitureMgr.Inst:ClearCurFurniture()
    end)

    self.m_CategoryTableView.m_DataSource = DefaultTableViewDataSource.Create2(
        function() return #self.m_Categorys end,
        function(view,row)
            local item=view:GetFromPool(2)
            self:InitCategoryItem(item, self.m_Categorys[row+1])
            return item
        end
    )
    self.m_CategoryTableView.OnSelectAtRow = DelegateFactory.Action_int(function(idx)
        local item = self.m_CategoryTableView:GetItemAtRow(idx)
        self.m_CategorySelectedId = idx + 1
        local categoryInfo = self.m_Categorys[self.m_CategorySelectedId]
        self:ApplyFilter(categoryInfo.filterGrade, categoryInfo.filterName)

        if self.FurnitureTypeId == EnumFurnitureType_lua.eJianzhu then
            if categoryInfo.filterGrade==4 or categoryInfo.filterGrade==5 then
                local alert = item.transform:Find("Alert").gameObject
                if alert.activeSelf then
                    alert:SetActive(false)
                    g_MessageMgr:ShowMessage("NewJianZhu_Can_Unlock")
                    if categoryInfo.filterGrade==4 then
                        COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.NewJianZhu_Level4)
                    end
                    if categoryInfo.filterGrade==5 then
                        COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.NewJianZhu_Level5)
                    end
                end
            end
        end
    end)
end
function CLuaFurnitureSmallTypeWnd:OnQueryChuanjiabaoInHouseResult( )
    if self.FurnitureTypeId == EnumFurnitureType_lua.eQinggong then
        self:Init0(self.FurnitureTypeId)
    end
end
function CLuaFurnitureSmallTypeWnd:Init()
    -- print("Init")
    self.transform:Find("Anchor/Tab/TweenGO/ExchangeButton").gameObject:SetActive(CClientHouseMgr.Inst:IsHouseOwner())

    self:Init0(1)
end
function CLuaFurnitureSmallTypeWnd:Init0( bigTypeId, dontInit) 
    self.FurnitureTypeId = bigTypeId

    if not dontInit then
        self.m_CategoryGo:SetActive(true)
        self.SingleRowPartGO:SetActive(true)
        self.AllPartGO:SetActive(false)
        self.showAllButtonSelectedSprite:SetActive(false)
        self.showAllButtonLabel.text = LocalString.GetString("展开")
        self.expandBtn:SetActive(true)
        LuaUtils.SetLocalPosition(self.m_CategoryTableView.transform, 0, 0, 0)
    end

    local tweener = CommonDefs.GetComponent_GameObject_Type(self.TweenGO, typeof(TweenPosition))
    if tweener ~= nil then
        tweener.value = Vector3.zero
    end

    local bIsMingyuan = false
    if CClientHouseMgr.Inst.mBasicProp ~= nil and CClientHouseMgr.Inst.mBasicProp.IsMingyuan ~= 0 then
        bIsMingyuan = true
    end
    self.m_FurnitureTemplateIds = {}
    self.m_FurnitureFilterInfos = nil
    self.m_ChuanjiabaoIds = {}
    

    local dataLookup = {}
    self.getData = function(id)
        if not dataLookup[id] then
            dataLookup[id] = Zhuangshiwu_Zhuangshiwu.GetData(id)
        end
        return dataLookup[id]
    end

    if bigTypeId == EnumFurnitureType_lua.eQinggong then
        if CClientHouseMgr.Inst.mFurnitureProp ~= nil then
            local ChuanjiabaoData = CClientHouseMgr.Inst.mFurnitureProp.ChuanjiabaoData
            -- local chuanjiabaoList = {}

            CommonDefs.DictIterate(ChuanjiabaoData, DelegateFactory.Action_object_object(function (pid, group) 
                CommonDefs.DictIterate(group.Chuanjiabaos, DelegateFactory.Action_object_object(function (___key, suit) 
                    for i=1,suit.ChuanjiabaoIds.Length do
                        local id = suit.ChuanjiabaoIds[i-1]
                        if id and id ~= "" then
                            local cjbItem = CItemMgr.Inst:GetById(id)
                            if cjbItem ~= nil then
                                table.insert( self.m_ChuanjiabaoIds,id)
                            end
                        end
                    end
                end))
            end))

            -- 是否需要排序
            table.sort( self.m_ChuanjiabaoIds,function(t1,t2)
                local item1 = CItemMgr.Inst:GetById(t1)
                local data1 = item1.Item.ChuanjiabaoItemInfo

                local item2 = CItemMgr.Inst:GetById(t2)
                local data2 = item2.Item.ChuanjiabaoItemInfo

                if data1 and data2 then
                    local star1 = data1.WordInfo.Star
                    local star2 = data2.WordInfo.Star
                    if star1>star2 then
                        return true
                    elseif star1<star2 then
                        return false
                    else
                        return t1>t2
                    end
                end
                return t1>t2
            end )

        end
    end


    local speicalBuildingLookup = {}
    local speicalBuildingCol = {}
    if self.FurnitureTypeId == EnumFurnitureType_lua.eJianzhu then
        local speicalBuilding = CClientHouseMgr.Inst.mFurnitureProp2.SpecialBuilding
        local size = speicalBuilding:GetBitSize()
        House_SpecialBuilding.Foreach(function(k,v)
            speicalBuildingCol[v.ZhuangshiwuId] = k
            speicalBuildingCol[v.PreZhuangshiwuId] = k

            if speicalBuilding:GetBit(k) then--开放了新家具
                --看有没有使用
                local zswdata = self.getData(v.PreZhuangshiwuId)
                local curBuilding = CClientFurnitureMgr.Inst:GetFurnitureBySubtype(zswdata.SubType)
                if curBuilding.TemplateId==v.ZhuangshiwuId then
                    speicalBuildingLookup[v.ZhuangshiwuId] = true
                    speicalBuildingLookup[v.PreZhuangshiwuId] = false
                else
                    speicalBuildingLookup[v.ZhuangshiwuId] = false
                    speicalBuildingLookup[v.PreZhuangshiwuId] = true
                end
            else
                speicalBuildingLookup[v.ZhuangshiwuId] = false
                speicalBuildingLookup[v.PreZhuangshiwuId] = true
            end
        end)
    end
    


    local lookup = {}
    --这个gc很大
    local ids = CLuaClientFurnitureMgr.GetIdsByType(bigTypeId)
    for i,v in ipairs(ids) do
        
        local data = Zhuangshiwu_Zhuangshiwu.GetData(v)
        local filtered = false
        if speicalBuildingCol[v] and not speicalBuildingLookup[v] then
            --如果是建筑，但是被过滤了，就不显示
            filtered = true
        end
        if not filtered and not (not bIsMingyuan and data.IsMingyuan == 1) then--普通家园不能显示名园的装饰物
            if bigTypeId == EnumFurnitureType_lua.eSkyBox then
                if data.Type == EnumFurnitureType_lua.eSkyBox and data.Status ~= 3 then
                    local skyBoxData = CLuaHouseMgr.GetSkyBoxItemByZSWId(data.ID)
                    if skyBoxData and self:IsValidSkyBox(skyBoxData, data) then
                        table.insert( self.m_FurnitureTemplateIds, data.FurnishTemplate )
                    end
                    
                end
            else
                if data.SubType==EnumFurnitureSubType_lua.eWeiqiang then
                    --名园不显示普通的围墙
                    if bIsMingyuan then
                        if data.IsMingyuan == 1 then
                            table.insert( self.m_FurnitureTemplateIds, data.FurnishTemplate )
                        end
                    else
                        table.insert( self.m_FurnitureTemplateIds, data.FurnishTemplate )
                    end
                else
                    table.insert( self.m_FurnitureTemplateIds, data.FurnishTemplate )
                end
            end
        end
    end

    local JianzhuSubtype2Order = {
        [EnumFurnitureSubType_lua.eZhengwu]=1,
        [EnumFurnitureSubType_lua.eXiangfang]=2,
        [EnumFurnitureSubType_lua.eCangku]=3,
        [EnumFurnitureSubType_lua.eMiaopu]=4,
        [EnumFurnitureSubType_lua.eWeiqiang]=5,
    }

    local isPenjingLookup = {}
    local function isPenjingFurniture(id)
        if not isPenjingLookup[id] then
            isPenjingLookup[id] = CClientFurnitureMgr.Inst:IsPenjingFurniture(id)
        end
        return isPenjingLookup[id]
    end

    local furCountLookup = CLuaClientFurnitureMgr.GetPlacedFurnitureCounts() 
    local function GetPlacedFurnitureCountByTemplateId(id)
        if not furCountLookup[id] then
            furCountLookup[id] = 0
        end
        return furCountLookup[id]
    end


    table.sort( self.m_FurnitureTemplateIds,function(t1,t2)
        local data1 = self.getData(t1)
        local data2 = self.getData(t2)

        if data1.Type == EnumFurnitureType_lua.eJianzhu and data2.Type == EnumFurnitureType_lua.eJianzhu then
            if data1.SubType ~= data2.SubType then
                return JianzhuSubtype2Order[data1.SubType]<JianzhuSubtype2Order[data2.SubType]
            else
                if data1.SubType == EnumFurnitureSubType_lua.eMiaopu then
                    return t1<t2
                end
                local grade1,grade2 = data1.HouseGrade,data2.HouseGrade
                if grade1>grade2 then
                    return false 
                elseif grade1<grade2 then
                    return true
                else
                    return t1>t2
                end
            end
        elseif data1.Type == EnumFurnitureType_lua.eWallPaper and data2.Type == EnumFurnitureType_lua.eWallPaper then
            local itemId1 = data1.ItemId
            local itemId2 = data2.ItemId
            local wallPaper1 = House_WallPaper.GetDataBySubKey("ItemID",itemId1)
            local wallPaper2 = House_WallPaper.GetDataBySubKey("ItemID",itemId2)
            local isUnlock1 = CLuaHouseMgr.IsWallPaperUnlock(wallPaper1.Id)
            local isUnlock2 = CLuaHouseMgr.IsWallPaperUnlock(wallPaper2.Id)
            if isUnlock1 and not isUnlock2 then
                return true
            elseif not isUnlock1 and isUnlock2 then
                return false
            end
            return wallPaper1.Order < wallPaper2.Order 
        elseif data1.Type == EnumFurnitureType_lua.ePoolFurniture and data2.Type == EnumFurnitureType_lua.ePoolFurniture then
            local t1count = CClientFurnitureMgr.Inst:GetRepositoryFurnitureCount(t1) + GetPlacedFurnitureCountByTemplateId(t1)
            local t2count = CClientFurnitureMgr.Inst:GetRepositoryFurnitureCount(t2) + GetPlacedFurnitureCountByTemplateId(t2)

            if LuaPoolMgr.IsPoolEnterZswTemplate(data1.ID) then
                return true
            elseif LuaPoolMgr.IsPoolEnterZswTemplate(data2.ID) then
                return false
            elseif t1count <= 0 and t2count > 0 then
                return false
            elseif t1count > 0 and t2count <= 0 then
                return true
            else
                return data1.ID < data2.ID
            end
        else
            if LuaHouseTerrainMgr.IsEditTerrainTemplateId(data1.ID) then
                return true
            elseif LuaHouseTerrainMgr.IsEditTerrainTemplateId(data2.ID) then
                return false
            elseif isPenjingFurniture(t1) and not isPenjingFurniture(t2) then
                return false
            elseif not isPenjingFurniture(t1) and isPenjingFurniture(t2) then
                return true
            else
                local t1count = CClientFurnitureMgr.Inst:GetRepositoryFurnitureCount(t1) + GetPlacedFurnitureCountByTemplateId(t1)
                local t2count = CClientFurnitureMgr.Inst:GetRepositoryFurnitureCount(t2) + GetPlacedFurnitureCountByTemplateId(t2)
                if t1count <= 0 and t2count > 0 then
                    return false
                elseif t1count > 0 and t2count <= 0 then
                    return true
                else
                    if data1.Type == EnumFurnitureType_lua.eLingshou and data2.Type == EnumFurnitureType_lua.eLingshou then
                        return t1>t2
                    else
                        local grade1,grade2 = data1.HouseGrade,data2.HouseGrade
                        if grade1>grade2 then
                            return false
                        elseif grade1<grade2 then
                            return true
                        else
                            return t1>t2
                        end
                    end
                end
            end
        end
    end)

    self:InitCategorys()
    self:ReloadData(true)
end

                    
--@desc 
function CLuaFurnitureSmallTypeWnd:IsValidSkyBox(skyBoxKind, zhuangshiwu)
    -- check1 ：限时家园天空盒
    -- 显示情况 1、在购买期限内（无论是否购买）
    --         2、超过购买期在使用期内，已购买

    if skyBoxKind.BuyDuration == "~" or skyBoxKind.UseDuration == "~" then
        return true
    end

    local inBuyDuration = CLuaHouseMgr.IsSkyBoxInBuyDuration(skyBoxKind)
    local inUseDuration = CLuaHouseMgr.IsSkyBoxInUseDuration(skyBoxKind)

    if inBuyDuration then return true end
    if inUseDuration and CLuaHouseMgr.IsSkyBoxUnlock(zhuangshiwu) then return true end
    
    return false
end

function CLuaFurnitureSmallTypeWnd:UpdateCurrentSkyBox()
    if self.FurnitureTypeId == EnumFurnitureType_lua.eSkyBox then
        self:Init0(self.FurnitureTypeId)
    end
end

function CLuaFurnitureSmallTypeWnd:UpdateCurrentWallPaper()
    if self.FurnitureTypeId == EnumFurnitureType_lua.eWallPaper then
        table.sort( self.m_FurnitureTemplateIds,function(t1,t2)
            local data1 = self.getData(t1)
            local data2 = self.getData(t2)             
            if data1.Type == EnumFurnitureType_lua.eWallPaper and data2.Type == EnumFurnitureType_lua.eWallPaper then
                local itemId1 = data1.ItemId
                local itemId2 = data2.ItemId
                local wallPaper1 = House_WallPaper.GetDataBySubKey("ItemID",itemId1)
                local wallPaper2 = House_WallPaper.GetDataBySubKey("ItemID",itemId2)
                local isUnlock1 = CLuaHouseMgr.IsWallPaperUnlock(wallPaper1.Id)
                local isUnlock2 = CLuaHouseMgr.IsWallPaperUnlock(wallPaper2.Id)
                if isUnlock1 and not isUnlock2 then
                    return true
                elseif not isUnlock1 and isUnlock2 then
                    return false
                end
                return wallPaper1.Order < wallPaper2.Order               
            else
               return true
            end
        end)
        self:ReloadData(true)
    end
end

function CLuaFurnitureSmallTypeWnd:OnEnable()
    g_ScriptEvent:AddListener("QueryChuanjiabaoInHouseResult",self,"OnQueryChuanjiabaoInHouseResult")
    g_ScriptEvent:AddListener("OnFurnitureTypeClicked",self,"OnFurnishTypeSelected")
    g_ScriptEvent:AddListener("UpdateCurrentSkyBox",self,"UpdateCurrentSkyBox")
    g_ScriptEvent:AddListener("OnUpdateExtraInfo",self,"UpdateCurrentSkyBox")
    g_ScriptEvent:AddListener("UpdateSkyBoxUnlock",self,"UpdateCurrentSkyBox")
    g_ScriptEvent:AddListener("UpdateWallPaperUnlock",self,"UpdateCurrentWallPaper")
    g_ScriptEvent:AddListener("UpdateCurrentWallPaper",self,"UpdateCurrentWallPaper")
    g_ScriptEvent:AddListener("ChangeBuildingAppearance", self, "OnChangeBuildingAppearance")
end


function CLuaFurnitureSmallTypeWnd:OnDisable()
    g_ScriptEvent:RemoveListener("QueryChuanjiabaoInHouseResult",self,"OnQueryChuanjiabaoInHouseResult")
    g_ScriptEvent:RemoveListener("OnFurnitureTypeClicked",self,"OnFurnishTypeSelected")
    g_ScriptEvent:RemoveListener("UpdateCurrentSkyBox",self,"UpdateCurrentSkyBox")
    g_ScriptEvent:RemoveListener("OnUpdateExtraInfo",self,"UpdateCurrentSkyBox")
    g_ScriptEvent:RemoveListener("UpdateSkyBoxUnlock",self,"UpdateCurrentSkyBox")
    g_ScriptEvent:RemoveListener("UpdateWallPaperUnlock",self,"UpdateCurrentWallPaper")
    g_ScriptEvent:RemoveListener("UpdateCurrentWallPaper",self,"UpdateCurrentWallPaper")
    g_ScriptEvent:RemoveListener("ChangeBuildingAppearance", self, "OnChangeBuildingAppearance")
    CLuaHouseMgr.ClickItemInfo = nil
end

function CLuaFurnitureSmallTypeWnd:OnChangeBuildingAppearance(furnitureId,targetTemplateId)
    --更换建筑外观
    if self.FurnitureTypeId == EnumFurnitureType_lua.eJianzhu then
        self:Init0(self.FurnitureTypeId)
    end
end

function CLuaFurnitureSmallTypeWnd:OnFurnishTypeSelected(selectedBigTypeId)
    self:Init0(selectedBigTypeId, true)
end

function CLuaFurnitureSmallTypeWnd:OnExpandBtnClicked( go) 
    if not self.showAllButtonSelectedSprite.activeSelf then
        local rotation = self.expandBtn.transform.eulerAngles
        self.expandBtn.transform.eulerAngles = CreateFromClass(Vector3, rotation.x, rotation.y, 180 - rotation.z)

        self.SingleRowPartGO:SetActive(not self.SingleRowPartGO.activeSelf)
        self.m_CategoryGo:SetActive(not self.m_CategoryGo.activeSelf)
    end
end
function CLuaFurnitureSmallTypeWnd:OnShowAllButtonClicked( go) 
    local bShowAll = not self.showAllButtonSelectedSprite.activeSelf
    self.showAllButtonSelectedSprite:SetActive(bShowAll)
    self.showAllButtonLabel.text = bShowAll and LocalString.GetString("收起") or LocalString.GetString("展开")

    if bShowAll then
        LuaUtils.SetLocalPosition(self.m_CategoryTableView.transform, 0, -50, 0)
        self.m_CategoryGo:SetActive(true)
        self.AllPartGO:SetActive(true)
        self.SingleRowPartGO:SetActive(false)
        self.expandBtn:SetActive(false)
    end

    self:PlayPositionTweener(bShowAll)
    self:ReloadData(false)
end
function CLuaFurnitureSmallTypeWnd:ReloadData(isSingle)
    if CGuideMgr.Inst:IsInPhase(EnumGuideKey.WanShengJieSkybox) then
        CGuideMgr.Inst:EndCurrentPhase()
    end
    if isSingle then
        self.m_SingleRowTableView:ReloadData(true,false)
    else
        self.m_AllRowTableView:ReloadData(true,false)
    end
end

function CLuaFurnitureSmallTypeWnd:onTweenFinished( )
    local tweener = CommonDefs.GetComponent_GameObject_Type(self.TweenGO, typeof(TweenPosition))
    if tweener ~= nil then
        tweener.enabled = false
    end

    if not self.showAllButtonSelectedSprite.activeSelf then
        LuaUtils.SetLocalPosition(self.m_CategoryTableView.transform, 0, 0, 0)
        self.AllPartGO:SetActive(false)
        self.SingleRowPartGO:SetActive(true)
        self.expandBtn:SetActive(true)

        self:ReloadData(true)
    end
end
function CLuaFurnitureSmallTypeWnd:PlayPositionTweener( bShowAll) 
    local tweener = CommonDefs.GetComponent_GameObject_Type(self.TweenGO, typeof(TweenPosition))
    if tweener ~= nil then
        tweener.enabled = true
        if bShowAll then
            tweener:PlayForward()
        else
            tweener:PlayReverse()
        end
    end
end
function CLuaFurnitureSmallTypeWnd:GetGuideGo( methodName) 
    if methodName == "GetMiaoPu3" then
        return self:GetMiaoPu3()
    elseif methodName=="GetWanShengJieSkybox" then
        local row=nil
        for i,v in ipairs(self.m_FurnitureTemplateIds) do
            if v==9200 or v==9496 then
                row = i-1
                break
            end
        end
        if row then
            local item = self.m_SingleRowTableView:GetItemAtRow(row)
            if item then return item.gameObject end
        else
            CGuideMgr.Inst:EndCurrentPhase()
        end
    end
    return nil
end
function CLuaFurnitureSmallTypeWnd:GetMiaoPu3( )
    if CGuideMgr.Inst:IsInPhase(EnumGuideKey.GetMiaoPu3) then
        --是否是主人 可能在别人家 在别人家的时候不中断引导
        if not CClientHouseMgr.Inst:IsCurHouseOwner() then
            return nil
        end
        if CClientFurnitureMgr.Inst:IsMiaopu3Placed() or not CClientFurnitureMgr.Inst:IsMiaopu3Exist() then
            CGuideMgr.Inst:EndCurrentPhase()
            return nil
        end
    end

    local row = 0
    for i,v in ipairs(self.m_FurnitureTemplateIds) do
        if v==7013 then
            row = i-1+#self.m_ChuanjiabaoIds
            self.m_SingleRowTableView:ScrollToRow(row)
            break
        end
    end
    local item = self.m_SingleRowTableView:GetItemAtRow(row)
    if item then return item.gameObject end
    return nil
end

function CLuaFurnitureSmallTypeWnd:Update() 
    if self.showAllButtonSelectedSprite.activeSelf then
        if Input.GetMouseButtonDown(0) then
            if not CUICommonDef.IsOverUI or
                (UICamera.lastHit.collider ~= nil and not CUICommonDef.IsChild(self.transform, UICamera.lastHit.collider.transform) 
                and CUICommonDef.GetNearestPanelDepth(self.transform) > CUICommonDef.GetNearestPanelDepth(UICamera.lastHit.collider.transform)) then
                self:OnShowAllButtonClicked(self.showAllButton)
            end
        end
    end
end

function CLuaFurnitureSmallTypeWnd:OnDestroy()
    CLuaClientFurnitureMgr.ClearZhuangshiwuIdCache()
end

function CLuaFurnitureSmallTypeWnd:InitCategorys()
    self:InitFilterInfos()
    self.m_Categorys = {}

    if #self.m_FurnitureFilterInfos > 0 and self.m_FurnitureFilterInfos[1].type ~= EnumFurnitureType_lua.eSkyBox then
        local allFilter = {displayName = LocalString.GetString("全部")}
        table.insert(self.m_Categorys, allFilter)

        local grades = {}
        local themeNames = {}

        for k, v in ipairs(self.m_FurnitureFilterInfos) do
            grades[v.grade] = true
            if v.themeName ~= "" then
                themeNames[v.themeName] = true
            end
        end

        for i = 1, 5 do
            if grades[i] then
                table.insert(self.m_Categorys, {displayName = SafeStringFormat3(LocalString.GetString("%d级"), i), filterGrade = i, filterName = nil})
            end
        end

        for k, v in pairs(themeNames) do
            table.insert(self.m_Categorys, {displayName = k, filterGrade = nil, filterName = k})
        end
    end

    self.m_CategoryTableView:ReloadData(true, false)
    self.m_CategorySelectedId = 1
    
    if s_initThemeName then
        for i,v in ipairs(self.m_Categorys) do
            if v.filterName == s_initThemeName then
                self.m_CategorySelectedId = i
                break
            end
        end
        s_initThemeName = nil
    end
    self.m_CategoryTableView:SetSelectRow(self.m_CategorySelectedId-1, true)
end

function CLuaFurnitureSmallTypeWnd:InitCategoryItem(item, info)
    local alert = item.transform:Find("Alert").gameObject
    alert:SetActive(false)

    if self.FurnitureTypeId == EnumFurnitureType_lua.eJianzhu then
        if info.filterGrade and (info.filterGrade==4 or info.filterGrade==5) then
            alert:SetActive(self:CheckAlert(info.filterGrade))
        end
    end

    item.m_Label.text = info.displayName
end

function CLuaFurnitureSmallTypeWnd:ApplyFilter(grade, name)
    self.m_FurnitureTemplateIds = {}
    for k, v in ipairs(self.m_FurnitureFilterInfos) do
        if (not grade and not name) or (grade == v.grade or name == v.themeName) then
            table.insert(self.m_FurnitureTemplateIds, v.templateId)
        end
    end   

    self.m_ChuanjiabaoIds = {}
    for k, v in ipairs(self.m_ChuanjiabaoFilterInfos) do
        if (not grade and not name) or grade == v.grade then
            table.insert(self.m_ChuanjiabaoIds, v.templateId)
        end
    end   

    self:ReloadData(not self.showAllButtonSelectedSprite.activeSelf)
end

function CLuaFurnitureSmallTypeWnd:InitFilterInfos()
    self.m_FurnitureFilterInfos = {}
    self.m_ChuanjiabaoFilterInfos = {}
    for k, v in ipairs(self.m_FurnitureTemplateIds) do
        local data = Zhuangshiwu_Zhuangshiwu.GetData(v)
        local filterInfo = {
            templateId = data.FurnishTemplate, 
            grade = data.Grade, 
            themeName = data.Theme, 
            type = data.Type,
            houseGrade = data.HouseGrade
        }
        table.insert(self.m_FurnitureFilterInfos, filterInfo)
    end

    for i, v in ipairs(self.m_ChuanjiabaoIds) do
        local filterInfo = {
            templateId = v, 
            grade = 1, 
        }
        table.insert(self.m_ChuanjiabaoFilterInfos, filterInfo)
    end
end

function CLuaFurnitureSmallTypeWnd:CheckAlert(grade)
    if grade==4 then
        if COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.NewJianZhu_Level4) then
            return false
        end
    elseif grade==5 then
        if COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.NewJianZhu_Level5) then
            return false
        end
    else
        return false
    end

    CLuaClientFurnitureMgr.InitSpecialBuilding()

    local speicalBuilding = CClientHouseMgr.Inst.mFurnitureProp2.SpecialBuilding
    local houseGrade = CClientHouseMgr.Inst:GetCurHouseGrade()

    local haveUnlock=false
    local isOpen = false
    for i,v in ipairs(self.m_FurnitureFilterInfos) do
        if v.grade==grade then
            if v.houseGrade<=houseGrade then
                isOpen = true
                local templateId = v.templateId
                local unlock = false
                local idx = CLuaClientFurnitureMgr.m_SpecialBuildingLookup[templateId]
                if idx then 
                    unlock = speicalBuilding:GetBit(idx)
                end

                if unlock then
                    haveUnlock = true
                    break
                end
            end
        end
    end
    if haveUnlock then
        return false
    end
    if not isOpen then
        return false
    end
    if isOpen and not haveUnlock then
        return true
    end
end
