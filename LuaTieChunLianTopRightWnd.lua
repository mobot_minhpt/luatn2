local CTopAndRightTipWnd=import "L10.UI.CTopAndRightTipWnd"

CLuaTieChunLianTopRightWnd = class()
RegistClassMember(CLuaTieChunLianTopRightWnd,"m_ExpandButton")
RegistClassMember(CLuaTieChunLianTopRightWnd,"m_ValueLabel")

function CLuaTieChunLianTopRightWnd:Init()
    self.m_ExpandButton=self.transform:Find("Anchor/ExpandButton").gameObject
    UIEventListener.Get(self.m_ExpandButton).onClick = DelegateFactory.VoidDelegate(function(go)
        LuaUtils.SetLocalRotation(self.m_ExpandButton.transform,0,0,0)
        CTopAndRightTipWnd.showPackage = false;
        CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
    end)

    self.m_ValueLabel = self.transform:Find("Anchor/ValueLabel"):GetComponent(typeof(UILabel))
    self.m_ValueLabel.text = 0
end

function CLuaTieChunLianTopRightWnd:OnEnable()
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("SyncTieChunLianPlayScore", self, "OnSyncTieChunLianPlayScore")
end

function CLuaTieChunLianTopRightWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("SyncTieChunLianPlayScore", self, "OnSyncTieChunLianPlayScore")
end

function CLuaTieChunLianTopRightWnd:OnHideTopAndRightTipWnd()
    LuaUtils.SetLocalRotation(self.m_ExpandButton.transform,0,0,180)
end
function CLuaTieChunLianTopRightWnd:OnSyncTieChunLianPlayScore(score)
    self.m_ValueLabel.text = tostring(score)

end