local max = math.max
local min = math.min
local abs = math.abs
local ceil = math.ceil
local floor = math.floor
local log = math.log
local power = function(a, b) return a^b end

local EnumCopyFightProp = {
	PermanentCor = 1,
	PermanentSta = 2,
	PermanentStr = 3,
	PermanentInt = 4,
	PermanentAgi = 5,
	PermanentHpFull = 6,
	Hp = 7,
	Mp = 8,
	CurrentHpFull = 9,
	CurrentMpFull = 10,
	EquipExchangeMF = 11,
	RevisePermanentCor = 12,
	RevisePermanentSta = 13,
	RevisePermanentStr = 14,
	RevisePermanentInt = 15,
	RevisePermanentAgi = 16,
	Class = 2001,
	Grade = 2002,
	Cor = 2003,
	AdjCor = 2004,
	MulCor = 2005,
	Sta = 2006,
	AdjSta = 2007,
	MulSta = 2008,
	Str = 2009,
	AdjStr = 2010,
	MulStr = 2011,
	Int = 2012,
	AdjInt = 2013,
	MulInt = 2014,
	Agi = 2015,
	AdjAgi = 2016,
	MulAgi = 2017,
	HpFull = 2018,
	AdjHpFull = 2019,
	MulHpFull = 2020,
	HpRecover = 2021,
	AdjHpRecover = 2022,
	MulHpRecover = 2023,
	MpFull = 2024,
	AdjMpFull = 2025,
	MulMpFull = 2026,
	MpRecover = 2027,
	AdjMpRecover = 2028,
	MulMpRecover = 2029,
	pAttMin = 2030,
	AdjpAttMin = 2031,
	MulpAttMin = 2032,
	pAttMax = 2033,
	AdjpAttMax = 2034,
	MulpAttMax = 2035,
	pHit = 2036,
	AdjpHit = 2037,
	MulpHit = 2038,
	pMiss = 2039,
	AdjpMiss = 2040,
	MulpMiss = 2041,
	pDef = 2042,
	AdjpDef = 2043,
	MulpDef = 2044,
	AdjpHurt = 2045,
	MulpHurt = 2046,
	pSpeed = 2047,
	OripSpeed = 2048,
	MulpSpeed = 2049,
	pFatal = 2050,
	AdjpFatal = 2051,
	AntipFatal = 2052,
	AdjAntipFatal = 2053,
	pFatalDamage = 2054,
	AdjpFatalDamage = 2055,
	AntipFatalDamage = 2056,
	AdjAntipFatalDamage = 2057,
	Block = 2058,
	AdjBlock = 2059,
	MulBlock = 2060,
	BlockDamage = 2061,
	AdjBlockDamage = 2062,
	AntiBlock = 2063,
	AdjAntiBlock = 2064,
	AntiBlockDamage = 2065,
	AdjAntiBlockDamage = 2066,
	mAttMin = 2067,
	AdjmAttMin = 2068,
	MulmAttMin = 2069,
	mAttMax = 2070,
	AdjmAttMax = 2071,
	MulmAttMax = 2072,
	mHit = 2073,
	AdjmHit = 2074,
	MulmHit = 2075,
	mMiss = 2076,
	AdjmMiss = 2077,
	MulmMiss = 2078,
	mDef = 2079,
	AdjmDef = 2080,
	MulmDef = 2081,
	AdjmHurt = 2082,
	MulmHurt = 2083,
	mSpeed = 2084,
	OrimSpeed = 2085,
	MulmSpeed = 2086,
	mFatal = 2087,
	AdjmFatal = 2088,
	AntimFatal = 2089,
	AdjAntimFatal = 2090,
	mFatalDamage = 2091,
	AdjmFatalDamage = 2092,
	AntimFatalDamage = 2093,
	AdjAntimFatalDamage = 2094,
	EnhanceFire = 2095,
	AdjEnhanceFire = 2096,
	MulEnhanceFire = 2097,
	EnhanceThunder = 2098,
	AdjEnhanceThunder = 2099,
	MulEnhanceThunder = 2100,
	EnhanceIce = 2101,
	AdjEnhanceIce = 2102,
	MulEnhanceIce = 2103,
	EnhancePoison = 2104,
	AdjEnhancePoison = 2105,
	MulEnhancePoison = 2106,
	EnhanceWind = 2107,
	AdjEnhanceWind = 2108,
	MulEnhanceWind = 2109,
	EnhanceLight = 2110,
	AdjEnhanceLight = 2111,
	MulEnhanceLight = 2112,
	EnhanceIllusion = 2113,
	AdjEnhanceIllusion = 2114,
	MulEnhanceIllusion = 2115,
	AntiFire = 2116,
	AdjAntiFire = 2117,
	MulAntiFire = 2118,
	AntiThunder = 2119,
	AdjAntiThunder = 2120,
	MulAntiThunder = 2121,
	AntiIce = 2122,
	AdjAntiIce = 2123,
	MulAntiIce = 2124,
	AntiPoison = 2125,
	AdjAntiPoison = 2126,
	MulAntiPoison = 2127,
	AntiWind = 2128,
	AdjAntiWind = 2129,
	MulAntiWind = 2130,
	AntiLight = 2131,
	AdjAntiLight = 2132,
	MulAntiLight = 2133,
	AntiIllusion = 2134,
	AdjAntiIllusion = 2135,
	MulAntiIllusion = 2136,
	IgnoreAntiFire = 2137,
	IgnoreAntiThunder = 2138,
	IgnoreAntiIce = 2139,
	IgnoreAntiPoison = 2140,
	IgnoreAntiWind = 2141,
	IgnoreAntiLight = 2142,
	IgnoreAntiIllusion = 2143,
	EnhanceDizzy = 2144,
	AdjEnhanceDizzy = 2145,
	MulEnhanceDizzy = 2146,
	EnhanceSleep = 2147,
	AdjEnhanceSleep = 2148,
	MulEnhanceSleep = 2149,
	EnhanceChaos = 2150,
	AdjEnhanceChaos = 2151,
	MulEnhanceChaos = 2152,
	EnhanceBind = 2153,
	AdjEnhanceBind = 2154,
	MulEnhanceBind = 2155,
	EnhanceSilence = 2156,
	AdjEnhanceSilence = 2157,
	MulEnhanceSilence = 2158,
	AntiDizzy = 2159,
	AdjAntiDizzy = 2160,
	MulAntiDizzy = 2161,
	AntiSleep = 2162,
	AdjAntiSleep = 2163,
	MulAntiSleep = 2164,
	AntiChaos = 2165,
	AdjAntiChaos = 2166,
	MulAntiChaos = 2167,
	AntiBind = 2168,
	AdjAntiBind = 2169,
	MulAntiBind = 2170,
	AntiSilence = 2171,
	AdjAntiSilence = 2172,
	MulAntiSilence = 2173,
	DizzyTimeChange = 2174,
	SleepTimeChange = 2175,
	ChaosTimeChange = 2176,
	BindTimeChange = 2177,
	SilenceTimeChange = 2178,
	BianhuTimeChange = 2179,
	FreezeTimeChange = 2180,
	PetrifyTimeChange = 2181,
	EnhanceHeal = 2182,
	AdjEnhanceHeal = 2183,
	MulEnhanceHeal = 2184,
	Speed = 2185,
	OriSpeed = 2186,
	AdjSpeed = 2187,
	MulSpeed = 2188,
	MF = 2189,
	AdjMF = 2190,
	Range = 2191,
	AdjRange = 2192,
	HatePlus = 2193,
	AdjHatePlus = 2194,
	HateDecrease = 2195,
	Invisible = 2196,
	AdjInvisible = 2197,
	TrueSight = 2198,
	OriTrueSight = 2199,
	AdjTrueSight = 2200,
	EyeSight = 2201,
	OriEyeSight = 2202,
	AdjEyeSight = 2203,
	EnhanceTian = 2204,
	EnhanceTianMul = 2205,
	EnhanceEGui = 2206,
	EnhanceEGuiMul = 2207,
	EnhanceXiuLuo = 2208,
	EnhanceXiuLuoMul = 2209,
	EnhanceDiYu = 2210,
	EnhanceDiYuMul = 2211,
	EnhanceRen = 2212,
	EnhanceRenMul = 2213,
	EnhanceChuSheng = 2214,
	EnhanceChuShengMul = 2215,
	EnhanceBuilding = 2216,
	EnhanceBuildingMul = 2217,
	EnhanceBoss = 2218,
	EnhanceBossMul = 2219,
	EnhanceSheShou = 2220,
	EnhanceSheShouMul = 2221,
	EnhanceJiaShi = 2222,
	EnhanceJiaShiMul = 2223,
	EnhanceFangShi = 2224,
	EnhanceFangShiMul = 2225,
	EnhanceYiShi = 2226,
	EnhanceYiShiMul = 2227,
	EnhanceMeiZhe = 2228,
	EnhanceMeiZheMul = 2229,
	EnhanceYiRen = 2230,
	EnhanceYiRenMul = 2231,
	IgnorepDef = 2232,
	IgnoremDef = 2233,
	EnhanceZhaoHuan = 2234,
	EnhanceZhaoHuanMul = 2235,
	DizzyProbability = 2236,
	SleepProbability = 2237,
	ChaosProbability = 2238,
	BindProbability = 2239,
	SilenceProbability = 2240,
	HpFullPow2 = 2241,
	HpFullPow1 = 2242,
	HpFullInit = 2243,
	MpFullPow1 = 2244,
	MpFullInit = 2245,
	MpRecoverPow1 = 2246,
	MpRecoverInit = 2247,
	PAttMinPow2 = 2248,
	PAttMinPow1 = 2249,
	PAttMinInit = 2250,
	PAttMaxPow2 = 2251,
	PAttMaxPow1 = 2252,
	PAttMaxInit = 2253,
	PhitPow1 = 2254,
	PHitInit = 2255,
	PMissPow1 = 2256,
	PMissInit = 2257,
	PSpeedPow1 = 2258,
	PSpeedInit = 2259,
	PDefPow1 = 2260,
	PDefInit = 2261,
	PfatalPow1 = 2262,
	PFatalInit = 2263,
	MAttMinPow2 = 2264,
	MAttMinPow1 = 2265,
	MAttMinInit = 2266,
	MAttMaxPow2 = 2267,
	MAttMaxPow1 = 2268,
	MAttMaxInit = 2269,
	MSpeedPow1 = 2270,
	MSpeedInit = 2271,
	MDefPow1 = 2272,
	MDefInit = 2273,
	MHitPow1 = 2274,
	MHitInit = 2275,
	MMissPow1 = 2276,
	MMissInit = 2277,
	MFatalPow1 = 2278,
	MFatalInit = 2279,
	BlockDamagePow1 = 2280,
	BlockDamageInit = 2281,
	RunSpeed = 2282,
	HpRecoverPow1 = 2283,
	HpRecoverInit = 2284,
	AntiBlockDamagePow1 = 2285,
	AntiBlockDamageInit = 2286,
	BBMax = 2287,
	AdjBBMax = 2288,
	AntiBreak = 2289,
	AdjAntiBreak = 2290,
	AntiPushPull = 2291,
	AdjAntiPushPull = 2292,
	AntiPetrify = 2293,
	AdjAntiPetrify = 2294,
	MulAntiPetrify = 2295,
	AntiTie = 2296,
	AdjAntiTie = 2297,
	MulAntiTie = 2298,
	EnhancePetrify = 2299,
	AdjEnhancePetrify = 2300,
	MulEnhancePetrify = 2301,
	EnhanceTie = 2302,
	AdjEnhanceTie = 2303,
	MulEnhanceTie = 2304,
	TieProbability = 2305,
	StrZizhi = 2306,
	CorZizhi = 2307,
	StaZizhi = 2308,
	AgiZizhi = 2309,
	IntZizhi = 2310,
	InitStrZizhi = 2311,
	InitCorZizhi = 2312,
	InitStaZizhi = 2313,
	InitAgiZizhi = 2314,
	InitIntZizhi = 2315,
	Wuxing = 2316,
	Xiuwei = 2317,
	SkillNum = 2318,
	PType = 2319,
	MType = 2320,
	PHType = 2321,
	GrowFactor = 2322,
	WuxingImprove = 2323,
	XiuweiImprove = 2324,
	EnhanceBeHeal = 2325,
	AdjEnhanceBeHeal = 2326,
	MulEnhanceBeHeal = 2327,
	LookUpCor = 2328,
	LookUpSta = 2329,
	LookUpStr = 2330,
	LookUpInt = 2331,
	LookUpAgi = 2332,
	LookUpHpFull = 2333,
	LookUpMpFull = 2334,
	LookUppAttMin = 2335,
	LookUppAttMax = 2336,
	LookUppHit = 2337,
	LookUppMiss = 2338,
	LookUppDef = 2339,
	LookUpmAttMin = 2340,
	LookUpmAttMax = 2341,
	LookUpmHit = 2342,
	LookUpmMiss = 2343,
	LookUpmDef = 2344,
	AdjHpFull2 = 2345,
	AdjpAttMin2 = 2346,
	AdjpAttMax2 = 2347,
	AdjmAttMin2 = 2348,
	AdjmAttMax2 = 2349,
	LingShouType = 2350,
	MHType = 2351,
	EnhanceLingShou = 2352,
	EnhanceLingShouMul = 2353,
	AntiAoe = 2354,
	AdjAntiAoe = 2355,
	AntiBlind = 2356,
	AdjAntiBlind = 2357,
	MulAntiBlind = 2358,
	AntiDecelerate = 2359,
	AdjAntiDecelerate = 2360,
	MulAntiDecelerate = 2361,
	MinGrade = 2362,
	CharacterCor = 2363,
	CharacterSta = 2364,
	CharacterStr = 2365,
	CharacterInt = 2366,
	CharacterAgi = 2367,
	EnhanceDaoKe = 2368,
	EnhanceDaoKeMul = 2369,
	ZuoQiSpeed = 2370,
	EnhanceBianHu = 2371,
	AdjEnhanceBianHu = 2372,
	MulEnhanceBianHu = 2373,
	AntiBianHu = 2374,
	AdjAntiBianHu = 2375,
	MulAntiBianHu = 2376,
	EnhanceXiaKe = 2377,
	EnhanceXiaKeMul = 2378,
	TieTimeChange = 2379,
	EnhanceYanShi = 2380,
	EnhanceYanShiMul = 2381,
	PAType = 2382,
	MAType = 2383,
	lifetimeNoReduceRate = 2384,
	AddLSHpDurgImprove = 2385,
	AddHpDurgImprove = 2386,
	AddMpDurgImprove = 2387,
	FireHurtReduce = 2388,
	ThunderHurtReduce = 2389,
	IceHurtReduce = 2390,
	PoisonHurtReduce = 2391,
	WindHurtReduce = 2392,
	LightHurtReduce = 2393,
	IllusionHurtReduce = 2394,
	FakepDef = 2395,
	FakemDef = 2396,
	EnhanceWater = 2397,
	AdjEnhanceWater = 2398,
	MulEnhanceWater = 2399,
	AntiWater = 2400,
	AdjAntiWater = 2401,
	MulAntiWater = 2402,
	WaterHurtReduce = 2403,
	IgnoreAntiWater = 2404,
	AntiFreeze = 2405,
	AdjAntiFreeze = 2406,
	MulAntiFreeze = 2407,
	EnhanceFreeze = 2408,
	AdjEnhanceFreeze = 2409,
	MulEnhanceFreeze = 2410,
	EnhanceHuaHun = 2411,
	EnhanceHuaHunMul = 2412,
	TrueSightProb = 2413,
	LSBBGrade = 2414,
	LSBBQuality = 2415,
	LSBBAdjHp = 2416,
	AdjIgnoreAntiFire = 2417,
	AdjIgnoreAntiThunder = 2418,
	AdjIgnoreAntiIce = 2419,
	AdjIgnoreAntiPoison = 2420,
	AdjIgnoreAntiWind = 2421,
	AdjIgnoreAntiLight = 2422,
	AdjIgnoreAntiIllusion = 2423,
	IgnoreAntiFireLSMul = 2424,
	IgnoreAntiThunderLSMul = 2425,
	IgnoreAntiIceLSMul = 2426,
	IgnoreAntiPoisonLSMul = 2427,
	IgnoreAntiWindLSMul = 2428,
	IgnoreAntiLightLSMul = 2429,
	IgnoreAntiIllusionLSMul = 2430,
	AdjIgnoreAntiWater = 2431,
	IgnoreAntiWaterLSMul = 2432,
	MulSpeed2 = 2433,
	MulConsumeMp = 2434,
	AdjAgi2 = 2435,
	AdjAntiBianHu2 = 2436,
	AdjAntiBind2 = 2437,
	AdjAntiBlind2 = 2438,
	AdjAntiBlock2 = 2439,
	AdjAntiBlockDamage2 = 2440,
	AdjAntiChaos2 = 2441,
	AdjAntiDecelerate2 = 2442,
	AdjAntiDizzy2 = 2443,
	AdjAntiFire2 = 2444,
	AdjAntiFreeze2 = 2445,
	AdjAntiIce2 = 2446,
	AdjAntiIllusion2 = 2447,
	AdjAntiLight2 = 2448,
	AdjAntimFatal2 = 2449,
	AdjAntimFatalDamage2 = 2450,
	AdjAntiPetrify2 = 2451,
	AdjAntipFatal2 = 2452,
	AdjAntipFatalDamage2 = 2453,
	AdjAntiPoison2 = 2454,
	AdjAntiSilence2 = 2455,
	AdjAntiSleep2 = 2456,
	AdjAntiThunder2 = 2457,
	AdjAntiTie2 = 2458,
	AdjAntiWater2 = 2459,
	AdjAntiWind2 = 2460,
	AdjBlock2 = 2461,
	AdjBlockDamage2 = 2462,
	AdjCor2 = 2463,
	AdjEnhanceBianHu2 = 2464,
	AdjEnhanceBind2 = 2465,
	AdjEnhanceChaos2 = 2466,
	AdjEnhanceDizzy2 = 2467,
	AdjEnhanceFire2 = 2468,
	AdjEnhanceFreeze2 = 2469,
	AdjEnhanceIce2 = 2470,
	AdjEnhanceIllusion2 = 2471,
	AdjEnhanceLight2 = 2472,
	AdjEnhancePetrify2 = 2473,
	AdjEnhancePoison2 = 2474,
	AdjEnhanceSilence2 = 2475,
	AdjEnhanceSleep2 = 2476,
	AdjEnhanceThunder2 = 2477,
	AdjEnhanceTie2 = 2478,
	AdjEnhanceWater2 = 2479,
	AdjEnhanceWind2 = 2480,
	AdjIgnoreAntiFire2 = 2481,
	AdjIgnoreAntiIce2 = 2482,
	AdjIgnoreAntiIllusion2 = 2483,
	AdjIgnoreAntiLight2 = 2484,
	AdjIgnoreAntiPoison2 = 2485,
	AdjIgnoreAntiThunder2 = 2486,
	AdjIgnoreAntiWater2 = 2487,
	AdjIgnoreAntiWind2 = 2488,
	AdjInt2 = 2489,
	AdjmDef2 = 2490,
	AdjmFatal2 = 2491,
	AdjmFatalDamage2 = 2492,
	AdjmHit2 = 2493,
	AdjmHurt2 = 2494,
	AdjmMiss2 = 2495,
	AdjmSpeed2 = 2496,
	AdjpDef2 = 2497,
	AdjpFatal2 = 2498,
	AdjpFatalDamage2 = 2499,
	AdjpHit2 = 2500,
	AdjpHurt2 = 2501,
	AdjpMiss2 = 2502,
	AdjpSpeed2 = 2503,
	AdjStr2 = 2504,
	LSpAttMin = 2505,
	LSpAttMax = 2506,
	LSmAttMin = 2507,
	LSmAttMax = 2508,
	LSpFatal = 2509,
	LSpFatalDamage = 2510,
	LSmFatal = 2511,
	LSmFatalDamage = 2512,
	LSAntiBlock = 2513,
	LSAntiBlockDamage = 2514,
	LSIgnoreAntiFire = 2515,
	LSIgnoreAntiThunder = 2516,
	LSIgnoreAntiIce = 2517,
	LSIgnoreAntiPoison = 2518,
	LSIgnoreAntiWind = 2519,
	LSIgnoreAntiLight = 2520,
	LSIgnoreAntiIllusion = 2521,
	LSIgnoreAntiWater = 2522,
	LSpAttMin_adj = 2523,
	LSpAttMax_adj = 2524,
	LSmAttMin_adj = 2525,
	LSmAttMax_adj = 2526,
	LSpFatal_adj = 2527,
	LSpFatalDamage_adj = 2528,
	LSmFatal_adj = 2529,
	LSmFatalDamage_adj = 2530,
	LSAntiBlock_adj = 2531,
	LSAntiBlockDamage_adj = 2532,
	LSIgnoreAntiFire_adj = 2533,
	LSIgnoreAntiThunder_adj = 2534,
	LSIgnoreAntiIce_adj = 2535,
	LSIgnoreAntiPoison_adj = 2536,
	LSIgnoreAntiWind_adj = 2537,
	LSIgnoreAntiLight_adj = 2538,
	LSIgnoreAntiIllusion_adj = 2539,
	LSIgnoreAntiWater_adj = 2540,
	LSpAttMin_jc = 2541,
	LSpAttMax_jc = 2542,
	LSmAttMin_jc = 2543,
	LSmAttMax_jc = 2544,
	LSpFatal_jc = 2545,
	LSpFatalDamage_jc = 2546,
	LSmFatal_jc = 2547,
	LSmFatalDamage_jc = 2548,
	LSAntiBlock_jc = 2549,
	LSAntiBlockDamage_jc = 2550,
	LSIgnoreAntiFire_jc = 2551,
	LSIgnoreAntiThunder_jc = 2552,
	LSIgnoreAntiIce_jc = 2553,
	LSIgnoreAntiPoison_jc = 2554,
	LSIgnoreAntiWind_jc = 2555,
	LSIgnoreAntiLight_jc = 2556,
	LSIgnoreAntiIllusion_jc = 2557,
	LSIgnoreAntiWater_jc = 2558,
	LSIgnoreAntiFire_mul = 2559,
	LSIgnoreAntiThunder_mul = 2560,
	LSIgnoreAntiIce_mul = 2561,
	LSIgnoreAntiPoison_mul = 2562,
	LSIgnoreAntiWind_mul = 2563,
	LSIgnoreAntiLight_mul = 2564,
	LSIgnoreAntiIllusion_mul = 2565,
	LSIgnoreAntiWater_mul = 2566,
	JieBan_Child_QiChangColor = 2567,
	JieBan_LingShou_Ratio = 2568,
	JieBan_LingShou_QinMi = 2569,
	JieBan_LingShou_CorZizhi = 2570,
	JieBan_LingShou_StaZizhi = 2571,
	JieBan_LingShou_StrZizhi = 2572,
	JieBan_LingShou_IntZizhi = 2573,
	JieBan_LingShou_AgiZizhi = 2574,
	Buff_DisplayValue1 = 2575,
	Buff_DisplayValue2 = 2576,
	Buff_DisplayValue3 = 2577,
	Buff_DisplayValue4 = 2578,
	Buff_DisplayValue5 = 2579,
	LSNature = 2580,
	PrayMulti = 2581,
	EnhanceYingLing = 2582,
	EnhanceYingLingMul = 2583,
	EnhanceFlag = 2584,
	EnhanceFlagMul = 2585,
	ObjectType = 2586,
	MonsterType = 2587,
	FightPropType = 2588,
	PlayHpFull = 2589,
	PlayHp = 2590,
	PlayAtt = 2591,
	PlayDef = 2592,
	PlayHit = 2593,
	PlayMiss = 2594,
	EnhanceDieKe = 2595,
	EnhanceDieKeMul = 2596,
	DotRemain = 2597,
	IsConfused = 2598,
	EnhanceSheShouKefu = 2599,
	EnhanceJiaShiKefu = 2600,
	EnhanceDaoKeKefu = 2601,
	EnhanceXiaKeKefu = 2602,
	EnhanceFangShiKefu = 2603,
	EnhanceYiShiKefu = 2604,
	EnhanceMeiZheKefu = 2605,
	EnhanceYiRenKefu = 2606,
	EnhanceYanShiKefu = 2607,
	EnhanceHuaHunKefu = 2608,
	EnhanceYingLingKefu = 2609,
	EnhanceDieKeKefu = 2610,
	AdjustPermanentCor = 2611,
	AdjustPermanentSta = 2612,
	AdjustPermanentStr = 2613,
	AdjustPermanentInt = 2614,
	AdjustPermanentAgi = 2615,
	SoulCoreLevel = 2616,
	AdjustPermanentMidCor = 2617,
	AdjustPermanentMidSta = 2618,
	AdjustPermanentMidStr = 2619,
	AdjustPermanentMidInt = 2620,
	AdjustPermanentMidAgi = 2621,
	SumPermanent = 2622,
	AntiTian = 2623,
	AntiEGui = 2624,
	AntiXiuLuo = 2625,
	AntiDiYu = 2626,
	AntiRen = 2627,
	AntiChuSheng = 2628,
	AntiBuilding = 2629,
	AntiZhaoHuan = 2630,
	AntiLingShou = 2631,
	AntiBoss = 2632,
	AntiSheShou = 2633,
	AntiJiaShi = 2634,
	AntiDaoKe = 2635,
	AntiXiaKe = 2636,
	AntiFangShi = 2637,
	AntiYiShi = 2638,
	AntiMeiZhe = 2639,
	AntiYiRen = 2640,
	AntiYanShi = 2641,
	AntiHuaHun = 2642,
	AntiYingLing = 2643,
	AntiDieKe = 2644,
	AntiFlag = 2645,
	EnhanceZhanKuang = 2646,
	EnhanceZhanKuangMul = 2647,
	EnhanceZhanKuangKefu = 2648,
	AntiZhanKuang = 2649,
	JumpSpeed = 2650,
	OriJumpSpeed = 2651,
	AdjJumpSpeed = 2652,
	GravityAcceleration = 2653,
	OriGravityAcceleration = 2654,
	AdjGravityAcceleration = 2655,
	HorizontalAcceleration = 2656,
	OriHorizontalAcceleration = 2657,
	AdjHorizontalAcceleration = 2658,
	SwimSpeed = 2659,
	OriSwimSpeed = 2660,
	AdjSwimSpeed = 2661,
	StrengthPoint = 2662,
	StrengthPointMax = 2663,
	OriStrengthPointMax = 2664,
	AdjStrengthPointMax = 2665,
	AntiMulEnhanceIllusion = 2666,
	GlideHorizontalSpeedWithUmbrella = 2667,
	GlideVerticalSpeedWithUmbrella = 2668,
	AdjpSpeed = 3001,
	AdjmSpeed = 3002,
	
}


CLuaCopyPropertyFight = {}

CLuaCopyPropertyFight.Param = nil
CLuaCopyPropertyFight.TmpParam = nil
local function SetParam_At(id, v)
	CLuaCopyPropertyFight.Param[id] = v
end
local function GetParam_At(id)
	return CLuaCopyPropertyFight.Param[id] or 0
end
local function SetTmpParam_At(id, v)
	CLuaCopyPropertyFight.TmpParam[id] = v
end
local function GetTmpParam_At(id)
	return CLuaCopyPropertyFight.TmpParam[id] or 0
end

local function LookUpGrow1(clazz, target, keys, defaultValue)
	local key = 0
	for i,v in ipairs(keys) do
		if i==1 then
			key = v*1000
		else
			key = key + v
		end
	end
	local data = Grow_Grow1.GetData(key)
	if data then
		return CommonDefs.DictGetValue_LuaCall(data.PropValues,target)
	end
	return defaultValue
end

local function LookUpGrow2(clazz, target, keys, defaultValue)
	local key = ""
	for i,v in ipairs(keys) do
		if i==1 then
			key = tostring(v)
		else
			key = key.."_"..v
		end
	end
	local data = Grow_Grow2.GetData(key)
	if data then
		return CommonDefs.DictGetValue_LuaCall(data.PropValues,target)
	end
	return defaultValue
end

local CCopyPropertyFight = import "L10.Game.CCopyPropertyFight"
local SetParamFunctions = {}
CCopyPropertyFight.m_hookSetParam = function(this,id,newv)
	CLuaCopyPropertyFight.Param = this.Param
	CLuaCopyPropertyFight.TmpParam = this.TmpParam
	if SetParamFunctions[id] then
		SetParamFunctions[id](newv)
	end
	CLuaCopyPropertyFight.Param = nil
	CLuaCopyPropertyFight.TmpParam = nil
end
CLuaCopyPropertyFight.SetParamPermanentCor = function(newv)
	SetParam_At(1, newv)
end
CLuaCopyPropertyFight.SetParamPermanentSta = function(newv)
	SetParam_At(2, newv)
end
CLuaCopyPropertyFight.SetParamPermanentStr = function(newv)
	SetParam_At(3, newv)
end
CLuaCopyPropertyFight.SetParamPermanentInt = function(newv)
	SetParam_At(4, newv)
end
CLuaCopyPropertyFight.SetParamPermanentAgi = function(newv)
	SetParam_At(5, newv)
end
CLuaCopyPropertyFight.SetParamPermanentHpFull = function(newv)
	SetParam_At(6, newv)
end
CLuaCopyPropertyFight.SetParamHp = function(newv)
	SetParam_At(7, newv)
end
CLuaCopyPropertyFight.SetParamMp = function(newv)
	SetParam_At(8, newv)
end
CLuaCopyPropertyFight.SetParamCurrentHpFull = function(newv)
	SetParam_At(9, newv)
end
CLuaCopyPropertyFight.SetParamCurrentMpFull = function(newv)
	SetParam_At(10, newv)
end
CLuaCopyPropertyFight.SetParamEquipExchangeMF = function(newv)
	SetParam_At(11, newv)
end
CLuaCopyPropertyFight.SetParamRevisePermanentCor = function(newv)
	SetParam_At(12, newv)
end
CLuaCopyPropertyFight.SetParamRevisePermanentSta = function(newv)
	SetParam_At(13, newv)
end
CLuaCopyPropertyFight.SetParamRevisePermanentStr = function(newv)
	SetParam_At(14, newv)
end
CLuaCopyPropertyFight.SetParamRevisePermanentInt = function(newv)
	SetParam_At(15, newv)
end
CLuaCopyPropertyFight.SetParamRevisePermanentAgi = function(newv)
	SetParam_At(16, newv)
end
CLuaCopyPropertyFight.SetParamClass = function(newv)
	SetTmpParam_At(1, newv)
end
CLuaCopyPropertyFight.SetParamGrade = function(newv)
	SetTmpParam_At(2, newv)
end
CLuaCopyPropertyFight.SetParamCor = function(newv)
	SetTmpParam_At(3, newv)
end
CLuaCopyPropertyFight.SetParamAdjCor = function(newv)
	SetTmpParam_At(4, newv)
end
CLuaCopyPropertyFight.SetParamMulCor = function(newv)
	SetTmpParam_At(5, newv)
end
CLuaCopyPropertyFight.SetParamSta = function(newv)
	SetTmpParam_At(6, newv)
end
CLuaCopyPropertyFight.SetParamAdjSta = function(newv)
	SetTmpParam_At(7, newv)
end
CLuaCopyPropertyFight.SetParamMulSta = function(newv)
	SetTmpParam_At(8, newv)
end
CLuaCopyPropertyFight.SetParamStr = function(newv)
	SetTmpParam_At(9, newv)
end
CLuaCopyPropertyFight.SetParamAdjStr = function(newv)
	SetTmpParam_At(10, newv)
end
CLuaCopyPropertyFight.SetParamMulStr = function(newv)
	SetTmpParam_At(11, newv)
end
CLuaCopyPropertyFight.SetParamInt = function(newv)
	SetTmpParam_At(12, newv)
end
CLuaCopyPropertyFight.SetParamAdjInt = function(newv)
	SetTmpParam_At(13, newv)
end
CLuaCopyPropertyFight.SetParamMulInt = function(newv)
	SetTmpParam_At(14, newv)
end
CLuaCopyPropertyFight.SetParamAgi = function(newv)
	SetTmpParam_At(15, newv)
end
CLuaCopyPropertyFight.SetParamAdjAgi = function(newv)
	SetTmpParam_At(16, newv)
end
CLuaCopyPropertyFight.SetParamMulAgi = function(newv)
	SetTmpParam_At(17, newv)
end
CLuaCopyPropertyFight.SetParamHpFull = function(newv)
	SetTmpParam_At(18, newv)
end
CLuaCopyPropertyFight.SetParamAdjHpFull = function(newv)
	SetTmpParam_At(19, newv)
end
CLuaCopyPropertyFight.SetParamMulHpFull = function(newv)
	SetTmpParam_At(20, newv)
end
CLuaCopyPropertyFight.SetParamHpRecover = function(newv)
	SetTmpParam_At(21, newv)
end
CLuaCopyPropertyFight.SetParamAdjHpRecover = function(newv)
	SetTmpParam_At(22, newv)
end
CLuaCopyPropertyFight.SetParamMulHpRecover = function(newv)
	SetTmpParam_At(23, newv)
end
CLuaCopyPropertyFight.SetParamMpFull = function(newv)
	SetTmpParam_At(24, newv)
end
CLuaCopyPropertyFight.SetParamAdjMpFull = function(newv)
	SetTmpParam_At(25, newv)
end
CLuaCopyPropertyFight.SetParamMulMpFull = function(newv)
	SetTmpParam_At(26, newv)
end
CLuaCopyPropertyFight.SetParamMpRecover = function(newv)
	SetTmpParam_At(27, newv)
end
CLuaCopyPropertyFight.SetParamAdjMpRecover = function(newv)
	SetTmpParam_At(28, newv)
end
CLuaCopyPropertyFight.SetParamMulMpRecover = function(newv)
	SetTmpParam_At(29, newv)
end
CLuaCopyPropertyFight.SetParampAttMin = function(newv)
	SetTmpParam_At(30, newv)
end
CLuaCopyPropertyFight.SetParamAdjpAttMin = function(newv)
	SetTmpParam_At(31, newv)
end
CLuaCopyPropertyFight.SetParamMulpAttMin = function(newv)
	SetTmpParam_At(32, newv)
end
CLuaCopyPropertyFight.SetParampAttMax = function(newv)
	SetTmpParam_At(33, newv)
end
CLuaCopyPropertyFight.SetParamAdjpAttMax = function(newv)
	SetTmpParam_At(34, newv)
end
CLuaCopyPropertyFight.SetParamMulpAttMax = function(newv)
	SetTmpParam_At(35, newv)
end
CLuaCopyPropertyFight.SetParampHit = function(newv)
	SetTmpParam_At(36, newv)
end
CLuaCopyPropertyFight.SetParamAdjpHit = function(newv)
	SetTmpParam_At(37, newv)
end
CLuaCopyPropertyFight.SetParamMulpHit = function(newv)
	SetTmpParam_At(38, newv)
end
CLuaCopyPropertyFight.SetParampMiss = function(newv)
	SetTmpParam_At(39, newv)
end
CLuaCopyPropertyFight.SetParamAdjpMiss = function(newv)
	SetTmpParam_At(40, newv)
end
CLuaCopyPropertyFight.SetParamMulpMiss = function(newv)
	SetTmpParam_At(41, newv)
end
CLuaCopyPropertyFight.SetParampDef = function(newv)
	SetTmpParam_At(42, newv)
end
CLuaCopyPropertyFight.SetParamAdjpDef = function(newv)
	SetTmpParam_At(43, newv)
end
CLuaCopyPropertyFight.SetParamMulpDef = function(newv)
	SetTmpParam_At(44, newv)
end
CLuaCopyPropertyFight.SetParamAdjpHurt = function(newv)
	SetTmpParam_At(45, newv)
end
CLuaCopyPropertyFight.SetParamMulpHurt = function(newv)
	SetTmpParam_At(46, newv)
end
CLuaCopyPropertyFight.SetParampSpeed = function(newv)
	SetTmpParam_At(47, newv)
end
CLuaCopyPropertyFight.SetParamOripSpeed = function(newv)
	SetTmpParam_At(48, newv)
end
CLuaCopyPropertyFight.SetParamMulpSpeed = function(newv)
	SetTmpParam_At(49, newv)
end
CLuaCopyPropertyFight.SetParampFatal = function(newv)
	SetTmpParam_At(50, newv)
end
CLuaCopyPropertyFight.SetParamAdjpFatal = function(newv)
	SetTmpParam_At(51, newv)
end
CLuaCopyPropertyFight.SetParamAntipFatal = function(newv)
	SetTmpParam_At(52, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntipFatal = function(newv)
	SetTmpParam_At(53, newv)
end
CLuaCopyPropertyFight.SetParampFatalDamage = function(newv)
	SetTmpParam_At(54, newv)
end
CLuaCopyPropertyFight.SetParamAdjpFatalDamage = function(newv)
	SetTmpParam_At(55, newv)
end
CLuaCopyPropertyFight.SetParamAntipFatalDamage = function(newv)
	SetTmpParam_At(56, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntipFatalDamage = function(newv)
	SetTmpParam_At(57, newv)
end
CLuaCopyPropertyFight.SetParamBlock = function(newv)
	SetTmpParam_At(58, newv)
end
CLuaCopyPropertyFight.SetParamAdjBlock = function(newv)
	SetTmpParam_At(59, newv)
end
CLuaCopyPropertyFight.SetParamMulBlock = function(newv)
	SetTmpParam_At(60, newv)
end
CLuaCopyPropertyFight.SetParamBlockDamage = function(newv)
	SetTmpParam_At(61, newv)
end
CLuaCopyPropertyFight.SetParamAdjBlockDamage = function(newv)
	SetTmpParam_At(62, newv)
end
CLuaCopyPropertyFight.SetParamAntiBlock = function(newv)
	SetTmpParam_At(63, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiBlock = function(newv)
	SetTmpParam_At(64, newv)
end
CLuaCopyPropertyFight.SetParamAntiBlockDamage = function(newv)
	SetTmpParam_At(65, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiBlockDamage = function(newv)
	SetTmpParam_At(66, newv)
end
CLuaCopyPropertyFight.SetParammAttMin = function(newv)
	SetTmpParam_At(67, newv)
end
CLuaCopyPropertyFight.SetParamAdjmAttMin = function(newv)
	SetTmpParam_At(68, newv)
end
CLuaCopyPropertyFight.SetParamMulmAttMin = function(newv)
	SetTmpParam_At(69, newv)
end
CLuaCopyPropertyFight.SetParammAttMax = function(newv)
	SetTmpParam_At(70, newv)
end
CLuaCopyPropertyFight.SetParamAdjmAttMax = function(newv)
	SetTmpParam_At(71, newv)
end
CLuaCopyPropertyFight.SetParamMulmAttMax = function(newv)
	SetTmpParam_At(72, newv)
end
CLuaCopyPropertyFight.SetParammHit = function(newv)
	SetTmpParam_At(73, newv)
end
CLuaCopyPropertyFight.SetParamAdjmHit = function(newv)
	SetTmpParam_At(74, newv)
end
CLuaCopyPropertyFight.SetParamMulmHit = function(newv)
	SetTmpParam_At(75, newv)
end
CLuaCopyPropertyFight.SetParammMiss = function(newv)
	SetTmpParam_At(76, newv)
end
CLuaCopyPropertyFight.SetParamAdjmMiss = function(newv)
	SetTmpParam_At(77, newv)
end
CLuaCopyPropertyFight.SetParamMulmMiss = function(newv)
	SetTmpParam_At(78, newv)
end
CLuaCopyPropertyFight.SetParammDef = function(newv)
	SetTmpParam_At(79, newv)
end
CLuaCopyPropertyFight.SetParamAdjmDef = function(newv)
	SetTmpParam_At(80, newv)
end
CLuaCopyPropertyFight.SetParamMulmDef = function(newv)
	SetTmpParam_At(81, newv)
end
CLuaCopyPropertyFight.SetParamAdjmHurt = function(newv)
	SetTmpParam_At(82, newv)
end
CLuaCopyPropertyFight.SetParamMulmHurt = function(newv)
	SetTmpParam_At(83, newv)
end
CLuaCopyPropertyFight.SetParammSpeed = function(newv)
	SetTmpParam_At(84, newv)
end
CLuaCopyPropertyFight.SetParamOrimSpeed = function(newv)
	SetTmpParam_At(85, newv)
end
CLuaCopyPropertyFight.SetParamMulmSpeed = function(newv)
	SetTmpParam_At(86, newv)
end
CLuaCopyPropertyFight.SetParammFatal = function(newv)
	SetTmpParam_At(87, newv)
end
CLuaCopyPropertyFight.SetParamAdjmFatal = function(newv)
	SetTmpParam_At(88, newv)
end
CLuaCopyPropertyFight.SetParamAntimFatal = function(newv)
	SetTmpParam_At(89, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntimFatal = function(newv)
	SetTmpParam_At(90, newv)
end
CLuaCopyPropertyFight.SetParammFatalDamage = function(newv)
	SetTmpParam_At(91, newv)
end
CLuaCopyPropertyFight.SetParamAdjmFatalDamage = function(newv)
	SetTmpParam_At(92, newv)
end
CLuaCopyPropertyFight.SetParamAntimFatalDamage = function(newv)
	SetTmpParam_At(93, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntimFatalDamage = function(newv)
	SetTmpParam_At(94, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceFire = function(newv)
	SetTmpParam_At(95, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceFire = function(newv)
	SetTmpParam_At(96, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhanceFire = function(newv)
	SetTmpParam_At(97, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceThunder = function(newv)
	SetTmpParam_At(98, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceThunder = function(newv)
	SetTmpParam_At(99, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhanceThunder = function(newv)
	SetTmpParam_At(100, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceIce = function(newv)
	SetTmpParam_At(101, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceIce = function(newv)
	SetTmpParam_At(102, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhanceIce = function(newv)
	SetTmpParam_At(103, newv)
end
CLuaCopyPropertyFight.SetParamEnhancePoison = function(newv)
	SetTmpParam_At(104, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhancePoison = function(newv)
	SetTmpParam_At(105, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhancePoison = function(newv)
	SetTmpParam_At(106, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceWind = function(newv)
	SetTmpParam_At(107, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceWind = function(newv)
	SetTmpParam_At(108, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhanceWind = function(newv)
	SetTmpParam_At(109, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceLight = function(newv)
	SetTmpParam_At(110, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceLight = function(newv)
	SetTmpParam_At(111, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhanceLight = function(newv)
	SetTmpParam_At(112, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceIllusion = function(newv)
	SetTmpParam_At(113, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceIllusion = function(newv)
	SetTmpParam_At(114, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhanceIllusion = function(newv)
	SetTmpParam_At(115, newv)
end
CLuaCopyPropertyFight.SetParamAntiFire = function(newv)
	SetTmpParam_At(116, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiFire = function(newv)
	SetTmpParam_At(117, newv)
end
CLuaCopyPropertyFight.SetParamMulAntiFire = function(newv)
	SetTmpParam_At(118, newv)
end
CLuaCopyPropertyFight.SetParamAntiThunder = function(newv)
	SetTmpParam_At(119, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiThunder = function(newv)
	SetTmpParam_At(120, newv)
end
CLuaCopyPropertyFight.SetParamMulAntiThunder = function(newv)
	SetTmpParam_At(121, newv)
end
CLuaCopyPropertyFight.SetParamAntiIce = function(newv)
	SetTmpParam_At(122, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiIce = function(newv)
	SetTmpParam_At(123, newv)
end
CLuaCopyPropertyFight.SetParamMulAntiIce = function(newv)
	SetTmpParam_At(124, newv)
end
CLuaCopyPropertyFight.SetParamAntiPoison = function(newv)
	SetTmpParam_At(125, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiPoison = function(newv)
	SetTmpParam_At(126, newv)
end
CLuaCopyPropertyFight.SetParamMulAntiPoison = function(newv)
	SetTmpParam_At(127, newv)
end
CLuaCopyPropertyFight.SetParamAntiWind = function(newv)
	SetTmpParam_At(128, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiWind = function(newv)
	SetTmpParam_At(129, newv)
end
CLuaCopyPropertyFight.SetParamMulAntiWind = function(newv)
	SetTmpParam_At(130, newv)
end
CLuaCopyPropertyFight.SetParamAntiLight = function(newv)
	SetTmpParam_At(131, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiLight = function(newv)
	SetTmpParam_At(132, newv)
end
CLuaCopyPropertyFight.SetParamMulAntiLight = function(newv)
	SetTmpParam_At(133, newv)
end
CLuaCopyPropertyFight.SetParamAntiIllusion = function(newv)
	SetTmpParam_At(134, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiIllusion = function(newv)
	SetTmpParam_At(135, newv)
end
CLuaCopyPropertyFight.SetParamMulAntiIllusion = function(newv)
	SetTmpParam_At(136, newv)
end
CLuaCopyPropertyFight.SetParamIgnoreAntiFire = function(newv)
	SetTmpParam_At(137, newv)
end
CLuaCopyPropertyFight.SetParamIgnoreAntiThunder = function(newv)
	SetTmpParam_At(138, newv)
end
CLuaCopyPropertyFight.SetParamIgnoreAntiIce = function(newv)
	SetTmpParam_At(139, newv)
end
CLuaCopyPropertyFight.SetParamIgnoreAntiPoison = function(newv)
	SetTmpParam_At(140, newv)
end
CLuaCopyPropertyFight.SetParamIgnoreAntiWind = function(newv)
	SetTmpParam_At(141, newv)
end
CLuaCopyPropertyFight.SetParamIgnoreAntiLight = function(newv)
	SetTmpParam_At(142, newv)
end
CLuaCopyPropertyFight.SetParamIgnoreAntiIllusion = function(newv)
	SetTmpParam_At(143, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceDizzy = function(newv)
	SetTmpParam_At(144, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceDizzy = function(newv)
	SetTmpParam_At(145, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhanceDizzy = function(newv)
	SetTmpParam_At(146, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceSleep = function(newv)
	SetTmpParam_At(147, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceSleep = function(newv)
	SetTmpParam_At(148, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhanceSleep = function(newv)
	SetTmpParam_At(149, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceChaos = function(newv)
	SetTmpParam_At(150, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceChaos = function(newv)
	SetTmpParam_At(151, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhanceChaos = function(newv)
	SetTmpParam_At(152, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceBind = function(newv)
	SetTmpParam_At(153, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceBind = function(newv)
	SetTmpParam_At(154, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhanceBind = function(newv)
	SetTmpParam_At(155, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceSilence = function(newv)
	SetTmpParam_At(156, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceSilence = function(newv)
	SetTmpParam_At(157, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhanceSilence = function(newv)
	SetTmpParam_At(158, newv)
end
CLuaCopyPropertyFight.SetParamAntiDizzy = function(newv)
	SetTmpParam_At(159, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiDizzy = function(newv)
	SetTmpParam_At(160, newv)
end
CLuaCopyPropertyFight.SetParamMulAntiDizzy = function(newv)
	SetTmpParam_At(161, newv)
end
CLuaCopyPropertyFight.SetParamAntiSleep = function(newv)
	SetTmpParam_At(162, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiSleep = function(newv)
	SetTmpParam_At(163, newv)
end
CLuaCopyPropertyFight.SetParamMulAntiSleep = function(newv)
	SetTmpParam_At(164, newv)
end
CLuaCopyPropertyFight.SetParamAntiChaos = function(newv)
	SetTmpParam_At(165, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiChaos = function(newv)
	SetTmpParam_At(166, newv)
end
CLuaCopyPropertyFight.SetParamMulAntiChaos = function(newv)
	SetTmpParam_At(167, newv)
end
CLuaCopyPropertyFight.SetParamAntiBind = function(newv)
	SetTmpParam_At(168, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiBind = function(newv)
	SetTmpParam_At(169, newv)
end
CLuaCopyPropertyFight.SetParamMulAntiBind = function(newv)
	SetTmpParam_At(170, newv)
end
CLuaCopyPropertyFight.SetParamAntiSilence = function(newv)
	SetTmpParam_At(171, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiSilence = function(newv)
	SetTmpParam_At(172, newv)
end
CLuaCopyPropertyFight.SetParamMulAntiSilence = function(newv)
	SetTmpParam_At(173, newv)
end
CLuaCopyPropertyFight.SetParamDizzyTimeChange = function(newv)
	SetTmpParam_At(174, newv)
end
CLuaCopyPropertyFight.SetParamSleepTimeChange = function(newv)
	SetTmpParam_At(175, newv)
end
CLuaCopyPropertyFight.SetParamChaosTimeChange = function(newv)
	SetTmpParam_At(176, newv)
end
CLuaCopyPropertyFight.SetParamBindTimeChange = function(newv)
	SetTmpParam_At(177, newv)
end
CLuaCopyPropertyFight.SetParamSilenceTimeChange = function(newv)
	SetTmpParam_At(178, newv)
end
CLuaCopyPropertyFight.SetParamBianhuTimeChange = function(newv)
	SetTmpParam_At(179, newv)
end
CLuaCopyPropertyFight.SetParamFreezeTimeChange = function(newv)
	SetTmpParam_At(180, newv)
end
CLuaCopyPropertyFight.SetParamPetrifyTimeChange = function(newv)
	SetTmpParam_At(181, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceHeal = function(newv)
	SetTmpParam_At(182, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceHeal = function(newv)
	SetTmpParam_At(183, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhanceHeal = function(newv)
	SetTmpParam_At(184, newv)
end
CLuaCopyPropertyFight.SetParamSpeed = function(newv)
	SetTmpParam_At(185, newv)
end
CLuaCopyPropertyFight.SetParamOriSpeed = function(newv)
	SetTmpParam_At(186, newv)
end
CLuaCopyPropertyFight.SetParamAdjSpeed = function(newv)
	SetTmpParam_At(187, newv)
end
CLuaCopyPropertyFight.SetParamMulSpeed = function(newv)
	SetTmpParam_At(188, newv)
end
CLuaCopyPropertyFight.SetParamMF = function(newv)
	SetTmpParam_At(189, newv)
end
CLuaCopyPropertyFight.SetParamAdjMF = function(newv)
	SetTmpParam_At(190, newv)
end
CLuaCopyPropertyFight.SetParamRange = function(newv)
	SetTmpParam_At(191, newv)
end
CLuaCopyPropertyFight.SetParamAdjRange = function(newv)
	SetTmpParam_At(192, newv)
end
CLuaCopyPropertyFight.SetParamHatePlus = function(newv)
	SetTmpParam_At(193, newv)
end
CLuaCopyPropertyFight.SetParamAdjHatePlus = function(newv)
	SetTmpParam_At(194, newv)
end
CLuaCopyPropertyFight.SetParamHateDecrease = function(newv)
	SetTmpParam_At(195, newv)
end
CLuaCopyPropertyFight.SetParamInvisible = function(newv)
	SetTmpParam_At(196, newv)
end
CLuaCopyPropertyFight.SetParamAdjInvisible = function(newv)
	SetTmpParam_At(197, newv)
end
CLuaCopyPropertyFight.SetParamTrueSight = function(newv)
	SetTmpParam_At(198, newv)
end
CLuaCopyPropertyFight.SetParamOriTrueSight = function(newv)
	SetTmpParam_At(199, newv)
end
CLuaCopyPropertyFight.SetParamAdjTrueSight = function(newv)
	SetTmpParam_At(200, newv)
end
CLuaCopyPropertyFight.SetParamEyeSight = function(newv)
	SetTmpParam_At(201, newv)
end
CLuaCopyPropertyFight.SetParamOriEyeSight = function(newv)
	SetTmpParam_At(202, newv)
end
CLuaCopyPropertyFight.SetParamAdjEyeSight = function(newv)
	SetTmpParam_At(203, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceTian = function(newv)
	SetTmpParam_At(204, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceTianMul = function(newv)
	SetTmpParam_At(205, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceEGui = function(newv)
	SetTmpParam_At(206, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceEGuiMul = function(newv)
	SetTmpParam_At(207, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceXiuLuo = function(newv)
	SetTmpParam_At(208, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceXiuLuoMul = function(newv)
	SetTmpParam_At(209, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceDiYu = function(newv)
	SetTmpParam_At(210, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceDiYuMul = function(newv)
	SetTmpParam_At(211, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceRen = function(newv)
	SetTmpParam_At(212, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceRenMul = function(newv)
	SetTmpParam_At(213, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceChuSheng = function(newv)
	SetTmpParam_At(214, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceChuShengMul = function(newv)
	SetTmpParam_At(215, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceBuilding = function(newv)
	SetTmpParam_At(216, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceBuildingMul = function(newv)
	SetTmpParam_At(217, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceBoss = function(newv)
	SetTmpParam_At(218, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceBossMul = function(newv)
	SetTmpParam_At(219, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceSheShou = function(newv)
	SetTmpParam_At(220, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceSheShouMul = function(newv)
	SetTmpParam_At(221, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceJiaShi = function(newv)
	SetTmpParam_At(222, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceJiaShiMul = function(newv)
	SetTmpParam_At(223, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceFangShi = function(newv)
	SetTmpParam_At(224, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceFangShiMul = function(newv)
	SetTmpParam_At(225, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceYiShi = function(newv)
	SetTmpParam_At(226, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceYiShiMul = function(newv)
	SetTmpParam_At(227, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceMeiZhe = function(newv)
	SetTmpParam_At(228, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceMeiZheMul = function(newv)
	SetTmpParam_At(229, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceYiRen = function(newv)
	SetTmpParam_At(230, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceYiRenMul = function(newv)
	SetTmpParam_At(231, newv)
end
CLuaCopyPropertyFight.SetParamIgnorepDef = function(newv)
	SetTmpParam_At(232, newv)
end
CLuaCopyPropertyFight.SetParamIgnoremDef = function(newv)
	SetTmpParam_At(233, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceZhaoHuan = function(newv)
	SetTmpParam_At(234, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceZhaoHuanMul = function(newv)
	SetTmpParam_At(235, newv)
end
CLuaCopyPropertyFight.SetParamDizzyProbability = function(newv)
	SetTmpParam_At(236, newv)
end
CLuaCopyPropertyFight.SetParamSleepProbability = function(newv)
	SetTmpParam_At(237, newv)
end
CLuaCopyPropertyFight.SetParamChaosProbability = function(newv)
	SetTmpParam_At(238, newv)
end
CLuaCopyPropertyFight.SetParamBindProbability = function(newv)
	SetTmpParam_At(239, newv)
end
CLuaCopyPropertyFight.SetParamSilenceProbability = function(newv)
	SetTmpParam_At(240, newv)
end
CLuaCopyPropertyFight.SetParamHpFullPow2 = function(newv)
	SetTmpParam_At(241, newv)
end
CLuaCopyPropertyFight.SetParamHpFullPow1 = function(newv)
	SetTmpParam_At(242, newv)
end
CLuaCopyPropertyFight.SetParamHpFullInit = function(newv)
	SetTmpParam_At(243, newv)
end
CLuaCopyPropertyFight.SetParamMpFullPow1 = function(newv)
	SetTmpParam_At(244, newv)
end
CLuaCopyPropertyFight.SetParamMpFullInit = function(newv)
	SetTmpParam_At(245, newv)
end
CLuaCopyPropertyFight.SetParamMpRecoverPow1 = function(newv)
	SetTmpParam_At(246, newv)
end
CLuaCopyPropertyFight.SetParamMpRecoverInit = function(newv)
	SetTmpParam_At(247, newv)
end
CLuaCopyPropertyFight.SetParamPAttMinPow2 = function(newv)
	SetTmpParam_At(248, newv)
end
CLuaCopyPropertyFight.SetParamPAttMinPow1 = function(newv)
	SetTmpParam_At(249, newv)
end
CLuaCopyPropertyFight.SetParamPAttMinInit = function(newv)
	SetTmpParam_At(250, newv)
end
CLuaCopyPropertyFight.SetParamPAttMaxPow2 = function(newv)
	SetTmpParam_At(251, newv)
end
CLuaCopyPropertyFight.SetParamPAttMaxPow1 = function(newv)
	SetTmpParam_At(252, newv)
end
CLuaCopyPropertyFight.SetParamPAttMaxInit = function(newv)
	SetTmpParam_At(253, newv)
end
CLuaCopyPropertyFight.SetParamPhitPow1 = function(newv)
	SetTmpParam_At(254, newv)
end
CLuaCopyPropertyFight.SetParamPHitInit = function(newv)
	SetTmpParam_At(255, newv)
end
CLuaCopyPropertyFight.SetParamPMissPow1 = function(newv)
	SetTmpParam_At(256, newv)
end
CLuaCopyPropertyFight.SetParamPMissInit = function(newv)
	SetTmpParam_At(257, newv)
end
CLuaCopyPropertyFight.SetParamPSpeedPow1 = function(newv)
	SetTmpParam_At(258, newv)
end
CLuaCopyPropertyFight.SetParamPSpeedInit = function(newv)
	SetTmpParam_At(259, newv)
end
CLuaCopyPropertyFight.SetParamPDefPow1 = function(newv)
	SetTmpParam_At(260, newv)
end
CLuaCopyPropertyFight.SetParamPDefInit = function(newv)
	SetTmpParam_At(261, newv)
end
CLuaCopyPropertyFight.SetParamPfatalPow1 = function(newv)
	SetTmpParam_At(262, newv)
end
CLuaCopyPropertyFight.SetParamPFatalInit = function(newv)
	SetTmpParam_At(263, newv)
end
CLuaCopyPropertyFight.SetParamMAttMinPow2 = function(newv)
	SetTmpParam_At(264, newv)
end
CLuaCopyPropertyFight.SetParamMAttMinPow1 = function(newv)
	SetTmpParam_At(265, newv)
end
CLuaCopyPropertyFight.SetParamMAttMinInit = function(newv)
	SetTmpParam_At(266, newv)
end
CLuaCopyPropertyFight.SetParamMAttMaxPow2 = function(newv)
	SetTmpParam_At(267, newv)
end
CLuaCopyPropertyFight.SetParamMAttMaxPow1 = function(newv)
	SetTmpParam_At(268, newv)
end
CLuaCopyPropertyFight.SetParamMAttMaxInit = function(newv)
	SetTmpParam_At(269, newv)
end
CLuaCopyPropertyFight.SetParamMSpeedPow1 = function(newv)
	SetTmpParam_At(270, newv)
end
CLuaCopyPropertyFight.SetParamMSpeedInit = function(newv)
	SetTmpParam_At(271, newv)
end
CLuaCopyPropertyFight.SetParamMDefPow1 = function(newv)
	SetTmpParam_At(272, newv)
end
CLuaCopyPropertyFight.SetParamMDefInit = function(newv)
	SetTmpParam_At(273, newv)
end
CLuaCopyPropertyFight.SetParamMHitPow1 = function(newv)
	SetTmpParam_At(274, newv)
end
CLuaCopyPropertyFight.SetParamMHitInit = function(newv)
	SetTmpParam_At(275, newv)
end
CLuaCopyPropertyFight.SetParamMMissPow1 = function(newv)
	SetTmpParam_At(276, newv)
end
CLuaCopyPropertyFight.SetParamMMissInit = function(newv)
	SetTmpParam_At(277, newv)
end
CLuaCopyPropertyFight.SetParamMFatalPow1 = function(newv)
	SetTmpParam_At(278, newv)
end
CLuaCopyPropertyFight.SetParamMFatalInit = function(newv)
	SetTmpParam_At(279, newv)
end
CLuaCopyPropertyFight.SetParamBlockDamagePow1 = function(newv)
	SetTmpParam_At(280, newv)
end
CLuaCopyPropertyFight.SetParamBlockDamageInit = function(newv)
	SetTmpParam_At(281, newv)
end
CLuaCopyPropertyFight.SetParamRunSpeed = function(newv)
	SetTmpParam_At(282, newv)
end
CLuaCopyPropertyFight.SetParamHpRecoverPow1 = function(newv)
	SetTmpParam_At(283, newv)
end
CLuaCopyPropertyFight.SetParamHpRecoverInit = function(newv)
	SetTmpParam_At(284, newv)
end
CLuaCopyPropertyFight.SetParamAntiBlockDamagePow1 = function(newv)
	SetTmpParam_At(285, newv)
end
CLuaCopyPropertyFight.SetParamAntiBlockDamageInit = function(newv)
	SetTmpParam_At(286, newv)
end
CLuaCopyPropertyFight.SetParamBBMax = function(newv)
	SetTmpParam_At(287, newv)
end
CLuaCopyPropertyFight.SetParamAdjBBMax = function(newv)
	SetTmpParam_At(288, newv)
end
CLuaCopyPropertyFight.SetParamAntiBreak = function(newv)
	SetTmpParam_At(289, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiBreak = function(newv)
	SetTmpParam_At(290, newv)
end
CLuaCopyPropertyFight.SetParamAntiPushPull = function(newv)
	SetTmpParam_At(291, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiPushPull = function(newv)
	SetTmpParam_At(292, newv)
end
CLuaCopyPropertyFight.SetParamAntiPetrify = function(newv)
	SetTmpParam_At(293, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiPetrify = function(newv)
	SetTmpParam_At(294, newv)
end
CLuaCopyPropertyFight.SetParamMulAntiPetrify = function(newv)
	SetTmpParam_At(295, newv)
end
CLuaCopyPropertyFight.SetParamAntiTie = function(newv)
	SetTmpParam_At(296, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiTie = function(newv)
	SetTmpParam_At(297, newv)
end
CLuaCopyPropertyFight.SetParamMulAntiTie = function(newv)
	SetTmpParam_At(298, newv)
end
CLuaCopyPropertyFight.SetParamEnhancePetrify = function(newv)
	SetTmpParam_At(299, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhancePetrify = function(newv)
	SetTmpParam_At(300, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhancePetrify = function(newv)
	SetTmpParam_At(301, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceTie = function(newv)
	SetTmpParam_At(302, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceTie = function(newv)
	SetTmpParam_At(303, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhanceTie = function(newv)
	SetTmpParam_At(304, newv)
end
CLuaCopyPropertyFight.SetParamTieProbability = function(newv)
	SetTmpParam_At(305, newv)
end
CLuaCopyPropertyFight.SetParamStrZizhi = function(newv)
	SetTmpParam_At(306, newv)
end
CLuaCopyPropertyFight.SetParamCorZizhi = function(newv)
	SetTmpParam_At(307, newv)
end
CLuaCopyPropertyFight.SetParamStaZizhi = function(newv)
	SetTmpParam_At(308, newv)
end
CLuaCopyPropertyFight.SetParamAgiZizhi = function(newv)
	SetTmpParam_At(309, newv)
end
CLuaCopyPropertyFight.SetParamIntZizhi = function(newv)
	SetTmpParam_At(310, newv)
end
CLuaCopyPropertyFight.SetParamInitStrZizhi = function(newv)
	SetTmpParam_At(311, newv)
end
CLuaCopyPropertyFight.SetParamInitCorZizhi = function(newv)
	SetTmpParam_At(312, newv)
end
CLuaCopyPropertyFight.SetParamInitStaZizhi = function(newv)
	SetTmpParam_At(313, newv)
end
CLuaCopyPropertyFight.SetParamInitAgiZizhi = function(newv)
	SetTmpParam_At(314, newv)
end
CLuaCopyPropertyFight.SetParamInitIntZizhi = function(newv)
	SetTmpParam_At(315, newv)
end
CLuaCopyPropertyFight.SetParamWuxing = function(newv)
	SetTmpParam_At(316, newv)
end
CLuaCopyPropertyFight.SetParamXiuwei = function(newv)
	SetTmpParam_At(317, newv)
end
CLuaCopyPropertyFight.SetParamSkillNum = function(newv)
	SetTmpParam_At(318, newv)
end
CLuaCopyPropertyFight.SetParamPType = function(newv)
	SetTmpParam_At(319, newv)
end
CLuaCopyPropertyFight.SetParamMType = function(newv)
	SetTmpParam_At(320, newv)
end
CLuaCopyPropertyFight.SetParamPHType = function(newv)
	SetTmpParam_At(321, newv)
end
CLuaCopyPropertyFight.SetParamGrowFactor = function(newv)
	SetTmpParam_At(322, newv)
end
CLuaCopyPropertyFight.SetParamWuxingImprove = function(newv)
	SetTmpParam_At(323, newv)
end
CLuaCopyPropertyFight.SetParamXiuweiImprove = function(newv)
	SetTmpParam_At(324, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceBeHeal = function(newv)
	SetTmpParam_At(325, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceBeHeal = function(newv)
	SetTmpParam_At(326, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhanceBeHeal = function(newv)
	SetTmpParam_At(327, newv)
end
CLuaCopyPropertyFight.SetParamLookUpCor = function(newv)
	SetTmpParam_At(328, newv)
end
CLuaCopyPropertyFight.SetParamLookUpSta = function(newv)
	SetTmpParam_At(329, newv)
end
CLuaCopyPropertyFight.SetParamLookUpStr = function(newv)
	SetTmpParam_At(330, newv)
end
CLuaCopyPropertyFight.SetParamLookUpInt = function(newv)
	SetTmpParam_At(331, newv)
end
CLuaCopyPropertyFight.SetParamLookUpAgi = function(newv)
	SetTmpParam_At(332, newv)
end
CLuaCopyPropertyFight.SetParamLookUpHpFull = function(newv)
	SetTmpParam_At(333, newv)
end
CLuaCopyPropertyFight.SetParamLookUpMpFull = function(newv)
	SetTmpParam_At(334, newv)
end
CLuaCopyPropertyFight.SetParamLookUppAttMin = function(newv)
	SetTmpParam_At(335, newv)
end
CLuaCopyPropertyFight.SetParamLookUppAttMax = function(newv)
	SetTmpParam_At(336, newv)
end
CLuaCopyPropertyFight.SetParamLookUppHit = function(newv)
	SetTmpParam_At(337, newv)
end
CLuaCopyPropertyFight.SetParamLookUppMiss = function(newv)
	SetTmpParam_At(338, newv)
end
CLuaCopyPropertyFight.SetParamLookUppDef = function(newv)
	SetTmpParam_At(339, newv)
end
CLuaCopyPropertyFight.SetParamLookUpmAttMin = function(newv)
	SetTmpParam_At(340, newv)
end
CLuaCopyPropertyFight.SetParamLookUpmAttMax = function(newv)
	SetTmpParam_At(341, newv)
end
CLuaCopyPropertyFight.SetParamLookUpmHit = function(newv)
	SetTmpParam_At(342, newv)
end
CLuaCopyPropertyFight.SetParamLookUpmMiss = function(newv)
	SetTmpParam_At(343, newv)
end
CLuaCopyPropertyFight.SetParamLookUpmDef = function(newv)
	SetTmpParam_At(344, newv)
end
CLuaCopyPropertyFight.SetParamAdjHpFull2 = function(newv)
	SetTmpParam_At(345, newv)
end
CLuaCopyPropertyFight.SetParamAdjpAttMin2 = function(newv)
	SetTmpParam_At(346, newv)
end
CLuaCopyPropertyFight.SetParamAdjpAttMax2 = function(newv)
	SetTmpParam_At(347, newv)
end
CLuaCopyPropertyFight.SetParamAdjmAttMin2 = function(newv)
	SetTmpParam_At(348, newv)
end
CLuaCopyPropertyFight.SetParamAdjmAttMax2 = function(newv)
	SetTmpParam_At(349, newv)
end
CLuaCopyPropertyFight.SetParamLingShouType = function(newv)
	SetTmpParam_At(350, newv)
end
CLuaCopyPropertyFight.SetParamMHType = function(newv)
	SetTmpParam_At(351, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceLingShou = function(newv)
	SetTmpParam_At(352, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceLingShouMul = function(newv)
	SetTmpParam_At(353, newv)
end
CLuaCopyPropertyFight.RefreshParamAntiAoe = function()
	local AdjAntiAoe = GetTmpParam_At(355)

	local newv = AdjAntiAoe
	SetTmpParam_At(354, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiAoe = function(newv)
	SetTmpParam_At(355, newv)

	CLuaCopyPropertyFight.RefreshParamAntiAoe()
end
CLuaCopyPropertyFight.SetParamAntiBlind = function(newv)
	SetTmpParam_At(356, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiBlind = function(newv)
	SetTmpParam_At(357, newv)
end
CLuaCopyPropertyFight.SetParamMulAntiBlind = function(newv)
	SetTmpParam_At(358, newv)
end
CLuaCopyPropertyFight.RefreshParamAntiDecelerate = function()
	local AdjAntiDecelerate = GetTmpParam_At(360)
	local MulAntiDecelerate = GetTmpParam_At(361)

	local newv = AdjAntiDecelerate*(1+MulAntiDecelerate)
	SetTmpParam_At(359, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiDecelerate = function(newv)
	SetTmpParam_At(360, newv)

	CLuaCopyPropertyFight.RefreshParamAntiDecelerate()
end
CLuaCopyPropertyFight.SetParamMulAntiDecelerate = function(newv)
	SetTmpParam_At(361, newv)

	CLuaCopyPropertyFight.RefreshParamAntiDecelerate()
end
CLuaCopyPropertyFight.SetParamMinGrade = function(newv)
	SetTmpParam_At(362, newv)
end
CLuaCopyPropertyFight.SetParamCharacterCor = function(newv)
	SetTmpParam_At(363, newv)
end
CLuaCopyPropertyFight.SetParamCharacterSta = function(newv)
	SetTmpParam_At(364, newv)
end
CLuaCopyPropertyFight.SetParamCharacterStr = function(newv)
	SetTmpParam_At(365, newv)
end
CLuaCopyPropertyFight.SetParamCharacterInt = function(newv)
	SetTmpParam_At(366, newv)
end
CLuaCopyPropertyFight.SetParamCharacterAgi = function(newv)
	SetTmpParam_At(367, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceDaoKe = function(newv)
	SetTmpParam_At(368, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceDaoKeMul = function(newv)
	SetTmpParam_At(369, newv)
end
CLuaCopyPropertyFight.SetParamZuoQiSpeed = function(newv)
	SetTmpParam_At(370, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceBianHu = function(newv)
	SetTmpParam_At(371, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceBianHu = function(newv)
	SetTmpParam_At(372, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhanceBianHu = function(newv)
	SetTmpParam_At(373, newv)
end
CLuaCopyPropertyFight.SetParamAntiBianHu = function(newv)
	SetTmpParam_At(374, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiBianHu = function(newv)
	SetTmpParam_At(375, newv)
end
CLuaCopyPropertyFight.SetParamMulAntiBianHu = function(newv)
	SetTmpParam_At(376, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceXiaKe = function(newv)
	SetTmpParam_At(377, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceXiaKeMul = function(newv)
	SetTmpParam_At(378, newv)
end
CLuaCopyPropertyFight.SetParamTieTimeChange = function(newv)
	SetTmpParam_At(379, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceYanShi = function(newv)
	SetTmpParam_At(380, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceYanShiMul = function(newv)
	SetTmpParam_At(381, newv)
end
CLuaCopyPropertyFight.SetParamPAType = function(newv)
	SetTmpParam_At(382, newv)
end
CLuaCopyPropertyFight.SetParamMAType = function(newv)
	SetTmpParam_At(383, newv)
end
CLuaCopyPropertyFight.SetParamlifetimeNoReduceRate = function(newv)
	SetTmpParam_At(384, newv)
end
CLuaCopyPropertyFight.SetParamAddLSHpDurgImprove = function(newv)
	SetTmpParam_At(385, newv)
end
CLuaCopyPropertyFight.SetParamAddHpDurgImprove = function(newv)
	SetTmpParam_At(386, newv)
end
CLuaCopyPropertyFight.SetParamAddMpDurgImprove = function(newv)
	SetTmpParam_At(387, newv)
end
CLuaCopyPropertyFight.SetParamFireHurtReduce = function(newv)
	SetTmpParam_At(388, newv)
end
CLuaCopyPropertyFight.SetParamThunderHurtReduce = function(newv)
	SetTmpParam_At(389, newv)
end
CLuaCopyPropertyFight.SetParamIceHurtReduce = function(newv)
	SetTmpParam_At(390, newv)
end
CLuaCopyPropertyFight.SetParamPoisonHurtReduce = function(newv)
	SetTmpParam_At(391, newv)
end
CLuaCopyPropertyFight.SetParamWindHurtReduce = function(newv)
	SetTmpParam_At(392, newv)
end
CLuaCopyPropertyFight.SetParamLightHurtReduce = function(newv)
	SetTmpParam_At(393, newv)
end
CLuaCopyPropertyFight.SetParamIllusionHurtReduce = function(newv)
	SetTmpParam_At(394, newv)
end
CLuaCopyPropertyFight.SetParamFakepDef = function(newv)
	SetTmpParam_At(395, newv)
end
CLuaCopyPropertyFight.SetParamFakemDef = function(newv)
	SetTmpParam_At(396, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceWater = function(newv)
	SetTmpParam_At(397, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceWater = function(newv)
	SetTmpParam_At(398, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhanceWater = function(newv)
	SetTmpParam_At(399, newv)
end
CLuaCopyPropertyFight.SetParamAntiWater = function(newv)
	SetTmpParam_At(400, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiWater = function(newv)
	SetTmpParam_At(401, newv)
end
CLuaCopyPropertyFight.SetParamMulAntiWater = function(newv)
	SetTmpParam_At(402, newv)
end
CLuaCopyPropertyFight.SetParamWaterHurtReduce = function(newv)
	SetTmpParam_At(403, newv)
end
CLuaCopyPropertyFight.SetParamIgnoreAntiWater = function(newv)
	SetTmpParam_At(404, newv)
end
CLuaCopyPropertyFight.SetParamAntiFreeze = function(newv)
	SetTmpParam_At(405, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiFreeze = function(newv)
	SetTmpParam_At(406, newv)
end
CLuaCopyPropertyFight.SetParamMulAntiFreeze = function(newv)
	SetTmpParam_At(407, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceFreeze = function(newv)
	SetTmpParam_At(408, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceFreeze = function(newv)
	SetTmpParam_At(409, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhanceFreeze = function(newv)
	SetTmpParam_At(410, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceHuaHun = function(newv)
	SetTmpParam_At(411, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceHuaHunMul = function(newv)
	SetTmpParam_At(412, newv)
end
CLuaCopyPropertyFight.SetParamTrueSightProb = function(newv)
	SetTmpParam_At(413, newv)
end
CLuaCopyPropertyFight.SetParamLSBBGrade = function(newv)
	SetTmpParam_At(414, newv)
end
CLuaCopyPropertyFight.SetParamLSBBQuality = function(newv)
	SetTmpParam_At(415, newv)
end
CLuaCopyPropertyFight.SetParamLSBBAdjHp = function(newv)
	SetTmpParam_At(416, newv)
end
CLuaCopyPropertyFight.SetParamAdjIgnoreAntiFire = function(newv)
	SetTmpParam_At(417, newv)
end
CLuaCopyPropertyFight.SetParamAdjIgnoreAntiThunder = function(newv)
	SetTmpParam_At(418, newv)
end
CLuaCopyPropertyFight.SetParamAdjIgnoreAntiIce = function(newv)
	SetTmpParam_At(419, newv)
end
CLuaCopyPropertyFight.SetParamAdjIgnoreAntiPoison = function(newv)
	SetTmpParam_At(420, newv)
end
CLuaCopyPropertyFight.SetParamAdjIgnoreAntiWind = function(newv)
	SetTmpParam_At(421, newv)
end
CLuaCopyPropertyFight.SetParamAdjIgnoreAntiLight = function(newv)
	SetTmpParam_At(422, newv)
end
CLuaCopyPropertyFight.SetParamAdjIgnoreAntiIllusion = function(newv)
	SetTmpParam_At(423, newv)
end
CLuaCopyPropertyFight.SetParamIgnoreAntiFireLSMul = function(newv)
	SetTmpParam_At(424, newv)
end
CLuaCopyPropertyFight.SetParamIgnoreAntiThunderLSMul = function(newv)
	SetTmpParam_At(425, newv)
end
CLuaCopyPropertyFight.SetParamIgnoreAntiIceLSMul = function(newv)
	SetTmpParam_At(426, newv)
end
CLuaCopyPropertyFight.SetParamIgnoreAntiPoisonLSMul = function(newv)
	SetTmpParam_At(427, newv)
end
CLuaCopyPropertyFight.SetParamIgnoreAntiWindLSMul = function(newv)
	SetTmpParam_At(428, newv)
end
CLuaCopyPropertyFight.SetParamIgnoreAntiLightLSMul = function(newv)
	SetTmpParam_At(429, newv)
end
CLuaCopyPropertyFight.SetParamIgnoreAntiIllusionLSMul = function(newv)
	SetTmpParam_At(430, newv)
end
CLuaCopyPropertyFight.SetParamAdjIgnoreAntiWater = function(newv)
	SetTmpParam_At(431, newv)
end
CLuaCopyPropertyFight.SetParamIgnoreAntiWaterLSMul = function(newv)
	SetTmpParam_At(432, newv)
end
CLuaCopyPropertyFight.SetParamMulSpeed2 = function(newv)
	SetTmpParam_At(433, newv)
end
CLuaCopyPropertyFight.SetParamMulConsumeMp = function(newv)
	SetTmpParam_At(434, newv)
end
CLuaCopyPropertyFight.SetParamAdjAgi2 = function(newv)
	SetTmpParam_At(435, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiBianHu2 = function(newv)
	SetTmpParam_At(436, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiBind2 = function(newv)
	SetTmpParam_At(437, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiBlind2 = function(newv)
	SetTmpParam_At(438, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiBlock2 = function(newv)
	SetTmpParam_At(439, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiBlockDamage2 = function(newv)
	SetTmpParam_At(440, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiChaos2 = function(newv)
	SetTmpParam_At(441, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiDecelerate2 = function(newv)
	SetTmpParam_At(442, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiDizzy2 = function(newv)
	SetTmpParam_At(443, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiFire2 = function(newv)
	SetTmpParam_At(444, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiFreeze2 = function(newv)
	SetTmpParam_At(445, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiIce2 = function(newv)
	SetTmpParam_At(446, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiIllusion2 = function(newv)
	SetTmpParam_At(447, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiLight2 = function(newv)
	SetTmpParam_At(448, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntimFatal2 = function(newv)
	SetTmpParam_At(449, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntimFatalDamage2 = function(newv)
	SetTmpParam_At(450, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiPetrify2 = function(newv)
	SetTmpParam_At(451, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntipFatal2 = function(newv)
	SetTmpParam_At(452, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntipFatalDamage2 = function(newv)
	SetTmpParam_At(453, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiPoison2 = function(newv)
	SetTmpParam_At(454, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiSilence2 = function(newv)
	SetTmpParam_At(455, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiSleep2 = function(newv)
	SetTmpParam_At(456, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiThunder2 = function(newv)
	SetTmpParam_At(457, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiTie2 = function(newv)
	SetTmpParam_At(458, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiWater2 = function(newv)
	SetTmpParam_At(459, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiWind2 = function(newv)
	SetTmpParam_At(460, newv)
end
CLuaCopyPropertyFight.SetParamAdjBlock2 = function(newv)
	SetTmpParam_At(461, newv)
end
CLuaCopyPropertyFight.SetParamAdjBlockDamage2 = function(newv)
	SetTmpParam_At(462, newv)
end
CLuaCopyPropertyFight.SetParamAdjCor2 = function(newv)
	SetTmpParam_At(463, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceBianHu2 = function(newv)
	SetTmpParam_At(464, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceBind2 = function(newv)
	SetTmpParam_At(465, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceChaos2 = function(newv)
	SetTmpParam_At(466, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceDizzy2 = function(newv)
	SetTmpParam_At(467, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceFire2 = function(newv)
	SetTmpParam_At(468, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceFreeze2 = function(newv)
	SetTmpParam_At(469, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceIce2 = function(newv)
	SetTmpParam_At(470, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceIllusion2 = function(newv)
	SetTmpParam_At(471, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceLight2 = function(newv)
	SetTmpParam_At(472, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhancePetrify2 = function(newv)
	SetTmpParam_At(473, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhancePoison2 = function(newv)
	SetTmpParam_At(474, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceSilence2 = function(newv)
	SetTmpParam_At(475, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceSleep2 = function(newv)
	SetTmpParam_At(476, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceThunder2 = function(newv)
	SetTmpParam_At(477, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceTie2 = function(newv)
	SetTmpParam_At(478, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceWater2 = function(newv)
	SetTmpParam_At(479, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceWind2 = function(newv)
	SetTmpParam_At(480, newv)
end
CLuaCopyPropertyFight.SetParamAdjIgnoreAntiFire2 = function(newv)
	SetTmpParam_At(481, newv)
end
CLuaCopyPropertyFight.SetParamAdjIgnoreAntiIce2 = function(newv)
	SetTmpParam_At(482, newv)
end
CLuaCopyPropertyFight.SetParamAdjIgnoreAntiIllusion2 = function(newv)
	SetTmpParam_At(483, newv)
end
CLuaCopyPropertyFight.SetParamAdjIgnoreAntiLight2 = function(newv)
	SetTmpParam_At(484, newv)
end
CLuaCopyPropertyFight.SetParamAdjIgnoreAntiPoison2 = function(newv)
	SetTmpParam_At(485, newv)
end
CLuaCopyPropertyFight.SetParamAdjIgnoreAntiThunder2 = function(newv)
	SetTmpParam_At(486, newv)
end
CLuaCopyPropertyFight.SetParamAdjIgnoreAntiWater2 = function(newv)
	SetTmpParam_At(487, newv)
end
CLuaCopyPropertyFight.SetParamAdjIgnoreAntiWind2 = function(newv)
	SetTmpParam_At(488, newv)
end
CLuaCopyPropertyFight.SetParamAdjInt2 = function(newv)
	SetTmpParam_At(489, newv)
end
CLuaCopyPropertyFight.SetParamAdjmDef2 = function(newv)
	SetTmpParam_At(490, newv)
end
CLuaCopyPropertyFight.SetParamAdjmFatal2 = function(newv)
	SetTmpParam_At(491, newv)
end
CLuaCopyPropertyFight.SetParamAdjmFatalDamage2 = function(newv)
	SetTmpParam_At(492, newv)
end
CLuaCopyPropertyFight.SetParamAdjmHit2 = function(newv)
	SetTmpParam_At(493, newv)
end
CLuaCopyPropertyFight.SetParamAdjmHurt2 = function(newv)
	SetTmpParam_At(494, newv)
end
CLuaCopyPropertyFight.SetParamAdjmMiss2 = function(newv)
	SetTmpParam_At(495, newv)
end
CLuaCopyPropertyFight.SetParamAdjmSpeed2 = function(newv)
	SetTmpParam_At(496, newv)
end
CLuaCopyPropertyFight.SetParamAdjpDef2 = function(newv)
	SetTmpParam_At(497, newv)
end
CLuaCopyPropertyFight.SetParamAdjpFatal2 = function(newv)
	SetTmpParam_At(498, newv)
end
CLuaCopyPropertyFight.SetParamAdjpFatalDamage2 = function(newv)
	SetTmpParam_At(499, newv)
end
CLuaCopyPropertyFight.SetParamAdjpHit2 = function(newv)
	SetTmpParam_At(500, newv)
end
CLuaCopyPropertyFight.SetParamAdjpHurt2 = function(newv)
	SetTmpParam_At(501, newv)
end
CLuaCopyPropertyFight.SetParamAdjpMiss2 = function(newv)
	SetTmpParam_At(502, newv)
end
CLuaCopyPropertyFight.SetParamAdjpSpeed2 = function(newv)
	SetTmpParam_At(503, newv)
end
CLuaCopyPropertyFight.SetParamAdjStr2 = function(newv)
	SetTmpParam_At(504, newv)
end
CLuaCopyPropertyFight.SetParamLSpAttMin = function(newv)
	SetTmpParam_At(505, newv)
end
CLuaCopyPropertyFight.SetParamLSpAttMax = function(newv)
	SetTmpParam_At(506, newv)
end
CLuaCopyPropertyFight.SetParamLSmAttMin = function(newv)
	SetTmpParam_At(507, newv)
end
CLuaCopyPropertyFight.SetParamLSmAttMax = function(newv)
	SetTmpParam_At(508, newv)
end
CLuaCopyPropertyFight.SetParamLSpFatal = function(newv)
	SetTmpParam_At(509, newv)
end
CLuaCopyPropertyFight.SetParamLSpFatalDamage = function(newv)
	SetTmpParam_At(510, newv)
end
CLuaCopyPropertyFight.SetParamLSmFatal = function(newv)
	SetTmpParam_At(511, newv)
end
CLuaCopyPropertyFight.SetParamLSmFatalDamage = function(newv)
	SetTmpParam_At(512, newv)
end
CLuaCopyPropertyFight.SetParamLSAntiBlock = function(newv)
	SetTmpParam_At(513, newv)
end
CLuaCopyPropertyFight.SetParamLSAntiBlockDamage = function(newv)
	SetTmpParam_At(514, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiFire = function(newv)
	SetTmpParam_At(515, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiThunder = function(newv)
	SetTmpParam_At(516, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiIce = function(newv)
	SetTmpParam_At(517, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiPoison = function(newv)
	SetTmpParam_At(518, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiWind = function(newv)
	SetTmpParam_At(519, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiLight = function(newv)
	SetTmpParam_At(520, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiIllusion = function(newv)
	SetTmpParam_At(521, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiWater = function(newv)
	SetTmpParam_At(522, newv)
end
CLuaCopyPropertyFight.SetParamLSpAttMin_adj = function(newv)
	SetTmpParam_At(523, newv)
end
CLuaCopyPropertyFight.SetParamLSpAttMax_adj = function(newv)
	SetTmpParam_At(524, newv)
end
CLuaCopyPropertyFight.SetParamLSmAttMin_adj = function(newv)
	SetTmpParam_At(525, newv)
end
CLuaCopyPropertyFight.SetParamLSmAttMax_adj = function(newv)
	SetTmpParam_At(526, newv)
end
CLuaCopyPropertyFight.SetParamLSpFatal_adj = function(newv)
	SetTmpParam_At(527, newv)
end
CLuaCopyPropertyFight.SetParamLSpFatalDamage_adj = function(newv)
	SetTmpParam_At(528, newv)
end
CLuaCopyPropertyFight.SetParamLSmFatal_adj = function(newv)
	SetTmpParam_At(529, newv)
end
CLuaCopyPropertyFight.SetParamLSmFatalDamage_adj = function(newv)
	SetTmpParam_At(530, newv)
end
CLuaCopyPropertyFight.SetParamLSAntiBlock_adj = function(newv)
	SetTmpParam_At(531, newv)
end
CLuaCopyPropertyFight.SetParamLSAntiBlockDamage_adj = function(newv)
	SetTmpParam_At(532, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiFire_adj = function(newv)
	SetTmpParam_At(533, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiThunder_adj = function(newv)
	SetTmpParam_At(534, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiIce_adj = function(newv)
	SetTmpParam_At(535, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiPoison_adj = function(newv)
	SetTmpParam_At(536, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiWind_adj = function(newv)
	SetTmpParam_At(537, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiLight_adj = function(newv)
	SetTmpParam_At(538, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiIllusion_adj = function(newv)
	SetTmpParam_At(539, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiWater_adj = function(newv)
	SetTmpParam_At(540, newv)
end
CLuaCopyPropertyFight.SetParamLSpAttMin_jc = function(newv)
	SetTmpParam_At(541, newv)
end
CLuaCopyPropertyFight.SetParamLSpAttMax_jc = function(newv)
	SetTmpParam_At(542, newv)
end
CLuaCopyPropertyFight.SetParamLSmAttMin_jc = function(newv)
	SetTmpParam_At(543, newv)
end
CLuaCopyPropertyFight.SetParamLSmAttMax_jc = function(newv)
	SetTmpParam_At(544, newv)
end
CLuaCopyPropertyFight.SetParamLSpFatal_jc = function(newv)
	SetTmpParam_At(545, newv)
end
CLuaCopyPropertyFight.SetParamLSpFatalDamage_jc = function(newv)
	SetTmpParam_At(546, newv)
end
CLuaCopyPropertyFight.SetParamLSmFatal_jc = function(newv)
	SetTmpParam_At(547, newv)
end
CLuaCopyPropertyFight.SetParamLSmFatalDamage_jc = function(newv)
	SetTmpParam_At(548, newv)
end
CLuaCopyPropertyFight.SetParamLSAntiBlock_jc = function(newv)
	SetTmpParam_At(549, newv)
end
CLuaCopyPropertyFight.SetParamLSAntiBlockDamage_jc = function(newv)
	SetTmpParam_At(550, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiFire_jc = function(newv)
	SetTmpParam_At(551, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiThunder_jc = function(newv)
	SetTmpParam_At(552, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiIce_jc = function(newv)
	SetTmpParam_At(553, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiPoison_jc = function(newv)
	SetTmpParam_At(554, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiWind_jc = function(newv)
	SetTmpParam_At(555, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiLight_jc = function(newv)
	SetTmpParam_At(556, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiIllusion_jc = function(newv)
	SetTmpParam_At(557, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiWater_jc = function(newv)
	SetTmpParam_At(558, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiFire_mul = function(newv)
	SetTmpParam_At(559, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiThunder_mul = function(newv)
	SetTmpParam_At(560, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiIce_mul = function(newv)
	SetTmpParam_At(561, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiPoison_mul = function(newv)
	SetTmpParam_At(562, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiWind_mul = function(newv)
	SetTmpParam_At(563, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiLight_mul = function(newv)
	SetTmpParam_At(564, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiIllusion_mul = function(newv)
	SetTmpParam_At(565, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiWater_mul = function(newv)
	SetTmpParam_At(566, newv)
end
CLuaCopyPropertyFight.SetParamJieBan_Child_QiChangColor = function(newv)
	SetTmpParam_At(567, newv)
end
CLuaCopyPropertyFight.SetParamJieBan_LingShou_Ratio = function(newv)
	SetTmpParam_At(568, newv)
end
CLuaCopyPropertyFight.SetParamJieBan_LingShou_QinMi = function(newv)
	SetTmpParam_At(569, newv)
end
CLuaCopyPropertyFight.SetParamJieBan_LingShou_CorZizhi = function(newv)
	SetTmpParam_At(570, newv)
end
CLuaCopyPropertyFight.SetParamJieBan_LingShou_StaZizhi = function(newv)
	SetTmpParam_At(571, newv)
end
CLuaCopyPropertyFight.SetParamJieBan_LingShou_StrZizhi = function(newv)
	SetTmpParam_At(572, newv)
end
CLuaCopyPropertyFight.SetParamJieBan_LingShou_IntZizhi = function(newv)
	SetTmpParam_At(573, newv)
end
CLuaCopyPropertyFight.SetParamJieBan_LingShou_AgiZizhi = function(newv)
	SetTmpParam_At(574, newv)
end
CLuaCopyPropertyFight.SetParamBuff_DisplayValue1 = function(newv)
	SetTmpParam_At(575, newv)
end
CLuaCopyPropertyFight.SetParamBuff_DisplayValue2 = function(newv)
	SetTmpParam_At(576, newv)
end
CLuaCopyPropertyFight.SetParamBuff_DisplayValue3 = function(newv)
	SetTmpParam_At(577, newv)
end
CLuaCopyPropertyFight.SetParamBuff_DisplayValue4 = function(newv)
	SetTmpParam_At(578, newv)
end
CLuaCopyPropertyFight.SetParamBuff_DisplayValue5 = function(newv)
	SetTmpParam_At(579, newv)
end
CLuaCopyPropertyFight.SetParamLSNature = function(newv)
	SetTmpParam_At(580, newv)
end
CLuaCopyPropertyFight.SetParamPrayMulti = function(newv)
	SetTmpParam_At(581, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceYingLing = function(newv)
	SetTmpParam_At(582, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceYingLingMul = function(newv)
	SetTmpParam_At(583, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceFlag = function(newv)
	SetTmpParam_At(584, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceFlagMul = function(newv)
	SetTmpParam_At(585, newv)
end
CLuaCopyPropertyFight.SetParamObjectType = function(newv)
	SetTmpParam_At(586, newv)
end
CLuaCopyPropertyFight.SetParamMonsterType = function(newv)
	SetTmpParam_At(587, newv)
end
CLuaCopyPropertyFight.SetParamFightPropType = function(newv)
	SetTmpParam_At(588, newv)
end
CLuaCopyPropertyFight.SetParamPlayHpFull = function(newv)
	SetTmpParam_At(589, newv)
end
CLuaCopyPropertyFight.SetParamPlayHp = function(newv)
	SetTmpParam_At(590, newv)
end
CLuaCopyPropertyFight.SetParamPlayAtt = function(newv)
	SetTmpParam_At(591, newv)
end
CLuaCopyPropertyFight.SetParamPlayDef = function(newv)
	SetTmpParam_At(592, newv)
end
CLuaCopyPropertyFight.SetParamPlayHit = function(newv)
	SetTmpParam_At(593, newv)
end
CLuaCopyPropertyFight.SetParamPlayMiss = function(newv)
	SetTmpParam_At(594, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceDieKe = function(newv)
	SetTmpParam_At(595, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceDieKeMul = function(newv)
	SetTmpParam_At(596, newv)
end
CLuaCopyPropertyFight.SetParamDotRemain = function(newv)
	SetTmpParam_At(597, newv)
end
CLuaCopyPropertyFight.SetParamIsConfused = function(newv)
	SetTmpParam_At(598, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceSheShouKefu = function(newv)
	SetTmpParam_At(599, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceJiaShiKefu = function(newv)
	SetTmpParam_At(600, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceDaoKeKefu = function(newv)
	SetTmpParam_At(601, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceXiaKeKefu = function(newv)
	SetTmpParam_At(602, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceFangShiKefu = function(newv)
	SetTmpParam_At(603, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceYiShiKefu = function(newv)
	SetTmpParam_At(604, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceMeiZheKefu = function(newv)
	SetTmpParam_At(605, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceYiRenKefu = function(newv)
	SetTmpParam_At(606, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceYanShiKefu = function(newv)
	SetTmpParam_At(607, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceHuaHunKefu = function(newv)
	SetTmpParam_At(608, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceYingLingKefu = function(newv)
	SetTmpParam_At(609, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceDieKeKefu = function(newv)
	SetTmpParam_At(610, newv)
end
CLuaCopyPropertyFight.SetParamAdjustPermanentCor = function(newv)
	SetTmpParam_At(611, newv)
end
CLuaCopyPropertyFight.SetParamAdjustPermanentSta = function(newv)
	SetTmpParam_At(612, newv)
end
CLuaCopyPropertyFight.SetParamAdjustPermanentStr = function(newv)
	SetTmpParam_At(613, newv)
end
CLuaCopyPropertyFight.SetParamAdjustPermanentInt = function(newv)
	SetTmpParam_At(614, newv)
end
CLuaCopyPropertyFight.SetParamAdjustPermanentAgi = function(newv)
	SetTmpParam_At(615, newv)
end
CLuaCopyPropertyFight.SetParamSoulCoreLevel = function(newv)
	SetTmpParam_At(616, newv)
end
CLuaCopyPropertyFight.SetParamAdjustPermanentMidCor = function(newv)
	SetTmpParam_At(617, newv)
end
CLuaCopyPropertyFight.SetParamAdjustPermanentMidSta = function(newv)
	SetTmpParam_At(618, newv)
end
CLuaCopyPropertyFight.SetParamAdjustPermanentMidStr = function(newv)
	SetTmpParam_At(619, newv)
end
CLuaCopyPropertyFight.SetParamAdjustPermanentMidInt = function(newv)
	SetTmpParam_At(620, newv)
end
CLuaCopyPropertyFight.SetParamAdjustPermanentMidAgi = function(newv)
	SetTmpParam_At(621, newv)
end
CLuaCopyPropertyFight.SetParamSumPermanent = function(newv)
	SetTmpParam_At(622, newv)
end
CLuaCopyPropertyFight.SetParamAntiTian = function(newv)
	SetTmpParam_At(623, newv)
end
CLuaCopyPropertyFight.SetParamAntiEGui = function(newv)
	SetTmpParam_At(624, newv)
end
CLuaCopyPropertyFight.SetParamAntiXiuLuo = function(newv)
	SetTmpParam_At(625, newv)
end
CLuaCopyPropertyFight.SetParamAntiDiYu = function(newv)
	SetTmpParam_At(626, newv)
end
CLuaCopyPropertyFight.SetParamAntiRen = function(newv)
	SetTmpParam_At(627, newv)
end
CLuaCopyPropertyFight.SetParamAntiChuSheng = function(newv)
	SetTmpParam_At(628, newv)
end
CLuaCopyPropertyFight.SetParamAntiBuilding = function(newv)
	SetTmpParam_At(629, newv)
end
CLuaCopyPropertyFight.SetParamAntiZhaoHuan = function(newv)
	SetTmpParam_At(630, newv)
end
CLuaCopyPropertyFight.SetParamAntiLingShou = function(newv)
	SetTmpParam_At(631, newv)
end
CLuaCopyPropertyFight.SetParamAntiBoss = function(newv)
	SetTmpParam_At(632, newv)
end
CLuaCopyPropertyFight.SetParamAntiSheShou = function(newv)
	SetTmpParam_At(633, newv)
end
CLuaCopyPropertyFight.SetParamAntiJiaShi = function(newv)
	SetTmpParam_At(634, newv)
end
CLuaCopyPropertyFight.SetParamAntiDaoKe = function(newv)
	SetTmpParam_At(635, newv)
end
CLuaCopyPropertyFight.SetParamAntiXiaKe = function(newv)
	SetTmpParam_At(636, newv)
end
CLuaCopyPropertyFight.SetParamAntiFangShi = function(newv)
	SetTmpParam_At(637, newv)
end
CLuaCopyPropertyFight.SetParamAntiYiShi = function(newv)
	SetTmpParam_At(638, newv)
end
CLuaCopyPropertyFight.SetParamAntiMeiZhe = function(newv)
	SetTmpParam_At(639, newv)
end
CLuaCopyPropertyFight.SetParamAntiYiRen = function(newv)
	SetTmpParam_At(640, newv)
end
CLuaCopyPropertyFight.SetParamAntiYanShi = function(newv)
	SetTmpParam_At(641, newv)
end
CLuaCopyPropertyFight.SetParamAntiHuaHun = function(newv)
	SetTmpParam_At(642, newv)
end
CLuaCopyPropertyFight.SetParamAntiYingLing = function(newv)
	SetTmpParam_At(643, newv)
end
CLuaCopyPropertyFight.SetParamAntiDieKe = function(newv)
	SetTmpParam_At(644, newv)
end
CLuaCopyPropertyFight.SetParamAntiFlag = function(newv)
	SetTmpParam_At(645, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceZhanKuang = function(newv)
	SetTmpParam_At(646, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceZhanKuangMul = function(newv)
	SetTmpParam_At(647, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceZhanKuangKefu = function(newv)
	SetTmpParam_At(648, newv)
end
CLuaCopyPropertyFight.SetParamAntiZhanKuang = function(newv)
	SetTmpParam_At(649, newv)
end
CLuaCopyPropertyFight.SetParamJumpSpeed = function(newv)
	SetTmpParam_At(650, newv)
end
CLuaCopyPropertyFight.SetParamOriJumpSpeed = function(newv)
	SetTmpParam_At(651, newv)
end
CLuaCopyPropertyFight.SetParamAdjJumpSpeed = function(newv)
	SetTmpParam_At(652, newv)
end
CLuaCopyPropertyFight.SetParamGravityAcceleration = function(newv)
	SetTmpParam_At(653, newv)
end
CLuaCopyPropertyFight.SetParamOriGravityAcceleration = function(newv)
	SetTmpParam_At(654, newv)
end
CLuaCopyPropertyFight.SetParamAdjGravityAcceleration = function(newv)
	SetTmpParam_At(655, newv)
end
CLuaCopyPropertyFight.SetParamHorizontalAcceleration = function(newv)
	SetTmpParam_At(656, newv)
end
CLuaCopyPropertyFight.SetParamOriHorizontalAcceleration = function(newv)
	SetTmpParam_At(657, newv)
end
CLuaCopyPropertyFight.SetParamAdjHorizontalAcceleration = function(newv)
	SetTmpParam_At(658, newv)
end
CLuaCopyPropertyFight.SetParamSwimSpeed = function(newv)
	SetTmpParam_At(659, newv)
end
CLuaCopyPropertyFight.SetParamOriSwimSpeed = function(newv)
	SetTmpParam_At(660, newv)
end
CLuaCopyPropertyFight.SetParamAdjSwimSpeed = function(newv)
	SetTmpParam_At(661, newv)
end
CLuaCopyPropertyFight.SetParamStrengthPoint = function(newv)
	SetTmpParam_At(662, newv)
end
CLuaCopyPropertyFight.SetParamStrengthPointMax = function(newv)
	SetTmpParam_At(663, newv)
end
CLuaCopyPropertyFight.SetParamOriStrengthPointMax = function(newv)
	SetTmpParam_At(664, newv)
end
CLuaCopyPropertyFight.SetParamAdjStrengthPointMax = function(newv)
	SetTmpParam_At(665, newv)
end
CLuaCopyPropertyFight.SetParamAntiMulEnhanceIllusion = function(newv)
	SetTmpParam_At(666, newv)
end
CLuaCopyPropertyFight.SetParamGlideHorizontalSpeedWithUmbrella = function(newv)
	SetTmpParam_At(667, newv)
end
CLuaCopyPropertyFight.SetParamGlideVerticalSpeedWithUmbrella = function(newv)
	SetTmpParam_At(668, newv)
end
CLuaCopyPropertyFight.SetParamAdjpSpeed = function(newv)
	SetTmpParam_At(1001, newv)
end
CLuaCopyPropertyFight.SetParamAdjmSpeed = function(newv)
	SetTmpParam_At(1002, newv)
end
CLuaCopyPropertyFight.SetParamPermanentCor = function(newv)
	SetParam_At(1, newv)
end
CLuaCopyPropertyFight.SetParamPermanentSta = function(newv)
	SetParam_At(2, newv)
end
CLuaCopyPropertyFight.SetParamPermanentStr = function(newv)
	SetParam_At(3, newv)
end
CLuaCopyPropertyFight.SetParamPermanentInt = function(newv)
	SetParam_At(4, newv)
end
CLuaCopyPropertyFight.SetParamPermanentAgi = function(newv)
	SetParam_At(5, newv)
end
CLuaCopyPropertyFight.SetParamPermanentHpFull = function(newv)
	SetParam_At(6, newv)
end
CLuaCopyPropertyFight.SetParamHp = function(newv)
	SetParam_At(7, newv)
end
CLuaCopyPropertyFight.SetParamMp = function(newv)
	SetParam_At(8, newv)
end
CLuaCopyPropertyFight.SetParamCurrentHpFull = function(newv)
	SetParam_At(9, newv)
end
CLuaCopyPropertyFight.SetParamCurrentMpFull = function(newv)
	SetParam_At(10, newv)
end
CLuaCopyPropertyFight.SetParamEquipExchangeMF = function(newv)
	SetParam_At(11, newv)
end
CLuaCopyPropertyFight.SetParamRevisePermanentCor = function(newv)
	SetParam_At(12, newv)
end
CLuaCopyPropertyFight.SetParamRevisePermanentSta = function(newv)
	SetParam_At(13, newv)
end
CLuaCopyPropertyFight.SetParamRevisePermanentStr = function(newv)
	SetParam_At(14, newv)
end
CLuaCopyPropertyFight.SetParamRevisePermanentInt = function(newv)
	SetParam_At(15, newv)
end
CLuaCopyPropertyFight.SetParamRevisePermanentAgi = function(newv)
	SetParam_At(16, newv)
end
CLuaCopyPropertyFight.SetParamClass = function(newv)
	SetTmpParam_At(1, newv)
end
CLuaCopyPropertyFight.SetParamGrade = function(newv)
	SetTmpParam_At(2, newv)
end
CLuaCopyPropertyFight.SetParamCor = function(newv)
	SetTmpParam_At(3, newv)
end
CLuaCopyPropertyFight.SetParamAdjCor = function(newv)
	SetTmpParam_At(4, newv)
end
CLuaCopyPropertyFight.SetParamMulCor = function(newv)
	SetTmpParam_At(5, newv)
end
CLuaCopyPropertyFight.SetParamSta = function(newv)
	SetTmpParam_At(6, newv)
end
CLuaCopyPropertyFight.SetParamAdjSta = function(newv)
	SetTmpParam_At(7, newv)
end
CLuaCopyPropertyFight.SetParamMulSta = function(newv)
	SetTmpParam_At(8, newv)
end
CLuaCopyPropertyFight.SetParamStr = function(newv)
	SetTmpParam_At(9, newv)
end
CLuaCopyPropertyFight.SetParamAdjStr = function(newv)
	SetTmpParam_At(10, newv)
end
CLuaCopyPropertyFight.SetParamMulStr = function(newv)
	SetTmpParam_At(11, newv)
end
CLuaCopyPropertyFight.SetParamInt = function(newv)
	SetTmpParam_At(12, newv)
end
CLuaCopyPropertyFight.SetParamAdjInt = function(newv)
	SetTmpParam_At(13, newv)
end
CLuaCopyPropertyFight.SetParamMulInt = function(newv)
	SetTmpParam_At(14, newv)
end
CLuaCopyPropertyFight.SetParamAgi = function(newv)
	SetTmpParam_At(15, newv)
end
CLuaCopyPropertyFight.SetParamAdjAgi = function(newv)
	SetTmpParam_At(16, newv)
end
CLuaCopyPropertyFight.SetParamMulAgi = function(newv)
	SetTmpParam_At(17, newv)
end
CLuaCopyPropertyFight.SetParamHpFull = function(newv)
	SetTmpParam_At(18, newv)
end
CLuaCopyPropertyFight.SetParamAdjHpFull = function(newv)
	SetTmpParam_At(19, newv)
end
CLuaCopyPropertyFight.SetParamMulHpFull = function(newv)
	SetTmpParam_At(20, newv)
end
CLuaCopyPropertyFight.SetParamHpRecover = function(newv)
	SetTmpParam_At(21, newv)
end
CLuaCopyPropertyFight.SetParamAdjHpRecover = function(newv)
	SetTmpParam_At(22, newv)
end
CLuaCopyPropertyFight.SetParamMulHpRecover = function(newv)
	SetTmpParam_At(23, newv)
end
CLuaCopyPropertyFight.SetParamMpFull = function(newv)
	SetTmpParam_At(24, newv)
end
CLuaCopyPropertyFight.SetParamAdjMpFull = function(newv)
	SetTmpParam_At(25, newv)
end
CLuaCopyPropertyFight.SetParamMulMpFull = function(newv)
	SetTmpParam_At(26, newv)
end
CLuaCopyPropertyFight.SetParamMpRecover = function(newv)
	SetTmpParam_At(27, newv)
end
CLuaCopyPropertyFight.SetParamAdjMpRecover = function(newv)
	SetTmpParam_At(28, newv)
end
CLuaCopyPropertyFight.SetParamMulMpRecover = function(newv)
	SetTmpParam_At(29, newv)
end
CLuaCopyPropertyFight.SetParampAttMin = function(newv)
	SetTmpParam_At(30, newv)
end
CLuaCopyPropertyFight.SetParamAdjpAttMin = function(newv)
	SetTmpParam_At(31, newv)
end
CLuaCopyPropertyFight.SetParamMulpAttMin = function(newv)
	SetTmpParam_At(32, newv)
end
CLuaCopyPropertyFight.SetParampAttMax = function(newv)
	SetTmpParam_At(33, newv)
end
CLuaCopyPropertyFight.SetParamAdjpAttMax = function(newv)
	SetTmpParam_At(34, newv)
end
CLuaCopyPropertyFight.SetParamMulpAttMax = function(newv)
	SetTmpParam_At(35, newv)
end
CLuaCopyPropertyFight.SetParampHit = function(newv)
	SetTmpParam_At(36, newv)
end
CLuaCopyPropertyFight.SetParamAdjpHit = function(newv)
	SetTmpParam_At(37, newv)
end
CLuaCopyPropertyFight.SetParamMulpHit = function(newv)
	SetTmpParam_At(38, newv)
end
CLuaCopyPropertyFight.SetParampMiss = function(newv)
	SetTmpParam_At(39, newv)
end
CLuaCopyPropertyFight.SetParamAdjpMiss = function(newv)
	SetTmpParam_At(40, newv)
end
CLuaCopyPropertyFight.SetParamMulpMiss = function(newv)
	SetTmpParam_At(41, newv)
end
CLuaCopyPropertyFight.SetParampDef = function(newv)
	SetTmpParam_At(42, newv)
end
CLuaCopyPropertyFight.SetParamAdjpDef = function(newv)
	SetTmpParam_At(43, newv)
end
CLuaCopyPropertyFight.SetParamMulpDef = function(newv)
	SetTmpParam_At(44, newv)
end
CLuaCopyPropertyFight.SetParamAdjpHurt = function(newv)
	SetTmpParam_At(45, newv)
end
CLuaCopyPropertyFight.SetParamMulpHurt = function(newv)
	SetTmpParam_At(46, newv)
end
CLuaCopyPropertyFight.SetParampSpeed = function(newv)
	SetTmpParam_At(47, newv)
end
CLuaCopyPropertyFight.SetParamOripSpeed = function(newv)
	SetTmpParam_At(48, newv)
end
CLuaCopyPropertyFight.SetParamMulpSpeed = function(newv)
	SetTmpParam_At(49, newv)
end
CLuaCopyPropertyFight.SetParampFatal = function(newv)
	SetTmpParam_At(50, newv)
end
CLuaCopyPropertyFight.SetParamAdjpFatal = function(newv)
	SetTmpParam_At(51, newv)
end
CLuaCopyPropertyFight.SetParamAntipFatal = function(newv)
	SetTmpParam_At(52, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntipFatal = function(newv)
	SetTmpParam_At(53, newv)
end
CLuaCopyPropertyFight.SetParampFatalDamage = function(newv)
	SetTmpParam_At(54, newv)
end
CLuaCopyPropertyFight.SetParamAdjpFatalDamage = function(newv)
	SetTmpParam_At(55, newv)
end
CLuaCopyPropertyFight.SetParamAntipFatalDamage = function(newv)
	SetTmpParam_At(56, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntipFatalDamage = function(newv)
	SetTmpParam_At(57, newv)
end
CLuaCopyPropertyFight.SetParamBlock = function(newv)
	SetTmpParam_At(58, newv)
end
CLuaCopyPropertyFight.SetParamAdjBlock = function(newv)
	SetTmpParam_At(59, newv)
end
CLuaCopyPropertyFight.SetParamMulBlock = function(newv)
	SetTmpParam_At(60, newv)
end
CLuaCopyPropertyFight.SetParamBlockDamage = function(newv)
	SetTmpParam_At(61, newv)
end
CLuaCopyPropertyFight.SetParamAdjBlockDamage = function(newv)
	SetTmpParam_At(62, newv)
end
CLuaCopyPropertyFight.SetParamAntiBlock = function(newv)
	SetTmpParam_At(63, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiBlock = function(newv)
	SetTmpParam_At(64, newv)
end
CLuaCopyPropertyFight.SetParamAntiBlockDamage = function(newv)
	SetTmpParam_At(65, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiBlockDamage = function(newv)
	SetTmpParam_At(66, newv)
end
CLuaCopyPropertyFight.SetParammAttMin = function(newv)
	SetTmpParam_At(67, newv)
end
CLuaCopyPropertyFight.SetParamAdjmAttMin = function(newv)
	SetTmpParam_At(68, newv)
end
CLuaCopyPropertyFight.SetParamMulmAttMin = function(newv)
	SetTmpParam_At(69, newv)
end
CLuaCopyPropertyFight.SetParammAttMax = function(newv)
	SetTmpParam_At(70, newv)
end
CLuaCopyPropertyFight.SetParamAdjmAttMax = function(newv)
	SetTmpParam_At(71, newv)
end
CLuaCopyPropertyFight.SetParamMulmAttMax = function(newv)
	SetTmpParam_At(72, newv)
end
CLuaCopyPropertyFight.SetParammHit = function(newv)
	SetTmpParam_At(73, newv)
end
CLuaCopyPropertyFight.SetParamAdjmHit = function(newv)
	SetTmpParam_At(74, newv)
end
CLuaCopyPropertyFight.SetParamMulmHit = function(newv)
	SetTmpParam_At(75, newv)
end
CLuaCopyPropertyFight.SetParammMiss = function(newv)
	SetTmpParam_At(76, newv)
end
CLuaCopyPropertyFight.SetParamAdjmMiss = function(newv)
	SetTmpParam_At(77, newv)
end
CLuaCopyPropertyFight.SetParamMulmMiss = function(newv)
	SetTmpParam_At(78, newv)
end
CLuaCopyPropertyFight.SetParammDef = function(newv)
	SetTmpParam_At(79, newv)
end
CLuaCopyPropertyFight.SetParamAdjmDef = function(newv)
	SetTmpParam_At(80, newv)
end
CLuaCopyPropertyFight.SetParamMulmDef = function(newv)
	SetTmpParam_At(81, newv)
end
CLuaCopyPropertyFight.SetParamAdjmHurt = function(newv)
	SetTmpParam_At(82, newv)
end
CLuaCopyPropertyFight.SetParamMulmHurt = function(newv)
	SetTmpParam_At(83, newv)
end
CLuaCopyPropertyFight.SetParammSpeed = function(newv)
	SetTmpParam_At(84, newv)
end
CLuaCopyPropertyFight.SetParamOrimSpeed = function(newv)
	SetTmpParam_At(85, newv)
end
CLuaCopyPropertyFight.SetParamMulmSpeed = function(newv)
	SetTmpParam_At(86, newv)
end
CLuaCopyPropertyFight.SetParammFatal = function(newv)
	SetTmpParam_At(87, newv)
end
CLuaCopyPropertyFight.SetParamAdjmFatal = function(newv)
	SetTmpParam_At(88, newv)
end
CLuaCopyPropertyFight.SetParamAntimFatal = function(newv)
	SetTmpParam_At(89, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntimFatal = function(newv)
	SetTmpParam_At(90, newv)
end
CLuaCopyPropertyFight.SetParammFatalDamage = function(newv)
	SetTmpParam_At(91, newv)
end
CLuaCopyPropertyFight.SetParamAdjmFatalDamage = function(newv)
	SetTmpParam_At(92, newv)
end
CLuaCopyPropertyFight.SetParamAntimFatalDamage = function(newv)
	SetTmpParam_At(93, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntimFatalDamage = function(newv)
	SetTmpParam_At(94, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceFire = function(newv)
	SetTmpParam_At(95, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceFire = function(newv)
	SetTmpParam_At(96, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhanceFire = function(newv)
	SetTmpParam_At(97, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceThunder = function(newv)
	SetTmpParam_At(98, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceThunder = function(newv)
	SetTmpParam_At(99, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhanceThunder = function(newv)
	SetTmpParam_At(100, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceIce = function(newv)
	SetTmpParam_At(101, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceIce = function(newv)
	SetTmpParam_At(102, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhanceIce = function(newv)
	SetTmpParam_At(103, newv)
end
CLuaCopyPropertyFight.SetParamEnhancePoison = function(newv)
	SetTmpParam_At(104, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhancePoison = function(newv)
	SetTmpParam_At(105, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhancePoison = function(newv)
	SetTmpParam_At(106, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceWind = function(newv)
	SetTmpParam_At(107, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceWind = function(newv)
	SetTmpParam_At(108, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhanceWind = function(newv)
	SetTmpParam_At(109, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceLight = function(newv)
	SetTmpParam_At(110, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceLight = function(newv)
	SetTmpParam_At(111, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhanceLight = function(newv)
	SetTmpParam_At(112, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceIllusion = function(newv)
	SetTmpParam_At(113, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceIllusion = function(newv)
	SetTmpParam_At(114, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhanceIllusion = function(newv)
	SetTmpParam_At(115, newv)
end
CLuaCopyPropertyFight.SetParamAntiFire = function(newv)
	SetTmpParam_At(116, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiFire = function(newv)
	SetTmpParam_At(117, newv)
end
CLuaCopyPropertyFight.SetParamMulAntiFire = function(newv)
	SetTmpParam_At(118, newv)
end
CLuaCopyPropertyFight.SetParamAntiThunder = function(newv)
	SetTmpParam_At(119, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiThunder = function(newv)
	SetTmpParam_At(120, newv)
end
CLuaCopyPropertyFight.SetParamMulAntiThunder = function(newv)
	SetTmpParam_At(121, newv)
end
CLuaCopyPropertyFight.SetParamAntiIce = function(newv)
	SetTmpParam_At(122, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiIce = function(newv)
	SetTmpParam_At(123, newv)
end
CLuaCopyPropertyFight.SetParamMulAntiIce = function(newv)
	SetTmpParam_At(124, newv)
end
CLuaCopyPropertyFight.SetParamAntiPoison = function(newv)
	SetTmpParam_At(125, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiPoison = function(newv)
	SetTmpParam_At(126, newv)
end
CLuaCopyPropertyFight.SetParamMulAntiPoison = function(newv)
	SetTmpParam_At(127, newv)
end
CLuaCopyPropertyFight.SetParamAntiWind = function(newv)
	SetTmpParam_At(128, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiWind = function(newv)
	SetTmpParam_At(129, newv)
end
CLuaCopyPropertyFight.SetParamMulAntiWind = function(newv)
	SetTmpParam_At(130, newv)
end
CLuaCopyPropertyFight.SetParamAntiLight = function(newv)
	SetTmpParam_At(131, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiLight = function(newv)
	SetTmpParam_At(132, newv)
end
CLuaCopyPropertyFight.SetParamMulAntiLight = function(newv)
	SetTmpParam_At(133, newv)
end
CLuaCopyPropertyFight.SetParamAntiIllusion = function(newv)
	SetTmpParam_At(134, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiIllusion = function(newv)
	SetTmpParam_At(135, newv)
end
CLuaCopyPropertyFight.SetParamMulAntiIllusion = function(newv)
	SetTmpParam_At(136, newv)
end
CLuaCopyPropertyFight.SetParamIgnoreAntiFire = function(newv)
	SetTmpParam_At(137, newv)
end
CLuaCopyPropertyFight.SetParamIgnoreAntiThunder = function(newv)
	SetTmpParam_At(138, newv)
end
CLuaCopyPropertyFight.SetParamIgnoreAntiIce = function(newv)
	SetTmpParam_At(139, newv)
end
CLuaCopyPropertyFight.SetParamIgnoreAntiPoison = function(newv)
	SetTmpParam_At(140, newv)
end
CLuaCopyPropertyFight.SetParamIgnoreAntiWind = function(newv)
	SetTmpParam_At(141, newv)
end
CLuaCopyPropertyFight.SetParamIgnoreAntiLight = function(newv)
	SetTmpParam_At(142, newv)
end
CLuaCopyPropertyFight.SetParamIgnoreAntiIllusion = function(newv)
	SetTmpParam_At(143, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceDizzy = function(newv)
	SetTmpParam_At(144, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceDizzy = function(newv)
	SetTmpParam_At(145, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhanceDizzy = function(newv)
	SetTmpParam_At(146, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceSleep = function(newv)
	SetTmpParam_At(147, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceSleep = function(newv)
	SetTmpParam_At(148, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhanceSleep = function(newv)
	SetTmpParam_At(149, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceChaos = function(newv)
	SetTmpParam_At(150, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceChaos = function(newv)
	SetTmpParam_At(151, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhanceChaos = function(newv)
	SetTmpParam_At(152, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceBind = function(newv)
	SetTmpParam_At(153, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceBind = function(newv)
	SetTmpParam_At(154, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhanceBind = function(newv)
	SetTmpParam_At(155, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceSilence = function(newv)
	SetTmpParam_At(156, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceSilence = function(newv)
	SetTmpParam_At(157, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhanceSilence = function(newv)
	SetTmpParam_At(158, newv)
end
CLuaCopyPropertyFight.SetParamAntiDizzy = function(newv)
	SetTmpParam_At(159, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiDizzy = function(newv)
	SetTmpParam_At(160, newv)
end
CLuaCopyPropertyFight.SetParamMulAntiDizzy = function(newv)
	SetTmpParam_At(161, newv)
end
CLuaCopyPropertyFight.SetParamAntiSleep = function(newv)
	SetTmpParam_At(162, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiSleep = function(newv)
	SetTmpParam_At(163, newv)
end
CLuaCopyPropertyFight.SetParamMulAntiSleep = function(newv)
	SetTmpParam_At(164, newv)
end
CLuaCopyPropertyFight.SetParamAntiChaos = function(newv)
	SetTmpParam_At(165, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiChaos = function(newv)
	SetTmpParam_At(166, newv)
end
CLuaCopyPropertyFight.SetParamMulAntiChaos = function(newv)
	SetTmpParam_At(167, newv)
end
CLuaCopyPropertyFight.SetParamAntiBind = function(newv)
	SetTmpParam_At(168, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiBind = function(newv)
	SetTmpParam_At(169, newv)
end
CLuaCopyPropertyFight.SetParamMulAntiBind = function(newv)
	SetTmpParam_At(170, newv)
end
CLuaCopyPropertyFight.SetParamAntiSilence = function(newv)
	SetTmpParam_At(171, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiSilence = function(newv)
	SetTmpParam_At(172, newv)
end
CLuaCopyPropertyFight.SetParamMulAntiSilence = function(newv)
	SetTmpParam_At(173, newv)
end
CLuaCopyPropertyFight.SetParamDizzyTimeChange = function(newv)
	SetTmpParam_At(174, newv)
end
CLuaCopyPropertyFight.SetParamSleepTimeChange = function(newv)
	SetTmpParam_At(175, newv)
end
CLuaCopyPropertyFight.SetParamChaosTimeChange = function(newv)
	SetTmpParam_At(176, newv)
end
CLuaCopyPropertyFight.SetParamBindTimeChange = function(newv)
	SetTmpParam_At(177, newv)
end
CLuaCopyPropertyFight.SetParamSilenceTimeChange = function(newv)
	SetTmpParam_At(178, newv)
end
CLuaCopyPropertyFight.SetParamBianhuTimeChange = function(newv)
	SetTmpParam_At(179, newv)
end
CLuaCopyPropertyFight.SetParamFreezeTimeChange = function(newv)
	SetTmpParam_At(180, newv)
end
CLuaCopyPropertyFight.SetParamPetrifyTimeChange = function(newv)
	SetTmpParam_At(181, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceHeal = function(newv)
	SetTmpParam_At(182, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceHeal = function(newv)
	SetTmpParam_At(183, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhanceHeal = function(newv)
	SetTmpParam_At(184, newv)
end
CLuaCopyPropertyFight.SetParamSpeed = function(newv)
	SetTmpParam_At(185, newv)
end
CLuaCopyPropertyFight.SetParamOriSpeed = function(newv)
	SetTmpParam_At(186, newv)
end
CLuaCopyPropertyFight.SetParamAdjSpeed = function(newv)
	SetTmpParam_At(187, newv)
end
CLuaCopyPropertyFight.SetParamMulSpeed = function(newv)
	SetTmpParam_At(188, newv)
end
CLuaCopyPropertyFight.SetParamMF = function(newv)
	SetTmpParam_At(189, newv)
end
CLuaCopyPropertyFight.SetParamAdjMF = function(newv)
	SetTmpParam_At(190, newv)
end
CLuaCopyPropertyFight.SetParamRange = function(newv)
	SetTmpParam_At(191, newv)
end
CLuaCopyPropertyFight.SetParamAdjRange = function(newv)
	SetTmpParam_At(192, newv)
end
CLuaCopyPropertyFight.SetParamHatePlus = function(newv)
	SetTmpParam_At(193, newv)
end
CLuaCopyPropertyFight.SetParamAdjHatePlus = function(newv)
	SetTmpParam_At(194, newv)
end
CLuaCopyPropertyFight.SetParamHateDecrease = function(newv)
	SetTmpParam_At(195, newv)
end
CLuaCopyPropertyFight.SetParamInvisible = function(newv)
	SetTmpParam_At(196, newv)
end
CLuaCopyPropertyFight.SetParamAdjInvisible = function(newv)
	SetTmpParam_At(197, newv)
end
CLuaCopyPropertyFight.SetParamTrueSight = function(newv)
	SetTmpParam_At(198, newv)
end
CLuaCopyPropertyFight.SetParamOriTrueSight = function(newv)
	SetTmpParam_At(199, newv)
end
CLuaCopyPropertyFight.SetParamAdjTrueSight = function(newv)
	SetTmpParam_At(200, newv)
end
CLuaCopyPropertyFight.SetParamEyeSight = function(newv)
	SetTmpParam_At(201, newv)
end
CLuaCopyPropertyFight.SetParamOriEyeSight = function(newv)
	SetTmpParam_At(202, newv)
end
CLuaCopyPropertyFight.SetParamAdjEyeSight = function(newv)
	SetTmpParam_At(203, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceTian = function(newv)
	SetTmpParam_At(204, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceTianMul = function(newv)
	SetTmpParam_At(205, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceEGui = function(newv)
	SetTmpParam_At(206, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceEGuiMul = function(newv)
	SetTmpParam_At(207, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceXiuLuo = function(newv)
	SetTmpParam_At(208, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceXiuLuoMul = function(newv)
	SetTmpParam_At(209, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceDiYu = function(newv)
	SetTmpParam_At(210, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceDiYuMul = function(newv)
	SetTmpParam_At(211, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceRen = function(newv)
	SetTmpParam_At(212, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceRenMul = function(newv)
	SetTmpParam_At(213, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceChuSheng = function(newv)
	SetTmpParam_At(214, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceChuShengMul = function(newv)
	SetTmpParam_At(215, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceBuilding = function(newv)
	SetTmpParam_At(216, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceBuildingMul = function(newv)
	SetTmpParam_At(217, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceBoss = function(newv)
	SetTmpParam_At(218, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceBossMul = function(newv)
	SetTmpParam_At(219, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceSheShou = function(newv)
	SetTmpParam_At(220, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceSheShouMul = function(newv)
	SetTmpParam_At(221, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceJiaShi = function(newv)
	SetTmpParam_At(222, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceJiaShiMul = function(newv)
	SetTmpParam_At(223, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceFangShi = function(newv)
	SetTmpParam_At(224, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceFangShiMul = function(newv)
	SetTmpParam_At(225, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceYiShi = function(newv)
	SetTmpParam_At(226, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceYiShiMul = function(newv)
	SetTmpParam_At(227, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceMeiZhe = function(newv)
	SetTmpParam_At(228, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceMeiZheMul = function(newv)
	SetTmpParam_At(229, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceYiRen = function(newv)
	SetTmpParam_At(230, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceYiRenMul = function(newv)
	SetTmpParam_At(231, newv)
end
CLuaCopyPropertyFight.SetParamIgnorepDef = function(newv)
	SetTmpParam_At(232, newv)
end
CLuaCopyPropertyFight.SetParamIgnoremDef = function(newv)
	SetTmpParam_At(233, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceZhaoHuan = function(newv)
	SetTmpParam_At(234, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceZhaoHuanMul = function(newv)
	SetTmpParam_At(235, newv)
end
CLuaCopyPropertyFight.SetParamDizzyProbability = function(newv)
	SetTmpParam_At(236, newv)
end
CLuaCopyPropertyFight.SetParamSleepProbability = function(newv)
	SetTmpParam_At(237, newv)
end
CLuaCopyPropertyFight.SetParamChaosProbability = function(newv)
	SetTmpParam_At(238, newv)
end
CLuaCopyPropertyFight.SetParamBindProbability = function(newv)
	SetTmpParam_At(239, newv)
end
CLuaCopyPropertyFight.SetParamSilenceProbability = function(newv)
	SetTmpParam_At(240, newv)
end
CLuaCopyPropertyFight.SetParamHpFullPow2 = function(newv)
	SetTmpParam_At(241, newv)
end
CLuaCopyPropertyFight.SetParamHpFullPow1 = function(newv)
	SetTmpParam_At(242, newv)
end
CLuaCopyPropertyFight.SetParamHpFullInit = function(newv)
	SetTmpParam_At(243, newv)
end
CLuaCopyPropertyFight.SetParamMpFullPow1 = function(newv)
	SetTmpParam_At(244, newv)
end
CLuaCopyPropertyFight.SetParamMpFullInit = function(newv)
	SetTmpParam_At(245, newv)
end
CLuaCopyPropertyFight.SetParamMpRecoverPow1 = function(newv)
	SetTmpParam_At(246, newv)
end
CLuaCopyPropertyFight.SetParamMpRecoverInit = function(newv)
	SetTmpParam_At(247, newv)
end
CLuaCopyPropertyFight.SetParamPAttMinPow2 = function(newv)
	SetTmpParam_At(248, newv)
end
CLuaCopyPropertyFight.SetParamPAttMinPow1 = function(newv)
	SetTmpParam_At(249, newv)
end
CLuaCopyPropertyFight.SetParamPAttMinInit = function(newv)
	SetTmpParam_At(250, newv)
end
CLuaCopyPropertyFight.SetParamPAttMaxPow2 = function(newv)
	SetTmpParam_At(251, newv)
end
CLuaCopyPropertyFight.SetParamPAttMaxPow1 = function(newv)
	SetTmpParam_At(252, newv)
end
CLuaCopyPropertyFight.SetParamPAttMaxInit = function(newv)
	SetTmpParam_At(253, newv)
end
CLuaCopyPropertyFight.SetParamPhitPow1 = function(newv)
	SetTmpParam_At(254, newv)
end
CLuaCopyPropertyFight.SetParamPHitInit = function(newv)
	SetTmpParam_At(255, newv)
end
CLuaCopyPropertyFight.SetParamPMissPow1 = function(newv)
	SetTmpParam_At(256, newv)
end
CLuaCopyPropertyFight.SetParamPMissInit = function(newv)
	SetTmpParam_At(257, newv)
end
CLuaCopyPropertyFight.SetParamPSpeedPow1 = function(newv)
	SetTmpParam_At(258, newv)
end
CLuaCopyPropertyFight.SetParamPSpeedInit = function(newv)
	SetTmpParam_At(259, newv)
end
CLuaCopyPropertyFight.SetParamPDefPow1 = function(newv)
	SetTmpParam_At(260, newv)
end
CLuaCopyPropertyFight.SetParamPDefInit = function(newv)
	SetTmpParam_At(261, newv)
end
CLuaCopyPropertyFight.SetParamPfatalPow1 = function(newv)
	SetTmpParam_At(262, newv)
end
CLuaCopyPropertyFight.SetParamPFatalInit = function(newv)
	SetTmpParam_At(263, newv)
end
CLuaCopyPropertyFight.SetParamMAttMinPow2 = function(newv)
	SetTmpParam_At(264, newv)
end
CLuaCopyPropertyFight.SetParamMAttMinPow1 = function(newv)
	SetTmpParam_At(265, newv)
end
CLuaCopyPropertyFight.SetParamMAttMinInit = function(newv)
	SetTmpParam_At(266, newv)
end
CLuaCopyPropertyFight.SetParamMAttMaxPow2 = function(newv)
	SetTmpParam_At(267, newv)
end
CLuaCopyPropertyFight.SetParamMAttMaxPow1 = function(newv)
	SetTmpParam_At(268, newv)
end
CLuaCopyPropertyFight.SetParamMAttMaxInit = function(newv)
	SetTmpParam_At(269, newv)
end
CLuaCopyPropertyFight.SetParamMSpeedPow1 = function(newv)
	SetTmpParam_At(270, newv)
end
CLuaCopyPropertyFight.SetParamMSpeedInit = function(newv)
	SetTmpParam_At(271, newv)
end
CLuaCopyPropertyFight.SetParamMDefPow1 = function(newv)
	SetTmpParam_At(272, newv)
end
CLuaCopyPropertyFight.SetParamMDefInit = function(newv)
	SetTmpParam_At(273, newv)
end
CLuaCopyPropertyFight.SetParamMHitPow1 = function(newv)
	SetTmpParam_At(274, newv)
end
CLuaCopyPropertyFight.SetParamMHitInit = function(newv)
	SetTmpParam_At(275, newv)
end
CLuaCopyPropertyFight.SetParamMMissPow1 = function(newv)
	SetTmpParam_At(276, newv)
end
CLuaCopyPropertyFight.SetParamMMissInit = function(newv)
	SetTmpParam_At(277, newv)
end
CLuaCopyPropertyFight.SetParamMFatalPow1 = function(newv)
	SetTmpParam_At(278, newv)
end
CLuaCopyPropertyFight.SetParamMFatalInit = function(newv)
	SetTmpParam_At(279, newv)
end
CLuaCopyPropertyFight.SetParamBlockDamagePow1 = function(newv)
	SetTmpParam_At(280, newv)
end
CLuaCopyPropertyFight.SetParamBlockDamageInit = function(newv)
	SetTmpParam_At(281, newv)
end
CLuaCopyPropertyFight.SetParamRunSpeed = function(newv)
	SetTmpParam_At(282, newv)
end
CLuaCopyPropertyFight.SetParamHpRecoverPow1 = function(newv)
	SetTmpParam_At(283, newv)
end
CLuaCopyPropertyFight.SetParamHpRecoverInit = function(newv)
	SetTmpParam_At(284, newv)
end
CLuaCopyPropertyFight.SetParamAntiBlockDamagePow1 = function(newv)
	SetTmpParam_At(285, newv)
end
CLuaCopyPropertyFight.SetParamAntiBlockDamageInit = function(newv)
	SetTmpParam_At(286, newv)
end
CLuaCopyPropertyFight.SetParamBBMax = function(newv)
	SetTmpParam_At(287, newv)
end
CLuaCopyPropertyFight.SetParamAdjBBMax = function(newv)
	SetTmpParam_At(288, newv)
end
CLuaCopyPropertyFight.SetParamAntiBreak = function(newv)
	SetTmpParam_At(289, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiBreak = function(newv)
	SetTmpParam_At(290, newv)
end
CLuaCopyPropertyFight.SetParamAntiPushPull = function(newv)
	SetTmpParam_At(291, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiPushPull = function(newv)
	SetTmpParam_At(292, newv)
end
CLuaCopyPropertyFight.SetParamAntiPetrify = function(newv)
	SetTmpParam_At(293, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiPetrify = function(newv)
	SetTmpParam_At(294, newv)
end
CLuaCopyPropertyFight.SetParamMulAntiPetrify = function(newv)
	SetTmpParam_At(295, newv)
end
CLuaCopyPropertyFight.SetParamAntiTie = function(newv)
	SetTmpParam_At(296, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiTie = function(newv)
	SetTmpParam_At(297, newv)
end
CLuaCopyPropertyFight.SetParamMulAntiTie = function(newv)
	SetTmpParam_At(298, newv)
end
CLuaCopyPropertyFight.SetParamEnhancePetrify = function(newv)
	SetTmpParam_At(299, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhancePetrify = function(newv)
	SetTmpParam_At(300, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhancePetrify = function(newv)
	SetTmpParam_At(301, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceTie = function(newv)
	SetTmpParam_At(302, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceTie = function(newv)
	SetTmpParam_At(303, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhanceTie = function(newv)
	SetTmpParam_At(304, newv)
end
CLuaCopyPropertyFight.SetParamTieProbability = function(newv)
	SetTmpParam_At(305, newv)
end
CLuaCopyPropertyFight.SetParamStrZizhi = function(newv)
	SetTmpParam_At(306, newv)
end
CLuaCopyPropertyFight.SetParamCorZizhi = function(newv)
	SetTmpParam_At(307, newv)
end
CLuaCopyPropertyFight.SetParamStaZizhi = function(newv)
	SetTmpParam_At(308, newv)
end
CLuaCopyPropertyFight.SetParamAgiZizhi = function(newv)
	SetTmpParam_At(309, newv)
end
CLuaCopyPropertyFight.SetParamIntZizhi = function(newv)
	SetTmpParam_At(310, newv)
end
CLuaCopyPropertyFight.SetParamInitStrZizhi = function(newv)
	SetTmpParam_At(311, newv)
end
CLuaCopyPropertyFight.SetParamInitCorZizhi = function(newv)
	SetTmpParam_At(312, newv)
end
CLuaCopyPropertyFight.SetParamInitStaZizhi = function(newv)
	SetTmpParam_At(313, newv)
end
CLuaCopyPropertyFight.SetParamInitAgiZizhi = function(newv)
	SetTmpParam_At(314, newv)
end
CLuaCopyPropertyFight.SetParamInitIntZizhi = function(newv)
	SetTmpParam_At(315, newv)
end
CLuaCopyPropertyFight.SetParamWuxing = function(newv)
	SetTmpParam_At(316, newv)
end
CLuaCopyPropertyFight.SetParamXiuwei = function(newv)
	SetTmpParam_At(317, newv)
end
CLuaCopyPropertyFight.SetParamSkillNum = function(newv)
	SetTmpParam_At(318, newv)
end
CLuaCopyPropertyFight.SetParamPType = function(newv)
	SetTmpParam_At(319, newv)
end
CLuaCopyPropertyFight.SetParamMType = function(newv)
	SetTmpParam_At(320, newv)
end
CLuaCopyPropertyFight.SetParamPHType = function(newv)
	SetTmpParam_At(321, newv)
end
CLuaCopyPropertyFight.SetParamGrowFactor = function(newv)
	SetTmpParam_At(322, newv)
end
CLuaCopyPropertyFight.SetParamWuxingImprove = function(newv)
	SetTmpParam_At(323, newv)
end
CLuaCopyPropertyFight.SetParamXiuweiImprove = function(newv)
	SetTmpParam_At(324, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceBeHeal = function(newv)
	SetTmpParam_At(325, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceBeHeal = function(newv)
	SetTmpParam_At(326, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhanceBeHeal = function(newv)
	SetTmpParam_At(327, newv)
end
CLuaCopyPropertyFight.SetParamLookUpCor = function(newv)
	SetTmpParam_At(328, newv)
end
CLuaCopyPropertyFight.SetParamLookUpSta = function(newv)
	SetTmpParam_At(329, newv)
end
CLuaCopyPropertyFight.SetParamLookUpStr = function(newv)
	SetTmpParam_At(330, newv)
end
CLuaCopyPropertyFight.SetParamLookUpInt = function(newv)
	SetTmpParam_At(331, newv)
end
CLuaCopyPropertyFight.SetParamLookUpAgi = function(newv)
	SetTmpParam_At(332, newv)
end
CLuaCopyPropertyFight.SetParamLookUpHpFull = function(newv)
	SetTmpParam_At(333, newv)
end
CLuaCopyPropertyFight.SetParamLookUpMpFull = function(newv)
	SetTmpParam_At(334, newv)
end
CLuaCopyPropertyFight.SetParamLookUppAttMin = function(newv)
	SetTmpParam_At(335, newv)
end
CLuaCopyPropertyFight.SetParamLookUppAttMax = function(newv)
	SetTmpParam_At(336, newv)
end
CLuaCopyPropertyFight.SetParamLookUppHit = function(newv)
	SetTmpParam_At(337, newv)
end
CLuaCopyPropertyFight.SetParamLookUppMiss = function(newv)
	SetTmpParam_At(338, newv)
end
CLuaCopyPropertyFight.SetParamLookUppDef = function(newv)
	SetTmpParam_At(339, newv)
end
CLuaCopyPropertyFight.SetParamLookUpmAttMin = function(newv)
	SetTmpParam_At(340, newv)
end
CLuaCopyPropertyFight.SetParamLookUpmAttMax = function(newv)
	SetTmpParam_At(341, newv)
end
CLuaCopyPropertyFight.SetParamLookUpmHit = function(newv)
	SetTmpParam_At(342, newv)
end
CLuaCopyPropertyFight.SetParamLookUpmMiss = function(newv)
	SetTmpParam_At(343, newv)
end
CLuaCopyPropertyFight.SetParamLookUpmDef = function(newv)
	SetTmpParam_At(344, newv)
end
CLuaCopyPropertyFight.SetParamAdjHpFull2 = function(newv)
	SetTmpParam_At(345, newv)
end
CLuaCopyPropertyFight.SetParamAdjpAttMin2 = function(newv)
	SetTmpParam_At(346, newv)
end
CLuaCopyPropertyFight.SetParamAdjpAttMax2 = function(newv)
	SetTmpParam_At(347, newv)
end
CLuaCopyPropertyFight.SetParamAdjmAttMin2 = function(newv)
	SetTmpParam_At(348, newv)
end
CLuaCopyPropertyFight.SetParamAdjmAttMax2 = function(newv)
	SetTmpParam_At(349, newv)
end
CLuaCopyPropertyFight.SetParamLingShouType = function(newv)
	SetTmpParam_At(350, newv)
end
CLuaCopyPropertyFight.SetParamMHType = function(newv)
	SetTmpParam_At(351, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceLingShou = function(newv)
	SetTmpParam_At(352, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceLingShouMul = function(newv)
	SetTmpParam_At(353, newv)
end
CLuaCopyPropertyFight.RefreshParamAntiAoe = function()
	local AdjAntiAoe = GetTmpParam_At(355)

	local newv = AdjAntiAoe
	SetTmpParam_At(354, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiAoe = function(newv)
	SetTmpParam_At(355, newv)

	CLuaCopyPropertyFight.RefreshParamAntiAoe()
end
CLuaCopyPropertyFight.SetParamAntiBlind = function(newv)
	SetTmpParam_At(356, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiBlind = function(newv)
	SetTmpParam_At(357, newv)
end
CLuaCopyPropertyFight.SetParamMulAntiBlind = function(newv)
	SetTmpParam_At(358, newv)
end
CLuaCopyPropertyFight.RefreshParamAntiDecelerate = function()
	local AdjAntiDecelerate = GetTmpParam_At(360)
	local MulAntiDecelerate = GetTmpParam_At(361)

	local newv = AdjAntiDecelerate*(1+MulAntiDecelerate)
	SetTmpParam_At(359, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiDecelerate = function(newv)
	SetTmpParam_At(360, newv)

	CLuaCopyPropertyFight.RefreshParamAntiDecelerate()
end
CLuaCopyPropertyFight.SetParamMulAntiDecelerate = function(newv)
	SetTmpParam_At(361, newv)

	CLuaCopyPropertyFight.RefreshParamAntiDecelerate()
end
CLuaCopyPropertyFight.SetParamMinGrade = function(newv)
	SetTmpParam_At(362, newv)
end
CLuaCopyPropertyFight.SetParamCharacterCor = function(newv)
	SetTmpParam_At(363, newv)
end
CLuaCopyPropertyFight.SetParamCharacterSta = function(newv)
	SetTmpParam_At(364, newv)
end
CLuaCopyPropertyFight.SetParamCharacterStr = function(newv)
	SetTmpParam_At(365, newv)
end
CLuaCopyPropertyFight.SetParamCharacterInt = function(newv)
	SetTmpParam_At(366, newv)
end
CLuaCopyPropertyFight.SetParamCharacterAgi = function(newv)
	SetTmpParam_At(367, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceDaoKe = function(newv)
	SetTmpParam_At(368, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceDaoKeMul = function(newv)
	SetTmpParam_At(369, newv)
end
CLuaCopyPropertyFight.SetParamZuoQiSpeed = function(newv)
	SetTmpParam_At(370, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceBianHu = function(newv)
	SetTmpParam_At(371, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceBianHu = function(newv)
	SetTmpParam_At(372, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhanceBianHu = function(newv)
	SetTmpParam_At(373, newv)
end
CLuaCopyPropertyFight.SetParamAntiBianHu = function(newv)
	SetTmpParam_At(374, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiBianHu = function(newv)
	SetTmpParam_At(375, newv)
end
CLuaCopyPropertyFight.SetParamMulAntiBianHu = function(newv)
	SetTmpParam_At(376, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceXiaKe = function(newv)
	SetTmpParam_At(377, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceXiaKeMul = function(newv)
	SetTmpParam_At(378, newv)
end
CLuaCopyPropertyFight.SetParamTieTimeChange = function(newv)
	SetTmpParam_At(379, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceYanShi = function(newv)
	SetTmpParam_At(380, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceYanShiMul = function(newv)
	SetTmpParam_At(381, newv)
end
CLuaCopyPropertyFight.SetParamPAType = function(newv)
	SetTmpParam_At(382, newv)
end
CLuaCopyPropertyFight.SetParamMAType = function(newv)
	SetTmpParam_At(383, newv)
end
CLuaCopyPropertyFight.SetParamlifetimeNoReduceRate = function(newv)
	SetTmpParam_At(384, newv)
end
CLuaCopyPropertyFight.SetParamAddLSHpDurgImprove = function(newv)
	SetTmpParam_At(385, newv)
end
CLuaCopyPropertyFight.SetParamAddHpDurgImprove = function(newv)
	SetTmpParam_At(386, newv)
end
CLuaCopyPropertyFight.SetParamAddMpDurgImprove = function(newv)
	SetTmpParam_At(387, newv)
end
CLuaCopyPropertyFight.SetParamFireHurtReduce = function(newv)
	SetTmpParam_At(388, newv)
end
CLuaCopyPropertyFight.SetParamThunderHurtReduce = function(newv)
	SetTmpParam_At(389, newv)
end
CLuaCopyPropertyFight.SetParamIceHurtReduce = function(newv)
	SetTmpParam_At(390, newv)
end
CLuaCopyPropertyFight.SetParamPoisonHurtReduce = function(newv)
	SetTmpParam_At(391, newv)
end
CLuaCopyPropertyFight.SetParamWindHurtReduce = function(newv)
	SetTmpParam_At(392, newv)
end
CLuaCopyPropertyFight.SetParamLightHurtReduce = function(newv)
	SetTmpParam_At(393, newv)
end
CLuaCopyPropertyFight.SetParamIllusionHurtReduce = function(newv)
	SetTmpParam_At(394, newv)
end
CLuaCopyPropertyFight.SetParamFakepDef = function(newv)
	SetTmpParam_At(395, newv)
end
CLuaCopyPropertyFight.SetParamFakemDef = function(newv)
	SetTmpParam_At(396, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceWater = function(newv)
	SetTmpParam_At(397, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceWater = function(newv)
	SetTmpParam_At(398, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhanceWater = function(newv)
	SetTmpParam_At(399, newv)
end
CLuaCopyPropertyFight.SetParamAntiWater = function(newv)
	SetTmpParam_At(400, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiWater = function(newv)
	SetTmpParam_At(401, newv)
end
CLuaCopyPropertyFight.SetParamMulAntiWater = function(newv)
	SetTmpParam_At(402, newv)
end
CLuaCopyPropertyFight.SetParamWaterHurtReduce = function(newv)
	SetTmpParam_At(403, newv)
end
CLuaCopyPropertyFight.SetParamIgnoreAntiWater = function(newv)
	SetTmpParam_At(404, newv)
end
CLuaCopyPropertyFight.SetParamAntiFreeze = function(newv)
	SetTmpParam_At(405, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiFreeze = function(newv)
	SetTmpParam_At(406, newv)
end
CLuaCopyPropertyFight.SetParamMulAntiFreeze = function(newv)
	SetTmpParam_At(407, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceFreeze = function(newv)
	SetTmpParam_At(408, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceFreeze = function(newv)
	SetTmpParam_At(409, newv)
end
CLuaCopyPropertyFight.SetParamMulEnhanceFreeze = function(newv)
	SetTmpParam_At(410, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceHuaHun = function(newv)
	SetTmpParam_At(411, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceHuaHunMul = function(newv)
	SetTmpParam_At(412, newv)
end
CLuaCopyPropertyFight.SetParamTrueSightProb = function(newv)
	SetTmpParam_At(413, newv)
end
CLuaCopyPropertyFight.SetParamLSBBGrade = function(newv)
	SetTmpParam_At(414, newv)
end
CLuaCopyPropertyFight.SetParamLSBBQuality = function(newv)
	SetTmpParam_At(415, newv)
end
CLuaCopyPropertyFight.SetParamLSBBAdjHp = function(newv)
	SetTmpParam_At(416, newv)
end
CLuaCopyPropertyFight.SetParamAdjIgnoreAntiFire = function(newv)
	SetTmpParam_At(417, newv)
end
CLuaCopyPropertyFight.SetParamAdjIgnoreAntiThunder = function(newv)
	SetTmpParam_At(418, newv)
end
CLuaCopyPropertyFight.SetParamAdjIgnoreAntiIce = function(newv)
	SetTmpParam_At(419, newv)
end
CLuaCopyPropertyFight.SetParamAdjIgnoreAntiPoison = function(newv)
	SetTmpParam_At(420, newv)
end
CLuaCopyPropertyFight.SetParamAdjIgnoreAntiWind = function(newv)
	SetTmpParam_At(421, newv)
end
CLuaCopyPropertyFight.SetParamAdjIgnoreAntiLight = function(newv)
	SetTmpParam_At(422, newv)
end
CLuaCopyPropertyFight.SetParamAdjIgnoreAntiIllusion = function(newv)
	SetTmpParam_At(423, newv)
end
CLuaCopyPropertyFight.SetParamIgnoreAntiFireLSMul = function(newv)
	SetTmpParam_At(424, newv)
end
CLuaCopyPropertyFight.SetParamIgnoreAntiThunderLSMul = function(newv)
	SetTmpParam_At(425, newv)
end
CLuaCopyPropertyFight.SetParamIgnoreAntiIceLSMul = function(newv)
	SetTmpParam_At(426, newv)
end
CLuaCopyPropertyFight.SetParamIgnoreAntiPoisonLSMul = function(newv)
	SetTmpParam_At(427, newv)
end
CLuaCopyPropertyFight.SetParamIgnoreAntiWindLSMul = function(newv)
	SetTmpParam_At(428, newv)
end
CLuaCopyPropertyFight.SetParamIgnoreAntiLightLSMul = function(newv)
	SetTmpParam_At(429, newv)
end
CLuaCopyPropertyFight.SetParamIgnoreAntiIllusionLSMul = function(newv)
	SetTmpParam_At(430, newv)
end
CLuaCopyPropertyFight.SetParamAdjIgnoreAntiWater = function(newv)
	SetTmpParam_At(431, newv)
end
CLuaCopyPropertyFight.SetParamIgnoreAntiWaterLSMul = function(newv)
	SetTmpParam_At(432, newv)
end
CLuaCopyPropertyFight.SetParamMulSpeed2 = function(newv)
	SetTmpParam_At(433, newv)
end
CLuaCopyPropertyFight.SetParamMulConsumeMp = function(newv)
	SetTmpParam_At(434, newv)
end
CLuaCopyPropertyFight.SetParamAdjAgi2 = function(newv)
	SetTmpParam_At(435, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiBianHu2 = function(newv)
	SetTmpParam_At(436, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiBind2 = function(newv)
	SetTmpParam_At(437, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiBlind2 = function(newv)
	SetTmpParam_At(438, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiBlock2 = function(newv)
	SetTmpParam_At(439, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiBlockDamage2 = function(newv)
	SetTmpParam_At(440, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiChaos2 = function(newv)
	SetTmpParam_At(441, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiDecelerate2 = function(newv)
	SetTmpParam_At(442, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiDizzy2 = function(newv)
	SetTmpParam_At(443, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiFire2 = function(newv)
	SetTmpParam_At(444, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiFreeze2 = function(newv)
	SetTmpParam_At(445, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiIce2 = function(newv)
	SetTmpParam_At(446, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiIllusion2 = function(newv)
	SetTmpParam_At(447, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiLight2 = function(newv)
	SetTmpParam_At(448, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntimFatal2 = function(newv)
	SetTmpParam_At(449, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntimFatalDamage2 = function(newv)
	SetTmpParam_At(450, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiPetrify2 = function(newv)
	SetTmpParam_At(451, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntipFatal2 = function(newv)
	SetTmpParam_At(452, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntipFatalDamage2 = function(newv)
	SetTmpParam_At(453, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiPoison2 = function(newv)
	SetTmpParam_At(454, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiSilence2 = function(newv)
	SetTmpParam_At(455, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiSleep2 = function(newv)
	SetTmpParam_At(456, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiThunder2 = function(newv)
	SetTmpParam_At(457, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiTie2 = function(newv)
	SetTmpParam_At(458, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiWater2 = function(newv)
	SetTmpParam_At(459, newv)
end
CLuaCopyPropertyFight.SetParamAdjAntiWind2 = function(newv)
	SetTmpParam_At(460, newv)
end
CLuaCopyPropertyFight.SetParamAdjBlock2 = function(newv)
	SetTmpParam_At(461, newv)
end
CLuaCopyPropertyFight.SetParamAdjBlockDamage2 = function(newv)
	SetTmpParam_At(462, newv)
end
CLuaCopyPropertyFight.SetParamAdjCor2 = function(newv)
	SetTmpParam_At(463, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceBianHu2 = function(newv)
	SetTmpParam_At(464, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceBind2 = function(newv)
	SetTmpParam_At(465, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceChaos2 = function(newv)
	SetTmpParam_At(466, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceDizzy2 = function(newv)
	SetTmpParam_At(467, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceFire2 = function(newv)
	SetTmpParam_At(468, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceFreeze2 = function(newv)
	SetTmpParam_At(469, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceIce2 = function(newv)
	SetTmpParam_At(470, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceIllusion2 = function(newv)
	SetTmpParam_At(471, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceLight2 = function(newv)
	SetTmpParam_At(472, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhancePetrify2 = function(newv)
	SetTmpParam_At(473, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhancePoison2 = function(newv)
	SetTmpParam_At(474, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceSilence2 = function(newv)
	SetTmpParam_At(475, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceSleep2 = function(newv)
	SetTmpParam_At(476, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceThunder2 = function(newv)
	SetTmpParam_At(477, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceTie2 = function(newv)
	SetTmpParam_At(478, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceWater2 = function(newv)
	SetTmpParam_At(479, newv)
end
CLuaCopyPropertyFight.SetParamAdjEnhanceWind2 = function(newv)
	SetTmpParam_At(480, newv)
end
CLuaCopyPropertyFight.SetParamAdjIgnoreAntiFire2 = function(newv)
	SetTmpParam_At(481, newv)
end
CLuaCopyPropertyFight.SetParamAdjIgnoreAntiIce2 = function(newv)
	SetTmpParam_At(482, newv)
end
CLuaCopyPropertyFight.SetParamAdjIgnoreAntiIllusion2 = function(newv)
	SetTmpParam_At(483, newv)
end
CLuaCopyPropertyFight.SetParamAdjIgnoreAntiLight2 = function(newv)
	SetTmpParam_At(484, newv)
end
CLuaCopyPropertyFight.SetParamAdjIgnoreAntiPoison2 = function(newv)
	SetTmpParam_At(485, newv)
end
CLuaCopyPropertyFight.SetParamAdjIgnoreAntiThunder2 = function(newv)
	SetTmpParam_At(486, newv)
end
CLuaCopyPropertyFight.SetParamAdjIgnoreAntiWater2 = function(newv)
	SetTmpParam_At(487, newv)
end
CLuaCopyPropertyFight.SetParamAdjIgnoreAntiWind2 = function(newv)
	SetTmpParam_At(488, newv)
end
CLuaCopyPropertyFight.SetParamAdjInt2 = function(newv)
	SetTmpParam_At(489, newv)
end
CLuaCopyPropertyFight.SetParamAdjmDef2 = function(newv)
	SetTmpParam_At(490, newv)
end
CLuaCopyPropertyFight.SetParamAdjmFatal2 = function(newv)
	SetTmpParam_At(491, newv)
end
CLuaCopyPropertyFight.SetParamAdjmFatalDamage2 = function(newv)
	SetTmpParam_At(492, newv)
end
CLuaCopyPropertyFight.SetParamAdjmHit2 = function(newv)
	SetTmpParam_At(493, newv)
end
CLuaCopyPropertyFight.SetParamAdjmHurt2 = function(newv)
	SetTmpParam_At(494, newv)
end
CLuaCopyPropertyFight.SetParamAdjmMiss2 = function(newv)
	SetTmpParam_At(495, newv)
end
CLuaCopyPropertyFight.SetParamAdjmSpeed2 = function(newv)
	SetTmpParam_At(496, newv)
end
CLuaCopyPropertyFight.SetParamAdjpDef2 = function(newv)
	SetTmpParam_At(497, newv)
end
CLuaCopyPropertyFight.SetParamAdjpFatal2 = function(newv)
	SetTmpParam_At(498, newv)
end
CLuaCopyPropertyFight.SetParamAdjpFatalDamage2 = function(newv)
	SetTmpParam_At(499, newv)
end
CLuaCopyPropertyFight.SetParamAdjpHit2 = function(newv)
	SetTmpParam_At(500, newv)
end
CLuaCopyPropertyFight.SetParamAdjpHurt2 = function(newv)
	SetTmpParam_At(501, newv)
end
CLuaCopyPropertyFight.SetParamAdjpMiss2 = function(newv)
	SetTmpParam_At(502, newv)
end
CLuaCopyPropertyFight.SetParamAdjpSpeed2 = function(newv)
	SetTmpParam_At(503, newv)
end
CLuaCopyPropertyFight.SetParamAdjStr2 = function(newv)
	SetTmpParam_At(504, newv)
end
CLuaCopyPropertyFight.SetParamLSpAttMin = function(newv)
	SetTmpParam_At(505, newv)
end
CLuaCopyPropertyFight.SetParamLSpAttMax = function(newv)
	SetTmpParam_At(506, newv)
end
CLuaCopyPropertyFight.SetParamLSmAttMin = function(newv)
	SetTmpParam_At(507, newv)
end
CLuaCopyPropertyFight.SetParamLSmAttMax = function(newv)
	SetTmpParam_At(508, newv)
end
CLuaCopyPropertyFight.SetParamLSpFatal = function(newv)
	SetTmpParam_At(509, newv)
end
CLuaCopyPropertyFight.SetParamLSpFatalDamage = function(newv)
	SetTmpParam_At(510, newv)
end
CLuaCopyPropertyFight.SetParamLSmFatal = function(newv)
	SetTmpParam_At(511, newv)
end
CLuaCopyPropertyFight.SetParamLSmFatalDamage = function(newv)
	SetTmpParam_At(512, newv)
end
CLuaCopyPropertyFight.SetParamLSAntiBlock = function(newv)
	SetTmpParam_At(513, newv)
end
CLuaCopyPropertyFight.SetParamLSAntiBlockDamage = function(newv)
	SetTmpParam_At(514, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiFire = function(newv)
	SetTmpParam_At(515, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiThunder = function(newv)
	SetTmpParam_At(516, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiIce = function(newv)
	SetTmpParam_At(517, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiPoison = function(newv)
	SetTmpParam_At(518, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiWind = function(newv)
	SetTmpParam_At(519, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiLight = function(newv)
	SetTmpParam_At(520, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiIllusion = function(newv)
	SetTmpParam_At(521, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiWater = function(newv)
	SetTmpParam_At(522, newv)
end
CLuaCopyPropertyFight.SetParamLSpAttMin_adj = function(newv)
	SetTmpParam_At(523, newv)
end
CLuaCopyPropertyFight.SetParamLSpAttMax_adj = function(newv)
	SetTmpParam_At(524, newv)
end
CLuaCopyPropertyFight.SetParamLSmAttMin_adj = function(newv)
	SetTmpParam_At(525, newv)
end
CLuaCopyPropertyFight.SetParamLSmAttMax_adj = function(newv)
	SetTmpParam_At(526, newv)
end
CLuaCopyPropertyFight.SetParamLSpFatal_adj = function(newv)
	SetTmpParam_At(527, newv)
end
CLuaCopyPropertyFight.SetParamLSpFatalDamage_adj = function(newv)
	SetTmpParam_At(528, newv)
end
CLuaCopyPropertyFight.SetParamLSmFatal_adj = function(newv)
	SetTmpParam_At(529, newv)
end
CLuaCopyPropertyFight.SetParamLSmFatalDamage_adj = function(newv)
	SetTmpParam_At(530, newv)
end
CLuaCopyPropertyFight.SetParamLSAntiBlock_adj = function(newv)
	SetTmpParam_At(531, newv)
end
CLuaCopyPropertyFight.SetParamLSAntiBlockDamage_adj = function(newv)
	SetTmpParam_At(532, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiFire_adj = function(newv)
	SetTmpParam_At(533, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiThunder_adj = function(newv)
	SetTmpParam_At(534, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiIce_adj = function(newv)
	SetTmpParam_At(535, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiPoison_adj = function(newv)
	SetTmpParam_At(536, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiWind_adj = function(newv)
	SetTmpParam_At(537, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiLight_adj = function(newv)
	SetTmpParam_At(538, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiIllusion_adj = function(newv)
	SetTmpParam_At(539, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiWater_adj = function(newv)
	SetTmpParam_At(540, newv)
end
CLuaCopyPropertyFight.SetParamLSpAttMin_jc = function(newv)
	SetTmpParam_At(541, newv)
end
CLuaCopyPropertyFight.SetParamLSpAttMax_jc = function(newv)
	SetTmpParam_At(542, newv)
end
CLuaCopyPropertyFight.SetParamLSmAttMin_jc = function(newv)
	SetTmpParam_At(543, newv)
end
CLuaCopyPropertyFight.SetParamLSmAttMax_jc = function(newv)
	SetTmpParam_At(544, newv)
end
CLuaCopyPropertyFight.SetParamLSpFatal_jc = function(newv)
	SetTmpParam_At(545, newv)
end
CLuaCopyPropertyFight.SetParamLSpFatalDamage_jc = function(newv)
	SetTmpParam_At(546, newv)
end
CLuaCopyPropertyFight.SetParamLSmFatal_jc = function(newv)
	SetTmpParam_At(547, newv)
end
CLuaCopyPropertyFight.SetParamLSmFatalDamage_jc = function(newv)
	SetTmpParam_At(548, newv)
end
CLuaCopyPropertyFight.SetParamLSAntiBlock_jc = function(newv)
	SetTmpParam_At(549, newv)
end
CLuaCopyPropertyFight.SetParamLSAntiBlockDamage_jc = function(newv)
	SetTmpParam_At(550, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiFire_jc = function(newv)
	SetTmpParam_At(551, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiThunder_jc = function(newv)
	SetTmpParam_At(552, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiIce_jc = function(newv)
	SetTmpParam_At(553, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiPoison_jc = function(newv)
	SetTmpParam_At(554, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiWind_jc = function(newv)
	SetTmpParam_At(555, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiLight_jc = function(newv)
	SetTmpParam_At(556, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiIllusion_jc = function(newv)
	SetTmpParam_At(557, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiWater_jc = function(newv)
	SetTmpParam_At(558, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiFire_mul = function(newv)
	SetTmpParam_At(559, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiThunder_mul = function(newv)
	SetTmpParam_At(560, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiIce_mul = function(newv)
	SetTmpParam_At(561, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiPoison_mul = function(newv)
	SetTmpParam_At(562, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiWind_mul = function(newv)
	SetTmpParam_At(563, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiLight_mul = function(newv)
	SetTmpParam_At(564, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiIllusion_mul = function(newv)
	SetTmpParam_At(565, newv)
end
CLuaCopyPropertyFight.SetParamLSIgnoreAntiWater_mul = function(newv)
	SetTmpParam_At(566, newv)
end
CLuaCopyPropertyFight.SetParamJieBan_Child_QiChangColor = function(newv)
	SetTmpParam_At(567, newv)
end
CLuaCopyPropertyFight.SetParamJieBan_LingShou_Ratio = function(newv)
	SetTmpParam_At(568, newv)
end
CLuaCopyPropertyFight.SetParamJieBan_LingShou_QinMi = function(newv)
	SetTmpParam_At(569, newv)
end
CLuaCopyPropertyFight.SetParamJieBan_LingShou_CorZizhi = function(newv)
	SetTmpParam_At(570, newv)
end
CLuaCopyPropertyFight.SetParamJieBan_LingShou_StaZizhi = function(newv)
	SetTmpParam_At(571, newv)
end
CLuaCopyPropertyFight.SetParamJieBan_LingShou_StrZizhi = function(newv)
	SetTmpParam_At(572, newv)
end
CLuaCopyPropertyFight.SetParamJieBan_LingShou_IntZizhi = function(newv)
	SetTmpParam_At(573, newv)
end
CLuaCopyPropertyFight.SetParamJieBan_LingShou_AgiZizhi = function(newv)
	SetTmpParam_At(574, newv)
end
CLuaCopyPropertyFight.SetParamBuff_DisplayValue1 = function(newv)
	SetTmpParam_At(575, newv)
end
CLuaCopyPropertyFight.SetParamBuff_DisplayValue2 = function(newv)
	SetTmpParam_At(576, newv)
end
CLuaCopyPropertyFight.SetParamBuff_DisplayValue3 = function(newv)
	SetTmpParam_At(577, newv)
end
CLuaCopyPropertyFight.SetParamBuff_DisplayValue4 = function(newv)
	SetTmpParam_At(578, newv)
end
CLuaCopyPropertyFight.SetParamBuff_DisplayValue5 = function(newv)
	SetTmpParam_At(579, newv)
end
CLuaCopyPropertyFight.SetParamLSNature = function(newv)
	SetTmpParam_At(580, newv)
end
CLuaCopyPropertyFight.SetParamPrayMulti = function(newv)
	SetTmpParam_At(581, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceYingLing = function(newv)
	SetTmpParam_At(582, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceYingLingMul = function(newv)
	SetTmpParam_At(583, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceFlag = function(newv)
	SetTmpParam_At(584, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceFlagMul = function(newv)
	SetTmpParam_At(585, newv)
end
CLuaCopyPropertyFight.SetParamObjectType = function(newv)
	SetTmpParam_At(586, newv)
end
CLuaCopyPropertyFight.SetParamMonsterType = function(newv)
	SetTmpParam_At(587, newv)
end
CLuaCopyPropertyFight.SetParamFightPropType = function(newv)
	SetTmpParam_At(588, newv)
end
CLuaCopyPropertyFight.SetParamPlayHpFull = function(newv)
	SetTmpParam_At(589, newv)
end
CLuaCopyPropertyFight.SetParamPlayHp = function(newv)
	SetTmpParam_At(590, newv)
end
CLuaCopyPropertyFight.SetParamPlayAtt = function(newv)
	SetTmpParam_At(591, newv)
end
CLuaCopyPropertyFight.SetParamPlayDef = function(newv)
	SetTmpParam_At(592, newv)
end
CLuaCopyPropertyFight.SetParamPlayHit = function(newv)
	SetTmpParam_At(593, newv)
end
CLuaCopyPropertyFight.SetParamPlayMiss = function(newv)
	SetTmpParam_At(594, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceDieKe = function(newv)
	SetTmpParam_At(595, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceDieKeMul = function(newv)
	SetTmpParam_At(596, newv)
end
CLuaCopyPropertyFight.SetParamDotRemain = function(newv)
	SetTmpParam_At(597, newv)
end
CLuaCopyPropertyFight.SetParamIsConfused = function(newv)
	SetTmpParam_At(598, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceSheShouKefu = function(newv)
	SetTmpParam_At(599, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceJiaShiKefu = function(newv)
	SetTmpParam_At(600, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceDaoKeKefu = function(newv)
	SetTmpParam_At(601, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceXiaKeKefu = function(newv)
	SetTmpParam_At(602, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceFangShiKefu = function(newv)
	SetTmpParam_At(603, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceYiShiKefu = function(newv)
	SetTmpParam_At(604, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceMeiZheKefu = function(newv)
	SetTmpParam_At(605, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceYiRenKefu = function(newv)
	SetTmpParam_At(606, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceYanShiKefu = function(newv)
	SetTmpParam_At(607, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceHuaHunKefu = function(newv)
	SetTmpParam_At(608, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceYingLingKefu = function(newv)
	SetTmpParam_At(609, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceDieKeKefu = function(newv)
	SetTmpParam_At(610, newv)
end
CLuaCopyPropertyFight.SetParamAdjustPermanentCor = function(newv)
	SetTmpParam_At(611, newv)
end
CLuaCopyPropertyFight.SetParamAdjustPermanentSta = function(newv)
	SetTmpParam_At(612, newv)
end
CLuaCopyPropertyFight.SetParamAdjustPermanentStr = function(newv)
	SetTmpParam_At(613, newv)
end
CLuaCopyPropertyFight.SetParamAdjustPermanentInt = function(newv)
	SetTmpParam_At(614, newv)
end
CLuaCopyPropertyFight.SetParamAdjustPermanentAgi = function(newv)
	SetTmpParam_At(615, newv)
end
CLuaCopyPropertyFight.SetParamSoulCoreLevel = function(newv)
	SetTmpParam_At(616, newv)
end
CLuaCopyPropertyFight.SetParamAdjustPermanentMidCor = function(newv)
	SetTmpParam_At(617, newv)
end
CLuaCopyPropertyFight.SetParamAdjustPermanentMidSta = function(newv)
	SetTmpParam_At(618, newv)
end
CLuaCopyPropertyFight.SetParamAdjustPermanentMidStr = function(newv)
	SetTmpParam_At(619, newv)
end
CLuaCopyPropertyFight.SetParamAdjustPermanentMidInt = function(newv)
	SetTmpParam_At(620, newv)
end
CLuaCopyPropertyFight.SetParamAdjustPermanentMidAgi = function(newv)
	SetTmpParam_At(621, newv)
end
CLuaCopyPropertyFight.SetParamSumPermanent = function(newv)
	SetTmpParam_At(622, newv)
end
CLuaCopyPropertyFight.SetParamAntiTian = function(newv)
	SetTmpParam_At(623, newv)
end
CLuaCopyPropertyFight.SetParamAntiEGui = function(newv)
	SetTmpParam_At(624, newv)
end
CLuaCopyPropertyFight.SetParamAntiXiuLuo = function(newv)
	SetTmpParam_At(625, newv)
end
CLuaCopyPropertyFight.SetParamAntiDiYu = function(newv)
	SetTmpParam_At(626, newv)
end
CLuaCopyPropertyFight.SetParamAntiRen = function(newv)
	SetTmpParam_At(627, newv)
end
CLuaCopyPropertyFight.SetParamAntiChuSheng = function(newv)
	SetTmpParam_At(628, newv)
end
CLuaCopyPropertyFight.SetParamAntiBuilding = function(newv)
	SetTmpParam_At(629, newv)
end
CLuaCopyPropertyFight.SetParamAntiZhaoHuan = function(newv)
	SetTmpParam_At(630, newv)
end
CLuaCopyPropertyFight.SetParamAntiLingShou = function(newv)
	SetTmpParam_At(631, newv)
end
CLuaCopyPropertyFight.SetParamAntiBoss = function(newv)
	SetTmpParam_At(632, newv)
end
CLuaCopyPropertyFight.SetParamAntiSheShou = function(newv)
	SetTmpParam_At(633, newv)
end
CLuaCopyPropertyFight.SetParamAntiJiaShi = function(newv)
	SetTmpParam_At(634, newv)
end
CLuaCopyPropertyFight.SetParamAntiDaoKe = function(newv)
	SetTmpParam_At(635, newv)
end
CLuaCopyPropertyFight.SetParamAntiXiaKe = function(newv)
	SetTmpParam_At(636, newv)
end
CLuaCopyPropertyFight.SetParamAntiFangShi = function(newv)
	SetTmpParam_At(637, newv)
end
CLuaCopyPropertyFight.SetParamAntiYiShi = function(newv)
	SetTmpParam_At(638, newv)
end
CLuaCopyPropertyFight.SetParamAntiMeiZhe = function(newv)
	SetTmpParam_At(639, newv)
end
CLuaCopyPropertyFight.SetParamAntiYiRen = function(newv)
	SetTmpParam_At(640, newv)
end
CLuaCopyPropertyFight.SetParamAntiYanShi = function(newv)
	SetTmpParam_At(641, newv)
end
CLuaCopyPropertyFight.SetParamAntiHuaHun = function(newv)
	SetTmpParam_At(642, newv)
end
CLuaCopyPropertyFight.SetParamAntiYingLing = function(newv)
	SetTmpParam_At(643, newv)
end
CLuaCopyPropertyFight.SetParamAntiDieKe = function(newv)
	SetTmpParam_At(644, newv)
end
CLuaCopyPropertyFight.SetParamAntiFlag = function(newv)
	SetTmpParam_At(645, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceZhanKuang = function(newv)
	SetTmpParam_At(646, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceZhanKuangMul = function(newv)
	SetTmpParam_At(647, newv)
end
CLuaCopyPropertyFight.SetParamEnhanceZhanKuangKefu = function(newv)
	SetTmpParam_At(648, newv)
end
CLuaCopyPropertyFight.SetParamAntiZhanKuang = function(newv)
	SetTmpParam_At(649, newv)
end
CLuaCopyPropertyFight.SetParamJumpSpeed = function(newv)
	SetTmpParam_At(650, newv)
end
CLuaCopyPropertyFight.SetParamOriJumpSpeed = function(newv)
	SetTmpParam_At(651, newv)
end
CLuaCopyPropertyFight.SetParamAdjJumpSpeed = function(newv)
	SetTmpParam_At(652, newv)
end
CLuaCopyPropertyFight.SetParamGravityAcceleration = function(newv)
	SetTmpParam_At(653, newv)
end
CLuaCopyPropertyFight.SetParamOriGravityAcceleration = function(newv)
	SetTmpParam_At(654, newv)
end
CLuaCopyPropertyFight.SetParamAdjGravityAcceleration = function(newv)
	SetTmpParam_At(655, newv)
end
CLuaCopyPropertyFight.SetParamHorizontalAcceleration = function(newv)
	SetTmpParam_At(656, newv)
end
CLuaCopyPropertyFight.SetParamOriHorizontalAcceleration = function(newv)
	SetTmpParam_At(657, newv)
end
CLuaCopyPropertyFight.SetParamAdjHorizontalAcceleration = function(newv)
	SetTmpParam_At(658, newv)
end
CLuaCopyPropertyFight.SetParamSwimSpeed = function(newv)
	SetTmpParam_At(659, newv)
end
CLuaCopyPropertyFight.SetParamOriSwimSpeed = function(newv)
	SetTmpParam_At(660, newv)
end
CLuaCopyPropertyFight.SetParamAdjSwimSpeed = function(newv)
	SetTmpParam_At(661, newv)
end
CLuaCopyPropertyFight.SetParamStrengthPoint = function(newv)
	SetTmpParam_At(662, newv)
end
CLuaCopyPropertyFight.SetParamStrengthPointMax = function(newv)
	SetTmpParam_At(663, newv)
end
CLuaCopyPropertyFight.SetParamOriStrengthPointMax = function(newv)
	SetTmpParam_At(664, newv)
end
CLuaCopyPropertyFight.SetParamAdjStrengthPointMax = function(newv)
	SetTmpParam_At(665, newv)
end
CLuaCopyPropertyFight.SetParamAntiMulEnhanceIllusion = function(newv)
	SetTmpParam_At(666, newv)
end
CLuaCopyPropertyFight.SetParamGlideHorizontalSpeedWithUmbrella = function(newv)
	SetTmpParam_At(667, newv)
end
CLuaCopyPropertyFight.SetParamGlideVerticalSpeedWithUmbrella = function(newv)
	SetTmpParam_At(668, newv)
end
CLuaCopyPropertyFight.SetParamAdjpSpeed = function(newv)
	SetTmpParam_At(1001, newv)
end
CLuaCopyPropertyFight.SetParamAdjmSpeed = function(newv)
	SetTmpParam_At(1002, newv)
end
CCopyPropertyFight.m_hookRefreshAll = function(this)
	CLuaCopyPropertyFight.Param = this.Param
	CLuaCopyPropertyFight.TmpParam = this.TmpParam

	CLuaCopyPropertyFight.RefreshParamAntiDecelerate()
	CLuaCopyPropertyFight.RefreshParamAntiAoe()
	
	CLuaCopyPropertyFight.Param = nil
	CLuaCopyPropertyFight.TmpParam = nil
end
SetParamFunctions[EnumCopyFightProp.PermanentCor] = CLuaCopyPropertyFight.SetParamPermanentCor
SetParamFunctions[EnumCopyFightProp.PermanentSta] = CLuaCopyPropertyFight.SetParamPermanentSta
SetParamFunctions[EnumCopyFightProp.PermanentStr] = CLuaCopyPropertyFight.SetParamPermanentStr
SetParamFunctions[EnumCopyFightProp.PermanentInt] = CLuaCopyPropertyFight.SetParamPermanentInt
SetParamFunctions[EnumCopyFightProp.PermanentAgi] = CLuaCopyPropertyFight.SetParamPermanentAgi
SetParamFunctions[EnumCopyFightProp.PermanentHpFull] = CLuaCopyPropertyFight.SetParamPermanentHpFull
SetParamFunctions[EnumCopyFightProp.Hp] = CLuaCopyPropertyFight.SetParamHp
SetParamFunctions[EnumCopyFightProp.Mp] = CLuaCopyPropertyFight.SetParamMp
SetParamFunctions[EnumCopyFightProp.CurrentHpFull] = CLuaCopyPropertyFight.SetParamCurrentHpFull
SetParamFunctions[EnumCopyFightProp.CurrentMpFull] = CLuaCopyPropertyFight.SetParamCurrentMpFull
SetParamFunctions[EnumCopyFightProp.EquipExchangeMF] = CLuaCopyPropertyFight.SetParamEquipExchangeMF
SetParamFunctions[EnumCopyFightProp.RevisePermanentCor] = CLuaCopyPropertyFight.SetParamRevisePermanentCor
SetParamFunctions[EnumCopyFightProp.RevisePermanentSta] = CLuaCopyPropertyFight.SetParamRevisePermanentSta
SetParamFunctions[EnumCopyFightProp.RevisePermanentStr] = CLuaCopyPropertyFight.SetParamRevisePermanentStr
SetParamFunctions[EnumCopyFightProp.RevisePermanentInt] = CLuaCopyPropertyFight.SetParamRevisePermanentInt
SetParamFunctions[EnumCopyFightProp.RevisePermanentAgi] = CLuaCopyPropertyFight.SetParamRevisePermanentAgi
SetParamFunctions[EnumCopyFightProp.Class] = CLuaCopyPropertyFight.SetParamClass
SetParamFunctions[EnumCopyFightProp.Grade] = CLuaCopyPropertyFight.SetParamGrade
SetParamFunctions[EnumCopyFightProp.Cor] = CLuaCopyPropertyFight.SetParamCor
SetParamFunctions[EnumCopyFightProp.AdjCor] = CLuaCopyPropertyFight.SetParamAdjCor
SetParamFunctions[EnumCopyFightProp.MulCor] = CLuaCopyPropertyFight.SetParamMulCor
SetParamFunctions[EnumCopyFightProp.Sta] = CLuaCopyPropertyFight.SetParamSta
SetParamFunctions[EnumCopyFightProp.AdjSta] = CLuaCopyPropertyFight.SetParamAdjSta
SetParamFunctions[EnumCopyFightProp.MulSta] = CLuaCopyPropertyFight.SetParamMulSta
SetParamFunctions[EnumCopyFightProp.Str] = CLuaCopyPropertyFight.SetParamStr
SetParamFunctions[EnumCopyFightProp.AdjStr] = CLuaCopyPropertyFight.SetParamAdjStr
SetParamFunctions[EnumCopyFightProp.MulStr] = CLuaCopyPropertyFight.SetParamMulStr
SetParamFunctions[EnumCopyFightProp.Int] = CLuaCopyPropertyFight.SetParamInt
SetParamFunctions[EnumCopyFightProp.AdjInt] = CLuaCopyPropertyFight.SetParamAdjInt
SetParamFunctions[EnumCopyFightProp.MulInt] = CLuaCopyPropertyFight.SetParamMulInt
SetParamFunctions[EnumCopyFightProp.Agi] = CLuaCopyPropertyFight.SetParamAgi
SetParamFunctions[EnumCopyFightProp.AdjAgi] = CLuaCopyPropertyFight.SetParamAdjAgi
SetParamFunctions[EnumCopyFightProp.MulAgi] = CLuaCopyPropertyFight.SetParamMulAgi
SetParamFunctions[EnumCopyFightProp.HpFull] = CLuaCopyPropertyFight.SetParamHpFull
SetParamFunctions[EnumCopyFightProp.AdjHpFull] = CLuaCopyPropertyFight.SetParamAdjHpFull
SetParamFunctions[EnumCopyFightProp.MulHpFull] = CLuaCopyPropertyFight.SetParamMulHpFull
SetParamFunctions[EnumCopyFightProp.HpRecover] = CLuaCopyPropertyFight.SetParamHpRecover
SetParamFunctions[EnumCopyFightProp.AdjHpRecover] = CLuaCopyPropertyFight.SetParamAdjHpRecover
SetParamFunctions[EnumCopyFightProp.MulHpRecover] = CLuaCopyPropertyFight.SetParamMulHpRecover
SetParamFunctions[EnumCopyFightProp.MpFull] = CLuaCopyPropertyFight.SetParamMpFull
SetParamFunctions[EnumCopyFightProp.AdjMpFull] = CLuaCopyPropertyFight.SetParamAdjMpFull
SetParamFunctions[EnumCopyFightProp.MulMpFull] = CLuaCopyPropertyFight.SetParamMulMpFull
SetParamFunctions[EnumCopyFightProp.MpRecover] = CLuaCopyPropertyFight.SetParamMpRecover
SetParamFunctions[EnumCopyFightProp.AdjMpRecover] = CLuaCopyPropertyFight.SetParamAdjMpRecover
SetParamFunctions[EnumCopyFightProp.MulMpRecover] = CLuaCopyPropertyFight.SetParamMulMpRecover
SetParamFunctions[EnumCopyFightProp.pAttMin] = CLuaCopyPropertyFight.SetParampAttMin
SetParamFunctions[EnumCopyFightProp.AdjpAttMin] = CLuaCopyPropertyFight.SetParamAdjpAttMin
SetParamFunctions[EnumCopyFightProp.MulpAttMin] = CLuaCopyPropertyFight.SetParamMulpAttMin
SetParamFunctions[EnumCopyFightProp.pAttMax] = CLuaCopyPropertyFight.SetParampAttMax
SetParamFunctions[EnumCopyFightProp.AdjpAttMax] = CLuaCopyPropertyFight.SetParamAdjpAttMax
SetParamFunctions[EnumCopyFightProp.MulpAttMax] = CLuaCopyPropertyFight.SetParamMulpAttMax
SetParamFunctions[EnumCopyFightProp.pHit] = CLuaCopyPropertyFight.SetParampHit
SetParamFunctions[EnumCopyFightProp.AdjpHit] = CLuaCopyPropertyFight.SetParamAdjpHit
SetParamFunctions[EnumCopyFightProp.MulpHit] = CLuaCopyPropertyFight.SetParamMulpHit
SetParamFunctions[EnumCopyFightProp.pMiss] = CLuaCopyPropertyFight.SetParampMiss
SetParamFunctions[EnumCopyFightProp.AdjpMiss] = CLuaCopyPropertyFight.SetParamAdjpMiss
SetParamFunctions[EnumCopyFightProp.MulpMiss] = CLuaCopyPropertyFight.SetParamMulpMiss
SetParamFunctions[EnumCopyFightProp.pDef] = CLuaCopyPropertyFight.SetParampDef
SetParamFunctions[EnumCopyFightProp.AdjpDef] = CLuaCopyPropertyFight.SetParamAdjpDef
SetParamFunctions[EnumCopyFightProp.MulpDef] = CLuaCopyPropertyFight.SetParamMulpDef
SetParamFunctions[EnumCopyFightProp.AdjpHurt] = CLuaCopyPropertyFight.SetParamAdjpHurt
SetParamFunctions[EnumCopyFightProp.MulpHurt] = CLuaCopyPropertyFight.SetParamMulpHurt
SetParamFunctions[EnumCopyFightProp.pSpeed] = CLuaCopyPropertyFight.SetParampSpeed
SetParamFunctions[EnumCopyFightProp.OripSpeed] = CLuaCopyPropertyFight.SetParamOripSpeed
SetParamFunctions[EnumCopyFightProp.MulpSpeed] = CLuaCopyPropertyFight.SetParamMulpSpeed
SetParamFunctions[EnumCopyFightProp.pFatal] = CLuaCopyPropertyFight.SetParampFatal
SetParamFunctions[EnumCopyFightProp.AdjpFatal] = CLuaCopyPropertyFight.SetParamAdjpFatal
SetParamFunctions[EnumCopyFightProp.AntipFatal] = CLuaCopyPropertyFight.SetParamAntipFatal
SetParamFunctions[EnumCopyFightProp.AdjAntipFatal] = CLuaCopyPropertyFight.SetParamAdjAntipFatal
SetParamFunctions[EnumCopyFightProp.pFatalDamage] = CLuaCopyPropertyFight.SetParampFatalDamage
SetParamFunctions[EnumCopyFightProp.AdjpFatalDamage] = CLuaCopyPropertyFight.SetParamAdjpFatalDamage
SetParamFunctions[EnumCopyFightProp.AntipFatalDamage] = CLuaCopyPropertyFight.SetParamAntipFatalDamage
SetParamFunctions[EnumCopyFightProp.AdjAntipFatalDamage] = CLuaCopyPropertyFight.SetParamAdjAntipFatalDamage
SetParamFunctions[EnumCopyFightProp.Block] = CLuaCopyPropertyFight.SetParamBlock
SetParamFunctions[EnumCopyFightProp.AdjBlock] = CLuaCopyPropertyFight.SetParamAdjBlock
SetParamFunctions[EnumCopyFightProp.MulBlock] = CLuaCopyPropertyFight.SetParamMulBlock
SetParamFunctions[EnumCopyFightProp.BlockDamage] = CLuaCopyPropertyFight.SetParamBlockDamage
SetParamFunctions[EnumCopyFightProp.AdjBlockDamage] = CLuaCopyPropertyFight.SetParamAdjBlockDamage
SetParamFunctions[EnumCopyFightProp.AntiBlock] = CLuaCopyPropertyFight.SetParamAntiBlock
SetParamFunctions[EnumCopyFightProp.AdjAntiBlock] = CLuaCopyPropertyFight.SetParamAdjAntiBlock
SetParamFunctions[EnumCopyFightProp.AntiBlockDamage] = CLuaCopyPropertyFight.SetParamAntiBlockDamage
SetParamFunctions[EnumCopyFightProp.AdjAntiBlockDamage] = CLuaCopyPropertyFight.SetParamAdjAntiBlockDamage
SetParamFunctions[EnumCopyFightProp.mAttMin] = CLuaCopyPropertyFight.SetParammAttMin
SetParamFunctions[EnumCopyFightProp.AdjmAttMin] = CLuaCopyPropertyFight.SetParamAdjmAttMin
SetParamFunctions[EnumCopyFightProp.MulmAttMin] = CLuaCopyPropertyFight.SetParamMulmAttMin
SetParamFunctions[EnumCopyFightProp.mAttMax] = CLuaCopyPropertyFight.SetParammAttMax
SetParamFunctions[EnumCopyFightProp.AdjmAttMax] = CLuaCopyPropertyFight.SetParamAdjmAttMax
SetParamFunctions[EnumCopyFightProp.MulmAttMax] = CLuaCopyPropertyFight.SetParamMulmAttMax
SetParamFunctions[EnumCopyFightProp.mHit] = CLuaCopyPropertyFight.SetParammHit
SetParamFunctions[EnumCopyFightProp.AdjmHit] = CLuaCopyPropertyFight.SetParamAdjmHit
SetParamFunctions[EnumCopyFightProp.MulmHit] = CLuaCopyPropertyFight.SetParamMulmHit
SetParamFunctions[EnumCopyFightProp.mMiss] = CLuaCopyPropertyFight.SetParammMiss
SetParamFunctions[EnumCopyFightProp.AdjmMiss] = CLuaCopyPropertyFight.SetParamAdjmMiss
SetParamFunctions[EnumCopyFightProp.MulmMiss] = CLuaCopyPropertyFight.SetParamMulmMiss
SetParamFunctions[EnumCopyFightProp.mDef] = CLuaCopyPropertyFight.SetParammDef
SetParamFunctions[EnumCopyFightProp.AdjmDef] = CLuaCopyPropertyFight.SetParamAdjmDef
SetParamFunctions[EnumCopyFightProp.MulmDef] = CLuaCopyPropertyFight.SetParamMulmDef
SetParamFunctions[EnumCopyFightProp.AdjmHurt] = CLuaCopyPropertyFight.SetParamAdjmHurt
SetParamFunctions[EnumCopyFightProp.MulmHurt] = CLuaCopyPropertyFight.SetParamMulmHurt
SetParamFunctions[EnumCopyFightProp.mSpeed] = CLuaCopyPropertyFight.SetParammSpeed
SetParamFunctions[EnumCopyFightProp.OrimSpeed] = CLuaCopyPropertyFight.SetParamOrimSpeed
SetParamFunctions[EnumCopyFightProp.MulmSpeed] = CLuaCopyPropertyFight.SetParamMulmSpeed
SetParamFunctions[EnumCopyFightProp.mFatal] = CLuaCopyPropertyFight.SetParammFatal
SetParamFunctions[EnumCopyFightProp.AdjmFatal] = CLuaCopyPropertyFight.SetParamAdjmFatal
SetParamFunctions[EnumCopyFightProp.AntimFatal] = CLuaCopyPropertyFight.SetParamAntimFatal
SetParamFunctions[EnumCopyFightProp.AdjAntimFatal] = CLuaCopyPropertyFight.SetParamAdjAntimFatal
SetParamFunctions[EnumCopyFightProp.mFatalDamage] = CLuaCopyPropertyFight.SetParammFatalDamage
SetParamFunctions[EnumCopyFightProp.AdjmFatalDamage] = CLuaCopyPropertyFight.SetParamAdjmFatalDamage
SetParamFunctions[EnumCopyFightProp.AntimFatalDamage] = CLuaCopyPropertyFight.SetParamAntimFatalDamage
SetParamFunctions[EnumCopyFightProp.AdjAntimFatalDamage] = CLuaCopyPropertyFight.SetParamAdjAntimFatalDamage
SetParamFunctions[EnumCopyFightProp.EnhanceFire] = CLuaCopyPropertyFight.SetParamEnhanceFire
SetParamFunctions[EnumCopyFightProp.AdjEnhanceFire] = CLuaCopyPropertyFight.SetParamAdjEnhanceFire
SetParamFunctions[EnumCopyFightProp.MulEnhanceFire] = CLuaCopyPropertyFight.SetParamMulEnhanceFire
SetParamFunctions[EnumCopyFightProp.EnhanceThunder] = CLuaCopyPropertyFight.SetParamEnhanceThunder
SetParamFunctions[EnumCopyFightProp.AdjEnhanceThunder] = CLuaCopyPropertyFight.SetParamAdjEnhanceThunder
SetParamFunctions[EnumCopyFightProp.MulEnhanceThunder] = CLuaCopyPropertyFight.SetParamMulEnhanceThunder
SetParamFunctions[EnumCopyFightProp.EnhanceIce] = CLuaCopyPropertyFight.SetParamEnhanceIce
SetParamFunctions[EnumCopyFightProp.AdjEnhanceIce] = CLuaCopyPropertyFight.SetParamAdjEnhanceIce
SetParamFunctions[EnumCopyFightProp.MulEnhanceIce] = CLuaCopyPropertyFight.SetParamMulEnhanceIce
SetParamFunctions[EnumCopyFightProp.EnhancePoison] = CLuaCopyPropertyFight.SetParamEnhancePoison
SetParamFunctions[EnumCopyFightProp.AdjEnhancePoison] = CLuaCopyPropertyFight.SetParamAdjEnhancePoison
SetParamFunctions[EnumCopyFightProp.MulEnhancePoison] = CLuaCopyPropertyFight.SetParamMulEnhancePoison
SetParamFunctions[EnumCopyFightProp.EnhanceWind] = CLuaCopyPropertyFight.SetParamEnhanceWind
SetParamFunctions[EnumCopyFightProp.AdjEnhanceWind] = CLuaCopyPropertyFight.SetParamAdjEnhanceWind
SetParamFunctions[EnumCopyFightProp.MulEnhanceWind] = CLuaCopyPropertyFight.SetParamMulEnhanceWind
SetParamFunctions[EnumCopyFightProp.EnhanceLight] = CLuaCopyPropertyFight.SetParamEnhanceLight
SetParamFunctions[EnumCopyFightProp.AdjEnhanceLight] = CLuaCopyPropertyFight.SetParamAdjEnhanceLight
SetParamFunctions[EnumCopyFightProp.MulEnhanceLight] = CLuaCopyPropertyFight.SetParamMulEnhanceLight
SetParamFunctions[EnumCopyFightProp.EnhanceIllusion] = CLuaCopyPropertyFight.SetParamEnhanceIllusion
SetParamFunctions[EnumCopyFightProp.AdjEnhanceIllusion] = CLuaCopyPropertyFight.SetParamAdjEnhanceIllusion
SetParamFunctions[EnumCopyFightProp.MulEnhanceIllusion] = CLuaCopyPropertyFight.SetParamMulEnhanceIllusion
SetParamFunctions[EnumCopyFightProp.AntiFire] = CLuaCopyPropertyFight.SetParamAntiFire
SetParamFunctions[EnumCopyFightProp.AdjAntiFire] = CLuaCopyPropertyFight.SetParamAdjAntiFire
SetParamFunctions[EnumCopyFightProp.MulAntiFire] = CLuaCopyPropertyFight.SetParamMulAntiFire
SetParamFunctions[EnumCopyFightProp.AntiThunder] = CLuaCopyPropertyFight.SetParamAntiThunder
SetParamFunctions[EnumCopyFightProp.AdjAntiThunder] = CLuaCopyPropertyFight.SetParamAdjAntiThunder
SetParamFunctions[EnumCopyFightProp.MulAntiThunder] = CLuaCopyPropertyFight.SetParamMulAntiThunder
SetParamFunctions[EnumCopyFightProp.AntiIce] = CLuaCopyPropertyFight.SetParamAntiIce
SetParamFunctions[EnumCopyFightProp.AdjAntiIce] = CLuaCopyPropertyFight.SetParamAdjAntiIce
SetParamFunctions[EnumCopyFightProp.MulAntiIce] = CLuaCopyPropertyFight.SetParamMulAntiIce
SetParamFunctions[EnumCopyFightProp.AntiPoison] = CLuaCopyPropertyFight.SetParamAntiPoison
SetParamFunctions[EnumCopyFightProp.AdjAntiPoison] = CLuaCopyPropertyFight.SetParamAdjAntiPoison
SetParamFunctions[EnumCopyFightProp.MulAntiPoison] = CLuaCopyPropertyFight.SetParamMulAntiPoison
SetParamFunctions[EnumCopyFightProp.AntiWind] = CLuaCopyPropertyFight.SetParamAntiWind
SetParamFunctions[EnumCopyFightProp.AdjAntiWind] = CLuaCopyPropertyFight.SetParamAdjAntiWind
SetParamFunctions[EnumCopyFightProp.MulAntiWind] = CLuaCopyPropertyFight.SetParamMulAntiWind
SetParamFunctions[EnumCopyFightProp.AntiLight] = CLuaCopyPropertyFight.SetParamAntiLight
SetParamFunctions[EnumCopyFightProp.AdjAntiLight] = CLuaCopyPropertyFight.SetParamAdjAntiLight
SetParamFunctions[EnumCopyFightProp.MulAntiLight] = CLuaCopyPropertyFight.SetParamMulAntiLight
SetParamFunctions[EnumCopyFightProp.AntiIllusion] = CLuaCopyPropertyFight.SetParamAntiIllusion
SetParamFunctions[EnumCopyFightProp.AdjAntiIllusion] = CLuaCopyPropertyFight.SetParamAdjAntiIllusion
SetParamFunctions[EnumCopyFightProp.MulAntiIllusion] = CLuaCopyPropertyFight.SetParamMulAntiIllusion
SetParamFunctions[EnumCopyFightProp.IgnoreAntiFire] = CLuaCopyPropertyFight.SetParamIgnoreAntiFire
SetParamFunctions[EnumCopyFightProp.IgnoreAntiThunder] = CLuaCopyPropertyFight.SetParamIgnoreAntiThunder
SetParamFunctions[EnumCopyFightProp.IgnoreAntiIce] = CLuaCopyPropertyFight.SetParamIgnoreAntiIce
SetParamFunctions[EnumCopyFightProp.IgnoreAntiPoison] = CLuaCopyPropertyFight.SetParamIgnoreAntiPoison
SetParamFunctions[EnumCopyFightProp.IgnoreAntiWind] = CLuaCopyPropertyFight.SetParamIgnoreAntiWind
SetParamFunctions[EnumCopyFightProp.IgnoreAntiLight] = CLuaCopyPropertyFight.SetParamIgnoreAntiLight
SetParamFunctions[EnumCopyFightProp.IgnoreAntiIllusion] = CLuaCopyPropertyFight.SetParamIgnoreAntiIllusion
SetParamFunctions[EnumCopyFightProp.EnhanceDizzy] = CLuaCopyPropertyFight.SetParamEnhanceDizzy
SetParamFunctions[EnumCopyFightProp.AdjEnhanceDizzy] = CLuaCopyPropertyFight.SetParamAdjEnhanceDizzy
SetParamFunctions[EnumCopyFightProp.MulEnhanceDizzy] = CLuaCopyPropertyFight.SetParamMulEnhanceDizzy
SetParamFunctions[EnumCopyFightProp.EnhanceSleep] = CLuaCopyPropertyFight.SetParamEnhanceSleep
SetParamFunctions[EnumCopyFightProp.AdjEnhanceSleep] = CLuaCopyPropertyFight.SetParamAdjEnhanceSleep
SetParamFunctions[EnumCopyFightProp.MulEnhanceSleep] = CLuaCopyPropertyFight.SetParamMulEnhanceSleep
SetParamFunctions[EnumCopyFightProp.EnhanceChaos] = CLuaCopyPropertyFight.SetParamEnhanceChaos
SetParamFunctions[EnumCopyFightProp.AdjEnhanceChaos] = CLuaCopyPropertyFight.SetParamAdjEnhanceChaos
SetParamFunctions[EnumCopyFightProp.MulEnhanceChaos] = CLuaCopyPropertyFight.SetParamMulEnhanceChaos
SetParamFunctions[EnumCopyFightProp.EnhanceBind] = CLuaCopyPropertyFight.SetParamEnhanceBind
SetParamFunctions[EnumCopyFightProp.AdjEnhanceBind] = CLuaCopyPropertyFight.SetParamAdjEnhanceBind
SetParamFunctions[EnumCopyFightProp.MulEnhanceBind] = CLuaCopyPropertyFight.SetParamMulEnhanceBind
SetParamFunctions[EnumCopyFightProp.EnhanceSilence] = CLuaCopyPropertyFight.SetParamEnhanceSilence
SetParamFunctions[EnumCopyFightProp.AdjEnhanceSilence] = CLuaCopyPropertyFight.SetParamAdjEnhanceSilence
SetParamFunctions[EnumCopyFightProp.MulEnhanceSilence] = CLuaCopyPropertyFight.SetParamMulEnhanceSilence
SetParamFunctions[EnumCopyFightProp.AntiDizzy] = CLuaCopyPropertyFight.SetParamAntiDizzy
SetParamFunctions[EnumCopyFightProp.AdjAntiDizzy] = CLuaCopyPropertyFight.SetParamAdjAntiDizzy
SetParamFunctions[EnumCopyFightProp.MulAntiDizzy] = CLuaCopyPropertyFight.SetParamMulAntiDizzy
SetParamFunctions[EnumCopyFightProp.AntiSleep] = CLuaCopyPropertyFight.SetParamAntiSleep
SetParamFunctions[EnumCopyFightProp.AdjAntiSleep] = CLuaCopyPropertyFight.SetParamAdjAntiSleep
SetParamFunctions[EnumCopyFightProp.MulAntiSleep] = CLuaCopyPropertyFight.SetParamMulAntiSleep
SetParamFunctions[EnumCopyFightProp.AntiChaos] = CLuaCopyPropertyFight.SetParamAntiChaos
SetParamFunctions[EnumCopyFightProp.AdjAntiChaos] = CLuaCopyPropertyFight.SetParamAdjAntiChaos
SetParamFunctions[EnumCopyFightProp.MulAntiChaos] = CLuaCopyPropertyFight.SetParamMulAntiChaos
SetParamFunctions[EnumCopyFightProp.AntiBind] = CLuaCopyPropertyFight.SetParamAntiBind
SetParamFunctions[EnumCopyFightProp.AdjAntiBind] = CLuaCopyPropertyFight.SetParamAdjAntiBind
SetParamFunctions[EnumCopyFightProp.MulAntiBind] = CLuaCopyPropertyFight.SetParamMulAntiBind
SetParamFunctions[EnumCopyFightProp.AntiSilence] = CLuaCopyPropertyFight.SetParamAntiSilence
SetParamFunctions[EnumCopyFightProp.AdjAntiSilence] = CLuaCopyPropertyFight.SetParamAdjAntiSilence
SetParamFunctions[EnumCopyFightProp.MulAntiSilence] = CLuaCopyPropertyFight.SetParamMulAntiSilence
SetParamFunctions[EnumCopyFightProp.DizzyTimeChange] = CLuaCopyPropertyFight.SetParamDizzyTimeChange
SetParamFunctions[EnumCopyFightProp.SleepTimeChange] = CLuaCopyPropertyFight.SetParamSleepTimeChange
SetParamFunctions[EnumCopyFightProp.ChaosTimeChange] = CLuaCopyPropertyFight.SetParamChaosTimeChange
SetParamFunctions[EnumCopyFightProp.BindTimeChange] = CLuaCopyPropertyFight.SetParamBindTimeChange
SetParamFunctions[EnumCopyFightProp.SilenceTimeChange] = CLuaCopyPropertyFight.SetParamSilenceTimeChange
SetParamFunctions[EnumCopyFightProp.BianhuTimeChange] = CLuaCopyPropertyFight.SetParamBianhuTimeChange
SetParamFunctions[EnumCopyFightProp.FreezeTimeChange] = CLuaCopyPropertyFight.SetParamFreezeTimeChange
SetParamFunctions[EnumCopyFightProp.PetrifyTimeChange] = CLuaCopyPropertyFight.SetParamPetrifyTimeChange
SetParamFunctions[EnumCopyFightProp.EnhanceHeal] = CLuaCopyPropertyFight.SetParamEnhanceHeal
SetParamFunctions[EnumCopyFightProp.AdjEnhanceHeal] = CLuaCopyPropertyFight.SetParamAdjEnhanceHeal
SetParamFunctions[EnumCopyFightProp.MulEnhanceHeal] = CLuaCopyPropertyFight.SetParamMulEnhanceHeal
SetParamFunctions[EnumCopyFightProp.Speed] = CLuaCopyPropertyFight.SetParamSpeed
SetParamFunctions[EnumCopyFightProp.OriSpeed] = CLuaCopyPropertyFight.SetParamOriSpeed
SetParamFunctions[EnumCopyFightProp.AdjSpeed] = CLuaCopyPropertyFight.SetParamAdjSpeed
SetParamFunctions[EnumCopyFightProp.MulSpeed] = CLuaCopyPropertyFight.SetParamMulSpeed
SetParamFunctions[EnumCopyFightProp.MF] = CLuaCopyPropertyFight.SetParamMF
SetParamFunctions[EnumCopyFightProp.AdjMF] = CLuaCopyPropertyFight.SetParamAdjMF
SetParamFunctions[EnumCopyFightProp.Range] = CLuaCopyPropertyFight.SetParamRange
SetParamFunctions[EnumCopyFightProp.AdjRange] = CLuaCopyPropertyFight.SetParamAdjRange
SetParamFunctions[EnumCopyFightProp.HatePlus] = CLuaCopyPropertyFight.SetParamHatePlus
SetParamFunctions[EnumCopyFightProp.AdjHatePlus] = CLuaCopyPropertyFight.SetParamAdjHatePlus
SetParamFunctions[EnumCopyFightProp.HateDecrease] = CLuaCopyPropertyFight.SetParamHateDecrease
SetParamFunctions[EnumCopyFightProp.Invisible] = CLuaCopyPropertyFight.SetParamInvisible
SetParamFunctions[EnumCopyFightProp.AdjInvisible] = CLuaCopyPropertyFight.SetParamAdjInvisible
SetParamFunctions[EnumCopyFightProp.TrueSight] = CLuaCopyPropertyFight.SetParamTrueSight
SetParamFunctions[EnumCopyFightProp.OriTrueSight] = CLuaCopyPropertyFight.SetParamOriTrueSight
SetParamFunctions[EnumCopyFightProp.AdjTrueSight] = CLuaCopyPropertyFight.SetParamAdjTrueSight
SetParamFunctions[EnumCopyFightProp.EyeSight] = CLuaCopyPropertyFight.SetParamEyeSight
SetParamFunctions[EnumCopyFightProp.OriEyeSight] = CLuaCopyPropertyFight.SetParamOriEyeSight
SetParamFunctions[EnumCopyFightProp.AdjEyeSight] = CLuaCopyPropertyFight.SetParamAdjEyeSight
SetParamFunctions[EnumCopyFightProp.EnhanceTian] = CLuaCopyPropertyFight.SetParamEnhanceTian
SetParamFunctions[EnumCopyFightProp.EnhanceTianMul] = CLuaCopyPropertyFight.SetParamEnhanceTianMul
SetParamFunctions[EnumCopyFightProp.EnhanceEGui] = CLuaCopyPropertyFight.SetParamEnhanceEGui
SetParamFunctions[EnumCopyFightProp.EnhanceEGuiMul] = CLuaCopyPropertyFight.SetParamEnhanceEGuiMul
SetParamFunctions[EnumCopyFightProp.EnhanceXiuLuo] = CLuaCopyPropertyFight.SetParamEnhanceXiuLuo
SetParamFunctions[EnumCopyFightProp.EnhanceXiuLuoMul] = CLuaCopyPropertyFight.SetParamEnhanceXiuLuoMul
SetParamFunctions[EnumCopyFightProp.EnhanceDiYu] = CLuaCopyPropertyFight.SetParamEnhanceDiYu
SetParamFunctions[EnumCopyFightProp.EnhanceDiYuMul] = CLuaCopyPropertyFight.SetParamEnhanceDiYuMul
SetParamFunctions[EnumCopyFightProp.EnhanceRen] = CLuaCopyPropertyFight.SetParamEnhanceRen
SetParamFunctions[EnumCopyFightProp.EnhanceRenMul] = CLuaCopyPropertyFight.SetParamEnhanceRenMul
SetParamFunctions[EnumCopyFightProp.EnhanceChuSheng] = CLuaCopyPropertyFight.SetParamEnhanceChuSheng
SetParamFunctions[EnumCopyFightProp.EnhanceChuShengMul] = CLuaCopyPropertyFight.SetParamEnhanceChuShengMul
SetParamFunctions[EnumCopyFightProp.EnhanceBuilding] = CLuaCopyPropertyFight.SetParamEnhanceBuilding
SetParamFunctions[EnumCopyFightProp.EnhanceBuildingMul] = CLuaCopyPropertyFight.SetParamEnhanceBuildingMul
SetParamFunctions[EnumCopyFightProp.EnhanceBoss] = CLuaCopyPropertyFight.SetParamEnhanceBoss
SetParamFunctions[EnumCopyFightProp.EnhanceBossMul] = CLuaCopyPropertyFight.SetParamEnhanceBossMul
SetParamFunctions[EnumCopyFightProp.EnhanceSheShou] = CLuaCopyPropertyFight.SetParamEnhanceSheShou
SetParamFunctions[EnumCopyFightProp.EnhanceSheShouMul] = CLuaCopyPropertyFight.SetParamEnhanceSheShouMul
SetParamFunctions[EnumCopyFightProp.EnhanceJiaShi] = CLuaCopyPropertyFight.SetParamEnhanceJiaShi
SetParamFunctions[EnumCopyFightProp.EnhanceJiaShiMul] = CLuaCopyPropertyFight.SetParamEnhanceJiaShiMul
SetParamFunctions[EnumCopyFightProp.EnhanceFangShi] = CLuaCopyPropertyFight.SetParamEnhanceFangShi
SetParamFunctions[EnumCopyFightProp.EnhanceFangShiMul] = CLuaCopyPropertyFight.SetParamEnhanceFangShiMul
SetParamFunctions[EnumCopyFightProp.EnhanceYiShi] = CLuaCopyPropertyFight.SetParamEnhanceYiShi
SetParamFunctions[EnumCopyFightProp.EnhanceYiShiMul] = CLuaCopyPropertyFight.SetParamEnhanceYiShiMul
SetParamFunctions[EnumCopyFightProp.EnhanceMeiZhe] = CLuaCopyPropertyFight.SetParamEnhanceMeiZhe
SetParamFunctions[EnumCopyFightProp.EnhanceMeiZheMul] = CLuaCopyPropertyFight.SetParamEnhanceMeiZheMul
SetParamFunctions[EnumCopyFightProp.EnhanceYiRen] = CLuaCopyPropertyFight.SetParamEnhanceYiRen
SetParamFunctions[EnumCopyFightProp.EnhanceYiRenMul] = CLuaCopyPropertyFight.SetParamEnhanceYiRenMul
SetParamFunctions[EnumCopyFightProp.IgnorepDef] = CLuaCopyPropertyFight.SetParamIgnorepDef
SetParamFunctions[EnumCopyFightProp.IgnoremDef] = CLuaCopyPropertyFight.SetParamIgnoremDef
SetParamFunctions[EnumCopyFightProp.EnhanceZhaoHuan] = CLuaCopyPropertyFight.SetParamEnhanceZhaoHuan
SetParamFunctions[EnumCopyFightProp.EnhanceZhaoHuanMul] = CLuaCopyPropertyFight.SetParamEnhanceZhaoHuanMul
SetParamFunctions[EnumCopyFightProp.DizzyProbability] = CLuaCopyPropertyFight.SetParamDizzyProbability
SetParamFunctions[EnumCopyFightProp.SleepProbability] = CLuaCopyPropertyFight.SetParamSleepProbability
SetParamFunctions[EnumCopyFightProp.ChaosProbability] = CLuaCopyPropertyFight.SetParamChaosProbability
SetParamFunctions[EnumCopyFightProp.BindProbability] = CLuaCopyPropertyFight.SetParamBindProbability
SetParamFunctions[EnumCopyFightProp.SilenceProbability] = CLuaCopyPropertyFight.SetParamSilenceProbability
SetParamFunctions[EnumCopyFightProp.HpFullPow2] = CLuaCopyPropertyFight.SetParamHpFullPow2
SetParamFunctions[EnumCopyFightProp.HpFullPow1] = CLuaCopyPropertyFight.SetParamHpFullPow1
SetParamFunctions[EnumCopyFightProp.HpFullInit] = CLuaCopyPropertyFight.SetParamHpFullInit
SetParamFunctions[EnumCopyFightProp.MpFullPow1] = CLuaCopyPropertyFight.SetParamMpFullPow1
SetParamFunctions[EnumCopyFightProp.MpFullInit] = CLuaCopyPropertyFight.SetParamMpFullInit
SetParamFunctions[EnumCopyFightProp.MpRecoverPow1] = CLuaCopyPropertyFight.SetParamMpRecoverPow1
SetParamFunctions[EnumCopyFightProp.MpRecoverInit] = CLuaCopyPropertyFight.SetParamMpRecoverInit
SetParamFunctions[EnumCopyFightProp.PAttMinPow2] = CLuaCopyPropertyFight.SetParamPAttMinPow2
SetParamFunctions[EnumCopyFightProp.PAttMinPow1] = CLuaCopyPropertyFight.SetParamPAttMinPow1
SetParamFunctions[EnumCopyFightProp.PAttMinInit] = CLuaCopyPropertyFight.SetParamPAttMinInit
SetParamFunctions[EnumCopyFightProp.PAttMaxPow2] = CLuaCopyPropertyFight.SetParamPAttMaxPow2
SetParamFunctions[EnumCopyFightProp.PAttMaxPow1] = CLuaCopyPropertyFight.SetParamPAttMaxPow1
SetParamFunctions[EnumCopyFightProp.PAttMaxInit] = CLuaCopyPropertyFight.SetParamPAttMaxInit
SetParamFunctions[EnumCopyFightProp.PhitPow1] = CLuaCopyPropertyFight.SetParamPhitPow1
SetParamFunctions[EnumCopyFightProp.PHitInit] = CLuaCopyPropertyFight.SetParamPHitInit
SetParamFunctions[EnumCopyFightProp.PMissPow1] = CLuaCopyPropertyFight.SetParamPMissPow1
SetParamFunctions[EnumCopyFightProp.PMissInit] = CLuaCopyPropertyFight.SetParamPMissInit
SetParamFunctions[EnumCopyFightProp.PSpeedPow1] = CLuaCopyPropertyFight.SetParamPSpeedPow1
SetParamFunctions[EnumCopyFightProp.PSpeedInit] = CLuaCopyPropertyFight.SetParamPSpeedInit
SetParamFunctions[EnumCopyFightProp.PDefPow1] = CLuaCopyPropertyFight.SetParamPDefPow1
SetParamFunctions[EnumCopyFightProp.PDefInit] = CLuaCopyPropertyFight.SetParamPDefInit
SetParamFunctions[EnumCopyFightProp.PfatalPow1] = CLuaCopyPropertyFight.SetParamPfatalPow1
SetParamFunctions[EnumCopyFightProp.PFatalInit] = CLuaCopyPropertyFight.SetParamPFatalInit
SetParamFunctions[EnumCopyFightProp.MAttMinPow2] = CLuaCopyPropertyFight.SetParamMAttMinPow2
SetParamFunctions[EnumCopyFightProp.MAttMinPow1] = CLuaCopyPropertyFight.SetParamMAttMinPow1
SetParamFunctions[EnumCopyFightProp.MAttMinInit] = CLuaCopyPropertyFight.SetParamMAttMinInit
SetParamFunctions[EnumCopyFightProp.MAttMaxPow2] = CLuaCopyPropertyFight.SetParamMAttMaxPow2
SetParamFunctions[EnumCopyFightProp.MAttMaxPow1] = CLuaCopyPropertyFight.SetParamMAttMaxPow1
SetParamFunctions[EnumCopyFightProp.MAttMaxInit] = CLuaCopyPropertyFight.SetParamMAttMaxInit
SetParamFunctions[EnumCopyFightProp.MSpeedPow1] = CLuaCopyPropertyFight.SetParamMSpeedPow1
SetParamFunctions[EnumCopyFightProp.MSpeedInit] = CLuaCopyPropertyFight.SetParamMSpeedInit
SetParamFunctions[EnumCopyFightProp.MDefPow1] = CLuaCopyPropertyFight.SetParamMDefPow1
SetParamFunctions[EnumCopyFightProp.MDefInit] = CLuaCopyPropertyFight.SetParamMDefInit
SetParamFunctions[EnumCopyFightProp.MHitPow1] = CLuaCopyPropertyFight.SetParamMHitPow1
SetParamFunctions[EnumCopyFightProp.MHitInit] = CLuaCopyPropertyFight.SetParamMHitInit
SetParamFunctions[EnumCopyFightProp.MMissPow1] = CLuaCopyPropertyFight.SetParamMMissPow1
SetParamFunctions[EnumCopyFightProp.MMissInit] = CLuaCopyPropertyFight.SetParamMMissInit
SetParamFunctions[EnumCopyFightProp.MFatalPow1] = CLuaCopyPropertyFight.SetParamMFatalPow1
SetParamFunctions[EnumCopyFightProp.MFatalInit] = CLuaCopyPropertyFight.SetParamMFatalInit
SetParamFunctions[EnumCopyFightProp.BlockDamagePow1] = CLuaCopyPropertyFight.SetParamBlockDamagePow1
SetParamFunctions[EnumCopyFightProp.BlockDamageInit] = CLuaCopyPropertyFight.SetParamBlockDamageInit
SetParamFunctions[EnumCopyFightProp.RunSpeed] = CLuaCopyPropertyFight.SetParamRunSpeed
SetParamFunctions[EnumCopyFightProp.HpRecoverPow1] = CLuaCopyPropertyFight.SetParamHpRecoverPow1
SetParamFunctions[EnumCopyFightProp.HpRecoverInit] = CLuaCopyPropertyFight.SetParamHpRecoverInit
SetParamFunctions[EnumCopyFightProp.AntiBlockDamagePow1] = CLuaCopyPropertyFight.SetParamAntiBlockDamagePow1
SetParamFunctions[EnumCopyFightProp.AntiBlockDamageInit] = CLuaCopyPropertyFight.SetParamAntiBlockDamageInit
SetParamFunctions[EnumCopyFightProp.BBMax] = CLuaCopyPropertyFight.SetParamBBMax
SetParamFunctions[EnumCopyFightProp.AdjBBMax] = CLuaCopyPropertyFight.SetParamAdjBBMax
SetParamFunctions[EnumCopyFightProp.AntiBreak] = CLuaCopyPropertyFight.SetParamAntiBreak
SetParamFunctions[EnumCopyFightProp.AdjAntiBreak] = CLuaCopyPropertyFight.SetParamAdjAntiBreak
SetParamFunctions[EnumCopyFightProp.AntiPushPull] = CLuaCopyPropertyFight.SetParamAntiPushPull
SetParamFunctions[EnumCopyFightProp.AdjAntiPushPull] = CLuaCopyPropertyFight.SetParamAdjAntiPushPull
SetParamFunctions[EnumCopyFightProp.AntiPetrify] = CLuaCopyPropertyFight.SetParamAntiPetrify
SetParamFunctions[EnumCopyFightProp.AdjAntiPetrify] = CLuaCopyPropertyFight.SetParamAdjAntiPetrify
SetParamFunctions[EnumCopyFightProp.MulAntiPetrify] = CLuaCopyPropertyFight.SetParamMulAntiPetrify
SetParamFunctions[EnumCopyFightProp.AntiTie] = CLuaCopyPropertyFight.SetParamAntiTie
SetParamFunctions[EnumCopyFightProp.AdjAntiTie] = CLuaCopyPropertyFight.SetParamAdjAntiTie
SetParamFunctions[EnumCopyFightProp.MulAntiTie] = CLuaCopyPropertyFight.SetParamMulAntiTie
SetParamFunctions[EnumCopyFightProp.EnhancePetrify] = CLuaCopyPropertyFight.SetParamEnhancePetrify
SetParamFunctions[EnumCopyFightProp.AdjEnhancePetrify] = CLuaCopyPropertyFight.SetParamAdjEnhancePetrify
SetParamFunctions[EnumCopyFightProp.MulEnhancePetrify] = CLuaCopyPropertyFight.SetParamMulEnhancePetrify
SetParamFunctions[EnumCopyFightProp.EnhanceTie] = CLuaCopyPropertyFight.SetParamEnhanceTie
SetParamFunctions[EnumCopyFightProp.AdjEnhanceTie] = CLuaCopyPropertyFight.SetParamAdjEnhanceTie
SetParamFunctions[EnumCopyFightProp.MulEnhanceTie] = CLuaCopyPropertyFight.SetParamMulEnhanceTie
SetParamFunctions[EnumCopyFightProp.TieProbability] = CLuaCopyPropertyFight.SetParamTieProbability
SetParamFunctions[EnumCopyFightProp.StrZizhi] = CLuaCopyPropertyFight.SetParamStrZizhi
SetParamFunctions[EnumCopyFightProp.CorZizhi] = CLuaCopyPropertyFight.SetParamCorZizhi
SetParamFunctions[EnumCopyFightProp.StaZizhi] = CLuaCopyPropertyFight.SetParamStaZizhi
SetParamFunctions[EnumCopyFightProp.AgiZizhi] = CLuaCopyPropertyFight.SetParamAgiZizhi
SetParamFunctions[EnumCopyFightProp.IntZizhi] = CLuaCopyPropertyFight.SetParamIntZizhi
SetParamFunctions[EnumCopyFightProp.InitStrZizhi] = CLuaCopyPropertyFight.SetParamInitStrZizhi
SetParamFunctions[EnumCopyFightProp.InitCorZizhi] = CLuaCopyPropertyFight.SetParamInitCorZizhi
SetParamFunctions[EnumCopyFightProp.InitStaZizhi] = CLuaCopyPropertyFight.SetParamInitStaZizhi
SetParamFunctions[EnumCopyFightProp.InitAgiZizhi] = CLuaCopyPropertyFight.SetParamInitAgiZizhi
SetParamFunctions[EnumCopyFightProp.InitIntZizhi] = CLuaCopyPropertyFight.SetParamInitIntZizhi
SetParamFunctions[EnumCopyFightProp.Wuxing] = CLuaCopyPropertyFight.SetParamWuxing
SetParamFunctions[EnumCopyFightProp.Xiuwei] = CLuaCopyPropertyFight.SetParamXiuwei
SetParamFunctions[EnumCopyFightProp.SkillNum] = CLuaCopyPropertyFight.SetParamSkillNum
SetParamFunctions[EnumCopyFightProp.PType] = CLuaCopyPropertyFight.SetParamPType
SetParamFunctions[EnumCopyFightProp.MType] = CLuaCopyPropertyFight.SetParamMType
SetParamFunctions[EnumCopyFightProp.PHType] = CLuaCopyPropertyFight.SetParamPHType
SetParamFunctions[EnumCopyFightProp.GrowFactor] = CLuaCopyPropertyFight.SetParamGrowFactor
SetParamFunctions[EnumCopyFightProp.WuxingImprove] = CLuaCopyPropertyFight.SetParamWuxingImprove
SetParamFunctions[EnumCopyFightProp.XiuweiImprove] = CLuaCopyPropertyFight.SetParamXiuweiImprove
SetParamFunctions[EnumCopyFightProp.EnhanceBeHeal] = CLuaCopyPropertyFight.SetParamEnhanceBeHeal
SetParamFunctions[EnumCopyFightProp.AdjEnhanceBeHeal] = CLuaCopyPropertyFight.SetParamAdjEnhanceBeHeal
SetParamFunctions[EnumCopyFightProp.MulEnhanceBeHeal] = CLuaCopyPropertyFight.SetParamMulEnhanceBeHeal
SetParamFunctions[EnumCopyFightProp.LookUpCor] = CLuaCopyPropertyFight.SetParamLookUpCor
SetParamFunctions[EnumCopyFightProp.LookUpSta] = CLuaCopyPropertyFight.SetParamLookUpSta
SetParamFunctions[EnumCopyFightProp.LookUpStr] = CLuaCopyPropertyFight.SetParamLookUpStr
SetParamFunctions[EnumCopyFightProp.LookUpInt] = CLuaCopyPropertyFight.SetParamLookUpInt
SetParamFunctions[EnumCopyFightProp.LookUpAgi] = CLuaCopyPropertyFight.SetParamLookUpAgi
SetParamFunctions[EnumCopyFightProp.LookUpHpFull] = CLuaCopyPropertyFight.SetParamLookUpHpFull
SetParamFunctions[EnumCopyFightProp.LookUpMpFull] = CLuaCopyPropertyFight.SetParamLookUpMpFull
SetParamFunctions[EnumCopyFightProp.LookUppAttMin] = CLuaCopyPropertyFight.SetParamLookUppAttMin
SetParamFunctions[EnumCopyFightProp.LookUppAttMax] = CLuaCopyPropertyFight.SetParamLookUppAttMax
SetParamFunctions[EnumCopyFightProp.LookUppHit] = CLuaCopyPropertyFight.SetParamLookUppHit
SetParamFunctions[EnumCopyFightProp.LookUppMiss] = CLuaCopyPropertyFight.SetParamLookUppMiss
SetParamFunctions[EnumCopyFightProp.LookUppDef] = CLuaCopyPropertyFight.SetParamLookUppDef
SetParamFunctions[EnumCopyFightProp.LookUpmAttMin] = CLuaCopyPropertyFight.SetParamLookUpmAttMin
SetParamFunctions[EnumCopyFightProp.LookUpmAttMax] = CLuaCopyPropertyFight.SetParamLookUpmAttMax
SetParamFunctions[EnumCopyFightProp.LookUpmHit] = CLuaCopyPropertyFight.SetParamLookUpmHit
SetParamFunctions[EnumCopyFightProp.LookUpmMiss] = CLuaCopyPropertyFight.SetParamLookUpmMiss
SetParamFunctions[EnumCopyFightProp.LookUpmDef] = CLuaCopyPropertyFight.SetParamLookUpmDef
SetParamFunctions[EnumCopyFightProp.AdjHpFull2] = CLuaCopyPropertyFight.SetParamAdjHpFull2
SetParamFunctions[EnumCopyFightProp.AdjpAttMin2] = CLuaCopyPropertyFight.SetParamAdjpAttMin2
SetParamFunctions[EnumCopyFightProp.AdjpAttMax2] = CLuaCopyPropertyFight.SetParamAdjpAttMax2
SetParamFunctions[EnumCopyFightProp.AdjmAttMin2] = CLuaCopyPropertyFight.SetParamAdjmAttMin2
SetParamFunctions[EnumCopyFightProp.AdjmAttMax2] = CLuaCopyPropertyFight.SetParamAdjmAttMax2
SetParamFunctions[EnumCopyFightProp.LingShouType] = CLuaCopyPropertyFight.SetParamLingShouType
SetParamFunctions[EnumCopyFightProp.MHType] = CLuaCopyPropertyFight.SetParamMHType
SetParamFunctions[EnumCopyFightProp.EnhanceLingShou] = CLuaCopyPropertyFight.SetParamEnhanceLingShou
SetParamFunctions[EnumCopyFightProp.EnhanceLingShouMul] = CLuaCopyPropertyFight.SetParamEnhanceLingShouMul
SetParamFunctions[EnumCopyFightProp.AdjAntiAoe] = CLuaCopyPropertyFight.SetParamAdjAntiAoe
SetParamFunctions[EnumCopyFightProp.AntiBlind] = CLuaCopyPropertyFight.SetParamAntiBlind
SetParamFunctions[EnumCopyFightProp.AdjAntiBlind] = CLuaCopyPropertyFight.SetParamAdjAntiBlind
SetParamFunctions[EnumCopyFightProp.MulAntiBlind] = CLuaCopyPropertyFight.SetParamMulAntiBlind
SetParamFunctions[EnumCopyFightProp.AdjAntiDecelerate] = CLuaCopyPropertyFight.SetParamAdjAntiDecelerate
SetParamFunctions[EnumCopyFightProp.MulAntiDecelerate] = CLuaCopyPropertyFight.SetParamMulAntiDecelerate
SetParamFunctions[EnumCopyFightProp.MinGrade] = CLuaCopyPropertyFight.SetParamMinGrade
SetParamFunctions[EnumCopyFightProp.CharacterCor] = CLuaCopyPropertyFight.SetParamCharacterCor
SetParamFunctions[EnumCopyFightProp.CharacterSta] = CLuaCopyPropertyFight.SetParamCharacterSta
SetParamFunctions[EnumCopyFightProp.CharacterStr] = CLuaCopyPropertyFight.SetParamCharacterStr
SetParamFunctions[EnumCopyFightProp.CharacterInt] = CLuaCopyPropertyFight.SetParamCharacterInt
SetParamFunctions[EnumCopyFightProp.CharacterAgi] = CLuaCopyPropertyFight.SetParamCharacterAgi
SetParamFunctions[EnumCopyFightProp.EnhanceDaoKe] = CLuaCopyPropertyFight.SetParamEnhanceDaoKe
SetParamFunctions[EnumCopyFightProp.EnhanceDaoKeMul] = CLuaCopyPropertyFight.SetParamEnhanceDaoKeMul
SetParamFunctions[EnumCopyFightProp.ZuoQiSpeed] = CLuaCopyPropertyFight.SetParamZuoQiSpeed
SetParamFunctions[EnumCopyFightProp.EnhanceBianHu] = CLuaCopyPropertyFight.SetParamEnhanceBianHu
SetParamFunctions[EnumCopyFightProp.AdjEnhanceBianHu] = CLuaCopyPropertyFight.SetParamAdjEnhanceBianHu
SetParamFunctions[EnumCopyFightProp.MulEnhanceBianHu] = CLuaCopyPropertyFight.SetParamMulEnhanceBianHu
SetParamFunctions[EnumCopyFightProp.AntiBianHu] = CLuaCopyPropertyFight.SetParamAntiBianHu
SetParamFunctions[EnumCopyFightProp.AdjAntiBianHu] = CLuaCopyPropertyFight.SetParamAdjAntiBianHu
SetParamFunctions[EnumCopyFightProp.MulAntiBianHu] = CLuaCopyPropertyFight.SetParamMulAntiBianHu
SetParamFunctions[EnumCopyFightProp.EnhanceXiaKe] = CLuaCopyPropertyFight.SetParamEnhanceXiaKe
SetParamFunctions[EnumCopyFightProp.EnhanceXiaKeMul] = CLuaCopyPropertyFight.SetParamEnhanceXiaKeMul
SetParamFunctions[EnumCopyFightProp.TieTimeChange] = CLuaCopyPropertyFight.SetParamTieTimeChange
SetParamFunctions[EnumCopyFightProp.EnhanceYanShi] = CLuaCopyPropertyFight.SetParamEnhanceYanShi
SetParamFunctions[EnumCopyFightProp.EnhanceYanShiMul] = CLuaCopyPropertyFight.SetParamEnhanceYanShiMul
SetParamFunctions[EnumCopyFightProp.PAType] = CLuaCopyPropertyFight.SetParamPAType
SetParamFunctions[EnumCopyFightProp.MAType] = CLuaCopyPropertyFight.SetParamMAType
SetParamFunctions[EnumCopyFightProp.lifetimeNoReduceRate] = CLuaCopyPropertyFight.SetParamlifetimeNoReduceRate
SetParamFunctions[EnumCopyFightProp.AddLSHpDurgImprove] = CLuaCopyPropertyFight.SetParamAddLSHpDurgImprove
SetParamFunctions[EnumCopyFightProp.AddHpDurgImprove] = CLuaCopyPropertyFight.SetParamAddHpDurgImprove
SetParamFunctions[EnumCopyFightProp.AddMpDurgImprove] = CLuaCopyPropertyFight.SetParamAddMpDurgImprove
SetParamFunctions[EnumCopyFightProp.FireHurtReduce] = CLuaCopyPropertyFight.SetParamFireHurtReduce
SetParamFunctions[EnumCopyFightProp.ThunderHurtReduce] = CLuaCopyPropertyFight.SetParamThunderHurtReduce
SetParamFunctions[EnumCopyFightProp.IceHurtReduce] = CLuaCopyPropertyFight.SetParamIceHurtReduce
SetParamFunctions[EnumCopyFightProp.PoisonHurtReduce] = CLuaCopyPropertyFight.SetParamPoisonHurtReduce
SetParamFunctions[EnumCopyFightProp.WindHurtReduce] = CLuaCopyPropertyFight.SetParamWindHurtReduce
SetParamFunctions[EnumCopyFightProp.LightHurtReduce] = CLuaCopyPropertyFight.SetParamLightHurtReduce
SetParamFunctions[EnumCopyFightProp.IllusionHurtReduce] = CLuaCopyPropertyFight.SetParamIllusionHurtReduce
SetParamFunctions[EnumCopyFightProp.FakepDef] = CLuaCopyPropertyFight.SetParamFakepDef
SetParamFunctions[EnumCopyFightProp.FakemDef] = CLuaCopyPropertyFight.SetParamFakemDef
SetParamFunctions[EnumCopyFightProp.EnhanceWater] = CLuaCopyPropertyFight.SetParamEnhanceWater
SetParamFunctions[EnumCopyFightProp.AdjEnhanceWater] = CLuaCopyPropertyFight.SetParamAdjEnhanceWater
SetParamFunctions[EnumCopyFightProp.MulEnhanceWater] = CLuaCopyPropertyFight.SetParamMulEnhanceWater
SetParamFunctions[EnumCopyFightProp.AntiWater] = CLuaCopyPropertyFight.SetParamAntiWater
SetParamFunctions[EnumCopyFightProp.AdjAntiWater] = CLuaCopyPropertyFight.SetParamAdjAntiWater
SetParamFunctions[EnumCopyFightProp.MulAntiWater] = CLuaCopyPropertyFight.SetParamMulAntiWater
SetParamFunctions[EnumCopyFightProp.WaterHurtReduce] = CLuaCopyPropertyFight.SetParamWaterHurtReduce
SetParamFunctions[EnumCopyFightProp.IgnoreAntiWater] = CLuaCopyPropertyFight.SetParamIgnoreAntiWater
SetParamFunctions[EnumCopyFightProp.AntiFreeze] = CLuaCopyPropertyFight.SetParamAntiFreeze
SetParamFunctions[EnumCopyFightProp.AdjAntiFreeze] = CLuaCopyPropertyFight.SetParamAdjAntiFreeze
SetParamFunctions[EnumCopyFightProp.MulAntiFreeze] = CLuaCopyPropertyFight.SetParamMulAntiFreeze
SetParamFunctions[EnumCopyFightProp.EnhanceFreeze] = CLuaCopyPropertyFight.SetParamEnhanceFreeze
SetParamFunctions[EnumCopyFightProp.AdjEnhanceFreeze] = CLuaCopyPropertyFight.SetParamAdjEnhanceFreeze
SetParamFunctions[EnumCopyFightProp.MulEnhanceFreeze] = CLuaCopyPropertyFight.SetParamMulEnhanceFreeze
SetParamFunctions[EnumCopyFightProp.EnhanceHuaHun] = CLuaCopyPropertyFight.SetParamEnhanceHuaHun
SetParamFunctions[EnumCopyFightProp.EnhanceHuaHunMul] = CLuaCopyPropertyFight.SetParamEnhanceHuaHunMul
SetParamFunctions[EnumCopyFightProp.TrueSightProb] = CLuaCopyPropertyFight.SetParamTrueSightProb
SetParamFunctions[EnumCopyFightProp.LSBBGrade] = CLuaCopyPropertyFight.SetParamLSBBGrade
SetParamFunctions[EnumCopyFightProp.LSBBQuality] = CLuaCopyPropertyFight.SetParamLSBBQuality
SetParamFunctions[EnumCopyFightProp.LSBBAdjHp] = CLuaCopyPropertyFight.SetParamLSBBAdjHp
SetParamFunctions[EnumCopyFightProp.AdjIgnoreAntiFire] = CLuaCopyPropertyFight.SetParamAdjIgnoreAntiFire
SetParamFunctions[EnumCopyFightProp.AdjIgnoreAntiThunder] = CLuaCopyPropertyFight.SetParamAdjIgnoreAntiThunder
SetParamFunctions[EnumCopyFightProp.AdjIgnoreAntiIce] = CLuaCopyPropertyFight.SetParamAdjIgnoreAntiIce
SetParamFunctions[EnumCopyFightProp.AdjIgnoreAntiPoison] = CLuaCopyPropertyFight.SetParamAdjIgnoreAntiPoison
SetParamFunctions[EnumCopyFightProp.AdjIgnoreAntiWind] = CLuaCopyPropertyFight.SetParamAdjIgnoreAntiWind
SetParamFunctions[EnumCopyFightProp.AdjIgnoreAntiLight] = CLuaCopyPropertyFight.SetParamAdjIgnoreAntiLight
SetParamFunctions[EnumCopyFightProp.AdjIgnoreAntiIllusion] = CLuaCopyPropertyFight.SetParamAdjIgnoreAntiIllusion
SetParamFunctions[EnumCopyFightProp.IgnoreAntiFireLSMul] = CLuaCopyPropertyFight.SetParamIgnoreAntiFireLSMul
SetParamFunctions[EnumCopyFightProp.IgnoreAntiThunderLSMul] = CLuaCopyPropertyFight.SetParamIgnoreAntiThunderLSMul
SetParamFunctions[EnumCopyFightProp.IgnoreAntiIceLSMul] = CLuaCopyPropertyFight.SetParamIgnoreAntiIceLSMul
SetParamFunctions[EnumCopyFightProp.IgnoreAntiPoisonLSMul] = CLuaCopyPropertyFight.SetParamIgnoreAntiPoisonLSMul
SetParamFunctions[EnumCopyFightProp.IgnoreAntiWindLSMul] = CLuaCopyPropertyFight.SetParamIgnoreAntiWindLSMul
SetParamFunctions[EnumCopyFightProp.IgnoreAntiLightLSMul] = CLuaCopyPropertyFight.SetParamIgnoreAntiLightLSMul
SetParamFunctions[EnumCopyFightProp.IgnoreAntiIllusionLSMul] = CLuaCopyPropertyFight.SetParamIgnoreAntiIllusionLSMul
SetParamFunctions[EnumCopyFightProp.AdjIgnoreAntiWater] = CLuaCopyPropertyFight.SetParamAdjIgnoreAntiWater
SetParamFunctions[EnumCopyFightProp.IgnoreAntiWaterLSMul] = CLuaCopyPropertyFight.SetParamIgnoreAntiWaterLSMul
SetParamFunctions[EnumCopyFightProp.MulSpeed2] = CLuaCopyPropertyFight.SetParamMulSpeed2
SetParamFunctions[EnumCopyFightProp.MulConsumeMp] = CLuaCopyPropertyFight.SetParamMulConsumeMp
SetParamFunctions[EnumCopyFightProp.AdjAgi2] = CLuaCopyPropertyFight.SetParamAdjAgi2
SetParamFunctions[EnumCopyFightProp.AdjAntiBianHu2] = CLuaCopyPropertyFight.SetParamAdjAntiBianHu2
SetParamFunctions[EnumCopyFightProp.AdjAntiBind2] = CLuaCopyPropertyFight.SetParamAdjAntiBind2
SetParamFunctions[EnumCopyFightProp.AdjAntiBlind2] = CLuaCopyPropertyFight.SetParamAdjAntiBlind2
SetParamFunctions[EnumCopyFightProp.AdjAntiBlock2] = CLuaCopyPropertyFight.SetParamAdjAntiBlock2
SetParamFunctions[EnumCopyFightProp.AdjAntiBlockDamage2] = CLuaCopyPropertyFight.SetParamAdjAntiBlockDamage2
SetParamFunctions[EnumCopyFightProp.AdjAntiChaos2] = CLuaCopyPropertyFight.SetParamAdjAntiChaos2
SetParamFunctions[EnumCopyFightProp.AdjAntiDecelerate2] = CLuaCopyPropertyFight.SetParamAdjAntiDecelerate2
SetParamFunctions[EnumCopyFightProp.AdjAntiDizzy2] = CLuaCopyPropertyFight.SetParamAdjAntiDizzy2
SetParamFunctions[EnumCopyFightProp.AdjAntiFire2] = CLuaCopyPropertyFight.SetParamAdjAntiFire2
SetParamFunctions[EnumCopyFightProp.AdjAntiFreeze2] = CLuaCopyPropertyFight.SetParamAdjAntiFreeze2
SetParamFunctions[EnumCopyFightProp.AdjAntiIce2] = CLuaCopyPropertyFight.SetParamAdjAntiIce2
SetParamFunctions[EnumCopyFightProp.AdjAntiIllusion2] = CLuaCopyPropertyFight.SetParamAdjAntiIllusion2
SetParamFunctions[EnumCopyFightProp.AdjAntiLight2] = CLuaCopyPropertyFight.SetParamAdjAntiLight2
SetParamFunctions[EnumCopyFightProp.AdjAntimFatal2] = CLuaCopyPropertyFight.SetParamAdjAntimFatal2
SetParamFunctions[EnumCopyFightProp.AdjAntimFatalDamage2] = CLuaCopyPropertyFight.SetParamAdjAntimFatalDamage2
SetParamFunctions[EnumCopyFightProp.AdjAntiPetrify2] = CLuaCopyPropertyFight.SetParamAdjAntiPetrify2
SetParamFunctions[EnumCopyFightProp.AdjAntipFatal2] = CLuaCopyPropertyFight.SetParamAdjAntipFatal2
SetParamFunctions[EnumCopyFightProp.AdjAntipFatalDamage2] = CLuaCopyPropertyFight.SetParamAdjAntipFatalDamage2
SetParamFunctions[EnumCopyFightProp.AdjAntiPoison2] = CLuaCopyPropertyFight.SetParamAdjAntiPoison2
SetParamFunctions[EnumCopyFightProp.AdjAntiSilence2] = CLuaCopyPropertyFight.SetParamAdjAntiSilence2
SetParamFunctions[EnumCopyFightProp.AdjAntiSleep2] = CLuaCopyPropertyFight.SetParamAdjAntiSleep2
SetParamFunctions[EnumCopyFightProp.AdjAntiThunder2] = CLuaCopyPropertyFight.SetParamAdjAntiThunder2
SetParamFunctions[EnumCopyFightProp.AdjAntiTie2] = CLuaCopyPropertyFight.SetParamAdjAntiTie2
SetParamFunctions[EnumCopyFightProp.AdjAntiWater2] = CLuaCopyPropertyFight.SetParamAdjAntiWater2
SetParamFunctions[EnumCopyFightProp.AdjAntiWind2] = CLuaCopyPropertyFight.SetParamAdjAntiWind2
SetParamFunctions[EnumCopyFightProp.AdjBlock2] = CLuaCopyPropertyFight.SetParamAdjBlock2
SetParamFunctions[EnumCopyFightProp.AdjBlockDamage2] = CLuaCopyPropertyFight.SetParamAdjBlockDamage2
SetParamFunctions[EnumCopyFightProp.AdjCor2] = CLuaCopyPropertyFight.SetParamAdjCor2
SetParamFunctions[EnumCopyFightProp.AdjEnhanceBianHu2] = CLuaCopyPropertyFight.SetParamAdjEnhanceBianHu2
SetParamFunctions[EnumCopyFightProp.AdjEnhanceBind2] = CLuaCopyPropertyFight.SetParamAdjEnhanceBind2
SetParamFunctions[EnumCopyFightProp.AdjEnhanceChaos2] = CLuaCopyPropertyFight.SetParamAdjEnhanceChaos2
SetParamFunctions[EnumCopyFightProp.AdjEnhanceDizzy2] = CLuaCopyPropertyFight.SetParamAdjEnhanceDizzy2
SetParamFunctions[EnumCopyFightProp.AdjEnhanceFire2] = CLuaCopyPropertyFight.SetParamAdjEnhanceFire2
SetParamFunctions[EnumCopyFightProp.AdjEnhanceFreeze2] = CLuaCopyPropertyFight.SetParamAdjEnhanceFreeze2
SetParamFunctions[EnumCopyFightProp.AdjEnhanceIce2] = CLuaCopyPropertyFight.SetParamAdjEnhanceIce2
SetParamFunctions[EnumCopyFightProp.AdjEnhanceIllusion2] = CLuaCopyPropertyFight.SetParamAdjEnhanceIllusion2
SetParamFunctions[EnumCopyFightProp.AdjEnhanceLight2] = CLuaCopyPropertyFight.SetParamAdjEnhanceLight2
SetParamFunctions[EnumCopyFightProp.AdjEnhancePetrify2] = CLuaCopyPropertyFight.SetParamAdjEnhancePetrify2
SetParamFunctions[EnumCopyFightProp.AdjEnhancePoison2] = CLuaCopyPropertyFight.SetParamAdjEnhancePoison2
SetParamFunctions[EnumCopyFightProp.AdjEnhanceSilence2] = CLuaCopyPropertyFight.SetParamAdjEnhanceSilence2
SetParamFunctions[EnumCopyFightProp.AdjEnhanceSleep2] = CLuaCopyPropertyFight.SetParamAdjEnhanceSleep2
SetParamFunctions[EnumCopyFightProp.AdjEnhanceThunder2] = CLuaCopyPropertyFight.SetParamAdjEnhanceThunder2
SetParamFunctions[EnumCopyFightProp.AdjEnhanceTie2] = CLuaCopyPropertyFight.SetParamAdjEnhanceTie2
SetParamFunctions[EnumCopyFightProp.AdjEnhanceWater2] = CLuaCopyPropertyFight.SetParamAdjEnhanceWater2
SetParamFunctions[EnumCopyFightProp.AdjEnhanceWind2] = CLuaCopyPropertyFight.SetParamAdjEnhanceWind2
SetParamFunctions[EnumCopyFightProp.AdjIgnoreAntiFire2] = CLuaCopyPropertyFight.SetParamAdjIgnoreAntiFire2
SetParamFunctions[EnumCopyFightProp.AdjIgnoreAntiIce2] = CLuaCopyPropertyFight.SetParamAdjIgnoreAntiIce2
SetParamFunctions[EnumCopyFightProp.AdjIgnoreAntiIllusion2] = CLuaCopyPropertyFight.SetParamAdjIgnoreAntiIllusion2
SetParamFunctions[EnumCopyFightProp.AdjIgnoreAntiLight2] = CLuaCopyPropertyFight.SetParamAdjIgnoreAntiLight2
SetParamFunctions[EnumCopyFightProp.AdjIgnoreAntiPoison2] = CLuaCopyPropertyFight.SetParamAdjIgnoreAntiPoison2
SetParamFunctions[EnumCopyFightProp.AdjIgnoreAntiThunder2] = CLuaCopyPropertyFight.SetParamAdjIgnoreAntiThunder2
SetParamFunctions[EnumCopyFightProp.AdjIgnoreAntiWater2] = CLuaCopyPropertyFight.SetParamAdjIgnoreAntiWater2
SetParamFunctions[EnumCopyFightProp.AdjIgnoreAntiWind2] = CLuaCopyPropertyFight.SetParamAdjIgnoreAntiWind2
SetParamFunctions[EnumCopyFightProp.AdjInt2] = CLuaCopyPropertyFight.SetParamAdjInt2
SetParamFunctions[EnumCopyFightProp.AdjmDef2] = CLuaCopyPropertyFight.SetParamAdjmDef2
SetParamFunctions[EnumCopyFightProp.AdjmFatal2] = CLuaCopyPropertyFight.SetParamAdjmFatal2
SetParamFunctions[EnumCopyFightProp.AdjmFatalDamage2] = CLuaCopyPropertyFight.SetParamAdjmFatalDamage2
SetParamFunctions[EnumCopyFightProp.AdjmHit2] = CLuaCopyPropertyFight.SetParamAdjmHit2
SetParamFunctions[EnumCopyFightProp.AdjmHurt2] = CLuaCopyPropertyFight.SetParamAdjmHurt2
SetParamFunctions[EnumCopyFightProp.AdjmMiss2] = CLuaCopyPropertyFight.SetParamAdjmMiss2
SetParamFunctions[EnumCopyFightProp.AdjmSpeed2] = CLuaCopyPropertyFight.SetParamAdjmSpeed2
SetParamFunctions[EnumCopyFightProp.AdjpDef2] = CLuaCopyPropertyFight.SetParamAdjpDef2
SetParamFunctions[EnumCopyFightProp.AdjpFatal2] = CLuaCopyPropertyFight.SetParamAdjpFatal2
SetParamFunctions[EnumCopyFightProp.AdjpFatalDamage2] = CLuaCopyPropertyFight.SetParamAdjpFatalDamage2
SetParamFunctions[EnumCopyFightProp.AdjpHit2] = CLuaCopyPropertyFight.SetParamAdjpHit2
SetParamFunctions[EnumCopyFightProp.AdjpHurt2] = CLuaCopyPropertyFight.SetParamAdjpHurt2
SetParamFunctions[EnumCopyFightProp.AdjpMiss2] = CLuaCopyPropertyFight.SetParamAdjpMiss2
SetParamFunctions[EnumCopyFightProp.AdjpSpeed2] = CLuaCopyPropertyFight.SetParamAdjpSpeed2
SetParamFunctions[EnumCopyFightProp.AdjStr2] = CLuaCopyPropertyFight.SetParamAdjStr2
SetParamFunctions[EnumCopyFightProp.LSpAttMin] = CLuaCopyPropertyFight.SetParamLSpAttMin
SetParamFunctions[EnumCopyFightProp.LSpAttMax] = CLuaCopyPropertyFight.SetParamLSpAttMax
SetParamFunctions[EnumCopyFightProp.LSmAttMin] = CLuaCopyPropertyFight.SetParamLSmAttMin
SetParamFunctions[EnumCopyFightProp.LSmAttMax] = CLuaCopyPropertyFight.SetParamLSmAttMax
SetParamFunctions[EnumCopyFightProp.LSpFatal] = CLuaCopyPropertyFight.SetParamLSpFatal
SetParamFunctions[EnumCopyFightProp.LSpFatalDamage] = CLuaCopyPropertyFight.SetParamLSpFatalDamage
SetParamFunctions[EnumCopyFightProp.LSmFatal] = CLuaCopyPropertyFight.SetParamLSmFatal
SetParamFunctions[EnumCopyFightProp.LSmFatalDamage] = CLuaCopyPropertyFight.SetParamLSmFatalDamage
SetParamFunctions[EnumCopyFightProp.LSAntiBlock] = CLuaCopyPropertyFight.SetParamLSAntiBlock
SetParamFunctions[EnumCopyFightProp.LSAntiBlockDamage] = CLuaCopyPropertyFight.SetParamLSAntiBlockDamage
SetParamFunctions[EnumCopyFightProp.LSIgnoreAntiFire] = CLuaCopyPropertyFight.SetParamLSIgnoreAntiFire
SetParamFunctions[EnumCopyFightProp.LSIgnoreAntiThunder] = CLuaCopyPropertyFight.SetParamLSIgnoreAntiThunder
SetParamFunctions[EnumCopyFightProp.LSIgnoreAntiIce] = CLuaCopyPropertyFight.SetParamLSIgnoreAntiIce
SetParamFunctions[EnumCopyFightProp.LSIgnoreAntiPoison] = CLuaCopyPropertyFight.SetParamLSIgnoreAntiPoison
SetParamFunctions[EnumCopyFightProp.LSIgnoreAntiWind] = CLuaCopyPropertyFight.SetParamLSIgnoreAntiWind
SetParamFunctions[EnumCopyFightProp.LSIgnoreAntiLight] = CLuaCopyPropertyFight.SetParamLSIgnoreAntiLight
SetParamFunctions[EnumCopyFightProp.LSIgnoreAntiIllusion] = CLuaCopyPropertyFight.SetParamLSIgnoreAntiIllusion
SetParamFunctions[EnumCopyFightProp.LSIgnoreAntiWater] = CLuaCopyPropertyFight.SetParamLSIgnoreAntiWater
SetParamFunctions[EnumCopyFightProp.LSpAttMin_adj] = CLuaCopyPropertyFight.SetParamLSpAttMin_adj
SetParamFunctions[EnumCopyFightProp.LSpAttMax_adj] = CLuaCopyPropertyFight.SetParamLSpAttMax_adj
SetParamFunctions[EnumCopyFightProp.LSmAttMin_adj] = CLuaCopyPropertyFight.SetParamLSmAttMin_adj
SetParamFunctions[EnumCopyFightProp.LSmAttMax_adj] = CLuaCopyPropertyFight.SetParamLSmAttMax_adj
SetParamFunctions[EnumCopyFightProp.LSpFatal_adj] = CLuaCopyPropertyFight.SetParamLSpFatal_adj
SetParamFunctions[EnumCopyFightProp.LSpFatalDamage_adj] = CLuaCopyPropertyFight.SetParamLSpFatalDamage_adj
SetParamFunctions[EnumCopyFightProp.LSmFatal_adj] = CLuaCopyPropertyFight.SetParamLSmFatal_adj
SetParamFunctions[EnumCopyFightProp.LSmFatalDamage_adj] = CLuaCopyPropertyFight.SetParamLSmFatalDamage_adj
SetParamFunctions[EnumCopyFightProp.LSAntiBlock_adj] = CLuaCopyPropertyFight.SetParamLSAntiBlock_adj
SetParamFunctions[EnumCopyFightProp.LSAntiBlockDamage_adj] = CLuaCopyPropertyFight.SetParamLSAntiBlockDamage_adj
SetParamFunctions[EnumCopyFightProp.LSIgnoreAntiFire_adj] = CLuaCopyPropertyFight.SetParamLSIgnoreAntiFire_adj
SetParamFunctions[EnumCopyFightProp.LSIgnoreAntiThunder_adj] = CLuaCopyPropertyFight.SetParamLSIgnoreAntiThunder_adj
SetParamFunctions[EnumCopyFightProp.LSIgnoreAntiIce_adj] = CLuaCopyPropertyFight.SetParamLSIgnoreAntiIce_adj
SetParamFunctions[EnumCopyFightProp.LSIgnoreAntiPoison_adj] = CLuaCopyPropertyFight.SetParamLSIgnoreAntiPoison_adj
SetParamFunctions[EnumCopyFightProp.LSIgnoreAntiWind_adj] = CLuaCopyPropertyFight.SetParamLSIgnoreAntiWind_adj
SetParamFunctions[EnumCopyFightProp.LSIgnoreAntiLight_adj] = CLuaCopyPropertyFight.SetParamLSIgnoreAntiLight_adj
SetParamFunctions[EnumCopyFightProp.LSIgnoreAntiIllusion_adj] = CLuaCopyPropertyFight.SetParamLSIgnoreAntiIllusion_adj
SetParamFunctions[EnumCopyFightProp.LSIgnoreAntiWater_adj] = CLuaCopyPropertyFight.SetParamLSIgnoreAntiWater_adj
SetParamFunctions[EnumCopyFightProp.LSpAttMin_jc] = CLuaCopyPropertyFight.SetParamLSpAttMin_jc
SetParamFunctions[EnumCopyFightProp.LSpAttMax_jc] = CLuaCopyPropertyFight.SetParamLSpAttMax_jc
SetParamFunctions[EnumCopyFightProp.LSmAttMin_jc] = CLuaCopyPropertyFight.SetParamLSmAttMin_jc
SetParamFunctions[EnumCopyFightProp.LSmAttMax_jc] = CLuaCopyPropertyFight.SetParamLSmAttMax_jc
SetParamFunctions[EnumCopyFightProp.LSpFatal_jc] = CLuaCopyPropertyFight.SetParamLSpFatal_jc
SetParamFunctions[EnumCopyFightProp.LSpFatalDamage_jc] = CLuaCopyPropertyFight.SetParamLSpFatalDamage_jc
SetParamFunctions[EnumCopyFightProp.LSmFatal_jc] = CLuaCopyPropertyFight.SetParamLSmFatal_jc
SetParamFunctions[EnumCopyFightProp.LSmFatalDamage_jc] = CLuaCopyPropertyFight.SetParamLSmFatalDamage_jc
SetParamFunctions[EnumCopyFightProp.LSAntiBlock_jc] = CLuaCopyPropertyFight.SetParamLSAntiBlock_jc
SetParamFunctions[EnumCopyFightProp.LSAntiBlockDamage_jc] = CLuaCopyPropertyFight.SetParamLSAntiBlockDamage_jc
SetParamFunctions[EnumCopyFightProp.LSIgnoreAntiFire_jc] = CLuaCopyPropertyFight.SetParamLSIgnoreAntiFire_jc
SetParamFunctions[EnumCopyFightProp.LSIgnoreAntiThunder_jc] = CLuaCopyPropertyFight.SetParamLSIgnoreAntiThunder_jc
SetParamFunctions[EnumCopyFightProp.LSIgnoreAntiIce_jc] = CLuaCopyPropertyFight.SetParamLSIgnoreAntiIce_jc
SetParamFunctions[EnumCopyFightProp.LSIgnoreAntiPoison_jc] = CLuaCopyPropertyFight.SetParamLSIgnoreAntiPoison_jc
SetParamFunctions[EnumCopyFightProp.LSIgnoreAntiWind_jc] = CLuaCopyPropertyFight.SetParamLSIgnoreAntiWind_jc
SetParamFunctions[EnumCopyFightProp.LSIgnoreAntiLight_jc] = CLuaCopyPropertyFight.SetParamLSIgnoreAntiLight_jc
SetParamFunctions[EnumCopyFightProp.LSIgnoreAntiIllusion_jc] = CLuaCopyPropertyFight.SetParamLSIgnoreAntiIllusion_jc
SetParamFunctions[EnumCopyFightProp.LSIgnoreAntiWater_jc] = CLuaCopyPropertyFight.SetParamLSIgnoreAntiWater_jc
SetParamFunctions[EnumCopyFightProp.LSIgnoreAntiFire_mul] = CLuaCopyPropertyFight.SetParamLSIgnoreAntiFire_mul
SetParamFunctions[EnumCopyFightProp.LSIgnoreAntiThunder_mul] = CLuaCopyPropertyFight.SetParamLSIgnoreAntiThunder_mul
SetParamFunctions[EnumCopyFightProp.LSIgnoreAntiIce_mul] = CLuaCopyPropertyFight.SetParamLSIgnoreAntiIce_mul
SetParamFunctions[EnumCopyFightProp.LSIgnoreAntiPoison_mul] = CLuaCopyPropertyFight.SetParamLSIgnoreAntiPoison_mul
SetParamFunctions[EnumCopyFightProp.LSIgnoreAntiWind_mul] = CLuaCopyPropertyFight.SetParamLSIgnoreAntiWind_mul
SetParamFunctions[EnumCopyFightProp.LSIgnoreAntiLight_mul] = CLuaCopyPropertyFight.SetParamLSIgnoreAntiLight_mul
SetParamFunctions[EnumCopyFightProp.LSIgnoreAntiIllusion_mul] = CLuaCopyPropertyFight.SetParamLSIgnoreAntiIllusion_mul
SetParamFunctions[EnumCopyFightProp.LSIgnoreAntiWater_mul] = CLuaCopyPropertyFight.SetParamLSIgnoreAntiWater_mul
SetParamFunctions[EnumCopyFightProp.JieBan_Child_QiChangColor] = CLuaCopyPropertyFight.SetParamJieBan_Child_QiChangColor
SetParamFunctions[EnumCopyFightProp.JieBan_LingShou_Ratio] = CLuaCopyPropertyFight.SetParamJieBan_LingShou_Ratio
SetParamFunctions[EnumCopyFightProp.JieBan_LingShou_QinMi] = CLuaCopyPropertyFight.SetParamJieBan_LingShou_QinMi
SetParamFunctions[EnumCopyFightProp.JieBan_LingShou_CorZizhi] = CLuaCopyPropertyFight.SetParamJieBan_LingShou_CorZizhi
SetParamFunctions[EnumCopyFightProp.JieBan_LingShou_StaZizhi] = CLuaCopyPropertyFight.SetParamJieBan_LingShou_StaZizhi
SetParamFunctions[EnumCopyFightProp.JieBan_LingShou_StrZizhi] = CLuaCopyPropertyFight.SetParamJieBan_LingShou_StrZizhi
SetParamFunctions[EnumCopyFightProp.JieBan_LingShou_IntZizhi] = CLuaCopyPropertyFight.SetParamJieBan_LingShou_IntZizhi
SetParamFunctions[EnumCopyFightProp.JieBan_LingShou_AgiZizhi] = CLuaCopyPropertyFight.SetParamJieBan_LingShou_AgiZizhi
SetParamFunctions[EnumCopyFightProp.Buff_DisplayValue1] = CLuaCopyPropertyFight.SetParamBuff_DisplayValue1
SetParamFunctions[EnumCopyFightProp.Buff_DisplayValue2] = CLuaCopyPropertyFight.SetParamBuff_DisplayValue2
SetParamFunctions[EnumCopyFightProp.Buff_DisplayValue3] = CLuaCopyPropertyFight.SetParamBuff_DisplayValue3
SetParamFunctions[EnumCopyFightProp.Buff_DisplayValue4] = CLuaCopyPropertyFight.SetParamBuff_DisplayValue4
SetParamFunctions[EnumCopyFightProp.Buff_DisplayValue5] = CLuaCopyPropertyFight.SetParamBuff_DisplayValue5
SetParamFunctions[EnumCopyFightProp.LSNature] = CLuaCopyPropertyFight.SetParamLSNature
SetParamFunctions[EnumCopyFightProp.PrayMulti] = CLuaCopyPropertyFight.SetParamPrayMulti
SetParamFunctions[EnumCopyFightProp.EnhanceYingLing] = CLuaCopyPropertyFight.SetParamEnhanceYingLing
SetParamFunctions[EnumCopyFightProp.EnhanceYingLingMul] = CLuaCopyPropertyFight.SetParamEnhanceYingLingMul
SetParamFunctions[EnumCopyFightProp.EnhanceFlag] = CLuaCopyPropertyFight.SetParamEnhanceFlag
SetParamFunctions[EnumCopyFightProp.EnhanceFlagMul] = CLuaCopyPropertyFight.SetParamEnhanceFlagMul
SetParamFunctions[EnumCopyFightProp.ObjectType] = CLuaCopyPropertyFight.SetParamObjectType
SetParamFunctions[EnumCopyFightProp.MonsterType] = CLuaCopyPropertyFight.SetParamMonsterType
SetParamFunctions[EnumCopyFightProp.FightPropType] = CLuaCopyPropertyFight.SetParamFightPropType
SetParamFunctions[EnumCopyFightProp.PlayHpFull] = CLuaCopyPropertyFight.SetParamPlayHpFull
SetParamFunctions[EnumCopyFightProp.PlayHp] = CLuaCopyPropertyFight.SetParamPlayHp
SetParamFunctions[EnumCopyFightProp.PlayAtt] = CLuaCopyPropertyFight.SetParamPlayAtt
SetParamFunctions[EnumCopyFightProp.PlayDef] = CLuaCopyPropertyFight.SetParamPlayDef
SetParamFunctions[EnumCopyFightProp.PlayHit] = CLuaCopyPropertyFight.SetParamPlayHit
SetParamFunctions[EnumCopyFightProp.PlayMiss] = CLuaCopyPropertyFight.SetParamPlayMiss
SetParamFunctions[EnumCopyFightProp.EnhanceDieKe] = CLuaCopyPropertyFight.SetParamEnhanceDieKe
SetParamFunctions[EnumCopyFightProp.EnhanceDieKeMul] = CLuaCopyPropertyFight.SetParamEnhanceDieKeMul
SetParamFunctions[EnumCopyFightProp.DotRemain] = CLuaCopyPropertyFight.SetParamDotRemain
SetParamFunctions[EnumCopyFightProp.IsConfused] = CLuaCopyPropertyFight.SetParamIsConfused
SetParamFunctions[EnumCopyFightProp.EnhanceSheShouKefu] = CLuaCopyPropertyFight.SetParamEnhanceSheShouKefu
SetParamFunctions[EnumCopyFightProp.EnhanceJiaShiKefu] = CLuaCopyPropertyFight.SetParamEnhanceJiaShiKefu
SetParamFunctions[EnumCopyFightProp.EnhanceDaoKeKefu] = CLuaCopyPropertyFight.SetParamEnhanceDaoKeKefu
SetParamFunctions[EnumCopyFightProp.EnhanceXiaKeKefu] = CLuaCopyPropertyFight.SetParamEnhanceXiaKeKefu
SetParamFunctions[EnumCopyFightProp.EnhanceFangShiKefu] = CLuaCopyPropertyFight.SetParamEnhanceFangShiKefu
SetParamFunctions[EnumCopyFightProp.EnhanceYiShiKefu] = CLuaCopyPropertyFight.SetParamEnhanceYiShiKefu
SetParamFunctions[EnumCopyFightProp.EnhanceMeiZheKefu] = CLuaCopyPropertyFight.SetParamEnhanceMeiZheKefu
SetParamFunctions[EnumCopyFightProp.EnhanceYiRenKefu] = CLuaCopyPropertyFight.SetParamEnhanceYiRenKefu
SetParamFunctions[EnumCopyFightProp.EnhanceYanShiKefu] = CLuaCopyPropertyFight.SetParamEnhanceYanShiKefu
SetParamFunctions[EnumCopyFightProp.EnhanceHuaHunKefu] = CLuaCopyPropertyFight.SetParamEnhanceHuaHunKefu
SetParamFunctions[EnumCopyFightProp.EnhanceYingLingKefu] = CLuaCopyPropertyFight.SetParamEnhanceYingLingKefu
SetParamFunctions[EnumCopyFightProp.EnhanceDieKeKefu] = CLuaCopyPropertyFight.SetParamEnhanceDieKeKefu
SetParamFunctions[EnumCopyFightProp.AdjustPermanentCor] = CLuaCopyPropertyFight.SetParamAdjustPermanentCor
SetParamFunctions[EnumCopyFightProp.AdjustPermanentSta] = CLuaCopyPropertyFight.SetParamAdjustPermanentSta
SetParamFunctions[EnumCopyFightProp.AdjustPermanentStr] = CLuaCopyPropertyFight.SetParamAdjustPermanentStr
SetParamFunctions[EnumCopyFightProp.AdjustPermanentInt] = CLuaCopyPropertyFight.SetParamAdjustPermanentInt
SetParamFunctions[EnumCopyFightProp.AdjustPermanentAgi] = CLuaCopyPropertyFight.SetParamAdjustPermanentAgi
SetParamFunctions[EnumCopyFightProp.SoulCoreLevel] = CLuaCopyPropertyFight.SetParamSoulCoreLevel
SetParamFunctions[EnumCopyFightProp.AdjustPermanentMidCor] = CLuaCopyPropertyFight.SetParamAdjustPermanentMidCor
SetParamFunctions[EnumCopyFightProp.AdjustPermanentMidSta] = CLuaCopyPropertyFight.SetParamAdjustPermanentMidSta
SetParamFunctions[EnumCopyFightProp.AdjustPermanentMidStr] = CLuaCopyPropertyFight.SetParamAdjustPermanentMidStr
SetParamFunctions[EnumCopyFightProp.AdjustPermanentMidInt] = CLuaCopyPropertyFight.SetParamAdjustPermanentMidInt
SetParamFunctions[EnumCopyFightProp.AdjustPermanentMidAgi] = CLuaCopyPropertyFight.SetParamAdjustPermanentMidAgi
SetParamFunctions[EnumCopyFightProp.SumPermanent] = CLuaCopyPropertyFight.SetParamSumPermanent
SetParamFunctions[EnumCopyFightProp.AntiTian] = CLuaCopyPropertyFight.SetParamAntiTian
SetParamFunctions[EnumCopyFightProp.AntiEGui] = CLuaCopyPropertyFight.SetParamAntiEGui
SetParamFunctions[EnumCopyFightProp.AntiXiuLuo] = CLuaCopyPropertyFight.SetParamAntiXiuLuo
SetParamFunctions[EnumCopyFightProp.AntiDiYu] = CLuaCopyPropertyFight.SetParamAntiDiYu
SetParamFunctions[EnumCopyFightProp.AntiRen] = CLuaCopyPropertyFight.SetParamAntiRen
SetParamFunctions[EnumCopyFightProp.AntiChuSheng] = CLuaCopyPropertyFight.SetParamAntiChuSheng
SetParamFunctions[EnumCopyFightProp.AntiBuilding] = CLuaCopyPropertyFight.SetParamAntiBuilding
SetParamFunctions[EnumCopyFightProp.AntiZhaoHuan] = CLuaCopyPropertyFight.SetParamAntiZhaoHuan
SetParamFunctions[EnumCopyFightProp.AntiLingShou] = CLuaCopyPropertyFight.SetParamAntiLingShou
SetParamFunctions[EnumCopyFightProp.AntiBoss] = CLuaCopyPropertyFight.SetParamAntiBoss
SetParamFunctions[EnumCopyFightProp.AntiSheShou] = CLuaCopyPropertyFight.SetParamAntiSheShou
SetParamFunctions[EnumCopyFightProp.AntiJiaShi] = CLuaCopyPropertyFight.SetParamAntiJiaShi
SetParamFunctions[EnumCopyFightProp.AntiDaoKe] = CLuaCopyPropertyFight.SetParamAntiDaoKe
SetParamFunctions[EnumCopyFightProp.AntiXiaKe] = CLuaCopyPropertyFight.SetParamAntiXiaKe
SetParamFunctions[EnumCopyFightProp.AntiFangShi] = CLuaCopyPropertyFight.SetParamAntiFangShi
SetParamFunctions[EnumCopyFightProp.AntiYiShi] = CLuaCopyPropertyFight.SetParamAntiYiShi
SetParamFunctions[EnumCopyFightProp.AntiMeiZhe] = CLuaCopyPropertyFight.SetParamAntiMeiZhe
SetParamFunctions[EnumCopyFightProp.AntiYiRen] = CLuaCopyPropertyFight.SetParamAntiYiRen
SetParamFunctions[EnumCopyFightProp.AntiYanShi] = CLuaCopyPropertyFight.SetParamAntiYanShi
SetParamFunctions[EnumCopyFightProp.AntiHuaHun] = CLuaCopyPropertyFight.SetParamAntiHuaHun
SetParamFunctions[EnumCopyFightProp.AntiYingLing] = CLuaCopyPropertyFight.SetParamAntiYingLing
SetParamFunctions[EnumCopyFightProp.AntiDieKe] = CLuaCopyPropertyFight.SetParamAntiDieKe
SetParamFunctions[EnumCopyFightProp.AntiFlag] = CLuaCopyPropertyFight.SetParamAntiFlag
SetParamFunctions[EnumCopyFightProp.EnhanceZhanKuang] = CLuaCopyPropertyFight.SetParamEnhanceZhanKuang
SetParamFunctions[EnumCopyFightProp.EnhanceZhanKuangMul] = CLuaCopyPropertyFight.SetParamEnhanceZhanKuangMul
SetParamFunctions[EnumCopyFightProp.EnhanceZhanKuangKefu] = CLuaCopyPropertyFight.SetParamEnhanceZhanKuangKefu
SetParamFunctions[EnumCopyFightProp.AntiZhanKuang] = CLuaCopyPropertyFight.SetParamAntiZhanKuang
SetParamFunctions[EnumCopyFightProp.JumpSpeed] = CLuaCopyPropertyFight.SetParamJumpSpeed
SetParamFunctions[EnumCopyFightProp.OriJumpSpeed] = CLuaCopyPropertyFight.SetParamOriJumpSpeed
SetParamFunctions[EnumCopyFightProp.AdjJumpSpeed] = CLuaCopyPropertyFight.SetParamAdjJumpSpeed
SetParamFunctions[EnumCopyFightProp.GravityAcceleration] = CLuaCopyPropertyFight.SetParamGravityAcceleration
SetParamFunctions[EnumCopyFightProp.OriGravityAcceleration] = CLuaCopyPropertyFight.SetParamOriGravityAcceleration
SetParamFunctions[EnumCopyFightProp.AdjGravityAcceleration] = CLuaCopyPropertyFight.SetParamAdjGravityAcceleration
SetParamFunctions[EnumCopyFightProp.HorizontalAcceleration] = CLuaCopyPropertyFight.SetParamHorizontalAcceleration
SetParamFunctions[EnumCopyFightProp.OriHorizontalAcceleration] = CLuaCopyPropertyFight.SetParamOriHorizontalAcceleration
SetParamFunctions[EnumCopyFightProp.AdjHorizontalAcceleration] = CLuaCopyPropertyFight.SetParamAdjHorizontalAcceleration
SetParamFunctions[EnumCopyFightProp.SwimSpeed] = CLuaCopyPropertyFight.SetParamSwimSpeed
SetParamFunctions[EnumCopyFightProp.OriSwimSpeed] = CLuaCopyPropertyFight.SetParamOriSwimSpeed
SetParamFunctions[EnumCopyFightProp.AdjSwimSpeed] = CLuaCopyPropertyFight.SetParamAdjSwimSpeed
SetParamFunctions[EnumCopyFightProp.StrengthPoint] = CLuaCopyPropertyFight.SetParamStrengthPoint
SetParamFunctions[EnumCopyFightProp.StrengthPointMax] = CLuaCopyPropertyFight.SetParamStrengthPointMax
SetParamFunctions[EnumCopyFightProp.OriStrengthPointMax] = CLuaCopyPropertyFight.SetParamOriStrengthPointMax
SetParamFunctions[EnumCopyFightProp.AdjStrengthPointMax] = CLuaCopyPropertyFight.SetParamAdjStrengthPointMax
SetParamFunctions[EnumCopyFightProp.AntiMulEnhanceIllusion] = CLuaCopyPropertyFight.SetParamAntiMulEnhanceIllusion
SetParamFunctions[EnumCopyFightProp.GlideHorizontalSpeedWithUmbrella] = CLuaCopyPropertyFight.SetParamGlideHorizontalSpeedWithUmbrella
SetParamFunctions[EnumCopyFightProp.GlideVerticalSpeedWithUmbrella] = CLuaCopyPropertyFight.SetParamGlideVerticalSpeedWithUmbrella
SetParamFunctions[EnumCopyFightProp.AdjpSpeed] = CLuaCopyPropertyFight.SetParamAdjpSpeed
SetParamFunctions[EnumCopyFightProp.AdjmSpeed] = CLuaCopyPropertyFight.SetParamAdjmSpeed

