--仙职技能引导
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"

CLuaRedDotForSkill = class()
RegistChildComponent(CLuaRedDotForSkill, "m_RedDot","RedDot", GameObject)


function CLuaRedDotForSkill:Awake()
    if not CClientMainPlayer.Inst then 
        self.m_RedDot:SetActive(false)
        return
    end
    if CGuideMgr.Inst:IsInPhase(EnumGuideKey.XianZhiSkill) then
        if CLuaXianzhiMgr.GetXianZhiSKillId()>0 then
            self.m_RedDot:SetActive(true)
        else
            self.m_RedDot:SetActive(false)
            CGuideMgr.Inst:EndCurrentPhase()
        end
    else
        self.m_RedDot:SetActive(false)
    end

end

function CLuaRedDotForSkill:Init()

end

function CLuaRedDotForSkill:OnEnable()
    g_ScriptEvent:AddListener("GuideEnd", self, "OnGuideEnd")
    g_ScriptEvent:AddListener("GuideStart", self, "OnGuideStart")
end
function CLuaRedDotForSkill:OnDisable()
    g_ScriptEvent:RemoveListener("GuideEnd", self, "OnGuideEnd")
    g_ScriptEvent:RemoveListener("GuideStart", self, "OnGuideStart")

end
function CLuaRedDotForSkill:OnGuideEnd(args)
    local phase = args[0]
    if phase == EnumGuideKey.XianZhiSkill then
        self.m_RedDot:SetActive(false)
    end
end
function CLuaRedDotForSkill:OnGuideStart(args)
    local phase = args[0]
    if phase == EnumGuideKey.XianZhiSkill then
        self.m_RedDot:SetActive(true)
    end
end