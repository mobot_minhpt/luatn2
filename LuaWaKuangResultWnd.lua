require("common/common_include")

local UIGrid = import "UIGrid"
local Profession = import "L10.Game.Profession"
local CUICommonDef = import "L10.UI.CUICommonDef"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"

LuaWaKuangResultWnd = class()

RegistChildComponent(LuaWaKuangResultWnd, "TimeLabel", UILabel)
RegistChildComponent(LuaWaKuangResultWnd, "RankBtn", GameObject)
RegistChildComponent(LuaWaKuangResultWnd, "ShareBtn", GameObject)
RegistChildComponent(LuaWaKuangResultWnd, "InfoGrid", UIGrid)
RegistChildComponent(LuaWaKuangResultWnd, "PlayerInfoTemplate", GameObject)

function LuaWaKuangResultWnd:Init()
	self.PlayerInfoTemplate:SetActive(false)

	self.TimeLabel.text = SafeStringFormat3(LocalString.GetString("[FFE400]挑战用时[-]    %s"), self:GetRemainTimeText(LuaWuYiMgr.m_FinishTime))

	self:UpdatePlayerInfo()
	
	CommonDefs.AddOnClickListener(self.RankBtn, DelegateFactory.Action_GameObject(function (go)
		self:OnRankBtnClicked(go)
	end), false)
	if CommonDefs.IS_VN_CLIENT then
		self.ShareBtn:SetActive(false)
	else
		CommonDefs.AddOnClickListener(self.ShareBtn, DelegateFactory.Action_GameObject(function (go)
			self:OnShareBtnClicked(go)
		end), false)
	end
end

function LuaWaKuangResultWnd:GetRemainTimeText(totalSeconds)
    if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        return SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end

function LuaWaKuangResultWnd:UpdatePlayerInfo()
	CUICommonDef.ClearTransform(self.InfoGrid.transform)

	for i = 1, #LuaWuYiMgr.m_PlayerInfos do
		local item = NGUITools.AddChild(self.InfoGrid.gameObject, self.PlayerInfoTemplate)
		self:InitPlayerInfo(item, LuaWuYiMgr.m_PlayerInfos[i])
		item:SetActive(true)
	end

	self.InfoGrid:Reposition()
end

function LuaWaKuangResultWnd:InitPlayerInfo(go, info)
	local Name = go.transform:Find("Name"):GetComponent(typeof(UILabel))
	local Class = go.transform:Find("Class"):GetComponent(typeof(UISprite))
	local Special = go.transform:Find("Special").gameObject

	Name.text = info.name
	Special:SetActive(info.special)
	Class.spriteName = Profession.GetIconByNumber(info.role)
end


function LuaWaKuangResultWnd:OnShareBtnClicked(go)
	--CUICommonDef.CaptureScreenAndShare()
	
	CUICommonDef.CaptureScreen(
        "screenshot",
        true,
        false,
        self.ShareBtn,
        DelegateFactory.Action_string_bytes(
            function(fullPath, jpgBytes)
                ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
            end
        ),
        false
    )
	
end

function LuaWaKuangResultWnd:OnRankBtnClicked(go)
	CUIManager.ShowUI("WaKuangRankWnd")
end
function LuaWaKuangResultWnd:OnEnable()
	CUIManager.CloseUI("WaKuangUpgradeWnd")
end

function LuaWaKuangResultWnd:OnDisable()
	-- body
end

return LuaWaKuangResultWnd