local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CTaskMgr = import "L10.Game.CTaskMgr"
local CScene = import "L10.Game.CScene"
local CGamePlayMgr = import "L10.Game.CGamePlayMgr"
local CButton = import "L10.UI.CButton"
local NGUIMath = import "NGUIMath"

LuaCommonGamePlayTaskView = class()

RegistClassMember(LuaCommonGamePlayTaskView, "m_SingleLineTemplate")
RegistClassMember(LuaCommonGamePlayTaskView, "m_MultiLineTemplate")
RegistClassMember(LuaCommonGamePlayTaskView, "m_ContentTable")
RegistClassMember(LuaCommonGamePlayTaskView, "m_ContentBg")
RegistClassMember(LuaCommonGamePlayTaskView, "m_LeaveButton")
RegistClassMember(LuaCommonGamePlayTaskView, "m_RuleButton")

RegistClassMember(LuaCommonGamePlayTaskView, "m_PlayInfo")
RegistClassMember(LuaCommonGamePlayTaskView, "m_TitleLabel")
RegistClassMember(LuaCommonGamePlayTaskView, "m_ContentLabel")
RegistClassMember(LuaCommonGamePlayTaskView, "m_CountDownLabel")

function LuaCommonGamePlayTaskView:Awake()
    self.m_SingleLineTemplate = self.transform:Find("SingleLineTemplate").gameObject
    self.m_MultiLineTemplate = self.transform:Find("MultiLineTemplate").gameObject
    self.m_ContentTable = self.transform:Find("Table"):GetComponent(typeof(UITable))
    self.m_ContentBg = self.transform:Find("Bg"):GetComponent(typeof(UIWidget))
    self.m_LeaveButton = self.transform:Find("LeaveButton"):GetComponent(typeof(CButton))
    self.m_RuleButton = self.transform:Find("RuleButton"):GetComponent(typeof(CButton))

    self.m_SingleLineTemplate:SetActive(false)
    self.m_MultiLineTemplate:SetActive(false)

    UIEventListener.Get(self.m_LeaveButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnLeaveButtonClick(go)
    end)

    UIEventListener.Get(self.m_RuleButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnRuleButtonClick(go)
    end)
end

function LuaCommonGamePlayTaskView:GetGuideGo(methodName)
    if methodName == "GetLeaveButton" then
        return self.m_LeaveButton.gameObject
    end
    return nil
end

function LuaCommonGamePlayTaskView:OnLeaveButtonClick(go)
    if self.m_PlayInfo and self.m_PlayInfo.clickLeaveBtnFunc then
        self.m_PlayInfo.clickLeaveBtnFunc()
    else
        CGamePlayMgr.Inst:LeavePlay()
    end
end

function LuaCommonGamePlayTaskView:OnRuleButtonClick(go)
    if self.m_PlayInfo and self.m_PlayInfo.clickRuleBtnFunc then
        self.m_PlayInfo.clickRuleBtnFunc()
    else
        --do nothing
    end
end

function LuaCommonGamePlayTaskView:Init()
    self.m_PlayInfo = LuaCommonGamePlayTaskViewMgr:GetCurPlayInfo()
    if not self.m_PlayInfo then return end
    Extensions.RemoveAllChildren(self.m_ContentTable.transform)
    --title
    if self.m_PlayInfo.bShowTitle then
        self.m_TitleLabel = self:AddTitle(self:GetTitle())
    else
        self.m_TitleLabel = nil
    end
    --content
    if self.m_PlayInfo.bShowContent then
        self.m_ContentLabel = self:AddMultiLineText(self:GetContent(), nil)
    else
        self.m_ContentLabel = nil
    end
    --leave button
    if self.m_PlayInfo.bShowLeaveBtn then
        self.m_LeaveButton.gameObject:SetActive(true)
        self.m_LeaveButton.Text = self.m_PlayInfo.leaveBtnText and self.m_PlayInfo.leaveBtnText or LocalString.GetString("离开")
    else
        self.m_LeaveButton.gameObject:SetActive(false)
    end
    --rule button
    if self.m_PlayInfo.bShowRuleBtn then
        self.m_RuleButton.gameObject:SetActive(true)
        self.m_RuleButton.Text = self.m_PlayInfo.ruleBtnText and self.m_PlayInfo.ruleBtnText or LocalString.GetString("规则")
    else
        self.m_RuleButton.gameObject:SetActive(false)
    end
    --count down
    if self.m_PlayInfo.bShowCountDown then
        self.m_CountDownLabel = self:AddSingleLineText("", nil)
    else
        self.m_CountDownLabel = nil
    end
    self:Layout()
end

function LuaCommonGamePlayTaskView:Layout()
    self.m_ContentTable:Reposition()
    local contentHeight = NGUIMath.CalculateRelativeWidgetBounds(self.m_ContentTable.transform).size.y + self.m_ContentTable.padding.y * 2
    self.m_ContentBg.height = math.ceil(contentHeight)
end


--添加标题
function LuaCommonGamePlayTaskView:AddTitle(title)
    return self:AddSingleLineText(title, "FBC701")
end
--添加倒计时
function LuaCommonGamePlayTaskView:AddCountDown()
    return self:AddSingleLineText("", "FFFFFF")
end

--添加单行文字
function LuaCommonGamePlayTaskView:AddSingleLineText(text, hexColorStr)
    local child = CUICommonDef.AddChild(self.m_ContentTable.gameObject, self.m_SingleLineTemplate)
    child:SetActive(true)
    local lbl = child:GetComponent(typeof(UILabel))
    lbl.text = CUICommonDef.TranslateToNGUIText(text)
    lbl.color = NGUIText.ParseColor24(hexColorStr and hexColorStr or "FFFFFF", 0)
    return lbl
end
--添加多行文字
function LuaCommonGamePlayTaskView:AddMultiLineText(text, hexColorStr)
    local child = CUICommonDef.AddChild(self.m_ContentTable.gameObject, self.m_MultiLineTemplate)
    child:SetActive(true)
    local lbl = child:GetComponent(typeof(UILabel))
    lbl.text = CUICommonDef.TranslateToNGUIText(text)
    lbl.color = NGUIText.ParseColor24(hexColorStr and hexColorStr or "FFFFFF", 0)
    return lbl
end

function LuaCommonGamePlayTaskView:OnEnable()
    g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
    g_ScriptEvent:AddListener("OnCommonGamePlayTaskViewUpdateTitle", self, "OnUpdateTitle")
    g_ScriptEvent:AddListener("OnCommonGamePlayTaskViewUpdateContent", self, "OnUpdateContent")
    g_ScriptEvent:AddListener("OnCommonGamePlayTaskViewUpdateTitleAndContent", self, "OnUpdateTitleAndContent")
end

function LuaCommonGamePlayTaskView:OnDisable()
    g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
    g_ScriptEvent:RemoveListener("OnCommonGamePlayTaskViewUpdateTitle", self, "OnUpdateTitle")
    g_ScriptEvent:RemoveListener("OnCommonGamePlayTaskViewUpdateContent", self, "OnUpdateContent")
    g_ScriptEvent:RemoveListener("OnCommonGamePlayTaskViewUpdateTitleAndContent", self, "OnUpdateTitleAndContent")
end

function LuaCommonGamePlayTaskView:OnSceneRemainTimeUpdate(args)
    if not self.m_CountDownLabel then return end
    if CScene.MainScene then
        if CScene.MainScene.ShowTimeCountDown then
            local leftTimeSeconds = self.m_PlayInfo and self.m_PlayInfo.getRemainTimeFunc and self.m_PlayInfo.getRemainTimeFunc() or CScene.MainScene.ShowTime
            self.m_CountDownLabel.text = SafeStringFormat3(LocalString.GetString("剩余 [c][00ff00]%s[-][/c]"), LuaGamePlayMgr:GetRemainTimeText(leftTimeSeconds))
        else
            self.m_CountDownLabel.text = nil
        end
    else
        self.m_CountDownLabel.text = nil
    end
end

function LuaCommonGamePlayTaskView:OnUpdateTitle()
    if not self.m_TitleLabel then return end
    self.m_TitleLabel.text = CUICommonDef.TranslateToNGUIText(self:GetTitle())
    self:Layout()
end

function LuaCommonGamePlayTaskView:OnUpdateContent()
    if not self.m_ContentLabel then return end
    self.m_ContentLabel.text = CUICommonDef.TranslateToNGUIText(self:GetContent())
    self:Layout()
end

function LuaCommonGamePlayTaskView:OnUpdateTitleAndContent()
    if not self.m_TitleLabel then return end
    self.m_TitleLabel.text = CUICommonDef.TranslateToNGUIText(self:GetTitle())
    if not self.m_ContentLabel then return end
    self.m_ContentLabel.text = CUICommonDef.TranslateToNGUIText(self:GetContent())
    self:Layout()
end

function LuaCommonGamePlayTaskView:GetTitle()
    if self.m_PlayInfo and self.m_PlayInfo.bUpdateBySyncStateInfoRPC then
        if self.m_PlayInfo.gameplayId == LuaCommonGamePlayTaskViewMgr.m_GameplayStateInfoByRPC.gameplayId
        -- 让一些玩法副本可以使用其它玩法副本的id同步
        or self.m_PlayInfo.bUpdateBySyncStateInfoRPC == LuaCommonGamePlayTaskViewMgr.m_GameplayStateInfoByRPC.gameplayId then
            return LuaCommonGamePlayTaskViewMgr.m_GameplayStateInfoByRPC.title
        else
            return nil
        end
    else
        return self.m_PlayInfo and self.m_PlayInfo.getTitleFunc() or nil
    end
end

function LuaCommonGamePlayTaskView:GetContent()
    if self.m_PlayInfo and self.m_PlayInfo.bUpdateBySyncStateInfoRPC then
        if self.m_PlayInfo.gameplayId == LuaCommonGamePlayTaskViewMgr.m_GameplayStateInfoByRPC.gameplayId
        -- 让一些玩法副本可以使用其它玩法副本的id同步
        or self.m_PlayInfo.bUpdateBySyncStateInfoRPC == LuaCommonGamePlayTaskViewMgr.m_GameplayStateInfoByRPC.gameplayId then
            return LuaCommonGamePlayTaskViewMgr.m_GameplayStateInfoByRPC.content
        else
            return nil
        end
    else
        return self.m_PlayInfo and self.m_PlayInfo.getContentFunc() or nil
    end
end

-----------------------------------------------------------------------------------

LuaCommonGamePlayTaskViewMgr = {}
LuaCommonGamePlayTaskViewMgr.m_GamePlayInfoTblInited = false
LuaCommonGamePlayTaskViewMgr.m_GamePlayInfoTbl = {}
LuaCommonGamePlayTaskViewMgr.m_GameplayStateInfoByRPC = {}

function LuaCommonGamePlayTaskViewMgr:GetCurPlayInfo()
    self:InitGamePlayInfoTbl()
    local gameplayId = CScene.MainScene and CScene.MainScene.GamePlayDesignId or 0
    if gameplayId == 0 then return nil end
    for playId,data in pairs(self.m_GamePlayInfoTbl) do
        if data.gameplayId == gameplayId then
            return data
        end
    end
    return nil
end

function LuaCommonGamePlayTaskViewMgr:IsInPlay()
    self:InitGamePlayInfoTbl()
    local gameplayId = CScene.MainScene and CScene.MainScene.GamePlayDesignId or 0
    if gameplayId == 0 then return false end
    for playId,data in pairs(self.m_GamePlayInfoTbl) do
        if data.gameplayId == gameplayId then
            return true
        end
    end
    return false
end

function LuaCommonGamePlayTaskViewMgr:OnPlayerLogin()
    g_ScriptEvent:RemoveListener("UpdateGamePlayStageInfo", self, "OnUpdateGamePlayStageInfo")
    g_ScriptEvent:AddListener("UpdateGamePlayStageInfo", self, "OnUpdateGamePlayStageInfo")
end

function LuaCommonGamePlayTaskViewMgr:OnUpdateGamePlayStageInfo(gameplayId,title,content)
    self.m_GameplayStateInfoByRPC.gameplayId = gameplayId
    self.m_GameplayStateInfoByRPC.title = title
    self.m_GameplayStateInfoByRPC.content = content
    self:OnUpdateTitleAndContent()
end

--将玩法自定义内容通过函数列表的形式加入到mgr里面
--[[
    @desc: 
    author:Codee
    time:2023-10-25 11:02:48
    --@gameplayId:
	--@bShowCountDown:自定义是否显示倒计时
	--@getRemainTimeFunc:默认传入nil，可以根据玩法需要传入自己的时间
	--@bUpdateBySyncStateInfoRPC:是否通过Gas2Gac.UpdateGamePlayStageInfo刷新标题和内容,少数玩法涉及，大部分情况为false即可
	--@bShowTitle:自定义显示标题
	--@getTitleFunc:
	--@bShowContent:自定义显示内容
	--@getContentFunc:
	--@bShowLeaveBtn:是否显示离开按钮（默认为true）
	--@leaveBtnText:离开按钮文本（传入nil时默认显示“离开”）
	--@clickLeaveBtnFunc:点击离开按钮的行为（默认是离开副本）
	--@bShowRuleBtn:自定义规则按钮：是否显示规则按钮（默认为true）
	--@ruleBtnText:规则按钮文本（传入nil时默认显示“规则”）
	--@clickRuleBtnFunc: 点击规则按钮的行为
    @return:
]]
function LuaCommonGamePlayTaskViewMgr:AppendGameplayInfo(
    gameplayId, 
    bShowCountDown, getRemainTimeFunc,
    bUpdateBySyncStateInfoRPC,
    bShowTitle, getTitleFunc,
    bShowContent, getContentFunc,
    bShowLeaveBtn, leaveBtnText, clickLeaveBtnFunc,
    bShowRuleBtn, ruleBtnText, clickRuleBtnFunc)

    if gameplayId==nil or gameplayId==0 then return end

    local tbl = {}
    tbl.gameplayId = gameplayId
    tbl.bShowCountDown = bShowCountDown
    tbl.getRemainTimeFunc = getRemainTimeFunc
    tbl.bUpdateBySyncStateInfoRPC = bUpdateBySyncStateInfoRPC
    tbl.bShowTitle = bShowTitle
    tbl.getTitleFunc = getTitleFunc
    tbl.bShowContent = bShowContent
    tbl.getContentFunc = getContentFunc
    tbl.bShowLeaveBtn = bShowLeaveBtn
    tbl.leaveBtnText = leaveBtnText
    tbl.clickLeaveBtnFunc = clickLeaveBtnFunc
    tbl.bShowRuleBtn = bShowRuleBtn
    tbl.ruleBtnText = ruleBtnText
    tbl.clickRuleBtnFunc = clickRuleBtnFunc
    self:AppendGameplayInfoByTbl(tbl)
end
--同AppendGameplayInfo，区别在于参数可以以table形式传入
function LuaCommonGamePlayTaskViewMgr:AppendGameplayInfoByTbl(infoTbl)
    if not infoTbl or not infoTbl.gameplayId or infoTbl.gameplayId==0 then
        error("invalid gameplay info tbl")
        return 
    end
    self.m_GamePlayInfoTbl[infoTbl.gameplayId] = infoTbl
end

--玩法可以通过以下两个方法刷新view的title和content部分
function LuaCommonGamePlayTaskViewMgr:OnUpdateTitle()
    g_ScriptEvent:BroadcastInLua("OnCommonGamePlayTaskViewUpdateTitle")
end

function LuaCommonGamePlayTaskViewMgr:OnUpdateContent()
    g_ScriptEvent:BroadcastInLua("OnCommonGamePlayTaskViewUpdateContent")
end

function LuaCommonGamePlayTaskViewMgr:OnUpdateTitleAndContent()
    g_ScriptEvent:BroadcastInLua("OnCommonGamePlayTaskViewUpdateTitleAndContent")
end

function LuaCommonGamePlayTaskViewMgr:InitGamePlayInfoTbl()
    if self.m_GamePlayInfoTblInited then return end 
    self.m_GamePlayInfoTblInited = true
    --[情义春秋]九夏果吟
    self:AppendGameplayInfo(LuaShiTuMgr:GetFruitPartyTaskPlayId(), 
        true, nil,
        false,
        true, function() return LuaShiTuMgr:GetFruitPartyTaskTitle() end,
        true, function() return LuaShiTuMgr:GetFruitPartyTaskContent() end,
        true, nil, nil,
        true, nil, function() LuaShiTuMgr:OnFruitPartyTaskRuleClick() end)
    --[元旦]元旦奇缘
    self:AppendGameplayInfo(LuaYuanDan2023Mgr.GetYuanDanQiYuanPlayId(),
        true, nil,
        false,
        true, function() return LuaYuanDan2023Mgr.GetYuanDanQiYuanTaskTitle() end,
        true, function() return LuaYuanDan2023Mgr.GetYuanDanQiYuanTaskDes() end,
        true, nil, nil,
        true, nil, function() LuaYuanDan2023Mgr.OnClickYuanDanQiYuanTask() end)
    --[春节]饕餮盛宴
    self:AppendGameplayInfo(LuaChunJie2023Mgr:GetTTSYPlayId(),
        true, function() return LuaChunJie2023Mgr:GetTTSYStateRemainTime() end,
        false,
        true, function() return LuaChunJie2023Mgr:GetTTSYTaskTitle() end,
        true, function() return LuaChunJie2023Mgr:GetTTSYTaskContent() end,
        true, nil, nil,
        true, nil, function() LuaChunJie2023Mgr:OnClickTTSYTask() end)
    --[春节]狩猎饕餮
    self:AppendGameplayInfo(LuaChunJie2023Mgr:GetSLTTPlayId(),
        true, nil,
        false,
        true, function() return LuaChunJie2023Mgr:GetSLTTTaskTitle() end,
        false, nil,
        true, nil, nil,
        true, LocalString.GetString("规则"), function() LuaChunJie2023Mgr:OnClickSLTTTask() end)
    --[圣诞]融雪逐麋鹿
    self:AppendGameplayInfo(LuaChristmas2022Mgr:GetTurkeyMatchPlayId(),
        true, nil,
        false,
        true, function() return LuaChristmas2022Mgr:GetTurkeyMatchTaskTitle() end,
        true, function() return LuaChristmas2022Mgr:GetTurkeyMatchTaskContent() end,
        true, nil, nil,
        true, nil, function() LuaChristmas2022Mgr:OnClickTurkeyMatchTask() end)
    --[寒假]雪魄历险
    HanJia2023_XuePoLiXianLevel.Foreach(function(_, v)
        local gamePlayId = v.GameplayId
        self:AppendGameplayInfo(gamePlayId,
                false, nil,
                true,
                true, function() return LuaHanJia2023Mgr:GetHanJia2023DefaultTaskTitle(gamePlayId) end,
                true, function() return LuaHanJia2023Mgr:GetHanJia2023DefaultTaskContent(gamePlayId) end,
                true, LocalString.GetString("规则"), function() g_MessageMgr:ShowMessage("SnowAdventure_InGameRule") end,
                true, LocalString.GetString("数据"), function() Gac2Gas.QueryXuePoLiXianBattleInfo() end)
    end)
    --[寒假]冰天试炼
    HanJia2023_BingTianShiLian.Foreach(function(_, v)
        local gamePlayId = v.GameplayId
        self:AppendGameplayInfo(gamePlayId,
                false, nil,
                true,
                true, function() return LuaHanJia2023Mgr:GetHanJia2023DefaultTaskTitle(gamePlayId) end,
                true, function() return LuaHanJia2023Mgr:GetHanJia2023DefaultTaskContent(gamePlayId) end,
                true, nil, nil,
                true, nil, function() g_MessageMgr:ShowMessage("HanJia2023_SnowManPVE_Tips") end)
    end)
    -- 情人节2023-灵感探秘
    self:AppendGameplayInfo(LuaValentine2023Mgr:GetEasyLGTMGameplayId(),
        true, nil,
        true,
        true, nil,
        true, nil,
        true, nil, nil,
        true, nil, function() LuaValentine2023Mgr:OnRuleButtonClick() end)
    self:AppendGameplayInfo(LuaValentine2023Mgr:GetHardLGTMGameplayId(),
        true, nil,
        true,
        true, nil,
        true, nil,
        true, nil, nil,
        true, nil, function() LuaValentine2023Mgr:OnRuleButtonClick() end)
    -- 清明2023-春游显神通
    self:AppendGameplayInfo(LuaQingMing2023Mgr:GetPVPGamePlayId(),
        true, nil,
        false,
        true, function() return LuaQingMing2023Mgr:GetTaskViewTitle() end,
        true, function() return LuaQingMing2023Mgr:GetTaskViewContent() end,
        true, nil, nil,
        true, nil, function() LuaQingMing2023Mgr:OnRuleButtonClick() end)
    -- 端午2023-PVE
    self:AppendGameplayInfo(LuaDuanWu2023Mgr:GetPVEGameplayId(),
        true, nil,
        true,
        true, nil,
        true, nil,
        true, nil, nil,
        true, nil, function() LuaDuanWu2023Mgr:OnPVERuleButtonClick() end)
    -- 横屿荡寇副本
    self:AppendGameplayInfo(LuaHengYuDangKouMgr:GetGamePlayId(),
        true, nil,
        false,
        true, function() return LuaHengYuDangKouMgr:GetTaskViewTitle() end,
        true, function() return LuaHengYuDangKouMgr:GetTaskViewContent() end,
        true, nil, nil,
        true, LocalString.GetString("战况"), function() LuaHengYuDangKouMgr:OnStatusButtonClick() end)
    --注意！除了这里添加，还可以在进入玩法时，在玩法Mgr里面调用AppendGameplayInfo或者AppendGameplayInfoByTbl方法，这样可以利用到Mgr延迟加载的机制
end


