-- Auto Generated!!
local CYuanDanLotteryResultWnd = import "L10.UI.CYuanDanLotteryResultWnd"
local CYuanDanMgr = import "L10.Game.CYuanDanMgr"
local LocalString = import "LocalString"
local NGUIText = import "NGUIText"
CYuanDanLotteryResultWnd.m_Init_CS2LuaHook = function (this) 
    local default
    if CYuanDanMgr.Inst.MainPlayerInAwardsList then
        default = LocalString.GetString("恭喜中奖！")
    else
        default = LocalString.GetString("非常遗憾未中奖")
    end
    this.titleLabel.text = default
    local extern
    if CYuanDanMgr.Inst.MainPlayerInAwardsList then
        extern = CYuanDanLotteryResultWnd.GreenColor
    else
        extern = CYuanDanLotteryResultWnd.GrayColor
    end
    this.titleLabel.color = NGUIText.ParseColor24(extern, 0)
    this.awardTextLabel.text = g_MessageMgr:FormatMessage("NewYear_ZhongJiang", CYuanDanMgr.Inst.AwardsListCount)
    this.awardsListContent.text = CYuanDanMgr.Inst.AwardsListContent
    this.scrollView:ResetPosition()
end
