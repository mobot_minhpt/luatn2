local UIProgressBar = import "UIProgressBar"
local CTopAndRightTipWnd = import "L10.UI.CTopAndRightTipWnd"
local CMainCamera = import "L10.Engine.CMainCamera"
local CCommonUIFxWnd = import "L10.UI.CCommonUIFxWnd"

LuaYuanXiaoDefenseTopRightWnd = class()

RegistClassMember(LuaYuanXiaoDefenseTopRightWnd, "ExpandButton")
RegistClassMember(LuaYuanXiaoDefenseTopRightWnd, "ScoreProgress")
RegistClassMember(LuaYuanXiaoDefenseTopRightWnd, "ProgressLabel")
RegistClassMember(LuaYuanXiaoDefenseTopRightWnd, "Mark1")
RegistClassMember(LuaYuanXiaoDefenseTopRightWnd, "Mark2")
RegistClassMember(LuaYuanXiaoDefenseTopRightWnd, "Mark3")
RegistClassMember(LuaYuanXiaoDefenseTopRightWnd, "Grid")
RegistClassMember(LuaYuanXiaoDefenseTopRightWnd, "Template")
RegistClassMember(LuaYuanXiaoDefenseTopRightWnd, "ScoreForeground")


function LuaYuanXiaoDefenseTopRightWnd:Awake()
    self.ExpandButton = self.transform:Find("Anchor/Offset/ExpandButton").gameObject
    self.ScoreProgress = self.transform:Find("Anchor/Offset/ScoreProgress"):GetComponent(typeof(UIProgressBar))
    self.ScoreForeground = self.ScoreProgress.transform:Find("ForegroundSprite"):GetComponent(typeof(UISprite))
    self.ProgressLabel = self.transform:Find("Anchor/Offset/ScoreProgress/ProgressLabel"):GetComponent(typeof(UILabel))
    self.Mark1 = self.transform:Find("Anchor/Offset/ScoreProgress/Mark1")
    self.Mark2 = self.transform:Find("Anchor/Offset/ScoreProgress/Mark2")
    self.Mark3 = self.transform:Find("Anchor/Offset/ScoreProgress/Mark3")
    self.Grid = self.transform:Find("Anchor/Offset/TableView/TableBody/Grid"):GetComponent(typeof(UIGrid))
    self.Template = self.transform:Find("Anchor/Offset/TableView/Pool/Template").gameObject
    self.Template:SetActive(false)

    UIEventListener.Get(self.ExpandButton).onClick = LuaUtils.VoidDelegate(function (go)
	    self:OnExpandButtonClick()
	end)
end

function LuaYuanXiaoDefenseTopRightWnd:OnEnable()
    self:SyncNaoYuanXiaoTeamPlayInfo(LuaYuanXiaoDefenseMgr.TeamScore, LuaYuanXiaoDefenseMgr.PlayerScore)
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("SyncNaoYuanXiaoTeamPlayInfo", self, "SyncNaoYuanXiaoTeamPlayInfo")
    g_ScriptEvent:AddListener("NaoYuanXiaoCommitFx", self, "NaoYuanXiaoCommitFx")
end

function LuaYuanXiaoDefenseTopRightWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("SyncNaoYuanXiaoTeamPlayInfo", self, "SyncNaoYuanXiaoTeamPlayInfo")
    g_ScriptEvent:RemoveListener("NaoYuanXiaoCommitFx", self, "NaoYuanXiaoCommitFx")
end

function LuaYuanXiaoDefenseTopRightWnd:NaoYuanXiaoCommitFx()
    if self.Grid.transform.childCount == 0 or not CClientMainPlayer.Inst then return end
    local nodeTransform = self.Grid.transform:GetChild(0)
    local moveTime = 1
    local screenPos = CMainCamera.Main:WorldToScreenPoint(CClientMainPlayer.Inst.RO.Position)
    screenPos.z = 0
    local nguiWorldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)
    CCommonUIFxWnd.Instance:PlayLineAni(nguiWorldPos, nodeTransform.position, moveTime, DelegateFactory.Action(function()end))
end

function LuaYuanXiaoDefenseTopRightWnd:Init()
    self.transform:GetComponent(typeof(UIPanel)).depth = 1
    self.transform:Find("Anchor/Offset/TableView/TableBody"):GetComponent(typeof(UIPanel)).depth = 2
end

function LuaYuanXiaoDefenseTopRightWnd:OnExpandButtonClick()
    LuaUtils.SetLocalRotation(self.ExpandButton.transform, 0, 0, 0)
	CTopAndRightTipWnd.showPackage = false
	CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

function LuaYuanXiaoDefenseTopRightWnd:OnHideTopAndRightTipWnd()
    self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
end

function LuaYuanXiaoDefenseTopRightWnd:SyncNaoYuanXiaoTeamPlayInfo(teamScore, playerScore)
    --[[ 
    --分数进度条已删除
    local setting = YuanXiao2023_NaoYuanXiao.GetData()

    local r1 = setting.RewardItemIdsToScore[0][1]
    local r2 = setting.RewardItemIdsToScore[1][1]
    local r3 = setting.RewardItemIdsToScore[2][1]
    local rp = {
        r1 / r3 * 0.85,
        r2 / r3 * 0.85,
        0.85,
    }
    -- x: -160 ~ 40
    local rx = {
        -160 + 200 * rp[1],
        -160 + 200 * rp[2],
        -160 + 200 * rp[3], 
    }

    if teamScore <= r3 then
        self.ScoreProgress.value = teamScore / r3 * 0.85
    elseif teamScore <= setting.TerminalScore then
        self.ScoreProgress.value = 0.85 + (teamScore - r3) / (setting.TerminalScore - r3)
    else
        self.ScoreProgress.value = 1
    end
    self.ProgressLabel.text = LocalString.GetString("总分 ")..teamScore
    for i = 1, 3 do
        if teamScore >= setting.RewardItemIdsToScore[i-1][1] then
            self["Mark"..i]:Find("Unlock").gameObject:SetActive(true)
        else
            self["Mark"..i]:Find("Unlock").gameObject:SetActive(false)
        end
        LuaUtils.SetLocalPositionX(self["Mark"..i], rx[i])
    end
    --]]

    Extensions.RemoveAllChildren(self.Grid.transform)

    if CClientMainPlayer.Inst then
        local score, rank
        for i = 1, #playerScore do
            if playerScore[i].name == CClientMainPlayer.Inst.Name then 
                score = playerScore[i].score
                rank = i
                break
            end
        end
        if rank then 
            local mainPlayerObj = NGUITools.AddChild(self.Grid.gameObject, self.Template)
            mainPlayerObj:SetActive(true)
            mainPlayerObj.transform:Find("Selected").gameObject:SetActive(true)
            local nameLabel = mainPlayerObj.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
            nameLabel.text = CClientMainPlayer.Inst.Name
            nameLabel.color = Color.green
            local scoreLabel = mainPlayerObj.transform:Find("ScoreLabel"):GetComponent(typeof(UILabel))
            scoreLabel.text = score
            scoreLabel.color = Color.green
            local rankTop3 = mainPlayerObj.transform:Find("Rank_Top3"):GetComponent(typeof(UISprite))
            local rankLabel = mainPlayerObj.transform:Find("Rank_Label"):GetComponent(typeof(UILabel))
            if rank > 3 then 
                rankTop3.gameObject:SetActive(false)
                rankLabel.gameObject:SetActive(true)
                rankLabel.text = rank
                rankLabel.color = Color.green
            else
                rankTop3.gameObject:SetActive(true)
                rankLabel.gameObject:SetActive(false)
                rankTop3.spriteName = "headinfownd_jiashang_0"..rank
            end
        end
    end

    for i = 1, 4 do
        if i > #playerScore then break end
        local obj = NGUITools.AddChild(self.Grid.gameObject, self.Template)
        obj:SetActive(true)
        obj.transform:Find("Selected").gameObject:SetActive(false)
        local nameLabel = obj.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
        nameLabel.text = playerScore[i].name
        local scoreLabel = obj.transform:Find("ScoreLabel"):GetComponent(typeof(UILabel))
        scoreLabel.text = playerScore[i].score
        local rankTop3 = obj.transform:Find("Rank_Top3"):GetComponent(typeof(UISprite))
        local rankLabel = obj.transform:Find("Rank_Label"):GetComponent(typeof(UILabel))
        if i > 3 then 
            rankTop3.gameObject:SetActive(false)
            rankLabel.gameObject:SetActive(true)
            rankLabel.text = i
        else
            rankTop3.gameObject:SetActive(true)
            rankLabel.gameObject:SetActive(false)
            rankTop3.spriteName = "headinfownd_jiashang_0"..i
        end
    end
    self.Grid:Reposition()
end
