local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local QnTableView = import "L10.UI.QnTableView"
local CButton = import "L10.UI.CButton"
local CPackageItemCell = import "L10.UI.CPackageItemCell"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"

LuaAntiProfessionConvertChooseWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaAntiProfessionConvertChooseWnd, "VoidLabel", "VoidLabel", GameObject)
RegistChildComponent(LuaAntiProfessionConvertChooseWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaAntiProfessionConvertChooseWnd, "CommitBtn", "CommitBtn", CButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaAntiProfessionConvertChooseWnd, "m_Class")
RegistClassMember(LuaAntiProfessionConvertChooseWnd, "m_Infos")
RegistClassMember(LuaAntiProfessionConvertChooseWnd, "m_LevelLimit")

function LuaAntiProfessionConvertChooseWnd:Awake()
    self.m_LevelLimit = SoulCore_Settings.GetData().AntiProfessionChangeLv
    self.m_LevelLimit = self.m_LevelLimit and self.m_LevelLimit or 20
    self.transform:Find("Anchor/Offset/DescLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("仅克符等级≥%d级的材料可转换"), self.m_LevelLimit)

    UIEventListener.Get(self.CommitBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        if self.m_Class ~= nil then
            g_ScriptEvent:BroadcastInLua("AntiProfessionConvertChoose", self.m_Class)
            CUIManager.CloseUI(CLuaUIResources.AntiProfessionConvertChooseWnd)
        else 
            g_MessageMgr:ShowMessage("ANTIPROFESSION_CONVERTCHOOSEWND_NOSELECT")
        end
	end)

    self.VoidLabel:SetActive(false)
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaAntiProfessionConvertChooseWnd:Init()
    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_Infos
        end,
        function(item, index)
            self:InitItem(item, self.m_Infos[index + 1])
        end
    )

    self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        local info = self.m_Infos[row+1]
        if info.Num == 1 then
            g_MessageMgr:ShowMessage("ANTIPROFESSION_CONVERTCHOOSEWND_NOTENOUGH")
            return
        end

        local tableItem = self.TableView:GetItemAtRow(row)
        local selectedItemCell = tableItem.transform:GetComponent(typeof(CPackageItemCell))

        for i=1,#self.m_Infos do
            local tableItem = self.TableView:GetItemAtRow(i-1)
            local itemCell = tableItem.transform:GetComponent(typeof(CPackageItemCell))
            if itemCell ~= nil and itemCell ~= selectedItemCell then
                itemCell.Selected = false
            end
        end

        selectedItemCell.Selected = true
        self.m_Class = info.Profession

        local itemId = SoulCore_Settings.GetData().AntiProfessionItemId[info.Profession - 1][1]
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
    end)

    self:RefreshData()
end

function LuaAntiProfessionConvertChooseWnd:OnEnable()
    g_ScriptEvent:AddListener("AntiProfessionUpdateScore", self, "RefreshData")
end

function LuaAntiProfessionConvertChooseWnd:OnDisable()
    g_ScriptEvent:RemoveListener("AntiProfessionUpdateScore", self, "RefreshData")
end

function LuaAntiProfessionConvertChooseWnd:RefreshData()
    self.m_Infos = {}

    CommonDefs.DictIterate(CClientMainPlayer.Inst.SkillProp.AntiProfessionSkill, DelegateFactory.Action_object_object(function(___key, ___value)
        if ___value.CurLevel >= self.m_LevelLimit then
            local ownScore = CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.SkillProp.AntiProfessionScore, ___value.Profession) and CClientMainPlayer.Inst.SkillProp.AntiProfessionScore[___value.Profession] or 0
            if ownScore >= 1 then
                table.insert(self.m_Infos, {Profession = ___value.Profession, CurLevel = ___value.CurLevel, Num = ownScore})
            end
        end
    end))
    table.sort(self.m_Infos, function (a, b)
        if a.Num > b.Num then
            return true
        elseif a.Num == b.Num then
            if a.CurLevel > b.CurLevel then
                return true
            elseif a.CurLevel == b.CurLevel then
                return a.Profession < b.Profession
            end
        end
        return false
    end)
    if #self.m_Infos == 0 then
        self.CommitBtn.Enabled = false
        self.VoidLabel:SetActive(true)
    else
        self.CommitBtn.Enabled = true
        self.VoidLabel:SetActive(false)
    end
    self.TableView:ReloadData(true, false)
end

function LuaAntiProfessionConvertChooseWnd:InitItem(item, info)
    local itemCell = item:GetComponent(typeof(CPackageItemCell))
    
    itemCell:Init("", false, false, false, nil)
    itemCell.Selected = false
    itemCell.iconTexture:LoadMaterial(LuaZongMenMgr:GetProfessionScoreIconPath(info.Profession))

    if info.Num == 1 then
        itemCell.disableSprite.gameObject:SetActive(true)
        itemCell.amountLabel.text = ""
    else
        itemCell.disableSprite.gameObject:SetActive(false)
        itemCell.amountLabel.text = info.Num 
    end
end

--@region UIEvent

--@endregion UIEvent

