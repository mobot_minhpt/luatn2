-- Auto Generated!!
local CLingShouSwitchSkillSlot = import "L10.UI.CLingShouSwitchSkillSlot"
CLingShouSwitchSkillSlot.m_OnChooseSkill_CS2LuaHook = function (this, id, state) 
    if this.skillId == id then
        if this.tickSprite ~= nil then
            this.tickSprite:SetActive(state)
        end
    end
end
CLingShouSwitchSkillSlot.m_OnLockSkill_CS2LuaHook = function (this, id, state) 
    if this.skillId == id then
        if this.lockSprite ~= nil then
            this.lockSprite:SetActive(state)
        end
    end
end
