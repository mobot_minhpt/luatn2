local CRankData=import "L10.UI.CRankData"
local Profession=import "L10.Game.Profession"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local CPlayerInfoMgr=import "CPlayerInfoMgr"
local EnumPlayerInfoContext=import "L10.Game.EnumPlayerInfoContext"
local EChatPanel=import "L10.Game.EChatPanel"
local AlignType=import "CPlayerInfoMgr+AlignType"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"

local UILabel=import "UILabel"
local QnTableView=import "L10.UI.QnTableView"


CLuaDouDiZhu2020RankWnd=class()
-- 主玩家相关信息
RegistClassMember(CLuaDouDiZhu2020RankWnd,"m_MyRankLabel")
RegistClassMember(CLuaDouDiZhu2020RankWnd,"m_MyNameLabel")
RegistClassMember(CLuaDouDiZhu2020RankWnd,"m_MyScoreLabel")
RegistClassMember(CLuaDouDiZhu2020RankWnd,"m_MyClsSprite")

RegistClassMember(CLuaDouDiZhu2020RankWnd,"m_TitleLabel")

RegistClassMember(CLuaDouDiZhu2020RankWnd,"m_TableViewDataSource")
RegistClassMember(CLuaDouDiZhu2020RankWnd,"m_TableView")
RegistClassMember(CLuaDouDiZhu2020RankWnd,"m_RankList")
-- CLuaDouDiZhu2020RankWnd.s_MyRank = 0
-- CLuaDouDiZhu2020RankWnd.s_MyHonor = 0

function CLuaDouDiZhu2020RankWnd:Init()

    local myNode = self.transform:Find("MainPlayerInfo")
    self.m_MyRankLabel=myNode:Find("RankLabel"):GetComponent(typeof(UILabel))
    self.m_MyNameLabel=myNode:Find("NameLabel"):GetComponent(typeof(UILabel))
    self.m_MyScoreLabel=myNode:Find("ScoreLabel"):GetComponent(typeof(UILabel))
    self.m_MyClsSprite = myNode:Find("ClsSprite"):GetComponent(typeof(UISprite))

    if CClientMainPlayer.Inst then
        self.m_MyNameLabel.text= CClientMainPlayer.Inst.Name
    end
    self.m_MyRankLabel.text=LocalString.GetString("未上榜")
    -- self.m_MyTimeLabel.text=LocalString.GetString("—")
    -- if CClientMainPlayer.Inst:IsInGuild() then
    --     self.m_MyTimeLabel.text=CGuildMgr.Inst.m_GuildName
    -- end
    self.m_MyScoreLabel.text=LocalString.GetString("—")

    self.m_TableView=FindChild(self.transform,"TableView"):GetComponent(typeof(QnTableView))
    local function initItem(item,index)
        if index % 2 == 1 then
            item:SetBackgroundTexture("common_bg_mission_background_n")
        else
            item:SetBackgroundTexture("common_bg_mission_background_s")
        end
        self:InitItem(item,index)
    end
    

    self.m_TableViewDataSource=DefaultTableViewDataSource.CreateByCount(0,initItem)
    self.m_TableView.m_DataSource=self.m_TableViewDataSource
    self.m_TableView.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)
    -- self:OnRankDataReady()
    Gac2Gas.QueryRank(41000201)
    
end

function CLuaDouDiZhu2020RankWnd:OnSelectAtRow(row)
    local data=self.m_RankList[row]
    if data then
        CPlayerInfoMgr.ShowPlayerPopupMenu(data.PlayerIndex, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,AlignType.Default)
    end
end
function CLuaDouDiZhu2020RankWnd:InitItem(item,index)
    local tf=item.transform
    local nameLabel=FindChild(tf,"NameLabel"):GetComponent(typeof(UILabel))
    nameLabel.text=" "
    local rankLabel=FindChild(tf,"RankLabel"):GetComponent(typeof(UILabel))
    rankLabel.text=""
    local rankSprite=FindChild(tf,"RankImage"):GetComponent(typeof(UISprite))
    rankSprite.spriteName=""
    local scoreLabel=FindChild(tf,"ScoreLabel"):GetComponent(typeof(UILabel))
    scoreLabel.text=""
    
    local info=self.m_RankList[index]
    local rank=info.Rank
    if rank==1 then
        rankSprite.spriteName="Rank_No.1"
    elseif rank==2 then
        rankSprite.spriteName="Rank_No.2png"
    elseif rank==3 then
        rankSprite.spriteName="Rank_No.3png"
    else
        rankLabel.text=tostring(rank)
    end
    local clsSprite = tf:Find("ClsSprite"):GetComponent(typeof(UISprite))
    clsSprite.spriteName = Profession.GetIconByNumber(EnumToInt(info.Job))

    nameLabel.text=info.Name
    scoreLabel.text=tostring(info.Value)
end
function CLuaDouDiZhu2020RankWnd:OnEnable()
    g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
    
end
function CLuaDouDiZhu2020RankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")

end
function CLuaDouDiZhu2020RankWnd:OnRankDataReady()
    local myInfo = CRankData.Inst.MainPlayerRankInfo
    if myInfo.Rank>0 then
        self.m_MyRankLabel.text=tostring(myInfo.Rank)
    else
        self.m_MyRankLabel.text=LocalString.GetString("未上榜")
    end

    self.m_MyNameLabel.text = ""
    if CClientMainPlayer.Inst then
        self.m_MyNameLabel.text= CClientMainPlayer.Inst.Name
        self.m_MyClsSprite.spriteName = Profession.GetIconByNumber(EnumToInt(CClientMainPlayer.Inst.Class))
    end
    if myInfo.Value>0 then
        self.m_MyScoreLabel.text = myInfo.Value
    else
        self.m_MyScoreLabel.text = "—"
    end


    self.m_RankList=CRankData.Inst.RankList
    self.m_TableViewDataSource.count=self.m_RankList.Count
    self.m_TableView:ReloadData(true,false)
end
