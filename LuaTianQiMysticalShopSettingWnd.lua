local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnTableView = import "L10.UI.QnTableView"
local QnCheckBox = import "L10.UI.QnCheckBox"
local GameSetting_Common_Wapper = import "L10.Game.GameSetting_Common_Wapper"
local Object = import "System.Object"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItem = import "L10.Game.CItem"

LuaTianQiMysticalShopSettingWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaTianQiMysticalShopSettingWnd, "QnTableView", "QnTableView", QnTableView)

--@endregion RegistChildComponent end
RegistClassMember(LuaTianQiMysticalShopSettingWnd, "m_List")
RegistClassMember(LuaTianQiMysticalShopSettingWnd, "m_SkipConfirmDict")

function LuaTianQiMysticalShopSettingWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaTianQiMysticalShopSettingWnd:Init()
    self.m_List = {}
    self.m_SkipConfirmDict = {}
    if not CClientMainPlayer.Inst then CUIManager.CloseUI(CLuaUIResources.TianQiMysticalShopSettingWnd) end
    local entryItemSet = {}
    TianQiMysticalShop_EntryItem.Foreach(function (key, data)
        for itemId in string.gmatch(data.ItemIdList,"(%d+)") do
            entryItemSet[tonumber(itemId)] = true
        end
    end)
    local specialItemConfirmKeys = {}
    TianQiMysticalShop_SaledItem.Foreach(function (key, data)
        if entryItemSet[data.ItemId] then
            specialItemConfirmKeys[data.SpecialItemConfirmID] = true
        end
    end)
    for key,_ in pairs(specialItemConfirmKeys) do
        local data = TianQiMysticalShop_SpecialItemConfirm.GetData(key)
        if data then
            local itemData = Item_Item.GetData(data.ShowItemId)
            table.insert(self.m_List, {ID = key, itemData = itemData})
            self.m_SkipConfirmDict[key] = CClientMainPlayer.Inst.PlayProp.TianQiSpecialItemSkipConfirm:GetBit(key)
        end
    end
    table.sort(self.m_List,function (a,b)
        local qualityA = EnumToInt(CItem.GetQualityType(a.itemData.ID)) 
        local qualityB = EnumToInt(CItem.GetQualityType(b.itemData.ID)) 
        if qualityA == qualityB then return a.ID < b.ID end
        return qualityA > qualityB
    end)
    self.QnTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return #self.m_List
        end,
        function(item, index)
            self:ItemAt(item,index)
        end)
    self.QnTableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)
    self.QnTableView:ReloadData(true, true)
end

--@region UIEvent

--@endregion UIEvent

function LuaTianQiMysticalShopSettingWnd:ItemAt(item,index)
    local data = self.m_List[index + 1]
    local id = data.ID
    local itemData = data.itemData
    local tex = item.transform:Find("Texture"):GetComponent(typeof(CUITexture))
    local border = item.transform:Find("Texture/Border"):GetComponent(typeof(UISprite))
    local label = item.transform:Find("Label"):GetComponent(typeof(UILabel))
    local checkBox = item.transform:Find("QnCheckBox"):GetComponent(typeof(QnCheckBox))
    tex:LoadMaterial(itemData.Icon)
    border.spriteName = CUICommonDef.GetItemCellBorder(itemData.NameColor)
    label.text = itemData.Name
    label.color = GameSetting_Common_Wapper.Inst:GetColor(itemData.NameColor)
    checkBox:SetSelected(not self.m_SkipConfirmDict[id],false)
    UIEventListener.Get(tex.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemData.ID, false, nil, AlignType.Default, 0, 0, 0, 0)
    end)
end

function LuaTianQiMysticalShopSettingWnd:OnSelectAtRow(row)
    local item = self.QnTableView:GetItemAtRow(row)
    local checkBox = item.transform:Find("QnCheckBox"):GetComponent(typeof(QnCheckBox))
    local data = self.m_List[row + 1]
    local id = data.ID
    self.m_SkipConfirmDict[id] = not self.m_SkipConfirmDict[id]
    checkBox:SetSelected(not self.m_SkipConfirmDict[id], false)
end

function LuaTianQiMysticalShopSettingWnd:SetTianQiSpecialItemSkipConfirm()
    if not CClientMainPlayer.Inst then return end
    local list = {}
    for id, isSelect in pairs(self.m_SkipConfirmDict) do
        if isSelect ~= CClientMainPlayer.Inst.PlayProp.TianQiSpecialItemSkipConfirm:GetBit(id) then
            table.insert(list,{id = id, isSelect = isSelect})    
        end
    end
    for i = 1,#list,20 do
        local dict = CreateFromClass(MakeGenericClass(Dictionary, String, Object))
        for j = i,i + 19 do
            if j <= #list then
                local id, isSelect = list[j].id, list[j].isSelect
                CommonDefs.DictAdd(dict, typeof(String), tostring(id), typeof(Object), tostring(isSelect and 1 or 0))
            end
        end
        Gac2Gas.SetTianQiSpecialItemSkipConfirm(MsgPackImpl.pack(dict))
    end
end

function LuaTianQiMysticalShopSettingWnd:OnDisable()
    self:SetTianQiSpecialItemSkipConfirm()
end
