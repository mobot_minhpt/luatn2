g_UIFxPaths = {
    TaskInProgressFxPath = "Fx/UI/Prefab/UI_renwujinxingzhong.prefab",
    RuMengJiBgFxPath = "Fx/UI/Prefab/UI_rumengji_qian.prefab",
    RuMengJiUpFxPath = "Fx/UI/Prefab/UI_rumengji_qian_hua.prefab",
    RuMengJiDownFxPath = "Fx/UI/Prefab/UI_rumengji_hou.prefab",
    RuMengJiJieSuoFx = "Fx/UI/Prefab/UI_rumengji_jiesuo.prefab",
    RuMengJiDianJiFx = "Fx/UI/Prefab/UI_dianji02.prefab",
    RuMengJiGuangQuanFx = "Fx/UI/Prefab/UI_rumengji_guangquan.prefab",
    QuShuiLiuShangLiuShuiFx1Path = "Fx/UI/Prefab/UI_qushuiliushang_liushui1.prefab",
    QuShuiLiuShangLiuShuiFx2Path = "Fx/UI/Prefab/UI_qushuiliushang_liushui2.prefab",
    QuShuiLiuShangHuaBanPath = "Fx/UI/Prefab/UI_qushuiliushang_huaban.prefab",
    QuShuiLiuShangHuaZhiFxPath = "Fx/UI/Prefab/UI_qushuiliushang_huazhi.prefab",
    XinShengHuaJiBrushFx = "Fx/UI/Prefab/UI_xinshenghuaji_huabituowei.prefab",
    XinShengHuaJiMoYunFx = "Fx/UI/Prefab/UI_xinshenghuaji_moyun.prefab",
    WeddingPromiseLightFx = "Fx/UI/Prefab/UI_haidiaotujian_guangshu.prefab",
    WeddingPlayStartFxPath = "Fx/UI/Prefab/UI_zhenjiaxinniang_youxikaishi.prefab",
    WeddingPlayEndFxPath = "Fx/UI/Prefab/UI_zhenjiaxinniang_youxijieshu.prefab",
    WeddingStageGoingFxPath = "Fx/UI/Prefab/UI_zhenjiaxinniang_huaduo.prefab",
    WeddingDaoJiShiFxPath = "Fx/UI/Prefab/UI_HunLi_Daojishi.prefab",
    WeddingQingJianFxPath = "Fx/UI/Prefab/UI_HunLi_QingJian.prefab",
    XinShengHuaJiUnlockFxPath = "Fx/UI/Prefab/UI_dianlianggezi_01.prefab",
    WeddingHelpAnswerFxPath = "Fx/UI/Prefab/UI_zhenjiaxinniang_tishi.prefab",
    QingMingPVEFxPath = "Fx/UI/Prefab/UI_qingmingPVE.prefab",
    HuaYiGongShangFxPath = "Fx/UI/Prefab/UI_huayi_jiemian.prefab",
    DouDiZhuBaoXiangDouDongFxPath = "Fx/UI/Prefab/UI_doudizhu_ddong.prefab",
    DouDiZhuBaoXiangDaKaiFxPath = "Fx/UI/Prefab/UI_doudizhu_kxiang.prefab",
    ShanYanJieYuanSaoGuangFxPath = "Fx/UI/Prefab/UI_jiesuoyaoban_anniuliuguang.prefab",
    ShanYanJieYuanJuJiFxPath = "Fx/UI/Prefab/UI_huanranyixin.prefab",
    GuanYaoJianDianLiangFxPath = "Fx/UI/Prefab/UI_liuxingyu_touxiangdianliang.prefab",
    JinDuTiaoFxPath = "Fx/UI/Prefab/UI_liuxingyu_jindutiao01.prefab",
    XinBaiProgressFxPath = "Fx/UI/Prefab/UI_shijian_shanguang.prefab",
    QianYingChuWenAChuFxPath = "Fx/UI/Prefab/UI_qianyingchuwen_achu.prefab",
 }