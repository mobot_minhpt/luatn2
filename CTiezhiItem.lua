-- Auto Generated!!
local BoolDelegate = import "UIEventListener+BoolDelegate"
local Boolean = import "System.Boolean"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CTiezhiItem = import "L10.UI.CTiezhiItem"
local CUITexture = import "L10.UI.CUITexture"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local PaiZhao_QiPao = import "L10.Game.PaiZhao_QiPao"
local PaiZhao_Setting = import "L10.Game.PaiZhao_Setting"
local PaiZhao_TieZhi = import "L10.Game.PaiZhao_TieZhi"
local Single = import "System.Single"
local UIBasicSprite = import "UIBasicSprite"
local UIEventListener = import "UIEventListener"
local UIInput = import "UIInput"
local UIPanel = import "UIPanel"
local UITexture = import "UITexture"
local Vector3 = import "UnityEngine.Vector3"
local VectorDelegate = import "UIEventListener+VectorDelegate"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CTiezhiItem.m_Awake_CS2LuaHook = function (this) 
    UIEventListener.Get(this.EditButton).onDrag = MakeDelegateFromCSFunction(this.OnEditDrag, VectorDelegate, this)
    UIEventListener.Get(this.texture.gameObject).onDrag = MakeDelegateFromCSFunction(this.OnItemDrag, VectorDelegate, this)
    UIEventListener.Get(this.texture.gameObject).onPress = MakeDelegateFromCSFunction(this.OnItemPress, BoolDelegate, this)
    UIEventListener.Get(this.ConfirmButton).onClick = MakeDelegateFromCSFunction(this.onConfirmClick, VoidDelegate, this)
    UIEventListener.Get(this.CancelButton).onClick = MakeDelegateFromCSFunction(this.onCancelCilck, VoidDelegate, this)
    UIEventListener.Get(this.MirrorButton).onClick = MakeDelegateFromCSFunction(this.onMirrorClick, VoidDelegate, this)
    EventManager.AddListenerInternal(EnumEventType.OnTiezhiItemEditEnable, MakeDelegateFromCSFunction(this.onOtherTiezhiEditEnable, MakeGenericClass(Action1, Single), this))
    EventManager.AddListenerInternal(EnumEventType.OnTiezhiModifyColor, MakeDelegateFromCSFunction(this.onSetTiezhiColor, MakeGenericClass(Action1, Color), this))
    EventManager.AddListenerInternal(EnumEventType.OnTiezhiModifyColorIndex, MakeDelegateFromCSFunction(this.onSetTiezhiColorIndex, MakeGenericClass(Action1, Single), this))
    this:setEditMode(true)
end
CTiezhiItem.m_SetTypeAndID_CS2LuaHook = function (this, type, id) 
    this.cTexture = CommonDefs.GetComponent_Component_Type(this.texture, typeof(CUITexture))
    this.showType = type
    this.tiezhiID = id
    repeat
        local default = type
        if default == 0 then
            local tiezhi = PaiZhao_TieZhi.GetData(id)
            this.cTexture:LoadMaterial(tiezhi.Texture, false, MakeDelegateFromCSFunction(this.loadMaterialCb, MakeGenericClass(Action1, Boolean), this))
            break
        elseif default == 2 then
            local qipao = PaiZhao_QiPao.GetData(id)
            this.cTexture:LoadMaterial(qipao.Texture, false, MakeDelegateFromCSFunction(this.loadMaterialCb, MakeGenericClass(Action1, Boolean), this))
            this.label.transform.localPosition = Vector3(qipao.Offset[0], qipao.Offset[1])
            local input = CommonDefs.GetComponent_Component_Type(this.label, typeof(UIInput))
            input.characterLimit = qipao.CharacterLimit
            break
        end
    until 1
end
CTiezhiItem.m_onMirrorClick_CS2LuaHook = function (this, go) 
    if this.texture.flip == UIBasicSprite.Flip.Nothing then
        this.texture.flip = UIBasicSprite.Flip.Horizontally
    elseif this.texture.flip == UIBasicSprite.Flip.Horizontally then
        this.texture.flip = UIBasicSprite.Flip.Nothing
    end
end
CTiezhiItem.m_setEditMode_CS2LuaHook = function (this, flag) 
    this.editing = flag
    this.ConfirmButton:SetActive(this.editing)
    this.EditButton:SetActive(this.editing)
    this.CancelButton:SetActive(this.editing)
    this.MirrorButton:SetActive(this.editing)
    if this.editing then
        EventManager.BroadcastInternalForLua(EnumEventType.OnTiezhiItemEditEnable, {this.gameObject:GetInstanceID()})
    end
end
CTiezhiItem.m_judgeSize_CS2LuaHook = function (this, pos) 
    local sizeRatio = this.oriTextureHeight / this.oriTextureWidth
    local pow = pos.x * pos.x + pos.y * pos.y
    local x = math.sqrt(pow / (1 + sizeRatio * sizeRatio))
    return x * 2 / this.oriTextureWidth
end
CTiezhiItem.m_onOtherTiezhiEditEnable_CS2LuaHook = function (this, instanceid) 
    if instanceid == this.gameObject:GetInstanceID() then
        return
    else
        this:setEditMode(false)
    end
end
CTiezhiItem.m_onSetTiezhiColorIndex_CS2LuaHook = function (this, index) 

    local data = PaiZhao_Setting.GetData().Hulu_color
    local c = data[math.floor(index)]
    this:onSetTiezhiColor(Color(c[0] / 255, c[1] / 255, c[2] / 255))
end
CTiezhiItem.m_onSetTiezhiColor_CS2LuaHook = function (this, color) 
    if not this.editing then
        return
    end

    local mat = this.cTexture.material
    mat:SetColor("_Color", color)
    local tex = CommonDefs.GetComponent_Component_Type(this.cTexture, typeof(UITexture))
    CommonDefs.GetComponent_Component_Type(this.transform.parent, typeof(UIPanel)):RebuildAllDrawCalls()
end
CTiezhiItem.m_angle_CS2LuaHook = function (this, point1, point2) 
    local v = CommonDefs.op_Subtraction_Vector3_Vector3(point2, point1)
    local z = Vector3.Angle(Vector3(1, 0, 0), v)
    if v.y < 0 then
        return 360 - z
    else
        return z
    end
end
