local UIProgressBar = import "UIProgressBar"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"


LuaYuanXiaoDefenseResultWnd = class()
LuaYuanXiaoDefenseResultWnd.s_TeamScore = 0
LuaYuanXiaoDefenseResultWnd.s_PlayerScore = {}
LuaYuanXiaoDefenseResultWnd.s_RewardItems = {}
LuaYuanXiaoDefenseResultWnd.s_RemainRewardTimes = {}


RegistClassMember(LuaYuanXiaoDefenseResultWnd, "Sect1")
RegistClassMember(LuaYuanXiaoDefenseResultWnd, "Sect2")
RegistClassMember(LuaYuanXiaoDefenseResultWnd, "Sect3")
RegistClassMember(LuaYuanXiaoDefenseResultWnd, "Sect4")
RegistClassMember(LuaYuanXiaoDefenseResultWnd, "Sect5")
RegistClassMember(LuaYuanXiaoDefenseResultWnd, "ItemGrid")
RegistClassMember(LuaYuanXiaoDefenseResultWnd, "PlayerGrid")
RegistClassMember(LuaYuanXiaoDefenseResultWnd, "ItemTemplate")
RegistClassMember(LuaYuanXiaoDefenseResultWnd, "PlayerTemplate")
RegistClassMember(LuaYuanXiaoDefenseResultWnd, "HintLabel")


function LuaYuanXiaoDefenseResultWnd:Awake()
    local title = self.transform:Find("Anchor/Title")
    for i = 0, 2 do
        if i < title.childCount then
            title:GetChild(i).gameObject:SetActive(false)
        end
    end
    self.transform:Find("Anchor/InfoView/Left/ScoreRing/TotalScore"):GetComponent(typeof(UILabel)).text = LuaYuanXiaoDefenseResultWnd.s_TeamScore

    self.ItemGrid = self.transform:Find("Anchor/InfoView/Right/Rewards/Grid"):GetComponent(typeof(UIGrid))
    self.PlayerGrid = self.transform:Find("Anchor/InfoView/Left/Grid"):GetComponent(typeof(UIGrid))
    self.ItemTemplate = self.transform:Find("Anchor/InfoView/Right/Rewards/ItemTemplate").gameObject
    self.PlayerTemplate = self.transform:Find("Anchor/InfoView/Left/PlayerTemplate").gameObject
    self.HintLabel = self.transform:Find("Anchor/InfoView/Right/Rewards/HintLabel"):GetComponent(typeof(UILabel))
    self.HintLabel.gameObject:SetActive(false)

    self.ItemTemplate:SetActive(false)
    self.PlayerTemplate:SetActive(false)

    for i = 1, 5 do
        self["Sect"..i] = self.transform:Find("Anchor/InfoView/Left/ScoreRing/Sect"..i):GetComponent(typeof(UIProgressBar))
        self["Sect"..i].gameObject:SetActive(false)
    end

    UIEventListener.Get(self.transform:Find("Anchor/InfoView/Right/RankButton").gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            CUIManager.ShowUI(CLuaUIResources.YuanXiaoDefenseRankWnd)
        end
    )

    UIEventListener.Get(self.transform:Find("Anchor/InfoView/Right/FightAgainButton").gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            CUIManager.ShowUI(CLuaUIResources.YuanXiaoDefenseSignUpWnd)
            CUIManager.CloseUI(CLuaUIResources.YuanXiaoDefenseResultWnd)
        end
    )
end

function LuaYuanXiaoDefenseResultWnd:OnEnable()
    g_ScriptEvent:AddListener("MainPlayerCreated", self, "OnMainPlayerCreated")
end

function LuaYuanXiaoDefenseResultWnd:OnDisable()
    g_ScriptEvent:RemoveListener("MainPlayerCreated", self, "OnMainPlayerCreated")
end

function LuaYuanXiaoDefenseResultWnd:OnMainPlayerCreated()
    if not LuaYuanXiaoDefenseResultWnd.s_PlayerScore then return end
    for i = 1, #LuaYuanXiaoDefenseResultWnd.s_PlayerScore do
        if LuaYuanXiaoDefenseResultWnd.s_PlayerScore[i].name == CClientMainPlayer.Inst.Name then
            local rankLabel = self.transform:Find("Anchor/InfoView/Left/ScoreRing/TotalScore/Label"):GetComponent(typeof(UILabel))
            rankLabel.fontSize = 52
            rankLabel.text = LocalString.GetString("第"..i.."名")
            break
        end
    end
end

function LuaYuanXiaoDefenseResultWnd:Init()
    --[[Extensions.RemoveAllChildren(self.PlayerGrid.transform)
    local prefixSum = 0
    for i = 1, #LuaYuanXiaoDefenseResultWnd.s_PlayerScore do
        prefixSum = prefixSum + LuaYuanXiaoDefenseResultWnd.s_PlayerScore[i].percent
        self["Sect"..i].value = prefixSum
        self["Sect"..i].gameObject:SetActive(true)

        local obj = NGUITools.AddChild(self.PlayerGrid.gameObject, self.PlayerTemplate)
        obj:SetActive(true)
        obj.transform:Find("Color"):GetComponent(typeof(UIWidget)).color = self["Sect"..i].transform:GetComponent(typeof(UITexture)).color
        local nameLabel = obj.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
        nameLabel.text = LuaYuanXiaoDefenseResultWnd.s_PlayerScore[i].name
        local scoreLabel = obj.transform:Find("ScoreLabel"):GetComponent(typeof(UILabel))
        scoreLabel.text = LuaYuanXiaoDefenseResultWnd.s_PlayerScore[i].score
        if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Name == LuaYuanXiaoDefenseResultWnd.s_PlayerScore[i].name then
            nameLabel.color = Color.green
            scoreLabel.color = Color.green
        end
    end
    self.PlayerGrid:Reposition()--]]
    if CClientMainPlayer.Inst then
        for i = 1, #LuaYuanXiaoDefenseResultWnd.s_PlayerScore do
            if LuaYuanXiaoDefenseResultWnd.s_PlayerScore[i].name == CClientMainPlayer.Inst.Name then
                local rankLabel = self.transform:Find("Anchor/InfoView/Left/ScoreRing/TotalScore/Label"):GetComponent(typeof(UILabel))
                rankLabel.fontSize = 52
                rankLabel.text = LocalString.GetString("第"..i.."名")
                break
            end
        end
    end

    Extensions.RemoveAllChildren(self.ItemGrid.transform)
    for i = 1, #LuaYuanXiaoDefenseResultWnd.s_RewardItems do
        local obj = NGUITools.AddChild(self.ItemGrid.gameObject, self.ItemTemplate)
        obj:SetActive(true)
        self:InitOneItem(obj, LuaYuanXiaoDefenseResultWnd.s_RewardItems[i])
    end
    self.ItemGrid:Reposition()

    if #LuaYuanXiaoDefenseResultWnd.s_RewardItems == 0 then
        if LuaYuanXiaoDefenseResultWnd.s_RemainRewardTimes[1] > 0 and LuaYuanXiaoDefenseResultWnd.s_RemainRewardTimes[2] > 0 then -- 排名奖没领的提示
            self.HintLabel.gameObject:SetActive(true)
            self.HintLabel.text = g_MessageMgr:FormatMessage("YuanXiao2023_NaoYuanXiao_NoRewardTimes")
        elseif LuaYuanXiaoDefenseResultWnd.s_RemainRewardTimes[3] > 0 then -- 挑战奖未领的提示
            self.HintLabel.gameObject:SetActive(true)
            self.HintLabel.text = g_MessageMgr:FormatMessage("YuanXiao2023_NaoYuanXiao_GetNextReward")
        end
    end
end

function LuaYuanXiaoDefenseResultWnd:InitOneItem(curItem, itemId)
    local texture = curItem.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local ItemData = Item_Item.GetData(itemId)

    if ItemData then 
        texture:LoadMaterial(ItemData.Icon) 
    end

    CommonDefs.AddOnClickListener(
        curItem,
        DelegateFactory.Action_GameObject(
            function(go)
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
            end
        ),
        false
    )
end
