-- Auto Generated!!
local CSnowBallMgr = import "L10.Game.CSnowBallMgr"
local CSnowBallOperateWnd = import "L10.UI.CSnowBallOperateWnd"
local CSocialWndMgr = import "L10.UI.CSocialWndMgr"
local CThumbnailChatWnd = import "L10.UI.CThumbnailChatWnd"
local CUITexture = import "L10.UI.CUITexture"
local DelegateFactory = import "DelegateFactory"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UIRoot = import "UIRoot"
local UIWidget = import "UIWidget"
local XueQiuDaZhan_ExtraSkill = import "L10.Game.XueQiuDaZhan_ExtraSkill"
CSnowBallOperateWnd.m_Init_CS2LuaHook = function (this)
    this.detailNode:SetActive(false)
    this.templateNode:SetActive(false)
    local chatBtn = this.transform:Find("Anchor/Skill/chatBtn").gameObject

    if CSnowBallMgr.Inst:GetSnowBallState() == 2 then
        UIEventListener.Get(chatBtn).onClick = DelegateFactory.VoidDelegate(function (p)
            CSocialWndMgr.ShowChatWnd(EChatPanel.Team)
        end)
    else
        UIEventListener.Get(chatBtn).onClick = DelegateFactory.VoidDelegate(function (p)
            CSocialWndMgr.ShowChatWnd()
        end)
    end
end
CSnowBallOperateWnd.m_OnEnable_CS2LuaHook = function (this)
    this:Init()
    this:IPhoneXAdaptation()
    EventManager.AddListener(EnumEventType.SnowBallUpdateSkillInfo, MakeDelegateFromCSFunction(this.UpdateSkillInfo, Action0, this))
end
CSnowBallOperateWnd.m_IPhoneXAdaptation_CS2LuaHook = function (this)
    if UIRoot.EnableIPhoneXAdaptation then
        if CommonDefs.IsAndroidPlatform() then return end
        local w = CommonDefs.GetComponent_Component_Type(this.transform:Find("Anchor"), typeof(UIWidget))
        w.bottomAnchor.absolute = 59 + CThumbnailChatWnd.GetHeightOffsetForIphoneX()
        w.topAnchor.absolute = 87 + CThumbnailChatWnd.GetHeightOffsetForIphoneX()
        --w:ResetAndUpdateAnchors()
    end
end
CSnowBallOperateWnd.m_SetDetailInfo_CS2LuaHook = function (this, skill, pos, skillId)
    local detailText = CommonDefs.GetComponent_Component_Type(this.detailNode.transform:Find("Label"), typeof(UILabel))
    local removeBtn = this.detailNode.transform:Find("removeBtn").gameObject
    local learnBtn = this.detailNode.transform:Find("learnBtn").gameObject

    detailText.text = skill.Display

    UIEventListener.Get(removeBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        Gac2Gas.RequestRemoveXueQiuSkillItem(pos, skillId, false)
    end)
    UIEventListener.Get(learnBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        Gac2Gas.RequestRemoveXueQiuSkillItem(pos, skillId, true)
    end)
end
CSnowBallOperateWnd.m_ClearShowInfo_CS2LuaHook = function (this)
    this.detailNode:SetActive(false)
    if this.saveSignNode ~= nil then
        this.saveSignNode:SetActive(false)
        this.saveSignNode = nil
    end
    do
        local i = 0
        while i < this.skillNodeArray.Length do
            this.skillNodeArray[i]:SetActive(true)
            i = i + 1
        end
    end
end
CSnowBallOperateWnd.m_OpenDetailPanel_CS2LuaHook = function (this, sign, skill, pos, skillId)
    if sign.activeSelf then
        sign:SetActive(false)
        this.saveSignNode = nil
        this.detailNode:SetActive(false)
    else
        if this.saveSignNode ~= nil then
            this.saveSignNode:SetActive(false)
        end

        this.saveSignNode = sign
        sign:SetActive(true)
        this.detailNode:SetActive(true)
        this:SetDetailInfo(skill, pos, skillId)
    end
end
CSnowBallOperateWnd.m_SetNodeInfo_CS2LuaHook = function (this, go, skillId, pos)
    local iconTexture = CommonDefs.GetComponent_Component_Type(go.transform:Find("SkillIcon"), typeof(CUITexture))
    local skillLabel = CommonDefs.GetComponent_Component_Type(go.transform:Find("name"), typeof(UILabel))
    local selectSprite = go.transform:Find("SelectedSprite").gameObject

    iconTexture.mainTexture = nil
    iconTexture.material = nil
    skillLabel.text = ""

    if skillId > 0 then
        local eskill = XueQiuDaZhan_ExtraSkill.GetData(skillId)
        if eskill ~= nil then
            local skill = Skill_AllSkills.GetData(eskill.SkillID)
            if skill ~= nil then
                iconTexture:LoadSkillIcon(skill.SkillIcon)
                skillLabel.text = skill.Name
            end
            --go.GetComponent<BoxCollider>().enabled = true;
            UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function (p)
                this:OpenDetailPanel(selectSprite, skill, pos, skillId)
            end)
        end
    else
        UIEventListener.Get(go).onClick = nil
        --go.GetComponent<BoxCollider>().enabled = false;
    end
end
CSnowBallOperateWnd.m_UpdateSkillInfo_CS2LuaHook = function (this)
    this:ClearShowInfo()

    if CSnowBallMgr.Inst.skill_list.Count ~= this.skillNodeArray.Length then
        return
    end

    this:UpdateSkillMove()

    do
        local i = 0
        while i < this.skillNodeArray.Length do
            local skillId = CSnowBallMgr.Inst.skill_list[i]
            this:SetNodeInfo(this.skillNodeArray[i], skillId, i + 1)
            i = i + 1
        end
    end
end
