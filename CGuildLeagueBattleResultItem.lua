-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CGuildLeagueBattleResultItem = import "L10.UI.CGuildLeagueBattleResultItem"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CUICommonDef = import "L10.UI.CUICommonDef"
local EnumClass = import "L10.Game.EnumClass"
local Profession = import "L10.Game.Profession"
CGuildLeagueBattleResultItem.m_Init_CS2LuaHook = function (this, data, rowIndex, isMain) 
    if rowIndex % 2 == 0 then
        this.bgSprite.alpha = 0
    else
        this.bgSprite.alpha = 40 / 255.0
    end

    this.nameLabel.text = data.playerName
    --   levelLabel.text ="Lv."+ data.level.ToString();
    this.ClazzSprite.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), data.clazz))
    this.killNumLabel.text = tostring(data.killCount)
    this.killedNumLabel.text = tostring(data.deadCount)
    this.dpsMumLabel.text = CUICommonDef.ConvertNumberOverMillion(data.dpsNum)
    this.healNumLabel.text = CUICommonDef.ConvertNumberOverMillion(data.healNum)
    this.underDamageLabel.text = CUICommonDef.ConvertNumberOverMillion(data.underDamage)

    this.controlLabel.text = tostring(data.controlCount)
    this.baoshiLabel.text = tostring(data.baoshiCount)
    this.rebornLabel.text = tostring(data.fuhuoCount)
    this.rmControlLabel.text = tostring(data.rmControlCount)

    if isMain then
        this.repairNumLabel.text = ""
    else
        this.repairNumLabel.text = tostring(data.repairCount)
    end

    -- 是本人
    if data.playerName == CClientMainPlayer.Inst.Name then
        this.nameLabel.color = Color.green
        --    levelLabel.color = Color.green;
        this.killNumLabel.color = Color.green
        this.killedNumLabel.color = Color.green
        this.repairNumLabel.color = Color.green
        this.dpsMumLabel.color = Color.green
        this.healNumLabel.color = Color.green
        this.underDamageLabel.color = Color.green

        this.controlLabel.color = Color.green
        this.baoshiLabel.color = Color.green
        this.rebornLabel.color = Color.green

        this.rmControlLabel.color = Color.green
    else
        this.nameLabel.color = Color.white
        --       levelLabel.color = Color.white;
        this.killNumLabel.color = Color.white
        this.killedNumLabel.color = Color.white
        this.repairNumLabel.color = Color.white
        this.dpsMumLabel.color = Color.white
        this.healNumLabel.color = Color.white
        this.underDamageLabel.color = Color.white

        this.controlLabel.color = Color.white
        this.baoshiLabel.color = Color.white
        this.rebornLabel.color = Color.white

        this.rmControlLabel.color = Color.white
    end
end
