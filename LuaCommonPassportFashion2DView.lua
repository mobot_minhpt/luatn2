local EnumGender = import "L10.Game.EnumGender"
local CFashionPreviewMgr = import "L10.UI.CFashionPreviewMgr"
local EnumPreviewType = import "L10.UI.EnumPreviewType"
local CCPlayerCtrl = import "L10.Game.CCPlayerCtrl"
local CWinCGMgr = import "L10.Game.CWinCGMgr"

LuaCommonPassportFashion2DView = class()

RegistClassMember(LuaCommonPassportFashion2DView, "curPreviewIdx")
RegistClassMember(LuaCommonPassportFashion2DView, "lastPreviewType")
RegistClassMember(LuaCommonPassportFashion2DView, "previewTick")

RegistClassMember(LuaCommonPassportFashion2DView, "m_Root")
RegistClassMember(LuaCommonPassportFashion2DView, "m_Title")
RegistClassMember(LuaCommonPassportFashion2DView, "m_NameLabel")
RegistClassMember(LuaCommonPassportFashion2DView, "m_Table")
RegistClassMember(LuaCommonPassportFashion2DView, "m_PlayBtn")

RegistClassMember(LuaCommonPassportFashion2DView, "m_Angles")
RegistClassMember(LuaCommonPassportFashion2DView, "m_DeltaDegree")
RegistClassMember(LuaCommonPassportFashion2DView, "m_Ids")
RegistClassMember(LuaCommonPassportFashion2DView, "m_Names")
RegistClassMember(LuaCommonPassportFashion2DView, "m_Urls")
RegistClassMember(LuaCommonPassportFashion2DView, "m_CgName")
RegistClassMember(LuaCommonPassportFashion2DView, "m_RootPos")

function LuaCommonPassportFashion2DView:Awake()
    self.m_Root = self.transform:Find("Root")
    self.m_Title = self.transform:Find("Title"):GetComponent(typeof(UILabel))
    self.m_PlayBtn = self.transform:Find("PlayBtn").gameObject
    self.m_Angles = {}

    if not LuaCommonPassportMgr.m_LastPreviewType then
        LuaCommonPassportMgr.m_LastPreviewType = CFashionPreviewMgr.curPreviewType
        self.lastPreviewType = true
    end

    self.curPreviewIdx = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Gender == EnumGender.Male and 2 or 1
end

function LuaCommonPassportFashion2DView:OnEnable()
    self:InitPreview()
end

function LuaCommonPassportFashion2DView:OnDisable()
    if self.lastPreviewType then
        CFashionPreviewMgr.curPreviewType = LuaCommonPassportMgr.m_LastPreviewType
        LuaCommonPassportMgr.m_LastPreviewType = nil
    end
    UnRegisterTick(self.previewTick)
    self.previewTick = nil
end

function LuaCommonPassportFashion2DView:InitPreview()
    for i = 1, 7 do
        local go = self.m_Root:GetChild(i - 1).gameObject
        UIEventListener.Get(go).onDrag =
            DelegateFactory.VectorDelegate(
            function(g, delta)
                self:OnDrag(g, delta)
            end
        )
        UIEventListener.Get(go).onDragEnd =
            DelegateFactory.VoidDelegate(
            function(g)
                self:OnDragEnd(g)
            end
        )
        UIEventListener.Get(go).onDragStart =
            DelegateFactory.VoidDelegate(
            function(g)
                self:OnDragStart(g)
            end
        )
    end
    local name = self.transform:Find("Name")
    self.m_Table = name:Find("Table"):GetComponent(typeof(UITable))
    local Template = name:Find("SpriteTemplate").gameObject
    Template:SetActive(false)
    self.m_NameLabel = name:Find("Label"):GetComponent(typeof(UILabel))
    Extensions.RemoveAllChildren(self.m_Table.transform)

    local cfg = Pass_ClientSetting.GetData(LuaCommonPassportMgr.passId)

    local previewLs = cfg.PassPreviewList
    local previewZswRootPos = cfg.PassPreviewZswRootPos
    self.m_Ids, self.m_Names, self.m_Urls, self.m_RootPos = {}, {}, {}, {}

    local j = 0
    for i = 0, previewLs.Length - 1 do
        local id = previewLs[i]
        table.insert(self.m_Ids, id)
        if id < 100000 then
            table.insert(self.m_Names, Zhuangshiwu_Zhuangshiwu.GetData(id).Name)
            table.insert(self.m_Urls, "")
            self.m_RootPos[i+1] = {previewZswRootPos[j], previewZswRootPos[j + 1], previewZswRootPos[j + 2]}
            j=j+3
        else
            local fashion = Fashion_Fashion.GetData(id)
            table.insert(self.m_Names, fashion.Name)
            table.insert(self.m_Urls, fashion.PreviewVideoUrl)
        end
    end

    self.m_DeltaDegree = math.floor(360 / #self.m_Ids)
    for i = 1, #self.m_Ids do
        local go = NGUITools.AddChild(self.m_Table.gameObject, Template)
        go:SetActive(true)
        go:GetComponent(typeof(UISprite)).enabled = self.curPreviewIdx == i
        self.m_Angles[i] = (i - 1) * self.m_DeltaDegree
    end
    self.m_Table:Reposition()
    self:UpdatePosition()
    self.m_NameLabel.text = self.m_Names[self.curPreviewIdx]

    self:Refresh(true)
    self.previewTick =
        RegisterTick(
        function()
            self:NextPage()
        end,
        1000 * cfg.PassPreviewInterval
    )

    UIEventListener.Get(name:Find("NextPageBtn").gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            UnRegisterTick(self.previewTick)
            self.previewTick = nil
            self:NextPage()
        end
    )
    UIEventListener.Get(name:Find("LastPageBtn").gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            UnRegisterTick(self.previewTick)
            self.previewTick = nil
            self:LastPage()
        end
    )
    UIEventListener.Get(self.transform:Find("PreviewBtn").gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            UnRegisterTick(self.previewTick)
            self.previewTick = nil
            if self.m_Ids[self.curPreviewIdx] > 100000 then
                CFashionPreviewMgr.curPreviewType = EnumPreviewType.PreviewFashion
                CFashionPreviewMgr.ShowFashionPreview(self.m_Ids[self.curPreviewIdx])
            else
                CFashionPreviewMgr.curPreviewType = EnumPreviewType.PreviewZsw
                LuaFashionPreviewTextureLoaderMgr.PreviewZswID = self.m_Ids[self.curPreviewIdx]
                LuaFashionPreviewTextureLoaderMgr.PreviewZswRootPos = self.m_RootPos[self.curPreviewIdx]
                CUIManager.ShowUI(CUIResources.FashionPreviewWnd)
            end
        end
    )
    UIEventListener.Get(self.m_PlayBtn).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            if self.m_CgName then
                if not CCPlayerCtrl.IsSupportted() then
                    g_MessageMgr:ShowMessage("CUSTOM_STRING1", LocalString.GetString("客户端引擎版本过旧，不能播放此视频，请更新版本后再试"))
                    return
                end
                local url = CommonDefs.IS_PUB_RELEASE and "http://l10.gph.netease.com/fashionpreviewvideo/" or "http://l10-patch.leihuo.netease.com/fashionpreviewvideo/"
                CWinCGMgr.Inst.m_CGUrl = url .. self.m_CgName
                CUIManager.ShowUI(CUIResources.WinCGWnd)
            end
        end
    )
end

function LuaCommonPassportFashion2DView:Refresh(init)
    for i = 1, #self.m_Ids do
        self.m_Root:GetChild(i - 1).gameObject:SetActive(self.curPreviewIdx == i)
    end
    self.m_Title.text = self.m_Ids[self.curPreviewIdx] > 100000 and LocalString.GetString("时装外观") or LocalString.GetString("家园装饰物")
    self.m_NameLabel.text = self.m_Names[self.curPreviewIdx]
    for i = 0, self.m_Table.transform.childCount - 1 do
        self.m_Table.transform:GetChild(i):GetComponent(typeof(UISprite)).enabled = self.curPreviewIdx == i + 1
    end
    self.m_CgName = self.m_Urls[self.curPreviewIdx]
    self.m_PlayBtn:SetActive(not System.String.IsNullOrEmpty(self.m_CgName))
    self:UpdatePage(init)
end

function LuaCommonPassportFashion2DView:NextPage()
    self.curPreviewIdx = self.curPreviewIdx + 1
    if self.curPreviewIdx > #self.m_Ids then
        self.curPreviewIdx = 1
    end
    self:Refresh()
end

function LuaCommonPassportFashion2DView:LastPage()
    self.curPreviewIdx = self.curPreviewIdx - 1
    if self.curPreviewIdx < 1 then
        self.curPreviewIdx = #self.m_Ids
    end
    self:Refresh()
end

function LuaCommonPassportFashion2DView:UpdatePage(init)
    for i, v in ipairs(self.m_Angles) do
        local degree = (i - self.curPreviewIdx) * self.m_DeltaDegree
        degree = degree - math.floor(degree / 360) * 360

        local moveTo = math.sin(0.0174533 * degree) * 250
        local scale = math.cos(0.0174533 * degree) * 0.2 + 0.8
        scale = math.max(0, math.min(1, scale))

        local child = self.m_Root:GetChild(i - 1)
        LuaTweenUtils.TweenPositionX(child, moveTo, init and 0 or 0.3)
        LuaTweenUtils.TweenScaleTo(child, Vector3(scale, scale, 1), init and 0 or 0.3)
        self.m_Angles[i] = degree
    end
end

function LuaCommonPassportFashion2DView:UpdatePosition()
    local childCount = self.m_Root.childCount
    for i = 0, childCount - 1 do
        local child = self.m_Root:GetChild(i)
        local angle = self.m_Angles[i + 1]
        angle = angle - math.floor(angle / 360) * 360

        local offset = math.sin(0.0174533 * angle) * 250
        LuaUtils.SetLocalPositionX(child, offset)

        local scale = math.cos(0.0174533 * angle) * 0.2 + 0.8
        LuaUtils.SetLocalScale(child, scale, scale, 1)
    end
end

function LuaCommonPassportFashion2DView:OnDragStart(go)
    UnRegisterTick(self.previewTick)
    self.previewTick = nil
    for i, v in ipairs(self.m_Angles) do
        local child = self.m_Root:GetChild(i - 1)
        LuaTweenUtils.DOKill(child, false)
    end
end

function LuaCommonPassportFashion2DView:OnDrag(go, delta)
    local moveDeg = 57.2958 * math.asin(math.max(-1, math.min(delta.x / 400, 1)))
    for i, v in ipairs(self.m_Angles) do
        local angle = v + moveDeg
        self.m_Angles[i] = angle - math.floor(angle / 360) * 360
    end

    self:UpdatePosition()
end

function LuaCommonPassportFashion2DView:OnDragEnd(go)
    local selectIndex = 0

    local index = go.transform:GetSiblingIndex() + 1
    local x = go.transform.localPosition.x
    local scale = go.transform.localScale.x
    --看划过的距离
    if x > -50 and x < 50 and scale > 0.7 then
        selectIndex = index
    elseif x < 0 then
        selectIndex = index + 1
    elseif x > 0 then
        selectIndex = index - 1
    end

    if selectIndex < 1 then
        selectIndex = selectIndex + #self.m_Angles
    end
    if selectIndex > #self.m_Angles then
        selectIndex = selectIndex - #self.m_Angles
    end

    for i, v in ipairs(self.m_Angles) do
        local degree = (i - selectIndex) * self.m_DeltaDegree
        degree = degree - math.floor(degree / 360) * 360

        local moveTo = math.sin(0.0174533 * degree) * 250
        local scale = math.cos(0.0174533 * degree) * 0.2 + 0.8
        scale = math.max(0, math.min(1, scale))

        local child = self.m_Root:GetChild(i - 1)
        LuaTweenUtils.TweenPositionX(child, moveTo, 0.3)
        LuaTweenUtils.TweenScaleTo(child, Vector3(scale, scale, 1), 0.3)
        self.m_Angles[i] = degree
    end

    if x < 0 then
        self:NextPage()
    elseif x > 0 then
        self:LastPage()
    end
end
