require("3rdParty/ScriptEvent")
require("common/common_include")

local Extensions = import "Extensions"
local UILabel = import "UILabel"
local UIEventListener = import "UIEventListener"
local LuaUtils = import "LuaUtils"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local DelegateFactory = import "DelegateFactory"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local AlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local EnumClass = import "L10.Game.EnumClass"
local Profession = import "L10.Game.Profession"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local CUITexture = import "L10.UI.CUITexture"
local CUICommonDef = import "L10.UI.CUICommonDef"
local QnMoneyCostMesssageMgr = import "L10.UI.QnMoneyCostMesssageMgr"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CUIFx = import "L10.UI.CUIFx"

CLuaCrossSXDDZWnd = class()
RegistClassMember(CLuaCrossSXDDZWnd, "m_ClassSelectorLabel")
RegistClassMember(CLuaCrossSXDDZWnd, "m_ClassSelectorExpandTrans")
RegistClassMember(CLuaCrossSXDDZWnd, "m_WatchBtnObj")
RegistClassMember(CLuaCrossSXDDZWnd, "m_CenterWatchBtnObj")
RegistClassMember(CLuaCrossSXDDZWnd, "m_ChallengeBtnObj")
RegistClassMember(CLuaCrossSXDDZWnd, "m_EnterSceneBtnObj")
RegistClassMember(CLuaCrossSXDDZWnd, "m_TipLabel")
RegistClassMember(CLuaCrossSXDDZWnd, "m_BottomRootObj")
RegistClassMember(CLuaCrossSXDDZWnd, "m_BeforeStartRootObj")

-- champion
RegistClassMember(CLuaCrossSXDDZWnd, "m_ChampionRootObj")
RegistClassMember(CLuaCrossSXDDZWnd, "m_ChampionPortraitTex")
RegistClassMember(CLuaCrossSXDDZWnd, "m_ChampionServerNameLabel")
RegistClassMember(CLuaCrossSXDDZWnd, "m_ChampionNameLabel")

-- challenger
RegistClassMember(CLuaCrossSXDDZWnd, "m_ChallengerRootObj")
RegistClassMember(CLuaCrossSXDDZWnd, "m_ChallengerPortraitTex")
RegistClassMember(CLuaCrossSXDDZWnd, "m_ChallengerServerNameLabel")
RegistClassMember(CLuaCrossSXDDZWnd, "m_ChallengerNameLabel")
RegistClassMember(CLuaCrossSXDDZWnd, "m_ChallengingFx")

-- no challenger
RegistClassMember(CLuaCrossSXDDZWnd, "m_NoChallengerRootObj")
RegistClassMember(CLuaCrossSXDDZWnd, "m_ChallengeLeftTimeLabel")

-- final champion
RegistClassMember(CLuaCrossSXDDZWnd, "m_FinalChampionRootObj")
RegistClassMember(CLuaCrossSXDDZWnd, "m_FinalChampionPortraitTex")
RegistClassMember(CLuaCrossSXDDZWnd, "m_FinalChampionServerNameLabel")
RegistClassMember(CLuaCrossSXDDZWnd, "m_FinalChampionNameLabel")

-- data member
RegistClassMember(CLuaCrossSXDDZWnd, "m_SelectedClass")
RegistClassMember(CLuaCrossSXDDZWnd, "m_ClassTable")
RegistClassMember(CLuaCrossSXDDZWnd, "m_PopupMenuItemArray")
RegistClassMember(CLuaCrossSXDDZWnd, "m_Tick")
RegistClassMember(CLuaCrossSXDDZWnd, "m_HasFinished")
RegistClassMember(CLuaCrossSXDDZWnd, "m_HasChallenger")
RegistClassMember(CLuaCrossSXDDZWnd, "m_RefreshTick")

function CLuaCrossSXDDZWnd:Init( ... )
	if not CClientMainPlayer.Inst then
		CUIManager.CloseUI(CLuaUIResources.CrossSXDDZWnd)
		return
	end
	self.m_SelectedClass = CClientMainPlayer.Inst.Class

	local classSelectorTrans = self.transform:Find("ClassSelector")
	self.m_ClassSelectorLabel = classSelectorTrans:Find("Class").gameObject:GetComponent(typeof(UILabel))
	self.m_ClassSelectorExpandTrans = classSelectorTrans:Find("ExpandFlag")
	UIEventListener.Get(classSelectorTrans.gameObject).onClick = LuaUtils.VoidDelegate(function (go)
		self:ClassSelectorOnClick(go)
	end)

	self.m_BeforeStartRootObj = self.transform:Find("BeforeStart").gameObject
	self.m_ChallengingFx = self.transform:Find("Challenging/Fx"):GetComponent(typeof(CUIFx))
	-- init class list
	self.m_ClassTable = {}
	local popupMenuItemTable = {}
	local flag = {}
	Initialization_Init.Foreach(function (key, data)
		if data.Status == 0 and not flag[data.Class] then
			flag[data.Class] = 1
			local eClass = EnumFromInt(EnumClass, data.Class)
			table.insert(self.m_ClassTable, eClass)
			table.insert(popupMenuItemTable, PopupMenuItemData(Profession.GetFullName(eClass), DelegateFactory.Action_int(function ( index )
				self.m_SelectedClass = self.m_ClassTable[index + 1]
				self:InitWndByClass()
			end), false, nil, EnumPopupMenuItemStyle.Light))
		end
	end)
	self.m_PopupMenuItemArray = Table2Array(popupMenuItemTable, MakeArrayClass(PopupMenuItemData))
	self:InitWndByClass()

	UIEventListener.Get(self.transform:Find("TipBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		g_MessageMgr:ShowMessage("HOW_TO_PLAY_MPZS")
	end)

	self:InitButtons()
	self:InitChallenger()
	self:InitRefreshTick()
end

function CLuaCrossSXDDZWnd:InitChallenger( ... )
	self.m_FinalChampionRootObj = self.transform:Find("Finished").gameObject
	self.m_FinalChampionPortraitTex = self.transform:Find("Finished/Portrait").gameObject:GetComponent(typeof(CUITexture))
	self.m_FinalChampionNameLabel = self.transform:Find("Finished/PlayerName").gameObject:GetComponent(typeof(UILabel))
	self.m_FinalChampionServerNameLabel = self.transform:Find("Finished/ServerName").gameObject:GetComponent(typeof(UILabel))

	self.m_ChampionRootObj = self.transform:Find("Challenging").gameObject
	self.m_ChampionPortraitTex = self.transform:Find("Challenging/Champion/Portrait").gameObject:GetComponent(typeof(CUITexture))
	self.m_ChampionNameLabel = self.transform:Find("Challenging/Champion/PlayerName").gameObject:GetComponent(typeof(UILabel))
	self.m_ChampionServerNameLabel = self.transform:Find("Challenging/Champion/ServerName").gameObject:GetComponent(typeof(UILabel))

	self.m_ChallengerRootObj = self.transform:Find("Challenging/Challenger/HasChallenger").gameObject
	self.m_ChallengerPortraitTex = self.transform:Find("Challenging/Challenger/HasChallenger/Portrait").gameObject:GetComponent(typeof(CUITexture))
	self.m_ChallengerNameLabel = self.transform:Find("Challenging/Challenger/HasChallenger/PlayerName").gameObject:GetComponent(typeof(UILabel))
	self.m_ChallengerServerNameLabel = self.transform:Find("Challenging/Challenger/HasChallenger/ServerName").gameObject:GetComponent(typeof(UILabel))

	self.m_NoChallengerRootObj = self.transform:Find("Challenging/Challenger/NoChallenger").gameObject
	self.m_ChallengeLeftTimeLabel = self.transform:Find("Challenging/Challenger/NoChallenger/Time").gameObject:GetComponent(typeof(UILabel))
end

function CLuaCrossSXDDZWnd:ApplyWatch( ... )
	local str = g_MessageMgr:FormatMessage("MPZS_WATCH_FIGHT_COST", ShouXiDaDiZi_Setting.GetData().CrossWatchCost)
	QnMoneyCostMesssageMgr.ShowMoneyCostMessageBox(EnumMoneyType.YinPiao, str, ShouXiDaDiZi_Setting.GetData().CrossWatchCost, DelegateFactory.Action(function ( ... )
		Gac2Gas.ApplyWatchCrossSXDDZ(self.m_SelectedClass)
	end), nil, false, true, EnumPlayScoreKey.NONE, false)
end

function CLuaCrossSXDDZWnd:InitButtons()
	self.m_EnterSceneBtnObj = self.transform:Find("Bottom/EnterSceneBtn").gameObject
	UIEventListener.Get(self.m_EnterSceneBtnObj).onClick = LuaUtils.VoidDelegate(function ( ... )
		Gac2Gas.RequestEnterCrossSXDDZPrepareScene()
	end)

	self.m_ChallengeBtnObj = self.transform:Find("Bottom/ChallengeBtn").gameObject
	UIEventListener.Get(self.m_ChallengeBtnObj).onClick = LuaUtils.VoidDelegate(function ( ... )
		Gac2Gas.ApplyChallengeCrossSXDDZChampion(self.m_SelectedClass)
	end)

	self.m_WatchBtnObj = self.transform:Find("Bottom/WatchBtn").gameObject
	UIEventListener.Get(self.m_WatchBtnObj).onClick = LuaUtils.VoidDelegate(function ( ... )
		self:ApplyWatch()
	end)

	self.m_CenterWatchBtnObj = self.transform:Find("Bottom/CenterWatchBtn").gameObject
	UIEventListener.Get(self.m_CenterWatchBtnObj).onClick = LuaUtils.VoidDelegate(function ( ... )
		self:ApplyWatch()
	end)

	UIEventListener.Get(self.transform:Find("Bottom/ChallengerListBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		CUIManager.ShowUI(CLuaUIResources.CrossSXDDZChallengerListWnd)
	end)

	self.m_TipLabel = self.transform:Find("Bottom/TipLabel"):GetComponent(typeof(UILabel))
	self.m_BottomRootObj = self.transform:Find("Bottom").gameObject
end

function CLuaCrossSXDDZWnd:ClassSelectorOnClick(go)
	Extensions.SetLocalRotationZ(self.m_ClassSelectorExpandTrans, 180)
	CPopupMenuInfoMgr.ShowPopupMenu(self.m_PopupMenuItemArray, go.transform, AlignType.Bottom, 1, DelegateFactory.Action(function ( ... )
		Extensions.SetLocalRotationZ(self.m_ClassSelectorExpandTrans, 0)
	end), 600, true, 296)
end

function CLuaCrossSXDDZWnd:InitWndByClass()
	self.m_ClassSelectorLabel.text = Profession.GetFullName(self.m_SelectedClass)
	Gac2Gas.CrossSXDDZQueryCurrentChallengeInfo(self.m_SelectedClass)
end

function CLuaCrossSXDDZWnd:UpdateChallengeData(championInfo, challengerInfo)
	-- hide the info
	self.m_BeforeStartRootObj:SetActive(false)
	-- clear the tick and fx
	self:InitTick(0)
	self:UpdateFx(false)

	local isFinalChampion = ((not challengerInfo or challengerInfo.Count <= 0) and championInfo[5] <= 0) or championInfo[0] <= 0
	self.m_FinalChampionRootObj:SetActive(isFinalChampion)
	self.m_ChampionRootObj:SetActive(not isFinalChampion)
	self.m_HasFinished = isFinalChampion

	-- final champion
	if isFinalChampion then
		-- no champion
		if championInfo[0] <= 0 then
			self.m_FinalChampionNameLabel.text = LocalString.GetString("未决出")
			self.m_FinalChampionPortraitTex:LoadPortrait("", false)
			self.m_FinalChampionServerNameLabel.text = ""
		else
			self.m_FinalChampionNameLabel.text = championInfo[1]
			self.m_FinalChampionServerNameLabel.text = championInfo[2]
			self.m_FinalChampionPortraitTex:LoadPortrait(CUICommonDef.GetPortraitName(championInfo[4], championInfo[3], -1), true)
		end
	else
		local hasChallenger = challengerInfo and challengerInfo.Count == 5
		self.m_HasChallenger = hasChallenger
		self.m_ChallengerRootObj:SetActive(hasChallenger)
		self.m_NoChallengerRootObj:SetActive(not hasChallenger)

		-- init champion info
		self.m_ChampionNameLabel.text = championInfo[1]
		self.m_ChampionServerNameLabel.text = championInfo[2]
		self.m_ChampionPortraitTex:LoadPortrait(CUICommonDef.GetPortraitName(championInfo[4], championInfo[3], -1), true)
		if hasChallenger then
			self:UpdateFx(true)
			self.m_ChallengerNameLabel.text = challengerInfo[1]
			self.m_ChallengerServerNameLabel.text = challengerInfo[2]
			self.m_ChallengerPortraitTex:LoadPortrait(CUICommonDef.GetPortraitName(challengerInfo[4], challengerInfo[3], -1), true)
		else
			-- no challenger
			self:InitTick(championInfo[5])
		end
	end
end

function CLuaCrossSXDDZWnd:InitTick(time)
	if self.m_Tick then
		UnRegisterTick(self.m_Tick)
		self.m_Tick = nil
	end
	if time <= 0 then return end
	local duration = ShouXiDaDiZi_Setting.GetData().NoChallengerWaitTime
	local endTime = time + duration
	self:UpdateTime(endTime)
	self.m_Tick = RegisterTickWithDuration(function ()
		self:UpdateTime(endTime)
	end, 1000, duration * 1000)
end

function CLuaCrossSXDDZWnd:UpdateTime(endTime)
	if CServerTimeMgr.Inst.timeStamp > endTime then
		UnRegisterTick(self.m_Tick)
		self.m_Tick = nil
		Gac2Gas.CrossSXDDZQueryCurrentChallengeInfo(self.m_SelectedClass)
	else
		local leftTime = endTime - CServerTimeMgr.Inst.timeStamp
		self.m_ChallengeLeftTimeLabel.text = SafeStringFormat3("%02d:%02d", math.floor(leftTime / 60), math.floor(leftTime % 60))
	end
end

function CLuaCrossSXDDZWnd:UpdateFx(hasFx)
	if hasFx then
		self.m_ChallengingFx:LoadFx("Fx/UI/Prefab/ui_duizhen001.prefab")
	else
		self.m_ChallengingFx:DestroyFx()
	end
end

function CLuaCrossSXDDZWnd:InitRefreshTick()
	if self.m_RefreshTick then
		UnRegisterTick(self.m_RefreshTick)
	end
	self.m_RefreshTick = RegisterTickWithDuration(function ()
		Gac2Gas.CrossSXDDZQueryCurrentChallengeInfo(self.m_SelectedClass)
	end, 3000, 3600 * 1000)
end

function CLuaCrossSXDDZWnd:InitBottomArea()
	self.m_BottomRootObj:SetActive(true)
	if not CClientMainPlayer.Inst then
		return
	end
	local isInList = false
	local isOutGame = true
	local isChampion = false
	for i = 0, CLuaCrossSXDDZWnd.ChallengerList.Count - 1 do
		if CLuaCrossSXDDZWnd.ChallengerList[i][0] == CClientMainPlayer.Inst.Id then
			isInList = true
			isOutGame = CLuaCrossSXDDZWnd.ChallengerList[i][6] == 3
			isChampion = CLuaCrossSXDDZWnd.ChallengerList[i][6] == 2
		end
	end

	self.m_TipLabel.gameObject:SetActive(not isInList or isOutGame)
	self.m_EnterSceneBtnObj:SetActive(isInList and not isOutGame)
	local showChallengeBtn = isInList and not isOutGame and not self.m_HasFinished and not isChampion
	self.m_ChallengeBtnObj:SetActive(showChallengeBtn)
	self.m_WatchBtnObj:SetActive(not showChallengeBtn)
	self.m_CenterWatchBtnObj:SetActive(showChallengeBtn)
	self.m_BottomRootObj:SetActive(not self.m_HasFinished)

	if not isInList then
		self.m_TipLabel.text = LocalString.GetString("你尚未具备挑战资格")
	elseif isOutGame then
		self.m_TipLabel.text = LocalString.GetString("你已淘汰出局")
	end
end

function CLuaCrossSXDDZWnd:CrossSXDDZQueryCurrentChallengeInfoResult( infoUD )
	local list = MsgPackImpl.unpack(infoUD)
	local championInfo = list[0]
	local challengerInfo = list.Count > 1 and list[1] or nil
	if not championInfo or championInfo.Count ~= 6 then
		CUIManager.CloseUI(CLuaUIResources.CrossSXDDZWnd)
		return
	end
	self:UpdateChallengeData(championInfo, challengerInfo)

	CLuaCrossSXDDZWnd.ChallengerList = list.Count > 2 and list[2] or nil
	self:InitBottomArea()
end

function CLuaCrossSXDDZWnd:OnEnable( ... )
	g_ScriptEvent:AddListener("CrossSXDDZQueryCurrentChallengeInfoResult", self, "CrossSXDDZQueryCurrentChallengeInfoResult")
end

function CLuaCrossSXDDZWnd:OnDisable( ... )
	g_ScriptEvent:RemoveListener("CrossSXDDZQueryCurrentChallengeInfoResult", self, "CrossSXDDZQueryCurrentChallengeInfoResult")
end

function CLuaCrossSXDDZWnd:OnDestroy( ... )
	if self.m_Tick then
		UnRegisterTick(self.m_Tick)
		self.m_Tick = nil
	end
	if self.m_RefreshTick then
		UnRegisterTick(self.m_RefreshTick)
		self.m_RefreshTick = nil
	end
end

return CLuaCrossSXDDZWnd
