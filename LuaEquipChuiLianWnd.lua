require("common/common_include")
--锤炼
local CItemChooseMgr=import "L10.UI.CItemChooseMgr"
local CItemMgr=import "L10.Game.CItemMgr"
local EnumItemPlace=import "L10.Game.EnumItemPlace"
local EquipmentTemplate_Equip=import "L10.Game.EquipmentTemplate_Equip"
local CItemCountUpdate=import "L10.UI.CItemCountUpdate"
local Item_Item=import "L10.Game.Item_Item"
local CUITexture=import "L10.UI.CUITexture"
local UITable=import "UITable"
local CItemAccessListMgr=import "L10.UI.CItemAccessListMgr"
-- local Gac2Gas=import "L10.Game.Gac2Gas"
local UIWidget=import "UIWidget"
local Word_Word=import "L10.Game.Word_Word"
local UILabel=import "UILabel"
local CEquipmentBaptizeMgr=import "L10.Game.CEquipmentBaptizeMgr"
local CPropertyDifMgr=import "L10.UI.CPropertyDifMgr"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local Convert=import "System.Convert"
local CTooltipAlignType=import "L10.UI.CTooltip+AlignType"
local PlayerShowDifProType=import "L10.Game.PlayerShowDifProType"
local MessageWndManager=import "L10.UI.MessageWndManager"

CLuaEquipChuiLianWnd=class()
RegistClassMember(CLuaEquipChuiLianWnd,"m_SelectEquipButton")
RegistClassMember(CLuaEquipChuiLianWnd,"m_ReplaceButton")
RegistClassMember(CLuaEquipChuiLianWnd,"m_ChuiLianButton")
RegistClassMember(CLuaEquipChuiLianWnd,"m_TipButton")
RegistClassMember(CLuaEquipChuiLianWnd,"m_SelectedEquip")

RegistClassMember(CLuaEquipChuiLianWnd,"m_SelectedEquipId")
RegistClassMember(CLuaEquipChuiLianWnd,"m_CostItem")

RegistClassMember(CLuaEquipChuiLianWnd,"m_PreDescTable")
RegistClassMember(CLuaEquipChuiLianWnd,"m_PostDescTable")
RegistClassMember(CLuaEquipChuiLianWnd,"m_DescItem")

RegistClassMember(CLuaEquipChuiLianWnd,"m_PreScoreLabel")
RegistClassMember(CLuaEquipChuiLianWnd,"m_PostScoreLabel")
RegistClassMember(CLuaEquipChuiLianWnd,"m_ChuiLianButtonLabel")

RegistClassMember(CLuaEquipChuiLianWnd,"m_LookupButton")

RegistClassMember(CLuaEquipChuiLianWnd,"m_NeedCount")
RegistClassMember(CLuaEquipChuiLianWnd,"m_CountUpdate")

RegistClassMember(CLuaEquipChuiLianWnd,"m_TitleLabel")
RegistClassMember(CLuaEquipChuiLianWnd,"m_AchieveGoal")
RegistClassMember(CLuaEquipChuiLianWnd,"m_ChangeProp")


function CLuaEquipChuiLianWnd:Awake()
    self.m_AchieveGoal=false
    self.m_NeedCount=0
    self.m_ChangeProp=nil

    self.m_TitleLabel=FindChild(self.transform,"TitleLabel"):GetComponent(typeof(UILabel))
    self.m_LookupButton=FindChild(self.transform,"LookupButton").gameObject
    UIEventListener.Get(self.m_LookupButton).onClick=LuaUtils.VoidDelegate(function(go)
        --查看一下是否装备
        if self.m_SelectedEquipId then
            local itemInfo = CItemMgr.Inst:GetItemInfo(self.m_SelectedEquipId)
            if itemInfo then
                local equipListOnBody = CItemMgr.Inst:GetPlayerEquipmentListAtPlace(EnumItemPlace.Body)
                local find = false
                for i=1,equipListOnBody.Count do
                    if equipListOnBody[i-1].itemId == self.m_SelectedEquipId then
                        find = true
                        break
                    end
                end
                if find then
                    local place=Convert.ToUInt32(itemInfo.place)
                    local pos=itemInfo.pos
                    Gac2Gas.TryConfirmBaptizeWord(place,pos,self.m_SelectedEquipId,true)
                else
                    g_MessageMgr:ShowMessage("Equip_Compare_NotWear_Tips",{})
                end
            end
        end
    end)

    self.m_PreDescTable=FindChild(self.transform,"PreDescTable"):GetComponent(typeof(UITable))
    self.m_PostDescTable=FindChild(self.transform,"PostDescTable"):GetComponent(typeof(UITable))
    self.m_DescItem=FindChild(self.transform,"DescItem").gameObject

    self.m_PreScoreLabel=FindChild(self.transform,"PreScoreLabel"):GetComponent(typeof(UILabel))
    self.m_PostScoreLabel=FindChild(self.transform,"PostScoreLabel"):GetComponent(typeof(UILabel))

    self.m_ChuiLianButtonLabel=FindChild(self.transform,"ChuiLianButtonLabel"):GetComponent(typeof(UILabel))
    self.m_SelectEquipButton=FindChild(self.transform,"SelectEquipButton").gameObject
    UIEventListener.Get(self.m_SelectEquipButton).onClick=LuaUtils.VoidDelegate(function(go)

        CItemChooseMgr.Inst.ItemIds=CLuaEquipZhouNianQingMgr.GetEquipList()
        CItemChooseMgr.Inst.OnChoose=DelegateFactory.Action_string(function(p)
            self.m_SelectedEquipId=p
            self:OnChooseEquip(p)
        end)
        CItemChooseMgr.ShowWnd(LocalString.GetString("选择装备"))

    end)

    self.m_ReplaceButton=FindChild(self.transform,"ReplaceButton").gameObject

    self.m_ChuiLianButton=FindChild(self.transform,"ChuiLianButton").gameObject
    self.m_TipButton=FindChild(self.transform,"TipButton").gameObject
    UIEventListener.Get(self.m_ReplaceButton).onClick=LuaUtils.VoidDelegate(function(go)
        self:OnClickReplaceButton()
    end)
    UIEventListener.Get(self.m_ChuiLianButton).onClick=LuaUtils.VoidDelegate(function(go)
        self:OnClickChuiLianButton()
    end)
    UIEventListener.Get(self.m_TipButton).onClick=LuaUtils.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("Equip_ChuiLian_Readme",{})
    end)

    self.m_SelectedEquip=FindChild(self.transform,"SelectedEquip").gameObject

    self.m_CostItem=FindChild(self.transform,"CostItem").gameObject
    
end
function CLuaEquipChuiLianWnd:OnClickReplaceButton()
    local itemInfo = CItemMgr.Inst:GetItemInfo(self.m_SelectedEquipId)
    if itemInfo then
        local place=Convert.ToUInt32(itemInfo.place)
        local pos=itemInfo.pos
        self.m_ChangeProp=nil
        Gac2Gas.BaptizeWordResultConfirm(place,pos,self.m_SelectedEquipId,true)
    end
end
function CLuaEquipChuiLianWnd:OnClickChuiLianButton()
    if self.m_SelectedEquipId then
        local function chuilian()
            if self.m_CountUpdate.count>=self.m_NeedCount then
                local itemInfo = CItemMgr.Inst:GetItemInfo(self.m_SelectedEquipId)
                if itemInfo then
                    local place=Convert.ToUInt32(itemInfo.place)
                    local pos=itemInfo.pos
                    self.m_ChangeProp=nil
                    Gac2Gas.RequestChuiLianEquipWord(place,pos,self.m_SelectedEquipId,CLuaEquipZhouNianQingMgr.m_ChuiLianItemId)
                end
            else
                g_MessageMgr:ShowMessage("Ghost_Equip_Compose_CaiLiao_Is_Lack",{})
            end
        end
        if self.m_AchieveGoal then
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Better_Crafting_Equipment"),DelegateFactory.Action(function () 
                chuilian()
            end),nil, nil, nil, false)
        else
            chuilian()
        end

    else
        g_MessageMgr:ShowMessage("Double_EquipWords_Need_Select_Equip",{})
    end
end

function CLuaEquipChuiLianWnd:Init()
    self.m_DescItem:SetActive(false)

    self:SetPreScore(0)
    self:SetPostScore(0)

    if CLuaEquipZhouNianQingMgr.m_ChuiLianItemId>0 then
        local template = Item_Item.GetData(CLuaEquipZhouNianQingMgr.m_ChuiLianItemId)
        if template then
            local name=template.Name
            self.m_ChuiLianButtonLabel.text=name
            self.m_TitleLabel.text=name
        end
    end

    self:InitSelectedEquip()


    self:InitCostItem()

    self:UpdateReplaceButtonState()

    -- self:TryShowLastResult()
end


function CLuaEquipChuiLianWnd:GetNeedCount()
    local equip = CItemMgr.Inst:GetById(self.m_SelectedEquipId)--CCommonItem
    if equip then
        return CLuaEquipZhouNianQingMgr.GetChuiLianCost(equip.Grade)
    end
    return 0
end


function CLuaEquipChuiLianWnd:InitCostItem()
    local tf=self.m_CostItem.transform
    local templateId=CLuaEquipZhouNianQingMgr.m_ChuiLianItemId
    local template = Item_Item.GetData(templateId)
    if template then
        local icon=FindChild(tf,"Icon"):GetComponent(typeof(CUITexture))
        icon:LoadMaterial(template.Icon)

        FindChild(tf,"NameLabel"):GetComponent(typeof(UILabel)).text=template.Name
        local mask=FindChild(tf,"Mask").gameObject
        self.m_CountUpdate=tf:GetComponent(typeof(CItemCountUpdate))
        self.m_CountUpdate.templateId=templateId
        self.m_CountUpdate.excludeExpireTime=true
        -- self.m_NeedCount=self:GetNeedCount()
        self.m_NeedCount=0
        local function OnCountChange(val)
            if val>=self.m_NeedCount then
                mask:SetActive(false)
                self.m_CountUpdate.format=SafeStringFormat3("{0}/%d",self.m_NeedCount)
            else
                mask:SetActive(true)
                self.m_CountUpdate.format=SafeStringFormat3("[ff0000]{0}[-]/%d",self.m_NeedCount)
            end
        end
        self.m_CountUpdate.onChange=DelegateFactory.Action_int(OnCountChange)
        self.m_CountUpdate:UpdateCount()

        --点击支机石
        UIEventListener.Get(self.m_CostItem).onClick=LuaUtils.VoidDelegate(function(go)
            if self.m_CountUpdate.count>=self.m_NeedCount then
                CItemInfoMgr.ShowLinkItemTemplateInfo(templateId,false,nil,CItemInfoMgr.AlignType.Default,0,0,0,0)
            else
                CItemAccessListMgr.Inst:ShowItemAccessInfo(templateId, false, tf, CTooltipAlignType.Right)
            end
        end)
    end
end

function CLuaEquipChuiLianWnd:InitSelectedEquip()
    local tf=self.m_SelectedEquip.transform
    local spriteMark=FindChild(tf,"EquipMark"):GetComponent(typeof(UISprite))
    spriteMark.spriteName = nil
    local icon=FindChild(tf,"EquipIcon"):GetComponent(typeof(CUITexture))
    icon:Clear()

    local label=FindChild(tf,"NameLabel"):GetComponent(typeof(UILabel))
    label.text=nil
end

function CLuaEquipChuiLianWnd:OnChooseEquip(itemId)
    if self.m_SelectedEquipId then
        self.m_AchieveGoal=false
        CEquipmentBaptizeMgr.Inst:InitEquipCondition(self.m_SelectedEquipId)
        self.m_NeedCount=self:GetNeedCount()
        self.m_CountUpdate:UpdateCount()

        local tf=self.m_SelectedEquip.transform
        local spriteMark=FindChild(tf,"EquipMark"):GetComponent(typeof(UISprite))
        local icon=FindChild(tf,"EquipIcon"):GetComponent(typeof(CUITexture))
        -- icon:Clear()

        local equip = CItemMgr.Inst:GetById(self.m_SelectedEquipId)--CCommonItem
        spriteMark.spriteName = equip.BindOrEquipCornerMark
        
        local template = EquipmentTemplate_Equip.GetData(equip.TemplateId)
        icon:LoadMaterial(template.Icon)

        local label=FindChild(tf,"NameLabel"):GetComponent(typeof(UILabel))
        label.text=equip.Equip.DisplayName
        label.color=equip.Equip.DisplayColor


        local qualitySprite=FindChild(tf,"QualitySprite"):GetComponent(typeof(UISprite))
        qualitySprite.spriteName=CUICommonDef.GetItemCellBorder(equip.Equip.QualityType)

        -- self.m_TipLabel.enabled=false

        self:InitPreDescTable()

        --查询是否有残留的临时词条
        self:TryShowLastResult()

        self:UpdateReplaceButtonState()
    end
end


function CLuaEquipChuiLianWnd:TryShowLastResult()
    self:SetPostScore(0)
    CUICommonDef.ClearTransform(self.m_PostDescTable.transform)


    local itemInfo = CItemMgr.Inst:GetItemInfo(self.m_SelectedEquipId)
    if itemInfo then
        local place=Convert.ToUInt32(itemInfo.place)
        local pos=itemInfo.pos
        if self:HasTempWordData() then
            Gac2Gas.RequestEquipPropertyChangedWithTempWords(place,pos,self.m_SelectedEquipId)
        end
    end
end


function CLuaEquipChuiLianWnd:InitPreDescTable()
    CUICommonDef.ClearTransform(self.m_PreDescTable.transform)
    local parent=self.m_PreDescTable.gameObject
    
    local item=CItemMgr.Inst:GetById(self.m_SelectedEquipId)
    if item then
        local color=item.Equip.DisplayColor
        self:SetPreScore(item.Equip.Score)

        local list1=item.Equip:GetFixedWordList(false)
        local list2=item.Equip:GetExtWordList(false)


        local t={}
        for i=1,list1.Count do
            table.insert( t,list1[i-1] )
        end
        for i=1,list2.Count do
            table.insert( t,list2[i-1] )
        end


        for i,v in ipairs(t) do
            local go=NGUITools.AddChild(parent,self.m_DescItem)
            go:SetActive(true)
            self:InitDescItem(go,v,color)
        end

        self.m_PreDescTable:Reposition()
    end

end
function CLuaEquipChuiLianWnd:InitPostDescTable(changeProperties)
    self.m_AchieveGoal=false
    CUICommonDef.ClearTransform(self.m_PostDescTable.transform)
    local parent=self.m_PostDescTable.gameObject
    
    local item=CItemMgr.Inst:GetById(self.m_SelectedEquipId)
    if item then
        self:SetPostScore(item.Equip.TempScore)

        local list1=item.Equip:GetTempFixedWordArray()
        local list2=item.Equip:GetTempExtWordArray()

        local count=0
        for i=1,list1.Length do
            if list1[i-1]>0 then
                count=count+1
            end
        end
        for i=1,list2.Length do
            if list2[i-1]>0 then
                count=count+1
            end
        end

        local color=item.Equip:GetNewColor(count)--item.Equip.DisplayColor

        if changeProperties and CommonDefs.IsDic(changeProperties) then
            local listtype = MakeGenericClass(List,PlayerShowDifProType)
            local difflist=CreateFromClass(listtype)
            CommonDefs.DictIterate(changeProperties,DelegateFactory.Action_object_object(function(key,val)
                local add = item.Equip:GetPropertyDif(key,val)
                if add then
                    CommonDefs.ListAdd(difflist,typeof(PlayerShowDifProType),add)
                end
            end))
            if difflist.Count>0 then
                local go=NGUITools.AddChild(parent,self.m_DescItem)
                go:SetActive(true)
                self:InitDescItem(go,0,color,difflist)
            end
        end

        local t={}
        local fixedWordList=CreateFromClass(MakeGenericClass(List,uint))
        local extWordList=CreateFromClass(MakeGenericClass(List,uint))
        
        for i=1,list1.Length do
            local wordId=list1[i-1]
            if wordId>0 then
                table.insert( t,wordId )
                CommonDefs.ListAdd(fixedWordList,typeof(uint),wordId)
            end
        end
        for i=1,list2.Length do
            local wordId=list2[i-1]
            if wordId>0 then
                table.insert( t,wordId )
                CommonDefs.ListAdd(extWordList,typeof(uint),wordId)
            end
        end
        
        if CLuaEquipXiLianMgr.CheckBaptizeResult(fixedWordList,extWordList) then
            self.m_AchieveGoal=true
        end

        for i,v in ipairs(t) do
            local go=NGUITools.AddChild(parent,self.m_DescItem)
            go:SetActive(true)
            self:InitDescItem(go,v,color)
        end

        self.m_PostDescTable:Reposition()
    end

end

function CLuaEquipChuiLianWnd:InitDescItem(go,wordId,color,difflist)
    local tf=go.transform
    local nameLabel=FindChild(tf,"NameLabel"):GetComponent(typeof(UILabel))
    local descLabel=FindChild(tf,"DescLabel"):GetComponent(typeof(UILabel))
    local bgWidget=go:GetComponent(typeof(UIWidget))
    -- local lineSprite=FindChild(tf,"LineSprite"):GetComponent(typeof(UISprite))
    local lookupButton=FindChild(tf,"LookupButton").gameObject

    if wordId>0 then
        nameLabel.color=color
        descLabel.color=color
        local data = Word_Word.GetData(wordId)
        if data then
            nameLabel.text=SafeStringFormat3( "[%s]",data.Name )
            descLabel.text=data.Description
        end
        lookupButton:SetActive(false)
    else
        lookupButton:SetActive(true)
        nameLabel.text=nil

        for i=1,difflist.Count do
        -- for i,v in ipairs(difflist) do
            local v=difflist[i-1]
            if v.oldValue>v.nowValue then
                descLabel.text=LocalString.GetString("[c][ff2525]基础属性降低[-][/c]")
                break
            else
                descLabel.text=LocalString.GetString("[c][00ff00]基础属性提升[-][/c]")
            end
        end
        descLabel.transform.localPosition=Vector3(-245,-5,0)
        descLabel.width=490

        UIEventListener.Get(lookupButton).onClick=LuaUtils.VoidDelegate(function(go)
            CPropertyDifMgr.ShowEquipDifWnd(difflist)
        end)

    end

    descLabel:UpdateNGUIText()
    -- lineSprite.enabled = true
    bgWidget.height = descLabel.localSize.y + 20
end



function CLuaEquipChuiLianWnd:OnEnable()
    g_ScriptEvent:AddListener("ChuiLianEquipWordSuccess", self, "OnChuiLianEquipWordSuccess")
    g_ScriptEvent:AddListener("SendItem", self, "OnSendItem")
    g_ScriptEvent:AddListener("ChuiLianEquipWordFail", self, "OnChuiLianEquipWordFail")
    g_ScriptEvent:AddListener("TryConfirmBaptizeWordResult", self, "OnTryConfirmBaptizeWordResult")
    g_ScriptEvent:AddListener("RequestEquipPropertyChangedWithTempWordsReturn", self, "OnRequestEquipPropertyChangedWithTempWordsReturn")
    
end


function CLuaEquipChuiLianWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ChuiLianEquipWordSuccess", self, "OnChuiLianEquipWordSuccess")
    g_ScriptEvent:RemoveListener("SendItem", self, "OnSendItem")
    g_ScriptEvent:RemoveListener("ChuiLianEquipWordFail", self, "OnChuiLianEquipWordFail")
    g_ScriptEvent:RemoveListener("TryConfirmBaptizeWordResult", self, "OnTryConfirmBaptizeWordResult")
    g_ScriptEvent:RemoveListener("RequestEquipPropertyChangedWithTempWordsReturn", self, "OnRequestEquipPropertyChangedWithTempWordsReturn")
end

function CLuaEquipChuiLianWnd:OnRequestEquipPropertyChangedWithTempWordsReturn(params)
    local dic=params[0]
    self.m_ChangeProp=dic
    self:InitPostDescTable(self.m_ChangeProp)
end

function CLuaEquipChuiLianWnd:OnTryConfirmBaptizeWordResult(params)
    local list=params[0]--CEquipmentBaptizeMgr.s_CachePropertyDifList
    if not list then return end

    if list.Count==0 then
        g_MessageMgr:ShowMessage("BaptizeWordResultNoPropDif",{})
        return
    end
    CPropertyDifMgr.ShowPlayerProDifWithEquip(list)
end

function CLuaEquipChuiLianWnd:SetPreScore(score)
    self.m_PreScoreLabel.text=SafeStringFormat3(LocalString.GetString("评分:%d"),score)
end
function CLuaEquipChuiLianWnd:SetPostScore(score)
    self.m_PostScoreLabel.text=SafeStringFormat3(LocalString.GetString("评分:%d"),score)
end

function CLuaEquipChuiLianWnd:OnChuiLianEquipWordSuccess(equipId, equipTempPropUD)
    if self.m_SelectedEquipId==equipId then

        local dic=MsgPackImpl.unpack(equipTempPropUD)

        self.m_ChangeProp=dic
        self:InitPostDescTable(self.m_ChangeProp)
        CUICommonDef.SetActive(self.m_ReplaceButton,true,true)
    end
end

function CLuaEquipChuiLianWnd:OnSendItem(params)
    local itemId=params[0]
    if self.m_SelectedEquipId==itemId then
        self:InitPreDescTable()
        self:InitPostDescTable(self.m_ChangeProp)
        -- CUICommonDef.SetActive(self.m_ReplaceButton,false,true)
        self:UpdateReplaceButtonState()
    end
end

function CLuaEquipChuiLianWnd:HasTempWordData()
    if self.m_SelectedEquipId then
        local item=CItemMgr.Inst:GetById(self.m_SelectedEquipId)
        if item then
            return item.Equip:HasTempWordData()
        end
    end
    return false
end
function CLuaEquipChuiLianWnd:UpdateReplaceButtonState()
    if self:HasTempWordData() then
        CUICommonDef.SetActive(self.m_ReplaceButton,true,true)
    else
        CUICommonDef.SetActive(self.m_ReplaceButton,false,true)
    end
end

function CLuaEquipChuiLianWnd:OnChuiLianEquipWordFail(equipId)
    if self.m_SelectedEquipId==equipId then
        CUICommonDef.SetActive(self.m_ChuiLianButton,true,true)
        -- CUICommonDef.SetActive(self.m_ReplaceButton,true,true)
        self:UpdateReplaceButtonState()
    end
end


return CLuaEquipChuiLianWnd
