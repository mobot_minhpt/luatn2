local UISprite = import "UISprite"
local UIWidget = import "UIWidget"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CIMMgr = import "L10.Game.CIMMgr"
local EnumNPCChatType = import "L10.Game.EnumNPCChatType"
local EnumNPCChatMsgLocation = import "L10.Game.EnumNPCChatMsgLocation"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CUIFx = import "L10.UI.CUIFx"

LuaNPCChatPanel = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaNPCChatPanel, "ChoosePanel", "ChoosePanel", GameObject)
RegistChildComponent(LuaNPCChatPanel, "NPCWaitPlayerPanel", "NPCWaitPlayerPanel", GameObject)
RegistChildComponent(LuaNPCChatPanel, "NPCChatChooseItem", "NPCChatChooseItem", GameObject)
RegistChildComponent(LuaNPCChatPanel, "Grid", "Grid", GameObject)
RegistChildComponent(LuaNPCChatPanel, "ChatInput", "ChatInput", UIWidget)
RegistChildComponent(LuaNPCChatPanel, "Pool", "Pool", GameObject)
RegistChildComponent(LuaNPCChatPanel, "NPCChatChooseEmpty", "NPCChatChooseEmpty", GameObject)
RegistChildComponent(LuaNPCChatPanel, "TipLabel", "TipLabel", UILabel)
RegistChildComponent(LuaNPCChatPanel, "Input", "Input", GameObject)
RegistChildComponent(LuaNPCChatPanel, "EmoticonButton", "EmoticonButton", GameObject)
RegistChildComponent(LuaNPCChatPanel, "SendMsgButton", "SendMsgButton", GameObject)
RegistChildComponent(LuaNPCChatPanel, "SwitchButton", "SwitchButton", GameObject)
RegistChildComponent(LuaNPCChatPanel, "Sprite", "Sprite", UISprite)
RegistChildComponent(LuaNPCChatPanel, "OptionFx", "OptionFx", CUIFx)

--@endregion RegistChildComponent end
RegistClassMember(LuaNPCChatPanel, "m_OppositeId")
RegistClassMember(LuaNPCChatPanel, "m_MsgID")
RegistClassMember(LuaNPCChatPanel, "m_NPCChatStatus")
RegistClassMember(LuaNPCChatPanel, "m_ShowNPCInputTick")
RegistClassMember(LuaNPCChatPanel, "m_ChooseIndex")

RegistClassMember(LuaNPCChatPanel, "m_OptionsList")
RegistClassMember(LuaNPCChatPanel, "m_OptionsGoList")

function LuaNPCChatPanel:Awake()
    self.m_ShowNPCInputTick = nil
    self.m_NPCChatStatus = EnumNPCChatStatus.FreeChat
    UIEventListener.Get(self.Input).onClick = DelegateFactory.VoidDelegate(function()
        self:ShowOptionMsg()
    end)
    UIEventListener.Get(self.EmoticonButton).onClick = DelegateFactory.VoidDelegate(function()
        self:ShowOptionMsg()
    end)
    UIEventListener.Get(self.SwitchButton).onClick = DelegateFactory.VoidDelegate(function()
        self:ShowOptionMsg()
    end)

    UIEventListener.Get(self.SendMsgButton).onClick = DelegateFactory.VoidDelegate(function()
        self:OnSendPlayerOption()
    end)
    local Fx = self.NPCWaitPlayerPanel.transform:Find("fx"):GetComponent(typeof(CUIFx))
    Fx:DestroyFx()
    Fx:LoadFx("fx/ui/prefab/UI_liaotian_saoguang.prefab")
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaNPCChatPanel:Init()

end
-- 等待NPC输入
function LuaNPCChatPanel:ShowWaitForNPCInput()
    if self.m_NPCChatStatus == EnumNPCChatStatus.WaitForNPC then 
        return 
    else
        self:ResetData()
        self.m_NPCChatStatus = EnumNPCChatStatus.WaitForNPC
        local Interval = 400
        local totalDotNum = 4
        local DotChar = "."
		local beforeText = g_MessageMgr:FormatMessage("NPCChat_WaitNPCInput")
		local tempNum = 0
		self.m_ShowNPCInputTick = RegisterTick(function()
			tempNum = (tempNum + 1) % totalDotNum
			local res = string.rep(DotChar,tempNum)
			self.TipLabel.text = beforeText..res
		end,Interval)
        self.NPCWaitPlayerPanel:SetActive(true)
        self.ChoosePanel:SetActive(false)
    end
end

function LuaNPCChatPanel:ShowWaitForPlayerFinishEvent(EventID,senderName)
    self:ResetData()
    self.m_NPCChatStatus = EnumNPCChatStatus.WaitForFinishEvent
    local Event = NpcChat_ChatEvent.GetData(EventID)
    if not Event then return end
    local type = Event.Type
    if type == EnumNPCChatEventType.RedPackage then
        self.TipLabel.text = g_MessageMgr:FormatMessage("NPCChat_WaitPlayer_RedPackage",senderName)
    elseif type == EnumNPCChatEventType.Gift then
        self.TipLabel.text = g_MessageMgr:FormatMessage("NPCChat_WaitPlayer_Gift",senderName)
    elseif type == EnumNPCChatEventType.Task then
        self.TipLabel.text = g_MessageMgr:FormatMessage("NPCChat_WaitPlayer_Task",senderName)
    end
    self.NPCWaitPlayerPanel:SetActive(true)
    self.ChoosePanel:SetActive(false)
end

function LuaNPCChatPanel:ShowOptions(MsgID,OppositeID)
    self:ResetData()
    self.m_OppositeId = OppositeID
    self.m_MsgID = MsgID
    self.m_NPCChatStatus = EnumNPCChatStatus.WaitForPlayer
    local data = NpcChat_ChatMsg.GetData(MsgID)
    if not data then return end
    local Content = data.Content
    self.m_OptionsList = {}
    self.m_OptionsGoList = {}

    local functionName,parameters = string.match(data.Content,"(%w+)%(([^%s]-)%)")
    local options = g_LuaUtil:StrSplitAdv(parameters,",")

    for i=1,#options do
        local opid = tonumber(options[i])
        local opdata = NpcChat_ChatOption.GetData(opid)
        if opdata then
            table.insert(self.m_OptionsList,opdata) 
        end
    end

    local opcount = #self.m_OptionsList
    if opcount == 0 then return end

    self.Input.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("请从下方选择回复内容")
    for i=self.Grid.transform.childCount - 1,0, - 1 do
        if i ~= 0 then 
            local CellObj = self.Grid.transform:GetChild(i)
            CellObj.gameObject:SetActive(false)
            CellObj.transform:SetParent(self.Pool.transform)
        end
    end
    local HasAddCell = false
    for i=1,4 do
        if i <= opcount then  -- 选项
            local CellObj = nil
            if i == 1 then 
                CellObj = self.Grid.transform:GetChild(0)
            else
                CellObj = self:AddOptionCell("NPCChatChooseItem")
            end
            if not CellObj then return end
            local Label = CellObj.transform:Find("Label"):GetComponent(typeof(UILabel))
            Label.text = "[b2e7f4]"..self.m_OptionsList[i].Title.."[-]"
            local SelectSprite = CellObj.transform:Find("SelectBtn").gameObject
            SelectSprite:SetActive(false)
            UIEventListener.Get(CellObj.gameObject).onClick = DelegateFactory.VoidDelegate(function()
                self:OnSelectOptions(i)
            end)
            self.m_OptionsGoList[i] = CellObj
        else    -- 空白选项(最多只添加一个)
            if i==2 or (i == 4 and opcount > 2) then
                self:AddOptionCell("NPCChatChooseEmpty")
                break
            end
        end
    end
    self.Grid:GetComponent(typeof(UIGrid)):Reposition()
    self.Sprite:ResetAndUpdateAnchors()
    self.ChatInput:ResetAndUpdateAnchors()
    self.NPCWaitPlayerPanel:SetActive(false)
    self.ChoosePanel:SetActive(true)

    self.OptionFx:LoadFx("fx/ui/prefab/UI_NPC_liaotian_saoguang.prefab")
    local trans = self.OptionFx.transform
    local scale = trans.localScale 
    local pos = trans.localPosition
    if opcount <= 2 then
        scale.y = 0.5
        pos.y = -45
    else
        scale.y = 1
        pos.y = -90
    end
    trans.localScale = scale
    trans.localPosition = pos
end

function LuaNPCChatPanel:AddOptionCell(CellName)
    for i=0,self.Pool.transform.childCount - 1 do
        local ChildGo = self.Pool.transform:GetChild(i)
        if string.find(ChildGo.name,CellName) then
            ChildGo.transform:SetParent(self.Grid.transform)
            ChildGo.gameObject:SetActive(true)
            return ChildGo
        end
    end
    if CellName == "NPCChatChooseEmpty" then
        local ChildGo = CUICommonDef.AddChild(self.Grid,self.NPCChatChooseEmpty)
        ChildGo.gameObject:SetActive(true)
        return ChildGo
    elseif CellName == "NPCChatChooseItem" then
        local ChildGo = CUICommonDef.AddChild(self.Grid,self.NPCChatChooseItem)
        ChildGo.gameObject:SetActive(true)
        return ChildGo
    end
    return nil
end

function LuaNPCChatPanel:OnSelectOptions(index)
    self.OptionFx:DestroyFx()
    if self.m_ChooseIndex ~= index then
        if self.m_ChooseIndex ~= 0 then
            local beforeCell = self.m_OptionsGoList[self.m_ChooseIndex]
            beforeCell.transform:Find("SelectBtn").gameObject:SetActive(false)
            beforeCell.transform:Find("Label"):GetComponent(typeof(UILabel)).text = "[b2e7f4]"..self.m_OptionsList[self.m_ChooseIndex].Title.."[-]"
        end
        local SelectedCell = self.m_OptionsGoList[index]
        SelectedCell.transform:Find("SelectBtn").gameObject:SetActive(true)
        SelectedCell.transform:Find("Label"):GetComponent(typeof(UILabel)).text = "[1e2b62]"..self.m_OptionsList[index].Title.."[-]"
        self.Input.transform:Find("Label"):GetComponent(typeof(UILabel)).text = self.m_OptionsList[index].Content
    end
    self.m_ChooseIndex = index
end
function LuaNPCChatPanel:OnSendPlayerOption()
    if self.m_ChooseIndex == 0 then
        self:ShowOptionMsg()
    else
        local TextMsg = self.m_OptionsList[self.m_ChooseIndex].Content
        local nextid = self.m_OptionsList[self.m_ChooseIndex].MsgID
        local IsFirstOrEnd = nextid == 0 and EnumToInt(EnumNPCChatMsgLocation.End) or EnumToInt(EnumNPCChatMsgLocation.Default)
        local msg = SafeStringFormat3(LuaNPCChatMgr.MsgFormat,nextid,EnumToInt(EnumNPCChatType.Text),TextMsg,IsFirstOrEnd)
        local MainPlayer = CClientMainPlayer.Inst
        if MainPlayer then
            CIMMgr.Inst:RecvMsg(self.m_OppositeId, MainPlayer.Id, MainPlayer.EngineId, MainPlayer.Name, math.floor(EnumToInt(MainPlayer.Class) or 0), math.floor(EnumToInt(MainPlayer.Gender) or 0), msg,  CServerTimeMgr.Inst.timeStamp)
            LuaNPCChatMgr:OnOptionsFinished(self.m_OppositeId,self.m_MsgID,nextid)
        end
    end
end

function LuaNPCChatPanel:ShowOptionMsg()
    g_MessageMgr:ShowMessage("CUSTOM_STRING2",LocalString.GetString("请选择一条回复并发送"))
end

function LuaNPCChatPanel:ResetData()
    if self.m_ShowNPCInputTick ~= nil then
        UnRegisterTick(self.m_ShowNPCInputTick)
        self.m_ShowNPCInputTick = nil
    end
    self.TipLabel.text = ""
    self.m_ChooseIndex = 0
    self.m_NPCChatStatus = EnumNPCChatStatus.FreeChat
    self.m_OptionsList = nil
    self.m_OptionsGoList = nil
end

function LuaNPCChatPanel:OnDisable()
    self:ResetData()
    self.m_NPCChatStatus = EnumNPCChatStatus.FreeChat
end
--@region UIEvent

--@endregion UIEvent

