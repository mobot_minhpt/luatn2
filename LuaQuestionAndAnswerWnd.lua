local UILabel = import "UILabel"
local UIScrollView = import "UIScrollView"
local UITable = import "UITable"

local GameObject = import "UnityEngine.GameObject"

LuaQuestionAndAnswerWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaQuestionAndAnswerWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaQuestionAndAnswerWnd, "ScrollView", "ScrollView", UIScrollView)
RegistChildComponent(LuaQuestionAndAnswerWnd, "QuestionLabel", "QuestionLabel", UILabel)
RegistChildComponent(LuaQuestionAndAnswerWnd, "Table", "Table", UITable)
RegistChildComponent(LuaQuestionAndAnswerWnd, "Choice", "Choice", GameObject)
RegistChildComponent(LuaQuestionAndAnswerWnd, "CloseButton", "CloseButton", GameObject)

--@endregion RegistChildComponent end

function LuaQuestionAndAnswerWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaQuestionAndAnswerWnd:Init()
    local question = Question_Task.GetData(LuaChristmas2021Mgr.m_QuestionId)
    if not question then 
        return
    end
    self.TitleLabel.text = LuaChristmas2021Mgr.m_QAWndTitle
    self.QuestionLabel.text = question.Question

    self.m_ChoiceTextList = {}
    table.insert(self.m_ChoiceTextList,question.Right)
    table.insert(self.m_ChoiceTextList,question.Wrong1)
    table.insert(self.m_ChoiceTextList,question.Wrong2)
    table.insert(self.m_ChoiceTextList,question.Wrong3)

    while #self.m_ChoiceTextList > 0 do
        local i = UnityEngine_Random(1,#self.m_ChoiceTextList+1)
        local instance = NGUITools.AddChild(self.Table.gameObject,self.Choice)
        instance:SetActive(true)
        instance.transform:Find("Label"):GetComponent(typeof(UILabel)).text = self.m_ChoiceTextList[i]
        if question.Right == self.m_ChoiceTextList[i] then
            self.m_RightInstance = instance
            UIEventListener.Get(instance).onClick = LuaChristmas2021Mgr.m_CorrectDelegate
        else
            UIEventListener.Get(instance).onClick = LuaChristmas2021Mgr.m_MistakeDelegate
        end
        table.remove(self.m_ChoiceTextList,i)
    end
    self.Table:Reposition()

    UIEventListener.Get(self.m_RightInstance).onClick = LuaChristmas2021Mgr.m_CorrectDelegate
end

--@region UIEvent

--@endregion UIEvent

