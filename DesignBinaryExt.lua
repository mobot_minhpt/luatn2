local DelegateFactory = import "DelegateFactory"
local CBinaryDesignData_CommonReader2=import "BinaryDesignData.CBinaryDesignData_CommonReader2"
local CBinaryDesignDataReader2 = import "BinaryDesignData.CBinaryDesignDataReader2"

function create_sheet_class(sheetName)
	local reader = CBinaryDesignDataReader2.Inst:GetReaderByName(sheetName)
	local newClass = {}

	local mt_class_index={}
	mt_class_index.GetData = function(key)
		local t = reader:GetData(key, true)
		return t
	end

	mt_class_index.GetDataCount = function() return reader:GetDataCount() end
	mt_class_index.Exists = function(key) return reader:Exists(key) end
	mt_class_index.ForeachKey = function(foo)
		reader:ForeachKey(DelegateFactory.Action_object(function(key) foo(key) end))
	end
	mt_class_index.Foreach = function(foo)
		reader:Foreach(DelegateFactory.Action_object_object(function(key,t) foo(key,t) end), true)
	end
	mt_class_index.PatchField = function(key,fieldName,fieldValue) reader:PatchField(key,fieldName,fieldValue) end
	mt_class_index.PatchData = function( key,val) reader:PatchData(key, val) end
	mt_class_index.GetDataBySubKey = function (subKeyName, subKey)
		local subKeyReader = CBinaryDesignDataReader2.Inst:GetReaderByName("__"..sheetName.."_SubIndex_"..subKeyName)
		if (subKeyReader) then
			local data = subKeyReader:GetData(subKey,true)
			if data then
				local id = data.KeyValue
				local t = reader:GetData(id,true)
				return t
			end
		end
		return nil
	end
	mt_class_index.GetKey = function(key)
		if string.find(sheetName,"_SubIndex_") then
			local t = reader:GetData(key, true)
			if t then return t.KeyValue end
		end
		return 0
	end

	setmetatable(newClass, { __index = mt_class_index })
	return newClass
end

function create_setting_class(sheetName)
	local reader = CBinaryDesignDataReader2.Inst:GetReaderByName(sheetName)
	local newClass = {}
	local mt_class_index={}
	mt_class_index.GetData = function() 
		return reader:GetData(true) 
	end
	mt_class_index.PatchField = function(fieldName,fieldValue) reader:PatchField(fieldName,fieldValue) end
	setmetatable(newClass, { __index = mt_class_index })
	return newClass
end

function ext_create_sheet_class(sheetName)
	local reader = CBinaryDesignDataReader2.Inst:GetReaderByName(sheetName)
	local template_reader = CBinaryDesignDataReader2.Inst:GetReaderByName("__"..sheetName.."_Template")
	local newClass = {}
	local common_designbinary_mt = {
		__index = function (t, k)
			local _CommonData_=rawget(t,"_CommonData_")
			if k=="Display" then
				return CBinaryDesignData_CommonReader2.FormatExtField(rawget(_CommonData_,k), t.__Display__Args,false)
			elseif k=="Display_jian" then
				return CBinaryDesignData_CommonReader2.FormatExtField(rawget(_CommonData_,k), t.__Display_jian__Args,false)
			elseif k=="Display_Diff" then
				return CBinaryDesignData_CommonReader2.FormatExtField(rawget(_CommonData_,k), t.__Display__Args,true)
			elseif k=="WordDescription" then
				return CBinaryDesignData_CommonReader2.FormatExtField(rawget(_CommonData_,k), t.__Description__Args,false)
			elseif k=="Description" then
				return CBinaryDesignData_CommonReader2.FormatExtField(rawget(_CommonData_,k), t.__Description__Args,false)
			else
				local v = rawget(t,k)
				if v then return v end
				return rawget(_CommonData_,k)
			end
			return nil
		end
	}

	local mt_class_index={}
	mt_class_index.GetData = function(key)
		local t = reader:GetData(key, true)
		if t then
			t._CommonData_ = template_reader:GetData(math.floor(key / 100),true)
			setmetatable(t, common_designbinary_mt)
		end
		return t
	end

	mt_class_index.GetDataCount = function() return reader:GetDataCount() end
	mt_class_index.Exists = function(key) return reader:Exists(key) end
	mt_class_index.ForeachKey = function(foo)
		reader:ForeachKey(DelegateFactory.Action_object(function(key) foo(key) end))
	end
	mt_class_index.Foreach = function(foo)
		reader:Foreach(DelegateFactory.Action_object_object(function(key,t) 
			if not getmetatable(t) then
				setmetatable(t, common_designbinary_mt)
			end
			t._CommonData_ = template_reader:GetData(math.floor(key / 100),true)
			foo(key,t) 
		end), true)
	end
	mt_class_index.PatchField = function(key,fieldName,fieldValue) reader:PatchField(key,fieldName,fieldValue) end
	mt_class_index.PatchData = function( key,val) reader:PatchData(key, val) end

	setmetatable(newClass, { __index = mt_class_index })
	return newClass
end
function ext_create_sheet_class_with_lazyfields(sheetName)
	local reader = CBinaryDesignDataReader2.Inst:GetReaderByName(sheetName)
	local lazy_reader = CBinaryDesignDataReader2.Inst:GetReaderByName(sheetName.."_Lazy")

	local template_reader = CBinaryDesignDataReader2.Inst:GetReaderByName("__"..sheetName.."_Template")
	local newClass = {}

	local lazy_fieldnames = {}
	local meta = CBinaryDesignDataReader2.Inst:GetMeta(sheetName.."_Lazy")
	for i=1,meta.m_FieldMetas.Length-1 do--key不考虑
		local fieldMeta = meta.m_FieldMetas[i]
		lazy_fieldnames[fieldMeta.Name] = true
	end


	local common_designbinary_mt = {
		__index = function (t, k)
			if lazy_fieldnames[k] then
				local lazy_data = rawget(t, "__lazy_data")
				if not lazy_data then
					lazy_data = lazy_reader:GetData(rawget(t,"__key"),true)
					rawset(t,"__lazy_data",lazy_data)
				end
				return lazy_data[k]
			end
			-- 扩展的字段不能加lazy标记
			local _CommonData_=rawget(t,"_CommonData_")
			if k=="Display" then
				return CBinaryDesignData_CommonReader2.FormatExtField(rawget(_CommonData_,k), t.__Display__Args,false)
			elseif k=="Display_jian" then
				return CBinaryDesignData_CommonReader2.FormatExtField(rawget(_CommonData_,k), t.__Display_jian__Args,false)
			elseif k=="Display_Diff" then
				return CBinaryDesignData_CommonReader2.FormatExtField(rawget(_CommonData_,k), t.__Display__Args,true)
			elseif k=="WordDescription" then
				return CBinaryDesignData_CommonReader2.FormatExtField(rawget(_CommonData_,k), t.__Description__Args,false)
			elseif k=="Description" then
				return CBinaryDesignData_CommonReader2.FormatExtField(rawget(_CommonData_,k), t.__Description__Args,false)
			else
				local v = rawget(t,k)
				if v then return v end
				return rawget(_CommonData_,k)
			end
			return nil
		end
	}

	local mt_class_index={}
	mt_class_index.GetData = function(key)
		local t = reader:GetData(key, true)
		if t then
			rawset(t,"__key",key)
			t._CommonData_ = template_reader:GetData(math.floor(key / 100),true)
			setmetatable(t, common_designbinary_mt)
		end
		return t
	end

	mt_class_index.GetDataCount = function() return reader:GetDataCount() end
	mt_class_index.Exists = function(key) return reader:Exists(key) end
	mt_class_index.ForeachKey = function(foo)
		reader:ForeachKey(DelegateFactory.Action_object(function(key) foo(key) end))
	end
	mt_class_index.Foreach = function(foo)
		reader:Foreach(DelegateFactory.Action_object_object(function(key,t) 
			if not getmetatable(t) then
				setmetatable(t, common_designbinary_mt)
			end
			rawset(t,"__key",key)
			rawset(t,"__lazy_data",nil)
			foo(key,t) 
		end), true)
	end
	mt_class_index.PatchField = function(key,fieldName,fieldValue) reader:PatchField(key,fieldName,fieldValue) end
	mt_class_index.PatchData = function( key,val) reader:PatchData(key, val) end

	setmetatable(newClass, { __index = mt_class_index })
	return newClass
end

function create_sheet_class_with_lazyfields(sheetName)
	local reader = CBinaryDesignDataReader2.Inst:GetReaderByName(sheetName)
	local lazy_reader = CBinaryDesignDataReader2.Inst:GetReaderByName(sheetName.."_Lazy")
	local newClass = {}

	local lazy_fieldnames = {}
	local meta = CBinaryDesignDataReader2.Inst:GetMeta(sheetName.."_Lazy")
	if meta then
		for i=1,meta.m_FieldMetas.Length-1 do--key不考虑
			local fieldMeta = meta.m_FieldMetas[i]
			lazy_fieldnames[fieldMeta.Name] = true
		end
	end

	local item_mt = {
		__index = function (t, k)
			if lazy_fieldnames[k] then
				local lazy_data = rawget(t, "__lazy_data")
				if not lazy_data then
					lazy_data = lazy_reader:GetData(rawget(t,"__key"),true)
					rawset(t,"__lazy_data",lazy_data)
				end
				return lazy_data[k]
			end
			return t[k]
		end
	}

	local mt_class_index={}
	mt_class_index.GetData = function(key)
		local t = reader:GetData(key, true)
		if t then
			rawset(t,"__key",key)
			setmetatable(t, item_mt)
		end
		return t
	end

	mt_class_index.GetDataCount = function() return reader:GetDataCount() end
	mt_class_index.Exists = function(key) return reader:Exists(key) end
	mt_class_index.ForeachKey = function(foo)
		reader:ForeachKey(DelegateFactory.Action_object(function(key) foo(key) end))
	end
	mt_class_index.Foreach = function(foo)
		reader:Foreach(DelegateFactory.Action_object_object(function(key,t) 
			if not getmetatable(t) then
				setmetatable(t, item_mt)
			end
			rawset(t,"__key",key)
			rawset(t,"__lazy_data",nil)
			foo(key,t) 
		end), true)
	end
	mt_class_index.PatchField = function(key,fieldName,fieldValue) reader:PatchField(key,fieldName,fieldValue) end
	mt_class_index.PatchData = function( key,val) reader:PatchData(key, val) end
	mt_class_index.GetDataBySubKey = function (subKeyName, subKey)
		local subKeyReader = CBinaryDesignDataReader2.Inst:GetReaderByName("__"..sheetName.."_SubIndex_"..subKeyName)
		if (subKeyReader) then
			local name = subKeyReader.m_Meta.m_FieldMetas[0].Name
			local subKeyInfo = subKeyReader:GetData(subKey,true)
			if subKeyInfo then
				local id = subKeyInfo.KeyValue
				local t = reader:GetData(id,true)
				return t
			end
		end
		return nil
	end

	setmetatable(newClass, { __index = mt_class_index })
	return newClass
end


function create_setting_class_with_lazyfields(sheetName)
	local reader = CBinaryDesignDataReader2.Inst:GetReaderByName(sheetName)
	local lazy_reader = CBinaryDesignDataReader2.Inst:GetReaderByName(sheetName.."_Lazy")
	local newClass = {}
	local mt_class_index={}

	local lazy_fieldnames = {}
	local meta = CBinaryDesignDataReader2.Inst:GetMeta(sheetName.."_Lazy")
	if meta then
		for i=1,meta.m_FieldMetas.Length do--key不考虑
			local fieldMeta = meta.m_FieldMetas[i-1]
			lazy_fieldnames[fieldMeta.Name] = true
		end
	end

	local item_mt = {
		__index = function (t, k)
			if lazy_fieldnames[k] then
				local lazy_data = rawget(t, "__lazy_data")
				if not lazy_data then
					lazy_data = lazy_reader:GetData(true)
					rawset(t,"__lazy_data",lazy_data)
				end
				return lazy_data[k]
			end
			return t[k]
		end
	}
	mt_class_index.GetData = function() 
		local t = reader:GetData(true) 
		if t then
			setmetatable(t, item_mt)
		end
		return t
	end
	mt_class_index.PatchField = function(fieldName,fieldValue) reader:PatchField(fieldName,fieldValue) end
	setmetatable(newClass, { __index = mt_class_index })
	return newClass
end

function GetExpressionDefineFx(expression_define,igender)
	local tb = {}
	if expression_define == nil then
		return tb
	end
	if expression_define.Fx then
		for i=0,expression_define.Fx.Length-1 do
			table.insert(tb,expression_define.Fx[i])
		end
	end
	if igender == EnumGender_lua.Male and expression_define.MaleAdditionalFx then
		for i=0,expression_define.MaleAdditionalFx.Length-1 do
			table.insert(tb,expression_define.MaleAdditionalFx[i])
		end
	end


	if igender == EnumGender_lua.Female and expression_define.FemaleAdditionalFx then
		for i=0,expression_define.FemaleAdditionalFx.Length-1 do
			table.insert(tb,expression_define.FemaleAdditionalFx[i])
		end
	end
	return tb
end