local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CMiddleNoticeMgr=import "L10.UI.CMiddleNoticeMgr"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local AlignType=import "L10.UI.CPopupMenuInfoMgr+AlignType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local MessageWndManager=import "L10.UI.MessageWndManager"
LuaGuoQingPvEWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGuoQingPvEWnd, "JoinButton", "JoinButton", GameObject)
RegistChildComponent(LuaGuoQingPvEWnd, "RankButton", "RankButton", GameObject)
RegistChildComponent(LuaGuoQingPvEWnd, "template_Text1", "template_Text1", GameObject)
RegistChildComponent(LuaGuoQingPvEWnd, "template_Text2", "template_Text2", GameObject)
RegistChildComponent(LuaGuoQingPvEWnd, "ChooseCount", "ChooseCount", UILabel)

--@endregion RegistChildComponent end


RegistClassMember(LuaGuoQingPvEWnd, "awarlimit")

function LuaGuoQingPvEWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    local setting = GuoQing2022_XiangYaoYuQiuSi.GetData()
    self.awarlimit = setting.AwardLimit
    local playtimes = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eXiangYaoYuQiuSi) or 0
    if playtimes > self.awarlimit then
        playtimes = self.awarlimit
    end
    self.ChooseCount.text = SafeStringFormat3("%s(%s/%s)",LocalString.GetString("今日已获得奖励次数"),tostring(playtimes),tostring(self.awarlimit))
    
    
    self.template_Text1.transform:Find("Text"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("XiangYaoYuQiuSi_Rule_Time")
    self.template_Text2.transform:Find("Text"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("XiangYaoYuQiuSi_Complete_Rule")
    

    UIEventListener.Get(self.RankButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRankButtonClick(go)
	end)
    UIEventListener.Get(self.JoinButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnJoinButtonClick(go)
	end)
end

function LuaGuoQingPvEWnd:Init()
    
end


--@region UIEvent

function LuaGuoQingPvEWnd:OnRankButtonClick(go)
    Gac2Gas.QueryXiangYaoYuQiuSiRank()
	
end



function LuaGuoQingPvEWnd:OnJoinButtonClick(go)

    local function Action(index)
        if index==0 then
            --普通难度
            local msg = g_MessageMgr:FormatMessage("XiangYaoYuQiuSi_Sign_Confirm_Easy")
		    MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
			    Gac2Gas.EnterXiangYaoYuQiuSiPlay(1)
		    end),nil,LocalString.GetString("确认"),LocalString.GetString("取消"),false)
            
            
        else
           --困难难度
           local msg = g_MessageMgr:FormatMessage("XiangYaoYuQiuSi_Sign_Confirm_Hard")
           MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
               Gac2Gas.EnterXiangYaoYuQiuSiPlay(2)
           end),nil,LocalString.GetString("确认"),LocalString.GetString("取消"),false)
        end
       
    end
    local selectItem=DelegateFactory.Action_int(Action)

    local t={}
    local item1=PopupMenuItemData(LocalString.GetString("普通难度"),selectItem,false,nil)
    table.insert(t, item1)
    local item2=PopupMenuItemData(LocalString.GetString("困难难度"),selectItem,false,nil)
    table.insert(t, item2)

    local array=Table2Array(t,MakeArrayClass(PopupMenuItemData))
    CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, AlignType.Top,1,nil,600,true,360)

end




--@endregion UIEvent

