local UILabel = import "UILabel"
local UITable = import "UITable"
local QnButton = import "L10.UI.QnButton"
local CButton = import "L10.UI.CButton"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local CYuanbaoMarketMgr = import "L10.UI.CYuanbaoMarketMgr"
local MessageMgr = import "L10.Game.MessageMgr"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local Extensions = import "Extensions"

CLuaDaoDanTipEnterWnd = class()

RegistChildComponent(CLuaDaoDanTipEnterWnd, "RuleTable", UITable)
RegistChildComponent(CLuaDaoDanTipEnterWnd, "ParagraphTemplate", GameObject)
RegistChildComponent(CLuaDaoDanTipEnterWnd, "ScaredCountLabel", UILabel)
RegistChildComponent(CLuaDaoDanTipEnterWnd, "CourageGradeLabel", UILabel)
RegistChildComponent(CLuaDaoDanTipEnterWnd, "GetScareItemBtn", QnButton)
RegistChildComponent(CLuaDaoDanTipEnterWnd, "GetCourageItemBtn", QnButton)
RegistChildComponent(CLuaDaoDanTipEnterWnd, "CommunityBtn", CButton)

RegistClassMember(CLuaDaoDanTipEnterWnd, "TerribleItemId")
RegistClassMember(CLuaDaoDanTipEnterWnd, "DanShiItemId")

function CLuaDaoDanTipEnterWnd:Awake()
    self.TerribleItemId = Halloween2020_MengGuiJie.GetData().TerribleItemId
    self.DanShiItemId = Halloween2020_MengGuiJie.GetData().DanShiItemId
end

function CLuaDaoDanTipEnterWnd:Init()
    Extensions.RemoveAllChildren(self.RuleTable.transform)
    local ruleMessage = MessageMgr.Inst:FormatMessage("HALLOWEEN2020_DAODAN_TIP", {})
    local ruleTipInfo = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(ruleMessage)
    if ruleTipInfo ~= nil then
        for i = 0, ruleTipInfo.paragraphs.Count - 1 do
            local rule = CUICommonDef.AddChild(self.RuleTable.gameObject, self.ParagraphTemplate):GetComponent(typeof(CTipParagraphItem))
            rule.gameObject:SetActive(true)
            rule:Init(ruleTipInfo.paragraphs[i], 0xffffffff)
        end
    end
    self.RuleTable:Reposition()

    self.GetScareItemBtn.OnClick = DelegateFactory.Action_QnButton(function ()
        CYuanbaoMarketMgr.TabViewIndex = 1
        CUIManager.ShowUI(CUIResources.YuanBaoMarketWnd)
    end)

    self.GetCourageItemBtn.OnClick = DelegateFactory.Action_QnButton(function ()
        CYuanbaoMarketMgr.ShowPlayerShop(self.DanShiItemId)
    end)

    CommonDefs.AddOnClickListener(self.CommunityBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
        CUIManager.ShowUI(CUIResources.CommunityWnd)
        CUIManager.CloseUI(CLuaUIResources.DaoDanTipEnterWnd)
    end), false)

    Gac2Gas.QueryHalloweenTerrifyAndDanShiInfo()
end

function CLuaDaoDanTipEnterWnd:OnEnable()
    g_ScriptEvent:AddListener("ReplyHalloweenTerrifyAndDanShiInfo", self, "OnReplyHalloweenTerrifyAndDanShiInfo")
end

function CLuaDaoDanTipEnterWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ReplyHalloweenTerrifyAndDanShiInfo",self, "OnReplyHalloweenTerrifyAndDanShiInfo")
end

function CLuaDaoDanTipEnterWnd:OnReplyHalloweenTerrifyAndDanShiInfo(terrifyCount, maxTerrifyCount, level, param1, param2)
    self.ScaredCountLabel.text = terrifyCount .. "/" .. maxTerrifyCount
    self.CourageGradeLabel.text = SafeStringFormat3(LocalString.GetString("%s级"), tostring(level)) .. SafeStringFormat3(LocalString.GetString("(经验 %d/%d)"), param1, param2)
end