local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local AlignType=import "L10.UI.CItemInfoMgr+AlignType"
local CItemChooseMgr=import "L10.UI.CItemChooseMgr"
local EnumItemPlace=import "L10.Game.EnumItemPlace"
local String = import "System.String"
local CUITexture=import "L10.UI.CUITexture"
local MessageWndManager = import "L10.UI.MessageWndManager"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local CItemMgr = import "L10.Game.CItemMgr"


CLuaWeddingRingTabenWnd = class()
RegistClassMember(CLuaWeddingRingTabenWnd,"instruction")
RegistClassMember(CLuaWeddingRingTabenWnd,"okButton")
RegistClassMember(CLuaWeddingRingTabenWnd,"cancelButton")
RegistClassMember(CLuaWeddingRingTabenWnd,"weddingring")
RegistClassMember(CLuaWeddingRingTabenWnd,"equip")
RegistClassMember(CLuaWeddingRingTabenWnd,"m_TargetId")

CLuaWeddingRingTabenWnd.weddingRingId = nil

function CLuaWeddingRingTabenWnd:Awake()
    self.m_TargetId = nil
    self.instruction = self.transform:Find("Anchor/Instruction"):GetComponent(typeof(UILabel))
    self.okButton = self.transform:Find("Anchor/OKButton").gameObject
    self.cancelButton = self.transform:Find("Anchor/CancelButton").gameObject

    UIEventListener.Get(self.okButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnOkButtonClick(go)
    end)
    UIEventListener.Get(self.cancelButton).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.CloseUI(CUIResources.WeddingRingTabenWnd)
    end)
end

function CLuaWeddingRingTabenWnd:Init()
    self:InitTabenInfo(CLuaWeddingRingTabenWnd.weddingRingId)
    self:InitTargetInfo(nil)
end

function CLuaWeddingRingTabenWnd:OnOkButtonClick( go) 
    if self.m_TargetId == nil then
        g_MessageMgr:ShowMessage("Taben_Equip_Material_Not_Exist")
    else
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("WEDDING_RING_TABEN_CONFIRM"), 
            DelegateFactory.Action(function() self:DoTaben() end),nil, nil, nil, false)
    end
end
function CLuaWeddingRingTabenWnd:DoTaben( )
    local s = CItemMgr.Inst:GetItemInfo(CLuaWeddingRingTabenWnd.weddingRingId)
    local t = CItemMgr.Inst:GetItemInfo(self.m_TargetId)
    Gac2Gas.RequestTaBenWeddingRing(s.itemId, EnumToInt(s.place), s.pos, t.itemId, EnumToInt(t.place), t.pos)
end

function CLuaWeddingRingTabenWnd:InitTabenInfo(itemId)
    local transform = FindChild(self.transform,"WeddingRing")
    local icon = transform:Find("Texture"):GetComponent(typeof(CUITexture))
    local quality = transform:Find("quality"):GetComponent(typeof(UISprite))
    local bind = transform:Find("Bind"):GetComponent(typeof(UISprite))
    local name = transform:Find("Name"):GetComponent(typeof(UILabel))



    local ring = CItemMgr.Inst:GetById(itemId)

    icon:LoadMaterial(ring.Icon)
    quality.spriteName = CUICommonDef.GetItemCellBorder(ring.Equip.QualityType)
    bind.spriteName = ring.BindOrEquipCornerMark
    name.text = ring.ColoredName
    

    UIEventListener.Get(transform.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        CItemInfoMgr.ShowLinkItemInfo(ring, false, nil, AlignType.Default, 0, 0, 0, 0, 0) 
    end)
end

function CLuaWeddingRingTabenWnd:InitTargetInfo(itemId)
    local transform = FindChild(self.transform,"AimRing")
    local icon = transform:Find("Texture"):GetComponent(typeof(CUITexture))
    local quality = transform:Find("quality"):GetComponent(typeof(UISprite))
    local bind = transform:Find("Bind"):GetComponent(typeof(UISprite))
    local add = transform:Find("Add").gameObject
    local name = transform:Find("Name"):GetComponent(typeof(UILabel))

    UIEventListener.Get(transform.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OpenEquipSelectWnd()
    end)

    if not itemId then
        icon.material = nil
        add:SetActive(true)
        quality.spriteName = CUICommonDef.GetItemCellBorder()
        bind.spriteName = ""
        name.text = LocalString.GetString("[c][acf8ff]添加戒指")


        return
    end

    local ring = CItemMgr.Inst:GetById(itemId)
    add:SetActive(false)

    icon:LoadMaterial(ring.Icon)
    quality.spriteName = CUICommonDef.GetItemCellBorder(ring.Equip.QualityType)
    bind.spriteName = ring.BindOrEquipCornerMark
    name.text = ring.ColoredName
end

function CLuaWeddingRingTabenWnd:OpenEquipSelectWnd( )
    local equipList = CreateFromClass(MakeGenericClass(List, String))

    local bagSize = CClientMainPlayer.Inst.ItemProp:GetPlaceSize(EnumItemPlace.Bag)
    do
        local i = 1
        while i <= bagSize do
            local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
            if not (id==nil or id=="") then
                local equip = CItemMgr.Inst:GetById(id)
                local templateId = equip.TemplateId
                if equip ~= nil and equip.IsBinded and equip.IsEquip and not CLuaWeddingMgr.IsWeddingRing(templateId) then
                    local template = EquipmentTemplate_Equip.GetData(templateId)
                    if template.Type == EnumBodyPosition_lua.Ring then
                        CommonDefs.ListAdd(equipList, typeof(String), equip.Id)
                    end
                end
            end
            i = i + 1
        end
    end
    if equipList.Count > 0 then
        CItemChooseMgr.Inst.ItemIds=equipList
        CItemChooseMgr.Inst.OnChoose=DelegateFactory.Action_string(function(itemId)
            self.m_TargetId = itemId
            local ring = CItemMgr.Inst:GetById(itemId)
            if not (ring and ring.IsEquip) then return end
        
            local equipment = EquipmentTemplate_Equip.GetData(ring.TemplateId)
            if not (equipment ~= nil and equipment.Type == EnumBodyPosition_lua.Ring and not CLuaWeddingMgr.IsWeddingRing(ring.TemplateId)) then
                return
            end

            self:InitTargetInfo(itemId)
        end)
        CItemChooseMgr.ShowWnd(LocalString.GetString("选择要制作拓本的戒指"))
    else
        g_MessageMgr:ShowMessage("JIEHUN_TABEN_HAVE_NO_RING")
    end
end

