LuaGuideEquipTip = class()

function LuaGuideEquipTip:OnEnable()
    --#205820 【引导】装备强化引导的时候关闭对话框
    CUIManager.SetUIVisibility(CUIResources.TaskDialogWnd, false, "equiptip_guide")
end
function LuaGuideEquipTip:OnDisable()
    CUIManager.SetUIVisibility(CUIResources.TaskDialogWnd, true, "equiptip_guide")
end