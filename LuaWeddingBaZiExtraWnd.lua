local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"

LuaWeddingBaZiExtraWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaWeddingBaZiExtraWnd, "descLabel")
RegistClassMember(LuaWeddingBaZiExtraWnd, "scrollView")

function LuaWeddingBaZiExtraWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
end

-- 初始化UI组件
function LuaWeddingBaZiExtraWnd:InitUIComponents()
    self.scrollView = self.transform:Find("Anchor"):Find("ScrollView"):GetComponent(typeof(CUIRestrictScrollView))
    self.descLabel = self.scrollView.transform:Find("Label"):GetComponent(typeof(UILabel))
end


function LuaWeddingBaZiExtraWnd:Init()
    self.descLabel.text = LuaWeddingIterationMgr.baZiStr
    self.scrollView:ResetPosition()
end

--@region UIEvent

--@endregion UIEvent

