local UILabel = import "UILabel"
local CUIFx = import "L10.UI.CUIFx"
local UIProgressBar = import "UIProgressBar"
local Ease = import "DG.Tweening.Ease"
local Animation = import "UnityEngine.Animation"

LuaZhuoYaoHuaFuWndProgress = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZhuoYaoHuaFuWndProgress, "ProgressLabel", "ProgressLabel", UILabel)
RegistChildComponent(LuaZhuoYaoHuaFuWndProgress, "LevelLabel", "LevelLabel", UILabel)
RegistChildComponent(LuaZhuoYaoHuaFuWndProgress, "ExpLabel", "ExpLabel", UILabel)
RegistChildComponent(LuaZhuoYaoHuaFuWndProgress, "ThumbFx", "ThumbFx", CUIFx)
RegistChildComponent(LuaZhuoYaoHuaFuWndProgress, "ProgressBar", "ProgressBar", UIProgressBar)
--@endregion RegistChildComponent end
RegistClassMember(LuaZhuoYaoHuaFuWndProgress, "m_ProgressBar")
RegistClassMember(LuaZhuoYaoHuaFuWndProgress, "m_Animation")

function LuaZhuoYaoHuaFuWndProgress:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaZhuoYaoHuaFuWndProgress:OnEnable()
    self.m_Animation = self.gameObject:GetComponent(typeof(Animation))
    self.m_ProgressBar = self.gameObject:GetComponent(typeof(UIProgressBar))
    local level = LuaZhuoYaoMgr.m_CatchYaoGuaiResultInfo.level
    local currentExp = LuaZhuoYaoMgr.m_CatchYaoGuaiResultInfo.currentExp
    local lastlevel = LuaZhuoYaoMgr.m_CatchYaoGuaiResultInfo.lastlevel
    local lastexp = LuaZhuoYaoMgr.m_CatchYaoGuaiResultInfo.lastexp
    local addExp = LuaZhuoYaoMgr.m_CatchYaoGuaiResultInfo.addExp
    self.ExpLabel.text = SafeStringFormat3("+%d",addExp)
    self.ExpLabel.depth = 30
    self:InitProgress(lastexp,lastlevel)
    if currentExp then
        self:InitProgressFx(lastexp,lastlevel,currentExp, level)
    end
end

function LuaZhuoYaoHuaFuWndProgress:InitProgress(exp, level)
    local expData = ZhuoYao_Exp.GetData(level)
    local fullExp = expData and expData.ExpFull or 0
    self.LevelLabel.text = SafeStringFormat3(LocalString.GetString("Lv.%d"),level)
    self.ProgressLabel.text = SafeStringFormat3("%d/%d",exp, fullExp)
    self.ProgressBar.value = exp / fullExp
    if level == 10 and fullExp == exp then
        self.ExpLabel.text = LocalString.GetString("满级")
    end
end

function LuaZhuoYaoHuaFuWndProgress:InitProgressFx(lastexp, lastlevel, currentExp, curlevel)
    LuaTweenUtils.DOKill(self.transform, false)
    if curlevel == lastlevel then
        local tween = LuaTweenUtils.TweenFloat(lastexp, currentExp, 2, function ( val )
            self:InitProgress(val,curlevel)
        end,function () 
            self.m_Animation:Play("common_jindu")
        end,Ease.OutSine)
        LuaTweenUtils.SetTarget(tween, self.transform)
    else
        local expData = ZhuoYao_Exp.GetData(lastlevel)
        local fullExp = expData and expData.ExpFull or 0
        local tween1 = LuaTweenUtils.TweenFloat(lastexp, fullExp , 1, function ( val )
            self:InitProgress(val, lastlevel)
        end,function () 
            self.m_Animation:Play("common_jinduzou")
            local tween2 = LuaTweenUtils.TweenFloat(0, currentExp , 1, function ( val )
                self:InitProgress(val, curlevel)
            end,function () 
                self.m_Animation:Play("common_jindu")
            end,Ease.OutSine)
            LuaTweenUtils.SetTarget(tween2, self.transform)
        end,Ease.OutSine)
        LuaTweenUtils.SetTarget(tween1, self.transform)
    end
end

function LuaZhuoYaoHuaFuWndProgress:OnDisable()
    LuaTweenUtils.DOKill(self.transform, false)
end

--@region UIEvent

--@endregion UIEvent

