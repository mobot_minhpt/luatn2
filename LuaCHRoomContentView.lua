local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local CPopupMenuInfoMgrAlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local CPlayerInfoMgrAlignType = import "CPlayerInfoMgr+AlignType"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local Vector3 = import "UnityEngine.Vector3"
local Object = import "System.Object"
local UIPanel = import "UIPanel"
local CButton = import "L10.UI.CButton"
local NGUIText = import "NGUIText"
local UIWidget = import "UIWidget"
local CCMiniAPIMgr=import "L10.Game.CCMiniAPIMgr"
local EnumCCMiniStreamType = import "L10.Game.EnumCCMiniStreamType"
local CCCChatMgr=import "L10.Game.CCCChatMgr"
local CChatInput = import "L10.UI.CChatInput"
local CChatHistoryMgr = import "L10.Game.CChatHistoryMgr"
local CChatHistoryMenu = import "L10.UI.CChatHistoryMenu"
local Ease = import "DG.Tweening.Ease"
local DefaultChatInputListener = import "L10.UI.DefaultChatInputListener"
local CChatInputMgrEParentType = import "L10.UI.CChatInputMgr+EParentType"
local CChatInputMgr = import "L10.UI.CChatInputMgr"
local UISimpleTableView = import "L10.UI.UISimpleTableView"
local DefaultUISimpleTableViewDataSource = import "L10.UI.DefaultUISimpleTableViewDataSource"
local Extensions = import "Extensions"
local CChatLinkMgr = import "CChatLinkMgr"
local UICamera = import "UICamera"
local Regex = import "System.Text.RegularExpressions.Regex"
local RegexOptions = import "System.Text.RegularExpressions.RegexOptions"
local CUIFx = import "L10.UI.CUIFx"
local CChildWnd = import "L10.UI.CChildWnd"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local Animation = import "UnityEngine.Animation"
local BetterList = import "BetterList`1"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"

LuaCHRoomContentView = class()

RegistClassMember(LuaCHRoomContentView, "m_BackButton")
RegistClassMember(LuaCHRoomContentView, "m_ExitButton")
RegistClassMember(LuaCHRoomContentView, "m_RoomIdLabel")
RegistClassMember(LuaCHRoomContentView, "m_InfoButton")
RegistClassMember(LuaCHRoomContentView, "m_ShareButton")

RegistClassMember(LuaCHRoomContentView, "m_ContentScrollView")
RegistClassMember(LuaCHRoomContentView, "m_ContentTop")
RegistClassMember(LuaCHRoomContentView, "m_RoomNamePanel")
RegistClassMember(LuaCHRoomContentView, "m_BroadcasterGrid")
RegistClassMember(LuaCHRoomContentView, "m_BroadcasterTemplate")
RegistClassMember(LuaCHRoomContentView, "m_AudienceButton")
RegistClassMember(LuaCHRoomContentView, "m_AudienceNumLabel")
RegistClassMember(LuaCHRoomContentView, "m_TemplatePoolRoot")

RegistClassMember(LuaCHRoomContentView, "m_LeaveButton") --离开房间

RegistClassMember(LuaCHRoomContentView, "m_MicButtonTable")
RegistClassMember(LuaCHRoomContentView, "m_DealWithApplyButton") --处理上麦请求
RegistClassMember(LuaCHRoomContentView, "m_LeaveMicButton") -- 下麦
RegistClassMember(LuaCHRoomContentView, "m_MuteMicButton") -- 开/闭 麦
RegistClassMember(LuaCHRoomContentView, "m_ApplyMicButton") --申请/取消申请 上麦

RegistClassMember(LuaCHRoomContentView, "m_BroadcasterItemTbl") --存放主播GameObject对象的表，用于处理刷新和交互

--- 房间信息相关
RegistClassMember(LuaCHRoomContentView, "m_RoomBg")    -- 房间背景
RegistClassMember(LuaCHRoomContentView, "m_RoomTag")    -- 房间标签
RegistClassMember(LuaCHRoomContentView, "m_HeatValueLabel")    -- 房间热度值
RegistClassMember(LuaCHRoomContentView, "m_IsSpecialRoom")    -- 特殊房间
--- 送礼相关
RegistClassMember(LuaCHRoomContentView, "m_GivePresentTopTable") -- 房间送礼排行前三表
RegistClassMember(LuaCHRoomContentView, "m_GivePresentTopTemplate") -- 送礼前三模板
RegistClassMember(LuaCHRoomContentView, "m_GivePresentTopItemTbl") -- 存放送礼前三GameObject的表
RegistClassMember(LuaCHRoomContentView, "m_GivePresentButton") -- 送礼
RegistClassMember(LuaCHRoomContentView, "m_PresentPopTip") -- 送礼弹窗
RegistClassMember(LuaCHRoomContentView, "m_PresentPopTipShowTime") -- 送礼弹窗显示时长
RegistClassMember(LuaCHRoomContentView, "m_PresentEffectList") -- 礼物动效播放队列
RegistClassMember(LuaCHRoomContentView, "m_IsPresentEffectShow")  -- 礼物动效是否正在播放
RegistClassMember(LuaCHRoomContentView, "m_PresentEffectTick")  -- 礼物动效播放Tick
RegistClassMember(LuaCHRoomContentView, "m_PresentLongEffectCount")  -- 长动效个数限制
RegistClassMember(LuaCHRoomContentView, "m_PresentSound")  -- 礼物音效播放Tick
RegistClassMember(LuaCHRoomContentView, "m_PresentSoundTick")  -- 礼物音效播放Tick
--- 聊天相关
RegistClassMember(LuaCHRoomContentView, "m_ChatInput") -- 聊天输入
RegistClassMember(LuaCHRoomContentView, "m_HistoryButton") --聊天历史按钮
RegistClassMember(LuaCHRoomContentView, "m_EmoticonButton") --聊天表情按钮
RegistClassMember(LuaCHRoomContentView, "m_RoomChatBg") -- 聊天框背景
RegistClassMember(LuaCHRoomContentView, "m_RoomChatMsgTemplate") -- 聊天消息模板
RegistClassMember(LuaCHRoomContentView, "m_RoomGivePresentMsgTemplate") -- 赠送礼物消息模板
RegistClassMember(LuaCHRoomContentView, "m_RoomChatSimpleTableView")
RegistClassMember(LuaCHRoomContentView, "m_DefaultChatInputListener")
RegistClassMember(LuaCHRoomContentView, "m_DefaultSimpleTableViewDataSource")
RegistClassMember(LuaCHRoomContentView, "m_UnReadMsgCount")
RegistClassMember(LuaCHRoomContentView, "m_BroadcasterEmotionTime") -- 主播头像表情显示时长
RegistClassMember(LuaCHRoomContentView, "m_UpdateMsgTick") -- 更新聊天内容Tick

RegistClassMember(LuaCHRoomContentView, "m_MengDaoPortraitPicTbl") -- 梦岛头像Texture缓存

RegistChildComponent(LuaCHRoomContentView, "m_ChatHistoryMenu", "ChatHistoryMenu", CChatHistoryMenu) --聊天历史

function LuaCHRoomContentView:Awake()

    self.m_BackButton = self.transform:Find("Header/BackButton").gameObject
    self.m_ExitButton = self.transform:Find("Header/ExitButton").gameObject
    self.m_RoomIdLabel = self.transform:Find("Header/RoomIdLabel"):GetComponent(typeof(UILabel))
    self.m_InfoButton = self.transform:Find("Header/InfoButton").gameObject
    self.m_ShareButton = self.transform:Find("Header/ShareButton").gameObject
    self.m_ContentScrollView = self.transform:Find("Content/View"):GetComponent(typeof(UIScrollView))
    self.m_ContentTop = self.transform:Find("Content/View/Top").gameObject
    self.m_RoomNamePanel = self.transform:Find("Content/View/Top/RoomInfo/RoomNamePanel"):GetComponent(typeof(CCommonLuaScript))
    self.m_RoomNamePanelMaxWidth = self.m_RoomNamePanel:GetComponent(typeof(UIPanel)).width
    self.m_BroadcasterGrid = self.transform:Find("Content/View/Top/BroadcasterGrid"):GetComponent(typeof(UIGrid))
    self.m_BroadcasterTemplate = self.transform:Find("Content/View/Pool/BroadcasterTemplate").gameObject
    self.m_AudienceButton = self.transform:Find("Content/View/Pool/AudienceButton"):GetComponent(typeof(CButton))
    self.m_TemplatePoolRoot = self.transform:Find("Content/View/Pool").transform

    self.m_LeaveButton = self.transform:Find("Bottom/LeaveRoomButton").gameObject
    self.m_MicButtonTable = self.transform:Find("Bottom/Table"):GetComponent(typeof(UITable))
    self.m_DealWithApplyButton = self.transform:Find("Bottom/Table/DealWithApplyButton").gameObject
    self.m_LeaveMicButton = self.transform:Find("Bottom/Table/LeaveMicButton").gameObject
    self.m_MuteMicButton = self.transform:Find("Bottom/Table/MuteMicButton").gameObject
    self.m_ApplyMicButton = self.transform:Find("Bottom/Table/ApplyMicButton").gameObject
    
    self.m_BroadcasterTemplate:SetActive(false)

    self.m_RoomBg = self.transform:Find("Content/ContentBg"):GetComponent(typeof(CUITexture))
    self.m_RoomTag = self.transform:Find("Content/View/Top/RoomInfo/RoomTag"):GetComponent(typeof(UISprite))
    self.m_HeatValueLabel = self.transform:Find("Content/View/Top/RoomInfo/HeatValueLabel"):GetComponent(typeof(UILabel))
    self.m_HeatValueSprite = self.transform:Find("Content/View/Top/RoomInfo/HeatValueLabel/Sprite"):GetComponent(typeof(UITexture))
    self.m_BroadcasterGridBg = self.transform:Find("Content/View/BroadcasterGridBg"):GetComponent(typeof(CUITexture))
    self.m_BroadcasterGridBg_Normal = self.transform:Find("Content/View/BroadcasterGridBg_Normal"):GetComponent(typeof(UISprite))
    --- 送礼相关
    self.m_PresentLeftBg_Normal = self.transform:Find("Bottom/PresentLeftButtons/BroadcasterGridBg1").gameObject
    self.m_PresentLeftBg_Special = self.transform:Find("Bottom/PresentLeftButtons/BroadcasterGridBg2").gameObject
    self.m_GivePresentButton = self.transform:Find("Bottom/PresentLeftButtons/GivePresentButton").gameObject
    self.m_PresentPopTip = self.transform:Find("Bottom/PresentLeftButtons/PresentPopTipTemplate").gameObject
    self.m_PresentPopTipList = self.transform:Find("Bottom/PresentLeftButtons/PresentPopTipList").gameObject
    self.m_GivePresentTopTable = self.transform:Find("Bottom/PresentLeftButtons/GivePresentTopTable"):GetComponent(typeof(UITable))
    self.m_GivePresentTopTemplate = self.transform:Find("Bottom/PresentLeftButtons/GivePresentTopTemplate").gameObject
    self.m_GivePresentTopTableBg1 = self.transform:Find("Bottom/PresentLeftButtons/BroadcasterGridBg1"):GetComponent(typeof(UITexture))
    self.m_GivePresentTopTableBg2 = self.transform:Find("Bottom/PresentLeftButtons/BroadcasterGridBg2"):GetComponent(typeof(UITexture))
    self.m_GivePresentView = self.transform:Find("Content/PresentView"):GetComponent(typeof(UIPanel))
    self.m_GivePresentCUIFx = self.transform:Find("Content/PresentView/CUIFx"):GetComponent(typeof(CUIFx))
    self.m_GivePresentChildWnd = self.transform:Find("Content/PresentView/ChildWnd"):GetComponent(typeof(CChildWnd))
    self.m_GivePresentCUIFx.OnLoadFxFinish = DelegateFactory.Action(function ()
        self:WiatPlayPresentEffectFinish(self.m_GivePresentCUIFx)
    end)
    --- 聊天相关
    self.m_RoomChatSimpleTableView = self.transform:Find("Content/View/RoomChatScrollView"):GetComponent(typeof(UISimpleTableView))
    self.m_ChatInput = self.transform:Find("Bottom/ChatInput"):GetComponent(typeof(CChatInput))
    self.m_HistoryButton = self.transform:Find("Bottom/ChatInput/keyboard/HistoryButton").gameObject
    self.m_EmoticonButton = self.transform:Find("Bottom/ChatInput/keyboard/EmoticonButton").gameObject
    self.m_NewMsgButton = self.transform:Find("Content/Panel/NewMessageButton"):GetComponent(typeof(CButton))
    self.m_RoomChatMsgTemplate = self.transform:Find("Content/View/Pool/ChatMsgTemplate").gameObject
    self.m_RoomGivePresentMsgTemplate = self.transform:Find("Content/View/Pool/GivePresentMsgTemplate").gameObject
    self.m_ChatInput.OnSend = DelegateFactory.Action_string(function (msg)
		self:OnSendMsg(msg)
	end)
    self.m_ChatInputCallback = DelegateFactory.Action_string_Dictionary_int_CChatInputLink(function (text, existingLinks)
		self:OnHistorySelect(text, existingLinks)
	end)

    CommonDefs.AddOnClickListener(self.m_BackButton, DelegateFactory.Action_GameObject(function(go) self:OnBackButtonClick() end), false)
    CommonDefs.AddOnClickListener(self.m_InfoButton, DelegateFactory.Action_GameObject(function(go) self:OnInfoButtonClick() end), false)
    CommonDefs.AddOnClickListener(self.m_ShareButton, DelegateFactory.Action_GameObject(function(go) self:OnShareButtonClick() end), false)
    CommonDefs.AddOnClickListener(self.m_LeaveButton, DelegateFactory.Action_GameObject(function(go) self:OnLeaveRoomButtonClick() end), false)
    CommonDefs.AddOnClickListener(self.m_ExitButton, DelegateFactory.Action_GameObject(function(go) self:OnLeaveRoomButtonClick() end), false)
    CommonDefs.AddOnClickListener(self.m_DealWithApplyButton, DelegateFactory.Action_GameObject(function(go) self:OnDealWithApplyButtonClick() end), false)
    CommonDefs.AddOnClickListener(self.m_LeaveMicButton, DelegateFactory.Action_GameObject(function(go) self:OnLeaveMicButtonClick() end), false)
    CommonDefs.AddOnClickListener(self.m_MuteMicButton, DelegateFactory.Action_GameObject(function(go) self:OnMuteMicButtonClick() end), false)
    CommonDefs.AddOnClickListener(self.m_ApplyMicButton, DelegateFactory.Action_GameObject(function(go) self:OnApplyMicButtonClick() end), false)
    CommonDefs.AddOnClickListener(self.m_AudienceButton.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnAudienceButtonClick() end), false)
    CommonDefs.AddOnClickListener(self.m_GivePresentButton, DelegateFactory.Action_GameObject(function(go) self:OnGivePresentButtonClick() end), false)
    CommonDefs.AddOnClickListener(self.m_HistoryButton, DelegateFactory.Action_GameObject(function(go) self:OnHistoryButtonClick() end), false)
    CommonDefs.AddOnClickListener(self.m_EmoticonButton, DelegateFactory.Action_GameObject(function(go) self:OnEmoticonButtonClick() end), false)
    CommonDefs.AddOnClickListener(self.m_NewMsgButton.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnNewMsgButtonClick() end), false)
end

function LuaCHRoomContentView:Init()
    --这句有点hack 在首次调用Init的时候 ScrolllView可能还没执行到Anchor布局，强制执行一下
    self.m_ContentScrollView:GetComponent(typeof(UIPanel)):ResetAndUpdateAnchors()
    local pos = Extensions.GetTopPos(self.m_ContentScrollView)
    pos.y = pos.y + 20
    self.m_ContentTop.transform.localPosition = pos
    self:InitRoom()
    LuaClubHouseMgr:CheckAndConfirmCCRoom()
    self.m_PresentPopTipNewId = 1
    self.m_PresentPopTipTbl = {}
    self.m_PresentPopTip:SetActive(false)
    self.m_PresentEffectList = {}
    self.m_IsPresentEffectShow = false
    
    local setData = GameplayItem_Setting.GetData()
    self.m_PresentPopTipShowTime = setData.ClubHouse_ComboDuration * 1000
    self.m_BroadcasterEmotionTime = setData.ClubHouse_BroadcasterEmotionTime
    self.m_PresentLongEffectCount = setData.ClubHouse_PresentLongEffectCount

    Extensions.RemoveAllChildren(self.m_PresentPopTipList.transform)    -- 进入房间时再清一遍弹窗
end

function LuaCHRoomContentView:OnEnable()
    LuaClubHouseMgr.m_RoomViewIsOpen = true
    g_ScriptEvent:AddListener("ClubHouse_Myself_Leave_Room", self, "OnClubHouseMyselfLeaveRoom")
    g_ScriptEvent:AddListener("ClubHouse_Member_Character_Changed", self, "OnClubHouseMemberCharacterChanged")
    g_ScriptEvent:AddListener("ClubHouse_Member_Update", self, "OnClubHouseMemberUpdate")
    g_ScriptEvent:AddListener("ClubHouse_Broadcaster_Leave_Room", self, "OnClubHouseBroadcasterLeaveRoom")
    g_ScriptEvent:AddListener("ClubHouse_Audience_Leave_Room", self, "OnClubHouseAudienceLeaveRoom")
    g_ScriptEvent:AddListener("ClubHouse_Broadcaster_Enter_Room", self, "OnClubHouseBroadcasterEnterRoom")
    g_ScriptEvent:AddListener("ClubHouse_Audience_Enter_Room", self, "OnClubHouseAudienceEnterRoom")

    g_ScriptEvent:AddListener("ClubHouse_OnRemoteAudioVolumeIndication", self, "OnClubHouseRemoteAudioVolumeIndication")
    g_ScriptEvent:AddListener("ClubHouse_OnLocalAudioVolumeIndication", self, "OnClubHouseLocalAudioVolumeIndication")

    g_ScriptEvent:AddListener("ClubHouse_Notify_ClearStatusAll", self, "OnClubHouseNotifyClearStatusAll")
    g_ScriptEvent:AddListener("ClubHouse_Notify_ForbidHandsUpChange", self, "OnClubHouseNotifyForbidHandsUpChange")

    g_ScriptEvent:AddListener("ClubHouse_Notify_ChangeOwner", self, "OnClubHouseChangeOwner")

    g_ScriptEvent:AddListener("ClubHouse_QueryAllListenersInRoom_Result_End", self, "OnClubHouseQueryAllListenersInRoomResultEnd")

    g_ScriptEvent:AddListener("OnGetCCSpeakingList", self, "OnGetCCSpeakingList")
    if LuaClubHouseMgr:IsCCRoom() then
        self:StartGetCCSpeakingListTick()
        self:StartUpdateMsgTick()
    end
    g_ScriptEvent:AddListener("ClubHouse_Notify_GivePresent", self, "OnClubHouseNotifyGivePresent")
    g_ScriptEvent:AddListener("ClubHouse_BroadcastMessageInRoom", self, "OnClubHouseBroadcastMessageInRoom")
	g_ScriptEvent:AddListener("OnScreenChange", self, "OnUpdateScreen")       -- 窗口大小变化时刷新聊天记录的位置
	
	g_ScriptEvent:AddListener("ClubHouse_PresentPopTip_Close", self, "OnClubHousePresentPopTipClose")    -- 送礼弹窗过期
    g_ScriptEvent:AddListener("ClubHouse_WithdrawPlayerMsg", self, "OnWithdrawPlayerMsg")     -- 言论回收

    g_ScriptEvent:AddListener("ClubHouse_Notify_UpdateTitle", self, "OnClubHouseNotifyUpdateTitle")
    g_ScriptEvent:AddListener("ClubHouse_QueryPassWord_Result", self, "OnClubHouseQueryPassWordResult")
    
    g_ScriptEvent:AddListener("ClubHouse_Notify_ResetRoomPresent", self, "OnClubHouseNotifyResetRoomPresent")
    g_ScriptEvent:AddListener("ClubHouse_Notify_UpdateHeat", self, "OnClubHouseNotifyUpdateHeat")

    CChatHistoryMgr.Inst.m_OnRecordSelect = CommonDefs.CombineListner_Action_string_Dictionary_int_CChatInputLink(CChatHistoryMgr.Inst.m_OnRecordSelect, self.m_ChatInputCallback, true)
end

function LuaCHRoomContentView:OnDisable()
    LuaClubHouseMgr.m_RoomViewIsOpen = false
    g_ScriptEvent:RemoveListener("ClubHouse_Myself_Leave_Room", self, "OnClubHouseMyselfLeaveRoom")
    g_ScriptEvent:RemoveListener("ClubHouse_Member_Character_Changed", self, "OnClubHouseMemberCharacterChanged")
    g_ScriptEvent:RemoveListener("ClubHouse_Member_Update", self, "OnClubHouseMemberUpdate")
    g_ScriptEvent:RemoveListener("ClubHouse_Broadcaster_Leave_Room", self, "OnClubHouseBroadcasterLeaveRoom")
    g_ScriptEvent:RemoveListener("ClubHouse_Audience_Leave_Room", self, "OnClubHouseAudienceLeaveRoom")
    g_ScriptEvent:RemoveListener("ClubHouse_Broadcaster_Enter_Room", self, "OnClubHouseBroadcasterEnterRoom")
    g_ScriptEvent:RemoveListener("ClubHouse_Audience_Enter_Room", self, "OnClubHouseAudienceEnterRoom")

    g_ScriptEvent:RemoveListener("ClubHouse_OnRemoteAudioVolumeIndication", self, "OnClubHouseRemoteAudioVolumeIndication")
    g_ScriptEvent:RemoveListener("ClubHouse_OnLocalAudioVolumeIndication", self, "OnClubHouseLocalAudioVolumeIndication")

    g_ScriptEvent:RemoveListener("ClubHouse_Notify_ClearStatusAll", self, "OnClubHouseNotifyClearStatusAll")
    g_ScriptEvent:RemoveListener("ClubHouse_Notify_ForbidHandsUpChange", self, "OnClubHouseNotifyForbidHandsUpChange")

    g_ScriptEvent:RemoveListener("ClubHouse_Notify_ChangeOwner", self, "OnClubHouseChangeOwner")

    g_ScriptEvent:RemoveListener("ClubHouse_QueryAllListenersInRoom_Result_End", self, "OnClubHouseQueryAllListenersInRoomResultEnd")

    g_ScriptEvent:RemoveListener("OnGetCCSpeakingList", self, "OnGetCCSpeakingList")
    g_ScriptEvent:RemoveListener("ClubHouse_Notify_GivePresent", self, "OnClubHouseNotifyGivePresent")
    g_ScriptEvent:RemoveListener("ClubHouse_BroadcastMessageInRoom", self, "OnClubHouseBroadcastMessageInRoom")
	g_ScriptEvent:RemoveListener("OnScreenChange", self, "OnUpdateScreen")

	g_ScriptEvent:RemoveListener("ClubHouse_PresentPopTip_Close", self, "OnClubHousePresentPopTipClose")
    g_ScriptEvent:RemoveListener("ClubHouse_WithdrawPlayerMsg", self, "OnWithdrawPlayerMsg")

    g_ScriptEvent:RemoveListener("ClubHouse_Notify_UpdateTitle", self, "OnClubHouseNotifyUpdateTitle") 
    g_ScriptEvent:RemoveListener("ClubHouse_QueryPassWord_Result", self, "OnClubHouseQueryPassWordResult")
    
    g_ScriptEvent:RemoveListener("ClubHouse_Notify_ResetRoomPresent", self, "OnClubHouseNotifyResetRoomPresent")
    g_ScriptEvent:RemoveListener("ClubHouse_Notify_UpdateHeat", self, "OnClubHouseNotifyUpdateHeat")

    CChatHistoryMgr.Inst.m_OnRecordSelect = CommonDefs.CombineListner_Action_string_Dictionary_int_CChatInputLink(CChatHistoryMgr.Inst.m_OnRecordSelect, self.m_ChatInputCallback, false)
    self:ClearAll()
end

function LuaCHRoomContentView:OnClubHouseMyselfLeaveRoom()
    
end

function LuaCHRoomContentView:OnClubHouseMemberCharacterChanged(member)
    --TODO 刷新性能问题
    self:InitBroadcasters()
    self:InitAudienceNum()
    self:LayoutContent(false)
    if member.IsMe then
        self:InitOperationButtons()
    end
    self:UpdateHeatValue()
end

function LuaCHRoomContentView:OnClubHouseMemberUpdate(member)
    if member.IsMe then
        self:InitOperationButtons()
    end
    --刷新主播状态
    for __,pair in pairs(self.m_BroadcasterItemTbl) do
        if pair.accid == member.AccId then
            self:InitBroadcasterStatus(member, pair.item)
			break
        end
    end
    self:UpdateHeatValue()
end

function LuaCHRoomContentView:OnClubHouseBroadcasterLeaveRoom()
    self:InitBroadcasters()
    self:LayoutContent(false)
    self:UpdateHeatValue()
end

function LuaCHRoomContentView:OnClubHouseAudienceLeaveRoom()
    self:InitAudienceNum()
    self:LayoutContent(false)
    self:UpdateHeatValue()
end

function LuaCHRoomContentView:OnClubHouseBroadcasterEnterRoom(member)
    self:InitBroadcasters()
    self:LayoutContent(false)
    self:UpdateHeatValue()
end

function LuaCHRoomContentView:OnClubHouseAudienceEnterRoom(member)
    self:InitAudienceNum()
    self:LayoutContent(false)
    self:UpdateHeatValue()
end

function LuaCHRoomContentView:UpdateHeatValue()
    self.m_HeatValueLabel.text = tostring(LuaClubHouseMgr:GetRoomHeat() or 0)
    self.m_HeatValueSprite:ResetAndUpdateAnchors()
    local newWidth = self.m_RoomNamePanelMaxWidth - self.m_HeatValueLabel.localSize.x
    if self.m_RoomNamePanelWidth ~= newWidth then
        self.m_RoomNamePanelWidth = newWidth
        self.m_RoomNamePanel.m_LuaSelf:SetNamePanelWidth(newWidth)
    end
end

function LuaCHRoomContentView:OnClubHouseRemoteAudioVolumeIndication(volumeTbl)
    local myself = LuaClubHouseMgr:GetMyselfInfo()
    for __,pair in pairs(self.m_BroadcasterItemTbl) do
        if myself.AccId ~= pair.accid then --除去自己
            self:InitBroadcasterIndication(volumeTbl[pair.accid] and true or false, pair.item)
        end
    end
end

function LuaCHRoomContentView:OnClubHouseLocalAudioVolumeIndication(volume)
    local myself = LuaClubHouseMgr:GetMyselfInfo()
    for __,pair in pairs(self.m_BroadcasterItemTbl) do
        if myself.AccId == pair.accid then
            self:InitBroadcasterIndication(volume>0, pair.item)
            break
        end
    end
end

function LuaCHRoomContentView:OnClubHouseCCAudioVolumeInication(cc_EidsTbl)
    for __,pair in pairs(self.m_BroadcasterItemTbl) do
        local info = LuaClubHouseMgr:GetMember(pair.accid)
        if info then
            local eid = info.IsInSameApp and CCCChatMgr.Inst:GetPlayerEId(info.PlayerId) or 0
            self:InitBroadcasterIndication(cc_EidsTbl[eid] and true or false, pair.item)
        end
    end
end

function LuaCHRoomContentView:OnClubHouseNotifyClearStatusAll(status)
    self:InitOperationButtons()
end

function LuaCHRoomContentView:OnClubHouseNotifyForbidHandsUpChange(bForbid)
    if LuaClubHouseMgr:IsAudience(LuaClubHouseMgr:GetMyselfInfo()) then
        self:InitOperationButtons() --观众更新举手状态
    end
end

function LuaCHRoomContentView:OnClubHouseChangeOwner()
    self:InitBroadcasters()
    self:LayoutContent(false)
    self:InitOperationButtons()
end

function LuaCHRoomContentView:OnClubHouseQueryAllListenersInRoomResultEnd()
    self:InitAudienceNum()
    self:LayoutContent(false)
end

function LuaCHRoomContentView:InitRoom()
    self:InitRoomInfo()

    self.m_PresentPopTip:SetActive(false)
    self.m_RoomChatMsgTemplate:SetActive(false)
    self.m_RoomGivePresentMsgTemplate:SetActive(false)
    self:SetNewMsgButtonVisible(false)

    self:InitBroadcasters()
    self:InitAudienceNum()
    self:InitGivePresentTops()

    self:LayoutContent(true)

    self:InitOperationButtons()
    
    self:InitChatInput()

    self.m_RoomChatSimpleTableView.scrollView.panel.onClipMove = 
        CommonDefs.CombineListner_OnClippingMoved(
            self.m_RoomChatSimpleTableView.scrollView.panel.onClipMove, 
        LuaUtils.OnClippingMoved(function()
            self:OnRoomChatSimpleTableViewClipMove()
        end), true)

    self:TryOpenPrivateRoomPasswordSetGuide()
end

function LuaCHRoomContentView:TryOpenPrivateRoomPasswordSetGuide()
    if LuaClubHouseMgr.m_CurRoomInfo.RoomType == EnumCHRoomType.private and LuaClubHouseMgr:GetMyselfInfo().Character == EnumCHCharacterType.Owner then
        CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.ClubHousePrivateRoomPasswordSet)
    end
end

function LuaCHRoomContentView:LayoutContent(needResetScollView)
    for __,pair in pairs(self.m_BroadcasterItemTbl) do
        local child = pair.item
        if child.gameObject.activeSelf then
            self:SetBroadcasterIndicationTemproryVisibility(true, child.gameObject)
        end
    end
    if needResetScollView then
        self.m_ContentScrollView:ResetPosition()
    end
    self:SetBroadcasterGridBgAndRoomChatViewSize(needResetScollView)   -- 各部分大小调整
end

function LuaCHRoomContentView:InitAudienceNum()
    local count = LuaClubHouseMgr:GetAudienceDisplayCount()
    local preActive = self.m_AudienceButton.gameObject.activeSelf
    self.m_AudienceButton.Text = tostring(count)
    self.m_AudienceButton.gameObject:SetActive(count > 0)
    if not preActive and count > 0 then
        self.m_BroadcasterGrid:Reposition()
    end
end

function LuaCHRoomContentView:InitBroadcasters()
    self.m_BroadcasterItemTbl = {}
    self.m_AudienceButton.transform.parent = self.m_TemplatePoolRoot -- 先从grid下移出
    local n = self.m_BroadcasterGrid.transform.childCount
    for i=0,n-1 do
        self.m_BroadcasterGrid.transform:GetChild(i).gameObject:SetActive(false)
    end
    local broadcasters = LuaClubHouseMgr:GetBroadcasters()
    local index = 0
    for __, broadcaster in pairs(broadcasters) do
        local go = nil
		if index<n then
			go = self.m_BroadcasterGrid.transform:GetChild(index).gameObject
		else
			go = CUICommonDef.AddChild(self.m_BroadcasterGrid.gameObject, self.m_BroadcasterTemplate)
		end
		index = index+1
        go:SetActive(true)
        table.insert(self.m_BroadcasterItemTbl, {playerid = broadcaster.PlayerId, accid = broadcaster.AccId, item = go})
        self:InitOneBroadcaster(go.transform, broadcaster)
        local accid = broadcaster.AccId
        CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(function(go) self:OnBroadcasterClick(go, accid) end), false)
    end
    self.m_AudienceButton.transform.parent = self.m_BroadcasterGrid.transform
    self.m_BroadcasterGrid:Reposition()
    g_ScriptEvent:BroadcastInLua("ClubHouse_RoomContentView_Broadcaster_Update")
end

function LuaCHRoomContentView:InitOneBroadcaster(itemTrans, member)
    local portrait = itemTrans:GetComponent(typeof(CUITexture))
    local nameLabel = itemTrans:Find("NameLabel"):GetComponent(typeof(UILabel))
    local ownerIcon = itemTrans:Find("Owner").gameObject
    local rankSprite = itemTrans:Find("Rank"):GetComponent(typeof(UISprite))
    local bg = itemTrans:Find("Bg"):GetComponent(typeof(UISprite))
    bg.spriteName = self.m_IsSpecialRoom and "clubhouse_touxiangkuang" or "clubhouse_touxiangkuang_02"
    portrait:LoadNPCPortrait(member.Portrait, false)
    self:WrapText(nameLabel, member.PlayerName, member.IsMe)
    LuaClubHouseMgr:SetGivePresentTopThreeSprite(rankSprite, member.AccId)
    ownerIcon:SetActive(member.AccId == LuaClubHouseMgr:GetRoomOwner().AccId)
    self:InitBroadcasterStatus(member, itemTrans.gameObject)
    self:InitBroadcasterIndication(false, itemTrans.gameObject)
    itemTrans:Find("EmotionBg").gameObject:SetActive(false)
    itemTrans:Find("EmotionLabel"):GetComponent(typeof(UILabel)).text = ""
    LuaClubHouseMgr:SetMengDaoProfile(portrait, member.PlayerId, "CHRoomBroadcaster")
end

function LuaCHRoomContentView:InitOneAudience(itemTrans, member)
    local portrait = itemTrans:GetComponent(typeof(CUITexture))
    local nameLabel = itemTrans:Find("NameLabel"):GetComponent(typeof(UILabel))
    portrait:LoadNPCPortrait(member.Portrait, false)
    self:WrapText(nameLabel, member.PlayerName, member.IsMe)
end

function LuaCHRoomContentView:WrapText(label, originText, isMe)
    --赋值对label各项参数进行初始化，否则下面的计算会出问题，label类型需要clamp content， maxlines = 1
    label.text = originText
    local fit = true
    local outerText = ""
    fit, outerText = label:Wrap(originText)
    if not fit then
        outerText = CommonDefs.StringSubstring2(outerText, 0, CommonDefs.StringLength(outerText) - 1) .. "..."
        label.text = isMe and "[c][00ff60]"..outerText.."[-][/c]" or outerText
    elseif isMe then
        label.text = "[c][00ff60]"..originText.."[-][/c]"
    end
end

function LuaCHRoomContentView:InitBroadcasterStatus(member, itemGo)
    local statusSprite = itemGo.transform:Find("Status/StatusSprite"):GetComponent(typeof(UISprite))
    statusSprite.spriteName = LuaClubHouseMgr:GetBroadcasterStatusSprite(member, false)
    if LuaClubHouseMgr:IsSpeaking(member) then
        statusSprite.color = NGUIText.ParseColor24("FFFFFF", 0) --开麦状态下显示白色图标
        statusSprite.transform.localScale = Vector3.one
    elseif LuaClubHouseMgr:IsForbidden(member) then
        statusSprite.color = NGUIText.ParseColor24("FF5050", 0) --禁言状态下显示红色图标
        statusSprite.transform.localScale = Vector3(0.7,0.7,1)
    else
        statusSprite.color = NGUIText.ParseColor24("8D8D8D", 0) --闭麦状态下显示灰色图标
        statusSprite.transform.localScale = Vector3.one
    end
end

function LuaCHRoomContentView:InitBroadcasterIndication(visible, itemGo)
    local indicationSprite = itemGo.transform:Find("IndicationSprite").gameObject
    indicationSprite:SetActive(visible)
end

function LuaCHRoomContentView:SetBroadcasterIndicationTemproryVisibility(visible, itemGo)
    local indicationSprite = itemGo.transform:Find("IndicationSprite").gameObject
    local widgets = CommonDefs.GetComponentsInChildren_Component_Type(indicationSprite.transform, typeof(UIWidget))
    for i=0,widgets.Length-1 do
        widgets[i].enabled = visible
    end
end

function LuaCHRoomContentView:OnBroadcasterClick(go, accid)
    local info = LuaClubHouseMgr:GetMember(accid)
    local extraInfo = CreateFromClass(MakeGenericClass(Dictionary, String, Object))
    CommonDefs.DictAdd_LuaCall(extraInfo, "AccId", info.AccId)
    CommonDefs.DictAdd_LuaCall(extraInfo, "AppName", info.AppName)
    CPlayerInfoMgr.ShowPlayerPopupMenu(info.PlayerId, EnumPlayerInfoContext.ChatRoom, EChatPanel.Undefined, nil, nil, extraInfo, go.transform.position, CPlayerInfoMgrAlignType.Right)
end

function LuaCHRoomContentView:OnAudienceClick(go, accid)
    local info = LuaClubHouseMgr:GetMember(accid)
    local extraInfo = CreateFromClass(MakeGenericClass(Dictionary, String, Object))
    CommonDefs.DictAdd_LuaCall(extraInfo, "AccId", info.AccId)
    CommonDefs.DictAdd_LuaCall(extraInfo, "AppName", info.AppName)
    CPlayerInfoMgr.ShowPlayerPopupMenu(info.PlayerId, EnumPlayerInfoContext.ChatRoom, EChatPanel.Undefined, nil, nil, extraInfo, go.transform.position, CPlayerInfoMgrAlignType.Right)
end

function LuaCHRoomContentView:InitOperationButtons()
    local myself = LuaClubHouseMgr:GetMyselfInfo()
    self.m_DealWithApplyButton:SetActive(myself.Character == EnumCHCharacterType.Owner)
    self.m_LeaveMicButton:SetActive(myself.Character == EnumCHCharacterType.Speaker)
    self.m_MuteMicButton:SetActive(myself.Character == EnumCHCharacterType.Owner or myself.Character == EnumCHCharacterType.Speaker)
    self.m_ApplyMicButton:SetActive(myself.Character == EnumCHCharacterType.Listener)
    self.m_MuteMicButton.transform:Find("Sprite"):GetComponent(typeof(UISprite)).spriteName = LuaClubHouseMgr:GetSpeakingSprite(myself)
    if self.m_ApplyMicButton.activeSelf then
        self.m_ApplyMicButton:GetComponent(typeof(CButton)).Enabled = not LuaClubHouseMgr:IsRoomForbiddenHandsUp()
        self.m_ApplyMicButton.transform:Find("Sprite"):GetComponent(typeof(UISprite)).spriteName = LuaClubHouseMgr:GetInverseHandsUpSprite(myself)
    end
    self.m_MicButtonTable:Reposition()
end

------------------
-- CC声音波纹
------------------
function LuaCHRoomContentView:StartGetCCSpeakingListTick()
    self:StopGetCCSpeakingListTick()
    if not CCMiniAPIMgr.Inst.IsCCMiniEnabled then return end
    self.m_GetCCSpeakingListTick = RegisterTick(function()
        if CCMiniAPIMgr.Inst:IsInClubHouseStream() then
            CCMiniAPIMgr.Inst:GetSpeakingList(EnumCCMiniStreamType.eClubHouse)
        else
            self:OnClubHouseCCAudioVolumeInication({})
        end
    end, 1000)
end

function LuaCHRoomContentView:StopGetCCSpeakingListTick()
    if self.m_GetCCSpeakingListTick then
        UnRegisterTick(self.m_GetCCSpeakingListTick)
        self.m_GetCCSpeakingListTick = nil
    end
end

function LuaCHRoomContentView:OnGetCCSpeakingList(args)
    local type = args[0]
    local eids = args[1]
    if type ~= EnumCCMiniStreamType.eClubHouse then return end
    local cc_EidsTbl = {}
    CommonDefs.EnumerableIterate(eids, DelegateFactory.Action_object(function(eid)
        cc_EidsTbl[eid] = true
    end))
    self:OnClubHouseCCAudioVolumeInication(cc_EidsTbl)
end

------------------------
--- 房间信息
------------------------
function LuaCHRoomContentView:InitRoomInfo()
    self.m_UnReadMsgCount = 0
    -- 房间Id
    self.m_RoomIdLabel.text = tostring(LuaClubHouseMgr:GetRoomId() or 0)
    -- 房间热度
    self:UpdateHeatValue()
    -- 房间是否为特殊房间
    local roomTitleInfo = GameplayItem_ClubHouseTitle.GetData(LuaClubHouseMgr:GetRoomTitle() or 0)
    self.m_IsSpecialRoom = roomTitleInfo ~= nil
    self.m_BroadcasterGridBg_Normal.gameObject:SetActive(not self.m_IsSpecialRoom)
    self.m_RoomBg.texture.alpha = self.m_IsSpecialRoom and 1 or 0
    self.m_BroadcasterGridBg.alpha = self.m_IsSpecialRoom and 1 or 0
    self.m_PresentLeftBg_Normal:SetActive(not self.m_IsSpecialRoom)
    self.m_PresentLeftBg_Special:SetActive(self.m_IsSpecialRoom)
    -- 房间标签
    local tagData = GameplayItem_ClubHouseTag.GetData(LuaClubHouseMgr:GetRoomTag() or 0)
    if tagData then
        self.m_RoomTag.gameObject:SetActive(true)
        self.m_RoomTag.spriteName = tagData.TagIcon
    else
        self.m_RoomTag.gameObject:SetActive(false)
    end
    -- 房间名
    self.m_RoomNamePanel.m_LuaSelf:Init(LuaClubHouseMgr:GetRoomId(), LuaClubHouseMgr:GetRoomName(), self.m_IsSpecialRoom)
end

------------------------
--- 聊天
------------------------
--- 调整聊天框位置和大小
function LuaCHRoomContentView:SetBroadcasterGridBgAndRoomChatViewSize(needResetScollView)
    local broadcasterGridHeight = NGUIMath.CalculateRelativeWidgetBounds(self.m_BroadcasterGrid.transform).size.y
    local delta_height = self.m_BroadcasterGrid.transform.childCount / 5 > 1 and 0 or broadcasterGridHeight
    self.m_NowBroadcasterGridBgPos = Vector3(60, 263 + delta_height, 0)
    local left = self.m_RoomChatSimpleTableView.scrollView.panel.leftAnchor.absolute
    local right = self.m_RoomChatSimpleTableView.scrollView.panel.rightAnchor.absolute
    local bottom = self.m_RoomChatSimpleTableView.scrollView.panel.bottomAnchor.absolute
    local top = self.m_RoomChatSimpleTableView.scrollView.panel.topAnchor.absolute
    if top ~= -70-broadcasterGridHeight then
        self.m_RoomChatSimpleTableView.scrollView.panel:SetAnchor(self.m_ContentScrollView.gameObject, left, bottom, right, -70-broadcasterGridHeight)
        if needResetScollView then
            self.m_RoomChatSimpleTableView.table.transform.localPosition = Extensions.GetTopPos(self.m_RoomChatSimpleTableView.scrollView)
        end
        self.m_BroadcasterGridBg.texture:ResetAndUpdateAnchors()
        self.m_BroadcasterGridBg_Normal:ResetAndUpdateAnchors()
    end
end

function LuaCHRoomContentView:OnUpdateScreen()
    local pos = Extensions.GetTopPos(self.m_ContentScrollView)
    pos.y = pos.y + 20
    self.m_ContentTop.transform.localPosition = pos
    self.m_BroadcasterGridBg.texture:ResetAndUpdateAnchors()
    self.m_BroadcasterGridBg_Normal:ResetAndUpdateAnchors()
    self.m_RoomChatSimpleTableView:LoadDataFromTail()
end

function LuaCHRoomContentView:InitChatInput()
    self.m_ChatInput.needAppendToChatHistory = true
	if not self.m_DefaultChatInputListener then
		self.m_DefaultChatInputListener = DefaultChatInputListener()
		self.m_DefaultChatInputListener:SetOnInputEmoticonFunc(function (prefix)
			self:OnInputEmoticon(prefix)
		end)
		self.m_DefaultChatInputListener:SetOnInputItemFunc(function (item)
			self:OnInputItemLink(item)
		end)
        self.m_DefaultChatInputListener:SetOnInputBabyFunc(function (babyId, babyName)
            self:OnInputBabyLink(babyId, babyName)
        end)
        self.m_DefaultChatInputListener:SetOnLingShouFunc(function (lingshouId, lingshouName)
            self:OnInputLingShouLink(lingshouId, lingshouName)
        end)
        self.m_DefaultChatInputListener:SetOnInputWeddingItemFunc(function (item)
            self:OnInputWeddingItemLink(item)
        end)
        self.m_DefaultChatInputListener:SetOnInputAchievementFunc(function (achievementId)
            self:OnInputAchievement(achievementId)
        end)
	end

    if not self.m_DefaultSimpleTableViewDataSource then
		self.m_DefaultSimpleTableViewDataSource = DefaultUISimpleTableViewDataSource.Create(function ()
			return #LuaClubHouseMgr.m_RoomMessageDataList
		end,
		function (index)
			return self:CellForRowAtIndex(index)
		end)
	end
    self.m_RoomChatSimpleTableView:Clear()
    self.m_RoomChatSimpleTableView.dataSource = self.m_DefaultSimpleTableViewDataSource
    self.m_RoomChatSimpleTableView:LoadDataFromTail()
end

function LuaCHRoomContentView:OnInputEmoticon(prefix)
	self.m_ChatInput:AddEmotion(prefix)
end

function LuaCHRoomContentView:OnInputItemLink(item)
	self.m_ChatInput:AddItem(item)
end

function LuaCHRoomContentView:OnInputBabyLink(babyId, babyName)
    self.m_ChatInput:AddBaby(babyId, babyName)
end

function LuaCHRoomContentView:OnInputLingShouLink(lingshouId, lingshouName)
    self.m_ChatInput:AddLingShou(lingshouId, lingshouName)
end

function LuaCHRoomContentView:OnInputWeddingItemLink(item)
    self.m_ChatInput:AddWeddingItem(item)
end

function LuaCHRoomContentView:OnInputAchievement(achievementId)
    self.m_ChatInput:AddAchievement(achievementId)
end

function LuaCHRoomContentView:CellForRowAtIndex(index)
    local msgData = LuaClubHouseMgr.m_RoomMessageDataList and LuaClubHouseMgr.m_RoomMessageDataList[index + 1]
    if msgData then
        return msgData.SendId > 0 and self:InitChatMsgTemplate(msgData) or self:InitPresentMsgTemplate(msgData)
    end
    return nil
end

function LuaCHRoomContentView:InitChatMsgTemplate(msgData)
    local cellIdentifier = "MsgTemplate"
    local cell = self.m_RoomChatSimpleTableView:DequeueReusableCellWithIdentifier(cellIdentifier)
    if cell == nil then
        cell = self.m_RoomChatSimpleTableView:AllocNewCellWithIdentifier(self.m_RoomChatMsgTemplate, cellIdentifier)
    end

    local label = cell:GetComponent(typeof(UILabel))
    local palyerLink = CChatLinkMgr.ChatPlayerLink.GenerateLink(msgData.SendId, msgData.SendName).displayTag
    
    local reserve = ""
    for i = 1, #msgData.TagList do
        reserve = reserve .. "               "
    end
    local msg = SafeStringFormat3("[c]%s[FF66FF]%s[c][ffffff]%s", reserve, palyerLink, msgData.content)
    label.text = CChatLinkMgr.TranslateToNGUIText(msg, false)
    label:UpdateNGUIText()
    label:ResizeCollider()

	local mTempVerts = CreateFromClass(MakeGenericClass(BetterList, Vector3))
    local mTempIndices = CreateFromClass(MakeGenericClass(BetterList, Int32))
    NGUIText.PrintExactCharacterPositions(label.text, mTempVerts, mTempIndices)
    if mTempVerts.size > 1 then
		local fisrtLineTopY = float.MinValue
		local firstLineBottomY = float.MaxValue
		for i = 1, mTempVerts.size - 1, 2 do
			if fisrtLineTopY <= mTempVerts[i].y then
				fisrtLineTopY = mTempVerts[i].y
				if firstLineBottomY > mTempVerts[i - 1].y then
					firstLineBottomY = mTempVerts[i - 1].y
				end
			end
		end
        self:InitMsgTags(cell, msgData.TagList, true, firstLineBottomY)
    else
        self:InitMsgTags(cell, msgData.TagList, false, 0)
    end

    CommonDefs.AddOnClickListener(label.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnChatMsgLabelClick(go, msgData.SendId) end), false)
    return cell
end

function LuaCHRoomContentView:InitMsgTags(cell, msgTagList, hasTempVerts, firstLineBottomY)
    local rankSprite1 = cell.transform:Find("Rank"):GetComponent(typeof(UISprite))
    local rankSprite2 = cell.transform:Find("Rank2"):GetComponent(typeof(UISprite))
    rankSprite1.gameObject:SetActive(false)
    rankSprite2.gameObject:SetActive(false)
    local rankSprites = {rankSprite1, rankSprite2}
    for i = 1, #msgTagList do
        rankSprites[i].gameObject:SetActive(true)
        rankSprites[i].spriteName = msgTagList[i]
        if hasTempVerts then
            LuaUtils.SetLocalPositionY(rankSprites[i].transform, firstLineBottomY + rankSprites[i].height * 0.5 - 3)
        else
            LuaUtils.SetLocalPositionY(rankSprites[i].transform, -rankSprites[i].height * 0.5 - 3)
        end
    end
end

function LuaCHRoomContentView:InitPresentMsgTemplate(msgData)
    local cellIdentifier = "PresentMsgTemplate"
    local cell = self.m_RoomChatSimpleTableView:DequeueReusableCellWithIdentifier(cellIdentifier)
    if cell == nil then
        cell = self.m_RoomChatSimpleTableView:AllocNewCellWithIdentifier(self.m_RoomGivePresentMsgTemplate, cellIdentifier)
    end
    local label = cell:GetComponent(typeof(UILabel))
    label.text = CChatLinkMgr.TranslateToNGUIText(msgData.content)
    label:UpdateNGUIText()
    label:ResizeCollider()
    CommonDefs.AddOnClickListener(label.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnChatMsgLabelClick(go) end), false)
    return cell
end

function LuaCHRoomContentView:OnSendMsg(msg)
    if LuaClubHouseMgr:DoSendChatMsg(msg) then
        self.m_ChatInput:Clear()
    end
end

function LuaCHRoomContentView:OnClubHouseBroadcastMessageInRoom(playerId, content)
    if self:LastChildIsVisible() then
        self.m_HasNewMsgToUpdate = true
    elseif not self.m_HasNewMsgToUpdate then
        self.m_UnReadMsgCount = self.m_UnReadMsgCount + 1
        self:SetNewMsgButtonVisible(true)
    end
    self:SetBroadcasterEmotion(playerId, content)
end

function LuaCHRoomContentView:StartUpdateMsgTick()
    if self.m_UpdateMsgTick then
        UnRegisterTick(self.m_UpdateMsgTick)
    end
    self.m_UpdateMsgTick = RegisterTick(function()
        if self.m_HasNewMsgToUpdate then
            self.m_RoomChatSimpleTableView:LoadDataFromTail()
            self.m_HasNewMsgToUpdate = false
        end
    end, 200)
end

function LuaCHRoomContentView:LastChildIsVisible()
    return #LuaClubHouseMgr.m_RoomMessageDataList == 1 or (#LuaClubHouseMgr.m_RoomMessageDataList > 1 and self.m_RoomChatSimpleTableView:IsVisible(#LuaClubHouseMgr.m_RoomMessageDataList - 2))
end

function LuaCHRoomContentView:OnHistorySelect(text, existingLinks) 
    self.m_ChatInput:RelaceContent(text, existingLinks)
    if self.m_ChatHistoryMenu ~= nil then
        self.m_ChatHistoryMenu.gameObject:SetActive(false)
    end
end

function LuaCHRoomContentView:OnChatMsgLabelClick(go, playerId)
    local url = go:GetComponent(typeof(UILabel)):GetUrlAtPosition(UICamera.lastWorldPosition)
    if url then
        if playerId then
            local member = LuaClubHouseMgr:GetMemberInfoByPlayerId(playerId)
            if member then
                local env = CreateFromClass(MakeGenericClass(Dictionary, String, Object))
                CommonDefs.DictAdd_LuaCall(env, "clubhouse", true)
                CommonDefs.DictAdd_LuaCall(env, "AccId", member.AccId)
                CommonDefs.DictAdd_LuaCall(env, "AppName", member.AppName)
                CChatLinkMgr.ProcessLinkClick(url, CChatLinkMgr.LinkSource.ClubHouse, env)
            else
                g_MessageMgr:ShowMessage("CLUBHOUSE_AUDIENCE_NOTFOUND")
            end
        else
            CChatLinkMgr.ProcessLinkClick(url, nil)
        end
    end
end

-- 麦上主播发送消息的第一个表情显示在其头像上
function LuaCHRoomContentView:SetBroadcasterEmotion(playerId, content)
    for __,pair in pairs(self.m_BroadcasterItemTbl) do
        if playerId == pair.playerid then
            local regex = CreateFromClass(Regex, "#(40[0-1]|310|30[0-9]|29[0-2]|2[0-8][0-9]|1[0-6][0-9]|19[0-9]|[1-9][0-9]|[0-9])", RegexOptions.None)
            local match = regex:Match(content)
            if match.Success then
                local emoticonSetData = GameplayItem_ClubHouseEmoticon.GetData(tonumber(ToStringWrap(match.Groups[1])))
                if emoticonSetData == nil or emoticonSetData.Scale == 0 then return end
                local emotionLabel = pair.item.transform:Find("EmotionLabel"):GetComponent(typeof(UILabel))
                local emotionBg = pair.item.transform:Find("EmotionBg")
                if pair.CloseEmotionTick then
                    UnRegisterTick(pair.CloseEmotionTick)
                end
                emotionBg.gameObject:SetActive(true)
                emotionLabel.text = "#"..ToStringWrap(match.Groups[1])
                local scale = emoticonSetData and emoticonSetData.Scale or 1
                emotionLabel.transform.localScale = Vector3(scale,scale,1)
                pair.CloseEmotionTick = RegisterTickOnce(function ()
                    emotionBg.gameObject:SetActive(false)
                    emotionLabel.text = ""
                end, self.m_BroadcasterEmotionTime)
            end
            break
        end
    end
end

function LuaCHRoomContentView:StopAllChatTick()
    if self.m_BroadcasterItemTbl then
        for __,pair in pairs(self.m_BroadcasterItemTbl) do
            if pair.CloseEmotionTick then
                UnRegisterTick(pair.CloseEmotionTick)
            end
            pair.item.transform:Find("EmotionBg").gameObject:SetActive(false)
            pair.item.transform:Find("EmotionLabel"):GetComponent(typeof(UILabel)).text = ""
        end
    end
end

function LuaCHRoomContentView:OnWithdrawPlayerMsg()
    self.m_RoomChatSimpleTableView:LoadDataFromTail()
end

------------------------
--- 送礼
------------------------
function LuaCHRoomContentView:InitGivePresentTops()
    if self.m_GivePresentTopItemTbl == nil then
        self.m_GivePresentTopItemTbl = {}
        self.m_GivePresentTopTemplate:SetActive(false)
        for i = 1, 3 do
            local item = CUICommonDef.AddChild(self.m_GivePresentTopTable.gameObject, self.m_GivePresentTopTemplate)
            table.insert(self.m_GivePresentTopItemTbl, {item = item})
            CommonDefs.AddOnClickListener(item, DelegateFactory.Action_GameObject(function(go) self:OnGivePresentTopsClick() end), false)
        end
    end
    self:UpdateGivePresentTops()
end

function LuaCHRoomContentView:OnClubHouseNotifyResetRoomPresent()
    self:UpdateGivePresentTops()
end

function LuaCHRoomContentView:UpdateGivePresentTops()
    for i = 1, 3 do
        local rank = 4 - i
        local item = self.m_GivePresentTopItemTbl[i].item
        local info = LuaClubHouseMgr.m_CurRoomInfo.PresentRankTbl[rank]
        if info then
            if self.m_GivePresentTopItemTbl[i].accId ~= info.member.AccId then
                local portrait = item.transform:GetComponent(typeof(CUITexture))
                local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
                local rankSprite = item.transform:Find("Rank"):GetComponent(typeof(UISprite))
                portrait:LoadNPCPortrait(info.member.Portrait, false)
                LuaClubHouseMgr:SetMengDaoProfile(portrait, info.member.PlayerId, "CHRoomGivePresentTops")
                self:WrapText(nameLabel, info.member.PlayerName, info.member.IsMe)
                rankSprite.spriteName = LuaClubHouseMgr:GetGivePresentTopThreeSprite(rank)
                self.m_GivePresentTopItemTbl[i].accId = info.member.AccId
            end
            item.gameObject:SetActive(true)
        else
            item.gameObject:SetActive(false)
        end
    end
    self.m_GivePresentTopTable:Reposition()
    local height = NGUIMath.CalculateRelativeWidgetBounds(self.m_GivePresentTopTable.transform).size.y + 130
    self.m_GivePresentTopTableBg1.height = height
    self.m_GivePresentTopTableBg2.height = height

    -- 主播队列排名更新
    for _, broadcaster in pairs(self.m_BroadcasterItemTbl) do
        local rankSprite = broadcaster.item.transform:Find("Rank"):GetComponent(typeof(UISprite))
        LuaClubHouseMgr:SetGivePresentTopThreeSprite(rankSprite, broadcaster.accid)
    end
end

function LuaCHRoomContentView:OnClubHouseNotifyGivePresent(srcPlayerName, dstPlayerName, PresentId, Combo, EffectTimes)
    self:UpdateGivePresentTops()    -- 前三名排行
    self:UpdateHeatValue()
    self:ShowPresent(srcPlayerName, dstPlayerName, PresentId, Combo, EffectTimes)
end

function LuaCHRoomContentView:ShowPresent(srcPlayerName, dstPlayerName, PresentId, Combo, EffectTimes)

    if self.m_PresentPopTipTbl[self.m_PresentPopTipNewId - 1]
        and self.m_PresentPopTipTbl[self.m_PresentPopTipNewId - 1].srcPlayerName == srcPlayerName
        and self.m_PresentPopTipTbl[self.m_PresentPopTipNewId - 1].PresentId == PresentId then
        self.m_PresentPopTipTbl[self.m_PresentPopTipNewId - 1].script:UpdateCombo(Combo, self.m_PresentPopTipShowTime)
    else
        for _, popTip in pairs(self.m_PresentPopTipTbl) do
            popTip.script:UpdateIndex(srcPlayerName, dstPlayerName, PresentId)
        end
        local popTip = CUICommonDef.AddChild(self.m_PresentPopTipList.gameObject, self.m_PresentPopTip)
        popTip.gameObject:SetActive(true)
        local script = popTip:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf

        script:Init(self.m_PresentPopTipNewId, self.m_PresentPopTipShowTime, srcPlayerName, dstPlayerName, PresentId, Combo)
        self.m_PresentPopTipTbl[self.m_PresentPopTipNewId] = {script = script, srcPlayerName = srcPlayerName, PresentId = PresentId}
        self.m_PresentPopTipNewId = self.m_PresentPopTipNewId + 1
    end

    local presentData = GameplayItem_ClubHousePresent.GetData(PresentId)
    if presentData and EffectTimes > 0 then
        for i = 1, EffectTimes do
            table.insert(self.m_PresentEffectList, PresentId)
        end
        if not self.m_IsPresentEffectShow then
            self:PlayPresentEffect()
        end
    end
end

function LuaCHRoomContentView:PlayPresentEffect()
    self.m_GivePresentCUIFx:DestroyFx()
    self.m_GivePresentChildWnd.gameObject:SetActive(false)
    if #self.m_PresentEffectList == 0 then
        -- 当前队列已不存在待播礼物
        self.m_IsPresentEffectShow = false
    else
        self.m_IsPresentEffectShow = true
        local presentId = self.m_PresentEffectList[1] -- 获得最新一个
        local presentData = GameplayItem_ClubHousePresent.GetData(presentId)
        if not System.String.IsNullOrEmpty(presentData.Fx) then
            self.m_GivePresentCUIFx:LoadFx(presentData.Fx)
        else
            self.m_GivePresentChildWnd.gameObject:SetActive(true)
			self.m_GivePresentChildWnd:Init(presentData.ChildWnd, DelegateFactory.Action(function ()
                self.m_GivePresentChildWnd:SetBasePanelDepth(self.m_GivePresentView.depth)      -- 设置childwnd的panel基准深度
                self:WiatPlayPresentEffectFinish(self.m_GivePresentChildWnd)
            end), {})
        end
    end
end

function LuaCHRoomContentView:WiatPlayPresentEffectFinish(root)
    if #self.m_PresentEffectList == 0 then return end
    local presentId = table.remove(self.m_PresentEffectList, 1) -- 获得最新一个并移出队列
    local presentData = GameplayItem_ClubHousePresent.GetData(presentId)
    -- 动画位置偏移调整
    root.transform.localPosition = presentData.FxOffset and Vector3(presentData.FxOffset[0], presentData.FxOffset[1], presentData.FxOffset[2]) or Vector3.zero
    -- 长动画播放音效
    if #self.m_PresentEffectList <= self.m_PresentLongEffectCount then
        self:PlayPresentSound(presentData.Sound, presentData.SoundBeginTime, presentData.SoundDuringTime)
    end
    -- 根据待播动画队列长度控制播放长短动画
    local anim = CommonDefs.GetComponentsInChildren_GameObject_Type_Boolean(root.gameObject, typeof(Animation), true)[0]
    local animClip = #self.m_PresentEffectList < self.m_PresentLongEffectCount and presentData.LongAnim or presentData.ShortAnim
    if anim:get_Item(animClip) then
        anim:get_Item(animClip).time = 0
        anim:Play(animClip)
        if self.m_PresentEffectTick then
            UnRegisterTick(self.m_PresentEffectTick)
        end
        self.m_PresentEffectTick = RegisterTickOnce(function ()
            self:PlayPresentEffect()
        end, anim:get_Item(animClip).length * 1000)
    else
        self:PlayPresentEffect()
    end
end

function LuaCHRoomContentView:PlayPresentSound(soundPath, beginTime, duringTime)
    if System.String.IsNullOrEmpty(soundPath) then return end
    if self.m_PresentSoundTick then
        UnRegisterTick(self.m_PresentSoundTick)
        SoundManager.Inst:StopSound(self.m_PresentSound)
    end
    self.m_PresentSound = SoundManager.Inst:PlayOneShot(soundPath,Vector3.zero,nil,beginTime)
    self.m_PresentSoundTick = RegisterTickOnce(function ()
        SoundManager.Inst:StopSound(self.m_PresentSound)
    end, duringTime)
end

function LuaCHRoomContentView:OnClubHousePresentPopTipClose(id)
    self.m_PresentPopTipTbl[id] = nil
end

function LuaCHRoomContentView:OnClubHouseNotifyUpdateTitle(RoomId, Title)
    if RoomId == LuaClubHouseMgr:GetRoomId() then
        self:InitRoom()
    end
end

function LuaCHRoomContentView:OnClubHouseNotifyUpdateHeat()
    self:UpdateHeatValue()
end

function LuaCHRoomContentView:ClearAll()
    self.m_ChatInput:Clear()
    self:StopGetCCSpeakingListTick()
    self:StopAllChatTick()
    if self.m_UpdateMsgTick then
        UnRegisterTick(self.m_UpdateMsgTick)
    end
    self.m_GivePresentCUIFx.gameObject:SetActive(false)
    self.m_GivePresentChildWnd.gameObject:SetActive(false)
    if self.m_PresentEffectTick then
        UnRegisterTick(self.m_PresentEffectTick)
    end
    if self.m_PresentSoundTick then
        UnRegisterTick(self.m_PresentSoundTick)
        SoundManager.Inst:StopSound(self.m_PresentSound)
    end
    -- 清空弹窗
    if self.m_PresentPopTipTbl then
        for id, presentPopTip in pairs(self.m_PresentPopTipTbl) do
            GameObject.Destroy(presentPopTip.script.gameObject)
        end
    end
    -- 强制关闭送礼界面
    if CUIManager.IsLoaded(CLuaUIResources.CHChooseGiftWnd) then
        CUIManager.CloseUI(CLuaUIResources.CHChooseGiftWnd)
    end
    -- 强制关闭听众界面
    if CUIManager.IsLoaded(CLuaUIResources.CHAudienceListWnd) then
        CUIManager.CloseUI(CLuaUIResources.CHAudienceListWnd)
    end
end

function LuaCHRoomContentView:OnClubHouseQueryPassWordResult(playerId, bSuccess, data_U)
    local data = g_MessagePack.unpack(data_U)
    if bSuccess and data.roRoomId ~= LuaClubHouseMgr.m_CurRoomInfo.RoomId and data.Context == "ShareChatRoom" then
        LuaInGameShareMgr.ShareChatRoom(LuaClubHouseMgr:GetRoomId(), LuaClubHouseMgr:GetRoomName(), data.PassWord, self.m_ShareButton.transform, CPopupMenuInfoMgrAlignType.Right)
    end
end

------------------
-- UI事件处理
------------------

function LuaCHRoomContentView:OnBackButtonClick()
    g_ScriptEvent:BroadcastInLua("RoomContentView_OnBackButtonClick")
end

function LuaCHRoomContentView:OnInfoButtonClick()
    g_MessageMgr:ShowMessage("CLUBHOUSE_USER_DESCRIPTION")
end

function LuaCHRoomContentView:OnShareButtonClick()
    if LuaClubHouseMgr.m_CurRoomInfo.RoomType == EnumCHRoomType.private then
        Gac2Gas.ClubHouse_QueryPassWord("ShareChatRoom")
    else
        LuaInGameShareMgr.ShareChatRoom(LuaClubHouseMgr:GetRoomId(), LuaClubHouseMgr:GetRoomName(), "", self.m_ShareButton.transform, CPopupMenuInfoMgrAlignType.Right)
    end
end

function LuaCHRoomContentView:OnLeaveRoomButtonClick()
    LuaClubHouseMgr:LeaveRoom()
end

function LuaCHRoomContentView:OnDealWithApplyButtonClick()
    local myself = LuaClubHouseMgr:GetMyselfInfo()
    if myself and myself.Character == EnumCHCharacterType.Owner then
        CUIManager.ShowUI("CHApplySpeakListWnd")
    end
end

function LuaCHRoomContentView:OnLeaveMicButtonClick()
    LuaClubHouseMgr:LeaveMic()
end

function LuaCHRoomContentView:OnMuteMicButtonClick()
    LuaClubHouseMgr:ChangeSpeakingStatus(not LuaClubHouseMgr:IsMyselfSpeaking())
end

function LuaCHRoomContentView:OnApplyMicButtonClick()
    LuaClubHouseMgr:ChangeHandsUpStatus(not LuaClubHouseMgr:IsMyselfHandsUp())
end

function LuaCHRoomContentView:OnAudienceButtonClick()
    LuaClubHouseMgr:QueryAllListenersInRoom()
    CUIManager.ShowUI(CLuaUIResources.CHAudienceListWnd)
end

function LuaCHRoomContentView:OnGivePresentTopsClick()
    CUIManager.ShowUI(CLuaUIResources.CHDayRankWnd)
end

function LuaCHRoomContentView:OnGivePresentButtonClick()
    CUIManager.ShowUI(CLuaUIResources.CHChooseGiftWnd)
end

function LuaCHRoomContentView:OnHistoryButtonClick()
    if self.m_ChatHistoryMenu ~= nil then
        self.m_ChatHistoryMenu:Init()
    end
end

function LuaCHRoomContentView:OnEmoticonButtonClick(go)
    CChatInputMgr.ShowChatInputWnd(CChatInputMgrEParentType.ClubHouse, self.m_DefaultChatInputListener, go, EChatPanel.Undefined, 0, 0)
end

function LuaCHRoomContentView:OnNewMsgButtonClick()
    self:SetNewMsgButtonVisible(false)
    self.m_RoomChatSimpleTableView:LoadDataFromTail()
end

function LuaCHRoomContentView:OnRoomChatSimpleTableViewClipMove()
    if self:LastChildIsVisible() then
        self:SetNewMsgButtonVisible(false)
    end
end

function LuaCHRoomContentView:SetNewMsgButtonVisible(visible) 
    if visible then
        if not self.m_NewMsgButton.gameObject.activeSelf then
            self.m_NewMsgButton.gameObject:SetActive(true)
        end
        self.m_NewMsgButton.Text = SafeStringFormat3(LocalString.GetString("%d条未读消息"), self.m_UnReadMsgCount)
    else
        self.m_UnReadMsgCount = 0
        if self.m_NewMsgButton.gameObject.activeSelf then
            self.m_NewMsgButton.gameObject:SetActive(false)
        end
    end
end