local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local QnAddSubAndInputButton=import "L10.UI.QnAddSubAndInputButton"
local CItemMgr = import "L10.Game.CItemMgr"
local QnTableView=import "L10.UI.QnTableView"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local UILongPressButton = import "L10.UI.UILongPressButton"
local Item_Item = import "L10.Game.Item_Item"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local CUITexture = import "L10.UI.CUITexture"
local CommonDefs = import "L10.Game.CommonDefs"
local QnButton = import "L10.UI.QnButton"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"

CLuaCityWarExchangeWnd =class()
CLuaCityWarExchangeWnd.Path = "ui/citywar/LuaCityWarExchangeWnd"

RegistClassMember(CLuaCityWarExchangeWnd,"m_TableView")
RegistClassMember(CLuaCityWarExchangeWnd,"m_ItemTable")
RegistClassMember(CLuaCityWarExchangeWnd,"m_NumButton")
RegistClassMember(CLuaCityWarExchangeWnd,"m_SelectedRow")
RegistClassMember(CLuaCityWarExchangeWnd,"m_CostLabel1")
RegistClassMember(CLuaCityWarExchangeWnd,"m_CostLabel2")
RegistClassMember(CLuaCityWarExchangeWnd,"m_Formula1")
RegistClassMember(CLuaCityWarExchangeWnd,"m_Formula2")
RegistClassMember(CLuaCityWarExchangeWnd,"m_ExchangeButton")
RegistClassMember(CLuaCityWarExchangeWnd, "m_GotLabel")
RegistClassMember(CLuaCityWarExchangeWnd, "m_TipLabel")
RegistClassMember(CLuaCityWarExchangeWnd, "m_LeftCount")

CLuaCityWarExchangeWnd.TempPlayDataKey = 153

function CLuaCityWarExchangeWnd:Init()
    self.m_GotLabel = self.transform:Find("Center/Info/GotLabel"):GetComponent(typeof(UILabel))
    self.m_TipLabel = self.transform:Find("Center/Info/LeftTipLabel"):GetComponent(typeof(UILabel))

    self.m_ExchangeButton = FindChild(self.transform,"SubmitButton"):GetComponent(typeof(QnButton))
    UIEventListener.Get(self.m_ExchangeButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickSubmitButton(go)
    end)

    self.m_NumButton=FindChild(self.transform,"QnIncreseAndDecreaseButton"):GetComponent(typeof(QnAddSubAndInputButton))
    self.m_NumButton:SetMinMax(0,0,1)
    self.m_NumButton.onValueChanged = DelegateFactory.Action_uint(function (value)
        self:OnNumChanged(value)
    end)

    self:InitLeftCount()
    self:InitTable(true)
end

function CLuaCityWarExchangeWnd:InitTable(bFirst)
    self.m_ItemTable={}
	CityWar_ZiCaiExchange.Foreach(function(k, v)
        if v.Status == 0 then
            local bindCount,notBindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(k)
            if bindCount > 0 then
                table.insert(self.m_ItemTable, {id = k, bBind = true, cnt = bindCount})
            end
            if notBindCount > 0 then
                table.insert(self.m_ItemTable, {id = k, bBind = false, cnt = notBindCount})
            end
        end
    end)
    if #self.m_ItemTable <= 0 then
        if bFirst then
            g_MessageMgr:ShowMessage("KFCZ_EXCHANGE_ZICAI_NO_NEED_ITEM")
        end
        CUIManager.CloseUI(CLuaUIResources.CityWarExchangeWnd)
        return
    end
    self.m_TableView = FindChild(self.transform,"ScrollView"):GetComponent(typeof(QnTableView))

    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() return #self.m_ItemTable end,
        function(item,row)
            self:InitItem(item,row)

        end)
    self.m_TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        self.m_SelectedRow = row + 1
        self:InitDetails(self.m_SelectedRow)
    end)
    self.m_SelectedRow = 1
    self.m_TableView:SetSelectRow(0,true)
    self.m_TableView:ReloadData(false, false)
end

function CLuaCityWarExchangeWnd:InitDetails(row)
    local id = self.m_ItemTable[row].id
    local tf = FindChild(self.transform,"SelectedItem")

    local icon=FindChild(tf,"IconTexture"):GetComponent(typeof(CUITexture))
    local nameLabel = FindChild(tf,"NameLabel"):GetComponent(typeof(UILabel))

    local item = Item_Item.GetData(id)
    if item then
        icon:LoadMaterial(item.Icon)
        nameLabel.text = item.Name
    else
      local equip = EquipmentTemplate_Equip.GetData(id)
      if equip then
        icon:LoadMaterial(equip.Icon)
        nameLabel.text = equip.Name
      end
    end

    self:InitSelectedItemCount()
end

function CLuaCityWarExchangeWnd:OnClickSubmitButton(go)
    local curRow = self.m_ItemTable[self.m_SelectedRow]
    local pos, itemId, ret = 0, "", false
    if curRow.bBind then
        ret, pos, itemId = CItemMgr.Inst:FindItemByTemplateIdPriorToBinded(EnumItemPlace.Bag, curRow.id)
    else
        ret, pos, itemId = CItemMgr.Inst:FindItemByTemplateIdPriorToNotBinded(EnumItemPlace.Bag, curRow.id)
    end
    if ret and CItemMgr.Inst:GetById(itemId).IsBinded == curRow.bBind then
        ret = true
    else
        ret = false
    end

    if ret then
        Gac2Gas.ExchangeCityWarGuildZiCai(EnumItemPlace.Bag, pos, itemId, self.m_NumButton:GetValue())
    else
        self:InitTable(false)
    end
end

function CLuaCityWarExchangeWnd:OnNumChanged(value)
    self.m_GotLabel.text = value * CityWar_ZiCaiExchange.GetData(self.m_ItemTable[self.m_SelectedRow].id).ZiCaiNum
end

function CLuaCityWarExchangeWnd:InitSelectedItemCount()
    self:RefreshMaxNum()
    self.m_NumButton:SetValue(1)
    self:OnNumChanged(1)
end

function CLuaCityWarExchangeWnd:RefreshMaxNum()
    local maxCnt = math.ceil(self.m_LeftCount / CityWar_ZiCaiExchange.GetData(self.m_ItemTable[self.m_SelectedRow].id).ZiCaiNum)
    maxCnt = math.min(maxCnt,self.m_ItemTable[self.m_SelectedRow].cnt)
    self.m_NumButton:SetMinMax(1, maxCnt, 1)
end

function CLuaCityWarExchangeWnd:InitItem(item,row)
    local tf = item.transform
    local go = item.gameObject

    local id = self.m_ItemTable[row+1].id
    FindChild(tf,"BindSprite").gameObject:SetActive(self.m_ItemTable[row+1].bBind)

    if not CClientMainPlayer.Inst then return end
    local commonItem
    local bagSize = CClientMainPlayer.Inst.ItemProp:GetPlaceSize(EnumItemPlace.Bag)
    for i = 1, bagSize do
      local itemId = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
      if itemId then
        commonItem = CItemMgr.Inst:GetById(itemId)
        if commonItem and commonItem.TemplateId == id and commonItem.IsBinded == self.m_ItemTable[row + 1].bBind then
          break
        end
      end
    end
    local icon=FindChild(tf,"Icon"):GetComponent(typeof(CUITexture))
    icon:LoadMaterial(commonItem.Icon)

    local cnt = self.m_ItemTable[row + 1].cnt
    FindChild(tf, "CountLabel"):GetComponent(typeof(UILabel)).text = cnt

    local longPressCmp = go:GetComponent(typeof(UILongPressButton))
    longPressCmp.OnLongPressDelegate = DelegateFactory.Action(function()
        --CItemInfoMgr.ShowLinkItemTemplateInfo(id, false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
        CItemInfoMgr.ShowLinkItemInfo(commonItem, false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0, 0)
    end)
end

function CLuaCityWarExchangeWnd:InitLeftCount( ... )
    self.m_LeftCount = CLuaCityWarMgr.MaxExchangeCnt - CLuaCityWarMgr.CurExchangeCnt
    if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, CLuaCityWarExchangeWnd.TempPlayDataKey) then
        if CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, CLuaCityWarExchangeWnd.TempPlayDataKey).ExpiredTime > CServerTimeMgr.Inst.timeStamp then
            self.m_LeftCount = CLuaCityWarMgr.MaxExchangeCnt - tonumber(CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, CLuaCityWarExchangeWnd.TempPlayDataKey).Data.StringData)
        end
    end
    self.m_TipLabel.text = SafeStringFormat3(LocalString.GetString("今日还可兑换%d资材"), self.m_LeftCount)
    self.m_ExchangeButton.Enabled = self.m_LeftCount > 0
end

function CLuaCityWarExchangeWnd:UpdateTempPlayDataWithKey( argv )
    if argv[0] ~= CLuaCityWarExchangeWnd.TempPlayDataKey then return end
    self:InitLeftCount()
    self:RefreshMaxNum()
end

function CLuaCityWarExchangeWnd:ExchangeCityWarGuildZiCaiSuccess( ... )
    self:InitTable(false)
end

function CLuaCityWarExchangeWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdateTempPlayDataWithKey", self, "UpdateTempPlayDataWithKey")
    g_ScriptEvent:AddListener("ExchangeCityWarGuildZiCaiSuccess", self, "ExchangeCityWarGuildZiCaiSuccess")
end
function CLuaCityWarExchangeWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateTempPlayDataWithKey", self, "UpdateTempPlayDataWithKey")
    g_ScriptEvent:RemoveListener("ExchangeCityWarGuildZiCaiSuccess", self, "ExchangeCityWarGuildZiCaiSuccess")
end
