-- Auto Generated!!
local Byte = import "System.Byte"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CExpressionEquipSelectItem = import "L10.UI.CExpressionEquipSelectItem"
local CommonDefs = import "L10.Game.CommonDefs"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local EnumGender = import "L10.Game.EnumGender"
local LocalString = import "LocalString"
local UInt32 = import "System.UInt32"
CExpressionEquipSelectItem.m_Init_CS2LuaHook = function (this, appearance, isDisable, isCurrent, isExpired) 
    this.appearance = appearance
    if CClientMainPlayer.Inst ~= nil then
        if CClientMainPlayer.Inst.Gender == EnumGender.Female then
            this.equipIcon:LoadMaterial(appearance.Icon)
        else
            this.equipIcon:LoadMaterial(appearance.Icon2)
        end
    end

    this.nameLabel.text = appearance.Name
    this.disableSprite:SetActive(isDisable)
    this.currentTag:SetActive(isCurrent)

    if not isDisable then
        if appearance.AvailableTime == 0 then
            this.timeLabel.text = LocalString.GetString("[9599A0]永远有效[-]")
        else
            if CClientMainPlayer.Inst == nil or CClientMainPlayer.Inst.PlayProp == nil then
                return
            end
            local appearanceData = CClientMainPlayer.Inst.PlayProp.ExpressionAppearanceData
            if CommonDefs.DictContains(appearanceData, typeof(Byte), appearance.Group) then
                local appearances = CommonDefs.DictGetValue(appearanceData, typeof(Byte), appearance.Group).Appearances
                if CommonDefs.DictContains(appearances, typeof(UInt32), appearance.ID) then
                    local info = CommonDefs.DictGetValue(appearances, typeof(UInt32), appearance.ID)
                    if info.IsExpired == 0 then
                        local time = CServerTimeMgr.ConvertTimeStampToZone8Time(info.ExpireTime)
                        local timeStr = ToStringWrap(time, "yyyy-MM-dd, HH:mm")
                        this.timeLabel.text = System.String.Format(LocalString.GetString("[9599A0]有效期至 {0}[-]"), timeStr)
                    end
                end
            end
        end
    else
        if isExpired then
            this.timeLabel.text = LocalString.GetString("[FF0000]已过期[-]")
        else
            this.timeLabel.text = LocalString.GetString("[FF0000]未获得[-]")
        end
    end
end
