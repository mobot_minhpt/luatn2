local DelegateFactory = import "DelegateFactory"
local UITexture       = import "UITexture"
local GameObject      = import "UnityEngine.GameObject"
local EnumGender      = import "L10.Game.EnumGender"
local Color           = import "UnityEngine.Color"
local NGUIText        = import "NGUIText"

local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local CPropertyAppearance          = import "L10.Game.CPropertyAppearance"
local CPropertySkill               = import "L10.Game.CPropertySkill"

LuaWeddingEnterSceneWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaWeddingEnterSceneWnd, "GroomModel", "GroomModel", UITexture)
RegistChildComponent(LuaWeddingEnterSceneWnd, "BrideModel", "BrideModel", UITexture)
RegistChildComponent(LuaWeddingEnterSceneWnd, "GroomSelected", "GroomSelected", GameObject)
RegistChildComponent(LuaWeddingEnterSceneWnd, "BrideSelected", "BrideSelected", GameObject)
RegistChildComponent(LuaWeddingEnterSceneWnd, "ConfirmButton", "ConfirmButton", GameObject)
--@endregion RegistChildComponent end

RegistClassMember(LuaWeddingEnterSceneWnd, "groomLabel")
RegistClassMember(LuaWeddingEnterSceneWnd, "brideLabel")

RegistClassMember(LuaWeddingEnterSceneWnd, "groomIdentifier")
RegistClassMember(LuaWeddingEnterSceneWnd, "brideIdentifier")
RegistClassMember(LuaWeddingEnterSceneWnd, "groomRO")
RegistClassMember(LuaWeddingEnterSceneWnd, "brideRO")
RegistClassMember(LuaWeddingEnterSceneWnd, "curSelect")

function LuaWeddingEnterSceneWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.GroomModel.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGroomModelClick()
	end)

	UIEventListener.Get(self.BrideModel.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBrideModelClick()
	end)

	UIEventListener.Get(self.ConfirmButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnConfirmButtonClick()
	end)

    --@endregion EventBind end
	self:InitUIComponents()
end

-- 初始化UI组件
function LuaWeddingEnterSceneWnd:InitUIComponents()
	self.groomLabel = self.transform:Find("GroomLabel"):GetComponent(typeof(UILabel))
	self.brideLabel = self.transform:Find("BrideLabel"):GetComponent(typeof(UILabel))
end

function LuaWeddingEnterSceneWnd:OnDisable()
	CUIManager.DestroyModelTexture(self.groomIdentifier)
	CUIManager.DestroyModelTexture(self.brideIdentifier)
end


function LuaWeddingEnterSceneWnd:Init()
	self:InitSelect()
	self:InitModel()
end

-- 初始化选择
function LuaWeddingEnterSceneWnd:InitSelect()
	self.curSelect = 0
	self:UpdateSelect()
end

-- 更新选择
function LuaWeddingEnterSceneWnd:UpdateSelect()
	self.GroomSelected:SetActive(self.curSelect == 1)
	self.BrideSelected:SetActive(self.curSelect == 2)

	self:UpdateColor()
end

-- 初始化模型
function LuaWeddingEnterSceneWnd:InitModel()
	self.groomIdentifier = SafeStringFormat3("__%s__", tostring(self.GroomModel.gameObject:GetInstanceID()))
	self.brideIdentifier = SafeStringFormat3("__%s__", tostring(self.BrideModel.gameObject:GetInstanceID()))

	local groomClass = LuaWeddingIterationMgr.cardInfo.groomClass
	local brideClass = LuaWeddingIterationMgr.cardInfo.brideClass

	self:CreateModel(self.groomIdentifier, self.GroomModel, EnumGender.Male, groomClass)
	self:CreateModel(self.brideIdentifier, self.BrideModel, EnumGender.Female, brideClass)

	LuaWeddingIterationMgr:SetWeaponVisible(self.groomRO)
	LuaWeddingIterationMgr:SetWeaponVisible(self.brideRO)
end

-- 创建人物模型
function LuaWeddingEnterSceneWnd:CreateModel(identifier, tex, gender, class)
    local loader = self:GetModelLoader(class, gender)

    tex.mainTexture = CUIManager.CreateModelTexture(identifier, loader,
        180, 0.05, -1, 4.66, false, true, 1.0, false, false)
end

-- 生成外观
function LuaWeddingEnterSceneWnd:GetModelLoader(class, gender)
    return LuaDefaultModelTextureLoader.Create(function (ro)
        local appearanceData = CPropertyAppearance.GetDataFromCustom(class, gender,
            CPropertySkill.GetDefaultYingLingStateByGender(gender), nil, nil, 0, 0, 0, 0,
            WeddingIteration_Setting.GetData().WeddingDressHeadId,
            WeddingIteration_Setting.GetData().WeddingDressBodyId,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, nil, nil, nil, nil, 0, 0, 0, 0, 0, 0, nil, 0)

        CClientMainPlayer.LoadResource(ro, appearanceData, true, 1.0,
            0, false, 0, false, 0, false, nil, false)

		if gender == EnumGender.Male then
			self.groomRO = ro
		else
			self.brideRO = ro
		end
    end)
end

-- 设置选中
function LuaWeddingEnterSceneWnd:UpdateColor()
	self.GroomModel.color = Color(1, 1, 1)
	self.BrideModel.color = Color(1, 1, 1)

	self.groomLabel.color = NGUIText.ParseColor24("FFE2C9", 0)
	self.brideLabel.color = NGUIText.ParseColor24("FFE2C9", 0)

	if self.curSelect == 1 then
		self.BrideModel.color = Color(0.5, 0.5, 0.5)
		self.brideLabel.color = NGUIText.ParseColor24("AFAFAF", 0)
	elseif self.curSelect == 2 then
		self.GroomModel.color = Color(0.5, 0.5, 0.5)
		self.groomLabel.color = NGUIText.ParseColor24("AFAFAF", 0)
	end
end


--@region UIEvent

function LuaWeddingEnterSceneWnd:OnConfirmButtonClick()
	if self.curSelect == 0 then
		g_MessageMgr:ShowMessage("WEDDING_NEED_SELECT_GUEST_IDENTITY")
	else
		-- RPC
		CUIManager.CloseUI(CLuaUIResources.WeddingEnterSceneWnd)
		local sceneId = LuaWeddingIterationMgr.cardInfo.sceneId
		local invitationType = LuaWeddingIterationMgr.cardInfo.type
		Gac2Gas.RequestEnterWeddingByNewInvitation(sceneId, invitationType, self.curSelect - 1)
	end
end

function LuaWeddingEnterSceneWnd:OnGroomModelClick()
	self.curSelect = 1
	self:UpdateSelect()
end

function LuaWeddingEnterSceneWnd:OnBrideModelClick()
	self.curSelect = 2
	self:UpdateSelect()
end


--@endregion UIEvent

