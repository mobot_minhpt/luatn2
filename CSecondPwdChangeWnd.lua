-- Auto Generated!!
local CSecondPwdChangeWnd = import "L10.UI.CSecondPwdChangeWnd"
local DelegateFactory = import "DelegateFactory"
local MessageWndManager = import "L10.UI.MessageWndManager"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CSecondPwdChangeWnd.m_Awake_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)
    UIEventListener.Get(this.cancleBtn).onClick = MakeDelegateFromCSFunction(this.OnClickCancleButton, VoidDelegate, this)
    UIEventListener.Get(this.okBtn).onClick = MakeDelegateFromCSFunction(this.OnClickOkButton, VoidDelegate, this)
    UIEventListener.Get(this.forgetPwdBtn).onClick = MakeDelegateFromCSFunction(this.OnClickForgetPwdButton, VoidDelegate, this)
end
CSecondPwdChangeWnd.m_OnClickForgetPwdButton_CS2LuaHook = function (this, go) 
    --忘记密码可通过关联手机并发送手机验证码进行强制解除。。。
    local str = g_MessageMgr:FormatMessage("Forget_Secondary_Password")
    MessageWndManager.ShowOKCancelMessage(str, DelegateFactory.Action(function () 
        Gac2Gas.RequestClearSecondaryPassword()
    end), nil, nil, nil, false)
end
CSecondPwdChangeWnd.m_OnClickOkButton_CS2LuaHook = function (this, go) 
    if this.newPwdInput.value ~= this.newPwdVerifyInput.value then
        g_MessageMgr:ShowMessage("Secondary_Password_Not_Match")
        return
    end
    if CommonDefs.StringLength(this.newPwdInput.value) ~= 6 then
        g_MessageMgr:ShowMessage("Password_Length_Not_Legal")
        return
    end

    Gac2Gas.RequestUpdateSecondaryPassword(this.oldPwdInput.value, this.newPwdInput.value)
end
