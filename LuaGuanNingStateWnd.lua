local Object=import "UnityEngine.Object"
local GameObject = import "UnityEngine.GameObject"
local Vector3 = import "UnityEngine.Vector3"
local CUITexture=import "L10.UI.CUITexture"
local Initialization_Init=import "L10.Game.Initialization_Init"
local GuanNing_Setting=import "L10.Game.GuanNing_Setting"
local Ease = import "DG.Tweening.Ease"
local CScene=import "L10.Game.CScene"
local CClientObjectMgr=import "L10.Game.CClientObjectMgr"
local CClientNpc = import "L10.Game.CClientNpc"
local CChatMgr = import "L10.Game.CChatMgr"
local CChatLinkMgr = import "CChatLinkMgr"
local CChatHelper = import "L10.UI.CChatHelper"
local EChatPanel = import "L10.Game.EChatPanel"
local CGuanNingMgr = import "L10.Game.CGuanNingMgr"
local CRightMenuWnd = import "L10.UI.CRightMenuWnd"
local EnumClass = import "L10.Game.EnumClass"
local EnumGender = import "L10.Game.EnumGender"

CLuaGuanNingStateWnd = class()
RegistClassMember(CLuaGuanNingStateWnd,"huxiaoSliders")
RegistClassMember(CLuaGuanNingStateWnd,"longyinSliders")
RegistClassMember(CLuaGuanNingStateWnd,"nameLabels")
-- RegistClassMember(CLuaGuanNingStateWnd,"isVisual")
RegistClassMember(CLuaGuanNingStateWnd,"contentNode")
RegistClassMember(CLuaGuanNingStateWnd,"bounds")
-- RegistClassMember(CLuaGuanNingStateWnd,"inited")
RegistClassMember(CLuaGuanNingStateWnd,"expandButton")
RegistClassMember(CLuaGuanNingStateWnd,"m_NormalAlert")
RegistClassMember(CLuaGuanNingStateWnd,"m_ZhiHuiAlert")
RegistClassMember(CLuaGuanNingStateWnd,"m_AlertNode")
RegistClassMember(CLuaGuanNingStateWnd, "m_BulletChatView")
RegistClassMember(CLuaGuanNingStateWnd, "m_BulletChatQueue")
RegistClassMember(CLuaGuanNingStateWnd, "m_Template")
RegistClassMember(CLuaGuanNingStateWnd, "m_RecvChatMsgAction")
RegistClassMember(CLuaGuanNingStateWnd, "m_BulletChatCnt")
RegistClassMember(CLuaGuanNingStateWnd, "m_Tweeners")
RegistClassMember(CLuaGuanNingStateWnd, "m_PushWndTweeners")

RegistClassMember(CLuaGuanNingStateWnd, "m_PushList")
RegistClassMember(CLuaGuanNingStateWnd, "m_CurPushIdx")
RegistClassMember(CLuaGuanNingStateWnd, "m_AtkScoreFx")
RegistClassMember(CLuaGuanNingStateWnd, "m_DefScoreFx")
RegistClassMember(CLuaGuanNingStateWnd, "m_TipButton")
RegistClassMember(CLuaGuanNingStateWnd, "m_PushWnd")

RegistChildComponent(CLuaGuanNingStateWnd, "LeftBossNameLabel", "LeftBossNameLabel", UILabel)
RegistChildComponent(CLuaGuanNingStateWnd, "RightBossNameLabel", "RightBossNameLabel", UILabel)
RegistChildComponent(CLuaGuanNingStateWnd, "BossView", "BossView", GameObject)
RegistChildComponent(CLuaGuanNingStateWnd, "LeftBossHpLabel", "LeftBossHpLabel", UILabel)
RegistChildComponent(CLuaGuanNingStateWnd, "RightBossHpLabel", "RightBossHpLabel", UILabel)
RegistChildComponent(CLuaGuanNingStateWnd, "LeftBossSlider", "LeftBossSlider", UISlider)
RegistChildComponent(CLuaGuanNingStateWnd, "RightBossSlider", "RightBossSlider", UISlider)
RegistChildComponent(CLuaGuanNingStateWnd, "BossViewFx", "BossViewFx", CUIFx)

function CLuaGuanNingStateWnd:Awake()
    self.m_Tweeners = {}
    self.m_PushWndTweeners = {}
    self.m_PushList = {}
    self.m_CurPushIdx = 0
    
    self.m_DefScoreFx = self.transform:Find("vfx_cuifx_b_zong"):GetComponent(typeof(UIWidget))
    self.m_AtkScoreFx = self.transform:Find("vfx_cuifx_red_zong"):GetComponent(typeof(UIWidget))
    self.transform:Find("Anchor/Left/Score").gameObject:SetActive(false)
    self.transform:Find("Anchor/Right/Score").gameObject:SetActive(false)

    self.m_PushWnd = self.transform:Find("PushWnd")
    self.m_PushWnd.gameObject:SetActive(false)

    self.m_BulletChatView = self.transform:Find("BulletChatView")
    self.m_BulletChatQueue = {}
    for i = 1, 8 do
        self.m_BulletChatQueue[i] = {}
    end
    self.m_Template = self.m_BulletChatView:Find("Template")
    self.m_Template.gameObject:SetActive(false)
    self.m_RecvChatMsgAction = DelegateFactory.Action_uint(function(channelDef)
        self:OnRecvChatMsg()
    end)
    self.m_BulletChatCnt = 0

    self.huxiaoSliders = {}
    self.huxiaoSliders[1]=FindChild(self.transform,"Slider1"):GetComponent(typeof(UISlider))
    self.huxiaoSliders[2]=FindChild(self.transform,"Slider2"):GetComponent(typeof(UISlider))
    self.huxiaoSliders[3]=FindChild(self.transform,"Slider3"):GetComponent(typeof(UISlider))
    self.longyinSliders = {}
    self.longyinSliders[1]=FindChild(self.transform,"Slider4"):GetComponent(typeof(UISlider))
    self.longyinSliders[2]=FindChild(self.transform,"Slider5"):GetComponent(typeof(UISlider))
    self.longyinSliders[3]=FindChild(self.transform,"Slider6"):GetComponent(typeof(UISlider))

    self.m_ZhiHuiAlert=FindChild(self.transform,"Zhihui_Alert").gameObject
    self.m_ZhiHuiAlert:SetActive(false)
    self.m_NormalAlert=FindChild(self.transform,"Normal_Alert").gameObject
    self.m_NormalAlert:SetActive(false)
    self.m_AlertNode=FindChild(self.transform,"AlertNode").gameObject

    self.m_TipButton = FindChild(self.transform,"TipButton").gameObject

    -- 关宁现在需要显示包裹按钮，需要将快捷指令左移
    local pos = self.m_TipButton.transform.localPosition
    pos.x = -280
    pos.y = -322
    self.m_TipButton.transform.localPosition = pos

    UIEventListener.Get(self.m_TipButton).onClick = DelegateFactory.VoidDelegate(function(go)
        CLuaGuanNingMgr.m_ClickSignalWorldPos=go.transform.position
        CUIManager.ShowUI(CLuaUIResources.GuanNingSignalWnd)
    end)

    local gamePlayId=0
    if CScene.MainScene then
        gamePlayId=CScene.MainScene.GamePlayDesignId
    end
    if gamePlayId==51100208 then--关宁训练场
        self.m_TipButton:SetActive(false)
    else
        self.m_TipButton:SetActive(true)
    end

    --self.nameLabels = ?
        -- self.isVisual = false
    self.contentNode = self.transform:Find("Anchor").gameObject
    self.contentNode:SetActive(true)
    self.BossView:SetActive(false)
    --self.bounds = ?
        -- self.inited = false
    self.expandButton = self.transform:Find("Tip/ExpandButton").gameObject

    UIEventListener.Get(self.expandButton).onClick = DelegateFactory.VoidDelegate(function (p) 
        self.expandButton.transform.localEulerAngles = Vector3(0, 0, 0)
        CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
    end)
end

-- Auto Generated!!
function CLuaGuanNingStateWnd:Init( )
    self.huxiaoSliders[1].value = 0
    self.huxiaoSliders[2].value = 0
    self.huxiaoSliders[3].value = 0
    self.longyinSliders[1].value = 0
    self.longyinSliders[2].value = 0
    self.longyinSliders[3].value = 0
end

function CLuaGuanNingStateWnd:OnEnable( )
    self:OnSyncGnjcZhiHuiInfo(CLuaGuanNingMgr.m_CommanderOpen)
    g_ScriptEvent:AddListener("SyncGnjcZhiHuiInfo", self, "OnSyncGnjcZhiHuiInfo")
    g_ScriptEvent:AddListener("UpdateOccupyHpInfo", self, "RefreshState")
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("SendGuanNingSignal", self, "OnSendGuanNingSignal")
    g_ScriptEvent:AddListener("SyncPVPCommand", self, "OnSyncPVPCommand")
    g_ScriptEvent:AddListener("OnSyncGnjcBossHpInfo", self, "OnSyncGnjcBossHpInfo")
    g_ScriptEvent:AddListener("GuanNingSignalStateChanged", self, "OnGuanNingSignalStateChanged")
    g_ScriptEvent:AddListener("GuanNingBulletChatChanged", self, "OnGuanNingBulletChatChanged")
    g_ScriptEvent:AddListener("SyncGnjcTotalScoreInfo", self, "OnSyncGnjcTotalScoreInfo")
    g_ScriptEvent:AddListener("SendGnjcRecommendDianZanInfo", self, "OnSendGnjcRecommendDianZanInfo")
    EventManager.AddListenerInternal(EnumEventType.RecvChatMsg, self.m_RecvChatMsgAction)
end
function CLuaGuanNingStateWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("SyncGnjcZhiHuiInfo", self, "OnSyncGnjcZhiHuiInfo")
    g_ScriptEvent:RemoveListener("UpdateOccupyHpInfo", self, "RefreshState")
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("SendGuanNingSignal", self, "OnSendGuanNingSignal")
    g_ScriptEvent:RemoveListener("SyncPVPCommand", self, "OnSyncPVPCommand")
    g_ScriptEvent:RemoveListener("OnSyncGnjcBossHpInfo", self, "OnSyncGnjcBossHpInfo")
    g_ScriptEvent:RemoveListener("GuanNingSignalStateChanged", self, "OnGuanNingSignalStateChanged")
    g_ScriptEvent:RemoveListener("GuanNingBulletChatChanged", self, "OnGuanNingBulletChatChanged")
    g_ScriptEvent:RemoveListener("SyncGnjcTotalScoreInfo", self, "OnSyncGnjcTotalScoreInfo")
    g_ScriptEvent:RemoveListener("SendGnjcRecommendDianZanInfo", self, "OnSendGnjcRecommendDianZanInfo")
    EventManager.RemoveListenerInternal(EnumEventType.RecvChatMsg, self.m_RecvChatMsgAction)

    --这个界面不显示的话，那么tip界面肯定也不显示
    if CUIManager.IsLoaded(CUIResources.TopAndRightTipWnd) then
        CUIManager.CloseUI(CUIResources.TopAndRightTipWnd)
    end

    LuaTweenUtils.DOKill(self.transform, false)
end

function CLuaGuanNingStateWnd:OnSyncGnjcZhiHuiInfo(bOpen, attackZhiHuiPlayerId, defendZhiHuiPlayerId, status)
    if not bOpen then return end
    if CLuaGuanNingMgr:IsCommander() then
        self.m_TipButton.transform:Find("ZhiHui").gameObject:SetActive(true)
        self.m_TipButton.transform:Find("Normal").gameObject:SetActive(false)
    else
        self.m_TipButton.transform:Find("ZhiHui").gameObject:SetActive(false)
        self.m_TipButton.transform:Find("Normal").gameObject:SetActive(true)
    end
end

function CLuaGuanNingStateWnd:ShowPushWnd()
    local widget = self.m_PushWnd:GetComponent(typeof(UIWidget))
    local lS = 40
    local lE = -430
    local rS = 450
    local rE = -20
    LuaTweenUtils.SetEase(LuaTweenUtils.TweenInt(lS, lE, 0.5, function (val)
		widget.leftAnchor:Set(1, val)
	end), Ease.OutQuint)
    LuaTweenUtils.SetEase(LuaTweenUtils.TweenInt(rS, rE, 0.5, function (val)
		widget.rightAnchor:Set(1, val)
	end), Ease.OutQuint) 
end

function CLuaGuanNingStateWnd:SwitchPushWnd()
    local idx = #self.m_PushWndTweeners + 1
    self.m_PushWndTweeners[idx] = {}

    local widget = self.m_PushWnd:GetComponent(typeof(UIWidget))
    local bS = -375
    local bE = -375 + 100
    local tS = -265
    local tE = -265 + 100
    self.m_PushWndTweeners[idx][1] = LuaTweenUtils.SetEase(LuaTweenUtils.TweenInt(bS, bE, 0.5, function(val)
        widget.bottomAnchor:Set(1, val)
    end), Ease.InQuart)
    self.m_PushWndTweeners[idx][2] = LuaTweenUtils.SetEase(LuaTweenUtils.TweenInt(tS, tE, 0.5, function(val)
        widget.topAnchor:Set(1, val)
    end), Ease.InQuart)
    self.m_PushWndTweeners[idx][3] = LuaTweenUtils.TweenAlpha(widget, 1, 0, 0.55)  

    LuaTweenUtils.OnComplete(self.m_PushWndTweeners[idx][1], function()
        widget.bottomAnchor:Set(1, -375)
    end)
    LuaTweenUtils.OnComplete(self.m_PushWndTweeners[idx][2], function()
        widget.topAnchor:Set(1, -265)
    end)
    LuaTweenUtils.OnComplete(self.m_PushWndTweeners[idx][3], function()
        widget.leftAnchor:Set(1, 40)
        widget.rightAnchor:Set(1, 450)
        widget.alpha = 1
        self:ShowPushWnd()
        self:PushNextPlayer()
    end)
end

function CLuaGuanNingStateWnd:PushNextPlayer()
    if not CGuanNingMgr.Inst.inGnjc then return end
    
    if self.m_PushWnd.gameObject.activeSelf == false then return end

    self.m_CurPushIdx = self.m_CurPushIdx + 1
    if self.m_CurPushIdx > #self.m_PushList then 
        self.m_PushWnd.gameObject:SetActive(false)
        return
    end
    
    local p = self.m_PushList[self.m_CurPushIdx]
    local cls = CommonDefs.ConvertIntToEnum(typeof(EnumClass), p.class)
    local gender = CommonDefs.ConvertIntToEnum(typeof(EnumGender), p.gender)
    self.m_PushWnd:Find("Player/Avatar"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(cls, gender, -1), false)
    self.m_PushWnd:Find("Player/RankIcon"):GetComponent(typeof(UISprite)).spriteName = "headinfownd_jiashang_0"..p.rank
    self.m_PushWnd:Find("NameLabel"):GetComponent(typeof(UILabel)).text = p.name

    CommonDefs.AddOnClickListener(self.m_PushWnd:Find("CloseButton").gameObject, DelegateFactory.Action_GameObject(function()
        self.m_PushWnd.gameObject:SetActive(false)
    end), false) 

    CommonDefs.AddOnClickListener(self.m_PushWnd:Find("OkButton").gameObject, DelegateFactory.Action_GameObject(function()
        Gac2Gas.RequestDianZanGnjcOtherPlayer(CLuaGuanNingMgr.m_PlayId, CLuaGuanNingMgr.m_ServerGroupId, tonumber(p.playerId))
        self:SwitchPushWnd()
    end), false) 
end

-- 一局只会收到一次
function CLuaGuanNingStateWnd:OnSendGnjcRecommendDianZanInfo(info)
    self.m_PushList = info
    self.m_CurPushIdx = 0
    
    self:ShowPushWnd()
    self.m_PushWnd.gameObject:SetActive(true)
    RegisterTickOnce(function()
        if CGuanNingMgr.Inst.inGnjc and not CommonDefs.IsNull(self.m_PushWnd) then 
            self.m_PushWnd.gameObject:SetActive(false)
        end
    end, GuanNing_Setting.GetData().CommandEvaluationCloseTime * 1000)

    self:PushNextPlayer()
end

function CLuaGuanNingStateWnd:OnSyncGnjcTotalScoreInfo(attackScore, defendScore)
    CLuaGuanNingMgr.m_HuxiaoScore = defendScore
    CLuaGuanNingMgr.m_LongyinScore = attackScore

    self.transform:Find("Anchor/Left/Score").gameObject:SetActive(true)
    self.transform:Find("Anchor/Right/Score").gameObject:SetActive(true)
    self.transform:Find("Anchor/Left/Score"):GetComponent(typeof(UILabel)).text = defendScore
    self.transform:Find("Anchor/Right/Score"):GetComponent(typeof(UILabel)).text = attackScore
    
    local factor = GuanNing_Setting.GetData().CounterattackNpcCondition
    if attackScore >= 0 and defendScore < 0 then 
        self.m_DefScoreFx.gameObject:SetActive(false)
        self.m_AtkScoreFx.gameObject:SetActive(true)
    elseif attackScore < 0 and defendScore >= 0 then
        self.m_DefScoreFx.gameObject:SetActive(true)
        self.m_AtkScoreFx.gameObject:SetActive(false)
    elseif attackScore >= 0 and defendScore >= 0 then
        --0/0=nan, 1/0=inf; nan compare anything return false, inf > any num
        self.m_DefScoreFx.gameObject:SetActive(attackScore / defendScore < factor)
        self.m_AtkScoreFx.gameObject:SetActive(defendScore / attackScore < factor)
    else
        self.m_DefScoreFx.gameObject:SetActive(attackScore / defendScore > 1/factor)
        self.m_AtkScoreFx.gameObject:SetActive(defendScore / attackScore > 1/factor)
    end
end

function CLuaGuanNingStateWnd:Update()
    if CUIManager.IsLoaded(CUIResources.RightMenuWnd) and CRightMenuWnd.Instance ~= nil then
        self.transform:GetComponent(typeof(UIPanel)).depth = CRightMenuWnd.Instance.transform:GetComponent(typeof(UIPanel)).depth + 1
    end

    if not self.m_DefScoreFx.gameObject.activeSelf and not self.m_AtkScoreFx.gameObject.activeSelf then return end
    local alpha = 1
    if CUIManager.instance.PopUIRoot.childCount > 0 then
        for i = 0, CUIManager.instance.PopUIRoot.childCount-1 do
            local ch = CUIManager.instance.PopUIRoot:GetChild(i)
            if ch then
                local bgMask = ch:Find("_BgMask_")
                if bgMask and bgMask:GetComponent(typeof(UITexture)).alpha == 1 then
                    alpha = 0
                    break
                end
            end
        end
    end
    self.m_DefScoreFx.alpha = alpha
    self.m_AtkScoreFx.alpha = alpha
end

function CLuaGuanNingStateWnd:OnRecvChatMsg()
    local chatMsg = CChatMgr.Inst:GetLatestMsg()
    if chatMsg then
        local channel =  CChatHelper.GetChannelByName(chatMsg.channelName)
        if channel == EChatPanel.Ally and not chatMsg.isGnjcSystemInfo then -- 屏蔽友方频道的系统消息
            self:LaunchBulletChat(chatMsg.message, math.random(1,8), CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == chatMsg.fromUserId)
        end
    end
end

function CLuaGuanNingStateWnd:LaunchBulletChat(text, row, isMy)
    if self.m_BulletChatCnt >= 20 then -- 控制弹幕数量上限
        return
    end
    if string.sub(text, 1, string.len("voice://")) == "voice://" then -- 屏蔽语音消息
        return
    end

    local obj = NGUITools.AddChild(self.m_BulletChatView.gameObject, self.m_Template.gameObject)
    obj:SetActive(true)
    local textLabel = obj.transform:Find("Label"):GetComponent(typeof(UILabel))
    local myWidget = obj.transform:Find("My"):GetComponent(typeof(UIWidget))

    local initialRoot = self.m_BulletChatView:GetChild(row)
    local initialPosX = initialRoot.transform.localPosition.x
    local initialPosY = initialRoot.transform.localPosition.y
    local startMovePosX = initialPosX
    local endMovePosX = - initialPosX - myWidget.width
    local duration = 10
    local dX = (startMovePosX - endMovePosX) / duration

    --调整弹幕长度为三十个字符
    text = NGUIText.StripSymbols(CUICommonDef.TranslateToNGUIText(text))
    local cur = 1
    local cnt = 0
    local len = string.len(text)
    while true do
        local step = 1
        local byteVal = string.byte(text, cur)
        if byteVal > 239 then
            step = 4
        elseif byteVal > 223 then
            step = 3
        elseif byteVal > 191 then
            step = 2
        else
            step = 1
        end
        cnt = cnt + 1
        cur = cur + step
        if cur > len then
            break
        elseif cnt >= 30 then
            text = string.sub(text, 1, cur - 1).."..."
            break
        end
    end
    textLabel.text = text
    myWidget:Update()
    myWidget.gameObject:SetActive(isMy)
    endMovePosX = - initialPosX - myWidget.width
    --新的弹幕要保证在同行弹幕后面
    if #self.m_BulletChatQueue[row] > 0 then 
        local lastObj = self.m_BulletChatQueue[row][#self.m_BulletChatQueue[row]]
        local lastWidget = lastObj.transform:Find("My"):GetComponent(typeof(UIWidget))
        startMovePosX = math.max(startMovePosX, lastObj.transform.localPosition.x + lastWidget.width + 10)
    end
    duration = (startMovePosX - endMovePosX) / dX
    --弹幕移动动画
    obj.transform.localPosition = Vector3(startMovePosX, initialPosY, 0)
    local tweener = LuaTweenUtils.TweenFloat(startMovePosX, endMovePosX, duration, function (val)
		obj.transform.localPosition = Vector3(val, initialPosY, 0)
	end)
    LuaTweenUtils.OnComplete(tweener, function ()
        --弹幕销毁
        if self.m_BulletChatQueue[row][1] and obj == self.m_BulletChatQueue[row][1] then
            self.m_BulletChatCnt = self.m_BulletChatCnt - 1
            table.remove(self.m_BulletChatQueue[row], 1)
        end
        GameObject.Destroy(obj)
	end)
    LuaTweenUtils.SetTarget(tweener,self.transform)
    self.m_BulletChatCnt = self.m_BulletChatCnt + 1
    table.insert(self.m_BulletChatQueue[row], obj)
end

function CLuaGuanNingStateWnd:OnGuanNingSignalStateChanged(open)
    self.m_AlertNode:SetActive(open)
end

function CLuaGuanNingStateWnd:OnGuanNingBulletChatChanged(open)
    self.m_BulletChatView.gameObject:SetActive(open)
end

function CLuaGuanNingStateWnd:OnSyncGnjcBossHpInfo(attackerHp, attackerFullHp, defenderHp, defenderFullHp)
    if not CLuaGuanNingMgr.m_IsSundayPlayOpen then return end
    self.contentNode:SetActive(false)
    self.BossView:SetActive(true)
    self.BossViewFx:LoadFx("fx/ui/prefab/UI_guanning_shuaxin.prefab")
    self.RightBossSlider.value = attackerFullHp > 0 and attackerHp / attackerFullHp or 0
    self.LeftBossSlider.value = defenderFullHp > 0 and defenderHp / defenderFullHp or 0
    self.RightBossHpLabel.text = attackerHp
    self.LeftBossHpLabel.text = defenderHp
end
function CLuaGuanNingStateWnd:OnSyncPVPCommand(class,gender,msgId, playerId, msg)
    local idx = #self.m_Tweeners + 1

    local count=self.m_AlertNode.transform.childCount
    if count>0 then
        for i=1,count do
            local item=self.m_AlertNode.transform:GetChild(i-1)
            LuaUtils.SetLocalPositionY(item,item.localPosition.y+64)
        end
    end

    local isCmder = playerId and CLuaGuanNingMgr:IsPlayerCommander(playerId)

    local go=NGUITools.AddChild(self.m_AlertNode,isCmder and self.m_ZhiHuiAlert or self.m_NormalAlert)
    go:SetActive(true)
    local tf=go.transform
    local label = tf:Find("Node/Label"):GetComponent(typeof(UILabel))
    local icon = tf:Find("Node/Icon"):GetComponent(typeof(CUITexture))
    local designData = GuanNing_QuickCommand.GetData(msgId)
    label.text = msg and msg ~= "" and CUICommonDef.TranslateToNGUIText(msg) or CUICommonDef.TranslateToNGUIText(designData.Content)
    
    UIEventListener.Get(label.gameObject).onClick = DelegateFactory.VoidDelegate(function()
        CChatLinkMgr.ProcessLinkClick(label:GetUrlAtPosition(UICamera.lastWorldPosition), nil)
    end)
    
--[[
    if msgId == 15 then
        local x, y
        local tid = CLuaGuanNingMgr:GetMyForce() == 1 and GuanNing_Setting.GetData().AttackZhiHuiFaZhenNpcId or GuanNing_Setting.GetData().DefendZhiHuiFaZhenNpcId
        CClientObjectMgr.Inst:ForeachObject(DelegateFactory.Action_CClientObject(function(obj)
            if TypeIs(obj, typeof(CClientNpc)) and obj.TemplateId == tid and obj.RO and obj.RO.transform then
                x = obj.RO.transform.position.x
                y = obj.RO.transform.position.z
            end
        end))
        if x and y and CScene.MainScene then
            local msg = CChatMgr.Inst:GetLatestMsg().message
            msg = string.sub(msg, string.find(msg, "[url]={\"type\":\"location\"") - 3)
            label.text = LocalString.GetString("已放置增益法阵")..msg
            --"<link location="..CScene.MainScene.SceneTemplateId..","..CScene.MainScene.SceneName..","..x..","..y..">"
            UIEventListener.Get(label.gameObject).onClick = DelegateFactory.VoidDelegate(function()
                CChatLinkMgr.ProcessLinkClick(label:GetUrlAtPosition(UICamera.lastWorldPosition))
            end)
        end
    end
--]]

    local initData=Initialization_Init.GetData(class*100+gender)
    if initData then
        icon:LoadPortrait(initData.ResName,false)
    end

    local timeOffset = 0

    local texture= tf:Find("Node/Texture"):GetComponent(typeof(CUITexture))
    if isCmder then
        texture:LoadMaterial(CLuaGuanNingMgr:GetMyForce() == 0 
            and "UI/Texture/Transparent/Material/guanningcommandwnd_icon_zhihui_blue.mat"
            or "UI/Texture/Transparent/Material/guanningcommandwnd_icon_zhihui_red.mat")
        --LuaUtils.SetLocalPositionX(label.transform,114)
        Object.Destroy(go,5+1)
        timeOffset = 3
    else
        --tf:Find("Bg"):GetComponent(typeof(UISprite)).color = NGUIText.ParseColor24("3a83c0", 0)
        --tf:Find("Node/Sprite"):GetComponent(typeof(UITexture)).color = NGUIText.ParseColor24("144468", 0)
        --tf:Find("Bg/Texture").gameObject:SetActive(false)
        if designData and designData.Icon then
            texture:LoadMaterial(designData.Icon)
            --LuaUtils.SetLocalPositionX(label.transform,114)
        else
            texture:Clear()
            --LuaUtils.SetLocalPositionX(label.transform,39)
        end
        Object.Destroy(go,2+1)
    end

    local node=tf:Find("Node")
    local bg=tf:Find("Bg")
    
    if isCmder then
        local reply = tf:Find("Node/Reply")
        if playerId and CClientMainPlayer.Inst and playerId == CClientMainPlayer.Inst.Id then
            reply.gameObject:SetActive(false)
        else
            reply.gameObject:SetActive(true)
            reply:GetComponent(typeof(UITexture)).enabled = false
            CommonDefs.AddOnClickListener(reply:Find("Clicked").gameObject, 
                DelegateFactory.Action_GameObject(function()
                    reply:GetComponent(typeof(UITexture)).enabled = true
                    reply:Find("Click_Motion").gameObject:SetActive(true)

                    if self.m_Tweeners[idx] then
                        for i = 1, #self.m_Tweeners[idx] do
                            LuaTweenUtils.Kill(self.m_Tweeners[idx][i], false)
                        end
                        self.m_Tweeners[idx] = {}
                    end

                    LuaTweenUtils.TweenAlpha(node,1,0,0.6)
                    LuaTweenUtils.TweenAlpha(bg,0.75,0,0.6)
                    LuaTweenUtils.SetEase(LuaTweenUtils.TweenPositionY(go.transform,200,0.6),Ease.InExpo)
                    local replyContent = GuanNing_QuickCommand.GetData(msgId).ReplyContent
                    local myForce = CLuaGuanNingMgr:GetMyForce()
                    if msgId == 17 or msgId == 18 then
                        if myForce == 0 and CLuaGuanNingMgr.m_HuxiaoScore < CLuaGuanNingMgr.m_LongyinScore then
                            replyContent = GuanNing_Setting.GetData().HuXiaoEncourageFightingReplaceContent
                        elseif myForce == 1 and CLuaGuanNingMgr.m_LongyinScore < CLuaGuanNingMgr.m_HuxiaoScore then
                            replyContent = GuanNing_Setting.GetData().LongYinEncourageFightingReplaceContent
                        end
                    end
                    CChatHelper.SendMsg(EChatPanel.Ally, CUICommonDef.TranslateToNGUIText(replyContent), 0)
                end), false)
        end
    end
    

    --总时长2s

    self.m_Tweeners[idx] = {}

    self.m_Tweeners[idx][#self.m_Tweeners[idx] + 1] = LuaTweenUtils.SetEase(LuaTweenUtils.TweenScale(go.transform, Vector3(0.5,0.5,1),Vector3(1,1,1),0.3), Ease.OutBack)
    self.m_Tweeners[idx][#self.m_Tweeners[idx] + 1] = LuaTweenUtils.TweenAlpha(node, 0,1,0.3)

    self.m_Tweeners[idx][#self.m_Tweeners[idx] + 1] = LuaTweenUtils.SetDelay(LuaTweenUtils.TweenAlpha(label.transform, 0,1,0.1),0.1)
    
    self.m_Tweeners[idx][#self.m_Tweeners[idx] + 1] = LuaTweenUtils.TweenAlpha(bg, 0,0.75,0.3)
    --local sprite=tf:Find("Sprite")
    --LuaTweenUtils.TweenAlpha(sprite, 0,0.3,0.3)

    self.m_Tweeners[idx][#self.m_Tweeners[idx] + 1] = LuaTweenUtils.TweenScale(bg, Vector3(0,0,1),Vector3(1,1,1),0.1)

    self.m_Tweeners[idx][#self.m_Tweeners[idx] + 1] = LuaTweenUtils.SetDelay(
            LuaTweenUtils.TweenAlpha(node,1,0,0.6),1.7+timeOffset)
            
    self.m_Tweeners[idx][#self.m_Tweeners[idx] + 1] = LuaTweenUtils.SetDelay(
        LuaTweenUtils.TweenAlpha(bg,0.75,0,0.6),1.7+timeOffset)
        
    --LuaTweenUtils.SetDelay(
    --    LuaTweenUtils.TweenAlpha(sprite,0.3,0,0.6),1.7+timeOffset)
            
    self.m_Tweeners[idx][#self.m_Tweeners[idx] + 1] = LuaTweenUtils.SetDelay(
        LuaTweenUtils.SetEase(
            LuaTweenUtils.TweenPositionY(go.transform,200,0.6),Ease.InExpo),1.7+timeOffset)
end

function CLuaGuanNingStateWnd:OnHideTopAndRightTipWnd( )
    self.expandButton.transform.localEulerAngles = Vector3(0, 0, 180)
end
function CLuaGuanNingStateWnd:RefreshState( engineIdList, hpList) 
    for i = 0, 2 do
        local id = engineIdList[i+1]
        local j = 0
        -- Array.IndexOf<uint>(CGuanNingMgr.Inst.occupyNpcIds, id, 0);
        local npcs = GuanNing_Setting.GetData().OccupyNpc
        for k=1,npcs.Length do
            if npcs[k-1]==id then
                j=k
                break
            end
        end

        if j > 0 then
            self.huxiaoSliders[j].value = hpList[i+1] / 100
            self.longyinSliders[j].value = 1 - self.huxiaoSliders[j].value
        end
    end
end


function CLuaGuanNingStateWnd:GetGuideGo(methodName)
	if methodName == "GetSignalBtn" then
		return FindChild(self.transform,"TipButton").gameObject
	end
	return nil
end

return CLuaGuanNingStateWnd
