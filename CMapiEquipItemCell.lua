-- Auto Generated!!
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local CItemMgr = import "L10.Game.CItemMgr"
local CLingfuBaptizeWnd = import "L10.UI.CLingfuBaptizeWnd"
local CLingfuQianghuaWnd = import "L10.UI.CLingfuQianghuaWnd"
local CLingfuView = import "L10.UI.CLingfuView"
local CMapiEquipItemCell = import "L10.UI.CMapiEquipItemCell"
local CommonDefs = import "L10.Game.CommonDefs"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local DelegateFactory = import "DelegateFactory"
local EnumLingfuBaptizeType = import "L10.UI.EnumLingfuBaptizeType"
local Gac2Gas = import "L10.Game.Gac2Gas"
local Item_Item = import "L10.Game.Item_Item"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local StringActionKeyValuePair = import "L10.Game.StringActionKeyValuePair"
local ZuoQi_MapiUpgrade = import "L10.Game.ZuoQi_MapiUpgrade"
local ZuoQi_Setting = import "L10.Game.ZuoQi_Setting"
CMapiEquipItemCell.m_Init_String_EnumMapiLingfuPos_String_Boolean_String_Action_Color_CS2LuaHook = function (this, zuoqiId, pos, equipId, bLocked, itemIcon, act, c) 
    this.mCurEquipPos = pos
    this.mCurZuoqiId = zuoqiId
    this.mCurEquipId = equipId

    this.lockSprite:SetActive(bLocked)
    this.texture.gameObject:SetActive(not bLocked)
    if not bLocked then
        this.texture:LoadMaterial(itemIcon)
    end

    this.qualitySprite.color = c

    this.clickAction = act
end
CMapiEquipItemCell.m_OnLockSpriteClicked_CS2LuaHook = function (this, go) 
    local zqdata = CZuoQiMgr.Inst:GetZuoQiById(CLingfuView.sCurZuoQiId)
    if not (zqdata ~= nil and zqdata.mapiInfo ~= nil and CZuoQiMgr.IsMapiTemplateId(zqdata.templateId)) then
        return
    end

    local openHoleNum = 0
    local upgradeData = ZuoQi_MapiUpgrade.GetData(zqdata.mapiInfo.Level)
    if upgradeData ~= nil then
        openHoleNum = upgradeData.Hole
    end
    if openHoleNum < EnumToInt(this.mCurEquipPos) then
        local gradeNeed = zqdata.mapiInfo.Level + 1
        for i = zqdata.mapiInfo.Level + 1, 99 do
            local newUpgradeData = ZuoQi_MapiUpgrade.GetData(i)
            if newUpgradeData == nil then
                break
            else
                if newUpgradeData.Hole >= EnumToInt(this.mCurEquipPos) then
                    gradeNeed = i
                    break
                end
            end
        end
        g_MessageMgr:ShowMessage("Mapi_Equip_Lock_Need_Mapi_Level", gradeNeed)
        return
    end
end
CMapiEquipItemCell.m_RequestFixLingfu_CS2LuaHook = function (this, equipId) 
    if CLingfuView.sCurZuoQiId ~= nil and not CZuoQiMgr.Inst:CheckMapiNotInMajiu(CLingfuView.sCurZuoQiId) then
        return
    end
    local item = CItemMgr.Inst:GetById(equipId)
    if item ~= nil then
        local data = item.Item.LingfuItemInfo
        if data ~= nil then
            local setting = ZuoQi_Setting.GetData()
            local fullDuration = setting.LingfuDurability[data.Quality - 1]
            local duration = data.Duration
            local silverNeed = math.floor(math.floor((fullDuration - duration) * setting.DurabilityFixCost))
            if silverNeed > 0 then
                local itemdata = Item_Item.GetData(item.TemplateId)
                local msg = g_MessageMgr:FormatMessage("Mapi_Fix_Lingfu_Confirm", silverNeed, itemdata.Name)
                MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
                    local zuoqiId = CLingfuView.sCurZuoQiId
                    if zuoqiId == nil then
                        zuoqiId = ""
                    end
                    Gac2Gas.RequestFixMapiLingfu(equipId, zuoqiId)
                end), nil, nil, nil, false)
            else
                g_MessageMgr:ShowMessage("Mapi_Fix_Lingfu_Already_Full")
            end
        end
    end
    CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
end
CMapiEquipItemCell.m_RequestQianghuaLingfu_CS2LuaHook = function (this, equipId) 
    if CLingfuView.sCurZuoQiId ~= nil and not CZuoQiMgr.Inst:CheckMapiNotInMajiu(CLingfuView.sCurZuoQiId) then
        return
    end
    CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)

    CLingfuQianghuaWnd.sQianghuaLingfuId = equipId
    CUIManager.ShowUI(CUIResources.LingfuQianghuaWnd)
end
CMapiEquipItemCell.m_GetActionPairs_CS2LuaHook = function (this, itemId, templateId) 


    local actionPairs = CreateFromClass(MakeGenericClass(List, StringActionKeyValuePair))
    local xiexiaAction = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("卸下"), DelegateFactory.Action(function () 
        this:PutBackLingfuEquip(itemId)
    end))
    local xilianCitiaoAction = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("洗炼词条"), DelegateFactory.Action(function () 
        if CLingfuView.sCurZuoQiId ~= nil and not CZuoQiMgr.Inst:CheckMapiNotInMajiu(CLingfuView.sCurZuoQiId) then
            return
        end
        CLingfuBaptizeWnd.sCurBaptizeLingfuId = itemId
        CLingfuBaptizeWnd.sCurBaptizeZuoQiId = CLingfuView.sCurZuoQiId
        CLingfuBaptizeWnd.sType = EnumLingfuBaptizeType.eWord
        CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
        CUIManager.ShowUI(CUIResources.LingfuBaptizeWnd)
    end))
    local xilianTaozhuangAction = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("洗炼套装"), DelegateFactory.Action(function () 
        if CLingfuView.sCurZuoQiId ~= nil and not CZuoQiMgr.Inst:CheckMapiNotInMajiu(CLingfuView.sCurZuoQiId) then
            return
        end
        CLingfuBaptizeWnd.sCurBaptizeLingfuId = itemId
        CLingfuBaptizeWnd.sCurBaptizeZuoQiId = CLingfuView.sCurZuoQiId
        CLingfuBaptizeWnd.sType = EnumLingfuBaptizeType.eTaozhuang
        CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
        CUIManager.ShowUI(CUIResources.LingfuBaptizeWnd)
    end))
    local fixAction = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("修复"), DelegateFactory.Action(function () 
        this:RequestFixLingfu(itemId)
    end))

    CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), xiexiaAction)
    local item = CItemMgr.Inst:GetById(itemId)
    if item ~= nil and (CZuoQiMgr.GetLingfuPosByTemplateId(item.TemplateId) == 1 or CZuoQiMgr.GetLingfuPosByTemplateId(item.TemplateId) == 3 or CZuoQiMgr.GetLingfuPosByTemplateId(item.TemplateId) == 5) then
        CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), xilianCitiaoAction)
    end
    CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), xilianTaozhuangAction)
    CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), fixAction)


    if item ~= nil and item.Item.LingfuItemInfo ~= nil and item.Item.LingfuItemInfo.Quality >= 3 then
        local qianghuaAction = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("强化"), DelegateFactory.Action(function () 
            this:RequestQianghuaLingfu(itemId)
        end))
        CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), qianghuaAction)
    end
    return actionPairs
end
