-- Auto Generated!!
local CChatHelper = import "L10.UI.CChatHelper"
local CChatInputWnd = import "L10.UI.CChatInputWnd"
local CExpertTeamMgr = import "L10.Game.CExpertTeamMgr"
local CFriendWnd = import "L10.UI.CFriendWnd"
local CGroupIMMgr = import "L10.Game.CGroupIMMgr"
local CIMMgr = import "L10.Game.CIMMgr"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local CJingLingMgr = import "L10.Game.CJingLingMgr"
local CMailMgr = import "L10.Game.CMailMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local Int32 = import "System.Int32"
local PlayerOperationData = import "L10.Game.PlayerOperationData"
local Screen = import "UnityEngine.Screen"
local UILabel = import "UILabel"
local UInt64 = import "System.UInt64"
local UIRoot = import "UIRoot"
CFriendWnd.m_Init_CS2LuaHook = function (this) 

    --tabBar.GetTabGoByIndex(2).SetActive(CSwitchMgr.EnableGroupIM);
    --这里面坑爹的逻辑，群聊在第三个tab但是被显示在第二个tab
    if CSwitchMgr.EnableGroupIM then
        this.tabBar:GetTabGoByIndex(2):SetActive(true)

        --邮件
        Extensions.SetLocalPositionY(this.tabBar:GetTabGoByIndex(1).transform, - 94.9)
        --群聊
        Extensions.SetLocalPositionY(this.tabBar:GetTabGoByIndex(2).transform, 103.46)
        --公告
        Extensions.SetLocalPositionY(this.tabBar:GetTabGoByIndex(3).transform, - 293.26)
        this.tabBar:GetTabGoByIndex(3):SetActive(CSwitchMgr.EnableAnnounceMail)
    else
        --邮件
        Extensions.SetLocalPositionY(this.tabBar:GetTabGoByIndex(1).transform, 103.46)
        --群聊
        this.tabBar:GetTabGoByIndex(2):SetActive(false)
        --公告
        Extensions.SetLocalPositionY(this.tabBar:GetTabGoByIndex(3).transform, - 94.9)
        this.tabBar:GetTabGoByIndex(3):SetActive(CSwitchMgr.EnableAnnounceMail)
    end


    this.tabBar.OnTabChange = MakeDelegateFromCSFunction(this.OnTabChange, MakeGenericClass(Action2, GameObject, Int32), this)

    if CChatHelper.s_ShowMailView then
        this.tabBar:ChangeTab(1, false)
    elseif CChatHelper.s_ShowGroupChat then
        this.tabBar:ChangeTab(2, false)
    elseif not PlayerOperationData.Inst.needChatWithOther and CIMMgr.Inst.ShowMailAlert then
        this.tabBar:ChangeTab(1, false)
    elseif not PlayerOperationData.Inst.needChatWithOther and CMailMgr.Inst.ShowAnnouncementMailAlert then
        this.tabBar:ChangeTab(3, false)
    else
        this.tabBar:ChangeTab(0, false)
    end
    this:UpdateAlert()
    CExpertTeamMgr.Inst:CheckNewAnswer(nil)
end
CFriendWnd.m_OnEnable_CS2LuaHook = function (this) 

    EventManager.AddListenerInternal(EnumEventType.RecvIMMsgInfo, MakeDelegateFromCSFunction(this.OnRecvIMMsgInfo, MakeGenericClass(Action1, UInt64), this))
    EventManager.AddListener(EnumEventType.IMShouldShowAlertValueChange, MakeDelegateFromCSFunction(this.UpdateAlert, Action0, this))
    EventManager.AddListener(EnumEventType.OnJingLingIMReadStatusChanged, MakeDelegateFromCSFunction(this.UpdateAlert, Action0, this))
    EventManager.AddListener(EnumEventType.FriendRequestAlertUpdate, MakeDelegateFromCSFunction(this.UpdateAlert, Action0, this))
    EventManager.AddListener(EnumEventType.UpdateGroupIMAlert, MakeDelegateFromCSFunction(this.UpdateAlert, Action0, this))
    EventManager.AddListener(EnumEventType.ShowAnnouncementMailAlert, MakeDelegateFromCSFunction(this.UpdateAlert, Action0, this))
end
CFriendWnd.m_OnDisable_CS2LuaHook = function (this) 

    EventManager.RemoveListenerInternal(EnumEventType.RecvIMMsgInfo, MakeDelegateFromCSFunction(this.OnRecvIMMsgInfo, MakeGenericClass(Action1, UInt64), this))
    EventManager.RemoveListener(EnumEventType.IMShouldShowAlertValueChange, MakeDelegateFromCSFunction(this.UpdateAlert, Action0, this))
    EventManager.RemoveListener(EnumEventType.OnJingLingIMReadStatusChanged, MakeDelegateFromCSFunction(this.UpdateAlert, Action0, this))
    EventManager.RemoveListener(EnumEventType.FriendRequestAlertUpdate, MakeDelegateFromCSFunction(this.UpdateAlert, Action0, this))
    EventManager.RemoveListener(EnumEventType.UpdateGroupIMAlert, MakeDelegateFromCSFunction(this.UpdateAlert, Action0, this))
    EventManager.RemoveListener(EnumEventType.ShowAnnouncementMailAlert, MakeDelegateFromCSFunction(this.UpdateAlert, Action0, this))
    g_ScriptEvent:BroadcastInLua("OnFriendWndClose")
end
CFriendWnd.m_Update_CS2LuaHook = function (this) 
    if CUIManager.IsLoaded(CIndirectUIResources.ChatInputWnd) and not this.chatInputVisible then
        this.chatInputVisible = true
        --表情输入窗出现
        local rect = CChatInputWnd.Instance.Area
        local scale = UIRoot.GetPixelSizeAdjustment(this.gameObject)
        local virtualScreenWidth = Screen.width * scale
        local virtualScreenHeight = Screen.height * scale
        Extensions.SetLocalPositionY(this.offsetRoot.transform, rect.height * virtualScreenHeight / Screen.height)
        Extensions.SetLocalPositionY(this.wndBgRoot.transform, this.offsetRoot.transform.localPosition.y - 0 + this.origionWndBgRootOffset)
    elseif not CUIManager.IsLoaded(CIndirectUIResources.ChatInputWnd) and this.chatInputVisible then
        this.chatInputVisible = false
        --表情输入窗隐藏
        Extensions.SetLocalPositionY(this.offsetRoot.transform, 0)
        Extensions.SetLocalPositionY(this.wndBgRoot.transform, this.origionWndBgRootOffset)
    end
end
CFriendWnd.m_OnTabChange_CS2LuaHook = function (this, go, index) 
    this.titleLabel.text = string.gsub(CommonDefs.GetComponentInChildren_GameObject_Type(go, typeof(UILabel)).text, "\n", CommonDefs.IS_VN_CLIENT and " " or "")
    if index == 0 then
        this.friendView.gameObject:SetActive(true)
        this.mailView.gameObject:SetActive(false)
        this.groupIMView.gameObject:SetActive(false)
        this.announcementMailView.gameObject:SetActive(false)
        this.friendView:Init()
    elseif index == 1 then
        this.friendView.gameObject:SetActive(false)
        this.mailView.gameObject:SetActive(true)
        this.groupIMView.gameObject:SetActive(false)
        this.announcementMailView.gameObject:SetActive(false)
    elseif index == 2 then
        this.groupIMView.gameObject:SetActive(true)
        this.friendView.gameObject:SetActive(false)
        this.mailView.gameObject:SetActive(false)
        this.announcementMailView.gameObject:SetActive(false)
        this.groupIMView:Init()
    elseif index == 3 then
        this.groupIMView.gameObject:SetActive(false)
        this.friendView.gameObject:SetActive(false)
        this.mailView.gameObject:SetActive(false)
        this.announcementMailView.gameObject:SetActive(true)
        this.announcementMailView:Init()
    end

    --#region Guide
    EventManager.BroadcastInternalForLua(EnumEventType.Guide_ChangeView, {CUIResources.FriendWnd.Name})
    --#endregion
end
CFriendWnd.m_UpdateAlert_CS2LuaHook = function (this) 
    this.friendAlertIcon.Visible = CIMMgr.Inst.ShowIMAlert_ContainSystemMsg or CJingLingMgr.Inst.ExistsUnreadMsgs or CIMMgr.Inst.ShouldShowFriendRequestAlert
    this.mailAlertIcon.Visible = CIMMgr.Inst.ShowMailAlert
    this.groupIMAlertIcon.Visible = CGroupIMMgr.Inst.ShowGroupIMAlert_ContainIgnore
    this.announcementAlertIcon.Visible = CMailMgr.Inst.ShowAnnouncementMailAlert
end
