local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local PopupMenuItemData=import "L10.UI.PopupMenuItemData"
local AlignType=import "L10.UI.CPopupMenuInfoMgr+AlignType"
local CPopupMenuInfoMgr=import "L10.UI.CPopupMenuInfoMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CScene = import "L10.Game.CScene"


LuaGuoQingPvPMgr = {}

function LuaGuoQingPvPMgr.IsInGamePlay()
	if CScene.MainScene then
		local gamePlayId = CScene.MainScene.GamePlayDesignId
		local setting = GuoQing2022_JinLuHunYuanZhan.GetData()
		if gamePlayId==setting.GameplayId then
			return true
		end
	end
	return false
end

function LuaGuoQingPvPMgr:InitAllSkillData(reason,data)
    self.AllTeamSkillInfoData = data
	self.DefendTeamSkillInfoData = {}
	self.AttackTeamSkillInfoData = {}
	for i=1, #data, 12 do
		local datalist = {}
		for j = 0,11 do
			table.insert(datalist,data[i+j])
		end
		local force = data[i + 4]
		if force == 0 then
			table.insert(self.DefendTeamSkillInfoData,datalist)
		else
			table.insert(self.AttackTeamSkillInfoData,datalist)
		end
	end
	if reason == "startplay" then
		CUIManager.CloseUI(CLuaUIResources.GuoQingPvPSkillInfoWnd)
		CUIManager.ShowUI(CLuaUIResources.GuoQingPvPStartWnd)
	else
		CUIManager.ShowUI(CLuaUIResources.GuoQingPvPSkillInfoWnd)
	end
end

function LuaGuoQingPvPMgr:InitPlayResult(winForce,SkillInfoData,data)
    self.WinForce = winForce
	self.AllTeamSkillInfoData = SkillInfoData
	self.DefendTeamSkillInfoData = {}
	self.AttackTeamSkillInfoData = {}
	self.ResultInfoData = {}
	for i=1, #SkillInfoData, 12 do
		local datalist = {}
		for j = 0,11 do
			table.insert(datalist,SkillInfoData[i+j])
		end
		local force = SkillInfoData[i + 4]
		if force == 0 then
			table.insert(self.DefendTeamSkillInfoData,datalist)
		else
			table.insert(self.AttackTeamSkillInfoData,datalist)
		end
		-- 12个数组为1组
		-- playerId, name, class, gender, force
		-- 7个skillId， 注意有的可能为0
	end
	
	for i=1, #data, 6 do
		local playerId = data[i]
		self.ResultInfoData[playerId] = {}
		for j = 1,5 do
			table.insert(self.ResultInfoData[playerId],data[i+j])
		end
		-- 6个数据为1组
		-- playerId, dpsFactor, healFactor, underFactor, controlFactor, evaluate
	end
	CUIManager.ShowUI(CLuaUIResources.GuoQingPvPResultWnd)
end


