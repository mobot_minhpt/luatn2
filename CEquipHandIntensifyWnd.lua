-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CEquipHandIntensifyItem = import "L10.UI.CEquipHandIntensifyItem"
local CEquipHandIntensifyWnd = import "L10.UI.CEquipHandIntensifyWnd"
local CEquipmentIntensifyMgr = import "L10.Game.CEquipmentIntensifyMgr"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CGetMoneyMgr = import "L10.UI.CGetMoneyMgr"
local CIntensifyDetailInfo = import "L10.Game.CIntensifyDetailInfo"
local CItemMgr = import "L10.Game.CItemMgr"
local CLogMgr = import "L10.CLogMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CUICommonDef = import "L10.UI.CUICommonDef"
local DelegateFactory = import "DelegateFactory"
local EquipIntensify_Intensify = import "L10.Game.EquipIntensify_Intensify"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local UIEventListener = import "UIEventListener"
CEquipHandIntensifyWnd.m_InitData_CS2LuaHook = function (this) 
    local equipment = CItemMgr.Inst:GetById(CEquipmentProcessMgr.Inst.SelectEquipment.itemId)
    CommonDefs.ListClear(this.m_IntesifyDetailInfoList)
    for i = 0, 19 do
        local info = CreateFromClass(CIntensifyDetailInfo)
        info.Level = i + 1
        local config = EquipIntensify_Intensify.GetData(info.Level)
        if config == nil then
            break
        end
        info.minAddValue = config.Range[0]
        info.maxAddValue = config.Range[config.Range.Length - 1]
        info.addValue = equipment.Equip:GetIntensifyValue(info.Level)
        CommonDefs.ListAdd(this.m_IntesifyDetailInfoList, typeof(CIntensifyDetailInfo), info)
    end

    this.equipmentItem:UpdateData(CEquipmentProcessMgr.Inst.SelectEquipment.itemId)
end
CEquipHandIntensifyWnd.m_UpdateData_CS2LuaHook = function (this) 
    local equipment = CItemMgr.Inst:GetById(CEquipmentProcessMgr.Inst.SelectEquipment.itemId)
    do
        local i = 0
        while i < this.m_IntesifyDetailInfoList.Count do
            local info = this.m_IntesifyDetailInfoList[i]
            info.addValue = equipment.Equip:GetIntensifyValue(info.Level)
            i = i + 1
        end
    end
    this.equipmentItem:UpdateData(CEquipmentProcessMgr.Inst.SelectEquipment.itemId)
end
CEquipHandIntensifyWnd.m_Awake_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = DelegateFactory.VoidDelegate(function (p) 
        this:Close()
    end)
    UIEventListener.Get(this.intensifyBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        if not CEquipmentProcessMgr.Inst:CheckOperationConflict(CEquipmentProcessMgr.Inst.SelectEquipment) then
            return
        end

        local info = CEquipmentIntensifyMgr.Inst.SelectedIntensifyDetail
        if info == nil then
            return
        end
        --如果吴山石不足
        if not this.intensifyCost.wushanshiEnough then
            if info.CostLingYu then
                --自动使用灵玉，然后灵玉不足
                if this.intensifyCost.autoCostLingYu and not this.intensifyCost.jadeEnough then
                    CGetMoneyMgr.Inst:GetLingYu(this.intensifyCost.LingYuCost, this.intensifyCost.TemplateId)
                    return
                end
            end
            if info.CostYuanBao then
                --如果勾选了自动消耗元宝，要处理没有元宝的情况
                if this.intensifyCost.autoCostYuanBao and not this.intensifyCost.yuanBaoEnough then
                    CGetMoneyMgr.Inst:GetYuanBao()
                    return
                end
            end
        end

        CEquipmentIntensifyMgr.Inst:CanIntensify(false, DelegateFactory.Action(function () 
            local level = CEquipmentIntensifyMgr.Inst.SelectedIntensifyDetail.Level
            local place = EnumToInt(CEquipmentProcessMgr.Inst.SelectEquipment.place)
            local pos = CEquipmentProcessMgr.Inst.SelectEquipment.pos
            local itemCount = CEquipmentIntensifyMgr.Inst.PutInItemCount
            --TODO 盛放
            Gac2Gas.RequestCanIntensifyEquipment(place, pos, level, itemCount, false, false, this.intensifyCost.autoCostYuanBao)
        end), this.intensifyCost.autoCostYuanBao)
    end)
end
CEquipHandIntensifyWnd.m_Start_CS2LuaHook = function (this) 
    this.tableView.m_DataSource = this
    this.tableView.OnSelectAtRow = DelegateFactory.Action_int(function (row) 
        local info = this.m_IntesifyDetailInfoList[row]
        this.intensifyCost:SetInfo(info)
        --如果选了1到10级，则玩家可以选择1颗到5颗
        --否则只能选5颗
        CEquipmentIntensifyMgr.Inst.SelectedIntensifyDetail = info

        if info.addValue > 0 then
            this.intensifyLabel.text = LocalString.GetString("补缀")
        else
            this.intensifyLabel.text = LocalString.GetString("强化")
        end

        if info.addValue == info.maxAddValue then
            CUICommonDef.SetActive(this.intensifyBtn, false, true)
        else
            CUICommonDef.SetActive(this.intensifyBtn, true, true)
        end
    end)
    this.tableView:ReloadData(false, false)

    local initRow = 0
    do
        local i = 0
        while i < this.m_IntesifyDetailInfoList.Count do
            local info = this.m_IntesifyDetailInfoList[i]
            if info.addValue < info.maxAddValue then
                initRow = i
                break
            else
                initRow = i
            end
            i = i + 1
        end
    end
    initRow = math.min(19, initRow)
    this.tableView:SetSelectRow(initRow, true)
    this.tableView:ScrollToRow(initRow)

    this:RequestCanIntensifyInfo()
end
CEquipHandIntensifyWnd.m_RequestCanIntensifyInfo_CS2LuaHook = function (this) 
    local lastIndex = 0
    do
        local i = 0
        while i < this.m_IntesifyDetailInfoList.Count do
            local info = this.m_IntesifyDetailInfoList[i]
            if info.addValue == 0 then
                lastIndex = i
                break
            end
            i = i + 1
        end
    end

    local lastInfo = this.m_IntesifyDetailInfoList[lastIndex]
    if lastInfo.addValue == 0 then
        --说明这是新的
        if CEquipmentProcessMgr.Inst.SelectEquipment ~= nil then
            local itemInfo = CEquipmentProcessMgr.Inst.SelectEquipment
            CEquipHandIntensifyItem.s_lastRequestItemId = itemInfo.itemId
            CEquipHandIntensifyItem.s_lastRequestLevel = lastInfo.Level
            CEquipHandIntensifyItem.s_canEquip = false
            Gac2Gas.GetEquipGradeByIntensifyLevel(EnumToInt(itemInfo.place), itemInfo.pos, itemInfo.itemId, lastInfo.Level)
        end
    end
end
CEquipHandIntensifyWnd.m_UpdateEquipIntensifyDetail_CS2LuaHook = function (this, equipmentId) 
    this:UpdateData()
    local count = this.tableView.Count
    do
        local i = 0
        while i < count do
            local cmp = TypeAs(this.tableView:GetItemAtRow(i), typeof(CEquipHandIntensifyItem))
            cmp:UpdateData(this.m_IntesifyDetailInfoList[i])
            i = i + 1
        end
    end
    if count < this.m_IntesifyDetailInfoList.Count then
        local info = this.m_IntesifyDetailInfoList[count - 1]
        if count < 20 then
            if info.addValue > 0 then
                --需要增加一个
                this.tableView:ReloadData(false, false)
                this.tableView:ScrollToEnd()
                this:RequestCanIntensifyInfo()
            end
        end
    end
    --如果当前强化的到达指定级别了，自动跳到下一个可以强化的列
    local detailInfo = CEquipmentIntensifyMgr.Inst.SelectedIntensifyDetail
    if detailInfo ~= nil then
        if detailInfo.maxAddValue == detailInfo.addValue then
            local row = this.tableView.currentSelectRow + 1
            if row < this.m_IntesifyDetailInfoList.Count then
                local info = this.m_IntesifyDetailInfoList[row]
                while info.addValue == info.maxAddValue do
                    CEquipmentIntensifyMgr.Inst.SelectedIntensifyDetail = this.m_IntesifyDetailInfoList[row]
                    row = row + 1
                    if row >= this.m_IntesifyDetailInfoList.Count then
                        break
                    end
                    info = this.m_IntesifyDetailInfoList[row]
                end
            end
            row = math.min(row, this.m_IntesifyDetailInfoList.Count - 1)
            this.tableView:SetSelectRow(row, true)
            this.tableView:ScrollToRow(row)
        end
    end
end
CEquipHandIntensifyWnd.m_OnSendItem_CS2LuaHook = function (this, itemId) 
    local info = CEquipmentIntensifyMgr.Inst.SelectedIntensifyDetail
    local config = EquipIntensify_Intensify.GetData(info.Level)
    local item = CItemMgr.Inst:GetById(itemId)
    --装备刷新
    if CEquipmentProcessMgr.Inst.SelectEquipment.itemId == itemId then
        this:UpdateEquipIntensifyDetail(itemId)
    end
end
CEquipHandIntensifyWnd.m_OnSetItemAt_CS2LuaHook = function (this, place, position, oldItemId, newItemId) 
    local info = CEquipmentIntensifyMgr.Inst.SelectedIntensifyDetail
    local config = EquipIntensify_Intensify.GetData(info.Level)
    if oldItemId == nil and newItemId == nil then
        return
    end
    local default
    if oldItemId ~= nil then
        default = oldItemId
    else
        default = newItemId
    end
    local itemId = default
    this:OnSendItem(itemId)
end
CEquipHandIntensifyWnd.m_OnRequestCanIntensifyEquipmentResult_CS2LuaHook = function (this, can, place, pos, equipNewGrade) 
    if not can then
        return
    end

    if CClientMainPlayer.Inst == nil then
        return
    end
    local equipment = CItemMgr.Inst:GetById(CEquipmentProcessMgr.Inst.SelectEquipment.itemId)

    local playerLevel = CClientMainPlayer.Inst.FinalMaxLevelForEquip
    local level = equipment.Equip.Grade
    -- CEquipmentIntensifyMgr.Inst.SelectedIntensifyDetail.Level;

    --等级提升
    if equipNewGrade > level then
        if equipNewGrade > playerLevel then
            -- 强化后%s使用等级将提升到%s级，高于角色当前等级，确认强化？
            local message = g_MessageMgr:FormatMessage("WUSHAN_UPGRADE_CONFIRM_LEVEL_OVER", equipment.Equip.ColoredDisplayName, equipNewGrade)
            MessageWndManager.ShowOKCancelMessage(message, MakeDelegateFromCSFunction(this.SendIntensify, Action0, this), nil, nil, nil, false)
        else
            --强化后%s使用等级将提升到%s级，确认强化？
            local message = g_MessageMgr:FormatMessage("WUSHAN_UPGRADE_CONFIRM", equipment.Equip.ColoredDisplayName, equipNewGrade)
            MessageWndManager.ShowOKCancelMessage(message, MakeDelegateFromCSFunction(this.SendIntensify, Action0, this), nil, nil, nil, false)
        end
    else
        this:SendIntensify()
    end
end
CEquipHandIntensifyWnd.m_SendIntensify_CS2LuaHook = function (this) 
    local level = CEquipmentIntensifyMgr.Inst.SelectedIntensifyDetail.Level
    local place = EnumToInt(CEquipmentProcessMgr.Inst.SelectEquipment.place)
    local pos = CEquipmentProcessMgr.Inst.SelectEquipment.pos
    local itemCount = CEquipmentIntensifyMgr.Inst.PutInItemCount
    --TODO 盛放
    Gac2Gas.RequestIntensifyEquipment(place, pos, level, itemCount, false, false, CEquipmentIntensifyMgr.Inst.isAutoCostYuanBao)
end
CEquipHandIntensifyWnd.m_NumberOfRows_CS2LuaHook = function (this, view) 
    --不能强化的
    --return 20;
    --不能强化的
    --return 20;
    do
        local i = 0
        while i < this.m_IntesifyDetailInfoList.Count do
            local info = this.m_IntesifyDetailInfoList[i]
            if info.addValue == 0 then
                return i + 1
            end
            i = i + 1
        end
    end
    return this.m_IntesifyDetailInfoList.Count
end
CEquipHandIntensifyWnd.m_GetGuideGo_CS2LuaHook = function (this, methodName) 

    if methodName == "GetIntensifyButton" then
        return this:GetIntensifyButton()
    else
        CLogMgr.LogError(LocalString.GetString("引导数据表填写错误"))
        return nil
    end
end
