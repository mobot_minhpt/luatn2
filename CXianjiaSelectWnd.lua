-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CommonDefs = import "L10.Game.CommonDefs"
local CXianjiaSelectMgr = import "L10.UI.CXianjiaSelectMgr"
local CXianjiaSelectTemplate = import "L10.UI.CXianjiaSelectTemplate"
local CXianjiaSelectWnd = import "L10.UI.CXianjiaSelectWnd"
local EnumEventType = import "EnumEventType"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local IdPartition = import "L10.Game.IdPartition"
local NGUITools = import "NGUITools"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local Talisman_MakeXianJia = import "L10.Game.Talisman_MakeXianJia"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CXianjiaSelectWnd.m_Init_CS2LuaHook = function (this) 
    CommonDefs.ListClear(this.buttonList)
    this.talismanTemplate:SetActive(false)
    Extensions.RemoveAllChildren(this.table.transform)
    local info = Talisman_MakeXianJia.GetData(CXianjiaSelectMgr.itemTemplateId)
    if info ~= nil then
        do
            local i = 0
            while i < info.Talismans.Length do
                local talisman = EquipmentTemplate_Equip.GetData(info.Talismans[i])
                if talisman ~= nil and IdPartition.IdIsTalisman(info.Talismans[i]) then
                    local raceFit = false
                    do
                        local j = 0
                        while j < talisman.Race.Length do
                            if talisman.Race[j] == EnumToInt(CClientMainPlayer.Inst.Class) then
                                raceFit = true
                                break
                            end
                            j = j + 1
                        end
                    end
                    if raceFit then
                        local instance = NGUITools.AddChild(this.table.gameObject, this.talismanTemplate)
                        instance:SetActive(true)
                        CommonDefs.ListAdd(this.buttonList, typeof(GameObject), instance)
                        CommonDefs.GetComponent_GameObject_Type(instance, typeof(CXianjiaSelectTemplate)):Init(talisman)
                        CommonDefs.GetComponent_GameObject_Type(instance, typeof(CXianjiaSelectTemplate)).OnIconClicked = CommonDefs.CombineListner_Action_GameObject(CommonDefs.GetComponent_GameObject_Type(instance, typeof(CXianjiaSelectTemplate)).OnIconClicked, MakeDelegateFromCSFunction(this.OnIconClick, MakeGenericClass(Action1, GameObject), this), true)
                        UIEventListener.Get(instance).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(instance).onClick, MakeDelegateFromCSFunction(this.OnButtonClick, VoidDelegate, this), true)
                    end
                end
                i = i + 1
            end
        end
        this.table:Reposition()
        this.scrollView:ResetPosition()
    end
end
CXianjiaSelectWnd.m_OnIconClick_CS2LuaHook = function (this, go) 

    do
        local i = 0
        while i < this.buttonList.Count do
            CommonDefs.GetComponent_GameObject_Type(this.buttonList[i], typeof(QnSelectableButton)):SetSelected(go == this.buttonList[i], false)
            i = i + 1
        end
    end
end
CXianjiaSelectWnd.m_OnButtonClick_CS2LuaHook = function (this, go) 

    do
        local i = 0
        while i < this.buttonList.Count do
            if go == this.buttonList[i] then
                EventManager.BroadcastInternalForLua(EnumEventType.OnXianjiaSelect, {CommonDefs.GetComponent_GameObject_Type(this.buttonList[i], typeof(CXianjiaSelectTemplate)).templateId})
                this:Close()
                break
            end
            i = i + 1
        end
    end
end
