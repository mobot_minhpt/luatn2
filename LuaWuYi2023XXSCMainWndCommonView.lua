local CButton = import "L10.UI.CButton"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local DelegateFactory  = import "DelegateFactory"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"

LuaWuYi2023XXSCMainWndCommonView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaWuYi2023XXSCMainWndCommonView, "DrawButton", "DrawButton", CButton)
RegistChildComponent(LuaWuYi2023XXSCMainWndCommonView, "CostNumLabel", "CostNumLabel", UILabel)
RegistChildComponent(LuaWuYi2023XXSCMainWndCommonView, "Wheel", "Wheel", GameObject)
RegistChildComponent(LuaWuYi2023XXSCMainWndCommonView, "ItemRoot", "ItemRoot", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaWuYi2023XXSCMainWndCommonView, "m_RoundCount")
RegistClassMember(LuaWuYi2023XXSCMainWndCommonView, "m_TargetAngle")
RegistClassMember(LuaWuYi2023XXSCMainWndCommonView, "m_DeltaSpeed")
RegistClassMember(LuaWuYi2023XXSCMainWndCommonView, "m_Speed")
RegistClassMember(LuaWuYi2023XXSCMainWndCommonView, "m_CurrentAngle")
RegistClassMember(LuaWuYi2023XXSCMainWndCommonView, "m_Itemid2Index")
RegistClassMember(LuaWuYi2023XXSCMainWndCommonView, "m_ShowFxTick")

function LuaWuYi2023XXSCMainWndCommonView:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.DrawButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDrawButtonClick()
	end)


    --@endregion EventBind end
end

function LuaWuYi2023XXSCMainWndCommonView:Start()
	self.m_RoundCount = 2
	self.m_Speed = 20
	self.m_TargetAngle = 1
	self.m_CurrentAngle = 0
	self.m_DeltaSpeed = 0.8
	self.CostNumLabel.text = WuYi2023_XinXiangShiCheng.GetData().CommonConsumeCoins
	self:InitWheelItems()
end

function LuaWuYi2023XXSCMainWndCommonView:InitWheelItems()
	local index = 0
	self.m_Itemid2Index = {}
	WuYi2023_CommonRewards.ForeachKey(function(key)
		local data = WuYi2023_CommonRewards.GetData(key)
		local item = self.ItemRoot.transform:GetChild(index)
		self:InitItem(item, data)
		index = index + 1
		self.m_Itemid2Index[key] = index
	end)
end

function LuaWuYi2023XXSCMainWndCommonView:InitItem(item, data)
	local icon = item:GetComponent(typeof(CUITexture))
	local label = item:Find("Label"):GetComponent(typeof(UILabel))
	local qualitySprite = item:Find("Quality"):GetComponent(typeof(UISprite))

	local itemData = Item_Item.GetData(data.ItemId)

	icon:LoadMaterial(itemData.Icon)
	label.text = data.Amount > 1 and data.Amount or ""
	qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(itemData.Quality)

	UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    CItemInfoMgr.ShowLinkItemTemplateInfo(data.ItemId, false, nil, AlignType.Default, 0, 0, 0, 0)
	end)
end

function LuaWuYi2023XXSCMainWndCommonView:Update()
	if self.m_TargetAngle >= 1 then return end
	self.m_CurrentAngle = self.m_CurrentAngle - self.m_Speed
	self.m_Speed = self.m_Speed - self.m_DeltaSpeed

	if self.m_CurrentAngle <= self.m_TargetAngle then
		self.m_CurrentAngle = self.m_TargetAngle
		self.m_TargetAngle = 1
		self:EndRotate()
	end
	Extensions.SetLocalRotationZ(self.Wheel.transform, self.m_CurrentAngle)
end

function LuaWuYi2023XXSCMainWndCommonView:EndRotate()
	self.DrawButton.Enabled = true
	self:CancelShowFxTick()
	self.m_ShowFxTick = RegisterTickOnce(function()
		LuaWuYi2023Mgr.m_GetRewardWndType = 1
		LuaCommonGetRewardWnd.m_materialName = "gongxihuode"
		LuaCommonGetRewardWnd.m_Reward1List = LuaWuYi2023Mgr.m_CommonResult
		LuaCommonGetRewardWnd.m_Reward2Label = ""
		LuaCommonGetRewardWnd.m_Reward2List = {  }
		LuaCommonGetRewardWnd.m_hint = ""
		LuaCommonGetRewardWnd.m_button = {
			{spriteName="blue", buttonLabel = LocalString.GetString("确定"), clickCB = function() 
				CUIManager.CloseUI(CLuaUIResources.WuYi2023CommonGetRewardWnd)
			end},
		}
		CUIManager.ShowUI(CLuaUIResources.WuYi2023CommonGetRewardWnd)
	end,700)
end

function LuaWuYi2023XXSCMainWndCommonView:CancelShowFxTick()
	if self.m_ShowFxTick then
		UnRegisterTick(self.m_ShowFxTick)
		self.m_ShowFxTick = nil
	end
end

function LuaWuYi2023XXSCMainWndCommonView:OnSyncDrawIndex(index)
	self.m_CurrentAngle = 0
	self.m_TargetAngle = -30 * (index - 1) - self.m_RoundCount * 360
	local n = 100
	self.m_DeltaSpeed = math.abs(self.m_TargetAngle) * 2 / (n * (n + 1))
	self.m_Speed = (n + 1) * self.m_DeltaSpeed
end

function LuaWuYi2023XXSCMainWndCommonView:OnEnable()
	g_ScriptEvent:AddListener("OnXinXiangShiChengCommonResult", self, "OnXinXiangShiChengCommonResult")
end

function LuaWuYi2023XXSCMainWndCommonView:OnDisable()
	g_ScriptEvent:RemoveListener("OnXinXiangShiChengCommonResult", self, "OnXinXiangShiChengCommonResult")
	self:CancelShowFxTick()
end

function LuaWuYi2023XXSCMainWndCommonView:OnXinXiangShiChengCommonResult()
	if not LuaWuYi2023Mgr.m_CommonResult or #LuaWuYi2023Mgr.m_CommonResult == 0 then
		return
	end
	local info = LuaWuYi2023Mgr.m_CommonResult[1]
	local index = self.m_Itemid2Index[info.ItemID]
	self:OnSyncDrawIndex(index)
end
--@region UIEvent

function LuaWuYi2023XXSCMainWndCommonView:OnDrawButtonClick()
	local info = LuaWuYi2023Mgr.m_LotteryInfo
	if info.hasBronze < WuYi2023_XinXiangShiCheng.GetData().CommonConsumeCoins then
		g_MessageMgr:ShowMessage("XinXiangShiCheng_NotEnoughBronzeCoin")
		return 
	end
	Gac2Gas.XinXiangShiChengCommonLottery()
	self.DrawButton.Enabled = false
end


--@endregion UIEvent

