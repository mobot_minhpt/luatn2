local CommonDefs            = import "L10.Game.CommonDefs"
local CServerTimeMgr        = import "L10.Game.CServerTimeMgr"
local CWelfareBonusTemplate = import "L10.UI.CWelfareBonusTemplate"

LuaWeddingAnniversaryGiftWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaWeddingAnniversaryGiftWnd, "awardTemplate")
RegistClassMember(LuaWeddingAnniversaryGiftWnd, "title")
RegistClassMember(LuaWeddingAnniversaryGiftWnd, "grid")
RegistClassMember(LuaWeddingAnniversaryGiftWnd, "scrollView")

function LuaWeddingAnniversaryGiftWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    self:InitUIComponents()
    self:InitActive()
end

-- 初始化UI组件
function LuaWeddingAnniversaryGiftWnd:InitUIComponents()
    local anchor = self.transform:Find("Anchor")

    self.title = anchor:Find("Title"):GetComponent(typeof(UILabel))
    self.scrollView = anchor:Find("ScrollView"):GetComponent(typeof(UIScrollView))
    self.awardTemplate = self.scrollView.transform:Find("AwardTemplate").gameObject
    self.grid = self.scrollView.transform:Find("Grid"):GetComponent(typeof(UIGrid))
end

-- 初始化active
function LuaWeddingAnniversaryGiftWnd:InitActive()
    self.awardTemplate:SetActive(false)
end

function LuaWeddingAnniversaryGiftWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSyncMarriageAnniversaryInfo", self, "OnSyncMarriageAnniInfo")
end

function LuaWeddingAnniversaryGiftWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnSyncMarriageAnniversaryInfo", self, "OnSyncMarriageAnniInfo")
end

function LuaWeddingAnniversaryGiftWnd:OnSyncMarriageAnniInfo()
    self:InitGrid()
end


function LuaWeddingAnniversaryGiftWnd:Init()
    self:InitTitle()
    self:InitGrid()
end

-- 初始化描述文字
function LuaWeddingAnniversaryGiftWnd:InitTitle()
    local marry = CServerTimeMgr.ConvertTimeStampToZone8Time(LuaWeddingIterationMgr.giftInfo.marryTime)
    local now = CServerTimeMgr.Inst:GetZone8Time()
    local days = CServerTimeMgr.Inst:DayDiff(now, marry) + 1
    local title = CommonDefs.String_Format_String_ArrayObject(LocalString.GetString("截至{0}-{1}-{2}, 你与{3}已经携手走过了[c][FFFF00]{4}天"), 
        {now.Year, now.Month, now.Day, LuaWeddingIterationMgr.giftInfo.mateName, days})
    self.title.text = title
end

-- 初始化Grid
function LuaWeddingAnniversaryGiftWnd:InitGrid()
    Extensions.RemoveAllChildren(self.grid.transform)
    WeddingDay_Gift.Foreach(function (key, value) 
        self:InitAward(key)
    end)
    self.grid:Reposition()
    self.scrollView:ResetPosition()
end

-- 初始化奖励
function LuaWeddingAnniversaryGiftWnd:InitAward(key)
    local instance = NGUITools.AddChild(self.grid.gameObject, self.awardTemplate)
    instance:SetActive(true)
    local template = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CWelfareBonusTemplate))
    local giftGot = false
    local time = 0
    local sprite1 = ""
    repeat
        local default = key
        if default == (1) then
            giftGot = LuaWeddingIterationMgr.giftInfo.honeyMoonGiftGot
            time = LuaWeddingIterationMgr.giftInfo.honeyMoonGiftTime
            sprite1 = "wedding_miyue"
            break
        elseif default == (2) then
            giftGot = LuaWeddingIterationMgr.giftInfo.hundredDayGiftGot
            time = LuaWeddingIterationMgr.giftInfo.hundredDayGiftTime
            sprite1 = "wedding_bairi"
            break
        elseif default == (3) then
            giftGot = LuaWeddingIterationMgr.giftInfo.anniversaryGiftGot
            time = LuaWeddingIterationMgr.giftInfo.anniversaryGiftTime
            sprite1 = "wedding_zhounian"
            break
        else
            break
        end
    until 1
    if template ~= nil then
        template:InitMarriageAnniversary(key, self.scrollview, giftGot, time, sprite1)
    end
end

--@region UIEvent

--@endregion UIEvent

