local CUIManager = import "L10.UI.CUIManager"

CLuaWuJianDiYuHeChengWnd = class()
RegistClassMember(CLuaWuJianDiYuHeChengWnd,"m_CloseTick")

function CLuaWuJianDiYuHeChengWnd:Init()
    local count = LuaWuJianDiYuMgr.m_JieTuoGuoNum
    local table = self.transform:Find("Table").gameObject
    local item = self.transform:Find("Item").gameObject
    local bg = self.transform:Find("Background").gameObject
    bg:SetActive(false)
    Extensions.RemoveAllChildren(table.transform)
    item:SetActive(false)
    local delta = 360/count
    for i=1,count do
        local go = NGUITools.AddChild(table,item)
        go:SetActive(true)
        local rot = i*delta
        LuaUtils.SetLocalRotation(go.transform,0,0,rot)
    end
    bg:SetActive(true)

    if(self.m_CloseTick)then
        UnRegisterTick(self.m_CloseTick)
    end
    self.m_CloseTick = RegisterTickOnce(function()
        CUIManager.CloseUI(CLuaUIResources.WuJianDiYuHeChengWnd)
    end,3500)
end

function CLuaWuJianDiYuHeChengWnd:OnDestroy()
    if(self.m_CloseTick)then
        UnRegisterTick(self.m_CloseTick)
        self.m_CloseTick=nil
    end
end
