local UIWidget = import "UIWidget"
local MessageWndManager = import "L10.UI.MessageWndManager"

LuaHaoYiXingCardsComposeResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaHaoYiXingCardsComposeResultWnd, "Template1", "Template1", CCommonLuaScript)
RegistChildComponent(LuaHaoYiXingCardsComposeResultWnd, "Template2", "Template2", CCommonLuaScript)
RegistChildComponent(LuaHaoYiXingCardsComposeResultWnd, "Template3", "Template3", CCommonLuaScript)
RegistChildComponent(LuaHaoYiXingCardsComposeResultWnd, "Template4", "Template4", CCommonLuaScript)
RegistChildComponent(LuaHaoYiXingCardsComposeResultWnd, "Template5", "Template5", CCommonLuaScript)
RegistChildComponent(LuaHaoYiXingCardsComposeResultWnd, "Fx", "Fx", CUIFx)
RegistChildComponent(LuaHaoYiXingCardsComposeResultWnd, "Button", "Button", GameObject)
RegistChildComponent(LuaHaoYiXingCardsComposeResultWnd, "Table","Table",UITable)
RegistChildComponent(LuaHaoYiXingCardsComposeResultWnd, "ItemCellTemplate","ItemCellTemplate",UIWidget)
RegistChildComponent(LuaHaoYiXingCardsComposeResultWnd, "Grid","Grid",UIGrid)
RegistChildComponent(LuaHaoYiXingCardsComposeResultWnd, "Button1", "Button1", GameObject)
RegistChildComponent(LuaHaoYiXingCardsComposeResultWnd, "Button2", "Button2", GameObject)
RegistChildComponent(LuaHaoYiXingCardsComposeResultWnd, "BlankArea", "BlankArea", GameObject)
--@endregion RegistChildComponent end
RegistClassMember(LuaHaoYiXingCardsComposeResultWnd, "m_Tick")

function LuaHaoYiXingCardsComposeResultWnd:Awake()
    if self.Button1 then
        self.Button1:SetActive(false)
        UIEventListener.Get(self.Button1).onClick = DelegateFactory.VoidDelegate(function (p)
            self:CancelTick()
            self.m_Tick = RegisterTickOnce(function ()
                CUIManager.CloseUI(CLuaUIResources.HaoYiXingCardsComposeResultWnd)
            end,100)
        end)
    end
    if self.Button2 then
        self.Button2:SetActive(false)
        UIEventListener.Get(self.Button2).onClick = DelegateFactory.VoidDelegate(function (p)
            local cost = tonumber(SpokesmanTCG_ErHaSetting.GetData("ComposeCardNeedHaiTangCount").Value) * (5 - LuaHaoYiXingMgr.m_FreeTimes)
            if LuaHaoYiXingMgr.m_HaitangCount < cost then
                MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("HaoYiXingCard_Haitanghua_NotEnough"), DelegateFactory.Action(function ()
                    CUIManager.ShowUI(CLuaUIResources.GetHaoYiXingHaiTangWnd)
                end), nil, nil, nil, false)
                return 
            end
            self.Button:SetActive(false)
            if self.Button1 then
                self.Button1:SetActive(false)
            end
            if self.Button2 then
                self.Button2:SetActive(false)
            end
            Gac2Gas.ErHaTCGComposeCard(true)
        end)
    end
    UIEventListener.Get(self.BlankArea).onClick = DelegateFactory.VoidDelegate(function (p)
        self:CancelTick()
        self.Fx:DestroyFx()
        self:ShowCards()
    end)
end

function LuaHaoYiXingCardsComposeResultWnd:Init()
    LuaHaoYiXingMgr.m_NeedSelectHiddenCardTab = false
    self.Template1.gameObject:SetActive(false)
    self.Template2.gameObject:SetActive(false)
    self.Template3.gameObject:SetActive(false)
    self.Template4.gameObject:SetActive(false)
    self.Template5.gameObject:SetActive(false)
    self.ItemCellTemplate.gameObject:SetActive(false)
    if not LuaHaoYiXingMgr.m_CardsComposeResultList then
        CUIManager.CloseUI(CLuaUIResources.HaoYiXingCardsComposeResultWnd)
    end
    self.Button:SetActive(false)
    if self.Button1 then
        self.Button1:SetActive(false)
    end
    if self.Button2 then
        self.Button2:SetActive(false)
    end
    UIEventListener.Get(self.Button).onClick = DelegateFactory.VoidDelegate(function (p)
        self:ShowCards()
        self:CancelTick()
        self.m_Tick = RegisterTickOnce(function ()
            CUIManager.CloseUI(CLuaUIResources.HaoYiXingCardsComposeResultWnd)
        end,100)
    end)
    self:OnSyncErHaTCGComposeResult()
end

function LuaHaoYiXingCardsComposeResultWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSyncErHaTCGComposeResult",self,"OnSyncErHaTCGComposeResult")
end

function LuaHaoYiXingCardsComposeResultWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnSyncErHaTCGComposeResult",self,"OnSyncErHaTCGComposeResult")
    self:CancelTick()
    g_MessageMgr:ShowMessage("HaoYiXingCard_GetCard")
end

function LuaHaoYiXingCardsComposeResultWnd:OnSyncErHaTCGComposeResult()
    self.Template1.gameObject:SetActive(false)
    self.Template2.gameObject:SetActive(false)
    self.Template3.gameObject:SetActive(false)
    self.Template4.gameObject:SetActive(false)
    self.Template5.gameObject:SetActive(false)
    self.ItemCellTemplate.gameObject:SetActive(false)
    self.Button:SetActive(false)
    if self.Button1 then
        self.Button1:SetActive(false)
    end
    if self.Button2 then
        self.Button2:SetActive(false)
    end
    Extensions.RemoveAllChildren(self.Table.transform)
    self.Fx:DestroyFx()
    self.Fx:LoadFx(#LuaHaoYiXingMgr.m_CardsComposeResultList == 1 and
    "Fx/UI/Prefab/UI_CharacterCardHeWnd_HeCheng_01.prefab" or
    "Fx/UI/Prefab/UI_CharacterCardHeWnd_HeCheng_02.prefab")
    self:CancelTick()
    self.m_Tick = RegisterTickOnce(function ()
        self:ShowCards()
    end,3000)
end

function LuaHaoYiXingCardsComposeResultWnd:CancelTick()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
end

function LuaHaoYiXingCardsComposeResultWnd:ShowCards()
    local scripts = { self.Template1,self.Template2, self.Template3,self.Template4,self.Template5}
    self.Button:SetActive(#LuaHaoYiXingMgr.m_CardsComposeResultList == 1)
    if self.Button1 then
        self.Button1:SetActive(#LuaHaoYiXingMgr.m_CardsComposeResultList > 1)
    end
    if self.Button2 then
        self.Button2:SetActive(#LuaHaoYiXingMgr.m_CardsComposeResultList > 1)
    end
    self.Template1.gameObject:SetActive(true)
    self.Template2.gameObject:SetActive(#LuaHaoYiXingMgr.m_CardsComposeResultList >= 3)
    self.Template3.gameObject:SetActive(#LuaHaoYiXingMgr.m_CardsComposeResultList >= 3)
    self.Template4.gameObject:SetActive(#LuaHaoYiXingMgr.m_CardsComposeResultList >= 5)
    self.Template5.gameObject:SetActive(#LuaHaoYiXingMgr.m_CardsComposeResultList >= 5)
    self.Grid:Reposition()
    local templateWidth = self.Grid.cellWidth
    Extensions.SetLocalPositionX(self.Grid.transform, - templateWidth / 2 * (#LuaHaoYiXingMgr.m_CardsComposeResultList - 1))
    for i, t in pairs(LuaHaoYiXingMgr.m_CardsComposeResultList) do
        local id, owner, isnew, pieceCount = t.id, false, t.isnew, 1
        scripts[i]:InitCard(id, owner, isnew, 0, pieceCount,false, true)
        --scripts[i].transform:Find("Fx"):GetComponent(typeof(CUIFx)):LoadFx("Fx/UI/Prefab/UI_CharacterCardHeWnd_HeCheng_01.prefab")
    end
    Extensions.RemoveAllChildren(self.Table.transform)
    for i, t in pairs(LuaHaoYiXingMgr.m_CardsComposeResultRewardsList) do
        local itemTemplateId,count = t.id,t.count
        local data = Item_Item.GetData(itemTemplateId)
        local obj = NGUITools.AddChild(self.Table.gameObject,self.ItemCellTemplate.gameObject)
        obj:SetActive(true)
        obj.transform:Find("NumLabel"):GetComponent(typeof(UILabel)).text = count
        obj.transform:Find("IconTexture"):GetComponent(typeof(CUITexture)):LoadMaterial(data.Icon)
    end
    local cellWidth = self.ItemCellTemplate.width
    local paddingX = self.Table.padding.x
    Extensions.SetLocalPositionX(self.Table.transform, - (cellWidth / 2 + paddingX) * #LuaHaoYiXingMgr.m_CardsComposeResultRewardsList)
    self.Table:Reposition()
end

--@region UIEvent

--@endregion UIEvent

