-- Auto Generated!!
local Boolean = import "System.Boolean"
local CTanBaoWnd = import "L10.UI.CTanBaoWnd"
local LocalString = import "LocalString"
local PlayerSettings = import "L10.Game.PlayerSettings"
CTanBaoWnd.m_Init_CS2LuaHook = function (this) 
    this.m_ApplyButton.Text = LocalString.GetString("启航")
    this.m_ApplyButton.Enabled = true
    this.status = 0
    this.m_TickAction = nil
    this.m_SkipAnimation.Selected = PlayerSettings.SkipTanBaoAnimation
    this.m_SkipAnimation.OnValueChanged = MakeDelegateFromCSFunction(this.OnCheckBoxValueChanged, MakeGenericClass(Action1, Boolean), this)
end
CTanBaoWnd.m_OnButtonClicked_CS2LuaHook = function (this, go) 
    if PlayerSettings.SkipTanBaoAnimation then
        this.status = 1
    end
    repeat
        local default = this.status
        if default == 0 then
            this.m_ApplyButton.Text = LocalString.GetString("停止")
            this.m_ApplyButton.background.spriteName = CTanBaoWnd.STOP_BUTTON_BG
            this.m_InitSpeed = 360
            this.status = 1
            break
        elseif default == 1 then
            this.m_ApplyButton.Enabled = false
            this.m_CloseButton:SetActive(false)
            this:RequestTanBao()
            this.status = 2
            break
        else
            break
        end
    until 1
end
CTanBaoWnd.m_OnChuHaiTanBaoSuccess_CS2LuaHook = function (this, id) 
    this.m_Result = id

    this.m_SkipAnimation.Enabled = false
    if not PlayerSettings.SkipTanBaoAnimation then
        this.m_RandomMore = 2
        local addtionalAngle = 360
        if this.m_ResultAngles[this.m_Result - 1] > this.m_TurnTable.localEulerAngles.z then
            addtionalAngle = this.m_ResultAngles[this.m_Result - 1] - this.m_TurnTable.localEulerAngles.z
        else
            addtionalAngle = 360 + this.m_ResultAngles[this.m_Result - 1] - this.m_TurnTable.localEulerAngles.z
        end
        this.m_TotalAngle = this.m_RandomMore * 360 + addtionalAngle
        this.m_Acceleration = 2 * (this.m_TotalAngle - (this.m_InitSpeed) * this.m_Time) / (this.m_Time * this.m_Time)

        this.m_StartAngle = this:GetAngleIn360(this.m_TurnTable.localEulerAngles.z)
        this.status = 3
    else
        this.status = 5
    end
end
CTanBaoWnd.m_GetAngleIn360_CS2LuaHook = function (this, angle) 
    local result = 0
    if angle >= 0 and angle < 360 then
        result = angle
    elseif angle < 0 then
        result = result % 360
        result = result + 360
    else
        result = result % 360
    end
    return result
end
CTanBaoWnd.m_CancelTick_CS2LuaHook = function (this) 
    if this.m_TickAction ~= nil then
        invoke(this.m_TickAction)
        this.m_TickAction = nil
    end
end
