local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CClientHouseMgr=import "L10.Game.CClientHouseMgr"
local AlignType1 = import "L10.UI.CTooltip+AlignType"
local CBaseWnd = import "L10.UI.CBaseWnd"
local EventDelegate = import "EventDelegate"
local CItemAccessListMgr=import "L10.UI.CItemAccessListMgr"
local EnumMoneyType=import "L10.Game.EnumMoneyType"
local UIToggle=import "UIToggle"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CCurentMoneyCtrl=import "L10.UI.CCurentMoneyCtrl"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local CClientChuanJiaBaoMgr=import "L10.Game.CClientChuanJiaBaoMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CItemMgr = import "L10.Game.CItemMgr"
local CGetMoneyMgr = import "L10.UI.CGetMoneyMgr"
-- local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
-- local CEquipmentBaptizeMgr = import "L10.Game.CEquipmentBaptizeMgr"
local CShopMallMgr=import "L10.UI.CShopMallMgr"
local CChuanjiabaoWordType = import "L10.Game.CChuanjiabaoWordType"


CLuaZhuShaBiXiLianWnd = class()
RegistClassMember(CLuaZhuShaBiXiLianWnd,"m_ItemId")
RegistClassMember(CLuaZhuShaBiXiLianWnd,"closeButton")
RegistClassMember(CLuaZhuShaBiXiLianWnd,"replaceBtn")
RegistClassMember(CLuaZhuShaBiXiLianWnd,"baptizeBtn")
RegistClassMember(CLuaZhuShaBiXiLianWnd,"baptizeCount")
RegistClassMember(CLuaZhuShaBiXiLianWnd,"countLabel")

RegistClassMember(CLuaZhuShaBiXiLianWnd,"m_DescItem")
RegistClassMember(CLuaZhuShaBiXiLianWnd,"m_Star")
RegistClassMember(CLuaZhuShaBiXiLianWnd,"m_YinLiangCost")
RegistClassMember(CLuaZhuShaBiXiLianWnd,"m_LingYuCost")

RegistClassMember(CLuaZhuShaBiXiLianWnd, "m_JingPingGo")
RegistClassMember(CLuaZhuShaBiXiLianWnd, "m_JingPingIcon")
RegistClassMember(CLuaZhuShaBiXiLianWnd, "m_JingPingMask")
RegistClassMember(CLuaZhuShaBiXiLianWnd, "m_JingPingCountLabel")

RegistClassMember(CLuaZhuShaBiXiLianWnd, "m_TitleLabel")

RegistClassMember(CLuaZhuShaBiXiLianWnd, "m_CostJingPingCount")

RegistClassMember(CLuaZhuShaBiXiLianWnd, "m_UseLingYuToggle")
RegistClassMember(CLuaZhuShaBiXiLianWnd, "m_IsBinded")


function CLuaZhuShaBiXiLianWnd:UpdateCount()
    self.countLabel.text = SafeStringFormat3(LocalString.GetString("已洗炼%d次"), self.baptizeCount)
end

function CLuaZhuShaBiXiLianWnd:Init( )
    self.closeButton = self.transform:Find("Wnd_Bg_Primary_Normal/CloseButton").gameObject
    UIEventListener.Get(self.closeButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:TryClose(go)
    end)

    self.needConfirm = false
    self.baptizeCount = 0
    self.countLabel = self.transform:Find("Anchor/EndLoglabel"):GetComponent(typeof(UILabel))
    self:UpdateCount()

    self.achieveTheGoal = false

    self.m_CostJingPingCount = 0

    self.m_Star = self.transform:Find("Anchor/Desc/Star").gameObject
    self.m_Star:SetActive(false)

    local jingpingTf = self.transform:Find("Anchor/Cost/JingPingCost")
    self.m_JingPingGo=jingpingTf.gameObject
    self.m_JingPingIcon = jingpingTf:Find("Icon"):GetComponent(typeof(CUITexture))

    self.m_JingPingMask=jingpingTf:Find("GetNode").gameObject
    self.m_JingPingCountLabel = jingpingTf:Find("CountLabel"):GetComponent(typeof(UILabel))


    self.m_YinLiangCost =self.transform:Find("Anchor/Cost/YinLiangCost"):GetComponent(typeof(CCurentMoneyCtrl))
    self.m_LingYuCost =self.transform:Find("Anchor/Cost/LingYuCost"):GetComponent(typeof(CCurentMoneyCtrl))
    local setting = ChuanjiabaoModel_Setting.GetData()

    local canUseBindLingyu = CShopMallMgr.CanUseBindJadeBuy(setting.WashZiJingPing)
    self.m_LingYuCost:SetType(canUseBindLingyu and EnumMoneyType.LingYu_WithBind or EnumMoneyType.LingYu)

    self.m_TitleLabel = self.transform:Find("TitleLabel"):GetComponent(typeof(UILabel))


    self.m_DescItem = self.transform:Find("Anchor/Desc/Desc/DescItem").gameObject
    self.m_DescItem:SetActive(false)

    self.m_UseLingYuToggle = self.transform:Find("Anchor/Cost/Toggle"):GetComponent(typeof(UIToggle))
    EventDelegate.Add(self.m_UseLingYuToggle.onChange,DelegateFactory.Callback(function () 
        self:RefreshToggleView()
    end))
    self.m_UseLingYuToggle:Set(false)

    self:InitInfo()
    self:UpdateJingPingCount()
end

function CLuaZhuShaBiXiLianWnd:RefreshToggleView(jingpingCount)
    --净瓶数量大于0的时候显示净瓶
    local count = jingpingCount
    if not jingpingCount then
        local setting = ChuanjiabaoModel_Setting.GetData()
        local c,d = CItemMgr.Inst:CalcItemCountInBagByTemplateId(setting.WashZiJingPing)
        if self.m_IsBinded then
            local a,b = CItemMgr.Inst:CalcItemCountInBagByTemplateIdExclueExpireTime(setting.WashYuJingPing)
            count = a+b+c+d
        else
            count = c+d
        end
    end
    if count>0 then
        self.m_LingYuCost.gameObject:SetActive(false)
        self.m_JingPingGo.gameObject:SetActive(true)
    else
        if self.m_UseLingYuToggle.value then
            self.m_LingYuCost.gameObject:SetActive(true)
            self.m_JingPingGo.gameObject:SetActive(false)
        else
            self.m_LingYuCost.gameObject:SetActive(false)
            self.m_JingPingGo.gameObject:SetActive(true)
        end
    end

end

function CLuaZhuShaBiXiLianWnd:InitInfo()
    self.m_IsBinded = false
    if CLuaZhuShaBiProcessWnd.m_SelectedItemId then
        local commonItem = CItemMgr.Inst:GetById(CLuaZhuShaBiProcessWnd.m_SelectedItemId)
        self.m_IsBinded = commonItem.IsBinded

        local setting = ChuanjiabaoModel_Setting.GetData()
        local formulaId = setting.WashCostFormulaId
        local formula = AllFormulas.Action_Formula[formulaId] and AllFormulas.Action_Formula[formulaId].Formula
        
        local name = Item_Item.GetData(commonItem.TemplateId).Name

        local cjbSetting = House_ChuanjiabaoSetting.GetData()
        local lv = 0
        local lv2 = 0
        if string.find(name,LocalString.GetString("1级")) then
            lv=cjbSetting.ChuanjiabaoLowLevel
            lv2 = 1
        elseif string.find(name,LocalString.GetString("2级")) then
            lv=cjbSetting.ChuanjiabaoNormalLevel
            lv2 = 2
        elseif string.find(name,LocalString.GetString("3级")) then
            lv=cjbSetting.ChuanjiabaoHighLevel
            lv2 = 3
        end
        --计算等级
       self.m_CostJingPingCount =  ChuanjiabaoModel_Wash_Cost.GetData(lv2).Cost
        --看看是不是绑定的
        if commonItem.IsBinded then
            --消耗银票
            self.m_YinLiangCost:SetType(EnumMoneyType.YinPiao,EnumPlayScoreKey.NONE, true)
        else
            --消耗银两
            self.m_YinLiangCost:SetType(EnumMoneyType.YinLiang,EnumPlayScoreKey.NONE, true)
        end
        self.m_YinLiangCost:SetCost(formula(nil,nil,{lv}))


        local tf = self.transform:Find("Item")
        local texture = tf.transform:Find("Icon"):GetComponent(typeof(CUITexture))
        texture:LoadMaterial(commonItem.Icon)
        local bindSprite = tf.transform:Find("BindSprite"):GetComponent(typeof(UISprite))
        bindSprite.spriteName = commonItem.BindOrEquipCornerMark
        local qualitySprite = tf.transform:GetComponent(typeof(UISprite))

        if commonItem.Item.Type==EnumItemType_lua.EvaluatedZhushabi then
            qualitySprite.spriteName = CClientChuanJiaBaoMgr.Inst:GetZhushabiBorder(commonItem)
            self.m_TitleLabel.text = LocalString.GetString("洗炼朱砂笔")
        else
            qualitySprite.spriteName = CClientChuanJiaBaoMgr.Inst:GetChuanjiabaoBorder(commonItem)
            self.m_TitleLabel.text = LocalString.GetString("洗炼传家宝")
        end
        UIEventListener.Get(tf.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            CItemInfoMgr.ShowLinkItemInfo(CLuaZhuShaBiProcessWnd.m_SelectedItemId)
        end)

        local wordInfo = nil
        if commonItem.Item.Type==EnumItemType_lua.EvaluatedZhushabi then
            if commonItem.Item.ExtraVarData and commonItem.Item.ExtraVarData.Data then
                wordInfo = CreateFromClass(CChuanjiabaoWordType)
                wordInfo:LoadFromString(commonItem.Item.ExtraVarData.Data)
            end
        else--传家宝
            wordInfo = commonItem.Item.ChuanjiabaoItemInfo and commonItem.Item.ChuanjiabaoItemInfo.WordInfo
        end
        -- local data = commonItem.Item.ChuanjiabaoItemInfo
        if wordInfo  then
            --显示words
            local tableGo = self.transform:Find("Anchor/Desc/Desc/Table").gameObject
            Extensions.RemoveAllChildren(tableGo.transform)

            local starGrid = self.transform:Find("Anchor/Desc/StarGrid").gameObject
            Extensions.RemoveAllChildren(starGrid.transform)
            
            local starGrid2 = self.transform:Find("Anchor/Desc/StarGrid2").gameObject
            Extensions.RemoveAllChildren(starGrid2.transform)

            local washedStar = wordInfo.WashedStar or 0
            local hasWashResult = washedStar>0--wordInfo.LastWashResult and wordInfo.LastWashResult.Words.Count>0
            if hasWashResult then
                self:InitStar(starGrid,wordInfo.Star,washedStar)
            else
                self:InitStar(starGrid,wordInfo.Star,wordInfo.Star)
            end

            local color = CClientChuanJiaBaoMgr.Inst:GetChuanjiabaoWordColor(wordInfo.Words.Count)
            local wordTable = {}
            CommonDefs.DictIterate(wordInfo.Words,DelegateFactory.Action_object_object(function (k, v)
                if v>0 then
                    table.insert(wordTable,k)
                end
            end))
            local extWordMaxTable = CLuaEquipMgr:ChuanjiabaoWordIsMaxWord(self.m_ItemId,wordTable)

            for index, k in ipairs(wordTable) do
                local go = NGUITools.AddChild(tableGo,self.m_DescItem)
                go:SetActive(true)
                self:InitDescItem(go,k,color,extWordMaxTable[index])
            end
            -- CommonDefs.DictIterate(wordInfo.Words,DelegateFactory.Action_object_object(function (k, v)
            --     if v>0 then
            --         local go = NGUITools.AddChild(table,self.m_DescItem)
            --         go:SetActive(true)
            --         self:InitDescItem(go,k,color)
            --     end
            -- end))
            tableGo:GetComponent(typeof(UITable)):Reposition()

            if wordInfo.LastWashResult then

                local tableGo = self.transform:Find("Anchor/Desc/Desc2/Table").gameObject
                Extensions.RemoveAllChildren(tableGo.transform)

                local count = wordInfo.LastWashResult.Words.Count
                local hasWashResult = count>0
                local color = CClientChuanJiaBaoMgr.Inst:GetChuanjiabaoWordColor(count)

                local wordTable = {}
                CommonDefs.DictIterate(wordInfo.LastWashResult.Words,DelegateFactory.Action_object_object(function (k, v)
                    -- if v>0 then
                        table.insert(wordTable,k)
                    -- end
                end))
                local extWordMaxTable = CLuaEquipMgr:ChuanjiabaoWordIsMaxWord(self.m_ItemId,wordTable)
                for index, k in ipairs(wordTable) do
                    local go = NGUITools.AddChild(tableGo,self.m_DescItem)
                    go:SetActive(true)
                    self:InitDescItem(go,k,color,extWordMaxTable[index])
                end
                
                -- CommonDefs.DictIterate(wordInfo.LastWashResult.Words,DelegateFactory.Action_object_object(function (k, v)
                --     -- if v>0 then
                --         local go = NGUITools.AddChild(table,self.m_DescItem)
                --         go:SetActive(true)
                --         self:InitDescItem(go,k,color)
                --     -- end
                -- end))
                tableGo:GetComponent(typeof(UITable)):Reposition()

                if hasWashResult then
                    self:InitStar(starGrid2,wordInfo.Star,wordInfo.LastWashResult.Star or 0)
                end
            end
        end
    end
end
function CLuaZhuShaBiXiLianWnd:InitStar(grid,star,washedStar)
    if star==0 then return end
    local c1 = math.max(star,washedStar)
    local c2 = ChuanjiabaoModel_Setting.GetData().Chuanjiabao_Total_Score
    for i=1,c2 do
        local g = NGUITools.AddChild(grid,self.m_Star)
        g:SetActive(true)
        local sprite = g:GetComponent(typeof(UISprite))
        sprite.alpha = 1
        if i<=c1 then
            sprite.spriteName = "playerAchievementView_star_1"
            if i<=star and i>washedStar then
                sprite.alpha = 0.3
            end
        else
            sprite.spriteName = "playerAchievementView_star_2"
        end

    end
    grid:GetComponent(typeof(UIGrid)):Reposition()
end

function CLuaZhuShaBiXiLianWnd:InitDescItem(go,wordTemplateId,color,isMax)
    local word = Word_Word.GetData(wordTemplateId)
    local nameLabel = go.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local descLabel = go.transform:Find("DescLabel"):GetComponent(typeof(UILabel))

    if word then
        nameLabel.text = SafeStringFormat3("[%s][%s][-]",color,word.Name)
        descLabel.text = SafeStringFormat3("[%s]%s[-]",color,word.Description)
        if isMax then
            descLabel.text = CLuaEquipMgr.WordMaxMark..descLabel.text
        end

        descLabel:UpdateNGUIText()
        go:GetComponent(typeof(UIWidget)).height = math.floor(descLabel.localSize.y+20)
    else
        nameLabel.text=nil
        descLabel.text=nil
    end
end

function CLuaZhuShaBiXiLianWnd:TryClose( go) 
    -- ZhuShaBi_XiLian_Close_Confirm
    local needConfirm = false
    local commonItem = CItemMgr.Inst:GetById(CLuaZhuShaBiProcessWnd.m_SelectedItemId)
    if commonItem then
        local wordInfo = nil
        if commonItem.Item.Type==EnumItemType_lua.EvaluatedZhushabi then
            if commonItem.Item.ExtraVarData and commonItem.Item.ExtraVarData.Data then
                wordInfo = CreateFromClass(CChuanjiabaoWordType)
                wordInfo:LoadFromString(commonItem.Item.ExtraVarData.Data)
            end
        else--传家宝
            wordInfo = commonItem.Item.ChuanjiabaoItemInfo and commonItem.Item.ChuanjiabaoItemInfo.WordInfo
        end
        if wordInfo then
            if wordInfo.LastWashResult then
                local hasWashResult = wordInfo.LastWashResult.Words.Count>0
                if hasWashResult then
                    needConfirm=true
                    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("ZhuShaBi_XiLian_Close_Confirm"), DelegateFactory.Action(function () 
                        self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
                    end), nil, nil, nil, false)
                end
            end
        end
    end
    if not needConfirm then
    	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
    end

    -- if self.needConfirm then
    --     local tip = g_MessageMgr:FormatMessage("Close_Crafting_Equipment")
    --     MessageWndManager.ShowOKCancelMessage(tip, DelegateFactory.Action(function () 
    --     	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
    --     end), nil, nil, nil, false)
    -- else
    -- end
end

function CLuaZhuShaBiXiLianWnd:Awake( )
    if CLuaZhuShaBiProcessWnd.m_SelectedItemId then
        self.m_ItemId = CLuaZhuShaBiProcessWnd.m_SelectedItemId
    end




    self.replaceBtn = self.transform:Find("Anchor/ReplaceBtn").gameObject
    self.baptizeBtn = self.transform:Find("Anchor/BaptizeBtn").gameObject

    UIEventListener.Get(self.replaceBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        self:Replace()
    end)
    UIEventListener.Get(self.baptizeBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        self:TryBaptize()
    end)
end
function CLuaZhuShaBiXiLianWnd:Replace( )
    local itemInfo = CItemMgr.Inst:GetItemInfo(CLuaZhuShaBiProcessWnd.m_SelectedItemId)
    if itemInfo then
        Gac2Gas.RequestAcceptChuanjiabaoWashResult(EnumToInt(itemInfo.place),itemInfo.pos,CLuaZhuShaBiProcessWnd.m_SelectedItemId)--place, pos, itemId
        --g_MessageMgr:ShowMessage("Baptize_Use_New_Word")
    else
        if not CClientHouseMgr.Inst:HaveHouse() then
            g_MessageMgr:ShowMessage("PLAYER_NO_HOUSE")
            return
        end
        --判断是否在家
        if CClientHouseMgr.Inst:IsInOwnHouse() then
            Gac2Gas.RequestAcceptChuanjiabaoWashResultInHouse(CLuaZhuShaBiProcessWnd.m_SelectedItemId)
            --g_MessageMgr:ShowMessage("Baptize_Use_New_Word")
        else
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("OPERATION_REQUEST_GO_HOME"), 
            DelegateFactory.Action(function () 
                Gac2Gas.RequestEnterHome()
            end), nil, nil, nil, false)
        end
    end
end



function CLuaZhuShaBiXiLianWnd:TryBaptize( )
    --检查银两、净瓶、灵玉
    local setting = ChuanjiabaoModel_Setting.GetData()

    local useLingYu=self.m_UseLingYuToggle.value
    if self.m_UseLingYuToggle.value then--使用灵玉
        --检查灵玉数量
        if not self.m_LingYuCost.moneyEnough then
            CGetMoneyMgr.Inst:GetLingYu(self.m_LingYuCost.cost, setting.WashZiJingPing)
            return false
        end
    else
        --检查净瓶数量
        local c,d = CItemMgr.Inst:CalcItemCountInBagByTemplateId(setting.WashZiJingPing)
        local count = c+d
        if self.m_IsBinded then
            local a,b = CItemMgr.Inst:CalcItemCountInBagByTemplateIdExclueExpireTime(setting.WashYuJingPing)
            count = a+b+c+d
        end
        if self.m_CostJingPingCount>count then
            g_MessageMgr:ShowMessage("BAPTIZE_EQUIP_BASIC_ITEM_NOT_ENOUGH",LocalString.GetString("净瓶"))
            return false
        end
    end

    if not self.m_YinLiangCost.moneyEnough then
        g_MessageMgr:ShowMessage("SILVER_NOT_ENOUGH")
        return false
    end

    local commonItem = CItemMgr.Inst:GetById(CLuaZhuShaBiProcessWnd.m_SelectedItemId)
    if commonItem then
        local wordInfo = nil
        if commonItem.Item.Type==EnumItemType_lua.EvaluatedZhushabi then
            if commonItem.Item.ExtraVarData and commonItem.Item.ExtraVarData.Data then
                wordInfo = CreateFromClass(CChuanjiabaoWordType)
                wordInfo:LoadFromString(commonItem.Item.ExtraVarData.Data)
            end
        else--传家宝
            wordInfo = commonItem.Item.ChuanjiabaoItemInfo and commonItem.Item.ChuanjiabaoItemInfo.WordInfo
        end
        if wordInfo then
            if wordInfo.LastWashResult then--有洗炼结果
                local hasNewResult = wordInfo.LastWashResult.Words.Count
                local preStar = wordInfo.Star
                local preWordCount = wordInfo.Words.Count
                if wordInfo.WashedStar and wordInfo.WashedStar>0  then
                    preStar = wordInfo.WashedStar
                end
                if CLuaZhuShaBiXiLianMgr.IsGoodResult( wordInfo.LastWashResult,preStar,preWordCount) then
                    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Better_Crafting_Equipment"), 
                    DelegateFactory.Action(function () 
                        self:XiLian()
                    end), 
                    DelegateFactory.Action(function () 
                        self:CancelXiLian()
                    end), nil, nil, false)
                else
                    self:XiLian()
                end
                return true
            else
                self:XiLian()
                return true
            end
        end
    end
    return false
end

function CLuaZhuShaBiXiLianWnd:XiLian()

    local useLingYu=self.m_UseLingYuToggle.value

    local itemInfo = CItemMgr.Inst:GetItemInfo(CLuaZhuShaBiProcessWnd.m_SelectedItemId)
    if itemInfo then
        Gac2Gas.RequestWashChuanjiabao(EnumToInt(itemInfo.place),itemInfo.pos,CLuaZhuShaBiProcessWnd.m_SelectedItemId,useLingYu)--place, pos, itemId
        CUICommonDef.SetActive(self.baptizeBtn, false, true)
        CUICommonDef.SetActive(self.replaceBtn, false, true)
    else
        if not CClientHouseMgr.Inst:HaveHouse() then
            g_MessageMgr:ShowMessage("PLAYER_NO_HOUSE")
            return
        end
    
        --判断是否在家
        if CClientHouseMgr.Inst:IsInOwnHouse() then
            Gac2Gas.RequestWashChuanjiabaoInHouse(CLuaZhuShaBiProcessWnd.m_SelectedItemId, useLingYu)
        else
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("OPERATION_REQUEST_GO_HOME"), 
            DelegateFactory.Action(function () 
                Gac2Gas.RequestEnterHome()
            end), nil, nil, nil, false)
        end
    end
end
function CLuaZhuShaBiXiLianWnd:CancelXiLian()

end

function CLuaZhuShaBiXiLianWnd:OnEnable( )
    g_ScriptEvent:AddListener("SendItem", self, "OnSendItem")

    g_ScriptEvent:AddListener("SetItemAt",self,"OnSetItemAt")
    g_ScriptEvent:AddListener("SendChuanjiabaoWashResult",self,"OnSendChuanjiabaoWashResult")
end
function CLuaZhuShaBiXiLianWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("SendItem", self, "OnSendItem")

    g_ScriptEvent:RemoveListener("SetItemAt",self,"OnSetItemAt")
    g_ScriptEvent:RemoveListener("SendChuanjiabaoWashResult",self,"OnSendChuanjiabaoWashResult")

end
function CLuaZhuShaBiXiLianWnd:OnSendChuanjiabaoWashResult(itemId,newStar,wordIds)
    if CLuaZhuShaBiProcessWnd.m_SelectedItemId==itemId then
        CUICommonDef.SetActive(self.baptizeBtn, true, true)
        CUICommonDef.SetActive(self.replaceBtn, true, true)
        self:InitInfo()

        self.baptizeCount = self.baptizeCount+1
        self:UpdateCount()
    end
end


function CLuaZhuShaBiXiLianWnd:OnSendItem( args) 
    local itemId = args[0]
    if CLuaZhuShaBiProcessWnd.m_SelectedItemId == itemId then
        --洗炼成功
        self:InitInfo()
    end

    local setting = ChuanjiabaoModel_Setting.GetData()
    local commonItem = CItemMgr.Inst:GetById(itemId)
    if commonItem then
        if commonItem.TemplateId == setting.WashYuJingPing or commonItem.TemplateId == setting.WashZiJingPing then
            self:UpdateJingPingCount()
        end
    end
end

function CLuaZhuShaBiXiLianWnd:OnSetItemAt(args)
    local place, pos, oldItemId, newItemId = args[0],args[1],args[2],args[3]
    local update = false
    local setting = ChuanjiabaoModel_Setting.GetData()

    if oldItemId and oldItemId~="" then
        local commonItem = CItemMgr.Inst:GetById(oldItemId)
        if not commonItem then return end
        if commonItem.TemplateId == setting.WashYuJingPing or commonItem.TemplateId == setting.WashZiJingPing then
            update=true
        end
    end
    if newItemId and newItemId~="" then
        local commonItem = CItemMgr.Inst:GetById(newItemId)
        if not commonItem then return end
        if commonItem.TemplateId == setting.WashYuJingPing or commonItem.TemplateId == setting.WashZiJingPing then
            update=true
        end
    end
    if update then
        self:UpdateJingPingCount()
    end
end

function CLuaZhuShaBiXiLianWnd:UpdateJingPingCount()
    local setting = ChuanjiabaoModel_Setting.GetData()
    local c,d = CItemMgr.Inst:CalcItemCountInBagByTemplateId(setting.WashZiJingPing)
    local a,b = 0,0
    local count = c+d
    if self.m_IsBinded then
        a,b = CItemMgr.Inst:CalcItemCountInBagByTemplateIdExclueExpireTime(setting.WashYuJingPing)
        count = a+b+c+d
    end
    
    if count<self.m_CostJingPingCount then
        self.m_JingPingCountLabel.text = SafeStringFormat3("[ff0000]%d[-]/%d",count,self.m_CostJingPingCount)
    else
        self.m_JingPingCountLabel.text = SafeStringFormat3("%d/%d",count,self.m_CostJingPingCount)

    end

    if count>=self.m_CostJingPingCount then
        self.m_JingPingMask:SetActive(false)
    else
        self.m_JingPingMask:SetActive(true)
    end

    local jingping = setting.WashZiJingPing
    if self.m_IsBinded and a+b>0 then
        jingping = setting.WashYuJingPing
        self.m_JingPingIcon:LoadMaterial(Item_Item.GetData(setting.WashYuJingPing).Icon)
    else
        self.m_JingPingIcon:LoadMaterial(Item_Item.GetData(setting.WashZiJingPing).Icon)
    end

    UIEventListener.Get(self.m_JingPingGo).onClick = DelegateFactory.VoidDelegate(function(go)
        if count>=self.m_CostJingPingCount then
            CItemInfoMgr.ShowLinkItemTemplateInfo(jingping,false,nil,AlignType.Default, 0, 0, 0, 0)
        else
            CItemAccessListMgr.Inst:ShowItemAccessInfo(jingping, true, nil, AlignType1.Right)
        end
    end)

    self:RefreshToggleView(count)
    self:UpdateXiLianCost(count)
end

function CLuaZhuShaBiXiLianWnd:UpdateXiLianCost(jingpingCount)
    if jingpingCount>=self.m_CostJingPingCount then
        self.m_LingYuCost:SetCost(0)
    else
        local setting = ChuanjiabaoModel_Setting.GetData()
        self.m_LingYuCost:SetCost(Mall_LingYuMall.GetData(setting.WashZiJingPing).Jade*(self.m_CostJingPingCount-jingpingCount))
    end
end
