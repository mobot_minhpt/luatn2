local UITable               = import "UITable"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local CommonDefs            = import "L10.Game.CommonDefs"
local DelegateFactory       = import "DelegateFactory"
local UILabel               = import "UILabel"
local GameObject            = import "UnityEngine.GameObject"
local CItemInfoMgr			= import "L10.UI.CItemInfoMgr"
local AlignType				= import "L10.UI.CItemInfoMgr+AlignType"
local Extensions			= import "Extensions"
local MessageMgr			= import "L10.Game.MessageMgr"
local CUICommonDef			= import "L10.UI.CUICommonDef"
local CMessageTipMgr		= import "L10.UI.CMessageTipMgr"
local CTipParagraphItem		= import "L10.UI.CTipParagraphItem"
local UIScrollView			= import "UIScrollView"
local UIScrollViewIndicator = import "UIScrollViewIndicator"
local CTipTitleItem			= import "CTipTitleItem"
local CClientMainPlayer     = import "L10.Game.CClientMainPlayer"

LuaGSNZEnterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGSNZEnterWnd, "CountLabel", "CountLabel", UILabel)
RegistChildComponent(LuaGSNZEnterWnd, "EnterBtn", "EnterBtn", GameObject)
RegistChildComponent(LuaGSNZEnterWnd, "CancelBtn", "CancelBtn", GameObject)
RegistChildComponent(LuaGSNZEnterWnd, "WaitLabel", "WaitLabel", UILabel)
RegistChildComponent(LuaGSNZEnterWnd, "BannerItem1", "BannerItem1", GameObject)
RegistChildComponent(LuaGSNZEnterWnd, "BannerItem2", "BannerItem2", GameObject)
RegistChildComponent(LuaGSNZEnterWnd, "Item1", "Item1", GameObject)
RegistChildComponent(LuaGSNZEnterWnd, "Item2", "Item2", GameObject)
RegistChildComponent(LuaGSNZEnterWnd, "Item3", "Item3", GameObject)
RegistChildComponent(LuaGSNZEnterWnd, "Content", "Content", GameObject)
RegistChildComponent(LuaGSNZEnterWnd, "Template", "Template", GameObject)
RegistChildComponent(LuaGSNZEnterWnd, "Table", "Table", UITable)
RegistChildComponent(LuaGSNZEnterWnd, "ScrollView", "ScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaGSNZEnterWnd, "Indicator", "Indicator", UIScrollViewIndicator)
RegistChildComponent(LuaGSNZEnterWnd, "Time", "Time", UILabel)

--@endregion RegistChildComponent end

function LuaGSNZEnterWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.EnterBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnEnterBtnClick()
	end)

	UIEventListener.Get(self.CancelBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCancelBtnClick()
	end)

    --@endregion EventBind end
    self:ParseRuleText()
end

function LuaGSNZEnterWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdateTempPlayTimesWithKey", self, "OnUpdateTempPlayTimesWithKey")
    g_ScriptEvent:AddListener("GlobalMatch_CheckInMatchingResult", self, "OnGlobalMatch_CheckInMatchingResult")
    g_ScriptEvent:AddListener("GlobalMatch_SignUpPlayResult", self, "OnGlobalMatch_SignUpPlayResult")
    g_ScriptEvent:AddListener("GlobalMatch_CancelSignUpResult", self, "OnGlobalMatch_CancelSignUpResult")
end

function LuaGSNZEnterWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateTempPlayTimesWithKey", self, "OnUpdateTempPlayTimesWithKey")
    g_ScriptEvent:RemoveListener("GlobalMatch_CheckInMatchingResult", self, "OnGlobalMatch_CheckInMatchingResult")
    g_ScriptEvent:RemoveListener("GlobalMatch_SignUpPlayResult", self, "OnGlobalMatch_SignUpPlayResult")
    g_ScriptEvent:RemoveListener("GlobalMatch_CancelSignUpResult", self, "OnGlobalMatch_CancelSignUpResult")
end

--@region 报名相关事件

--[[
    @desc:次数 
    author:Codee
    time:2022-05-26 14:31:00
    --@args: 
    @return:
]]
function LuaGSNZEnterWnd:OnUpdateTempPlayTimesWithKey(args)
    local key = args[0]
    if key == 245 then
        self:RefreshCount()
    end
end

function LuaGSNZEnterWnd:OnGlobalMatch_CheckInMatchingResult(playerId, playId, isInMatching, resultStr)
    if playId==ZhongYuanJie2022Mgr.GSNZGamePlayId and CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == playerId then
        self:UpdateBtns(isInMatching)
    end
end

function LuaGSNZEnterWnd:OnGlobalMatch_SignUpPlayResult(playId, success)
    if playId==ZhongYuanJie2022Mgr.GSNZGamePlayId and success then
        self:UpdateBtns(true)
    end
end

function LuaGSNZEnterWnd:OnGlobalMatch_CancelSignUpResult(playId, success)
    if playId==ZhongYuanJie2022Mgr.GSNZGamePlayId and success then
        self:UpdateBtns(false)
    end
end
--@endregion

function LuaGSNZEnterWnd:Init()
    local setting = ZhongYuanJie_Setting2022.GetData()
    local items = setting.GSNZ_RewardItems
    local ctrls = {self.Item1, self.Item2, self.Item3}
    for i = 0, 2 do
        if i < items.Length then
            self:InitItem(items[i], ctrls[i + 1])
        else
            ctrls[i + 1]:SetActive(false)
        end
    end

    self:InitItem(setting.GSNZ_LostReward,self.BannerItem1)
    self:InitItem(setting.GSNZ_WinReward,self.BannerItem2)

    self.Time.text = setting.GSNZ_TimeDesc

    --奖励次数
    self:RefreshCount()
    if CClientMainPlayer.Inst ~= nil then
        Gac2Gas.GlobalMatch_RequestCheckSignUp(ZhongYuanJie2022Mgr.GSNZGamePlayId, CClientMainPlayer.Inst.Id)
    end
end

function LuaGSNZEnterWnd:RefreshCount()
    local max = ZhongYuanJie_Setting2022.GetData().GSNZ_MaxCount
    local count = self:GetCount()
    local lastcount = max - count
    self.CountLabel.text = lastcount.."/"..max

    local color = (lastcount == 0) and Color(1, 80/255, 80/255, 1) or Color(1, 1, 1, 1)
    self.CountLabel.color = color
end

function LuaGSNZEnterWnd:InitItem(itemid,item)
    local itemcfg = Item_Item.GetData(itemid)
    local trans = item.transform
    local iconTex = FindChildWithType(trans,"IconTexture",typeof(CUITexture))
    local ctTxt = FindChildWithType(trans,"AmountLabel",typeof(UILabel))
    if itemcfg then
        iconTex:LoadMaterial(itemcfg.Icon)
    end

    UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemid, false, nil, AlignType.Default, 0, 0, 0, 0)
    end)
end


function LuaGSNZEnterWnd:ParseRuleText()
	
	Extensions.RemoveAllChildren(self.Table.transform)

    local msg =	MessageMgr.Inst:FormatMessage("GSNZ_RULE",{});
    if System.String.IsNullOrEmpty(msg) then
        return
    end

    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)
    if info == nil then return end

    if info.titleVisible then
    end

    do
        local i = 0
        while i < info.paragraphs.Count do
            local paragraphGo = CUICommonDef.AddChild(self.Table.gameObject, self.Template)
            paragraphGo:SetActive(true)
			local tip = CommonDefs.GetComponent_GameObject_Type(paragraphGo, typeof(CTipParagraphItem))
            tip:Init(info.paragraphs[i], 4294967295)
            i = i + 1
        end
    end

    self.ScrollView:ResetPosition()
    self.Indicator:Layout()
    self.Table:Reposition()
end

function LuaGSNZEnterWnd:GetCount()
    return ZhongYuanJie2022Mgr.GetGSNZCount()
end

function LuaGSNZEnterWnd:UpdateBtns(matching)
    self.EnterBtn:SetActive(not matching)
    self.CancelBtn:SetActive(matching)
    self.WaitLabel.gameObject:SetActive(matching)
end

--@region UIEvent

function LuaGSNZEnterWnd:OnEnterBtnClick()
    local count = self:GetCount()
    local max = ZhongYuanJie_Setting2022.GetData().GSNZ_MaxCount
    if count >= max then
        g_MessageMgr:ShowMessage("GSNZ_NOREWARD")        
    end
    Gac2Gas.GlobalMatch_RequestSignUp(ZhongYuanJie2022Mgr.GSNZGamePlayId)
end

function LuaGSNZEnterWnd:OnCancelBtnClick()
    Gac2Gas.GlobalMatch_RequestCancelSignUp(ZhongYuanJie2022Mgr.GSNZGamePlayId)
end


--@endregion UIEvent

