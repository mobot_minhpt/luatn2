-- Auto Generated!!
local CBiWuDaHuiMgr = import "L10.Game.CBiWuDaHuiMgr"
local CFightingSpiritMgr = import "L10.Game.CFightingSpiritMgr"
local CQMJJMgr = import "L10.Game.CQMJJMgr"
local CTaskAndTeamWnd = import "L10.UI.CTaskAndTeamWnd"
local CTowerDefenseMgr = import "L10.Game.CTowerDefenseMgr"
local CValentineMgr = import "L10.UI.CValentineMgr"
local CXialvPKMgr = import "L10.Game.CXialvPKMgr"


g_AutoChangeToTaskTab = false
CTaskAndTeamWnd.m_OnMainPlayerCreated_CS2LuaHook = function (this) 
    if CBiWuDaHuiMgr.Inst.inBiWuPrepare or CBiWuDaHuiMgr.Inst.inBiWu or CQMJJMgr.Inst.inQMJJPrepare or CQMJJMgr.Inst.inQMJJ or CXialvPKMgr.Inst.InXiaLvPK or CXialvPKMgr.Inst.InXialvPKPrepare or CValentineMgr.Inst.InValentineVoiceLove or LuaSeaFishingMgr.IsInPlay() then
        this:ChangeToTaskTab()
    end
    if CFightingSpiritMgr.Instance:IsDouhunPrepare() or CTowerDefenseMgr.Inst:IsInTowerDefenseScene() then
        this:swithToTaskTab()
    end

    if LuaWuYiMgr.IsInWaKuang() or LuaFruitPartyMgr.IsInFruitParty() or LuaQiXi2021Mgr.IsQueQiaoXianQuPlay() or LuaHMLZMgr:IsInPlay2021() or LuaHalloween2022Mgr.IsInTrafficPlay() or LuaChristmas2022Mgr:IsInTurkeyMatch() then
        this:ChangeToTaskTab()
    end
    --在玩法类的OnMainPlayerCreated方法中设置
    if g_AutoChangeToTaskTab then
        this:ChangeToTaskTab()
    end
end

