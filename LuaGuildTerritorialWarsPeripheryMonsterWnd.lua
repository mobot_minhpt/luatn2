local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local QnTableView = import "L10.UI.QnTableView"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CTeamMatchMgr = import "L10.Game.CTeamMatchMgr"

LuaGuildTerritorialWarsPeripheryMonsterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaGuildTerritorialWarsPeripheryMonsterWnd, "TopLabel", "TopLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsPeripheryMonsterWnd, "BottomLabel", "BottomLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsPeripheryMonsterWnd, "RefreshButton", "RefreshButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsPeripheryMonsterWnd, "CreateTeamButton", "CreateTeamButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsPeripheryMonsterWnd, "TableView", "TableView", QnTableView)

--@endregion RegistChildComponent end
RegistClassMember(LuaGuildTerritorialWarsPeripheryMonsterWnd,"m_List")

function LuaGuildTerritorialWarsPeripheryMonsterWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.RefreshButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRefreshButtonClick()
	end)


	
	UIEventListener.Get(self.CreateTeamButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCreateTeamButtonClick()
	end)


    --@endregion EventBind end
end

function LuaGuildTerritorialWarsPeripheryMonsterWnd:Init()
	self.TopLabel.text = g_MessageMgr:FormatMessage("GuildTerritorialWarsPeripheryMonsterWnd_TopLabel")
	self.BottomLabel.text = g_MessageMgr:FormatMessage("GuildTerritorialWarsPeripheryMonsterWnd_BottomLabel")
	self.TableView.m_DataSource = DefaultTableViewDataSource.Create(function() 
        return #self.m_List
    end, function(item, index)
        self:InitItem(item, index)
    end)
	self.m_List = LuaGuildTerritorialWarsMgr.m_MonsterSceneInfo
    self.TableView:ReloadData(true,true)
	self.CreateTeamButton.gameObject:SetActive(not CTeamMgr.Inst:TeamExists()) 
end


function LuaGuildTerritorialWarsPeripheryMonsterWnd:OnEnable()
	g_ScriptEvent:AddListener("TeamInfoChange",self,"OnTeamInfoChange")
end

function LuaGuildTerritorialWarsPeripheryMonsterWnd:OnDisable()
	g_ScriptEvent:RemoveListener("TeamInfoChange",self,"OnTeamInfoChange")
end

function LuaGuildTerritorialWarsPeripheryMonsterWnd:OnTeamInfoChange()
	self.CreateTeamButton.gameObject:SetActive(not CTeamMgr.Inst:TeamExists()) 
end

function LuaGuildTerritorialWarsPeripheryMonsterWnd:InitItem(item, index)
	local data = self.m_List[index + 1]
	local designData = GuildTerritoryWar_Map.GetData(data.gridId)
	local regionData = GuildTerritoryWar_Region.GetData(designData.Region)
	local button = item.transform:Find("Button")
	UIEventListener.Get(button.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButtonClick(data)
	end)
	local numLabel = item.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
	local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
	local centerMap = item.transform:Find("CenterMap")
	local level2Map = item.transform:Find("Level2Map")
	local level3Map = item.transform:Find("Level3Map")

	numLabel.text = SafeStringFormat3(LocalString.GetString("剩余%d"),data.monsterCount)
	nameLabel.text = SafeStringFormat3(LocalString.GetString("%s(%d,%d)"),regionData.Name, designData.PosX, designData.PosY)
	centerMap.gameObject:SetActive(designData.level == 4)
	level3Map.gameObject:SetActive(designData.level == 3)
	level2Map.gameObject:SetActive(designData.level == 2)
end

--@region UIEvent

function LuaGuildTerritorialWarsPeripheryMonsterWnd:OnRefreshButtonClick()
	Gac2Gas.RequestGTWRelatedPlayMonsterSceneInfo()
end

function LuaGuildTerritorialWarsPeripheryMonsterWnd:OnCreateTeamButtonClick()
	CTeamMgr.Inst:RequestCreateTeam()
end

function LuaGuildTerritorialWarsPeripheryMonsterWnd:OnButtonClick(data)
	Gac2Gas.RequestTerritoryWarTeleport(data.gridId, 0)
end
--@endregion UIEvent

