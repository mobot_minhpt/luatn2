local UISlider = import "UISlider"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CNPCHeadInfoItem = import "L10.UI.CNPCHeadInfoItem"

LuaPreTaskCleanProgress = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaPreTaskCleanProgress, "Slider", "Slider", UISlider)
RegistChildComponent(LuaPreTaskCleanProgress, "Cleaning", "Cleaning", UILabel)

--@endregion RegistChildComponent end

function LuaPreTaskCleanProgress:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.Slider.gameObject:SetActive(false)
end

function LuaPreTaskCleanProgress:Init()

end

function LuaPreTaskCleanProgress:OnEnable()
    g_ScriptEvent:AddListener("SyncNpcCountDownProgress", self, "OnSyncNpcCountDownProgress")
end

function LuaPreTaskCleanProgress:OnDisable()
    g_ScriptEvent:RemoveListener("SyncNpcCountDownProgress", self, "OnSyncNpcCountDownProgress")
end

function LuaPreTaskCleanProgress:OnSyncNpcCountDownProgress(engineId, bDisplay, leftTime, totalTime)
    local selfId = nil
    local father = self.transform.parent
    if father then
        local item = father.parent
        if item then
            local npcTemplate = item:GetComponent(typeof(CNPCHeadInfoItem))
            selfId = npcTemplate.objId
        end
    end
    print(selfId,engineId, bDisplay, leftTime, totalTime)
    if selfId and selfId == engineId then
        print("here",leftTime,totalTime,leftTime/totalTime)
        self.Slider.gameObject:SetActive(bDisplay)
        self.Slider.value = leftTime/totalTime
    end
end


--@region UIEvent

--@endregion UIEvent

