require("common/common_include")

local Random = import "UnityEngine.Random"
local Object = import "UnityEngine.Object"
local Time = import "UnityEngine.Time"
local UITexture = import "UITexture"

LuaFallingDownEffect = class()

RegistClassMember(LuaFallingDownEffect, "m_ToyTexture")

RegistClassMember(LuaFallingDownEffect, "m_DropSpeed")
RegistClassMember(LuaFallingDownEffect, "m_RotateSpeed")
RegistClassMember(LuaFallingDownEffect, "m_LifeTime")
RegistClassMember(LuaFallingDownEffect, "m_Fin")

function LuaFallingDownEffect:OnEnable()

    self.m_ToyTexture = self.transform:GetComponent(typeof(UITexture))
	self.m_DropSpeed = Random.Range(500 - 100, 500 + 100)
	self.m_RotateSpeed = Random.Range(180 - 100, 180 + 100)

	self.m_LifeTime = 8
	self.m_Fin = false

    local scale = Random.Range(0.5, 2)
    -- 根据scale来调整透明度, 越小越透明
    local lowAlpha = 0.6
    local highAlpha= 0.9

    local alpha = scale / 1.5 * (highAlpha - lowAlpha) + (lowAlpha - 0.5 / 1.5 * (highAlpha - lowAlpha))
    self.m_ToyTexture.alpha = alpha
    self.transform.localScale = Vector3(scale, scale, scale)

	Object.Destroy(self.gameObject, self.m_LifeTime)
	
end

function LuaFallingDownEffect:Update()
	if not self.m_Fin then
		local newPos = Vector3(self.transform.localPosition.x, self.transform.localPosition.y - self.m_DropSpeed * Time.deltaTime,  0)
		self.transform.localPosition = newPos
    	self.transform.localEulerAngles = Vector3(0, 0, self.transform.localEulerAngles.z + self.m_RotateSpeed * Time.deltaTime)

    	local x = newPos.x
    	local y = newPos.y
    	local linePos = math.floor((x + 1920 / 2) / 150)
    	if LuaZhuJueJuQingMgr.m_BottomLines and LuaZhuJueJuQingMgr.m_BottomLines[linePos] then
    		if y - LuaZhuJueJuQingMgr.m_BottomLines[linePos] < 157 then
    			self.m_Fin = true
    			LuaZhuJueJuQingMgr.m_BottomLines[linePos] = y
    		end
    	end
	end
    
end

return LuaFallingDownEffect
