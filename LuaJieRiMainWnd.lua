local UIPanel = import "UIPanel"
local UITabBar = import "L10.UI.UITabBar"
local CChildWnd = import "L10.UI.CChildWnd"
local DelegateFactory = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CCommonLuaWnd = import "L10.UI.CCommonLuaWnd"

LuaJieRiMainWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaJieRiMainWnd, "Bg01", "Bg01", GameObject)
RegistChildComponent(LuaJieRiMainWnd, "ChildWnd1", "ChildWnd1", CChildWnd)
RegistChildComponent(LuaJieRiMainWnd, "ChildWnd2", "ChildWnd2", CChildWnd)
RegistChildComponent(LuaJieRiMainWnd, "ChildWnd3", "ChildWnd3", CChildWnd)
RegistChildComponent(LuaJieRiMainWnd, "Tabs", "Tabs", UITabBar)
RegistChildComponent(LuaJieRiMainWnd, "MaskPanel", "MaskPanel", UIPanel)

--@endregion RegistChildComponent end

RegistClassMember(LuaJieRiMainWnd, "m_jieriIDs")
RegistClassMember(LuaJieRiMainWnd, "m_wnds")

function LuaJieRiMainWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	self.Tabs.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
	    self:OnTabsTabChange(index)
	end)

    --@endregion EventBind end
    self.m_jieriIDs = {}
end

function LuaJieRiMainWnd:Init()
    self.m_jieriIDs = {}
    local panel = self.gameObject:GetComponent(typeof(UIPanel))
    self.m_wnds = {self.ChildWnd1, self.ChildWnd3, self.ChildWnd3}
    for i=1,#self.m_wnds do
        self.m_wnds[i]:SetBasePanelDepth(panel.depth)  
    end
    local cur = 0
    local count = 0
    local items = CLuaScheduleMgr:GetTodayFestivals()
    if items then
        for i = 1, #items do
            local jierigroup = items[i]
            if not String.IsNullOrEmpty(jierigroup.ActivityTabName) then
                local btn = FindChild(self.Tabs.transform, jierigroup.ActivityTabName)
                if btn then
                    local tabindex = self.Tabs:GetTabIndexByGo(btn.gameObject)
                    btn.gameObject:SetActive(true)
                    if jierigroup.ID == LuaCommonJieRiMgr.jieRiId  then
                        cur = tabindex
                    end
                    self.m_jieriIDs[tabindex] = jierigroup
                    count = count + 1
                end
            end
        end
    end
    if count <= 1 then
        self.Bg01:SetActive(false)
        self.Tabs:ChangeTab(0)
    else
        self.Tabs:ChangeTab(cur)
    end
end

function LuaJieRiMainWnd:InitSubWnd(childwnd)
    local childgo = childwnd.m_Instance
    local wnd = childgo:GetComponent(typeof(CCommonLuaWnd))
    if wnd and wnd.m_CloseButton then
        wnd.m_CloseButton:SetActive(false)
    end
end

--@region UIEvent

function LuaJieRiMainWnd:OnTabsTabChange(index)
    local jierigroup = self.m_jieriIDs[index]
    local wnd = self.m_wnds[1]
    local w = CLuaUIResources[jierigroup.OpenWnd]
    local path = w.Path
    local onloaded = DelegateFactory.Action(function ()
        self:InitSubWnd(wnd)
    end)
    wnd:Init(path,onloaded,nil)
end

--@endregion UIEvent
