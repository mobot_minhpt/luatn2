local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CConversationMgr  = import "L10.Game.CConversationMgr"

LuaLiuRuShiMgr = {}

-- 通用失败
LuaLiuRuShiMgr.m_TaskFailData = {}
function LuaLiuRuShiMgr:TaskFail(data)
    self.m_TaskFailData = {}
    if data then
        data = MsgPackImpl.unpack(data)
        if data and CommonDefs.IsDic(data) and CommonDefs.DictContains_LuaCall(data, "args")then
            self.m_TaskFailData.m_RestartTaskId = data["args"][0]
            self.m_TaskFailData.m_FailDescText = data["args"][1]
            self.m_TaskFailData.m_CancelBtnText = data["args"][2]
            self.m_TaskFailData.m_RestartBtnText = data["args"][3]
        end
    end
    CUIManager.ShowUI(CLuaUIResources.LiuRuShiTaskFailWnd)
end

function LuaLiuRuShiMgr:ShowLiuRuShiTaskFailWnd(taskId, taskFailWndText)
    self.m_TaskFailData = {}
    self.m_TaskFailData.m_RestartTaskId = taskId
    self.m_TaskFailData.m_FailDescText = taskFailWndText[0]
    self.m_TaskFailData.m_CancelBtnText = taskFailWndText[1]
    self.m_TaskFailData.m_RestartBtnText = taskFailWndText[2]
    CUIManager.ShowUI(CLuaUIResources.LiuRuShiTaskFailWnd)
end

-- 通用结局
LuaLiuRuShiMgr.m_EndId = 1
function LuaLiuRuShiMgr:ShowLiuRuShiEndWnd(endId)
    self.m_EndId = endId
    CLuaLetterDisplayMgr.ShowSpecialLetterWnd("LiuRuShiEndWnd")
end

-- 打开送礼玩法
LuaLiuRuShiMgr.m_GiftTaskId = nil
LuaLiuRuShiMgr.m_GiftConditionId = nil
function LuaLiuRuShiMgr:OpenLiuRuShiGiftWnd(taskId, conditionId)
    self.m_GiftTaskId = taskId
    self.m_GiftConditionId = conditionId or 1
    CUIManager.ShowUI(CLuaUIResources.LiuRuShiGiftWnd)
end

-- 打开答题玩法
LuaLiuRuShiMgr.m_TaskId = nil
function LuaLiuRuShiMgr:OpenLiuRuShiDaTiWnd(taskId)
    self.m_TaskId = taskId
    CUIManager.ShowUI(CLuaUIResources.LiuRuShiDaTiWnd)
end

-- 打开换装玩法
LuaLiuRuShiMgr.m_HuanZhuangTaskId = nil
function LuaLiuRuShiMgr:OpenLiuRuShiHuanZhuangWnd(taskId)
    self.m_HuanZhuangTaskId = taskId
    local setData = NanDuFanHua_LiuRuShiSetting.GetData()
    local buffId = setData.HuanZhuangPassBuffId
    if CClientMainPlayer.Inst ~= nil and CommonDefs.DictContains(CClientMainPlayer.Inst.BuffProp.Buffs, typeof(UInt32), buffId) then
        CUIManager.ShowUI(CLuaUIResources.LiuRuShiHuanZhuangWnd)
    else
        local dialogId = setData.HuanZhuangNoPassDialogId
        local dialogMsg = Dialog_TaskDialog.GetData(dialogId).Dialog
        CConversationMgr.Inst:OpenStoryDialog(dialogMsg, DelegateFactory.Action(function ()
            Gac2Gas.DoSayDialogEnd(dialogId)
        end))
    end
end

LuaLiuRuShiMgr.m_TearCalendarInfo = {}
function LuaLiuRuShiMgr:OpenTearCalendarWnd(taskId, conditionId)
	LuaLiuRuShiMgr.m_TearCalendarInfo.calendarId = conditionId
	LuaLiuRuShiMgr.m_TearCalendarInfo.taskId = taskId
	CUIManager.ShowUI(CLuaUIResources.TearCalendarWnd)
end

LuaLiuRuShiMgr.m_JianShuInfo = {}
function LuaLiuRuShiMgr:OpenJianShuWnd(taskId, conditionId)
	LuaLiuRuShiMgr.m_JianShuInfo.bookId = conditionId
	LuaLiuRuShiMgr.m_JianShuInfo.taskId = taskId
	CUIManager.ShowUI(CLuaUIResources.LiuRuShiJianShuWnd)
end

