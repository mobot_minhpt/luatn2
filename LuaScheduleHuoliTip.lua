local NGUIMath = import "NGUIMath"
local CScheduleMgr = import "L10.Game.CScheduleMgr"

CLuaScheduleHuoliTip = class()
RegistClassMember(CLuaScheduleHuoliTip,"template")
RegistClassMember(CLuaScheduleHuoliTip,"grid")
RegistClassMember(CLuaScheduleHuoliTip,"huoliLabel")
RegistClassMember(CLuaScheduleHuoliTip,"node")
RegistClassMember(CLuaScheduleHuoliTip,"bgSprite")

function CLuaScheduleHuoliTip:Awake()
    self.template = self.transform:Find("Background/Template").gameObject
    self.template:SetActive(false)
    self.grid = self.transform:Find("Background/Node/Grid"):GetComponent(typeof(UIGrid))
    self.huoliLabel = self.transform:Find("Background/Node/HuoliLabel"):GetComponent(typeof(UILabel))
    self.node = self.transform:Find("Background/Node")
    self.bgSprite = self.transform:Find("Background"):GetComponent(typeof(UISprite))

end

function CLuaScheduleHuoliTip:Init( )
    local huoli = 0
    local maxHuoli = 0
    local infos = CScheduleMgr.Inst:GetCanJoinSchedules()
    local t = {}
    for i=1,infos.Count do
        table.insert( t,infos[i-1] )
    end

    local level = CClientMainPlayer.Inst and CClientMainPlayer.Inst.MaxLevel or 0
    local level2 = level
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.HasFeiSheng then
        level2 = CClientMainPlayer.Inst.XianShenLevel
    end

    table.sort( t,function(a,b)
        local schedule1= Task_Schedule.GetData(a.activityId)
        local schedule2= Task_Schedule.GetData(b.activityId)
        local MaxDailyHuoli1 = CLuaScheduleMgr.GetHuoli(schedule1.HuoLi) * CLuaScheduleMgr.GetDailyTimes(schedule1)
        local MaxDailyHuoli2 = CLuaScheduleMgr.GetHuoli(schedule2.HuoLi) * CLuaScheduleMgr.GetDailyTimes(schedule2)
        if MaxDailyHuoli1>MaxDailyHuoli2 then
            return true
        elseif MaxDailyHuoli1<MaxDailyHuoli2 then
            return false
        else
            return a.activityId<b.activityId
        end
    end )


    local processedSchedule = {}

    for i,item in ipairs(t) do
        local info = Task_Schedule.GetData(item.activityId)
        if CLuaScheduleMgr.ScheduleInfoFilter(item, info) and CLuaScheduleMgr.ScheduleInfoFilter2(item, info) then
	        if not (info.Type==nil or info.Type=="") and processedSchedule[info.Type] then
	        else
	            processedSchedule[info.Type] = true
	            if info ~= nil then
	                local curHuoli = CLuaScheduleMgr.GetHuoli(info.HuoLi)
					if info.Type == "NewBieSchoolTask" then
						curHuoli = NewbieSchoolTask_Setting.GetData().HuoLiAward
					end
	                local all = (curHuoli * CLuaScheduleMgr.GetDailyTimes(info))
	                maxHuoli = maxHuoli + all
	                if all > 0 then
	                    local go = NGUITools.AddChild(self.grid.gameObject,self.template)
	                    go:SetActive(true)
	                    local have = math.min(item.FinishedTimes, CLuaScheduleMgr.GetDailyTimes(info)) * curHuoli
	                    huoli = huoli + have
	                    local nameLabel =go.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
	                    local valLabel = go.transform:Find("ValLabel"):GetComponent(typeof(UILabel))
	                    nameLabel.text = info.TaskName
	                    valLabel.text = SafeStringFormat3("%d/%d",have,all)
	                end
	            end
	        end
	    end
    end
    self.grid:Reposition()

    self.huoliLabel.text = SafeStringFormat3("%d/%d", huoli, maxHuoli)

    local b = NGUIMath.CalculateRelativeWidgetBounds(self.node)
    self.bgSprite.height = math.floor(b.size.y) + 45
    self.node.gameObject:SetActive(false)
    self.node.gameObject:SetActive(true)
end

