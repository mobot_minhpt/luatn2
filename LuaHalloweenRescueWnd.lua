--import
local GameObject			= import "UnityEngine.GameObject"

local DelegateFactory       = import "DelegateFactory"
local UILabel               = import "UILabel"

local CommonDefs        = import "L10.Game.CommonDefs"

local CUITexture        = import "L10.UI.CUITexture"
--define 万圣节拯救开发组界面
CLuaHalloweenRescueWnd = class()

--RegistChildComponent
RegistChildComponent(CLuaHalloweenRescueWnd, "Des",		UILabel)
RegistChildComponent(CLuaHalloweenRescueWnd, "Letter1",	GameObject)
RegistChildComponent(CLuaHalloweenRescueWnd, "Letter2",	GameObject)
RegistChildComponent(CLuaHalloweenRescueWnd, "Letter3",	GameObject)
RegistChildComponent(CLuaHalloweenRescueWnd, "Letter4",	GameObject)
RegistChildComponent(CLuaHalloweenRescueWnd, "Letter5",	GameObject)
RegistChildComponent(CLuaHalloweenRescueWnd, "Letter6",	GameObject)

--RegistClassMember
RegistClassMember(CLuaHalloweenRescueWnd, "m_TempDatas")
RegistClassMember(CLuaHalloweenRescueWnd, "m_NetDatas")
RegistClassMember(CLuaHalloweenRescueWnd, "m_ctrls")
RegistClassMember(CLuaHalloweenRescueWnd, "m_Inited")

--@region flow function

function CLuaHalloweenRescueWnd:Init()
    self:InitData()
    self:InitView()
    self.m_Inited=true
    Gac2Gas.QueryHalloweenTaskProgress()
end

function CLuaHalloweenRescueWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSyncHalloweenTaskProgress", self, "OnSyncHalloweenTaskProgress")
    g_ScriptEvent:AddListener("SyncHalloweenCollectProgress", self, "SyncHalloweenCollectProgress")
end

function CLuaHalloweenRescueWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnSyncHalloweenTaskProgress", self, "OnSyncHalloweenTaskProgress")
    g_ScriptEvent:RemoveListener("SyncHalloweenCollectProgress", self, "SyncHalloweenCollectProgress")
end

--@endregion

function CLuaHalloweenRescueWnd:InitData()
    self.m_TempDatas = {}
    local mats = {
        "UI/Texture/Transparent/Material/HalloweenRescueWnd_cehua.mat",
        "UI/Texture/Transparent/Material/HalloweenRescueWnd_chengxu.mat",
        "UI/Texture/Transparent/Material/HalloweenRescueWnd_meishu.mat",
        "UI/Texture/Transparent/Material/HalloweenRescueWnd_ceshi.mat",
    }
    mats[0] = "UI/Texture/Transparent/Material/HalloweenRescueWnd_cehua.mat"
    Halloween_RescueMai.Foreach(function(tid, data)
        local newdata ={
            TemplateId = tid, 
            Description = data.Description, 
            NeedTimes = data.NeedTimes,
            Group = data.Group,
            Mat = mats[data.Group],
            Content = data.Content,
            TaskId = data.TaskId
        }
        table.insert(self.m_TempDatas,newdata)
    end)
end

function CLuaHalloweenRescueWnd:OnSyncHalloweenTaskProgress(list)
    -- print("OnSyncHalloweenTaskProgress")
    self.m_NetDatas={}
    local index = 0
    for i = 1,list.Count,3 do
		local progress = list[i-1]  	-- 当前进度
		local totalProgress = list[i] 	-- 总进度
		local status = list[i+1]		-- 当前状态 EnumHalloweenTaskStatus
		-- EnumHalloweenTaskStatus = {
		-- 		Locked 	= 0, -- 没有解锁,
		-- 		Unlock 	= 1, -- 解锁成功
		--		Doing 	= 2, -- 进行中
		--		Done 	= 3, -- 完成
        -- }
        index = index + 1
        self.m_NetDatas[index]={Progress = progress,State = status }
        if self.m_Inited then 
            self:RefreshLetter(index)
        end
	end
end


function CLuaHalloweenRescueWnd:SyncHalloweenCollectProgress(mailId)
    local tdata = self.m_TempDatas[mailId]           --在接受任务时弹出信封
    CLuaHalloweenLetterWnd.Show(tdata.Content,tdata.Group)
end

function CLuaHalloweenRescueWnd:InitView()
    self.Des.text = g_MessageMgr:FormatMessage("Halloween2019_Rescue_Des")
    self.m_ctrls={self.Letter1,self.Letter2,self.Letter3,self.Letter4,self.Letter5,self.Letter6}
    for i = 1,#self.m_TempDatas do
        self:InitLetter(i)
    end
end

--[[
    @desc: 信封点击事件，如果已经解锁且接受，则可以查看信封内容
    author:CodeGize
    time:2019-08-28 18:01:09
    --@index: 
    @return:
]]
function CLuaHalloweenRescueWnd:OnLetterClick(index)
    if self.m_NetDatas == nil then return end
    local ndata = self.m_NetDatas[index]
    if ndata == nil or ndata.State < 2 then 
        g_MessageMgr:ShowMessage("ZHENGJIUKFZ_CONDITIONS_NOT_ACHIEVED")
        return 
    end --没有服务器数据或者未接受该任务(含未解锁)
    local tdata = self.m_TempDatas[index]
    CLuaHalloweenLetterWnd.Show(tdata.Content,tdata.Group)
end

--[[
    @desc: 初始化信封，如类型
    author:CodeGize
    time:2019-08-28 17:44:32
    --@index: 
    @return:
]]
function CLuaHalloweenRescueWnd:InitLetter(index)
    local tdata = self.m_TempDatas[index]

    local ctrl = self.m_ctrls[index]
    local trans = ctrl.transform
    local grouptex = trans:Find("bg/Texture/Texture"):GetComponent(typeof(CUITexture))
    grouptex:LoadMaterial(tdata.Mat)

    local letter = trans:Find("bg/Texture").gameObject
    local click = function(go)
        self:OnLetterClick(index)
    end
    CommonDefs.AddOnClickListener(letter, DelegateFactory.Action_GameObject(click), false)
end

--[[
    @desc: 刷新信封的显示
    author:CodeGize
    time:2019-08-15 15:21:19
    --@index: 控件索引，1开始
    --@state: 0-未满足条件；1-等待接受；2-进行中；3-已完成
    @return:
]]
function CLuaHalloweenRescueWnd:RefreshLetter(index)
    local ctrl = self.m_ctrls[index]
    local trans = ctrl.transform
    local mask = trans:Find("bg/TextureMask").gameObject
    local lbcon = trans:Find("LbCondition").gameObject:GetComponent(typeof(UILabel))
    local btn = trans:Find("BtnDoTask").gameObject
    local sping = trans:Find("SpProcessing").gameObject
    local spcmp = trans:Find("SpCompleted").gameObject

    local state = self.m_NetDatas[index].State
    local progress = self.m_NetDatas[index].Progress
    local f = self.m_TempDatas[index].Description
    local times = self.m_TempDatas[index].NeedTimes
    
    mask:SetActive(state >= 2)
    lbcon.gameObject:SetActive(state == 0)
    if state == 0 then
        lbcon.text = f.."\n("..progress.."/"..times..")"
    end
    btn:SetActive(state == 1)
    if state == 1 then
        local click = function(go)
            self:OnAcceptTask(index)
        end
        CommonDefs.AddOnClickListener(btn,DelegateFactory.Action_GameObject(click),false)
    end
    sping:SetActive(state == 2)
    spcmp:SetActive(state == 3)
end

function CLuaHalloweenRescueWnd:OnAcceptTask(index)
    Gac2Gas.RequestAccTaskByHalloweenIdx(index)
end
