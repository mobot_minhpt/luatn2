local max = math.max
local min = math.min
local abs = math.abs
local ceil = math.ceil
local floor = math.floor
local log = math.log
local power = function(a, b) return a^b end

local EnumLingShouFightProp = {
	PermanentCor = 1,
	PermanentSta = 2,
	PermanentStr = 3,
	PermanentInt = 4,
	PermanentAgi = 5,
	PermanentHpFull = 6,
	Hp = 7,
	Mp = 8,
	CurrentHpFull = 9,
	CurrentMpFull = 10,
	EquipExchangeMF = 11,
	RevisePermanentCor = 12,
	RevisePermanentSta = 13,
	RevisePermanentStr = 14,
	RevisePermanentInt = 15,
	RevisePermanentAgi = 16,
	Class = 2001,
	Grade = 2002,
	Cor = 2003,
	AdjCor = 2004,
	MulCor = 2005,
	Sta = 2006,
	AdjSta = 2007,
	MulSta = 2008,
	Str = 2009,
	AdjStr = 2010,
	MulStr = 2011,
	Int = 2012,
	AdjInt = 2013,
	MulInt = 2014,
	Agi = 2015,
	AdjAgi = 2016,
	MulAgi = 2017,
	HpFull = 2018,
	AdjHpFull = 2019,
	MulHpFull = 2020,
	HpRecover = 2021,
	AdjHpRecover = 2022,
	MulHpRecover = 2023,
	MpFull = 2024,
	AdjMpFull = 2025,
	MulMpFull = 2026,
	MpRecover = 2027,
	AdjMpRecover = 2028,
	MulMpRecover = 2029,
	pAttMin = 2030,
	AdjpAttMin = 2031,
	MulpAttMin = 2032,
	pAttMax = 2033,
	AdjpAttMax = 2034,
	MulpAttMax = 2035,
	pHit = 2036,
	AdjpHit = 2037,
	MulpHit = 2038,
	pMiss = 2039,
	AdjpMiss = 2040,
	MulpMiss = 2041,
	pDef = 2042,
	AdjpDef = 2043,
	MulpDef = 2044,
	AdjpHurt = 2045,
	MulpHurt = 2046,
	pSpeed = 2047,
	OripSpeed = 2048,
	MulpSpeed = 2049,
	pFatal = 2050,
	AdjpFatal = 2051,
	AntipFatal = 2052,
	AdjAntipFatal = 2053,
	pFatalDamage = 2054,
	AdjpFatalDamage = 2055,
	AntipFatalDamage = 2056,
	AdjAntipFatalDamage = 2057,
	Block = 2058,
	AdjBlock = 2059,
	MulBlock = 2060,
	BlockDamage = 2061,
	AdjBlockDamage = 2062,
	AntiBlock = 2063,
	AdjAntiBlock = 2064,
	AntiBlockDamage = 2065,
	AdjAntiBlockDamage = 2066,
	mAttMin = 2067,
	AdjmAttMin = 2068,
	MulmAttMin = 2069,
	mAttMax = 2070,
	AdjmAttMax = 2071,
	MulmAttMax = 2072,
	mHit = 2073,
	AdjmHit = 2074,
	MulmHit = 2075,
	mMiss = 2076,
	AdjmMiss = 2077,
	MulmMiss = 2078,
	mDef = 2079,
	AdjmDef = 2080,
	MulmDef = 2081,
	AdjmHurt = 2082,
	MulmHurt = 2083,
	mSpeed = 2084,
	OrimSpeed = 2085,
	MulmSpeed = 2086,
	mFatal = 2087,
	AdjmFatal = 2088,
	AntimFatal = 2089,
	AdjAntimFatal = 2090,
	mFatalDamage = 2091,
	AdjmFatalDamage = 2092,
	AntimFatalDamage = 2093,
	AdjAntimFatalDamage = 2094,
	EnhanceFire = 2095,
	AdjEnhanceFire = 2096,
	MulEnhanceFire = 2097,
	EnhanceThunder = 2098,
	AdjEnhanceThunder = 2099,
	MulEnhanceThunder = 2100,
	EnhanceIce = 2101,
	AdjEnhanceIce = 2102,
	MulEnhanceIce = 2103,
	EnhancePoison = 2104,
	AdjEnhancePoison = 2105,
	MulEnhancePoison = 2106,
	EnhanceWind = 2107,
	AdjEnhanceWind = 2108,
	MulEnhanceWind = 2109,
	EnhanceLight = 2110,
	AdjEnhanceLight = 2111,
	MulEnhanceLight = 2112,
	EnhanceIllusion = 2113,
	AdjEnhanceIllusion = 2114,
	MulEnhanceIllusion = 2115,
	AntiFire = 2116,
	AdjAntiFire = 2117,
	MulAntiFire = 2118,
	AntiThunder = 2119,
	AdjAntiThunder = 2120,
	MulAntiThunder = 2121,
	AntiIce = 2122,
	AdjAntiIce = 2123,
	MulAntiIce = 2124,
	AntiPoison = 2125,
	AdjAntiPoison = 2126,
	MulAntiPoison = 2127,
	AntiWind = 2128,
	AdjAntiWind = 2129,
	MulAntiWind = 2130,
	AntiLight = 2131,
	AdjAntiLight = 2132,
	MulAntiLight = 2133,
	AntiIllusion = 2134,
	AdjAntiIllusion = 2135,
	MulAntiIllusion = 2136,
	IgnoreAntiFire = 2137,
	IgnoreAntiThunder = 2138,
	IgnoreAntiIce = 2139,
	IgnoreAntiPoison = 2140,
	IgnoreAntiWind = 2141,
	IgnoreAntiLight = 2142,
	IgnoreAntiIllusion = 2143,
	EnhanceDizzy = 2144,
	AdjEnhanceDizzy = 2145,
	MulEnhanceDizzy = 2146,
	EnhanceSleep = 2147,
	AdjEnhanceSleep = 2148,
	MulEnhanceSleep = 2149,
	EnhanceChaos = 2150,
	AdjEnhanceChaos = 2151,
	MulEnhanceChaos = 2152,
	EnhanceBind = 2153,
	AdjEnhanceBind = 2154,
	MulEnhanceBind = 2155,
	EnhanceSilence = 2156,
	AdjEnhanceSilence = 2157,
	MulEnhanceSilence = 2158,
	AntiDizzy = 2159,
	AdjAntiDizzy = 2160,
	MulAntiDizzy = 2161,
	AntiSleep = 2162,
	AdjAntiSleep = 2163,
	MulAntiSleep = 2164,
	AntiChaos = 2165,
	AdjAntiChaos = 2166,
	MulAntiChaos = 2167,
	AntiBind = 2168,
	AdjAntiBind = 2169,
	MulAntiBind = 2170,
	AntiSilence = 2171,
	AdjAntiSilence = 2172,
	MulAntiSilence = 2173,
	DizzyTimeChange = 2174,
	SleepTimeChange = 2175,
	ChaosTimeChange = 2176,
	BindTimeChange = 2177,
	SilenceTimeChange = 2178,
	BianhuTimeChange = 2179,
	FreezeTimeChange = 2180,
	PetrifyTimeChange = 2181,
	EnhanceHeal = 2182,
	AdjEnhanceHeal = 2183,
	MulEnhanceHeal = 2184,
	Speed = 2185,
	OriSpeed = 2186,
	AdjSpeed = 2187,
	MulSpeed = 2188,
	MF = 2189,
	AdjMF = 2190,
	Range = 2191,
	AdjRange = 2192,
	HatePlus = 2193,
	AdjHatePlus = 2194,
	HateDecrease = 2195,
	Invisible = 2196,
	AdjInvisible = 2197,
	TrueSight = 2198,
	OriTrueSight = 2199,
	AdjTrueSight = 2200,
	EyeSight = 2201,
	OriEyeSight = 2202,
	AdjEyeSight = 2203,
	EnhanceTian = 2204,
	EnhanceTianMul = 2205,
	EnhanceEGui = 2206,
	EnhanceEGuiMul = 2207,
	EnhanceXiuLuo = 2208,
	EnhanceXiuLuoMul = 2209,
	EnhanceDiYu = 2210,
	EnhanceDiYuMul = 2211,
	EnhanceRen = 2212,
	EnhanceRenMul = 2213,
	EnhanceChuSheng = 2214,
	EnhanceChuShengMul = 2215,
	EnhanceBuilding = 2216,
	EnhanceBuildingMul = 2217,
	EnhanceBoss = 2218,
	EnhanceBossMul = 2219,
	EnhanceSheShou = 2220,
	EnhanceSheShouMul = 2221,
	EnhanceJiaShi = 2222,
	EnhanceJiaShiMul = 2223,
	EnhanceFangShi = 2224,
	EnhanceFangShiMul = 2225,
	EnhanceYiShi = 2226,
	EnhanceYiShiMul = 2227,
	EnhanceMeiZhe = 2228,
	EnhanceMeiZheMul = 2229,
	EnhanceYiRen = 2230,
	EnhanceYiRenMul = 2231,
	IgnorepDef = 2232,
	IgnoremDef = 2233,
	EnhanceZhaoHuan = 2234,
	EnhanceZhaoHuanMul = 2235,
	DizzyProbability = 2236,
	SleepProbability = 2237,
	ChaosProbability = 2238,
	BindProbability = 2239,
	SilenceProbability = 2240,
	HpFullPow2 = 2241,
	HpFullPow1 = 2242,
	HpFullInit = 2243,
	MpFullPow1 = 2244,
	MpFullInit = 2245,
	MpRecoverPow1 = 2246,
	MpRecoverInit = 2247,
	PAttMinPow2 = 2248,
	PAttMinPow1 = 2249,
	PAttMinInit = 2250,
	PAttMaxPow2 = 2251,
	PAttMaxPow1 = 2252,
	PAttMaxInit = 2253,
	PhitPow1 = 2254,
	PHitInit = 2255,
	PMissPow1 = 2256,
	PMissInit = 2257,
	PSpeedPow1 = 2258,
	PSpeedInit = 2259,
	PDefPow1 = 2260,
	PDefInit = 2261,
	PfatalPow1 = 2262,
	PFatalInit = 2263,
	MAttMinPow2 = 2264,
	MAttMinPow1 = 2265,
	MAttMinInit = 2266,
	MAttMaxPow2 = 2267,
	MAttMaxPow1 = 2268,
	MAttMaxInit = 2269,
	MSpeedPow1 = 2270,
	MSpeedInit = 2271,
	MDefPow1 = 2272,
	MDefInit = 2273,
	MHitPow1 = 2274,
	MHitInit = 2275,
	MMissPow1 = 2276,
	MMissInit = 2277,
	MFatalPow1 = 2278,
	MFatalInit = 2279,
	BlockDamagePow1 = 2280,
	BlockDamageInit = 2281,
	RunSpeed = 2282,
	HpRecoverPow1 = 2283,
	HpRecoverInit = 2284,
	AntiBlockDamagePow1 = 2285,
	AntiBlockDamageInit = 2286,
	BBMax = 2287,
	AdjBBMax = 2288,
	AntiBreak = 2289,
	AdjAntiBreak = 2290,
	AntiPushPull = 2291,
	AdjAntiPushPull = 2292,
	AntiPetrify = 2293,
	AdjAntiPetrify = 2294,
	MulAntiPetrify = 2295,
	AntiTie = 2296,
	AdjAntiTie = 2297,
	MulAntiTie = 2298,
	EnhancePetrify = 2299,
	AdjEnhancePetrify = 2300,
	MulEnhancePetrify = 2301,
	EnhanceTie = 2302,
	AdjEnhanceTie = 2303,
	MulEnhanceTie = 2304,
	TieProbability = 2305,
	StrZizhi = 2306,
	CorZizhi = 2307,
	StaZizhi = 2308,
	AgiZizhi = 2309,
	IntZizhi = 2310,
	InitStrZizhi = 2311,
	InitCorZizhi = 2312,
	InitStaZizhi = 2313,
	InitAgiZizhi = 2314,
	InitIntZizhi = 2315,
	Wuxing = 2316,
	Xiuwei = 2317,
	SkillNum = 2318,
	PType = 2319,
	MType = 2320,
	PHType = 2321,
	GrowFactor = 2322,
	WuxingImprove = 2323,
	XiuweiImprove = 2324,
	EnhanceBeHeal = 2325,
	AdjEnhanceBeHeal = 2326,
	MulEnhanceBeHeal = 2327,
	LookUpCor = 2328,
	LookUpSta = 2329,
	LookUpStr = 2330,
	LookUpInt = 2331,
	LookUpAgi = 2332,
	LookUpHpFull = 2333,
	LookUpMpFull = 2334,
	LookUppAttMin = 2335,
	LookUppAttMax = 2336,
	LookUppHit = 2337,
	LookUppMiss = 2338,
	LookUppDef = 2339,
	LookUpmAttMin = 2340,
	LookUpmAttMax = 2341,
	LookUpmHit = 2342,
	LookUpmMiss = 2343,
	LookUpmDef = 2344,
	AdjHpFull2 = 2345,
	AdjpAttMin2 = 2346,
	AdjpAttMax2 = 2347,
	AdjmAttMin2 = 2348,
	AdjmAttMax2 = 2349,
	LingShouType = 2350,
	MHType = 2351,
	EnhanceLingShou = 2352,
	EnhanceLingShouMul = 2353,
	AntiAoe = 2354,
	AdjAntiAoe = 2355,
	AntiBlind = 2356,
	AdjAntiBlind = 2357,
	MulAntiBlind = 2358,
	AntiDecelerate = 2359,
	AdjAntiDecelerate = 2360,
	MulAntiDecelerate = 2361,
	MinGrade = 2362,
	CharacterCor = 2363,
	CharacterSta = 2364,
	CharacterStr = 2365,
	CharacterInt = 2366,
	CharacterAgi = 2367,
	EnhanceDaoKe = 2368,
	EnhanceDaoKeMul = 2369,
	ZuoQiSpeed = 2370,
	EnhanceBianHu = 2371,
	AdjEnhanceBianHu = 2372,
	MulEnhanceBianHu = 2373,
	AntiBianHu = 2374,
	AdjAntiBianHu = 2375,
	MulAntiBianHu = 2376,
	EnhanceXiaKe = 2377,
	EnhanceXiaKeMul = 2378,
	TieTimeChange = 2379,
	EnhanceYanShi = 2380,
	EnhanceYanShiMul = 2381,
	PAType = 2382,
	MAType = 2383,
	lifetimeNoReduceRate = 2384,
	AddLSHpDurgImprove = 2385,
	AddHpDurgImprove = 2386,
	AddMpDurgImprove = 2387,
	FireHurtReduce = 2388,
	ThunderHurtReduce = 2389,
	IceHurtReduce = 2390,
	PoisonHurtReduce = 2391,
	WindHurtReduce = 2392,
	LightHurtReduce = 2393,
	IllusionHurtReduce = 2394,
	FakepDef = 2395,
	FakemDef = 2396,
	EnhanceWater = 2397,
	AdjEnhanceWater = 2398,
	MulEnhanceWater = 2399,
	AntiWater = 2400,
	AdjAntiWater = 2401,
	MulAntiWater = 2402,
	WaterHurtReduce = 2403,
	IgnoreAntiWater = 2404,
	AntiFreeze = 2405,
	AdjAntiFreeze = 2406,
	MulAntiFreeze = 2407,
	EnhanceFreeze = 2408,
	AdjEnhanceFreeze = 2409,
	MulEnhanceFreeze = 2410,
	EnhanceHuaHun = 2411,
	EnhanceHuaHunMul = 2412,
	TrueSightProb = 2413,
	LSBBGrade = 2414,
	LSBBQuality = 2415,
	LSBBAdjHp = 2416,
	AdjIgnoreAntiFire = 2417,
	AdjIgnoreAntiThunder = 2418,
	AdjIgnoreAntiIce = 2419,
	AdjIgnoreAntiPoison = 2420,
	AdjIgnoreAntiWind = 2421,
	AdjIgnoreAntiLight = 2422,
	AdjIgnoreAntiIllusion = 2423,
	IgnoreAntiFireLSMul = 2424,
	IgnoreAntiThunderLSMul = 2425,
	IgnoreAntiIceLSMul = 2426,
	IgnoreAntiPoisonLSMul = 2427,
	IgnoreAntiWindLSMul = 2428,
	IgnoreAntiLightLSMul = 2429,
	IgnoreAntiIllusionLSMul = 2430,
	AdjIgnoreAntiWater = 2431,
	IgnoreAntiWaterLSMul = 2432,
	MulSpeed2 = 2433,
	MulConsumeMp = 2434,
	AdjAgi2 = 2435,
	AdjAntiBianHu2 = 2436,
	AdjAntiBind2 = 2437,
	AdjAntiBlind2 = 2438,
	AdjAntiBlock2 = 2439,
	AdjAntiBlockDamage2 = 2440,
	AdjAntiChaos2 = 2441,
	AdjAntiDecelerate2 = 2442,
	AdjAntiDizzy2 = 2443,
	AdjAntiFire2 = 2444,
	AdjAntiFreeze2 = 2445,
	AdjAntiIce2 = 2446,
	AdjAntiIllusion2 = 2447,
	AdjAntiLight2 = 2448,
	AdjAntimFatal2 = 2449,
	AdjAntimFatalDamage2 = 2450,
	AdjAntiPetrify2 = 2451,
	AdjAntipFatal2 = 2452,
	AdjAntipFatalDamage2 = 2453,
	AdjAntiPoison2 = 2454,
	AdjAntiSilence2 = 2455,
	AdjAntiSleep2 = 2456,
	AdjAntiThunder2 = 2457,
	AdjAntiTie2 = 2458,
	AdjAntiWater2 = 2459,
	AdjAntiWind2 = 2460,
	AdjBlock2 = 2461,
	AdjBlockDamage2 = 2462,
	AdjCor2 = 2463,
	AdjEnhanceBianHu2 = 2464,
	AdjEnhanceBind2 = 2465,
	AdjEnhanceChaos2 = 2466,
	AdjEnhanceDizzy2 = 2467,
	AdjEnhanceFire2 = 2468,
	AdjEnhanceFreeze2 = 2469,
	AdjEnhanceIce2 = 2470,
	AdjEnhanceIllusion2 = 2471,
	AdjEnhanceLight2 = 2472,
	AdjEnhancePetrify2 = 2473,
	AdjEnhancePoison2 = 2474,
	AdjEnhanceSilence2 = 2475,
	AdjEnhanceSleep2 = 2476,
	AdjEnhanceThunder2 = 2477,
	AdjEnhanceTie2 = 2478,
	AdjEnhanceWater2 = 2479,
	AdjEnhanceWind2 = 2480,
	AdjIgnoreAntiFire2 = 2481,
	AdjIgnoreAntiIce2 = 2482,
	AdjIgnoreAntiIllusion2 = 2483,
	AdjIgnoreAntiLight2 = 2484,
	AdjIgnoreAntiPoison2 = 2485,
	AdjIgnoreAntiThunder2 = 2486,
	AdjIgnoreAntiWater2 = 2487,
	AdjIgnoreAntiWind2 = 2488,
	AdjInt2 = 2489,
	AdjmDef2 = 2490,
	AdjmFatal2 = 2491,
	AdjmFatalDamage2 = 2492,
	AdjmHit2 = 2493,
	AdjmHurt2 = 2494,
	AdjmMiss2 = 2495,
	AdjmSpeed2 = 2496,
	AdjpDef2 = 2497,
	AdjpFatal2 = 2498,
	AdjpFatalDamage2 = 2499,
	AdjpHit2 = 2500,
	AdjpHurt2 = 2501,
	AdjpMiss2 = 2502,
	AdjpSpeed2 = 2503,
	AdjStr2 = 2504,
	LSpAttMin = 2505,
	LSpAttMax = 2506,
	LSmAttMin = 2507,
	LSmAttMax = 2508,
	LSpFatal = 2509,
	LSpFatalDamage = 2510,
	LSmFatal = 2511,
	LSmFatalDamage = 2512,
	LSAntiBlock = 2513,
	LSAntiBlockDamage = 2514,
	LSIgnoreAntiFire = 2515,
	LSIgnoreAntiThunder = 2516,
	LSIgnoreAntiIce = 2517,
	LSIgnoreAntiPoison = 2518,
	LSIgnoreAntiWind = 2519,
	LSIgnoreAntiLight = 2520,
	LSIgnoreAntiIllusion = 2521,
	LSIgnoreAntiWater = 2522,
	LSpAttMin_adj = 2523,
	LSpAttMax_adj = 2524,
	LSmAttMin_adj = 2525,
	LSmAttMax_adj = 2526,
	LSpFatal_adj = 2527,
	LSpFatalDamage_adj = 2528,
	LSmFatal_adj = 2529,
	LSmFatalDamage_adj = 2530,
	LSAntiBlock_adj = 2531,
	LSAntiBlockDamage_adj = 2532,
	LSIgnoreAntiFire_adj = 2533,
	LSIgnoreAntiThunder_adj = 2534,
	LSIgnoreAntiIce_adj = 2535,
	LSIgnoreAntiPoison_adj = 2536,
	LSIgnoreAntiWind_adj = 2537,
	LSIgnoreAntiLight_adj = 2538,
	LSIgnoreAntiIllusion_adj = 2539,
	LSIgnoreAntiWater_adj = 2540,
	LSpAttMin_jc = 2541,
	LSpAttMax_jc = 2542,
	LSmAttMin_jc = 2543,
	LSmAttMax_jc = 2544,
	LSpFatal_jc = 2545,
	LSpFatalDamage_jc = 2546,
	LSmFatal_jc = 2547,
	LSmFatalDamage_jc = 2548,
	LSAntiBlock_jc = 2549,
	LSAntiBlockDamage_jc = 2550,
	LSIgnoreAntiFire_jc = 2551,
	LSIgnoreAntiThunder_jc = 2552,
	LSIgnoreAntiIce_jc = 2553,
	LSIgnoreAntiPoison_jc = 2554,
	LSIgnoreAntiWind_jc = 2555,
	LSIgnoreAntiLight_jc = 2556,
	LSIgnoreAntiIllusion_jc = 2557,
	LSIgnoreAntiWater_jc = 2558,
	LSIgnoreAntiFire_mul = 2559,
	LSIgnoreAntiThunder_mul = 2560,
	LSIgnoreAntiIce_mul = 2561,
	LSIgnoreAntiPoison_mul = 2562,
	LSIgnoreAntiWind_mul = 2563,
	LSIgnoreAntiLight_mul = 2564,
	LSIgnoreAntiIllusion_mul = 2565,
	LSIgnoreAntiWater_mul = 2566,
	JieBan_Child_QiChangColor = 2567,
	JieBan_LingShou_Ratio = 2568,
	JieBan_LingShou_QinMi = 2569,
	JieBan_LingShou_CorZizhi = 2570,
	JieBan_LingShou_StaZizhi = 2571,
	JieBan_LingShou_StrZizhi = 2572,
	JieBan_LingShou_IntZizhi = 2573,
	JieBan_LingShou_AgiZizhi = 2574,
	Buff_DisplayValue1 = 2575,
	Buff_DisplayValue2 = 2576,
	Buff_DisplayValue3 = 2577,
	Buff_DisplayValue4 = 2578,
	Buff_DisplayValue5 = 2579,
	LSNature = 2580,
	PrayMulti = 2581,
	EnhanceYingLing = 2582,
	EnhanceYingLingMul = 2583,
	EnhanceFlag = 2584,
	EnhanceFlagMul = 2585,
	ObjectType = 2586,
	MonsterType = 2587,
	FightPropType = 2588,
	PlayHpFull = 2589,
	PlayHp = 2590,
	PlayAtt = 2591,
	PlayDef = 2592,
	PlayHit = 2593,
	PlayMiss = 2594,
	EnhanceDieKe = 2595,
	EnhanceDieKeMul = 2596,
	DotRemain = 2597,
	IsConfused = 2598,
	EnhanceSheShouKefu = 2599,
	EnhanceJiaShiKefu = 2600,
	EnhanceDaoKeKefu = 2601,
	EnhanceXiaKeKefu = 2602,
	EnhanceFangShiKefu = 2603,
	EnhanceYiShiKefu = 2604,
	EnhanceMeiZheKefu = 2605,
	EnhanceYiRenKefu = 2606,
	EnhanceYanShiKefu = 2607,
	EnhanceHuaHunKefu = 2608,
	EnhanceYingLingKefu = 2609,
	EnhanceDieKeKefu = 2610,
	AdjustPermanentCor = 2611,
	AdjustPermanentSta = 2612,
	AdjustPermanentStr = 2613,
	AdjustPermanentInt = 2614,
	AdjustPermanentAgi = 2615,
	SoulCoreLevel = 2616,
	AdjustPermanentMidCor = 2617,
	AdjustPermanentMidSta = 2618,
	AdjustPermanentMidStr = 2619,
	AdjustPermanentMidInt = 2620,
	AdjustPermanentMidAgi = 2621,
	SumPermanent = 2622,
	AntiTian = 2623,
	AntiEGui = 2624,
	AntiXiuLuo = 2625,
	AntiDiYu = 2626,
	AntiRen = 2627,
	AntiChuSheng = 2628,
	AntiBuilding = 2629,
	AntiZhaoHuan = 2630,
	AntiLingShou = 2631,
	AntiBoss = 2632,
	AntiSheShou = 2633,
	AntiJiaShi = 2634,
	AntiDaoKe = 2635,
	AntiXiaKe = 2636,
	AntiFangShi = 2637,
	AntiYiShi = 2638,
	AntiMeiZhe = 2639,
	AntiYiRen = 2640,
	AntiYanShi = 2641,
	AntiHuaHun = 2642,
	AntiYingLing = 2643,
	AntiDieKe = 2644,
	AntiFlag = 2645,
	EnhanceZhanKuang = 2646,
	EnhanceZhanKuangMul = 2647,
	EnhanceZhanKuangKefu = 2648,
	AntiZhanKuang = 2649,
	JumpSpeed = 2650,
	OriJumpSpeed = 2651,
	AdjJumpSpeed = 2652,
	GravityAcceleration = 2653,
	OriGravityAcceleration = 2654,
	AdjGravityAcceleration = 2655,
	HorizontalAcceleration = 2656,
	OriHorizontalAcceleration = 2657,
	AdjHorizontalAcceleration = 2658,
	SwimSpeed = 2659,
	OriSwimSpeed = 2660,
	AdjSwimSpeed = 2661,
	StrengthPoint = 2662,
	StrengthPointMax = 2663,
	OriStrengthPointMax = 2664,
	AdjStrengthPointMax = 2665,
	AntiMulEnhanceIllusion = 2666,
	GlideHorizontalSpeedWithUmbrella = 2667,
	GlideVerticalSpeedWithUmbrella = 2668,
	AdjpSpeed = 3001,
	AdjmSpeed = 3002,
	
}


CLuaLingShouPropertyFight = {}

CLuaLingShouPropertyFight.Param = nil
CLuaLingShouPropertyFight.TmpParam = nil
local function SetParam_At(id, v)
	CLuaLingShouPropertyFight.Param[id] = v
end
local function GetParam_At(id)
	return CLuaLingShouPropertyFight.Param[id] or 0
end
local function SetTmpParam_At(id, v)
	CLuaLingShouPropertyFight.TmpParam[id] = v
end
local function GetTmpParam_At(id)
	return CLuaLingShouPropertyFight.TmpParam[id] or 0
end

local function LookUpGrow1(clazz, target, keys, defaultValue)
	local key = 0
	for i,v in ipairs(keys) do
		if i==1 then
			key = v*1000
		else
			key = key + v
		end
	end
	local data = Grow_Grow1.GetData(key)
	if data then
		return CommonDefs.DictGetValue_LuaCall(data.PropValues,target)
	end
	return defaultValue
end

local function LookUpGrow2(clazz, target, keys, defaultValue)
	local key = ""
	for i,v in ipairs(keys) do
		if i==1 then
			key = tostring(v)
		else
			key = key.."_"..v
		end
	end
	local data = Grow_Grow2.GetData(key)
	if data then
		return CommonDefs.DictGetValue_LuaCall(data.PropValues,target)
	end
	return defaultValue
end

local CLingShouPropertyFight = import "L10.Game.CLingShouPropertyFight"
local SetParamFunctions = {}
CLingShouPropertyFight.m_hookSetParam = function(this,id,newv)
	CLuaLingShouPropertyFight.Param = this.Param
	CLuaLingShouPropertyFight.TmpParam = this.TmpParam
	if SetParamFunctions[id] then
		SetParamFunctions[id](newv)
	end
	CLuaLingShouPropertyFight.Param = nil
	CLuaLingShouPropertyFight.TmpParam = nil
end
CLuaLingShouPropertyFight.SetParamPermanentCor = function(newv)
	SetParam_At(1, newv)
end
CLuaLingShouPropertyFight.SetParamPermanentSta = function(newv)
	SetParam_At(2, newv)
end
CLuaLingShouPropertyFight.SetParamPermanentStr = function(newv)
	SetParam_At(3, newv)
end
CLuaLingShouPropertyFight.SetParamPermanentInt = function(newv)
	SetParam_At(4, newv)
end
CLuaLingShouPropertyFight.SetParamPermanentAgi = function(newv)
	SetParam_At(5, newv)
end
CLuaLingShouPropertyFight.SetParamPermanentHpFull = function(newv)
	SetParam_At(6, newv)
end
CLuaLingShouPropertyFight.SetParamHp = function(newv)
	SetParam_At(7, newv)
end
CLuaLingShouPropertyFight.SetParamMp = function(newv)
	SetParam_At(8, newv)
end
CLuaLingShouPropertyFight.SetParamCurrentHpFull = function(newv)
	SetParam_At(9, newv)
end
CLuaLingShouPropertyFight.SetParamCurrentMpFull = function(newv)
	SetParam_At(10, newv)
end
CLuaLingShouPropertyFight.SetParamEquipExchangeMF = function(newv)
	SetParam_At(11, newv)
end
CLuaLingShouPropertyFight.SetParamRevisePermanentCor = function(newv)
	SetParam_At(12, newv)
end
CLuaLingShouPropertyFight.SetParamRevisePermanentSta = function(newv)
	SetParam_At(13, newv)
end
CLuaLingShouPropertyFight.SetParamRevisePermanentStr = function(newv)
	SetParam_At(14, newv)
end
CLuaLingShouPropertyFight.SetParamRevisePermanentInt = function(newv)
	SetParam_At(15, newv)
end
CLuaLingShouPropertyFight.SetParamRevisePermanentAgi = function(newv)
	SetParam_At(16, newv)
end
CLuaLingShouPropertyFight.SetParamClass = function(newv)
	SetTmpParam_At(1, newv)
end
CLuaLingShouPropertyFight.SetParamGrade = function(newv)
	SetTmpParam_At(2, newv)

	CLuaLingShouPropertyFight.RefreshParamSta()

	CLuaLingShouPropertyFight.RefreshParamAgi()

	CLuaLingShouPropertyFight.RefreshParamCor()

	CLuaLingShouPropertyFight.RefreshParamInt()

	CLuaLingShouPropertyFight.RefreshParamStr()

	CLuaLingShouPropertyFight.RefreshParammMiss()

	CLuaLingShouPropertyFight.RefreshParampHit()

	CLuaLingShouPropertyFight.RefreshParammFatalDamage()

	CLuaLingShouPropertyFight.RefreshParammFatal()

	CLuaLingShouPropertyFight.RefreshParampFatal()

	CLuaLingShouPropertyFight.RefreshParammHit()

	CLuaLingShouPropertyFight.RefreshParampFatalDamage()

	CLuaLingShouPropertyFight.RefreshParampMiss()

	CLuaLingShouPropertyFight.RefreshParamHpFull()

	CLuaLingShouPropertyFight.RefreshParammAttMax()

	CLuaLingShouPropertyFight.RefreshParammAttMin()

	CLuaLingShouPropertyFight.RefreshParammDef()

	CLuaLingShouPropertyFight.RefreshParampDef()

	CLuaLingShouPropertyFight.RefreshParampAttMin()

	CLuaLingShouPropertyFight.RefreshParampAttMax()

	CLuaLingShouPropertyFight.RefreshParamHpRecover()
end
CLuaLingShouPropertyFight.RefreshParamCor = function()
	local AdjCor = GetTmpParam_At(4)
	local MulCor = GetTmpParam_At(5)
	local Xiuwei = GetTmpParam_At(317)
	local Grade = GetTmpParam_At(2)
	local PHType = GetTmpParam_At(321)
	local MHType = GetTmpParam_At(351)

	local newv = ((1+3*(PHType+MHType))*(Grade+(Grade-50)*min(1,max(0,Grade-50))+(Grade-100)*min(1,max(0,Grade-100))-3*max(0,Grade-150))+Xiuwei*2+AdjCor)*(1+MulCor)
	SetTmpParam_At(3, newv)
end
CLuaLingShouPropertyFight.SetParamAdjCor = function(newv)
	SetTmpParam_At(4, newv)

	CLuaLingShouPropertyFight.RefreshParamCor()

	CLuaLingShouPropertyFight.RefreshParamHpFull()

	CLuaLingShouPropertyFight.RefreshParamHpRecover()
end
CLuaLingShouPropertyFight.SetParamMulCor = function(newv)
	SetTmpParam_At(5, newv)

	CLuaLingShouPropertyFight.RefreshParamCor()

	CLuaLingShouPropertyFight.RefreshParamHpFull()

	CLuaLingShouPropertyFight.RefreshParamHpRecover()
end
CLuaLingShouPropertyFight.RefreshParamSta = function()
	local AdjSta = GetTmpParam_At(7)
	local MulSta = GetTmpParam_At(8)
	local Xiuwei = GetTmpParam_At(317)
	local Grade = GetTmpParam_At(2)

	local newv = (Grade+(Grade-50)*min(1,max(0,Grade-50))+(Grade-100)*min(1,max(0,Grade-100))-3*max(0,Grade-150)+Xiuwei*2+AdjSta)*(1+MulSta)
	SetTmpParam_At(6, newv)
end
CLuaLingShouPropertyFight.SetParamAdjSta = function(newv)
	SetTmpParam_At(7, newv)

	CLuaLingShouPropertyFight.RefreshParamSta()

	CLuaLingShouPropertyFight.RefreshParammDef()

	CLuaLingShouPropertyFight.RefreshParampDef()
end
CLuaLingShouPropertyFight.SetParamMulSta = function(newv)
	SetTmpParam_At(8, newv)

	CLuaLingShouPropertyFight.RefreshParamSta()

	CLuaLingShouPropertyFight.RefreshParammDef()

	CLuaLingShouPropertyFight.RefreshParampDef()
end
CLuaLingShouPropertyFight.RefreshParamStr = function()
	local AdjStr = GetTmpParam_At(10)
	local MulStr = GetTmpParam_At(11)
	local Xiuwei = GetTmpParam_At(317)
	local PAType = GetTmpParam_At(382)
	local PType = GetTmpParam_At(319)
	local Grade = GetTmpParam_At(2)
	local PHType = GetTmpParam_At(321)

	local newv = ((1+2*PHType+5*PType+3*PAType)*(Grade+(Grade-50)*min(1,max(0,Grade-50))+(Grade-100)*min(1,max(0,Grade-100))-3*max(0,Grade-150))+Xiuwei*2+AdjStr)*(1+MulStr)
	SetTmpParam_At(9, newv)
end
CLuaLingShouPropertyFight.SetParamAdjStr = function(newv)
	SetTmpParam_At(10, newv)

	CLuaLingShouPropertyFight.RefreshParamStr()

	CLuaLingShouPropertyFight.RefreshParampDef()

	CLuaLingShouPropertyFight.RefreshParampAttMin()

	CLuaLingShouPropertyFight.RefreshParampAttMax()
end
CLuaLingShouPropertyFight.SetParamMulStr = function(newv)
	SetTmpParam_At(11, newv)

	CLuaLingShouPropertyFight.RefreshParamStr()

	CLuaLingShouPropertyFight.RefreshParampDef()

	CLuaLingShouPropertyFight.RefreshParampAttMin()

	CLuaLingShouPropertyFight.RefreshParampAttMax()
end
CLuaLingShouPropertyFight.RefreshParamInt = function()
	local AdjInt = GetTmpParam_At(13)
	local MulInt = GetTmpParam_At(14)
	local Xiuwei = GetTmpParam_At(317)
	local MAType = GetTmpParam_At(383)
	local MType = GetTmpParam_At(320)
	local Grade = GetTmpParam_At(2)
	local MHType = GetTmpParam_At(351)

	local newv = ((1+2*MHType+5*MType+3*MAType)*(Grade+(Grade-50)*min(1,max(0,Grade-50))+(Grade-100)*min(1,max(0,Grade-100))-3*max(0,Grade-150))+Xiuwei*2+AdjInt)*(1+MulInt)
	SetTmpParam_At(12, newv)
end
CLuaLingShouPropertyFight.SetParamAdjInt = function(newv)
	SetTmpParam_At(13, newv)

	CLuaLingShouPropertyFight.RefreshParamInt()

	CLuaLingShouPropertyFight.RefreshParammAttMax()

	CLuaLingShouPropertyFight.RefreshParammAttMin()

	CLuaLingShouPropertyFight.RefreshParammDef()
end
CLuaLingShouPropertyFight.SetParamMulInt = function(newv)
	SetTmpParam_At(14, newv)

	CLuaLingShouPropertyFight.RefreshParamInt()

	CLuaLingShouPropertyFight.RefreshParammAttMax()

	CLuaLingShouPropertyFight.RefreshParammAttMin()

	CLuaLingShouPropertyFight.RefreshParammDef()
end
CLuaLingShouPropertyFight.RefreshParamAgi = function()
	local AdjAgi = GetTmpParam_At(16)
	local MulAgi = GetTmpParam_At(17)
	local Xiuwei = GetTmpParam_At(317)
	local MAType = GetTmpParam_At(383)
	local PAType = GetTmpParam_At(382)
	local Grade = GetTmpParam_At(2)

	local newv = ((1+2*PAType+2*MAType)*(Grade+(Grade-50)*min(1,max(0,Grade-50))+(Grade-100)*min(1,max(0,Grade-100))-3*max(0,Grade-150))+Xiuwei*2+AdjAgi)*(1+MulAgi)
	SetTmpParam_At(15, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAgi = function(newv)
	SetTmpParam_At(16, newv)

	CLuaLingShouPropertyFight.RefreshParamAgi()

	CLuaLingShouPropertyFight.RefreshParammMiss()

	CLuaLingShouPropertyFight.RefreshParampHit()

	CLuaLingShouPropertyFight.RefreshParammFatalDamage()

	CLuaLingShouPropertyFight.RefreshParammFatal()

	CLuaLingShouPropertyFight.RefreshParampFatal()

	CLuaLingShouPropertyFight.RefreshParammHit()

	CLuaLingShouPropertyFight.RefreshParampFatalDamage()

	CLuaLingShouPropertyFight.RefreshParampMiss()
end
CLuaLingShouPropertyFight.SetParamMulAgi = function(newv)
	SetTmpParam_At(17, newv)

	CLuaLingShouPropertyFight.RefreshParamAgi()

	CLuaLingShouPropertyFight.RefreshParammMiss()

	CLuaLingShouPropertyFight.RefreshParampHit()

	CLuaLingShouPropertyFight.RefreshParammFatalDamage()

	CLuaLingShouPropertyFight.RefreshParammFatal()

	CLuaLingShouPropertyFight.RefreshParampFatal()

	CLuaLingShouPropertyFight.RefreshParammHit()

	CLuaLingShouPropertyFight.RefreshParampFatalDamage()

	CLuaLingShouPropertyFight.RefreshParampMiss()
end
CLuaLingShouPropertyFight.RefreshParamHpFull = function()
	local LSBBAdjHp = GetTmpParam_At(416)
	local AdjHpFull = GetTmpParam_At(19)
	local MulHpFull = GetTmpParam_At(20)
	local GrowFactor = GetTmpParam_At(322)
	local Cor = GetTmpParam_At(3)
	local CorZizhi = GetTmpParam_At(307)
	local Grade = GetTmpParam_At(2)
	local MHType = GetTmpParam_At(351)
	local PHType = GetTmpParam_At(321)
	local MAType = GetTmpParam_At(383)
	local MType = GetTmpParam_At(320)
	local PType = GetTmpParam_At(319)
	local PAType = GetTmpParam_At(382)
	local SkillNum = GetTmpParam_At(318)

	local newv = max(1,floor((24*Grade*GrowFactor*(1+(PHType)+(MHType))-12*(PType+PAType+MType+MAType)*max(0,Grade-150)*GrowFactor+6.8*CorZizhi*Cor/1000+AdjHpFull)*(1+min(1,max(0,(SkillNum-3)/4))*0.1+MulHpFull)))+LSBBAdjHp
	SetTmpParam_At(18, newv)
end
CLuaLingShouPropertyFight.SetParamAdjHpFull = function(newv)
	SetTmpParam_At(19, newv)

	CLuaLingShouPropertyFight.RefreshParamHpFull()

	CLuaLingShouPropertyFight.RefreshParamHpRecover()
end
CLuaLingShouPropertyFight.SetParamMulHpFull = function(newv)
	SetTmpParam_At(20, newv)

	CLuaLingShouPropertyFight.RefreshParamHpFull()

	CLuaLingShouPropertyFight.RefreshParamHpRecover()
end
CLuaLingShouPropertyFight.RefreshParamHpRecover = function()
	local AdjHpRecover = GetTmpParam_At(22)
	local MulHpRecover = GetTmpParam_At(23)
	local HpFull = GetTmpParam_At(18)

	local newv = (AdjHpRecover+HpFull/200)*(1+ MulHpRecover)
	SetTmpParam_At(21, newv)
end
CLuaLingShouPropertyFight.SetParamAdjHpRecover = function(newv)
	SetTmpParam_At(22, newv)

	CLuaLingShouPropertyFight.RefreshParamHpRecover()
end
CLuaLingShouPropertyFight.SetParamMulHpRecover = function(newv)
	SetTmpParam_At(23, newv)

	CLuaLingShouPropertyFight.RefreshParamHpRecover()
end
CLuaLingShouPropertyFight.SetParamMpFull = function(newv)
	SetTmpParam_At(24, newv)
end
CLuaLingShouPropertyFight.SetParamAdjMpFull = function(newv)
	SetTmpParam_At(25, newv)
end
CLuaLingShouPropertyFight.SetParamMulMpFull = function(newv)
	SetTmpParam_At(26, newv)
end
CLuaLingShouPropertyFight.SetParamMpRecover = function(newv)
	SetTmpParam_At(27, newv)
end
CLuaLingShouPropertyFight.SetParamAdjMpRecover = function(newv)
	SetTmpParam_At(28, newv)
end
CLuaLingShouPropertyFight.SetParamMulMpRecover = function(newv)
	SetTmpParam_At(29, newv)
end
CLuaLingShouPropertyFight.RefreshParampAttMin = function()
	local AdjpAttMin = GetTmpParam_At(31)
	local MulpAttMin = GetTmpParam_At(32)
	local AdjpAttMax = GetTmpParam_At(34)
	local MulpAttMax = GetTmpParam_At(35)
	local GrowFactor = GetTmpParam_At(322)
	local Str = GetTmpParam_At(9)
	local StrZizhi = GetTmpParam_At(306)
	local SkillNum = GetTmpParam_At(318)
	local PType = GetTmpParam_At(319)
	local PAType = GetTmpParam_At(382)
	local Grade = GetTmpParam_At(2)

	local newv = min(max(1,((GrowFactor*(3.5*Grade+3.5*Grade*(PType+PAType)+5*max(0,Grade-50)+15*max(0,Grade-50)*(PType+PAType)+5*max(0,Grade-100)+15*max(0,Grade-100)*(PType+PAType)-(8+28*(PType+PAType))*(Grade-150)*min(1,max(0,Grade-150)))+1.8*StrZizhi*Str/1000+50)*0.8+AdjpAttMin)*(1+min(1,max(0,(SkillNum-3)/4))*0.1+MulpAttMin)),max(1,((GrowFactor*(3.5*Grade+3.5*Grade*(PType+PAType)+5*max(0,Grade-50)+15*max(0,Grade-50)*(PType+PAType)+5*max(0,Grade-100)+15*max(0,Grade-100)*(PType+PAType)-(8+28*(PType+PAType))*(Grade-150)*min(1,max(0,Grade-150)))+1.8*StrZizhi*Str/1000+50)*1.2+AdjpAttMax)*(1+min(1,max(0,(SkillNum-3)/4))*0.1+MulpAttMax)))
	SetTmpParam_At(30, newv)
end
CLuaLingShouPropertyFight.SetParamAdjpAttMin = function(newv)
	SetTmpParam_At(31, newv)

	CLuaLingShouPropertyFight.RefreshParampAttMin()
end
CLuaLingShouPropertyFight.SetParamMulpAttMin = function(newv)
	SetTmpParam_At(32, newv)

	CLuaLingShouPropertyFight.RefreshParampAttMin()
end
CLuaLingShouPropertyFight.RefreshParampAttMax = function()
	local AdjpAttMax = GetTmpParam_At(34)
	local MulpAttMax = GetTmpParam_At(35)
	local GrowFactor = GetTmpParam_At(322)
	local Str = GetTmpParam_At(9)
	local StrZizhi = GetTmpParam_At(306)
	local SkillNum = GetTmpParam_At(318)
	local PType = GetTmpParam_At(319)
	local PAType = GetTmpParam_At(382)
	local Grade = GetTmpParam_At(2)

	local newv = max(1,((GrowFactor*(3.5*Grade+3.5*Grade*(PType+PAType)+5*max(0,Grade-50)+15*max(0,Grade-50)*(PType+PAType)+5*max(0,Grade-100)+15*max(0,Grade-100)*(PType+PAType)-(8+28*(PType+PAType))*(Grade-150)*min(1,max(0,Grade-150)))+1.8*StrZizhi*Str/1000+50)*1.2+AdjpAttMax)*(1+min(1,max(0,(SkillNum-3)/4))*0.1+MulpAttMax))
	SetTmpParam_At(33, newv)
end
CLuaLingShouPropertyFight.SetParamAdjpAttMax = function(newv)
	SetTmpParam_At(34, newv)

	CLuaLingShouPropertyFight.RefreshParampAttMin()

	CLuaLingShouPropertyFight.RefreshParampAttMax()
end
CLuaLingShouPropertyFight.SetParamMulpAttMax = function(newv)
	SetTmpParam_At(35, newv)

	CLuaLingShouPropertyFight.RefreshParampAttMin()

	CLuaLingShouPropertyFight.RefreshParampAttMax()
end
CLuaLingShouPropertyFight.RefreshParampHit = function()
	local AdjpHit = GetTmpParam_At(37)
	local MulpHit = GetTmpParam_At(38)
	local Agi = GetTmpParam_At(15)
	local GrowFactor = GetTmpParam_At(322)
	local Grade = GetTmpParam_At(2)
	local AgiZizhi = GetTmpParam_At(309)
	local MAType = GetTmpParam_At(383)
	local PAType = GetTmpParam_At(382)

	local newv = ((2-0.4*PAType-0.4*MAType)*Grade*GrowFactor-(1-0.2*PAType-0.2*MAType)*max(0,Grade-150)*GrowFactor+Agi*min(1200,AgiZizhi)/(2000*(1+PAType+MAType))+Agi*max(0,AgiZizhi-1200)/(10000-6000*(PAType+MAType))-max(0,Agi-600-600*PAType-600*MAType)*(min(1200,AgiZizhi)/(2000*(1+PAType+MAType))+max(0,AgiZizhi-1200)/(10000-6000*(PAType+MAType)))*0.5+AdjpHit)*(1+MulpHit)
	SetTmpParam_At(36, newv)
end
CLuaLingShouPropertyFight.SetParamAdjpHit = function(newv)
	SetTmpParam_At(37, newv)

	CLuaLingShouPropertyFight.RefreshParampHit()
end
CLuaLingShouPropertyFight.SetParamMulpHit = function(newv)
	SetTmpParam_At(38, newv)

	CLuaLingShouPropertyFight.RefreshParampHit()
end
CLuaLingShouPropertyFight.RefreshParampMiss = function()
	local AdjpMiss = GetTmpParam_At(40)
	local MulpMiss = GetTmpParam_At(41)
	local Agi = GetTmpParam_At(15)
	local GrowFactor = GetTmpParam_At(322)
	local Grade = GetTmpParam_At(2)
	local MAType = GetTmpParam_At(383)
	local AgiZizhi = GetTmpParam_At(309)
	local PAType = GetTmpParam_At(382)
	local PType = GetTmpParam_At(319)
	local MType = GetTmpParam_At(320)

	local newv = (0.8*Grade*GrowFactor*(1+PAType+MAType)-0.2*(3+PAType+MAType)*max(0,Grade-150)*GrowFactor+Agi*min(1200,AgiZizhi)/(2000+2000*PAType+2000*MAType)+Agi*max(0,AgiZizhi-1200)/(2000+3200*(PType+MType)+2000*(PAType+MAType))-max(0,Agi-600-600*PAType-600*MAType)*(min(1200,AgiZizhi)/(2000+2000*(PAType+MAType))+max(0,AgiZizhi-1200)/(2000+3200*(PType+MType)+2000*(PAType+MAType)))*0.5+AdjpMiss)*(1+MulpMiss)
	SetTmpParam_At(39, newv)
end
CLuaLingShouPropertyFight.SetParamAdjpMiss = function(newv)
	SetTmpParam_At(40, newv)

	CLuaLingShouPropertyFight.RefreshParampMiss()
end
CLuaLingShouPropertyFight.SetParamMulpMiss = function(newv)
	SetTmpParam_At(41, newv)

	CLuaLingShouPropertyFight.RefreshParampMiss()
end
CLuaLingShouPropertyFight.RefreshParampDef = function()
	local AdjpDef = GetTmpParam_At(43)
	local MulpDef = GetTmpParam_At(44)
	local GrowFactor = GetTmpParam_At(322)
	local StrZizhi = GetTmpParam_At(306)
	local Str = GetTmpParam_At(9)
	local StaZizhi = GetTmpParam_At(308)
	local Sta = GetTmpParam_At(6)
	local Grade = GetTmpParam_At(2)
	local MHType = GetTmpParam_At(351)
	local PHType = GetTmpParam_At(321)
	local SkillNum = GetTmpParam_At(318)

	local newv = max(1,(3*Grade*GrowFactor*(1+PHType+MHType)-(1-PHType-MHType)*(Grade-150)*min(1,max(0,Grade-150))*GrowFactor+StaZizhi*Sta/2000*(4.5-4.25*(PHType+MHType))+StrZizhi*Str/2000*(0.1+4.1*(PHType+MHType))+35+AdjpDef)*(1+min(1,max(0,(SkillNum-3)/4))*0.1+MulpDef))
	SetTmpParam_At(42, newv)
end
CLuaLingShouPropertyFight.SetParamAdjpDef = function(newv)
	SetTmpParam_At(43, newv)

	CLuaLingShouPropertyFight.RefreshParampDef()
end
CLuaLingShouPropertyFight.SetParamMulpDef = function(newv)
	SetTmpParam_At(44, newv)

	CLuaLingShouPropertyFight.RefreshParampDef()
end
CLuaLingShouPropertyFight.SetParamAdjpHurt = function(newv)
	SetTmpParam_At(45, newv)
end
CLuaLingShouPropertyFight.SetParamMulpHurt = function(newv)
	SetTmpParam_At(46, newv)
end
CLuaLingShouPropertyFight.RefreshParampSpeed = function()
	local AdjpSpeed = GetTmpParam_At(1001)
	local MulpSpeed = GetTmpParam_At(49)

	local newv = (1+AdjpSpeed)*(1+ MulpSpeed)
	SetTmpParam_At(47, newv)
end
CLuaLingShouPropertyFight.SetParamOripSpeed = function(newv)
	SetTmpParam_At(48, newv)
end
CLuaLingShouPropertyFight.SetParamMulpSpeed = function(newv)
	SetTmpParam_At(49, newv)

	CLuaLingShouPropertyFight.RefreshParampSpeed()
end
CLuaLingShouPropertyFight.RefreshParampFatal = function()
	local AdjpFatal = GetTmpParam_At(51)
	local PAType = GetTmpParam_At(382)
	local MAType = GetTmpParam_At(383)
	local GrowFactor = GetTmpParam_At(322)
	local AgiZizhi = GetTmpParam_At(309)
	local Grade = GetTmpParam_At(2)
	local Agi = GetTmpParam_At(15)

	local newv = (0.201*min(150,Grade)*GrowFactor+min(1200,Agi)*AgiZizhi/110000*1.5)*(PAType+MAType)+AdjpFatal
	SetTmpParam_At(50, newv)
end
CLuaLingShouPropertyFight.SetParamAdjpFatal = function(newv)
	SetTmpParam_At(51, newv)

	CLuaLingShouPropertyFight.RefreshParampFatal()
end
CLuaLingShouPropertyFight.RefreshParamAntipFatal = function()
	local AdjAntipFatal = GetTmpParam_At(53)

	local newv = AdjAntipFatal
	SetTmpParam_At(52, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntipFatal = function(newv)
	SetTmpParam_At(53, newv)

	CLuaLingShouPropertyFight.RefreshParamAntipFatal()
end
CLuaLingShouPropertyFight.RefreshParampFatalDamage = function()
	local AdjpFatalDamage = GetTmpParam_At(55)
	local PAType = GetTmpParam_At(382)
	local MAType = GetTmpParam_At(383)
	local GrowFactor = GetTmpParam_At(322)
	local AgiZizhi = GetTmpParam_At(309)
	local Grade = GetTmpParam_At(2)
	local Agi = GetTmpParam_At(15)

	local newv = (AdjpFatalDamage + 0.5+(0.201*min(150,Grade)*GrowFactor+min(1200,Agi)*AgiZizhi/110000*1.5)*(PAType+MAType)/300)
	SetTmpParam_At(54, newv)
end
CLuaLingShouPropertyFight.SetParamAdjpFatalDamage = function(newv)
	SetTmpParam_At(55, newv)

	CLuaLingShouPropertyFight.RefreshParampFatalDamage()
end
CLuaLingShouPropertyFight.RefreshParamAntipFatalDamage = function()
	local AdjAntipFatalDamage = GetTmpParam_At(57)

	local newv = AdjAntipFatalDamage
	SetTmpParam_At(56, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntipFatalDamage = function(newv)
	SetTmpParam_At(57, newv)

	CLuaLingShouPropertyFight.RefreshParamAntipFatalDamage()
end
CLuaLingShouPropertyFight.RefreshParamBlock = function()
	local AdjBlock = GetTmpParam_At(59)
	local MulBlock = GetTmpParam_At(60)

	local newv = AdjBlock*(1+MulBlock)
	SetTmpParam_At(58, newv)
end
CLuaLingShouPropertyFight.SetParamAdjBlock = function(newv)
	SetTmpParam_At(59, newv)

	CLuaLingShouPropertyFight.RefreshParamBlock()
end
CLuaLingShouPropertyFight.SetParamMulBlock = function(newv)
	SetTmpParam_At(60, newv)

	CLuaLingShouPropertyFight.RefreshParamBlock()
end
CLuaLingShouPropertyFight.RefreshParamBlockDamage = function()
	local AdjBlockDamage = GetTmpParam_At(62)

	local newv = max(0,AdjBlockDamage)
	SetTmpParam_At(61, newv)
end
CLuaLingShouPropertyFight.SetParamAdjBlockDamage = function(newv)
	SetTmpParam_At(62, newv)

	CLuaLingShouPropertyFight.RefreshParamBlockDamage()
end
CLuaLingShouPropertyFight.RefreshParamAntiBlock = function()
	local AdjAntiBlock = GetTmpParam_At(64)

	local newv = AdjAntiBlock+1900
	SetTmpParam_At(63, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiBlock = function(newv)
	SetTmpParam_At(64, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiBlock()
end
CLuaLingShouPropertyFight.RefreshParamAntiBlockDamage = function()
	local AdjAntiBlockDamage = GetTmpParam_At(66)

	local newv = AdjAntiBlockDamage
	SetTmpParam_At(65, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiBlockDamage = function(newv)
	SetTmpParam_At(66, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiBlockDamage()
end
CLuaLingShouPropertyFight.RefreshParammAttMin = function()
	local AdjmAttMin = GetTmpParam_At(68)
	local MulmAttMin = GetTmpParam_At(69)
	local AdjmAttMax = GetTmpParam_At(71)
	local MulmAttMax = GetTmpParam_At(72)
	local GrowFactor = GetTmpParam_At(322)
	local IntZizhi = GetTmpParam_At(310)
	local Int = GetTmpParam_At(12)
	local SkillNum = GetTmpParam_At(318)
	local MType = GetTmpParam_At(320)
	local MAType = GetTmpParam_At(383)
	local Grade = GetTmpParam_At(2)

	local newv = min(max(1,((GrowFactor*(3.5*Grade+3.5*Grade*(MType+MAType)+5*max(0,Grade-50)+15*max(0,Grade-50)*(MType+MAType)+5*max(0,Grade-100)+15*max(0,Grade-100)*(MType+MAType)-(8+28*MType+37*MAType)*(Grade-150)*min(1,max(0,Grade-150)))+1.8*Int*IntZizhi/1000+50)*0.8+AdjmAttMin)*(1+min(1,max(0,(SkillNum-3)/4))*0.1+MulmAttMin)),max(1,((GrowFactor*(3.5*Grade+3.5*Grade*(MType+MAType)+5*max(0,Grade-50)+15*max(0,Grade-50)*(MType+MAType)+5*max(0,Grade-100)+15*max(0,Grade-100)*(MType+MAType)-(8+28*MType+37*MAType)*(Grade-150)*min(1,max(0,Grade-150)))+1.8*Int*IntZizhi/1000+50)*1.2+AdjmAttMax)*(1+min(1,max(0,(SkillNum-3)/4))*0.1+MulmAttMax)))
	SetTmpParam_At(67, newv)
end
CLuaLingShouPropertyFight.SetParamAdjmAttMin = function(newv)
	SetTmpParam_At(68, newv)

	CLuaLingShouPropertyFight.RefreshParammAttMin()
end
CLuaLingShouPropertyFight.SetParamMulmAttMin = function(newv)
	SetTmpParam_At(69, newv)

	CLuaLingShouPropertyFight.RefreshParammAttMin()
end
CLuaLingShouPropertyFight.RefreshParammAttMax = function()
	local AdjmAttMax = GetTmpParam_At(71)
	local MulmAttMax = GetTmpParam_At(72)
	local GrowFactor = GetTmpParam_At(322)
	local IntZizhi = GetTmpParam_At(310)
	local Int = GetTmpParam_At(12)
	local SkillNum = GetTmpParam_At(318)
	local MType = GetTmpParam_At(320)
	local MAType = GetTmpParam_At(383)
	local Grade = GetTmpParam_At(2)

	local newv = max(1,((GrowFactor*(3.5*Grade+3.5*Grade*(MType+MAType)+5*max(0,Grade-50)+15*max(0,Grade-50)*(MType+MAType)+5*max(0,Grade-100)+15*max(0,Grade-100)*(MType+MAType)-(8+28*MType+37*MAType)*(Grade-150)*min(1,max(0,Grade-150)))+1.8*Int*IntZizhi/1000+50)*1.2+AdjmAttMax)*(1+min(1,max(0,(SkillNum-3)/4))*0.1+MulmAttMax))
	SetTmpParam_At(70, newv)
end
CLuaLingShouPropertyFight.SetParamAdjmAttMax = function(newv)
	SetTmpParam_At(71, newv)

	CLuaLingShouPropertyFight.RefreshParammAttMax()

	CLuaLingShouPropertyFight.RefreshParammAttMin()
end
CLuaLingShouPropertyFight.SetParamMulmAttMax = function(newv)
	SetTmpParam_At(72, newv)

	CLuaLingShouPropertyFight.RefreshParammAttMax()

	CLuaLingShouPropertyFight.RefreshParammAttMin()
end
CLuaLingShouPropertyFight.RefreshParammHit = function()
	local AdjmHit = GetTmpParam_At(74)
	local MulmHit = GetTmpParam_At(75)
	local Agi = GetTmpParam_At(15)
	local GrowFactor = GetTmpParam_At(322)
	local Grade = GetTmpParam_At(2)
	local AgiZizhi = GetTmpParam_At(309)
	local MAType = GetTmpParam_At(383)
	local PAType = GetTmpParam_At(382)

	local newv = ((2-0.4*PAType-0.4*MAType)*Grade*GrowFactor-(1-0.2*PAType-0.2*MAType)*max(0,Grade-150)*GrowFactor+Agi*min(1200,AgiZizhi)/(2000*(1+PAType+MAType))+Agi*max(0,AgiZizhi-1200)/(10000-6000*(PAType+MAType))-max(0,Agi-600-600*PAType-600*MAType)*(min(1200,AgiZizhi)/(2000*(1+PAType+MAType))+max(0,AgiZizhi-1200)/(10000-6000*(PAType+MAType)))*0.5+AdjmHit)*(1+MulmHit)
	SetTmpParam_At(73, newv)
end
CLuaLingShouPropertyFight.SetParamAdjmHit = function(newv)
	SetTmpParam_At(74, newv)

	CLuaLingShouPropertyFight.RefreshParammHit()
end
CLuaLingShouPropertyFight.SetParamMulmHit = function(newv)
	SetTmpParam_At(75, newv)

	CLuaLingShouPropertyFight.RefreshParammHit()
end
CLuaLingShouPropertyFight.RefreshParammMiss = function()
	local AdjmMiss = GetTmpParam_At(77)
	local MulmMiss = GetTmpParam_At(78)
	local Agi = GetTmpParam_At(15)
	local GrowFactor = GetTmpParam_At(322)
	local Grade = GetTmpParam_At(2)
	local MAType = GetTmpParam_At(383)
	local AgiZizhi = GetTmpParam_At(309)
	local PAType = GetTmpParam_At(382)
	local PType = GetTmpParam_At(319)
	local MType = GetTmpParam_At(320)

	local newv = (0.8*Grade*GrowFactor*(1+PAType+MAType)-0.2*(3+PAType+MAType)*max(0,Grade-150)*GrowFactor+Agi*min(1200,AgiZizhi)/(2000+2000*PAType+2000*MAType)+Agi*max(0,AgiZizhi-1200)/(2000+3200*(PType+MType)+2000*(PAType+MAType))-max(0,Agi-600-600*PAType-600*MAType)*(min(1200,AgiZizhi)/(2000+2000*(PAType+MAType))+max(0,AgiZizhi-1200)/(2000+3200*(PType+MType)+2000*(PAType+MAType)))*0.5+AdjmMiss)*(1+MulmMiss)
	SetTmpParam_At(76, newv)
end
CLuaLingShouPropertyFight.SetParamAdjmMiss = function(newv)
	SetTmpParam_At(77, newv)

	CLuaLingShouPropertyFight.RefreshParammMiss()
end
CLuaLingShouPropertyFight.SetParamMulmMiss = function(newv)
	SetTmpParam_At(78, newv)

	CLuaLingShouPropertyFight.RefreshParammMiss()
end
CLuaLingShouPropertyFight.RefreshParammDef = function()
	local AdjmDef = GetTmpParam_At(80)
	local MulmDef = GetTmpParam_At(81)
	local GrowFactor = GetTmpParam_At(322)
	local IntZizhi = GetTmpParam_At(310)
	local Int = GetTmpParam_At(12)
	local StaZizhi = GetTmpParam_At(308)
	local Sta = GetTmpParam_At(6)
	local Grade = GetTmpParam_At(2)
	local MHType = GetTmpParam_At(351)
	local PHType = GetTmpParam_At(321)
	local SkillNum = GetTmpParam_At(318)

	local newv = max(1,(3*Grade*GrowFactor*(1+PHType+MHType)-(1-PHType-MHType)*(Grade-150)*min(1,max(0,Grade-150))*GrowFactor+StaZizhi*Sta/2000*(4.5-4.25*(PHType+MHType))+IntZizhi*Int/2000*(0.1+4.1*(PHType+MHType))+35+AdjmDef)*(1+min(1,max(0,(SkillNum-3)/4))*0.1+MulmDef))
	SetTmpParam_At(79, newv)
end
CLuaLingShouPropertyFight.SetParamAdjmDef = function(newv)
	SetTmpParam_At(80, newv)

	CLuaLingShouPropertyFight.RefreshParammDef()
end
CLuaLingShouPropertyFight.SetParamMulmDef = function(newv)
	SetTmpParam_At(81, newv)

	CLuaLingShouPropertyFight.RefreshParammDef()
end
CLuaLingShouPropertyFight.SetParamAdjmHurt = function(newv)
	SetTmpParam_At(82, newv)
end
CLuaLingShouPropertyFight.SetParamMulmHurt = function(newv)
	SetTmpParam_At(83, newv)
end
CLuaLingShouPropertyFight.RefreshParammSpeed = function()
	local AdjmSpeed = GetTmpParam_At(1002)
	local MulmSpeed = GetTmpParam_At(86)

	local newv = (1+AdjmSpeed)*(1+ MulmSpeed)
	SetTmpParam_At(84, newv)
end
CLuaLingShouPropertyFight.SetParamOrimSpeed = function(newv)
	SetTmpParam_At(85, newv)
end
CLuaLingShouPropertyFight.SetParamMulmSpeed = function(newv)
	SetTmpParam_At(86, newv)

	CLuaLingShouPropertyFight.RefreshParammSpeed()
end
CLuaLingShouPropertyFight.RefreshParammFatal = function()
	local AdjmFatal = GetTmpParam_At(88)
	local PAType = GetTmpParam_At(382)
	local MAType = GetTmpParam_At(383)
	local GrowFactor = GetTmpParam_At(322)
	local AgiZizhi = GetTmpParam_At(309)
	local Grade = GetTmpParam_At(2)
	local Agi = GetTmpParam_At(15)

	local newv = (0.201*min(150,Grade)*GrowFactor+min(1200,Agi)*AgiZizhi/110000*1.5)*(PAType+MAType)+AdjmFatal
	SetTmpParam_At(87, newv)
end
CLuaLingShouPropertyFight.SetParamAdjmFatal = function(newv)
	SetTmpParam_At(88, newv)

	CLuaLingShouPropertyFight.RefreshParammFatal()
end
CLuaLingShouPropertyFight.RefreshParamAntimFatal = function()
	local AdjAntimFatal = GetTmpParam_At(90)

	local newv = AdjAntimFatal
	SetTmpParam_At(89, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntimFatal = function(newv)
	SetTmpParam_At(90, newv)

	CLuaLingShouPropertyFight.RefreshParamAntimFatal()
end
CLuaLingShouPropertyFight.RefreshParammFatalDamage = function()
	local AdjmFatalDamage = GetTmpParam_At(92)
	local PAType = GetTmpParam_At(382)
	local MAType = GetTmpParam_At(383)
	local GrowFactor = GetTmpParam_At(322)
	local AgiZizhi = GetTmpParam_At(309)
	local Grade = GetTmpParam_At(2)
	local Agi = GetTmpParam_At(15)

	local newv = (AdjmFatalDamage+0.5+(0.201*min(150,Grade)*GrowFactor+min(1200,Agi)*AgiZizhi/110000*1.5)*(PAType+MAType)/300)
	SetTmpParam_At(91, newv)
end
CLuaLingShouPropertyFight.SetParamAdjmFatalDamage = function(newv)
	SetTmpParam_At(92, newv)

	CLuaLingShouPropertyFight.RefreshParammFatalDamage()
end
CLuaLingShouPropertyFight.RefreshParamAntimFatalDamage = function()
	local AdjAntimFatalDamage = GetTmpParam_At(94)

	local newv = AdjAntimFatalDamage
	SetTmpParam_At(93, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntimFatalDamage = function(newv)
	SetTmpParam_At(94, newv)

	CLuaLingShouPropertyFight.RefreshParamAntimFatalDamage()
end
CLuaLingShouPropertyFight.RefreshParamEnhanceFire = function()
	local AdjEnhanceFire = GetTmpParam_At(96)

	local newv = AdjEnhanceFire
	SetTmpParam_At(95, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceFire = function(newv)
	SetTmpParam_At(96, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceFire()
end
CLuaLingShouPropertyFight.SetParamMulEnhanceFire = function(newv)
	SetTmpParam_At(97, newv)
end
CLuaLingShouPropertyFight.RefreshParamEnhanceThunder = function()
	local AdjEnhanceThunder = GetTmpParam_At(99)

	local newv = AdjEnhanceThunder
	SetTmpParam_At(98, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceThunder = function(newv)
	SetTmpParam_At(99, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceThunder()
end
CLuaLingShouPropertyFight.SetParamMulEnhanceThunder = function(newv)
	SetTmpParam_At(100, newv)
end
CLuaLingShouPropertyFight.RefreshParamEnhanceIce = function()
	local AdjEnhanceIce = GetTmpParam_At(102)

	local newv = AdjEnhanceIce
	SetTmpParam_At(101, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceIce = function(newv)
	SetTmpParam_At(102, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceIce()
end
CLuaLingShouPropertyFight.SetParamMulEnhanceIce = function(newv)
	SetTmpParam_At(103, newv)
end
CLuaLingShouPropertyFight.RefreshParamEnhancePoison = function()
	local AdjEnhancePoison = GetTmpParam_At(105)

	local newv = AdjEnhancePoison
	SetTmpParam_At(104, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhancePoison = function(newv)
	SetTmpParam_At(105, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhancePoison()
end
CLuaLingShouPropertyFight.SetParamMulEnhancePoison = function(newv)
	SetTmpParam_At(106, newv)
end
CLuaLingShouPropertyFight.RefreshParamEnhanceWind = function()
	local AdjEnhanceWind = GetTmpParam_At(108)

	local newv = AdjEnhanceWind
	SetTmpParam_At(107, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceWind = function(newv)
	SetTmpParam_At(108, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceWind()
end
CLuaLingShouPropertyFight.SetParamMulEnhanceWind = function(newv)
	SetTmpParam_At(109, newv)
end
CLuaLingShouPropertyFight.RefreshParamEnhanceLight = function()
	local AdjEnhanceLight = GetTmpParam_At(111)

	local newv = AdjEnhanceLight
	SetTmpParam_At(110, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceLight = function(newv)
	SetTmpParam_At(111, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceLight()
end
CLuaLingShouPropertyFight.SetParamMulEnhanceLight = function(newv)
	SetTmpParam_At(112, newv)
end
CLuaLingShouPropertyFight.RefreshParamEnhanceIllusion = function()
	local AdjEnhanceIllusion = GetTmpParam_At(114)

	local newv = AdjEnhanceIllusion
	SetTmpParam_At(113, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceIllusion = function(newv)
	SetTmpParam_At(114, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceIllusion()
end
CLuaLingShouPropertyFight.SetParamMulEnhanceIllusion = function(newv)
	SetTmpParam_At(115, newv)
end
CLuaLingShouPropertyFight.RefreshParamAntiFire = function()
	local AdjAntiFire = GetTmpParam_At(117)
	local MulAntiFire = GetTmpParam_At(118)

	local newv = AdjAntiFire*(1+MulAntiFire)
	SetTmpParam_At(116, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiFire = function(newv)
	SetTmpParam_At(117, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiFire()
end
CLuaLingShouPropertyFight.SetParamMulAntiFire = function(newv)
	SetTmpParam_At(118, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiFire()
end
CLuaLingShouPropertyFight.RefreshParamAntiThunder = function()
	local AdjAntiThunder = GetTmpParam_At(120)
	local MulAntiThunder = GetTmpParam_At(121)

	local newv = AdjAntiThunder*(1+MulAntiThunder)
	SetTmpParam_At(119, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiThunder = function(newv)
	SetTmpParam_At(120, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiThunder()
end
CLuaLingShouPropertyFight.SetParamMulAntiThunder = function(newv)
	SetTmpParam_At(121, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiThunder()
end
CLuaLingShouPropertyFight.RefreshParamAntiIce = function()
	local AdjAntiIce = GetTmpParam_At(123)
	local MulAntiIce = GetTmpParam_At(124)

	local newv = AdjAntiIce*(1+MulAntiIce)
	SetTmpParam_At(122, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiIce = function(newv)
	SetTmpParam_At(123, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiIce()
end
CLuaLingShouPropertyFight.SetParamMulAntiIce = function(newv)
	SetTmpParam_At(124, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiIce()
end
CLuaLingShouPropertyFight.RefreshParamAntiPoison = function()
	local AdjAntiPoison = GetTmpParam_At(126)
	local MulAntiPoison = GetTmpParam_At(127)

	local newv = AdjAntiPoison*(1+MulAntiPoison)
	SetTmpParam_At(125, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiPoison = function(newv)
	SetTmpParam_At(126, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiPoison()
end
CLuaLingShouPropertyFight.SetParamMulAntiPoison = function(newv)
	SetTmpParam_At(127, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiPoison()
end
CLuaLingShouPropertyFight.RefreshParamAntiWind = function()
	local AdjAntiWind = GetTmpParam_At(129)
	local MulAntiWind = GetTmpParam_At(130)

	local newv = AdjAntiWind*(1+MulAntiWind)
	SetTmpParam_At(128, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiWind = function(newv)
	SetTmpParam_At(129, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiWind()
end
CLuaLingShouPropertyFight.SetParamMulAntiWind = function(newv)
	SetTmpParam_At(130, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiWind()
end
CLuaLingShouPropertyFight.RefreshParamAntiLight = function()
	local AdjAntiLight = GetTmpParam_At(132)
	local MulAntiLight = GetTmpParam_At(133)

	local newv = AdjAntiLight*(1+MulAntiLight)
	SetTmpParam_At(131, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiLight = function(newv)
	SetTmpParam_At(132, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiLight()
end
CLuaLingShouPropertyFight.SetParamMulAntiLight = function(newv)
	SetTmpParam_At(133, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiLight()
end
CLuaLingShouPropertyFight.RefreshParamAntiIllusion = function()
	local AdjAntiIllusion = GetTmpParam_At(135)
	local MulAntiIllusion = GetTmpParam_At(136)

	local newv = AdjAntiIllusion*(1+MulAntiIllusion)
	SetTmpParam_At(134, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiIllusion = function(newv)
	SetTmpParam_At(135, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiIllusion()
end
CLuaLingShouPropertyFight.SetParamMulAntiIllusion = function(newv)
	SetTmpParam_At(136, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiIllusion()
end
CLuaLingShouPropertyFight.RefreshParamIgnoreAntiFire = function()
	local AdjIgnoreAntiFire = GetTmpParam_At(417)
	local IgnoreAntiFireLSMul = GetTmpParam_At(424)

	local newv = AdjIgnoreAntiFire*(1+IgnoreAntiFireLSMul)
	SetTmpParam_At(137, newv)
end
CLuaLingShouPropertyFight.RefreshParamIgnoreAntiThunder = function()
	local AdjIgnoreAntiThunder = GetTmpParam_At(418)
	local IgnoreAntiThunderLSMul = GetTmpParam_At(425)

	local newv = AdjIgnoreAntiThunder*(1+IgnoreAntiThunderLSMul)
	SetTmpParam_At(138, newv)
end
CLuaLingShouPropertyFight.RefreshParamIgnoreAntiIce = function()
	local AdjIgnoreAntiIce = GetTmpParam_At(419)
	local IgnoreAntiIceLSMul = GetTmpParam_At(426)

	local newv = AdjIgnoreAntiIce*(1+IgnoreAntiIceLSMul)
	SetTmpParam_At(139, newv)
end
CLuaLingShouPropertyFight.RefreshParamIgnoreAntiPoison = function()
	local AdjIgnoreAntiPoison = GetTmpParam_At(420)
	local IgnoreAntiPoisonLSMul = GetTmpParam_At(427)

	local newv = AdjIgnoreAntiPoison*(1+IgnoreAntiPoisonLSMul)
	SetTmpParam_At(140, newv)
end
CLuaLingShouPropertyFight.RefreshParamIgnoreAntiWind = function()
	local AdjIgnoreAntiWind = GetTmpParam_At(421)
	local IgnoreAntiWindLSMul = GetTmpParam_At(428)

	local newv = AdjIgnoreAntiWind*(1+IgnoreAntiWindLSMul)
	SetTmpParam_At(141, newv)
end
CLuaLingShouPropertyFight.RefreshParamIgnoreAntiLight = function()
	local AdjIgnoreAntiLight = GetTmpParam_At(422)
	local IgnoreAntiLightLSMul = GetTmpParam_At(429)

	local newv = AdjIgnoreAntiLight*(1+IgnoreAntiLightLSMul)
	SetTmpParam_At(142, newv)
end
CLuaLingShouPropertyFight.RefreshParamIgnoreAntiIllusion = function()
	local AdjIgnoreAntiIllusion = GetTmpParam_At(423)
	local IgnoreAntiIllusionLSMul = GetTmpParam_At(430)

	local newv = AdjIgnoreAntiIllusion*(1+IgnoreAntiIllusionLSMul)
	SetTmpParam_At(143, newv)
end
CLuaLingShouPropertyFight.RefreshParamEnhanceDizzy = function()
	local AdjEnhanceDizzy = GetTmpParam_At(145)
	local MulEnhanceDizzy = GetTmpParam_At(146)

	local newv = AdjEnhanceDizzy*(1+MulEnhanceDizzy)
	SetTmpParam_At(144, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceDizzy = function(newv)
	SetTmpParam_At(145, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceDizzy()
end
CLuaLingShouPropertyFight.SetParamMulEnhanceDizzy = function(newv)
	SetTmpParam_At(146, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceDizzy()
end
CLuaLingShouPropertyFight.RefreshParamEnhanceSleep = function()
	local AdjEnhanceSleep = GetTmpParam_At(148)
	local MulEnhanceSleep = GetTmpParam_At(149)

	local newv = AdjEnhanceSleep*(1+MulEnhanceSleep)
	SetTmpParam_At(147, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceSleep = function(newv)
	SetTmpParam_At(148, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceSleep()
end
CLuaLingShouPropertyFight.SetParamMulEnhanceSleep = function(newv)
	SetTmpParam_At(149, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceSleep()
end
CLuaLingShouPropertyFight.RefreshParamEnhanceChaos = function()
	local AdjEnhanceChaos = GetTmpParam_At(151)
	local MulEnhanceChaos = GetTmpParam_At(152)

	local newv = AdjEnhanceChaos*(1+MulEnhanceChaos)
	SetTmpParam_At(150, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceChaos = function(newv)
	SetTmpParam_At(151, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceChaos()
end
CLuaLingShouPropertyFight.SetParamMulEnhanceChaos = function(newv)
	SetTmpParam_At(152, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceChaos()
end
CLuaLingShouPropertyFight.RefreshParamEnhanceBind = function()
	local AdjEnhanceBind = GetTmpParam_At(154)
	local MulEnhanceBind = GetTmpParam_At(155)

	local newv = AdjEnhanceBind*(1+MulEnhanceBind)
	SetTmpParam_At(153, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceBind = function(newv)
	SetTmpParam_At(154, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceBind()
end
CLuaLingShouPropertyFight.SetParamMulEnhanceBind = function(newv)
	SetTmpParam_At(155, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceBind()
end
CLuaLingShouPropertyFight.RefreshParamEnhanceSilence = function()
	local AdjEnhanceSilence = GetTmpParam_At(157)
	local MulEnhanceSilence = GetTmpParam_At(158)

	local newv = AdjEnhanceSilence*(1+MulEnhanceSilence)
	SetTmpParam_At(156, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceSilence = function(newv)
	SetTmpParam_At(157, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceSilence()
end
CLuaLingShouPropertyFight.SetParamMulEnhanceSilence = function(newv)
	SetTmpParam_At(158, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceSilence()
end
CLuaLingShouPropertyFight.RefreshParamAntiDizzy = function()
	local AdjAntiDizzy = GetTmpParam_At(160)
	local MulAntiDizzy = GetTmpParam_At(161)

	local newv = AdjAntiDizzy*(1+MulAntiDizzy)
	SetTmpParam_At(159, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiDizzy = function(newv)
	SetTmpParam_At(160, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiDizzy()
end
CLuaLingShouPropertyFight.SetParamMulAntiDizzy = function(newv)
	SetTmpParam_At(161, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiDizzy()
end
CLuaLingShouPropertyFight.RefreshParamAntiSleep = function()
	local AdjAntiSleep = GetTmpParam_At(163)
	local MulAntiSleep = GetTmpParam_At(164)

	local newv = AdjAntiSleep*(1+MulAntiSleep)
	SetTmpParam_At(162, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiSleep = function(newv)
	SetTmpParam_At(163, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiSleep()
end
CLuaLingShouPropertyFight.SetParamMulAntiSleep = function(newv)
	SetTmpParam_At(164, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiSleep()
end
CLuaLingShouPropertyFight.RefreshParamAntiChaos = function()
	local AdjAntiChaos = GetTmpParam_At(166)
	local MulAntiChaos = GetTmpParam_At(167)

	local newv = AdjAntiChaos*(1+MulAntiChaos)
	SetTmpParam_At(165, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiChaos = function(newv)
	SetTmpParam_At(166, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiChaos()
end
CLuaLingShouPropertyFight.SetParamMulAntiChaos = function(newv)
	SetTmpParam_At(167, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiChaos()
end
CLuaLingShouPropertyFight.RefreshParamAntiBind = function()
	local AdjAntiBind = GetTmpParam_At(169)
	local MulAntiBind = GetTmpParam_At(170)

	local newv = AdjAntiBind*(1+MulAntiBind)
	SetTmpParam_At(168, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiBind = function(newv)
	SetTmpParam_At(169, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiBind()
end
CLuaLingShouPropertyFight.SetParamMulAntiBind = function(newv)
	SetTmpParam_At(170, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiBind()
end
CLuaLingShouPropertyFight.RefreshParamAntiSilence = function()
	local AdjAntiSilence = GetTmpParam_At(172)
	local MulAntiSilence = GetTmpParam_At(173)

	local newv = AdjAntiSilence*(1+MulAntiSilence)
	SetTmpParam_At(171, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiSilence = function(newv)
	SetTmpParam_At(172, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiSilence()
end
CLuaLingShouPropertyFight.SetParamMulAntiSilence = function(newv)
	SetTmpParam_At(173, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiSilence()
end
CLuaLingShouPropertyFight.SetParamDizzyTimeChange = function(newv)
	SetTmpParam_At(174, newv)
end
CLuaLingShouPropertyFight.SetParamSleepTimeChange = function(newv)
	SetTmpParam_At(175, newv)
end
CLuaLingShouPropertyFight.SetParamChaosTimeChange = function(newv)
	SetTmpParam_At(176, newv)
end
CLuaLingShouPropertyFight.SetParamBindTimeChange = function(newv)
	SetTmpParam_At(177, newv)
end
CLuaLingShouPropertyFight.SetParamSilenceTimeChange = function(newv)
	SetTmpParam_At(178, newv)
end
CLuaLingShouPropertyFight.SetParamBianhuTimeChange = function(newv)
	SetTmpParam_At(179, newv)
end
CLuaLingShouPropertyFight.SetParamFreezeTimeChange = function(newv)
	SetTmpParam_At(180, newv)
end
CLuaLingShouPropertyFight.SetParamPetrifyTimeChange = function(newv)
	SetTmpParam_At(181, newv)
end
CLuaLingShouPropertyFight.RefreshParamEnhanceHeal = function()
	local AdjEnhanceHeal = GetTmpParam_At(183)

	local newv = AdjEnhanceHeal
	SetTmpParam_At(182, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceHeal = function(newv)
	SetTmpParam_At(183, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceHeal()
end
CLuaLingShouPropertyFight.SetParamMulEnhanceHeal = function(newv)
	SetTmpParam_At(184, newv)
end
CLuaLingShouPropertyFight.RefreshParamSpeed = function()
	local AdjSpeed = GetTmpParam_At(187)
	local MulSpeed = GetTmpParam_At(188)

	local newv = (4.5+AdjSpeed)*max(0.3,1+MulSpeed)
	SetTmpParam_At(185, newv)
end
CLuaLingShouPropertyFight.SetParamOriSpeed = function(newv)
	SetTmpParam_At(186, newv)
end
CLuaLingShouPropertyFight.SetParamAdjSpeed = function(newv)
	SetTmpParam_At(187, newv)

	CLuaLingShouPropertyFight.RefreshParamSpeed()
end
CLuaLingShouPropertyFight.SetParamMulSpeed = function(newv)
	SetTmpParam_At(188, newv)

	CLuaLingShouPropertyFight.RefreshParamSpeed()
end
CLuaLingShouPropertyFight.SetParamMF = function(newv)
	SetTmpParam_At(189, newv)
end
CLuaLingShouPropertyFight.SetParamAdjMF = function(newv)
	SetTmpParam_At(190, newv)
end
CLuaLingShouPropertyFight.SetParamRange = function(newv)
	SetTmpParam_At(191, newv)
end
CLuaLingShouPropertyFight.SetParamAdjRange = function(newv)
	SetTmpParam_At(192, newv)
end
CLuaLingShouPropertyFight.SetParamHatePlus = function(newv)
	SetTmpParam_At(193, newv)
end
CLuaLingShouPropertyFight.SetParamAdjHatePlus = function(newv)
	SetTmpParam_At(194, newv)
end
CLuaLingShouPropertyFight.SetParamHateDecrease = function(newv)
	SetTmpParam_At(195, newv)
end
CLuaLingShouPropertyFight.RefreshParamInvisible = function()
	local AdjInvisible = GetTmpParam_At(197)

	local newv = AdjInvisible
	SetTmpParam_At(196, newv)
end
CLuaLingShouPropertyFight.SetParamAdjInvisible = function(newv)
	SetTmpParam_At(197, newv)

	CLuaLingShouPropertyFight.RefreshParamInvisible()
end
CLuaLingShouPropertyFight.RefreshParamTrueSight = function()
	local OriTrueSight = GetTmpParam_At(199)
	local AdjTrueSight = GetTmpParam_At(200)

	local newv = OriTrueSight + AdjTrueSight
	SetTmpParam_At(198, newv)
end
CLuaLingShouPropertyFight.SetParamOriTrueSight = function(newv)
	SetTmpParam_At(199, newv)

	CLuaLingShouPropertyFight.RefreshParamTrueSight()
end
CLuaLingShouPropertyFight.SetParamAdjTrueSight = function(newv)
	SetTmpParam_At(200, newv)

	CLuaLingShouPropertyFight.RefreshParamTrueSight()
end
CLuaLingShouPropertyFight.RefreshParamEyeSight = function()
	local AdjEyeSight = GetTmpParam_At(203)

	local newv = 15 + AdjEyeSight
	SetTmpParam_At(201, newv)
end
CLuaLingShouPropertyFight.SetParamOriEyeSight = function(newv)
	SetTmpParam_At(202, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEyeSight = function(newv)
	SetTmpParam_At(203, newv)

	CLuaLingShouPropertyFight.RefreshParamEyeSight()
end
CLuaLingShouPropertyFight.SetParamEnhanceTian = function(newv)
	SetTmpParam_At(204, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceTianMul = function(newv)
	SetTmpParam_At(205, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceEGui = function(newv)
	SetTmpParam_At(206, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceEGuiMul = function(newv)
	SetTmpParam_At(207, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceXiuLuo = function(newv)
	SetTmpParam_At(208, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceXiuLuoMul = function(newv)
	SetTmpParam_At(209, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceDiYu = function(newv)
	SetTmpParam_At(210, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceDiYuMul = function(newv)
	SetTmpParam_At(211, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceRen = function(newv)
	SetTmpParam_At(212, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceRenMul = function(newv)
	SetTmpParam_At(213, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceChuSheng = function(newv)
	SetTmpParam_At(214, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceChuShengMul = function(newv)
	SetTmpParam_At(215, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceBuilding = function(newv)
	SetTmpParam_At(216, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceBuildingMul = function(newv)
	SetTmpParam_At(217, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceBoss = function(newv)
	SetTmpParam_At(218, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceBossMul = function(newv)
	SetTmpParam_At(219, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceSheShou = function(newv)
	SetTmpParam_At(220, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceSheShouMul = function(newv)
	SetTmpParam_At(221, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceJiaShi = function(newv)
	SetTmpParam_At(222, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceJiaShiMul = function(newv)
	SetTmpParam_At(223, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceFangShi = function(newv)
	SetTmpParam_At(224, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceFangShiMul = function(newv)
	SetTmpParam_At(225, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceYiShi = function(newv)
	SetTmpParam_At(226, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceYiShiMul = function(newv)
	SetTmpParam_At(227, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceMeiZhe = function(newv)
	SetTmpParam_At(228, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceMeiZheMul = function(newv)
	SetTmpParam_At(229, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceYiRen = function(newv)
	SetTmpParam_At(230, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceYiRenMul = function(newv)
	SetTmpParam_At(231, newv)
end
CLuaLingShouPropertyFight.SetParamIgnorepDef = function(newv)
	SetTmpParam_At(232, newv)
end
CLuaLingShouPropertyFight.SetParamIgnoremDef = function(newv)
	SetTmpParam_At(233, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceZhaoHuan = function(newv)
	SetTmpParam_At(234, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceZhaoHuanMul = function(newv)
	SetTmpParam_At(235, newv)
end
CLuaLingShouPropertyFight.SetParamDizzyProbability = function(newv)
	SetTmpParam_At(236, newv)
end
CLuaLingShouPropertyFight.SetParamSleepProbability = function(newv)
	SetTmpParam_At(237, newv)
end
CLuaLingShouPropertyFight.SetParamChaosProbability = function(newv)
	SetTmpParam_At(238, newv)
end
CLuaLingShouPropertyFight.SetParamBindProbability = function(newv)
	SetTmpParam_At(239, newv)
end
CLuaLingShouPropertyFight.SetParamSilenceProbability = function(newv)
	SetTmpParam_At(240, newv)
end
CLuaLingShouPropertyFight.SetParamHpFullPow2 = function(newv)
	SetTmpParam_At(241, newv)
end
CLuaLingShouPropertyFight.SetParamHpFullPow1 = function(newv)
	SetTmpParam_At(242, newv)
end
CLuaLingShouPropertyFight.SetParamHpFullInit = function(newv)
	SetTmpParam_At(243, newv)
end
CLuaLingShouPropertyFight.SetParamMpFullPow1 = function(newv)
	SetTmpParam_At(244, newv)
end
CLuaLingShouPropertyFight.SetParamMpFullInit = function(newv)
	SetTmpParam_At(245, newv)
end
CLuaLingShouPropertyFight.SetParamMpRecoverPow1 = function(newv)
	SetTmpParam_At(246, newv)
end
CLuaLingShouPropertyFight.SetParamMpRecoverInit = function(newv)
	SetTmpParam_At(247, newv)
end
CLuaLingShouPropertyFight.SetParamPAttMinPow2 = function(newv)
	SetTmpParam_At(248, newv)
end
CLuaLingShouPropertyFight.SetParamPAttMinPow1 = function(newv)
	SetTmpParam_At(249, newv)
end
CLuaLingShouPropertyFight.SetParamPAttMinInit = function(newv)
	SetTmpParam_At(250, newv)
end
CLuaLingShouPropertyFight.SetParamPAttMaxPow2 = function(newv)
	SetTmpParam_At(251, newv)
end
CLuaLingShouPropertyFight.SetParamPAttMaxPow1 = function(newv)
	SetTmpParam_At(252, newv)
end
CLuaLingShouPropertyFight.SetParamPAttMaxInit = function(newv)
	SetTmpParam_At(253, newv)
end
CLuaLingShouPropertyFight.SetParamPhitPow1 = function(newv)
	SetTmpParam_At(254, newv)
end
CLuaLingShouPropertyFight.SetParamPHitInit = function(newv)
	SetTmpParam_At(255, newv)
end
CLuaLingShouPropertyFight.SetParamPMissPow1 = function(newv)
	SetTmpParam_At(256, newv)
end
CLuaLingShouPropertyFight.SetParamPMissInit = function(newv)
	SetTmpParam_At(257, newv)
end
CLuaLingShouPropertyFight.SetParamPSpeedPow1 = function(newv)
	SetTmpParam_At(258, newv)
end
CLuaLingShouPropertyFight.SetParamPSpeedInit = function(newv)
	SetTmpParam_At(259, newv)
end
CLuaLingShouPropertyFight.SetParamPDefPow1 = function(newv)
	SetTmpParam_At(260, newv)
end
CLuaLingShouPropertyFight.SetParamPDefInit = function(newv)
	SetTmpParam_At(261, newv)
end
CLuaLingShouPropertyFight.SetParamPfatalPow1 = function(newv)
	SetTmpParam_At(262, newv)
end
CLuaLingShouPropertyFight.SetParamPFatalInit = function(newv)
	SetTmpParam_At(263, newv)
end
CLuaLingShouPropertyFight.SetParamMAttMinPow2 = function(newv)
	SetTmpParam_At(264, newv)
end
CLuaLingShouPropertyFight.SetParamMAttMinPow1 = function(newv)
	SetTmpParam_At(265, newv)
end
CLuaLingShouPropertyFight.SetParamMAttMinInit = function(newv)
	SetTmpParam_At(266, newv)
end
CLuaLingShouPropertyFight.SetParamMAttMaxPow2 = function(newv)
	SetTmpParam_At(267, newv)
end
CLuaLingShouPropertyFight.SetParamMAttMaxPow1 = function(newv)
	SetTmpParam_At(268, newv)
end
CLuaLingShouPropertyFight.SetParamMAttMaxInit = function(newv)
	SetTmpParam_At(269, newv)
end
CLuaLingShouPropertyFight.SetParamMSpeedPow1 = function(newv)
	SetTmpParam_At(270, newv)
end
CLuaLingShouPropertyFight.SetParamMSpeedInit = function(newv)
	SetTmpParam_At(271, newv)
end
CLuaLingShouPropertyFight.SetParamMDefPow1 = function(newv)
	SetTmpParam_At(272, newv)
end
CLuaLingShouPropertyFight.SetParamMDefInit = function(newv)
	SetTmpParam_At(273, newv)
end
CLuaLingShouPropertyFight.SetParamMHitPow1 = function(newv)
	SetTmpParam_At(274, newv)
end
CLuaLingShouPropertyFight.SetParamMHitInit = function(newv)
	SetTmpParam_At(275, newv)
end
CLuaLingShouPropertyFight.SetParamMMissPow1 = function(newv)
	SetTmpParam_At(276, newv)
end
CLuaLingShouPropertyFight.SetParamMMissInit = function(newv)
	SetTmpParam_At(277, newv)
end
CLuaLingShouPropertyFight.SetParamMFatalPow1 = function(newv)
	SetTmpParam_At(278, newv)
end
CLuaLingShouPropertyFight.SetParamMFatalInit = function(newv)
	SetTmpParam_At(279, newv)
end
CLuaLingShouPropertyFight.SetParamBlockDamagePow1 = function(newv)
	SetTmpParam_At(280, newv)
end
CLuaLingShouPropertyFight.SetParamBlockDamageInit = function(newv)
	SetTmpParam_At(281, newv)
end
CLuaLingShouPropertyFight.SetParamRunSpeed = function(newv)
	SetTmpParam_At(282, newv)
end
CLuaLingShouPropertyFight.SetParamHpRecoverPow1 = function(newv)
	SetTmpParam_At(283, newv)
end
CLuaLingShouPropertyFight.SetParamHpRecoverInit = function(newv)
	SetTmpParam_At(284, newv)
end
CLuaLingShouPropertyFight.SetParamAntiBlockDamagePow1 = function(newv)
	SetTmpParam_At(285, newv)
end
CLuaLingShouPropertyFight.SetParamAntiBlockDamageInit = function(newv)
	SetTmpParam_At(286, newv)
end
CLuaLingShouPropertyFight.SetParamBBMax = function(newv)
	SetTmpParam_At(287, newv)
end
CLuaLingShouPropertyFight.SetParamAdjBBMax = function(newv)
	SetTmpParam_At(288, newv)
end
CLuaLingShouPropertyFight.RefreshParamAntiBreak = function()
	local AdjAntiBreak = GetTmpParam_At(290)

	local newv = AdjAntiBreak
	SetTmpParam_At(289, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiBreak = function(newv)
	SetTmpParam_At(290, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiBreak()
end
CLuaLingShouPropertyFight.RefreshParamAntiPushPull = function()
	local AdjAntiPushPull = GetTmpParam_At(292)

	local newv = AdjAntiPushPull
	SetTmpParam_At(291, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiPushPull = function(newv)
	SetTmpParam_At(292, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiPushPull()
end
CLuaLingShouPropertyFight.RefreshParamAntiPetrify = function()
	local AdjAntiPetrify = GetTmpParam_At(294)
	local MulAntiPetrify = GetTmpParam_At(295)

	local newv = AdjAntiPetrify*(1+MulAntiPetrify)
	SetTmpParam_At(293, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiPetrify = function(newv)
	SetTmpParam_At(294, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiPetrify()
end
CLuaLingShouPropertyFight.SetParamMulAntiPetrify = function(newv)
	SetTmpParam_At(295, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiPetrify()
end
CLuaLingShouPropertyFight.RefreshParamAntiTie = function()
	local AdjAntiTie = GetTmpParam_At(297)
	local MulAntiTie = GetTmpParam_At(298)

	local newv = AdjAntiTie*(1+MulAntiTie)
	SetTmpParam_At(296, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiTie = function(newv)
	SetTmpParam_At(297, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiTie()
end
CLuaLingShouPropertyFight.SetParamMulAntiTie = function(newv)
	SetTmpParam_At(298, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiTie()
end
CLuaLingShouPropertyFight.RefreshParamEnhancePetrify = function()
	local AdjEnhancePetrify = GetTmpParam_At(300)
	local MulEnhancePetrify = GetTmpParam_At(301)

	local newv = AdjEnhancePetrify*(1+MulEnhancePetrify)
	SetTmpParam_At(299, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhancePetrify = function(newv)
	SetTmpParam_At(300, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhancePetrify()
end
CLuaLingShouPropertyFight.SetParamMulEnhancePetrify = function(newv)
	SetTmpParam_At(301, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhancePetrify()
end
CLuaLingShouPropertyFight.RefreshParamEnhanceTie = function()
	local AdjEnhanceTie = GetTmpParam_At(303)
	local MulEnhanceTie = GetTmpParam_At(304)

	local newv = AdjEnhanceTie*(1+MulEnhanceTie)
	SetTmpParam_At(302, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceTie = function(newv)
	SetTmpParam_At(303, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceTie()
end
CLuaLingShouPropertyFight.SetParamMulEnhanceTie = function(newv)
	SetTmpParam_At(304, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceTie()
end
CLuaLingShouPropertyFight.SetParamTieProbability = function(newv)
	SetTmpParam_At(305, newv)
end
CLuaLingShouPropertyFight.RefreshParamStrZizhi = function()
	local InitStrZizhi = GetTmpParam_At(311)
	local Xiuwei = GetTmpParam_At(317)
	local Wuxing = GetTmpParam_At(316)

	local newv = InitStrZizhi*(1+(Wuxing/100+max(0,Wuxing-2)/100+max(0,Wuxing-3)/100+max(0,Wuxing-4)/100+max(0,Wuxing-5)/100+max(0,Wuxing-6)/100+max(0,Wuxing-7)/100+max(0,Wuxing-8)/100+max(0,Wuxing-9)/100)+Xiuwei/200)
	SetTmpParam_At(306, newv)
end
CLuaLingShouPropertyFight.RefreshParamCorZizhi = function()
	local InitCorZizhi = GetTmpParam_At(312)
	local Xiuwei = GetTmpParam_At(317)
	local Wuxing = GetTmpParam_At(316)

	local newv = InitCorZizhi*(1+(Wuxing/100+max(0,Wuxing-2)/100+max(0,Wuxing-3)/100+max(0,Wuxing-4)/100+max(0,Wuxing-5)/100+max(0,Wuxing-6)/100+max(0,Wuxing-7)/100+max(0,Wuxing-8)/100+max(0,Wuxing-9)/100)+Xiuwei/200)
	SetTmpParam_At(307, newv)
end
CLuaLingShouPropertyFight.RefreshParamStaZizhi = function()
	local InitStaZizhi = GetTmpParam_At(313)
	local Xiuwei = GetTmpParam_At(317)
	local Wuxing = GetTmpParam_At(316)

	local newv = InitStaZizhi*(1+(Wuxing/100+max(0,Wuxing-2)/100+max(0,Wuxing-3)/100+max(0,Wuxing-4)/100+max(0,Wuxing-5)/100+max(0,Wuxing-6)/100+max(0,Wuxing-7)/100+max(0,Wuxing-8)/100+max(0,Wuxing-9)/100)+Xiuwei/200)
	SetTmpParam_At(308, newv)
end
CLuaLingShouPropertyFight.RefreshParamAgiZizhi = function()
	local InitAgiZizhi = GetTmpParam_At(314)
	local Xiuwei = GetTmpParam_At(317)
	local Wuxing = GetTmpParam_At(316)

	local newv = InitAgiZizhi*(1+(Wuxing/100+max(0,Wuxing-2)/100+max(0,Wuxing-3)/100+max(0,Wuxing-4)/100+max(0,Wuxing-5)/100+max(0,Wuxing-6)/100+max(0,Wuxing-7)/100+max(0,Wuxing-8)/100+max(0,Wuxing-9)/100)+Xiuwei/200)
	SetTmpParam_At(309, newv)
end
CLuaLingShouPropertyFight.RefreshParamIntZizhi = function()
	local InitIntZizhi = GetTmpParam_At(315)
	local Xiuwei = GetTmpParam_At(317)
	local Wuxing = GetTmpParam_At(316)

	local newv = InitIntZizhi*(1+(Wuxing/100+max(0,Wuxing-2)/100+max(0,Wuxing-3)/100+max(0,Wuxing-4)/100+max(0,Wuxing-5)/100+max(0,Wuxing-6)/100+max(0,Wuxing-7)/100+max(0,Wuxing-8)/100+max(0,Wuxing-9)/100)+Xiuwei/200)
	SetTmpParam_At(310, newv)
end
CLuaLingShouPropertyFight.SetParamInitStrZizhi = function(newv)
	SetTmpParam_At(311, newv)

	CLuaLingShouPropertyFight.RefreshParamStrZizhi()

	CLuaLingShouPropertyFight.RefreshParampDef()

	CLuaLingShouPropertyFight.RefreshParampAttMin()

	CLuaLingShouPropertyFight.RefreshParampAttMax()
end
CLuaLingShouPropertyFight.SetParamInitCorZizhi = function(newv)
	SetTmpParam_At(312, newv)

	CLuaLingShouPropertyFight.RefreshParamCorZizhi()

	CLuaLingShouPropertyFight.RefreshParamHpFull()

	CLuaLingShouPropertyFight.RefreshParamHpRecover()
end
CLuaLingShouPropertyFight.SetParamInitStaZizhi = function(newv)
	SetTmpParam_At(313, newv)

	CLuaLingShouPropertyFight.RefreshParamStaZizhi()

	CLuaLingShouPropertyFight.RefreshParammDef()

	CLuaLingShouPropertyFight.RefreshParampDef()
end
CLuaLingShouPropertyFight.SetParamInitAgiZizhi = function(newv)
	SetTmpParam_At(314, newv)

	CLuaLingShouPropertyFight.RefreshParamAgiZizhi()

	CLuaLingShouPropertyFight.RefreshParammMiss()

	CLuaLingShouPropertyFight.RefreshParampHit()

	CLuaLingShouPropertyFight.RefreshParammFatalDamage()

	CLuaLingShouPropertyFight.RefreshParammFatal()

	CLuaLingShouPropertyFight.RefreshParampFatal()

	CLuaLingShouPropertyFight.RefreshParammHit()

	CLuaLingShouPropertyFight.RefreshParampFatalDamage()

	CLuaLingShouPropertyFight.RefreshParampMiss()
end
CLuaLingShouPropertyFight.SetParamInitIntZizhi = function(newv)
	SetTmpParam_At(315, newv)

	CLuaLingShouPropertyFight.RefreshParamIntZizhi()

	CLuaLingShouPropertyFight.RefreshParammAttMax()

	CLuaLingShouPropertyFight.RefreshParammAttMin()

	CLuaLingShouPropertyFight.RefreshParammDef()
end
CLuaLingShouPropertyFight.SetParamWuxing = function(newv)
	SetTmpParam_At(316, newv)

	CLuaLingShouPropertyFight.RefreshParamWuxingImprove()

	CLuaLingShouPropertyFight.RefreshParamIntZizhi()

	CLuaLingShouPropertyFight.RefreshParamAgiZizhi()

	CLuaLingShouPropertyFight.RefreshParamCorZizhi()

	CLuaLingShouPropertyFight.RefreshParamStrZizhi()

	CLuaLingShouPropertyFight.RefreshParamStaZizhi()

	CLuaLingShouPropertyFight.RefreshParammMiss()

	CLuaLingShouPropertyFight.RefreshParampHit()

	CLuaLingShouPropertyFight.RefreshParammFatalDamage()

	CLuaLingShouPropertyFight.RefreshParammFatal()

	CLuaLingShouPropertyFight.RefreshParampFatal()

	CLuaLingShouPropertyFight.RefreshParammHit()

	CLuaLingShouPropertyFight.RefreshParampFatalDamage()

	CLuaLingShouPropertyFight.RefreshParampMiss()

	CLuaLingShouPropertyFight.RefreshParamHpFull()

	CLuaLingShouPropertyFight.RefreshParammAttMax()

	CLuaLingShouPropertyFight.RefreshParammAttMin()

	CLuaLingShouPropertyFight.RefreshParammDef()

	CLuaLingShouPropertyFight.RefreshParampDef()

	CLuaLingShouPropertyFight.RefreshParampAttMin()

	CLuaLingShouPropertyFight.RefreshParampAttMax()

	CLuaLingShouPropertyFight.RefreshParamHpRecover()
end
CLuaLingShouPropertyFight.SetParamXiuwei = function(newv)
	SetTmpParam_At(317, newv)

	CLuaLingShouPropertyFight.RefreshParamSta()

	CLuaLingShouPropertyFight.RefreshParamWuxingImprove()

	CLuaLingShouPropertyFight.RefreshParamIntZizhi()

	CLuaLingShouPropertyFight.RefreshParamAgiZizhi()

	CLuaLingShouPropertyFight.RefreshParamCorZizhi()

	CLuaLingShouPropertyFight.RefreshParamStrZizhi()

	CLuaLingShouPropertyFight.RefreshParamStaZizhi()

	CLuaLingShouPropertyFight.RefreshParamXiuweiImprove()

	CLuaLingShouPropertyFight.RefreshParamAgi()

	CLuaLingShouPropertyFight.RefreshParamCor()

	CLuaLingShouPropertyFight.RefreshParamInt()

	CLuaLingShouPropertyFight.RefreshParamStr()

	CLuaLingShouPropertyFight.RefreshParammMiss()

	CLuaLingShouPropertyFight.RefreshParampHit()

	CLuaLingShouPropertyFight.RefreshParammFatalDamage()

	CLuaLingShouPropertyFight.RefreshParammFatal()

	CLuaLingShouPropertyFight.RefreshParampFatal()

	CLuaLingShouPropertyFight.RefreshParammHit()

	CLuaLingShouPropertyFight.RefreshParampFatalDamage()

	CLuaLingShouPropertyFight.RefreshParampMiss()

	CLuaLingShouPropertyFight.RefreshParamHpFull()

	CLuaLingShouPropertyFight.RefreshParammAttMax()

	CLuaLingShouPropertyFight.RefreshParammAttMin()

	CLuaLingShouPropertyFight.RefreshParammDef()

	CLuaLingShouPropertyFight.RefreshParampDef()

	CLuaLingShouPropertyFight.RefreshParampAttMin()

	CLuaLingShouPropertyFight.RefreshParampAttMax()

	CLuaLingShouPropertyFight.RefreshParamHpRecover()
end
CLuaLingShouPropertyFight.SetParamSkillNum = function(newv)
	SetTmpParam_At(318, newv)

	CLuaLingShouPropertyFight.RefreshParamHpFull()

	CLuaLingShouPropertyFight.RefreshParammAttMax()

	CLuaLingShouPropertyFight.RefreshParammAttMin()

	CLuaLingShouPropertyFight.RefreshParammDef()

	CLuaLingShouPropertyFight.RefreshParampDef()

	CLuaLingShouPropertyFight.RefreshParampAttMin()

	CLuaLingShouPropertyFight.RefreshParampAttMax()

	CLuaLingShouPropertyFight.RefreshParamHpRecover()
end
CLuaLingShouPropertyFight.RefreshParamPType = function()
	local LingShouType = GetTmpParam_At(350)

	local newv = 1-min(1,abs(LingShouType-1))
	SetTmpParam_At(319, newv)
end
CLuaLingShouPropertyFight.RefreshParamMType = function()
	local LingShouType = GetTmpParam_At(350)

	local newv = 1-min(1,abs(LingShouType-2))
	SetTmpParam_At(320, newv)
end
CLuaLingShouPropertyFight.RefreshParamPHType = function()
	local LingShouType = GetTmpParam_At(350)

	local newv = 1-min(1,abs(LingShouType-3))
	SetTmpParam_At(321, newv)
end
CLuaLingShouPropertyFight.SetParamGrowFactor = function(newv)
	SetTmpParam_At(322, newv)

	CLuaLingShouPropertyFight.RefreshParammMiss()

	CLuaLingShouPropertyFight.RefreshParampHit()

	CLuaLingShouPropertyFight.RefreshParammFatalDamage()

	CLuaLingShouPropertyFight.RefreshParammFatal()

	CLuaLingShouPropertyFight.RefreshParampFatal()

	CLuaLingShouPropertyFight.RefreshParammHit()

	CLuaLingShouPropertyFight.RefreshParampFatalDamage()

	CLuaLingShouPropertyFight.RefreshParampMiss()

	CLuaLingShouPropertyFight.RefreshParamHpFull()

	CLuaLingShouPropertyFight.RefreshParammAttMax()

	CLuaLingShouPropertyFight.RefreshParammAttMin()

	CLuaLingShouPropertyFight.RefreshParammDef()

	CLuaLingShouPropertyFight.RefreshParampDef()

	CLuaLingShouPropertyFight.RefreshParampAttMin()

	CLuaLingShouPropertyFight.RefreshParampAttMax()

	CLuaLingShouPropertyFight.RefreshParamHpRecover()
end
CLuaLingShouPropertyFight.RefreshParamWuxingImprove = function()
	local Xiuwei = GetTmpParam_At(317)
	local Wuxing = GetTmpParam_At(316)

	local newv = (Wuxing/100+max(0,Wuxing-2)/100+max(0,Wuxing-3)/100+max(0,Wuxing-4)/100+max(0,Wuxing-5)/100+max(0,Wuxing-6)/100+max(0,Wuxing-7)/100+max(0,Wuxing-8)/100+max(0,Wuxing-9)/100)+Xiuwei/200
	SetTmpParam_At(323, newv)
end
CLuaLingShouPropertyFight.RefreshParamXiuweiImprove = function()
	local Xiuwei = GetTmpParam_At(317)

	local newv = Xiuwei/200
	SetTmpParam_At(324, newv)
end
CLuaLingShouPropertyFight.RefreshParamEnhanceBeHeal = function()
	local AdjEnhanceBeHeal = GetTmpParam_At(326)

	local newv = AdjEnhanceBeHeal
	SetTmpParam_At(325, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceBeHeal = function(newv)
	SetTmpParam_At(326, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceBeHeal()
end
CLuaLingShouPropertyFight.SetParamMulEnhanceBeHeal = function(newv)
	SetTmpParam_At(327, newv)
end
CLuaLingShouPropertyFight.SetParamLookUpCor = function(newv)
	SetTmpParam_At(328, newv)
end
CLuaLingShouPropertyFight.SetParamLookUpSta = function(newv)
	SetTmpParam_At(329, newv)
end
CLuaLingShouPropertyFight.SetParamLookUpStr = function(newv)
	SetTmpParam_At(330, newv)
end
CLuaLingShouPropertyFight.SetParamLookUpInt = function(newv)
	SetTmpParam_At(331, newv)
end
CLuaLingShouPropertyFight.SetParamLookUpAgi = function(newv)
	SetTmpParam_At(332, newv)
end
CLuaLingShouPropertyFight.SetParamLookUpHpFull = function(newv)
	SetTmpParam_At(333, newv)
end
CLuaLingShouPropertyFight.SetParamLookUpMpFull = function(newv)
	SetTmpParam_At(334, newv)
end
CLuaLingShouPropertyFight.SetParamLookUppAttMin = function(newv)
	SetTmpParam_At(335, newv)
end
CLuaLingShouPropertyFight.SetParamLookUppAttMax = function(newv)
	SetTmpParam_At(336, newv)
end
CLuaLingShouPropertyFight.SetParamLookUppHit = function(newv)
	SetTmpParam_At(337, newv)
end
CLuaLingShouPropertyFight.SetParamLookUppMiss = function(newv)
	SetTmpParam_At(338, newv)
end
CLuaLingShouPropertyFight.SetParamLookUppDef = function(newv)
	SetTmpParam_At(339, newv)
end
CLuaLingShouPropertyFight.SetParamLookUpmAttMin = function(newv)
	SetTmpParam_At(340, newv)
end
CLuaLingShouPropertyFight.SetParamLookUpmAttMax = function(newv)
	SetTmpParam_At(341, newv)
end
CLuaLingShouPropertyFight.SetParamLookUpmHit = function(newv)
	SetTmpParam_At(342, newv)
end
CLuaLingShouPropertyFight.SetParamLookUpmMiss = function(newv)
	SetTmpParam_At(343, newv)
end
CLuaLingShouPropertyFight.SetParamLookUpmDef = function(newv)
	SetTmpParam_At(344, newv)
end
CLuaLingShouPropertyFight.SetParamAdjHpFull2 = function(newv)
	SetTmpParam_At(345, newv)
end
CLuaLingShouPropertyFight.SetParamAdjpAttMin2 = function(newv)
	SetTmpParam_At(346, newv)
end
CLuaLingShouPropertyFight.SetParamAdjpAttMax2 = function(newv)
	SetTmpParam_At(347, newv)
end
CLuaLingShouPropertyFight.SetParamAdjmAttMin2 = function(newv)
	SetTmpParam_At(348, newv)
end
CLuaLingShouPropertyFight.SetParamAdjmAttMax2 = function(newv)
	SetTmpParam_At(349, newv)
end
CLuaLingShouPropertyFight.SetParamLingShouType = function(newv)
	SetTmpParam_At(350, newv)

	CLuaLingShouPropertyFight.RefreshParamMHType()

	CLuaLingShouPropertyFight.RefreshParamPAType()

	CLuaLingShouPropertyFight.RefreshParamMAType()

	CLuaLingShouPropertyFight.RefreshParamPHType()

	CLuaLingShouPropertyFight.RefreshParamMType()

	CLuaLingShouPropertyFight.RefreshParamPType()

	CLuaLingShouPropertyFight.RefreshParamAgi()

	CLuaLingShouPropertyFight.RefreshParamCor()

	CLuaLingShouPropertyFight.RefreshParamInt()

	CLuaLingShouPropertyFight.RefreshParamStr()

	CLuaLingShouPropertyFight.RefreshParammMiss()

	CLuaLingShouPropertyFight.RefreshParampHit()

	CLuaLingShouPropertyFight.RefreshParammFatalDamage()

	CLuaLingShouPropertyFight.RefreshParammFatal()

	CLuaLingShouPropertyFight.RefreshParampFatal()

	CLuaLingShouPropertyFight.RefreshParammHit()

	CLuaLingShouPropertyFight.RefreshParampFatalDamage()

	CLuaLingShouPropertyFight.RefreshParampMiss()

	CLuaLingShouPropertyFight.RefreshParamHpFull()

	CLuaLingShouPropertyFight.RefreshParammAttMax()

	CLuaLingShouPropertyFight.RefreshParammAttMin()

	CLuaLingShouPropertyFight.RefreshParammDef()

	CLuaLingShouPropertyFight.RefreshParampDef()

	CLuaLingShouPropertyFight.RefreshParampAttMin()

	CLuaLingShouPropertyFight.RefreshParampAttMax()

	CLuaLingShouPropertyFight.RefreshParamHpRecover()
end
CLuaLingShouPropertyFight.RefreshParamMHType = function()
	local LingShouType = GetTmpParam_At(350)

	local newv = 1-min(1,abs(LingShouType-4))
	SetTmpParam_At(351, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceLingShou = function(newv)
	SetTmpParam_At(352, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceLingShouMul = function(newv)
	SetTmpParam_At(353, newv)
end
CLuaLingShouPropertyFight.RefreshParamAntiAoe = function()
	local AdjAntiAoe = GetTmpParam_At(355)

	local newv = AdjAntiAoe
	SetTmpParam_At(354, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiAoe = function(newv)
	SetTmpParam_At(355, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiAoe()
end
CLuaLingShouPropertyFight.RefreshParamAntiBlind = function()
	local AdjAntiBlind = GetTmpParam_At(357)
	local MulAntiBlind = GetTmpParam_At(358)

	local newv = AdjAntiBlind*(1+MulAntiBlind)
	SetTmpParam_At(356, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiBlind = function(newv)
	SetTmpParam_At(357, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiBlind()
end
CLuaLingShouPropertyFight.SetParamMulAntiBlind = function(newv)
	SetTmpParam_At(358, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiBlind()
end
CLuaLingShouPropertyFight.RefreshParamAntiDecelerate = function()
	local AdjAntiDecelerate = GetTmpParam_At(360)
	local MulAntiDecelerate = GetTmpParam_At(361)

	local newv = AdjAntiDecelerate*(1+MulAntiDecelerate)
	SetTmpParam_At(359, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiDecelerate = function(newv)
	SetTmpParam_At(360, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiDecelerate()
end
CLuaLingShouPropertyFight.SetParamMulAntiDecelerate = function(newv)
	SetTmpParam_At(361, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiDecelerate()
end
CLuaLingShouPropertyFight.SetParamMinGrade = function(newv)
	SetTmpParam_At(362, newv)
end
CLuaLingShouPropertyFight.SetParamCharacterCor = function(newv)
	SetTmpParam_At(363, newv)
end
CLuaLingShouPropertyFight.SetParamCharacterSta = function(newv)
	SetTmpParam_At(364, newv)
end
CLuaLingShouPropertyFight.SetParamCharacterStr = function(newv)
	SetTmpParam_At(365, newv)
end
CLuaLingShouPropertyFight.SetParamCharacterInt = function(newv)
	SetTmpParam_At(366, newv)
end
CLuaLingShouPropertyFight.SetParamCharacterAgi = function(newv)
	SetTmpParam_At(367, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceDaoKe = function(newv)
	SetTmpParam_At(368, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceDaoKeMul = function(newv)
	SetTmpParam_At(369, newv)
end
CLuaLingShouPropertyFight.SetParamZuoQiSpeed = function(newv)
	SetTmpParam_At(370, newv)
end
CLuaLingShouPropertyFight.RefreshParamEnhanceBianHu = function()
	local AdjEnhanceBianHu = GetTmpParam_At(372)
	local MulEnhanceBianHu = GetTmpParam_At(373)

	local newv = AdjEnhanceBianHu*(1+MulEnhanceBianHu)
	SetTmpParam_At(371, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceBianHu = function(newv)
	SetTmpParam_At(372, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceBianHu()
end
CLuaLingShouPropertyFight.SetParamMulEnhanceBianHu = function(newv)
	SetTmpParam_At(373, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceBianHu()
end
CLuaLingShouPropertyFight.RefreshParamAntiBianHu = function()
	local AdjAntiBianHu = GetTmpParam_At(375)
	local MulAntiBianHu = GetTmpParam_At(376)

	local newv = AdjAntiBianHu*(1+MulAntiBianHu)
	SetTmpParam_At(374, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiBianHu = function(newv)
	SetTmpParam_At(375, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiBianHu()
end
CLuaLingShouPropertyFight.SetParamMulAntiBianHu = function(newv)
	SetTmpParam_At(376, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiBianHu()
end
CLuaLingShouPropertyFight.SetParamEnhanceXiaKe = function(newv)
	SetTmpParam_At(377, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceXiaKeMul = function(newv)
	SetTmpParam_At(378, newv)
end
CLuaLingShouPropertyFight.SetParamTieTimeChange = function(newv)
	SetTmpParam_At(379, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceYanShi = function(newv)
	SetTmpParam_At(380, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceYanShiMul = function(newv)
	SetTmpParam_At(381, newv)
end
CLuaLingShouPropertyFight.RefreshParamPAType = function()
	local LingShouType = GetTmpParam_At(350)

	local newv = 1-min(1,abs(LingShouType-5))
	SetTmpParam_At(382, newv)
end
CLuaLingShouPropertyFight.RefreshParamMAType = function()
	local LingShouType = GetTmpParam_At(350)

	local newv = 1-min(1,abs(LingShouType-6))
	SetTmpParam_At(383, newv)
end
CLuaLingShouPropertyFight.SetParamlifetimeNoReduceRate = function(newv)
	SetTmpParam_At(384, newv)
end
CLuaLingShouPropertyFight.SetParamAddLSHpDurgImprove = function(newv)
	SetTmpParam_At(385, newv)
end
CLuaLingShouPropertyFight.SetParamAddHpDurgImprove = function(newv)
	SetTmpParam_At(386, newv)
end
CLuaLingShouPropertyFight.SetParamAddMpDurgImprove = function(newv)
	SetTmpParam_At(387, newv)
end
CLuaLingShouPropertyFight.SetParamFireHurtReduce = function(newv)
	SetTmpParam_At(388, newv)
end
CLuaLingShouPropertyFight.SetParamThunderHurtReduce = function(newv)
	SetTmpParam_At(389, newv)
end
CLuaLingShouPropertyFight.SetParamIceHurtReduce = function(newv)
	SetTmpParam_At(390, newv)
end
CLuaLingShouPropertyFight.SetParamPoisonHurtReduce = function(newv)
	SetTmpParam_At(391, newv)
end
CLuaLingShouPropertyFight.SetParamWindHurtReduce = function(newv)
	SetTmpParam_At(392, newv)
end
CLuaLingShouPropertyFight.SetParamLightHurtReduce = function(newv)
	SetTmpParam_At(393, newv)
end
CLuaLingShouPropertyFight.SetParamIllusionHurtReduce = function(newv)
	SetTmpParam_At(394, newv)
end
CLuaLingShouPropertyFight.SetParamFakepDef = function(newv)
	SetTmpParam_At(395, newv)
end
CLuaLingShouPropertyFight.SetParamFakemDef = function(newv)
	SetTmpParam_At(396, newv)
end
CLuaLingShouPropertyFight.RefreshParamEnhanceWater = function()
	local AdjEnhanceWater = GetTmpParam_At(398)

	local newv = AdjEnhanceWater
	SetTmpParam_At(397, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceWater = function(newv)
	SetTmpParam_At(398, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceWater()
end
CLuaLingShouPropertyFight.SetParamMulEnhanceWater = function(newv)
	SetTmpParam_At(399, newv)
end
CLuaLingShouPropertyFight.RefreshParamAntiWater = function()
	local AdjAntiWater = GetTmpParam_At(401)
	local MulAntiWater = GetTmpParam_At(402)

	local newv = AdjAntiWater*(1+MulAntiWater)
	SetTmpParam_At(400, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiWater = function(newv)
	SetTmpParam_At(401, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiWater()
end
CLuaLingShouPropertyFight.SetParamMulAntiWater = function(newv)
	SetTmpParam_At(402, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiWater()
end
CLuaLingShouPropertyFight.SetParamWaterHurtReduce = function(newv)
	SetTmpParam_At(403, newv)
end
CLuaLingShouPropertyFight.RefreshParamIgnoreAntiWater = function()
	local AdjIgnoreAntiWater = GetTmpParam_At(431)
	local IgnoreAntiWaterLSMul = GetTmpParam_At(432)

	local newv = AdjIgnoreAntiWater*(1+IgnoreAntiWaterLSMul)
	SetTmpParam_At(404, newv)
end
CLuaLingShouPropertyFight.RefreshParamAntiFreeze = function()
	local AdjAntiFreeze = GetTmpParam_At(406)
	local MulAntiFreeze = GetTmpParam_At(407)

	local newv = AdjAntiFreeze*(1+MulAntiFreeze)
	SetTmpParam_At(405, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiFreeze = function(newv)
	SetTmpParam_At(406, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiFreeze()
end
CLuaLingShouPropertyFight.SetParamMulAntiFreeze = function(newv)
	SetTmpParam_At(407, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiFreeze()
end
CLuaLingShouPropertyFight.RefreshParamEnhanceFreeze = function()
	local AdjEnhanceFreeze = GetTmpParam_At(409)
	local MulEnhanceFreeze = GetTmpParam_At(410)

	local newv = AdjEnhanceFreeze*(1+MulEnhanceFreeze)
	SetTmpParam_At(408, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceFreeze = function(newv)
	SetTmpParam_At(409, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceFreeze()
end
CLuaLingShouPropertyFight.SetParamMulEnhanceFreeze = function(newv)
	SetTmpParam_At(410, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceFreeze()
end
CLuaLingShouPropertyFight.SetParamEnhanceHuaHun = function(newv)
	SetTmpParam_At(411, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceHuaHunMul = function(newv)
	SetTmpParam_At(412, newv)
end
CLuaLingShouPropertyFight.SetParamTrueSightProb = function(newv)
	SetTmpParam_At(413, newv)
end
CLuaLingShouPropertyFight.SetParamLSBBGrade = function(newv)
	SetTmpParam_At(414, newv)

	CLuaLingShouPropertyFight.RefreshParamLSBBAdjHp()

	CLuaLingShouPropertyFight.RefreshParamHpFull()

	CLuaLingShouPropertyFight.RefreshParamHpRecover()
end
CLuaLingShouPropertyFight.SetParamLSBBQuality = function(newv)
	SetTmpParam_At(415, newv)

	CLuaLingShouPropertyFight.RefreshParamLSBBAdjHp()

	CLuaLingShouPropertyFight.RefreshParamHpFull()

	CLuaLingShouPropertyFight.RefreshParamHpRecover()
end
CLuaLingShouPropertyFight.RefreshParamLSBBAdjHp = function()
	local LSBBGrade = GetTmpParam_At(414)
	local LSBBQuality = GetTmpParam_At(415)

	local newv = floor((power(1.032,LSBBGrade-10)+LSBBGrade*0.261-(LSBBGrade-150)*min(1,max(0,LSBBGrade-150)))*6.64*(0.5+min(1,max(0,LSBBQuality-1))*0.5+min(1,max(0,LSBBQuality-2))*0.5+min(1,max(0,LSBBQuality-3))*0.5+min(1,max(0,LSBBQuality-4))*2+min(1,max(0,LSBBQuality-5))*2)*min(1,max(0,LSBBGrade-1)))
	SetTmpParam_At(416, newv)
end
CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiFire = function(newv)
	SetTmpParam_At(417, newv)

	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiFire()
end
CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiThunder = function(newv)
	SetTmpParam_At(418, newv)

	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiThunder()
end
CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiIce = function(newv)
	SetTmpParam_At(419, newv)

	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiIce()
end
CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiPoison = function(newv)
	SetTmpParam_At(420, newv)

	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiPoison()
end
CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiWind = function(newv)
	SetTmpParam_At(421, newv)

	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiWind()
end
CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiLight = function(newv)
	SetTmpParam_At(422, newv)

	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiLight()
end
CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiIllusion = function(newv)
	SetTmpParam_At(423, newv)

	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiIllusion()
end
CLuaLingShouPropertyFight.SetParamIgnoreAntiFireLSMul = function(newv)
	SetTmpParam_At(424, newv)

	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiFire()
end
CLuaLingShouPropertyFight.SetParamIgnoreAntiThunderLSMul = function(newv)
	SetTmpParam_At(425, newv)

	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiThunder()
end
CLuaLingShouPropertyFight.SetParamIgnoreAntiIceLSMul = function(newv)
	SetTmpParam_At(426, newv)

	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiIce()
end
CLuaLingShouPropertyFight.SetParamIgnoreAntiPoisonLSMul = function(newv)
	SetTmpParam_At(427, newv)

	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiPoison()
end
CLuaLingShouPropertyFight.SetParamIgnoreAntiWindLSMul = function(newv)
	SetTmpParam_At(428, newv)

	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiWind()
end
CLuaLingShouPropertyFight.SetParamIgnoreAntiLightLSMul = function(newv)
	SetTmpParam_At(429, newv)

	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiLight()
end
CLuaLingShouPropertyFight.SetParamIgnoreAntiIllusionLSMul = function(newv)
	SetTmpParam_At(430, newv)

	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiIllusion()
end
CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiWater = function(newv)
	SetTmpParam_At(431, newv)

	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiWater()
end
CLuaLingShouPropertyFight.SetParamIgnoreAntiWaterLSMul = function(newv)
	SetTmpParam_At(432, newv)

	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiWater()
end
CLuaLingShouPropertyFight.SetParamMulSpeed2 = function(newv)
	SetTmpParam_At(433, newv)
end
CLuaLingShouPropertyFight.SetParamMulConsumeMp = function(newv)
	SetTmpParam_At(434, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAgi2 = function(newv)
	SetTmpParam_At(435, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiBianHu2 = function(newv)
	SetTmpParam_At(436, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiBind2 = function(newv)
	SetTmpParam_At(437, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiBlind2 = function(newv)
	SetTmpParam_At(438, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiBlock2 = function(newv)
	SetTmpParam_At(439, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiBlockDamage2 = function(newv)
	SetTmpParam_At(440, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiChaos2 = function(newv)
	SetTmpParam_At(441, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiDecelerate2 = function(newv)
	SetTmpParam_At(442, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiDizzy2 = function(newv)
	SetTmpParam_At(443, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiFire2 = function(newv)
	SetTmpParam_At(444, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiFreeze2 = function(newv)
	SetTmpParam_At(445, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiIce2 = function(newv)
	SetTmpParam_At(446, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiIllusion2 = function(newv)
	SetTmpParam_At(447, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiLight2 = function(newv)
	SetTmpParam_At(448, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntimFatal2 = function(newv)
	SetTmpParam_At(449, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntimFatalDamage2 = function(newv)
	SetTmpParam_At(450, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiPetrify2 = function(newv)
	SetTmpParam_At(451, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntipFatal2 = function(newv)
	SetTmpParam_At(452, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntipFatalDamage2 = function(newv)
	SetTmpParam_At(453, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiPoison2 = function(newv)
	SetTmpParam_At(454, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiSilence2 = function(newv)
	SetTmpParam_At(455, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiSleep2 = function(newv)
	SetTmpParam_At(456, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiThunder2 = function(newv)
	SetTmpParam_At(457, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiTie2 = function(newv)
	SetTmpParam_At(458, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiWater2 = function(newv)
	SetTmpParam_At(459, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiWind2 = function(newv)
	SetTmpParam_At(460, newv)
end
CLuaLingShouPropertyFight.SetParamAdjBlock2 = function(newv)
	SetTmpParam_At(461, newv)
end
CLuaLingShouPropertyFight.SetParamAdjBlockDamage2 = function(newv)
	SetTmpParam_At(462, newv)
end
CLuaLingShouPropertyFight.SetParamAdjCor2 = function(newv)
	SetTmpParam_At(463, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceBianHu2 = function(newv)
	SetTmpParam_At(464, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceBind2 = function(newv)
	SetTmpParam_At(465, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceChaos2 = function(newv)
	SetTmpParam_At(466, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceDizzy2 = function(newv)
	SetTmpParam_At(467, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceFire2 = function(newv)
	SetTmpParam_At(468, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceFreeze2 = function(newv)
	SetTmpParam_At(469, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceIce2 = function(newv)
	SetTmpParam_At(470, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceIllusion2 = function(newv)
	SetTmpParam_At(471, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceLight2 = function(newv)
	SetTmpParam_At(472, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhancePetrify2 = function(newv)
	SetTmpParam_At(473, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhancePoison2 = function(newv)
	SetTmpParam_At(474, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceSilence2 = function(newv)
	SetTmpParam_At(475, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceSleep2 = function(newv)
	SetTmpParam_At(476, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceThunder2 = function(newv)
	SetTmpParam_At(477, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceTie2 = function(newv)
	SetTmpParam_At(478, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceWater2 = function(newv)
	SetTmpParam_At(479, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceWind2 = function(newv)
	SetTmpParam_At(480, newv)
end
CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiFire2 = function(newv)
	SetTmpParam_At(481, newv)
end
CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiIce2 = function(newv)
	SetTmpParam_At(482, newv)
end
CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiIllusion2 = function(newv)
	SetTmpParam_At(483, newv)
end
CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiLight2 = function(newv)
	SetTmpParam_At(484, newv)
end
CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiPoison2 = function(newv)
	SetTmpParam_At(485, newv)
end
CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiThunder2 = function(newv)
	SetTmpParam_At(486, newv)
end
CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiWater2 = function(newv)
	SetTmpParam_At(487, newv)
end
CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiWind2 = function(newv)
	SetTmpParam_At(488, newv)
end
CLuaLingShouPropertyFight.SetParamAdjInt2 = function(newv)
	SetTmpParam_At(489, newv)
end
CLuaLingShouPropertyFight.SetParamAdjmDef2 = function(newv)
	SetTmpParam_At(490, newv)
end
CLuaLingShouPropertyFight.SetParamAdjmFatal2 = function(newv)
	SetTmpParam_At(491, newv)
end
CLuaLingShouPropertyFight.SetParamAdjmFatalDamage2 = function(newv)
	SetTmpParam_At(492, newv)
end
CLuaLingShouPropertyFight.SetParamAdjmHit2 = function(newv)
	SetTmpParam_At(493, newv)
end
CLuaLingShouPropertyFight.SetParamAdjmHurt2 = function(newv)
	SetTmpParam_At(494, newv)
end
CLuaLingShouPropertyFight.SetParamAdjmMiss2 = function(newv)
	SetTmpParam_At(495, newv)
end
CLuaLingShouPropertyFight.SetParamAdjmSpeed2 = function(newv)
	SetTmpParam_At(496, newv)
end
CLuaLingShouPropertyFight.SetParamAdjpDef2 = function(newv)
	SetTmpParam_At(497, newv)
end
CLuaLingShouPropertyFight.SetParamAdjpFatal2 = function(newv)
	SetTmpParam_At(498, newv)
end
CLuaLingShouPropertyFight.SetParamAdjpFatalDamage2 = function(newv)
	SetTmpParam_At(499, newv)
end
CLuaLingShouPropertyFight.SetParamAdjpHit2 = function(newv)
	SetTmpParam_At(500, newv)
end
CLuaLingShouPropertyFight.SetParamAdjpHurt2 = function(newv)
	SetTmpParam_At(501, newv)
end
CLuaLingShouPropertyFight.SetParamAdjpMiss2 = function(newv)
	SetTmpParam_At(502, newv)
end
CLuaLingShouPropertyFight.SetParamAdjpSpeed2 = function(newv)
	SetTmpParam_At(503, newv)
end
CLuaLingShouPropertyFight.SetParamAdjStr2 = function(newv)
	SetTmpParam_At(504, newv)
end
CLuaLingShouPropertyFight.SetParamLSpAttMin = function(newv)
	SetTmpParam_At(505, newv)
end
CLuaLingShouPropertyFight.SetParamLSpAttMax = function(newv)
	SetTmpParam_At(506, newv)
end
CLuaLingShouPropertyFight.SetParamLSmAttMin = function(newv)
	SetTmpParam_At(507, newv)
end
CLuaLingShouPropertyFight.SetParamLSmAttMax = function(newv)
	SetTmpParam_At(508, newv)
end
CLuaLingShouPropertyFight.SetParamLSpFatal = function(newv)
	SetTmpParam_At(509, newv)
end
CLuaLingShouPropertyFight.SetParamLSpFatalDamage = function(newv)
	SetTmpParam_At(510, newv)
end
CLuaLingShouPropertyFight.SetParamLSmFatal = function(newv)
	SetTmpParam_At(511, newv)
end
CLuaLingShouPropertyFight.SetParamLSmFatalDamage = function(newv)
	SetTmpParam_At(512, newv)
end
CLuaLingShouPropertyFight.SetParamLSAntiBlock = function(newv)
	SetTmpParam_At(513, newv)
end
CLuaLingShouPropertyFight.SetParamLSAntiBlockDamage = function(newv)
	SetTmpParam_At(514, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiFire = function(newv)
	SetTmpParam_At(515, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiThunder = function(newv)
	SetTmpParam_At(516, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiIce = function(newv)
	SetTmpParam_At(517, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiPoison = function(newv)
	SetTmpParam_At(518, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiWind = function(newv)
	SetTmpParam_At(519, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiLight = function(newv)
	SetTmpParam_At(520, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiIllusion = function(newv)
	SetTmpParam_At(521, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiWater = function(newv)
	SetTmpParam_At(522, newv)
end
CLuaLingShouPropertyFight.SetParamLSpAttMin_adj = function(newv)
	SetTmpParam_At(523, newv)
end
CLuaLingShouPropertyFight.SetParamLSpAttMax_adj = function(newv)
	SetTmpParam_At(524, newv)
end
CLuaLingShouPropertyFight.SetParamLSmAttMin_adj = function(newv)
	SetTmpParam_At(525, newv)
end
CLuaLingShouPropertyFight.SetParamLSmAttMax_adj = function(newv)
	SetTmpParam_At(526, newv)
end
CLuaLingShouPropertyFight.SetParamLSpFatal_adj = function(newv)
	SetTmpParam_At(527, newv)
end
CLuaLingShouPropertyFight.SetParamLSpFatalDamage_adj = function(newv)
	SetTmpParam_At(528, newv)
end
CLuaLingShouPropertyFight.SetParamLSmFatal_adj = function(newv)
	SetTmpParam_At(529, newv)
end
CLuaLingShouPropertyFight.SetParamLSmFatalDamage_adj = function(newv)
	SetTmpParam_At(530, newv)
end
CLuaLingShouPropertyFight.SetParamLSAntiBlock_adj = function(newv)
	SetTmpParam_At(531, newv)
end
CLuaLingShouPropertyFight.SetParamLSAntiBlockDamage_adj = function(newv)
	SetTmpParam_At(532, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiFire_adj = function(newv)
	SetTmpParam_At(533, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiThunder_adj = function(newv)
	SetTmpParam_At(534, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiIce_adj = function(newv)
	SetTmpParam_At(535, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiPoison_adj = function(newv)
	SetTmpParam_At(536, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiWind_adj = function(newv)
	SetTmpParam_At(537, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiLight_adj = function(newv)
	SetTmpParam_At(538, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiIllusion_adj = function(newv)
	SetTmpParam_At(539, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiWater_adj = function(newv)
	SetTmpParam_At(540, newv)
end
CLuaLingShouPropertyFight.SetParamLSpAttMin_jc = function(newv)
	SetTmpParam_At(541, newv)
end
CLuaLingShouPropertyFight.SetParamLSpAttMax_jc = function(newv)
	SetTmpParam_At(542, newv)
end
CLuaLingShouPropertyFight.SetParamLSmAttMin_jc = function(newv)
	SetTmpParam_At(543, newv)
end
CLuaLingShouPropertyFight.SetParamLSmAttMax_jc = function(newv)
	SetTmpParam_At(544, newv)
end
CLuaLingShouPropertyFight.SetParamLSpFatal_jc = function(newv)
	SetTmpParam_At(545, newv)
end
CLuaLingShouPropertyFight.SetParamLSpFatalDamage_jc = function(newv)
	SetTmpParam_At(546, newv)
end
CLuaLingShouPropertyFight.SetParamLSmFatal_jc = function(newv)
	SetTmpParam_At(547, newv)
end
CLuaLingShouPropertyFight.SetParamLSmFatalDamage_jc = function(newv)
	SetTmpParam_At(548, newv)
end
CLuaLingShouPropertyFight.SetParamLSAntiBlock_jc = function(newv)
	SetTmpParam_At(549, newv)
end
CLuaLingShouPropertyFight.SetParamLSAntiBlockDamage_jc = function(newv)
	SetTmpParam_At(550, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiFire_jc = function(newv)
	SetTmpParam_At(551, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiThunder_jc = function(newv)
	SetTmpParam_At(552, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiIce_jc = function(newv)
	SetTmpParam_At(553, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiPoison_jc = function(newv)
	SetTmpParam_At(554, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiWind_jc = function(newv)
	SetTmpParam_At(555, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiLight_jc = function(newv)
	SetTmpParam_At(556, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiIllusion_jc = function(newv)
	SetTmpParam_At(557, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiWater_jc = function(newv)
	SetTmpParam_At(558, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiFire_mul = function(newv)
	SetTmpParam_At(559, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiThunder_mul = function(newv)
	SetTmpParam_At(560, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiIce_mul = function(newv)
	SetTmpParam_At(561, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiPoison_mul = function(newv)
	SetTmpParam_At(562, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiWind_mul = function(newv)
	SetTmpParam_At(563, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiLight_mul = function(newv)
	SetTmpParam_At(564, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiIllusion_mul = function(newv)
	SetTmpParam_At(565, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiWater_mul = function(newv)
	SetTmpParam_At(566, newv)
end
CLuaLingShouPropertyFight.SetParamJieBan_Child_QiChangColor = function(newv)
	SetTmpParam_At(567, newv)
end
CLuaLingShouPropertyFight.SetParamJieBan_LingShou_Ratio = function(newv)
	SetTmpParam_At(568, newv)
end
CLuaLingShouPropertyFight.SetParamJieBan_LingShou_QinMi = function(newv)
	SetTmpParam_At(569, newv)
end
CLuaLingShouPropertyFight.SetParamJieBan_LingShou_CorZizhi = function(newv)
	SetTmpParam_At(570, newv)
end
CLuaLingShouPropertyFight.SetParamJieBan_LingShou_StaZizhi = function(newv)
	SetTmpParam_At(571, newv)
end
CLuaLingShouPropertyFight.SetParamJieBan_LingShou_StrZizhi = function(newv)
	SetTmpParam_At(572, newv)
end
CLuaLingShouPropertyFight.SetParamJieBan_LingShou_IntZizhi = function(newv)
	SetTmpParam_At(573, newv)
end
CLuaLingShouPropertyFight.SetParamJieBan_LingShou_AgiZizhi = function(newv)
	SetTmpParam_At(574, newv)
end
CLuaLingShouPropertyFight.SetParamBuff_DisplayValue1 = function(newv)
	SetTmpParam_At(575, newv)
end
CLuaLingShouPropertyFight.SetParamBuff_DisplayValue2 = function(newv)
	SetTmpParam_At(576, newv)
end
CLuaLingShouPropertyFight.SetParamBuff_DisplayValue3 = function(newv)
	SetTmpParam_At(577, newv)
end
CLuaLingShouPropertyFight.SetParamBuff_DisplayValue4 = function(newv)
	SetTmpParam_At(578, newv)
end
CLuaLingShouPropertyFight.SetParamBuff_DisplayValue5 = function(newv)
	SetTmpParam_At(579, newv)
end
CLuaLingShouPropertyFight.SetParamLSNature = function(newv)
	SetTmpParam_At(580, newv)
end
CLuaLingShouPropertyFight.SetParamPrayMulti = function(newv)
	SetTmpParam_At(581, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceYingLing = function(newv)
	SetTmpParam_At(582, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceYingLingMul = function(newv)
	SetTmpParam_At(583, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceFlag = function(newv)
	SetTmpParam_At(584, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceFlagMul = function(newv)
	SetTmpParam_At(585, newv)
end
CLuaLingShouPropertyFight.SetParamObjectType = function(newv)
	SetTmpParam_At(586, newv)
end
CLuaLingShouPropertyFight.SetParamMonsterType = function(newv)
	SetTmpParam_At(587, newv)
end
CLuaLingShouPropertyFight.SetParamFightPropType = function(newv)
	SetTmpParam_At(588, newv)
end
CLuaLingShouPropertyFight.SetParamPlayHpFull = function(newv)
	SetTmpParam_At(589, newv)
end
CLuaLingShouPropertyFight.SetParamPlayHp = function(newv)
	SetTmpParam_At(590, newv)
end
CLuaLingShouPropertyFight.SetParamPlayAtt = function(newv)
	SetTmpParam_At(591, newv)
end
CLuaLingShouPropertyFight.SetParamPlayDef = function(newv)
	SetTmpParam_At(592, newv)
end
CLuaLingShouPropertyFight.SetParamPlayHit = function(newv)
	SetTmpParam_At(593, newv)
end
CLuaLingShouPropertyFight.SetParamPlayMiss = function(newv)
	SetTmpParam_At(594, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceDieKe = function(newv)
	SetTmpParam_At(595, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceDieKeMul = function(newv)
	SetTmpParam_At(596, newv)
end
CLuaLingShouPropertyFight.SetParamDotRemain = function(newv)
	SetTmpParam_At(597, newv)
end
CLuaLingShouPropertyFight.SetParamIsConfused = function(newv)
	SetTmpParam_At(598, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceSheShouKefu = function(newv)
	SetTmpParam_At(599, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceJiaShiKefu = function(newv)
	SetTmpParam_At(600, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceDaoKeKefu = function(newv)
	SetTmpParam_At(601, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceXiaKeKefu = function(newv)
	SetTmpParam_At(602, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceFangShiKefu = function(newv)
	SetTmpParam_At(603, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceYiShiKefu = function(newv)
	SetTmpParam_At(604, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceMeiZheKefu = function(newv)
	SetTmpParam_At(605, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceYiRenKefu = function(newv)
	SetTmpParam_At(606, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceYanShiKefu = function(newv)
	SetTmpParam_At(607, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceHuaHunKefu = function(newv)
	SetTmpParam_At(608, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceYingLingKefu = function(newv)
	SetTmpParam_At(609, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceDieKeKefu = function(newv)
	SetTmpParam_At(610, newv)
end
CLuaLingShouPropertyFight.SetParamAdjustPermanentCor = function(newv)
	SetTmpParam_At(611, newv)
end
CLuaLingShouPropertyFight.SetParamAdjustPermanentSta = function(newv)
	SetTmpParam_At(612, newv)
end
CLuaLingShouPropertyFight.SetParamAdjustPermanentStr = function(newv)
	SetTmpParam_At(613, newv)
end
CLuaLingShouPropertyFight.SetParamAdjustPermanentInt = function(newv)
	SetTmpParam_At(614, newv)
end
CLuaLingShouPropertyFight.SetParamAdjustPermanentAgi = function(newv)
	SetTmpParam_At(615, newv)
end
CLuaLingShouPropertyFight.SetParamSoulCoreLevel = function(newv)
	SetTmpParam_At(616, newv)
end
CLuaLingShouPropertyFight.SetParamAdjustPermanentMidCor = function(newv)
	SetTmpParam_At(617, newv)
end
CLuaLingShouPropertyFight.SetParamAdjustPermanentMidSta = function(newv)
	SetTmpParam_At(618, newv)
end
CLuaLingShouPropertyFight.SetParamAdjustPermanentMidStr = function(newv)
	SetTmpParam_At(619, newv)
end
CLuaLingShouPropertyFight.SetParamAdjustPermanentMidInt = function(newv)
	SetTmpParam_At(620, newv)
end
CLuaLingShouPropertyFight.SetParamAdjustPermanentMidAgi = function(newv)
	SetTmpParam_At(621, newv)
end
CLuaLingShouPropertyFight.SetParamSumPermanent = function(newv)
	SetTmpParam_At(622, newv)
end
CLuaLingShouPropertyFight.SetParamAntiTian = function(newv)
	SetTmpParam_At(623, newv)
end
CLuaLingShouPropertyFight.SetParamAntiEGui = function(newv)
	SetTmpParam_At(624, newv)
end
CLuaLingShouPropertyFight.SetParamAntiXiuLuo = function(newv)
	SetTmpParam_At(625, newv)
end
CLuaLingShouPropertyFight.SetParamAntiDiYu = function(newv)
	SetTmpParam_At(626, newv)
end
CLuaLingShouPropertyFight.SetParamAntiRen = function(newv)
	SetTmpParam_At(627, newv)
end
CLuaLingShouPropertyFight.SetParamAntiChuSheng = function(newv)
	SetTmpParam_At(628, newv)
end
CLuaLingShouPropertyFight.SetParamAntiBuilding = function(newv)
	SetTmpParam_At(629, newv)
end
CLuaLingShouPropertyFight.SetParamAntiZhaoHuan = function(newv)
	SetTmpParam_At(630, newv)
end
CLuaLingShouPropertyFight.SetParamAntiLingShou = function(newv)
	SetTmpParam_At(631, newv)
end
CLuaLingShouPropertyFight.SetParamAntiBoss = function(newv)
	SetTmpParam_At(632, newv)
end
CLuaLingShouPropertyFight.SetParamAntiSheShou = function(newv)
	SetTmpParam_At(633, newv)
end
CLuaLingShouPropertyFight.SetParamAntiJiaShi = function(newv)
	SetTmpParam_At(634, newv)
end
CLuaLingShouPropertyFight.SetParamAntiDaoKe = function(newv)
	SetTmpParam_At(635, newv)
end
CLuaLingShouPropertyFight.SetParamAntiXiaKe = function(newv)
	SetTmpParam_At(636, newv)
end
CLuaLingShouPropertyFight.SetParamAntiFangShi = function(newv)
	SetTmpParam_At(637, newv)
end
CLuaLingShouPropertyFight.SetParamAntiYiShi = function(newv)
	SetTmpParam_At(638, newv)
end
CLuaLingShouPropertyFight.SetParamAntiMeiZhe = function(newv)
	SetTmpParam_At(639, newv)
end
CLuaLingShouPropertyFight.SetParamAntiYiRen = function(newv)
	SetTmpParam_At(640, newv)
end
CLuaLingShouPropertyFight.SetParamAntiYanShi = function(newv)
	SetTmpParam_At(641, newv)
end
CLuaLingShouPropertyFight.SetParamAntiHuaHun = function(newv)
	SetTmpParam_At(642, newv)
end
CLuaLingShouPropertyFight.SetParamAntiYingLing = function(newv)
	SetTmpParam_At(643, newv)
end
CLuaLingShouPropertyFight.SetParamAntiDieKe = function(newv)
	SetTmpParam_At(644, newv)
end
CLuaLingShouPropertyFight.SetParamAntiFlag = function(newv)
	SetTmpParam_At(645, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceZhanKuang = function(newv)
	SetTmpParam_At(646, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceZhanKuangMul = function(newv)
	SetTmpParam_At(647, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceZhanKuangKefu = function(newv)
	SetTmpParam_At(648, newv)
end
CLuaLingShouPropertyFight.SetParamAntiZhanKuang = function(newv)
	SetTmpParam_At(649, newv)
end
CLuaLingShouPropertyFight.SetParamJumpSpeed = function(newv)
	SetTmpParam_At(650, newv)
end
CLuaLingShouPropertyFight.SetParamOriJumpSpeed = function(newv)
	SetTmpParam_At(651, newv)
end
CLuaLingShouPropertyFight.SetParamAdjJumpSpeed = function(newv)
	SetTmpParam_At(652, newv)
end
CLuaLingShouPropertyFight.SetParamGravityAcceleration = function(newv)
	SetTmpParam_At(653, newv)
end
CLuaLingShouPropertyFight.SetParamOriGravityAcceleration = function(newv)
	SetTmpParam_At(654, newv)
end
CLuaLingShouPropertyFight.SetParamAdjGravityAcceleration = function(newv)
	SetTmpParam_At(655, newv)
end
CLuaLingShouPropertyFight.SetParamHorizontalAcceleration = function(newv)
	SetTmpParam_At(656, newv)
end
CLuaLingShouPropertyFight.SetParamOriHorizontalAcceleration = function(newv)
	SetTmpParam_At(657, newv)
end
CLuaLingShouPropertyFight.SetParamAdjHorizontalAcceleration = function(newv)
	SetTmpParam_At(658, newv)
end
CLuaLingShouPropertyFight.SetParamSwimSpeed = function(newv)
	SetTmpParam_At(659, newv)
end
CLuaLingShouPropertyFight.SetParamOriSwimSpeed = function(newv)
	SetTmpParam_At(660, newv)
end
CLuaLingShouPropertyFight.SetParamAdjSwimSpeed = function(newv)
	SetTmpParam_At(661, newv)
end
CLuaLingShouPropertyFight.SetParamStrengthPoint = function(newv)
	SetTmpParam_At(662, newv)
end
CLuaLingShouPropertyFight.SetParamStrengthPointMax = function(newv)
	SetTmpParam_At(663, newv)
end
CLuaLingShouPropertyFight.SetParamOriStrengthPointMax = function(newv)
	SetTmpParam_At(664, newv)
end
CLuaLingShouPropertyFight.SetParamAdjStrengthPointMax = function(newv)
	SetTmpParam_At(665, newv)
end
CLuaLingShouPropertyFight.SetParamAntiMulEnhanceIllusion = function(newv)
	SetTmpParam_At(666, newv)
end
CLuaLingShouPropertyFight.SetParamGlideHorizontalSpeedWithUmbrella = function(newv)
	SetTmpParam_At(667, newv)
end
CLuaLingShouPropertyFight.SetParamGlideVerticalSpeedWithUmbrella = function(newv)
	SetTmpParam_At(668, newv)
end
CLuaLingShouPropertyFight.SetParamAdjpSpeed = function(newv)
	SetTmpParam_At(1001, newv)

	CLuaLingShouPropertyFight.RefreshParampSpeed()
end
CLuaLingShouPropertyFight.SetParamAdjmSpeed = function(newv)
	SetTmpParam_At(1002, newv)

	CLuaLingShouPropertyFight.RefreshParammSpeed()
end
CLuaLingShouPropertyFight.SetParamPermanentCor = function(newv)
	SetParam_At(1, newv)
end
CLuaLingShouPropertyFight.SetParamPermanentSta = function(newv)
	SetParam_At(2, newv)
end
CLuaLingShouPropertyFight.SetParamPermanentStr = function(newv)
	SetParam_At(3, newv)
end
CLuaLingShouPropertyFight.SetParamPermanentInt = function(newv)
	SetParam_At(4, newv)
end
CLuaLingShouPropertyFight.SetParamPermanentAgi = function(newv)
	SetParam_At(5, newv)
end
CLuaLingShouPropertyFight.SetParamPermanentHpFull = function(newv)
	SetParam_At(6, newv)
end
CLuaLingShouPropertyFight.SetParamHp = function(newv)
	SetParam_At(7, newv)
end
CLuaLingShouPropertyFight.SetParamMp = function(newv)
	SetParam_At(8, newv)
end
CLuaLingShouPropertyFight.SetParamCurrentHpFull = function(newv)
	SetParam_At(9, newv)
end
CLuaLingShouPropertyFight.SetParamCurrentMpFull = function(newv)
	SetParam_At(10, newv)
end
CLuaLingShouPropertyFight.SetParamEquipExchangeMF = function(newv)
	SetParam_At(11, newv)
end
CLuaLingShouPropertyFight.SetParamRevisePermanentCor = function(newv)
	SetParam_At(12, newv)
end
CLuaLingShouPropertyFight.SetParamRevisePermanentSta = function(newv)
	SetParam_At(13, newv)
end
CLuaLingShouPropertyFight.SetParamRevisePermanentStr = function(newv)
	SetParam_At(14, newv)
end
CLuaLingShouPropertyFight.SetParamRevisePermanentInt = function(newv)
	SetParam_At(15, newv)
end
CLuaLingShouPropertyFight.SetParamRevisePermanentAgi = function(newv)
	SetParam_At(16, newv)
end
CLuaLingShouPropertyFight.SetParamClass = function(newv)
	SetTmpParam_At(1, newv)
end
CLuaLingShouPropertyFight.SetParamGrade = function(newv)
	SetTmpParam_At(2, newv)

	CLuaLingShouPropertyFight.RefreshParamSta()

	CLuaLingShouPropertyFight.RefreshParamAgi()

	CLuaLingShouPropertyFight.RefreshParamCor()

	CLuaLingShouPropertyFight.RefreshParamInt()

	CLuaLingShouPropertyFight.RefreshParamStr()

	CLuaLingShouPropertyFight.RefreshParammMiss()

	CLuaLingShouPropertyFight.RefreshParampHit()

	CLuaLingShouPropertyFight.RefreshParammFatalDamage()

	CLuaLingShouPropertyFight.RefreshParammFatal()

	CLuaLingShouPropertyFight.RefreshParampFatal()

	CLuaLingShouPropertyFight.RefreshParammHit()

	CLuaLingShouPropertyFight.RefreshParampFatalDamage()

	CLuaLingShouPropertyFight.RefreshParampMiss()

	CLuaLingShouPropertyFight.RefreshParamHpFull()

	CLuaLingShouPropertyFight.RefreshParammAttMax()

	CLuaLingShouPropertyFight.RefreshParammAttMin()

	CLuaLingShouPropertyFight.RefreshParammDef()

	CLuaLingShouPropertyFight.RefreshParampDef()

	CLuaLingShouPropertyFight.RefreshParampAttMin()

	CLuaLingShouPropertyFight.RefreshParampAttMax()

	CLuaLingShouPropertyFight.RefreshParamHpRecover()
end
CLuaLingShouPropertyFight.RefreshParamCor = function()
	local AdjCor = GetTmpParam_At(4)
	local MulCor = GetTmpParam_At(5)
	local Xiuwei = GetTmpParam_At(317)
	local Grade = GetTmpParam_At(2)
	local PHType = GetTmpParam_At(321)
	local MHType = GetTmpParam_At(351)

	local newv = ((1+3*(PHType+MHType))*(Grade+(Grade-50)*min(1,max(0,Grade-50))+(Grade-100)*min(1,max(0,Grade-100))-3*max(0,Grade-150))+Xiuwei*2+AdjCor)*(1+MulCor)
	SetTmpParam_At(3, newv)
end
CLuaLingShouPropertyFight.SetParamAdjCor = function(newv)
	SetTmpParam_At(4, newv)

	CLuaLingShouPropertyFight.RefreshParamCor()

	CLuaLingShouPropertyFight.RefreshParamHpFull()

	CLuaLingShouPropertyFight.RefreshParamHpRecover()
end
CLuaLingShouPropertyFight.SetParamMulCor = function(newv)
	SetTmpParam_At(5, newv)

	CLuaLingShouPropertyFight.RefreshParamCor()

	CLuaLingShouPropertyFight.RefreshParamHpFull()

	CLuaLingShouPropertyFight.RefreshParamHpRecover()
end
CLuaLingShouPropertyFight.RefreshParamSta = function()
	local AdjSta = GetTmpParam_At(7)
	local MulSta = GetTmpParam_At(8)
	local Xiuwei = GetTmpParam_At(317)
	local Grade = GetTmpParam_At(2)

	local newv = (Grade+(Grade-50)*min(1,max(0,Grade-50))+(Grade-100)*min(1,max(0,Grade-100))-3*max(0,Grade-150)+Xiuwei*2+AdjSta)*(1+MulSta)
	SetTmpParam_At(6, newv)
end
CLuaLingShouPropertyFight.SetParamAdjSta = function(newv)
	SetTmpParam_At(7, newv)

	CLuaLingShouPropertyFight.RefreshParamSta()

	CLuaLingShouPropertyFight.RefreshParammDef()

	CLuaLingShouPropertyFight.RefreshParampDef()
end
CLuaLingShouPropertyFight.SetParamMulSta = function(newv)
	SetTmpParam_At(8, newv)

	CLuaLingShouPropertyFight.RefreshParamSta()

	CLuaLingShouPropertyFight.RefreshParammDef()

	CLuaLingShouPropertyFight.RefreshParampDef()
end
CLuaLingShouPropertyFight.RefreshParamStr = function()
	local AdjStr = GetTmpParam_At(10)
	local MulStr = GetTmpParam_At(11)
	local Xiuwei = GetTmpParam_At(317)
	local PAType = GetTmpParam_At(382)
	local PType = GetTmpParam_At(319)
	local Grade = GetTmpParam_At(2)
	local PHType = GetTmpParam_At(321)

	local newv = ((1+2*PHType+5*PType+3*PAType)*(Grade+(Grade-50)*min(1,max(0,Grade-50))+(Grade-100)*min(1,max(0,Grade-100))-3*max(0,Grade-150))+Xiuwei*2+AdjStr)*(1+MulStr)
	SetTmpParam_At(9, newv)
end
CLuaLingShouPropertyFight.SetParamAdjStr = function(newv)
	SetTmpParam_At(10, newv)

	CLuaLingShouPropertyFight.RefreshParamStr()

	CLuaLingShouPropertyFight.RefreshParampDef()

	CLuaLingShouPropertyFight.RefreshParampAttMin()

	CLuaLingShouPropertyFight.RefreshParampAttMax()
end
CLuaLingShouPropertyFight.SetParamMulStr = function(newv)
	SetTmpParam_At(11, newv)

	CLuaLingShouPropertyFight.RefreshParamStr()

	CLuaLingShouPropertyFight.RefreshParampDef()

	CLuaLingShouPropertyFight.RefreshParampAttMin()

	CLuaLingShouPropertyFight.RefreshParampAttMax()
end
CLuaLingShouPropertyFight.RefreshParamInt = function()
	local AdjInt = GetTmpParam_At(13)
	local MulInt = GetTmpParam_At(14)
	local Xiuwei = GetTmpParam_At(317)
	local MAType = GetTmpParam_At(383)
	local MType = GetTmpParam_At(320)
	local Grade = GetTmpParam_At(2)
	local MHType = GetTmpParam_At(351)

	local newv = ((1+2*MHType+5*MType+3*MAType)*(Grade+(Grade-50)*min(1,max(0,Grade-50))+(Grade-100)*min(1,max(0,Grade-100))-3*max(0,Grade-150))+Xiuwei*2+AdjInt)*(1+MulInt)
	SetTmpParam_At(12, newv)
end
CLuaLingShouPropertyFight.SetParamAdjInt = function(newv)
	SetTmpParam_At(13, newv)

	CLuaLingShouPropertyFight.RefreshParamInt()

	CLuaLingShouPropertyFight.RefreshParammAttMax()

	CLuaLingShouPropertyFight.RefreshParammAttMin()

	CLuaLingShouPropertyFight.RefreshParammDef()
end
CLuaLingShouPropertyFight.SetParamMulInt = function(newv)
	SetTmpParam_At(14, newv)

	CLuaLingShouPropertyFight.RefreshParamInt()

	CLuaLingShouPropertyFight.RefreshParammAttMax()

	CLuaLingShouPropertyFight.RefreshParammAttMin()

	CLuaLingShouPropertyFight.RefreshParammDef()
end
CLuaLingShouPropertyFight.RefreshParamAgi = function()
	local AdjAgi = GetTmpParam_At(16)
	local MulAgi = GetTmpParam_At(17)
	local Xiuwei = GetTmpParam_At(317)
	local MAType = GetTmpParam_At(383)
	local PAType = GetTmpParam_At(382)
	local Grade = GetTmpParam_At(2)

	local newv = ((1+2*PAType+2*MAType)*(Grade+(Grade-50)*min(1,max(0,Grade-50))+(Grade-100)*min(1,max(0,Grade-100))-3*max(0,Grade-150))+Xiuwei*2+AdjAgi)*(1+MulAgi)
	SetTmpParam_At(15, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAgi = function(newv)
	SetTmpParam_At(16, newv)

	CLuaLingShouPropertyFight.RefreshParamAgi()

	CLuaLingShouPropertyFight.RefreshParammMiss()

	CLuaLingShouPropertyFight.RefreshParampHit()

	CLuaLingShouPropertyFight.RefreshParammFatalDamage()

	CLuaLingShouPropertyFight.RefreshParammFatal()

	CLuaLingShouPropertyFight.RefreshParampFatal()

	CLuaLingShouPropertyFight.RefreshParammHit()

	CLuaLingShouPropertyFight.RefreshParampFatalDamage()

	CLuaLingShouPropertyFight.RefreshParampMiss()
end
CLuaLingShouPropertyFight.SetParamMulAgi = function(newv)
	SetTmpParam_At(17, newv)

	CLuaLingShouPropertyFight.RefreshParamAgi()

	CLuaLingShouPropertyFight.RefreshParammMiss()

	CLuaLingShouPropertyFight.RefreshParampHit()

	CLuaLingShouPropertyFight.RefreshParammFatalDamage()

	CLuaLingShouPropertyFight.RefreshParammFatal()

	CLuaLingShouPropertyFight.RefreshParampFatal()

	CLuaLingShouPropertyFight.RefreshParammHit()

	CLuaLingShouPropertyFight.RefreshParampFatalDamage()

	CLuaLingShouPropertyFight.RefreshParampMiss()
end
CLuaLingShouPropertyFight.RefreshParamHpFull = function()
	local LSBBAdjHp = GetTmpParam_At(416)
	local AdjHpFull = GetTmpParam_At(19)
	local MulHpFull = GetTmpParam_At(20)
	local GrowFactor = GetTmpParam_At(322)
	local Cor = GetTmpParam_At(3)
	local CorZizhi = GetTmpParam_At(307)
	local Grade = GetTmpParam_At(2)
	local MHType = GetTmpParam_At(351)
	local PHType = GetTmpParam_At(321)
	local MAType = GetTmpParam_At(383)
	local MType = GetTmpParam_At(320)
	local PType = GetTmpParam_At(319)
	local PAType = GetTmpParam_At(382)
	local SkillNum = GetTmpParam_At(318)

	local newv = max(1,floor((24*Grade*GrowFactor*(1+(PHType)+(MHType))-12*(PType+PAType+MType+MAType)*max(0,Grade-150)*GrowFactor+6.8*CorZizhi*Cor/1000+AdjHpFull)*(1+min(1,max(0,(SkillNum-3)/4))*0.1+MulHpFull)))+LSBBAdjHp
	SetTmpParam_At(18, newv)
end
CLuaLingShouPropertyFight.SetParamAdjHpFull = function(newv)
	SetTmpParam_At(19, newv)

	CLuaLingShouPropertyFight.RefreshParamHpFull()

	CLuaLingShouPropertyFight.RefreshParamHpRecover()
end
CLuaLingShouPropertyFight.SetParamMulHpFull = function(newv)
	SetTmpParam_At(20, newv)

	CLuaLingShouPropertyFight.RefreshParamHpFull()

	CLuaLingShouPropertyFight.RefreshParamHpRecover()
end
CLuaLingShouPropertyFight.RefreshParamHpRecover = function()
	local AdjHpRecover = GetTmpParam_At(22)
	local MulHpRecover = GetTmpParam_At(23)
	local HpFull = GetTmpParam_At(18)

	local newv = (AdjHpRecover+HpFull/200)*(1+ MulHpRecover)
	SetTmpParam_At(21, newv)
end
CLuaLingShouPropertyFight.SetParamAdjHpRecover = function(newv)
	SetTmpParam_At(22, newv)

	CLuaLingShouPropertyFight.RefreshParamHpRecover()
end
CLuaLingShouPropertyFight.SetParamMulHpRecover = function(newv)
	SetTmpParam_At(23, newv)

	CLuaLingShouPropertyFight.RefreshParamHpRecover()
end
CLuaLingShouPropertyFight.SetParamMpFull = function(newv)
	SetTmpParam_At(24, newv)
end
CLuaLingShouPropertyFight.SetParamAdjMpFull = function(newv)
	SetTmpParam_At(25, newv)
end
CLuaLingShouPropertyFight.SetParamMulMpFull = function(newv)
	SetTmpParam_At(26, newv)
end
CLuaLingShouPropertyFight.SetParamMpRecover = function(newv)
	SetTmpParam_At(27, newv)
end
CLuaLingShouPropertyFight.SetParamAdjMpRecover = function(newv)
	SetTmpParam_At(28, newv)
end
CLuaLingShouPropertyFight.SetParamMulMpRecover = function(newv)
	SetTmpParam_At(29, newv)
end
CLuaLingShouPropertyFight.RefreshParampAttMin = function()
	local AdjpAttMin = GetTmpParam_At(31)
	local MulpAttMin = GetTmpParam_At(32)
	local AdjpAttMax = GetTmpParam_At(34)
	local MulpAttMax = GetTmpParam_At(35)
	local GrowFactor = GetTmpParam_At(322)
	local Str = GetTmpParam_At(9)
	local StrZizhi = GetTmpParam_At(306)
	local SkillNum = GetTmpParam_At(318)
	local PType = GetTmpParam_At(319)
	local PAType = GetTmpParam_At(382)
	local Grade = GetTmpParam_At(2)

	local newv = min(max(1,((GrowFactor*(3.5*Grade+3.5*Grade*(PType+PAType)+5*max(0,Grade-50)+15*max(0,Grade-50)*(PType+PAType)+5*max(0,Grade-100)+15*max(0,Grade-100)*(PType+PAType)-(8+28*(PType+PAType))*(Grade-150)*min(1,max(0,Grade-150)))+1.8*StrZizhi*Str/1000+50)*0.8+AdjpAttMin)*(1+min(1,max(0,(SkillNum-3)/4))*0.1+MulpAttMin)),max(1,((GrowFactor*(3.5*Grade+3.5*Grade*(PType+PAType)+5*max(0,Grade-50)+15*max(0,Grade-50)*(PType+PAType)+5*max(0,Grade-100)+15*max(0,Grade-100)*(PType+PAType)-(8+28*(PType+PAType))*(Grade-150)*min(1,max(0,Grade-150)))+1.8*StrZizhi*Str/1000+50)*1.2+AdjpAttMax)*(1+min(1,max(0,(SkillNum-3)/4))*0.1+MulpAttMax)))
	SetTmpParam_At(30, newv)
end
CLuaLingShouPropertyFight.SetParamAdjpAttMin = function(newv)
	SetTmpParam_At(31, newv)

	CLuaLingShouPropertyFight.RefreshParampAttMin()
end
CLuaLingShouPropertyFight.SetParamMulpAttMin = function(newv)
	SetTmpParam_At(32, newv)

	CLuaLingShouPropertyFight.RefreshParampAttMin()
end
CLuaLingShouPropertyFight.RefreshParampAttMax = function()
	local AdjpAttMax = GetTmpParam_At(34)
	local MulpAttMax = GetTmpParam_At(35)
	local GrowFactor = GetTmpParam_At(322)
	local Str = GetTmpParam_At(9)
	local StrZizhi = GetTmpParam_At(306)
	local SkillNum = GetTmpParam_At(318)
	local PType = GetTmpParam_At(319)
	local PAType = GetTmpParam_At(382)
	local Grade = GetTmpParam_At(2)

	local newv = max(1,((GrowFactor*(3.5*Grade+3.5*Grade*(PType+PAType)+5*max(0,Grade-50)+15*max(0,Grade-50)*(PType+PAType)+5*max(0,Grade-100)+15*max(0,Grade-100)*(PType+PAType)-(8+28*(PType+PAType))*(Grade-150)*min(1,max(0,Grade-150)))+1.8*StrZizhi*Str/1000+50)*1.2+AdjpAttMax)*(1+min(1,max(0,(SkillNum-3)/4))*0.1+MulpAttMax))
	SetTmpParam_At(33, newv)
end
CLuaLingShouPropertyFight.SetParamAdjpAttMax = function(newv)
	SetTmpParam_At(34, newv)

	CLuaLingShouPropertyFight.RefreshParampAttMin()

	CLuaLingShouPropertyFight.RefreshParampAttMax()
end
CLuaLingShouPropertyFight.SetParamMulpAttMax = function(newv)
	SetTmpParam_At(35, newv)

	CLuaLingShouPropertyFight.RefreshParampAttMin()

	CLuaLingShouPropertyFight.RefreshParampAttMax()
end
CLuaLingShouPropertyFight.RefreshParampHit = function()
	local AdjpHit = GetTmpParam_At(37)
	local MulpHit = GetTmpParam_At(38)
	local Agi = GetTmpParam_At(15)
	local GrowFactor = GetTmpParam_At(322)
	local Grade = GetTmpParam_At(2)
	local AgiZizhi = GetTmpParam_At(309)
	local MAType = GetTmpParam_At(383)
	local PAType = GetTmpParam_At(382)

	local newv = ((2-0.4*PAType-0.4*MAType)*Grade*GrowFactor-(1-0.2*PAType-0.2*MAType)*max(0,Grade-150)*GrowFactor+Agi*min(1200,AgiZizhi)/(2000*(1+PAType+MAType))+Agi*max(0,AgiZizhi-1200)/(10000-6000*(PAType+MAType))-max(0,Agi-600-600*PAType-600*MAType)*(min(1200,AgiZizhi)/(2000*(1+PAType+MAType))+max(0,AgiZizhi-1200)/(10000-6000*(PAType+MAType)))*0.5+AdjpHit)*(1+MulpHit)
	SetTmpParam_At(36, newv)
end
CLuaLingShouPropertyFight.SetParamAdjpHit = function(newv)
	SetTmpParam_At(37, newv)

	CLuaLingShouPropertyFight.RefreshParampHit()
end
CLuaLingShouPropertyFight.SetParamMulpHit = function(newv)
	SetTmpParam_At(38, newv)

	CLuaLingShouPropertyFight.RefreshParampHit()
end
CLuaLingShouPropertyFight.RefreshParampMiss = function()
	local AdjpMiss = GetTmpParam_At(40)
	local MulpMiss = GetTmpParam_At(41)
	local Agi = GetTmpParam_At(15)
	local GrowFactor = GetTmpParam_At(322)
	local Grade = GetTmpParam_At(2)
	local MAType = GetTmpParam_At(383)
	local AgiZizhi = GetTmpParam_At(309)
	local PAType = GetTmpParam_At(382)
	local PType = GetTmpParam_At(319)
	local MType = GetTmpParam_At(320)

	local newv = (0.8*Grade*GrowFactor*(1+PAType+MAType)-0.2*(3+PAType+MAType)*max(0,Grade-150)*GrowFactor+Agi*min(1200,AgiZizhi)/(2000+2000*PAType+2000*MAType)+Agi*max(0,AgiZizhi-1200)/(2000+3200*(PType+MType)+2000*(PAType+MAType))-max(0,Agi-600-600*PAType-600*MAType)*(min(1200,AgiZizhi)/(2000+2000*(PAType+MAType))+max(0,AgiZizhi-1200)/(2000+3200*(PType+MType)+2000*(PAType+MAType)))*0.5+AdjpMiss)*(1+MulpMiss)
	SetTmpParam_At(39, newv)
end
CLuaLingShouPropertyFight.SetParamAdjpMiss = function(newv)
	SetTmpParam_At(40, newv)

	CLuaLingShouPropertyFight.RefreshParampMiss()
end
CLuaLingShouPropertyFight.SetParamMulpMiss = function(newv)
	SetTmpParam_At(41, newv)

	CLuaLingShouPropertyFight.RefreshParampMiss()
end
CLuaLingShouPropertyFight.RefreshParampDef = function()
	local AdjpDef = GetTmpParam_At(43)
	local MulpDef = GetTmpParam_At(44)
	local GrowFactor = GetTmpParam_At(322)
	local StrZizhi = GetTmpParam_At(306)
	local Str = GetTmpParam_At(9)
	local StaZizhi = GetTmpParam_At(308)
	local Sta = GetTmpParam_At(6)
	local Grade = GetTmpParam_At(2)
	local MHType = GetTmpParam_At(351)
	local PHType = GetTmpParam_At(321)
	local SkillNum = GetTmpParam_At(318)

	local newv = max(1,(3*Grade*GrowFactor*(1+PHType+MHType)-(1-PHType-MHType)*(Grade-150)*min(1,max(0,Grade-150))*GrowFactor+StaZizhi*Sta/2000*(4.5-4.25*(PHType+MHType))+StrZizhi*Str/2000*(0.1+4.1*(PHType+MHType))+35+AdjpDef)*(1+min(1,max(0,(SkillNum-3)/4))*0.1+MulpDef))
	SetTmpParam_At(42, newv)
end
CLuaLingShouPropertyFight.SetParamAdjpDef = function(newv)
	SetTmpParam_At(43, newv)

	CLuaLingShouPropertyFight.RefreshParampDef()
end
CLuaLingShouPropertyFight.SetParamMulpDef = function(newv)
	SetTmpParam_At(44, newv)

	CLuaLingShouPropertyFight.RefreshParampDef()
end
CLuaLingShouPropertyFight.SetParamAdjpHurt = function(newv)
	SetTmpParam_At(45, newv)
end
CLuaLingShouPropertyFight.SetParamMulpHurt = function(newv)
	SetTmpParam_At(46, newv)
end
CLuaLingShouPropertyFight.RefreshParampSpeed = function()
	local AdjpSpeed = GetTmpParam_At(1001)
	local MulpSpeed = GetTmpParam_At(49)

	local newv = (1+AdjpSpeed)*(1+ MulpSpeed)
	SetTmpParam_At(47, newv)
end
CLuaLingShouPropertyFight.SetParamOripSpeed = function(newv)
	SetTmpParam_At(48, newv)
end
CLuaLingShouPropertyFight.SetParamMulpSpeed = function(newv)
	SetTmpParam_At(49, newv)

	CLuaLingShouPropertyFight.RefreshParampSpeed()
end
CLuaLingShouPropertyFight.RefreshParampFatal = function()
	local AdjpFatal = GetTmpParam_At(51)
	local PAType = GetTmpParam_At(382)
	local MAType = GetTmpParam_At(383)
	local GrowFactor = GetTmpParam_At(322)
	local AgiZizhi = GetTmpParam_At(309)
	local Grade = GetTmpParam_At(2)
	local Agi = GetTmpParam_At(15)

	local newv = (0.201*min(150,Grade)*GrowFactor+min(1200,Agi)*AgiZizhi/110000*1.5)*(PAType+MAType)+AdjpFatal
	SetTmpParam_At(50, newv)
end
CLuaLingShouPropertyFight.SetParamAdjpFatal = function(newv)
	SetTmpParam_At(51, newv)

	CLuaLingShouPropertyFight.RefreshParampFatal()
end
CLuaLingShouPropertyFight.RefreshParamAntipFatal = function()
	local AdjAntipFatal = GetTmpParam_At(53)

	local newv = AdjAntipFatal
	SetTmpParam_At(52, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntipFatal = function(newv)
	SetTmpParam_At(53, newv)

	CLuaLingShouPropertyFight.RefreshParamAntipFatal()
end
CLuaLingShouPropertyFight.RefreshParampFatalDamage = function()
	local AdjpFatalDamage = GetTmpParam_At(55)
	local PAType = GetTmpParam_At(382)
	local MAType = GetTmpParam_At(383)
	local GrowFactor = GetTmpParam_At(322)
	local AgiZizhi = GetTmpParam_At(309)
	local Grade = GetTmpParam_At(2)
	local Agi = GetTmpParam_At(15)

	local newv = (AdjpFatalDamage + 0.5+(0.201*min(150,Grade)*GrowFactor+min(1200,Agi)*AgiZizhi/110000*1.5)*(PAType+MAType)/300)
	SetTmpParam_At(54, newv)
end
CLuaLingShouPropertyFight.SetParamAdjpFatalDamage = function(newv)
	SetTmpParam_At(55, newv)

	CLuaLingShouPropertyFight.RefreshParampFatalDamage()
end
CLuaLingShouPropertyFight.RefreshParamAntipFatalDamage = function()
	local AdjAntipFatalDamage = GetTmpParam_At(57)

	local newv = AdjAntipFatalDamage
	SetTmpParam_At(56, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntipFatalDamage = function(newv)
	SetTmpParam_At(57, newv)

	CLuaLingShouPropertyFight.RefreshParamAntipFatalDamage()
end
CLuaLingShouPropertyFight.RefreshParamBlock = function()
	local AdjBlock = GetTmpParam_At(59)
	local MulBlock = GetTmpParam_At(60)

	local newv = AdjBlock*(1+MulBlock)
	SetTmpParam_At(58, newv)
end
CLuaLingShouPropertyFight.SetParamAdjBlock = function(newv)
	SetTmpParam_At(59, newv)

	CLuaLingShouPropertyFight.RefreshParamBlock()
end
CLuaLingShouPropertyFight.SetParamMulBlock = function(newv)
	SetTmpParam_At(60, newv)

	CLuaLingShouPropertyFight.RefreshParamBlock()
end
CLuaLingShouPropertyFight.RefreshParamBlockDamage = function()
	local AdjBlockDamage = GetTmpParam_At(62)

	local newv = max(0,AdjBlockDamage)
	SetTmpParam_At(61, newv)
end
CLuaLingShouPropertyFight.SetParamAdjBlockDamage = function(newv)
	SetTmpParam_At(62, newv)

	CLuaLingShouPropertyFight.RefreshParamBlockDamage()
end
CLuaLingShouPropertyFight.RefreshParamAntiBlock = function()
	local AdjAntiBlock = GetTmpParam_At(64)

	local newv = AdjAntiBlock+1900
	SetTmpParam_At(63, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiBlock = function(newv)
	SetTmpParam_At(64, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiBlock()
end
CLuaLingShouPropertyFight.RefreshParamAntiBlockDamage = function()
	local AdjAntiBlockDamage = GetTmpParam_At(66)

	local newv = AdjAntiBlockDamage
	SetTmpParam_At(65, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiBlockDamage = function(newv)
	SetTmpParam_At(66, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiBlockDamage()
end
CLuaLingShouPropertyFight.RefreshParammAttMin = function()
	local AdjmAttMin = GetTmpParam_At(68)
	local MulmAttMin = GetTmpParam_At(69)
	local AdjmAttMax = GetTmpParam_At(71)
	local MulmAttMax = GetTmpParam_At(72)
	local GrowFactor = GetTmpParam_At(322)
	local IntZizhi = GetTmpParam_At(310)
	local Int = GetTmpParam_At(12)
	local SkillNum = GetTmpParam_At(318)
	local MType = GetTmpParam_At(320)
	local MAType = GetTmpParam_At(383)
	local Grade = GetTmpParam_At(2)

	local newv = min(max(1,((GrowFactor*(3.5*Grade+3.5*Grade*(MType+MAType)+5*max(0,Grade-50)+15*max(0,Grade-50)*(MType+MAType)+5*max(0,Grade-100)+15*max(0,Grade-100)*(MType+MAType)-(8+28*MType+37*MAType)*(Grade-150)*min(1,max(0,Grade-150)))+1.8*Int*IntZizhi/1000+50)*0.8+AdjmAttMin)*(1+min(1,max(0,(SkillNum-3)/4))*0.1+MulmAttMin)),max(1,((GrowFactor*(3.5*Grade+3.5*Grade*(MType+MAType)+5*max(0,Grade-50)+15*max(0,Grade-50)*(MType+MAType)+5*max(0,Grade-100)+15*max(0,Grade-100)*(MType+MAType)-(8+28*MType+37*MAType)*(Grade-150)*min(1,max(0,Grade-150)))+1.8*Int*IntZizhi/1000+50)*1.2+AdjmAttMax)*(1+min(1,max(0,(SkillNum-3)/4))*0.1+MulmAttMax)))
	SetTmpParam_At(67, newv)
end
CLuaLingShouPropertyFight.SetParamAdjmAttMin = function(newv)
	SetTmpParam_At(68, newv)

	CLuaLingShouPropertyFight.RefreshParammAttMin()
end
CLuaLingShouPropertyFight.SetParamMulmAttMin = function(newv)
	SetTmpParam_At(69, newv)

	CLuaLingShouPropertyFight.RefreshParammAttMin()
end
CLuaLingShouPropertyFight.RefreshParammAttMax = function()
	local AdjmAttMax = GetTmpParam_At(71)
	local MulmAttMax = GetTmpParam_At(72)
	local GrowFactor = GetTmpParam_At(322)
	local IntZizhi = GetTmpParam_At(310)
	local Int = GetTmpParam_At(12)
	local SkillNum = GetTmpParam_At(318)
	local MType = GetTmpParam_At(320)
	local MAType = GetTmpParam_At(383)
	local Grade = GetTmpParam_At(2)

	local newv = max(1,((GrowFactor*(3.5*Grade+3.5*Grade*(MType+MAType)+5*max(0,Grade-50)+15*max(0,Grade-50)*(MType+MAType)+5*max(0,Grade-100)+15*max(0,Grade-100)*(MType+MAType)-(8+28*MType+37*MAType)*(Grade-150)*min(1,max(0,Grade-150)))+1.8*Int*IntZizhi/1000+50)*1.2+AdjmAttMax)*(1+min(1,max(0,(SkillNum-3)/4))*0.1+MulmAttMax))
	SetTmpParam_At(70, newv)
end
CLuaLingShouPropertyFight.SetParamAdjmAttMax = function(newv)
	SetTmpParam_At(71, newv)

	CLuaLingShouPropertyFight.RefreshParammAttMax()

	CLuaLingShouPropertyFight.RefreshParammAttMin()
end
CLuaLingShouPropertyFight.SetParamMulmAttMax = function(newv)
	SetTmpParam_At(72, newv)

	CLuaLingShouPropertyFight.RefreshParammAttMax()

	CLuaLingShouPropertyFight.RefreshParammAttMin()
end
CLuaLingShouPropertyFight.RefreshParammHit = function()
	local AdjmHit = GetTmpParam_At(74)
	local MulmHit = GetTmpParam_At(75)
	local Agi = GetTmpParam_At(15)
	local GrowFactor = GetTmpParam_At(322)
	local Grade = GetTmpParam_At(2)
	local AgiZizhi = GetTmpParam_At(309)
	local MAType = GetTmpParam_At(383)
	local PAType = GetTmpParam_At(382)

	local newv = ((2-0.4*PAType-0.4*MAType)*Grade*GrowFactor-(1-0.2*PAType-0.2*MAType)*max(0,Grade-150)*GrowFactor+Agi*min(1200,AgiZizhi)/(2000*(1+PAType+MAType))+Agi*max(0,AgiZizhi-1200)/(10000-6000*(PAType+MAType))-max(0,Agi-600-600*PAType-600*MAType)*(min(1200,AgiZizhi)/(2000*(1+PAType+MAType))+max(0,AgiZizhi-1200)/(10000-6000*(PAType+MAType)))*0.5+AdjmHit)*(1+MulmHit)
	SetTmpParam_At(73, newv)
end
CLuaLingShouPropertyFight.SetParamAdjmHit = function(newv)
	SetTmpParam_At(74, newv)

	CLuaLingShouPropertyFight.RefreshParammHit()
end
CLuaLingShouPropertyFight.SetParamMulmHit = function(newv)
	SetTmpParam_At(75, newv)

	CLuaLingShouPropertyFight.RefreshParammHit()
end
CLuaLingShouPropertyFight.RefreshParammMiss = function()
	local AdjmMiss = GetTmpParam_At(77)
	local MulmMiss = GetTmpParam_At(78)
	local Agi = GetTmpParam_At(15)
	local GrowFactor = GetTmpParam_At(322)
	local Grade = GetTmpParam_At(2)
	local MAType = GetTmpParam_At(383)
	local AgiZizhi = GetTmpParam_At(309)
	local PAType = GetTmpParam_At(382)
	local PType = GetTmpParam_At(319)
	local MType = GetTmpParam_At(320)

	local newv = (0.8*Grade*GrowFactor*(1+PAType+MAType)-0.2*(3+PAType+MAType)*max(0,Grade-150)*GrowFactor+Agi*min(1200,AgiZizhi)/(2000+2000*PAType+2000*MAType)+Agi*max(0,AgiZizhi-1200)/(2000+3200*(PType+MType)+2000*(PAType+MAType))-max(0,Agi-600-600*PAType-600*MAType)*(min(1200,AgiZizhi)/(2000+2000*(PAType+MAType))+max(0,AgiZizhi-1200)/(2000+3200*(PType+MType)+2000*(PAType+MAType)))*0.5+AdjmMiss)*(1+MulmMiss)
	SetTmpParam_At(76, newv)
end
CLuaLingShouPropertyFight.SetParamAdjmMiss = function(newv)
	SetTmpParam_At(77, newv)

	CLuaLingShouPropertyFight.RefreshParammMiss()
end
CLuaLingShouPropertyFight.SetParamMulmMiss = function(newv)
	SetTmpParam_At(78, newv)

	CLuaLingShouPropertyFight.RefreshParammMiss()
end
CLuaLingShouPropertyFight.RefreshParammDef = function()
	local AdjmDef = GetTmpParam_At(80)
	local MulmDef = GetTmpParam_At(81)
	local GrowFactor = GetTmpParam_At(322)
	local IntZizhi = GetTmpParam_At(310)
	local Int = GetTmpParam_At(12)
	local StaZizhi = GetTmpParam_At(308)
	local Sta = GetTmpParam_At(6)
	local Grade = GetTmpParam_At(2)
	local MHType = GetTmpParam_At(351)
	local PHType = GetTmpParam_At(321)
	local SkillNum = GetTmpParam_At(318)

	local newv = max(1,(3*Grade*GrowFactor*(1+PHType+MHType)-(1-PHType-MHType)*(Grade-150)*min(1,max(0,Grade-150))*GrowFactor+StaZizhi*Sta/2000*(4.5-4.25*(PHType+MHType))+IntZizhi*Int/2000*(0.1+4.1*(PHType+MHType))+35+AdjmDef)*(1+min(1,max(0,(SkillNum-3)/4))*0.1+MulmDef))
	SetTmpParam_At(79, newv)
end
CLuaLingShouPropertyFight.SetParamAdjmDef = function(newv)
	SetTmpParam_At(80, newv)

	CLuaLingShouPropertyFight.RefreshParammDef()
end
CLuaLingShouPropertyFight.SetParamMulmDef = function(newv)
	SetTmpParam_At(81, newv)

	CLuaLingShouPropertyFight.RefreshParammDef()
end
CLuaLingShouPropertyFight.SetParamAdjmHurt = function(newv)
	SetTmpParam_At(82, newv)
end
CLuaLingShouPropertyFight.SetParamMulmHurt = function(newv)
	SetTmpParam_At(83, newv)
end
CLuaLingShouPropertyFight.RefreshParammSpeed = function()
	local AdjmSpeed = GetTmpParam_At(1002)
	local MulmSpeed = GetTmpParam_At(86)

	local newv = (1+AdjmSpeed)*(1+ MulmSpeed)
	SetTmpParam_At(84, newv)
end
CLuaLingShouPropertyFight.SetParamOrimSpeed = function(newv)
	SetTmpParam_At(85, newv)
end
CLuaLingShouPropertyFight.SetParamMulmSpeed = function(newv)
	SetTmpParam_At(86, newv)

	CLuaLingShouPropertyFight.RefreshParammSpeed()
end
CLuaLingShouPropertyFight.RefreshParammFatal = function()
	local AdjmFatal = GetTmpParam_At(88)
	local PAType = GetTmpParam_At(382)
	local MAType = GetTmpParam_At(383)
	local GrowFactor = GetTmpParam_At(322)
	local AgiZizhi = GetTmpParam_At(309)
	local Grade = GetTmpParam_At(2)
	local Agi = GetTmpParam_At(15)

	local newv = (0.201*min(150,Grade)*GrowFactor+min(1200,Agi)*AgiZizhi/110000*1.5)*(PAType+MAType)+AdjmFatal
	SetTmpParam_At(87, newv)
end
CLuaLingShouPropertyFight.SetParamAdjmFatal = function(newv)
	SetTmpParam_At(88, newv)

	CLuaLingShouPropertyFight.RefreshParammFatal()
end
CLuaLingShouPropertyFight.RefreshParamAntimFatal = function()
	local AdjAntimFatal = GetTmpParam_At(90)

	local newv = AdjAntimFatal
	SetTmpParam_At(89, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntimFatal = function(newv)
	SetTmpParam_At(90, newv)

	CLuaLingShouPropertyFight.RefreshParamAntimFatal()
end
CLuaLingShouPropertyFight.RefreshParammFatalDamage = function()
	local AdjmFatalDamage = GetTmpParam_At(92)
	local PAType = GetTmpParam_At(382)
	local MAType = GetTmpParam_At(383)
	local GrowFactor = GetTmpParam_At(322)
	local AgiZizhi = GetTmpParam_At(309)
	local Grade = GetTmpParam_At(2)
	local Agi = GetTmpParam_At(15)

	local newv = (AdjmFatalDamage+0.5+(0.201*min(150,Grade)*GrowFactor+min(1200,Agi)*AgiZizhi/110000*1.5)*(PAType+MAType)/300)
	SetTmpParam_At(91, newv)
end
CLuaLingShouPropertyFight.SetParamAdjmFatalDamage = function(newv)
	SetTmpParam_At(92, newv)

	CLuaLingShouPropertyFight.RefreshParammFatalDamage()
end
CLuaLingShouPropertyFight.RefreshParamAntimFatalDamage = function()
	local AdjAntimFatalDamage = GetTmpParam_At(94)

	local newv = AdjAntimFatalDamage
	SetTmpParam_At(93, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntimFatalDamage = function(newv)
	SetTmpParam_At(94, newv)

	CLuaLingShouPropertyFight.RefreshParamAntimFatalDamage()
end
CLuaLingShouPropertyFight.RefreshParamEnhanceFire = function()
	local AdjEnhanceFire = GetTmpParam_At(96)

	local newv = AdjEnhanceFire
	SetTmpParam_At(95, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceFire = function(newv)
	SetTmpParam_At(96, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceFire()
end
CLuaLingShouPropertyFight.SetParamMulEnhanceFire = function(newv)
	SetTmpParam_At(97, newv)
end
CLuaLingShouPropertyFight.RefreshParamEnhanceThunder = function()
	local AdjEnhanceThunder = GetTmpParam_At(99)

	local newv = AdjEnhanceThunder
	SetTmpParam_At(98, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceThunder = function(newv)
	SetTmpParam_At(99, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceThunder()
end
CLuaLingShouPropertyFight.SetParamMulEnhanceThunder = function(newv)
	SetTmpParam_At(100, newv)
end
CLuaLingShouPropertyFight.RefreshParamEnhanceIce = function()
	local AdjEnhanceIce = GetTmpParam_At(102)

	local newv = AdjEnhanceIce
	SetTmpParam_At(101, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceIce = function(newv)
	SetTmpParam_At(102, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceIce()
end
CLuaLingShouPropertyFight.SetParamMulEnhanceIce = function(newv)
	SetTmpParam_At(103, newv)
end
CLuaLingShouPropertyFight.RefreshParamEnhancePoison = function()
	local AdjEnhancePoison = GetTmpParam_At(105)

	local newv = AdjEnhancePoison
	SetTmpParam_At(104, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhancePoison = function(newv)
	SetTmpParam_At(105, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhancePoison()
end
CLuaLingShouPropertyFight.SetParamMulEnhancePoison = function(newv)
	SetTmpParam_At(106, newv)
end
CLuaLingShouPropertyFight.RefreshParamEnhanceWind = function()
	local AdjEnhanceWind = GetTmpParam_At(108)

	local newv = AdjEnhanceWind
	SetTmpParam_At(107, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceWind = function(newv)
	SetTmpParam_At(108, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceWind()
end
CLuaLingShouPropertyFight.SetParamMulEnhanceWind = function(newv)
	SetTmpParam_At(109, newv)
end
CLuaLingShouPropertyFight.RefreshParamEnhanceLight = function()
	local AdjEnhanceLight = GetTmpParam_At(111)

	local newv = AdjEnhanceLight
	SetTmpParam_At(110, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceLight = function(newv)
	SetTmpParam_At(111, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceLight()
end
CLuaLingShouPropertyFight.SetParamMulEnhanceLight = function(newv)
	SetTmpParam_At(112, newv)
end
CLuaLingShouPropertyFight.RefreshParamEnhanceIllusion = function()
	local AdjEnhanceIllusion = GetTmpParam_At(114)

	local newv = AdjEnhanceIllusion
	SetTmpParam_At(113, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceIllusion = function(newv)
	SetTmpParam_At(114, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceIllusion()
end
CLuaLingShouPropertyFight.SetParamMulEnhanceIllusion = function(newv)
	SetTmpParam_At(115, newv)
end
CLuaLingShouPropertyFight.RefreshParamAntiFire = function()
	local AdjAntiFire = GetTmpParam_At(117)
	local MulAntiFire = GetTmpParam_At(118)

	local newv = AdjAntiFire*(1+MulAntiFire)
	SetTmpParam_At(116, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiFire = function(newv)
	SetTmpParam_At(117, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiFire()
end
CLuaLingShouPropertyFight.SetParamMulAntiFire = function(newv)
	SetTmpParam_At(118, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiFire()
end
CLuaLingShouPropertyFight.RefreshParamAntiThunder = function()
	local AdjAntiThunder = GetTmpParam_At(120)
	local MulAntiThunder = GetTmpParam_At(121)

	local newv = AdjAntiThunder*(1+MulAntiThunder)
	SetTmpParam_At(119, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiThunder = function(newv)
	SetTmpParam_At(120, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiThunder()
end
CLuaLingShouPropertyFight.SetParamMulAntiThunder = function(newv)
	SetTmpParam_At(121, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiThunder()
end
CLuaLingShouPropertyFight.RefreshParamAntiIce = function()
	local AdjAntiIce = GetTmpParam_At(123)
	local MulAntiIce = GetTmpParam_At(124)

	local newv = AdjAntiIce*(1+MulAntiIce)
	SetTmpParam_At(122, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiIce = function(newv)
	SetTmpParam_At(123, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiIce()
end
CLuaLingShouPropertyFight.SetParamMulAntiIce = function(newv)
	SetTmpParam_At(124, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiIce()
end
CLuaLingShouPropertyFight.RefreshParamAntiPoison = function()
	local AdjAntiPoison = GetTmpParam_At(126)
	local MulAntiPoison = GetTmpParam_At(127)

	local newv = AdjAntiPoison*(1+MulAntiPoison)
	SetTmpParam_At(125, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiPoison = function(newv)
	SetTmpParam_At(126, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiPoison()
end
CLuaLingShouPropertyFight.SetParamMulAntiPoison = function(newv)
	SetTmpParam_At(127, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiPoison()
end
CLuaLingShouPropertyFight.RefreshParamAntiWind = function()
	local AdjAntiWind = GetTmpParam_At(129)
	local MulAntiWind = GetTmpParam_At(130)

	local newv = AdjAntiWind*(1+MulAntiWind)
	SetTmpParam_At(128, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiWind = function(newv)
	SetTmpParam_At(129, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiWind()
end
CLuaLingShouPropertyFight.SetParamMulAntiWind = function(newv)
	SetTmpParam_At(130, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiWind()
end
CLuaLingShouPropertyFight.RefreshParamAntiLight = function()
	local AdjAntiLight = GetTmpParam_At(132)
	local MulAntiLight = GetTmpParam_At(133)

	local newv = AdjAntiLight*(1+MulAntiLight)
	SetTmpParam_At(131, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiLight = function(newv)
	SetTmpParam_At(132, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiLight()
end
CLuaLingShouPropertyFight.SetParamMulAntiLight = function(newv)
	SetTmpParam_At(133, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiLight()
end
CLuaLingShouPropertyFight.RefreshParamAntiIllusion = function()
	local AdjAntiIllusion = GetTmpParam_At(135)
	local MulAntiIllusion = GetTmpParam_At(136)

	local newv = AdjAntiIllusion*(1+MulAntiIllusion)
	SetTmpParam_At(134, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiIllusion = function(newv)
	SetTmpParam_At(135, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiIllusion()
end
CLuaLingShouPropertyFight.SetParamMulAntiIllusion = function(newv)
	SetTmpParam_At(136, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiIllusion()
end
CLuaLingShouPropertyFight.RefreshParamIgnoreAntiFire = function()
	local AdjIgnoreAntiFire = GetTmpParam_At(417)
	local IgnoreAntiFireLSMul = GetTmpParam_At(424)

	local newv = AdjIgnoreAntiFire*(1+IgnoreAntiFireLSMul)
	SetTmpParam_At(137, newv)
end
CLuaLingShouPropertyFight.RefreshParamIgnoreAntiThunder = function()
	local AdjIgnoreAntiThunder = GetTmpParam_At(418)
	local IgnoreAntiThunderLSMul = GetTmpParam_At(425)

	local newv = AdjIgnoreAntiThunder*(1+IgnoreAntiThunderLSMul)
	SetTmpParam_At(138, newv)
end
CLuaLingShouPropertyFight.RefreshParamIgnoreAntiIce = function()
	local AdjIgnoreAntiIce = GetTmpParam_At(419)
	local IgnoreAntiIceLSMul = GetTmpParam_At(426)

	local newv = AdjIgnoreAntiIce*(1+IgnoreAntiIceLSMul)
	SetTmpParam_At(139, newv)
end
CLuaLingShouPropertyFight.RefreshParamIgnoreAntiPoison = function()
	local AdjIgnoreAntiPoison = GetTmpParam_At(420)
	local IgnoreAntiPoisonLSMul = GetTmpParam_At(427)

	local newv = AdjIgnoreAntiPoison*(1+IgnoreAntiPoisonLSMul)
	SetTmpParam_At(140, newv)
end
CLuaLingShouPropertyFight.RefreshParamIgnoreAntiWind = function()
	local AdjIgnoreAntiWind = GetTmpParam_At(421)
	local IgnoreAntiWindLSMul = GetTmpParam_At(428)

	local newv = AdjIgnoreAntiWind*(1+IgnoreAntiWindLSMul)
	SetTmpParam_At(141, newv)
end
CLuaLingShouPropertyFight.RefreshParamIgnoreAntiLight = function()
	local AdjIgnoreAntiLight = GetTmpParam_At(422)
	local IgnoreAntiLightLSMul = GetTmpParam_At(429)

	local newv = AdjIgnoreAntiLight*(1+IgnoreAntiLightLSMul)
	SetTmpParam_At(142, newv)
end
CLuaLingShouPropertyFight.RefreshParamIgnoreAntiIllusion = function()
	local AdjIgnoreAntiIllusion = GetTmpParam_At(423)
	local IgnoreAntiIllusionLSMul = GetTmpParam_At(430)

	local newv = AdjIgnoreAntiIllusion*(1+IgnoreAntiIllusionLSMul)
	SetTmpParam_At(143, newv)
end
CLuaLingShouPropertyFight.RefreshParamEnhanceDizzy = function()
	local AdjEnhanceDizzy = GetTmpParam_At(145)
	local MulEnhanceDizzy = GetTmpParam_At(146)

	local newv = AdjEnhanceDizzy*(1+MulEnhanceDizzy)
	SetTmpParam_At(144, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceDizzy = function(newv)
	SetTmpParam_At(145, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceDizzy()
end
CLuaLingShouPropertyFight.SetParamMulEnhanceDizzy = function(newv)
	SetTmpParam_At(146, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceDizzy()
end
CLuaLingShouPropertyFight.RefreshParamEnhanceSleep = function()
	local AdjEnhanceSleep = GetTmpParam_At(148)
	local MulEnhanceSleep = GetTmpParam_At(149)

	local newv = AdjEnhanceSleep*(1+MulEnhanceSleep)
	SetTmpParam_At(147, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceSleep = function(newv)
	SetTmpParam_At(148, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceSleep()
end
CLuaLingShouPropertyFight.SetParamMulEnhanceSleep = function(newv)
	SetTmpParam_At(149, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceSleep()
end
CLuaLingShouPropertyFight.RefreshParamEnhanceChaos = function()
	local AdjEnhanceChaos = GetTmpParam_At(151)
	local MulEnhanceChaos = GetTmpParam_At(152)

	local newv = AdjEnhanceChaos*(1+MulEnhanceChaos)
	SetTmpParam_At(150, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceChaos = function(newv)
	SetTmpParam_At(151, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceChaos()
end
CLuaLingShouPropertyFight.SetParamMulEnhanceChaos = function(newv)
	SetTmpParam_At(152, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceChaos()
end
CLuaLingShouPropertyFight.RefreshParamEnhanceBind = function()
	local AdjEnhanceBind = GetTmpParam_At(154)
	local MulEnhanceBind = GetTmpParam_At(155)

	local newv = AdjEnhanceBind*(1+MulEnhanceBind)
	SetTmpParam_At(153, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceBind = function(newv)
	SetTmpParam_At(154, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceBind()
end
CLuaLingShouPropertyFight.SetParamMulEnhanceBind = function(newv)
	SetTmpParam_At(155, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceBind()
end
CLuaLingShouPropertyFight.RefreshParamEnhanceSilence = function()
	local AdjEnhanceSilence = GetTmpParam_At(157)
	local MulEnhanceSilence = GetTmpParam_At(158)

	local newv = AdjEnhanceSilence*(1+MulEnhanceSilence)
	SetTmpParam_At(156, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceSilence = function(newv)
	SetTmpParam_At(157, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceSilence()
end
CLuaLingShouPropertyFight.SetParamMulEnhanceSilence = function(newv)
	SetTmpParam_At(158, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceSilence()
end
CLuaLingShouPropertyFight.RefreshParamAntiDizzy = function()
	local AdjAntiDizzy = GetTmpParam_At(160)
	local MulAntiDizzy = GetTmpParam_At(161)

	local newv = AdjAntiDizzy*(1+MulAntiDizzy)
	SetTmpParam_At(159, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiDizzy = function(newv)
	SetTmpParam_At(160, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiDizzy()
end
CLuaLingShouPropertyFight.SetParamMulAntiDizzy = function(newv)
	SetTmpParam_At(161, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiDizzy()
end
CLuaLingShouPropertyFight.RefreshParamAntiSleep = function()
	local AdjAntiSleep = GetTmpParam_At(163)
	local MulAntiSleep = GetTmpParam_At(164)

	local newv = AdjAntiSleep*(1+MulAntiSleep)
	SetTmpParam_At(162, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiSleep = function(newv)
	SetTmpParam_At(163, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiSleep()
end
CLuaLingShouPropertyFight.SetParamMulAntiSleep = function(newv)
	SetTmpParam_At(164, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiSleep()
end
CLuaLingShouPropertyFight.RefreshParamAntiChaos = function()
	local AdjAntiChaos = GetTmpParam_At(166)
	local MulAntiChaos = GetTmpParam_At(167)

	local newv = AdjAntiChaos*(1+MulAntiChaos)
	SetTmpParam_At(165, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiChaos = function(newv)
	SetTmpParam_At(166, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiChaos()
end
CLuaLingShouPropertyFight.SetParamMulAntiChaos = function(newv)
	SetTmpParam_At(167, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiChaos()
end
CLuaLingShouPropertyFight.RefreshParamAntiBind = function()
	local AdjAntiBind = GetTmpParam_At(169)
	local MulAntiBind = GetTmpParam_At(170)

	local newv = AdjAntiBind*(1+MulAntiBind)
	SetTmpParam_At(168, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiBind = function(newv)
	SetTmpParam_At(169, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiBind()
end
CLuaLingShouPropertyFight.SetParamMulAntiBind = function(newv)
	SetTmpParam_At(170, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiBind()
end
CLuaLingShouPropertyFight.RefreshParamAntiSilence = function()
	local AdjAntiSilence = GetTmpParam_At(172)
	local MulAntiSilence = GetTmpParam_At(173)

	local newv = AdjAntiSilence*(1+MulAntiSilence)
	SetTmpParam_At(171, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiSilence = function(newv)
	SetTmpParam_At(172, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiSilence()
end
CLuaLingShouPropertyFight.SetParamMulAntiSilence = function(newv)
	SetTmpParam_At(173, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiSilence()
end
CLuaLingShouPropertyFight.SetParamDizzyTimeChange = function(newv)
	SetTmpParam_At(174, newv)
end
CLuaLingShouPropertyFight.SetParamSleepTimeChange = function(newv)
	SetTmpParam_At(175, newv)
end
CLuaLingShouPropertyFight.SetParamChaosTimeChange = function(newv)
	SetTmpParam_At(176, newv)
end
CLuaLingShouPropertyFight.SetParamBindTimeChange = function(newv)
	SetTmpParam_At(177, newv)
end
CLuaLingShouPropertyFight.SetParamSilenceTimeChange = function(newv)
	SetTmpParam_At(178, newv)
end
CLuaLingShouPropertyFight.SetParamBianhuTimeChange = function(newv)
	SetTmpParam_At(179, newv)
end
CLuaLingShouPropertyFight.SetParamFreezeTimeChange = function(newv)
	SetTmpParam_At(180, newv)
end
CLuaLingShouPropertyFight.SetParamPetrifyTimeChange = function(newv)
	SetTmpParam_At(181, newv)
end
CLuaLingShouPropertyFight.RefreshParamEnhanceHeal = function()
	local AdjEnhanceHeal = GetTmpParam_At(183)

	local newv = AdjEnhanceHeal
	SetTmpParam_At(182, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceHeal = function(newv)
	SetTmpParam_At(183, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceHeal()
end
CLuaLingShouPropertyFight.SetParamMulEnhanceHeal = function(newv)
	SetTmpParam_At(184, newv)
end
CLuaLingShouPropertyFight.RefreshParamSpeed = function()
	local AdjSpeed = GetTmpParam_At(187)
	local MulSpeed = GetTmpParam_At(188)

	local newv = (4.5+AdjSpeed)*max(0.3,1+MulSpeed)
	SetTmpParam_At(185, newv)
end
CLuaLingShouPropertyFight.SetParamOriSpeed = function(newv)
	SetTmpParam_At(186, newv)
end
CLuaLingShouPropertyFight.SetParamAdjSpeed = function(newv)
	SetTmpParam_At(187, newv)

	CLuaLingShouPropertyFight.RefreshParamSpeed()
end
CLuaLingShouPropertyFight.SetParamMulSpeed = function(newv)
	SetTmpParam_At(188, newv)

	CLuaLingShouPropertyFight.RefreshParamSpeed()
end
CLuaLingShouPropertyFight.SetParamMF = function(newv)
	SetTmpParam_At(189, newv)
end
CLuaLingShouPropertyFight.SetParamAdjMF = function(newv)
	SetTmpParam_At(190, newv)
end
CLuaLingShouPropertyFight.SetParamRange = function(newv)
	SetTmpParam_At(191, newv)
end
CLuaLingShouPropertyFight.SetParamAdjRange = function(newv)
	SetTmpParam_At(192, newv)
end
CLuaLingShouPropertyFight.SetParamHatePlus = function(newv)
	SetTmpParam_At(193, newv)
end
CLuaLingShouPropertyFight.SetParamAdjHatePlus = function(newv)
	SetTmpParam_At(194, newv)
end
CLuaLingShouPropertyFight.SetParamHateDecrease = function(newv)
	SetTmpParam_At(195, newv)
end
CLuaLingShouPropertyFight.RefreshParamInvisible = function()
	local AdjInvisible = GetTmpParam_At(197)

	local newv = AdjInvisible
	SetTmpParam_At(196, newv)
end
CLuaLingShouPropertyFight.SetParamAdjInvisible = function(newv)
	SetTmpParam_At(197, newv)

	CLuaLingShouPropertyFight.RefreshParamInvisible()
end
CLuaLingShouPropertyFight.RefreshParamTrueSight = function()
	local OriTrueSight = GetTmpParam_At(199)
	local AdjTrueSight = GetTmpParam_At(200)

	local newv = OriTrueSight + AdjTrueSight
	SetTmpParam_At(198, newv)
end
CLuaLingShouPropertyFight.SetParamOriTrueSight = function(newv)
	SetTmpParam_At(199, newv)

	CLuaLingShouPropertyFight.RefreshParamTrueSight()
end
CLuaLingShouPropertyFight.SetParamAdjTrueSight = function(newv)
	SetTmpParam_At(200, newv)

	CLuaLingShouPropertyFight.RefreshParamTrueSight()
end
CLuaLingShouPropertyFight.RefreshParamEyeSight = function()
	local AdjEyeSight = GetTmpParam_At(203)

	local newv = 15 + AdjEyeSight
	SetTmpParam_At(201, newv)
end
CLuaLingShouPropertyFight.SetParamOriEyeSight = function(newv)
	SetTmpParam_At(202, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEyeSight = function(newv)
	SetTmpParam_At(203, newv)

	CLuaLingShouPropertyFight.RefreshParamEyeSight()
end
CLuaLingShouPropertyFight.SetParamEnhanceTian = function(newv)
	SetTmpParam_At(204, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceTianMul = function(newv)
	SetTmpParam_At(205, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceEGui = function(newv)
	SetTmpParam_At(206, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceEGuiMul = function(newv)
	SetTmpParam_At(207, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceXiuLuo = function(newv)
	SetTmpParam_At(208, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceXiuLuoMul = function(newv)
	SetTmpParam_At(209, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceDiYu = function(newv)
	SetTmpParam_At(210, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceDiYuMul = function(newv)
	SetTmpParam_At(211, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceRen = function(newv)
	SetTmpParam_At(212, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceRenMul = function(newv)
	SetTmpParam_At(213, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceChuSheng = function(newv)
	SetTmpParam_At(214, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceChuShengMul = function(newv)
	SetTmpParam_At(215, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceBuilding = function(newv)
	SetTmpParam_At(216, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceBuildingMul = function(newv)
	SetTmpParam_At(217, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceBoss = function(newv)
	SetTmpParam_At(218, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceBossMul = function(newv)
	SetTmpParam_At(219, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceSheShou = function(newv)
	SetTmpParam_At(220, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceSheShouMul = function(newv)
	SetTmpParam_At(221, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceJiaShi = function(newv)
	SetTmpParam_At(222, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceJiaShiMul = function(newv)
	SetTmpParam_At(223, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceFangShi = function(newv)
	SetTmpParam_At(224, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceFangShiMul = function(newv)
	SetTmpParam_At(225, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceYiShi = function(newv)
	SetTmpParam_At(226, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceYiShiMul = function(newv)
	SetTmpParam_At(227, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceMeiZhe = function(newv)
	SetTmpParam_At(228, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceMeiZheMul = function(newv)
	SetTmpParam_At(229, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceYiRen = function(newv)
	SetTmpParam_At(230, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceYiRenMul = function(newv)
	SetTmpParam_At(231, newv)
end
CLuaLingShouPropertyFight.SetParamIgnorepDef = function(newv)
	SetTmpParam_At(232, newv)
end
CLuaLingShouPropertyFight.SetParamIgnoremDef = function(newv)
	SetTmpParam_At(233, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceZhaoHuan = function(newv)
	SetTmpParam_At(234, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceZhaoHuanMul = function(newv)
	SetTmpParam_At(235, newv)
end
CLuaLingShouPropertyFight.SetParamDizzyProbability = function(newv)
	SetTmpParam_At(236, newv)
end
CLuaLingShouPropertyFight.SetParamSleepProbability = function(newv)
	SetTmpParam_At(237, newv)
end
CLuaLingShouPropertyFight.SetParamChaosProbability = function(newv)
	SetTmpParam_At(238, newv)
end
CLuaLingShouPropertyFight.SetParamBindProbability = function(newv)
	SetTmpParam_At(239, newv)
end
CLuaLingShouPropertyFight.SetParamSilenceProbability = function(newv)
	SetTmpParam_At(240, newv)
end
CLuaLingShouPropertyFight.SetParamHpFullPow2 = function(newv)
	SetTmpParam_At(241, newv)
end
CLuaLingShouPropertyFight.SetParamHpFullPow1 = function(newv)
	SetTmpParam_At(242, newv)
end
CLuaLingShouPropertyFight.SetParamHpFullInit = function(newv)
	SetTmpParam_At(243, newv)
end
CLuaLingShouPropertyFight.SetParamMpFullPow1 = function(newv)
	SetTmpParam_At(244, newv)
end
CLuaLingShouPropertyFight.SetParamMpFullInit = function(newv)
	SetTmpParam_At(245, newv)
end
CLuaLingShouPropertyFight.SetParamMpRecoverPow1 = function(newv)
	SetTmpParam_At(246, newv)
end
CLuaLingShouPropertyFight.SetParamMpRecoverInit = function(newv)
	SetTmpParam_At(247, newv)
end
CLuaLingShouPropertyFight.SetParamPAttMinPow2 = function(newv)
	SetTmpParam_At(248, newv)
end
CLuaLingShouPropertyFight.SetParamPAttMinPow1 = function(newv)
	SetTmpParam_At(249, newv)
end
CLuaLingShouPropertyFight.SetParamPAttMinInit = function(newv)
	SetTmpParam_At(250, newv)
end
CLuaLingShouPropertyFight.SetParamPAttMaxPow2 = function(newv)
	SetTmpParam_At(251, newv)
end
CLuaLingShouPropertyFight.SetParamPAttMaxPow1 = function(newv)
	SetTmpParam_At(252, newv)
end
CLuaLingShouPropertyFight.SetParamPAttMaxInit = function(newv)
	SetTmpParam_At(253, newv)
end
CLuaLingShouPropertyFight.SetParamPhitPow1 = function(newv)
	SetTmpParam_At(254, newv)
end
CLuaLingShouPropertyFight.SetParamPHitInit = function(newv)
	SetTmpParam_At(255, newv)
end
CLuaLingShouPropertyFight.SetParamPMissPow1 = function(newv)
	SetTmpParam_At(256, newv)
end
CLuaLingShouPropertyFight.SetParamPMissInit = function(newv)
	SetTmpParam_At(257, newv)
end
CLuaLingShouPropertyFight.SetParamPSpeedPow1 = function(newv)
	SetTmpParam_At(258, newv)
end
CLuaLingShouPropertyFight.SetParamPSpeedInit = function(newv)
	SetTmpParam_At(259, newv)
end
CLuaLingShouPropertyFight.SetParamPDefPow1 = function(newv)
	SetTmpParam_At(260, newv)
end
CLuaLingShouPropertyFight.SetParamPDefInit = function(newv)
	SetTmpParam_At(261, newv)
end
CLuaLingShouPropertyFight.SetParamPfatalPow1 = function(newv)
	SetTmpParam_At(262, newv)
end
CLuaLingShouPropertyFight.SetParamPFatalInit = function(newv)
	SetTmpParam_At(263, newv)
end
CLuaLingShouPropertyFight.SetParamMAttMinPow2 = function(newv)
	SetTmpParam_At(264, newv)
end
CLuaLingShouPropertyFight.SetParamMAttMinPow1 = function(newv)
	SetTmpParam_At(265, newv)
end
CLuaLingShouPropertyFight.SetParamMAttMinInit = function(newv)
	SetTmpParam_At(266, newv)
end
CLuaLingShouPropertyFight.SetParamMAttMaxPow2 = function(newv)
	SetTmpParam_At(267, newv)
end
CLuaLingShouPropertyFight.SetParamMAttMaxPow1 = function(newv)
	SetTmpParam_At(268, newv)
end
CLuaLingShouPropertyFight.SetParamMAttMaxInit = function(newv)
	SetTmpParam_At(269, newv)
end
CLuaLingShouPropertyFight.SetParamMSpeedPow1 = function(newv)
	SetTmpParam_At(270, newv)
end
CLuaLingShouPropertyFight.SetParamMSpeedInit = function(newv)
	SetTmpParam_At(271, newv)
end
CLuaLingShouPropertyFight.SetParamMDefPow1 = function(newv)
	SetTmpParam_At(272, newv)
end
CLuaLingShouPropertyFight.SetParamMDefInit = function(newv)
	SetTmpParam_At(273, newv)
end
CLuaLingShouPropertyFight.SetParamMHitPow1 = function(newv)
	SetTmpParam_At(274, newv)
end
CLuaLingShouPropertyFight.SetParamMHitInit = function(newv)
	SetTmpParam_At(275, newv)
end
CLuaLingShouPropertyFight.SetParamMMissPow1 = function(newv)
	SetTmpParam_At(276, newv)
end
CLuaLingShouPropertyFight.SetParamMMissInit = function(newv)
	SetTmpParam_At(277, newv)
end
CLuaLingShouPropertyFight.SetParamMFatalPow1 = function(newv)
	SetTmpParam_At(278, newv)
end
CLuaLingShouPropertyFight.SetParamMFatalInit = function(newv)
	SetTmpParam_At(279, newv)
end
CLuaLingShouPropertyFight.SetParamBlockDamagePow1 = function(newv)
	SetTmpParam_At(280, newv)
end
CLuaLingShouPropertyFight.SetParamBlockDamageInit = function(newv)
	SetTmpParam_At(281, newv)
end
CLuaLingShouPropertyFight.SetParamRunSpeed = function(newv)
	SetTmpParam_At(282, newv)
end
CLuaLingShouPropertyFight.SetParamHpRecoverPow1 = function(newv)
	SetTmpParam_At(283, newv)
end
CLuaLingShouPropertyFight.SetParamHpRecoverInit = function(newv)
	SetTmpParam_At(284, newv)
end
CLuaLingShouPropertyFight.SetParamAntiBlockDamagePow1 = function(newv)
	SetTmpParam_At(285, newv)
end
CLuaLingShouPropertyFight.SetParamAntiBlockDamageInit = function(newv)
	SetTmpParam_At(286, newv)
end
CLuaLingShouPropertyFight.SetParamBBMax = function(newv)
	SetTmpParam_At(287, newv)
end
CLuaLingShouPropertyFight.SetParamAdjBBMax = function(newv)
	SetTmpParam_At(288, newv)
end
CLuaLingShouPropertyFight.RefreshParamAntiBreak = function()
	local AdjAntiBreak = GetTmpParam_At(290)

	local newv = AdjAntiBreak
	SetTmpParam_At(289, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiBreak = function(newv)
	SetTmpParam_At(290, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiBreak()
end
CLuaLingShouPropertyFight.RefreshParamAntiPushPull = function()
	local AdjAntiPushPull = GetTmpParam_At(292)

	local newv = AdjAntiPushPull
	SetTmpParam_At(291, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiPushPull = function(newv)
	SetTmpParam_At(292, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiPushPull()
end
CLuaLingShouPropertyFight.RefreshParamAntiPetrify = function()
	local AdjAntiPetrify = GetTmpParam_At(294)
	local MulAntiPetrify = GetTmpParam_At(295)

	local newv = AdjAntiPetrify*(1+MulAntiPetrify)
	SetTmpParam_At(293, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiPetrify = function(newv)
	SetTmpParam_At(294, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiPetrify()
end
CLuaLingShouPropertyFight.SetParamMulAntiPetrify = function(newv)
	SetTmpParam_At(295, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiPetrify()
end
CLuaLingShouPropertyFight.RefreshParamAntiTie = function()
	local AdjAntiTie = GetTmpParam_At(297)
	local MulAntiTie = GetTmpParam_At(298)

	local newv = AdjAntiTie*(1+MulAntiTie)
	SetTmpParam_At(296, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiTie = function(newv)
	SetTmpParam_At(297, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiTie()
end
CLuaLingShouPropertyFight.SetParamMulAntiTie = function(newv)
	SetTmpParam_At(298, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiTie()
end
CLuaLingShouPropertyFight.RefreshParamEnhancePetrify = function()
	local AdjEnhancePetrify = GetTmpParam_At(300)
	local MulEnhancePetrify = GetTmpParam_At(301)

	local newv = AdjEnhancePetrify*(1+MulEnhancePetrify)
	SetTmpParam_At(299, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhancePetrify = function(newv)
	SetTmpParam_At(300, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhancePetrify()
end
CLuaLingShouPropertyFight.SetParamMulEnhancePetrify = function(newv)
	SetTmpParam_At(301, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhancePetrify()
end
CLuaLingShouPropertyFight.RefreshParamEnhanceTie = function()
	local AdjEnhanceTie = GetTmpParam_At(303)
	local MulEnhanceTie = GetTmpParam_At(304)

	local newv = AdjEnhanceTie*(1+MulEnhanceTie)
	SetTmpParam_At(302, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceTie = function(newv)
	SetTmpParam_At(303, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceTie()
end
CLuaLingShouPropertyFight.SetParamMulEnhanceTie = function(newv)
	SetTmpParam_At(304, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceTie()
end
CLuaLingShouPropertyFight.SetParamTieProbability = function(newv)
	SetTmpParam_At(305, newv)
end
CLuaLingShouPropertyFight.RefreshParamStrZizhi = function()
	local InitStrZizhi = GetTmpParam_At(311)
	local Xiuwei = GetTmpParam_At(317)
	local Wuxing = GetTmpParam_At(316)

	local newv = InitStrZizhi*(1+(Wuxing/100+max(0,Wuxing-2)/100+max(0,Wuxing-3)/100+max(0,Wuxing-4)/100+max(0,Wuxing-5)/100+max(0,Wuxing-6)/100+max(0,Wuxing-7)/100+max(0,Wuxing-8)/100+max(0,Wuxing-9)/100)+Xiuwei/200)
	SetTmpParam_At(306, newv)
end
CLuaLingShouPropertyFight.RefreshParamCorZizhi = function()
	local InitCorZizhi = GetTmpParam_At(312)
	local Xiuwei = GetTmpParam_At(317)
	local Wuxing = GetTmpParam_At(316)

	local newv = InitCorZizhi*(1+(Wuxing/100+max(0,Wuxing-2)/100+max(0,Wuxing-3)/100+max(0,Wuxing-4)/100+max(0,Wuxing-5)/100+max(0,Wuxing-6)/100+max(0,Wuxing-7)/100+max(0,Wuxing-8)/100+max(0,Wuxing-9)/100)+Xiuwei/200)
	SetTmpParam_At(307, newv)
end
CLuaLingShouPropertyFight.RefreshParamStaZizhi = function()
	local InitStaZizhi = GetTmpParam_At(313)
	local Xiuwei = GetTmpParam_At(317)
	local Wuxing = GetTmpParam_At(316)

	local newv = InitStaZizhi*(1+(Wuxing/100+max(0,Wuxing-2)/100+max(0,Wuxing-3)/100+max(0,Wuxing-4)/100+max(0,Wuxing-5)/100+max(0,Wuxing-6)/100+max(0,Wuxing-7)/100+max(0,Wuxing-8)/100+max(0,Wuxing-9)/100)+Xiuwei/200)
	SetTmpParam_At(308, newv)
end
CLuaLingShouPropertyFight.RefreshParamAgiZizhi = function()
	local InitAgiZizhi = GetTmpParam_At(314)
	local Xiuwei = GetTmpParam_At(317)
	local Wuxing = GetTmpParam_At(316)

	local newv = InitAgiZizhi*(1+(Wuxing/100+max(0,Wuxing-2)/100+max(0,Wuxing-3)/100+max(0,Wuxing-4)/100+max(0,Wuxing-5)/100+max(0,Wuxing-6)/100+max(0,Wuxing-7)/100+max(0,Wuxing-8)/100+max(0,Wuxing-9)/100)+Xiuwei/200)
	SetTmpParam_At(309, newv)
end
CLuaLingShouPropertyFight.RefreshParamIntZizhi = function()
	local InitIntZizhi = GetTmpParam_At(315)
	local Xiuwei = GetTmpParam_At(317)
	local Wuxing = GetTmpParam_At(316)

	local newv = InitIntZizhi*(1+(Wuxing/100+max(0,Wuxing-2)/100+max(0,Wuxing-3)/100+max(0,Wuxing-4)/100+max(0,Wuxing-5)/100+max(0,Wuxing-6)/100+max(0,Wuxing-7)/100+max(0,Wuxing-8)/100+max(0,Wuxing-9)/100)+Xiuwei/200)
	SetTmpParam_At(310, newv)
end
CLuaLingShouPropertyFight.SetParamInitStrZizhi = function(newv)
	SetTmpParam_At(311, newv)

	CLuaLingShouPropertyFight.RefreshParamStrZizhi()

	CLuaLingShouPropertyFight.RefreshParampDef()

	CLuaLingShouPropertyFight.RefreshParampAttMin()

	CLuaLingShouPropertyFight.RefreshParampAttMax()
end
CLuaLingShouPropertyFight.SetParamInitCorZizhi = function(newv)
	SetTmpParam_At(312, newv)

	CLuaLingShouPropertyFight.RefreshParamCorZizhi()

	CLuaLingShouPropertyFight.RefreshParamHpFull()

	CLuaLingShouPropertyFight.RefreshParamHpRecover()
end
CLuaLingShouPropertyFight.SetParamInitStaZizhi = function(newv)
	SetTmpParam_At(313, newv)

	CLuaLingShouPropertyFight.RefreshParamStaZizhi()

	CLuaLingShouPropertyFight.RefreshParammDef()

	CLuaLingShouPropertyFight.RefreshParampDef()
end
CLuaLingShouPropertyFight.SetParamInitAgiZizhi = function(newv)
	SetTmpParam_At(314, newv)

	CLuaLingShouPropertyFight.RefreshParamAgiZizhi()

	CLuaLingShouPropertyFight.RefreshParammMiss()

	CLuaLingShouPropertyFight.RefreshParampHit()

	CLuaLingShouPropertyFight.RefreshParammFatalDamage()

	CLuaLingShouPropertyFight.RefreshParammFatal()

	CLuaLingShouPropertyFight.RefreshParampFatal()

	CLuaLingShouPropertyFight.RefreshParammHit()

	CLuaLingShouPropertyFight.RefreshParampFatalDamage()

	CLuaLingShouPropertyFight.RefreshParampMiss()
end
CLuaLingShouPropertyFight.SetParamInitIntZizhi = function(newv)
	SetTmpParam_At(315, newv)

	CLuaLingShouPropertyFight.RefreshParamIntZizhi()

	CLuaLingShouPropertyFight.RefreshParammAttMax()

	CLuaLingShouPropertyFight.RefreshParammAttMin()

	CLuaLingShouPropertyFight.RefreshParammDef()
end
CLuaLingShouPropertyFight.SetParamWuxing = function(newv)
	SetTmpParam_At(316, newv)

	CLuaLingShouPropertyFight.RefreshParamWuxingImprove()

	CLuaLingShouPropertyFight.RefreshParamIntZizhi()

	CLuaLingShouPropertyFight.RefreshParamAgiZizhi()

	CLuaLingShouPropertyFight.RefreshParamCorZizhi()

	CLuaLingShouPropertyFight.RefreshParamStrZizhi()

	CLuaLingShouPropertyFight.RefreshParamStaZizhi()

	CLuaLingShouPropertyFight.RefreshParammMiss()

	CLuaLingShouPropertyFight.RefreshParampHit()

	CLuaLingShouPropertyFight.RefreshParammFatalDamage()

	CLuaLingShouPropertyFight.RefreshParammFatal()

	CLuaLingShouPropertyFight.RefreshParampFatal()

	CLuaLingShouPropertyFight.RefreshParammHit()

	CLuaLingShouPropertyFight.RefreshParampFatalDamage()

	CLuaLingShouPropertyFight.RefreshParampMiss()

	CLuaLingShouPropertyFight.RefreshParamHpFull()

	CLuaLingShouPropertyFight.RefreshParammAttMax()

	CLuaLingShouPropertyFight.RefreshParammAttMin()

	CLuaLingShouPropertyFight.RefreshParammDef()

	CLuaLingShouPropertyFight.RefreshParampDef()

	CLuaLingShouPropertyFight.RefreshParampAttMin()

	CLuaLingShouPropertyFight.RefreshParampAttMax()

	CLuaLingShouPropertyFight.RefreshParamHpRecover()
end
CLuaLingShouPropertyFight.SetParamXiuwei = function(newv)
	SetTmpParam_At(317, newv)

	CLuaLingShouPropertyFight.RefreshParamSta()

	CLuaLingShouPropertyFight.RefreshParamWuxingImprove()

	CLuaLingShouPropertyFight.RefreshParamIntZizhi()

	CLuaLingShouPropertyFight.RefreshParamAgiZizhi()

	CLuaLingShouPropertyFight.RefreshParamCorZizhi()

	CLuaLingShouPropertyFight.RefreshParamStrZizhi()

	CLuaLingShouPropertyFight.RefreshParamStaZizhi()

	CLuaLingShouPropertyFight.RefreshParamXiuweiImprove()

	CLuaLingShouPropertyFight.RefreshParamAgi()

	CLuaLingShouPropertyFight.RefreshParamCor()

	CLuaLingShouPropertyFight.RefreshParamInt()

	CLuaLingShouPropertyFight.RefreshParamStr()

	CLuaLingShouPropertyFight.RefreshParammMiss()

	CLuaLingShouPropertyFight.RefreshParampHit()

	CLuaLingShouPropertyFight.RefreshParammFatalDamage()

	CLuaLingShouPropertyFight.RefreshParammFatal()

	CLuaLingShouPropertyFight.RefreshParampFatal()

	CLuaLingShouPropertyFight.RefreshParammHit()

	CLuaLingShouPropertyFight.RefreshParampFatalDamage()

	CLuaLingShouPropertyFight.RefreshParampMiss()

	CLuaLingShouPropertyFight.RefreshParamHpFull()

	CLuaLingShouPropertyFight.RefreshParammAttMax()

	CLuaLingShouPropertyFight.RefreshParammAttMin()

	CLuaLingShouPropertyFight.RefreshParammDef()

	CLuaLingShouPropertyFight.RefreshParampDef()

	CLuaLingShouPropertyFight.RefreshParampAttMin()

	CLuaLingShouPropertyFight.RefreshParampAttMax()

	CLuaLingShouPropertyFight.RefreshParamHpRecover()
end
CLuaLingShouPropertyFight.SetParamSkillNum = function(newv)
	SetTmpParam_At(318, newv)

	CLuaLingShouPropertyFight.RefreshParamHpFull()

	CLuaLingShouPropertyFight.RefreshParammAttMax()

	CLuaLingShouPropertyFight.RefreshParammAttMin()

	CLuaLingShouPropertyFight.RefreshParammDef()

	CLuaLingShouPropertyFight.RefreshParampDef()

	CLuaLingShouPropertyFight.RefreshParampAttMin()

	CLuaLingShouPropertyFight.RefreshParampAttMax()

	CLuaLingShouPropertyFight.RefreshParamHpRecover()
end
CLuaLingShouPropertyFight.RefreshParamPType = function()
	local LingShouType = GetTmpParam_At(350)

	local newv = 1-min(1,abs(LingShouType-1))
	SetTmpParam_At(319, newv)
end
CLuaLingShouPropertyFight.RefreshParamMType = function()
	local LingShouType = GetTmpParam_At(350)

	local newv = 1-min(1,abs(LingShouType-2))
	SetTmpParam_At(320, newv)
end
CLuaLingShouPropertyFight.RefreshParamPHType = function()
	local LingShouType = GetTmpParam_At(350)

	local newv = 1-min(1,abs(LingShouType-3))
	SetTmpParam_At(321, newv)
end
CLuaLingShouPropertyFight.SetParamGrowFactor = function(newv)
	SetTmpParam_At(322, newv)

	CLuaLingShouPropertyFight.RefreshParammMiss()

	CLuaLingShouPropertyFight.RefreshParampHit()

	CLuaLingShouPropertyFight.RefreshParammFatalDamage()

	CLuaLingShouPropertyFight.RefreshParammFatal()

	CLuaLingShouPropertyFight.RefreshParampFatal()

	CLuaLingShouPropertyFight.RefreshParammHit()

	CLuaLingShouPropertyFight.RefreshParampFatalDamage()

	CLuaLingShouPropertyFight.RefreshParampMiss()

	CLuaLingShouPropertyFight.RefreshParamHpFull()

	CLuaLingShouPropertyFight.RefreshParammAttMax()

	CLuaLingShouPropertyFight.RefreshParammAttMin()

	CLuaLingShouPropertyFight.RefreshParammDef()

	CLuaLingShouPropertyFight.RefreshParampDef()

	CLuaLingShouPropertyFight.RefreshParampAttMin()

	CLuaLingShouPropertyFight.RefreshParampAttMax()

	CLuaLingShouPropertyFight.RefreshParamHpRecover()
end
CLuaLingShouPropertyFight.RefreshParamWuxingImprove = function()
	local Xiuwei = GetTmpParam_At(317)
	local Wuxing = GetTmpParam_At(316)

	local newv = (Wuxing/100+max(0,Wuxing-2)/100+max(0,Wuxing-3)/100+max(0,Wuxing-4)/100+max(0,Wuxing-5)/100+max(0,Wuxing-6)/100+max(0,Wuxing-7)/100+max(0,Wuxing-8)/100+max(0,Wuxing-9)/100)+Xiuwei/200
	SetTmpParam_At(323, newv)
end
CLuaLingShouPropertyFight.RefreshParamXiuweiImprove = function()
	local Xiuwei = GetTmpParam_At(317)

	local newv = Xiuwei/200
	SetTmpParam_At(324, newv)
end
CLuaLingShouPropertyFight.RefreshParamEnhanceBeHeal = function()
	local AdjEnhanceBeHeal = GetTmpParam_At(326)

	local newv = AdjEnhanceBeHeal
	SetTmpParam_At(325, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceBeHeal = function(newv)
	SetTmpParam_At(326, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceBeHeal()
end
CLuaLingShouPropertyFight.SetParamMulEnhanceBeHeal = function(newv)
	SetTmpParam_At(327, newv)
end
CLuaLingShouPropertyFight.SetParamLookUpCor = function(newv)
	SetTmpParam_At(328, newv)
end
CLuaLingShouPropertyFight.SetParamLookUpSta = function(newv)
	SetTmpParam_At(329, newv)
end
CLuaLingShouPropertyFight.SetParamLookUpStr = function(newv)
	SetTmpParam_At(330, newv)
end
CLuaLingShouPropertyFight.SetParamLookUpInt = function(newv)
	SetTmpParam_At(331, newv)
end
CLuaLingShouPropertyFight.SetParamLookUpAgi = function(newv)
	SetTmpParam_At(332, newv)
end
CLuaLingShouPropertyFight.SetParamLookUpHpFull = function(newv)
	SetTmpParam_At(333, newv)
end
CLuaLingShouPropertyFight.SetParamLookUpMpFull = function(newv)
	SetTmpParam_At(334, newv)
end
CLuaLingShouPropertyFight.SetParamLookUppAttMin = function(newv)
	SetTmpParam_At(335, newv)
end
CLuaLingShouPropertyFight.SetParamLookUppAttMax = function(newv)
	SetTmpParam_At(336, newv)
end
CLuaLingShouPropertyFight.SetParamLookUppHit = function(newv)
	SetTmpParam_At(337, newv)
end
CLuaLingShouPropertyFight.SetParamLookUppMiss = function(newv)
	SetTmpParam_At(338, newv)
end
CLuaLingShouPropertyFight.SetParamLookUppDef = function(newv)
	SetTmpParam_At(339, newv)
end
CLuaLingShouPropertyFight.SetParamLookUpmAttMin = function(newv)
	SetTmpParam_At(340, newv)
end
CLuaLingShouPropertyFight.SetParamLookUpmAttMax = function(newv)
	SetTmpParam_At(341, newv)
end
CLuaLingShouPropertyFight.SetParamLookUpmHit = function(newv)
	SetTmpParam_At(342, newv)
end
CLuaLingShouPropertyFight.SetParamLookUpmMiss = function(newv)
	SetTmpParam_At(343, newv)
end
CLuaLingShouPropertyFight.SetParamLookUpmDef = function(newv)
	SetTmpParam_At(344, newv)
end
CLuaLingShouPropertyFight.SetParamAdjHpFull2 = function(newv)
	SetTmpParam_At(345, newv)
end
CLuaLingShouPropertyFight.SetParamAdjpAttMin2 = function(newv)
	SetTmpParam_At(346, newv)
end
CLuaLingShouPropertyFight.SetParamAdjpAttMax2 = function(newv)
	SetTmpParam_At(347, newv)
end
CLuaLingShouPropertyFight.SetParamAdjmAttMin2 = function(newv)
	SetTmpParam_At(348, newv)
end
CLuaLingShouPropertyFight.SetParamAdjmAttMax2 = function(newv)
	SetTmpParam_At(349, newv)
end
CLuaLingShouPropertyFight.SetParamLingShouType = function(newv)
	SetTmpParam_At(350, newv)

	CLuaLingShouPropertyFight.RefreshParamMHType()

	CLuaLingShouPropertyFight.RefreshParamPAType()

	CLuaLingShouPropertyFight.RefreshParamMAType()

	CLuaLingShouPropertyFight.RefreshParamPHType()

	CLuaLingShouPropertyFight.RefreshParamMType()

	CLuaLingShouPropertyFight.RefreshParamPType()

	CLuaLingShouPropertyFight.RefreshParamAgi()

	CLuaLingShouPropertyFight.RefreshParamCor()

	CLuaLingShouPropertyFight.RefreshParamInt()

	CLuaLingShouPropertyFight.RefreshParamStr()

	CLuaLingShouPropertyFight.RefreshParammMiss()

	CLuaLingShouPropertyFight.RefreshParampHit()

	CLuaLingShouPropertyFight.RefreshParammFatalDamage()

	CLuaLingShouPropertyFight.RefreshParammFatal()

	CLuaLingShouPropertyFight.RefreshParampFatal()

	CLuaLingShouPropertyFight.RefreshParammHit()

	CLuaLingShouPropertyFight.RefreshParampFatalDamage()

	CLuaLingShouPropertyFight.RefreshParampMiss()

	CLuaLingShouPropertyFight.RefreshParamHpFull()

	CLuaLingShouPropertyFight.RefreshParammAttMax()

	CLuaLingShouPropertyFight.RefreshParammAttMin()

	CLuaLingShouPropertyFight.RefreshParammDef()

	CLuaLingShouPropertyFight.RefreshParampDef()

	CLuaLingShouPropertyFight.RefreshParampAttMin()

	CLuaLingShouPropertyFight.RefreshParampAttMax()

	CLuaLingShouPropertyFight.RefreshParamHpRecover()
end
CLuaLingShouPropertyFight.RefreshParamMHType = function()
	local LingShouType = GetTmpParam_At(350)

	local newv = 1-min(1,abs(LingShouType-4))
	SetTmpParam_At(351, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceLingShou = function(newv)
	SetTmpParam_At(352, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceLingShouMul = function(newv)
	SetTmpParam_At(353, newv)
end
CLuaLingShouPropertyFight.RefreshParamAntiAoe = function()
	local AdjAntiAoe = GetTmpParam_At(355)

	local newv = AdjAntiAoe
	SetTmpParam_At(354, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiAoe = function(newv)
	SetTmpParam_At(355, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiAoe()
end
CLuaLingShouPropertyFight.RefreshParamAntiBlind = function()
	local AdjAntiBlind = GetTmpParam_At(357)
	local MulAntiBlind = GetTmpParam_At(358)

	local newv = AdjAntiBlind*(1+MulAntiBlind)
	SetTmpParam_At(356, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiBlind = function(newv)
	SetTmpParam_At(357, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiBlind()
end
CLuaLingShouPropertyFight.SetParamMulAntiBlind = function(newv)
	SetTmpParam_At(358, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiBlind()
end
CLuaLingShouPropertyFight.RefreshParamAntiDecelerate = function()
	local AdjAntiDecelerate = GetTmpParam_At(360)
	local MulAntiDecelerate = GetTmpParam_At(361)

	local newv = AdjAntiDecelerate*(1+MulAntiDecelerate)
	SetTmpParam_At(359, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiDecelerate = function(newv)
	SetTmpParam_At(360, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiDecelerate()
end
CLuaLingShouPropertyFight.SetParamMulAntiDecelerate = function(newv)
	SetTmpParam_At(361, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiDecelerate()
end
CLuaLingShouPropertyFight.SetParamMinGrade = function(newv)
	SetTmpParam_At(362, newv)
end
CLuaLingShouPropertyFight.SetParamCharacterCor = function(newv)
	SetTmpParam_At(363, newv)
end
CLuaLingShouPropertyFight.SetParamCharacterSta = function(newv)
	SetTmpParam_At(364, newv)
end
CLuaLingShouPropertyFight.SetParamCharacterStr = function(newv)
	SetTmpParam_At(365, newv)
end
CLuaLingShouPropertyFight.SetParamCharacterInt = function(newv)
	SetTmpParam_At(366, newv)
end
CLuaLingShouPropertyFight.SetParamCharacterAgi = function(newv)
	SetTmpParam_At(367, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceDaoKe = function(newv)
	SetTmpParam_At(368, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceDaoKeMul = function(newv)
	SetTmpParam_At(369, newv)
end
CLuaLingShouPropertyFight.SetParamZuoQiSpeed = function(newv)
	SetTmpParam_At(370, newv)
end
CLuaLingShouPropertyFight.RefreshParamEnhanceBianHu = function()
	local AdjEnhanceBianHu = GetTmpParam_At(372)
	local MulEnhanceBianHu = GetTmpParam_At(373)

	local newv = AdjEnhanceBianHu*(1+MulEnhanceBianHu)
	SetTmpParam_At(371, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceBianHu = function(newv)
	SetTmpParam_At(372, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceBianHu()
end
CLuaLingShouPropertyFight.SetParamMulEnhanceBianHu = function(newv)
	SetTmpParam_At(373, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceBianHu()
end
CLuaLingShouPropertyFight.RefreshParamAntiBianHu = function()
	local AdjAntiBianHu = GetTmpParam_At(375)
	local MulAntiBianHu = GetTmpParam_At(376)

	local newv = AdjAntiBianHu*(1+MulAntiBianHu)
	SetTmpParam_At(374, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiBianHu = function(newv)
	SetTmpParam_At(375, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiBianHu()
end
CLuaLingShouPropertyFight.SetParamMulAntiBianHu = function(newv)
	SetTmpParam_At(376, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiBianHu()
end
CLuaLingShouPropertyFight.SetParamEnhanceXiaKe = function(newv)
	SetTmpParam_At(377, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceXiaKeMul = function(newv)
	SetTmpParam_At(378, newv)
end
CLuaLingShouPropertyFight.SetParamTieTimeChange = function(newv)
	SetTmpParam_At(379, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceYanShi = function(newv)
	SetTmpParam_At(380, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceYanShiMul = function(newv)
	SetTmpParam_At(381, newv)
end
CLuaLingShouPropertyFight.RefreshParamPAType = function()
	local LingShouType = GetTmpParam_At(350)

	local newv = 1-min(1,abs(LingShouType-5))
	SetTmpParam_At(382, newv)
end
CLuaLingShouPropertyFight.RefreshParamMAType = function()
	local LingShouType = GetTmpParam_At(350)

	local newv = 1-min(1,abs(LingShouType-6))
	SetTmpParam_At(383, newv)
end
CLuaLingShouPropertyFight.SetParamlifetimeNoReduceRate = function(newv)
	SetTmpParam_At(384, newv)
end
CLuaLingShouPropertyFight.SetParamAddLSHpDurgImprove = function(newv)
	SetTmpParam_At(385, newv)
end
CLuaLingShouPropertyFight.SetParamAddHpDurgImprove = function(newv)
	SetTmpParam_At(386, newv)
end
CLuaLingShouPropertyFight.SetParamAddMpDurgImprove = function(newv)
	SetTmpParam_At(387, newv)
end
CLuaLingShouPropertyFight.SetParamFireHurtReduce = function(newv)
	SetTmpParam_At(388, newv)
end
CLuaLingShouPropertyFight.SetParamThunderHurtReduce = function(newv)
	SetTmpParam_At(389, newv)
end
CLuaLingShouPropertyFight.SetParamIceHurtReduce = function(newv)
	SetTmpParam_At(390, newv)
end
CLuaLingShouPropertyFight.SetParamPoisonHurtReduce = function(newv)
	SetTmpParam_At(391, newv)
end
CLuaLingShouPropertyFight.SetParamWindHurtReduce = function(newv)
	SetTmpParam_At(392, newv)
end
CLuaLingShouPropertyFight.SetParamLightHurtReduce = function(newv)
	SetTmpParam_At(393, newv)
end
CLuaLingShouPropertyFight.SetParamIllusionHurtReduce = function(newv)
	SetTmpParam_At(394, newv)
end
CLuaLingShouPropertyFight.SetParamFakepDef = function(newv)
	SetTmpParam_At(395, newv)
end
CLuaLingShouPropertyFight.SetParamFakemDef = function(newv)
	SetTmpParam_At(396, newv)
end
CLuaLingShouPropertyFight.RefreshParamEnhanceWater = function()
	local AdjEnhanceWater = GetTmpParam_At(398)

	local newv = AdjEnhanceWater
	SetTmpParam_At(397, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceWater = function(newv)
	SetTmpParam_At(398, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceWater()
end
CLuaLingShouPropertyFight.SetParamMulEnhanceWater = function(newv)
	SetTmpParam_At(399, newv)
end
CLuaLingShouPropertyFight.RefreshParamAntiWater = function()
	local AdjAntiWater = GetTmpParam_At(401)
	local MulAntiWater = GetTmpParam_At(402)

	local newv = AdjAntiWater*(1+MulAntiWater)
	SetTmpParam_At(400, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiWater = function(newv)
	SetTmpParam_At(401, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiWater()
end
CLuaLingShouPropertyFight.SetParamMulAntiWater = function(newv)
	SetTmpParam_At(402, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiWater()
end
CLuaLingShouPropertyFight.SetParamWaterHurtReduce = function(newv)
	SetTmpParam_At(403, newv)
end
CLuaLingShouPropertyFight.RefreshParamIgnoreAntiWater = function()
	local AdjIgnoreAntiWater = GetTmpParam_At(431)
	local IgnoreAntiWaterLSMul = GetTmpParam_At(432)

	local newv = AdjIgnoreAntiWater*(1+IgnoreAntiWaterLSMul)
	SetTmpParam_At(404, newv)
end
CLuaLingShouPropertyFight.RefreshParamAntiFreeze = function()
	local AdjAntiFreeze = GetTmpParam_At(406)
	local MulAntiFreeze = GetTmpParam_At(407)

	local newv = AdjAntiFreeze*(1+MulAntiFreeze)
	SetTmpParam_At(405, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiFreeze = function(newv)
	SetTmpParam_At(406, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiFreeze()
end
CLuaLingShouPropertyFight.SetParamMulAntiFreeze = function(newv)
	SetTmpParam_At(407, newv)

	CLuaLingShouPropertyFight.RefreshParamAntiFreeze()
end
CLuaLingShouPropertyFight.RefreshParamEnhanceFreeze = function()
	local AdjEnhanceFreeze = GetTmpParam_At(409)
	local MulEnhanceFreeze = GetTmpParam_At(410)

	local newv = AdjEnhanceFreeze*(1+MulEnhanceFreeze)
	SetTmpParam_At(408, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceFreeze = function(newv)
	SetTmpParam_At(409, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceFreeze()
end
CLuaLingShouPropertyFight.SetParamMulEnhanceFreeze = function(newv)
	SetTmpParam_At(410, newv)

	CLuaLingShouPropertyFight.RefreshParamEnhanceFreeze()
end
CLuaLingShouPropertyFight.SetParamEnhanceHuaHun = function(newv)
	SetTmpParam_At(411, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceHuaHunMul = function(newv)
	SetTmpParam_At(412, newv)
end
CLuaLingShouPropertyFight.SetParamTrueSightProb = function(newv)
	SetTmpParam_At(413, newv)
end
CLuaLingShouPropertyFight.SetParamLSBBGrade = function(newv)
	SetTmpParam_At(414, newv)

	CLuaLingShouPropertyFight.RefreshParamLSBBAdjHp()

	CLuaLingShouPropertyFight.RefreshParamHpFull()

	CLuaLingShouPropertyFight.RefreshParamHpRecover()
end
CLuaLingShouPropertyFight.SetParamLSBBQuality = function(newv)
	SetTmpParam_At(415, newv)

	CLuaLingShouPropertyFight.RefreshParamLSBBAdjHp()

	CLuaLingShouPropertyFight.RefreshParamHpFull()

	CLuaLingShouPropertyFight.RefreshParamHpRecover()
end
CLuaLingShouPropertyFight.RefreshParamLSBBAdjHp = function()
	local LSBBGrade = GetTmpParam_At(414)
	local LSBBQuality = GetTmpParam_At(415)

	local newv = floor((power(1.032,LSBBGrade-10)+LSBBGrade*0.261-(LSBBGrade-150)*min(1,max(0,LSBBGrade-150)))*6.64*(0.5+min(1,max(0,LSBBQuality-1))*0.5+min(1,max(0,LSBBQuality-2))*0.5+min(1,max(0,LSBBQuality-3))*0.5+min(1,max(0,LSBBQuality-4))*2+min(1,max(0,LSBBQuality-5))*2)*min(1,max(0,LSBBGrade-1)))
	SetTmpParam_At(416, newv)
end
CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiFire = function(newv)
	SetTmpParam_At(417, newv)

	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiFire()
end
CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiThunder = function(newv)
	SetTmpParam_At(418, newv)

	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiThunder()
end
CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiIce = function(newv)
	SetTmpParam_At(419, newv)

	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiIce()
end
CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiPoison = function(newv)
	SetTmpParam_At(420, newv)

	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiPoison()
end
CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiWind = function(newv)
	SetTmpParam_At(421, newv)

	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiWind()
end
CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiLight = function(newv)
	SetTmpParam_At(422, newv)

	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiLight()
end
CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiIllusion = function(newv)
	SetTmpParam_At(423, newv)

	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiIllusion()
end
CLuaLingShouPropertyFight.SetParamIgnoreAntiFireLSMul = function(newv)
	SetTmpParam_At(424, newv)

	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiFire()
end
CLuaLingShouPropertyFight.SetParamIgnoreAntiThunderLSMul = function(newv)
	SetTmpParam_At(425, newv)

	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiThunder()
end
CLuaLingShouPropertyFight.SetParamIgnoreAntiIceLSMul = function(newv)
	SetTmpParam_At(426, newv)

	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiIce()
end
CLuaLingShouPropertyFight.SetParamIgnoreAntiPoisonLSMul = function(newv)
	SetTmpParam_At(427, newv)

	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiPoison()
end
CLuaLingShouPropertyFight.SetParamIgnoreAntiWindLSMul = function(newv)
	SetTmpParam_At(428, newv)

	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiWind()
end
CLuaLingShouPropertyFight.SetParamIgnoreAntiLightLSMul = function(newv)
	SetTmpParam_At(429, newv)

	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiLight()
end
CLuaLingShouPropertyFight.SetParamIgnoreAntiIllusionLSMul = function(newv)
	SetTmpParam_At(430, newv)

	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiIllusion()
end
CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiWater = function(newv)
	SetTmpParam_At(431, newv)

	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiWater()
end
CLuaLingShouPropertyFight.SetParamIgnoreAntiWaterLSMul = function(newv)
	SetTmpParam_At(432, newv)

	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiWater()
end
CLuaLingShouPropertyFight.SetParamMulSpeed2 = function(newv)
	SetTmpParam_At(433, newv)
end
CLuaLingShouPropertyFight.SetParamMulConsumeMp = function(newv)
	SetTmpParam_At(434, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAgi2 = function(newv)
	SetTmpParam_At(435, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiBianHu2 = function(newv)
	SetTmpParam_At(436, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiBind2 = function(newv)
	SetTmpParam_At(437, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiBlind2 = function(newv)
	SetTmpParam_At(438, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiBlock2 = function(newv)
	SetTmpParam_At(439, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiBlockDamage2 = function(newv)
	SetTmpParam_At(440, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiChaos2 = function(newv)
	SetTmpParam_At(441, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiDecelerate2 = function(newv)
	SetTmpParam_At(442, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiDizzy2 = function(newv)
	SetTmpParam_At(443, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiFire2 = function(newv)
	SetTmpParam_At(444, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiFreeze2 = function(newv)
	SetTmpParam_At(445, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiIce2 = function(newv)
	SetTmpParam_At(446, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiIllusion2 = function(newv)
	SetTmpParam_At(447, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiLight2 = function(newv)
	SetTmpParam_At(448, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntimFatal2 = function(newv)
	SetTmpParam_At(449, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntimFatalDamage2 = function(newv)
	SetTmpParam_At(450, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiPetrify2 = function(newv)
	SetTmpParam_At(451, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntipFatal2 = function(newv)
	SetTmpParam_At(452, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntipFatalDamage2 = function(newv)
	SetTmpParam_At(453, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiPoison2 = function(newv)
	SetTmpParam_At(454, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiSilence2 = function(newv)
	SetTmpParam_At(455, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiSleep2 = function(newv)
	SetTmpParam_At(456, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiThunder2 = function(newv)
	SetTmpParam_At(457, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiTie2 = function(newv)
	SetTmpParam_At(458, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiWater2 = function(newv)
	SetTmpParam_At(459, newv)
end
CLuaLingShouPropertyFight.SetParamAdjAntiWind2 = function(newv)
	SetTmpParam_At(460, newv)
end
CLuaLingShouPropertyFight.SetParamAdjBlock2 = function(newv)
	SetTmpParam_At(461, newv)
end
CLuaLingShouPropertyFight.SetParamAdjBlockDamage2 = function(newv)
	SetTmpParam_At(462, newv)
end
CLuaLingShouPropertyFight.SetParamAdjCor2 = function(newv)
	SetTmpParam_At(463, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceBianHu2 = function(newv)
	SetTmpParam_At(464, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceBind2 = function(newv)
	SetTmpParam_At(465, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceChaos2 = function(newv)
	SetTmpParam_At(466, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceDizzy2 = function(newv)
	SetTmpParam_At(467, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceFire2 = function(newv)
	SetTmpParam_At(468, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceFreeze2 = function(newv)
	SetTmpParam_At(469, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceIce2 = function(newv)
	SetTmpParam_At(470, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceIllusion2 = function(newv)
	SetTmpParam_At(471, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceLight2 = function(newv)
	SetTmpParam_At(472, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhancePetrify2 = function(newv)
	SetTmpParam_At(473, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhancePoison2 = function(newv)
	SetTmpParam_At(474, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceSilence2 = function(newv)
	SetTmpParam_At(475, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceSleep2 = function(newv)
	SetTmpParam_At(476, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceThunder2 = function(newv)
	SetTmpParam_At(477, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceTie2 = function(newv)
	SetTmpParam_At(478, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceWater2 = function(newv)
	SetTmpParam_At(479, newv)
end
CLuaLingShouPropertyFight.SetParamAdjEnhanceWind2 = function(newv)
	SetTmpParam_At(480, newv)
end
CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiFire2 = function(newv)
	SetTmpParam_At(481, newv)
end
CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiIce2 = function(newv)
	SetTmpParam_At(482, newv)
end
CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiIllusion2 = function(newv)
	SetTmpParam_At(483, newv)
end
CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiLight2 = function(newv)
	SetTmpParam_At(484, newv)
end
CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiPoison2 = function(newv)
	SetTmpParam_At(485, newv)
end
CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiThunder2 = function(newv)
	SetTmpParam_At(486, newv)
end
CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiWater2 = function(newv)
	SetTmpParam_At(487, newv)
end
CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiWind2 = function(newv)
	SetTmpParam_At(488, newv)
end
CLuaLingShouPropertyFight.SetParamAdjInt2 = function(newv)
	SetTmpParam_At(489, newv)
end
CLuaLingShouPropertyFight.SetParamAdjmDef2 = function(newv)
	SetTmpParam_At(490, newv)
end
CLuaLingShouPropertyFight.SetParamAdjmFatal2 = function(newv)
	SetTmpParam_At(491, newv)
end
CLuaLingShouPropertyFight.SetParamAdjmFatalDamage2 = function(newv)
	SetTmpParam_At(492, newv)
end
CLuaLingShouPropertyFight.SetParamAdjmHit2 = function(newv)
	SetTmpParam_At(493, newv)
end
CLuaLingShouPropertyFight.SetParamAdjmHurt2 = function(newv)
	SetTmpParam_At(494, newv)
end
CLuaLingShouPropertyFight.SetParamAdjmMiss2 = function(newv)
	SetTmpParam_At(495, newv)
end
CLuaLingShouPropertyFight.SetParamAdjmSpeed2 = function(newv)
	SetTmpParam_At(496, newv)
end
CLuaLingShouPropertyFight.SetParamAdjpDef2 = function(newv)
	SetTmpParam_At(497, newv)
end
CLuaLingShouPropertyFight.SetParamAdjpFatal2 = function(newv)
	SetTmpParam_At(498, newv)
end
CLuaLingShouPropertyFight.SetParamAdjpFatalDamage2 = function(newv)
	SetTmpParam_At(499, newv)
end
CLuaLingShouPropertyFight.SetParamAdjpHit2 = function(newv)
	SetTmpParam_At(500, newv)
end
CLuaLingShouPropertyFight.SetParamAdjpHurt2 = function(newv)
	SetTmpParam_At(501, newv)
end
CLuaLingShouPropertyFight.SetParamAdjpMiss2 = function(newv)
	SetTmpParam_At(502, newv)
end
CLuaLingShouPropertyFight.SetParamAdjpSpeed2 = function(newv)
	SetTmpParam_At(503, newv)
end
CLuaLingShouPropertyFight.SetParamAdjStr2 = function(newv)
	SetTmpParam_At(504, newv)
end
CLuaLingShouPropertyFight.SetParamLSpAttMin = function(newv)
	SetTmpParam_At(505, newv)
end
CLuaLingShouPropertyFight.SetParamLSpAttMax = function(newv)
	SetTmpParam_At(506, newv)
end
CLuaLingShouPropertyFight.SetParamLSmAttMin = function(newv)
	SetTmpParam_At(507, newv)
end
CLuaLingShouPropertyFight.SetParamLSmAttMax = function(newv)
	SetTmpParam_At(508, newv)
end
CLuaLingShouPropertyFight.SetParamLSpFatal = function(newv)
	SetTmpParam_At(509, newv)
end
CLuaLingShouPropertyFight.SetParamLSpFatalDamage = function(newv)
	SetTmpParam_At(510, newv)
end
CLuaLingShouPropertyFight.SetParamLSmFatal = function(newv)
	SetTmpParam_At(511, newv)
end
CLuaLingShouPropertyFight.SetParamLSmFatalDamage = function(newv)
	SetTmpParam_At(512, newv)
end
CLuaLingShouPropertyFight.SetParamLSAntiBlock = function(newv)
	SetTmpParam_At(513, newv)
end
CLuaLingShouPropertyFight.SetParamLSAntiBlockDamage = function(newv)
	SetTmpParam_At(514, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiFire = function(newv)
	SetTmpParam_At(515, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiThunder = function(newv)
	SetTmpParam_At(516, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiIce = function(newv)
	SetTmpParam_At(517, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiPoison = function(newv)
	SetTmpParam_At(518, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiWind = function(newv)
	SetTmpParam_At(519, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiLight = function(newv)
	SetTmpParam_At(520, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiIllusion = function(newv)
	SetTmpParam_At(521, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiWater = function(newv)
	SetTmpParam_At(522, newv)
end
CLuaLingShouPropertyFight.SetParamLSpAttMin_adj = function(newv)
	SetTmpParam_At(523, newv)
end
CLuaLingShouPropertyFight.SetParamLSpAttMax_adj = function(newv)
	SetTmpParam_At(524, newv)
end
CLuaLingShouPropertyFight.SetParamLSmAttMin_adj = function(newv)
	SetTmpParam_At(525, newv)
end
CLuaLingShouPropertyFight.SetParamLSmAttMax_adj = function(newv)
	SetTmpParam_At(526, newv)
end
CLuaLingShouPropertyFight.SetParamLSpFatal_adj = function(newv)
	SetTmpParam_At(527, newv)
end
CLuaLingShouPropertyFight.SetParamLSpFatalDamage_adj = function(newv)
	SetTmpParam_At(528, newv)
end
CLuaLingShouPropertyFight.SetParamLSmFatal_adj = function(newv)
	SetTmpParam_At(529, newv)
end
CLuaLingShouPropertyFight.SetParamLSmFatalDamage_adj = function(newv)
	SetTmpParam_At(530, newv)
end
CLuaLingShouPropertyFight.SetParamLSAntiBlock_adj = function(newv)
	SetTmpParam_At(531, newv)
end
CLuaLingShouPropertyFight.SetParamLSAntiBlockDamage_adj = function(newv)
	SetTmpParam_At(532, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiFire_adj = function(newv)
	SetTmpParam_At(533, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiThunder_adj = function(newv)
	SetTmpParam_At(534, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiIce_adj = function(newv)
	SetTmpParam_At(535, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiPoison_adj = function(newv)
	SetTmpParam_At(536, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiWind_adj = function(newv)
	SetTmpParam_At(537, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiLight_adj = function(newv)
	SetTmpParam_At(538, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiIllusion_adj = function(newv)
	SetTmpParam_At(539, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiWater_adj = function(newv)
	SetTmpParam_At(540, newv)
end
CLuaLingShouPropertyFight.SetParamLSpAttMin_jc = function(newv)
	SetTmpParam_At(541, newv)
end
CLuaLingShouPropertyFight.SetParamLSpAttMax_jc = function(newv)
	SetTmpParam_At(542, newv)
end
CLuaLingShouPropertyFight.SetParamLSmAttMin_jc = function(newv)
	SetTmpParam_At(543, newv)
end
CLuaLingShouPropertyFight.SetParamLSmAttMax_jc = function(newv)
	SetTmpParam_At(544, newv)
end
CLuaLingShouPropertyFight.SetParamLSpFatal_jc = function(newv)
	SetTmpParam_At(545, newv)
end
CLuaLingShouPropertyFight.SetParamLSpFatalDamage_jc = function(newv)
	SetTmpParam_At(546, newv)
end
CLuaLingShouPropertyFight.SetParamLSmFatal_jc = function(newv)
	SetTmpParam_At(547, newv)
end
CLuaLingShouPropertyFight.SetParamLSmFatalDamage_jc = function(newv)
	SetTmpParam_At(548, newv)
end
CLuaLingShouPropertyFight.SetParamLSAntiBlock_jc = function(newv)
	SetTmpParam_At(549, newv)
end
CLuaLingShouPropertyFight.SetParamLSAntiBlockDamage_jc = function(newv)
	SetTmpParam_At(550, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiFire_jc = function(newv)
	SetTmpParam_At(551, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiThunder_jc = function(newv)
	SetTmpParam_At(552, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiIce_jc = function(newv)
	SetTmpParam_At(553, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiPoison_jc = function(newv)
	SetTmpParam_At(554, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiWind_jc = function(newv)
	SetTmpParam_At(555, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiLight_jc = function(newv)
	SetTmpParam_At(556, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiIllusion_jc = function(newv)
	SetTmpParam_At(557, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiWater_jc = function(newv)
	SetTmpParam_At(558, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiFire_mul = function(newv)
	SetTmpParam_At(559, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiThunder_mul = function(newv)
	SetTmpParam_At(560, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiIce_mul = function(newv)
	SetTmpParam_At(561, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiPoison_mul = function(newv)
	SetTmpParam_At(562, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiWind_mul = function(newv)
	SetTmpParam_At(563, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiLight_mul = function(newv)
	SetTmpParam_At(564, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiIllusion_mul = function(newv)
	SetTmpParam_At(565, newv)
end
CLuaLingShouPropertyFight.SetParamLSIgnoreAntiWater_mul = function(newv)
	SetTmpParam_At(566, newv)
end
CLuaLingShouPropertyFight.SetParamJieBan_Child_QiChangColor = function(newv)
	SetTmpParam_At(567, newv)
end
CLuaLingShouPropertyFight.SetParamJieBan_LingShou_Ratio = function(newv)
	SetTmpParam_At(568, newv)
end
CLuaLingShouPropertyFight.SetParamJieBan_LingShou_QinMi = function(newv)
	SetTmpParam_At(569, newv)
end
CLuaLingShouPropertyFight.SetParamJieBan_LingShou_CorZizhi = function(newv)
	SetTmpParam_At(570, newv)
end
CLuaLingShouPropertyFight.SetParamJieBan_LingShou_StaZizhi = function(newv)
	SetTmpParam_At(571, newv)
end
CLuaLingShouPropertyFight.SetParamJieBan_LingShou_StrZizhi = function(newv)
	SetTmpParam_At(572, newv)
end
CLuaLingShouPropertyFight.SetParamJieBan_LingShou_IntZizhi = function(newv)
	SetTmpParam_At(573, newv)
end
CLuaLingShouPropertyFight.SetParamJieBan_LingShou_AgiZizhi = function(newv)
	SetTmpParam_At(574, newv)
end
CLuaLingShouPropertyFight.SetParamBuff_DisplayValue1 = function(newv)
	SetTmpParam_At(575, newv)
end
CLuaLingShouPropertyFight.SetParamBuff_DisplayValue2 = function(newv)
	SetTmpParam_At(576, newv)
end
CLuaLingShouPropertyFight.SetParamBuff_DisplayValue3 = function(newv)
	SetTmpParam_At(577, newv)
end
CLuaLingShouPropertyFight.SetParamBuff_DisplayValue4 = function(newv)
	SetTmpParam_At(578, newv)
end
CLuaLingShouPropertyFight.SetParamBuff_DisplayValue5 = function(newv)
	SetTmpParam_At(579, newv)
end
CLuaLingShouPropertyFight.SetParamLSNature = function(newv)
	SetTmpParam_At(580, newv)
end
CLuaLingShouPropertyFight.SetParamPrayMulti = function(newv)
	SetTmpParam_At(581, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceYingLing = function(newv)
	SetTmpParam_At(582, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceYingLingMul = function(newv)
	SetTmpParam_At(583, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceFlag = function(newv)
	SetTmpParam_At(584, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceFlagMul = function(newv)
	SetTmpParam_At(585, newv)
end
CLuaLingShouPropertyFight.SetParamObjectType = function(newv)
	SetTmpParam_At(586, newv)
end
CLuaLingShouPropertyFight.SetParamMonsterType = function(newv)
	SetTmpParam_At(587, newv)
end
CLuaLingShouPropertyFight.SetParamFightPropType = function(newv)
	SetTmpParam_At(588, newv)
end
CLuaLingShouPropertyFight.SetParamPlayHpFull = function(newv)
	SetTmpParam_At(589, newv)
end
CLuaLingShouPropertyFight.SetParamPlayHp = function(newv)
	SetTmpParam_At(590, newv)
end
CLuaLingShouPropertyFight.SetParamPlayAtt = function(newv)
	SetTmpParam_At(591, newv)
end
CLuaLingShouPropertyFight.SetParamPlayDef = function(newv)
	SetTmpParam_At(592, newv)
end
CLuaLingShouPropertyFight.SetParamPlayHit = function(newv)
	SetTmpParam_At(593, newv)
end
CLuaLingShouPropertyFight.SetParamPlayMiss = function(newv)
	SetTmpParam_At(594, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceDieKe = function(newv)
	SetTmpParam_At(595, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceDieKeMul = function(newv)
	SetTmpParam_At(596, newv)
end
CLuaLingShouPropertyFight.SetParamDotRemain = function(newv)
	SetTmpParam_At(597, newv)
end
CLuaLingShouPropertyFight.SetParamIsConfused = function(newv)
	SetTmpParam_At(598, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceSheShouKefu = function(newv)
	SetTmpParam_At(599, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceJiaShiKefu = function(newv)
	SetTmpParam_At(600, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceDaoKeKefu = function(newv)
	SetTmpParam_At(601, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceXiaKeKefu = function(newv)
	SetTmpParam_At(602, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceFangShiKefu = function(newv)
	SetTmpParam_At(603, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceYiShiKefu = function(newv)
	SetTmpParam_At(604, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceMeiZheKefu = function(newv)
	SetTmpParam_At(605, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceYiRenKefu = function(newv)
	SetTmpParam_At(606, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceYanShiKefu = function(newv)
	SetTmpParam_At(607, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceHuaHunKefu = function(newv)
	SetTmpParam_At(608, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceYingLingKefu = function(newv)
	SetTmpParam_At(609, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceDieKeKefu = function(newv)
	SetTmpParam_At(610, newv)
end
CLuaLingShouPropertyFight.SetParamAdjustPermanentCor = function(newv)
	SetTmpParam_At(611, newv)
end
CLuaLingShouPropertyFight.SetParamAdjustPermanentSta = function(newv)
	SetTmpParam_At(612, newv)
end
CLuaLingShouPropertyFight.SetParamAdjustPermanentStr = function(newv)
	SetTmpParam_At(613, newv)
end
CLuaLingShouPropertyFight.SetParamAdjustPermanentInt = function(newv)
	SetTmpParam_At(614, newv)
end
CLuaLingShouPropertyFight.SetParamAdjustPermanentAgi = function(newv)
	SetTmpParam_At(615, newv)
end
CLuaLingShouPropertyFight.SetParamSoulCoreLevel = function(newv)
	SetTmpParam_At(616, newv)
end
CLuaLingShouPropertyFight.SetParamAdjustPermanentMidCor = function(newv)
	SetTmpParam_At(617, newv)
end
CLuaLingShouPropertyFight.SetParamAdjustPermanentMidSta = function(newv)
	SetTmpParam_At(618, newv)
end
CLuaLingShouPropertyFight.SetParamAdjustPermanentMidStr = function(newv)
	SetTmpParam_At(619, newv)
end
CLuaLingShouPropertyFight.SetParamAdjustPermanentMidInt = function(newv)
	SetTmpParam_At(620, newv)
end
CLuaLingShouPropertyFight.SetParamAdjustPermanentMidAgi = function(newv)
	SetTmpParam_At(621, newv)
end
CLuaLingShouPropertyFight.SetParamSumPermanent = function(newv)
	SetTmpParam_At(622, newv)
end
CLuaLingShouPropertyFight.SetParamAntiTian = function(newv)
	SetTmpParam_At(623, newv)
end
CLuaLingShouPropertyFight.SetParamAntiEGui = function(newv)
	SetTmpParam_At(624, newv)
end
CLuaLingShouPropertyFight.SetParamAntiXiuLuo = function(newv)
	SetTmpParam_At(625, newv)
end
CLuaLingShouPropertyFight.SetParamAntiDiYu = function(newv)
	SetTmpParam_At(626, newv)
end
CLuaLingShouPropertyFight.SetParamAntiRen = function(newv)
	SetTmpParam_At(627, newv)
end
CLuaLingShouPropertyFight.SetParamAntiChuSheng = function(newv)
	SetTmpParam_At(628, newv)
end
CLuaLingShouPropertyFight.SetParamAntiBuilding = function(newv)
	SetTmpParam_At(629, newv)
end
CLuaLingShouPropertyFight.SetParamAntiZhaoHuan = function(newv)
	SetTmpParam_At(630, newv)
end
CLuaLingShouPropertyFight.SetParamAntiLingShou = function(newv)
	SetTmpParam_At(631, newv)
end
CLuaLingShouPropertyFight.SetParamAntiBoss = function(newv)
	SetTmpParam_At(632, newv)
end
CLuaLingShouPropertyFight.SetParamAntiSheShou = function(newv)
	SetTmpParam_At(633, newv)
end
CLuaLingShouPropertyFight.SetParamAntiJiaShi = function(newv)
	SetTmpParam_At(634, newv)
end
CLuaLingShouPropertyFight.SetParamAntiDaoKe = function(newv)
	SetTmpParam_At(635, newv)
end
CLuaLingShouPropertyFight.SetParamAntiXiaKe = function(newv)
	SetTmpParam_At(636, newv)
end
CLuaLingShouPropertyFight.SetParamAntiFangShi = function(newv)
	SetTmpParam_At(637, newv)
end
CLuaLingShouPropertyFight.SetParamAntiYiShi = function(newv)
	SetTmpParam_At(638, newv)
end
CLuaLingShouPropertyFight.SetParamAntiMeiZhe = function(newv)
	SetTmpParam_At(639, newv)
end
CLuaLingShouPropertyFight.SetParamAntiYiRen = function(newv)
	SetTmpParam_At(640, newv)
end
CLuaLingShouPropertyFight.SetParamAntiYanShi = function(newv)
	SetTmpParam_At(641, newv)
end
CLuaLingShouPropertyFight.SetParamAntiHuaHun = function(newv)
	SetTmpParam_At(642, newv)
end
CLuaLingShouPropertyFight.SetParamAntiYingLing = function(newv)
	SetTmpParam_At(643, newv)
end
CLuaLingShouPropertyFight.SetParamAntiDieKe = function(newv)
	SetTmpParam_At(644, newv)
end
CLuaLingShouPropertyFight.SetParamAntiFlag = function(newv)
	SetTmpParam_At(645, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceZhanKuang = function(newv)
	SetTmpParam_At(646, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceZhanKuangMul = function(newv)
	SetTmpParam_At(647, newv)
end
CLuaLingShouPropertyFight.SetParamEnhanceZhanKuangKefu = function(newv)
	SetTmpParam_At(648, newv)
end
CLuaLingShouPropertyFight.SetParamAntiZhanKuang = function(newv)
	SetTmpParam_At(649, newv)
end
CLuaLingShouPropertyFight.SetParamJumpSpeed = function(newv)
	SetTmpParam_At(650, newv)
end
CLuaLingShouPropertyFight.SetParamOriJumpSpeed = function(newv)
	SetTmpParam_At(651, newv)
end
CLuaLingShouPropertyFight.SetParamAdjJumpSpeed = function(newv)
	SetTmpParam_At(652, newv)
end
CLuaLingShouPropertyFight.SetParamGravityAcceleration = function(newv)
	SetTmpParam_At(653, newv)
end
CLuaLingShouPropertyFight.SetParamOriGravityAcceleration = function(newv)
	SetTmpParam_At(654, newv)
end
CLuaLingShouPropertyFight.SetParamAdjGravityAcceleration = function(newv)
	SetTmpParam_At(655, newv)
end
CLuaLingShouPropertyFight.SetParamHorizontalAcceleration = function(newv)
	SetTmpParam_At(656, newv)
end
CLuaLingShouPropertyFight.SetParamOriHorizontalAcceleration = function(newv)
	SetTmpParam_At(657, newv)
end
CLuaLingShouPropertyFight.SetParamAdjHorizontalAcceleration = function(newv)
	SetTmpParam_At(658, newv)
end
CLuaLingShouPropertyFight.SetParamSwimSpeed = function(newv)
	SetTmpParam_At(659, newv)
end
CLuaLingShouPropertyFight.SetParamOriSwimSpeed = function(newv)
	SetTmpParam_At(660, newv)
end
CLuaLingShouPropertyFight.SetParamAdjSwimSpeed = function(newv)
	SetTmpParam_At(661, newv)
end
CLuaLingShouPropertyFight.SetParamStrengthPoint = function(newv)
	SetTmpParam_At(662, newv)
end
CLuaLingShouPropertyFight.SetParamStrengthPointMax = function(newv)
	SetTmpParam_At(663, newv)
end
CLuaLingShouPropertyFight.SetParamOriStrengthPointMax = function(newv)
	SetTmpParam_At(664, newv)
end
CLuaLingShouPropertyFight.SetParamAdjStrengthPointMax = function(newv)
	SetTmpParam_At(665, newv)
end
CLuaLingShouPropertyFight.SetParamAntiMulEnhanceIllusion = function(newv)
	SetTmpParam_At(666, newv)
end
CLuaLingShouPropertyFight.SetParamGlideHorizontalSpeedWithUmbrella = function(newv)
	SetTmpParam_At(667, newv)
end
CLuaLingShouPropertyFight.SetParamGlideVerticalSpeedWithUmbrella = function(newv)
	SetTmpParam_At(668, newv)
end
CLuaLingShouPropertyFight.SetParamAdjpSpeed = function(newv)
	SetTmpParam_At(1001, newv)

	CLuaLingShouPropertyFight.RefreshParampSpeed()
end
CLuaLingShouPropertyFight.SetParamAdjmSpeed = function(newv)
	SetTmpParam_At(1002, newv)

	CLuaLingShouPropertyFight.RefreshParammSpeed()
end
CLingShouPropertyFight.m_hookRefreshAll = function(this)
	CLuaLingShouPropertyFight.Param = this.Param
	CLuaLingShouPropertyFight.TmpParam = this.TmpParam

	CLuaLingShouPropertyFight.RefreshParamAntiPushPull()
	CLuaLingShouPropertyFight.RefreshParamBlockDamage()
	CLuaLingShouPropertyFight.RefreshParamMHType()
	CLuaLingShouPropertyFight.RefreshParamAntimFatalDamage()
	CLuaLingShouPropertyFight.RefreshParamInvisible()
	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiLight()
	CLuaLingShouPropertyFight.RefreshParamAntiBreak()
	CLuaLingShouPropertyFight.RefreshParamAntiSilence()
	CLuaLingShouPropertyFight.RefreshParamEnhanceChaos()
	CLuaLingShouPropertyFight.RefreshParamPAType()
	CLuaLingShouPropertyFight.RefreshParamAntiBlock()
	CLuaLingShouPropertyFight.RefreshParamAntiWater()
	CLuaLingShouPropertyFight.RefreshParamSta()
	CLuaLingShouPropertyFight.RefreshParamLSBBAdjHp()
	CLuaLingShouPropertyFight.RefreshParamAntimFatal()
	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiThunder()
	CLuaLingShouPropertyFight.RefreshParamAntipFatal()
	CLuaLingShouPropertyFight.RefreshParamAntiWind()
	CLuaLingShouPropertyFight.RefreshParamEnhancePetrify()
	CLuaLingShouPropertyFight.RefreshParamAntiChaos()
	CLuaLingShouPropertyFight.RefreshParamEnhanceIce()
	CLuaLingShouPropertyFight.RefreshParamEnhanceWind()
	CLuaLingShouPropertyFight.RefreshParamWuxingImprove()
	CLuaLingShouPropertyFight.RefreshParamAntiIllusion()
	CLuaLingShouPropertyFight.RefreshParamAntiThunder()
	CLuaLingShouPropertyFight.RefreshParamBlock()
	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiFire()
	CLuaLingShouPropertyFight.RefreshParamSpeed()
	CLuaLingShouPropertyFight.RefreshParamEnhanceBind()
	CLuaLingShouPropertyFight.RefreshParamAntiFreeze()
	CLuaLingShouPropertyFight.RefreshParamAntiSleep()
	CLuaLingShouPropertyFight.RefreshParamEnhanceFreeze()
	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiWater()
	CLuaLingShouPropertyFight.RefreshParamEnhanceHeal()
	CLuaLingShouPropertyFight.RefreshParamAntiDizzy()
	CLuaLingShouPropertyFight.RefreshParamAntiBind()
	CLuaLingShouPropertyFight.RefreshParamAntiPoison()
	CLuaLingShouPropertyFight.RefreshParamEnhanceSleep()
	CLuaLingShouPropertyFight.RefreshParamEnhanceWater()
	CLuaLingShouPropertyFight.RefreshParamMAType()
	CLuaLingShouPropertyFight.RefreshParamAntipFatalDamage()
	CLuaLingShouPropertyFight.RefreshParamEyeSight()
	CLuaLingShouPropertyFight.RefreshParamAntiDecelerate()
	CLuaLingShouPropertyFight.RefreshParamAntiBlind()
	CLuaLingShouPropertyFight.RefreshParamIntZizhi()
	CLuaLingShouPropertyFight.RefreshParamEnhanceBeHeal()
	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiIllusion()
	CLuaLingShouPropertyFight.RefreshParamEnhanceThunder()
	CLuaLingShouPropertyFight.RefreshParamPHType()
	CLuaLingShouPropertyFight.RefreshParamTrueSight()
	CLuaLingShouPropertyFight.RefreshParamMType()
	CLuaLingShouPropertyFight.RefreshParamEnhanceSilence()
	CLuaLingShouPropertyFight.RefreshParammSpeed()
	CLuaLingShouPropertyFight.RefreshParamAntiAoe()
	CLuaLingShouPropertyFight.RefreshParamAntiPetrify()
	CLuaLingShouPropertyFight.RefreshParamAgiZizhi()
	CLuaLingShouPropertyFight.RefreshParamAntiBianHu()
	CLuaLingShouPropertyFight.RefreshParamCorZizhi()
	CLuaLingShouPropertyFight.RefreshParamAntiLight()
	CLuaLingShouPropertyFight.RefreshParamStrZizhi()
	CLuaLingShouPropertyFight.RefreshParamEnhanceTie()
	CLuaLingShouPropertyFight.RefreshParamAntiIce()
	CLuaLingShouPropertyFight.RefreshParampSpeed()
	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiPoison()
	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiIce()
	CLuaLingShouPropertyFight.RefreshParamEnhanceBianHu()
	CLuaLingShouPropertyFight.RefreshParamIgnoreAntiWind()
	CLuaLingShouPropertyFight.RefreshParamStaZizhi()
	CLuaLingShouPropertyFight.RefreshParamAntiTie()
	CLuaLingShouPropertyFight.RefreshParamEnhanceLight()
	CLuaLingShouPropertyFight.RefreshParamAntiFire()
	CLuaLingShouPropertyFight.RefreshParamEnhancePoison()
	CLuaLingShouPropertyFight.RefreshParamEnhanceFire()
	CLuaLingShouPropertyFight.RefreshParamPType()
	CLuaLingShouPropertyFight.RefreshParamAntiBlockDamage()
	CLuaLingShouPropertyFight.RefreshParamXiuweiImprove()
	CLuaLingShouPropertyFight.RefreshParamEnhanceIllusion()
	CLuaLingShouPropertyFight.RefreshParamEnhanceDizzy()
	CLuaLingShouPropertyFight.RefreshParamAgi()
	CLuaLingShouPropertyFight.RefreshParamCor()
	CLuaLingShouPropertyFight.RefreshParamInt()
	CLuaLingShouPropertyFight.RefreshParamStr()
	CLuaLingShouPropertyFight.RefreshParammMiss()
	CLuaLingShouPropertyFight.RefreshParampHit()
	CLuaLingShouPropertyFight.RefreshParammFatalDamage()
	CLuaLingShouPropertyFight.RefreshParammFatal()
	CLuaLingShouPropertyFight.RefreshParampFatal()
	CLuaLingShouPropertyFight.RefreshParammHit()
	CLuaLingShouPropertyFight.RefreshParampFatalDamage()
	CLuaLingShouPropertyFight.RefreshParampMiss()
	CLuaLingShouPropertyFight.RefreshParamHpFull()
	CLuaLingShouPropertyFight.RefreshParammAttMax()
	CLuaLingShouPropertyFight.RefreshParammAttMin()
	CLuaLingShouPropertyFight.RefreshParammDef()
	CLuaLingShouPropertyFight.RefreshParampDef()
	CLuaLingShouPropertyFight.RefreshParampAttMin()
	CLuaLingShouPropertyFight.RefreshParampAttMax()
	CLuaLingShouPropertyFight.RefreshParamHpRecover()
	
	CLuaLingShouPropertyFight.Param = nil
	CLuaLingShouPropertyFight.TmpParam = nil
end
SetParamFunctions[EnumLingShouFightProp.PermanentCor] = CLuaLingShouPropertyFight.SetParamPermanentCor
SetParamFunctions[EnumLingShouFightProp.PermanentSta] = CLuaLingShouPropertyFight.SetParamPermanentSta
SetParamFunctions[EnumLingShouFightProp.PermanentStr] = CLuaLingShouPropertyFight.SetParamPermanentStr
SetParamFunctions[EnumLingShouFightProp.PermanentInt] = CLuaLingShouPropertyFight.SetParamPermanentInt
SetParamFunctions[EnumLingShouFightProp.PermanentAgi] = CLuaLingShouPropertyFight.SetParamPermanentAgi
SetParamFunctions[EnumLingShouFightProp.PermanentHpFull] = CLuaLingShouPropertyFight.SetParamPermanentHpFull
SetParamFunctions[EnumLingShouFightProp.Hp] = CLuaLingShouPropertyFight.SetParamHp
SetParamFunctions[EnumLingShouFightProp.Mp] = CLuaLingShouPropertyFight.SetParamMp
SetParamFunctions[EnumLingShouFightProp.CurrentHpFull] = CLuaLingShouPropertyFight.SetParamCurrentHpFull
SetParamFunctions[EnumLingShouFightProp.CurrentMpFull] = CLuaLingShouPropertyFight.SetParamCurrentMpFull
SetParamFunctions[EnumLingShouFightProp.EquipExchangeMF] = CLuaLingShouPropertyFight.SetParamEquipExchangeMF
SetParamFunctions[EnumLingShouFightProp.RevisePermanentCor] = CLuaLingShouPropertyFight.SetParamRevisePermanentCor
SetParamFunctions[EnumLingShouFightProp.RevisePermanentSta] = CLuaLingShouPropertyFight.SetParamRevisePermanentSta
SetParamFunctions[EnumLingShouFightProp.RevisePermanentStr] = CLuaLingShouPropertyFight.SetParamRevisePermanentStr
SetParamFunctions[EnumLingShouFightProp.RevisePermanentInt] = CLuaLingShouPropertyFight.SetParamRevisePermanentInt
SetParamFunctions[EnumLingShouFightProp.RevisePermanentAgi] = CLuaLingShouPropertyFight.SetParamRevisePermanentAgi
SetParamFunctions[EnumLingShouFightProp.Class] = CLuaLingShouPropertyFight.SetParamClass
SetParamFunctions[EnumLingShouFightProp.Grade] = CLuaLingShouPropertyFight.SetParamGrade
SetParamFunctions[EnumLingShouFightProp.AdjCor] = CLuaLingShouPropertyFight.SetParamAdjCor
SetParamFunctions[EnumLingShouFightProp.MulCor] = CLuaLingShouPropertyFight.SetParamMulCor
SetParamFunctions[EnumLingShouFightProp.AdjSta] = CLuaLingShouPropertyFight.SetParamAdjSta
SetParamFunctions[EnumLingShouFightProp.MulSta] = CLuaLingShouPropertyFight.SetParamMulSta
SetParamFunctions[EnumLingShouFightProp.AdjStr] = CLuaLingShouPropertyFight.SetParamAdjStr
SetParamFunctions[EnumLingShouFightProp.MulStr] = CLuaLingShouPropertyFight.SetParamMulStr
SetParamFunctions[EnumLingShouFightProp.AdjInt] = CLuaLingShouPropertyFight.SetParamAdjInt
SetParamFunctions[EnumLingShouFightProp.MulInt] = CLuaLingShouPropertyFight.SetParamMulInt
SetParamFunctions[EnumLingShouFightProp.AdjAgi] = CLuaLingShouPropertyFight.SetParamAdjAgi
SetParamFunctions[EnumLingShouFightProp.MulAgi] = CLuaLingShouPropertyFight.SetParamMulAgi
SetParamFunctions[EnumLingShouFightProp.AdjHpFull] = CLuaLingShouPropertyFight.SetParamAdjHpFull
SetParamFunctions[EnumLingShouFightProp.MulHpFull] = CLuaLingShouPropertyFight.SetParamMulHpFull
SetParamFunctions[EnumLingShouFightProp.AdjHpRecover] = CLuaLingShouPropertyFight.SetParamAdjHpRecover
SetParamFunctions[EnumLingShouFightProp.MulHpRecover] = CLuaLingShouPropertyFight.SetParamMulHpRecover
SetParamFunctions[EnumLingShouFightProp.MpFull] = CLuaLingShouPropertyFight.SetParamMpFull
SetParamFunctions[EnumLingShouFightProp.AdjMpFull] = CLuaLingShouPropertyFight.SetParamAdjMpFull
SetParamFunctions[EnumLingShouFightProp.MulMpFull] = CLuaLingShouPropertyFight.SetParamMulMpFull
SetParamFunctions[EnumLingShouFightProp.MpRecover] = CLuaLingShouPropertyFight.SetParamMpRecover
SetParamFunctions[EnumLingShouFightProp.AdjMpRecover] = CLuaLingShouPropertyFight.SetParamAdjMpRecover
SetParamFunctions[EnumLingShouFightProp.MulMpRecover] = CLuaLingShouPropertyFight.SetParamMulMpRecover
SetParamFunctions[EnumLingShouFightProp.AdjpAttMin] = CLuaLingShouPropertyFight.SetParamAdjpAttMin
SetParamFunctions[EnumLingShouFightProp.MulpAttMin] = CLuaLingShouPropertyFight.SetParamMulpAttMin
SetParamFunctions[EnumLingShouFightProp.AdjpAttMax] = CLuaLingShouPropertyFight.SetParamAdjpAttMax
SetParamFunctions[EnumLingShouFightProp.MulpAttMax] = CLuaLingShouPropertyFight.SetParamMulpAttMax
SetParamFunctions[EnumLingShouFightProp.AdjpHit] = CLuaLingShouPropertyFight.SetParamAdjpHit
SetParamFunctions[EnumLingShouFightProp.MulpHit] = CLuaLingShouPropertyFight.SetParamMulpHit
SetParamFunctions[EnumLingShouFightProp.AdjpMiss] = CLuaLingShouPropertyFight.SetParamAdjpMiss
SetParamFunctions[EnumLingShouFightProp.MulpMiss] = CLuaLingShouPropertyFight.SetParamMulpMiss
SetParamFunctions[EnumLingShouFightProp.AdjpDef] = CLuaLingShouPropertyFight.SetParamAdjpDef
SetParamFunctions[EnumLingShouFightProp.MulpDef] = CLuaLingShouPropertyFight.SetParamMulpDef
SetParamFunctions[EnumLingShouFightProp.AdjpHurt] = CLuaLingShouPropertyFight.SetParamAdjpHurt
SetParamFunctions[EnumLingShouFightProp.MulpHurt] = CLuaLingShouPropertyFight.SetParamMulpHurt
SetParamFunctions[EnumLingShouFightProp.OripSpeed] = CLuaLingShouPropertyFight.SetParamOripSpeed
SetParamFunctions[EnumLingShouFightProp.MulpSpeed] = CLuaLingShouPropertyFight.SetParamMulpSpeed
SetParamFunctions[EnumLingShouFightProp.AdjpFatal] = CLuaLingShouPropertyFight.SetParamAdjpFatal
SetParamFunctions[EnumLingShouFightProp.AdjAntipFatal] = CLuaLingShouPropertyFight.SetParamAdjAntipFatal
SetParamFunctions[EnumLingShouFightProp.AdjpFatalDamage] = CLuaLingShouPropertyFight.SetParamAdjpFatalDamage
SetParamFunctions[EnumLingShouFightProp.AdjAntipFatalDamage] = CLuaLingShouPropertyFight.SetParamAdjAntipFatalDamage
SetParamFunctions[EnumLingShouFightProp.AdjBlock] = CLuaLingShouPropertyFight.SetParamAdjBlock
SetParamFunctions[EnumLingShouFightProp.MulBlock] = CLuaLingShouPropertyFight.SetParamMulBlock
SetParamFunctions[EnumLingShouFightProp.AdjBlockDamage] = CLuaLingShouPropertyFight.SetParamAdjBlockDamage
SetParamFunctions[EnumLingShouFightProp.AdjAntiBlock] = CLuaLingShouPropertyFight.SetParamAdjAntiBlock
SetParamFunctions[EnumLingShouFightProp.AdjAntiBlockDamage] = CLuaLingShouPropertyFight.SetParamAdjAntiBlockDamage
SetParamFunctions[EnumLingShouFightProp.AdjmAttMin] = CLuaLingShouPropertyFight.SetParamAdjmAttMin
SetParamFunctions[EnumLingShouFightProp.MulmAttMin] = CLuaLingShouPropertyFight.SetParamMulmAttMin
SetParamFunctions[EnumLingShouFightProp.AdjmAttMax] = CLuaLingShouPropertyFight.SetParamAdjmAttMax
SetParamFunctions[EnumLingShouFightProp.MulmAttMax] = CLuaLingShouPropertyFight.SetParamMulmAttMax
SetParamFunctions[EnumLingShouFightProp.AdjmHit] = CLuaLingShouPropertyFight.SetParamAdjmHit
SetParamFunctions[EnumLingShouFightProp.MulmHit] = CLuaLingShouPropertyFight.SetParamMulmHit
SetParamFunctions[EnumLingShouFightProp.AdjmMiss] = CLuaLingShouPropertyFight.SetParamAdjmMiss
SetParamFunctions[EnumLingShouFightProp.MulmMiss] = CLuaLingShouPropertyFight.SetParamMulmMiss
SetParamFunctions[EnumLingShouFightProp.AdjmDef] = CLuaLingShouPropertyFight.SetParamAdjmDef
SetParamFunctions[EnumLingShouFightProp.MulmDef] = CLuaLingShouPropertyFight.SetParamMulmDef
SetParamFunctions[EnumLingShouFightProp.AdjmHurt] = CLuaLingShouPropertyFight.SetParamAdjmHurt
SetParamFunctions[EnumLingShouFightProp.MulmHurt] = CLuaLingShouPropertyFight.SetParamMulmHurt
SetParamFunctions[EnumLingShouFightProp.OrimSpeed] = CLuaLingShouPropertyFight.SetParamOrimSpeed
SetParamFunctions[EnumLingShouFightProp.MulmSpeed] = CLuaLingShouPropertyFight.SetParamMulmSpeed
SetParamFunctions[EnumLingShouFightProp.AdjmFatal] = CLuaLingShouPropertyFight.SetParamAdjmFatal
SetParamFunctions[EnumLingShouFightProp.AdjAntimFatal] = CLuaLingShouPropertyFight.SetParamAdjAntimFatal
SetParamFunctions[EnumLingShouFightProp.AdjmFatalDamage] = CLuaLingShouPropertyFight.SetParamAdjmFatalDamage
SetParamFunctions[EnumLingShouFightProp.AdjAntimFatalDamage] = CLuaLingShouPropertyFight.SetParamAdjAntimFatalDamage
SetParamFunctions[EnumLingShouFightProp.AdjEnhanceFire] = CLuaLingShouPropertyFight.SetParamAdjEnhanceFire
SetParamFunctions[EnumLingShouFightProp.MulEnhanceFire] = CLuaLingShouPropertyFight.SetParamMulEnhanceFire
SetParamFunctions[EnumLingShouFightProp.AdjEnhanceThunder] = CLuaLingShouPropertyFight.SetParamAdjEnhanceThunder
SetParamFunctions[EnumLingShouFightProp.MulEnhanceThunder] = CLuaLingShouPropertyFight.SetParamMulEnhanceThunder
SetParamFunctions[EnumLingShouFightProp.AdjEnhanceIce] = CLuaLingShouPropertyFight.SetParamAdjEnhanceIce
SetParamFunctions[EnumLingShouFightProp.MulEnhanceIce] = CLuaLingShouPropertyFight.SetParamMulEnhanceIce
SetParamFunctions[EnumLingShouFightProp.AdjEnhancePoison] = CLuaLingShouPropertyFight.SetParamAdjEnhancePoison
SetParamFunctions[EnumLingShouFightProp.MulEnhancePoison] = CLuaLingShouPropertyFight.SetParamMulEnhancePoison
SetParamFunctions[EnumLingShouFightProp.AdjEnhanceWind] = CLuaLingShouPropertyFight.SetParamAdjEnhanceWind
SetParamFunctions[EnumLingShouFightProp.MulEnhanceWind] = CLuaLingShouPropertyFight.SetParamMulEnhanceWind
SetParamFunctions[EnumLingShouFightProp.AdjEnhanceLight] = CLuaLingShouPropertyFight.SetParamAdjEnhanceLight
SetParamFunctions[EnumLingShouFightProp.MulEnhanceLight] = CLuaLingShouPropertyFight.SetParamMulEnhanceLight
SetParamFunctions[EnumLingShouFightProp.AdjEnhanceIllusion] = CLuaLingShouPropertyFight.SetParamAdjEnhanceIllusion
SetParamFunctions[EnumLingShouFightProp.MulEnhanceIllusion] = CLuaLingShouPropertyFight.SetParamMulEnhanceIllusion
SetParamFunctions[EnumLingShouFightProp.AdjAntiFire] = CLuaLingShouPropertyFight.SetParamAdjAntiFire
SetParamFunctions[EnumLingShouFightProp.MulAntiFire] = CLuaLingShouPropertyFight.SetParamMulAntiFire
SetParamFunctions[EnumLingShouFightProp.AdjAntiThunder] = CLuaLingShouPropertyFight.SetParamAdjAntiThunder
SetParamFunctions[EnumLingShouFightProp.MulAntiThunder] = CLuaLingShouPropertyFight.SetParamMulAntiThunder
SetParamFunctions[EnumLingShouFightProp.AdjAntiIce] = CLuaLingShouPropertyFight.SetParamAdjAntiIce
SetParamFunctions[EnumLingShouFightProp.MulAntiIce] = CLuaLingShouPropertyFight.SetParamMulAntiIce
SetParamFunctions[EnumLingShouFightProp.AdjAntiPoison] = CLuaLingShouPropertyFight.SetParamAdjAntiPoison
SetParamFunctions[EnumLingShouFightProp.MulAntiPoison] = CLuaLingShouPropertyFight.SetParamMulAntiPoison
SetParamFunctions[EnumLingShouFightProp.AdjAntiWind] = CLuaLingShouPropertyFight.SetParamAdjAntiWind
SetParamFunctions[EnumLingShouFightProp.MulAntiWind] = CLuaLingShouPropertyFight.SetParamMulAntiWind
SetParamFunctions[EnumLingShouFightProp.AdjAntiLight] = CLuaLingShouPropertyFight.SetParamAdjAntiLight
SetParamFunctions[EnumLingShouFightProp.MulAntiLight] = CLuaLingShouPropertyFight.SetParamMulAntiLight
SetParamFunctions[EnumLingShouFightProp.AdjAntiIllusion] = CLuaLingShouPropertyFight.SetParamAdjAntiIllusion
SetParamFunctions[EnumLingShouFightProp.MulAntiIllusion] = CLuaLingShouPropertyFight.SetParamMulAntiIllusion
SetParamFunctions[EnumLingShouFightProp.AdjEnhanceDizzy] = CLuaLingShouPropertyFight.SetParamAdjEnhanceDizzy
SetParamFunctions[EnumLingShouFightProp.MulEnhanceDizzy] = CLuaLingShouPropertyFight.SetParamMulEnhanceDizzy
SetParamFunctions[EnumLingShouFightProp.AdjEnhanceSleep] = CLuaLingShouPropertyFight.SetParamAdjEnhanceSleep
SetParamFunctions[EnumLingShouFightProp.MulEnhanceSleep] = CLuaLingShouPropertyFight.SetParamMulEnhanceSleep
SetParamFunctions[EnumLingShouFightProp.AdjEnhanceChaos] = CLuaLingShouPropertyFight.SetParamAdjEnhanceChaos
SetParamFunctions[EnumLingShouFightProp.MulEnhanceChaos] = CLuaLingShouPropertyFight.SetParamMulEnhanceChaos
SetParamFunctions[EnumLingShouFightProp.AdjEnhanceBind] = CLuaLingShouPropertyFight.SetParamAdjEnhanceBind
SetParamFunctions[EnumLingShouFightProp.MulEnhanceBind] = CLuaLingShouPropertyFight.SetParamMulEnhanceBind
SetParamFunctions[EnumLingShouFightProp.AdjEnhanceSilence] = CLuaLingShouPropertyFight.SetParamAdjEnhanceSilence
SetParamFunctions[EnumLingShouFightProp.MulEnhanceSilence] = CLuaLingShouPropertyFight.SetParamMulEnhanceSilence
SetParamFunctions[EnumLingShouFightProp.AdjAntiDizzy] = CLuaLingShouPropertyFight.SetParamAdjAntiDizzy
SetParamFunctions[EnumLingShouFightProp.MulAntiDizzy] = CLuaLingShouPropertyFight.SetParamMulAntiDizzy
SetParamFunctions[EnumLingShouFightProp.AdjAntiSleep] = CLuaLingShouPropertyFight.SetParamAdjAntiSleep
SetParamFunctions[EnumLingShouFightProp.MulAntiSleep] = CLuaLingShouPropertyFight.SetParamMulAntiSleep
SetParamFunctions[EnumLingShouFightProp.AdjAntiChaos] = CLuaLingShouPropertyFight.SetParamAdjAntiChaos
SetParamFunctions[EnumLingShouFightProp.MulAntiChaos] = CLuaLingShouPropertyFight.SetParamMulAntiChaos
SetParamFunctions[EnumLingShouFightProp.AdjAntiBind] = CLuaLingShouPropertyFight.SetParamAdjAntiBind
SetParamFunctions[EnumLingShouFightProp.MulAntiBind] = CLuaLingShouPropertyFight.SetParamMulAntiBind
SetParamFunctions[EnumLingShouFightProp.AdjAntiSilence] = CLuaLingShouPropertyFight.SetParamAdjAntiSilence
SetParamFunctions[EnumLingShouFightProp.MulAntiSilence] = CLuaLingShouPropertyFight.SetParamMulAntiSilence
SetParamFunctions[EnumLingShouFightProp.DizzyTimeChange] = CLuaLingShouPropertyFight.SetParamDizzyTimeChange
SetParamFunctions[EnumLingShouFightProp.SleepTimeChange] = CLuaLingShouPropertyFight.SetParamSleepTimeChange
SetParamFunctions[EnumLingShouFightProp.ChaosTimeChange] = CLuaLingShouPropertyFight.SetParamChaosTimeChange
SetParamFunctions[EnumLingShouFightProp.BindTimeChange] = CLuaLingShouPropertyFight.SetParamBindTimeChange
SetParamFunctions[EnumLingShouFightProp.SilenceTimeChange] = CLuaLingShouPropertyFight.SetParamSilenceTimeChange
SetParamFunctions[EnumLingShouFightProp.BianhuTimeChange] = CLuaLingShouPropertyFight.SetParamBianhuTimeChange
SetParamFunctions[EnumLingShouFightProp.FreezeTimeChange] = CLuaLingShouPropertyFight.SetParamFreezeTimeChange
SetParamFunctions[EnumLingShouFightProp.PetrifyTimeChange] = CLuaLingShouPropertyFight.SetParamPetrifyTimeChange
SetParamFunctions[EnumLingShouFightProp.AdjEnhanceHeal] = CLuaLingShouPropertyFight.SetParamAdjEnhanceHeal
SetParamFunctions[EnumLingShouFightProp.MulEnhanceHeal] = CLuaLingShouPropertyFight.SetParamMulEnhanceHeal
SetParamFunctions[EnumLingShouFightProp.OriSpeed] = CLuaLingShouPropertyFight.SetParamOriSpeed
SetParamFunctions[EnumLingShouFightProp.AdjSpeed] = CLuaLingShouPropertyFight.SetParamAdjSpeed
SetParamFunctions[EnumLingShouFightProp.MulSpeed] = CLuaLingShouPropertyFight.SetParamMulSpeed
SetParamFunctions[EnumLingShouFightProp.MF] = CLuaLingShouPropertyFight.SetParamMF
SetParamFunctions[EnumLingShouFightProp.AdjMF] = CLuaLingShouPropertyFight.SetParamAdjMF
SetParamFunctions[EnumLingShouFightProp.Range] = CLuaLingShouPropertyFight.SetParamRange
SetParamFunctions[EnumLingShouFightProp.AdjRange] = CLuaLingShouPropertyFight.SetParamAdjRange
SetParamFunctions[EnumLingShouFightProp.HatePlus] = CLuaLingShouPropertyFight.SetParamHatePlus
SetParamFunctions[EnumLingShouFightProp.AdjHatePlus] = CLuaLingShouPropertyFight.SetParamAdjHatePlus
SetParamFunctions[EnumLingShouFightProp.HateDecrease] = CLuaLingShouPropertyFight.SetParamHateDecrease
SetParamFunctions[EnumLingShouFightProp.AdjInvisible] = CLuaLingShouPropertyFight.SetParamAdjInvisible
SetParamFunctions[EnumLingShouFightProp.OriTrueSight] = CLuaLingShouPropertyFight.SetParamOriTrueSight
SetParamFunctions[EnumLingShouFightProp.AdjTrueSight] = CLuaLingShouPropertyFight.SetParamAdjTrueSight
SetParamFunctions[EnumLingShouFightProp.OriEyeSight] = CLuaLingShouPropertyFight.SetParamOriEyeSight
SetParamFunctions[EnumLingShouFightProp.AdjEyeSight] = CLuaLingShouPropertyFight.SetParamAdjEyeSight
SetParamFunctions[EnumLingShouFightProp.EnhanceTian] = CLuaLingShouPropertyFight.SetParamEnhanceTian
SetParamFunctions[EnumLingShouFightProp.EnhanceTianMul] = CLuaLingShouPropertyFight.SetParamEnhanceTianMul
SetParamFunctions[EnumLingShouFightProp.EnhanceEGui] = CLuaLingShouPropertyFight.SetParamEnhanceEGui
SetParamFunctions[EnumLingShouFightProp.EnhanceEGuiMul] = CLuaLingShouPropertyFight.SetParamEnhanceEGuiMul
SetParamFunctions[EnumLingShouFightProp.EnhanceXiuLuo] = CLuaLingShouPropertyFight.SetParamEnhanceXiuLuo
SetParamFunctions[EnumLingShouFightProp.EnhanceXiuLuoMul] = CLuaLingShouPropertyFight.SetParamEnhanceXiuLuoMul
SetParamFunctions[EnumLingShouFightProp.EnhanceDiYu] = CLuaLingShouPropertyFight.SetParamEnhanceDiYu
SetParamFunctions[EnumLingShouFightProp.EnhanceDiYuMul] = CLuaLingShouPropertyFight.SetParamEnhanceDiYuMul
SetParamFunctions[EnumLingShouFightProp.EnhanceRen] = CLuaLingShouPropertyFight.SetParamEnhanceRen
SetParamFunctions[EnumLingShouFightProp.EnhanceRenMul] = CLuaLingShouPropertyFight.SetParamEnhanceRenMul
SetParamFunctions[EnumLingShouFightProp.EnhanceChuSheng] = CLuaLingShouPropertyFight.SetParamEnhanceChuSheng
SetParamFunctions[EnumLingShouFightProp.EnhanceChuShengMul] = CLuaLingShouPropertyFight.SetParamEnhanceChuShengMul
SetParamFunctions[EnumLingShouFightProp.EnhanceBuilding] = CLuaLingShouPropertyFight.SetParamEnhanceBuilding
SetParamFunctions[EnumLingShouFightProp.EnhanceBuildingMul] = CLuaLingShouPropertyFight.SetParamEnhanceBuildingMul
SetParamFunctions[EnumLingShouFightProp.EnhanceBoss] = CLuaLingShouPropertyFight.SetParamEnhanceBoss
SetParamFunctions[EnumLingShouFightProp.EnhanceBossMul] = CLuaLingShouPropertyFight.SetParamEnhanceBossMul
SetParamFunctions[EnumLingShouFightProp.EnhanceSheShou] = CLuaLingShouPropertyFight.SetParamEnhanceSheShou
SetParamFunctions[EnumLingShouFightProp.EnhanceSheShouMul] = CLuaLingShouPropertyFight.SetParamEnhanceSheShouMul
SetParamFunctions[EnumLingShouFightProp.EnhanceJiaShi] = CLuaLingShouPropertyFight.SetParamEnhanceJiaShi
SetParamFunctions[EnumLingShouFightProp.EnhanceJiaShiMul] = CLuaLingShouPropertyFight.SetParamEnhanceJiaShiMul
SetParamFunctions[EnumLingShouFightProp.EnhanceFangShi] = CLuaLingShouPropertyFight.SetParamEnhanceFangShi
SetParamFunctions[EnumLingShouFightProp.EnhanceFangShiMul] = CLuaLingShouPropertyFight.SetParamEnhanceFangShiMul
SetParamFunctions[EnumLingShouFightProp.EnhanceYiShi] = CLuaLingShouPropertyFight.SetParamEnhanceYiShi
SetParamFunctions[EnumLingShouFightProp.EnhanceYiShiMul] = CLuaLingShouPropertyFight.SetParamEnhanceYiShiMul
SetParamFunctions[EnumLingShouFightProp.EnhanceMeiZhe] = CLuaLingShouPropertyFight.SetParamEnhanceMeiZhe
SetParamFunctions[EnumLingShouFightProp.EnhanceMeiZheMul] = CLuaLingShouPropertyFight.SetParamEnhanceMeiZheMul
SetParamFunctions[EnumLingShouFightProp.EnhanceYiRen] = CLuaLingShouPropertyFight.SetParamEnhanceYiRen
SetParamFunctions[EnumLingShouFightProp.EnhanceYiRenMul] = CLuaLingShouPropertyFight.SetParamEnhanceYiRenMul
SetParamFunctions[EnumLingShouFightProp.IgnorepDef] = CLuaLingShouPropertyFight.SetParamIgnorepDef
SetParamFunctions[EnumLingShouFightProp.IgnoremDef] = CLuaLingShouPropertyFight.SetParamIgnoremDef
SetParamFunctions[EnumLingShouFightProp.EnhanceZhaoHuan] = CLuaLingShouPropertyFight.SetParamEnhanceZhaoHuan
SetParamFunctions[EnumLingShouFightProp.EnhanceZhaoHuanMul] = CLuaLingShouPropertyFight.SetParamEnhanceZhaoHuanMul
SetParamFunctions[EnumLingShouFightProp.DizzyProbability] = CLuaLingShouPropertyFight.SetParamDizzyProbability
SetParamFunctions[EnumLingShouFightProp.SleepProbability] = CLuaLingShouPropertyFight.SetParamSleepProbability
SetParamFunctions[EnumLingShouFightProp.ChaosProbability] = CLuaLingShouPropertyFight.SetParamChaosProbability
SetParamFunctions[EnumLingShouFightProp.BindProbability] = CLuaLingShouPropertyFight.SetParamBindProbability
SetParamFunctions[EnumLingShouFightProp.SilenceProbability] = CLuaLingShouPropertyFight.SetParamSilenceProbability
SetParamFunctions[EnumLingShouFightProp.HpFullPow2] = CLuaLingShouPropertyFight.SetParamHpFullPow2
SetParamFunctions[EnumLingShouFightProp.HpFullPow1] = CLuaLingShouPropertyFight.SetParamHpFullPow1
SetParamFunctions[EnumLingShouFightProp.HpFullInit] = CLuaLingShouPropertyFight.SetParamHpFullInit
SetParamFunctions[EnumLingShouFightProp.MpFullPow1] = CLuaLingShouPropertyFight.SetParamMpFullPow1
SetParamFunctions[EnumLingShouFightProp.MpFullInit] = CLuaLingShouPropertyFight.SetParamMpFullInit
SetParamFunctions[EnumLingShouFightProp.MpRecoverPow1] = CLuaLingShouPropertyFight.SetParamMpRecoverPow1
SetParamFunctions[EnumLingShouFightProp.MpRecoverInit] = CLuaLingShouPropertyFight.SetParamMpRecoverInit
SetParamFunctions[EnumLingShouFightProp.PAttMinPow2] = CLuaLingShouPropertyFight.SetParamPAttMinPow2
SetParamFunctions[EnumLingShouFightProp.PAttMinPow1] = CLuaLingShouPropertyFight.SetParamPAttMinPow1
SetParamFunctions[EnumLingShouFightProp.PAttMinInit] = CLuaLingShouPropertyFight.SetParamPAttMinInit
SetParamFunctions[EnumLingShouFightProp.PAttMaxPow2] = CLuaLingShouPropertyFight.SetParamPAttMaxPow2
SetParamFunctions[EnumLingShouFightProp.PAttMaxPow1] = CLuaLingShouPropertyFight.SetParamPAttMaxPow1
SetParamFunctions[EnumLingShouFightProp.PAttMaxInit] = CLuaLingShouPropertyFight.SetParamPAttMaxInit
SetParamFunctions[EnumLingShouFightProp.PhitPow1] = CLuaLingShouPropertyFight.SetParamPhitPow1
SetParamFunctions[EnumLingShouFightProp.PHitInit] = CLuaLingShouPropertyFight.SetParamPHitInit
SetParamFunctions[EnumLingShouFightProp.PMissPow1] = CLuaLingShouPropertyFight.SetParamPMissPow1
SetParamFunctions[EnumLingShouFightProp.PMissInit] = CLuaLingShouPropertyFight.SetParamPMissInit
SetParamFunctions[EnumLingShouFightProp.PSpeedPow1] = CLuaLingShouPropertyFight.SetParamPSpeedPow1
SetParamFunctions[EnumLingShouFightProp.PSpeedInit] = CLuaLingShouPropertyFight.SetParamPSpeedInit
SetParamFunctions[EnumLingShouFightProp.PDefPow1] = CLuaLingShouPropertyFight.SetParamPDefPow1
SetParamFunctions[EnumLingShouFightProp.PDefInit] = CLuaLingShouPropertyFight.SetParamPDefInit
SetParamFunctions[EnumLingShouFightProp.PfatalPow1] = CLuaLingShouPropertyFight.SetParamPfatalPow1
SetParamFunctions[EnumLingShouFightProp.PFatalInit] = CLuaLingShouPropertyFight.SetParamPFatalInit
SetParamFunctions[EnumLingShouFightProp.MAttMinPow2] = CLuaLingShouPropertyFight.SetParamMAttMinPow2
SetParamFunctions[EnumLingShouFightProp.MAttMinPow1] = CLuaLingShouPropertyFight.SetParamMAttMinPow1
SetParamFunctions[EnumLingShouFightProp.MAttMinInit] = CLuaLingShouPropertyFight.SetParamMAttMinInit
SetParamFunctions[EnumLingShouFightProp.MAttMaxPow2] = CLuaLingShouPropertyFight.SetParamMAttMaxPow2
SetParamFunctions[EnumLingShouFightProp.MAttMaxPow1] = CLuaLingShouPropertyFight.SetParamMAttMaxPow1
SetParamFunctions[EnumLingShouFightProp.MAttMaxInit] = CLuaLingShouPropertyFight.SetParamMAttMaxInit
SetParamFunctions[EnumLingShouFightProp.MSpeedPow1] = CLuaLingShouPropertyFight.SetParamMSpeedPow1
SetParamFunctions[EnumLingShouFightProp.MSpeedInit] = CLuaLingShouPropertyFight.SetParamMSpeedInit
SetParamFunctions[EnumLingShouFightProp.MDefPow1] = CLuaLingShouPropertyFight.SetParamMDefPow1
SetParamFunctions[EnumLingShouFightProp.MDefInit] = CLuaLingShouPropertyFight.SetParamMDefInit
SetParamFunctions[EnumLingShouFightProp.MHitPow1] = CLuaLingShouPropertyFight.SetParamMHitPow1
SetParamFunctions[EnumLingShouFightProp.MHitInit] = CLuaLingShouPropertyFight.SetParamMHitInit
SetParamFunctions[EnumLingShouFightProp.MMissPow1] = CLuaLingShouPropertyFight.SetParamMMissPow1
SetParamFunctions[EnumLingShouFightProp.MMissInit] = CLuaLingShouPropertyFight.SetParamMMissInit
SetParamFunctions[EnumLingShouFightProp.MFatalPow1] = CLuaLingShouPropertyFight.SetParamMFatalPow1
SetParamFunctions[EnumLingShouFightProp.MFatalInit] = CLuaLingShouPropertyFight.SetParamMFatalInit
SetParamFunctions[EnumLingShouFightProp.BlockDamagePow1] = CLuaLingShouPropertyFight.SetParamBlockDamagePow1
SetParamFunctions[EnumLingShouFightProp.BlockDamageInit] = CLuaLingShouPropertyFight.SetParamBlockDamageInit
SetParamFunctions[EnumLingShouFightProp.RunSpeed] = CLuaLingShouPropertyFight.SetParamRunSpeed
SetParamFunctions[EnumLingShouFightProp.HpRecoverPow1] = CLuaLingShouPropertyFight.SetParamHpRecoverPow1
SetParamFunctions[EnumLingShouFightProp.HpRecoverInit] = CLuaLingShouPropertyFight.SetParamHpRecoverInit
SetParamFunctions[EnumLingShouFightProp.AntiBlockDamagePow1] = CLuaLingShouPropertyFight.SetParamAntiBlockDamagePow1
SetParamFunctions[EnumLingShouFightProp.AntiBlockDamageInit] = CLuaLingShouPropertyFight.SetParamAntiBlockDamageInit
SetParamFunctions[EnumLingShouFightProp.BBMax] = CLuaLingShouPropertyFight.SetParamBBMax
SetParamFunctions[EnumLingShouFightProp.AdjBBMax] = CLuaLingShouPropertyFight.SetParamAdjBBMax
SetParamFunctions[EnumLingShouFightProp.AdjAntiBreak] = CLuaLingShouPropertyFight.SetParamAdjAntiBreak
SetParamFunctions[EnumLingShouFightProp.AdjAntiPushPull] = CLuaLingShouPropertyFight.SetParamAdjAntiPushPull
SetParamFunctions[EnumLingShouFightProp.AdjAntiPetrify] = CLuaLingShouPropertyFight.SetParamAdjAntiPetrify
SetParamFunctions[EnumLingShouFightProp.MulAntiPetrify] = CLuaLingShouPropertyFight.SetParamMulAntiPetrify
SetParamFunctions[EnumLingShouFightProp.AdjAntiTie] = CLuaLingShouPropertyFight.SetParamAdjAntiTie
SetParamFunctions[EnumLingShouFightProp.MulAntiTie] = CLuaLingShouPropertyFight.SetParamMulAntiTie
SetParamFunctions[EnumLingShouFightProp.AdjEnhancePetrify] = CLuaLingShouPropertyFight.SetParamAdjEnhancePetrify
SetParamFunctions[EnumLingShouFightProp.MulEnhancePetrify] = CLuaLingShouPropertyFight.SetParamMulEnhancePetrify
SetParamFunctions[EnumLingShouFightProp.AdjEnhanceTie] = CLuaLingShouPropertyFight.SetParamAdjEnhanceTie
SetParamFunctions[EnumLingShouFightProp.MulEnhanceTie] = CLuaLingShouPropertyFight.SetParamMulEnhanceTie
SetParamFunctions[EnumLingShouFightProp.TieProbability] = CLuaLingShouPropertyFight.SetParamTieProbability
SetParamFunctions[EnumLingShouFightProp.InitStrZizhi] = CLuaLingShouPropertyFight.SetParamInitStrZizhi
SetParamFunctions[EnumLingShouFightProp.InitCorZizhi] = CLuaLingShouPropertyFight.SetParamInitCorZizhi
SetParamFunctions[EnumLingShouFightProp.InitStaZizhi] = CLuaLingShouPropertyFight.SetParamInitStaZizhi
SetParamFunctions[EnumLingShouFightProp.InitAgiZizhi] = CLuaLingShouPropertyFight.SetParamInitAgiZizhi
SetParamFunctions[EnumLingShouFightProp.InitIntZizhi] = CLuaLingShouPropertyFight.SetParamInitIntZizhi
SetParamFunctions[EnumLingShouFightProp.Wuxing] = CLuaLingShouPropertyFight.SetParamWuxing
SetParamFunctions[EnumLingShouFightProp.Xiuwei] = CLuaLingShouPropertyFight.SetParamXiuwei
SetParamFunctions[EnumLingShouFightProp.SkillNum] = CLuaLingShouPropertyFight.SetParamSkillNum
SetParamFunctions[EnumLingShouFightProp.GrowFactor] = CLuaLingShouPropertyFight.SetParamGrowFactor
SetParamFunctions[EnumLingShouFightProp.AdjEnhanceBeHeal] = CLuaLingShouPropertyFight.SetParamAdjEnhanceBeHeal
SetParamFunctions[EnumLingShouFightProp.MulEnhanceBeHeal] = CLuaLingShouPropertyFight.SetParamMulEnhanceBeHeal
SetParamFunctions[EnumLingShouFightProp.LookUpCor] = CLuaLingShouPropertyFight.SetParamLookUpCor
SetParamFunctions[EnumLingShouFightProp.LookUpSta] = CLuaLingShouPropertyFight.SetParamLookUpSta
SetParamFunctions[EnumLingShouFightProp.LookUpStr] = CLuaLingShouPropertyFight.SetParamLookUpStr
SetParamFunctions[EnumLingShouFightProp.LookUpInt] = CLuaLingShouPropertyFight.SetParamLookUpInt
SetParamFunctions[EnumLingShouFightProp.LookUpAgi] = CLuaLingShouPropertyFight.SetParamLookUpAgi
SetParamFunctions[EnumLingShouFightProp.LookUpHpFull] = CLuaLingShouPropertyFight.SetParamLookUpHpFull
SetParamFunctions[EnumLingShouFightProp.LookUpMpFull] = CLuaLingShouPropertyFight.SetParamLookUpMpFull
SetParamFunctions[EnumLingShouFightProp.LookUppAttMin] = CLuaLingShouPropertyFight.SetParamLookUppAttMin
SetParamFunctions[EnumLingShouFightProp.LookUppAttMax] = CLuaLingShouPropertyFight.SetParamLookUppAttMax
SetParamFunctions[EnumLingShouFightProp.LookUppHit] = CLuaLingShouPropertyFight.SetParamLookUppHit
SetParamFunctions[EnumLingShouFightProp.LookUppMiss] = CLuaLingShouPropertyFight.SetParamLookUppMiss
SetParamFunctions[EnumLingShouFightProp.LookUppDef] = CLuaLingShouPropertyFight.SetParamLookUppDef
SetParamFunctions[EnumLingShouFightProp.LookUpmAttMin] = CLuaLingShouPropertyFight.SetParamLookUpmAttMin
SetParamFunctions[EnumLingShouFightProp.LookUpmAttMax] = CLuaLingShouPropertyFight.SetParamLookUpmAttMax
SetParamFunctions[EnumLingShouFightProp.LookUpmHit] = CLuaLingShouPropertyFight.SetParamLookUpmHit
SetParamFunctions[EnumLingShouFightProp.LookUpmMiss] = CLuaLingShouPropertyFight.SetParamLookUpmMiss
SetParamFunctions[EnumLingShouFightProp.LookUpmDef] = CLuaLingShouPropertyFight.SetParamLookUpmDef
SetParamFunctions[EnumLingShouFightProp.AdjHpFull2] = CLuaLingShouPropertyFight.SetParamAdjHpFull2
SetParamFunctions[EnumLingShouFightProp.AdjpAttMin2] = CLuaLingShouPropertyFight.SetParamAdjpAttMin2
SetParamFunctions[EnumLingShouFightProp.AdjpAttMax2] = CLuaLingShouPropertyFight.SetParamAdjpAttMax2
SetParamFunctions[EnumLingShouFightProp.AdjmAttMin2] = CLuaLingShouPropertyFight.SetParamAdjmAttMin2
SetParamFunctions[EnumLingShouFightProp.AdjmAttMax2] = CLuaLingShouPropertyFight.SetParamAdjmAttMax2
SetParamFunctions[EnumLingShouFightProp.LingShouType] = CLuaLingShouPropertyFight.SetParamLingShouType
SetParamFunctions[EnumLingShouFightProp.EnhanceLingShou] = CLuaLingShouPropertyFight.SetParamEnhanceLingShou
SetParamFunctions[EnumLingShouFightProp.EnhanceLingShouMul] = CLuaLingShouPropertyFight.SetParamEnhanceLingShouMul
SetParamFunctions[EnumLingShouFightProp.AdjAntiAoe] = CLuaLingShouPropertyFight.SetParamAdjAntiAoe
SetParamFunctions[EnumLingShouFightProp.AdjAntiBlind] = CLuaLingShouPropertyFight.SetParamAdjAntiBlind
SetParamFunctions[EnumLingShouFightProp.MulAntiBlind] = CLuaLingShouPropertyFight.SetParamMulAntiBlind
SetParamFunctions[EnumLingShouFightProp.AdjAntiDecelerate] = CLuaLingShouPropertyFight.SetParamAdjAntiDecelerate
SetParamFunctions[EnumLingShouFightProp.MulAntiDecelerate] = CLuaLingShouPropertyFight.SetParamMulAntiDecelerate
SetParamFunctions[EnumLingShouFightProp.MinGrade] = CLuaLingShouPropertyFight.SetParamMinGrade
SetParamFunctions[EnumLingShouFightProp.CharacterCor] = CLuaLingShouPropertyFight.SetParamCharacterCor
SetParamFunctions[EnumLingShouFightProp.CharacterSta] = CLuaLingShouPropertyFight.SetParamCharacterSta
SetParamFunctions[EnumLingShouFightProp.CharacterStr] = CLuaLingShouPropertyFight.SetParamCharacterStr
SetParamFunctions[EnumLingShouFightProp.CharacterInt] = CLuaLingShouPropertyFight.SetParamCharacterInt
SetParamFunctions[EnumLingShouFightProp.CharacterAgi] = CLuaLingShouPropertyFight.SetParamCharacterAgi
SetParamFunctions[EnumLingShouFightProp.EnhanceDaoKe] = CLuaLingShouPropertyFight.SetParamEnhanceDaoKe
SetParamFunctions[EnumLingShouFightProp.EnhanceDaoKeMul] = CLuaLingShouPropertyFight.SetParamEnhanceDaoKeMul
SetParamFunctions[EnumLingShouFightProp.ZuoQiSpeed] = CLuaLingShouPropertyFight.SetParamZuoQiSpeed
SetParamFunctions[EnumLingShouFightProp.AdjEnhanceBianHu] = CLuaLingShouPropertyFight.SetParamAdjEnhanceBianHu
SetParamFunctions[EnumLingShouFightProp.MulEnhanceBianHu] = CLuaLingShouPropertyFight.SetParamMulEnhanceBianHu
SetParamFunctions[EnumLingShouFightProp.AdjAntiBianHu] = CLuaLingShouPropertyFight.SetParamAdjAntiBianHu
SetParamFunctions[EnumLingShouFightProp.MulAntiBianHu] = CLuaLingShouPropertyFight.SetParamMulAntiBianHu
SetParamFunctions[EnumLingShouFightProp.EnhanceXiaKe] = CLuaLingShouPropertyFight.SetParamEnhanceXiaKe
SetParamFunctions[EnumLingShouFightProp.EnhanceXiaKeMul] = CLuaLingShouPropertyFight.SetParamEnhanceXiaKeMul
SetParamFunctions[EnumLingShouFightProp.TieTimeChange] = CLuaLingShouPropertyFight.SetParamTieTimeChange
SetParamFunctions[EnumLingShouFightProp.EnhanceYanShi] = CLuaLingShouPropertyFight.SetParamEnhanceYanShi
SetParamFunctions[EnumLingShouFightProp.EnhanceYanShiMul] = CLuaLingShouPropertyFight.SetParamEnhanceYanShiMul
SetParamFunctions[EnumLingShouFightProp.lifetimeNoReduceRate] = CLuaLingShouPropertyFight.SetParamlifetimeNoReduceRate
SetParamFunctions[EnumLingShouFightProp.AddLSHpDurgImprove] = CLuaLingShouPropertyFight.SetParamAddLSHpDurgImprove
SetParamFunctions[EnumLingShouFightProp.AddHpDurgImprove] = CLuaLingShouPropertyFight.SetParamAddHpDurgImprove
SetParamFunctions[EnumLingShouFightProp.AddMpDurgImprove] = CLuaLingShouPropertyFight.SetParamAddMpDurgImprove
SetParamFunctions[EnumLingShouFightProp.FireHurtReduce] = CLuaLingShouPropertyFight.SetParamFireHurtReduce
SetParamFunctions[EnumLingShouFightProp.ThunderHurtReduce] = CLuaLingShouPropertyFight.SetParamThunderHurtReduce
SetParamFunctions[EnumLingShouFightProp.IceHurtReduce] = CLuaLingShouPropertyFight.SetParamIceHurtReduce
SetParamFunctions[EnumLingShouFightProp.PoisonHurtReduce] = CLuaLingShouPropertyFight.SetParamPoisonHurtReduce
SetParamFunctions[EnumLingShouFightProp.WindHurtReduce] = CLuaLingShouPropertyFight.SetParamWindHurtReduce
SetParamFunctions[EnumLingShouFightProp.LightHurtReduce] = CLuaLingShouPropertyFight.SetParamLightHurtReduce
SetParamFunctions[EnumLingShouFightProp.IllusionHurtReduce] = CLuaLingShouPropertyFight.SetParamIllusionHurtReduce
SetParamFunctions[EnumLingShouFightProp.FakepDef] = CLuaLingShouPropertyFight.SetParamFakepDef
SetParamFunctions[EnumLingShouFightProp.FakemDef] = CLuaLingShouPropertyFight.SetParamFakemDef
SetParamFunctions[EnumLingShouFightProp.AdjEnhanceWater] = CLuaLingShouPropertyFight.SetParamAdjEnhanceWater
SetParamFunctions[EnumLingShouFightProp.MulEnhanceWater] = CLuaLingShouPropertyFight.SetParamMulEnhanceWater
SetParamFunctions[EnumLingShouFightProp.AdjAntiWater] = CLuaLingShouPropertyFight.SetParamAdjAntiWater
SetParamFunctions[EnumLingShouFightProp.MulAntiWater] = CLuaLingShouPropertyFight.SetParamMulAntiWater
SetParamFunctions[EnumLingShouFightProp.WaterHurtReduce] = CLuaLingShouPropertyFight.SetParamWaterHurtReduce
SetParamFunctions[EnumLingShouFightProp.AdjAntiFreeze] = CLuaLingShouPropertyFight.SetParamAdjAntiFreeze
SetParamFunctions[EnumLingShouFightProp.MulAntiFreeze] = CLuaLingShouPropertyFight.SetParamMulAntiFreeze
SetParamFunctions[EnumLingShouFightProp.AdjEnhanceFreeze] = CLuaLingShouPropertyFight.SetParamAdjEnhanceFreeze
SetParamFunctions[EnumLingShouFightProp.MulEnhanceFreeze] = CLuaLingShouPropertyFight.SetParamMulEnhanceFreeze
SetParamFunctions[EnumLingShouFightProp.EnhanceHuaHun] = CLuaLingShouPropertyFight.SetParamEnhanceHuaHun
SetParamFunctions[EnumLingShouFightProp.EnhanceHuaHunMul] = CLuaLingShouPropertyFight.SetParamEnhanceHuaHunMul
SetParamFunctions[EnumLingShouFightProp.TrueSightProb] = CLuaLingShouPropertyFight.SetParamTrueSightProb
SetParamFunctions[EnumLingShouFightProp.LSBBGrade] = CLuaLingShouPropertyFight.SetParamLSBBGrade
SetParamFunctions[EnumLingShouFightProp.LSBBQuality] = CLuaLingShouPropertyFight.SetParamLSBBQuality
SetParamFunctions[EnumLingShouFightProp.AdjIgnoreAntiFire] = CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiFire
SetParamFunctions[EnumLingShouFightProp.AdjIgnoreAntiThunder] = CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiThunder
SetParamFunctions[EnumLingShouFightProp.AdjIgnoreAntiIce] = CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiIce
SetParamFunctions[EnumLingShouFightProp.AdjIgnoreAntiPoison] = CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiPoison
SetParamFunctions[EnumLingShouFightProp.AdjIgnoreAntiWind] = CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiWind
SetParamFunctions[EnumLingShouFightProp.AdjIgnoreAntiLight] = CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiLight
SetParamFunctions[EnumLingShouFightProp.AdjIgnoreAntiIllusion] = CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiIllusion
SetParamFunctions[EnumLingShouFightProp.IgnoreAntiFireLSMul] = CLuaLingShouPropertyFight.SetParamIgnoreAntiFireLSMul
SetParamFunctions[EnumLingShouFightProp.IgnoreAntiThunderLSMul] = CLuaLingShouPropertyFight.SetParamIgnoreAntiThunderLSMul
SetParamFunctions[EnumLingShouFightProp.IgnoreAntiIceLSMul] = CLuaLingShouPropertyFight.SetParamIgnoreAntiIceLSMul
SetParamFunctions[EnumLingShouFightProp.IgnoreAntiPoisonLSMul] = CLuaLingShouPropertyFight.SetParamIgnoreAntiPoisonLSMul
SetParamFunctions[EnumLingShouFightProp.IgnoreAntiWindLSMul] = CLuaLingShouPropertyFight.SetParamIgnoreAntiWindLSMul
SetParamFunctions[EnumLingShouFightProp.IgnoreAntiLightLSMul] = CLuaLingShouPropertyFight.SetParamIgnoreAntiLightLSMul
SetParamFunctions[EnumLingShouFightProp.IgnoreAntiIllusionLSMul] = CLuaLingShouPropertyFight.SetParamIgnoreAntiIllusionLSMul
SetParamFunctions[EnumLingShouFightProp.AdjIgnoreAntiWater] = CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiWater
SetParamFunctions[EnumLingShouFightProp.IgnoreAntiWaterLSMul] = CLuaLingShouPropertyFight.SetParamIgnoreAntiWaterLSMul
SetParamFunctions[EnumLingShouFightProp.MulSpeed2] = CLuaLingShouPropertyFight.SetParamMulSpeed2
SetParamFunctions[EnumLingShouFightProp.MulConsumeMp] = CLuaLingShouPropertyFight.SetParamMulConsumeMp
SetParamFunctions[EnumLingShouFightProp.AdjAgi2] = CLuaLingShouPropertyFight.SetParamAdjAgi2
SetParamFunctions[EnumLingShouFightProp.AdjAntiBianHu2] = CLuaLingShouPropertyFight.SetParamAdjAntiBianHu2
SetParamFunctions[EnumLingShouFightProp.AdjAntiBind2] = CLuaLingShouPropertyFight.SetParamAdjAntiBind2
SetParamFunctions[EnumLingShouFightProp.AdjAntiBlind2] = CLuaLingShouPropertyFight.SetParamAdjAntiBlind2
SetParamFunctions[EnumLingShouFightProp.AdjAntiBlock2] = CLuaLingShouPropertyFight.SetParamAdjAntiBlock2
SetParamFunctions[EnumLingShouFightProp.AdjAntiBlockDamage2] = CLuaLingShouPropertyFight.SetParamAdjAntiBlockDamage2
SetParamFunctions[EnumLingShouFightProp.AdjAntiChaos2] = CLuaLingShouPropertyFight.SetParamAdjAntiChaos2
SetParamFunctions[EnumLingShouFightProp.AdjAntiDecelerate2] = CLuaLingShouPropertyFight.SetParamAdjAntiDecelerate2
SetParamFunctions[EnumLingShouFightProp.AdjAntiDizzy2] = CLuaLingShouPropertyFight.SetParamAdjAntiDizzy2
SetParamFunctions[EnumLingShouFightProp.AdjAntiFire2] = CLuaLingShouPropertyFight.SetParamAdjAntiFire2
SetParamFunctions[EnumLingShouFightProp.AdjAntiFreeze2] = CLuaLingShouPropertyFight.SetParamAdjAntiFreeze2
SetParamFunctions[EnumLingShouFightProp.AdjAntiIce2] = CLuaLingShouPropertyFight.SetParamAdjAntiIce2
SetParamFunctions[EnumLingShouFightProp.AdjAntiIllusion2] = CLuaLingShouPropertyFight.SetParamAdjAntiIllusion2
SetParamFunctions[EnumLingShouFightProp.AdjAntiLight2] = CLuaLingShouPropertyFight.SetParamAdjAntiLight2
SetParamFunctions[EnumLingShouFightProp.AdjAntimFatal2] = CLuaLingShouPropertyFight.SetParamAdjAntimFatal2
SetParamFunctions[EnumLingShouFightProp.AdjAntimFatalDamage2] = CLuaLingShouPropertyFight.SetParamAdjAntimFatalDamage2
SetParamFunctions[EnumLingShouFightProp.AdjAntiPetrify2] = CLuaLingShouPropertyFight.SetParamAdjAntiPetrify2
SetParamFunctions[EnumLingShouFightProp.AdjAntipFatal2] = CLuaLingShouPropertyFight.SetParamAdjAntipFatal2
SetParamFunctions[EnumLingShouFightProp.AdjAntipFatalDamage2] = CLuaLingShouPropertyFight.SetParamAdjAntipFatalDamage2
SetParamFunctions[EnumLingShouFightProp.AdjAntiPoison2] = CLuaLingShouPropertyFight.SetParamAdjAntiPoison2
SetParamFunctions[EnumLingShouFightProp.AdjAntiSilence2] = CLuaLingShouPropertyFight.SetParamAdjAntiSilence2
SetParamFunctions[EnumLingShouFightProp.AdjAntiSleep2] = CLuaLingShouPropertyFight.SetParamAdjAntiSleep2
SetParamFunctions[EnumLingShouFightProp.AdjAntiThunder2] = CLuaLingShouPropertyFight.SetParamAdjAntiThunder2
SetParamFunctions[EnumLingShouFightProp.AdjAntiTie2] = CLuaLingShouPropertyFight.SetParamAdjAntiTie2
SetParamFunctions[EnumLingShouFightProp.AdjAntiWater2] = CLuaLingShouPropertyFight.SetParamAdjAntiWater2
SetParamFunctions[EnumLingShouFightProp.AdjAntiWind2] = CLuaLingShouPropertyFight.SetParamAdjAntiWind2
SetParamFunctions[EnumLingShouFightProp.AdjBlock2] = CLuaLingShouPropertyFight.SetParamAdjBlock2
SetParamFunctions[EnumLingShouFightProp.AdjBlockDamage2] = CLuaLingShouPropertyFight.SetParamAdjBlockDamage2
SetParamFunctions[EnumLingShouFightProp.AdjCor2] = CLuaLingShouPropertyFight.SetParamAdjCor2
SetParamFunctions[EnumLingShouFightProp.AdjEnhanceBianHu2] = CLuaLingShouPropertyFight.SetParamAdjEnhanceBianHu2
SetParamFunctions[EnumLingShouFightProp.AdjEnhanceBind2] = CLuaLingShouPropertyFight.SetParamAdjEnhanceBind2
SetParamFunctions[EnumLingShouFightProp.AdjEnhanceChaos2] = CLuaLingShouPropertyFight.SetParamAdjEnhanceChaos2
SetParamFunctions[EnumLingShouFightProp.AdjEnhanceDizzy2] = CLuaLingShouPropertyFight.SetParamAdjEnhanceDizzy2
SetParamFunctions[EnumLingShouFightProp.AdjEnhanceFire2] = CLuaLingShouPropertyFight.SetParamAdjEnhanceFire2
SetParamFunctions[EnumLingShouFightProp.AdjEnhanceFreeze2] = CLuaLingShouPropertyFight.SetParamAdjEnhanceFreeze2
SetParamFunctions[EnumLingShouFightProp.AdjEnhanceIce2] = CLuaLingShouPropertyFight.SetParamAdjEnhanceIce2
SetParamFunctions[EnumLingShouFightProp.AdjEnhanceIllusion2] = CLuaLingShouPropertyFight.SetParamAdjEnhanceIllusion2
SetParamFunctions[EnumLingShouFightProp.AdjEnhanceLight2] = CLuaLingShouPropertyFight.SetParamAdjEnhanceLight2
SetParamFunctions[EnumLingShouFightProp.AdjEnhancePetrify2] = CLuaLingShouPropertyFight.SetParamAdjEnhancePetrify2
SetParamFunctions[EnumLingShouFightProp.AdjEnhancePoison2] = CLuaLingShouPropertyFight.SetParamAdjEnhancePoison2
SetParamFunctions[EnumLingShouFightProp.AdjEnhanceSilence2] = CLuaLingShouPropertyFight.SetParamAdjEnhanceSilence2
SetParamFunctions[EnumLingShouFightProp.AdjEnhanceSleep2] = CLuaLingShouPropertyFight.SetParamAdjEnhanceSleep2
SetParamFunctions[EnumLingShouFightProp.AdjEnhanceThunder2] = CLuaLingShouPropertyFight.SetParamAdjEnhanceThunder2
SetParamFunctions[EnumLingShouFightProp.AdjEnhanceTie2] = CLuaLingShouPropertyFight.SetParamAdjEnhanceTie2
SetParamFunctions[EnumLingShouFightProp.AdjEnhanceWater2] = CLuaLingShouPropertyFight.SetParamAdjEnhanceWater2
SetParamFunctions[EnumLingShouFightProp.AdjEnhanceWind2] = CLuaLingShouPropertyFight.SetParamAdjEnhanceWind2
SetParamFunctions[EnumLingShouFightProp.AdjIgnoreAntiFire2] = CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiFire2
SetParamFunctions[EnumLingShouFightProp.AdjIgnoreAntiIce2] = CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiIce2
SetParamFunctions[EnumLingShouFightProp.AdjIgnoreAntiIllusion2] = CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiIllusion2
SetParamFunctions[EnumLingShouFightProp.AdjIgnoreAntiLight2] = CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiLight2
SetParamFunctions[EnumLingShouFightProp.AdjIgnoreAntiPoison2] = CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiPoison2
SetParamFunctions[EnumLingShouFightProp.AdjIgnoreAntiThunder2] = CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiThunder2
SetParamFunctions[EnumLingShouFightProp.AdjIgnoreAntiWater2] = CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiWater2
SetParamFunctions[EnumLingShouFightProp.AdjIgnoreAntiWind2] = CLuaLingShouPropertyFight.SetParamAdjIgnoreAntiWind2
SetParamFunctions[EnumLingShouFightProp.AdjInt2] = CLuaLingShouPropertyFight.SetParamAdjInt2
SetParamFunctions[EnumLingShouFightProp.AdjmDef2] = CLuaLingShouPropertyFight.SetParamAdjmDef2
SetParamFunctions[EnumLingShouFightProp.AdjmFatal2] = CLuaLingShouPropertyFight.SetParamAdjmFatal2
SetParamFunctions[EnumLingShouFightProp.AdjmFatalDamage2] = CLuaLingShouPropertyFight.SetParamAdjmFatalDamage2
SetParamFunctions[EnumLingShouFightProp.AdjmHit2] = CLuaLingShouPropertyFight.SetParamAdjmHit2
SetParamFunctions[EnumLingShouFightProp.AdjmHurt2] = CLuaLingShouPropertyFight.SetParamAdjmHurt2
SetParamFunctions[EnumLingShouFightProp.AdjmMiss2] = CLuaLingShouPropertyFight.SetParamAdjmMiss2
SetParamFunctions[EnumLingShouFightProp.AdjmSpeed2] = CLuaLingShouPropertyFight.SetParamAdjmSpeed2
SetParamFunctions[EnumLingShouFightProp.AdjpDef2] = CLuaLingShouPropertyFight.SetParamAdjpDef2
SetParamFunctions[EnumLingShouFightProp.AdjpFatal2] = CLuaLingShouPropertyFight.SetParamAdjpFatal2
SetParamFunctions[EnumLingShouFightProp.AdjpFatalDamage2] = CLuaLingShouPropertyFight.SetParamAdjpFatalDamage2
SetParamFunctions[EnumLingShouFightProp.AdjpHit2] = CLuaLingShouPropertyFight.SetParamAdjpHit2
SetParamFunctions[EnumLingShouFightProp.AdjpHurt2] = CLuaLingShouPropertyFight.SetParamAdjpHurt2
SetParamFunctions[EnumLingShouFightProp.AdjpMiss2] = CLuaLingShouPropertyFight.SetParamAdjpMiss2
SetParamFunctions[EnumLingShouFightProp.AdjpSpeed2] = CLuaLingShouPropertyFight.SetParamAdjpSpeed2
SetParamFunctions[EnumLingShouFightProp.AdjStr2] = CLuaLingShouPropertyFight.SetParamAdjStr2
SetParamFunctions[EnumLingShouFightProp.LSpAttMin] = CLuaLingShouPropertyFight.SetParamLSpAttMin
SetParamFunctions[EnumLingShouFightProp.LSpAttMax] = CLuaLingShouPropertyFight.SetParamLSpAttMax
SetParamFunctions[EnumLingShouFightProp.LSmAttMin] = CLuaLingShouPropertyFight.SetParamLSmAttMin
SetParamFunctions[EnumLingShouFightProp.LSmAttMax] = CLuaLingShouPropertyFight.SetParamLSmAttMax
SetParamFunctions[EnumLingShouFightProp.LSpFatal] = CLuaLingShouPropertyFight.SetParamLSpFatal
SetParamFunctions[EnumLingShouFightProp.LSpFatalDamage] = CLuaLingShouPropertyFight.SetParamLSpFatalDamage
SetParamFunctions[EnumLingShouFightProp.LSmFatal] = CLuaLingShouPropertyFight.SetParamLSmFatal
SetParamFunctions[EnumLingShouFightProp.LSmFatalDamage] = CLuaLingShouPropertyFight.SetParamLSmFatalDamage
SetParamFunctions[EnumLingShouFightProp.LSAntiBlock] = CLuaLingShouPropertyFight.SetParamLSAntiBlock
SetParamFunctions[EnumLingShouFightProp.LSAntiBlockDamage] = CLuaLingShouPropertyFight.SetParamLSAntiBlockDamage
SetParamFunctions[EnumLingShouFightProp.LSIgnoreAntiFire] = CLuaLingShouPropertyFight.SetParamLSIgnoreAntiFire
SetParamFunctions[EnumLingShouFightProp.LSIgnoreAntiThunder] = CLuaLingShouPropertyFight.SetParamLSIgnoreAntiThunder
SetParamFunctions[EnumLingShouFightProp.LSIgnoreAntiIce] = CLuaLingShouPropertyFight.SetParamLSIgnoreAntiIce
SetParamFunctions[EnumLingShouFightProp.LSIgnoreAntiPoison] = CLuaLingShouPropertyFight.SetParamLSIgnoreAntiPoison
SetParamFunctions[EnumLingShouFightProp.LSIgnoreAntiWind] = CLuaLingShouPropertyFight.SetParamLSIgnoreAntiWind
SetParamFunctions[EnumLingShouFightProp.LSIgnoreAntiLight] = CLuaLingShouPropertyFight.SetParamLSIgnoreAntiLight
SetParamFunctions[EnumLingShouFightProp.LSIgnoreAntiIllusion] = CLuaLingShouPropertyFight.SetParamLSIgnoreAntiIllusion
SetParamFunctions[EnumLingShouFightProp.LSIgnoreAntiWater] = CLuaLingShouPropertyFight.SetParamLSIgnoreAntiWater
SetParamFunctions[EnumLingShouFightProp.LSpAttMin_adj] = CLuaLingShouPropertyFight.SetParamLSpAttMin_adj
SetParamFunctions[EnumLingShouFightProp.LSpAttMax_adj] = CLuaLingShouPropertyFight.SetParamLSpAttMax_adj
SetParamFunctions[EnumLingShouFightProp.LSmAttMin_adj] = CLuaLingShouPropertyFight.SetParamLSmAttMin_adj
SetParamFunctions[EnumLingShouFightProp.LSmAttMax_adj] = CLuaLingShouPropertyFight.SetParamLSmAttMax_adj
SetParamFunctions[EnumLingShouFightProp.LSpFatal_adj] = CLuaLingShouPropertyFight.SetParamLSpFatal_adj
SetParamFunctions[EnumLingShouFightProp.LSpFatalDamage_adj] = CLuaLingShouPropertyFight.SetParamLSpFatalDamage_adj
SetParamFunctions[EnumLingShouFightProp.LSmFatal_adj] = CLuaLingShouPropertyFight.SetParamLSmFatal_adj
SetParamFunctions[EnumLingShouFightProp.LSmFatalDamage_adj] = CLuaLingShouPropertyFight.SetParamLSmFatalDamage_adj
SetParamFunctions[EnumLingShouFightProp.LSAntiBlock_adj] = CLuaLingShouPropertyFight.SetParamLSAntiBlock_adj
SetParamFunctions[EnumLingShouFightProp.LSAntiBlockDamage_adj] = CLuaLingShouPropertyFight.SetParamLSAntiBlockDamage_adj
SetParamFunctions[EnumLingShouFightProp.LSIgnoreAntiFire_adj] = CLuaLingShouPropertyFight.SetParamLSIgnoreAntiFire_adj
SetParamFunctions[EnumLingShouFightProp.LSIgnoreAntiThunder_adj] = CLuaLingShouPropertyFight.SetParamLSIgnoreAntiThunder_adj
SetParamFunctions[EnumLingShouFightProp.LSIgnoreAntiIce_adj] = CLuaLingShouPropertyFight.SetParamLSIgnoreAntiIce_adj
SetParamFunctions[EnumLingShouFightProp.LSIgnoreAntiPoison_adj] = CLuaLingShouPropertyFight.SetParamLSIgnoreAntiPoison_adj
SetParamFunctions[EnumLingShouFightProp.LSIgnoreAntiWind_adj] = CLuaLingShouPropertyFight.SetParamLSIgnoreAntiWind_adj
SetParamFunctions[EnumLingShouFightProp.LSIgnoreAntiLight_adj] = CLuaLingShouPropertyFight.SetParamLSIgnoreAntiLight_adj
SetParamFunctions[EnumLingShouFightProp.LSIgnoreAntiIllusion_adj] = CLuaLingShouPropertyFight.SetParamLSIgnoreAntiIllusion_adj
SetParamFunctions[EnumLingShouFightProp.LSIgnoreAntiWater_adj] = CLuaLingShouPropertyFight.SetParamLSIgnoreAntiWater_adj
SetParamFunctions[EnumLingShouFightProp.LSpAttMin_jc] = CLuaLingShouPropertyFight.SetParamLSpAttMin_jc
SetParamFunctions[EnumLingShouFightProp.LSpAttMax_jc] = CLuaLingShouPropertyFight.SetParamLSpAttMax_jc
SetParamFunctions[EnumLingShouFightProp.LSmAttMin_jc] = CLuaLingShouPropertyFight.SetParamLSmAttMin_jc
SetParamFunctions[EnumLingShouFightProp.LSmAttMax_jc] = CLuaLingShouPropertyFight.SetParamLSmAttMax_jc
SetParamFunctions[EnumLingShouFightProp.LSpFatal_jc] = CLuaLingShouPropertyFight.SetParamLSpFatal_jc
SetParamFunctions[EnumLingShouFightProp.LSpFatalDamage_jc] = CLuaLingShouPropertyFight.SetParamLSpFatalDamage_jc
SetParamFunctions[EnumLingShouFightProp.LSmFatal_jc] = CLuaLingShouPropertyFight.SetParamLSmFatal_jc
SetParamFunctions[EnumLingShouFightProp.LSmFatalDamage_jc] = CLuaLingShouPropertyFight.SetParamLSmFatalDamage_jc
SetParamFunctions[EnumLingShouFightProp.LSAntiBlock_jc] = CLuaLingShouPropertyFight.SetParamLSAntiBlock_jc
SetParamFunctions[EnumLingShouFightProp.LSAntiBlockDamage_jc] = CLuaLingShouPropertyFight.SetParamLSAntiBlockDamage_jc
SetParamFunctions[EnumLingShouFightProp.LSIgnoreAntiFire_jc] = CLuaLingShouPropertyFight.SetParamLSIgnoreAntiFire_jc
SetParamFunctions[EnumLingShouFightProp.LSIgnoreAntiThunder_jc] = CLuaLingShouPropertyFight.SetParamLSIgnoreAntiThunder_jc
SetParamFunctions[EnumLingShouFightProp.LSIgnoreAntiIce_jc] = CLuaLingShouPropertyFight.SetParamLSIgnoreAntiIce_jc
SetParamFunctions[EnumLingShouFightProp.LSIgnoreAntiPoison_jc] = CLuaLingShouPropertyFight.SetParamLSIgnoreAntiPoison_jc
SetParamFunctions[EnumLingShouFightProp.LSIgnoreAntiWind_jc] = CLuaLingShouPropertyFight.SetParamLSIgnoreAntiWind_jc
SetParamFunctions[EnumLingShouFightProp.LSIgnoreAntiLight_jc] = CLuaLingShouPropertyFight.SetParamLSIgnoreAntiLight_jc
SetParamFunctions[EnumLingShouFightProp.LSIgnoreAntiIllusion_jc] = CLuaLingShouPropertyFight.SetParamLSIgnoreAntiIllusion_jc
SetParamFunctions[EnumLingShouFightProp.LSIgnoreAntiWater_jc] = CLuaLingShouPropertyFight.SetParamLSIgnoreAntiWater_jc
SetParamFunctions[EnumLingShouFightProp.LSIgnoreAntiFire_mul] = CLuaLingShouPropertyFight.SetParamLSIgnoreAntiFire_mul
SetParamFunctions[EnumLingShouFightProp.LSIgnoreAntiThunder_mul] = CLuaLingShouPropertyFight.SetParamLSIgnoreAntiThunder_mul
SetParamFunctions[EnumLingShouFightProp.LSIgnoreAntiIce_mul] = CLuaLingShouPropertyFight.SetParamLSIgnoreAntiIce_mul
SetParamFunctions[EnumLingShouFightProp.LSIgnoreAntiPoison_mul] = CLuaLingShouPropertyFight.SetParamLSIgnoreAntiPoison_mul
SetParamFunctions[EnumLingShouFightProp.LSIgnoreAntiWind_mul] = CLuaLingShouPropertyFight.SetParamLSIgnoreAntiWind_mul
SetParamFunctions[EnumLingShouFightProp.LSIgnoreAntiLight_mul] = CLuaLingShouPropertyFight.SetParamLSIgnoreAntiLight_mul
SetParamFunctions[EnumLingShouFightProp.LSIgnoreAntiIllusion_mul] = CLuaLingShouPropertyFight.SetParamLSIgnoreAntiIllusion_mul
SetParamFunctions[EnumLingShouFightProp.LSIgnoreAntiWater_mul] = CLuaLingShouPropertyFight.SetParamLSIgnoreAntiWater_mul
SetParamFunctions[EnumLingShouFightProp.JieBan_Child_QiChangColor] = CLuaLingShouPropertyFight.SetParamJieBan_Child_QiChangColor
SetParamFunctions[EnumLingShouFightProp.JieBan_LingShou_Ratio] = CLuaLingShouPropertyFight.SetParamJieBan_LingShou_Ratio
SetParamFunctions[EnumLingShouFightProp.JieBan_LingShou_QinMi] = CLuaLingShouPropertyFight.SetParamJieBan_LingShou_QinMi
SetParamFunctions[EnumLingShouFightProp.JieBan_LingShou_CorZizhi] = CLuaLingShouPropertyFight.SetParamJieBan_LingShou_CorZizhi
SetParamFunctions[EnumLingShouFightProp.JieBan_LingShou_StaZizhi] = CLuaLingShouPropertyFight.SetParamJieBan_LingShou_StaZizhi
SetParamFunctions[EnumLingShouFightProp.JieBan_LingShou_StrZizhi] = CLuaLingShouPropertyFight.SetParamJieBan_LingShou_StrZizhi
SetParamFunctions[EnumLingShouFightProp.JieBan_LingShou_IntZizhi] = CLuaLingShouPropertyFight.SetParamJieBan_LingShou_IntZizhi
SetParamFunctions[EnumLingShouFightProp.JieBan_LingShou_AgiZizhi] = CLuaLingShouPropertyFight.SetParamJieBan_LingShou_AgiZizhi
SetParamFunctions[EnumLingShouFightProp.Buff_DisplayValue1] = CLuaLingShouPropertyFight.SetParamBuff_DisplayValue1
SetParamFunctions[EnumLingShouFightProp.Buff_DisplayValue2] = CLuaLingShouPropertyFight.SetParamBuff_DisplayValue2
SetParamFunctions[EnumLingShouFightProp.Buff_DisplayValue3] = CLuaLingShouPropertyFight.SetParamBuff_DisplayValue3
SetParamFunctions[EnumLingShouFightProp.Buff_DisplayValue4] = CLuaLingShouPropertyFight.SetParamBuff_DisplayValue4
SetParamFunctions[EnumLingShouFightProp.Buff_DisplayValue5] = CLuaLingShouPropertyFight.SetParamBuff_DisplayValue5
SetParamFunctions[EnumLingShouFightProp.LSNature] = CLuaLingShouPropertyFight.SetParamLSNature
SetParamFunctions[EnumLingShouFightProp.PrayMulti] = CLuaLingShouPropertyFight.SetParamPrayMulti
SetParamFunctions[EnumLingShouFightProp.EnhanceYingLing] = CLuaLingShouPropertyFight.SetParamEnhanceYingLing
SetParamFunctions[EnumLingShouFightProp.EnhanceYingLingMul] = CLuaLingShouPropertyFight.SetParamEnhanceYingLingMul
SetParamFunctions[EnumLingShouFightProp.EnhanceFlag] = CLuaLingShouPropertyFight.SetParamEnhanceFlag
SetParamFunctions[EnumLingShouFightProp.EnhanceFlagMul] = CLuaLingShouPropertyFight.SetParamEnhanceFlagMul
SetParamFunctions[EnumLingShouFightProp.ObjectType] = CLuaLingShouPropertyFight.SetParamObjectType
SetParamFunctions[EnumLingShouFightProp.MonsterType] = CLuaLingShouPropertyFight.SetParamMonsterType
SetParamFunctions[EnumLingShouFightProp.FightPropType] = CLuaLingShouPropertyFight.SetParamFightPropType
SetParamFunctions[EnumLingShouFightProp.PlayHpFull] = CLuaLingShouPropertyFight.SetParamPlayHpFull
SetParamFunctions[EnumLingShouFightProp.PlayHp] = CLuaLingShouPropertyFight.SetParamPlayHp
SetParamFunctions[EnumLingShouFightProp.PlayAtt] = CLuaLingShouPropertyFight.SetParamPlayAtt
SetParamFunctions[EnumLingShouFightProp.PlayDef] = CLuaLingShouPropertyFight.SetParamPlayDef
SetParamFunctions[EnumLingShouFightProp.PlayHit] = CLuaLingShouPropertyFight.SetParamPlayHit
SetParamFunctions[EnumLingShouFightProp.PlayMiss] = CLuaLingShouPropertyFight.SetParamPlayMiss
SetParamFunctions[EnumLingShouFightProp.EnhanceDieKe] = CLuaLingShouPropertyFight.SetParamEnhanceDieKe
SetParamFunctions[EnumLingShouFightProp.EnhanceDieKeMul] = CLuaLingShouPropertyFight.SetParamEnhanceDieKeMul
SetParamFunctions[EnumLingShouFightProp.DotRemain] = CLuaLingShouPropertyFight.SetParamDotRemain
SetParamFunctions[EnumLingShouFightProp.IsConfused] = CLuaLingShouPropertyFight.SetParamIsConfused
SetParamFunctions[EnumLingShouFightProp.EnhanceSheShouKefu] = CLuaLingShouPropertyFight.SetParamEnhanceSheShouKefu
SetParamFunctions[EnumLingShouFightProp.EnhanceJiaShiKefu] = CLuaLingShouPropertyFight.SetParamEnhanceJiaShiKefu
SetParamFunctions[EnumLingShouFightProp.EnhanceDaoKeKefu] = CLuaLingShouPropertyFight.SetParamEnhanceDaoKeKefu
SetParamFunctions[EnumLingShouFightProp.EnhanceXiaKeKefu] = CLuaLingShouPropertyFight.SetParamEnhanceXiaKeKefu
SetParamFunctions[EnumLingShouFightProp.EnhanceFangShiKefu] = CLuaLingShouPropertyFight.SetParamEnhanceFangShiKefu
SetParamFunctions[EnumLingShouFightProp.EnhanceYiShiKefu] = CLuaLingShouPropertyFight.SetParamEnhanceYiShiKefu
SetParamFunctions[EnumLingShouFightProp.EnhanceMeiZheKefu] = CLuaLingShouPropertyFight.SetParamEnhanceMeiZheKefu
SetParamFunctions[EnumLingShouFightProp.EnhanceYiRenKefu] = CLuaLingShouPropertyFight.SetParamEnhanceYiRenKefu
SetParamFunctions[EnumLingShouFightProp.EnhanceYanShiKefu] = CLuaLingShouPropertyFight.SetParamEnhanceYanShiKefu
SetParamFunctions[EnumLingShouFightProp.EnhanceHuaHunKefu] = CLuaLingShouPropertyFight.SetParamEnhanceHuaHunKefu
SetParamFunctions[EnumLingShouFightProp.EnhanceYingLingKefu] = CLuaLingShouPropertyFight.SetParamEnhanceYingLingKefu
SetParamFunctions[EnumLingShouFightProp.EnhanceDieKeKefu] = CLuaLingShouPropertyFight.SetParamEnhanceDieKeKefu
SetParamFunctions[EnumLingShouFightProp.AdjustPermanentCor] = CLuaLingShouPropertyFight.SetParamAdjustPermanentCor
SetParamFunctions[EnumLingShouFightProp.AdjustPermanentSta] = CLuaLingShouPropertyFight.SetParamAdjustPermanentSta
SetParamFunctions[EnumLingShouFightProp.AdjustPermanentStr] = CLuaLingShouPropertyFight.SetParamAdjustPermanentStr
SetParamFunctions[EnumLingShouFightProp.AdjustPermanentInt] = CLuaLingShouPropertyFight.SetParamAdjustPermanentInt
SetParamFunctions[EnumLingShouFightProp.AdjustPermanentAgi] = CLuaLingShouPropertyFight.SetParamAdjustPermanentAgi
SetParamFunctions[EnumLingShouFightProp.SoulCoreLevel] = CLuaLingShouPropertyFight.SetParamSoulCoreLevel
SetParamFunctions[EnumLingShouFightProp.AdjustPermanentMidCor] = CLuaLingShouPropertyFight.SetParamAdjustPermanentMidCor
SetParamFunctions[EnumLingShouFightProp.AdjustPermanentMidSta] = CLuaLingShouPropertyFight.SetParamAdjustPermanentMidSta
SetParamFunctions[EnumLingShouFightProp.AdjustPermanentMidStr] = CLuaLingShouPropertyFight.SetParamAdjustPermanentMidStr
SetParamFunctions[EnumLingShouFightProp.AdjustPermanentMidInt] = CLuaLingShouPropertyFight.SetParamAdjustPermanentMidInt
SetParamFunctions[EnumLingShouFightProp.AdjustPermanentMidAgi] = CLuaLingShouPropertyFight.SetParamAdjustPermanentMidAgi
SetParamFunctions[EnumLingShouFightProp.SumPermanent] = CLuaLingShouPropertyFight.SetParamSumPermanent
SetParamFunctions[EnumLingShouFightProp.AntiTian] = CLuaLingShouPropertyFight.SetParamAntiTian
SetParamFunctions[EnumLingShouFightProp.AntiEGui] = CLuaLingShouPropertyFight.SetParamAntiEGui
SetParamFunctions[EnumLingShouFightProp.AntiXiuLuo] = CLuaLingShouPropertyFight.SetParamAntiXiuLuo
SetParamFunctions[EnumLingShouFightProp.AntiDiYu] = CLuaLingShouPropertyFight.SetParamAntiDiYu
SetParamFunctions[EnumLingShouFightProp.AntiRen] = CLuaLingShouPropertyFight.SetParamAntiRen
SetParamFunctions[EnumLingShouFightProp.AntiChuSheng] = CLuaLingShouPropertyFight.SetParamAntiChuSheng
SetParamFunctions[EnumLingShouFightProp.AntiBuilding] = CLuaLingShouPropertyFight.SetParamAntiBuilding
SetParamFunctions[EnumLingShouFightProp.AntiZhaoHuan] = CLuaLingShouPropertyFight.SetParamAntiZhaoHuan
SetParamFunctions[EnumLingShouFightProp.AntiLingShou] = CLuaLingShouPropertyFight.SetParamAntiLingShou
SetParamFunctions[EnumLingShouFightProp.AntiBoss] = CLuaLingShouPropertyFight.SetParamAntiBoss
SetParamFunctions[EnumLingShouFightProp.AntiSheShou] = CLuaLingShouPropertyFight.SetParamAntiSheShou
SetParamFunctions[EnumLingShouFightProp.AntiJiaShi] = CLuaLingShouPropertyFight.SetParamAntiJiaShi
SetParamFunctions[EnumLingShouFightProp.AntiDaoKe] = CLuaLingShouPropertyFight.SetParamAntiDaoKe
SetParamFunctions[EnumLingShouFightProp.AntiXiaKe] = CLuaLingShouPropertyFight.SetParamAntiXiaKe
SetParamFunctions[EnumLingShouFightProp.AntiFangShi] = CLuaLingShouPropertyFight.SetParamAntiFangShi
SetParamFunctions[EnumLingShouFightProp.AntiYiShi] = CLuaLingShouPropertyFight.SetParamAntiYiShi
SetParamFunctions[EnumLingShouFightProp.AntiMeiZhe] = CLuaLingShouPropertyFight.SetParamAntiMeiZhe
SetParamFunctions[EnumLingShouFightProp.AntiYiRen] = CLuaLingShouPropertyFight.SetParamAntiYiRen
SetParamFunctions[EnumLingShouFightProp.AntiYanShi] = CLuaLingShouPropertyFight.SetParamAntiYanShi
SetParamFunctions[EnumLingShouFightProp.AntiHuaHun] = CLuaLingShouPropertyFight.SetParamAntiHuaHun
SetParamFunctions[EnumLingShouFightProp.AntiYingLing] = CLuaLingShouPropertyFight.SetParamAntiYingLing
SetParamFunctions[EnumLingShouFightProp.AntiDieKe] = CLuaLingShouPropertyFight.SetParamAntiDieKe
SetParamFunctions[EnumLingShouFightProp.AntiFlag] = CLuaLingShouPropertyFight.SetParamAntiFlag
SetParamFunctions[EnumLingShouFightProp.EnhanceZhanKuang] = CLuaLingShouPropertyFight.SetParamEnhanceZhanKuang
SetParamFunctions[EnumLingShouFightProp.EnhanceZhanKuangMul] = CLuaLingShouPropertyFight.SetParamEnhanceZhanKuangMul
SetParamFunctions[EnumLingShouFightProp.EnhanceZhanKuangKefu] = CLuaLingShouPropertyFight.SetParamEnhanceZhanKuangKefu
SetParamFunctions[EnumLingShouFightProp.AntiZhanKuang] = CLuaLingShouPropertyFight.SetParamAntiZhanKuang
SetParamFunctions[EnumLingShouFightProp.JumpSpeed] = CLuaLingShouPropertyFight.SetParamJumpSpeed
SetParamFunctions[EnumLingShouFightProp.OriJumpSpeed] = CLuaLingShouPropertyFight.SetParamOriJumpSpeed
SetParamFunctions[EnumLingShouFightProp.AdjJumpSpeed] = CLuaLingShouPropertyFight.SetParamAdjJumpSpeed
SetParamFunctions[EnumLingShouFightProp.GravityAcceleration] = CLuaLingShouPropertyFight.SetParamGravityAcceleration
SetParamFunctions[EnumLingShouFightProp.OriGravityAcceleration] = CLuaLingShouPropertyFight.SetParamOriGravityAcceleration
SetParamFunctions[EnumLingShouFightProp.AdjGravityAcceleration] = CLuaLingShouPropertyFight.SetParamAdjGravityAcceleration
SetParamFunctions[EnumLingShouFightProp.HorizontalAcceleration] = CLuaLingShouPropertyFight.SetParamHorizontalAcceleration
SetParamFunctions[EnumLingShouFightProp.OriHorizontalAcceleration] = CLuaLingShouPropertyFight.SetParamOriHorizontalAcceleration
SetParamFunctions[EnumLingShouFightProp.AdjHorizontalAcceleration] = CLuaLingShouPropertyFight.SetParamAdjHorizontalAcceleration
SetParamFunctions[EnumLingShouFightProp.SwimSpeed] = CLuaLingShouPropertyFight.SetParamSwimSpeed
SetParamFunctions[EnumLingShouFightProp.OriSwimSpeed] = CLuaLingShouPropertyFight.SetParamOriSwimSpeed
SetParamFunctions[EnumLingShouFightProp.AdjSwimSpeed] = CLuaLingShouPropertyFight.SetParamAdjSwimSpeed
SetParamFunctions[EnumLingShouFightProp.StrengthPoint] = CLuaLingShouPropertyFight.SetParamStrengthPoint
SetParamFunctions[EnumLingShouFightProp.StrengthPointMax] = CLuaLingShouPropertyFight.SetParamStrengthPointMax
SetParamFunctions[EnumLingShouFightProp.OriStrengthPointMax] = CLuaLingShouPropertyFight.SetParamOriStrengthPointMax
SetParamFunctions[EnumLingShouFightProp.AdjStrengthPointMax] = CLuaLingShouPropertyFight.SetParamAdjStrengthPointMax
SetParamFunctions[EnumLingShouFightProp.AntiMulEnhanceIllusion] = CLuaLingShouPropertyFight.SetParamAntiMulEnhanceIllusion
SetParamFunctions[EnumLingShouFightProp.GlideHorizontalSpeedWithUmbrella] = CLuaLingShouPropertyFight.SetParamGlideHorizontalSpeedWithUmbrella
SetParamFunctions[EnumLingShouFightProp.GlideVerticalSpeedWithUmbrella] = CLuaLingShouPropertyFight.SetParamGlideVerticalSpeedWithUmbrella
SetParamFunctions[EnumLingShouFightProp.AdjpSpeed] = CLuaLingShouPropertyFight.SetParamAdjpSpeed
SetParamFunctions[EnumLingShouFightProp.AdjmSpeed] = CLuaLingShouPropertyFight.SetParamAdjmSpeed

