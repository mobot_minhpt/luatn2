require("common/common_include")

local Input = import "UnityEngine.Input"
local Application = import "UnityEngine.Application"
local RuntimePlatform = import "UnityEngine.RuntimePlatform"
local Vector3 = import "UnityEngine.Vector3"
local TouchPhase = import "UnityEngine.TouchPhase"
local LuaTweenUtils = import "LuaTweenUtils"
local UITexture = import "UITexture"

LuaXianZhiShenZhiWnd = class()

RegistChildComponent(LuaXianZhiShenZhiWnd, "Stamp", GameObject)
RegistChildComponent(LuaXianZhiShenZhiWnd, "PlayerNameLabel", UILabel)
RegistChildComponent(LuaXianZhiShenZhiWnd, "ContentLabel", UILabel)
RegistChildComponent(LuaXianZhiShenZhiWnd, "HitCollider", GameObject)
RegistChildComponent(LuaXianZhiShenZhiWnd, "Yin", UITexture)
RegistChildComponent(LuaXianZhiShenZhiWnd, "HintLabel", UILabel)

RegistClassMember(LuaXianZhiShenZhiWnd, "IsStampTaken")
RegistClassMember(LuaXianZhiShenZhiWnd, "IsSealStamp") -- 是否在盖印动画中
RegistClassMember(LuaXianZhiShenZhiWnd, "LastPos")
RegistClassMember(LuaXianZhiShenZhiWnd, "StampDefaultPos")
RegistClassMember(LuaXianZhiShenZhiWnd, "Tick")

function LuaXianZhiShenZhiWnd:Init()
	self.IsStampTaken = false
	self.IsSealStamp = false
	self.LastPos = nil
	self.FingerIndex = -1
	self.Yin.alpha = 0
    self.StampDefaultPos = Vector3(556, -138, 0)
    self.HintLabel.gameObject:SetActive(true)
    self:DestroyTick()

    self:UpdateShenZhiContent()

	local onDragStamp = function (go)
		self:OnDragStamp(go)
	end
	CommonDefs.AddOnDragStartListener(self.Stamp.gameObject, DelegateFactory.Action_GameObject(onDragStamp), false)

end

function LuaXianZhiShenZhiWnd:UpdateShenZhiContent()
    if not CLuaXianzhiMgr.m_XianZhiManagePlayer or not CClientMainPlayer.Inst then
        CUIManager.CloseUI(CLuaUIResources.XianZhiShenZhiWnd)
        return
    end

    self.PlayerNameLabel.text = SafeStringFormat3(LocalString.GetString("[2A4253]上神[-][FFC300]%s[-][2A4253]有旨：[-]"), CClientMainPlayer.Inst.Name)
    local operationName, resultName = self:GetOperationName(CLuaXianzhiMgr.m_OperateType)
    if not operationName or not resultName then
        CUIManager.CloseUI(CLuaUIResources.XianZhiShenZhiWnd)
        return
    end

    local regionId = CLuaXianzhiMgr.m_XianZhiManagePlayer.regionId
    local titleId = CLuaXianzhiMgr.m_XianZhiManagePlayer.titleId
    local xianZhiName = CLuaXianzhiMgr.GetXianZhiNameByRegionAndTitle(regionId, titleId)
    local lastSeconds = self:GetOperationLastTime()

    self.ContentLabel.text = SafeStringFormat3(LocalString.GetString("   %s[FFFE91]【%s】%s[-]神力，仙职技能暂[FFC300]%s[-]，持续[FFC300]%s[-]，即时生效。"),
        operationName, xianZhiName, CLuaXianzhiMgr.m_XianZhiManagePlayer.playerName, resultName, self:GetRemainTimeText(lastSeconds))
end

function LuaXianZhiShenZhiWnd:GetOperationName(operationtype)
    if operationtype == EnumXianZhiOperateType.ePromote then
        return LocalString.GetString("增益"), SafeStringFormat3(LocalString.GetString("提升至%s级"), CLuaXianzhiMgr.m_XianZhiManagePlayer.skillLevel+1)
    elseif operationtype == EnumXianZhiOperateType.eWeaken then
        return LocalString.GetString("减损"), SafeStringFormat3(LocalString.GetString("下降至%s级"), CLuaXianzhiMgr.m_XianZhiManagePlayer.skillLevel-1)
    elseif operationtype == EnumXianZhiOperateType.eForbid then
        return LocalString.GetString("封印"), LocalString.GetString("无法使用")
    end
    return nil
end

-- return the last time of operation in seconds
function LuaXianZhiShenZhiWnd:GetOperationLastTime()
    local status = CLuaXianzhiMgr.GetXianZhiStatus()
    local setting = XianZhi_Setting.GetData()

    if status == EnumXianZhiStatus.NotFengXian or status == EnumXianZhiStatus.DistrictRegion then
        return 0
    elseif status == EnumXianZhiStatus.CityRegion then
        return setting.OperateEffectDuration
    elseif status == EnumXianZhiStatus.SanJie then
        return setting.SanJieOperateEffectDuration
    end
    return 0
end

function LuaXianZhiShenZhiWnd:GetRemainTimeText(totalSeconds)
    if totalSeconds >= 3600 then
        return SafeStringFormat3(LocalString.GetString("%02d小时%02d分钟"), math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        return SafeStringFormat3(LocalString.GetString("%02d分钟"), math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end

function LuaXianZhiShenZhiWnd:OnDragStamp(go)
	self.IsStampTaken = true
	self.LastPos = Input.mousePosition

	if Application.platform == RuntimePlatform.IPhonePlayer or Application.platform == RuntimePlatform.Android then
		self.FingerIndex = Input.GetTouch(0).fingerId
		local pos = Input.GetTouch(0).position
		self.LastPos = Vector3(pos.x, pos.y, 0)
    end
end

function LuaXianZhiShenZhiWnd:Update()

	if not self.IsStampTaken then
		return
	end

	if self.IsSealStamp then
		return
	end

	if Application.platform == RuntimePlatform.IPhonePlayer or Application.platform == RuntimePlatform.Android then
        do
            local i = 0
            while i < Input.touchCount do
                local touch = Input.GetTouch(i)
                if touch.fingerId == self.FingerIndex then
                    if touch.phase ~= TouchPhase.Ended and touch.phase ~= TouchPhase.Canceled then
                        if touch.phase ~= TouchPhase.Stationary then
                            local tempPos = Vector3(touch.position.x, touch.position.y, 0)
                        	local pos = CUIManager.UIMainCamera:ScreenToWorldPoint(tempPos)
                            self.Stamp.transform.position = pos
                            self.LastPos = pos
                        end
                        return
                    else
                        break
                    end
                end
                i = i + 1
            end
        end
    else
        if Input.GetMouseButton(0) then
        	local pos = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition)
            self.Stamp.transform.position = pos
            self.LastPos = Input.mousePosition
            if not Input.GetMouseButtonUp(0) then
                return
            end
        end
    end

    self.Dragging = false
    self.FingerIndex = -1

    local hoveredObject = CUICommonDef.SelectedUIWithRacast
	self:ToStampShenZhi(hoveredObject)
end

function LuaXianZhiShenZhiWnd:ToStampShenZhi(hoveredObject)
	if self.HitCollider:Equals(hoveredObject) then
		self.IsSealStamp = true

		self.IsStampTaken = false
        self.HintLabel.gameObject:SetActive(false)
		-- 印章向下移动
		local tween = LuaTweenUtils.DOLocalMoveY(self.Stamp.transform, self.Stamp.transform.localPosition.y - 30, 0.3, false)
		LuaTweenUtils.OnComplete(tween, function ()
        	self.Yin.transform.position = self.Stamp.transform.position
        	LuaTweenUtils.TweenAlpha(self.Yin.transform, 0, 1, 0.5)
            self:RequestOperateXianZhiSkill()
        	self:ResetStamp()
        end)
	end
end

function LuaXianZhiShenZhiWnd:RequestOperateXianZhiSkill()
    CLuaXianzhiMgr.RequestOperateXianZhi()
end

function LuaXianZhiShenZhiWnd:ResetStamp()
    local tween = LuaTweenUtils.TweenPosition(self.Stamp.transform, self.StampDefaultPos.x, self.StampDefaultPos.y, self.StampDefaultPos.z, 0.8)
    self.Tick = RegisterTickOnce(function ()
        CUIManager.CloseUI(CLuaUIResources.XianZhiShenZhiWnd)
        CUIManager.CloseUI(CLuaUIResources.XianZhiManageWnd)
    end, 2000)
end

function LuaXianZhiShenZhiWnd:DestroyTick()
    if self.Tick ~= nil then
      UnRegisterTick(self.Tick)
      self.Tick=nil
    end
end

return LuaXianZhiShenZhiWnd
