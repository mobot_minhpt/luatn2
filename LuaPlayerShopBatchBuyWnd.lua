local CPlayerShopMgr=import "L10.UI.CPlayerShopMgr"
local CQnSymbolParser=import "CQnSymbolParser"
local CItem=import "L10.Game.CItem"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CItemMgr = import "L10.Game.CItemMgr"

CLuaPlayerShopBatchBuyWnd=class()
RegistClassMember(CLuaPlayerShopBatchBuyWnd,"m_ItemTexture")
RegistClassMember(CLuaPlayerShopBatchBuyWnd,"m_LevelLabel")
RegistClassMember(CLuaPlayerShopBatchBuyWnd,"m_NameLabel")
RegistClassMember(CLuaPlayerShopBatchBuyWnd,"m_DescLabel")
RegistClassMember(CLuaPlayerShopBatchBuyWnd,"m_OwnedNumLabel")
RegistClassMember(CLuaPlayerShopBatchBuyWnd,"m_PriceInput")
RegistClassMember(CLuaPlayerShopBatchBuyWnd,"m_CountInput")
RegistClassMember(CLuaPlayerShopBatchBuyWnd,"m_RecommendPriceCompareLabel")

function CLuaPlayerShopBatchBuyWnd:Awake()
    UIEventListener.Get(FindChild(self.transform,"TipButton").gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("PlayerShop_BatchBuy_Tip",{})
    end)
    UIEventListener.Get(FindChild(self.transform,"CancelButton").gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.CloseUI(CUIResources.PlayerShopBatchBuyWnd)
    end)
    UIEventListener.Get(FindChild(self.transform,"ConfirmButton").gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:BatchBuy()
        CUIManager.CloseUI(CUIResources.PlayerShopBatchBuyWnd)
    end)

    self.m_ItemTexture=FindChild(self.transform,"ItemTexture"):GetComponent(typeof(CUITexture))
    self.m_LevelLabel=FindChild(self.transform,"LevelLabel"):GetComponent(typeof(UILabel))
    self.m_NameLabel=FindChild(self.transform,"NameLabel"):GetComponent(typeof(UILabel))
    self.m_DescLabel=FindChild(self.transform,"DescLabel"):GetComponent(typeof(UILabel))
    self.m_OwnedNumLabel = FindChild(self.transform,"OwnedNumLabel"):GetComponent(typeof(UILabel))
    self.m_PriceInput=FindChild(self.transform,"PriceInput"):GetComponent(typeof(QnAddSubAndInputButton))
    self.m_CountInput=FindChild(self.transform,"CountInput"):GetComponent(typeof(QnAddSubAndInputButton))
    self.m_RecommendPriceCompareLabel=FindChild(self.transform,"RecommendPriceCompareLabel"):GetComponent(typeof(UILabel))
    self.m_RecommendPriceCompareLabel.color=Color.green
    self.m_RecommendPriceCompareLabel.text=SafeStringFormat3(LocalString.GetString("推荐价格:%s%%"),0)
end

function CLuaPlayerShopBatchBuyWnd:OnEnable()
    g_ScriptEvent:AddListener("QueryPlayerShopRecommendPriceResult",self,"OnQueryPlayerShopRecommendPriceResult")
end
function CLuaPlayerShopBatchBuyWnd:OnDisable()
    g_ScriptEvent:RemoveListener("QueryPlayerShopRecommendPriceResult",self,"OnQueryPlayerShopRecommendPriceResult")
end

function CLuaPlayerShopBatchBuyWnd:OnQueryPlayerShopRecommendPriceResult(args)
    local templateId, basisPrice, minPrice, maxPrice = args[0],args[1],args[2],args[3]
    if templateId == CPlayerShopMgr.BatchBuyTemplateId then
        CPlayerShopMgr.BatchBuyCurrentPrice = basisPrice
        CPlayerShopMgr.BatchBuyRecommendPrice = basisPrice
        CPlayerShopMgr.BatchBuyMinPrice = minPrice
        CPlayerShopMgr.BatchBuyMaxPrice = maxPrice
    end

    local templateId=CPlayerShopMgr.BatchBuyTemplateId
    local template = Item_Item.GetData(templateId)
    self.m_OwnedNumLabel.text = SafeStringFormat3(LocalString.GetString("已拥有:%d"),CItemMgr.Inst:GetItemCount(templateId))
    if template then
        self.m_ItemTexture:LoadMaterial(template.Icon)
        self.m_LevelLabel.text = SafeStringFormat3(LocalString.GetString("%s级"), CLuaDesignMgr.Item_Item_GetAdjustedGradeCheck(template));
        self.m_NameLabel.color=CItem.GetColor(templateId)
        self.m_NameLabel.text=template.Name
        self.m_DescLabel.text= CQnSymbolParser.ConvertQnTextToNGUIText(CItem.GetItemDescription(templateId,true))
    end
    
    self.m_CountInput:SetMinMax(CPlayerShopMgr.BatchBuyMinCount,CPlayerShopMgr.BatchBuyMaxCount,1)
    self.m_CountInput:SetValue(CPlayerShopMgr.BatchBuyRecommendCount,false)
    local step=math.floor((CPlayerShopMgr.BatchBuyMaxPrice-CPlayerShopMgr.BatchBuyMinPrice)* CPlayerShopMgr.PriceParameters)
    if step<1 then step=1 end

    self.m_PriceInput:SetMinMax(CPlayerShopMgr.Inst:GetBatchBuyBottomPrice(),CPlayerShopMgr.BatchBuyMaxPrice,step)
    self.m_PriceInput.onValueChanged=DelegateFactory.Action_uint(function(val)
        local recommend=CPlayerShopMgr.BatchBuyRecommendPrice
        local change=math.floor((val-recommend)/recommend*100)
        self.m_RecommendPriceCompareLabel.text=SafeStringFormat3(LocalString.GetString("推荐价格:%s%%"),change)
        if change<=0 then
            self.m_RecommendPriceCompareLabel.color=Color.green
        else
            self.m_RecommendPriceCompareLabel.color=Color.red
        end
    end)
    self.m_PriceInput:SetValue(math.max(CPlayerShopMgr.BatchBuyCurrentPrice,LuaPlayerShopMgr.m_BatchBuyLowestPrice),true)     
end

function CLuaPlayerShopBatchBuyWnd:Init()
    local templateId=CPlayerShopMgr.BatchBuyTemplateId
    if templateId>0 then
        Gac2Gas.QueryPlayerShopRecommendPrice(templateId)
    end
end

function CLuaPlayerShopBatchBuyWnd:BatchBuy()
    local count=self.m_CountInput:GetValue()
    local price=self.m_PriceInput:GetValue()
    CPlayerShopMgr.Inst:BatchBuy(price,count)
end
