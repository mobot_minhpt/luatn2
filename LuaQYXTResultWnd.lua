local CUIFx = import "L10.UI.CUIFx"
local QnTableView=import "L10.UI.QnTableView"
local UILabel = import "UILabel"
local Color = import "UnityEngine.Color"
local Profession = import "L10.Game.Profession"
local GameObject = import "UnityEngine.GameObject"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"

CLuaQYXTResultWnd = class()

RegistChildComponent(CLuaQYXTResultWnd, "StageDescView", GameObject)
RegistChildComponent(CLuaQYXTResultWnd, "TopRankView", GameObject)
RegistChildComponent(CLuaQYXTResultWnd, "StageDescLabel1", UILabel)
RegistChildComponent(CLuaQYXTResultWnd, "StageDescLabel2", UILabel)
RegistChildComponent(CLuaQYXTResultWnd, "MyRankLabel", UILabel)
RegistChildComponent(CLuaQYXTResultWnd, "FxNode", CUIFx)
RegistChildComponent(CLuaQYXTResultWnd, "TableView", QnTableView)
RegistChildComponent(CLuaQYXTResultWnd, "TitleProgressLabel", UILabel)
RegistClassMember(CLuaQYXTResultWnd,"m_ItemInfoList")
RegistClassMember(CLuaQYXTResultWnd,"m_CurStage")
RegistClassMember(CLuaQYXTResultWnd,"m_Rank")
RegistClassMember(CLuaQYXTResultWnd,"m_IsEnd")

function CLuaQYXTResultWnd:Awake()
    self.m_ItemInfoList = {}
    self.m_CurStage = 1
    self.m_IsEnd = 0
    self.StageDescLabel1.text = Valentine2020_Setting.GetData().QYXTCaiHuaZhanKuang
    self.StageDescLabel2.text = Valentine2020_Setting.GetData().QYXTDouQingZhanKuang
end

function CLuaQYXTResultWnd:Init()
    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return #self.m_ItemInfoList
        end,
        function(item,row)
            self:InitItem(item,row)
        end)
    -- self.TableView:ReloadData(false, false)

    if CLuaQYXTMgr.RefreshResultWndNotShow == true then
        CLuaQYXTMgr.RefreshResultWndNotShow = false
        if not CLuaQYXTMgr.battleInfoTblUD then
            return
        end
        self:OnRefreshQYXTBattleInfo(CLuaQYXTMgr.curSatge, CLuaQYXTMgr.isEnd, CLuaQYXTMgr.battleInfoTblUD)
        
    end

    self.TableView:ReloadData(false, false)
end

function CLuaQYXTResultWnd:InitItem(item,row)
    local info = self.m_ItemInfoList[row+1]
    if info then
        local rankLabel = item.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
        local member1 = item.transform:Find("Member1"):GetComponent(typeof(UISprite))
        local member2 = item.transform:Find("Member2"):GetComponent(typeof(UISprite))
        local memberName1 = member1.transform:Find("PlayerName"):GetComponent(typeof(UILabel))
        local memberName2 = member2.transform:Find("PlayerName"):GetComponent(typeof(UILabel))
        --完成进度/剩余复活次数
        local progressLabel = item.transform:Find("ProgressLabel"):GetComponent(typeof(UILabel))
        local scoreLabel = item.transform:Find("ScoreLabel"):GetComponent(typeof(UILabel))

        rankLabel.text = row + 1
        memberName1.text = info.playerName1
        memberName2.text = info.playerName2
        member1.spriteName = Profession.GetIconByNumber(info.class1)
        member2.spriteName = Profession.GetIconByNumber(info.class2)

        local textColor = Color.white
        if (info.playerId1 and CClientMainPlayer.IsPlayerId(info.playerId1)) or (info.playerId2 and CClientMainPlayer.IsPlayerId(info.playerId2)) then
            textColor = Color.green
            self.m_Rank = row + 1  
        end

        rankLabel.color = textColor
        memberName1.color = textColor
        memberName2.color = textColor
        progressLabel.color = textColor
        scoreLabel.color = textColor

        if self.m_CurStage == 1 then
            self.TitleProgressLabel.text = SafeStringFormat3(LocalString.GetString("完成进度"))
            --finished 
            if  info.round == 5 then
                progressLabel.text = self:GetRemainTimeText(info.finishedTime)
                scoreLabel.text = info.score
            else
            --unfinish
                local maxRound = Valentine2020_Setting.GetData().PickMaxRound
                progressLabel.text = SafeStringFormat3(LocalString.GetString("%d/%d"), info.round,maxRound)
                if self.m_IsEnd and self.m_IsEnd ~= 0 then
                    scoreLabel.text = info.score
                else
                    scoreLabel.text = LocalString.GetString("—")
                end
            end
            member1.alpha = 1
            member2.alpha = 1
        elseif self.m_CurStage == 2 then
            self.TitleProgressLabel.text = SafeStringFormat3(LocalString.GetString("击杀数"))
            progressLabel.text = info.killNum
            scoreLabel.text = info.score
        end
         
    end
end

function CLuaQYXTResultWnd:RefreshRankView(stage, isEnd)

    self.m_IsEnd = isEnd
    self.TableView:ReloadData(false, false)
    if isEnd and isEnd ~= 0 then
        self.StageDescView:SetActive(false)
        self.TopRankView:SetActive(true)
        self.MyRankLabel.text = self.m_Rank
        --self.FxNode:LoadFx("Fx/UI/Prefab/UI_dagongshouce_kekaiqicishu.prefab")
    else
        --self.FxNode:DestroyFx()
        self.StageDescView:SetActive(true)
        self.TopRankView:SetActive(false)
        self.StageDescLabel1.gameObject:SetActive(stage == 1)
        self.StageDescLabel2.gameObject:SetActive(stage == 2)
    end
end

function CLuaQYXTResultWnd:OnRefreshQYXTBattleInfo(stage, isEnd, progressInfoUD)
    local list = MsgPackImpl.unpack(progressInfoUD)
    self.m_ItemInfoList = {}
    self.m_CurStage = stage
    if stage == 1 then
        for i=0, list.Count-1, 9 do
            local t = {}
            t.playerId1 = list[i]
            t.class1 = list[i + 1]
            t.playerName1 = list[i + 2]
            t.playerId2 = list[i + 3]
            t.class2 = list[i + 4]
            t.playerName2 = list[i + 5]
            t.round = list[i + 6]
            t.finishedTime = list[i + 7]
            t.score = list[i + 8]
            table.insert(self.m_ItemInfoList,t)
        end
    elseif stage == 2 then
        for i=0, list.Count-1, 8 do--10
            local t = {}
            t.playerId1 = list[i]
            t.class1 = list[i + 1]
            t.playerName1 = list[i + 2]
            t.playerId2 = list[i + 3]
            t.class2 = list[i + 4]
            t.playerName2 = list[i + 5]
            t.killNum = list[i + 6]
            t.score = list[i + 7]
            table.insert(self.m_ItemInfoList,t)
        end
    end
    self:RefreshRankView(stage, isEnd)
end

function CLuaQYXTResultWnd:GetRemainTimeText(totalSeconds)
    if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        return SafeStringFormat3("00:%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end

function CLuaQYXTResultWnd:OnEnable()
    g_ScriptEvent:AddListener("RefreshQYXTBattleInfo",self,"OnRefreshQYXTBattleInfo")
end

function CLuaQYXTResultWnd:OnDisable()
    g_ScriptEvent:RemoveListener("RefreshQYXTBattleInfo",self,"OnRefreshQYXTBattleInfo")
end