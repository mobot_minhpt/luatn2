local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"

LuaTopNoticeCenter = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaTopNoticeCenter, "Anchor", "Anchor", GameObject)
RegistChildComponent(LuaTopNoticeCenter, "TopLabel", "TopLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaTopNoticeCenter, "m_Tick")
RegistClassMember(LuaTopNoticeCenter, "m_MaxCountdown")
RegistClassMember(LuaTopNoticeCenter, "m_InitialSeconds")

function LuaTopNoticeCenter:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaTopNoticeCenter:Init()
    self.Anchor.gameObject:SetActive(false)
    self.TopLabel.text = ""
end

--@region UIEvent

--@endregion UIEvent

function LuaTopNoticeCenter:OnEnable()
    g_ScriptEvent:AddListener("OnShowTopNotice", self, "OnShowTopNotice")
    g_ScriptEvent:AddListener("OnHideTopNotice", self, "OnHideTopNotice")
end

function LuaTopNoticeCenter:OnDisable()
    g_ScriptEvent:RemoveListener("OnShowTopNotice", self, "OnShowTopNotice")
    g_ScriptEvent:RemoveListener("OnHideTopNotice", self, "OnHideTopNotice")
    self:CancelTick()
end

function LuaTopNoticeCenter:OnShowTopNotice(getTextAction, countdown)
    self.Anchor.gameObject:SetActive(false)
    self.TopLabel.text = ""
    if getTextAction and countdown then
        self.m_MaxCountdown = countdown
        self.m_InitialSeconds = Time.realtimeSinceStartup
        self.Anchor.gameObject:SetActive(true)
        self.TopLabel.text = getTextAction(self.m_InitialSeconds)
        self:CancelTick()
        self.m_Tick = RegisterTick(function ()
            local seconds = Time.realtimeSinceStartup - self.m_InitialSeconds
            self.TopLabel.text = getTextAction(seconds)
            if seconds >= self.m_MaxCountdown then
                self:CancelTick()
                self.Anchor.gameObject:SetActive(false)
            end
        end, 500)
    end
end

function LuaTopNoticeCenter:OnHideTopNotice()
    self.Anchor.gameObject:SetActive(false)
    self.TopLabel.text = ""
    self:CancelTick()
end

function LuaTopNoticeCenter:CancelTick()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
end