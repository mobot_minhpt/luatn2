local QnInput = import "L10.UI.QnInput"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
local CWordFilterMgr=import "L10.Game.CWordFilterMgr"

local CUITexture = import "L10.UI.CUITexture"

local UILabel = import "UILabel"
local CIMMgr=import "L10.Game.CIMMgr"

local GameObject = import "UnityEngine.GameObject"
local CCommonPlayerListMgr=import "L10.Game.CCommonPlayerListMgr"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"


LuaChunJie2022HeKaWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChunJie2022HeKaWnd, "AddFriendButton", "AddFriendButton", GameObject)
RegistChildComponent(LuaChunJie2022HeKaWnd, "EditNode", "EditNode", GameObject)
RegistChildComponent(LuaChunJie2022HeKaWnd, "ContentNode", "ContentNode", GameObject)
RegistChildComponent(LuaChunJie2022HeKaWnd, "MailLabel", "MailLabel", UILabel)
RegistChildComponent(LuaChunJie2022HeKaWnd, "NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaChunJie2022HeKaWnd, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaChunJie2022HeKaWnd, "PortraitIcon", "PortraitIcon", CUITexture)
RegistChildComponent(LuaChunJie2022HeKaWnd, "Button", "Button", GameObject)
RegistChildComponent(LuaChunJie2022HeKaWnd, "Input", "Input", QnInput)
RegistChildComponent(LuaChunJie2022HeKaWnd, "NoFriend", "NoFriend", GameObject)
RegistChildComponent(LuaChunJie2022HeKaWnd, "SenderPortraitIcon", "SenderPortraitIcon", CUITexture)

--@endregion RegistChildComponent end
RegistClassMember(LuaChunJie2022HeKaWnd,"m_PlayerId")

LuaChunJie2022HeKaWnd.s_Content = nil
function LuaChunJie2022HeKaWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.AddFriendButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAddFriendButtonClick()
	end)


	
	UIEventListener.Get(self.Button.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButtonClick()
	end)


    --@endregion EventBind end
    UIEventListener.Get(self.PortraitIcon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAddFriendButtonClick()
	end)
end

function LuaChunJie2022HeKaWnd:Init()
    self.m_PlayerId = 0
    if LuaChunJie2022HeKaWnd.s_Content then
        self.ContentNode:SetActive(true)
        self.EditNode:SetActive(false)
        local t = LuaChunJie2022HeKaWnd.s_Content
        self.MailLabel.text = t.content
        self.NameLabel.text = t.name
        local dateTime = CServerTimeMgr.ConvertTimeStampToZone8Time(t.time)
        self.TimeLabel.text = SafeStringFormat3(LocalString.GetString("%d年%d月%d日"),
            dateTime.Year,dateTime.Month,dateTime.Day)

        local portrait = CUICommonDef.GetPortraitName(t.class, t.gender, -1)
        self.SenderPortraitIcon:LoadNPCPortrait(portrait, false)
    else
        self.ContentNode:SetActive(false)
        self.EditNode:SetActive(true)
    end
end

--@region UIEvent

function LuaChunJie2022HeKaWnd:OnAddFriendButtonClick()
    CCommonPlayerListMgr.Inst:ShowFriendsToGivePresent(DelegateFactory.Action_ulong(function(playerId)
        local basicInfo = CIMMgr.Inst:GetBasicInfo(playerId)
        local portrait =  CUICommonDef.GetPortraitName(basicInfo.Class, basicInfo.Gender, -1)
        self.PortraitIcon:LoadNPCPortrait(portrait, false)
        self.m_PlayerId = playerId

        self.NoFriend:SetActive(false)
        self.AddFriendButton:SetActive(false)
    end))
end


function LuaChunJie2022HeKaWnd:OnButtonClick()
    if self.m_PlayerId == 0 then
        g_MessageMgr:ShowMessage("SEND_PLAYER_IS_EMPTY")
        return
    end
    local content = self.Input.Text
    if content == nil or content=="" then
        g_MessageMgr:ShowMessage("WISHCONTENT_IS_EMPTY")
        return
    end
    if CommonDefs.StringLength(content) > 30 then
        g_MessageMgr:ShowMessage("Nianwei_IS_TOO_LONG")
        return
    end
    local ret = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(content, true)
    if (not ret.msg) or ret.shouldBeIgnore then 
        g_MessageMgr:ShowMessage("Speech_Violation")
        return 
    end

    content = ret.msg
    -- content = CWordFilterMgr.Inst:DoFilterOnReceiveWithoutVoiceCheck(0, content, true)
    if content == nil or content=="" then
        return
    end
    
    Gac2Gas.XZNW_EditCardContent(self.m_PlayerId,content)
    Gac2Gas.XZNW_PlayerSendCard()

    CUIManager.CloseUI(CLuaUIResources.ChunJie2022HeKaWnd)
end


--@endregion UIEvent

function LuaChunJie2022HeKaWnd:OnEnable()
end
function LuaChunJie2022HeKaWnd:OnDisable()
end
function LuaChunJie2022HeKaWnd:OnDestroy()
    LuaChunJie2022HeKaWnd.s_Content = nil
end
