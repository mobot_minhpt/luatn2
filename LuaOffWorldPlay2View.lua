local GameObject = import "UnityEngine.GameObject"
local CGamePlayMgr=import "L10.Game.CGamePlayMgr"
local CScene = import "L10.Game.CScene"
local UITable = import "UITable"

LuaOffWorldPlay2View = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaOffWorldPlay2View, "TaskName", "TaskName", UILabel)
RegistChildComponent(LuaOffWorldPlay2View, "TaskDes1", "TaskDes1", UILabel)
RegistChildComponent(LuaOffWorldPlay2View, "LeaveBtn", "LeaveBtn", GameObject)
RegistChildComponent(LuaOffWorldPlay2View, "InviteBtn", "InviteBtn", GameObject)
RegistChildComponent(LuaOffWorldPlay2View, "stage1", "stage1", GameObject)
RegistChildComponent(LuaOffWorldPlay2View, "stage2", "stage2", GameObject)

--@endregion RegistChildComponent end

function LuaOffWorldPlay2View:Awake()
    --@region EventBind: Dont Modify Manually!    --@endregion EventBind end
end

function LuaOffWorldPlay2View:OnEnable()
	g_ScriptEvent:AddListener("UpdateOffWorldPlay2Data", self, "UpdateInfo")
	g_ScriptEvent:AddListener("UpdateOffWorldPlay2TaskData", self, "UpdateTask")
  g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
	--g_ScriptEvent:AddListener("OnScreenChange", self, "UpdateScreen")
end

function LuaOffWorldPlay2View:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateOffWorldPlay2Data", self, "UpdateInfo")
	g_ScriptEvent:RemoveListener("UpdateOffWorldPlay2TaskData", self, "UpdateTask")
  g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
	--g_ScriptEvent:RemoveListener("OnScreenChange", self, "UpdateScreen")
end

function LuaOffWorldPlay2View:GetRemainTimeText(totalSeconds)
    if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        return SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end

function LuaOffWorldPlay2View:OnSceneRemainTimeUpdate(args)
    if CScene.MainScene then
        if CScene.MainScene.ShowTimeCountDown then
            self.TaskDes1.text = self:GetRemainTimeText(CScene.MainScene.ShowTime)
        else
            self.TaskDes1.text = nil
        end
    else
        self.TaskDes1.text = nil
    end
end

function LuaOffWorldPlay2View:UpdateTask()
  if LuaOffWorldPassMgr.Play2TaskData and LuaOffWorldPassMgr.Play2TaskData[1] then
    if LuaOffWorldPassMgr.Play2StageData[3] and LuaOffWorldPassMgr.Play2StageData[3] > 0 then
      self.stage1:SetActive(false)
      self.stage2:SetActive(true)
      if LuaOffWorldPassMgr.Play2StageData[3] == CClientMainPlayer.Inst.Id then
        self.stage2.transform:Find('TaskName1'):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage('OffWorldPlay2T1') --LocalString.GetString('击败同伴')
        self.stage2.transform:Find('TaskName2'):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage('OffWorldPlay2T2') --LocalString.GetString('中了鬼司仪的降头，击败同伴获取晶石')
        self.stage2.transform:Find('num').gameObject:SetActive(false)
      else
        self.stage2.transform:Find('TaskName1'):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage('OffWorldPlay2T3') --LocalString.GetString('击败小鬼司仪')
        self.stage2.transform:Find('TaskName2'):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage('OffWorldPlay2T4') --LocalString.GetString('击败被下降头的同伴后才能继续任务')
        self.stage2.transform:Find('num').gameObject:SetActive(true)
      end
    else
      self.stage1:SetActive(true)
      self.stage2:SetActive(false)

			local fxNode = self.stage1.transform:Find('Fx').gameObject
			fxNode:SetActive(true)
			local fxSc = fxNode:GetComponent(typeof(CUIFx))
			fxSc:DestroyFx()
			fxSc:LoadFx('fx/ui/prefab/UI_caidiezhen_shuaxin.prefab')

      local taskType = LuaOffWorldPassMgr.Play2TaskData[1]
      local taskId = nil
			OffWorldPass_CaiDieTask.Foreach(function(i,v)
				if v.type == taskType then
					taskId = v.id
				end
			end)

			if taskId then
				local taskData = OffWorldPass_CaiDieTask.GetData(taskId)
				if taskData then
					self.stage1.transform:Find('TaskName1'):GetComponent(typeof(UILabel)).text = taskData.description
					if taskData.reward > 0 then
						self.stage1.transform:Find('num').gameObject:SetActive(true)
						self.stage1.transform:Find('num'):GetComponent(typeof(UILabel)).text = '+' .. taskData.reward
					else
						self.stage1.transform:Find('num').gameObject:SetActive(false)
					end
				else
					self.stage1:SetActive(false)
					self.stage2:SetActive(false)
				end
			else
				self.stage1:SetActive(false)
				self.stage2:SetActive(false)
			end
    end
  else
    self.stage1:SetActive(true)
    self.stage2:SetActive(false)
    self.stage1.transform:Find('TaskName1'):GetComponent(typeof(UILabel)).text = ''
    self.stage1.transform:Find('num'):GetComponent(typeof(UILabel)).text = ''
  end

  self.transform:GetComponent(typeof(UITable)):Reposition()
end

function LuaOffWorldPlay2View:UpdateInfo()
  if LuaOffWorldPassMgr.Play2StageData then
    if LuaOffWorldPassMgr.Play2StageData[1] == 1 then
      self:UpdateTask()
    elseif LuaOffWorldPassMgr.Play2StageData[1] == 2 then
      --self.stage1:SetActive(false)
      --self.stage2:SetActive(false)
      --self:UpdateTask()
    end
  end

  self.transform:GetComponent(typeof(UITable)):Reposition()
end

function LuaOffWorldPlay2View:InitPhaText()
  local sceneName = CScene.MainScene.SceneName
  --local pha1Text = g_MessageMgr:FormatMessage("QingMingNHTQText")
  local showText = '[c][FFC800]' .. sceneName .. '[-][c]'

  self.TaskName.text = showText
  self.TaskDes1.text = ''
end

function LuaOffWorldPlay2View:Init()
  UIEventListener.Get(self.LeaveBtn.gameObject).onClick = LuaUtils.VoidDelegate(function(go)
    CGamePlayMgr.Inst:LeavePlay()
  end)
  UIEventListener.Get(self.InviteBtn.gameObject).onClick = LuaUtils.VoidDelegate(function(go)
    --g_MessageMgr:ShowMessage('OffWorldPlay2Tip')
		LuaOffWorldPassMgr:ShowRuleWnd()
  end)

  self:InitPhaText()
  self:UpdateInfo()

end



--@region UIEvent

--@endregion UIEvent
