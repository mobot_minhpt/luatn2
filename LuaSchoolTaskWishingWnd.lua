local CBaseWnd = import "L10.UI.CBaseWnd"
local DelegateFactory = import "DelegateFactory"
local CommonDefs = import "L10.Game.CommonDefs"
local CUICommonDef = import "L10.UI.CUICommonDef"
local UIInput = import "UIInput"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"

LuaSchoolTaskWishingWnd = class()
RegistClassMember(LuaSchoolTaskWishingWnd,"m_CloseBtn")
RegistClassMember(LuaSchoolTaskWishingWnd,"m_Input")
RegistClassMember(LuaSchoolTaskWishingWnd,"m_ConfirmBtn")

function LuaSchoolTaskWishingWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaSchoolTaskWishingWnd:Init()
	self.m_CloseBtn = self.transform:Find("Wnd_Bg/CloseButton").gameObject
	CommonDefs.AddOnClickListener(self.m_CloseBtn, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)

	self.m_Input = self.transform:Find("Anchor/Input"):GetComponent(typeof(UIInput))
	self.m_ConfirmBtn = self.transform:Find("Anchor/Button").gameObject

	CommonDefs.AddOnClickListener(self.m_ConfirmBtn, DelegateFactory.Action_GameObject(function(go) self:OnConfirmButtonClick() end), false)

	self.m_Input.characterLimit = 200
    CommonDefs.AddEventDelegate(self.m_Input.onChange, DelegateFactory.Action(function ( ... )
       self:OnInputChange()
    end))
end

function LuaSchoolTaskWishingWnd:OnInputChange()
	 local str = self.m_Input.value
            if CUICommonDef.GetStrByteLength(str) > 40 then
            repeat
                str = CommonDefs.StringSubstring2(str, 0, CommonDefs.StringLength(str) - 1)
            until not (CUICommonDef.GetStrByteLength(str) > 40)
            self.m_Input.value = str
    end
end

function LuaSchoolTaskWishingWnd:OnConfirmButtonClick()
	local text = CUICommonDef.Trim(self.m_Input.value)
	if text == "" then
		g_MessageMgr:ShowMessage("Newbie_School_Task_Wishing_Is_Empty")
		return
	end

	--检查是否有敏感词
	local ret = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(text, false)
    if ret.msg == nil or ret.shouldBeIgnore then
        g_MessageMgr:ShowMessage("Speech_Violation")
        return
    end

    LuaSchoolTaskMgr:SubmitWishingWords(ret.msg)
	self:Close()
end
