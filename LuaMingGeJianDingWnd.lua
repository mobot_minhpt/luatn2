local Ease = import "DG.Tweening.Ease"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local Animation = import "UnityEngine.Animation"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"

LuaMingGeJianDingWnd = class()

RegistChildComponent(LuaMingGeJianDingWnd ,"m_MingPanView","MingPanView", UIPanel)
RegistChildComponent(LuaMingGeJianDingWnd ,"m_WuXingView","WuXingView", UIPanel)
RegistChildComponent(LuaMingGeJianDingWnd ,"m_MingPan","MingPan", GameObject)
RegistChildComponent(LuaMingGeJianDingWnd ,"m_Metal","Metal", GameObject)
RegistChildComponent(LuaMingGeJianDingWnd ,"m_Plant","Plant", GameObject)
RegistChildComponent(LuaMingGeJianDingWnd ,"m_Water","Water", GameObject)
RegistChildComponent(LuaMingGeJianDingWnd ,"m_Fire","Fire", GameObject)
RegistChildComponent(LuaMingGeJianDingWnd ,"m_Soil","Soil", GameObject)
RegistChildComponent(LuaMingGeJianDingWnd ,"m_Circle1","Circle1", GameObject)
RegistChildComponent(LuaMingGeJianDingWnd ,"m_Circle2","Circle2", GameObject)
RegistChildComponent(LuaMingGeJianDingWnd ,"m_IntroductionLabel","IntroductionLabel", UILabel)
RegistChildComponent(LuaMingGeJianDingWnd ,"m_WuXingTex","WuXingTex", CUITexture)
RegistChildComponent(LuaMingGeJianDingWnd ,"m_MingPan1","MingPan1", GameObject)
RegistChildComponent(LuaMingGeJianDingWnd ,"m_MingPan2","MingPan2", GameObject)
RegistChildComponent(LuaMingGeJianDingWnd ,"m_MingPan3","MingPan3", GameObject)
RegistChildComponent(LuaMingGeJianDingWnd ,"m_FaZhenView","FaZhenView", GameObject)
RegistChildComponent(LuaMingGeJianDingWnd ,"m_FaZhenWuXingTex","FaZhenWuXingTex", CUITexture)
RegistChildComponent(LuaMingGeJianDingWnd ,"m_MyWuXingTex","MyWuXingTex", CUITexture)
RegistChildComponent(LuaMingGeJianDingWnd ,"m_WuXingNumLabel","WuXingNumLabel", UILabel)
RegistChildComponent(LuaMingGeJianDingWnd ,"m_CloseButton","CloseButton", GameObject)
RegistChildComponent(LuaMingGeJianDingWnd ,"m_ConsumeItem","ConsumeItem", GameObject)
RegistChildComponent(LuaMingGeJianDingWnd ,"m_ConsumeButton","ConsumeButton", GameObject)
RegistChildComponent(LuaMingGeJianDingWnd,"m_TipButton","TipButton", GameObject)
RegistClassMember(LuaMingGeJianDingWnd, "m_Circle1Radius")
RegistClassMember(LuaMingGeJianDingWnd, "m_Circle2Radius")
RegistClassMember(LuaMingGeJianDingWnd, "m_IsRotated")
RegistClassMember(LuaMingGeJianDingWnd, "m_FaZhenWuXing")
RegistClassMember(LuaMingGeJianDingWnd, "m_MingGe")
RegistClassMember(LuaMingGeJianDingWnd, "m_FaZhenStatus")
RegistClassMember(LuaMingGeJianDingWnd, "m_WuXingViewAni")

function LuaMingGeJianDingWnd:Awake()
    UIEventListener.Get(self.m_MingPan).onDragStart= DelegateFactory.VoidDelegate(function(g)
        self:StartJianDing()
    end)
    UIEventListener.Get(self.m_CloseButton).onClick = DelegateFactory.VoidDelegate(function(g)
        CUIManager.CloseUI(CLuaUIResources.MingGeJianDingWnd)
        CUIManager.CloseUI(CLuaUIResources.NewMingGeJianDingWnd)
    end)
    UIEventListener.Get(self.m_ConsumeButton).onClick = DelegateFactory.VoidDelegate(function(g)
        self:OnConsumeButtonClick()
    end)
    if self.m_TipButton then
        UIEventListener.Get(self.m_TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnTipButtonClicked(go) end)
    end
end

function LuaMingGeJianDingWnd:Init()
    self.m_FaZhenView.gameObject:SetActive(LuaZongMenMgr.m_EnableNewMingGeJianDing)
    --self.m_CloseButton.gameObject:SetActive(not LuaZongMenMgr.m_EnableNewMingGeJianDing)
    self.m_ConsumeItem.gameObject:SetActive(false)
    self.m_ConsumeButton.gameObject:SetActive(false)
    self.m_WuXingView.gameObject:SetActive(true)
    self.m_Circle1Radius = self.m_Circle1.transform.localPosition.y
    self.m_Circle2Radius = self.m_Circle2.transform.localPosition.y
    self.m_WuXingView.alpha = 0
    self.m_MingPanView.gameObject:SetActive(true)
    self:InitWuXingPos()
    local vfx = self.transform:Find("Anchor/vfx")
    if vfx then
        vfx.gameObject:SetActive(false)
    end
    self.m_IntroductionLabel.text = LuaZongMenMgr.m_EnableNewMingGeJianDing and LocalString.GetString("滑动命盘获取新的五行") or LocalString.GetString("滑动命盘进行命格鉴定")
    if LuaZongMenMgr.m_OpenNewSystem then
        self.m_WuXingViewAni = self.m_WuXingView.gameObject:GetComponent(typeof(Animation))
    end
    if LuaZongMenMgr.m_EnableNewMingGeJianDing then
        Gac2Gas.QuerySectFaZhenCurrentInfo()
        Gac2Gas.QueryPlayerMingGe()
    end
end

function LuaMingGeJianDingWnd:StartJianDing()
    if self.m_IsRotated then return end
    self.m_IsRotated = true
    local updateIsRotatedTween = LuaTweenUtils.TweenFloat(0, 1, 3, function (val) end,function () 
        self.m_IsRotated = false
    end ,Ease.OutSine)
    LuaTweenUtils.SetTarget(updateIsRotatedTween, self.transform)
    self.m_ConsumeItem.gameObject:SetActive(false)
    self.m_ConsumeButton.gameObject:SetActive(false)
    if LuaZongMenMgr.m_EnableNewMingGeJianDing then
        local sectId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.SectId or 0
        Gac2Gas.RequestRandomWuXing_Ite(sectId)
    else
        Gac2Gas.RequestJianDingMingGe()
    end
end

function LuaMingGeJianDingWnd:RotateTwoCircles(move1, move2)
    LuaTweenUtils.DOKill(self.transform, false)
    local circle1RotateTween = LuaTweenUtils.TweenFloat(0, 360*5 + move1 * 72, 5, function (val)
        local r = math.rad(val)
        local x = math.sin(r)
        local y = math.cos(r)
        self.m_Circle1.transform.localPosition = Vector3(x * self.m_Circle1Radius,y * self.m_Circle1Radius,0)
        Extensions.SetLocalRotationZ(self.m_Circle1.transform, -val)
    end,function () self:OnMingPanViewFadeOut() end,Ease.OutSine)
    local circle2RotateTween = LuaTweenUtils.TweenFloat(0, 360*5 + move2 * 72, 5, function (val)
        local r = math.rad(val)
        local x = math.sin(r)
        local y = math.cos(r)
        self.m_Circle2.transform.localPosition = Vector3(-x * self.m_Circle2Radius,y * self.m_Circle2Radius,0)
        Extensions.SetLocalRotationZ(self.m_Circle2.transform, val)
    end,function () end ,Ease.OutSine)
    local mingePanRotateTween = LuaTweenUtils.TweenFloat(0, 360*5, 5, function (val)
        if not self.m_WuXingViewAni then
            Extensions.SetLocalRotationZ(self.m_MingPan1.transform, val)
            Extensions.SetLocalRotationZ(self.m_MingPan2.transform,  - val)
        end
    end,function () end ,Ease.OutSine)
    LuaTweenUtils.SetTarget(circle1RotateTween, self.transform)
    LuaTweenUtils.SetTarget(circle2RotateTween, self.transform)
    LuaTweenUtils.SetTarget(mingePanRotateTween, self.transform)
end

function LuaMingGeJianDingWnd:OnMingPanViewFadeOut()
    LuaTweenUtils.DOKill(self.transform, true)
    if self.m_WuXingViewAni then
        self:OnFinished()
    else
        local mingPanFadeoutTween = LuaTweenUtils.TweenFloat(2, 0, 1, function (val)
                if val <= 1 then
                    self.m_WuXingView.alpha = 1 - val
                    self.m_MingPanView.alpha = val
                end
            end,function () self:OnFinished() end)
        LuaTweenUtils.SetTarget(mingPanFadeoutTween, self.transform)
    end
end

function LuaMingGeJianDingWnd:OnFinished()
    if self.m_WuXingViewAni and not LuaZongMenMgr.m_EnableNewMingGeJianDing then
        self.transform:Find("Anchor/BG_panel/bg_Texture/zuoxiajiao_Texture").gameObject:SetActive(false)
        self.m_MingPanView.alpha = 0
        self.m_WuXingView.alpha = 1
        self.m_WuXingView.gameObject:SetActive(false)
        self.m_WuXingView.gameObject:SetActive(true)
        self.m_WuXingViewAni:Stop()
        self.m_WuXingViewAni:Play("minggejiandingwnd_wuxingview_show")
    end
    if LuaZongMenMgr.m_EnableNewMingGeJianDing and LuaZongMenMgr.m_OpenNewSystem then
        --self:OnSyncPlayerMingGe(self.m_MingGe)
        self.m_IntroductionLabel.text = ""
        --self.m_CloseButton.gameObject:SetActive(true)
        local vfx = self.transform:Find("Anchor/vfx")
        if vfx then
            vfx.gameObject:SetActive(false)
            vfx.gameObject:SetActive(true)
        end
        LuaTweenUtils.DOKill(self.transform, true)
        local tween = LuaTweenUtils.TweenFloat(0, 0, 1.2, function (val)
            end,function () 
                self:OnSyncPlayerMingGe(self.m_MingGe)
                Gac2Gas.QuerySectFaZhenCurrentInfo()
            end)
        LuaTweenUtils.SetTarget(tween, self.transform)
        if self.m_FaZhenWuXing == self.m_MingGe then
            tween = LuaTweenUtils.TweenFloat(0, 0, 2, function (val)
            end,function () 
                local vfx2 = self.transform:Find("Anchor/FaZhenView/vfx")
                if vfx2 then
                    vfx2.gameObject:SetActive(false) 
                    vfx2.gameObject:SetActive(true) 
                end
            end)
            LuaTweenUtils.SetTarget(tween, self.transform)
        end
        self:InitConsumeItemAndBtn()
        return
    end
    self.m_IntroductionLabel.text = LocalString.GetString("命格鉴定成功")
end

function LuaMingGeJianDingWnd:InitConsumeItemAndBtn()
    self.m_ConsumeItem.gameObject:SetActive(true)
    self.m_ConsumeButton.gameObject:SetActive(true)
    local itemId = tonumber(Menpai_Setting.GetData("RandomMinggeItemId").Value)
    local itemData = Item_Item.GetData(itemId)
    self.m_ConsumeItem.transform:Find("Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(itemData.Icon)
    local count = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, itemId)
    local label = self.m_ConsumeItem.transform:Find("CountLabel"):GetComponent(typeof(UILabel))
    if count < 1 then
        label.text = SafeStringFormat3("[c][ff0000]%d[-][/c]/%d", count, 1)
    else
        label.text = SafeStringFormat3("%d/%d", count, 1)
    end
    self.m_ConsumeItem.transform:Find("GetItemLabel").gameObject:SetActive(count < 1)
    UIEventListener.Get(self.m_ConsumeItem.gameObject).onClick = DelegateFactory.VoidDelegate(function(g)
        if count < 1 then
            CItemAccessListMgr.Inst:ShowItemAccessInfo(itemId, false, self.m_ConsumeItem.transform,  CTooltipAlignType.Right)
        else
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemId)
        end
    end)
end

function LuaMingGeJianDingWnd:OnEnable()
    g_ScriptEvent:AddListener("OnZongMenMingGeJianDing", self, "OnZongMenMingGeJianDing")
    g_ScriptEvent:AddListener("OnSyncPlayerMingGe", self, "OnSyncPlayerMingGe")
    g_ScriptEvent:AddListener("QuerySectFaZhenCurrentInfoResult", self, "OnQuerySectFaZhenCurrentInfoResult")
end

function LuaMingGeJianDingWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnZongMenMingGeJianDing", self, "OnZongMenMingGeJianDing")
    g_ScriptEvent:RemoveListener("OnSyncPlayerMingGe", self, "OnSyncPlayerMingGe")
    g_ScriptEvent:RemoveListener("QuerySectFaZhenCurrentInfoResult", self, "OnQuerySectFaZhenCurrentInfoResult")
    LuaTweenUtils.DOKill(self.transform, false)
    LuaZongMenMgr.m_EnableNewMingGeJianDing = false
end

function LuaMingGeJianDingWnd:OnQuerySectFaZhenCurrentInfoResult(status)
    self.m_FaZhenStatus = status
    self.m_FaZhenWuXing = status["fazhenWuXing"]
    self.m_FaZhenWuXingTex:LoadMaterial(LuaZongMenMgr:GetMingGeMatPath(self.m_FaZhenWuXing))
    local colorStr = ""
    if status["wuxingFactor"] >1 then
        colorStr = "[00FF00]"
    elseif status["wuxingFactor"] <1 then
        colorStr = "[FF5050]"
    end
    local wuxingExtraDesc = ""
    if status["wuxingFactor"] and status["isInputSoulCore"] then
        local index = -1
        local descStr = {LocalString.GetString("（强相克）"), LocalString.GetString("（中相克）"), LocalString.GetString("（弱相克）"), LocalString.GetString("（均衡）"),
        LocalString.GetString("（弱相生）"),LocalString.GetString("（中相生）"),LocalString.GetString("（强相生）")}
        local params = LianHua_Setting.GetData().WuXingFxControlParams
        for i=0, params.Length-1 do
            if math.abs(params[i]-status["wuxingFactor"]) < 0.02 then
                index = i+1
                break
            end
        end
        if index>0 and index<8 then
            wuxingExtraDesc = descStr[index]
        end
    end
    self.m_WuXingNumLabel.color = Color.white
    self.m_WuXingNumLabel.text = colorStr .. tostring(status["wuxingFactor"]) .. wuxingExtraDesc
end

function LuaMingGeJianDingWnd:OnSyncPlayerMingGe(mingge)
    self.m_MingGe = mingge
    self.m_MyWuXingTex:LoadMaterial(LuaZongMenMgr:GetMingGeMatPath(mingge))
    g_ScriptEvent:RemoveListener("OnSyncPlayerMingGe", self, "OnSyncPlayerMingGe")
end

function LuaMingGeJianDingWnd:OnZongMenMingGeJianDing(mingge)
    self.m_MingGe = mingge
    self.m_WuXingTex:LoadMaterial(LuaZongMenMgr:GetMingGeMatPath(mingge))
    local data = SoulCore_WuXing.GetData(mingge)
    local res = {}
    if data.Name1 == 1 then
        table.insert(res, 2)
    end
    if data.Name2 == 1 then
        table.insert(res, 3)
    end
    if data.Name3 == 1 then
        table.insert(res, 4)
    end
    if data.Name4 == 1 then
        table.insert(res, 5)
    end
    if data.Name5 == 1 then
        table.insert(res, 1)
    end
    local move1 = res[1]
    local move2 = (#res == 2) and res[2] or res[1]
    self:RotateTwoCircles(move1,5 - move2)
end

function LuaMingGeJianDingWnd:InitWuXingPos()
    local radius = self.m_Fire.transform.localPosition.y
    local a = math.sin(math.rad(72)) * radius
    local b = math.cos(math.rad(72)) * radius
    local c = math.sin(math.rad(36)) * radius
    local d = math.cos(math.rad(36)) * radius
    self.m_Fire.transform.localPosition = Vector3(0,radius,0)
    self.m_Plant.transform.localPosition = Vector3(-a,b,0) 
    self.m_Soil.transform.localPosition = Vector3(a,b,0) 
    self.m_Water.transform.localPosition = Vector3(-c,-d,0) 
    self.m_Metal.transform.localPosition = Vector3(c,-d,0) 
end

function LuaMingGeJianDingWnd:OnConsumeButtonClick()
    if LuaZongMenMgr:CheckFaZhenOperation(LocalString.GetString("修改命盘")) then
        local sectId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.SectId or 0
        self.m_ConsumeItem.gameObject:SetActive(false)
        self.m_ConsumeButton.gameObject:SetActive(false)
        local isFirst = false
        if isFirst then
            Gac2Gas.RequestRandomWuXing_Ite(sectId) 
        else
            self.m_IsRotated = false
            self.m_IntroductionLabel.text = LuaZongMenMgr.m_EnableNewMingGeJianDing and LocalString.GetString("滑动命盘获取新的五行") or LocalString.GetString("滑动命盘进行命格鉴定")
        end
    end
end

function LuaMingGeJianDingWnd:OnTipButtonClicked(go)
    g_MessageMgr:ShowMessage("NewMingGeJianDingWnd_ReadMe")
end

