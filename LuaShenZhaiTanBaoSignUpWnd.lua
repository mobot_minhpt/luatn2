local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CQnSymbolParser = import "CQnSymbolParser"

LuaShenZhaiTanBaoSignUpWnd = class()


function LuaShenZhaiTanBaoSignUpWnd:Awake()
    self:InitGameplayData()
    self:InitUI()
    self:InitSendShenZhaiTanBaoPlayTimes()
end

function LuaShenZhaiTanBaoSignUpWnd:Init()

end

function LuaShenZhaiTanBaoSignUpWnd:InitGameplayData()
    self.SZTBConfigData = Double11_SZTB.GetData()
    self.gameplayId = self.SZTBConfigData.PlayDesignId
    self.rewardTimeLimit = self.SZTBConfigData.RewardTimesLimit
end

function LuaShenZhaiTanBaoSignUpWnd:InitUI()
    self.openTimeText = self.transform:Find("Anchor/InfoView/Content/RuleDetailView/OpenTimeLabel"):GetComponent(typeof(UILabel))
    self.ruleBriefText = self.transform:Find("Anchor/InfoView/Content/RuleDetailView/RuleBriefLabel"):GetComponent(typeof(UILabel))
    self.rewardItem = self.transform:Find("Anchor/InfoView/Content/RewardView/RewardItem")
    self.rewardTimeText = self.transform:Find("Anchor/InfoView/Content/RewardView/RewardTime"):GetComponent(typeof(UILabel))
    self.ruleButton = self.transform:Find("Anchor/InfoView/Content/RewardView/RuleButton")
    self.enterButton = self.transform:Find("Anchor/InfoView/Content/EnterButton")
    
    self.openTimeText.text = CQnSymbolParser.ConvertQnTextToNGUIText(LocalString.GetString(self.SZTBConfigData.SZTBTime))
    self.ruleBriefText.text = CQnSymbolParser.ConvertQnTextToNGUIText(LocalString.GetString(self.SZTBConfigData.SZTBTaskDisc))

    self:InitOneItem(self.rewardItem.gameObject, self.SZTBConfigData.RewardItemId)

    UIEventListener.Get(self.enterButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        Gac2Gas.Double11SZTB_EnterGame()
    end)

    UIEventListener.Get(self.ruleButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        g_MessageMgr:ShowMessage("Double11_ShenZhaiTanBao_Tips")
    end)
end

function LuaShenZhaiTanBaoSignUpWnd:InitSendShenZhaiTanBaoPlayTimes()
    local rewardTimes = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eDouble11SZTBRewardTimes)
    local remainRewards = self.SZTBConfigData.RewardTimesLimit - rewardTimes
    if remainRewards == 0 then
        self.rewardTimeText.text = System.String.Format("[c][ff5050]{0}[-][/c]/{1}", remainRewards, self.SZTBConfigData.RewardTimesLimit)
    else
        self.rewardTimeText.text = System.String.Format("{0}/{1}", remainRewards, self.SZTBConfigData.RewardTimesLimit)
    end
end

function LuaShenZhaiTanBaoSignUpWnd:InitOneItem(curItem, itemID)
    local texture = curItem.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local ItemData = Item_Item.GetData(itemID)
    if ItemData then texture:LoadMaterial(ItemData.Icon) end
    UIEventListener.Get(curItem).onClick = DelegateFactory.VoidDelegate(function (_)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemID, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
    end)
end

function LuaShenZhaiTanBaoSignUpWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncSZTBPlayState", self, "OnCloseSignUpWnd")
end

function LuaShenZhaiTanBaoSignUpWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncSZTBPlayState", self, "OnCloseSignUpWnd")
end

function LuaShenZhaiTanBaoSignUpWnd:OnCloseSignUpWnd()
    CUIManager.CloseUI(CLuaUIResources.ShenZhaiTanBaoSignUpWnd)
    CUIManager.CloseUI(CLuaUIResources.Shuangshiyi2022MainWnd)
end 