local CUIFxPaths = import "L10.UI.CUIFxPaths"

LuaGuildTerritorialWarsZhanShenView = class()

RegistClassMember(LuaGuildTerritorialWarsZhanShenView, "Remind")
RegistClassMember(LuaGuildTerritorialWarsZhanShenView, "RemindFx")
RegistClassMember(LuaGuildTerritorialWarsZhanShenView, "TaskInfo")
RegistClassMember(LuaGuildTerritorialWarsZhanShenView, "TaskFinish")
RegistClassMember(LuaGuildTerritorialWarsZhanShenView, "TaskInfoLabel")

RegistClassMember(LuaGuildTerritorialWarsZhanShenView, "m_InitialParentY")

function LuaGuildTerritorialWarsZhanShenView:Awake()
    self.Remind = self.transform:Find("ZhanShenRemind").gameObject
    self.RemindFx = self.Remind.transform:Find("Fx"):GetComponent(typeof(CUIFx))
    self.TaskInfo = self.transform:Find("ZhanShenTask").gameObject
    self.TaskFinish = self.TaskInfo.transform:Find("Finish").gameObject
    self.TaskInfoLabel = self.TaskInfo.transform:Find("TaskInfo"):GetComponent(typeof(UILabel))

    UIEventListener.Get(self.Remind).onClick = DelegateFactory.VoidDelegate(function (go)
        self.RemindFx:DestroyFx()
        CUIManager.ShowUI(CLuaUIResources.GuildTerritorialWarsZhanShenTaskWnd)
    end)

    self.m_InitialParentY = -76
end

function LuaGuildTerritorialWarsZhanShenView:OnEnable()
    g_ScriptEvent:AddListener("SendGTWZhanShenTaskChooseData", self, "Refresh")
    g_ScriptEvent:AddListener("SyncGTWZhanShenTaskInfo", self, "Refresh")
    g_ScriptEvent:AddListener("MainPlayerDestroyed", self, "OnMainPlayerDestroyed")

    self:Refresh()
    Gac2Gas.RequestGTWZhanShenTaskInfo()
end

function LuaGuildTerritorialWarsZhanShenView:OnDisable()
    g_ScriptEvent:RemoveListener("SendGTWZhanShenTaskChooseData", self, "Refresh")
    g_ScriptEvent:RemoveListener("SyncGTWZhanShenTaskInfo", self, "Refresh")
    g_ScriptEvent:RemoveListener("MainPlayerDestroyed", self, "OnMainPlayerDestroyed")    
end

function LuaGuildTerritorialWarsZhanShenView:OnMainPlayerDestroyed()
    LuaGuildTerritorialWarsMgr.m_ZhanShenAcceptedTaskId = 0
    LuaGuildTerritorialWarsMgr.m_ZhanShenProgress = 0
    LuaGuildTerritorialWarsMgr.m_ZhanShenIsFinish = false
    LuaGuildTerritorialWarsMgr.m_ZhanShenTaskIds = {}
end

function LuaGuildTerritorialWarsZhanShenView:Refresh()
    self.Remind:SetActive(false)
    self.TaskInfo:SetActive(false)

    local acceptedTaskId = LuaGuildTerritorialWarsMgr.m_ZhanShenAcceptedTaskId
    local progress = LuaGuildTerritorialWarsMgr.m_ZhanShenProgress
    local isFinish = LuaGuildTerritorialWarsMgr.m_ZhanShenIsFinish
    if acceptedTaskId > 0 then
        self.TaskInfo:SetActive(true)
        self.TaskFinish:SetActive(isFinish)
        local data = GuildTerritoryWar_ZhanShenTask.GetData(acceptedTaskId)
        if data then
            local str = SafeStringFormat3("%s%s[-]/%s", isFinish and "[00ff60]" or "[ff0000]", progress, data.Target) 
            self.TaskInfoLabel.text = SafeStringFormat3(data.Desc, str)
        end
    elseif not self.Remind.activeSelf then
        self.Remind:SetActive(true)
        self.RemindFx:DestroyFx()
        self.RemindFx:LoadFx(CUIFxPaths.CommonYellowLineFxPath)
        LuaTweenUtils.DOKill(self.RemindFx.FxRoot,false)
        local path={Vector3(180,35,0),Vector3(180,-35,0),Vector3(-180,-35,0),Vector3(-180,35,0),Vector3(180,35,0)}
        local array = Table2Array(path, MakeArrayClass(Vector3))
        LuaUtils.SetLocalPosition(self.RemindFx.FxRoot,180,35,0)
        LuaTweenUtils.DODefaultLocalPath(self.RemindFx.FxRoot,array,2)
    end
    self:Layout()
end

function LuaGuildTerritorialWarsZhanShenView:Layout()
    local height = NGUIMath.CalculateRelativeWidgetBounds(self.transform).size.y
    LuaUtils.SetLocalPositionY(self.transform, height - 5)
    if self.TaskInfo.activeSelf then 
        height = height - 62 --NGUIMath.CalculateRelativeWidgetBounds(self.Remind.transform, self.Remind.transform, true, true).size.y 
    end
    LuaUtils.SetLocalPositionY(self.transform.parent, self.m_InitialParentY - height)
end
