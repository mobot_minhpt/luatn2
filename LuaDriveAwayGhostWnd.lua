require("common/common_include")

local CBaseWnd = import "L10.UI.CBaseWnd"
local CDangongBulletAndBird = import "CDangongBulletAndBird"
local CommonDefs = import "L10.Game.CommonDefs"
local CUIManager = import "L10.UI.CUIManager"
local DelegateFactory = import "DelegateFactory"
local Quaternion = import "UnityEngine.Quaternion"
local Random = import "UnityEngine.Random"
local Screen = import "UnityEngine.Screen"
local UICamera = import "UICamera"
local UIRoot = import "UIRoot"
local Vector2 = import "UnityEngine.Vector2"
local Vector3 = import "UnityEngine.Vector3"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaDriveAwayGhostWnd = class()
RegistClassMember(LuaDriveAwayGhostWnd,"m_Bullet")
RegistClassMember(LuaDriveAwayGhostWnd,"m_DiZuo")
RegistClassMember(LuaDriveAwayGhostWnd,"m_AimLineRoot")
RegistClassMember(LuaDriveAwayGhostWnd,"m_AimLine")
RegistClassMember(LuaDriveAwayGhostWnd,"m_HitCountLabel")
RegistClassMember(LuaDriveAwayGhostWnd,"m_TimeLabel")
RegistClassMember(LuaDriveAwayGhostWnd,"m_BirdTemplate")
RegistClassMember(LuaDriveAwayGhostWnd,"m_CloseButton")
RegistClassMember(LuaDriveAwayGhostWnd,"m_BulletRoot")
RegistClassMember(LuaDriveAwayGhostWnd,"m_HitEffect")

--弹弓左右侧的绳子末端
RegistClassMember(LuaDriveAwayGhostWnd,"m_DanGongLeftAnchor")
RegistClassMember(LuaDriveAwayGhostWnd,"m_DanGongRightAnchor")
--弹弓中间的左右侧绳子末端
RegistClassMember(LuaDriveAwayGhostWnd,"m_BulletLeftAnchor")
RegistClassMember(LuaDriveAwayGhostWnd,"m_BulletRightAnchor")
--弹弓左右两个绳子
RegistClassMember(LuaDriveAwayGhostWnd,"m_DanGongLeftLine")
RegistClassMember(LuaDriveAwayGhostWnd,"m_DanGongRightLine")

RegistClassMember(LuaDriveAwayGhostWnd,"m_HitCount")
RegistClassMember(LuaDriveAwayGhostWnd,"m_TotalCount")
RegistClassMember(LuaDriveAwayGhostWnd,"m_EndTime")
RegistClassMember(LuaDriveAwayGhostWnd,"m_UpdateTick")

RegistClassMember(LuaDriveAwayGhostWnd,"m_DanGongPosY")
RegistClassMember(LuaDriveAwayGhostWnd,"m_Background")

RegistClassMember(LuaDriveAwayGhostWnd,"m_BulletCanHit")

--新手指引
RegistClassMember(LuaDriveAwayGhostWnd,"m_GuideRoot")
RegistClassMember(LuaDriveAwayGhostWnd,"m_GuideFinger")
RegistClassMember(LuaDriveAwayGhostWnd,"m_GuideArrow")
RegistClassMember(LuaDriveAwayGhostWnd,"m_GuideTick")

function LuaDriveAwayGhostWnd:Awake()
    self.gameObject:GetComponent(typeof(UIPanel)).IgnoreIphoneXMargin = true
end

function LuaDriveAwayGhostWnd:Close()
    self:EndShowGuide()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaDriveAwayGhostWnd:Init()

	self.m_CloseButton = self.transform:Find("ShootingArea/BulletRoot/CloseButton").gameObject
    CommonDefs.AddOnClickListener(self.m_CloseButton, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)

	self.m_Bullet = self.transform:Find("ShootingArea/BulletRoot/Bullet"):GetComponent(typeof(CDangongBulletAndBird))
    self.m_DiZuo = self.transform:Find("ShootingArea/BulletRoot/Bullet/Texture").gameObject
    self.m_AimLineRoot = self.transform:Find("ShootingArea/BulletRoot/AimLineRoot").gameObject
	self.m_AimLine = self.transform:Find("ShootingArea/BulletRoot/AimLineRoot/AimLine"):GetComponent(typeof(UISprite))
	self.m_HitCountLabel = self.transform:Find("Info/HitLabel"):GetComponent(typeof(UILabel))
    self.m_TimeLabel = self.transform:Find("Info/TimeLabel"):GetComponent(typeof(UILabel))
	self.m_BirdTemplate = self.transform:Find("ShootingArea/BirdRoot/BirdTemplate"):GetComponent(typeof(CDangongBulletAndBird))
	self.m_BulletRoot = self.transform:Find("ShootingArea/BulletRoot").transform
	self.m_DanGongLeftAnchor = self.transform:Find("ShootingArea/BulletRoot/LeftDangongAnchor").transform
	self.m_DanGongRightAnchor = self.transform:Find("ShootingArea/BulletRoot/RightDangongAnchor").transform
	self.m_BulletLeftAnchor = self.transform:Find("ShootingArea/BulletRoot/Bullet/LeftAnchor").transform
	self.m_BulletRightAnchor = self.transform:Find("ShootingArea/BulletRoot/Bullet/RightAnchor").transform
	self.m_DanGongLeftLine = self.transform:Find("ShootingArea/BulletRoot/LeftLine"):GetComponent(typeof(UITexture))
	self.m_DanGongRightLine = self.transform:Find("ShootingArea/BulletRoot/RightLine"):GetComponent(typeof(UITexture))
    self.m_Background = self.transform:Find("Bg"):GetComponent(typeof(UITexture))
    self.m_GuideRoot = self.transform:Find("ShootingArea/BulletRoot/GuideRoot").transform
    self.m_GuideFinger = self.m_GuideRoot:Find("Finger").transform
    self.m_GuideArrow = self.m_GuideRoot:Find("Arrow").transform
    self.m_GuideRoot.gameObject:SetActive(false)
    self.m_GuideTick = nil
    self.m_HitEffect = self.transform:Find("ShootingArea/BulletRoot/HitEffect").gameObject
    self.m_HitEffect:SetActive(false)

	self.m_HitCount = 0
	self.m_TotalCount = CLuaTaskMgr.m_DriveAwayGhostNeedCount
	self.m_EndTime = CServerTimeMgr.Inst.timeStamp + CLuaTaskMgr.m_DriveAwayGhostTotalSeconds
    self:RegisterTick()
    self.m_BulletCanHit = true

    self.m_BirdTemplate:LoadBird(20000421) -- NPC小鬼
    self.m_DanGongPosY = self.m_BulletRoot.transform.localPosition.y

	CommonDefs.AddOnDragListener(self.m_Bullet.gameObject,DelegateFactory.Action_GameObject_Vector2(function (go, delta)
		self:OnBulletDrag(go, delta)
	end),false)
	CommonDefs.AddOnPressListener(self.m_Bullet.gameObject,DelegateFactory.Action_GameObject_bool(function (go, pressed)
		self:OnBulletPress(go, pressed)
    end),false)
    

	self:InitBullet()
	self:LaunchBirds()
	self:RefreshShow()
    self:AdjustBackground()

    self:StartShowGuide()
end

function LuaDriveAwayGhostWnd:AdjustBackground()
    local screenRatio = Screen.width / Screen.height
    if screenRatio > 2 then
        self.m_Background.aspectRatio = screenRatio
        self.m_Background:ResetAnchors()
    else
        self.m_Background.aspectRatio = 2
        self.m_Background:ResetAnchors()
    end
end

function LuaDriveAwayGhostWnd:RegisterTick()
    self:CancelTick()
    self.m_UpdateTick = RegisterTick(function ( ... )
        self:OnUpdate()
        end, 500)
end

function LuaDriveAwayGhostWnd:CancelTick()
    if self.m_UpdateTick then
        UnRegisterTick(self.m_UpdateTick)
        self.m_UpdateTick = nil
    end
end

function LuaDriveAwayGhostWnd:OnUpdate()
    local diff =  self.m_EndTime - CServerTimeMgr.Inst.timeStamp
    if diff < 0 then
        self:OnTimeOut()
    else
        local min =  math.floor(diff/60)
        local sec = diff - min * 60
        self.m_TimeLabel.text = SafeStringFormat3("%02d:%02d", min, sec)
    end
end

function LuaDriveAwayGhostWnd:OnTimeOut()
    self:CancelTick()
    self:Close()
    g_MessageMgr:ShowMessage("Drive_Away_Ghost_Timeout")
end

function LuaDriveAwayGhostWnd:OnBulletDrag(go, delta)
	self.m_Bullet.Moving = false
    local currentPos = UICamera.currentTouch.pos
    local worldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(Vector3(currentPos.x, currentPos.y, 0))
    worldPos.z = 0
    local localPos = self.m_BulletRoot:InverseTransformPoint(worldPos)
    if localPos.y < 0 then
        self.m_Bullet.LocalPosition = localPos
        local angle = Vector3.Angle(Vector3(-localPos.x, -localPos.y, -localPos.z), Vector3(0, 1, 0))
        if localPos.x < 0 then
            angle = -angle
        end

        self.m_Bullet.LocalRotation = Quaternion.Euler(Vector3(0, 0, angle))
    else
        self.m_Bullet.LocalPosition = Vector3.zero
        self.m_Bullet.LocalRotation = Quaternion.Euler(Vector3.zero)
    end
    self:RefreshLinePos()
end

function LuaDriveAwayGhostWnd:OnBulletPress(go, pressed)
	local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
	local virtualScreenWidth = Screen.width * UICamera.currentCamera.rect.width * scale
	local virtualScreenHeight = Screen.height * scale
	if pressed then
		self.m_AimLine.gameObject:SetActive(true)
	else
		self.m_AimLine.gameObject:SetActive(false)
		if Vector3.Distance(Vector3.zero, go.transform.localPosition) > 50 then
            self:OnBulletDragEnd(go)
            self.m_DiZuo:SetActive(false)
            self.m_DanGongLeftLine.gameObject:SetActive(false)
            self.m_DanGongRightLine.gameObject:SetActive(false)
        else
            self:ResetBullet()
        end
	end
end

function LuaDriveAwayGhostWnd:OnBulletDragEnd(go)
    self:EndShowGuide()
	local localPos = self.m_Bullet.transform.localPosition
    self.m_Bullet.Moving = true
    self.m_Bullet.NormalizedDirection = CommonDefs.ImplicitConvert_Vector2_Vector3(CommonDefs.op_UnaryNegation_Vector3(localPos.normalized))
    self.m_Bullet.Speed = localPos.x == 0 and (- localPos.y) * 0.5 / self.m_Bullet.NormalizedDirection.y or (- localPos.x) * 0.5 / self.m_Bullet.NormalizedDirection.x
end

function LuaDriveAwayGhostWnd:ResetBullet()
    self.m_BulletCanHit = true
    --self.m_Bullet.gameObject:GetComponent(typeof(BoxCollider)).enabled = true
	self.m_Bullet.LocalPosition = Vector3.zero
    self.m_Bullet.LocalRotation = Quaternion.Euler(Vector3.zero)
    self.m_Bullet.Moving = false
    self.m_DiZuo:SetActive(true)
    self.m_DanGongLeftLine.gameObject:SetActive(true)
    self.m_DanGongRightLine.gameObject:SetActive(true)
    self:RefreshLinePos()
end

function LuaDriveAwayGhostWnd:LaunchBirds()
    local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    local virtualScreenWidth = Screen.width * scale * CUIManager.UIMainCamera.rect.width
    local virtualScreenHeight = Screen.height * scale
	local halfHeight = math.floor(virtualScreenHeight / 2)
	local halfWidth =  math.floor(virtualScreenWidth / 2)
	local LY = (Random.value * virtualScreenHeight) - halfHeight --[-halfHeight, halfHeight]
    local RY = (Random.value * virtualScreenHeight) - halfHeight --[-halfHeight, halfHeight]
    LY = math.min(math.max(self.m_DanGongPosY, LY), halfHeight * 0.3)
    RY = math.min(math.max(self.m_DanGongPosY, RY), halfHeight * 0.3)
    -- local LX = -halfWidth - 100
    -- local RX = halfWidth + 100
    local LX = -halfWidth * 0.8
    local RX = halfWidth * 0.8

    local lp = Vector3(LX, LY, 0)
    local rp = Vector3(RX, RY, 0)
    local bird = self:GenerateBird(Vector3.Distance(lp,rp))
    if Random.value > 0.5 then
        bird.LocalPosition = lp
        bird.NormalizedDirection = CommonDefs.ImplicitConvert_Vector2_Vector3((CommonDefs.op_Subtraction_Vector3_Vector3(rp, lp)).normalized)
    else
        bird.LocalPosition = rp
        bird.NormalizedDirection = CommonDefs.ImplicitConvert_Vector2_Vector3((CommonDefs.op_Subtraction_Vector3_Vector3(lp, rp)).normalized)
    end
    bird.Speed = Random.value * 10 + 20
    bird.Moving = true
    bird.gameObject:SetActive(true)
    if not CommonDefs.ListContains(self.m_BirdList, typeof(CDangongBulletAndBird), bird) then
        CommonDefs.ListAdd(self.m_BirdList, typeof(CDangongBulletAndBird), bird)
    end
    self.m_BirdTemplate:playAnimation("run")
end

function LuaDriveAwayGhostWnd:InitBullet()
	self.m_Bullet.LocalPosition = Vector3.zero
    self.m_Bullet.FlyDistance = 2000
    self.m_Bullet.OnCollideCallback = DelegateFactory.Action_GameObject(function (go) self:OnBulletHit(go) end)
    self.m_Bullet.OnStopCallback = DelegateFactory.Action_GameObject(function (go) self:OnBulletStop(go) end)
    self:RefreshLinePos()
    self.m_AimLine.gameObject:SetActive(false)
end

function LuaDriveAwayGhostWnd:RefreshShow()
	self.m_HitCountLabel.text = SafeStringFormat3("%d/%d", self.m_HitCount, self.m_TotalCount)
end

function LuaDriveAwayGhostWnd:GenerateBird(flyDistance)
	self.m_BirdTemplate.OnStopCallback = DelegateFactory.Action_GameObject(function (go) self:OnBirdStop(go) end)
	self.m_BirdTemplate.FlyDistance = flyDistance
	return self.m_BirdTemplate
end

function LuaDriveAwayGhostWnd:RefreshLinePos()
	local dV = self.m_DanGongLeftAnchor.localPosition
	local bV = self.m_BulletRoot:InverseTransformPoint(self.m_BulletLeftAnchor.position)
	self.m_DanGongLeftLine.transform.localPosition = CommonDefs.op_Division_Vector3_Single((CommonDefs.op_Addition_Vector3_Vector3(dV, bV)), 2)
	local angle = - Vector3.Angle(Vector3(1, 0, 0), (CommonDefs.op_Subtraction_Vector3_Vector3(bV, dV)))
	if bV.y > dV.y then
        angle = angle * - 1
    end
    self.m_DanGongLeftLine.transform.localRotation = Quaternion.Euler(Vector3(0, 0, angle))
    self.m_DanGongLeftLine.width = math.floor(Vector3.Distance(dV, bV))

    dV = self.m_DanGongRightAnchor.localPosition
	bV = self.m_BulletRoot:InverseTransformPoint(self.m_BulletRightAnchor.position)
	self.m_DanGongRightLine.transform.localPosition = CommonDefs.op_Division_Vector3_Single((CommonDefs.op_Addition_Vector3_Vector3(dV, bV)), 2)
	local angle = Vector3.Angle(Vector3(1, 0, 0), (CommonDefs.op_Subtraction_Vector3_Vector3(dV, bV)))
	if bV.y > dV.y then
        angle = angle * - 1
    end
    self.m_DanGongRightLine.transform.localRotation = Quaternion.Euler(Vector3(0, 0, angle))
    self.m_DanGongRightLine.width = math.floor(Vector3.Distance(dV, bV))

    local offset = Vector3.Distance(Vector3.zero, self.m_Bullet.LocalPosition)
    self.m_AimLine.topAnchor.absolute = math.floor((offset * 4 + self.m_AimLine.bottomAnchor.absolute))
    self.m_AimLineRoot.transform.localRotation = self.m_Bullet.transform.localRotation
    self.m_AimLineRoot.transform.localPosition = self.m_Bullet.LocalPosition

    self.m_GuideRoot.localRotation = self.m_Bullet.transform.localRotation
    self.m_GuideRoot.localPosition = self.m_Bullet.LocalPosition
end

function LuaDriveAwayGhostWnd:OnBulletHit(go)
    if(self.m_BulletCanHit)then
        self.m_BulletCanHit = false
        self.m_HitCount = self.m_HitCount + 1
        self.m_HitEffect.transform.localPosition = self.m_Bullet.LocalPosition
        self.m_HitEffect:SetActive(false)
        self.m_HitEffect:SetActive(true)
        --self.m_Bullet.gameObject:GetComponent(typeof(BoxCollider)).enabled = false
        g_MessageMgr:ShowMessage("Drive_Away_Ghost_Hit_One")
        self:BirdFall()
        self:RefreshShow()
    end
end

function LuaDriveAwayGhostWnd:OnBirdStop(go)
	if self.m_HitCount >= self.m_TotalCount then
        CLuaTaskMgr.CompleteDriveAwayChostTask()
        self:Close()
        CUIManager.CloseUI(CUIResources.DanGongWnd)
    else
        self:LaunchBirds()
    end
end

function LuaDriveAwayGhostWnd:BirdFall()
	self.m_BirdTemplate.NormalizedDirection = Vector2(0, - 1)
    self.m_BirdTemplate.Speed = 100
    self.m_BirdTemplate:ResetStartPos()
    self.m_BirdTemplate:playAnimation("die01")
end

function LuaDriveAwayGhostWnd:OnBulletStop(go)
	self:ResetBullet()
end

function LuaDriveAwayGhostWnd:OnDestroy()
    self:CancelTick()
end

function LuaDriveAwayGhostWnd:GetGuideGo(method)
    if method=="GetGuideButton" then
        return self.m_Bullet.gameObject
    end
end

function LuaDriveAwayGhostWnd:StartShowGuide()
    if(CLuaGuideMgr.TryTriggerZhuoGuiGuide())then
        self.m_GuideRoot.gameObject:SetActive(true)
        self:PlayGuide()
        if(self.m_GuideTick)then
            UnRegisterTick(self.m_GuideTick)
        end
        self.m_GuideTick = RegisterTick(function()
            self:PlayGuide()
        end,2000)
    end
end

function LuaDriveAwayGhostWnd:EndShowGuide()
    if(self.m_GuideTick)then
        UnRegisterTick(self.m_GuideTick)
        self.m_GuideTick = nil
    end
    self.m_GuideRoot.gameObject:SetActive(false)
    if(L10.Game.Guide.CGuideMgr.Inst:IsInPhase(71))then
        L10.Game.Guide.CGuideMgr.Inst:EndCurrentPhase()
    end
end

function LuaDriveAwayGhostWnd:PlayGuide()
    self.m_GuideFinger.localPosition = Vector3(55,-60,0)
    LuaTweenUtils.TweenAlpha(self.m_GuideFinger,0,1,0.5)
    LuaTweenUtils.TweenAlpha(self.m_GuideArrow,0,1,0.5)

    LuaTweenUtils.SetDelay(LuaTweenUtils.TweenPosition(self.m_GuideFinger,55,-240,0,0.5),0.5)

    LuaTweenUtils.SetDelay(LuaTweenUtils.TweenAlpha(self.m_GuideFinger,1,0,0.5),1.0)
    LuaTweenUtils.SetDelay(LuaTweenUtils.TweenAlpha(self.m_GuideArrow,1,0,0.5),1.0)
end

-- if L10.Game.Guide.CGuideMgr.Inst:IsInPhase(71) then
--     L10.Game.Guide.CGuideMgr.Inst:EndCurrentPhase()
-- end
