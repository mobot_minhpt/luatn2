local UILabel = import "UILabel"
local QnButton = import "L10.UI.QnButton"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CRankData = import "L10.UI.CRankData"
local CTrackMgr = import "L10.Game.CTrackMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"

LuaBusinessResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaBusinessResultWnd, "CommonLab", "CommonLab", UILabel)
RegistChildComponent(LuaBusinessResultWnd, "MoneyLab", "MoneyLab", UILabel)
RegistChildComponent(LuaBusinessResultWnd, "DayLab", "DayLab", UILabel)
RegistChildComponent(LuaBusinessResultWnd, "RankBtn", "RankBtn", QnButton)
RegistChildComponent(LuaBusinessResultWnd, "PlayAgainBtn", "PlayAgainBtn", QnButton)
RegistChildComponent(LuaBusinessResultWnd, "ConfirmBtn", "ConfirmBtn", QnButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaBusinessResultWnd, "m_TexTab")
RegistClassMember(LuaBusinessResultWnd, "m_ShareBtn")

function LuaBusinessResultWnd:Awake()
    self.m_IsStoryMode = LuaBusinessMgr.m_ResultGameMode == 5
    self.m_TexTab = {}
    for i = 1, 3 do
        self.m_TexTab[i] = self.transform:Find("Bg/wangcheng_q_"..i).gameObject
    end
    self.m_ShareBtn = self.transform:Find("Anchor/Share_Btn"):GetComponent(typeof(QnButton))
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.RankBtn.OnClick = DelegateFactory.Action_QnButton(function(button) 
        self:OnRankBtnClick()
    end)

    self.PlayAgainBtn.OnClick = DelegateFactory.Action_QnButton(function(button)
        self:OnPlayAgainBtnClick()
    end)

    self.ConfirmBtn.OnClick = DelegateFactory.Action_QnButton(function(button)
        self:OnConfirmBtnClick()
    end)

    self.m_ShareBtn.OnClick = DelegateFactory.Action_QnButton(function(button)
        self:OnShareBtnClick()
    end)

    self.RankBtn.gameObject:SetActive(not self.m_IsStoryMode)
    self.PlayAgainBtn.gameObject:SetActive(not self.m_IsStoryMode)
    self.ConfirmBtn.gameObject:SetActive(self.m_IsStoryMode)
    CUIManager.CloseUI(CLuaUIResources.BusinessMainWnd)
end

function LuaBusinessResultWnd:Init()
    self.m_ShareBtn.gameObject:SetActive(not CommonDefs.IS_VN_CLIENT)
    if self.m_IsStoryMode then
        self.DayLab.text = SafeStringFormat3(LocalString.GetString("你和王成一共做了[c][ffc800]%d[-][/c]天生意。"), LuaBusinessMgr.m_CurDay)
        self.MoneyLab.text = SafeStringFormat3(LocalString.GetString("%d天中，你们赚了[c][ffc800]%.f[-][/c]通宝！"), LuaBusinessMgr.m_CurDay, LuaBusinessMgr.m_OwnMoney)
    else
        self.DayLab.text = SafeStringFormat3(LocalString.GetString("你这段时间一共做了[c][ffc800]%d[-][/c]天生意。"), LuaBusinessMgr.m_CurDay)
        self.MoneyLab.text = SafeStringFormat3(LocalString.GetString("%d天中，你赚了[c][ffc800]%.f[-][/c]通宝！"), LuaBusinessMgr.m_CurDay, LuaBusinessMgr.m_OwnMoney)
    end
    local endingMessage, endingState = LuaBusinessMgr:GetEndingMessageAndState()
    self.CommonLab.text = SafeStringFormat3(LocalString.GetString("正可谓是[c][ffc800]【%s】[-][/c]啦！"), endingMessage)
    self:ShowTex(endingState)
end

--@region UIEvent

--@endregion UIEvent
function LuaBusinessResultWnd:ShowTex(index)
    for i = 1, 3 do
        self.m_TexTab[i]:SetActive(index == i)
    end
end

function LuaBusinessResultWnd:OnRankBtnClick()
    CRankData.Inst.InitRankId = 41000254
    if IsOpenNewRankWnd() then
	    CUIManager.ShowUI(CLuaUIResources.NewRankWnd)
	else
		CUIManager.ShowUI(CUIResources.RankWnd)
	end
end

function LuaBusinessResultWnd:OnPlayAgainBtnClick()
    CUIManager.ShowUI(CLuaUIResources.BusinessMainWnd)
    CUIManager.CloseUI(CLuaUIResources.BusinessResultWnd)
end

function LuaBusinessResultWnd:OnConfirmBtnClick()
    CUIManager.CloseUI(CLuaUIResources.BusinessResultWnd)
end

function LuaBusinessResultWnd:OnShareBtnClick()
    CUICommonDef.CaptureScreen(
        "screenshot",
        true,
        false,
        nil,
        DelegateFactory.Action_string_bytes(
            function(fullPath, jpgBytes)
                ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
            end
        ),
        false
    )
end