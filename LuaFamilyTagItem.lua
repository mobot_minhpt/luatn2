local UILabel = import "UILabel"
local CFamilyTreeMgr = import "L10.Game.CFamilyTreeMgr"
local TweenPosition = import "TweenPosition"
local Vector3 = import "UnityEngine.Vector3"

LuaFamilyTagItem=class()

RegistClassMember(LuaFamilyTagItem,"TagLable")
RegistClassMember(LuaFamilyTagItem,"DetailBtn")
RegistClassMember(LuaFamilyTagItem,"Heart")
RegistClassMember(LuaFamilyTagItem,"MyOwn")
RegistClassMember(LuaFamilyTagItem,"DelBtn")
RegistClassMember(LuaFamilyTagItem,"Tweener")
RegistClassMember(LuaFamilyTagItem,"TweenerRoot")

RegistClassMember(LuaFamilyTagItem,"TagZanLevel")       --标签等级表
RegistClassMember(LuaFamilyTagItem,"TagLevel")          --标签等级（随点赞数改变）
RegistClassMember(LuaFamilyTagItem,"TagRank")          --标签等级（随点赞数改变）
RegistClassMember(LuaFamilyTagItem,"favorNum")          --点赞数
RegistClassMember(LuaFamilyTagItem,"tagId")             --TagId
RegistClassMember(LuaFamilyTagItem,"tagName")           --tagName

function LuaFamilyTagItem:Init(parents,i,Tags,DeleteCallback)    
    self.TagRank = i
    self.tagId = luaFamilyMgr.m_TagData[i].tagId
    self.tagName = luaFamilyMgr.m_TagData[i].tagName
    self.favorNum = luaFamilyMgr.m_TagIndexData[self.tagId].favorNum
    self.TagZanLevel = FamilyTree_Setting.GetData().TagZanLevel
    self.DeleteCallback = DeleteCallback
    self:GetLevel()
    
    local go = Tags.transform:Find("Tag_"..self.TagLevel).gameObject
    local TagObj=NGUITools.AddChild(parents.gameObject,go)
    
    self.Heart = TagObj.transform:Find("TweenerRoot/heart").gameObject
    self.TagLable = TagObj.transform:Find("TweenerRoot/Bg/TagLable"):GetComponent(typeof(UILabel))
    self.DetailBtn = TagObj.transform:Find("TweenerRoot/Bg").gameObject
    -- self.favorLabel = TagObj.transform:Find("TweenerRoot/heart/Label"):GetComponent(typeof(UILabel))
    self.MyOwn = TagObj.transform:Find("TweenerRoot/MyOwn").gameObject
    self.Tweener = TagObj.transform:GetComponent(typeof(TweenPosition))
    self.TweenerRoot = TagObj.transform:Find("TweenerRoot"):GetComponent(typeof(TweenPosition))
    self:SetTweeener()
    self.MyOwn:SetActive(self.tagId == luaFamilyMgr.m_selfTagId)
    self.DelBtn = TagObj.transform:Find("TweenerRoot/DelBtn").gameObject
    self.DelBtn:SetActive(false)
    local HashCode = CommonDefs.GetHashCode(self.tagName)
    self.TagLable.text = self.tagName
    self.TagLable.color = NGUIText.ParseColor24(luaFamilyMgr.ColorTable[HashCode%#luaFamilyMgr.ColorTable + 1], 0)

    self:RefreshTag(self.tagId == luaFamilyMgr.m_favoredTagId)

    UIEventListener.Get(self.DetailBtn).onClick = DelegateFactory.VoidDelegate(function(p)
        -- FamilyTagDetailWnd
        Gac2Gas.QueryPlayerFamilyTreeTagDetail(luaFamilyMgr.TagCenterPlayerID,self.tagId)
    end)

    UIEventListener.Get(self.DelBtn).onClick = DelegateFactory.VoidDelegate(function(p)
        -- LuaAddTagWnd
        self.DeleteCallback(self.tagId)
        if CFamilyTreeMgr.Instance.CenterPlayerID == CClientMainPlayer.Inst.Id then
            if luaFamilyMgr.DelTagId ~= "" and luaFamilyMgr.DelTagId ~= luaFamilyMgr.m_selfTagId then
                CUIManager.ShowUI(CLuaUIResources.LuaDelOtherTagWnd)
                return
            end
        end
        CUIManager.ShowUI(CLuaUIResources.LuaDelTagWnd)
    end)

end

function LuaFamilyTagItem:ShowDel(DelState)
    self.DelBtn:SetActive(DelState)
end

function LuaFamilyTagItem:GetLevel()
    for i=self.TagZanLevel.Length - 1,0,-1 do
        if self.TagRank <= self.TagZanLevel[i] then
            self.TagLevel = self.TagZanLevel.Length-i 
        end
    end
end

function LuaFamilyTagItem:RefreshTag(favor)
    if favor then
        self.Heart:SetActive(true)
    else
        self.Heart:SetActive(false)
    end
end

function LuaFamilyTagItem:SetTweeener()
    local x = math.random( 30 ) - 15
    local y = math.random( 30 ) - 15
    local time = math.random(4,5)
    local time2 = math.random(4,5)
    self.Tweener.to = Vector3(x, 0)
    self.Tweener.duration = time
    self.TweenerRoot.to = Vector3(0,y)
    self.TweenerRoot.duration = time2
end
