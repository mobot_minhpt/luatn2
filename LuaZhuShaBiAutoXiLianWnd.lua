local CGetMoneyMgr = import "L10.UI.CGetMoneyMgr"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CClientHouseMgr=import "L10.Game.CClientHouseMgr"
local Time = import "UnityEngine.Time"
local CChuanjiabaoWordType=import "L10.Game.CChuanjiabaoWordType"
local CItemMgr=import "L10.Game.CItemMgr"
local CUIFx=import "L10.UI.CUIFx"
EnumZhuShaBiXiLianRequestState = {
    eNone = 0,
    eNormal = 1,
    eWaitResult=2,
    eResult = 3,
    eWaitDelta=4,
}

CLuaZhuShaBiAutoXiLianWnd = class(CLuaZhuShaBiXiLianWnd)

RegistClassMember(CLuaZhuShaBiAutoXiLianWnd,"m_RequestState")
RegistClassMember(CLuaZhuShaBiAutoXiLianWnd,"m_RequestLabel")
RegistClassMember(CLuaZhuShaBiAutoXiLianWnd,"m_SpeedLabel")
RegistClassMember(CLuaZhuShaBiAutoXiLianWnd,"m_SpeedButton")

RegistClassMember(CLuaZhuShaBiAutoXiLianWnd,"m_InitRate")--初始请求速度
RegistClassMember(CLuaZhuShaBiAutoXiLianWnd,"m_RequestRate")--请求速度
RegistClassMember(CLuaZhuShaBiAutoXiLianWnd,"m_StartWaitTime")

RegistClassMember(CLuaZhuShaBiAutoXiLianWnd,"m_FxRoot")
RegistClassMember(CLuaZhuShaBiAutoXiLianWnd,"m_Fx2")

RegistClassMember(CLuaZhuShaBiAutoXiLianWnd,"m_TargetButtonFx")

function CLuaZhuShaBiAutoXiLianWnd:Init()
    CLuaZhuShaBiXiLianWnd.Init(self)

    self.m_FxRoot = self.transform:Find("Anchor/FxRoot"):GetComponent(typeof(CUIFx))
    self.m_Fx2 = self.transform:Find("Item/Fx"):GetComponent(typeof(CUIFx))


    local targetButton = self.transform:Find("Anchor/TargetBtn").gameObject
    UIEventListener.Get(targetButton).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CLuaUIResources.ZhuShaBiXiLianConditionWnd)
        self.m_TargetButtonFx:DestroyFx()
    end)

    self.m_TargetButtonFx = targetButton.transform:Find("Fx"):GetComponent(typeof(CUIFx))
    self.m_TargetButtonFx:LoadFx(CUIFxPaths.CommonBlueLineFxPath)
    local path={Vector3(110,40,0),Vector3(110,-40,0),Vector3(-110,-40,0),Vector3(-110,40,0),Vector3(110,40,0)}
    local array = Table2Array(path, MakeArrayClass(Vector3))
    LuaTweenUtils.DOKill(self.m_TargetButtonFx.FxRoot, true)
    LuaUtils.SetLocalPosition(self.m_TargetButtonFx.FxRoot,110,40,0)
    LuaTweenUtils.DODefaultLocalPath(self.m_TargetButtonFx.FxRoot,array,2)
    

    self.m_RequestLabel = self.transform:Find("Anchor/BaptizeBtn/Label"):GetComponent(typeof(UILabel))
    self.m_RequestState = EnumZhuShaBiXiLianRequestState.eNone

    self.m_SpeedButton = self.transform:Find("Anchor/AccBtn").gameObject
    UIEventListener.Get(self.m_SpeedButton).onClick = DelegateFactory.VoidDelegate(function(go)
        local text = self.m_SpeedLabel.text    
        if text == LocalString.GetString("速度x1") then
            self.m_SpeedLabel.text = LocalString.GetString("速度x2")
            self.m_RequestRate = self.m_InitRate / 2
        elseif text == LocalString.GetString("速度x2") then
            self.m_SpeedLabel.text = LocalString.GetString("速度x4")
            self.m_RequestRate = self.m_InitRate / 4
        elseif text == LocalString.GetString("速度x4") then
            self.m_SpeedLabel.text = LocalString.GetString("速度x1")
            self.m_RequestRate = self.m_InitRate
        end

    end)
    self.m_SpeedLabel = self.m_SpeedButton.transform:Find("Label"):GetComponent(typeof(UILabel))
    self.m_InitRate=GameSetting_Client.GetData().EQUIP_AUTO_XILIAN_LIMIT_TIME
    self.m_RequestRate = self.m_InitRate

    self:StopAutoWash()
end
function CLuaZhuShaBiAutoXiLianWnd:ShowFx()
    self.m_FxRoot:DestroyFx()
    self.m_FxRoot:LoadFx("Fx/UI/Prefab/UI_zhuangbeixilian.prefab")

    LuaTweenUtils.DOKill(self.m_Fx2.FxRoot,false)
    self.m_Fx2:DestroyFx()
    self.m_Fx2:LoadFx("Fx/UI/Prefab/UI_kuang_blue02.prefab")
    local path={Vector3(64,-64,0),Vector3(-64,-64,0),Vector3(-64,64,0),Vector3(64,64,0),Vector3(64,-64,0)}
    local array = Table2Array(path, MakeArrayClass(Vector3))
    LuaUtils.SetLocalPosition(self.m_Fx2.FxRoot,64,-64,0)
    LuaTweenUtils.DODefaultLocalPath(self.m_Fx2.FxRoot,array,2)
end

--开始洗炼之前设置界面状态
function CLuaZhuShaBiAutoXiLianWnd:SetUIBeforeRequest()
    self.m_RequestLabel.text=LocalString.GetString("停止洗炼")
    CUICommonDef.SetActive(self.replaceBtn, false,true)
    self.m_Pause=false

    CUICommonDef.SetActive(self.baptizeBtn, true,true)
    UIEventListener.Get(self.baptizeBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        self:StopAutoWash()
    end)
end

function CLuaZhuShaBiAutoXiLianWnd:Replace( )
    CLuaZhuShaBiXiLianWnd.Replace(self)
end

--结束洗炼之后设置界面状态
function CLuaZhuShaBiAutoXiLianWnd:SetUIAfterRequest()
    self.m_RequestLabel.text=LocalString.GetString("开始洗炼")
    CUICommonDef.SetActive(self.replaceBtn, true,true)
    self.m_Pause=false
    UIEventListener.Get(self.baptizeBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        self.m_Pause=false
        if self:TryBaptize() then
            self:SetUIBeforeRequest()
        else
            self:StopAutoWash()
        end
    end)
end

function CLuaZhuShaBiAutoXiLianWnd:StopAutoWash()
    self:SetUIAfterRequest()
    self.m_RequestState=EnumZhuShaBiXiLianRequestState.eNone
end

function CLuaZhuShaBiAutoXiLianWnd:Update()
    if self.m_Pause then
        return
    end

    if CClientMainPlayer.Inst==nil then
        self:StopAutoWash()
        return
    end

    if self.m_RequestLabel.text==LocalString.GetString("开始洗炼") then
        self:StopAutoWash()
        return
    end

    if self.m_RequestState==EnumZhuShaBiXiLianRequestState.eNormal then
        local setting = ChuanjiabaoModel_Setting.GetData()

        local useLingYu=self.m_UseLingYuToggle.value
        if self.m_UseLingYuToggle.value then--使用灵玉
            --检查灵玉数量
            if not self.m_LingYuCost.moneyEnough then
                -- g_MessageMgr:ShowMessage("")
                CGetMoneyMgr.Inst:GetLingYu(self.m_LingYuCost.cost, setting.WashZiJingPing)
                self:StopAutoWash()
                return
            end
        else
            --检查净瓶数量
            
            local c,d = CItemMgr.Inst:CalcItemCountInBagByTemplateId(setting.WashZiJingPing)
            local count = c+d
            if self.m_IsBinded then
                local a,b = CItemMgr.Inst:CalcItemCountInBagByTemplateIdExclueExpireTime(setting.WashYuJingPing)
                count = a+b+c+d
            end
            
            if self.m_CostJingPingCount>count then
                -- g_MessageMgr:ShowMessage("")
                g_MessageMgr:ShowMessage("BAPTIZE_EQUIP_BASIC_ITEM_NOT_ENOUGH",LocalString.GetString("净瓶"))
                self:StopAutoWash()
                return
            end
        end
        if not self.m_YinLiangCost.moneyEnough then
            g_MessageMgr:ShowMessage("SILVER_NOT_ENOUGH")
            self:StopAutoWash()
            return
        end

        local processed = false
        local commonItem = CItemMgr.Inst:GetById(CLuaZhuShaBiProcessWnd.m_SelectedItemId)
        if commonItem then
            local wordInfo = nil
            if commonItem.Item.Type==EnumItemType_lua.EvaluatedZhushabi then
                if commonItem.Item.ExtraVarData and commonItem.Item.ExtraVarData.Data then
                    wordInfo = CreateFromClass(CChuanjiabaoWordType)
                    wordInfo:LoadFromString(commonItem.Item.ExtraVarData.Data)
                end
            else--传家宝
                wordInfo = commonItem.Item.ChuanjiabaoItemInfo and commonItem.Item.ChuanjiabaoItemInfo.WordInfo
            end
            if wordInfo then
                if wordInfo.LastWashResult then--有洗炼结果
                    local hasNewResult = wordInfo.LastWashResult.Words.Count
                    local preStar = wordInfo.Star
                    local preWordCount = wordInfo.Words.Count
                    if wordInfo.WashedStar and wordInfo.WashedStar>0  then
                        local preStar = wordInfo.WashedStar
                    end
                    processed=true
                    self:XiLian()
                end
            end
        end
        if not processed then
            self:StopAutoWash()
        end
    elseif self.m_RequestState==EnumZhuShaBiXiLianRequestState.eResult then
        --看看结果如何
        --继续洗 等待一点时间
        self.m_RequestState=EnumZhuShaBiXiLianRequestState.eWaitDelta
        self.m_StartWaitTime=Time.time
    elseif self.m_RequestState==EnumZhuShaBiXiLianRequestState.eWaitDelta then
        if Time.time>self.m_StartWaitTime+self.m_RequestRate then
            self.m_RequestState=EnumZhuShaBiXiLianRequestState.eNormal
        end
    end
end

function CLuaZhuShaBiAutoXiLianWnd:XiLian()
    if not CClientHouseMgr.Inst:HaveHouse() then
        g_MessageMgr:ShowMessage("PLAYER_NO_HOUSE")
        self:StopAutoWash()
        return
    end

    self.m_Fx2:DestroyFx()

    local useLingYu=self.m_UseLingYuToggle.value
    local itemInfo = CItemMgr.Inst:GetItemInfo(CLuaZhuShaBiProcessWnd.m_SelectedItemId)
    if itemInfo then
        Gac2Gas.RequestWashChuanjiabao(EnumToInt(itemInfo.place),itemInfo.pos,CLuaZhuShaBiProcessWnd.m_SelectedItemId,useLingYu)--place, pos, itemId
        self.m_RequestState=EnumZhuShaBiXiLianRequestState.eWaitResult
    else
        --判断是否在家
        if CClientHouseMgr.Inst:IsInOwnHouse() then
            Gac2Gas.RequestWashChuanjiabaoInHouse(CLuaZhuShaBiProcessWnd.m_SelectedItemId, useLingYu)
            self.m_RequestState=EnumZhuShaBiXiLianRequestState.eWaitResult
        else
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("OPERATION_REQUEST_GO_HOME"), DelegateFactory.Action(function () 
                Gac2Gas.RequestEnterHome()
                self:StopAutoWash()
            end), DelegateFactory.Action(function () 
                self:StopAutoWash()
            end), nil, nil, false)
            self:StopAutoWash()
        end
    end
end
function CLuaZhuShaBiAutoXiLianWnd:CancelXiLian()
    self:StopAutoWash()
end

function CLuaZhuShaBiAutoXiLianWnd:OnEnable( )
    CLuaZhuShaBiXiLianWnd.OnEnable(self)
    g_ScriptEvent:AddListener("Guide_ChangeView", self, "OnChangeView")

end
function CLuaZhuShaBiAutoXiLianWnd:OnDisable( )
    CLuaZhuShaBiXiLianWnd.OnDisable(self)
    g_ScriptEvent:RemoveListener("Guide_ChangeView", self, "OnChangeView")
end

function CLuaZhuShaBiAutoXiLianWnd:OnChangeView(args)
    if not CClientMainPlayer.Inst then
        self.m_Pause=true
    end
    local top=CUIManager.instance:GetTopPopUI()
    if top then
        if top.Name=="ZhuShaBiAutoXiLianWnd" then
            self.m_Pause=false
        else
            self.m_Pause=true
        end
    end
end

function CLuaZhuShaBiAutoXiLianWnd:OnSendChuanjiabaoWashResult(itemId,newStar,wordIds)
    CLuaZhuShaBiXiLianWnd.OnSendChuanjiabaoWashResult(self,itemId,newStar,wordIds)
    if CLuaZhuShaBiProcessWnd.m_SelectedItemId==itemId then

        local commonItem = CItemMgr.Inst:GetById(CLuaZhuShaBiProcessWnd.m_SelectedItemId)
        local wordInfo = nil
        if commonItem.Item.Type==EnumItemType_lua.EvaluatedZhushabi then
            if commonItem.Item.ExtraVarData and commonItem.Item.ExtraVarData.Data then
                wordInfo = CreateFromClass(CChuanjiabaoWordType)
                wordInfo:LoadFromString(commonItem.Item.ExtraVarData.Data)
            end
        else--传家宝
            wordInfo = commonItem.Item.ChuanjiabaoItemInfo and commonItem.Item.ChuanjiabaoItemInfo.WordInfo
        end
        if wordInfo and wordInfo.LastWashResult then
            if CLuaZhuShaBiXiLianMgr.CheckCondition(wordInfo.LastWashResult) then--达到目标
                self:ShowFx()
                self:StopAutoWash()
            else
                self.m_RequestState=EnumZhuShaBiXiLianRequestState.eResult
            end
        end
    end
end
-- DeepCopyAllClasses()
