
local MsgPackImpl = import "MsgPackImpl"
local JsonMapper = import "JsonMapper"
local AdvertU3d = import "NtUniSdk.Unity3d.AdvertU3d"
local AdvertConstProp = import "NtUniSdk.Unity3d.AdvertConstProp"

LuaHuiLiuMgr = {}

--@desc 同步回流礼包信息
LuaHuiLiuMgr.GiftCountDownEndTime = 0
LuaHuiLiuMgr.GiftCountDownNowTime = 0
LuaHuiLiuMgr.GiftInfos = nil
function LuaHuiLiuMgr.SyncHuiLiuGiftInfos(endTime, nowTime, giftInfos)
	LuaHuiLiuMgr.GiftCountDownEndTime = endTime
	LuaHuiLiuMgr.GiftCountDownNowTime = nowTime
	LuaHuiLiuMgr.GiftInfos = {}

	local dict = MsgPackImpl.unpack(giftInfos)
	if not dict then return end

	CommonDefs.DictIterate(dict, DelegateFactory.Action_object_object(function(key, val)
		local itemId =  tonumber(key)
		local canBuy = val
		LuaHuiLiuMgr.GiftInfos[itemId] = canBuy
	end))

	local isOpen = nowTime <= endTime
	local timeRemain = endTime - nowTime
	CLuaActivityAlert.s_HuiLiuGiftStatus = isOpen
	g_ScriptEvent:BroadcastInLua("SyncHuiLiuGiftInfos", isOpen, timeRemain)
end

function LuaHuiLiuMgr.OnMainPlayerCreated()
	g_ScriptEvent:AddListener("NotifyChargeDone", LuaHuiLiuMgr, "NotifyChargeDone")
end

function LuaHuiLiuMgr.OnMainPlayerDestroyed()
	g_ScriptEvent:RemoveListener("NotifyChargeDone", LuaHuiLiuMgr, "NotifyChargeDone")
end

function LuaHuiLiuMgr.NotifyChargeDone(self, args)
	if not LuaHuiLiuMgr.GiftInfos then return end

	if not args or args.Length <= 1 then return end

	for k, v in pairs(LuaHuiLiuMgr.GiftInfos) do
		local mall = Mall_LingYuMallLimit.GetData(k)
		if mall.RmbPID == args[0] then
			LuaHuiLiuMgr.GiftInfos[k] = false
		end
	end

	g_ScriptEvent:BroadcastInLua("HuiLiuGiftNotifyChargeDone")
end

--@region 回流基金

function LuaHuiLiuMgr.OnPlayerLogin()
	LuaHuiLiuMgr.Reset()
end

function LuaHuiLiuMgr.Reset()
	LuaHuiLiuMgr.FundExpiredTime = 0
	LuaHuiLiuMgr.FundNowTime = 0
	LuaHuiLiuMgr.FundPurchased = false
	LuaHuiLiuMgr.FundProgressInfo = nil
	LuaHuiLiuMgr.FundAllAwarded = false
	LuaHuiLiuMgr.FundShowAlert = false

	LuaHuiLiuMgr.GiftCountDownEndTime = 0
end

--回流基金过期时间
LuaHuiLiuMgr.FundExpiredTime = 0
LuaHuiLiuMgr.FundNowTime = 0
LuaHuiLiuMgr.FundPurchased = false
LuaHuiLiuMgr.FundProgressInfo = nil -- 只有有进度的回流任务信息会存
LuaHuiLiuMgr.FundAllAwarded = false -- 奖励是否全部领取
LuaHuiLiuMgr.FundShowAlert = false	-- 回流基金是否需要显示小红点
--@desc 更新回流基金的整体信息
function LuaHuiLiuMgr.SyncHuiLiuFundInfo(expiredTime, nowTime, purchased, progressInfo, allAwarded, showAlert)
	LuaHuiLiuMgr.FundExpiredTime = expiredTime
	LuaHuiLiuMgr.FundNowTime = nowTime
	LuaHuiLiuMgr.FundPurchased = purchased
	LuaHuiLiuMgr.FundProgressInfo = progressInfo
	LuaHuiLiuMgr.FundAllAwarded = allAwarded
	LuaHuiLiuMgr.FundShowAlert = showAlert

	g_ScriptEvent:BroadcastInLua("SyncHuiLiuFundInfo")
end

--@desc 回流基金购买成功
function LuaHuiLiuMgr.HuiLiuFundBought(expiredTime)
	LuaHuiLiuMgr.FundExpiredTime = expiredTime
	LuaHuiLiuMgr.FundPurchased = true

	g_ScriptEvent:BroadcastInLua("SyncHuiLiuFundInfo")
end

--@desc 更新回流基金的任务进度
function LuaHuiLiuMgr.UpdateHuiLiuFundTaskProgress(expiredTime, id, progress, finished)
	LuaHuiLiuMgr.FundExpiredTime = expiredTime

	LuaHuiLiuMgr.FundProgressInfo[id] = {
		progress = progress,
		isFinished = finished,
		isAwarded = false,
	}

	g_ScriptEvent:BroadcastInLua("UpdateHuiLiuFundTaskProgress")
end

--@desc 回流基金是否有可以领取的奖励
function LuaHuiLiuMgr.HasFundAwardToTake()
	if not LuaHuiLiuMgr.FundPurchased or not LuaHuiLiuMgr.FundProgressInfo or LuaHuiLiuMgr.FundAllAwarded then return false end

	local hasAward = false
	for key, value in pairs(LuaHuiLiuMgr.FundProgressInfo) do
		if value.isFinished and not value.isAwarded then
			hasAward = true
		end
	end
	return hasAward
end

--@desc 领取回流基金奖励成功
function LuaHuiLiuMgr.GetHuiLiuFundTaskAwardSuccess(expiredTime, id)
	LuaHuiLiuMgr.FundExpiredTime = expiredTime

	local info = LuaHuiLiuMgr.FundProgressInfo[id]
	if info then
		info.isAwarded = true
		g_ScriptEvent:BroadcastInLua("GetHuiLiuFundTaskAwardSuccess")
	end
	SAFE_CALL(function()
		local jsonStr = SafeStringFormat3("{\"expiredtime\":\"%s\",\"id\":\"%s\"}", expiredTime, id)
		local param = JsonMapper.ToObject(jsonStr)
		AdvertU3d.trackEvent(AdvertConstProp.AD_SDK_L10_HUILIUFUND_AWARD_SUCCESS, param);
	end)
end
--@endregion


--@region Gac2Gas

function Gas2Gac.SyncHuiGuiJieBanData(huiGuiJieBanDataUD)
	if CClientMainPlayer.Inst then
		CClientMainPlayer.Inst.PlayProp.HuiGuiJieBanData:LoadFromString(huiGuiJieBanDataUD)
	end
end

function Gas2Gac.SendLiuShiFriends(playerIdTblUD)
	LuaHuiLiuMgr.PlayerInfoTable = {}
	LuaHuiLiuMgr.PlayerType = 1
	local playerData = MsgPackImpl.unpack(playerIdTblUD)
	for i = 0, playerData.Count - 1, 6 do
		local playerId = playerData[i]
		local playerName = playerData[i + 1]
		local playerClass = playerData[i + 2]
		local playerGrade = playerData[i + 3]
		local playerXianGrade = playerData[i + 4]
		local playerGender = playerData[i + 5]
		table.insert(LuaHuiLiuMgr.PlayerInfoTable, {playerId,playerName,playerClass,playerGrade,playerXianGrade,playerGender})
	end

	CUIManager.ShowUI(CLuaUIResources.HuiLiuMainWnd)
end

function Gas2Gac.InviteFriendSuccess(playerId)
	g_ScriptEvent:BroadcastInLua("UpdateHuiLiuInfo")
end

function Gas2Gac.SendPlayerInviters(playerIdTblUD)
	LuaHuiLiuMgr.PlayerInfoTable = {}
	LuaHuiLiuMgr.PlayerType = 2
	local playerData = MsgPackImpl.unpack(playerIdTblUD)
	for i = 0, playerData.Count - 1, 6 do
		local playerId = playerData[i]
		local playerName = playerData[i + 1]
		local playerClass = playerData[i + 2]
		local playerGrade = playerData[i + 3]
		local playerXianGrade = playerData[i + 4]
		local playerGender = playerData[i + 5]
		table.insert(LuaHuiLiuMgr.PlayerInfoTable, {playerId,playerName,playerClass,playerGrade,playerXianGrade,playerGender})
	end

	CUIManager.ShowUI(CLuaUIResources.HuiLiuMainWnd)
end

function Gas2Gac.NotifyInviterSuccess(playerId)
	g_ScriptEvent:BroadcastInLua("UpdateHuiLiuInfo")
end

function Gas2Gac.RemoveJieBanPlayerSuccess()
	g_ScriptEvent:BroadcastInLua("RemoveHuiLiuPlayer")
end

function Gas2Gac.OpenHuiGuiJieBanWnd()
	CUIManager.ShowUI(CLuaUIResources.HuiLiuTaskWnd)
end

function Gas2Gac.SyncHuiLiuLimitRmbItemInfo(endTime, nowTime, itemNuyInfo)
	LuaHuiLiuMgr.SyncHuiLiuGiftInfos(endTime, nowTime, itemNuyInfo)
end

--@desc 同步回流基金整体信息
function Gas2Gac.SyncHuiLiuFundInfo(expiredTime, nowTime, purchased, allAwarded, finished, awarded, progressListUD, showAlert)
	local progressList = MsgPackImpl.unpack(progressListUD)
	if not progressList then return end
	local progressInfo = {}
	local tempInfo = {}

	-- NOTE: 此处的progressList中只包含有进度的任务，已经完成的任务以及尚未开始的任务并不包含在此
	for i = 0, progressList.Count - 1, 2 do
		local id = progressList[i]
		local progress = progressList[i+1]
		tempInfo[id] = progress
	end

	Huiliu_FundTask.ForeachKey(function (key)
		local isFinished = bit.band(finished, bit.lshift(1, key - 1)) > 0
		local isAwarded = bit.band(awarded, bit.lshift(1, key - 1)) > 0
		local process = tempInfo[key] or 0
		progressInfo[key] = {
			progress = process,
			isFinished = isFinished,
			isAwarded = isAwarded,
		}
	end)

	LuaHuiLiuMgr.SyncHuiLiuFundInfo(expiredTime, nowTime, purchased, progressInfo, allAwarded, showAlert)
end

--@desc 回流基金购买成功回调
function Gas2Gac.NotifyChargeHuiLiuFundDone(expiredTime)
	LuaHuiLiuMgr.HuiLiuFundBought(expiredTime)
end

--@desc 更新回流基金的任务进度
function Gas2Gac.UpdateHuiLiuFundTaskProgress(expiredTime, id, progress, finished)
	LuaHuiLiuMgr.UpdateHuiLiuFundTaskProgress(expiredTime, id, progress, finished)
end

--@desc 领取回流基金奖励
function Gas2Gac.GetHuiLiuFundTaskAwardSuccess(expiredTime, id, allAwarded)
	LuaHuiLiuMgr.GetHuiLiuFundTaskAwardSuccess(expiredTime, id)
end

--@endregion


-- 召回

local CLoginMgr = import "L10.Game.CLoginMgr"
LuaHuiLiuMgr.NewHuiLiuOpen = true

function LuaHuiLiuMgr.CheckNewHuiLiuOpen()
	if LuaHuiLiuMgr.NewHuiLiuOpen then
		local config = PlayConfig_ServerIdEnabled.GetData(CLoginMgr.Inst.ServerGroupId)
		if config and config.ZhaoHuiNew == 1 then
			return true
		else
			return false
		end
	else
		return false
	end
end

function Gas2Gac.SyncZhaoHuiFriendList(friendIdList)
	LuaHuiLiuMgr.ZhaoHuiFriendList = {}
	local list = MsgPackImpl.unpack(friendIdList)
	for i = 0, list.Count - 1 do
		local friendPlayerId = tonumber(list[i])
		table.insert(LuaHuiLiuMgr.ZhaoHuiFriendList,friendPlayerId)
	end

	g_ScriptEvent:BroadcastInLua("UpdateZhaoHuiFriendInfo")
end

function Gas2Gac.SyncAlreadyZhaoHuiFriendList(friendList)
	LuaHuiLiuMgr.AlreadyZhaoHuiFriendList = {}
	local list = MsgPackImpl.unpack(friendList)
	for i = 0, list.Count - 1, 3 do
		local friendPlayerId = list[i]
		local isBind = list[i+1]
		local taskExpireTime = list[i+2]
		table.insert(LuaHuiLiuMgr.AlreadyZhaoHuiFriendList,{friendPlayerId,isBind,taskExpireTime})
	end
	g_ScriptEvent:BroadcastInLua("UpdateAlreadyZhaoHuiFriendInfo")
end

function Gas2Gac.SendZhaoHuiInviteFriendSucc(friendPlayerId)
end

function Gas2Gac.SyncZhaoHuiTaskInfo(friendPlayerId, taskExpireTime, taskInfo)
	LuaHuiLiuMgr.AlreadyZhaoHuiTaskInfo = {}
	LuaHuiLiuMgr.AlreadyZhaoHuiTaskExpireTime = taskExpireTime
	LuaHuiLiuMgr.AlreadyZhaoHuiFriendId = friendPlayerId
	local list = MsgPackImpl.unpack(taskInfo)
	for i = 0, list.Count - 1, 4 do
		local taskId = list[i]
		local progress = list[i+1]
		local stage = list[i+2]
		local isRewarded = list[i+3]
		table.insert(LuaHuiLiuMgr.AlreadyZhaoHuiTaskInfo,{taskId,progress,isRewarded,stage})
	end
	g_ScriptEvent:BroadcastInLua("UpdateZhaoHuiTaskInfo")
end

function Gas2Gac.SendGetZhaoHuiTaskRewardSucc(friendPlayerId, taskId, progress, stage, isRewarded)
	g_ScriptEvent:BroadcastInLua("UpdateZhaoHuiTaskReward")
	Gac2Gas.QueryZhaoHuiTaskInfo(friendPlayerId)
end

function Gas2Gac.SyncHuiGuiInviterInfo(expireTime, inviterInfo, notFriendInviterInfo)
	LuaHuiLiuMgr.InviterInfo = {}
	LuaHuiLiuMgr.InviterInfo.inviter = {}
	LuaHuiLiuMgr.InviterInfo.inviter2 = {}
	LuaHuiLiuMgr.InviterInfo.expireTime = expireTime

	local showAlert = false
	local list = MsgPackImpl.unpack(inviterInfo)
	for i = 0, list.Count - 1 do
		local playerId = list[i]
		showAlert = true
		table.insert(LuaHuiLiuMgr.InviterInfo.inviter,playerId)
	end

	local list2 = MsgPackImpl.unpack(notFriendInviterInfo)
	for i = 0, list2.Count - 1, 6 do
		local playerId = list2[i]
		local name = list2[i+1]
		local class = list2[i+2]
		local gender = list2[i+3]
		local level = list2[i+4]
		local xianshen = list2[i+5]
		showAlert = true
		table.insert(LuaHuiLiuMgr.InviterInfo.inviter2,{playerId, name, class, gender, level, xianshen})
	end

	g_ScriptEvent:BroadcastInLua("UpdateWelfareNewHuiLiu",showAlert)
end

function Gas2Gac.SyncHuiGuiBindedTaskInfo(inviterPlayerId, huiguiScore,taskExpireTime, taskInfo)
	LuaHuiLiuMgr.InviterInfo = {}
	LuaHuiLiuMgr.InviterInfo.task = {}
	LuaHuiLiuMgr.InviterInfo.taskExpireTime= taskExpireTime
	LuaHuiLiuMgr.InviterInfo.inviterId = inviterPlayerId
	LuaHuiLiuMgr.InviterInfo.huiguiScore = huiguiScore

	local showAlert = false
	local finishTask = {}
	local doingTask = {}
	local rewardedTask = {}
	local list = MsgPackImpl.unpack(taskInfo)
	for i = 0, list.Count - 1, 4 do
		local taskId = list[i]
		local progress = list[i+1]
		local stage = list[i+2]
		local isRewarded = list[i+3]
		if isRewarded then
			table.insert(rewardedTask,{taskId, progress, isRewarded, stage})
		else
			local taskInfo = HuiLiuNew_HuiGuiTask.GetData(taskId)
			if taskInfo then
				local targetTable = g_LuaUtil:StrSplit(taskInfo.Target,',')
				local targetNum = tonumber(targetTable[stage])

				local percent = progress/targetNum
				if percent >= 1 then
					showAlert = true
					table.insert(finishTask,{taskId, progress, isRewarded, stage})
				else
					table.insert(doingTask,{taskId, progress, isRewarded, stage})
				end
			end
		end
	end

	table.sort(finishTask,function(a,b)
		return a[1] < b[1]
	end)
	table.sort(doingTask,function(a,b)
		return a[1] < b[1]
	end)
	table.sort(rewardedTask,function(a,b)
		return a[1] < b[1]
	end)
	local totalTable = {finishTask,doingTask,rewardedTask}
	for i,v in ipairs(totalTable) do
		for j,k in ipairs(v) do
			table.insert(LuaHuiLiuMgr.InviterInfo.task,k)
		end
	end
	g_ScriptEvent:BroadcastInLua("UpdateWelfareNewHuiLiu",showAlert)
end

function Gas2Gac.SendHuiGuiInviteBindSucc(friendPlayerId)
	Gac2Gas.QueryHuiGuiInviterInfo()
end

function Gas2Gac.SendGetHuiGuiTaskRewardSucc(friendPlayerId, taskId, progress, stage, isRewarded)
	Gac2Gas.QueryHuiGuiInviterInfo()
end

function Gas2Gac.ShowNewZhaoHuiBtn()
	g_ScriptEvent:BroadcastInLua("UpdateHuiLiuBtn")
end

function Gas2Gac.HuiLiuShowConfirmBindWithInvitee(friendId, accountName, characterName)
	local text = g_MessageMgr:FormatMessage("Friend_Huiliu_Confirm", characterName)
	MessageWndManager.ShowDefaultConfirm(text, 0, DelegateFactory.Action(function()
		Gac2Gas.RequestAcceptBindWithInvitee(friendId, accountName)
	end), nil, LocalString.GetString("绑定"), LocalString.GetString("取消"))
end
