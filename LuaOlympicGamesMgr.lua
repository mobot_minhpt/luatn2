local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CScene = import "L10.Game.CScene"
local CUIFxPaths = import "L10.UI.CUIFxPaths"

LuaOlympicGamesMgr = {}
LuaOlympicGamesMgr.m_IsPlayStart = false
LuaOlympicGamesMgr.m_IsBossAlive = false

function LuaOlympicGamesMgr:IsInPlay()
    return self.m_IsPlayStart
end

function LuaOlympicGamesMgr:SyncOlympicBossStatus(isPlayStart, isBossAlive)
    self.m_IsPlayStart = isPlayStart
    self.m_IsBossAlive = isBossAlive
    g_ScriptEvent:BroadcastInLua("SyncOlympicBossStatus")
end

function LuaOlympicGamesMgr:SyncOlympicBossHp(bossEngineId, bossHp)
    if CUIManager.IsLoaded(CLuaUIResources.OlympicGamePlayWnd) then
        g_ScriptEvent:BroadcastInLua("SyncOlympicBossHp", bossEngineId, bossHp)
    end
end

function LuaOlympicGamesMgr:SyncOlympicBossInfo(currentBossHp, mapNpcCountInfo)
    local list = MsgPackImpl.unpack(mapNpcCountInfo)
    local mapId2CountDict = {}
    for i = 0, list.Count - 1, 2 do
        local mapId = list[i]
        local count = list[i+1]
        mapId2CountDict[mapId] = count
    end
    if CUIManager.IsLoaded(CLuaUIResources.OlympicGamePlayWnd) then
        g_ScriptEvent:BroadcastInLua("SyncOlympicBossInfo", currentBossHp, mapId2CountDict)
    else
        CUIManager.ShowUI(CLuaUIResources.OlympicGamePlayWnd)
    end
end

function LuaOlympicGamesMgr:OnMainPlayerDestroyed()
    self.m_IsPlayStart = false
    self.m_IsBossAlive = false
end


-- 奥运竞猜界面
LuaOlympicGamesMgr.id = nil
LuaOlympicGamesMgr.myChoice = nil
LuaOlympicGamesMgr.betCount = nil
LuaOlympicGamesMgr.totalBet = nil
LuaOlympicGamesMgr.gambleData = nil

-- 请求打开奥运竞猜界面
function LuaOlympicGamesMgr:RequsetOlymmpicGambleWnd()
    Gac2Gas.RequestOlympicGambleData()
end

function LuaOlympicGamesMgr:SyncOlympicGambleData(id, myChoice, betCount, totalBet, gambleData)
    LuaOlympicGamesMgr.id = id
    LuaOlympicGamesMgr.myChoice = myChoice
    LuaOlympicGamesMgr.betCount = betCount
    LuaOlympicGamesMgr.totalBet = totalBet
    LuaOlympicGamesMgr.gambleData = gambleData
    g_ScriptEvent:BroadcastInLua("SendOlympicGambleData2021", id, myChoice, betCount, totalBet, gambleData)

    if not CUIManager.IsLoaded(CLuaUIResources.OlympicGamesGambleWnd) then
        CUIManager.ShowUI(CLuaUIResources.OlympicGamesGambleWnd)
    end
end

--冬奥会
function LuaOlympicGamesMgr:ShowCurlingGameTaskView()
    local arr = {LocalString.GetString("暂无"),LocalString.GetString("暂无"),LocalString.GetString("暂无")}
    for i = 1, 3 do
        if self.m_CurlingExpiredTimeList[i] and EnumCurlingState.Idle ~= self.m_CurlingGameState then 
            local now = CServerTimeMgr.Inst.timeStamp
            local t = self.m_CurlingExpiredTimeList[i] - now
            arr[i] =  SafeStringFormat3(LocalString.GetString("%02d:%02d"),math.floor(t % 3600 / 60),t % 60)
        end
        if self.m_CurlingDistanceList[i] then 
            if self.m_CurlingDistanceList[i] >= 0 then
                arr[i] = SafeStringFormat3(LocalString.GetString("距终点%.1f米"),self.m_CurlingDistanceList[i] / 64) 
            else
                arr[i] = LocalString.GetString("无效")
            end
        else
            if LuaOlympicGamesMgr.m_CurlingGameRound > i then
                arr[i] = LocalString.GetString("无效")
            end
        end
    end
    g_ScriptEvent:BroadcastInLua("OnCommonCountDownViewUpdate",
        g_MessageMgr:FormatMessage("OlympicWinterGames_CurlingGameTaskView", arr[1], arr[2], arr[3]),
        true, self.m_CurlingGameState ~= EnumCurlingState.Idle, nil, LocalString.GetString("赛况"),nil,function(go)
            CUIManager.ShowUI(CLuaUIResources.CurlingOlympicWinterGamesRankWnd)
        end)

    if CUIManager.IsLoaded(CLuaUIResources.CurlingOlympicWinterGamesPlayWnd) and (self.m_CurlingGameState == EnumCurlingState.WaitThrow or self.m_CurlingGameState == EnumCurlingState.WaitRoundEnd) and self.m_CurlingDistanceList[self.m_CurlingGameRound] then
        g_ScriptEvent:BroadcastInLua("CurlingOlympicGamesThrowBall",self.m_CurlingDistanceList[self.m_CurlingGameRound])
    end
end

function LuaOlympicGamesMgr:ShowCurlingGameRule()
    local imagePaths = {
                "UI/Texture/NonTransparent/Material/CurlingOlympicWinterGames_yindao_1.mat",
                "UI/Texture/NonTransparent/Material/CurlingOlympicWinterGames_yindao_2.mat",
                "UI/Texture/NonTransparent/Material/CurlingOlympicWinterGames_yindao_3.mat",
                "UI/Texture/NonTransparent/Material/CurlingOlympicWinterGames_yindao_4.mat",
                "UI/Texture/NonTransparent/Material/CurlingOlympicWinterGames_yindao_5.mat",
            }
    local msgs = { "","","","",""}
    LuaImageRuleMgr:ShowCommonImageRuleWnd(imagePaths, msgs)
end

LuaOlympicGamesMgr.m_CurlingExpiredTimeList = {}
LuaOlympicGamesMgr.m_CurlingGameState = 0
LuaOlympicGamesMgr.m_CurlingGameRound = 0
function LuaOlympicGamesMgr:Curling_SyncPlayState(playState, round, expiredTime)
    self.m_CurlingExpiredTimeList = {}
    if EnumCurlingState.WaitThrow == playState then
        CUIManager.CloseUI(CLuaUIResources.CurlingOlympicWinterGamesRankWnd)
        local arr = { LocalString.GetString("一") ,LocalString.GetString("二"),LocalString.GetString("三")}
        if arr[round] then
            g_MessageMgr:ShowMessage("CurlingOlympicWinterGames_RoundStart",arr[round])
        end
        self.m_CurlingExpiredTimeList[round] = expiredTime
        EventManager.BroadcastInternalForLua(EnumEventType.OnLoadCommonUIFx, {CUIFxPaths.HuiHeStartFxPath})
    elseif EnumCurlingState.Idle == playState then
        self:ShowCurlingGameRule()
    elseif EnumCurlingState.RoundEnd == playState then
        if self.m_CurlingGameState ~= playState and CUIManager.IsLoaded(CLuaUIResources.CurlingOlympicWinterGamesPlayWnd) then
            if self.m_CurlingDistanceList[round] then
                g_ScriptEvent:BroadcastInLua("CurlingOlympicGamesRoundStop",self.m_CurlingDistanceList[round])
            end
            EventManager.BroadcastInternalForLua(EnumEventType.OnLoadCommonUIFx, {CUIFxPaths.HuiHeEndFxPath})
            if round ~= 3 then
                RegisterTickOnce(function ()
                    if CUIManager.IsLoaded(CLuaUIResources.CurlingOlympicWinterGamesPlayWnd) then
                        CUIManager.ShowUI(CLuaUIResources.CurlingOlympicWinterGamesRankWnd)
                    end
                end,2000)
            end
        end
    end
    self.m_CurlingGameState = playState
    self.m_CurlingGameRound = round
    self:ShowCurlingGameTaskView()
    if not CUIManager.IsLoaded(CLuaUIResources.CurlingOlympicWinterGamesPlayWnd) then
        CUIManager.ShowUI(CLuaUIResources.CurlingOlympicWinterGamesPlayWnd)
    end
end

LuaOlympicGamesMgr.m_CurlingDistanceList = {}
function LuaOlympicGamesMgr:Curling_SyncPlayerInfo(playerInfo_U)
    local dict = MsgPackImpl.unpack(playerInfo_U)
    CommonDefs.DictIterate(dict, DelegateFactory.Action_object_object(function(key,val)
        if key == "Rounds" then
            self.m_CurlingDistanceList = {}
            for i = 1, math.min(3,val.Count) do
                CommonDefs.DictIterate(val[i - 1], DelegateFactory.Action_object_object(function(_key,_val)
                    if _key == "Dist" then
                        self.m_CurlingDistanceList[i] = _val
                    end
                end))
            end
        end
	end))
    self:ShowCurlingGameTaskView()
end

LuaOlympicGamesMgr.m_RankResult = {}
function LuaOlympicGamesMgr:Curling_QueryRankResult(rank_U, context)
    local list = MsgPackImpl.unpack(rank_U)
    local result = {}
    for i = 0, list.Count - 1 do
        local t = {}
        CommonDefs.DictIterate(list[i], DelegateFactory.Action_object_object(function(key,val)
            --print(context,i,key,val)
            t[key] = val --Id, TotalScore,Name,Rounds
            if key == "Rounds" then
                t[key] = {}
                for j = 0,val.Count - 1 do
                    local t2 = {}
                    CommonDefs.DictIterate(val[j], DelegateFactory.Action_object_object(function(key2,val2)
                        --print(i,j,key2,val2)
                        t2[key2] = val2 --Dist,Score,ThrowBallId
                    end))
                    table.insert(t[key],t2)
                end
            end
        end))
        table.insert(result, t)
    end
    self.m_RankResult = result
    if context == "Reward" then       
        CUIManager.ShowUI(CLuaUIResources.CurlingOlympicWinterGamesPlayResultWnd)
    else
        g_ScriptEvent:BroadcastInLua("OnCurlingQueryRankResult")
    end
end

function LuaOlympicGamesMgr:IsInOlympicWinterGamesSnowBattlePlay()
    if not CScene.MainScene then return false end
    local gamePlayId = CScene.MainScene.GamePlayDesignId
    return gamePlayId == WinterOlympic_SnowballFightSetting.GetData().PlayDesignId
end

LuaOlympicGamesMgr.m_SnowballFightPlayState = 0
LuaOlympicGamesMgr.m_SnowballFightPlayProgress = nil
function LuaOlympicGamesMgr:SnowballFight_SyncPlayState(playState, expiredTime, forceInfos_U)
    self.m_SnowballFightPlayState = playState
    local list = MsgPackImpl.unpack(forceInfos_U)
    local arr = {0,0}
    for i = 1,2 do
        CommonDefs.DictIterate(list[i - 1], DelegateFactory.Action_object_object(function(key,val)
            if key == "Progress" then
                arr[i] = val
            end
        end))
    end
    self.m_SnowballFightPlayProgress = arr
    g_ScriptEvent:BroadcastInLua("OnSnowballFightSyncPlayState",arr[1],arr[2])
    g_ScriptEvent:BroadcastInLua("OnSnowballFightSyncPlayerInfo")
end

LuaOlympicGamesMgr.m_SnowballFightFragmentNum = 0
LuaOlympicGamesMgr.m_SnowballFightBulletNum = 0
LuaOlympicGamesMgr.m_SnowballFightSubmitNum = 0
LuaOlympicGamesMgr.m_SnowballLeftTimes = 0
LuaOlympicGamesMgr.m_SnowballBulletId = 0
function LuaOlympicGamesMgr:SnowballFight_SyncPlayerInfo(playerInfo_U)
    local dict = MsgPackImpl.unpack(playerInfo_U)
    self.m_SnowballFightFragmentNum, self.m_SnowballFightBulletNum,self.m_SnowballFightSubmitNum,self.m_SnowballLeftTimes,self.m_SnowballBulletId = dict["FragmentNum"],dict["BulletNum"],dict["SubmitNum"],dict["LeftTimes"],dict["BulletId"]
    self:ShowSnowballFightTaskView()
    g_ScriptEvent:BroadcastInLua("OnSnowballFightSyncPlayerInfo")
end

LuaOlympicGamesMgr.m_SnowballFightResultScore = 0
LuaOlympicGamesMgr.m_SnowballFightPlayResult = 0
LuaOlympicGamesMgr.m_SnowballFightRewardItemId = 0
function LuaOlympicGamesMgr:SnowballFight_RewardResult(score, playResult, rewardItemId)
    self.m_SnowballFightResultScore = score
    self.m_SnowballFightPlayResult = playResult
    self.m_SnowballFightRewardItemId = rewardItemId
    g_ScriptEvent:BroadcastInLua("OnSnowballFightRewardResult")
    EventManager.BroadcastInternalForLua(EnumEventType.OnLoadCommonUIFx, {
        playResult == EnumSnowballFightPlayResult.Tie and CUIFxPaths.PingjuFxPath or (
            playResult == EnumSnowballFightPlayResult.Win and CUIFxPaths.ShengliFxPath or CUIFxPaths.ShibaiFxPath
        )})
    RegisterTickOnce(function ()
        CUIManager.ShowUI(CLuaUIResources.OlympicWinterGamesSnowBattleResultWnd)
    end,3000)
end

function LuaOlympicGamesMgr:ShowSnowballFightTaskView()
    g_ScriptEvent:BroadcastInLua("OnCommonCountDownViewUpdate",
        g_MessageMgr:FormatMessage("OlympicWinterGames_SnowballFightTaskView",self.m_SnowballFightSubmitNum, self.m_SnowballFightFragmentNum,"%s"),
        true, true, nil, LocalString.GetString("规则"),nil,function(go)
            g_MessageMgr:ShowMessage("OlympicWinterGamesSnowballFightReadMe")
        end)
end
