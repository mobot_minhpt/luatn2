local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local QnRadioBox = import "L10.UI.QnRadioBox"
local CFightingSpiritMgr = import "L10.Game.CFightingSpiritMgr"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local EnumCommonForce = import "L10.UI.EnumCommonForce"
local CFightingSpiritLiveWatchWndMgr = import "L10.UI.CFightingSpiritLiveWatchWndMgr"

LuaFightingSpiritCheerWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaFightingSpiritCheerWnd, "ReadMeLabel", "ReadMeLabel", UILabel)
RegistChildComponent(LuaFightingSpiritCheerWnd, "Button", "Button", GameObject)
RegistChildComponent(LuaFightingSpiritCheerWnd, "QnRadioBox", "QnRadioBox", QnRadioBox)

--@endregion RegistChildComponent end
RegistClassMember(LuaFightingSpiritCheerWnd,"m_SelectIndex")

function LuaFightingSpiritCheerWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.Button.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButtonClick()
	end)


    --@endregion EventBind end
end

function LuaFightingSpiritCheerWnd:Init()
	self.ReadMeLabel.text = g_MessageMgr:FormatMessage("FightingSpiritCheerWnd_ReadMe")
	self.QnRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn, index)
		self:OnSelect(btn,index)
	end)
	local info1=CFightingSpiritLiveWatchWndMgr.LeftForce2ServerName
    local info2=CFightingSpiritLiveWatchWndMgr.RightForce2ServerName
    local leftInfo=info1
    local rightInfo=info2
    if CQuanMinPKMgr.Inst.IsInQuanMinPKWatch or CLuaStarBiwuMgr:IsInStarBiwuWatch() or CFightingSpiritMgr.Instance:IsDouhunFighting() then
        if info1.Force~= EnumCommonForce.eAttack then
            leftInfo=info2
            rightInfo=info1
        end
    end

	local textArray = {leftInfo.ServerName,rightInfo.ServerName, LocalString.GetString("同时为双方助威")}
	for i = 0, self.QnRadioBox.m_RadioButtons.Length - 1 do
		local btn = self.QnRadioBox.m_RadioButtons[i]
		btn.transform:Find("Cur").gameObject:SetActive(CLuaFightingSpiritMgr.m_CheerIndex == i)
		btn.Text = textArray[i + 1]
	end
	self.QnRadioBox:ChangeTo(CLuaFightingSpiritMgr.m_CheerIndex < 0 and 2 or CLuaFightingSpiritMgr.m_CheerIndex, false)
end

--@region UIEvent

function LuaFightingSpiritCheerWnd:OnButtonClick()
	-- EnumCommonForce = {
	-- 	eDefend = 0,
	-- 	eAttack =1,
	-- 	eNeutral = 2,
	-- }
	CLuaFightingSpiritMgr.m_CheerIndex = self.m_SelectIndex
	g_ScriptEvent:BroadcastInLua("OnStarBiwuRequestZhuWei")
	CUIManager.CloseUI(CLuaUIResources.FightingSpiritCheerWnd)
	local arr = {LocalString.GetString("蓝方"),LocalString.GetString("红方"),LocalString.GetString("中立")}
	g_MessageMgr:ShowMessage("FightingSpiritCheerWnd_OnSelect",arr[CLuaFightingSpiritMgr.m_CheerIndex + 1])
end

--@endregion UIEvent

function LuaFightingSpiritCheerWnd:OnSelect(btn,index)
	self.m_SelectIndex = index
	self:OnButtonClick()
end