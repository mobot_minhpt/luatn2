
local DefaultTableViewDataSource    = import "L10.UI.DefaultTableViewDataSource"
local QnTableView                   = import "L10.UI.QnTableView"
local CWordFilterMgr                = import "L10.Game.CWordFilterMgr"
local CUICommonDef                  = import "L10.UI.CUICommonDef"
local YuanDan2020_Setting           = import "L10.Game.YuanDan2020_Setting"

LuaGuildVoteEditWnd = class()

--------RegistChildComponent-------
RegistChildComponent(LuaGuildVoteEditWnd,           "GroupBtn",         GameObject)
RegistChildComponent(LuaGuildVoteEditWnd,           "Arrow",            GameObject)
RegistChildComponent(LuaGuildVoteEditWnd,           "InputContent",     UIInput)
RegistChildComponent(LuaGuildVoteEditWnd,           "InputTitle",       UIInput)
RegistChildComponent(LuaGuildVoteEditWnd,           "OptionMenu",       GameObject)
RegistChildComponent(LuaGuildVoteEditWnd,           "TableView",        QnTableView)
RegistChildComponent(LuaGuildVoteEditWnd,           "VoteBtn",          GameObject)

---------RegistClassMember-------
RegistClassMember(LuaGuildVoteEditWnd,              "m_ContentMinInput")
RegistClassMember(LuaGuildVoteEditWnd,              "m_ContentMaxInput")
RegistClassMember(LuaGuildVoteEditWnd,              "m_TitleMinInput")
RegistClassMember(LuaGuildVoteEditWnd,              "m_TitleMaxInput")
RegistClassMember(LuaGuildVoteEditWnd,              "m_HelpContent")

function LuaGuildVoteEditWnd:OnEnable()
    -- g_ScriptEvent:AddListener("SendYuanDanGuildVoteInfo", self, "SendYuanDanGuildVoteInfo")
end

function LuaGuildVoteEditWnd:OnDisable()
    -- g_ScriptEvent:RemoveListener("SendYuanDanGuildVoteInfo", self, "SendYuanDanGuildVoteInfo")
    luaGuildVoteMgr:ClearEditData()
end

function LuaGuildVoteEditWnd:Init()
    self.m_ContentMaxInput = 80       --  即最多输入40个汉字
    self.m_ContentMinInput = 1
    self.m_TitleMaxInput = 10
    self.m_TitleMinInput = 6
    self.m_HelpContent = YuanDan2020_Setting.GetData().RecommandQuestions
    self:InitAdvView()
    self.OptionMenu:SetActive(false)
    UIEventListener.Get(self.GroupBtn).onClick = DelegateFactory.VoidDelegate(function(p)
        self:TurnOptionMenu(not self.OptionMenu.activeSelf)
    end)
    UIEventListener.Get(self.VoteBtn).onClick = DelegateFactory.VoidDelegate(function(p)
        self:OnVoteBtnClick()
    end)
end

function LuaGuildVoteEditWnd:InitAdvView()
    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(function() 
        return self.m_HelpContent.Length
    end, function(item, index)
        self:InitItem(item, index)
    end)

    self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        self:TurnOptionMenu(false)
        self.InputContent.value = self.m_HelpContent[row]
    end)
    self.TableView:ReloadData(true,false)
end

function LuaGuildVoteEditWnd:InitItem(item, index)
    -- YuanDan2020_Setting
    local Contentlable = item.transform:Find("Contentlable"):GetComponent(typeof(UILabel))
    Contentlable.text = self.m_HelpContent[index]
end

function LuaGuildVoteEditWnd:TurnOptionMenu(isOpen)
    self.OptionMenu:SetActive(isOpen)
    if isOpen then
        -- self.TableView:SetSelectRow(-1,true)
        self.TableView.m_ScrollView:ResetPosition()
    end
end

function LuaGuildVoteEditWnd:OnVoteBtnClick()
    if self:CheckInputValue() then
        CUIManager.ShowUI(CLuaUIResources.GuildVoteSelectWnd)
    end
end

function LuaGuildVoteEditWnd:CheckInputValue()

    local QuestionStr = string.gsub(self.InputContent.value, " ", "")
	if CommonDefs.IS_VN_CLIENT then
		QuestionStr = self.InputContent.value
	end
    luaGuildVoteMgr.SendContent = QuestionStr
    
    local ContentLength = CUICommonDef.GetStrByteLength(QuestionStr)
    if ContentLength < self.m_ContentMinInput or ContentLength > self.m_ContentMaxInput then
        g_MessageMgr:ShowMessage("Guild_Vote_Content_Length_Not_Fit")
        return false
    end

    if not CWordFilterMgr.Inst:CheckName(QuestionStr) then
        g_MessageMgr:ShowMessage("Guild_Vote_VIOLATION")
        return false
    end

    local TitleStr = string.gsub(self.InputTitle.value, " ", "")
	if CommonDefs.IS_VN_CLIENT then
		TitleStr = self.InputTitle.value
	end
    luaGuildVoteMgr.SendTitle = TitleStr
    
    local TitleLength = CUICommonDef.GetStrByteLength(TitleStr)
    if TitleLength < self.m_TitleMinInput or TitleLength > self.m_TitleMaxInput then
        g_MessageMgr:ShowMessage("Guild_Vote_Title_Length_Not_Fit")
        return false
    end

    if not CWordFilterMgr.Inst:CheckName(TitleStr) then
        g_MessageMgr:ShowMessage("Guild_Vote_VIOLATION")
        return false
    end

    return true
end