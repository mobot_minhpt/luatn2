local UIGrid = import "UIGrid"

local UILabel = import "UILabel"

local QnTipButton = import "L10.UI.QnTipButton"
local UITable = import "UITable"
local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgrAlignType = import  "L10.UI.CPopupMenuInfoMgr+AlignType"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CPlayerShopPopupMenuInfoMgr = import "L10.UI.CPlayerShopPopupMenuInfoMgr"

LuaShuangshiyi2023DiscountPackageWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShuangshiyi2023DiscountPackageWnd, "CouponsItem", "CouponsItem", GameObject)
RegistChildComponent(LuaShuangshiyi2023DiscountPackageWnd, "DiscountPackageText", "DiscountPackageText", UILabel)
RegistChildComponent(LuaShuangshiyi2023DiscountPackageWnd, "CompositeButton", "CompositeButton", GameObject)
RegistChildComponent(LuaShuangshiyi2023DiscountPackageWnd, "BuyButton", "BuyButton", GameObject)
RegistChildComponent(LuaShuangshiyi2023DiscountPackageWnd, "RuleButton", "RuleButton", GameObject)
RegistChildComponent(LuaShuangshiyi2023DiscountPackageWnd, "QnTipButton", "QnTipButton", QnTipButton)
RegistChildComponent(LuaShuangshiyi2023DiscountPackageWnd, "CouponsGrid", "CouponsGrid", UIGrid)

--@endregion RegistChildComponent end

function LuaShuangshiyi2023DiscountPackageWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

    self:InitWndData()
    self:RefreshConstUI()
    self:RefreshVariableUI()
    self:InitUIEvent()
end

function LuaShuangshiyi2023DiscountPackageWnd:Init()

end

--@region UIEvent

--@endregion UIEvent

function LuaShuangshiyi2023DiscountPackageWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdateTempPlayDataWithKey", self, "UpdateUI")
end

function LuaShuangshiyi2023DiscountPackageWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateTempPlayDataWithKey", self, "UpdateUI")
end

function LuaShuangshiyi2023DiscountPackageWnd:UpdateUI()
    self.couponData = {}
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer and CommonDefs.DictContains_LuaCall(mainPlayer.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11ExteriorDiscountCouponsData) then
        local playDataU = CommonDefs.DictGetValue_LuaCall(mainPlayer.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11ExteriorDiscountCouponsData)
        local list = g_MessagePack.unpack(playDataU.Data.Data)
        self.couponData = list[2]
    end
    self.couponPostProcessData = self:SortCouponData(self.couponData)
    self:RefreshVariableUI()
end

function LuaShuangshiyi2023DiscountPackageWnd:SortCouponData(couponData)
    local tbl = {}
    for couponId, couponNumber in pairs(couponData) do
        local couponData = Double11_ExteriorDiscountCoupons.GetData(couponId)
        if couponNumber ~= 0 then
            local quality = couponData.Quality
            local type = couponData.Type
            local sortExtraParam = 0
            if type == 1 then
                sortExtraParam = 1
            elseif type == 2 then
                sortExtraParam = 3
            elseif type == 3 then
                sortExtraParam = 2
            end
            table.insert(tbl, {couponId = couponId, couponNumber = couponNumber, quality = quality, sortExtraParam = sortExtraParam})
        end
    end
    table.sort(tbl, function(a, b)
        if (a.quality == b.quality) then
            if a.sortExtraParam == b.sortExtraParam then
                return a.couponId < b.couponId
            else
                return a.sortExtraParam > b.sortExtraParam
            end
        else
            return a.quality > b.quality
        end
    end)
    return tbl
end

function LuaShuangshiyi2023DiscountPackageWnd:InitWndData()
    self.filterList = {{filterOption=0, name= LocalString.GetString("显示全部")}, 
                       {filterOption=1, name= LocalString.GetString("显示伞面")}, 
                       {filterOption=2, name= LocalString.GetString("显示坐骑")},
                       {filterOption=3, name= LocalString.GetString("显示背饰")}}

    self.filterOption = 0
    self.filterAction = {}
    for i = 1, #self.filterList do
        local filterData = self.filterList[i]
        table.insert(self.filterAction, PopupMenuItemData(filterData.name, DelegateFactory.Action_int(function (index)
            self.filterOption = filterData.filterOption
            self.selectCouponId = nil
            self:RefreshPackage()
        end), false, nil))
    end

    self.selectCouponId = nil
    self.couponData = {}
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer and CommonDefs.DictContains_LuaCall(mainPlayer.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11ExteriorDiscountCouponsData) then
        local playDataU = CommonDefs.DictGetValue_LuaCall(mainPlayer.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11ExteriorDiscountCouponsData)
        local list = g_MessagePack.unpack(playDataU.Data.Data)
        self.couponData = list[2]
    end
    self.couponPostProcessData = self:SortCouponData(self.couponData)


    self.couponTypeIconList = {}
    local rawData = Double11_DiscountCouponsSetting.GetData().CouponsTypeIcon
    local rawDataList = g_LuaUtil:StrSplit(rawData, ";")
    for i = 1, #rawDataList do
        if rawDataList[i] ~= "" then
            table.insert(self.couponTypeIconList, rawDataList[i])
        end
    end
    
    self.couponShopData = {}
    local rawShopData = Double11_DiscountCouponsSetting.GetData().ExteriorList
    local rawShopDataList = g_LuaUtil:StrSplit(rawShopData, ";")
    for i = 1, #rawShopDataList do
        self.couponShopData[i] = {}
        if rawShopDataList[i] ~= "" then
            local itemList = g_LuaUtil:StrSplit(rawShopDataList[i], ",")
            for j = 1, #itemList do
                if itemList[j] ~= "" then
                    table.insert(self.couponShopData[i],  tonumber(itemList[j]))
                end
            end
        end
    end

    self.couponsList = {}
    local rawCouponsData = Double11_DiscountCouponsSetting.GetData().BigIconList
    local couponsList = g_LuaUtil:StrSplit(rawCouponsData, ";")
    for i = 1, #couponsList do
        if couponsList[i] ~= "" then
            local couponsData = g_LuaUtil:StrSplit(couponsList[i], ",")
            local coupons = {}
            coupons.name = couponsData[1]
            coupons.icon = couponsData[2]
            table.insert(self.couponsList, coupons)
        end
    end
end 

function LuaShuangshiyi2023DiscountPackageWnd:RefreshConstUI()
    self.DiscountPackageText.text = g_MessageMgr:FormatMessage("Double11_2023Discount_Package_Tips")
end

function LuaShuangshiyi2023DiscountPackageWnd:RefreshVariableUI()
    self:RefreshPackage()
end 

function LuaShuangshiyi2023DiscountPackageWnd:InitUIEvent()
    UIEventListener.Get(self.QnTipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        self:OnClickFilterButton()
    end)

    UIEventListener.Get(self.CompositeButton).onClick = DelegateFactory.VoidDelegate(function (_)
        if self.selectCouponId then
            local isExist = false
            isExist = self.couponData[self.selectCouponId] and self.couponData[self.selectCouponId] > 0 or false
            if not isExist then
                g_MessageMgr:ShowMessage("Double11_2023Discount_Package_Select_Coupon_To_Composite")
                return
            end

            local couponData = Double11_ExteriorDiscountCoupons.GetData(self.selectCouponId)
            if couponData.NextID == 0 then
                g_MessageMgr:ShowMessage("Double11_2023Discount_Package_Current_Coupon_Cannot_Composite")
            else
                LuaShuangshiyi2023DiscountCompositeWnd.sourceCouponId = self.selectCouponId
                LuaShuangshiyi2023DiscountCompositeWnd.targetCouponId = couponData.NextID
                CUIManager.ShowUI(CLuaUIResources.Shuangshiyi2023DiscountCompositeWnd)
            end
        else
            g_MessageMgr:ShowMessage("Double11_2023Discount_Package_Select_Coupon_To_Composite")
        end
    end)

    UIEventListener.Get(self.BuyButton).onClick = DelegateFactory.VoidDelegate(function (_)
        if self.selectCouponId then
            local isExist = false
            isExist = self.couponData[self.selectCouponId] and self.couponData[self.selectCouponId] > 0 or false

            if isExist then
                local couponData = Double11_ExteriorDiscountCoupons.GetData(self.selectCouponId)
                LuaShuangshiyi2023DiscountBuyFashionWnd.couponId = self.selectCouponId
                LuaShuangshiyi2023DiscountBuyFashionWnd.shoppingIdList = self.couponShopData[couponData.Type]
                CUIManager.ShowUI(CLuaUIResources.Shuangshiyi2023DiscountBuyFashionWnd)
            else
                g_MessageMgr:ShowMessage("Double11_2023Discount_Package_Select_Coupon_To_Buy")
            end
        else
            g_MessageMgr:ShowMessage("Double11_2023Discount_Package_Select_Coupon_To_Buy")
        end
    end)

    UIEventListener.Get(self.RuleButton).onClick = DelegateFactory.VoidDelegate(function (_)
        g_MessageMgr:ShowMessage("Double11_2023Discount_Package_Rule")
    end)
end 

function LuaShuangshiyi2023DiscountPackageWnd:OnClickFilterButton()
    self.QnTipButton:SetTipStatus(false)
    CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenuWithdefaultSelectedIndex(Table2Array(self.filterAction, MakeArrayClass(PopupMenuItemData)), self.filterOption, self.QnTipButton.transform, CPopupMenuInfoMgrAlignType.Bottom, 1, nil, nil, DelegateFactory.Action(function ()
        self.QnTipButton:SetTipStatus(true)
    end), 600,300)
end 

function LuaShuangshiyi2023DiscountPackageWnd:RefreshPackage()
    self.QnTipButton.transform:Find("Label"):GetComponent(typeof(UILabel)).text = 
        self.filterList[self.filterOption + 1].name

    self.couponObjTbl = {}
    Extensions.RemoveAllChildren(self.CouponsGrid.transform)
    self.CouponsItem:SetActive(false)
    for i = 1, #self.couponPostProcessData do
        local couponId = self.couponPostProcessData[i].couponId
        local couponNumber = self.couponPostProcessData[i].couponNumber
        local couponData = Double11_ExteriorDiscountCoupons.GetData(couponId)
        if (couponData.Type == self.filterOption or self.filterOption == 0) and (couponNumber ~= 0) then
            local templateItem = CommonDefs.Object_Instantiate(self.CouponsItem)
            templateItem:SetActive(true)
            templateItem.transform.parent = self.CouponsGrid.transform
            templateItem.transform.localScale = Vector3.one

            local quality = couponData.Quality
            local icon = self.couponsList[quality].icon
            local textureComp = templateItem.transform:Find("Card"):GetComponent(typeof(CUITexture))
            textureComp:LoadMaterial(icon)

            local name = couponData.Name
            templateItem.transform:Find("Name"):GetComponent(typeof(UILabel)).text = name

            local couponType = couponData.Type
            templateItem.transform:Find("TypeIcon/san").gameObject:SetActive(couponType == 1)
            templateItem.transform:Find("TypeIcon/zuoji").gameObject:SetActive(couponType == 2)
            templateItem.transform:Find("TypeIcon/beishi").gameObject:SetActive(couponType == 3)
            templateItem.transform:Find("TypeIcon/san"):GetComponent(typeof(UITexture)).color = LuaShuangshiyi2023Mgr.DiscountQualityColor[quality]
            templateItem.transform:Find("TypeIcon/zuoji"):GetComponent(typeof(UITexture)).color = LuaShuangshiyi2023Mgr.DiscountQualityColor[quality]
            templateItem.transform:Find("TypeIcon/beishi"):GetComponent(typeof(UITexture)).color = LuaShuangshiyi2023Mgr.DiscountQualityColor[quality]
            
            templateItem.transform:Find("Number"):GetComponent(typeof(UILabel)).text = ("X"..couponNumber)
            templateItem.transform:Find("IsSelected").gameObject:SetActive(self.selectCouponId and couponId == self.selectCouponId or false)

            UIEventListener.Get(templateItem).onClick = DelegateFactory.VoidDelegate(function (_)
                self.selectCouponId = couponId
                for k, v in pairs(self.couponObjTbl) do
                    v.transform:Find("IsSelected").gameObject:SetActive(self.selectCouponId and k == self.selectCouponId or false)
                end
            end)
            
            self.couponObjTbl[couponId] = templateItem
        end
    end
    self.CouponsGrid:Reposition()
end 
