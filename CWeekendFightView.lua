-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CScene = import "L10.Game.CScene"
local CWeekendFightMgr = import "L10.Game.CWeekendFightMgr"
local CWeekendFightView = import "L10.UI.CWeekendFightView"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CWeekendFightView.m_Init_CS2LuaHook = function (this) 
    this.remainTimeLabel.enabled = false
    if CClientMainPlayer.Inst ~= nil and CScene.MainScene ~= nil then
        this.remainTimeLabel.enabled = CScene.MainScene.ShowTimeCountDown
        this:SetRemainTimeVal()
    end
    this.titleLabel.text = CommonDefs.String_Format_String_ArrayObject(LocalString.GetString("仙踪山"), {})
end
CWeekendFightView.m_Start_CS2LuaHook = function (this) 
    UIEventListener.Get(this.leaveBtn).onClick = MakeDelegateFromCSFunction(this.OnLeaveButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.checkBtn).onClick = MakeDelegateFromCSFunction(this.OpenInfoWnd, VoidDelegate, this)
    this.rankLabel.text = ""
    this.sLabel1.text = ""
    this.sLabel2.text = ""
    this.sLabel3.text = ""
end
CWeekendFightView.m_OnEnable_CS2LuaHook = function (this) 
    this:Init()
    EventManager.AddListener(EnumEventType.MainPlayerCreated, MakeDelegateFromCSFunction(this.OnMainPlayCreated, Action0, this))
    EventManager.AddListener(EnumEventType.ShowTimeCountDown, MakeDelegateFromCSFunction(this.OnShowTimeCountDown, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.SceneRemainTimeUpdate, MakeDelegateFromCSFunction(this.OnRemainTimeUpdate, MakeGenericClass(Action1, Int32), this))
end
CWeekendFightView.m_SetRemainTimeVal_CS2LuaHook = function (this) 
    if CScene.MainScene ~= nil then
        if CScene.MainScene.ShowTimeCountDown then
            this.remainTimeLabel.text = this:GetRemainTimeText(CScene.MainScene.ShowTime)
        else
            this.remainTimeLabel.text = nil
        end
    else
        this.remainTimeLabel.text = nil
    end
end
CWeekendFightView.m_GetRemainTimeText_CS2LuaHook = function (this, totalSeconds) 
    local fInfo = CWeekendFightMgr.Inst:GetSelfForceInfo()
    if fInfo == nil then
        this.rankLabel.text = ""
        this.sLabel1.text = ""
        this.sLabel2.text = ""
        this.sLabel3.text = ""
    else
        this.rankLabel.text = tostring(fInfo.rank)
        this.sLabel1.text = tostring(fInfo.num1)
        this.sLabel2.text = tostring(fInfo.num2)
        this.sLabel3.text = tostring(fInfo.num3)
    end

    if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return System.String.Format("[ACF9FF]{0:00}:{1:00}:{2:00}[-]", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        return System.String.Format("[ACF9FF]{0:00}:{1:00}[-]", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end
