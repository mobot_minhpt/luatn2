local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CSkillMgr = import "L10.Game.CSkillMgr"
local CPlayerHeadInfoItem = import "L10.UI.CPlayerHeadInfoItem"
local CScene = import "L10.Game.CScene"
local CGuanNingMgr = import "L10.Game.CGuanNingMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientOtherPlayer = import "L10.Game.CClientOtherPlayer"
local CClientFakePlayer = import "L10.Game.CClientFakePlayer"
local CGameVideoPlayer = import "L10.Game.CGameVideoPlayer"
local Profession = import "L10.Game.Profession"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Type = import "UIBasicSprite.Type"
local Effect = import "UILabel.Effect"
local Vector2 = import "UnityEngine.Vector2"
local CForcesMgr = import "L10.Game.CForcesMgr"
local CTeamMgr = import "L10.Game.CTeamMgr"

LuaPlayerHeadInfoItem = class()
RegistClassMember(LuaPlayerHeadInfoItem, "m_DiekeEngineId2BubbleInfo")
RegistClassMember(LuaPlayerHeadInfoItem, "m_FollowId")
RegistClassMember(LuaPlayerHeadInfoItem, "m_PlayerHeadInfoItem")
RegistClassMember(LuaPlayerHeadInfoItem, "m_JianLaJiNode")
RegistClassMember(LuaPlayerHeadInfoItem, "m_JianLaJiBuffTick")
RegistClassMember(LuaPlayerHeadInfoItem, "m_TickNode")      -- 玩家头顶显示倒计时通用图标

RegistClassMember(LuaPlayerHeadInfoItem, "m_CountDownTick")
RegistClassMember(LuaPlayerHeadInfoItem, "m_TickNodeType")
function LuaPlayerHeadInfoItem:OnEnable()
    self.m_PlayerHeadInfoItem = self.gameObject:GetComponent(typeof(CPlayerHeadInfoItem))
    g_ScriptEvent:AddListener("UpdatePlayerHeadInfoCountDown",self,"ShowCountDownIcon")
    g_ScriptEvent:AddListener("UpdatePreTaskPoisonProgress",self,"OnUpdatePreTaskPoisonProgress")
end
function LuaPlayerHeadInfoItem:OnDisable()
    g_ScriptEvent:RemoveListener("UpdatePlayerHeadInfoCountDown",self,"ShowCountDownIcon")
    g_ScriptEvent:RemoveListener("UpdatePreTaskPoisonProgress",self,"OnUpdatePreTaskPoisonProgress")
    self:CloseCountDownIcon()
    self:StopJianLaJiBuffTick()
end 
--使用InitHeadInfo留意性能问题
function LuaPlayerHeadInfoItem:InitHeadInfo(args)
    local this = args[0]
    if self.m_JianLaJiNode == nil then
        self.m_JianLaJiNode = self.transform:Find("Table/JianLaJiNode")
    end
    if self.m_DoubleOneBaoYuDuoYuNode == nil then
        self.m_DoubleOneBaoYuDuoYuNode = self.transform:Find("Table/DoubleOneBaoYuDuoYuNode")
    end

    if LuaShiTuMgr:IsInJianLaJiPlay() then
        local clientObj = CClientObjectMgr.Inst:GetObject(this:FollowId())
        self.m_JianLaJiNode.gameObject:SetActive(clientObj and CommonDefs.DictContains_LuaCall(clientObj.BuffProp.Buffs, ShiTuTask_JianLaJiSetting.GetData().SubmitErrBuffId) or false)
        if self.m_JianLaJiNode.gameObject.activeSelf then
            self:StartJianLaJiBuffTick(clientObj)
        else
            self:StopJianLaJiBuffTick()
        end
    else
        self.m_JianLaJiNode.gameObject:SetActive(false)
    end
    
end

function LuaPlayerHeadInfoItem:StartJianLaJiBuffTick(clientObj)
    self:StopJianLaJiBuffTick()
    local buffId = ShiTuTask_JianLaJiSetting.GetData().SubmitErrBuffId
    local leftTime = 0
    if clientObj ~= nil and CommonDefs.DictContains_LuaCall(clientObj.BuffProp.Buffs, buffId) then
        local b = CommonDefs.DictGetValue_LuaCall(clientObj.BuffProp.Buffs, buffId)
        if b.EndTime>0 then
            local ts = CommonDefs.op_Subtraction_DateTime_DateTime(CServerTimeMgr.Inst:GetZone8Time(), b.receivedTime)
            leftTime = math.max(0, b.EndTime - ts.TotalSeconds)
        end
    end
    if leftTime<=0 then return end
    local finishTime = leftTime + CServerTimeMgr.Inst.timeStamp
    self.m_JianLaJiBuffTick = RegisterTick(function()
        self.m_JianLaJiNode:GetComponent(typeof(UIWidget)).fillAmount = math.max(0, finishTime - CServerTimeMgr.Inst.timeStamp) / leftTime
    end, 100)
end

function LuaPlayerHeadInfoItem:StopJianLaJiBuffTick()
    if self.m_JianLaJiBuffTick ~= nil then
        UnRegisterTick(self.m_JianLaJiBuffTick)
        self.m_JianLaJiBuffTick = nil
    end
end
function LuaPlayerHeadInfoItem:CloseCountDownIcon()
    if self.m_CountDownTick ~= nil then
        UnRegisterTick(self.m_CountDownTick)
        self.m_CountDownTick = nil
    end
    if self.m_TickNode then 
        self.m_TickNode.gameObject:SetActive(false)
        -- 特效销毁
        self.m_TickNode.transform:Find("FX").gameObject:GetComponent(typeof(CUIFx)):DestroyFx()
    end
end
function LuaPlayerHeadInfoItem:ShowCountDownIcon(args)
    if not self.m_TickNode then self.m_TickNode = self.transform:Find("Table/TickNode") end
    local engineId,textureForm,LabelForm,CountDownForm,needShow,TickNodeType = args[0],args[1],args[2],args[3],args[4],args[5]
    if TickNodeType == self.m_TickNodeType and (not needShow) and (not engineId) then -- 根据Type统一隐藏掉所有的TickNode
        self.m_TickNode.gameObject:SetActive(false) 
        if self.m_CountDownTick ~= nil then
            UnRegisterTick(self.m_CountDownTick)
            self.m_CountDownTick = nil
        end
        self.m_PlayerHeadInfoItem.needReposition = true
        return 
    end   
    self.m_TickNodeType = TickNodeType
    -- 处理enginedID对应的TickNodeType
    if engineId ~= self.m_PlayerHeadInfoItem:FollowId() then return end
    if needShow then
        self.m_TickNode.gameObject:SetActive(true)
        self.m_PlayerHeadInfoItem.needReposition = true
    else
        self.m_TickNode.gameObject:SetActive(false)
        if self.m_CountDownTick ~= nil then
            UnRegisterTick(self.m_CountDownTick)
            self.m_CountDownTick = nil
        end
        self.m_PlayerHeadInfoItem.needReposition = true
        return
    end
    self:ShowCountDownTexture(textureForm)
    self:ShowCountDownLabel(LabelForm)
    self:OnSetCountDown(CountDownForm)
end

function LuaPlayerHeadInfoItem:ShowCountDownTexture(textureForm)
    if not textureForm then return end
    local spriteBg = self.m_TickNode.transform:Find("SpriteBg"):GetComponent(typeof(UISprite))
    local uiTexture = self.m_TickNode.transform:Find("Texture").gameObject:GetComponent(typeof(CUITexture))
    
    spriteBg.spriteName = textureForm.spriteName or ""
    uiTexture:LoadMaterial(textureForm.texturePath or "")
    uiTexture.transform.localPosition = textureForm.textureLocalPosition or Vector3.zero
    uiTexture.texture.width = (textureForm.textureSize or {})[1] or 100
    uiTexture.texture.height = (textureForm.textureSize or {})[2] or 100
    uiTexture.texture.type = textureForm.textureType or Type.Simple
    uiTexture.texture.color = textureForm.color or Color.white
    uiTexture.texture.alpha = textureForm.alpha or 1

    -- #186345【晶元争夺战】，新增特效
    local fx = self.m_TickNode.transform:Find("FX").gameObject:GetComponent(typeof(CUIFx))
    fx:DestroyFx()
    if textureForm.FxPath then fx:LoadFx(textureForm.FxPath) end
end

function LuaPlayerHeadInfoItem:ShowCountDownLabel(LabelForm)
    if not LabelForm then return end

    local commonLabel = self.m_TickNode.transform:Find("CountdownLabel").gameObject:GetComponent(typeof(UILabel))
    local commonV2Label = self.m_TickNode.transform:Find("CountdownV2Label").gameObject:GetComponent(typeof(UILabel))
    local shufaLabel = self.m_TickNode.transform:Find("ShuFaLabel").gameObject:GetComponent(typeof(UILabel))
    local uiLabel = nil
    if LabelForm.FontType == "shufa" then
        uiLabel = shufaLabel
        commonLabel.gameObject:SetActive(false)
        commonV2Label.gameObject:SetActive(false)
        shufaLabel.gameObject:SetActive(true)
    elseif LabelForm.FontType == "defaultFont" then
        uiLabel = commonV2Label
        commonLabel.gameObject:SetActive(false)
        commonV2Label.gameObject:SetActive(true)
        shufaLabel.gameObject:SetActive(false)
    else
        uiLabel = commonLabel
        commonLabel.gameObject:SetActive(true)
        commonV2Label.gameObject:SetActive(false)
        shufaLabel.gameObject:SetActive(false)
    end

    uiLabel.color = LabelForm.Color or Color.white
    uiLabel.fontSize = LabelForm.LabelSize or 32
    uiLabel.transform.localPosition = LabelForm.LabelLocalPosition or Vector3.zero
    uiLabel.effectStyle = LabelForm.LabelEffectStyle or Effect.None
    uiLabel.effectDistance = LabelForm.LabelEffectOffset or Vector2(1,1)
    uiLabel.effectColor = LabelForm.LabelEffectColor or Color.white
    
    if LabelForm.content then uiLabel.text = LabelForm.content end
end

function LuaPlayerHeadInfoItem:OnSetCountDown(CountDownForm)
    local uiLabel = self.m_TickNode.transform:Find("CountdownLabel").gameObject:GetComponent(typeof(UILabel))
    if not CountDownForm then
        uiLabel.text = ""
        return
    end
    if CountDownForm.content then       -- content传入的数据需要为倒计时的结束时间戳
        local tempTime = CountDownForm.content - CServerTimeMgr.Inst.timeStamp
        uiLabel.text = math.floor(math.max(0,tempTime % 60))
        if CountDownForm.autoTick then
            if self.m_CountDownTick ~= nil then
                UnRegisterTick(self.m_CountDownTick)
                self.m_CountDownTick = nil
            end
            local interval = CountDownForm.tickInterval or 1000
            self.m_CountDownTick = RegisterTick(function ()
                tempTime = CountDownForm.content - CServerTimeMgr.Inst.timeStamp
                if not uiLabel or tempTime <= 0 then 
                    UnRegisterTick(self.m_CountDownTick)
                    self.m_CountDownTick = nil
                    if CountDownForm.autoRemove then
                        self.m_TickNode.gameObject:SetActive(false)
                    end
                    return
                end
                uiLabel.text = math.floor(math.max(0,tempTime % 60))
            end,interval)
        end
    end
end

function LuaPlayerHeadInfoItem:OnSpeakingCreate(args)
    local engineId, content, type, duration = args[0],args[1],args[2],args[3]
    self.m_FollowId = self.m_PlayerHeadInfoItem:FollowId()
    local player = CClientObjectMgr.Inst:GetObject(engineId)
    if player then
        local processedPlayer = CSkillMgr.Inst:GetPossessTarget(player)
        if processedPlayer then
            if processedPlayer.EngineId == self.m_FollowId then
                self:AddBubble(content, type, duration, engineId)
            end
        end
    end
    if self.m_PlayerHeadInfoItem.isShadow or self.m_FollowId ~= engineId then return end
    -- 头顶气泡样式
    local info = LuaGamePlayMgr:GetPlayerSpeakBubbleInfo(engineId)
    self.m_PlayerHeadInfoItem.speakingBubble.textLabel.fontSize = info and info.fontSize or self.m_PlayerHeadInfoItem.defaultNameFontSize
    self.m_PlayerHeadInfoItem.processingDiekeSpeakingBubble.textLabel.fontSize = info and info.fontSize or self.m_PlayerHeadInfoItem.defaultNameFontSize

    self:AddBubble(content, type, duration)
end

function LuaPlayerHeadInfoItem:OnDieKeUnPossessByOther(args)
    if not self.m_DiekeEngineId2BubbleInfo then
        self.m_DiekeEngineId2BubbleInfo = {}
    end
    local engineId, targetEngineId, aggressivePossessCount, positivePossessCount= args[0],args[1],args[2],args[3]
    if targetEngineId ~= self.m_FollowId then return end
    local t = self:GetBubbles()
    for i = 2,1,-1 do
        local bubbleInfo = t[i]
        if bubbleInfo and bubbleInfo.diekeEngineId == engineId then
            self:DeleteBubble(bubbleInfo)
        end
    end
end

function LuaPlayerHeadInfoItem:GetBubbles()
    local targetEngineId = self.m_FollowId
    if not self.m_DiekeEngineId2BubbleInfo then
        self.m_DiekeEngineId2BubbleInfo = {}
    end
    if not self.m_DiekeEngineId2BubbleInfo[targetEngineId] then
        self.m_DiekeEngineId2BubbleInfo[targetEngineId] = {m_FirstBubble = nil, m_SecondBubble = nil}
    end
    return self.m_DiekeEngineId2BubbleInfo[targetEngineId]
end

function LuaPlayerHeadInfoItem:AddBubble(content, type, duration, diekeEngineId)
    local t = self:GetBubbles()
    if not diekeEngineId then
        for i = 2,1,-1 do
            local bubbleInfo = t[i]
            if bubbleInfo and not bubbleInfo.diekeEngineId then
                table.remove(t,i)
            end
        end
    end
    table.insert(t,{content = content, type = type, hideTime = Time.realtimeSinceStartup + duration, diekeEngineId = diekeEngineId})
    if #t > 2 then
        table.remove(t,1)
    end
    self:ShowBubbles()
end

function LuaPlayerHeadInfoItem:ShowBubbles()
    local t = self:GetBubbles()
    for i = 1, 2 do
        local bubble = i == 1 and self.m_PlayerHeadInfoItem.speakingBubble or self.m_PlayerHeadInfoItem.processingDiekeSpeakingBubble
        local bubbleInfo = t[i]
        if not bubbleInfo then
            bubble.gameObject:SetActive(false)
        else
            local content, type, duration, diekeEngineId = bubbleInfo.content,bubbleInfo.type,bubbleInfo.hideTime - Time.realtimeSinceStartup,bubbleInfo.diekeEngineId
            bubble:Init(content, type,DelegateFactory.Action(function()
                self:DeleteBubble(bubbleInfo)
                self.m_PlayerHeadInfoItem.needReposition = true
            end), duration)
            local diekeSprite = bubble.transform:Find("Background/DiekeSprite").gameObject:GetComponent(typeof(UITexture))
            diekeSprite.gameObject:SetActive(diekeEngineId ~= nil)
            diekeSprite:ResetAndUpdateAnchors()
        end
    end
    self.m_PlayerHeadInfoItem.needReposition = true
end

function LuaPlayerHeadInfoItem:DeleteBubble(bubbleInfo)
    local t = self:GetBubbles()
    local pos = nil
    for index,b in pairs(t) do
        if b == bubbleInfo then
            pos = index
        end
    end
    if pos then
        table.remove(t,pos)
    end
    self:ShowBubbles()
end

function LuaPlayerHeadInfoItem:OnUpdatePreTaskPoisonProgress(lv)
    local poisonItem = self.transform:Find("Table/NameBoard/NameBoard/PreTaskPoisonProgress")
    if poisonItem then
        if poisonItem.gameObject.activeSelf and lv <= 0 then
            poisonItem.gameObject:SetActive(false)
            return
        end
        if not poisonItem.gameObject.activeSelf and lv > 0 then
            poisonItem.gameObject:SetActive(true)
        end
        local label = poisonItem:Find("PoisonLabel")
        if label then
            label = poisonItem:Find("PoisonLabel"):GetComponent(typeof(UILabel))
            label.text = lv
        end
    end
end

-- C# hook 暂时先放这里,调整内容时注意考虑性能开销
-- 请使用LuaGamePlayMgr:SetPlayerHeadSpeIconInfos方法（尽量不要在prefab中创建新的节点）
CPlayerHeadInfoItem.m_hookCheckSpeIconShowInLua = function(this, sign, playerId)
    --根据hook的位置，这里sign一定为true并且playerId>0
    local playId = CScene.MainScene and CScene.MainScene.GamePlayDesignId or 0
    local SpeTexture = this.speIconContainer.transform:Find("SpeTexture"):GetComponent(typeof(CUITexture))
    local showSpeTexture = false
    local showIcon = false
    if playId == LuaHanJiaMgr:GetXueJingKuangHuanPlayId() then
        if LuaHanJiaMgr.m_XueJingKuangHuanTempPlayInfo[playerId] then
            showIcon = true
            this.speIconTexture.spriteName = LuaHanJiaMgr.m_XueJingKuangHuanTempPlayInfo[playerId]
            this.speIconTexture:MakePixelPerfect()
        end
    elseif LuaTangYuan2022Mgr.IsInPlay() then
        local rank = 4
        if LuaTangYuan2022Mgr.PlayerInfo and next(LuaTangYuan2022Mgr.PlayerInfo) then
            for i = 1, #LuaTangYuan2022Mgr.PlayerInfo, 1 do
                local playerInfo = LuaTangYuan2022Mgr.PlayerInfo[i]
                if playerInfo.playerId == playerId  then
                    rank = playerInfo.rank
                end
            end
        end
        if rank <= 3 then
            showIcon = true
            local spriteName = "common_huangguan_0"..rank
            this.speIconTexture.spriteName = spriteName
            this.speIconTexture:MakePixelPerfect()
        end
    elseif CLuaStarBiwuMgr:IsInStarBiwu() or CLuaStarBiwuMgr:IsInStarBiwuWatch() then
        local clientObj = CClientObjectMgr.Inst:GetObject(this.objId)
        local player = nil
        if TypeIs(clientObj, typeof(CClientMainPlayer)) then
            player = CClientMainPlayer.Inst
        elseif TypeIs(clientObj, typeof(CClientOtherPlayer)) then
            player = TypeAs(clientObj, typeof(CClientOtherPlayer))
        elseif TypeIs(clientObj, typeof(CClientFakePlayer)) then
            player = TypeAs(clientObj, typeof(CClientFakePlayer))
        elseif TypeIs(clientObj, typeof(CGameVideoPlayer)) then
            player = TypeAs(clientObj, typeof(CGameVideoPlayer))
        end
        if player then
            this.speIconTexture.atlas = CPlayerHeadInfoItem.MainUIAtlas
            this.speIconTexture.spriteName = Profession.GetIcon(player.Class)
            showIcon = true
            this.speIconTexture:MakePixelPerfect()
        end
    elseif playId == LuaChunJie2023Mgr:GetTTSYPlayId() then
        -- [2023春节]饕餮盛宴
        if playerId > 0 then
            local rank = LuaChunJie2023Mgr.TTSY_HeadInfoTbl[playerId]
            if rank then
                this.GuanNingEquipRankIcon.gameObject:SetActive(true)
                this.GuanNingEquipRankIcon.spriteName = rank
            else
                this.GuanNingEquipRankIcon.gameObject:SetActive(false)
            end
        end
    elseif playId == Christmas2022_Setting.GetData().GamePlayId then
        local force = CForcesMgr.Inst:GetPlayerForce(playerId)
        this.speIconTexture.transform.localPosition = Vector3(33, -4, 0)
        this.speIconTexture.spriteName = "guildhorse_mingci"
        this.speIconTexture.color = force == 1 and NGUIText.ParseColor24("ff4634", 0) or NGUIText.ParseColor24("58ceff", 0) 
        showIcon = true
        this.speIconTexture:MakePixelPerfect()
    else
        local info = LuaGamePlayMgr:GetPlayerHeadSpeIconInfo(playerId, this:FollowId())
        if info then
            showSpeTexture = true
            showIcon = true
            if info.atlas and info.atlas == "MainUI" then
                this.speIconTexture.atlas = CPlayerHeadInfoItem.MainUIAtlas
            end
            this.speIconTexture.spriteName = info.spriteName or ""
            if info.spriteName then this.speIconTexture:MakePixelPerfect() end
            SpeTexture:LoadMaterial(info.texturePath or "")

            if not System.String.IsNullOrEmpty(info.label) then
                this.speIconLabel.text = info.label
                this.speIconLabel.color = info.labelColor and info.labelColor or Color.white
            end
        end
    end
    SpeTexture.gameObject:SetActive(showSpeTexture)

    local headTexure = this.HeadTexture
    headTexure.texture.width = 60
    headTexure.texture.height = 60
    if CGuanNingMgr.Inst.inGnjc then
        if CLuaGuanNingMgr:IsPlayerCommander(playerId) then
            headTexure.gameObject:SetActive(true)
            if CLuaGuanNingMgr:GetPlayerForce(playerId) == 0 then
                headTexure:LoadMaterial(GuanNing_Setting.GetData().CommandIconResourceBlue)
            else
                headTexure:LoadMaterial(GuanNing_Setting.GetData().CommandIconResourceRed)
            end
        else
            headTexure.gameObject:SetActive(false)
        end
    elseif playId == QingMing2023_PVPSetting.GetData().GamePlayId then
        local icon = LuaQingMing2023Mgr:ShowPlayerHeadInfoKey(playerId)
        headTexure.gameObject:SetActive(icon ~= nil)
        if icon ~= nil then headTexure:LoadMaterial(icon) end
        this.table:Reposition()
    elseif playId == Double11_MQLD.GetData().GamePlayId then
        headTexure.texture.width = 54
        headTexure.texture.height = 47
        local icon = LuaShuangshiyi2023Mgr:GetMQLDHeadTexturePath(playerId)
        headTexure.gameObject:SetActive(icon ~= nil)
        if icon ~= nil then headTexure:LoadMaterial(icon) end
        this.table:Reposition()
    else
        headTexure.gameObject:SetActive(false)
    end
    this.speIconContainer.gameObject:SetActive(showIcon)
end
