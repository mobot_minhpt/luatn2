local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnTabView = import "L10.UI.QnTabView"
local GameObject = import "UnityEngine.GameObject"
local UITable = import "UITable"
local QnButton = import "L10.UI.QnButton"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local MessageMgr = import "L10.Game.MessageMgr"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local AlignType2 = import "CPlayerInfoMgr+AlignType"

LuaZongMenPracticeRuleAndRankWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZongMenPracticeRuleAndRankWnd, "TabBar", "TabBar", QnTabView)
RegistChildComponent(LuaZongMenPracticeRuleAndRankWnd, "RankList", "RankList", GameObject)
RegistChildComponent(LuaZongMenPracticeRuleAndRankWnd, "RankReward", "RankReward", QnButton)
RegistChildComponent(LuaZongMenPracticeRuleAndRankWnd, "RewardLab", "RewardLab", UILabel)
RegistChildComponent(LuaZongMenPracticeRuleAndRankWnd, "NoTip", "NoTip", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaZongMenPracticeRuleAndRankWnd, "m_TableView")
RegistClassMember(LuaZongMenPracticeRuleAndRankWnd, "m_RankRewardIcon")
RegistClassMember(LuaZongMenPracticeRuleAndRankWnd, "m_RankItemId")
RegistClassMember(LuaZongMenPracticeRuleAndRankWnd, "m_RankList")
RegistClassMember(LuaZongMenPracticeRuleAndRankWnd, "m_TopNum")

function LuaZongMenPracticeRuleAndRankWnd:Awake()
    self.m_RankRewardIcon   = self.RankReward.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    self.m_TableView        = self.RankList.transform:Find("TableBody"):GetComponent(typeof(QnTableView))
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create(function()
        return #self.m_RankList
    end, function(item, index)
        item:SetBackgroundTexture(index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)
        self:InitItem(item, self.m_RankList[index + 1])
    end)

    self.m_TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        local data = self.m_RankList[row + 1]
        if data then
            CPlayerInfoMgr.ShowPlayerPopupMenu(data.PlayerIndex, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined,
                "", nil, Vector3.zero, AlignType2.Default)
        end
    end)

    self.m_TopNum = tonumber(Menpai_Setting.GetData("XiuxingRewardCount").Value)
    self.m_RankItemId = tonumber(Menpai_Setting.GetData("XiuxingRewardItem").Value)
end

function LuaZongMenPracticeRuleAndRankWnd:Init()
    self.RewardLab.text = SafeStringFormat3(LocalString.GetString("排名前%d可获得奖励"), self.m_TopNum)
    UIEventListener.Get(self.RankReward.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    CItemInfoMgr.ShowLinkItemTemplateInfo(self.m_RankItemId, false, nil, AlignType.Default, 0, 0, 0, 0)
	end)
    local data = Item_Item.GetData(self.m_RankItemId)
    if data then
        self.m_RankRewardIcon:LoadMaterial(data.Icon)
    end

    local mainplayer = CClientMainPlayer.Inst
    if mainplayer then
        self:InitMainPlayerRankInfo(self.RankList, {Name = mainplayer.Name})
    end

    if CClientMainPlayer.Inst then
        Gac2Gas.QuerySectBoomRank_Ite(CClientMainPlayer.Inst.BasicProp.SectId)
    end
end

function LuaZongMenPracticeRuleAndRankWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncSectBoomRank_Ite", self, "OnSyncSectBoomRank_Ite")
end

function LuaZongMenPracticeRuleAndRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncSectBoomRank_Ite", self, "OnSyncSectBoomRank_Ite")
end

--@region UIEvent

--@endregion UIEvent


function LuaZongMenPracticeRuleAndRankWnd:OnSyncSectBoomRank_Ite(sectId, rankTab, sectScore)
    self:InitZongMenPlayerRank(sectId, rankTab, sectScore)
end

function LuaZongMenPracticeRuleAndRankWnd:InitZongMenPlayerRank(sectId, rankTab, sectScore)
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer == nil then
        return
    end
    local mainPlayerInfo = nil
    self.m_RankList = {}
    for i = 1, #rankTab do
        local data = rankTab[i]
        local info = {PlayerIndex = data.PlayerId, Rank = i, Name = data.Name, Value3 = data.Damage}
        table.insert(self.m_RankList, info)
        if mainplayer.Id == data.PlayerId then
            mainPlayerInfo = info
        end
    end
    self:InitMainPlayerRankInfo(self.RankList, mainPlayerInfo or {Name = mainplayer.Name})
    self.m_TableView:ReloadData(true, false)
    self.NoTip:SetActive(#self.m_RankList == 0)
end

function LuaZongMenPracticeRuleAndRankWnd:InitMainPlayerRankInfo(RankList, info)
    local tf = RankList.transform:Find("MainPlayerInfo")

    self:InitItem(tf, info)
end

function LuaZongMenPracticeRuleAndRankWnd:InitItem(item, info)
    local tf = item.transform:Find("Table")
    local nameLabel = tf:Find("name"):GetComponent(typeof(UILabel))
    nameLabel.text = info.Name
    local rankLabel = tf:Find("rank"):GetComponent(typeof(UILabel))
    rankLabel.text = ""
    local rankSprite = tf:Find("rank/rankImage"):GetComponent(typeof(UISprite))
    rankSprite.spriteName = ""
    local rank = info.Rank
    if rank == 1 then
        rankSprite.spriteName = "Rank_No.1"
    elseif rank == 2 then
        rankSprite.spriteName = "Rank_No.2png"
    elseif rank == 3 then
        rankSprite.spriteName = "Rank_No.3png"
    else
        rankLabel.text = (rank and rank > 0) and tostring(rank) or LocalString.GetString("未上榜")
    end

    local paramTf3
    local param3

    paramTf3 = tf:Find("param3")
    if paramTf3 then
        local param3 = paramTf3:GetComponent(typeof(UILabel))
        param3.text = info.Value3 or LocalString.GetString("暂无")
    end
end