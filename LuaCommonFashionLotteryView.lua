local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local Animation = import "UnityEngine.Animation"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CButton = import "L10.UI.CButton"

LuaCommonFashionLotteryView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaCommonFashionLotteryView, "ModelRoot", Animation)

RegistClassMember(LuaCommonFashionLotteryView, "m_CloseBtn")
RegistClassMember(LuaCommonFashionLotteryView, "m_RuleBtn")

RegistClassMember(LuaCommonFashionLotteryView, "m_RaffleTicketLabel")
RegistClassMember(LuaCommonFashionLotteryView, "m_RaffleTicketAddBtn")
RegistClassMember(LuaCommonFashionLotteryView, "m_AllPurposeTicketLabel")

RegistClassMember(LuaCommonFashionLotteryView, "m_ExchangeBtn")
RegistClassMember(LuaCommonFashionLotteryView, "m_TryDressBtn")
RegistClassMember(LuaCommonFashionLotteryView, "m_PrizePoolBtn")

RegistClassMember(LuaCommonFashionLotteryView, "m_OnceBtn")
RegistClassMember(LuaCommonFashionLotteryView, "m_TenBtn")
RegistClassMember(LuaCommonFashionLotteryView, "m_MinimumTip")
RegistClassMember(LuaCommonFashionLotteryView, "m_MinimumLabel")

RegistClassMember(LuaCommonFashionLotteryView, "m_ActivitySetInfo")
RegistClassMember(LuaCommonFashionLotteryView, "m_ModelInfo")
RegistClassMember(LuaCommonFashionLotteryView, "m_ModelIndex")
RegistClassMember(LuaCommonFashionLotteryView, "m_ShowModelTick")
RegistClassMember(LuaCommonFashionLotteryView, "m_ShowModelCountDown")

RegistClassMember(LuaCommonFashionLotteryView, "isClosing")

--@endregion RegistChildComponent end

function LuaCommonFashionLotteryView:Awake()
    self.m_CloseBtn = self.transform:Find("CloseBtn").gameObject
    self.m_RuleBtn = self.transform:Find("Right/RuleBtn").gameObject

    self.m_LotteryTicketLabel = self.transform:Find("Top/LotteryTicket/Label"):GetComponent(typeof(UILabel))
    self.m_LotteryTicketAddBtn = self.transform:Find("Top/LotteryTicket/AddBtn").gameObject
    self.m_UniTicketLabel = self.transform:Find("Top/UniTicket/Label"):GetComponent(typeof(UILabel))

    self.m_ExchangeBtn = self.transform:Find("Right/ExchangeBtn").gameObject
    self.m_TryDressBtn = self.transform:Find("Right/TryDressBtn").gameObject
    self.m_PrizePoolBtn = self.transform:Find("Right/PrizePoolBtn").gameObject

    self.m_OnceBtn = self.transform:Find("Main/OnceBtn"):GetComponent(typeof(CButton))
    self.m_TenBtn = self.transform:Find("Main/TenBtn"):GetComponent(typeof(CButton))
    self.m_MinimumTip = self.transform:Find("Main/Minimum").gameObject
    self.m_MinimumLabel = self.transform:Find("Main/Minimum/TimeLabel"):GetComponent(typeof(UILabel))

    UIEventListener.Get(self.m_CloseBtn).onClick =  DelegateFactory.VoidDelegate(function (go)
        self:OnCloseBtnClick(go)
    end)
    UIEventListener.Get(self.m_RuleBtn).onClick =  DelegateFactory.VoidDelegate(function (go)
        self:OnRuleBtnClick(go)
    end)
    
    UIEventListener.Get(self.m_ExchangeBtn).onClick =  DelegateFactory.VoidDelegate(function (go)
        self:OnExchangeBtnClick(go)
    end)
    UIEventListener.Get(self.m_TryDressBtn).onClick =  DelegateFactory.VoidDelegate(function (go)
        self:OnTryDressBtnClick(go)
    end)
    UIEventListener.Get(self.m_PrizePoolBtn).onClick =  DelegateFactory.VoidDelegate(function (go)
        self:OnPrizePoolBtnClick(go)
    end)

    UIEventListener.Get(self.m_OnceBtn.gameObject).onClick =  DelegateFactory.VoidDelegate(function (go)
        self:FashionLotteryRequestDraw(1)
    end)
    UIEventListener.Get(self.m_TenBtn.gameObject).onClick =  DelegateFactory.VoidDelegate(function (go)
        self:FashionLotteryRequestDraw(10)
    end)

    UIEventListener.Get(self.m_LotteryTicketAddBtn).onClick =  DelegateFactory.VoidDelegate(function (go)
        self:OnLotteryTicketAddBtnClick(go)
    end)
end

function LuaCommonFashionLotteryView:Init()
    self.isClosing = false
    self.m_ActivitySetInfo = FashionLottery_Activity.GetData(LuaFashionLotteryMgr.m_ActivityId)
    self.m_WndInfo = FashionLottery_MainWndInfo.GetData(LuaFashionLotteryMgr.m_ActivityId)
    self:InitTicket()
    self:InitModelSet()
    Gac2Gas.CheckFashionLotteryEnsuranceInfo()
    self.transform.localScale = Vector3.one
    local closeSprite = self.m_CloseBtn:GetComponent(typeof(UISprite))
    closeSprite:ResetAndUpdateAnchors()
end

function LuaCommonFashionLotteryView:InitTicket()
    local lotteryTicketIcon = self.transform:Find("Top/LotteryTicket/Icon").gameObject
    local uniTicketIcon = self.transform:Find("Top/UniTicket/Icon").gameObject
    UIEventListener.Get(lotteryTicketIcon).onClick =  DelegateFactory.VoidDelegate(function (go)
        self:OnTicketIconClick(go, LuaFashionLotteryMgr.m_LotteryTicketId)
    end)
    UIEventListener.Get(uniTicketIcon).onClick =  DelegateFactory.VoidDelegate(function (go)
        self:OnTicketIconClick(go, LuaFashionLotteryMgr.m_UniTicketId)
    end)
    self:RefreshTicket()
end

function LuaCommonFashionLotteryView:RefreshTicket()
    local ticketCount = LuaFashionLotteryMgr:GetLotteryTicketCount()
    if CommonDefs.IS_VN_CLIENT then
        if ticketCount < 1 then
            self.m_OnceBtn.Text = LocalString.GetString("[c][9f7f8a]1[-][/c]")
            self.m_TenBtn.Text = LocalString.GetString("[c][9f7f8a]10[-][/c]")
        elseif ticketCount < 10 then
            self.m_OnceBtn.Text = LocalString.GetString("1")
            self.m_TenBtn.Text = LocalString.GetString("[c][9f7f8a]10[-][/c]")
        else
            self.m_OnceBtn.Text = LocalString.GetString("1")
            self.m_TenBtn.Text = LocalString.GetString("10")
        end
    else
        if ticketCount < 1 then
            self.m_OnceBtn.Text = LocalString.GetString("再抽1次          [c][9f7f8a]1[-][/c]")
            self.m_TenBtn.Text = LocalString.GetString("再抽10次          [c][9f7f8a]10[-][/c]")
        elseif ticketCount < 10 then
            self.m_OnceBtn.Text = LocalString.GetString("再抽1次          1")
            self.m_TenBtn.Text = LocalString.GetString("再抽10次          [c][9f7f8a]10[-][/c]")
        else
            self.m_OnceBtn.Text = LocalString.GetString("再抽1次          1")
            self.m_TenBtn.Text = LocalString.GetString("再抽10次          10")
        end 
    end
    self.m_LotteryTicketLabel.text = tostring(ticketCount)
    self.m_UniTicketLabel.text = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, LuaFashionLotteryMgr.m_UniTicketId)
end

function LuaCommonFashionLotteryView:InitModelSet()
    self.ModelRoot.gameObject:SetActive(true)
    self.m_ModelShowTimeList = FashionLottery_Settings.GetData().FashionShowTimeList
    self.m_ModelInfo = {}
    for i = 0, self.ModelRoot.transform.childCount - 1 do
        local data = {}
        data.Panel = self.ModelRoot.transform:GetChild(i):GetComponent(typeof(UIPanel))
        data.NameGo = data.Panel.transform:Find("Name").gameObject
        table.insert(self.m_ModelInfo, data)
    end
end

function LuaCommonFashionLotteryView:BeginShowModelTick()
    if self.m_ModelInfo and #self.m_ModelInfo > 0 then
        for i = 0, self.ModelRoot.transform.childCount - 1 do
            self.ModelRoot.transform:GetChild(i).gameObject:SetActive(true)
            self.ModelRoot.transform:GetChild(i).gameObject:GetComponent(typeof(UIPanel)).alpha = 0
        end
        if self.m_ShowModelTick ~= nil then
            UnRegisterTick(self.m_ShowModelTick)
        end
        self.m_ModelIndex = 1
        self.m_ModelInfo[1].NameGo:SetActive(true)
        self.m_ModelInfo[1].Panel.alpha = 1
        self.m_ShowModelTick = RegisterTickOnce(function ()
            self.ModelRoot:Play("jiuweihufashionlotterywnd_renqiehuan")
        end, 5000)
    end
end

function LuaCommonFashionLotteryView:StopShowModelTick()
    if self.m_ShowModelTick ~= nil then
        UnRegisterTick(self.m_ShowModelTick)
    end
    self.ModelRoot:Stop()
    if self.m_ModelInfo and #self.m_ModelInfo > 0 then
        -- 复原到第一个
        for i = 1, self.ModelRoot.transform.childCount - 1 do
            self.ModelRoot.transform:GetChild(i).gameObject:SetActive(false)
            self.ModelRoot.transform:GetChild(i).gameObject:GetComponent(typeof(UIPanel)).alpha = 0
        end
        self.m_ModelInfo[1].Panel.alpha = 1
        self.m_ModelInfo[1].NameGo:SetActive(false)
        self.m_ModelIndex = 1
    end
end

function LuaCommonFashionLotteryView:OnCheckFashionLotteryEnsuranceInfoResult(remainCount)
    self.m_MinimumLabel.text = remainCount
end

--@region UIEvent
function LuaCommonFashionLotteryView:OnCloseBtnClick(go)
    if not self.isClosing then
        -- print(self.isClosing)
        self.isClosing = true
        if LuaFashionLotteryMgr.m_OpenView == "LotteryView" then
            CUIManager.CloseUI(LuaFashionLotteryMgr.m_OpenWnd.MainWnd)
        else
            self.transform.parent.parent.gameObject:SetActive(false)
            g_ScriptEvent:BroadcastInLua("PlayFashionLotteryAnimation", "jiuweihufashionlotterywnd_show02", -1.0)
            Gac2Gas:CheckFashionLotteryDiscountInfo()
        end
    end
end

function LuaCommonFashionLotteryView:OnRuleBtnClick(go)
    g_MessageMgr:ShowMessage(self.m_WndInfo.LotteryRuleMsg)
end

function LuaCommonFashionLotteryView:OnExchangeBtnClick(go)
    CUIManager.ShowUI(LuaFashionLotteryMgr.m_OpenWnd.ExchangeWnd)
end

function LuaCommonFashionLotteryView:OnTryDressBtnClick(go)
    local index = 0
    local maxAlpha = 0
    for i = 1, #self.m_ModelInfo do
        -- 试穿当前模型透明度最大的那一套
        if self.m_ModelInfo[i].Panel.alpha > maxAlpha then
            index = i - 1
            maxAlpha = self.m_ModelInfo[i].Panel.alpha
        end
    end
    LuaAppearancePreviewMgr:ShowFashionSuitSubview(LuaFashionLotteryMgr.m_OpenWnd.TryDressSuitId[index])
end

function LuaCommonFashionLotteryView:OnPrizePoolBtnClick(go)
    CUIManager.ShowUI(LuaFashionLotteryMgr.m_OpenWnd.PrizePoolWnd)
end

function LuaCommonFashionLotteryView:FashionLotteryRequestDraw(drawCount)
    LuaFashionLotteryMgr:FashionLotteryRequestDraw(drawCount)
end

function LuaCommonFashionLotteryView:OnLotteryTicketAddBtnClick(go)
    CUIManager.ShowUI(LuaFashionLotteryMgr.m_OpenWnd.BuyTicketWnd)
end

function LuaCommonFashionLotteryView:OnTicketIconClick(go, itemId)
    CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, CItemInfoMgr.AlignType.Right, go.transform.position.x, go.transform.position.y, 0, 0)
end
--@endregion UIEvent

function LuaCommonFashionLotteryView:OnEnable()
    self:Init()
    self:BeginShowModelTick()
    self:RefreshTicket()
    self:OnCheckFashionLotteryEnsuranceInfoResult(LuaFashionLotteryMgr.m_LotteryEnsurance)
	g_ScriptEvent:AddListener("SendItem", self, "RefreshTicket")
	g_ScriptEvent:AddListener("SetItemAt", self, "RefreshTicket")
	g_ScriptEvent:AddListener("CheckFashionLotteryEnsuranceInfoResult", self, "OnCheckFashionLotteryEnsuranceInfoResult")
end

function LuaCommonFashionLotteryView:OnDisable()
	g_ScriptEvent:RemoveListener("SendItem", self, "RefreshTicket")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "RefreshTicket")
    g_ScriptEvent:RemoveListener("CheckFashionLotteryEnsuranceInfoResult", self, "OnCheckFashionLotteryEnsuranceInfoResult")
	self:StopShowModelTick()
end
