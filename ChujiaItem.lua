-- Auto Generated!!
local ChujiaItem = import "L10.UI.ChujiaItem"
local LocalString = import "LocalString"
local Object = import "System.Object"
ChujiaItem.m_Init_CS2LuaHook = function (this, playerId, playerName, portraitName, level, opName, silver) 
    this.PlayerId = playerId
    this.Name = playerName
    this.Silver = silver
    this.nameLabel.text = playerName
    this.portrait:LoadNPCPortrait(portraitName, false)
    this.levelLabel.text = System.String.Format(LocalString.GetString("{0}级"), level)
    this.opTextLabel.text = opName
end
ChujiaItem.m_OnOpButtonClick_CS2LuaHook = function (this, go) 
    if this.OnOpButtonClickDelegate ~= nil then
        GenericDelegateInvoke(this.OnOpButtonClickDelegate, Table2ArrayWithCount({this.PlayerId, this.Name, this.Silver}, 3, MakeArrayClass(Object)))
    end
end
