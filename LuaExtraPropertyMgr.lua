local EPlayerFightProp = import "L10.Game.EPlayerFightProp"
LuaExtraPropertyMgr = {}
LuaExtraPropertyMgr.m_WashExtraPropertyWndType = 1 --1代表默认消耗道具，2代表默认消耗经验和银两

function LuaExtraPropertyMgr:WashPermanentPropSuccess(consumeType, index, times, before_U, after_U)
    local before_list = MsgPackImpl.unpack(before_U)
    local after_list = MsgPackImpl.unpack(after_U)
    local beforeArray = {}
    local afterArray = {}
    for i = 1, 5 do 
        table.insert(beforeArray, before_list[EnumToInt(EPlayerFightProp.PermanentCor) + i - 1] + before_list[EnumToInt(EPlayerFightProp.RevisePermanentCor) + i - 1])
        table.insert(afterArray, after_list[EnumToInt(EPlayerFightProp.PermanentCor) + i - 1] + after_list[EnumToInt(EPlayerFightProp.RevisePermanentCor) + i - 1])
    end
    g_ScriptEvent:BroadcastInLua("OnWashPermanentPropSuccess",beforeArray,afterArray,consumeType, index, times)
end