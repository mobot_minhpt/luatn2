--local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CButton = import "L10.UI.CButton"
local UIVariableScrollView = import "L10.UI.UIVariableScrollView"
local Animator = import "UnityEngine.Animator"

LuaZYJYaoZhengPuWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZYJYaoZhengPuWnd, "TableView", "TableView", QnTableView)

--@endregion RegistChildComponent end

RegistClassMember(LuaZYJYaoZhengPuWnd, "m_cfgs")
RegistClassMember(LuaZYJYaoZhengPuWnd, "m_datas")

function LuaZYJYaoZhengPuWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaZYJYaoZhengPuWnd:OnEnable()
    g_ScriptEvent:AddListener("GuZhengSheetUnlockSuccess", self, "GuZhengSheetUnlockSuccess")
end

function LuaZYJYaoZhengPuWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GuZhengSheetUnlockSuccess", self, "GuZhengSheetUnlockSuccess")
end

function LuaZYJYaoZhengPuWnd:GuZhengSheetUnlockSuccess(id)
    if not self.m_datas then
        return
    end
    self.m_datas[id] = true

    for i = 1, #self.m_cfgs do
        if self.m_cfgs[i].Id == id then
            local item = self.TableView:GetItemAtRow(i - 1)
            self:PlayItemAnim(item, i, true)
            break
        end
    end
end

function LuaZYJYaoZhengPuWnd:InitData()
    
end

function LuaZYJYaoZhengPuWnd:Init()
    self.m_cfgs = {}
    ZhongYuanJie_YaoZhengDoc.Foreach(
        function(id, data)
            if data.Status == 0 then
                local cfg = {Id = id, MusicName = data.MusicName, Cost = data.Cost}
                table.insert(self.m_cfgs, cfg)
            end
        end
    )

    local fake = {Id = 0, MusicName = LocalString.GetString("敬请期待"), Cost = 0}
    table.insert(self.m_cfgs, fake)

    self.m_datas = {}
    
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp then
        --EnumPersistPlayDataKey.eGuZhengUnlockSheet = 221, -- 瑶筝清音解锁乐谱
        local data = CClientMainPlayer.Inst.PlayProp:GetPersistPlayDataWithKey(221)
        if data then
            local datas = g_MessagePack.unpack(data.Data)
            if datas then
                self.m_datas = datas
            end
        end
    end

    local initItem = function(item, row)
        self:InitItem(item, row + 1)
    end

    self.TableView.m_DataSource =
        DefaultTableViewDataSource.Create(
        function()
            return #self.m_cfgs
        end,
        initItem
    )
    self.TableView:ReloadData(false, false)
end

function LuaZYJYaoZhengPuWnd:Refresh()
    self.TableView:ReloadData(false, false)
end

function LuaZYJYaoZhengPuWnd:PlayItemAnim(item, index, playanim)
    local cfg = self.m_cfgs[index]
    local data = self.m_datas[cfg.Id]

    local trans = item.transform

    local fx = FindChild(trans, "vfx_cuifx_zong")
    fx.gameObject:SetActive(playanim)

    local btn = FindChildWithType(trans, "MusicDocItem/Btn", typeof(CButton))
    if cfg.Id == 0 then
        btn.gameObject:SetActive(false)
    else
        local unlocked = (data and data == true) --已经解锁

        local anim = trans.gameObject:GetComponent(typeof(Animator))
        anim.enabled = true
        if not unlocked then
            anim:Play("locked")
        elseif playanim then
            anim:Play("unlocking")
        else
            anim:Play("unlocked")
        end

        btn.gameObject:SetActive(true)
        UIEventListener.Get(btn.gameObject).onClick =
            DelegateFactory.VoidDelegate(
            function(go)
                if unlocked then
                    ZhongYuanJie2022Mgr.ReqPlayMusicDoc(InstrumentMgr.FurnitureID, cfg.Id)
                    CUIManager.CloseUI(CLuaUIResources.ZYJYaoZhengPuWnd)
                else
                    ZhongYuanJie2022Mgr.ShowItemConsumeWnd(cfg.Id)
                end
            end
        )
        if unlocked and playanim then
            RegisterTickOnce(
                function()
                    btn.Text = data and LocalString.GetString("演奏") or LocalString.GetString("解锁")
                end,
                200
            )
        else
            btn.Text = data and LocalString.GetString("演奏") or LocalString.GetString("解锁")
        end
    end
    local namelb = FindChildWithType(trans, "MusicDocItem/Upper/LbMusicName", typeof(UILabel))
    namelb.text = cfg.MusicName
end

function LuaZYJYaoZhengPuWnd:InitItem(item, index)
    self:PlayItemAnim(item, index, false)
end

--@region UIEvent

--@endregion UIEvent
