require("common/common_include")
local DelegateFactory = import "DelegateFactory"
local CommonDefs = import "L10.Game.CommonDefs"
local CUIManager = import "L10.UI.CUIManager"
local LocalString = import "LocalString"
local CUIResources = import "L10.UI.CUIResources"
local GameObject  = import "UnityEngine.GameObject"
local UIInput = import "UIInput"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
CLuaBindDashenWnd=class()
RegistChildComponent(CLuaBindDashenWnd, "BindButton", GameObject)
RegistChildComponent(CLuaBindDashenWnd, "InfoBtn", GameObject)
RegistChildComponent(CLuaBindDashenWnd, "CancelButton", GameObject)
RegistChildComponent(CLuaBindDashenWnd, "Input", GameObject)


function CLuaBindDashenWnd:Init()

	CommonDefs.AddOnClickListener(self.BindButton,DelegateFactory.Action_GameObject(function(go) self:OnConfirmClick() end), false)
	CommonDefs.AddOnClickListener(self.InfoBtn,DelegateFactory.Action_GameObject(function(go) self:OnHintClick() end), false)
	CommonDefs.AddOnClickListener(self.CancelButton,DelegateFactory.Action_GameObject(function(go) self:OnCancelClick() end), false)
	if CClientMainPlayer.Inst.PlayProp.DaShenAppData.BindedId ~= "" then 
		self.BindButton.transform.parent.gameObject:SetActive(false)
		local str = SafeStringFormat3(LocalString.GetString([=[已绑定大神账号#n%s#n是否解绑]=]),CClientMainPlayer.Inst.PlayProp.DaShenAppData.BindedId)
		MessageWndManager.ShowOKCancelMessage(str,DelegateFactory.Action(function() self:ConfirmUnBind() end),DelegateFactory.Action(function() CUIManager.CloseUI(CUIResources.BindDashenWnd) end),nil,nil,false)
	end
end


function CLuaBindDashenWnd:OnConfirmClick()
	local input = self.Input:GetComponent(typeof(UIInput))
	if input.text ~= nil then
		if input.text ~= "" then 
			Gac2Gas.ConfirmBindRoleForDaShen(input.text, "logToken")
			CUIManager.CloseUI(CUIResources.BindDashenWnd)
		end
	end
end

function CLuaBindDashenWnd:OnHintClick()
	g_MessageMgr:ShowMessage("BIND_DASHEN_HINT")
end
function CLuaBindDashenWnd:OnCancelClick()
	CUIManager.CloseUI(CUIResources.BindDashenWnd)
end
function CLuaBindDashenWnd:ConfirmUnBind()
	local str = LocalString.GetString([=[确认解除绑定吗]=])
	MessageWndManager.ShowOKCancelMessage(str,DelegateFactory.Action(function() self:DoUnBind() end),DelegateFactory.Action(function() CUIManager.CloseUI(CUIResources.BindDashenWnd) end),nil,nil,false)
end
function CLuaBindDashenWnd:DoUnBind()
	Gac2Gas.TryUnBindRoleForDaShen()
	CUIManager.CloseUI(CUIResources.BindDashenWnd)
end


return CLuaSchedulePicWnd
