local CFightingSpiritLiveWatchWndMgr = import "L10.UI.CFightingSpiritLiveWatchWndMgr"
local CFightingSpiritPlayerInfo = import "L10.UI.CFightingSpiritPlayerInfo"
local CFightingSpiritServerInfo = import "L10.UI.CFightingSpiritServerInfo"
local CFightingSpiritMatchRecordWnd = import "L10.UI.CFightingSpiritMatchRecordWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
local UInt32 = import "System.UInt32"
local EnumCommonForce = import "L10.UI.EnumCommonForce"
local Gas2Gac2 = import "L10.Game.Gas2Gac"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local CFightingSpiritMgr = import "L10.Game.CFightingSpiritMgr"
local CFightingSpiritCommitMaterialWnd = import "L10.UI.CFightingSpiritCommitMaterialWnd"
local CFightingSpiritSelfTeamWnd = import "L10.UI.CFightingSpiritSelfTeamWnd"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CGameVideoPlayer = import "L10.Game.CGameVideoPlayer"
local DateTime = import "System.DateTime"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CScene=import "L10.Game.CScene"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
CLuaFightingSpiritMgr = {}

CLuaFightingSpiritMgr.m_FavorTeamList = {}
CLuaFightingSpiritMgr.m_FavorRankList = {}
CLuaFightingSpiritMgr.m_FavorServerId = 0
CLuaFightingSpiritMgr.m_FavorInfoList = {}

CLuaFightingSpiritMgr.m_CurrentTeam = nil
CLuaFightingSpiritMgr.m_LeftNumOfWinPoints = nil
CLuaFightingSpiritMgr.m_RightNumOfWinPoints = nil
CLuaFightingSpiritMgr.m_DiekePossessingData = {}

--助威
CLuaFightingSpiritMgr.m_CheerIndex = -1

function CLuaFightingSpiritMgr.OpenOtherTeam(teanInfo)
	CLuaFightingSpiritMgr.m_CurrentTeam = teanInfo
	CUIManager.ShowUI(CLuaUIResources.FightingSpiritOtherTeamWnd)
end

function CLuaFightingSpiritMgr:GetVideoPlayerId(engineId)
	local co = CClientObjectMgr.Inst:GetObject(engineId)
	local playerId = 0
	if TypeIs(co, typeof(CGameVideoPlayer)) then
		playerId = TypeAs(co, typeof(CGameVideoPlayer)).PlayerId
	end
	return playerId
end

function CLuaFightingSpiritMgr:GetDataForFightingSpiritWatchTeamItem(info,playerinfos)
	if playerinfos.Count < 13 then return end
	self.m_DiekePossessingData[info.PlayerId] = playerinfos[12]
end

function CLuaFightingSpiritMgr:CacheFuncDieKeDoPossessByOther(engineId, fake_TargetEngineId, aggressivePossessCount, positivePossessCount, isEnemy)
	local playerId = self:GetVideoPlayerId(fake_TargetEngineId)
	if self.m_DiekePossessingData[playerId] then
		self.m_DiekePossessingData[playerId][4], self.m_DiekePossessingData[playerId][5] = aggressivePossessCount, positivePossessCount
	end
end

function CLuaFightingSpiritMgr:CacheFuncDieKeUnPossessByOther(fake_EngineId, fake_TargetEngineId, aggressivePossessCount, positivePossessCount)
 	local playerId = self:GetVideoPlayerId(fake_TargetEngineId)
 	if self.m_DiekePossessingData[playerId] then
 		self.m_DiekePossessingData[playerId][4], self.m_DiekePossessingData[playerId][5] = aggressivePossessCount, positivePossessCount
	end
end

CFightingSpiritLiveWatchWndMgr.m_hookOpenWnd = function (playerinfos, serverinfos)
	CFightingSpiritLiveWatchWndMgr.PlayerInfos = CreateFromClass(MakeGenericClass(List, CFightingSpiritPlayerInfo))
	CLuaFightingSpiritMgr.m_DiekePossessingData = {}
	do
		local i = 0
		while i < playerinfos.Count do
			local infodata = TypeAs(playerinfos[i], typeof(MakeGenericClass(List, Object)))
			local info = CFightingSpiritLiveWatchWndMgr.ToFightingSpiritPlayerInfo(infodata)
			CLuaFightingSpiritMgr:GetDataForFightingSpiritWatchTeamItem(info,infodata)
			CommonDefs.ListAdd(CFightingSpiritLiveWatchWndMgr.PlayerInfos, typeof(CFightingSpiritPlayerInfo), info)
			i = i + 1
		end
	end

	if serverinfos.Count >= 2 then
		local serverdata = TypeAs(serverinfos[0], typeof(MakeGenericClass(List, Object)))
		CFightingSpiritLiveWatchWndMgr.LeftForce2ServerName = CreateFromClass(CFightingSpiritServerInfo)
		CFightingSpiritLiveWatchWndMgr.LeftForce2ServerName.Force = CommonDefs.ConvertIntToEnum(typeof(EnumCommonForce), serverdata[0])
		CFightingSpiritLiveWatchWndMgr.LeftForce2ServerName.ServerName = ToStringWrap(serverdata[1])

		if serverdata.Count >= 3 then
			-- 兼容老版本
			CFightingSpiritLiveWatchWndMgr.LeftForce2ServerName.FutiLv = math.floor(tonumber(serverdata[2] or 0))
			if serverdata.Count >= 4 then
				local futiInfos = TypeAs(MsgPackImpl.unpack(TypeAs(serverdata[3], typeof(MakeArrayClass(System.Byte)))), typeof(MakeGenericClass(List, Object)))
				if futiInfos ~= nil and futiInfos.Count >= 1 then
					CFightingSpiritLiveWatchWndMgr.LeftForce2ServerName.ShenLiLv = math.floor(tonumber(futiInfos[0] or 0))
					CFightingSpiritLiveWatchWndMgr.LeftForce2ServerName.FutiInfos = CreateFromClass(MakeGenericClass(List, UInt32))
					do
						local i = 1
						while i < futiInfos.Count do
							CommonDefs.ListAdd(CFightingSpiritLiveWatchWndMgr.LeftForce2ServerName.FutiInfos, typeof(UInt32), math.floor(tonumber(futiInfos[i] or 0)))
							i = i + 1
						end
					end
				end
				if serverdata.Count >= 5 then
					CLuaFightingSpiritMgr.m_LeftNumOfWinPoints = tonumber(serverdata[4])
				end
			end
		else
			CFightingSpiritLiveWatchWndMgr.LeftForce2ServerName.FutiLv = - 1
		end


		serverdata = TypeAs(serverinfos[1], typeof(MakeGenericClass(List, Object)))
		CFightingSpiritLiveWatchWndMgr.RightForce2ServerName = CreateFromClass(CFightingSpiritServerInfo)
		CFightingSpiritLiveWatchWndMgr.RightForce2ServerName.Force = CommonDefs.ConvertIntToEnum(typeof(EnumCommonForce), serverdata[0])
		CFightingSpiritLiveWatchWndMgr.RightForce2ServerName.ServerName = ToStringWrap(serverdata[1])
		if serverdata.Count >= 3 then
			CFightingSpiritLiveWatchWndMgr.RightForce2ServerName.FutiLv = math.floor(tonumber(serverdata[2] or 0))
			if serverdata.Count >= 4 then
				local futiInfos = TypeAs(MsgPackImpl.unpack(TypeAs(serverdata[3], typeof(MakeArrayClass(System.Byte)))), typeof(MakeGenericClass(List, Object)))
				if futiInfos ~= nil and futiInfos.Count >= 1 then
					CFightingSpiritLiveWatchWndMgr.RightForce2ServerName.ShenLiLv = math.floor(tonumber(futiInfos[0] or 0))
					CFightingSpiritLiveWatchWndMgr.RightForce2ServerName.FutiInfos = CreateFromClass(MakeGenericClass(List, UInt32))
					do
						local i = 1
						while i < futiInfos.Count do
							CommonDefs.ListAdd(CFightingSpiritLiveWatchWndMgr.RightForce2ServerName.FutiInfos, typeof(UInt32), math.floor(tonumber(futiInfos[i] or 0)))
							i = i + 1
						end
					end
				end
				if serverdata.Count >= 5 then
					CLuaFightingSpiritMgr.m_RightNumOfWinPoints = tonumber(serverdata[4])
				end
			end
		else
			CFightingSpiritLiveWatchWndMgr.RightForce2ServerName.FutiLv = - 1
		end
	end
	CFightingSpiritLiveWatchWndMgr.SetCommonUIVisible(false)
	CUIManager.ShowUI(CUIResources.FightingSpiritLiveWatchWnd)
	if CLuaFightingSpiritMgr.m_RightNumOfWinPoints and CLuaFightingSpiritMgr.m_LeftNumOfWinPoints and CUIManager.IsLoaded(CUIResources.FightingSpiritLiveWatchWnd) then
		CLuaFightingSpiritMgr:CheckLeftRightWinPoint()
		g_ScriptEvent:BroadcastInLua("UpdateWinPoints")
	end
end
function CLuaFightingSpiritMgr:CheckLeftRightWinPoint()
	-- openWnd时服务器发来的数据是serverinfos[0] 守方，serverinfos[1] 攻方，全民pk这里左右反了
	if CQuanMinPKMgr.Inst.IsInQuanMinPKWatch  then
		local defendPoint = CLuaFightingSpiritMgr.m_LeftNumOfWinPoints
		local attackPoint = CLuaFightingSpiritMgr.m_RightNumOfWinPoints
		if CFightingSpiritLiveWatchWndMgr.LeftForce2ServerName.Force ~= EnumCommonForce.eAttack then
			CLuaFightingSpiritMgr.m_LeftNumOfWinPoints = attackPoint
			CLuaFightingSpiritMgr.m_RightNumOfWinPoints = defendPoint
		end
	end
end
----- 斗魂坛迭代相关
-- 打开界面相关
CLuaFightingSpiritMgr.m_SkillAppearancePerview = false
-- 1-3
CLuaFightingSpiritMgr.m_DouHunWndPageIndex = 0
-- 0-2
CLuaFightingSpiritMgr.m_RuleWndPageIndex = 0
-- 1-2
CLuaFightingSpiritMgr.m_GambleWndPageIndex = 0
CLuaFightingSpiritMgr.m_WildCardGambleOpen = false
CLuaFightingSpiritMgr.m_MainRaceGambleOpen = false
-- 斗魂竞猜奖励
CLuaFightingSpiritMgr.m_GambleRewardSilver = 0
CLuaFightingSpiritMgr.m_Des = 0
CLuaFightingSpiritMgr.m_Multi = 0

function CLuaFightingSpiritMgr:ClearOpenWndParam()
	self.m_SkillAppearancePerview = false
	self.m_DouHunWndPageIndex = 0
	self.m_RuleWndPageIndex = 0
	self.m_GambleWndPageIndex = 0
end

function CLuaFightingSpiritMgr:OpenGambleRewardWnd(silver, des, multi)
	self.m_GambleRewardSilver = silver
	self.m_Des = des
	self.m_Multi = multi
	if not CUIManager.IsLoaded(CLuaUIResources.FightingSpiritGambleResultWnd) then
		CUIManager.ShowUI(CLuaUIResources.FightingSpiritGambleResultWnd)
	end
	g_ScriptEvent:BroadcastInLua("OpenGambleRewardWnd", silver, des, multi)
end

function CLuaFightingSpiritMgr:OpenGambleWnd(gamlePageIndex)
	if CommonDefs.IS_CN_CLIENT then
		self.m_GambleWndPageIndex = gamlePageIndex
		Gac2Gas.OpenDouHunCrossGambleWnd()
	end
end

EnumDouHunGambleStage = {
	eNotOpen = 1,
	eWaika = 2,
	eMain = 3,
	eReward = 4,
	eEnd = 5,
}

function CLuaFightingSpiritMgr:CanOpenGambleWnd(isWenDa)
	if CLuaFightingSpiritMgr:HasGambleEnd() then
		g_MessageMgr:ShowMessage("DOU_HUN_CROSS_GAMBLE_CLOSED")
	elseif isWenDa then
		if CLuaFightingSpiritMgr:HasWenDaOpen() then
			return true
		else
			self:ShowGambleNotOpen(false)
			return false
		end
	else
		if CLuaFightingSpiritMgr:HasGambleOpen() then
			return true
		else
			self:ShowGambleNotOpen(true)
			return false
		end
	end
end

function CLuaFightingSpiritMgr:TryToOpenGambleWnd(gambleStage)
	-- 因为竞猜赛制变更的原因 gambleStage暂时不用了
	CLuaFightingSpiritMgr.m_MainRaceGambleOpen = CLuaFightingSpiritMgr:HasGambleOpen()
	CLuaFightingSpiritMgr.m_WildCardGambleOpen = CLuaFightingSpiritMgr:HasWenDaOpen()

	if CClientMainPlayer.Inst and not CClientMainPlayer.Inst.IsCrossServerPlayer then
		if self.m_GambleWndPageIndex == 2 and self:CanOpenGambleWnd(false) then
			-- 主赛事
			CUIManager.ShowUI(CLuaUIResources.FightingSpiritGambleWnd)
		elseif self.m_GambleWndPageIndex == 1 and self:CanOpenGambleWnd(true) then
			CUIManager.ShowUI(CLuaUIResources.FightingSpiritGambleWnd)
		end
	else
		g_MessageMgr:ShowMessage("CROSS_SERVER_NOT_USE_THE_FEATURE")
	end
end

function CLuaFightingSpiritMgr:ShowGambleNotOpen(isMainRace)
	local setting = DouHunCross_Setting.GetData()
	if isMainRace then
		g_MessageMgr:ShowMessage("DouHunMainBetNotStart", setting.MainBetStartTime)
	else
		g_MessageMgr:ShowMessage("DouHunWaiKaBetNotStart", setting.WaiKaBetStartTime)
	end
end

function CLuaFightingSpiritMgr:GetMyDouHunLevel()
	if CClientMainPlayer.Inst then
		local info = CFightingSpiritMgr.Instance:GetServerInfoFromID(CClientMainPlayer.Inst:GetMyServerId())
		if info then
			return info.Level
		end
	end
	return 0
end

-- 本服对阵情况
function Gas2Gac.QueryLocalDouHunPlayWatchInfoResult(chlgPlayMemberInfo_U, matchInfo_U)
	local memberData = MsgPackImpl.unpack(chlgPlayMemberInfo_U)
	local matchData = g_MessagePack.unpack(matchInfo_U)
	print("QueryLocalDouHunPlayWatchInfoResult")
	g_ScriptEvent:BroadcastInLua("QueryLocalDouHunPlayWatchInfoResult", memberData, matchData)
end

-- 请求打开资格界面
function Gas2Gac.QueryDouHunChlgTeamVoteInfoResultEnd(voteTeamId, selfTeamId, playStage, qualifiedGroupId_U)
	CFightingSpiritMgr.Instance.VotedTeamID = voteTeamId
    CFightingSpiritMgr.Instance.SelfTeamID = selfTeamId
	CFightingSpiritMgr.Instance.VoteStage = playStage

	local groupList = g_MessagePack.unpack(qualifiedGroupId_U)

	local dataList = CreateFromClass(MakeGenericClass(List, String))
    for i=1, #groupList do
        CommonDefs.ListAdd(dataList, typeof(String), groupList[i])
    end
	CFightingSpiritMgr.Instance.QualifiedTeamList = dataList
	CFightingSpiritMgr.Instance.IsVoting = playStage == EnumDouHunLocalStage.eVote

	CUIManager.ShowUI(CUIResources.FightingSpiritTeamVoteWnd)
end

-- 规则
Gas2Gac.OpenDouHunRuleWnd = function()
	CLuaFightingSpiritMgr.m_RuleWndPageIndex = 0
	CUIManager.ShowUI(CLuaUIResources.FightingSpiritRuleWnd)
end

-- 赛程
Gas2Gac.OpenDouHunScheduleWnd = function()
	CLuaFightingSpiritMgr.m_RuleWndPageIndex = 1
	CUIManager.ShowUI(CLuaUIResources.FightingSpiritRuleWnd)
end

-- 世界观
Gas2Gac.OpenDouHunBackgroundWnd = function()
	CLuaFightingSpiritMgr.m_RuleWndPageIndex = 2
	CUIManager.ShowUI(CLuaUIResources.FightingSpiritRuleWnd)
end

-- 斗魂进度
Gas2Gac.OpenDouHunTrainInfoWnd = function()
	CLuaFightingSpiritMgr.m_DouHunWndPageIndex = 1
	CUIManager.ShowUI(CLuaUIResources.FightingSpiritDouHunWnd)
end

-- 战队界面 斗魂附体界面打开
CFightingSpiritSelfTeamWnd.m_hookOnFutiButtonClick = function()
	if not CFightingSpiritMgr.Instance.IsSetFutiStage then
		g_MessageMgr:ShowMessage("DOU_HUN_FU_TI_ENABLE_NOT_OPEN")
	end
	CLuaFightingSpiritMgr.m_DouHunWndPageIndex = 3
	CUIManager.ShowUI(CLuaUIResources.FightingSpiritDouHunWnd)
end

-- 斗魂提交
Gas2Gac.OpenDouHunSubmitItemWnd = function(curDailyScore, maxDailyScore)
	CFightingSpiritCommitMaterialWnd.currentScore = curDailyScore
	CFightingSpiritCommitMaterialWnd.dailyMaxScore = maxDailyScore

	EventManager.BroadcastInternalForLua(EnumEventType.FightingSpiritSubmitInfoReceive, {curDailyScore, maxDailyScore})

	if not CUIManager.IsLoaded(CLuaUIResources.FightingSpiritDouHunWnd) then
		CLuaFightingSpiritMgr.m_DouHunWndPageIndex = 2
		CUIManager.ShowUI(CLuaUIResources.FightingSpiritDouHunWnd)
	end
end

-- 斗魂附体
Gas2Gac.OpenDouHunFuTiInfoWnd = function()
	CLuaFightingSpiritMgr.m_DouHunWndPageIndex = 3
	CUIManager.ShowUI(CLuaUIResources.FightingSpiritDouHunWnd)
end

Gas2Gac.OpenCrossDouHunWatchWndResult = function(stage)
	CFightingSpiritMatchRecordWnd.CurrentStage = stage
	if not CLuaFightingSpiritMgr:HasDouHunEnd() then
		CUIManager.ShowUI("FightingSpiritMatchInfoWnd")
	else
		g_MessageMgr:ShowMessage("DOU_HUN_CROSS_WATCH_END")
	end
end

CLuaFightingSpiritMgr.GroupAppearanceInfo = nil
CLuaFightingSpiritMgr.LevelName = nil
CLuaFightingSpiritMgr.ServerName = nil
CLuaFightingSpiritMgr.ChampionID2Lv = nil

function Gas2Gac.QueryDouHunGroupAppearanceInfoResult(appearance_U)
    CLuaFightingSpiritMgr.GroupAppearanceInfo = MsgPackImpl.unpack(appearance_U)
	CUIManager.ShowUI(CLuaUIResources.FightingSpiritChampionPreviewWnd)
end

-- 检查问答是否开启
function CLuaFightingSpiritMgr:HasWenDaOpen()
	-- -- 当前时间
	local now = CServerTimeMgr.Inst:GetZone8Time()


	local firstData = DouHunCross_WenDa.GetData(1)
    local date = firstData.Date
	local year = math.floor(date/10000)
	local month = math.floor(math.floor(date%10000)/100)
	local day = math.floor(date%100)
	local startdate = CreateFromClass(DateTime, year, month, day, 12, 0, 0)

	local timeDiff = CommonDefs.op_Subtraction_DateTime_DateTime(startdate, now)
	if CommonDefs.op_GreaterThanOrEqual_DateTime_DateTime(now, startdate) then
		-- 已经开启
		return true, timeDiff
	end
	return false, timeDiff
end

-- 检查竞猜是否开启
function CLuaFightingSpiritMgr:HasGambleOpen()
	-- 海外版本不开放，时间不太好翻译，直接返回
	if not CommonDefs.IS_CN_CLIENT then return false, 0 end
	-- -- 当前时间
	local now = CServerTimeMgr.Inst:GetZone8Time()

	local setting = DouHunCross_Setting.GetData()
	local startTime = setting.MainBetStartTime
	local _, _, MM, dd, hh, mm = string.find(tostring(startTime), LocalString.GetString("(%d+)月(%d+)日 (%d+):(%d+)"))

	local startdate = CreateFromClass(DateTime, now.Year,  MM, dd, hh, mm, 0)
	local timeDiff = CommonDefs.op_Subtraction_DateTime_DateTime(startdate, now)
	if CommonDefs.op_GreaterThanOrEqual_DateTime_DateTime(now, startdate) then
		-- 已经开启
		return true, timeDiff
	end
	return false, timeDiff
end

-- 斗魂竞猜是否关闭
-- 用来判断界面是否可以打开
function CLuaFightingSpiritMgr:HasGambleEnd()
	-- -- 当前时间
	local now = CServerTimeMgr.Inst:GetZone8Time()

	local setting = DouHunCross_Setting.GetData()
	local endTime = setting.GambleFinishCron
	local _, _, mm, hh, dd, MM = string.find(endTime, "(%d+) (%d+) (%d+) (%d+) *")

	local endData = CreateFromClass(DateTime, now.Year, MM, dd, hh, mm, 0)
	local timeDiff = CommonDefs.op_Subtraction_DateTime_DateTime(endData, now)
	if CommonDefs.op_GreaterThanOrEqual_DateTime_DateTime(now, endData) then
		-- 已经关闭
		return true, timeDiff
	end
	return false, timeDiff
end

-- 斗魂竞猜结束时间
function CLuaFightingSpiritMgr:HasGambleFinish()
	-- 当前时间
	local now = CServerTimeMgr.Inst:GetZone8Time()

	local endTime = DouHunCross_RoundCron.GetData(11).CronStr
	local _, _, mm, hh, dd, MM = string.find(endTime, "(%d+) (%d+) (%d+) (%d+) *")
	local endData = CreateFromClass(DateTime, now.Year, MM, dd, hh, mm, 0)
	local timeDiff = CommonDefs.op_Subtraction_DateTime_DateTime(endData, now)

	if CommonDefs.op_GreaterThanOrEqual_DateTime_DateTime(now, endData) then
		-- 已经关闭
		return true, timeDiff
	end
	return false, timeDiff
end

-- 斗魂结束不给打开战况界面
function CLuaFightingSpiritMgr:HasDouHunEnd()
	-- 当前时间
	local now = CServerTimeMgr.Inst:GetZone8Time()

	local signdate = DouHunTan_Cron.GetData("SignUp").Value
	local _, _, mm, hh, dd, MM = string.find(signdate, "(%d+) (%d+) (%d+) (%d+) *")
	local startTime = CreateFromClass(DateTime, now.Year, MM, dd, hh, mm, 0)

	-- 斗魂已经开始报名
	if not CommonDefs.op_GreaterThanOrEqual_DateTime_DateTime(now, startTime) then
		return true
	end

	local endTime = DouHunTan_Setting.GetData().AchievementTimeout
	local YY, MM, dd, hh, mm = string.match(endTime, "(%d+)-(%d+)-(%d+) (%d+):(%d+)")
	local endData = CreateFromClass(DateTime, YY, MM, dd, hh, mm, 0)

	if CommonDefs.op_GreaterThanOrEqual_DateTime_DateTime(now, endData) then
		-- #210074 比赛结束后，在界面关闭前还是能显示出循环赛和淘汰赛的胜负数据（利用斗魂成就过期时间）
		return true
	end
	return false
end

function CLuaFightingSpiritMgr:GetMyLevel()
	if CClientMainPlayer.Inst then
		local serverId = CClientMainPlayer.Inst:GetMyServerId()
    	return self:GetSeverLevel(serverId)
	end
	return 1
end

function CLuaFightingSpiritMgr:GetSeverLevel(serverId)
	local data = DouHunCross_GuanWangServer.GetData(serverId)
	if data == nil then
		data = DouHunCross_YingHeServer.GetData(serverId)
	end

	if data then
		return math.ceil(data.Index/8)
	end
	return 1
end

-- 判断当前服务器是官网还是硬核
function CLuaFightingSpiritMgr:IsMyServerGuanwang()
	local isGuanWang = true
	if CClientMainPlayer.Inst then
		local serverId = CClientMainPlayer.Inst:GetMyServerId()
		if DouHunCross_YingHeServer.Exists(serverId) then
			isGuanWang = false
		end
	end
	return isGuanWang
end

-- 判断当前服务器是官网还是硬核
function CLuaFightingSpiritMgr:CanViewMatch()
	if CScene.MainScene then
        local sceneId = CScene.MainScene.SceneTemplateId
        if sceneId == 16000006 or sceneId == 16000003 then
            return true
        end
    end
	g_MessageMgr:ShowMessage("Need_Go_To_Main_City")
    return false
end

CLuaFightingSpiritMgr.m_ShowLeiGuView = false

function CLuaFightingSpiritMgr:GetMatchIndexDesc(matchIndex)
	local text = nil
    if matchIndex > 5 then
        -- 第几场
        local index = matchIndex % 100
        -- 第几轮
        local round = math.floor(matchIndex / 100) % 100
        -- 第几组
        local level = math.floor(matchIndex / 10000) % 10
        -- 第几届
        local nthMatch = math.floor(matchIndex / 100000)

		local nameList = DouHunCross_Setting.GetData().LevelName

		if nameList.Length >= level then
			text = nameList[level-1]
		end

        if round <= 5 then
			text = SafeStringFormat3(LocalString.GetString("%s循环赛第%d轮"), text, round)
		else
			local roundText = ""
			round = round - 5
			if round == 1 then
				roundText = LocalString.GetString("胜者组第一轮")
			elseif round == 2 and index <=2 then
				roundText = LocalString.GetString("胜者组第二轮")
			elseif round == 2 then
				roundText = LocalString.GetString("败者组第一轮")
			elseif round == 3 then
				roundText = LocalString.GetString("败者组第二轮")
			elseif round == 4 and index <= 1 then
				roundText = LocalString.GetString("胜者组决赛")
			elseif round == 4 then
				roundText = LocalString.GetString("败者组第三轮")
			elseif round == 5 then
				roundText = LocalString.GetString("败者组决赛")
			else
				roundText = LocalString.GetString("总决赛")
			end
			text = SafeStringFormat3(LocalString.GetString("%s%s"), text, roundText)
        end
    elseif matchIndex > 0 then
		local localDesc = {LocalString.GetString("服内选拔胜者组"), LocalString.GetString("服内选拔胜者组"), 
			LocalString.GetString("服内选拔乾资格战"),LocalString.GetString("服内选拔败者组"),LocalString.GetString("服内选拔坤资格战")}
		text = localDesc[matchIndex]
    end
	return text
end