local CUIFx = import "L10.UI.CUIFx"

local CUITexture = import "L10.UI.CUITexture"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UITexture = import "UITexture"
local TweenScale = import "TweenScale"
local UIPanel = import "UIPanel"
local Vector3 = import "UnityEngine.Vector3"

LuaMusicHitNode = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaMusicHitNode, "LongPress", "LongPress", GameObject)
RegistChildComponent(LuaMusicHitNode, "LongPreeProgress", "LongPreeProgress", UITexture)
RegistChildComponent(LuaMusicHitNode, "ScaleCircle", "ScaleCircle", TweenScale)
RegistChildComponent(LuaMusicHitNode, "HitBtn", "HitBtn", CUITexture)
RegistChildComponent(LuaMusicHitNode, "BtnFx", "BtnFx", CUIFx)

--@endregion RegistChildComponent end
RegistClassMember(LuaMusicHitNode, "m_BtnIdx")
RegistClassMember(LuaMusicHitNode, "m_LongBtnIdx")
RegistClassMember(LuaMusicHitNode, "m_LongPressTick")
RegistClassMember(LuaMusicHitNode, "m_IsLong")
RegistClassMember(LuaMusicHitNode, "m_Duration")
RegistClassMember(LuaMusicHitNode, "m_ScaleCircleTexture")
RegistClassMember(LuaMusicHitNode, "m_Perform")
RegistClassMember(LuaMusicHitNode, "m_LongProgressBg")
function LuaMusicHitNode:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.m_Panel = self.transform:GetComponent(typeof(UIPanel))
    --self.m_Panel.alpha = 0
    self.HitBtn.gameObject:SetActive(false)
    self.m_ScaleCircleTexture = self.ScaleCircle.transform:GetComponent(typeof(CUITexture))
    UIEventListener.Get(self.HitBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBtnClick()
	end)
    CommonDefs.AddOnPressListener(self.HitBtn.gameObject, DelegateFactory.Action_GameObject_bool(function (go, isPressed)
		self:OnBtnPressed(go, isPressed)
	end), true)

    self.m_Perfect = 0.5*(1/10)
    self.m_Good = 0.5*(4/10)
    self.m_Ok = 0.5*(8/10)
    self.m_Miss = 0.5*(10/10)
    self.m_TickInterbal = 10
    self.TetxureName = "purple"
    self.m_LongProgressBg = self.LongPress.transform:Find("bg"):GetComponent(typeof(CUITexture))
    self.LongPreeProgressTex = self.LongPreeProgress.transform:GetComponent(typeof(CUITexture))
end

function LuaMusicHitNode:Show(operator,duration,btnIdx,rhythmIdx)
    self:Hide()
    
    local rgbstr,tetxureName
    if operator == 0 then--单人
        rgbstr = "F8DDFF"
        tetxureName = "purple"
    elseif operator==1 then--队长
        rgbstr = "F8DDFF"
        tetxureName = "purple"
    else
        rgbstr = "B3DEFF"
        tetxureName = "blue"
    end
    self.m_BtnOperator = operator
    --self.LongPreeProgress.color = NGUIText.ParseColor24(rgbstr, 0)
    self.HitBtn:LoadMaterial(SafeStringFormat3("UI/Texture/Transparent/Material/musicplaywnd_dianji_%s_normal.mat",tetxureName))
    self.m_ScaleCircleTexture:LoadMaterial(SafeStringFormat3("UI/Texture/Transparent/Material/musicplaywnd_dianji_%s_guangquan.mat",tetxureName))
    self.m_LongProgressBg:LoadMaterial(SafeStringFormat3("UI/Texture/Transparent/Material/musicplaywnd_dianji_%s_highlight_02.mat",tetxureName))
    self.LongPreeProgressTex:LoadMaterial(SafeStringFormat3("UI/Texture/Transparent/Material/musicplaywnd_dianji_%s_highlight_02.mat",tetxureName))
    self.TetxureName = tetxureName

    self.m_Duration = duration
    self.HitBtn.gameObject:SetActive(true)
    self.m_BtnIdx = btnIdx
    self.m_LongBtnIdx = btnIdx + 100
    self.m_RhythmIdx = rhythmIdx
    --print("self.m_RhythmIdx",self.m_RhythmIdx)

    if duration == 0 then
        self.m_IsLong = false
        self:InitShortButton(operator)
    else
        self.m_IsLong = true
        self:InitLongButton(operator,duration)
    end
    self.ScaleCircle.enabled = true
    CommonDefs.ListClear(self.ScaleCircle.onFinished)
    self.ScaleCircle:SetOnFinished(DelegateFactory.Callback(function ()
        if not self.m_IsLong then
            self:Hide()
            if self:CheakOperatorCanClick() then
                Gac2Gas.SongPlayPressButtom(self.m_RhythmIdx,self.m_BtnIdx,EnumButtonPressPerform.Ignore-1)
            end
        else
            --如果缩圈结束还没有点击长按，则不需要进行duration tick，视为miss
            if not self.m_IsLongPressing then
                self:Hide()
                if self:CheakOperatorCanClick() then
                    Gac2Gas.SongPlayPressButtom(self.m_RhythmIdx,self.m_LongBtnIdx,EnumButtonPressPerform.Ignore-1)
                end
            end
        end       
    end))
    if self.m_IsLong then
        self.m_LongBtnMaxShowTick = RegisterTickOnce(function()
            self:Hide()

            if self:CheakOperatorCanClick() then
                Gac2Gas.SongPlayPressButtom(self.m_RhythmIdx,self.m_LongBtnIdx,EnumButtonPressPerform.Miss-1)
            end
            
        end,(duration+2)*1000)
    end

    self.ScaleCircle:ResetToBeginning()
    self.ScaleCircle:PlayForward()
    
end
function LuaMusicHitNode:InitShortButton(operator)
    self.LongPress.gameObject:SetActive(false)
end

function LuaMusicHitNode:InitLongButton(operator,duration)
    self.LongPress.gameObject:SetActive(true)
    self.LongPreeProgress.fillAmount = 0
end

function LuaMusicHitNode:Hide()
    CommonDefs.ListClear(self.ScaleCircle.onFinished)

    self.HitBtn.gameObject:SetActive(false)
    self.ScaleCircle.enabled = false
    if self.m_LongPressTick then
        UnRegisterTick(self.m_LongPressTick)
        self.m_LongPressTick = nil
    end
    if self.m_LongBtnMaxShowTick then
        UnRegisterTick(self.m_LongBtnMaxShowTick)
        self.m_LongBtnMaxShowTick = nil
    end
    self.ScaleCircle.from = Vector3(1,1,1)
    self.ScaleCircle.to = Vector3(0.1,0.1,1)
    self.ScaleCircle.duration = 1.8
    self.m_IsLongPressing = false
    self.transform.localScale = Vector3(1,1,1)
end

function LuaMusicHitNode:ShowPerformEffect(perform)
    if perform == EnumButtonPressPerform.Perfect then
        self:Perfect()
    elseif perform == EnumButtonPressPerform.Good then
        self:Good()
    elseif perform == EnumButtonPressPerform.Ok then
        self:Ok()
    elseif perform == EnumButtonPressPerform.Miss then
        self:Miss()
    else
        self:Ignore()
    end
    self:Hide()
end

function LuaMusicHitNode:Perfect()
    --print("Perfect")
    self.BtnFx:DestroyFx()
    self.BtnFx:LoadFx("fx/ui/prefab/UI_yinyou_wanmei.prefab")
end
function LuaMusicHitNode:Good()
    --print("Good")
    self.BtnFx:DestroyFx()
    self.BtnFx:LoadFx("fx/ui/prefab/UI_yinyou_Normal.prefab")
end
function LuaMusicHitNode:Ok()
    --print("ok")
    self.BtnFx:DestroyFx()
    self.BtnFx:LoadFx("fx/ui/prefab/UI_yinyou_Normal.prefab")
end
function LuaMusicHitNode:Miss()
    --print("miss")
    self.BtnFx:DestroyFx()
    self.BtnFx:LoadFx("fx/ui/prefab/UI_yinyou_shibai.prefab")
end
function LuaMusicHitNode:Ignore()
    self.BtnFx:DestroyFx()
end

function LuaMusicHitNode:JudgePerform()
    local perform = EnumButtonPressPerform.Miss
    local scale = self.m_ScaleCircleTexture.transform.localScale.x
    local off = math.abs(scale - 0.5)
    if off <= self.m_Perfect then--完美
        perform = EnumButtonPressPerform.Perfect
    elseif off <= self.m_Good then
        perform = EnumButtonPressPerform.Good
    elseif off <= self.m_Ok then
        perform = EnumButtonPressPerform.Ok
    else--Miss
        perform = EnumButtonPressPerform.Miss
    end
    return perform
end

function LuaMusicHitNode:CheakOperatorCanClick()
    local res = false
    if self.m_BtnOperator == 0 then--单人
        res = true
    elseif self.m_BtnOperator == 1 then --队长
        res = LuaMeiXiangLouMgr.m_IsLeader
    else
        res = not LuaMeiXiangLouMgr.m_IsLeader
    end
    return res
end

function LuaMusicHitNode:OnBtnClick()
    if not self:CheakOperatorCanClick() then
        return
    end

    if not self.m_IsLong then
        self.HitBtn:LoadMaterial(SafeStringFormat3("UI/Texture/Transparent/Material/musicplaywnd_dianji_%s_highlight.mat",self.TetxureName))
        local perform = self:JudgePerform()
        self:Hide()
        Gac2Gas.SongPlayPressButtom(self.m_RhythmIdx,self.m_BtnIdx,perform-1)       
    end
end

function LuaMusicHitNode:OnBtnPressed(go, isPressed)
    if not self:CheakOperatorCanClick() then
        return
    end
    if not self.m_IsLong then
        self:OnBtnClick()
        return
    end
    local longBtnIdx = self.m_LongBtnIdx

    local perform = EnumButtonPressPerform.Miss
    if isPressed then
        self.transform.localScale = Vector3(1.6,1.6,1)
        self.HitBtn:LoadMaterial(SafeStringFormat3("UI/Texture/Transparent/Material/musicplaywnd_dianji_%s_highlight.mat",self.TetxureName))
        if self.m_LongPressTick then
            UnRegisterTick(self.m_LongPressTick)
            self.m_LongPressTick = nil
        end
        perform = self:JudgePerform()

        self.m_IsLongPressing = true
        local internal = 1 / (self.m_Duration*1000 / self.m_TickInterbal)
        self.m_LongPressTick = RegisterTick(function()
            self.LongPreeProgress.fillAmount = self.LongPreeProgress.fillAmount + internal
            if self.LongPreeProgress.fillAmount >= 1 then
                self:Hide()              
                if self:CheakOperatorCanClick() then
                    Gac2Gas.SongPlayPressButtom(self.m_RhythmIdx,longBtnIdx,perform-1)
                end               
            end
        end,self.m_TickInterbal)
    else
        self.m_IsLongPressing = false
        if self.m_LongPressTick then
            UnRegisterTick(self.m_LongPressTick)
            self.m_LongPressTick = nil
        end
        if self.LongPreeProgress.fillAmount < 1 then      
            self:Hide()           
            if self:CheakOperatorCanClick() then
                Gac2Gas.SongPlayPressButtom(self.m_RhythmIdx,longBtnIdx,EnumButtonPressPerform.Miss-1)
            end
            
        else
            self:Hide()
            if self:CheakOperatorCanClick() then
                Gac2Gas.SongPlayPressButtom(self.m_RhythmIdx,longBtnIdx,perform-1)
            end
        end
        
    end
end

function LuaMusicHitNode:OnDisable()
    self:Hide()
end

--@region UIEvent

--@endregion UIEvent

