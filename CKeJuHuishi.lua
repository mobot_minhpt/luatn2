-- Auto Generated!!
local Callback = import "EventDelegate+Callback"
local CCCChatMgr = import "L10.Game.CCCChatMgr"
local CKeJuHuishi = import "L10.UI.CKeJuHuishi"
local CKeJuMgr = import "L10.Game.CKeJuMgr"
local CKeJuQuestionTemplate = import "L10.UI.CKeJuQuestionTemplate"
local CKeJuRankTemplate = import "L10.UI.CKeJuRankTemplate"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local DelegateFactory = import "DelegateFactory"
local Ease = import "DG.Tweening.Ease"
local EnumEventType = import "EnumEventType"
local EnumKeJuStatus = import "L10.Game.EnumKeJuStatus"
local ETickType = import "L10.Engine.ETickType"
local EventDelegate = import "EventDelegate"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local KeJu_Setting = import "L10.Game.KeJu_Setting"
local L10 = import "L10"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local Object1 = import "UnityEngine.Object"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
CKeJuHuishi.m_Init_CS2LuaHook = function (this, questionTemplate) 
    this.scoreLabel.text = ""
    if CCCChatMgr.Inst.EnableKejuCC then
        this.live.gameObject:SetActive(true)
        this.live:Init()
    else
        this.live.gameObject:SetActive(false)
    end
    if CKeJuMgr.Inst.huishiValid then
        this.defaultChatLabel.text = LocalString.GetString("输入文字聊天")
        this.chatInput.defaultText = LocalString.GetString("输入文字聊天")
    else
        this.defaultChatLabel.text = g_MessageMgr:FormatMessage("KEJU_WEIGUAN_TIP")
        this.chatInput.defaultText = g_MessageMgr:FormatMessage("KEJU_WEIGUAN_TIP")
    end
    if CKeJuMgr.Inst.CurrentStatus ~= EnumKeJuStatus.huishiCountDown then
        this:InitHuishi(questionTemplate)
    else
        this:InitHuishiCountDown(questionTemplate)
    end
    --HMT屏蔽字幕输入(patch:403925)
    if CommonDefs.IS_HMT_CLIENT then 
        this.chatInput.gameObject:SetActive(false)
		this.sendButton.gameObject:SetActive(false)
    end
end
CKeJuHuishi.m_InitHuishi_CS2LuaHook = function (this, questionTemplate) 
    this.rankTitleGo:SetActive(true)
    this.progressBarGo:SetActive(true)
    this.instructionLabel.text = ""
    questionTemplate:SetActive(false)
    this.bulletTemplate:SetActive(false)
    Extensions.RemoveAllChildren(this.questionAnchor)
    local question = NGUITools.AddChild(this.questionAnchor.gameObject, questionTemplate)
    question.transform.localPosition = Vector3.zero
    question:SetActive(true)
    CommonDefs.GetComponent_GameObject_Type(question, typeof(CKeJuQuestionTemplate)):InitWithPlayOption(CKeJuMgr.Inst.bVoice, true)
    EventDelegate.Add(this.chatInput.onChange, MakeDelegateFromCSFunction(this.OnChatInputLabelChanged, Callback, this))

    this:InitRank()
    if this.cancel ~= nil then
        invoke(this.cancel)
    end
    this.countDown = CKeJuMgr.Inst.questionCountDown
    if this.countDown <= 1 then
        this.countdownLabel.text = LocalString.GetString("0秒后下一题")
        this.progressBar.value = 0
    else
        this.countdownLabel.text = System.String.Format(LocalString.GetString("{0}秒后下一题"), this.countDown)
        this.progressBar.value = this.countDown / CKeJuMgr.Inst.questionCountDown
    end
    this.cancel = L10.Engine.CTickMgr.Register(DelegateFactory.Action(function () 
        if this.countDown <= 1 then
            this.countdownLabel.text = LocalString.GetString("0秒后下一题")
            this.progressBar.value = 0
            invoke(this.cancel)
        else
            this.countDown = this.countDown - 1
            this.countdownLabel.text = System.String.Format(LocalString.GetString("{0}秒后下一题"), this.countDown)
            this.progressBar.value = this.countDown / CKeJuMgr.Inst.questionCountDown
        end
    end), 1000, ETickType.Loop)
    if CKeJuMgr.Inst.huishiValid then
        this.scoreLabel.text = System.String.Format(LocalString.GetString("[c][acf8ff]得分  [-][/c]{0}"), CKeJuMgr.Inst.playerScore)
    end
end
CKeJuHuishi.m_InitHuishiCountDown_CS2LuaHook = function (this, questionTemplate) 
    this.rankTitleGo:SetActive(false)
    this.progressBarGo:SetActive(false)
    questionTemplate:SetActive(false)
    this.bulletTemplate:SetActive(false)
    Extensions.RemoveAllChildren(this.questionAnchor)
    this.countdownLabel.text = ""
    EventDelegate.Add(this.chatInput.onChange, MakeDelegateFromCSFunction(this.OnChatInputLabelChanged, Callback, this))
    this:InitRank()
    this.instructionLabel.text = g_MessageMgr:FormatMessage("KEJU_HUISHI_PREPARE_TIP")
    if this.cancel ~= nil then
        invoke(this.cancel)
    end
end
CKeJuHuishi.m_DisableCountDown_CS2LuaHook = function (this) 
    if this.cancel ~= nil then
        invoke(this.cancel)
    end
    this.countdownLabel.text = ""
    this.progressBarGo:SetActive(false)
end
CKeJuHuishi.m_InitRank_CS2LuaHook = function (this) 

    this.playerTemplate:SetActive(false)
    Extensions.RemoveAllChildren(this.rankTable.transform)
    do
        local i = 0
        while i < CKeJuMgr.Inst.rankList.Count do
            local player = NGUITools.AddChild(this.rankTable.gameObject, this.playerTemplate)
            player:SetActive(true)
            CommonDefs.GetComponent_GameObject_Type(player, typeof(CKeJuRankTemplate)):Init(CKeJuMgr.Inst.rankList[i], (i % 2) == 1 and "common_textbg_02_dark" or "common_textbg_02_light")
            i = i + 1
        end
    end
    this.rankTable:Reposition()
    this.rankScrollview:ResetPosition()
end
CKeJuHuishi.m_OnDisable_CS2LuaHook = function (this) 
    LuaDanMuMgr:Clean()
    UIEventListener.Get(this.sendButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.sendButton).onClick, MakeDelegateFromCSFunction(this.OnSendButtonClicked, VoidDelegate, this), false)
    EventManager.RemoveListener(EnumEventType.KeJuBulletMsgSend, MakeDelegateFromCSFunction(this.OnRecvChatMsg, Action0, this))
    EventManager.RemoveListener(EnumEventType.KeJuRankInfoUpdate, MakeDelegateFromCSFunction(this.InitRank, Action0, this))
    EventManager.RemoveListener(EnumEventType.KeJuScoreUpdate, MakeDelegateFromCSFunction(this.OnScoreUpdate, Action0, this))

    if this.cancel ~= nil then
        invoke(this.cancel)
    end
end

--CKeJuHuishi.m_OnSendButtonClicked_CS2LuaHook = nil
CKeJuHuishi.m_OnSendButtonClicked_CS2LuaHook = function (this, go)
    if(not this.enableChatInput)then
        g_MessageMgr:ShowMessage("KeJu_HuiGu_Weiguan")
    else
        local forbid_type = CClientMainPlayer.Inst.PlayProp.ForbidSpeakType
        local expire_time = CClientMainPlayer.Inst.PlayProp.ForbidSpeakExpiredTime
        local forbidden = false
        if((forbid_type == 1) or (forbid_type == -1))then
            if(expire_time == -1)then -- 永久禁言
                forbidden = true
                g_MessageMgr:ShowMessage("CANNOT_TALK_EVER")
            else
                if(CServerTimeMgr.Inst.timeStamp > expire_time)then --已过期
                    CClientMainPlayer.Inst.PlayProp.ForbidSpeakType = 0
                    CClientMainPlayer.Inst.PlayProp.ForbidSpeakExpiredTime = 0

                else    --普通禁言



                    forbidden = true
                    g_MessageMgr:ShowMessage("SPEAK_BAOSHIJIN_GUIDE", (expire_time - CServerTimeMgr.Inst.timeStamp))
                end
            end
        end
        if(forbidden)then --被禁言

        else

            if(not System.String.IsNullOrEmpty(this.chatContent))then
                local chat_len = #this.chatInput.value

                -- for i=0, (#this.chatInput.value - 1), 1 do
                --     if(this.chatInput.value[i] >= 'a' and this.chatInput.value[i] <= 'z')then
                --         chat_len = chat_len + 1
                --     elseif(this.chatInput.value[i] >= 'A' and this.chatInput.value[i] <= 'Z')then
                --         chat_len = chat_len + 1
                --     elseif(this.chatInput.value[i] >= '0' and this.chatInput.value[i] <= '9')then
                --         chat_len = chat_len + 1
                --     else
                --         chat_len = chat_len + 3
                --     end
                -- end
                if(chat_len <= KeJu_Setting.GetData().DanMuLength)then
                    if(not CKeJuMgr.Inst.bulletCDing)then
                        local ret = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(this.chatContent, true)
                        this.chatContent = ret.msg
                        if(not System.String.IsNullOrEmpty(this.chatContent))then
                            Gac2Gas.SendKeJuDanMu(this.chatContent, ret.shouldBeIgnore)
                            CKeJuMgr.Inst:SendBullet()
                            this.chatInput.value = ""
                        end
                    else
                        g_MessageMgr:ShowMessage("KEJU_DANMU_CD")
                    end
                else
                    g_MessageMgr:ShowMessage("KEJU_DANMU_LENGTH")
                end
            end
        end
    end
end

CKeJuHuishi.m_OnRecvChatMsg_CS2LuaHook = function (this) 

    do
        local i = 0
        while i < CKeJuMgr.Inst.bulletMsgList.Count do
            local continue
            repeat
                local player_id = TypeAs(CKeJuMgr.Inst.bulletMsgList[i], typeof(System.String))
                local bullet = TypeAs(CKeJuMgr.Inst.bulletMsgList[i + 1], typeof(System.String))
                bullet = CWordFilterMgr.Inst:DoFilterOnReceiveWithoutVoiceCheck(0, bullet, nil, true)

                if bullet == nil then
                    continue = true
                    break
                end

                this:OnAddBullet(bullet)
                LuaDanMuMgr:SendDanMu(bullet, player_id)
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 2
        end
        LuaDanMuMgr:SyncDanMu()
    end
end
CKeJuHuishi.m_OnAddBullet_CS2LuaHook = function (this, content) 

    if System.String.IsNullOrEmpty(content) then
        return
    end
    local label = NGUITools.AddChild(this.bulletScreen.gameObject, this.bulletTemplate)
    label:SetActive(true)
    local bullet = CommonDefs.GetComponent_GameObject_Type(label, typeof(UILabel))
    bullet.text = content
    bullet.color = Color(UnityEngine_Random(0.5, 1), UnityEngine_Random(0.5, 1), UnityEngine_Random(0.5, 1))
    bullet.fontSize = UnityEngine_Random(38, 52)
    bullet.transform.localPosition = Vector3(math.floor(this.bulletScreen.width / 2), UnityEngine_Random(math.floor(- this.bulletScreen.height / 2), math.floor(this.bulletScreen.height / 2)), 0)
    CommonDefs.OnComplete_Tweener(CommonDefs.SetEase_Tweener(LuaTweenUtils.DOLocalMoveX(bullet.transform, math.floor(- this.bulletScreen.width / 2) - bullet.width, UnityEngine_Random(6, 12), false), Ease.Linear), DelegateFactory.TweenCallback(function () 
        label.transform.parent = nil
        Object1.Destroy(label)
    end))
end
