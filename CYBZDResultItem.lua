-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CYBZDResultItem = import "L10.UI.CYBZDResultItem"
local EnumClass = import "L10.Game.EnumClass"
local Profession = import "L10.Game.Profession"
CYBZDResultItem.m_Init_CS2LuaHook = function (this, info, row) 
    this.m_Info = info

    if row % 2 == 0 then
        this.m_BGSprite.alpha = 0
    else
        this.m_BGSprite.alpha = 1
    end

    local infoColor = Color.white
    if info.playerId == CClientMainPlayer.Inst.Id then
        infoColor = Color.green
    end

    this.m_ClsSprite.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), info.playerCls))

    this.m_PlayerName.text = info.playerName
    this.m_PlayerName.color = infoColor

    this.m_KillCountLabel.text = tostring(info.killCount)
    this.m_KillCountLabel.color = infoColor

    this.m_ScoreLabel.text = tostring(info.score)
    this.m_ScoreLabel.color = infoColor

    this.m_AssistLabel.text = tostring(info.assistCount)
    this.m_AssistLabel.color = infoColor

    if info.isEnd and info.isMVP == 1 then
        if info.isWinSide then
            this.m_BestAll:SetActive(true)
        else
            this.m_BestInSide:SetActive(true)
        end
    else
        this.m_BestAll:SetActive(false)
        this.m_BestInSide:SetActive(false)
    end
end
