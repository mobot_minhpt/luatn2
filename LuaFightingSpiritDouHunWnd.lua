local QnTabView = import "L10.UI.QnTabView"

local GameObject = import "UnityEngine.GameObject"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaFightingSpiritDouHunWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaFightingSpiritDouHunWnd, "FightingSpiritDouHunStateRoot", "FightingSpiritDouHunStateRoot", GameObject)
RegistChildComponent(LuaFightingSpiritDouHunWnd, "FightingSpiritDouHunFosterRoot", "FightingSpiritDouHunFosterRoot", GameObject)
RegistChildComponent(LuaFightingSpiritDouHunWnd, "FightingSpiritDouHunAppendageRoot", "FightingSpiritDouHunAppendageRoot", GameObject)
RegistChildComponent(LuaFightingSpiritDouHunWnd, "ShowArea", "ShowArea", QnTabView)

--@endregion RegistChildComponent end
RegistClassMember(LuaFightingSpiritDouHunWnd, "m_PageList")

function LuaFightingSpiritDouHunWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaFightingSpiritDouHunWnd:Init()
    self.m_PageList = {}
    table.insert(self.m_PageList, self.FightingSpiritDouHunStateRoot)
    table.insert(self.m_PageList, self.FightingSpiritDouHunFosterRoot)
    table.insert(self.m_PageList, self.FightingSpiritDouHunAppendageRoot)

    -- 打开特定页面
    if CLuaFightingSpiritMgr.m_DouHunWndPageIndex then
        self.ShowArea:ChangeTo(CLuaFightingSpiritMgr.m_DouHunWndPageIndex -1)
        self:ShowPage(CLuaFightingSpiritMgr.m_DouHunWndPageIndex, 0)
    end
end

function LuaFightingSpiritDouHunWnd:ShowPage(index, subIndex)
    self.ShowArea:ChangeTo(index-1)
    for i=1,3 do
        self.m_PageList[i]:SetActive(i == index)
        if index == 2 and i == 2 then
            if subIndex then
                local root =  self.m_PageList[i].transform:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
                root:ChangeTab(subIndex)
            end
        end
    end
end

function  LuaFightingSpiritDouHunWnd:OnEnable()
    g_ScriptEvent:AddListener("FightingSpiritDouHunWndShowPage",self,"ShowPage")
end

function  LuaFightingSpiritDouHunWnd:OnDisable()
    g_ScriptEvent:RemoveListener("FightingSpiritDouHunWndShowPage",self,"ShowPage")
    CLuaFightingSpiritMgr:ClearOpenWndParam()
end

--@region UIEvent

--@endregion UIEvent

