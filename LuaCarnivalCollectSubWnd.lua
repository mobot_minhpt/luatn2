local CItemMgr           = import "L10.Game.CItemMgr"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local AlignType          = import "L10.UI.CTooltip+AlignType"
local BoxCollider        = import "UnityEngine.BoxCollider"
local EnumItemPlace      = import "L10.Game.EnumItemPlace"
local ClientAction       = import "L10.UI.ClientAction"
local Item_Item          = import "L10.Game.Item_Item"

LuaCarnivalCollectSubWnd = class()

RegistClassMember(LuaCarnivalCollectSubWnd, "itemCount")
RegistClassMember(LuaCarnivalCollectSubWnd, "itemGot")
RegistClassMember(LuaCarnivalCollectSubWnd, "itemOrigin")
RegistClassMember(LuaCarnivalCollectSubWnd, "itemClone")

RegistClassMember(LuaCarnivalCollectSubWnd, "unFinishedGo2StampId")
RegistClassMember(LuaCarnivalCollectSubWnd, "stampId2Go")

function LuaCarnivalCollectSubWnd:Awake()
	local anchor = self.transform:Find("Anchor")

	local itemRoot = anchor:Find("Item")
	self.itemCount = itemRoot:Find("Panel/Count"):GetComponent(typeof(UILabel))
	self.itemGot = itemRoot:Find("Mask/Got").gameObject
	self.itemClone = itemRoot:Find("CloneItem").gameObject
	self.itemOrigin = itemRoot:Find("Mask/Icon").gameObject
	UIEventListener.Get(self.itemOrigin).onDragStart = LuaUtils.VoidDelegate(function (go)
		self:OnItemDragStart(go)
	end)

	UIEventListener.Get(self.itemOrigin).onDrag = LuaUtils.VectorDelegate(function (go, delta)
		self:OnItemDrag(go, delta)
	end)

	UIEventListener.Get(self.itemOrigin).onDragEnd = LuaUtils.VoidDelegate(function (go)
		self:OnItemDragEnd(go)
	end)

	UIEventListener.Get(self.itemGot).onClick = LuaUtils.VoidDelegate(function (go)
		self:OnItemGotClick()
	end)
	self.itemOrigin:SetActive(true)
	self.itemClone:SetActive(false)
end

function LuaCarnivalCollectSubWnd:OnEnable()
	g_ScriptEvent:AddListener("SendItem", self, "RefreshItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "RefreshItem")
	g_ScriptEvent:AddListener("SyncStampCollectProgress", self, "OnSyncStampCollectProgress")
end

function LuaCarnivalCollectSubWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SendItem", self, "RefreshItem")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "RefreshItem")
	g_ScriptEvent:RemoveListener("SyncStampCollectProgress", self, "OnSyncStampCollectProgress")
end

function LuaCarnivalCollectSubWnd:RefreshItem(args)
	self:UpdateItemCount()
end

function LuaCarnivalCollectSubWnd:OnSyncStampCollectProgress(stampId, newCount)
	self:UpdateStampStatus(stampId)
end


function LuaCarnivalCollectSubWnd:Init()
	self:InitScene()
	self:InitItem()
	self.transform:Find("Anchor/HintLabel"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("CARNIVAL_COLLECT_SUB_HINT")
	self:UpdateItemCount()
	self:InitGrid()

	self.unFinishedGo2StampId = {}
	for id, _ in pairs(self.stampId2Go) do
		self:UpdateStampStatus(id)
	end
end

function LuaCarnivalCollectSubWnd:InitScene()
	local locationId = LuaCarnivalCollectMgr.locationId
	local mapName = JiaNianHua_Location.GetData(locationId).PublicMapName

	local sceneRoot = self.transform:Find("Anchor/Scene")
	local childCount = sceneRoot.childCount
	for i = 1, childCount do
		local child = sceneRoot:GetChild(i - 1)
		child.gameObject:SetActive(child.name == mapName)
	end
end

function LuaCarnivalCollectSubWnd:InitItem()
	local itemId = JiaNianHua_Setting.GetData().FreeStampId
	local itemRoot = self.transform:Find("Anchor/Item")
	itemRoot:Find("Label"):GetComponent(typeof(UILabel)).text = Item_Item.GetData(itemId).Name
	itemRoot:Find("Label1"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("CARNIVAL_COLLECT_DRAG_ITEM_HINT")
end

function LuaCarnivalCollectSubWnd:UpdateItemCount()
	local itemId = JiaNianHua_Setting.GetData().FreeStampId
	local count = CItemMgr.Inst:GetItemCount(itemId)
	self.itemCount.text = count > 0 and count or ""
	self.itemGot:SetActive(count == 0)
	self.itemOrigin.transform:GetComponent(typeof(BoxCollider)).enabled = count > 0
end

function LuaCarnivalCollectSubWnd:InitGrid()
	local location2StampIds = LuaCarnivalCollectMgr:ParseLocation2StampIds()
	local stampIds = location2StampIds[LuaCarnivalCollectMgr.locationId]

	local grid = self.transform:Find("Anchor/Grid"):GetComponent(typeof(UIGrid))
	local template = self.transform:Find("Anchor/Template").gameObject
	template:SetActive(false)

	Extensions.RemoveAllChildren(grid.transform)
	self.stampId2Go = {}
	for _, id in ipairs(stampIds) do
		local child = NGUITools.AddChild(grid.gameObject, template)
		child:SetActive(true)

		local desc = JiaNianHua_Stamp.GetData(id).Description
		child.transform:Find("Finished/Desc"):GetComponent(typeof(UILabel)).text = desc
		child.transform:Find("UnFinished/Desc"):GetComponent(typeof(UILabel)).text = desc

		self.stampId2Go[id] = child
	end
	grid:Reposition()
end

function LuaCarnivalCollectSubWnd:UpdateStampStatus(id)
	local trans = self.stampId2Go[id].transform

	local progress = LuaCarnivalCollectMgr:GetStampProgress(id)
	local totalProgress = LuaCarnivalCollectMgr:GetStampTotalProgress(id)
	local isFinished = progress >= totalProgress
	trans:Find("Finished").gameObject:SetActive(isFinished)
	trans:Find("UnFinished").gameObject:SetActive(not isFinished)
	LuaUtils.SetLocalPositionZ(trans:Find("StampTex"), isFinished and 0 or -1)

	if not isFinished then
		trans:Find("UnFinished/Count"):GetComponent(typeof(UILabel)).text = progress
		local quantifier = JiaNianHua_Stamp.GetData(id).Quantifier
		trans:Find("UnFinished/Total"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("%d%s", totalProgress, quantifier)

		UIEventListener.Get(trans:Find("UnFinished/GoToButton").gameObject).onClick = LuaUtils.VoidDelegate(function (go)
			self:OnGoToButtonClick(id)
		end)
	else
		UIEventListener.Get(trans.gameObject).onClick = nil
	end
	self.unFinishedGo2StampId[trans.gameObject] = not isFinished and id
end



function LuaCarnivalCollectSubWnd:OnItemDragStart(go)
	self.itemClone:SetActive(true)
end

function LuaCarnivalCollectSubWnd:OnItemDrag(go, delta)
	local worldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition)
    local localPos = self.itemClone.transform.parent:InverseTransformPoint(worldPos)
    self.itemClone.transform.localPosition = localPos
end

function LuaCarnivalCollectSubWnd:OnItemDragEnd(go)
	self.itemClone:SetActive(false)

	local selectGo = CUICommonDef.SelectedUIWithRacast
	local id = self.unFinishedGo2StampId[selectGo]
	if id then
		g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("CARNIVALCOLLECT_USE_WANNENGZHANG", JiaNianHua_Stamp.GetData(id).Description), function()
			local itemId = JiaNianHua_Setting.GetData().FreeStampId
			local itemList = CItemMgr.Inst:GetPlayerItemsAtPlaceByTemplateId(EnumItemPlace.Bag, itemId)
			if itemList.Count > 0 then
				Gac2Gas.RequestUseFreeStamp(id, itemList[0].itemId, EnumItemPlace.Bag, itemList[0].pos)
			end
		end, nil, nil, nil, false)
	end
end

function LuaCarnivalCollectSubWnd:OnItemGotClick()
	local itemId = JiaNianHua_Setting.GetData().FreeStampId
	CItemAccessListMgr.Inst:ShowItemAccessInfo(itemId, false, self.itemGot.transform, AlignType.Right)
end

function LuaCarnivalCollectSubWnd:OnGoToButtonClick(id)
	local data = JiaNianHua_Stamp.GetData(id)
	local action = data.ClientAction
	if not String.IsNullOrEmpty(action) then
		ClientAction.DoAction(action)
	end

	if data.ClickCloseInterface == 1 then
		CUIManager.CloseUI(CLuaUIResources.CarnivalCollectSubWnd)
		CUIManager.CloseUI(CLuaUIResources.CarnivalCollectWnd)
		CUIManager.CloseUI(CLuaUIResources.Carnival2023MainWnd)
	end
end
