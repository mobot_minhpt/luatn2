-- Auto Generated!!
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local BodyItemInfo = import "L10.UI.CItemInfoMgr+BodyItemInfo"
local CCityWarMgr = import "L10.Game.CCityWarMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CEquipRemindInfo = import "L10.UI.CItemInfoMgr+CEquipRemindInfo"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CQishuFurnitureTemplate = import "L10.UI.CQishuFurnitureTemplate"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local CQueryIndicatorMgr = import "L10.UI.CQueryIndicatorMgr"
local CUIManager = import "L10.UI.CUIManager"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EventManager = import "EventManager"
local GameSetting_Common_Wapper = import "L10.Game.GameSetting_Common_Wapper"
local Int32 = import "System.Int32"
local Item_Item = import "L10.Game.Item_Item"
local L10 = import "L10"
local LinkItemInfo = import "L10.UI.CItemInfoMgr+LinkItemInfo"
local PackageItemInfo = import "L10.UI.CItemInfoMgr+PackageItemInfo"
local ShowType = import "L10.UI.CItemInfoMgr+ShowType"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local String = import "System.String"
local Vector2 = import "UnityEngine.Vector2"
local Vector3 = import "UnityEngine.Vector3"
local Wedding_Setting = import "L10.Game.Wedding_Setting"
local GameSetting_Client = import "L10.Game.GameSetting_Client"
local EnumItemFlag = import "L10.Game.EnumItemFlag"


CItemInfoMgr.m_OnPlayerLogin_CS2LuaHook = function (this) 
    if CItemInfoMgr.QuickUseItemIds ~= nil then
        CommonDefs.ListClear(CItemInfoMgr.QuickUseItemIds)
    end
    if CItemInfoMgr.QuickUseBookIds ~= nil then
        CommonDefs.ListClear(CItemInfoMgr.QuickUseBookIds)
    end
    if CItemInfoMgr.EquipRemindInfoList ~= nil then
        CommonDefs.ListClear(CItemInfoMgr.EquipRemindInfoList)
    end
end
CItemInfoMgr.m_ShowPackageItemInfo_CS2LuaHook = function (itemId, packagePos, dataSource, navDelegate) 
    CItemInfoMgr.showType = ShowType.Package
    CItemInfoMgr.packageItemInfo = CreateFromClass(PackageItemInfo, itemId, packagePos, dataSource, navDelegate)
    CItemInfoMgr.packageItemInfo.alignType = AlignType.ScreenLeft
    local item = L10.Game.CItemMgr.Inst:GetById(itemId)
    if item ~= nil then
        CItemInfoMgr.packageItemInfo.templateId = item.TemplateId
        if not item.IsEquip then
            CUIManager.ShowUI(CIndirectUIResources.ItemInfoWnd)
        else
            CUIManager.ShowUI(CIndirectUIResources.EquipInfoWnd)
        end
    end
end
CItemInfoMgr.m_ShowBodyItemInfo_CS2LuaHook = function (itemId, bodyPos, dataSource) 
    CItemInfoMgr.showType = ShowType.Body
    CItemInfoMgr.bodyItemInfo = CreateFromClass(BodyItemInfo, itemId, bodyPos, dataSource)
    CItemInfoMgr.bodyItemInfo.alignType = AlignType.ScreenRight
    local item = L10.Game.CItemMgr.Inst:GetById(itemId)
    if item ~= nil then
        CItemInfoMgr.bodyItemInfo.templateId = item.TemplateId
        if not item.IsEquip then
            CUIManager.ShowUI(CIndirectUIResources.ItemInfoWnd)
        else
            local equip = item.Equip
            if item:FlagIsSet(EnumItemFlag.BLOCKED) then
                -- 斗魂坛期间背饰失效的提示
                g_MessageMgr:ShowMessage("DouHun_BeiShi_Blocked_Tip")
            end
            CUIManager.ShowUI(CIndirectUIResources.EquipInfoWnd)
        end
    end
end
CItemInfoMgr.m_ShowLinkItemInfo_CCommonItem_Boolean_IItemActionsDataSource_AlignType_Single_Single_Single_Single_Single_CS2LuaHook = function (item, needCompare, dataSource, alignType, worldCenterX, worldCenterY, targetSizeX, targetSizeY, dragAmount) 
    CItemInfoMgr.showType = ShowType.Link
    CItemInfoMgr.linkItemInfo = LinkItemInfo(item, needCompare, dataSource)
    CItemInfoMgr.linkItemInfo.alignType = alignType
    CItemInfoMgr.linkItemInfo.targetSize = Vector2(targetSizeX, targetSizeY)
    CItemInfoMgr.linkItemInfo.targetCenterPos = Vector3(worldCenterX, worldCenterY)
    CItemInfoMgr.linkItemInfo.dragAmount = dragAmount
    if item ~= nil then
        if not item.IsEquip then
            CUIManager.ShowUI(CIndirectUIResources.ItemInfoWnd)
        else
            CUIManager.ShowUI(CIndirectUIResources.EquipInfoWnd)
        end
    end
end
CItemInfoMgr.m_ShowOtherPlayerItemInfo_CS2LuaHook = function (item, otherPlayerInfo, needCompare, dataSource, alignType, worldCenterX, worldCenterY, targetSizeX, targetSizeY) 
    CItemInfoMgr.showType = ShowType.Link
    CItemInfoMgr.linkItemInfo = LinkItemInfo(item, false, nil)
    CItemInfoMgr.linkItemInfo.alignType = alignType
    CItemInfoMgr.linkItemInfo.targetSize = Vector2(targetSizeX, targetSizeY)
    CItemInfoMgr.linkItemInfo.targetCenterPos = Vector3(worldCenterX, worldCenterY)
    CItemInfoMgr.linkItemInfo.otherPlayerInfo = otherPlayerInfo
    CItemInfoMgr.linkItemInfo.isOtherPlayerItem = true
    if item ~= nil then
        CUIManager.ShowUI(CIndirectUIResources.EquipInfoWnd)
    end
end
CItemInfoMgr.m_ShowLinkItemTemplateInfo_CS2LuaHook = function (templateId, isFake, dataSource, alignType, worldCenterX, worldCenterY, targetSizeX, targetSizeY) 
    CItemInfoMgr.showType = ShowType.Link
    CItemInfoMgr.linkItemInfo = LinkItemInfo(templateId, isFake, dataSource)
    CItemInfoMgr.linkItemInfo.alignType = alignType
    CItemInfoMgr.linkItemInfo.targetSize = Vector2(targetSizeX, targetSizeY)
    CItemInfoMgr.linkItemInfo.targetCenterPos = Vector3(worldCenterX, worldCenterY)
    if CItemMgr.Inst:GetItemTemplate(templateId) ~= nil then
        CUIManager.ShowUI(CIndirectUIResources.ItemInfoWnd)
    elseif CItemMgr.Inst:GetEquipTemplate(templateId) ~= nil then
        CUIManager.ShowUI(CIndirectUIResources.EquipInfoWnd)
    end
end
CItemInfoMgr.m_ShowLinkItemTemplateInfoWithBind_CS2LuaHook = function (templateId, isBind, isFake, dataSource, alignType, worldCenterX, worldCenterY, targetSizeX, targetSizeY) 
    CItemInfoMgr.showType = ShowType.Link
    CItemInfoMgr.linkItemInfo = LinkItemInfo(templateId, isFake, dataSource)
    CItemInfoMgr.linkItemInfo.alignType = alignType
    CItemInfoMgr.linkItemInfo.targetSize = Vector2(targetSizeX, targetSizeY)
    CItemInfoMgr.linkItemInfo.targetCenterPos = Vector3(worldCenterX, worldCenterY)
    CItemInfoMgr.linkItemInfo.isBind = isBind
    if CItemMgr.Inst:GetItemTemplate(templateId) ~= nil then
        CUIManager.ShowUI(CIndirectUIResources.ItemInfoWnd)
    elseif CItemMgr.Inst:GetEquipTemplate(templateId) ~= nil then
        CUIManager.ShowUI(CIndirectUIResources.EquipInfoWnd)
    end
end
CItemInfoMgr.m_ShowLinkItemTemplateInfoWithBindAndExtra_CS2LuaHook = function (templateId, isBind, extraUD, isFake, dataSource, alignType, worldCenterX, worldCenterY, targetSizeX, targetSizeY) 
    CItemInfoMgr.showType = ShowType.Link
    CItemInfoMgr.linkItemInfo = LinkItemInfo(templateId, isFake, dataSource)
    CItemInfoMgr.linkItemInfo.alignType = alignType
    CItemInfoMgr.linkItemInfo.targetSize = Vector2(targetSizeX, targetSizeY)
    CItemInfoMgr.linkItemInfo.targetCenterPos = Vector3(worldCenterX, worldCenterY)
    CItemInfoMgr.linkItemInfo.isBind = isBind
    CItemInfoMgr.linkItemExtraUD = extraUD
    if CItemMgr.Inst:GetItemTemplate(templateId) ~= nil then
        CUIManager.ShowUI(CIndirectUIResources.ItemInfoWnd)
    elseif CItemMgr.Inst:GetEquipTemplate(templateId) ~= nil then
        CUIManager.ShowUI(CIndirectUIResources.EquipInfoWnd)
    end
end
CItemInfoMgr.m_ShowLinkItemInfo_String_Boolean_IItemActionsDataSource_AlignType_Single_Single_Single_Single_CS2LuaHook = function (itemId, needCompare, dataSource, alignType, worldCenterX, worldCenterY, targetSizeX, targetSizeY) 
    CItemInfoMgr.LastNeedCompareVal = needCompare
    CItemInfoMgr.LastNeedDataSourceVal = dataSource
    if not System.String.IsNullOrEmpty(itemId) then
        if CItemMgr.Inst:ContainsItem(itemId) then
            local item = CItemMgr.Inst:GetById(itemId)
            CItemInfoMgr.ShowLinkItemInfo(CItemMgr.Inst:GetById(itemId), needCompare, dataSource, alignType, worldCenterX, worldCenterY, targetSizeX, targetSizeY, 0)
        else
            CQueryIndicatorMgr.Show(g_MessageMgr:FormatMessage("Query_Item_Hint"))
            CItemInfoMgr.ShowWeddingItem = false
            Gac2Gas.QueryItemInfo(itemId)
            --从服务器获取该item的信息
        end
    end
end
CItemInfoMgr.m_ShowGlobalLinkItemInfo_CS2LuaHook = function (itemId, needCompare, dataSource, alignType, worldCenterX, worldCenterY, targetSizeX, targetSizeY) 
    CItemInfoMgr.LastNeedCompareVal = needCompare
    CItemInfoMgr.LastNeedDataSourceVal = dataSource
    if not System.String.IsNullOrEmpty(itemId) then
        if CItemMgr.Inst:ContainsItem(itemId) then
            local item = CItemMgr.Inst:GetById(itemId)
            CItemInfoMgr.ShowLinkItemInfo(CItemMgr.Inst:GetById(itemId), needCompare, dataSource, alignType, worldCenterX, worldCenterY, targetSizeX, targetSizeY, 0)
        else
            CQueryIndicatorMgr.Show(g_MessageMgr:FormatMessage("Query_Item_Hint"))
            CItemInfoMgr.ShowWeddingItem = false
            Gac2Gas.GlobalQueryItemInfo(itemId)
            --从服务器获取该item的信息
        end
    end
end
CItemInfoMgr.m_ShowTempLinkItemInfo_CS2LuaHook = function (itemId) 
    CItemInfoMgr.LastNeedCompareVal = false
    CItemInfoMgr.LastNeedDataSourceVal = nil
    if not System.String.IsNullOrEmpty(itemId) then
        CQueryIndicatorMgr.Show(g_MessageMgr:FormatMessage("Query_Item_Hint"))
        CItemInfoMgr.ShowWeddingItem = false
        Gac2Gas.QueryTradeItem(itemId)
    end
end
CItemInfoMgr.m_ShowLinkWeddingItemInfo_CS2LuaHook = function (itemId) 
    if not System.String.IsNullOrEmpty(itemId) then
        if CItemMgr.Inst:ContainsItem(itemId) then
            local item = CItemMgr.Inst:GetById(itemId)
            if item.TemplateId == Wedding_Setting.GetData().BaZiCardItemId then
                LuaWeddingIterationMgr:OpenBaZiResultWnd(item)
            elseif item.TemplateId == Wedding_Setting.GetData().CertificationItemId then
                LuaWeddingIterationMgr:OpenCertificationWnd(item)
            end
        else
            CQueryIndicatorMgr.Show(g_MessageMgr:FormatMessage("Query_Item_Hint"))
            CItemInfoMgr.ShowWeddingItem = true
            Gac2Gas.QueryItemInfo(itemId)
            --从服务器获取该item的信息
        end
    end
end
CItemInfoMgr.m_ShowQianKunDaiItem_CS2LuaHook = function (itemId, needCompare, dataSource, alignType) 
    if not System.String.IsNullOrEmpty(itemId) then
        if CItemMgr.Inst:ContainsItem(itemId) then
            CItemInfoMgr.showType = ShowType.QianKunDai
            local item = CItemMgr.Inst:GetById(itemId)
            CItemInfoMgr.linkItemInfo = LinkItemInfo(item, needCompare, dataSource)
            CItemInfoMgr.linkItemInfo.alignType = alignType
            if item ~= nil then
                if not item.IsEquip then
                    CUIManager.ShowUI(CIndirectUIResources.ItemInfoWnd)
                else
                    CUIManager.ShowUI(CIndirectUIResources.EquipInfoWnd)
                end
            end
        end
    end
end
CItemInfoMgr.m_ShowLinkItemInfoByQuery_CS2LuaHook = function (item) 
    if CItemInfoMgr.ShowWeddingItem and item.TemplateId == Wedding_Setting.GetData().BaZiCardItemId then
        LuaWeddingIterationMgr:OpenBaZiResultWnd(item)
    elseif CItemInfoMgr.ShowWeddingItem and item.TemplateId == Wedding_Setting.GetData().CertificationItemId then
        LuaWeddingIterationMgr:OpenCertificationWnd(item)
    else
        CItemInfoMgr.ShowLinkItemInfo(item, CItemInfoMgr.LastNeedCompareVal, CItemInfoMgr.LastNeedDataSourceVal, AlignType.Default, 0, 0, 0, 0, 0)
    end
end
CItemInfoMgr.m_ShowSigninLinkItemTemplateInfo_CS2LuaHook = function (templateId, alignType, worldCenterX, worldCenterY, targetSizeX, targetSizeY) 
    CItemInfoMgr.showType = ShowType.Signin
    CItemInfoMgr.linkItemInfo = LinkItemInfo(templateId, false, nil)
    CItemInfoMgr.linkItemInfo.alignType = alignType
    CItemInfoMgr.linkItemInfo.targetSize = Vector2(targetSizeX, targetSizeY)
    CItemInfoMgr.linkItemInfo.targetCenterPos = Vector3(worldCenterX, worldCenterY)
    if CItemMgr.Inst:GetItemTemplate(templateId) ~= nil then
        CUIManager.ShowUI(CIndirectUIResources.ItemInfoWnd)
    end
end
CItemInfoMgr.m_ShowFurnitureItemInfo_CS2LuaHook = function (itemid, dataSource) 
    local item = CItemMgr.Inst:GetById(itemid)
    if item ~= nil then
        CQishuFurnitureTemplate.CurFurnitureItemId = itemid

        CItemInfoMgr.showType = ShowType.FurnitureStore
        CItemInfoMgr.linkItemInfo = LinkItemInfo(item, false, dataSource)
        CItemInfoMgr.linkItemInfo.alignType = AlignType.ScreenRight
        CUIManager.ShowUI(CIndirectUIResources.ItemInfoWnd)
    end
end
CItemInfoMgr.m_ShowMajiuMapiInfo_CS2LuaHook = function (zuoqiId, dataSource) 
    local majiuMapiInfo = nil
    CommonDefs.DictIterate(CZuoQiMgr.Inst.m_MajiuMapiInfo, DelegateFactory.Action_object_object(function (___key, ___value) 
        local v = {}
        v.Key = ___key
        v.Value = ___value
        if v.Value.ZuoQiId == zuoqiId then
            majiuMapiInfo = v.Value
            return
        end
    end))
    if majiuMapiInfo ~= nil then
        CZuoQiMgr.s_SelectMajiuCurMapi = zuoqiId
        CItemInfoMgr.showType = ShowType.MajiuMapi
        CItemInfoMgr.linkItemInfo = LinkItemInfo(0, false, dataSource)
        CItemInfoMgr.linkItemInfo.alignType = AlignType.ScreenRight
        CUIManager.ShowUI(CIndirectUIResources.ItemInfoWnd)
    end
end
CItemInfoMgr.m_ShowCityWarUnitInfo_CS2LuaHook = function (unitTemplateId, dataSource) 
    CCityWarMgr.Inst.CurShowUnitTemplateId = unitTemplateId
    CItemInfoMgr.showType = ShowType.CityWarUnit
    CItemInfoMgr.linkItemInfo = LinkItemInfo(unitTemplateId, false, dataSource)
    CItemInfoMgr.linkItemInfo.alignType = AlignType.ScreenRight
    CUIManager.ShowUI(CIndirectUIResources.ItemInfoWnd)
end
CItemInfoMgr.m_ShowRepositoryFurnitureInfo_CS2LuaHook = function (furTemplateId, dataSource) 
    CLuaFurnitureSmallTypeItem.CurShowFurnitureTemplateId = furTemplateId
    CItemInfoMgr.showType = ShowType.FurnitureRepository
    CItemInfoMgr.linkItemInfo = LinkItemInfo(furTemplateId, false, dataSource)
    CItemInfoMgr.linkItemInfo.alignType = AlignType.ScreenRight
    CUIManager.ShowUI(CIndirectUIResources.ItemInfoWnd)
end
CItemInfoMgr.m_ShowRepositoryChuanjiabaoInfo_CS2LuaHook = function (chuanjiabaoId, dataSource) 
    local item = CItemMgr.Inst:GetById(chuanjiabaoId)
    if item ~= nil then
        CLuaFurnitureSmallTypeItem.CurShowChuanjiabaoItemId = chuanjiabaoId
        CItemInfoMgr.showType = ShowType.ChuanjiabaoRepository
        CItemInfoMgr.linkItemInfo = LinkItemInfo(item, false, dataSource)
        CItemInfoMgr.linkItemInfo.alignType = AlignType.ScreenRight
        CUIManager.ShowUI(CIndirectUIResources.ItemInfoWnd)
    end
end
CItemInfoMgr.m_ShowQuickUseItemInfo_CS2LuaHook = function (itemId) 
    CItemInfoMgr.taskTriggeredTaskItemId = ""
    CItemInfoMgr.taskTriggeredTaskId = 0
    CItemInfoMgr.quickUseTemplateID = itemId
    CUIManager.ShowUI(CIndirectUIResources.ItemUsageWnd)
end
CItemInfoMgr.m_ShowQuickUseItem_CS2LuaHook = function (itemId)
    if CQuanMinPKMgr.Inst.m_IsQuanMinPKServer then
        return
    end

    local item = CItemMgr.Inst:GetById(itemId)
    if item and item:IsItemExpire() then
        return --过期的道具不显示快捷弹窗
    end
    if item.TemplateId == Constants.DunJiaTianShuTemplateId then
        if (CUIManager.IsLoaded("TianShu2023OpenWnd")) then
            return
        end
    end
    if (CUIManager.IsLoaded("Shuangshiyi2023LotteryWnd") or CUIManager.IsLoading(CLuaUIResources.Shuangshiyi2023LotteryWnd)) then
        return
    end

    CItemInfoMgr.usingItem = true
    local index = CommonDefs.ListIndexOf(CItemInfoMgr.QuickUseItemIds, itemId)
    if index >= 0 and index < CItemInfoMgr.QuickUseItemIds.Count then
        CommonDefs.ListRemoveAt(CItemInfoMgr.QuickUseItemIds, index)
    end
    CommonDefs.ListInsert(CItemInfoMgr.QuickUseItemIds, 0, typeof(String), itemId)
    CUIManager.ShowUI(CIndirectUIResources.ItemAvailableWnd)
end
CItemInfoMgr.m_ShowQuickUseBook_CS2LuaHook = function (itemId) 
    if CQuanMinPKMgr.Inst.m_IsQuanMinPKServer then
        return
    end

    CItemInfoMgr.usingItem = false
    local index = CommonDefs.ListIndexOf(CItemInfoMgr.QuickUseBookIds, itemId)
    if index >= 0 and index < CItemInfoMgr.QuickUseBookIds.Count then
        CommonDefs.ListRemoveAt(CItemInfoMgr.QuickUseBookIds, index)
    end
    CommonDefs.ListInsert(CItemInfoMgr.QuickUseBookIds, 0, typeof(String), itemId)
    CUIManager.ShowUI(CIndirectUIResources.ItemAvailableWnd)
end
CItemInfoMgr.m_EquipRemind_CS2LuaHook = function (pos, itemId) 
    if CQuanMinPKMgr.Inst.m_IsQuanMinPKServer then
        return
    end

	LuaItemInfoMgr:DoEquipRemind(pos, itemId)

    if not CUIManager.IsLoaded(CIndirectUIResources.EquipRemindWnd) then
        CUIManager.ShowUI(CIndirectUIResources.EquipRemindWnd)
    else
        EventManager.Broadcast(EnumEventType.EquipRemindUpdate)
    end
end
CItemInfoMgr.m_ShowLetterDisplayWnd_CS2LuaHook = function (place, itemId, pos) 
    CLuaLetterDisplayMgr.ShowItemLetterDisplayWnd(place, itemId, pos)
end
Gas2Gac.ItemAddRemind = function (isEquip, place, pos, itemId) 
    if CClientMainPlayer.Inst == nil then
        return
    end
    --if (!COpenEntryMgr.Inst.EnableShowQuickUseItemWnd) return;//给新手引导提供的接口，取值为false时不弹出快捷窗口
    if isEquip then
        L10.UI.CItemInfoMgr.EquipRemind(pos, itemId)
    else
        local Id = CClientMainPlayer.Inst.ItemProp:GetItemAt(CommonDefs.ConvertIntToEnum(typeof(EnumItemPlace), place), pos)
        local item = CItemMgr.Inst:GetById(Id)
        if item ~= nil and item.IsItem and item.MainPlayerIsFit and item.AvailableAmountLimit > 0 then
            local template = Item_Item.GetData(item.TemplateId)
            local adjustedGradeCheck = CLuaDesignMgr.Item_Item_GetAdjustedGradeCheck(template)
            if template ~= nil and CItemMgr.Inst:IsQuickUseItemType(template.Type) and not CItemMgr.Inst:IsQuickUseForbidden(item.TemplateId) and adjustedGradeCheck <= CClientMainPlayer.Inst.Level then
                L10.UI.CItemInfoMgr.ShowQuickUseItem(Id)
            elseif template ~= nil and (template.Type == EnumItemType_lua.Book or template.Type == EnumItemType_lua.Jueji) then
                local raceCheckList = InitializeListWithArray(CreateFromClass(MakeGenericClass(List, Int32)), Int32, template.RaceCheck)
                if CommonDefs.ListContains(raceCheckList, typeof(Int32), CClientMainPlayer.Inst.BasicProp.Class) then
                    local skillBooks = GameSetting_Common_Wapper.Inst.SkillCls2BookIdDict
                    CommonDefs.DictIterate(skillBooks, DelegateFactory.Action_object_object(function (___key, ___value) 
                        local pair = {}
                        pair.Key = ___key
                        pair.Value = ___value
                        if template.ID == pair.Value.Key then
                            local skill = Skill_AllSkills.GetData(pair.Key * 100 + 1)
                            if skill ~= nil and not CClientMainPlayer.Inst.SkillProp:IsOriginalSkillClsExist(pair.Key) and CClientMainPlayer.Inst.Level >= adjustedGradeCheck and LuaSkillMgr.PreSkillConditionIsFit(skill) then
                                L10.UI.CItemInfoMgr.ShowQuickUseBook(item.Id)
                            end
                        end
                    end))
                end
            end
        end
    end
end

LuaItemInfoMgr = class()
-- 评分高于40%，直接推荐
-- 品质、评分、等级差各计1分或-1分；0分时品质>评分>等级差
function LuaItemInfoMgr:IsBetterEquipRemind(item1, item2)
	if not item1 or not item2 then return end
	if not item1.IsEquip or not item2.IsEquip then return end

    local setting = GameSetting_Client.GetData()

	if item1.Equip.Score >= (1+setting.BETTER_EQUIP_REMIND_EQUIP_SCORE_RATE) * item2.Equip.Score then
		return true
	end

	local better = 0
	local colorBetter = 0
	if EnumToInt(item1.Equip.Color) > EnumToInt(item2.Equip.Color) then
		colorBetter = 1
	elseif EnumToInt(item1.Equip.Color) < EnumToInt(item2.Equip.Color) then
		colorBetter = -1
	end
	better = better + colorBetter

	local scoreBetter = 0
	if item1.Equip.Score > item2.Equip.Score then
		scoreBetter = 1
	elseif item1.Equip.Score < item2.Equip.Score then
		scoreBetter = -1
	end
	better = better + scoreBetter

	local gradeBetter = 0
	if item1.Equip.Grade - item2.Equip.Grade >= setting.BETTER_EQUIP_REMIND_EQUIP_GRADE_DIS then
		gradeBetter = 1
	elseif item2.Equip.Grade - item1.Equip.Grade >= setting.BETTER_EQUIP_REMIND_EQUIP_GRADE_DIS then
		gradeBetter = -1
	end
	better = better + gradeBetter

	if better == 0 then
		if colorBetter == 0 then
			if scoreBetter == 0 then
				return gradeBetter > 0
			else
				return scoreBetter > 0
			end
		else
			return colorBetter > 0
		end
	else
		return better > 0 
	end
end

function LuaItemInfoMgr:DoEquipRemind(pos, itemId)
	local item = CItemMgr.Inst:GetById(itemId)
	if not (item and item.IsEquip) then return end
	
	local equipData = EquipmentTemplate_Equip.GetData(item.TemplateId)
	
	if CItemInfoMgr.EquipRemindInfoList == nil then
        CItemInfoMgr.EquipRemindInfoList = CreateFromClass(MakeGenericClass(List, CEquipRemindInfo))
    end

    local info = CreateFromClass(CEquipRemindInfo)
    info.EquipId = itemId
    info.Pos = pos

	local find1, find2
	for i = 0, CItemInfoMgr.EquipRemindInfoList.Count - 1 do
		local info = CItemInfoMgr.EquipRemindInfoList[i]
		local existItem = CItemMgr.Inst:GetById(info.EquipId)
		if existItem and existItem.IsEquip then
			local existEquipData = EquipmentTemplate_Equip.GetData(existItem.TemplateId)
			if equipData.Type == existEquipData.Type then
				if not find1 then
					find1 = i
				else
					find2 = i
					break
				end
			end
		end
	end

	-- 列表里没有，直接加入
	if not find1 then
		CommonDefs.ListAdd(CItemInfoMgr.EquipRemindInfoList, typeof(CEquipRemindInfo), info)
		return
	end

	local info1 = CItemInfoMgr.EquipRemindInfoList[find1]
	local existItem1 = CItemMgr.Inst:GetById(info1.EquipId)

	if equipData.Type == EnumToInt(EnumBodyPosition_lua.Ring) or equipData.Type == EnumToInt(EnumBodyPosition_lua.Bracelet) then
		-- 戒指、手镯，保留较好的两个，较好的在前面(后显示)
		if not find2 then
			-- 有一个
			if self:IsBetterEquipRemind(item, existItem1) then
				CItemInfoMgr.EquipRemindInfoList[find1] = info
				CommonDefs.ListAdd(CItemInfoMgr.EquipRemindInfoList, typeof(CEquipRemindInfo), info1)
			else
				CommonDefs.ListAdd(CItemInfoMgr.EquipRemindInfoList, typeof(CEquipRemindInfo), info)
			end
		else
			-- 有两个
			local info2 = CItemInfoMgr.EquipRemindInfoList[find2]
			local existItem2 = CItemMgr.Inst:GetById(info2.EquipId)
			if self:IsBetterEquipRemind(item, existItem1) then
				CItemInfoMgr.EquipRemindInfoList[find1] = info
				CItemInfoMgr.EquipRemindInfoList[find2] = info1
			elseif self:IsBetterEquipRemind(item, existItem2) then
				CItemInfoMgr.EquipRemindInfoList[find2] = info
			end
		end
	else
		-- 其他装备，保留较好的一个
		if self:IsBetterEquipRemind(item, existItem1) then
			CItemInfoMgr.EquipRemindInfoList[find1] = info
		end
	end
end

--------------------
-- 通用的道具消耗确认窗
--------------------

LuaItemInfoMgr.m_ConsumeItemInfo = {}

function LuaItemInfoMgr:GetCurrentConsumeItemInfo()
    return self.m_ConsumeItemInfo
end

function LuaItemInfoMgr:ShowItemConsumeWndWithQuickAccess(templateId, count, desc, bShowCountInDesc, bCloseWhenClickOKButton, okText, okFunc, cancelFunc, bShowAccessWhenClickOKIfNotEnough)
    self:ShowItemConsumeWnd(templateId, count, desc, bShowCountInDesc, true, bCloseWhenClickOKButton, okText, LocalString.GetString("取消"), okFunc, cancelFunc, bShowAccessWhenClickOKIfNotEnough)
end

function LuaItemInfoMgr:ShowItemConsumeWndWithTip(templateId, count, desc, bShowCountInDesc, bCloseWhenClickOKButton, okText, okFunc, cancelFunc, bShowAccessWhenClickOKIfNotEnough)
    self:ShowItemConsumeWnd(templateId, count, desc, bShowCountInDesc, false, bCloseWhenClickOKButton, okText, LocalString.GetString("取消"), okFunc, cancelFunc, bShowAccessWhenClickOKIfNotEnough)
end

function LuaItemInfoMgr:ShowItemConsumeWnd(templateId, count, desc, bShowCountInDesc, bShowAccessWhenClickIcon, bCloseWhenClickOKButton, okText, cancelText, okFunc, cancelFunc, bShowAccessWhenClickOKIfNotEnough)
    self.m_ConsumeItemInfo.m_TemplateId = templateId --道具ID
    self.m_ConsumeItemInfo.m_ConsumeCount = count --需要消耗的数量
    self.m_ConsumeItemInfo.m_Desc = desc --描述文本,当需要动态显示消耗信息时，需要确保参数为C#参数序号方式标记
    self.m_ConsumeItemInfo.m_ShowCountInDesc = bShowCountInDesc --是否需要在描述文本中显示消耗信息，为true时desc中需要加一个%s
    self.m_ConsumeItemInfo.m_ShowAccessWhenClickIcon = bShowAccessWhenClickIcon --点击道具的行为，true为显示获取途径，false为打开道具tip
    self.m_ConsumeItemInfo.m_CloseWhenClickOKButton = bCloseWhenClickOKButton --点击确定后是否关闭窗口
    self.m_ConsumeItemInfo.m_OKText = okText --确定按钮文本
    self.m_ConsumeItemInfo.m_CancelText = cancelText and cancelText or LocalString.GetString("取消")--取消按钮文本
    self.m_ConsumeItemInfo.m_OKFunc = okFunc --确定按钮点击回调
    self.m_ConsumeItemInfo.m_CancelFunc = cancelFunc --取消按钮点击回调
    self.m_ConsumeItemInfo.m_ShowAccessWhenClickOkIfNotEnough = bShowAccessWhenClickOKIfNotEnough and true or false -- 东西不足时点击确认按键是否弹出获取途径
    CUIManager.ShowUI(CIndirectUIResources.ItemConsumeWnd)
end

function LuaItemInfoMgr:CloseItemConsumeWnd()
    CUIManager.CloseUI(CIndirectUIResources.ItemConsumeWnd)
    self:ClearData()
end

function LuaItemInfoMgr:ClearData()
    self.m_ConsumeItemInfo = {}
end
