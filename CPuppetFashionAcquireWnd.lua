-- Auto Generated!!
local CommonDefs = import "L10.Game.CommonDefs"
local CPuppetFashionAcquireWnd = import "L10.UI.CPuppetFashionAcquireWnd"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Fashion_Setting = import "L10.Game.Fashion_Setting"
local Item_Item = import "L10.Game.Item_Item"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CPuppetFashionAcquireWnd.m_Init_CS2LuaHook = function (this) 
    this.newFashionIconTexture.material = nil
    this.chargeFashionIconTexture.material = nil
    this.tabenDetailView:Init()
    local mallFashionId = System.UInt32.Parse(Fashion_Setting.GetData("Mall_New_Fashion_ItemId").Value)
    local chargeFashionId = System.UInt32.Parse(Fashion_Setting.GetData("Charge_New_Fashion_ItemId").Value)
    this.mallFashion = Item_Item.GetData(mallFashionId)
    this.chargeFashion = Item_Item.GetData(chargeFashionId)
    if this.mallFashion ~= nil then
        this.newFashionIconTexture:LoadMaterial(this.mallFashion.Icon)
    end
    if this.chargeFashion ~= nil then
        this.chargeFashionIconTexture:LoadMaterial(this.chargeFashion.Icon)
    end
    this.chargeFashionGo:SetActive(this.chargeFashion ~= nil)
end
CPuppetFashionAcquireWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.openMallButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.openMallButton).onClick, MakeDelegateFromCSFunction(this.OnOpenMallButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.chargeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.chargeButton).onClick, MakeDelegateFromCSFunction(this.OnChargeButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.mallFashionGo).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.mallFashionGo).onClick, MakeDelegateFromCSFunction(this.OnMallFashionClick, VoidDelegate, this), true)
    UIEventListener.Get(this.chargeFashionGo).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.chargeFashionGo).onClick, MakeDelegateFromCSFunction(this.OnChargeFashionClick, VoidDelegate, this), true)

    EventManager.AddListener(EnumEventType.OnAddFashion, MakeDelegateFromCSFunction(this.OnAddFashion, Action0, this))
end
CPuppetFashionAcquireWnd.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.openMallButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.openMallButton).onClick, MakeDelegateFromCSFunction(this.OnOpenMallButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.chargeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.chargeButton).onClick, MakeDelegateFromCSFunction(this.OnChargeButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.mallFashionGo).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.mallFashionGo).onClick, MakeDelegateFromCSFunction(this.OnMallFashionClick, VoidDelegate, this), false)
    UIEventListener.Get(this.chargeFashionGo).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.chargeFashionGo).onClick, MakeDelegateFromCSFunction(this.OnChargeFashionClick, VoidDelegate, this), false)

    EventManager.RemoveListener(EnumEventType.OnAddFashion, MakeDelegateFromCSFunction(this.OnAddFashion, Action0, this))
end
