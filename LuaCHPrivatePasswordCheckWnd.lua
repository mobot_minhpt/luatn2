local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaCHPrivatePasswordCheckWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistClassMember(LuaCHPrivatePasswordCheckWnd, "m_PasswordInput")
RegistClassMember(LuaCHPrivatePasswordCheckWnd, "m_NoPasswardTip")

RegistClassMember(LuaCHPrivatePasswordCheckWnd, "m_CancelBtn")
RegistClassMember(LuaCHPrivatePasswordCheckWnd, "m_SubmitBtn")

--@endregion RegistChildComponent end

function LuaCHPrivatePasswordCheckWnd:Awake()
    -- 私密房间密码部分
    self.m_PasswordInput = self.transform:Find("Anchor/Password/PasswordInput"):GetComponent(typeof(UIInput))
    self.m_NoPasswardTip = self.transform:Find("Anchor/Password/NoPasswardTip").gameObject
    self.m_CancelBtn = self.transform:Find("Anchor/CancelBtn").gameObject
    self.m_SubmitBtn = self.transform:Find("Anchor/SubmitBtn").gameObject
    CommonDefs.AddEventDelegate(self.m_PasswordInput.onChange, DelegateFactory.Action(function ()
        self:OnPasswordInputValueChanged()
    end))
    UIEventListener.Get(self.m_CancelBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnCancelButtonClick()
    end)
    UIEventListener.Get(self.m_SubmitBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnSubmitButtonClick()
    end)
end

function LuaCHPrivatePasswordCheckWnd:Init()
    self:OnPasswordInputValueChanged()
end

--@region UIEvent
function LuaCHPrivatePasswordCheckWnd:OnPasswordInputValueChanged()
    local str = self.m_PasswordInput.value
    self.m_NoPasswardTip:SetActive(System.String.IsNullOrEmpty(str))
    CUICommonDef.SetActive(self.m_SubmitBtn.gameObject, CommonDefs.StringLength(str) >= 6, true)
end

function LuaCHPrivatePasswordCheckWnd:OnCancelButtonClick()
    CUIManager.CloseUI(CLuaUIResources.CHPrivatePasswordCheckWnd)
end

function LuaCHPrivatePasswordCheckWnd:OnSubmitButtonClick()
    LuaClubHouseMgr:QueryEnterRoom(LuaClubHouseMgr.m_QueryEnterRoomId, self.m_PasswordInput.value, false)
    self.m_PasswordInput.value = ""
end
--@endregion UIEvent

