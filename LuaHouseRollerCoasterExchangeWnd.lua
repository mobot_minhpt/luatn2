local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local CClientHouseMgr=import "L10.Game.CClientHouseMgr"
local CClientFurnitureMgr = import "L10.Game.CClientFurnitureMgr"

LuaHouseRollerCoasterExchangeWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHouseRollerCoasterExchangeWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaHouseRollerCoasterExchangeWnd, "Button", "Button", GameObject)
RegistChildComponent(LuaHouseRollerCoasterExchangeWnd, "CountInput", "CountInput", QnAddSubAndInputButton)
RegistChildComponent(LuaHouseRollerCoasterExchangeWnd, "CostLabel", "CostLabel", UILabel)
RegistChildComponent(LuaHouseRollerCoasterExchangeWnd, "OwnLabel", "OwnLabel", UILabel)
RegistChildComponent(LuaHouseRollerCoasterExchangeWnd, "AddButton", "AddButton", GameObject)
RegistChildComponent(LuaHouseRollerCoasterExchangeWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaHouseRollerCoasterExchangeWnd, "Icon", "Icon", CUITexture)
RegistChildComponent(LuaHouseRollerCoasterExchangeWnd, "Toggle", "Toggle", UIToggle)
RegistChildComponent(LuaHouseRollerCoasterExchangeWnd, "DescLabel", "DescLabel", UILabel)
RegistChildComponent(LuaHouseRollerCoasterExchangeWnd, "FoodCost", "FoodCost", GameObject)
RegistChildComponent(LuaHouseRollerCoasterExchangeWnd, "CountLabel", "CountLabel", UILabel)
RegistChildComponent(LuaHouseRollerCoasterExchangeWnd, "YinLiangCost", "YinLiangCost", GameObject)
RegistChildComponent(LuaHouseRollerCoasterExchangeWnd, "ButtonLabel", "ButtonLabel", UILabel)
RegistChildComponent(LuaHouseRollerCoasterExchangeWnd, "LimitLabel", "LimitLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaHouseRollerCoasterExchangeWnd,"m_Keys")
RegistClassMember(LuaHouseRollerCoasterExchangeWnd,"m_MoneyCtrl")
RegistClassMember(LuaHouseRollerCoasterExchangeWnd,"m_LimitCount")

local s_SelectedTemplateId = 0
function LuaHouseRollerCoasterExchangeWnd.Show(templateId)
    s_SelectedTemplateId = templateId
    CUIManager.ShowUI(CLuaUIResources.HouseRollerCoasterExchangeWnd)
end

function LuaHouseRollerCoasterExchangeWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.m_LimitCount = 2
    
    UIEventListener.Get(self.Button).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickButton()
    end)
    UIEventListener.Get(self.AddButton).onClick = DelegateFactory.VoidDelegate(function(go)
        -- 选中五石仓
        CShopMallMgr.ShowLinyuShoppingMall(21003558)
    end)

    self.m_Keys = {}
    Zhuangshiwu_RollerCoaster.ForeachKey(function(key)
        if key>0 then
            table.insert(self.m_Keys,key)
        end
    end)

    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_Keys
        end,
        function(item,row)
            self:InitItem(item,self.m_Keys[row+1])
        end)

    self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function(index)
        self:OnSelectAtRow(index)
    end)

    self.CountInput.onValueChanged = DelegateFactory.Action_uint(function(value)
        local key = self.m_Keys[self.TableView.currentSelectRow+1]
        local data = Zhuangshiwu_RollerCoaster.GetData(key)
        self.CostLabel.text = tostring(data.Food*value)

        if self.m_MoneyCtrl then
            local cost = 0
            if data.LingYu>0 then
                cost = data.LingYu
            elseif data.Silver>0 then
                cost = data.Silver
            end
            self.m_MoneyCtrl:SetCost(cost * value)
        end
    end)

    EventDelegate.Add(self.Toggle.onChange,DelegateFactory.Callback(function () 
        self:RefreshToggle()
    end))
end

function LuaHouseRollerCoasterExchangeWnd:RefreshToggle()
    local key = self.m_Keys[self.TableView.currentSelectRow+1]
    local data = Zhuangshiwu_RollerCoaster.GetData(key)

    if data.Food==0 then
        if not self.Toggle.value then
            g_MessageMgr:ShowMessage("Exchange_Coaster_Not_Use_Food")
            self.Toggle.value = true
            return
        end
    end

    if self.Toggle.value then
        self.YinLiangCost:SetActive(true)
        self.FoodCost:SetActive(false)

        self.ButtonLabel.text = LocalString.GetString("购买")

        self.CountInput:SetMinMax(1,self.m_LimitCount,1)

        if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, 333) then
            local playData = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, 333)
            if playData.ExpiredTime > CServerTimeMgr.Inst.timeStamp and playData.Data and playData.Data.Data then
                local data = g_MessagePack.unpack(playData.Data.Data)
                local cnt = data[key] or 0
                
                local leftCnt = self.m_LimitCount-cnt
                if leftCnt>0 then
                    self.CountInput:SetMinMax(1,leftCnt,1)
                else
                    self.CountInput:SetMinMax(0,0,1)
                end
            end
        end

    else
        self.YinLiangCost:SetActive(false)
        self.FoodCost:SetActive(true)

        self.ButtonLabel.text = LocalString.GetString("兑换")

        self.CountInput:SetMinMax(1,99,1)
    end
    self.CountInput:OnValueChange()
end

function LuaHouseRollerCoasterExchangeWnd:OnClickButton()
    local key = self.m_Keys[self.TableView.currentSelectRow+1]
    local type = 1 

    local data = Zhuangshiwu_RollerCoaster.GetData(key)

    local houseGrade = CClientHouseMgr.Inst:GetCurHouseGrade()
    local zswData = Zhuangshiwu_Zhuangshiwu.GetData(key)
    if houseGrade < zswData.HouseGrade then
        g_MessageMgr:ShowMessage("Cant_Exchange_Coaster_House_Grade_Low")
        return
    end

    if self.Toggle.value or data.Food == 0 then
        if data.Silver>0 then
            type = 2
        elseif data.LingYu>0 then
            type = 3
        end
    end

    --检查限购
    if type>1 then
        if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, 333) then
            local playData = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, 333)
            if playData.ExpiredTime > CServerTimeMgr.Inst.timeStamp and playData.Data and playData.Data.Data then
                local data = g_MessagePack.unpack(playData.Data.Data)
                local cnt = data[key] or 0
                if cnt>=self.m_LimitCount then
                    g_MessageMgr:ShowMessage("HOUSE_COASTER_BUY_WEEK_LIMIT")
                    return
                end
            end
        end
    end

    local cost = 0
    local icon = nil
    if type==1 then
        local food = CClientHouseMgr.Inst.mConstructProp.Food
        cost = data.Food*self.CountInput:GetValue()
        if cost>food then
            g_MessageMgr:ShowMessage("HOUSE_FOOD_NOT_ENOUGH")
            return
        end
    elseif type==2 then
        icon = "#287"
        local yinliang = CClientMainPlayer.Inst.Silver
        cost = data.Silver*self.CountInput:GetValue()
        if cost>yinliang then
            g_MessageMgr:ShowMessage("SILVER_NOT_ENOUGH")
            return
        end
    elseif type==3 then
        icon = "#285"
        local lingyu = CClientMainPlayer.Inst.Jade + CClientMainPlayer.Inst.BindJade
        cost = data.LingYu*self.CountInput:GetValue()
        if cost>lingyu then
            g_MessageMgr:ShowMessage("CARNIVAL_JADEUSED_LIMIT")
            return
        end
    end
    local v1 = tostring(cost)
    if icon then
        v1 = icon..v1
    else
        v1 = v1..LocalString.GetString("粮食")
    end
    local msg = g_MessageMgr:FormatMessage("IS_CONSUME_FOOD_OR_OTHERS",v1,zswData.Name)
    g_MessageMgr:ShowOkCancelMessage(msg,function()
        Gac2Gas.RequestBuyRollerCoasterFurniture(key,self.CountInput:GetValue(),type)
    end, nil, nil, nil, false)
end

function LuaHouseRollerCoasterExchangeWnd:InitItem(item,key)
    local data = Zhuangshiwu_RollerCoaster.GetData(key)
    local tf = item.transform
    local icon = tf:Find("Icon"):GetComponent(typeof(CUITexture))

    local nameLabel = tf:Find("NameLabel"):GetComponent(typeof(UILabel))
    local zswData = Zhuangshiwu_Zhuangshiwu.GetData(key)
    nameLabel.text = tostring(zswData.Name)
    
    icon:LoadMaterial(Item_Item.GetData(zswData.ItemId).Icon)
    local countLabel=  tf:Find("AmountLabel"):GetComponent(typeof(UILabel))
    
    local curNum = CLuaClientFurnitureMgr.GetPlacedFurnitureCountByTemplateId(key)
    local cnt = CClientFurnitureMgr.Inst:GetRepositoryFurnitureCount(key)
    countLabel.text = tostring(curNum+cnt)

	-- local max = LuaHouseTerrainMgr.GetGrassMaxNum(key)
    -- countLabel.text = SafeStringFormat3("%d/%d", self.m_Lookup[key] or 0,max)
end
function LuaHouseRollerCoasterExchangeWnd:OnSelectAtRow(row)
    local key = self.m_Keys[row+1]
    local data = Zhuangshiwu_RollerCoaster.GetData(key)

    local zswData = Zhuangshiwu_Zhuangshiwu.GetData(key)
    self.TitleLabel.text = tostring(zswData.Name)
    self.Icon:LoadMaterial(Item_Item.GetData(zswData.ItemId).Icon)
    UIEventListener.Get(self.Icon.gameObject).onClick = DelegateFactory.VoidDelegate(function(g)
        CItemInfoMgr.ShowRepositoryFurnitureInfo(key,nil)
    end)


    if data.Silver>0 then
        self.DescLabel.text = LocalString.GetString("消耗银两")

    elseif data.LingYu>0 then
        self.DescLabel.text = LocalString.GetString("消耗灵玉")
    else
        self.DescLabel.text = nil
    end
    self.CountInput:SetMinMax(1,99,1)

    if data.Food==0 then
        self.Toggle.value=true
        self.FoodCost:SetActive(false)
        self.YinLiangCost:SetActive(true)
    else
        self.Toggle.value=false

        if self.Toggle.value then
            self.FoodCost:SetActive(false)
            self.YinLiangCost:SetActive(true)
        else
            self.FoodCost:SetActive(true)
            self.YinLiangCost:SetActive(false)
        end
        
    end

    if data.LingYu>0 then
        self.m_MoneyCtrl:SetType(EnumMoneyType.LingYu_WithBind, EnumPlayScoreKey.NONE, false)
    else
        self.m_MoneyCtrl:SetType(EnumMoneyType.YinLiang, EnumPlayScoreKey.NONE, false)
    end
    self:UpdateCount(key)
    self.CountInput:SetValue(1,true)
    self.CountInput:OnValueChange()

    self.CostLabel.text = tostring(data.Food*self.CountInput:GetValue())
end

function LuaHouseRollerCoasterExchangeWnd:Init()
    local food = CClientHouseMgr.Inst.mConstructProp.Food
    self.OwnLabel.text = tostring(food)
    self.Toggle.value = false

    local script = self.YinLiangCost:GetComponent(typeof(CCommonLuaScript))
    script:Awake()
    self.m_MoneyCtrl = script.m_LuaSelf

    self.TableView:ReloadData(true,false)
    local idx = 0
    if s_SelectedTemplateId>0 then
        for i,v in ipairs(self.m_Keys) do
            if v==s_SelectedTemplateId then
                idx=i-1
            end
        end
        s_SelectedTemplateId = 0
    end
    self.TableView:SetSelectRow(idx,true)
end

--@region UIEvent

--@endregion UIEvent

function LuaHouseRollerCoasterExchangeWnd:OnEnable()
    g_ScriptEvent:AddListener("OnUpdateHouseResource", self, "OnUpdateHouseResource")
    g_ScriptEvent:AddListener("SetRepositoryFurnitureCount",self,"OnSetRepositoryFurnitureCount")
end
function LuaHouseRollerCoasterExchangeWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnUpdateHouseResource", self, "OnUpdateHouseResource")
    g_ScriptEvent:RemoveListener("SetRepositoryFurnitureCount",self,"OnSetRepositoryFurnitureCount")
end
function LuaHouseRollerCoasterExchangeWnd:OnUpdateHouseResource(args)
    local food = CClientHouseMgr.Inst.mConstructProp.Food
    self.OwnLabel.text = tostring(food)
    self:OnSelectAtRow(self.TableView.currentSelectRow)
end
function LuaHouseRollerCoasterExchangeWnd:OnSetRepositoryFurnitureCount(templateId,count)
    local key = self.m_Keys[self.TableView.currentSelectRow+1]
    for i,v in ipairs(self.m_Keys) do
        if v==templateId then
            local item = self.TableView:GetItemAtRow(i-1)
            self:InitItem(item,v)
            if v==key then
                self:UpdateCount(key)
            end
            break
        end        
    end
    self:RefreshToggle()
end
function LuaHouseRollerCoasterExchangeWnd:UpdateCount(key)
    self.LimitLabel.text = SafeStringFormat3(LocalString.GetString("（本周限购%d/%d）"),0,self.m_LimitCount)
    if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, 333) then
        local playData = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, 333)
        if playData.ExpiredTime > CServerTimeMgr.Inst.timeStamp and playData.Data and playData.Data.Data then
            local data = g_MessagePack.unpack(playData.Data.Data)
            local cnt = data[key] or 0
            self.LimitLabel.text = SafeStringFormat3(LocalString.GetString("（本周限购%d/%d）"),cnt,self.m_LimitCount)
        end
    end

    local curNum = CLuaClientFurnitureMgr.GetPlacedFurnitureCountByTemplateId(key)
    local cnt = CClientFurnitureMgr.Inst:GetRepositoryFurnitureCount(key)
    self.CountLabel.text = SafeStringFormat3(LocalString.GetString("已拥有%s"),tostring(cnt+curNum))
end
