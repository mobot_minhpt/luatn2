-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CFreightEquipSubmitMgr = import "L10.UI.CFreightEquipSubmitMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CXianjiaRawMaterial = import "L10.UI.CXianjiaRawMaterial"
local CXianjiaUpgradeMgr = import "L10.UI.CXianjiaUpgradeMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EnumQualityType = import "L10.Game.EnumQualityType"
local IdPartition = import "L10.Game.IdPartition"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local Object = import "System.Object"
local String = import "System.String"
local Talisman_XianJia = import "L10.Game.Talisman_XianJia"
CXianjiaRawMaterial.m_Init_CS2LuaHook = function (this, mainRaw) 

    this.mainRaw = mainRaw
    this.selectItem = nil
    this.iconTexture.material = nil
    this.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(EnumQualityType.White)
    this.selectSprite.enabled = false
    this.nameLabel.text = LocalString.GetString("原料法宝")
    this.nameLabel.color = Color.white
end
CXianjiaRawMaterial.m_OnRawSelect_CS2LuaHook = function (this, itemId, key) 

    if not this.selectSprite.enabled then
        return
    end
    if System.String.IsNullOrEmpty(itemId) then
        this.selectSprite.enabled = false
        return
    end
    this.selectSprite.enabled = false
    this.selectItem = CItemMgr.Inst:GetById(itemId)
    this.iconTexture:LoadMaterial(this.selectItem.Icon)
    this.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(this.selectItem.Equip.QualityType)
    this.nameLabel.text = this.selectItem.Name
    this.nameLabel.color = this.selectItem.Equip.DisplayColor
    if this.OnRawMatSelect ~= nil then
        GenericDelegateInvoke(this.OnRawMatSelect, Table2ArrayWithCount({this.selectItem}, 1, MakeArrayClass(Object)))
    end
end
CXianjiaRawMaterial.m_OnButtonClick_CS2LuaHook = function (this, go) 

    if this.mainRaw == nil then
        g_MessageMgr:ShowMessage("Talisman_Upgrade_Choose_Target")
    else
        this.selectSprite.enabled = true
        local dic = CreateFromClass(MakeGenericClass(Dictionary, Int32, MakeGenericClass(List, String)))
        local equipList = CreateFromClass(MakeGenericClass(List, String))
        local bagSize = CClientMainPlayer.Inst.ItemProp:GetPlaceSize(EnumItemPlace.Bag)
        do
            local i = 1
            while i <= bagSize do
                local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
                if not System.String.IsNullOrEmpty(id) then
                    local equip = CItemMgr.Inst:GetById(id)
                    if equip ~= nil and equip.IsEquip and IdPartition.IdIsTalisman(equip.TemplateId) and id ~= CXianjiaUpgradeMgr.leftRawMatId and id ~= CXianjiaUpgradeMgr.rightRawMatId and id ~= this.mainRaw.itemId then
                        local xianjia = Talisman_XianJia.GetData(equip.TemplateId)
                        local mainXianjia = Talisman_XianJia.GetData(this.mainRaw.templateId)
                        if xianjia ~= nil and mainXianjia ~= nil and xianjia.Grade == mainXianjia.Grade then
                            CommonDefs.ListAdd(equipList, typeof(String), equip.Id)
                        end
                    end
                end
                i = i + 1
            end
        end
        if equipList.Count > 0 then
            CommonDefs.DictAdd(dic, typeof(Int32), 0, typeof(MakeGenericClass(List, String)), equipList)
            CFreightEquipSubmitMgr.OpenEquipmentChooseWnd("", dic, LocalString.GetString("选择原料法宝"), 0, false, LocalString.GetString("选择"), "", nil, "")
        else
            g_MessageMgr:ShowMessage("Talisman_Upgrade_No_Item")
            this.selectSprite.enabled = false
        end
    end
end
