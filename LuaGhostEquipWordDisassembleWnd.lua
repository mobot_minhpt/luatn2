local LuaGameObject=import "LuaGameObject"
local MessageMgr=import "L10.Game.MessageMgr"
local EquipBaptize_Setting=import "L10.Game.EquipBaptize_Setting"
local LuaUtils=import "LuaUtils"
local Item_Item=import "L10.Game.Item_Item"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local CItemMgr=import "L10.Game.CItemMgr"
local CItemAccessListMgr=import "L10.UI.CItemAccessListMgr"
local AlignType=import "L10.UI.CItemInfoMgr+AlignType"
local EnumMoneyType=import "L10.Game.EnumMoneyType"
local CItemCountUpdate = import "L10.UI.CItemCountUpdate"

CLuaGhostEquipWordDisassembleWnd=class()
RegistClassMember(CLuaGhostEquipWordDisassembleWnd,"m_DisassembleButton")

RegistClassMember(CLuaGhostEquipWordDisassembleWnd,"m_CostTipLabel")

RegistClassMember(CLuaGhostEquipWordDisassembleWnd,"m_ItemCountUpdate")
RegistClassMember(CLuaGhostEquipWordDisassembleWnd,"m_ItemCountUpdate2")

RegistClassMember(CLuaGhostEquipWordDisassembleWnd,"m_NeedCount")
RegistClassMember(CLuaGhostEquipWordDisassembleWnd,"m_NeedCount2")
RegistClassMember(CLuaGhostEquipWordDisassembleWnd,"m_CostTemplateId")
RegistClassMember(CLuaGhostEquipWordDisassembleWnd,"m_CostTemplateId2")

RegistClassMember(CLuaGhostEquipWordDisassembleWnd,"m_ItemId") --拆解的装备id

RegistClassMember(CLuaGhostEquipWordDisassembleWnd,"m_Toggle")
RegistClassMember(CLuaGhostEquipWordDisassembleWnd,"m_Toggle2")

RegistClassMember(CLuaGhostEquipWordDisassembleWnd,"m_GetItemTemplate")

RegistClassMember(CLuaGhostEquipWordDisassembleWnd,"m_GetItems")
RegistClassMember(CLuaGhostEquipWordDisassembleWnd,"m_GetJiuQuZhuItemId")
RegistClassMember(CLuaGhostEquipWordDisassembleWnd,"m_GetJiuQuZhuNum")
RegistClassMember(CLuaGhostEquipWordDisassembleWnd,"m_GetDongGuangZhuItemId")
RegistClassMember(CLuaGhostEquipWordDisassembleWnd,"m_GetDongGuangZhuNum")

RegistClassMember(CLuaGhostEquipWordDisassembleWnd,"m_Grid")

RegistClassMember(CLuaGhostEquipWordDisassembleWnd,"m_MoneyCtrl")
RegistClassMember(CLuaGhostEquipWordDisassembleWnd,"m_CanUseCCM")

function CLuaGhostEquipWordDisassembleWnd:InitMoney()
    self.m_MoneyCtrl=LuaGameObject.GetChildNoGC(self.transform,"QnCostAndOwnMoney").moneyCtrl
    --绑定鬼装 银两+银票
    local commonItem = CItemMgr.Inst:GetById(self.m_ItemId)
    if commonItem and commonItem.IsBinded then
        self.m_MoneyCtrl.m_Type=EnumMoneyType.YinPiao
    end
    self.m_MoneyCtrl:SetCost(CLuaEquipMgr.GetCostYinLiang(self.m_ItemId))
end

function CLuaGhostEquipWordDisassembleWnd:InitEquip1()
    local equip1=LuaGameObject.GetChildNoGC(self.transform,"Equip1").transform
    self:InitItem(equip1,self.m_ItemId)
    --点击icon弹出信息框
    CommonDefs.AddOnClickListener(equip1.gameObject,DelegateFactory.Action_GameObject(function(go)
        local commonItem = CItemMgr.Inst:GetById(self.m_ItemId)
        if commonItem then
            CItemInfoMgr.ShowLinkItemInfo(commonItem)
        end
    end),false)
end

function CLuaGhostEquipWordDisassembleWnd:Init()
    self.m_ItemId=CLuaEquipMgr.GhostDisassembleEquip

    local citem = CItemMgr.Inst:GetById(self.m_ItemId)
    self.m_CanUseCCM = citem.Equip.WordsCount >= 6

    local settingdata = EquipBaptize_Setting.GetData()

    local getitems,ccm = CLuaEquipMgr.GetDisassembleRetItems(self.m_ItemId)
    self.m_GetItems=getitems
    self.m_NeedCount2 = 0
    if ccm[1] then
        self.m_NeedCount2 = ccm[1]
    end
    self.m_CostTemplateId2 = settingdata.ChangChunMuItemId

    self.m_GetDongGuangZhuNum = 0
    if ccm[2] then
        self.m_GetDongGuangZhuNum = ccm[2]
    end
    self.m_GetDongGuangZhuItemId=settingdata.DongGuangZhuItemId

    self.m_GetJiuQuZhuItemId=settingdata.JiuQuZhuItemId
    self.m_GetJiuQuZhuNum=self:GetJiuQuZhuNum(self.m_ItemId) 

    self.m_CostTemplateId=settingdata.BuJinMuItemId
    self.m_NeedCount=self:GetBuJinMuNum(self.m_ItemId)

    self:InitMoney()

    self:InitEquip1()
    
    self.m_GetItemTemplate=LuaGameObject.GetChildNoGC(self.transform,"GetItem").gameObject
    self.m_GetItemTemplate:SetActive(false)

    self.m_Grid=LuaGameObject.GetChildNoGC(self.transform,"Cost").grid

    self.m_CostTipLabel=LuaGameObject.GetChildNoGC(self.transform,"CostTipLabel").label

    self.m_ItemCountUpdate=FindChildWithType(self.transform,"CostItem",typeof(CItemCountUpdate))
    self.m_ItemCountUpdate2=FindChildWithType(self.transform,"CostItem2",typeof(CItemCountUpdate))

    self:InitCostItem(self.m_ItemCountUpdate,self.m_CostTemplateId,self.m_NeedCount)
    self:InitCostItem(self.m_ItemCountUpdate2,self.m_CostTemplateId2,self.m_NeedCount2)

    --拆解
    self.m_DisassembleButton =LuaGameObject.GetChildNoGC(self.transform,"DisassembleButton").gameObject
    CommonDefs.AddOnClickListener(self.m_DisassembleButton ,DelegateFactory.Action_GameObject(function(go)
        self:OnDisassembleBtnClick()
    end),false)

    self.m_Toggle=FindChildWithType(self.transform,"Toggle",typeof(UIToggle))
    self.m_Toggle2=FindChildWithType(self.transform,"Toggle2",typeof(UIToggle))

    if self.m_Toggle then
        
        LuaUtils.OnToggleChange(self.m_Toggle,DelegateFactory.Action(function()
            self:OnToggleChange(1)
        end))
    end
    if self.m_Toggle2 then
        self.m_Toggle:Set(not self.m_CanUseCCM)
        self.m_Toggle2:Set(self.m_CanUseCCM)
        LuaUtils.OnToggleChange(self.m_Toggle2,DelegateFactory.Action(function()
            self:OnToggleChange(2)
        end))
        
    end

    --self:InitGetItems()
end

function CLuaGhostEquipWordDisassembleWnd:OnToggleChange(type)
    if not CLuaEquipMgr.EnableVersion2 then
        if self.m_Toggle.value then
            self.m_CostTipLabel.text=LocalString.GetString("可勾选消耗不尽木以获得额外物品[c][FFE767](当前消耗)[-][/c]")
        else
            self.m_CostTipLabel.text=LocalString.GetString("可勾选消耗不尽木以获得额外物品[c][FFE767](当前不消耗)[-][/c]")
        end
    else
        if not self.m_Toggle.value and not self.m_Toggle2.value then
            self.m_CostTipLabel.text=LocalString.GetString("可勾选消耗不尽木以获得额外物品[c][FFE767](当前不消耗)[-][/c]")
        else
            local fmt = LocalString.GetString("可勾选消耗不尽木/长春木以获得额外物品[c][FFE767](当前消耗%s)[-][/c]")
            local msg = ""

            if type == 1 then --操作的是不尽木
                if self.m_Toggle.value then
                    msg = LocalString.GetString("不尽木")
                    self.m_Toggle2.value = false
                end
            elseif type == 2 then --操作的是长春木
                if self.m_Toggle2.value then
                    if not self.m_CanUseCCM then
                        self.m_Toggle2.value = false
                        g_MessageMgr:ShowMessage("Cant_Use_ChangChunMu")
                        return
                    end
                    msg = LocalString.GetString("长春木")
                    self.m_Toggle.value = false
                end
            end
            self.m_CostTipLabel.text=SafeStringFormat3(fmt,msg)
        end
    end
    self:InitGetItems()
end

function CLuaGhostEquipWordDisassembleWnd:InitCostItem(itemcu,tid,needcount)
    if itemcu == nil then return end
    local trans = itemcu.transform
    local icon=LuaGameObject.GetChildNoGC(trans,"MatIcon").cTexture
    local mask=LuaGameObject.GetChildNoGC(trans,"Mask").gameObject
    local template = CItemMgr.Inst:GetItemTemplate(tid)
    if template then
        icon:LoadMaterial(template.Icon)
    else
        icon:Clear()
    end
    
    itemcu.templateId=tid
    local function OnCountChange(val)
        if val>=needcount then
            mask:SetActive(false)
            itemcu.format=SafeStringFormat3("{0}/%d",needcount)
        else
            mask:SetActive(true)
            itemcu.format=SafeStringFormat3("[ff0000]{0}[-]/%d",needcount)
        end
    end
    itemcu.onChange=DelegateFactory.Action_int(OnCountChange)
    itemcu:UpdateCount()

    local itemGo=itemcu.gameObject
    CommonDefs.AddOnClickListener(itemGo,DelegateFactory.Action_GameObject(function(go)
        if itemcu.count>=needcount then
            CItemInfoMgr.ShowLinkItemTemplateInfo(tid,false,nil,AlignType.ScreenRight,0,0,0,0)
        else
            CItemAccessListMgr.Inst:ShowItemAccessInfo(tid, false, go.transform);
        end
    end),false)
end

function CLuaGhostEquipWordDisassembleWnd:OnDisassembleBtnClick()
    if not self.m_MoneyCtrl.moneyEnough then
        MessageMgr.Inst:ShowMessage("SILVER_NOT_ENOUGH", {})
        return
    end
    
    local canBuy=true
    if self.m_Toggle.value then
        if self.m_ItemCountUpdate.count<self.m_NeedCount then
            canBuy=false
            MessageMgr.Inst:ShowMessage("Ghost_Equip_ChaiJie_CaiLiao_Is_Lack", {})
        end
    end

    if self.m_Toggle2 and self.m_Toggle2.value then
        if self.m_ItemCountUpdate2.count<self.m_NeedCount2 then
            canBuy=false
            MessageMgr.Inst:ShowMessage("Equip_ChaiJie_ChangChunMuItem_Is_Lack", {})
        end
    end

    if canBuy then
        local itemInfo = CItemMgr.Inst:GetItemInfo(self.m_ItemId)
        if itemInfo then
            --关闭界面
            local useccm = false
            if self.m_Toggle2 then
                useccm = self.m_Toggle2.value
            end
            CLuaEquipMgr.RequestDisassembleEquip(itemInfo.place, self.m_ItemId,self.m_Toggle.value,useccm)
        end
    end
end

function CLuaGhostEquipWordDisassembleWnd:InitItem(tf,itemId)
    local icon=LuaGameObject.GetChildNoGC(tf,"EquipIcon").cTexture
    local qualitySprite=LuaGameObject.GetChildNoGC(tf,"Quality").sprite
    local bindSprite=LuaGameObject.GetChildNoGC(tf,"BindSprite").sprite

    local nameLabel=LuaGameObject.GetChildNoGC(tf,"Label").label

    local commonItem = CItemMgr.Inst:GetById(itemId)

    icon:LoadMaterial(commonItem.Icon)

    qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(commonItem.Equip.QualityType)

    if bindSprite then
        bindSprite.spriteName = commonItem.BindOrEquipCornerMark;
    end
    nameLabel.text=commonItem.Equip.ColoredDisplayName
end


--不尽木
function CLuaGhostEquipWordDisassembleWnd:GetBuJinMuNum(itemId)
    local commonItem = CItemMgr.Inst:GetById(itemId)
    local costNum=0
    local grade = commonItem.Grade
    local settingData=EquipBaptize_Setting.GetData()

    local bujinmuCosts=settingData.ChaiJieGhostUseBuJinMuCost
    for i=1,bujinmuCosts.Length,4 do
        local lower=bujinmuCosts[i-1]
        local upper=bujinmuCosts[i]
        if grade<=upper and grade>=lower then
            costNum=bujinmuCosts[i+1]
            break
        end
    end
	return costNum
end
--九曲珠
function CLuaGhostEquipWordDisassembleWnd:GetJiuQuZhuNum(itemId)
    local commonItem = CItemMgr.Inst:GetById(itemId)
    local costNum=0
    local grade = commonItem.Grade
    local settingData=EquipBaptize_Setting.GetData()

    local bujinmuCosts=settingData.ChaiJieGhostUseBuJinMuCost
    for i=1,bujinmuCosts.Length,4 do
        local lower=bujinmuCosts[i-1]
        local upper=bujinmuCosts[i]
        if grade<=upper and grade>=lower then
            costNum=bujinmuCosts[i+2]
            break
        end
    end
	return costNum
end

function CLuaGhostEquipWordDisassembleWnd:InitGetItems()
    CUICommonDef.ClearTransform(self.m_Grid.transform)
    if self.m_Toggle2 and self.m_Toggle2.value then
        local tempid = self.m_GetDongGuangZhuItemId --可以获得的额外的道具id
        local count = self.m_GetDongGuangZhuNum --可以获得的额外的道具数量

        if tempid > 0 then
            local go = NGUITools.AddChild(self.m_Grid.gameObject, self.m_GetItemTemplate)
            go:SetActive(true)
            self:InitGetItem(go, tempid, count)
        end
    end
    if self.m_Toggle.value then
        local itemId=self.m_GetJiuQuZhuItemId
        local count=self.m_GetJiuQuZhuNum
        local go=NGUITools.AddChild(self.m_Grid.gameObject,self.m_GetItemTemplate)
        go:SetActive(true)
        self:InitGetItem(go,itemId,count)
    end
    for i,v in ipairs(self.m_GetItems) do
        local itemId=v[1]
        local count=v[2]
        local go=NGUITools.AddChild(self.m_Grid.gameObject,self.m_GetItemTemplate)
        go:SetActive(true)
        self:InitGetItem(go,itemId,count)
    end

    local parent=self.m_Grid.transform
    local childCount=parent.childCount
    if childCount==1 then
        LuaUtils.SetLocalPosition(parent:GetChild(0),0,0,0)
    elseif childCount==2 then
        LuaUtils.SetLocalPosition(parent:GetChild(0),-70,0,0)
        LuaUtils.SetLocalPosition(parent:GetChild(1),70,0,0)
    elseif childCount==3 then
        LuaUtils.SetLocalPosition(parent:GetChild(0),0,70,0)
        LuaUtils.SetLocalPosition(parent:GetChild(1),-70,-70,0)
        LuaUtils.SetLocalPosition(parent:GetChild(2),70,-70,0)
    elseif childCount==4 then
        LuaUtils.SetLocalPosition(parent:GetChild(0),-70,70,0)
        LuaUtils.SetLocalPosition(parent:GetChild(1),70,70,0)
        LuaUtils.SetLocalPosition(parent:GetChild(2),-70,-70,0)
        LuaUtils.SetLocalPosition(parent:GetChild(3),70,-70,0)
    else
        self.m_Grid:Reposition()
    end
end
function CLuaGhostEquipWordDisassembleWnd:OnClickGetItem(go)
    
end
function CLuaGhostEquipWordDisassembleWnd:InitGetItem(go,itemId,count)
    local icon=LuaGameObject.GetChildNoGC(go.transform,"Icon").cTexture
    local label=LuaGameObject.GetChildNoGC(go.transform,"Label").label
    local data=Item_Item.GetData(itemId)
    if data then
        icon:LoadMaterial(data.Icon)
    else
        icon:Clear()
    end
    label.text=tostring(count)

    CommonDefs.AddOnClickListener(go,DelegateFactory.Action_GameObject(function(go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemId)
    end),false)
end
