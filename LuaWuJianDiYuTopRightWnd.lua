local CTopAndRightTipWnd = import "L10.UI.CTopAndRightTipWnd"
local CUIResources = import "L10.UI.CUIResources"
local CUIManager = import "L10.UI.CUIManager"
local CUIFx = import "L10.UI.CUIFx"
local LuaTweenUtils = import "LuaTweenUtils"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CMainCamera = import "L10.Engine.CMainCamera"

CLuaWuJianDiYuTopRightWnd = class()
RegistClassMember(CLuaWuJianDiYuTopRightWnd,"m_ExpandButton")
RegistClassMember(CLuaWuJianDiYuTopRightWnd,"m_CountLabel")
RegistClassMember(CLuaWuJianDiYuTopRightWnd,"m_Items")
RegistClassMember(CLuaWuJianDiYuTopRightWnd,"m_StageLabel")
RegistClassMember(CLuaWuJianDiYuTopRightWnd,"m_CenterLabelFx")
RegistClassMember(CLuaWuJianDiYuTopRightWnd,"m_WinLoseFx")
RegistClassMember(CLuaWuJianDiYuTopRightWnd,"m_JieTuoGuoIconTemplate")
RegistClassMember(CLuaWuJianDiYuTopRightWnd,"m_JieTuoGuoIconRoot")
RegistClassMember(CLuaWuJianDiYuTopRightWnd,"m_JieTuoGuoIcons")
RegistClassMember(CLuaWuJianDiYuTopRightWnd,"m_CountLabelPos")

function CLuaWuJianDiYuTopRightWnd:Init()
    self.m_ExpandButton=self.transform:Find("Anchor/Tip/ExpandButton").gameObject
    UIEventListener.Get(self.m_ExpandButton).onClick=DelegateFactory.VoidDelegate(function(go)
        LuaUtils.SetLocalRotation(self.m_ExpandButton.transform,0,0,0)
        CTopAndRightTipWnd.showPackage = false
        CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
    end)
    self:NotifyEnterHellStage()
    
    self:RefreshCollectionFx()
end

function CLuaWuJianDiYuTopRightWnd:RefreshCollectionFx()
    if(self.m_JieTuoGuoIcons and #self.m_JieTuoGuoIcons > 0)then
        for k, v in ipairs(self.m_JieTuoGuoIcons)do
            UnRegisterTick(v.tick)
        end
    end
    Extensions.RemoveAllChildren(self.m_JieTuoGuoIconRoot.transform)
    self.m_JieTuoGuoIcons = {}
end

function CLuaWuJianDiYuTopRightWnd:OnEnable()
    --地狱层级
    --初始化束缚状态数据
    local grid = self.transform:Find("Anchor/Content/Grid").gameObject
    Extensions.RemoveAllChildren(grid.transform)
    local item = self.transform:Find("Anchor/Content/Item").gameObject
    self.m_StageLabel = self.transform:Find("Anchor/Content/NameLabel1"):GetComponent(typeof(UILabel))
    self.m_CenterLabelFx = self.transform:Find("Front/CenterLabelFx"):GetComponent(typeof(CUIFx))
    self.m_WinLoseFx = self.transform:Find("Front/WinLoseFx"):GetComponent(typeof(CUIFx))
    self.m_JieTuoGuoIconTemplate = self.transform:Find("JieTuoGuoIcon").gameObject
    self.m_JieTuoGuoIconTemplate:SetActive(false)
    self.m_JieTuoGuoIconRoot = self.transform:Find("JieTuoGuoFxRoot").gameObject
    self.m_CountLabelPos = self.transform:Find("CountLabelPos").gameObject

    self.m_JieTuoGuoIcons = {}
    self:RefreshCollectionFx()

    -- --3个boss的图标
    -- do
    --     LuaWuJianDiYuMgr:InitSettingData()
    --     local bossicons = {}
    --     local icon = self.transform:Find("Anchor/BossIcons/BossIcon0").gameObject
    --     icon:SetActive(false)
    --     bossicons[LuaWuJianDiYuMgr.m_BossIds[1]] = icon
    --     icon = self.transform:Find("Anchor/BossIcons/BossIcon1").gameObject
    --     icon:SetActive(false)
    --     bossicons[LuaWuJianDiYuMgr.m_BossIds[2]] = icon
    --     --CLuaMiniMapPopWnd:InitHellBossIcons()
    -- end

    self.m_CenterLabelFx.gameObject:SetActive(false)
    item:SetActive(false)

    self.m_Items = {}
    for i=1,5 do
        local go = NGUITools.AddChild(grid,item)
        go:SetActive(false)
        table.insert( self.m_Items, go )
    end

    --初始化解脱果数据
    self.m_CountLabel = self.transform:Find("Anchor/Content/CountLabel"):GetComponent(typeof(UILabel))
    self.m_CountLabel.text = ""

    --添加监听事件
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")

    g_ScriptEvent:RemoveListener("PlayerCollectJieTuoGuo", self, "OnPlayerCollectJieTuoGuo")
    g_ScriptEvent:AddListener("PlayerCollectJieTuoGuo", self, "OnPlayerCollectJieTuoGuo")

    --填充解脱果数据
    if(LuaWuJianDiYuMgr.m_JieTuoGuoInfoCache)then
        local jietuoguo_info = LuaWuJianDiYuMgr.m_JieTuoGuoInfoCache
        local format = "[ff0000]%d[-]/%d"
        if(jietuoguo_info.num == jietuoguo_info.totalNum)then
            format = "%d/%d"
        end
        self.m_CountLabel.text = SafeStringFormat3(format,jietuoguo_info.num,jietuoguo_info.totalNum)
    end
    --填充束缚状态数据
    if(LuaWuJianDiYuMgr.m_PlayerImprismInfoCache)then
        for i,v in ipairs(LuaWuJianDiYuMgr.m_PlayerImprismInfoCache) do
            self.m_Items[i]:SetActive(true)
            local lock = self.m_Items[i].transform:Find("Lock").gameObject
            if v.isHaveBuff then
                lock:SetActive(true)
            else
                lock:SetActive(false)
            end
        end
    end
    --初始化层级数据
    self:RefreshStageLabel()

    LuaWuJianDiYuMgr.m_TopRightWindow = self
end

function CLuaWuJianDiYuTopRightWnd:OnDisable()
    --解除监听事件
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("PlayerCollectJieTuoGuo", self, "OnPlayerCollectJieTuoGuo")
    self:RefreshCollectionFx()
    if CUIManager.IsLoaded(CUIResources.TopAndRightTipWnd) then
        CUIManager.CloseUI(CUIResources.TopAndRightTipWnd)
    end
    --赋空值
    -- CLuaMiniMapPopWnd.HellImprismIcon = nil
    -- CLuaMiniMapPopWnd.m_WuJianDiYuBossIconTemplates = nil

    self.m_Items = nil
    self.m_CountLabel = nil
    LuaWuJianDiYuMgr.m_TopRightWindow = nil
end

function CLuaWuJianDiYuTopRightWnd:OnHideTopAndRightTipWnd()
    LuaUtils.SetLocalRotation(self.m_ExpandButton.transform,0,0,180)
end

function CLuaWuJianDiYuTopRightWnd:NotifyUpdateHellInfos()
    if(self.m_Items)then
        --更新解脱果数据
        if(LuaWuJianDiYuMgr.m_JieTuoGuoInfoCache)then

            local jietuoguo_info = LuaWuJianDiYuMgr.m_JieTuoGuoInfoCache
            if(jietuoguo_info.totalNum == 0)then
                self.m_CountLabel.gameObject:SetActive(false)
            else
                self.m_CountLabel.gameObject:SetActive(true)
                local format = "[ff0000]%d[-]/%d"
                if(jietuoguo_info.num == jietuoguo_info.totalNum)then
                    format = "%d/%d"
                end
                self.m_CountLabel.text = SafeStringFormat3(format,jietuoguo_info.num,jietuoguo_info.totalNum)
            end
        end
        --更新束缚状态数据
        if(LuaWuJianDiYuMgr.m_PlayerImprismInfoCache)then
            local t = LuaWuJianDiYuMgr.m_PlayerImprismInfoCache
            for i,v in ipairs(self.m_Items) do
                v:SetActive(false)
            end
            for i,v in ipairs(t) do
                self.m_Items[i]:SetActive(true)
                local lock = self.m_Items[i].transform:Find("Lock").gameObject
                if v.isHaveBuff then
                    lock:SetActive(true)
                else
                    lock:SetActive(false)
                end
            end
        end
        --更新层级数据
        self:RefreshStageLabel()
    end
end

function CLuaWuJianDiYuTopRightWnd:RefreshStageLabel()
    if(LuaWuJianDiYuMgr.HellStage == 1)then
        self.m_StageLabel.text = LocalString.GetString("第一层解脱果")
    elseif(LuaWuJianDiYuMgr.HellStage == 2)then
        self.m_StageLabel.text = LocalString.GetString("第二层解脱果")
    else
        self.m_StageLabel.text = LocalString.GetString("第三层：击杀BOSS")
    end
end

function CLuaWuJianDiYuTopRightWnd:NotifyEnterHellStage()

    if(self.m_CenterLabelFx and LuaWuJianDiYuMgr.NewLevel)then
        LuaWuJianDiYuMgr.NewLevel = false
        self.m_CenterLabelFx.gameObject:SetActive(true)
        self.m_CenterLabelFx:DestroyFx()
        self:RefreshCollectionFx()

        -- local tmp_label = self.m_CenterLabelFx.gameObject:GetComponent(typeof(UILabel))
        -- self.m_CenterLabelFx:DestroyFx()

        if(LuaWuJianDiYuMgr.HellStage == 1)then
            self.m_CenterLabelFx:LoadFx("Fx/UI/Prefab/UI_wujiandiyu_cengshu01.prefab")
        elseif(LuaWuJianDiYuMgr.HellStage == 2)then
            self.m_CenterLabelFx:LoadFx("Fx/UI/Prefab/UI_wujiandiyu_cengshu02.prefab")
        else
            self.m_CenterLabelFx:LoadFx("Fx/UI/Prefab/UI_wujiandiyu_cengshu03.prefab")
        end

        -- LuaTweenUtils.TweenAlpha(tmp_label.transform, 0, 1, 1)
        -- LuaTweenUtils.SetDelay(LuaTweenUtils.TweenAlpha(tmp_label.transform, 1, 0, 1), 1)
    end
end

function CLuaWuJianDiYuTopRightWnd:OnPlayerCollectJieTuoGuo(_engine_id)



    local obj = CClientObjectMgr.Inst:GetObject(_engine_id)
    if(obj)then

        local go = NGUITools.AddChild(self.m_JieTuoGuoIconRoot,self.m_JieTuoGuoIconTemplate)
        go:SetActive(true)

        local screenPos = CMainCamera.Main:WorldToScreenPoint(obj.RO.transform.localPosition)
        screenPos.z = 0
        local fromNGUIworldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)
        local fromNGUIrelativePos = self.m_JieTuoGuoIconRoot.transform:InverseTransformPoint(fromNGUIworldPos)


        go.transform.localPosition = fromNGUIrelativePos
        LuaTweenUtils.TweenPosition(go.transform, self.m_CountLabelPos.transform.localPosition.x,  self.m_CountLabelPos.transform.localPosition.y, 0, 1.0)
        local new_tick = RegisterTickOnce(function()
            go:SetActive(false)
        end, 1000)
        table.insert(self.m_JieTuoGuoIcons, {object = go, tick = new_tick})
    else
    end
    --CClientMainPlayer.Inst.WorldPos
end
