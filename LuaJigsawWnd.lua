local UIRoot=import "UIRoot"
local UICamera  = import "UICamera"
local Vector3   = import "UnityEngine.Vector3"
local Physics   = import "UnityEngine.Physics"
local CUIFx     = import "L10.UI.CUIFx"

LuaJigsawWnd = class()      --JigsawWnd 

--------RegistChildComponent-------
RegistChildComponent(LuaJigsawWnd,      "Pic_1",                    GameObject)
RegistChildComponent(LuaJigsawWnd,      "Pic_2",                    GameObject)
RegistChildComponent(LuaJigsawWnd,      "Pic_3",                    GameObject)
RegistChildComponent(LuaJigsawWnd,      "Pic_4",                    GameObject)
RegistChildComponent(LuaJigsawWnd,      "Pic_5",                    GameObject)
RegistChildComponent(LuaJigsawWnd,      "Pic_6",                    GameObject)
RegistChildComponent(LuaJigsawWnd,      "BG",                       GameObject)
RegistChildComponent(LuaJigsawWnd,      "CompletePic",              GameObject)
RegistChildComponent(LuaJigsawWnd,      "DisLabel",                 GameObject)
RegistChildComponent(LuaJigsawWnd,      "CompletepicEffect",        CUIFx)

---------RegistClassMember-------
RegistClassMember(LuaJigsawWnd,         "PicsTable")
RegistClassMember(LuaJigsawWnd,         "PicStateTable")
RegistClassMember(LuaJigsawWnd,         "IsComplete")
RegistClassMember(LuaJigsawWnd,         "JigsawEffectTick")                 --特效计时器
RegistClassMember(LuaJigsawWnd,         "CodeValue")                 

RegistClassMember(LuaJigsawWnd,         "CountDownTick")

LuaJigsawWnd.CardOrderValue = ""        --服务器原始码

function LuaJigsawWnd:Awake()
    Gac2Gas.RequestHalloweenCardOrder()
    self.CompletepicEffect.gameObject:SetActive(false)
    self.IsComplete =false
    self.CompletePic:SetActive(false)
    self.BG:SetActive(true)
    self.DisLabel:SetActive(true)

    self:InitPicsTable()
    self:InitPicStateTable()
end

function LuaJigsawWnd:InitPicsTable()
    self.PicsTable = {self.Pic_1,self.Pic_2,self.Pic_3,self.Pic_4,self.Pic_5,self.Pic_6}
    for i,v in ipairs(self.PicsTable) do
        UIEventListener.Get(self.PicsTable[i]).onDrag = DelegateFactory.VectorDelegate(function(go,delta)
            self:OnPicDrag(go,delta)
        end)
        UIEventListener.Get(self.PicsTable[i]).onDragEnd = DelegateFactory.VoidDelegate(function(go)
            self:OnPicDragEnd(go,i)
        end)
    end
end

function LuaJigsawWnd:InitPicStateTable()
    self.PicStateTable = {}
    for i=1,#self.PicsTable do
        self.PicStateTable[i] = 0
    end
end
function LuaJigsawWnd:OnDisable()
    UnRegisterTick(self.JigsawEffectTick)
    UnRegisterTick(self.CountDownTick)
end

function LuaJigsawWnd:OnPicDrag(go,delta)
    -- go.transform.position = UICamera.mainCamera:ScreenToWorldPoint(Vector3(Input.mousePosition.x,Input.mousePosition.y,0))
    local scale = UIRoot.GetPixelSizeAdjustment(go.transform.parent.gameObject);
    local trans = go.transform
    local pos = trans.localPosition
    pos.x = pos.x + delta.x*scale
    pos.y = pos.y + delta.y*scale
    trans.localPosition = pos
end

function LuaJigsawWnd:OnPicDragEnd(go,index)
    local currentPos = UICamera.currentTouch.pos
    local ray = UICamera.currentCamera:ScreenPointToRay(Vector3(currentPos.x,currentPos.y,0))
    local hits = Physics.RaycastAll(ray, 99999,  UICamera.currentCamera.cullingMask)

    local transParent = self.transform:Find("Content/Pic/Origin_"..index)    
    local transScale = 0.8
    local HitNum = 0
    for i=0,hits.Length-1 do
        local collider = hits[i].collider.gameObject
        if (string.find(collider.name, "Col_", 1, true) ~= nil) then
            HitNum = tonumber(string.sub(collider.name, -1))
            if HitNum == index then
                transParent = collider.transform
                transScale = 1
            end
        end
    end
    self:RefreshGrid(go.transform,HitNum,transParent,index,transScale)
end

function LuaJigsawWnd:RefreshGrid(pic,HitNum,transParent,index,transScale)
    if HitNum == index then       --放到了拼图板上且位置正确
        self.PicStateTable[index] = index
        self.DisLabel:SetActive(false)
        self:FixPos(pic,transParent,transScale)
    else
        self.PicStateTable[index] = 0
        self:FixPos(pic,transParent,transScale)
    end
    self:CheckComplete()
end

function LuaJigsawWnd:CheckComplete()
    self.IsComplete = true
    for i=1,#self.PicsTable do
        if self.PicStateTable[i] == 0 then
            self.IsComplete = false
        end
    end
    if self.IsComplete then
        self:ShowComplete()
    end
end

function LuaJigsawWnd:ShowComplete()
    self:TransCode()
    self.CompletepicEffect.gameObject:SetActive(true)
    
    Gac2Gas.RequestFinishHalloweenCard(tonumber(LuaJigsawWnd.CardOrderValue),self.CodeValue)

    if self.JigsawEffectTick ~= nil then
        invoke(self.JigsawEffectTick)
    end

    self.JigsawEffectTick = RegisterTickOnce(function () 
        self.CompletepicEffect.gameObject:SetActive(false)
        self.BG:SetActive(false)
        self.CompletePic:SetActive(true)
    end, 1650)

    if self.CountDownTick ~= nil then
        invoke(self.CountDownTick)
    end
    
    self.CountDownTick = RegisterTickOnce(function()
        CUIManager.CloseUI("JigsawWnd")
    end, 10000)
end

function LuaJigsawWnd:TransCode()
    local strList = {}
    if #LuaJigsawWnd.CardOrderValue == 6 then
        for i=1,#LuaJigsawWnd.CardOrderValue do
            strList[i]=string.sub(LuaJigsawWnd.CardOrderValue,i,i)
        end
    end
    self.CodeValue = tonumber(strList[1]..strList[4]..strList[2]..strList[5]..strList[3]..strList[6])
end

function LuaJigsawWnd:FixPos(pic,transParent,transScale)
    pic:SetParent(transParent)
    pic.localPosition = Vector3.zero
    pic.localRotation = Vector3.zero
    pic.localScale = Vector3(transScale,transScale,1)
end
