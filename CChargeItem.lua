-- Auto Generated!!
local CChargeItem = import "L10.UI.CChargeItem"
local CChargeWnd = import "L10.UI.CChargeWnd"
local CChatLinkMgr = import "CChatLinkMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Charge_Charge = import "L10.Game.Charge_Charge"
local CommonDefs = import "L10.Game.CommonDefs"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local LocalString = import "LocalString"
local Money = import "L10.Game.Money"
CChargeItem.m_UpdateView_CS2LuaHook = function (this, ChargeItemId) 
    local item = Charge_Charge.GetData(ChargeItemId)
    if not System.String.IsNullOrEmpty(item.Icon) then
        this.m_Texture:LoadMaterial(item.Icon)
    end
    this.m_Jade.text = tostring(item.Jade)
    this.m_MonthCardMark:SetActive(item.IsMonthCard > 0)
    if item.IsMonthCard > 0 then --月卡
        this.m_Description02.text = ""
    else
        this.m_Description02.text = CChatLinkMgr.TranslateToNGUIText(item.Description02, false)
    end

    if CommonDefs.IS_KR_CLIENT or CommonDefs.IS_HMT_CLIENT then
        this.m_PriceLabel.gameObject:SetActive(false)
        this.m_PriceLabel2.gameObject:SetActive(true)
        this.m_PriceLabel2.text = item.ShowPrice
    elseif CommonDefs.IS_VN_CLIENT then
        this.m_PriceLabel.gameObject:SetActive(false)
        this.m_PriceLabel2.gameObject:SetActive(true)
        local itemPid = item.PID
        local showPrice = LuaSEASdkMgr.cachePriceData and (LuaSEASdkMgr.cachePriceData[itemPid] and LuaSEASdkMgr.cachePriceData[itemPid]) or ""
        this.m_PriceLabel2.text = showPrice
        if showPrice == "" then
            RegisterTickOnce(function ()
                showPrice = LuaSEASdkMgr.cachePriceData and (LuaSEASdkMgr.cachePriceData[itemPid] and LuaSEASdkMgr.cachePriceData[itemPid]) or item.ShowPrice
                if not CommonDefs.IsNull(this.m_PriceLabel2) then
                    this.m_PriceLabel2.text = showPrice
                end
            end, 300)
        end
    else
        this.m_PriceLabel.text = System.String.Format(LocalString.GetString("{0}"), item.Price)
    end
    
    if item.IsMonthCard > 0 then
        if ChargeItemId == LuaWelfareMgr.BigMonthCardId then
            local privilege = this.transform:Find("Privilege").gameObject
            privilege:SetActive(true)
            UIEventListener.Get(privilege).onClick = DelegateFactory.VoidDelegate(function(go)
                g_MessageMgr:ShowMessage("Privilege_Tips")
            end)
            privilege.transform:Find("Label"):GetComponent(typeof(UILabel)).text = Charge_ChargeSetting.GetData("BigMonthCardPrivilegeHint").Value
        end
        this:UpdateMonthCardDesp02()
    end
    if this.m_AdditionalSprite then
        if item.AdditionalMoneyType == 0 then
            this.m_AdditionalSprite.spriteName = Money.GetIconName(EnumMoneyType.QnPoint, EnumPlayScoreKey.NONE)
        else
            this.m_AdditionalSprite.spriteName = Money.GetIconName(EnumMoneyType.YuanBao, EnumPlayScoreKey.NONE)
        end
    end
    this.m_Description02.gameObject:SetActive(not System.String.IsNullOrEmpty(this.m_Description02.text))
    this.m_RecommandMark:SetActive(item.IsRecommand > 0)
end
CChargeItem.m_UpdateMonthCardDesp02_CS2LuaHook = function (this)    
    local isShowAdditionalSprite = true
    local fx = this.transform:Find("vfx")
    if fx then
        fx.gameObject:SetActive(false)
    end
    if CClientMainPlayer.Inst ~= nil then
        local privilege = this.transform:Find("Privilege")
        local monthCardInfo = privilege and privilege.gameObject.activeSelf and CClientMainPlayer.Inst.ItemProp.BigMonthCardInfo or CClientMainPlayer.Inst.ItemProp.MonthCard
        --有月卡信息
        if monthCardInfo ~= nil and monthCardInfo.BuyTime ~= 0 then
            local leftDay = LuaWelfareMgr:MonthCardLeftDay(monthCardInfo)
            if leftDay > 0 then
                this.m_Description02.gameObject:SetActive(true)
                this.m_Description02.text = System.String.Format(LocalString.GetString("月卡剩余{0}天"), leftDay)
                isShowAdditionalSprite = false
            else
                fx.gameObject:SetActive(true)
            end
        elseif fx then
            fx.gameObject:SetActive(true)
        end
    end
    if this.m_AdditionalSprite then
        this.m_AdditionalSprite.gameObject:SetActive(isShowAdditionalSprite)
    end
end
