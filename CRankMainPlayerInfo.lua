-- Auto Generated!!
local CRankClassTemplate = import "L10.UI.CRankClassTemplate"
local CRankMainPlayerInfo = import "L10.UI.CRankMainPlayerInfo"
local Profession = import "L10.Game.Profession"
local CRankData = import "L10.UI.CRankData"
CRankMainPlayerInfo.m_Init_CS2LuaHook = function (this, mainPlayerRankInfo, showTag, width, clsList, job, countryCode) 
    do
        local i = 0
        while i < this.columns.Length do
            if i < mainPlayerRankInfo.Count then
                this.columns[i].text = mainPlayerRankInfo[i]
                if i < width.Count then
                    this.columns[i].width = width[i]
                else
                    this.columns[i].width = 0
                end
            else
                this.columns[i].text = nil
            end
            i = i + 1
        end
    end
    if this.career then
        if job > 0 then
            this.career.spriteName = Profession.GetIconByNumber(job)
            this.career.alpha = 1
        else
            this.career.spriteName = ""
            this.career.alpha = 0
        end
    end
    local cls = CommonDefs.GetComponent_Component_Type(this.columns[2], typeof(CRankClassTemplate))
    if cls ~= nil then
        if clsList ~= nil and this.columns ~= nil and this.columns.Length >= 3 then
            cls:Init(clsList)
        else
            cls:Init(nil)
        end
    end
    local flagSpriteName = LuaSEASdkMgr:GetFlagSpriteName(countryCode)
    if this.countryFlag then
        this.countryFlag.spriteName = flagSpriteName
    end
    
    this.mainPlayerTag:SetActive(showTag)
    this.table:Reposition()
end

