
local UILabel = import "UILabel"

local QnTableView = import "L10.UI.QnTableView"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CButton = import "L10.UI.CButton"
local CIMMgr = import "L10.Game.CIMMgr"
local Profession = import "L10.Game.Profession"

LuaKunxiansuoFoeSelectWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaKunxiansuoFoeSelectWnd, "SelectBtn", "SelectBtn", CButton)
RegistChildComponent(LuaKunxiansuoFoeSelectWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaKunxiansuoFoeSelectWnd, "VoidLabel", "VoidLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaKunxiansuoFoeSelectWnd,"m_TableViewDataSource")
RegistClassMember(LuaKunxiansuoFoeSelectWnd,"m_FoeData")

function LuaKunxiansuoFoeSelectWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.SelectBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSelectBtnClick()
	end)


    --@endregion EventBind end
end

function LuaKunxiansuoFoeSelectWnd:Init()
	local function initItem(item,index)
        self:InitItem(item,index)
    end

	self.m_FoeData = {}
    local enemies = CClientMainPlayer.Inst and CClientMainPlayer.Inst.RelationshipProp.Enemies
    if enemies then
        CommonDefs.DictIterate(enemies, DelegateFactory.Action_object_object(function(key,val)
            if key ~=nil then
                table.insert(self.m_FoeData, CIMMgr.Inst:GetBasicInfo(key))
            end
        end))
    end

	self.m_TableViewDataSource = DefaultTableViewDataSource.CreateByCount(#self.m_FoeData,initItem)
    self.TableView.m_DataSource = self.m_TableViewDataSource
    self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)
    self.TableView:ReloadData(true,false)
    self.VoidLabel.gameObject:SetActive(#self.m_FoeData ==0)
end

function LuaKunxiansuoFoeSelectWnd:InitItem(item,index)
    local tf = item.transform
    local nameLabel = FindChild(tf,"PlayerNameLabel"):GetComponent(typeof(UILabel))
    local clsIcon = FindChild(tf,"ClsIcon"):GetComponent(typeof(UISprite))
    local levelLabel = FindChild(tf,"LevelLabel"):GetComponent(typeof(UILabel))
    
    local data = self.m_FoeData[index+1]

    clsIcon.spriteName = Profession.GetIconByNumber(data.Class)
    nameLabel.text = data.Name
    levelLabel.text = SafeStringFormat3("lv.%d", data.Level)
end

function LuaKunxiansuoFoeSelectWnd:OnDisable()
    g_ScriptEvent:BroadcastInLua("OnKunxiansuoButtonDown")
end

function LuaKunxiansuoFoeSelectWnd:OnSelectAtRow(row)
    local data = self.m_FoeData[row+1]
    g_ScriptEvent:BroadcastInLua("OnKunxiansuoFoeSelect", data.ID, data.Name)
    CUIManager.CloseUI(CLuaUIResources.KunxiansuoFoeSelectWnd)
end

--@region UIEvent

function LuaKunxiansuoFoeSelectWnd:OnSelectBtnClick()
	CUIManager.ShowUI(CLuaUIResources.CommonSearchPlayerWnd)
    CUIManager.CloseUI(CLuaUIResources.KunxiansuoFoeSelectWnd)
end

--@endregion UIEvent

