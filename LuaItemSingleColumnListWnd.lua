require("common/common_include")

local CBaseWnd = import "L10.UI.CBaseWnd"
local CItemMgr = import "L10.Game.CItemMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local UILabel = import "UILabel"
local UIScrollView = import "UIScrollView"
local UIWidget = import "UIWidget"
local UIGrid = import "UIGrid"
local Extensions = import "Extensions"
local NGUIMath = import "NGUIMath"
local CUItexture = import "L10.UI.CUITexture"
local UISprite = import "UISprite"
local Item_Item = import "L10.Game.Item_Item"
local DelegateFactory = import "DelegateFactory"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"

LuaItemSingleColumnListWnd = class()
RegistClassMember(LuaItemSingleColumnListWnd,"m_TitleLabel")
RegistClassMember(LuaItemSingleColumnListWnd,"m_Background")
RegistClassMember(LuaItemSingleColumnListWnd,"m_ScrollView")
RegistClassMember(LuaItemSingleColumnListWnd,"m_Grid")
RegistClassMember(LuaItemSingleColumnListWnd,"m_Template")
RegistClassMember(LuaItemSingleColumnListWnd,"m_ItemObjs")


function LuaItemSingleColumnListWnd:Awake()
    
end

function LuaItemSingleColumnListWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaItemSingleColumnListWnd:Init()

	self.m_Background = self.transform:Find("Anchor/Background"):GetComponent(typeof(UIWidget))
	self.m_TitleLabel = self.transform:Find("Anchor/Background/TitleLabel"):GetComponent(typeof(UILabel))
	self.m_ScrollView = self.transform:Find("Anchor/Background/ScrollView"):GetComponent(typeof(UIScrollView))
	self.m_Grid = self.transform:Find("Anchor/Background/ScrollView/Grid"):GetComponent(typeof(UIGrid))
	self.m_Template = self.transform:Find("Anchor/Background/ScrollView/Template").gameObject

	self.m_Template:SetActive(false)

	self.m_TitleLabel.text = LuaItemSingleColumnListMgr.m_Title
	Extensions.RemoveAllChildren(self.m_Grid.transform)
	local dataList = LuaItemSingleColumnListMgr.m_AllData
	self.m_ItemObjs = {}
	for index, data in ipairs(dataList) do
		local go = CUICommonDef.AddChild(self.m_Grid.gameObject, self.m_Template)
		go:SetActive(true)
		table.insert(self.m_ItemObjs, index, go)
	end
	self:RefreshData()
	self.m_Grid:Reposition()
	self.m_Background.height = math.min(574, NGUIMath.CalculateRelativeWidgetBounds(self.m_Grid.transform).size.y + math.abs(self.m_ScrollView.panel.topAnchor.absolute) + math.abs(self.m_ScrollView.panel.bottomAnchor.absolute))
	self.m_ScrollView.panel:ResetAndUpdateAnchors()
	self.m_TitleLabel:ResetAndUpdateAnchors()
	self.m_ScrollView:ResetPosition()
	self:LayoutWnd()
end

function LuaItemSingleColumnListWnd:RefreshData()
	if not self.m_ItemObjs then return end
	if #self.m_ItemObjs ~= #LuaItemSingleColumnListMgr.m_AllData then return end
	for index, go in ipairs(self.m_ItemObjs) do
		local data = LuaItemSingleColumnListMgr.m_AllData[index]
		local cellGo = go.transform:Find("ItemCell").gameObject
		local iconTexture = go.transform:Find("ItemCell/IconTexture"):GetComponent(typeof(CUItexture))
		local disableSprite = go.transform:Find("ItemCell/DisableSprite").gameObject
		local qualitySprite = go.transform:Find("ItemCell/QualitySprite"):GetComponent(typeof(UISprite))
		local amountLabel = go.transform:Find("ItemCell/AmountLabel"):GetComponent(typeof(UILabel))
		local nameLabel = go.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
		local descLabel = go.transform:Find("DescLabel"):GetComponent(typeof(UILabel))

		-- itemTemplateId, needNum
		local ownNum = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, data.itemTemplateId)
		local itemTemplateData = Item_Item.GetData(data.itemTemplateId)
		iconTexture:LoadMaterial(itemTemplateData.Icon)
		disableSprite:SetActive(ownNum < data.needNum)
		qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(itemTemplateData, nil, false)
		if ownNum>=data.needNum then
			amountLabel.text = SafeStringFormat3("%d/%d", ownNum, data.needNum)
		else
			amountLabel.text = SafeStringFormat3("[c][ff0000]%d[-][/c]/%d", ownNum, data.needNum)
		end
		nameLabel.text = itemTemplateData.Name
		descLabel.text = nil

		CommonDefs.AddOnClickListener(cellGo, DelegateFactory.Action_GameObject(function(go) self:OnItemCellClick(go, data) end), false)
		CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(function(go) self:OnItemClick(go, index) end), false)
	end
end

function LuaItemSingleColumnListWnd:OnItemCellClick(go, data)
	local ownNum = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, data.itemTemplateId)
	if ownNum < data.needNum then
		CItemAccessListMgr.Inst:ShowItemAccessInfo(data.itemTemplateId, false, go.transform, CTooltipAlignType.Right)
	end
end

function LuaItemSingleColumnListWnd:OnItemClick(go, index)
	LuaItemSingleColumnListMgr.OnSelect(index)
	self:Close()
end

function LuaItemSingleColumnListWnd:LayoutWnd()
	local mgr = LuaItemSingleColumnListMgr
	local newNGUIWorldCenter = CUICommonDef.AdjustPosition(mgr.alignType, self.m_Background.width, self.m_Background.height, mgr.targetWorldPos, mgr.targetWidth, mgr.targetHeight)
	self.m_Background.transform.localPosition = newNGUIWorldCenter
end

return LuaItemSingleColumnListWnd
