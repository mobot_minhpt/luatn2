local CLoginMgr=import "L10.Game.CLoginMgr"
CLuaCrossDouDiZhuShareWnd = class()


CLuaCrossDouDiZhuShareWnd.m_Rank = 0
CLuaCrossDouDiZhuShareWnd.m_Score = 0
function CLuaCrossDouDiZhuShareWnd:Init()

	local jijun = self.transform:Find("jijun").gameObject
	local yajun = self.transform:Find("yajun").gameObject
	local guanjun = self.transform:Find("guanjun").gameObject
	if CLuaCrossDouDiZhuShareWnd.m_Rank==1 then
		jijun:SetActive(false)
		yajun:SetActive(false)
		guanjun:SetActive(true)
	elseif CLuaCrossDouDiZhuShareWnd.m_Rank==2 then
		jijun:SetActive(false)
		yajun:SetActive(true)
		guanjun:SetActive(false)
	elseif CLuaCrossDouDiZhuShareWnd.m_Rank==3 then
		jijun:SetActive(true)
		yajun:SetActive(false)
		guanjun:SetActive(false)
	end

	local scoreLabel = self.transform:Find("ScoreLabel"):GetComponent(typeof(UILabel))
    scoreLabel.text = tostring(CLuaCrossDouDiZhuShareWnd.m_Score)

    local rankLabel = self.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
	rankLabel.text = tostring(CLuaCrossDouDiZhuShareWnd.m_Rank)
	
    local shareButton=FindChild(self.transform,"ShareButton").gameObject
    UIEventListener.Get(shareButton).onClick=DelegateFactory.VoidDelegate(function(go)
        CUICommonDef.CaptureScreenAndShare()
    end)
    
    local tf=FindChild(self.transform,"PlayerInfoNode")
    local nameLabel = tf:Find("NameLabel"):GetComponent(typeof(UILabel))
	local serverLabel = tf:Find("ServerLabel"):GetComponent(typeof(UILabel))
	-- self.m_SelfLvlLabel = tf:Find("LevelLabel"):GetComponent(typeof(UILabel))
	local idLabel = tf:Find("ID"):GetComponent(typeof(UILabel))
	local icon = tf:Find("Icon"):GetComponent(typeof(CUITexture))
	if CClientMainPlayer.Inst then
		nameLabel.text = CClientMainPlayer.Inst.Name
		local myserver = CLoginMgr.Inst:GetSelectedGameServer()
		serverLabel.text = myserver.name
		-- self.m_SelfLvlLabel.text = CClientMainPlayer.Inst.Level .. ""
		idLabel.text = CClientMainPlayer.Inst.Id .. ""
		-- local portraitName = CUICommonDef.GetPortraitName(EnumToInt(CClientMainPlayer.Inst.Class), 
		-- EnumToInt(CClientMainPlayer.Inst.Gender),-1)
		-- self.m_SelfIconTexture:LoadNPCPortrait(portraitName,false)

		icon:LoadNPCPortrait(CClientMainPlayer.Inst.PortraitName,false)
    end
	
	local fx=self.transform:Find("Fx"):GetComponent(typeof(CUIFx))
	if CLuaCrossDouDiZhuShareWnd.m_Rank==1 then
		fx:LoadFx("Fx/UI/Prefab/UI_paishen_zongjuesai_guanjun.prefab")
	elseif CLuaCrossDouDiZhuShareWnd.m_Rank==2 then
		fx:LoadFx("Fx/UI/Prefab/UI_paishen_zongjuesai_yajun.prefab")
	elseif CLuaCrossDouDiZhuShareWnd.m_Rank==3 then
		fx:LoadFx("Fx/UI/Prefab/UI_paishen_zongjuesai_jijun.prefab")
	end
end
