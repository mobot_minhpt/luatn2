require("3rdParty/ScriptEvent")
require("common/common_include")

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local UITable = import "UITable"
local UIScrollView = import "UIScrollView"
local CButton = import "L10.UI.CButton"
local Extensions = import "Extensions"
local MessageWndManager = import "L10.UI.MessageWndManager"
local GameObject = import "UnityEngine.GameObject"
local Gac2Gas = import "L10.Game.Gac2Gas"
local Baby_Setting = import "L10.Game.Baby_Setting"
local Baby_BabyFund = import "L10.Game.Baby_BabyFund"
local NGUITools = import "NGUITools"
local Item_Item = import "L10.Game.Item_Item"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"

LuaBabyFundWindow=class()
RegistChildComponent(LuaBabyFundWindow,"tipBtn", GameObject)
RegistChildComponent(LuaBabyFundWindow,"buyBtn", GameObject)
RegistChildComponent(LuaBabyFundWindow,"lingyuLabel", UILabel)
RegistChildComponent(LuaBabyFundWindow,"scrollview", UIScrollView)
RegistChildComponent(LuaBabyFundWindow,"table", UITable)
RegistChildComponent(LuaBabyFundWindow,"infoNode", GameObject)

local function split(str, pat)
   local t = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
         table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end
--RegistClassMember(LuaBabyFundWindow, "cameraNode")

function LuaBabyFundWindow:OnEnable()
	g_ScriptEvent:AddListener("ReceiveBabyFundInfo", self, "Init")
end

function LuaBabyFundWindow:OnDisable()
	g_ScriptEvent:RemoveListener("ReceiveBabyFundInfo", self, "Init")
end

function LuaBabyFundWindow:InitFundTable()
	Extensions.RemoveAllChildren(self.table.transform)
	local fundCount = Baby_BabyFund.GetDataCount()
  local firstShow = false
	for i=1,fundCount do
		local fundData = Baby_BabyFund.GetData(i)
		if fundData then
			local node = NGUITools.AddChild(self.table.gameObject, self.infoNode)
			node:SetActive(true)
			node.transform:Find("babylevel/text"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("宝宝%s级"),fundData.Level)
			local itemTemplateNode = node.transform:Find("AwardList/Award").gameObject
			itemTemplateNode:SetActive(false)
			local awardTable = node.transform:Find("AwardList/Table"):GetComponent(typeof(UITable))
			local awardBtn = node.transform:Find("GetBonusBtn").gameObject
			if LuaBabyMgr.m_BabyFundInfo and LuaBabyMgr.m_BabyFundInfo[i] then
				if LuaBabyMgr.m_BabyFundInfo[i] == 1 then
					awardBtn:SetActive(true)
					awardBtn.transform:Find("Alert").gameObject:SetActive(true)
					awardBtn:GetComponent(typeof(CButton)).Enabled = true
					awardBtn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("可领取")
					local index = i
					local onGetClick = function(go)
						Gac2Gas.BabyFundRequestRecvPackage(index)
					end
					CommonDefs.AddOnClickListener(awardBtn,DelegateFactory.Action_GameObject(onGetClick),false)
				elseif LuaBabyMgr.m_BabyFundInfo[i] == 2 then
					awardBtn:SetActive(true)
					awardBtn.transform:Find("Alert").gameObject:SetActive(false)
					awardBtn:GetComponent(typeof(CButton)).Enabled = false
					awardBtn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("已领取")
				else
          if not firstShow then
            awardBtn:SetActive(true)
            awardBtn.transform:Find("Alert").gameObject:SetActive(false)
            awardBtn:GetComponent(typeof(CButton)).Enabled = true
            local onGetClick = function(go)
              g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("宝宝等级不足或未将宝宝带在身上，领取失败"))
            end
            awardBtn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("领取")
            CommonDefs.AddOnClickListener(awardBtn,DelegateFactory.Action_GameObject(onGetClick),false)
            firstShow = true
          else
            awardBtn:SetActive(false)
          end
				end
			else
				awardBtn:SetActive(false)
			end

			local itemDataArray = split(fundData.Award,';')
			Extensions.RemoveAllChildren(awardTable.transform)
			for j,b in ipairs(itemDataArray) do
				if b then
					local dataTable = split(b,',')
					if dataTable and #dataTable == 3 then
						local instance = NGUITools.AddChild(awardTable.gameObject, itemTemplateNode);
						instance:SetActive(true)
            local templateId, amount = dataTable[1], dataTable[2]

            local item = Item_Item.GetData(templateId);
            local templateScript = instance:GetComponent(typeof(CQnReturnAwardTemplate))
            if item and templateScript then
              templateScript:Init(item, amount)
						end

					end
				end
				awardTable:Reposition()
			end
		end
	end
	self.table:Reposition()
	self.scrollview:ResetPosition()
end

function LuaBabyFundWindow:Init()
	local babyFundPrice = Baby_Setting.GetData().BabyFundPrice
	self.lingyuLabel.text = babyFundPrice
	if LuaBabyMgr.m_BabyFundBuy == 1 then
		self.buyBtn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("已购买")
		self.buyBtn:GetComponent(typeof(CButton)).Enabled = false
	else
		self.buyBtn:GetComponent(typeof(CButton)).Enabled = true
		local onBuyClick = function(go)
			MessageWndManager.ShowOKCancelMessage(LocalString.GetString(SafeStringFormat3(LocalString.GetString("确定花费%s灵玉购买养育基金吗"),babyFundPrice)), DelegateFactory.Action(function ()
					Gac2Gas.BabyFundRequestBuyFund()
			end), nil, nil, nil, false)
		end
		CommonDefs.AddOnClickListener(self.buyBtn,DelegateFactory.Action_GameObject(onBuyClick),false)
		self.buyBtn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("购买")
	end


	local onTipClick = function(go)
		g_MessageMgr:ShowMessage("BABYFUND_TIP_MESSAGE")
	end
	CommonDefs.AddOnClickListener(self.tipBtn,DelegateFactory.Action_GameObject(onTipClick),false)
	self.infoNode:SetActive(false)
	self:InitFundTable()
end

function LuaBabyFundWindow:OnDestroy()

end

return LuaBabyFundWindow
