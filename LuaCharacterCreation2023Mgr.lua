local AMPlayer = import "L10.Game.CutScene.AMPlayer"
local CPropertyAppearance = import "L10.Game.CPropertyAppearance"
local EUIModuleGroup = import "L10.UI.CUIManager+EUIModuleGroup"
local CUIModule = import "L10.UI.CUIModule"
local Setting = import "L10.Engine.Setting"
local CUIManager = import "L10.UI.CUIManager"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local CMainCamera = import "L10.Engine.CMainCamera"
local CLoadingWnd = import "L10.UI.CLoadingWnd"

LuaCharacterCreation2023Mgr = {}

--进入创角前盲选的职业
LuaCharacterCreation2023Mgr.m_SelectedClassForCutscene = 1
LuaCharacterCreation2023Mgr.m_MndModel = 1--1:creating 2:exiting
LuaCharacterCreation2023Mgr.ForceOpenDengzhongshijie = false

LuaCharacterCreation2023Mgr.m_LodingFxPath = "fx/Vfx/Prefab/Dynamic/ui_lodingshow01.prefab"

function LuaCharacterCreation2023Mgr:JumpTimelineToFrame(frameIdx)
    -- to do
end

LuaCharacterCreation2023Mgr.m_LastCjName = nil
LuaCharacterCreation2023Mgr.mEnterClassGender = nil
LuaCharacterCreation2023Mgr.m_CanJump = false
function LuaCharacterCreation2023Mgr:PlayTimeline(name,time,id,isFemale,curclass)
    local dic = CreateFromClass(MakeGenericClass(Dictionary, cs_string, CPropertyAppearance))
    if not id then
        id = 0
    end
    
    local cdata = Initialization_ChuangJue2023.GetData(id)
	local scenenName = cdata.SubScene

    local idx = curclass * 100 + (isFemale and 1 or 0)
    local mainplayerData = Initialization_ChuangJue2023.GetData(idx)
    AMPlayer.Inst.isXianShenChuangJue = false--只播放凡身动画
    AMPlayer.Inst.isFemale = isFemale
    AMPlayer.Inst.cjClassName = mainplayerData and mainplayerData.ResName or ""
    AMPlayer.Inst:PlayChuangJue(name,nil,nil,dic,time,id)
    local scenenName = cdata.SubScene
    if scenenName ~= LuaCharacterCreation2023Mgr.m_LastSceneName then
        LuaCharacterCreation2023Mgr.m_LastSceneName = scenenName
        if LuaCharacterCreation2023Mgr.BGM then
            SoundManager.Inst:StopSound(LuaCharacterCreation2023Mgr.BGM)
            LuaCharacterCreation2023Mgr.BGM = nil
        end
        LuaCharacterCreation2023Mgr.BGM = SoundManager.Inst:PlaySound(cdata.BGM,Vector3.zero, 0.5, nil, 0)
    end
    LuaCharacterCreation2023Mgr.m_LastCjName = name
end

LuaCharacterCreation2023Mgr.m_NeedGoStill = false
LuaCharacterCreation2023Mgr.m_ShowTick = nil
function LuaCharacterCreation2023Mgr:OpenCreatingOrExitingWnd(class,needGoStill)
    LuaCharacterCreation2023Mgr.m_NeedGoStill = needGoStill
    if needGoStill then
        LuaCharacterCreation2023Mgr:CancelShowWndTick()
        LuaCharacterCreation2023Mgr:ShowCrossingFx()
        LuaCharacterCreation2023Mgr.m_ShowTick = RegisterTickOnce(function ()
            CUIManager.CloseUI(CLuaUIResources.PinchFaceWnd)
            local selecteClass = (class and class > 0) and class or 1
            LuaCharacterCreation2023Mgr.m_SelectedClassForCutscene = selecteClass
            CUIManager.ShowUI(CUIResources.CharacterCreation2023Wnd)
            CMainCamera.Inst:SetCameraEnableStatus(true, "Enter_ChuangJue", false)
        end, 1000)
    else
        local selecteClass = (class and class > 0) and class or 1
        LuaCharacterCreation2023Mgr.m_SelectedClassForCutscene = selecteClass
        CUIManager.ShowUI(CUIResources.CharacterCreation2023Wnd)
        CMainCamera.Inst:SetCameraEnableStatus(true, "Enter_ChuangJue", false)
    end
end

function LuaCharacterCreation2023Mgr:OnlyPlayTimeline(class)
    local dic = CreateFromClass(MakeGenericClass(Dictionary, cs_string, CPropertyAppearance))
    local idx = class * 100 + 1
    local mainplayerData = Initialization_ChuangJue2023.GetData(idx)
    AMPlayer.Inst.isXianShenChuangJue = false
    AMPlayer.Inst.isFemale = isFemale
    AMPlayer.Inst.cjClassName = mainplayerData and mainplayerData.ResName or ""
    AMPlayer.Inst:PlayChuangJue(mainplayerData.TimelineName,nil,nil,dic,0,idx)
end

function LuaCharacterCreation2023Mgr:CancelShowWndTick()
    if LuaCharacterCreation2023Mgr.m_ShowTick then
        UnRegisterTick(LuaCharacterCreation2023Mgr.m_ShowTick)
        LuaCharacterCreation2023Mgr.m_ShowTick = nil
    end
end

LuaCharacterCreation2023Mgr.m_IsPlayerOperating = false
function LuaCharacterCreation2023Mgr:IsPlayerOperating()
    local result = LuaCharacterCreation2023Mgr.m_IsPlayerOperating
    LuaCharacterCreation2023Mgr.m_IsPlayerOperating = false
    return result
end

function LuaCharacterCreation2023Mgr:SetPlayerOperating()
    LuaCharacterCreation2023Mgr.m_IsPlayerOperating = true
end

function LuaCharacterCreation2023Mgr:ResetIsOperating()
    LuaCharacterCreation2023Mgr.m_IsPlayerOperating = false
end

function LuaCharacterCreation2023Mgr:ShowLoginUI()
    local list = CLoginMgr.Inst.m_ExistingPlayerList
    local isLoginExistingCharacter = list.Count > 0
    if isLoginExistingCharacter then
        CUIManager.ShowUI(CUIResources.CharacterCreation2023Wnd)
    else
        CUIManager.ShowUI(CUIResources.BeforeEnterDengZhongShiJieWnd)
    end
end

function LuaCharacterCreation2023Mgr:ShowCrossingFx()
    local showingPinchFaceFx = CLoadingWnd.Inst.PinchFaceFx.gameObject.activeSelf
    if showingPinchFaceFx then
        return
    end
    CLoadingWnd.Inst:ShowPinchFaceFx(3, nil)
end

function LuaCharacterCreation2023Mgr:HideCrossingFx()
    CLoadingWnd.Inst:HidePinchFaceFx()
end