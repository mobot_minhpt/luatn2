local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local StringBuilder = import "System.Text.StringBuilder"
local CBaseWnd = import "L10.UI.CBaseWnd"
local Screen = import "UnityEngine.Screen"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CPos = import "L10.Engine.CPos"
local CMainCamera = import "L10.Engine.CMainCamera"
LuaDaFuWongPlayerTip = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaDaFuWongPlayerTip, "PlayerName", "PlayerName", UILabel)
RegistChildComponent(LuaDaFuWongPlayerTip, "PlayerTexture", "PlayerTexture", GameObject)
RegistChildComponent(LuaDaFuWongPlayerTip, "TipTemplate", "TipTemplate", UILabel)
RegistChildComponent(LuaDaFuWongPlayerTip, "Money", "Money", UILabel)
RegistChildComponent(LuaDaFuWongPlayerTip, "TotalMoney", "TotalMoney", UILabel)
RegistChildComponent(LuaDaFuWongPlayerTip, "Table", "Table", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongPlayerTip, "m_PlayerData")
RegistClassMember(LuaDaFuWongPlayerTip, "m_PlayerID")
RegistClassMember(LuaDaFuWongPlayerTip, "m_MinHeight")
function LuaDaFuWongPlayerTip:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_PlayerID = 0
    self.m_PlayerData = nil
    self.m_MinHeight = 200
end

function LuaDaFuWongPlayerTip:Init()
    if LuaDaFuWongMgr.CurPlayerId == 0 then return end
    local playerInfo = LuaDaFuWongMgr.PlayerInfo and LuaDaFuWongMgr.PlayerInfo[LuaDaFuWongMgr.CurPlayerId]

    if not playerInfo then return end
    self.TipTemplate.gameObject:SetActive(false)
    self.m_PlayerID = LuaDaFuWongMgr.CurPlayerId
    self.m_PlayerData = playerInfo
    self:InitPlayerHead()
    self:InitPlayerMoney()
    self:InitContent()
end
function LuaDaFuWongPlayerTip:InitPlayerHead()
    self.PlayerName.text = self.m_PlayerData.name
    self.PlayerTexture:GetComponent(typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(self.m_PlayerData.class, self.m_PlayerData.gender, -1), false)
end

function LuaDaFuWongPlayerTip:InitPlayerMoney()
    self.Money.text = self.m_PlayerData.Money
    self.TotalMoney.text = self.m_PlayerData.TotalMoney
end

function LuaDaFuWongPlayerTip:InitContent()
    Extensions.RemoveAllChildren(self.Table.transform)
    local tableData = {}
    table.insert(tableData,self:GetItemInfo())
    table.insert(tableData,self:GetLandInfo())
    table.insert(tableData,self:GetGoodsInfo())
    table.insert(tableData,self:GetBuffInfo())
    for i = 1,#tableData do
        local go = CUICommonDef.AddChild(self.Table.gameObject,self.TipTemplate.gameObject)
        go.gameObject:SetActive(true)
        go:GetComponent(typeof(UILabel)).text = tableData[i].content
        go.transform:Find("Label"):GetComponent(typeof(UILabel)).text = tableData[i].name
    end
    self.Table:GetComponent(typeof(UITable)):Reposition()
    self:LayoutWnd()
end

function LuaDaFuWongPlayerTip:LayoutWnd()
    local panel = self.transform:Find("Background/Right/Panel"):GetComponent(typeof(UIPanel))
    local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    local virtualScreenWidth = Screen.width * scale * CUIManager.UIMainCamera.rect.width
    local virtualScreenHeight = Screen.height * scale

    local contentTopPadding = math.abs(panel.topAnchor.absolute)
    local contentBottomPadding = math.abs(panel.bottomAnchor.absolute)

    --计算高度
    local totalWndHeight = self:TotalHeightOfScrollViewContent() + contentTopPadding + contentBottomPadding + panel.clipSoftness.y * 2 + 1
    --加1避免由于精度误差导致scrollview刚好显示全table内容时可以滚动的问题
    local displayWndHeight = math.min(math.max(totalWndHeight, self.m_MinHeight), virtualScreenHeight)

    --设置背景高度
    self.transform:Find("Background"):GetComponent(typeof(UIWidget)).height = math.ceil(displayWndHeight)

    local rects = CommonDefs.GetComponentsInChildren_Component_Type_Boolean(self.gameObject:GetComponent(typeof(CBaseWnd)), typeof(UIRect), true)
    do
        local i = 0
        while i < rects.Length do
            if rects[i].updateAnchors == UIRect.AnchorUpdate.OnEnable or rects[i].updateAnchors == UIRect.AnchorUpdate.OnStart then
                rects[i]:ResetAndUpdateAnchors()
            end
            i = i + 1
        end
    end
    --self.ContentScrollView:ResetPosition()
    --self.transform:Find("ScrollViewIndicator"):GetComponent(typeof(UIScrollViewIndicator)):Layout()
    --self.m_Background.transform.localPosition = Vector3.zero
end

function LuaDaFuWongPlayerTip:TotalHeightOfScrollViewContent()
    local table = self.Table:GetComponent(typeof(UITable))
    return NGUIMath.CalculateRelativeWidgetBounds(table.transform).size.y + table.padding.y * 2
end

function LuaDaFuWongPlayerTip:GetItemInfo()
    local cardData = 0
    for k,v in pairs(self.m_PlayerData.CardInfo) do
        if v and v > 0 then cardData = cardData + v end
    end
    local t = {
        name = LocalString.GetString("秘宝"),
        content = SafeStringFormat3(LocalString.GetString("%d个"),cardData)
    }
    return t
end

function LuaDaFuWongPlayerTip:GetLandInfo()
    local stringbuilder = NewStringBuilderWraper(StringBuilder)
    local landInfo = {}
    for k,v in pairs(self.m_PlayerData.LandsInfo) do
        if v then table.insert(landInfo,k) end
    end
    local maxCount = DaFuWeng_Setting.GetData().MaxShowLands
    if #landInfo < 1 then
        stringbuilder:Append(LocalString.GetString("无"))
    else
        for i = 1,#landInfo do
            local landData = DaFuWeng_Land.GetData(landInfo[i])
            if landData then
                if i <= maxCount then
                    stringbuilder:Append(landData.Name)
                    if i ~= #landInfo and i ~= maxCount then stringbuilder:Append(LocalString.GetString("、")) end
                end
            end
        end
        if #landInfo > maxCount then stringbuilder:Append(SafeStringFormat3(DaFuWeng_Setting.GetData().ShowLandsLabel,#landInfo)) end
    end
   
    local t = {
        name = LocalString.GetString("房产"),
        content = stringbuilder:ToString()
    }
    return t
end

function LuaDaFuWongPlayerTip:GetGoodsInfo()
    local stringbuilder = NewStringBuilderWraper(StringBuilder)
    local count = 0
    local maxCount = DaFuWeng_Setting.GetData().MaxShowGoods
    for k,v in pairs(self.m_PlayerData.GoodsInfo) do
        local goodsData = DaFuWeng_Goods.GetData(v.goodsId)
        if goodsData then
            if count <= maxCount then
                if count ~= 0 then stringbuilder:Append(LocalString.GetString("、")) end
                stringbuilder:Append(goodsData.Name)
            end
            
            count = count + 1
        end
        
    end

    if count < 1 then stringbuilder:Append(LocalString.GetString("无")) end
    if count > maxCount then stringbuilder:Append(SafeStringFormat3(DaFuWeng_Setting.GetData().ShowGoodsLabel,count)) end
    local t = {
        name = LocalString.GetString("货物"),
        content = stringbuilder:ToString()
    }
    return t
end

function LuaDaFuWongPlayerTip:GetBuffInfo()
    local stringbuilder = NewStringBuilderWraper(StringBuilder)
    local buffInfo = LuaDaFuWongMgr.PlayerInfo[self.m_PlayerID].BuffInfo
    local count = 0
    for k,v in pairs(buffInfo) do
        local buffData = DaFuWeng_Buff.GetData(k)
        if buffData and v > 0 then
            if count ~= 0 then
                stringbuilder:Append(LocalString.GetString("、"))
            end
            stringbuilder:Append(buffData.Name)
            stringbuilder:Append(SafeStringFormat3(LocalString.GetString("(%d回合)"),v))
            count = count + 1
        end
        
    end
    if count <= 0 then
        stringbuilder:Append(LocalString.GetString("无"))
    end

    local t = {
        name = LocalString.GetString("其他"),
        content = stringbuilder:ToString()
    }
    return t 
end

--@region UIEvent

--@endregion UIEvent

