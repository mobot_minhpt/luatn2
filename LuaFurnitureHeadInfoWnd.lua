local Vector3 = import "UnityEngine.Vector3"
local Object = import "UnityEngine.Object"
local Extensions = import "Extensions"
local CMatongHeadInfo = import "L10.UI.CMatongHeadInfo"
local CGardenHeadInfo = import "L10.UI.CGardenHeadInfo"
local CClientFurnitureMgr = import "L10.Game.CClientFurnitureMgr"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"


CLuaFurnitureHeadInfoWnd = class()
RegistClassMember(CLuaFurnitureHeadInfoWnd,"m_GardenTemplate")
RegistClassMember(CLuaFurnitureHeadInfoWnd,"m_MatongTemplate")
RegistClassMember(CLuaFurnitureHeadInfoWnd,"headInfoDict")
RegistClassMember(CLuaFurnitureHeadInfoWnd,"matongHeadInfoDict")
RegistClassMember(CLuaFurnitureHeadInfoWnd, "Root")
RegistClassMember(CLuaFurnitureHeadInfoWnd, "headInfoList")

function CLuaFurnitureHeadInfoWnd:Awake()
    self.m_GardenTemplate = self.transform:Find("GardenTemplate").gameObject
    self.m_MatongTemplate = self.transform:Find("MatongTemplate").gameObject
    self.headInfoDict = {}
    self.matongHeadInfoDict = {}
-- self.headInfoList = {}
    self.Root = self.transform:Find("Root")

    self.m_GardenTemplate:SetActive(false)
    self.m_MatongTemplate:SetActive(false)
end

-- Auto Generated!!
function CLuaFurnitureHeadInfoWnd:Init( )

    if CameraFollow.Inst == nil then
        return
    end
    self:SetScaleFactor(CameraFollow.Inst:GetFontFactor())
end
function CLuaFurnitureHeadInfoWnd:OnEnable( )
    self:InitHeadInfos()
    if self.m_GardenTemplate ~= nil and self.m_MatongTemplate ~= nil then
        g_ScriptEvent:AddListener("ClientFurnitureCreate",self,"OnClientObjCreate")
        g_ScriptEvent:AddListener("ClientFurnitureDestroy",self,"OnClientObjDestroy")
        g_ScriptEvent:AddListener("MainCameraAngleChanged",self,"OnMainCameraAngleChanged")
    end
end
function CLuaFurnitureHeadInfoWnd:OnDisable( )

    if self.m_GardenTemplate ~= nil and self.m_MatongTemplate ~= nil then
        g_ScriptEvent:RemoveListener("ClientFurnitureCreate",self,"OnClientObjCreate")
        g_ScriptEvent:RemoveListener("ClientFurnitureDestroy",self,"OnClientObjDestroy")
        g_ScriptEvent:RemoveListener("MainCameraAngleChanged",self,"OnMainCameraAngleChanged")
     end
end

function CLuaFurnitureHeadInfoWnd:Update()
    if self.headInfoDict then
        for k,v in pairs(self.headInfoDict) do
            v:OnUpdate()
        end
    end
    if self.matongHeadInfoDict then
        for k,v in pairs(self.matongHeadInfoDict) do
            v:OnUpdate()
        end
    end
end

function CLuaFurnitureHeadInfoWnd:InitHeadInfos( )
    Extensions.RemoveAllChildren(self.Root)
    self.headInfoDict = {}
    self.headInfoList = {}

    self.matongHeadInfoDict = {}
    CommonDefs.DictIterate(CClientFurnitureMgr.Inst.FurnitureList, DelegateFactory.Action_object_object(function (___key, obj) 
        if obj then
            self:CreateHeadInfoWnd(obj.ID)
        end
    end))
end
function CLuaFurnitureHeadInfoWnd:CreateHeadInfoWnd( objId ) 
    local clientObj = CClientFurnitureMgr.Inst:GetFurniture(objId)
    if clientObj == nil then
        return
    end

    if self.headInfoDict[objId] then
        self.headInfoDict[objId]:Init(objId)
    elseif self.matongHeadInfoDict[objId] then
        self.matongHeadInfoDict[objId]:Init(objId)
    else
        if clientObj:IsGarden() and self.m_GardenTemplate ~= nil then
            local go = NGUITools.AddChild(self.Root.gameObject,self.m_GardenTemplate)
            go:SetActive(true)
            local headInfo = CommonDefs.GetComponent_GameObject_Type(go, typeof(CGardenHeadInfo))
            self.headInfoDict[objId] = headInfo
            headInfo:Init(objId)
        elseif CClientFurnitureMgr.Inst:IsMatongFurnitureId(clientObj.TemplateId) and self.m_MatongTemplate ~= nil then
            local go = NGUITools.AddChild(self.Root.gameObject,self.m_MatongTemplate)
            go:SetActive(true)
            local headInfo = CommonDefs.GetComponent_GameObject_Type(go, typeof(CMatongHeadInfo))
            self.matongHeadInfoDict[objId] = headInfo
            headInfo:Init(objId)
        end
    end
end
function CLuaFurnitureHeadInfoWnd:OnClientObjCreate(args)
    local objId=args[0]
    self:CreateHeadInfoWnd(objId)
end
function CLuaFurnitureHeadInfoWnd:OnClientObjDestroy( args) 
    local objId=args[0]
    if self.headInfoDict[objId] then
        Object.Destroy(self.headInfoDict[objId].gameObject)
        self.headInfoDict[objId] = nil
    elseif self.matongHeadInfoDict[objId] then
        Object.Destroy(self.matongHeadInfoDict[objId].gameObject)
        self.matongHeadInfoDict[objId] = nil
    end
end

function CLuaFurnitureHeadInfoWnd:SetScaleFactor(factor)
    self.transform.localScale = Vector3(factor,factor,factor)
end
function CLuaFurnitureHeadInfoWnd:OnMainCameraAngleChanged()
    self:SetScaleFactor(CameraFollow.Inst:GetFontFactor())
end
