local DelegateFactory  = import "DelegateFactory"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local UISlider = import "UISlider"
local GameObject = import "UnityEngine.GameObject"
local NativeTools = import "L10.Engine.NativeTools"
local EShareType = import "L10.UI.EShareType"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local ETickType = import "L10.Engine.ETickType"
local LoadPicMgr = import "L10.Game.LoadPicMgr"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local Main = import "L10.Engine.Main"
local Screen = import "UnityEngine.Screen"
local LineRenderer = import "UnityEngine.LineRenderer"
local Camera = import "UnityEngine.Camera"
local RenderTexture = import "UnityEngine.RenderTexture"
local RenderTextureFormat = import "UnityEngine.RenderTextureFormat"
local RenderTextureReadWrite = import "UnityEngine.RenderTextureReadWrite"
local BoxCollider = import "UnityEngine.BoxCollider"
local Material = import "UnityEngine.Material"
local Object = import "UnityEngine.Object"
local Constants = import "L10.Game.Constants"
local Rect = import "UnityEngine.Rect"

LuaGuildTerritorialWarsDrawMapPanel = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGuildTerritorialWarsDrawMapPanel, "BrushSlider", "BrushSlider", UISlider)
RegistChildComponent(LuaGuildTerritorialWarsDrawMapPanel, "EraserSlider", "EraserSlider", UISlider)
RegistChildComponent(LuaGuildTerritorialWarsDrawMapPanel, "BackButton", "BackButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsDrawMapPanel, "ForwardButton", "ForwardButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsDrawMapPanel, "ExitButton", "ExitButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsDrawMapPanel, "ShareButton", "ShareButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsDrawMapPanel, "BtnsView", "BtnsView", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsDrawMapPanel, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsDrawMapPanel, "LineGroup", "LineGroup", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsDrawMapPanel, "LineRendererTemplate", "LineRendererTemplate", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsDrawMapPanel, "Camera", "Camera", Camera)
RegistChildComponent(LuaGuildTerritorialWarsDrawMapPanel, "DrawTexture", "DrawTexture", UITexture)
RegistChildComponent(LuaGuildTerritorialWarsDrawMapPanel, "LineWidthSprite", "LineWidthSprite", UIWidget)
RegistChildComponent(LuaGuildTerritorialWarsDrawMapPanel, "BrushButton", "BrushButton", QnSelectableButton)
RegistChildComponent(LuaGuildTerritorialWarsDrawMapPanel, "EraserButton", "EraserButton", QnSelectableButton)
RegistChildComponent(LuaGuildTerritorialWarsDrawMapPanel, "SelectColorButton", "SelectColorButton", GameObject)
--@endregion RegistChildComponent end
RegistClassMember(LuaGuildTerritorialWarsDrawMapPanel,"m_ScreenWidth")
RegistClassMember(LuaGuildTerritorialWarsDrawMapPanel,"m_ScreenHeight")
RegistClassMember(LuaGuildTerritorialWarsDrawMapPanel,"m_LineRenderers")
RegistClassMember(LuaGuildTerritorialWarsDrawMapPanel,"m_LineRenderersWidthList")
RegistClassMember(LuaGuildTerritorialWarsDrawMapPanel,"m_LastPosition")
RegistClassMember(LuaGuildTerritorialWarsDrawMapPanel,"m_CurVertexCount")
RegistClassMember(LuaGuildTerritorialWarsDrawMapPanel,"m_RenderTexture")
RegistClassMember(LuaGuildTerritorialWarsDrawMapPanel,"m_SelectIndex")
RegistClassMember(LuaGuildTerritorialWarsDrawMapPanel,"m_BrushLineWidth")
RegistClassMember(LuaGuildTerritorialWarsDrawMapPanel,"m_EraserLineWidth")
RegistClassMember(LuaGuildTerritorialWarsDrawMapPanel,"m_CurIndex")
RegistClassMember(LuaGuildTerritorialWarsDrawMapPanel,"m_CurLineRenderer")
RegistClassMember(LuaGuildTerritorialWarsDrawMapPanel,"m_LastLineSortingOrder")
RegistClassMember(LuaGuildTerritorialWarsDrawMapPanel,"m_CurColor")
RegistClassMember(LuaGuildTerritorialWarsDrawMapPanel,"m_PressedThumb")

function LuaGuildTerritorialWarsDrawMapPanel:Awake()

	self.BrushSlider.OnChangeValue = DelegateFactory.Action_float(function(value)
		self:OnBrushSliderChanged(value)
	end)

	self.EraserSlider.OnChangeValue = DelegateFactory.Action_float(function(value)
		self:OnEraserSliderChanged(value)
	end)

	self.BrushButton.OnButtonSelected_02 = DelegateFactory.Action_QnSelectableButton_bool(function (btn, selected)
		self:OnBrushButtonSelected(btn, selected)
	end)

	self.EraserButton.OnButtonSelected_02 = DelegateFactory.Action_QnSelectableButton_bool(function (btn, selected)
		self:OnEraserButtonSelected(btn, selected)
	end)

	UIEventListener.Get(self.SelectColorButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSelectColorButtonClick()
	end)
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.BackButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBackButtonClick()
	end)


	
	UIEventListener.Get(self.ForwardButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnForwardButtonClick()
	end)


	
	UIEventListener.Get(self.ExitButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnExitButtonClick()
	end)


	
	UIEventListener.Get(self.ShareButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareButtonClick()
	end)

	UIEventListener.Get(self.BrushSlider.thumb.gameObject).onPress = DelegateFactory.BoolDelegate(function (go, isPressed)
        self.m_PressedThumb = isPressed
    end)

	UIEventListener.Get(self.EraserSlider.thumb.gameObject).onPress = DelegateFactory.BoolDelegate(function (go, isPressed)
        self.m_PressedThumb = isPressed
    end)
    --@endregion EventBind end
end

function LuaGuildTerritorialWarsDrawMapPanel:Init()
	self:ClearObjects()
	self.LineRendererTemplate.gameObject:SetActive(false)
	self.m_ScreenWidth = Screen.width
	self.m_ScreenHeight = Screen.height
	self.LineWidthSprite.width = self.DrawTexture.height / 2
	self.LineWidthSprite.height = self.DrawTexture.height / 2
	local lineWdith = 0
	self.m_LineRenderers = {}
	self.m_LineRenderersWidthList = {}
	self.m_CurVertexCount = 0
	self.m_CurIndex = 0
	self.m_LastLineSortingOrder = 0
	self.m_RenderTexture = RenderTexture.GetTemporary(self.m_ScreenWidth, self.m_ScreenHeight, 0, RenderTextureFormat.ARGB32,RenderTextureReadWrite.Default)
	if CommonDefs.IsWinSocialWndOpened() then
		self.m_RenderTexture = RenderTexture.GetTemporary(self.m_ScreenWidth * (1 - Constants.WinSocialWndRatio), self.m_ScreenHeight, 0, RenderTextureFormat.ARGB32,RenderTextureReadWrite.Default)
	end
	self.Camera.targetTexture = self.m_RenderTexture
	self.DrawTexture.mainTexture = self.m_RenderTexture
	self.BrushSlider.value = 0.1
	self.EraserSlider.value = 1
	self.BrushButton:SetSelected(true, false)
	self.EraserButton:SetSelected(false, false)
	self.LineWidthSprite.transform.localScale = Vector3(lineWdith, lineWdith, 1)
	self.m_RenderTexture:Release()
	self.m_RenderTexture:Create()
	self.m_CurLineRenderer = nil
	LuaGuildTerritorialWarsMgr.m_DrawColorIndex = 3
	local data = GuildTerritoryWar_Setting.GetData().CommandToolBrushColor[LuaGuildTerritorialWarsMgr.m_DrawColorIndex - 1]
	self.m_CurColor = NGUIText.ParseColor24(data[1],0)
	LuaGuildTerritorialWarsMgr.m_DrawColorIndex = 3
	Extensions.RemoveAllChildren(self.LineGroup.transform)
	self:UpdateBackAndForwardBtn()
end

function LuaGuildTerritorialWarsDrawMapPanel:OnEnable()
	self.BrushSlider.value = 0
	self.EraserSlider.value = 0
	self:Init()
	self.CloseButton.gameObject:SetActive(false)
	g_ScriptEvent:AddListener("OnGuildTerritorialWarsDrawColorSelect",self,"OnColorSelect")
	g_ScriptEvent:AddListener("SendPlayerShareCommandPicResult",self,"OnSendPlayerShareCommandPicResult")
end

function LuaGuildTerritorialWarsDrawMapPanel:OnDisable()
	self.BrushSlider.value = 0
	self.EraserSlider.value = 0
	self.CloseButton.gameObject:SetActive(true)
	g_ScriptEvent:RemoveListener("OnGuildTerritorialWarsDrawColorSelect",self,"OnColorSelect")
	g_ScriptEvent:RemoveListener("SendPlayerShareCommandPicResult",self,"OnSendPlayerShareCommandPicResult")
end

function LuaGuildTerritorialWarsDrawMapPanel:OnColorSelect()
	local data = GuildTerritoryWar_Setting.GetData().CommandToolBrushColor[LuaGuildTerritorialWarsMgr.m_DrawColorIndex - 1]
	self.m_CurColor = NGUIText.ParseColor24(data[1],0)
	self.SelectColorButton.transform:Find("Color"):GetComponent(typeof(UISprite)).color = self.m_CurColor
	self.LineWidthSprite.gameObject:GetComponent(typeof(UITexture)).color = self.m_CurColor
end

function LuaGuildTerritorialWarsDrawMapPanel:OnSendPlayerShareCommandPicResult(isSucc)
	if not isSucc then
		return
	end
	self.BtnsView.gameObject:SetActive(false)
	CTickMgr.Register(DelegateFactory.Action(function ()
		local filename = Utility.persistentDataPath .. "/screenshot.jpg"
		local data = CUICommonDef.DoCaptureScreen(filename, true, true, false)
		NativeTools.SaveImage(data)
		CTickMgr.Register(DelegateFactory.Action(function ()
			ShareBoxMgr.ImagePath = filename
			ShareBoxMgr.ShareType = EShareType.ShareChatPanelImage
			CUIManager.ShowUI(CUIResources.ShareBox3)
			if self.BtnsView then
				self.BtnsView.gameObject:SetActive(true)
			end
		end), 1000, ETickType.Once)
	end), 200, ETickType.Once)
end

function LuaGuildTerritorialWarsDrawMapPanel:OnDestroy()
	self:ClearObjects()
end

function LuaGuildTerritorialWarsDrawMapPanel:ClearObjects()
	RenderTexture.ReleaseTemporary(self.m_RenderTexture)
	self.m_RenderTexture = nil
	if self.m_LineRenderers then
		for i = 1, #self.m_LineRenderers do
			Object.Destroy(self.m_LineRenderers[i].material)
			self.m_LineRenderers[i].material = nil
		end
	end
end

function LuaGuildTerritorialWarsDrawMapPanel:Update()
	if self.m_ScreenWidth ~= Screen.width or self.m_ScreenHeight ~= Screen.height then
		self:Init()
		return
	end
	if self.BrushButton:isSeleted() or self.EraserButton:isSeleted() then
		if Input.GetMouseButtonDown(0) then
			self:InitNewLineRenderer()
		end
		if Input.GetMouseButton(0) and not self.m_PressedThumb then
			self:SetCurLineRenderer()
		end
		if Input.GetMouseButtonUp(0) then
			self:DeleteUselessLineRenderers()
		end
	end
end

function LuaGuildTerritorialWarsDrawMapPanel:InitNewLineRenderer()
	local obj = NGUITools.AddChild(self.LineGroup.gameObject, self.LineRendererTemplate.gameObject)
	self.m_LastLineSortingOrder = self.m_LastLineSortingOrder + 1
	obj.gameObject:SetActive(true)
	local lineRenderer = obj:GetComponent(typeof(LineRenderer))
	lineRenderer.material = Material(lineRenderer.material)
	local color = self.m_CurColor
	color.a = self.BrushButton:isSeleted() and 1 or 0
	lineRenderer.material:SetColor("_Color", color)
	local lineWdith = self.BrushButton:isSeleted() and self.m_BrushLineWidth or self.m_EraserLineWidth
	lineRenderer:SetWidth(lineWdith, lineWdith) 
	self.LineWidthSprite.transform.localScale = Vector3(lineWdith, lineWdith, 1)
	self.m_CurVertexCount = 0
	self.m_CurLineRenderer = lineRenderer
end

function LuaGuildTerritorialWarsDrawMapPanel:SetCurLineRenderer()
	if nil == self.m_LineRenderers then 
		return 
	end
	if nil == self.m_CurLineRenderer then 
		return 
	end
	local z = 2
	local pos = Vector3(Input.mousePosition.x, Input.mousePosition.y, z)
	pos = self.Camera:ScreenToWorldPoint(pos)
	if self.m_LastPosition and pos.x == self.m_LastPosition.x and pos.y == self.m_LastPosition.y then
		return
	end
	self.m_CurVertexCount = self.m_CurVertexCount + 1
	local curLineRenderer = self.m_CurLineRenderer
	local lineWdith = self.BrushButton:isSeleted() and self.m_BrushLineWidth or self.m_EraserLineWidth
	if curLineRenderer then
		curLineRenderer:SetWidth(lineWdith, lineWdith) 
		curLineRenderer:SetVertexCount(self.m_CurVertexCount)
		curLineRenderer:SetPosition(self.m_CurVertexCount - 1, pos)
		curLineRenderer.sortingOrder = self.m_LastLineSortingOrder
	end
	self.m_LastPosition = pos
end

function LuaGuildTerritorialWarsDrawMapPanel:DeleteUselessLineRenderers()
	local lastLineRenderer = self.m_CurLineRenderer
	if lastLineRenderer == nil then
		return
	end
	if self.m_CurVertexCount <= 5 then
		for _, lineRenderer in pairs(self.m_LineRenderers) do
			if lineRenderer == lastLineRenderer then
				return
			end
		end
		GameObject.Destroy(lastLineRenderer.gameObject)
	else
		self.m_CurIndex = self.m_CurIndex + 1
		for i = self.m_CurIndex, #self.m_LineRenderers do
			self.m_LineRenderers[i] = nil 
		end
		self.m_LineRenderers[self.m_CurIndex] = self.m_CurLineRenderer
		self:UpdateBackAndForwardBtn()
		local lineWdith = self.BrushButton:isSeleted() and self.m_BrushLineWidth or self.m_EraserLineWidth
		table.insert(self.m_LineRenderersWidthList,lineWdith)
	end
end

function LuaGuildTerritorialWarsDrawMapPanel:UpdateBackAndForwardBtn()
	self.BackButton.gameObject:GetComponent(typeof(UISprite)).alpha = (self.m_CurIndex <= 0) and 0.3 or 1
	self.ForwardButton.gameObject:GetComponent(typeof(UISprite)).alpha = (self.m_CurIndex >= #self.m_LineRenderers) and 0.3 or 1
end

--@region UIEvent

function LuaGuildTerritorialWarsDrawMapPanel:OnBrushSliderChanged(value)
	local commandToolBrushSize = GuildTerritoryWar_Setting.GetData().CommandToolBrushSize
	self.m_BrushLineWidth = math.lerp(commandToolBrushSize[0], commandToolBrushSize[1], value)
	self.LineWidthSprite.transform.localScale = Vector3(self.m_BrushLineWidth, self.m_BrushLineWidth, 1)
end

function LuaGuildTerritorialWarsDrawMapPanel:OnEraserSliderChanged(value)
	local commandToolEraserSize = GuildTerritoryWar_Setting.GetData().CommandToolEraserSize
	self.m_EraserLineWidth = math.lerp(commandToolEraserSize[0], commandToolEraserSize[1], value)
	self.LineWidthSprite.transform.localScale = Vector3(self.m_EraserLineWidth, self.m_EraserLineWidth, 1)
end

function LuaGuildTerritorialWarsDrawMapPanel:OnBackButtonClick()
	if self.m_CurIndex <= 0 then
		return
	end
	local curLineRenderer = self.m_LineRenderers[self.m_CurIndex]
	if curLineRenderer then
		curLineRenderer:SetWidth(0, 0) 
	end
	self.m_CurIndex = self.m_CurIndex - 1
	self:DeleteUselessLineRenderers()
	self.m_CurLineRenderer = nil
	self:UpdateBackAndForwardBtn()
end

function LuaGuildTerritorialWarsDrawMapPanel:OnForwardButtonClick()
	if self.m_CurIndex >= #self.m_LineRenderers then
		return
	end
	self.m_CurIndex = self.m_CurIndex + 1
	local lineWdith = self.m_LineRenderersWidthList[self.m_CurIndex]
	local curLineRenderer = self.m_LineRenderers[self.m_CurIndex]
	self:DeleteUselessLineRenderers()
	self.m_CurLineRenderer = nil
	if curLineRenderer then
		curLineRenderer:SetWidth(lineWdith, lineWdith) 
	end
	self:UpdateBackAndForwardBtn()
end

function LuaGuildTerritorialWarsDrawMapPanel:OnExitButtonClick()
	local msg = g_MessageMgr:FormatMessage("GuildTerritorialWarsDrawMapPanel_ExitButtonClick_Confirm")
	MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
		g_ScriptEvent:BroadcastInLua("CloseGuildTerritorialWarsDrawMapPanel")
	end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
end

function LuaGuildTerritorialWarsDrawMapPanel:OnShareButtonClick()
	if CClientMainPlayer.Inst and GameSetting_Common.GetData().PictureUpLevel > CClientMainPlayer.Inst.MaxLevel then
		g_MessageMgr:ShowMessage("GuildTerritorialWarsDrawMapPanel_Upload_Pic_Level",GameSetting_Common.GetData().PictureUpLevel)
		return
	elseif CommonDefs.IsAndroidPlatform() and Main.Inst.EngineVersion <= CPersonalSpaceMgr.AndroidSetPortraitNotSupportMaxVersion then
		g_MessageMgr:ShowMessage("DOWNLOAD_NEW_VERSION")
		return
	elseif not LoadPicMgr.EnableChatSendPic then
		g_MessageMgr:ShowMessage("FUNCTION_NOT_OPEN")
		return
	end
	Gac2Gas.RequestTerritoryWarDoDrawCommand()
end

function LuaGuildTerritorialWarsDrawMapPanel:OnSelectColorButtonClick()
	CUIManager.ShowUI(CLuaUIResources.GuildTerritorialWarsDrawColorWnd)
end

function LuaGuildTerritorialWarsDrawMapPanel:OnBrushButtonSelected(btn, selected)
	self.DrawTexture.transform:GetComponent(typeof(BoxCollider)).enabled = self.BrushButton:isSeleted() or self.EraserButton:isSeleted()
	self.BrushSlider.gameObject:SetActive(selected)
	if selected then
		self.EraserButton:SetSelected(false, false)
		self.LineWidthSprite.transform.localScale = Vector3(self.m_BrushLineWidth, self.m_BrushLineWidth, 1)
	end
	self.LineWidthSprite.gameObject:SetActive(self.EraserButton:isSeleted() or self.BrushButton:isSeleted())
end

function LuaGuildTerritorialWarsDrawMapPanel:OnEraserButtonSelected(btn, selected)
	self.DrawTexture.transform:GetComponent(typeof(BoxCollider)).enabled = self.BrushButton:isSeleted() or self.EraserButton:isSeleted()
	self.EraserSlider.gameObject:SetActive(selected)
	if selected then
		self.BrushButton:SetSelected(false, false)
		self.LineWidthSprite.transform.localScale = Vector3(self.m_EraserLineWidth, self.m_EraserLineWidth, 1)
	end
	self.LineWidthSprite.gameObject:SetActive(self.EraserButton:isSeleted() or self.BrushButton:isSeleted())
end
--@endregion UIEvent

