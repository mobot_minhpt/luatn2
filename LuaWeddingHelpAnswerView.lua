local DelegateFactory = import "DelegateFactory"
local UIEventListener = import "UIEventListener"
local CUIFx           = import "L10.UI.CUIFx"

LuaWeddingHelpAnswerView = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaWeddingHelpAnswerView, "fx")

function LuaWeddingHelpAnswerView:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    UIEventListener.Get(self.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButtonClick()
	end)

    self:InitUIComponents()
end

-- 初始化UI组件
function LuaWeddingHelpAnswerView:InitUIComponents()
    self.fx = self.transform:Find("ShowFx"):GetComponent(typeof(CUIFx))
end


function LuaWeddingHelpAnswerView:OnEnable()
    g_ScriptEvent:AddListener("SyncNewWeddingChoiceConfirm", self, "OnSyncChoiceConfirm")
    self.fx:LoadFx(g_UIFxPaths.WeddingHelpAnswerFxPath)
    self.fx.gameObject:SetActive(false)
end

function LuaWeddingHelpAnswerView:OnDisable()
    g_ScriptEvent:RemoveListener("SyncNewWeddingChoiceConfirm", self, "OnSyncChoiceConfirm")
    self.fx:DestroyFx()
end

-- 假新娘被询问
function LuaWeddingHelpAnswerView:OnSyncChoiceConfirm(senderId, choiceIdx)
    self.fx.gameObject:SetActive(true)
end

function LuaWeddingHelpAnswerView:Init()
end

--@region UIEvent

function LuaWeddingHelpAnswerView:OnButtonClick()
    Gac2Gas.ResumeNewWeddingConversation()
end

--@endregion UIEvent

