local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local Color = import "UnityEngine.Color"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local Constants = import "L10.Game.Constants"
local UISprite = import "UISprite"

LuaCakeCuttingRankWnd = class()
RegistChildComponent(LuaCakeCuttingRankWnd,"closeBtn", GameObject)
RegistChildComponent(LuaCakeCuttingRankWnd,"selfInfo", GameObject)
RegistChildComponent(LuaCakeCuttingRankWnd,"templateNode", GameObject)
RegistChildComponent(LuaCakeCuttingRankWnd,"table", UITable)
RegistChildComponent(LuaCakeCuttingRankWnd,"scrollView", UIScrollView)

RegistClassMember(LuaCakeCuttingRankWnd, "colorIndexTable")

function LuaCakeCuttingRankWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.CakeCuttingRankWnd)
end

function LuaCakeCuttingRankWnd:Split(str, pat)
   local t = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
         table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end

function LuaCakeCuttingRankWnd:OnEnable()
	--g_ScriptEvent:AddListener("LotteryAddSucc", self, "AddLotterySucc")
end

function LuaCakeCuttingRankWnd:OnDisable()
	--g_ScriptEvent:RemoveListener("LotteryAddSucc", self, "AddLotterySucc")
end

function LuaCakeCuttingRankWnd:InitNode(node,playerInfo,rank,gridNum,maxGridNum)
	local maxWidth = 234
	--local totalPixelNum = LuaCakeCuttingMgr.MapHeight * LuaCakeCuttingMgr.MapWidth

	local bg1 = node.transform:Find('bg1')
	local bg2 = node.transform:Find('bg2')
	if bg1 and bg2 then
		if rank % 2 == 0 then
			bg1.gameObject:SetActive(true)
			bg2.gameObject:SetActive(false)
		else
			bg1.gameObject:SetActive(false)
			bg2.gameObject:SetActive(true)
		end
	end

	node.transform:Find('name'):GetComponent(typeof(UILabel)).text = playerInfo.Name
	--playerInfo.KillNum
	node.transform:Find('info/num'):GetComponent(typeof(UILabel)).text = SafeStringFormat3('%.2f%%', gridNum * 100)
	local classNameTable = {LocalString.GetString("射手"),LocalString.GetString("甲士"), LocalString.GetString("刀客"),
	LocalString.GetString("侠客"), LocalString.GetString("方士"), LocalString.GetString("医师"),
	LocalString.GetString("魅者"), LocalString.GetString("异人"), LocalString.GetString("偃师"),
	LocalString.GetString("画魂"), LocalString.GetString("影灵")}
	local className = classNameTable[playerInfo.Class]
	if not className then
		className = LocalString.GetString("未知")
	end
	node.transform:Find('num'):GetComponent(typeof(UILabel)).text = className
	--node.transform:Find('info/percent'):GetComponent(typeof(UISprite)).width = gridNum / maxGridNum * maxWidth
	--if self.colorIndexTable[playerInfo.ColorIdx] then
	--	node.transform:Find('info/percent'):GetComponent(typeof(UISprite)).color = self.colorIndexTable[playerInfo.ColorIdx]
	--end
	if rank <= 0 then
		node.transform:Find('rank'):GetComponent(typeof(UILabel)).text = LocalString.GetString('未上榜')
	elseif rank > 3 then
		node.transform:Find('rank'):GetComponent(typeof(UILabel)).text = rank
	else
		local rankSprite = {Constants.RankFirstSpriteName,Constants.RankSecondSpriteName,Constants.RankThirdSpriteName}
		node.transform:Find('rank'):GetComponent(typeof(UILabel)).text = ''
		node.transform:Find('rank/spe').gameObject:SetActive(true)
		node.transform:Find('rank/spe'):GetComponent(typeof(UISprite)).spriteName = rankSprite[rank]
	end
end

function LuaCakeCuttingRankWnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	self.colorIndexTable = {}
	for i = 1,10 do
		local colorString = ZhouNianQing_CakeCuttingColor.GetData(i).Color
		local colorTable = self:Split(colorString,',')
		self.colorIndexTable[i] = Color(colorTable[1]/255,colorTable[2]/255,colorTable[3]/255)
	end

	self.templateNode:SetActive(false)

	local maxValue = 1
	if LuaCakeCuttingMgr.PlayerServerRankTable and #LuaCakeCuttingMgr.PlayerServerRankTable > 0 then
		maxValue = LuaCakeCuttingMgr.PlayerServerRankTable[1].value
		for i,v in ipairs(LuaCakeCuttingMgr.PlayerServerRankTable) do
			local playerInfo = v
			if playerInfo.Rank > 0 then
				local node = NGUITools.AddChild(self.table.gameObject, self.templateNode)
				node:SetActive(true)
				self:InitNode(node,playerInfo,v.Rank,v.AreaRate,maxValue)
			end
			--if v.PlayerId == CClientMainPlayer.Inst.Id then
				--self:InitNode(self.selfInfo,playerInfo,v.Rank,v.Area,maxValue)
			--end
		end
	end
	if LuaCakeCuttingMgr.PlayerServerRankSelfInfo then
		local v = LuaCakeCuttingMgr.PlayerServerRankSelfInfo
		self:InitNode(self.selfInfo,v,v.Rank,v.AreaRate,maxValue)
	end
	LuaCakeCuttingMgr.PlayerServerRankTable = nil

  self.table:Reposition()
  self.scrollView:ResetPosition()

end

return LuaCakeCuttingRankWnd
