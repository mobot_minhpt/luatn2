local VARBINARY = import "L10.Game.Properties.VARBINARY"
local CTalismanMgr = import "L10.Game.CTalismanMgr"

LuaTalismanMgr = {}

--这里的判断只能针对特殊法宝外观
function LuaTalismanMgr:CheckContain(id)
	local fid = id % 100
	local level = math.floor(id/100)
	local maxLevel = 4
	for i=level,maxLevel do
		local checkId = fid + i * 100
		if checkId >= id then
			local got = CommonDefs.ListContains(CClientMainPlayer.Inst.TalismanAppearanceList, typeof(Int32), checkId)
			if got then
				return true
			end
		end
	end
	return false
end

function LuaTalismanMgr:CheckTalismanReturnOpen()
	local result = false
  ItemGet_TalismanFX.Foreach(function (key, data)
    --if data.TalismanID > 100 then
      local got = LuaTalismanMgr:CheckContain(data.TalismanID)
      if got then
				result = true
      end
    --end
  end)

  return result
end

function LuaTalismanMgr:GetFixWordCount(id)
	local lockData = Talisman_XianJiaAppearanceLevel.GetData(id)
  if lockData then
    return lockData.FixWordCount
  end
end

LuaTalismanMgr.m_FilteredTalismanAppearanceList = nil
function LuaTalismanMgr:GetMainPlayerTalismanAppearanceList()
    if self.m_FilteredTalismanAppearanceList == nil then
        self.m_FilteredTalismanAppearanceList = CreateFromClass(MakeGenericClass(List, Int32))
    end
    CommonDefs.ListClear(self.m_FilteredTalismanAppearanceList)
    local allTalismanIds = {}
    if CClientMainPlayer.Inst then
        CommonDefs.ListIterate(CClientMainPlayer.Inst.TalismanAppearanceList, DelegateFactory.Action_object(function(___value)
            local talismanId = tonumber(___value) --TalismanID为特殊法宝特效时，对应策划表ItemGet_TalismanFx/Talisman_XianJiaAppearanceLevel中的TalismanID
            if LuaAppearancePreviewMgr.m_EnableNewAppearanceWnd then
                --新界面，针对1-4阶法宝外观的特效ID需要去重
                local fxId = CTalismanMgr.Inst:FindTalismanFx(talismanId % 100)
                if allTalismanIds[fxId] then return end
                allTalismanIds[fxId] = talismanId
            else
                --旧界面，针对1-4阶法宝外观进行低等级剔除，每阶法宝只保留一个外观
                local lv,grade,count=math.ceil(talismanId / 100), math.floor((talismanId % 100) / 10), talismanId % 10
                if grade>=1 and grade<=4 then 
                    if not allTalismanIds[grade] or allTalismanIds[grade]<talismanId then
                        allTalismanIds[grade] = talismanId
                    end
                else
                    allTalismanIds[talismanId] = talismanId
                end
            end
        end))
        for __,id in pairs(allTalismanIds) do
            CommonDefs.ListAdd_LuaCall(self.m_FilteredTalismanAppearanceList, id)
        end
    end
    return self.m_FilteredTalismanAppearanceList
end
----------------------------------
function Gas2Gac.SyncUnLockSpecialFaBaoFxLevelUp(info_U)
	if CClientMainPlayer.Inst == nil then
		return
	end

	--CClientMainPlayer.Inst.PlayProp.PersistPlayData[EnumPersistPlayDataKey_lua.eUnLockSpecialFaBaoFxLevelUp] = new VARBINARY()
	if not CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.PersistPlayData, typeof(UInt32), EnumPersistPlayDataKey_lua.eUnLockSpecialFaBaoFxLevelUp) then
		CClientMainPlayer.Inst.PlayProp.PersistPlayData[EnumPersistPlayDataKey_lua.eUnLockSpecialFaBaoFxLevelUp]	= VARBINARY()
	end
	local data = CommonDefs.ASCIIEncoding_GetBytes(info_U)
	local bufLen = data.Length
	local startIndex = 0
	local default
	default, bufLen, startIndex = CClientMainPlayer.Inst.PlayProp.PersistPlayData[EnumPersistPlayDataKey_lua.eUnLockSpecialFaBaoFxLevelUp]:LoadFromBytes(data, bufLen, bufLen, startIndex)
end

function Gas2Gac.GetSpecialFaBaoFxSuccess(appearanceKey)
	g_ScriptEvent:BroadcastInLua("TalismanFxUnlock",appearanceKey)
end
