-- Auto Generated!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Color = import "UnityEngine.Color"
local CSnowBallFightInfoWnd = import "L10.UI.CSnowBallFightInfoWnd"
local CSnowBallMgr = import "L10.Game.CSnowBallMgr"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local NGUITools = import "NGUITools"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
CSnowBallFightInfoWnd.m_Init_CS2LuaHook = function (this)
    UIEventListener.Get(this.closeBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        this:Close()
    end)

    this.templdateNode:SetActive(false)

    this:InitList()
end
CSnowBallFightInfoWnd.m_InitList_CS2LuaHook = function (this)
    Extensions.RemoveAllChildren(this.table.transform)
    if CSnowBallMgr.Inst.snowRankList.Count > 0 then
        do
            local i = 0
            while i < CSnowBallMgr.Inst.snowRankList.Count do
                local info = CSnowBallMgr.Inst.snowRankList[i]
                local node = NGUITools.AddChild(this.table.gameObject, this.templdateNode)
                node:SetActive(true)

                local name = CommonDefs.GetComponent_Component_Type(node.transform:Find("Name/text"), typeof(UILabel))
                local rank = CommonDefs.GetComponent_Component_Type(node.transform:Find("rank"), typeof(UILabel))
                local kill = CommonDefs.GetComponent_Component_Type(node.transform:Find("kill"), typeof(UILabel))
                local score = CommonDefs.GetComponent_Component_Type(node.transform:Find("score"), typeof(UILabel))

                if info.id == CClientMainPlayer.Inst.Id then
                    name.color = Color.green
                    rank.color = Color.green
                    kill.color = Color.green
                    score.color = Color.green
                elseif info.teammate then
                    name.color = Color.yellow
                    rank.color = Color.yellow
                    kill.color = Color.yellow
                    score.color = Color.yellow
                else
                    if info.executed then
                        name.color = Color.grey
                        rank.color = Color.grey
                        kill.color = Color.grey
                        score.color = Color.grey
                    else
                        name.color = Color.white
                        rank.color = Color.white
                        kill.color = Color.white
                        score.color = Color.white
                    end
                end

                name.text = info.name
                if info.rank <= 0 then
                    rank.text = "-"
                else
                    rank.text = tostring(info.rank)
                end
                kill.text = tostring(info.killNum)
                score.text = tostring(info.score)
                i = i + 1
            end
        end
        this.table:Reposition()
        this.scrollView:ResetPosition()
    end
end
