local CButton = import "L10.UI.CButton"
local UIProgressBar = import "UIProgressBar"
local CUIFx = import "L10.UI.CUIFx"
local QnButton = import "L10.UI.QnButton"
local UILabel = import "UILabel"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local Transform = import "UnityEngine.Transform"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local Ease = import "DG.Tweening.Ease"
local Color = import "UnityEngine.Color"
local Vector4 = import "UnityEngine.Vector4"
local CUICommonDef = import "L10.UI.CUICommonDef"
local UITexture = import "UITexture"
local CTaskMgr = import "L10.Game.CTaskMgr"
local CTickMgr = import "L10.Engine.CTickMgr"
local ETickType = import "L10.Engine.ETickType"
local UISprite = import "UISprite"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local UIPanel = import "UIPanel"
local LuaTweenUtils = import "LuaTweenUtils"
local ShareMgr = import "ShareMgr"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CCommonLuaWnd = import "L10.UI.CCommonLuaWnd"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CUITexture = import "L10.UI.CUITexture"
local CImageSpreadEffectHelper = import "CImageSpreadEffectHelper"
local PathMode = import "DG.Tweening.PathMode"
local PathType = import "DG.Tweening.PathType"
local CTrackMgr = import "L10.Game.CTrackMgr"
local CPos = import "L10.Engine.CPos"
local CStatusMgr = import "L10.Game.CStatusMgr"
local CChatLinkMgr = import "CChatLinkMgr"
local EnumEvent = import "L10.Game.EnumEvent"
local DateTime = import "System.DateTime"

LuaShanYeMiZongMainWnd = class()
LuaShanYeMiZongMainWnd.s_OpenWndAndChooseClueId = nil
LuaShanYeMiZongMainWnd.s_UnlockNewExtraTask = false
LuaShanYeMiZongMainWnd.s_OpenWndWithInfo = nil

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShanYeMiZongMainWnd, "ExtraStoryTexture", "ExtraStoryTexture", CImageSpreadEffectHelper)
RegistChildComponent(LuaShanYeMiZongMainWnd, "MainView", "MainView", GameObject)
RegistChildComponent(LuaShanYeMiZongMainWnd, "Tab1", "Tab1", QnSelectableButton)
RegistChildComponent(LuaShanYeMiZongMainWnd, "Tab2", "Tab2", QnSelectableButton)
RegistChildComponent(LuaShanYeMiZongMainWnd, "Tab3", "Tab3", QnSelectableButton)
RegistChildComponent(LuaShanYeMiZongMainWnd, "Tab4", "Tab4", QnSelectableButton)
RegistChildComponent(LuaShanYeMiZongMainWnd, "ActivityTimeLabel", "ActivityTimeLabel", UILabel)
RegistChildComponent(LuaShanYeMiZongMainWnd, "ChapterTip", "ChapterTip", QnButton)
RegistChildComponent(LuaShanYeMiZongMainWnd, "ClueTitleLabel", "ClueTitleLabel", UILabel)
RegistChildComponent(LuaShanYeMiZongMainWnd, "ClueSubLabel", "ClueSubLabel", UILabel)
RegistChildComponent(LuaShanYeMiZongMainWnd, "ClueDescLabel", "ClueDescLabel", UILabel)
RegistChildComponent(LuaShanYeMiZongMainWnd, "CluePicTexture", "CluePicTexture", CUITexture)
RegistChildComponent(LuaShanYeMiZongMainWnd, "CluePicDescLabel", "CluePicDescLabel", UILabel)
RegistChildComponent(LuaShanYeMiZongMainWnd, "ClueGainBtn", "ClueGainBtn", QnButton)
RegistChildComponent(LuaShanYeMiZongMainWnd, "ClueGainFx", "ClueGainFx", CUIFx)
RegistChildComponent(LuaShanYeMiZongMainWnd, "ClueHint", "ClueHint", GameObject)
RegistChildComponent(LuaShanYeMiZongMainWnd, "ChapterUnlockLabel", "ChapterUnlockLabel", UILabel)
RegistChildComponent(LuaShanYeMiZongMainWnd, "ChapterRewardBtn", "ChapterRewardBtn", QnButton)
RegistChildComponent(LuaShanYeMiZongMainWnd, "ChapterRewardFx", "ChapterRewardFx", CUIFx)
RegistChildComponent(LuaShanYeMiZongMainWnd, "GuessProgress", "GuessProgress", UIProgressBar)
RegistChildComponent(LuaShanYeMiZongMainWnd, "RetroButton", "RetroButton", QnButton)
RegistChildComponent(LuaShanYeMiZongMainWnd, "TipButton", "TipButton", QnButton)
RegistChildComponent(LuaShanYeMiZongMainWnd, "Guess1", "Guess1", CButton)
RegistChildComponent(LuaShanYeMiZongMainWnd, "Guess2", "Guess2", CButton)
RegistChildComponent(LuaShanYeMiZongMainWnd, "Guess3", "Guess3", CButton)
RegistChildComponent(LuaShanYeMiZongMainWnd, "RealMurderer", "RealMurderer", GameObject)
RegistChildComponent(LuaShanYeMiZongMainWnd, "FinalReward", "FinalReward", QnButton)
RegistChildComponent(LuaShanYeMiZongMainWnd, "FinalRewardFx", "FinalRewardFx", CUIFx)
RegistChildComponent(LuaShanYeMiZongMainWnd, "ExtraStoryHint", "ExtraStoryHint", GameObject)
RegistChildComponent(LuaShanYeMiZongMainWnd, "RewardItem", "RewardItem", GameObject)
RegistChildComponent(LuaShanYeMiZongMainWnd, "TaskGroup", "TaskGroup", GameObject)
RegistChildComponent(LuaShanYeMiZongMainWnd, "ExtraStoryView", "ExtraStoryView", GameObject)
RegistChildComponent(LuaShanYeMiZongMainWnd, "ClueBg", "ClueBg", GameObject)
RegistChildComponent(LuaShanYeMiZongMainWnd, "MiWuLingBtn", "MiWuLingBtn", QnButton)

--@endregion RegistChildComponent end

RegistClassMember(LuaShanYeMiZongMainWnd, "m_Panel")
RegistClassMember(LuaShanYeMiZongMainWnd, "m_ClueInfo") -- 线索信息，[clueId] = rewarded
RegistClassMember(LuaShanYeMiZongMainWnd, "m_ChapterInfo") --章节信息，[chapterId] = {rewarded, clueCount}
RegistClassMember(LuaShanYeMiZongMainWnd, "m_JoinedGuess") -- 已参加的猜想
RegistClassMember(LuaShanYeMiZongMainWnd, "m_Tweeners")
RegistClassMember(LuaShanYeMiZongMainWnd, "m_CurTabId")
RegistClassMember(LuaShanYeMiZongMainWnd, "m_CurClueId")
RegistClassMember(LuaShanYeMiZongMainWnd, "m_Stage") -- val∈{1,2,3,4}，表示当前属于哪一阶段，影响标签页的解锁和默认、推理进度条和线索的自动解锁
                                                                        -- 每一幕进入猜想时，将认为属于下一阶段（下一幕）
                                                                        -- 4表示番外阶段，此时第三幕已结束，但标签页仍默认显示第三幕
RegistClassMember(LuaShanYeMiZongMainWnd, "m_bIsRevealMurderer") -- 当前阶段为4时，不一定已经揭露真凶，还可能处于猜想阶段而不解锁番外
RegistClassMember(LuaShanYeMiZongMainWnd, "m_FinalRewarded") -- 是否领取最终奖励
RegistClassMember(LuaShanYeMiZongMainWnd, "m_ExtraRewarded") -- 是否领取番外奖励
RegistClassMember(LuaShanYeMiZongMainWnd, "m_ProgressTick") -- 全服推理进度和猜想的tick
RegistClassMember(LuaShanYeMiZongMainWnd, "m_InGuess") -- 1,2,3 表示在对应幕的猜想；0表示不在猜想
RegistClassMember(LuaShanYeMiZongMainWnd, "m_curGuessEndStamp") -- 缓存当前猜想结束时间
RegistClassMember(LuaShanYeMiZongMainWnd, "m_ClueBtn") -- id:0..9
RegistClassMember(LuaShanYeMiZongMainWnd, "m_DailySpeedWeight") 
RegistClassMember(LuaShanYeMiZongMainWnd, "m_TotalSpeedWeight") 
RegistClassMember(LuaShanYeMiZongMainWnd, "m_SpeedWeightDict")

function LuaShanYeMiZongMainWnd:Awake()
    self.m_Panel = self.transform:GetComponent(typeof(UIPanel))
    self.m_Panel.alpha = 0

    self.m_SpeedWeightDict = {}
    self.m_DailySpeedWeight = 0
    ShuJia2022_SpeedPerTime.Foreach(function(k, v)
        self.m_DailySpeedWeight = self.m_DailySpeedWeight + v.Speed
        self.m_SpeedWeightDict[k] = v.Speed
    end)

    --进度条零点 6-30 10:00
    --进度条跑到猜想三 7-10 12:00
    --跑满 7-11 12:00
    --猜想三到揭晓真凶这一段匀速
    self.m_TotalSpeedWeight = self.m_DailySpeedWeight * 10 + self.m_SpeedWeightDict[10] + self.m_SpeedWeightDict[11]

    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.Tab1.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTab1Click()
	end)


	
	UIEventListener.Get(self.Tab2.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTab2Click()
	end)


	
	UIEventListener.Get(self.Tab3.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTab3Click()
	end)


	
	UIEventListener.Get(self.Tab4.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTab4Click()
	end)


	
	UIEventListener.Get(self.ChapterTip.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChapterTipClick()
	end)


	
	UIEventListener.Get(self.ClueGainBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnClueGainBtnClick()
	end)


	
	UIEventListener.Get(self.ChapterRewardBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChapterRewardBtnClick()
	end)


	
	UIEventListener.Get(self.RetroButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRetroButtonClick()
	end)


	
	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)


	
	UIEventListener.Get(self.Guess1.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGuess1Click()
	end)


	
	UIEventListener.Get(self.Guess2.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGuess2Click()
	end)


	
	UIEventListener.Get(self.Guess3.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGuess3Click()
	end)


	
	UIEventListener.Get(self.RealMurderer.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRealMurdererClick()
	end)


	
	UIEventListener.Get(self.FinalReward.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnFinalRewardClick()
	end)


	
	UIEventListener.Get(self.MiWuLingBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMiWuLingBtnClick()
	end)


    --@endregion EventBind end

    self.m_Tweeners = {}
    self.m_ClueBtn = {}
end

function LuaShanYeMiZongMainWnd:Init()
    self.ActivityTimeLabel.text = ShuJia2022_Setting.GetData().ShanYeMiZongTime
    self.transform:Find("Anchor/Bg/Fx"):GetComponent(typeof(CUIFx)):LoadFx("Fx/UI/Prefab/UI_Shanye_guangshu.prefab")
    self.transform:Find("Anchor/Bg/Fx"):GetComponent(typeof(UITexture)).alpha = 0
    if LuaShanYeMiZongMainWnd.s_OpenWndWithInfo then
        self:RefreshAll(unpack(LuaShanYeMiZongMainWnd.s_OpenWndWithInfo))
        LuaShanYeMiZongMainWnd.s_OpenWndWithInfo = nil
    else
        Gac2Gas.QueryShanYeMiZongInfo()
    end
end

function LuaShanYeMiZongMainWnd:OnEnable()
    g_ScriptEvent:AddListener("SendShanYeMiZongInfo", self, "RefreshAll")
end

function LuaShanYeMiZongMainWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendShanYeMiZongInfo", self, "RefreshAll")
end

function LuaShanYeMiZongMainWnd:OnDestroy()
    self:ClearTweeners()
    self:CancelProgressTick()
end

function LuaShanYeMiZongMainWnd:OnTabClick(tabId)
    if tabId < 4 and self.m_Stage and self.m_Stage >= tabId then
        self:ChooseChapter(tabId)
    elseif tabId == 4 and self.m_bIsRevealMurderer then
        self:ChangeToTab(tabId)
    else
        g_MessageMgr:ShowMessage("ShuJia2022_Chapter_Not_Open")
        --print("Tab Lock:"..tabId)
    end
end

function LuaShanYeMiZongMainWnd:OnGuessClick(guessId)
    if self.m_InGuess == guessId then
        LuaShanYeMiZongGuessWnd.s_GuessId = guessId
        LuaShanYeMiZongGuessWnd.s_GuessState = self.m_JoinedGuess[guessId] and 1 or 0
        CUIManager.ShowUI(CLuaUIResources.ShanYeMiZongGuessWnd)
    elseif self.m_Stage > guessId then
        LuaShanYeMiZongGuessWnd.s_GuessId = guessId
        if self.m_bIsRevealMurderer then
            LuaShanYeMiZongGuessWnd.s_GuessState = 2
        else
            LuaShanYeMiZongGuessWnd.s_GuessState = 1
        end
        CUIManager.ShowUI(CLuaUIResources.ShanYeMiZongGuessWnd)
    else
        --print("Guess Lock:"..guessId)
        g_MessageMgr:ShowMessage("ShuJia2022_CaiXiang_Not_Open")
    end
end

-- 按钮事件的绑定和按钮UI表现分离，全部单独进行
-- UI点击事件需要collider
--@region UIEvent

function LuaShanYeMiZongMainWnd:OnTab1Click()
    self:OnTabClick(1)
end

function LuaShanYeMiZongMainWnd:OnTab2Click()
    self:OnTabClick(2)
end

function LuaShanYeMiZongMainWnd:OnTab3Click()
    self:OnTabClick(3)
end

function LuaShanYeMiZongMainWnd:OnTab4Click()
    self:OnTabClick(4)
end

function LuaShanYeMiZongMainWnd:OnChapterTipClick()
    for j = 0, 9 do
        self.m_ClueBtn[j]:SetSelected(false)
    end
    self:ShowChapterInfo(self.m_CurTabId)
end

function LuaShanYeMiZongMainWnd:OnRetroButtonClick()
    CUIManager.ShowUI(CLuaUIResources.ShanYeMiZongRetroWnd)
end

function LuaShanYeMiZongMainWnd:OnTipButtonClick()
    g_MessageMgr:ShowMessage("ShuJia2022_Shanyemizong_Tips")
end

function LuaShanYeMiZongMainWnd:OnRealMurdererClick()
    if self.m_bIsRevealMurderer then -- 交互修改: 现在没有揭晓真凶之前不让打开了
        LuaShanYeMiZongRevealMurdererWnd.s_bIsRevealMurderer = self.m_bIsRevealMurderer
        CUIManager.ShowUI(CLuaUIResources.ShanYeMiZongRevealMurdererWnd)
    else
        g_MessageMgr:ShowMessage("ShuJia2022_XiongShou_Not_Open")
    end
end

function LuaShanYeMiZongMainWnd:OnFinalRewardClick()
    if not self.m_ClueInfo or not next(self.m_ClueInfo) then
        g_MessageMgr:ShowMessage("ShuJia2022_XianSuo_Not_Get")
    elseif self.m_bIsRevealMurderer then
        CUIManager.ShowUI(CLuaUIResources.ShanYeMiZongRewardWnd)
    else
        g_MessageMgr:ShowMessage("ShuJia2022_XiongShou_Not_Open")
    end
end

function LuaShanYeMiZongMainWnd:OnGuess1Click()
    self:OnGuessClick(1)
end

function LuaShanYeMiZongMainWnd:OnGuess2Click()
    self:OnGuessClick(2)
end

function LuaShanYeMiZongMainWnd:OnGuess3Click()
    self:OnGuessClick(3)
end

function LuaShanYeMiZongMainWnd:OnClueGainBtnClick()
    local rewarded = self.m_ClueInfo[self.m_CurClueId]
    if rewarded then
        g_MessageMgr:ShowMessage("ShuJia2022_Get_Reward_Already")
        --print("ClueGainBtn Rewarded")
    else
        Gac2Gas.ShanYeMiZongRequestGetXianSuoReward(self.m_CurClueId)
    end
end

function LuaShanYeMiZongMainWnd:OnChapterRewardBtnClick()
    local info = self.m_ChapterInfo[self.m_CurTabId]
    if info.clueCount == 10 then
        if info.rewarded then 
            CItemInfoMgr.ShowLinkItemTemplateInfo(ShuJia2022_Chapter.GetData(self.m_CurTabId).Reward)
            --print("ChapterRewardBtn Rewarded")
        else
            Gac2Gas.ShanYeMiZongRequestGetChapterReward(self.m_CurTabId)
        end
    else
        CItemInfoMgr.ShowLinkItemTemplateInfo(ShuJia2022_Chapter.GetData(self.m_CurTabId).Reward)
        --print("ChapterRewardBtn Lock")
    end
end

function LuaShanYeMiZongMainWnd:OnRewardItemClick()
    if not CClientMainPlayer.Inst.TaskProp:IsMainPlayerTaskFinished(ShuJia2022_Setting.GetData().FanWaiTaskID) then
        return
    end
    if not self.m_ExtraRewarded then
        Gac2Gas.ShanYeMiZongRequestGetExtraTaskReward()
    end
end


function LuaShanYeMiZongMainWnd:OnMiWuLingBtnClick()
    --[[
    --改成传送
    CUIManager.CloseUI(CLuaUIResources.ShanYeMiZongMainWnd)
    local pos = CPos(ShuJia2022_Setting.GetData().MiWuLingPosition[0], ShuJia2022_Setting.GetData().MiWuLingPosition[1])
    pos = Utility.GridPos2PixelPos(pos)
    CTrackMgr.Inst:Track(nil, ShuJia2022_Setting.GetData().MiWuLingMapID, pos, Utility.Grid2Pixel(2.0), nil, nil, nil)
    --]]
    g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("MIWULING_CONFIRM"), function()
        CUIManager.CloseUI(CLuaUIResources.ShanYeMiZongMainWnd)
        if CClientMainPlayer.Inst then
            -- 服务器有处理打断 
            -- CStatusMgr.Inst:ApplyInterrupt(CClientMainPlayer.Inst, EnumEvent.GetId("LevelTeleport"))
            Gac2Gas.LevelTeleport(ShuJia2022_Setting.GetData().MiWuLingMapID)
        end
    end, nil, nil, nil, false)
end


--@endregion UIEvent

-- 线索id对应第几幕(1~3)
function LuaShanYeMiZongMainWnd.ToTabId(clueId) 
    return math.floor((clueId - 1) / 10) + 1
end

-- 线索id对应幕的局部id(0~9)
function LuaShanYeMiZongMainWnd.ToTabClueId(clueId) 
    return math.fmod(clueId - 1, 10)
end

-- enabled=false时UI置灰，通过置灰sprite可以模拟未激活但可触发点击的按钮
function LuaShanYeMiZongMainWnd.SetEnabled(trans, enabled)
    local pos = trans.position
    pos.z = enabled and 0 or -1
    trans.position = pos
end

-- 只由Gas2Gac调用，使用服务器数据全部重刷界面
-- 触发时机：
-- 1. 打开界面时
-- 2. 玩家在界面上进行操作并成功同步到服务器时
-- 3. 进入下一阶段或猜想结束时
function LuaShanYeMiZongMainWnd:RefreshAll(finalRewarded, extraTaskRewarded, clue, chapter, guess)
    self.m_FinalRewarded = finalRewarded
    self.m_ExtraRewarded = extraTaskRewarded
    self.m_ClueInfo = clue
    self.m_ChapterInfo = chapter
    self.m_JoinedGuess = guess

    self:CheckStage()
    if self.m_bIsRevealMurderer then
        self.RetroButton.gameObject:SetActive(true)
    else
        self.RetroButton.gameObject:SetActive(false)
    end

    self:InitTab()
    self:InitGuessProgress()

    if LuaShanYeMiZongMainWnd.s_OpenWndAndChooseClueId then
        self:ChooseClue(LuaShanYeMiZongMainWnd.s_OpenWndAndChooseClueId)
        LuaShanYeMiZongMainWnd.s_OpenWndAndChooseClueId = nil
    elseif LuaShanYeMiZongMainWnd.s_UnlockNewExtraTask then
        self:ChangeToTab(4)
    else
        if self.m_CurClueId and self.m_CurClueId > 0 then
            self:ChooseClue(self.m_CurClueId)
        elseif self.m_CurTabId then
            self:ChangeToTab(self.m_CurTabId)
        else
            self:ChooseChapter(math.min(3, self.m_Stage))
        end
    end

    self:CancelProgressTick()
    self:ProgressTickFunc()
    self.m_ProgressTick = CTickMgr.Register(DelegateFactory.Action(function ()
        self:ProgressTickFunc()
    end), 1000, ETickType.Loop)

    self.m_Panel.alpha = 1
end

-- 检查属于哪个阶段和是否已揭晓真凶
function LuaShanYeMiZongMainWnd:CheckStage()
    self.m_InGuess = 0
    local date
    for i = 1, 3 do 
        local t = ShuJia2022_Chapter.GetData(i).Time -- 每一幕的结束时间，也是进入猜想的时间
        local d = {}
        for s in string.gmatch(t, '%d+') do 
            table.insert(d, s)
        end
        date = {
            year = tonumber(d[1]), month = tonumber(d[2]), day = tonumber(d[3]),
            hour = tonumber(d[4]), min = tonumber(d[5]), sec = 0
        }
        if os.time(date) > CServerTimeMgr.Inst.timeStamp then
            self.m_Stage = i
            return
        else
            date.day = date.day + 1
            if i == 3 then
                self.m_Stage = 4
                local stamp = os.time(date)
                if stamp > CServerTimeMgr.Inst.timeStamp then
                    self.m_InGuess = i
                    self.m_curGuessEndStamp = stamp
                else
                    self.m_bIsRevealMurderer = true
                end
            else
                local stamp = os.time(date)
                if stamp > CServerTimeMgr.Inst.timeStamp then
                    self.m_InGuess = i
                    self.m_curGuessEndStamp = stamp
                end
            end
        end
    end
end

-- 根据当前阶段刷新tab的解锁状态
function LuaShanYeMiZongMainWnd:InitTab()
    if self.m_bIsRevealMurderer then
        self:InitOneTab(self.Tab4, true)
    else
        self:InitOneTab(self.Tab4, false)
    end
    local tabId = math.min(3, self.m_Stage)
    for i = 1, 3 do
        if i > tabId then
            self:InitOneTab(self["Tab"..i], false)
        else
            self:InitOneTab(self["Tab"..i], true)
            local info = self.m_ChapterInfo[i]
            local redDot = self["Tab"..i].transform:Find("RedDot").gameObject
            redDot:SetActive(false)
            if not info.rewarded and info.clueCount == 10 then
                redDot:SetActive(true)
            else
                for j = (i - 1) * 10 + 1, i * 10 do
                    if self.m_ClueInfo[j] == false then
                        redDot:SetActive(true)
                        break
                    end
                end
            end
        end
    end
    if self.m_Stage >= 3 then
        self.MiWuLingBtn.gameObject:SetActive(true)
        self.MiWuLingBtn.transform:Find("Fx"):GetComponent(typeof(CUIFx)):LoadFx("Fx/UI/Prefab/UI_Shanye_jiaoyin.prefab")
        self.MiWuLingBtn.transform:Find("Fx"):GetComponent(typeof(UITexture)).alpha = 0
    else
        self.MiWuLingBtn.gameObject:SetActive(false)
        self.MiWuLingBtn.transform:Find("Fx"):GetComponent(typeof(CUIFx)):DestroyFx()
    end
end

function LuaShanYeMiZongMainWnd:InitOneTab(tab, open)
    tab.Selectable = false -- SetSelected Manually
    if open then
        tab.transform:Find("NormalLabel").gameObject:SetActive(true)
        tab.transform:Find("LockSprite").gameObject:SetActive(false)
    else
        tab.transform:Find("NormalLabel").gameObject:SetActive(false)
        tab.transform:Find("LockSprite").gameObject:SetActive(true)
    end
end

function LuaShanYeMiZongMainWnd:CurGuessTimeLabel()
    if self.m_InGuess > 0 then
        return self["Guess"..self.m_InGuess].transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
    end
end

-- 根据当前阶段刷新全局推理进度条上的猜想、真凶、最终奖励
function LuaShanYeMiZongMainWnd:InitGuessProgress()
    --self.GuessProgress.value = self.m_Stage / 4
    for i = 1, 3 do
        local guess = self["Guess"..i]
        if i >= self.m_Stage then -- 未开放
            self.SetEnabled(guess.transform:Find("Sprite"), false)
            guess.transform:Find("KuangFx"):GetComponent(typeof(CUIFx)):DestroyFx()
            guess.transform:Find("TimeLabel").gameObject:SetActive(false)
            guess.transform:Find("UnlockLabel").gameObject:SetActive(false)
        else
            self.SetEnabled(guess.transform:Find("Sprite"), true)
            if i == self.m_InGuess and not self.m_JoinedGuess[i] then -- 进行猜想中
                self:CurGuessTimeLabel().gameObject:SetActive(true)
                local diff = LuaShanYeMiZongMgr.timediff(self.m_curGuessEndStamp, CServerTimeMgr.Inst.timeStamp)
                if diff.hour < 10 then diff.hour = "0"..diff.hour end
                if diff.min < 10 then diff.min = "0"..diff.min end
                self:CurGuessTimeLabel().text = diff.hour..":"..diff.min..LocalString.GetString("后结束")
                self:LoadKuangFx(guess.transform:Find("KuangFx"):GetComponent(typeof(CUIFx)), guess.transform:Find("Sprite"))
                guess.transform:Find("UnlockLabel").gameObject:SetActive(false)
            else -- 已开放or已揭晓
                guess.transform:Find("KuangFx"):GetComponent(typeof(CUIFx)):DestroyFx()
                guess.transform:Find("TimeLabel").gameObject:SetActive(false)
                if self.m_bIsRevealMurderer then
                    guess.transform:Find("UnlockLabel").gameObject:SetActive(true)
                else
                    guess.transform:Find("UnlockLabel").gameObject:SetActive(false)
                end
            end
        end
    end
    if self.m_bIsRevealMurderer then
        self.RealMurderer.transform:Find("Label"):GetComponent(typeof(UILabel)).color = NGUIText.ParseColor("C8836B", 0)
        self.RealMurderer.transform:Find("Profile").gameObject:SetActive(false)
        self.RealMurderer.transform:Find("RealMurderer").gameObject:SetActive(true)

        if self.m_ClueInfo and next(self.m_ClueInfo) then
            --self.SetEnabled(self.FinalReward.transform:Find("Sprite"), true)
            self.FinalReward.transform:Find("Sprite"):GetComponent(typeof(UISprite)).color = Color(1, 1, 1, 1)
            self.FinalReward.transform:Find("Texture"):GetComponent(typeof(UITexture)).color = Color(1, 1, 1, 1)
            if self.m_FinalRewarded then
                self.FinalRewardFx:DestroyFx()
                self.FinalReward.transform:Find("CheckBoxSprite").gameObject:SetActive(true)
                self.FinalReward.transform:Find("Mask").gameObject:SetActive(true)
            else
                self:LoadKuangFx(self.FinalRewardFx, self.FinalReward.transform)
                self.FinalReward.transform:Find("CheckBoxSprite").gameObject:SetActive(false)
                self.FinalReward.transform:Find("Mask").gameObject:SetActive(false)
            end
        else
            --self.SetEnabled(self.FinalReward.transform:Find("Sprite"), false)
            self.FinalReward.transform:Find("Sprite"):GetComponent(typeof(UISprite)).color = Color(1, 1, 1, 0.5)
            self.FinalReward.transform:Find("Texture"):GetComponent(typeof(UITexture)).color = Color(1, 1, 1, 0.5)
            self.FinalReward.transform:Find("CheckBoxSprite").gameObject:SetActive(false)
            self.FinalReward.transform:Find("Mask").gameObject:SetActive(false)
        end
    else
        --self.SetEnabled(self.FinalReward.transform:Find("Sprite"), false)
        self.FinalReward.transform:Find("Sprite"):GetComponent(typeof(UISprite)).color = Color(1, 1, 1, 0.5)
        self.FinalReward.transform:Find("Texture"):GetComponent(typeof(UITexture)).color = Color(1, 1, 1, 0.5)
        self.FinalReward.transform:Find("CheckBoxSprite").gameObject:SetActive(false)
        self.FinalReward.transform:Find("Mask").gameObject:SetActive(false)
    end
end

function LuaShanYeMiZongMainWnd:ShowFlipPageFx()
    --local fx = self.MainView.transform:Find("ClueInfo/ClueBg/Fx"):GetComponent(typeof(CUIFx))
    --fx:DestroyFx()
    --fx:LoadFx("Fx/UI/Prefab/UI_Shanye_fanye.prefab")
    --fx:GetComponent(typeof(UITexture)).alpha = 0

    self:ClearTweeners()

    local gainBtnTex = self.ClueGainBtn.transform:Find("Sprite"):GetComponent(typeof(UITexture))
    --local gainFxTex = self.ClueGainFx:GetComponent(typeof(UITexture))
    local clueHintLabel = self.ClueHint:GetComponent(typeof(UILabel))

    local tweener = LuaTweenUtils.TweenFloat(0, 1, 3, function(val)
        self.ClueTitleLabel.alpha = val
        self.ClueSubLabel.alpha = val
        self.ClueDescLabel.alpha = val
        self.CluePicTexture.alpha = val
        self.CluePicDescLabel.alpha = val
        gainBtnTex.alpha = val
        --gainFxTex.alpha = val
        clueHintLabel.alpha = val
    end)
    
    LuaTweenUtils.SetEase(tweener, Ease.OutQuart) -- OutBack
    table.insert(self.m_Tweeners, tweener)
end

-- 切换到某一幕并显示该幕介绍
function LuaShanYeMiZongMainWnd:ChooseChapter(chapterId) 
    self:ChangeToTab(chapterId)
    for j = 0, 9 do
        self.m_ClueBtn[j]:SetSelected(false)
    end
    self:ShowChapterInfo(chapterId)
end

-- 切换到线索对应的幕并显示该线索的介绍
function LuaShanYeMiZongMainWnd:ChooseClue(clueId)
    self:ChangeToTab(self.ToTabId(clueId))
    local id = self.ToTabClueId(clueId)
    for j = 0, 9 do
        if j == id then
            self.m_ClueBtn[j]:SetSelected(true)
        else
            self.m_ClueBtn[j]:SetSelected(false)
        end
    end
    self:ShowClueInfo(clueId)
end

function LuaShanYeMiZongMainWnd:GetSpeedWeight(now)
    local weight = 0
    if now.Month == 6 and now.Day == 30 and now.Hour >= 10 then
        for i = 10, now.Hour - 1 do
            weight = weight + self.m_SpeedWeightDict[i]
        end
    elseif now.Month == 7 and (now.Day < 10 or now.Day == 10 and now.Hour < 12) then
        for i = 10, 23 do
            weight = weight + self.m_SpeedWeightDict[i]
        end
        weight = weight + self.m_DailySpeedWeight * (now.Day - 1)
        for i = 0, now.Hour - 1 do
            weight = weight + self.m_SpeedWeightDict[i]
        end
    elseif now.Month >= 7 then
        weight = self.m_TotalSpeedWeight
    end
    return weight
end

-- 06-30 10:00
-- 07-03 20:00
-- 07-06 22:00
-- 07-10 12:00
function LuaShanYeMiZongMainWnd:GetSpeedPercent(now)
    local t1 = self:GetSpeedWeight(CreateFromClass(DateTime, now.Year, 7, 3, 20, 0, 0))
    local t2 = self:GetSpeedWeight(CreateFromClass(DateTime, now.Year, 7, 6, 22, 0, 0))
    local t3 = self:GetSpeedWeight(CreateFromClass(DateTime, now.Year, 7, 10, 12, 0, 0))
    if now.Month < 7 or now.Month == 7 and now.Day < 3 or now.Month == 7 and now.Day == 3 and now.Hour < 20 then
        return 1/3*self:GetSpeedWeight(now) / t1
    elseif now.Month == 7 and now.Day < 6 or now.Month == 7 and now.Day == 6 and now.Hour < 22 then
        return 1/3 + 1/3*(self:GetSpeedWeight(now) - t1) / (t2 - t1)
    elseif now.Month == 7 and now.Day < 10 or now.Month == 7 and now.Day == 10 and now.Hour < 12 then
        return 2/3 + 1/3*(self:GetSpeedWeight(now) - t2) / (t3 - t2)
    elseif now.Month >= 7 then
        return 1
    end
    return 0
end

function LuaShanYeMiZongMainWnd:GetRevealProgress()
    if self.m_bIsRevealMurderer then
        return 1
    elseif self.m_InGuess == 3 then
        return 1 - math.max(0, math.floor(self.m_curGuessEndStamp - CServerTimeMgr.Inst.timeStamp)) / 86400
    end
    return 0
end

-- 根据服务器时间判断推理进度并显示猜想倒计时
function LuaShanYeMiZongMainWnd:ProgressTickFunc()
    local stage = self.m_Stage
    local reveal = self.m_bIsRevealMurderer
    local inGuess = self.m_InGuess
    self:CheckStage()

    -- 以猜想三的位置为进度条终点，最后一段匀速滚动
    self.GuessProgress.value = self:GetSpeedPercent(CServerTimeMgr.Inst:GetZone8Time()) * 0.84 + self:GetRevealProgress() * 0.16

    if stage ~= self.m_Stage or reveal ~= self.m_bIsRevealMurderer then
        self:CancelProgressTick()
        Gac2Gas.QueryShanYeMiZongInfo()
    else
        if self.m_InGuess ~= 0 then
            local diff = LuaShanYeMiZongMgr.timediff(self.m_curGuessEndStamp, CServerTimeMgr.Inst.timeStamp)
            if diff.hour < 10 then diff.hour = "0"..diff.hour end
            if diff.min < 10 then diff.min = "0"..diff.min end
            self:CurGuessTimeLabel().text = diff.hour..":"..diff.min..LocalString.GetString("后结束")
        elseif inGuess ~= 0 then -- 猜想结束时
            Gac2Gas.QueryShanYeMiZongInfo()
        end
    end
end

function LuaShanYeMiZongMainWnd:SetClueEnabled(enabled)
    self.SetEnabled(self.ClueTitleLabel.transform, enabled)
    self.SetEnabled(self.ClueSubLabel.transform, enabled)
    --self.SetEnabled(self.ClueDescLabel.transform, enabled)
    self.SetEnabled(self.CluePicTexture.transform, enabled)
    --self.SetEnabled(self.CluePicDescLabel.transform, enabled)
    self.SetEnabled(self.ClueGainBtn.transform, enabled)
    self.SetEnabled(self.ClueGainFx.transform, enabled)
    self.SetEnabled(self.ClueHint.transform, enabled)
    self.SetEnabled(self.ClueBg.transform, enabled)
end

function LuaShanYeMiZongMainWnd:ShowChapterInfo(chapterId)
    self:SetClueEnabled(true)
    local data = ShuJia2022_Chapter.GetData(chapterId)

    self.ClueTitleLabel.text = data and data.Name
    self.ClueSubLabel.gameObject:SetActive(false)

    self.ClueDescLabel.gameObject:SetActive(true)
    self.ClueDescLabel.text = data and "[411D0E]"..CUICommonDef.TranslateToNGUIText(data.Description)
    
    self.CluePicTexture.gameObject:SetActive(false)
    self.CluePicDescLabel.gameObject:SetActive(false)

    self.ClueGainBtn.gameObject:SetActive(false)
    self.ClueGainFx:DestroyFx()

    self.ClueHint:SetActive(false)

    self:ShowFlipPageFx()
end

function LuaShanYeMiZongMainWnd:ShowClueInfo(clueId)
    local data = ShuJia2022_XianSuo.GetData(clueId)
    self.m_CurClueId = clueId
    if self.m_ClueInfo[clueId] ~= nil  then
        if data.BigPic and data.BigPic ~= "" then 
            self:ShowPicClueInfo(clueId, self.m_ClueInfo[clueId])
        else
            self:ShowTextClueInfo(clueId, self.m_ClueInfo[clueId])
        end
    else
        if self.ToTabId(clueId) < self.m_Stage then
            if data.BigPic and data.BigPic ~= "" then 
                self:ShowAutoUnlockPicClueInfo(clueId)
            else
                self:ShowAutoUnlockClueInfo(clueId)
            end
        else
            self:ShowLockClueInfo(clueId)
        end
    end

    self:ShowFlipPageFx()
end

function LuaShanYeMiZongMainWnd:ShowTextClueInfo(clueId, rewarded)
    self:SetClueEnabled(true)
    local data = ShuJia2022_XianSuo.GetData(clueId)

    self.ClueTitleLabel.text = data and data.Name
    self.ClueSubLabel.gameObject:SetActive(true)
    self.ClueSubLabel.text = data and data.Index

    self.ClueDescLabel.gameObject:SetActive(true)
    self.ClueDescLabel.text = data and "[411D0E]"..CUICommonDef.TranslateToNGUIText(data.Description)
    
    self.CluePicTexture.gameObject:SetActive(false)
    self.CluePicDescLabel.gameObject:SetActive(false)

    self.ClueGainBtn.gameObject:SetActive(true)
    if rewarded then
        self.ClueGainBtn.transform:Find("CheckBoxSprite").gameObject:SetActive(true)
        self.ClueGainBtn.transform:Find("Sprite").gameObject:SetActive(true)
        self.SetEnabled(self.ClueGainBtn.transform:Find("Sprite"), false)
        self.ClueGainFx:DestroyFx()
    else
        self.ClueGainBtn.transform:Find("CheckBoxSprite").gameObject:SetActive(false)
        self.ClueGainBtn.transform:Find("Sprite").gameObject:SetActive(false)
        self.ClueGainFx:LoadFx("Fx/UI/Prefab/UI_huangdonglibao.prefab")
    end

    self.ClueHint:SetActive(false)
end

function LuaShanYeMiZongMainWnd:ShowPicClueInfo(clueId, rewarded)
    self:SetClueEnabled(true)
    local data = ShuJia2022_XianSuo.GetData(clueId)

    self.ClueTitleLabel.text = data and data.Name
    self.ClueSubLabel.gameObject:SetActive(true)
    self.ClueSubLabel.text = data and data.Index
    
    self.ClueDescLabel.gameObject:SetActive(false)
    
    if data and data.Bigicon and data.Bigicon ~= "" then
        self.CluePicTexture.gameObject:SetActive(true)
        self.CluePicTexture:LoadMaterial(data.Bigicon)
    end
    if data and data.BigPic and data.BigPic ~= "" then 
        CommonDefs.AddOnClickListener(self.CluePicTexture.transform:Find("ZoomButton").gameObject, DelegateFactory.Action_GameObject(function (go)
            LuaShanYeMiZongBigPicWnd.s_Pic = data.BigPic
            LuaShanYeMiZongBigPicWnd.s_Desc = CUICommonDef.TranslateToNGUIText(data.Description)
            CUIManager.ShowUI(CLuaUIResources.ShanYeMiZongBigPicWnd)
        end), false)
    end
    self.CluePicDescLabel.gameObject:SetActive(true)
    self.CluePicDescLabel.text = data and "[411D0E]"..CUICommonDef.TranslateToNGUIText(data.Description)
    
    self.ClueGainBtn.gameObject:SetActive(true)
    if rewarded then
        self.ClueGainBtn.transform:Find("CheckBoxSprite").gameObject:SetActive(true)
        self.ClueGainBtn.transform:Find("Sprite").gameObject:SetActive(true)
        self.SetEnabled(self.ClueGainBtn.transform:Find("Sprite"), false)
        self.ClueGainFx:DestroyFx()
    else
        self.ClueGainBtn.transform:Find("CheckBoxSprite").gameObject:SetActive(false)
        self.ClueGainBtn.transform:Find("Sprite").gameObject:SetActive(false)
        self.ClueGainFx:LoadFx("Fx/UI/Prefab/UI_huangdonglibao.prefab")
    end

    self.ClueHint:SetActive(false)
end

function LuaShanYeMiZongMainWnd:ShowLockClueInfo(clueId)
    self:SetClueEnabled(false)
    local data = ShuJia2022_XianSuo.GetData(clueId)

    self.ClueTitleLabel.text = LocalString.GetString("？？？")
    self.ClueSubLabel.gameObject:SetActive(true)
    self.ClueSubLabel.text = data and data.Index
    
    self.ClueDescLabel.gameObject:SetActive(true)
    local txt = data and "[411D0E]"..data.XianSuoGet
    local st, ed, str
    st, ed = string.find(txt, "#n")
    if st and ed then
        txt = string.sub(txt, 1, st - 1).."[411D0E]"..string.sub(txt, ed + 1)
    end
    txt = CUICommonDef.TranslateToNGUIText(txt)
    st, ed, str = string.find(txt, "%[%((.+),(%d+),(%d+)%)%]")
    if str then
        txt = string.sub(txt, 1, st - 1).."[02641a]["..str.."][411D0E]"..string.sub(txt, ed + 4)
        UIEventListener.Get(self.ClueDescLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function()
            if CChatLinkMgr.ProcessLinkClick(self.ClueDescLabel:GetUrlAtPosition(UICamera.lastWorldPosition)) then
                CUIManager.CloseUI(CLuaUIResources.ShanYeMiZongMainWnd)
            end
        end)
    end
    self.ClueDescLabel.text = txt
    
    self.CluePicTexture.gameObject:SetActive(false)
    self.CluePicDescLabel.gameObject:SetActive(false)

    self.ClueGainBtn.gameObject:SetActive(false)
    self.ClueGainFx:DestroyFx()
    
    self.ClueHint:SetActive(false)
end

function LuaShanYeMiZongMainWnd:ShowAutoUnlockClueInfo(clueId)
    self:SetClueEnabled(false)
    local data = ShuJia2022_XianSuo.GetData(clueId)
    
    self.ClueTitleLabel.text = data and data.Name
    self.ClueSubLabel.gameObject:SetActive(true)
    self.ClueSubLabel.text = data and data.Index
    
    self.ClueDescLabel.gameObject:SetActive(true)
    self.ClueDescLabel.text = data and "[411D0E]"..CUICommonDef.TranslateToNGUIText(data.Description)
    
    self.CluePicTexture.gameObject:SetActive(false)
    self.CluePicDescLabel.gameObject:SetActive(false)
    
    self.ClueGainBtn.gameObject:SetActive(false)
    self.ClueGainFx:DestroyFx()

    if self.m_bIsRevealMurderer then
        self.ClueHint:GetComponent(typeof(UILabel)).text = LocalString.GetString("真凶已揭晓，无法再获得新线索（好友赠送除外）")
    end
    self.ClueHint:SetActive(true)
end

function LuaShanYeMiZongMainWnd:ShowAutoUnlockPicClueInfo(clueId)
    self:SetClueEnabled(false)
    local data = ShuJia2022_XianSuo.GetData(clueId)
    
    self.ClueTitleLabel.text = data and data.Name
    self.ClueSubLabel.gameObject:SetActive(true)
    self.ClueSubLabel.text = data and data.Index
    
    self.ClueDescLabel.gameObject:SetActive(false)
    
    if data and data.Bigicon and data.Bigicon ~= "" then
        self.CluePicTexture.gameObject:SetActive(true)
        self.CluePicTexture:LoadMaterial(data.Bigicon)
    end
    if data and data.BigPic and data.BigPic ~= "" then 
        CommonDefs.AddOnClickListener(self.CluePicTexture.transform:Find("ZoomButton").gameObject, DelegateFactory.Action_GameObject(function (go)
            LuaShanYeMiZongBigPicWnd.s_Pic = data.BigPic
            LuaShanYeMiZongBigPicWnd.s_Desc = CUICommonDef.TranslateToNGUIText(data.Description)
            CUIManager.ShowUI(CLuaUIResources.ShanYeMiZongBigPicWnd)
        end), false)
    end
    self.CluePicDescLabel.gameObject:SetActive(true)
    self.CluePicDescLabel.text = data and "[411D0E]"..CUICommonDef.TranslateToNGUIText(data.Description)
    
    self.ClueGainBtn.gameObject:SetActive(false)
    self.ClueGainFx:DestroyFx()
    
    if self.m_bIsRevealMurderer then
        self.ClueHint:GetComponent(typeof(UILabel)).text = LocalString.GetString("真凶已揭晓，无法再获得新线索（好友赠送除外）")
    end
    self.ClueHint:SetActive(true)
end

function LuaShanYeMiZongMainWnd:ChangeToTab(tabId) -- 1, 2, 3, 4
    self["Tab"..tabId]:SetSelected(true)
    self["Tab"..tabId].transform:Find("Fx"):GetComponent(typeof(CUIFx)):LoadFx("Fx/UI/Prefab/UI_Shanye_mubu.prefab")
    self["Tab"..tabId].transform:Find("Fx"):GetComponent(typeof(UITexture)).alpha = 0
    self["Tab"..tabId].transform:Find("NormalLabel"):GetComponent(typeof(UILabel)).color = NGUIText.ParseColor("47210e", 0)
    LuaUtils.SetLocalPositionX(self["Tab"..tabId].transform:Find("RedDot"), 91)
    self.m_CurTabId = tabId
    self.m_CurClueId = 0
    for i = 1, 4 do
        if i ~= tabId then
            self["Tab"..i]:SetSelected(false)
            self["Tab"..i].transform:Find("Fx"):GetComponent(typeof(CUIFx)):DestroyFx()
            self["Tab"..i].transform:Find("NormalLabel"):GetComponent(typeof(UILabel)).color = NGUIText.ParseColor("997758", 0)
            LuaUtils.SetLocalPositionX(self["Tab"..i].transform:Find("RedDot"), 71)
        end
    end
    if tabId == 4 then
        self.MainView:SetActive(false)
        self.ExtraStoryView:SetActive(true)
        self:InitExtraStory()
    else
        self.ChapterTip.transform.parent:GetComponent(typeof(UILabel)).text = ShuJia2022_Chapter.GetData(tabId).Name
        self:InitChapterReward(tabId)
        self:InitClues(tabId)
        self.ExtraStoryView:SetActive(false)
        self.MainView:SetActive(true)
        local bottomTip = self.MainView.transform:Find("Label"):GetComponent(typeof(UILabel))
        if tabId == 1 then
            bottomTip.text = ShuJia2022_Setting.GetData().XianSuoTips1 or ""
        elseif tabId == 2 then
            bottomTip.text = ShuJia2022_Setting.GetData().XianSuoTips2 or ""
        elseif tabId == 3 then
            bottomTip.text = ShuJia2022_Setting.GetData().XianSuoTips3 or ""
        end
    end
end

-- 0, 1, 2, 3, 4, 5
-- 1~5对应番外任务完成/解锁情况，0表示已接取番外任务但未完成第一个，1表示已完成第一个并解锁第二个，nil表示未接取
-- 完成一个任务时自动解锁下一个任务
function LuaShanYeMiZongMainWnd:GetExtraStoryStage()
    if not CClientMainPlayer.Inst or not CClientMainPlayer.Inst.TaskProp then
        return
    end
    local taskList = ShuJia2022_Setting.GetData().FanWaiTaskList
    if taskList then
        for i = 5, 1, -1 do 
            --CommonDefs.ListContains(CClientMainPlayer.Inst.TaskProp.CurrentTaskList, typeof(UInt32), taskId)
            --taskList[i-1]为第i个任务的最后一个子任务
            if CClientMainPlayer.Inst.TaskProp:IsMainPlayerTaskFinished(taskList[i - 1]) then
                return i
            end
        end
        --是否已接受或完成第一个任务的第一个子任务
        if CTaskMgr.Inst:IsInProgress(ShuJia2022_Setting.GetData().FanWaiTaskList2[0]) or 
            CClientMainPlayer.Inst.TaskProp:IsMainPlayerTaskFinished(ShuJia2022_Setting.GetData().FanWaiTaskList2[0]) then
            return 0
        end
    end
end

function LuaShanYeMiZongMainWnd:InitExtraStory()
    local taskStage = self:GetExtraStoryStage()
    if taskStage then
        self.ExtraStoryTexture.gameObject:SetActive(true)
        self.TaskGroup:SetActive(true)
        self.ExtraStoryHint:SetActive(false)
        for i = 1, 5 do
            local taskHead = self.TaskGroup.transform:Find("TaskName_"..(i - 1).."/Number"):GetComponent(typeof(UILabel))
            local task = self.TaskGroup.transform:Find("TaskName_"..(i - 1)):GetComponent(typeof(UILabel))

            local numDict = {}
            numDict[1] = LocalString.GetString("一")
            numDict[2] = LocalString.GetString("二")
            numDict[3] = LocalString.GetString("三")
            numDict[4] = LocalString.GetString("四")
            numDict[5] = LocalString.GetString("五")
            if i <= taskStage then -- 已完成
                taskHead.text = LocalString.GetString("完")
                taskHead.color = NGUIText.ParseColor("ffde7b", 0)
            else
                taskHead.text = numDict[i]
                if i == taskStage + 1 then -- 已解锁
                    local startTask = ShuJia2022_Setting.GetData().FanWaiTaskList2[i-1]
                    if CTaskMgr.Inst:IsInProgress(startTask) or CClientMainPlayer.Inst.TaskProp:IsMainPlayerTaskFinished(startTask) then -- 已接受
                        CommonDefs.AddOnClickListener(task.gameObject, DelegateFactory.Action_GameObject(function (go)
                            g_MessageMgr:ShowMessage("SYMZ_FAMWAITASK_GET_ALREADY")
                        end), false)
                    else
                        CommonDefs.AddOnClickListener(task.gameObject, DelegateFactory.Action_GameObject(function (go) -- 未接受
                            g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("SYMZ_FANWAI_CONFIRM"), function()
                                CUIManager.CloseUI(CLuaUIResources.ShanYeMiZongMainWnd)
                                local pos = CPos(ShuJia2022_Setting.GetData().FanWaiTaskPosition[i-1][0], ShuJia2022_Setting.GetData().FanWaiTaskPosition[i-1][1])
                                pos = Utility.GridPos2PixelPos(pos)
                                CTrackMgr.Inst:Track(nil, ShuJia2022_Setting.GetData().MiWuLingMapID, pos, Utility.Grid2Pixel(2.0), nil, nil, nil)
                            end, nil, nil, nil, false)
                        end), false)
                    end
                else -- 未解锁
                    task.text = LocalString.GetString("待解锁")
                    task.color = Color(task.color.r, task.color.g, task.color.b, 0.5)
                    CommonDefs.AddOnClickListener(task.gameObject, DelegateFactory.Action_GameObject(function (go)
                        g_MessageMgr:ShowMessage("ShuJia2022_FanWai_Not_Open")
                    end), false)
                end
            end
        end
        if LuaShanYeMiZongMainWnd.s_UnlockNewExtraTask then
            self:ShowImageSpreadEffect(3, taskStage, true)
            LuaShanYeMiZongMainWnd.s_UnlockNewExtraTask = false
        else
            self:ShowImageSpreadEffect(0, taskStage)
        end
    else
        self.ExtraStoryTexture.gameObject:SetActive(false)
        self.TaskGroup:SetActive(false)
        self.ExtraStoryHint:SetActive(true)
        UIEventListener.Get(self.ExtraStoryHint.transform:Find("MiWuLing").gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            CUIManager.CloseUI(CLuaUIResources.ShanYeMiZongMainWnd)
            local pos = CPos(ShuJia2022_Setting.GetData().FanWaiTaskPosition[0][0], ShuJia2022_Setting.GetData().FanWaiTaskPosition[0][1])
            pos = Utility.GridPos2PixelPos(pos)
            CTrackMgr.Inst:Track(nil, ShuJia2022_Setting.GetData().MiWuLingMapID, pos, Utility.Grid2Pixel(2.0), nil, nil, nil)
        end)
    end

    local ItemData = Item_Item.GetData(ShuJia2022_Setting.GetData().FanwaiReward)
    if ItemData then 
        UIEventListener.Get(self.RewardItem.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnRewardItemClick()
        end)
        self.RewardItem.transform:Find("Item"):GetComponent(typeof(CUITexture)):LoadMaterial(ItemData.Icon) 
    end
    local rewardFx = self.RewardItem.transform:Find("KuangFx"):GetComponent(typeof(CUIFx))
    if CClientMainPlayer.Inst.TaskProp:IsMainPlayerTaskFinished(ShuJia2022_Setting.GetData().FanWaiTaskID) then
        local hint = self.RewardItem.transform:Find("HintLabel"):GetComponent(typeof(UILabel))
        if self.m_ExtraRewarded then
            self.RewardItem.transform:Find("Item"):GetComponent(typeof(UITexture)).color = Color(1, 1, 1, 1)
            self.RewardItem.transform:Find("LockSprite").gameObject:SetActive(false)
            self.RewardItem.transform:Find("Mask").gameObject:SetActive(false)
            hint.text = LocalString.GetString("已领取")
            hint.color = Color.green
            rewardFx.gameObject:SetActive(false)
            rewardFx:DestroyFx()
        else
            self.RewardItem.transform:Find("Item"):GetComponent(typeof(UITexture)).color = Color(1, 1, 1, 1)
            self.RewardItem.transform:Find("LockSprite").gameObject:SetActive(false)
            self.RewardItem.transform:Find("Mask").gameObject:SetActive(false)
            hint.text = LocalString.GetString("可领取")
            hint.color = Color.yellow
            if not rewardFx.gameObject.activeSelf then
                rewardFx.OnLoadFxFinish = DelegateFactory.Action(function() rewardFx:GetComponent(typeof(UITexture)).alpha = 0 end)
                self:LoadKuangFx(rewardFx, self.RewardItem.transform:Find("Border"), true)
            end
        end
    else
        self.RewardItem.transform:Find("Item"):GetComponent(typeof(UITexture)).color = Color(1, 1, 1, 0.5)
        self.RewardItem.transform:Find("LockSprite").gameObject:SetActive(true)
        self.RewardItem.transform:Find("Mask").gameObject:SetActive(true)
        rewardFx.gameObject:SetActive(false)
        rewardFx:DestroyFx()
    end
end

function LuaShanYeMiZongMainWnd:LoadKuangFx(fx, trans, static)
    LuaTweenUtils.DOKill(fx.transform, false)
    fx.transform.localPosition = Vector3(0, 0, 0)
    local b = NGUIMath.CalculateRelativeWidgetBounds(trans.parent, trans)
    local waypoints = CUICommonDef.GetWayPoints(Vector4(b.min.x, b.max.y, b.size.x, b.size.y), 1, true)
    fx.transform.localPosition = waypoints[0]
    if static then
        fx.gameObject:SetActive(true)
    else
        fx:LoadFx("fx/ui/prefab/UI_kuang_yellow02.prefab")
    end
    fx:GetComponent(typeof(UITexture)).alpha = 0
    LuaTweenUtils.DOLuaLocalPath(fx.transform, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, - 1, Ease.Linear)
end

function LuaShanYeMiZongMainWnd:InitChapter_Lock(clueCount)
    self.SetEnabled(self.ChapterRewardBtn.transform:Find("Sprite"), true)
    self.ChapterUnlockLabel.text = LocalString.GetString("线索").."[c][ff0000]"..clueCount.."/10[000000]"
    local sprite = self.ChapterRewardBtn.transform:Find("Sprite"):GetComponent(typeof(UITexture))
    sprite.color = Color(sprite.color.r, sprite.color.g, sprite.color.b, 0.5)
    self.ChapterRewardFx.gameObject:SetActive(false)
    self.ChapterRewardFx:DestroyFx()
    self.ChapterRewardBtn.transform:Find("CheckBoxSprite").gameObject:SetActive(false)
end

function LuaShanYeMiZongMainWnd:InitChapter_CanReward()
    self.SetEnabled(self.ChapterRewardBtn.transform:Find("Sprite"), true)
    self.ChapterUnlockLabel.text = LocalString.GetString("线索").."10/10"
    local sprite = self.ChapterRewardBtn.transform:Find("Sprite"):GetComponent(typeof(UITexture))
    sprite.color = Color(sprite.color.r, sprite.color.g, sprite.color.b, 1)
    if not self.ChapterRewardFx.gameObject.activeSelf then
        self:LoadKuangFx(self.ChapterRewardFx, self.ChapterRewardBtn.transform)
    end
    self.ChapterRewardBtn.transform:Find("CheckBoxSprite").gameObject:SetActive(false)
end

function LuaShanYeMiZongMainWnd:InitChapter_Rewarded()
    self.SetEnabled(self.ChapterRewardBtn.transform:Find("Sprite"), false)
    self.ChapterUnlockLabel.text = LocalString.GetString("线索").."10/10"
    local sprite = self.ChapterRewardBtn.transform:Find("Sprite"):GetComponent(typeof(UITexture))
    sprite.color = Color(sprite.color.r, sprite.color.g, sprite.color.b, 1)
    self.ChapterRewardFx.gameObject:SetActive(false)
    self.ChapterRewardFx:DestroyFx()
    self.ChapterRewardBtn.transform:Find("CheckBoxSprite").gameObject:SetActive(true)
end

-- 章节线索解锁奖励
function LuaShanYeMiZongMainWnd:InitChapterReward(chapterId)
    local chapterInfo = self.m_ChapterInfo[chapterId]
    if chapterInfo.clueCount == 10 then
        if chapterInfo.rewarded then
            self:InitChapter_Rewarded()
        else
            self:InitChapter_CanReward()
        end
    else
        self:InitChapter_Lock(chapterInfo.clueCount)
    end
end

function LuaShanYeMiZongMainWnd:InitClues(tabId) 
    --wyh PanelMask的奇怪问题? 会导致下面的icon出现色差
    self.MainView.transform:Find("HiddenClue_0/Panel"):GetComponent(typeof(UIPanel)).depth = 
    self.MainView.transform:Find("HiddenClue_1/Panel"):GetComponent(typeof(UIPanel)).depth

    for i = 0, 9 do
        local prefix 
        local idx
        if i < 8 then
            idx = i
            prefix = "NormalClue_"
        else
            idx = i - 8
            prefix = "HiddenClue_"
        end
        local clue = self.MainView.transform:Find(prefix..idx)
        if clue then
            if not self.m_ClueBtn[i] then
                self.m_ClueBtn[i] = clue:GetComponent(typeof(QnSelectableButton))
                self.m_ClueBtn[i].Selectable = false
            end
            clue:Find("RedDot").gameObject:SetActive(false)

            local clueId = (tabId - 1) * 10 + i + 1
            if self.m_ClueInfo[clueId] ~= nil then
                local data = ShuJia2022_XianSuo.GetData(clueId)
                if data then
                    clue:Find("NameLabel"):GetComponent(typeof(UILabel)).text = data.Name
                    local tex = clue:Find("Panel/Item"):GetComponent(typeof(CUITexture))
                    if tex and data.icon and data.icon ~= "" then 
                        tex:LoadMaterial(data.icon)
                        local c = tex.color
                        tex.color = Color(c.r, c.g, c.b, 1)
                        if not self.m_ClueInfo[clueId] then 
                            clue:Find("RedDot").gameObject:SetActive(true)
                        end
                    end
                    clue:Find("Panel").gameObject:SetActive(true)
                    clue:Find("NameLabel").gameObject:SetActive(true)
                    clue:Find("Lock").gameObject:SetActive(false)
                end
            else
                if tabId == self.m_Stage then
                    clue:Find("Panel").gameObject:SetActive(false)
                    clue:Find("NameLabel").gameObject:SetActive(false)
                    clue:Find("Lock").gameObject:SetActive(true)
                else 
                    local data = ShuJia2022_XianSuo.GetData(clueId)
                    if data then
                        clue:Find("NameLabel"):GetComponent(typeof(UILabel)).text = data.Name
                        local tex = clue:Find("Panel/Item"):GetComponent(typeof(CUITexture))
                        if tex and data.icon and data.icon ~= "" then 
                            tex:LoadMaterial(data.icon)
                            local c = tex.color
                            tex.color = Color(c.r, c.g, c.b, 0.3)
                        end
                        clue:Find("Panel").gameObject:SetActive(true)
                        clue:Find("NameLabel").gameObject:SetActive(true)
                        clue:Find("Lock").gameObject:SetActive(false)
                    end
                end
            end
            UIEventListener.Get(clue.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
                for j = 0, 9 do
                    if j == i then
                        self.m_ClueBtn[j]:SetSelected(true)
                    else
                        self.m_ClueBtn[j]:SetSelected(false)
                    end
                end
                self:ShowClueInfo(clueId)
            end)
        end
    end
end

function LuaShanYeMiZongMainWnd:ShowImageSpreadEffect(duration, taskStage, startFromPrev) -- clip: 0 ~ 20
    local clipDict = {} -- taskStage=i时的clip值 
    clipDict[-1] = 0
    clipDict[0] = 2.7
    clipDict[1] = 7.4
    clipDict[2] = 10
    clipDict[3] = 13.5
    clipDict[4] = 17.2
    clipDict[5] = 20
    
    self.ExtraStoryTexture.m_WidthHeightRate = 0.3
    self.ExtraStoryTexture.baseClipRegion = Vector4(0, 0.5, 0, 0)
    self.ExtraStoryTexture.m_MinClipAmount = 0
    self.ExtraStoryTexture.m_MaxClipAmount = 20
    --self.ExtraStoryTexture:SetAlpha(0)

    local from
    local to = clipDict[taskStage]
    if startFromPrev then
        from = clipDict[taskStage - 1]
    else
        from = 0
    end
    self.ExtraStoryTexture:SetClip(from)

    local tweener = LuaTweenUtils.TweenFloat(from, to, duration, function(val)
        self.ExtraStoryTexture:SetClip(val)
    end)
    LuaTweenUtils.SetEase(tweener, Ease.OutQuart)
    table.insert(self.m_Tweeners, tweener)

    --tweener = LuaTweenUtils.TweenFloat(0, 1, duration, function(val)
    --    self.ExtraStoryTexture:SetAlpha(val)
    --end)
    --LuaTweenUtils.SetEase(tweener, Ease.OutQuart)
    --table.insert(self.m_Tweeners, tweener)
end

function LuaShanYeMiZongMainWnd:ClearTweeners()
    if self.m_Tweeners then
        for i = 1, #self.m_Tweeners do
            LuaTweenUtils.Kill(self.m_Tweeners[i], false)
        end
    end
    self.m_Tweeners = {}
end

function LuaShanYeMiZongMainWnd:CancelProgressTick()
    if self.m_ProgressTick then
        invoke(self.m_ProgressTick)
        self.m_ProgressTick = nil
    end
end
