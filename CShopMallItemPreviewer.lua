-- Auto Generated!!
local CClientMainPlayer      = import "L10.Game.CClientMainPlayer"
local CItem                  = import "L10.Game.CItem"
local CItemMgr               = import "L10.Game.CItemMgr"
local Color                  = import "UnityEngine.Color"
local CommonDefs             = import "L10.Game.CommonDefs"
local CShopMallItemPreviewer = import "L10.UI.CShopMallItemPreviewer"
local CUICommonDef           = import "L10.UI.CUICommonDef"
local DelegateFactory        = import "DelegateFactory"
local LocalString            = import "LocalString"
local UIRect                 = import "UIRect"
local UIScrollView           = import "UIScrollView"
local UISprite               = import "UISprite"
local Vector3                = import "UnityEngine.Vector3"
local UIWidget               = import "UIWidget"

CShopMallItemPreviewer.m_Awake_CS2LuaHook = function (this) 
    this.m_ContainerOriginalHeight = this.m_DescriptionContainer.height
    if this.m_HasCountLabel ~= nil then
        this.m_HasCountLabelOriginalPos = this.m_HasCountLabel.transform.localPosition
    end
    this.m_DiscountInfoRootOriginalPos = this.m_DiscountInfoRoot.transform.localPosition
    this.m_ItemPreviewerOriginalHeight = CommonDefs.GetComponent_Component_Type(this, typeof(UISprite)).height
end

CShopMallItemPreviewer.m_Init_CS2LuaHook = function (this) 
    this.m_NameLabel.text = ""
    this.m_Level.text = ""
    this.m_Description.text = ""
    this.m_LimitCountLabel.text = ""
    this.m_DiscountInfoRoot:SetActive(false)
end

CShopMallItemPreviewer.m_UpdateData_CS2LuaHook = function (this, template, addtionalHeight, otherCategoryName) 
    if template == nil then
        this:Init()
        return
    end
    local item = CItemMgr.Inst:GetItemTemplate(template.ItemId)
    if item ~= nil then
        this.m_NameLabel.text = item.Name
        if item.AdjustedGradeCheck > 0 then
            this.m_Level.gameObject:SetActive(true)
            this.m_Level.text = System.String.Format("lv.{0}", item.AdjustedGradeCheck)
        else
            this.m_Level.gameObject:SetActive(false)
        end
        if CClientMainPlayer.Inst ~= nil and item.AdjustedGradeCheck > CClientMainPlayer.Inst.Level then
            this.m_Level.color = Color.red
        else
            this.m_Level.color = Color.white
        end

        if template.AvalibleCount >= 0 then
            this.m_LimitCountLabel.gameObject:SetActive(true)
            if template.IsGlobalLimitRmbPackage == 1 then
                this.m_LimitCountLabel.text = g_MessageMgr:FormatMessage("QuanFuXianGouAmount")
            elseif template.IsAllLifeLimit then
                this.m_LimitCountLabel.gameObject:SetActive(true)
                if template.AvalibleCount ~= 0 then
                    this.m_LimitCountLabel.text = System.String.Format(LocalString.GetString("限购{0}次"), template.AvalibleCount)
                else
                    this.m_LimitCountLabel.text = LocalString.GetString("已售完")
                end
            else
                this.m_LimitCountLabel.gameObject:SetActive(true)
                if template.AvalibleCount > 0 then
                    local str = ""
                    if template.IsMonthLimit then
                        str = System.String.Format(LocalString.GetString("本月限购{0}次"), template.AvalibleCount)
                    else
                        str = System.String.Format(LocalString.GetString("本周限购{0}次"), template.AvalibleCount)
                    end
                    this.m_LimitCountLabel.text = str
                else
                    this.m_LimitCountLabel.text = LocalString.GetString("已售完")
                end
            end
        else
            this.m_LimitCountLabel.gameObject:SetActive(false)
        end

        local description = CItem.GetItemDescription(template.ItemId,false)

		if not System.String.IsNullOrEmpty(template.LimitGroupDesc) then
			description = "#Y".. template.LimitGroupDesc .. "#n\n" .. description
		end
        -- if not System.String.IsNullOrEmpty(otherCategoryName) then
        --     description = (description .. LocalString.GetString("#r#G上架位置：")) .. otherCategoryName
        -- end
        if not System.String.IsNullOrEmpty(template.DeleteTime) then
            description = (description .. LocalString.GetString("#r#G下架时间：")) .. template.DeleteTime
        end
        local importDescription = this.m_DescriptionContainer.transform:Find("DesScroll/ImportDescription")
        if importDescription then
            importDescription = importDescription:GetComponent(typeof(UILabel))
            importDescription.gameObject:SetActive(false)
            importDescription.text = ""
            local itemdata = Item_Item.GetData(template.ItemId)
            if itemdata then
                local _,_,s = string.find(description, "import(.-)endimport")
                if s then
                    importDescription.gameObject:SetActive(true)
                    importDescription.text = CUICommonDef.TranslateToNGUIText(s)
                end
            end
            local importViewBg = importDescription.transform:Find("ImportViewBg"):GetComponent(typeof(UIWidget))
            if importViewBg then
                importViewBg:Update()
                Extensions.SetLocalPositionY(this.m_Description.transform, importViewBg.height == 0 and 0 or (-importViewBg.height - 20))
            end
        end
        description = string.gsub(description, "import(.-)endimport(.-)#r", "")
        this.m_Description.text = CUICommonDef.TranslateToNGUIText(description)

        --获取当前包裹中的个数
        if this.m_HasCountLabel ~= nil then
            local hasCount = CItemMgr.Inst:GetItemCount(template.ItemId)
            this.m_HasCountLabel.text = System.String.Format(LocalString.GetString("已拥有{0}个"), hasCount)
        end
        if template.Discount > 0.05 then
            this.m_DiscountInfoRoot:SetActive(true)
            local originalPrice = math.floor(tonumber(template.Price * 10 / template.Discount or 0))
            if System.String.IsNullOrEmpty(template.RmbPID) then
                this.m_OriginalPriceLabel.text = tostring(originalPrice)
                this.m_DiscountPriceLabel.text = tostring(template.Price)
            else
                this.m_OriginalPriceLabel.text = tostring(originalPrice) .. LocalString.GetString("元")
                this.m_DiscountPriceLabel.text = tostring(template.Price) .. LocalString.GetString("元")
            end
            local showDiscount = template.Discount
			if CommonDefs.IS_VN_CLIENT then
                if LocalString.isCN then
                    this.m_DiscountLabel.text = System.String.Format(LocalString.GetString("{0:N1}折"), showDiscount)
                else
                    this.m_DiscountLabel.text = System.String.Format("-{0:N0}%", 100 - showDiscount * 10)
                end
			elseif CommonDefs.IS_KR_CLIENT then
				showDiscount = showDiscount * 10
				this.m_DiscountLabel.text = System.String.Format(LocalString.GetString("{0:N1}%"), showDiscount)
			else
				this.m_DiscountLabel.text = System.String.Format(LocalString.GetString("{0:N1}折"), showDiscount)
			end
        else
            this.m_DiscountInfoRoot:SetActive(false)
        end
        --人民币礼包不显示折扣
        this.m_DiscountMoneySprite1.gameObject:SetActive(System.String.IsNullOrEmpty(template.RmbPID))
        this.m_DiscountMoneySprite2.gameObject:SetActive(System.String.IsNullOrEmpty(template.RmbPID))

        --更新位置
        this.m_DiscountInfoRoot.transform.localPosition = CommonDefs.op_Addition_Vector3_Vector3(this.m_DiscountInfoRootOriginalPos, CommonDefs.op_Multiply_Vector3_Single(Vector3.down, addtionalHeight))
        CommonDefs.GetComponent_Component_Type(this, typeof(UISprite)).height = this.m_ItemPreviewerOriginalHeight + addtionalHeight

        if not this.m_DiscountInfoRoot.activeSelf then
            if this.m_HasCountLabel ~= nil then
                this.m_HasCountLabel.transform.localPosition = CommonDefs.op_Addition_Vector3_Vector3(this.m_HasCountLabelOriginalPos, CommonDefs.op_Multiply_Vector3_Single(Vector3.down, addtionalHeight))
            end
            this.m_DescriptionContainer.height = this.m_ContainerOriginalHeight + addtionalHeight + 100
            local widgets = CommonDefs.GetComponentsInChildren_Component_Type(this.m_DescriptionContainer, typeof(UIRect))
            CommonDefs.EnumerableIterate(widgets, DelegateFactory.Action_object(function (___value) 
                local widget = ___value
                widget:ResetAndUpdateAnchors()
            end))
        else
            if this.m_HasCountLabel ~= nil then
                this.m_HasCountLabel.transform.localPosition = CommonDefs.op_Addition_Vector3_Vector3(this.m_HasCountLabelOriginalPos, CommonDefs.op_Multiply_Vector3_Single(Vector3.down, addtionalHeight))
            end
            this.m_DescriptionContainer.height = this.m_ContainerOriginalHeight + addtionalHeight
            local widgets = CommonDefs.GetComponentsInChildren_Component_Type(this.m_DescriptionContainer, typeof(UIRect))
            CommonDefs.EnumerableIterate(widgets, DelegateFactory.Action_object(function (___value) 
                local widget = ___value
                widget:ResetAndUpdateAnchors()
            end))
        end


        local scrollView = CommonDefs.GetComponentInChildren_Component_Type(this.m_DescriptionContainer, typeof(UIScrollView))
        if scrollView ~= nil then
            scrollView:ResetPosition()
        end
    end
end
