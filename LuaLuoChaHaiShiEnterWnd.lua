local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"

LuaLuoChaHaiShiEnterWnd = class()

LuaLuoChaHaiShiEnterWnd.s_CurRewardTimes = 0
LuaLuoChaHaiShiEnterWnd.s_MaxRewardTimes = 0

RegistClassMember(LuaLuoChaHaiShiEnterWnd, "JoinTimeLabel")
RegistClassMember(LuaLuoChaHaiShiEnterWnd, "JoinTeamLabel")
RegistClassMember(LuaLuoChaHaiShiEnterWnd, "FirstReward")
RegistClassMember(LuaLuoChaHaiShiEnterWnd, "DailyReward")
RegistClassMember(LuaLuoChaHaiShiEnterWnd, "TipBtn")
RegistClassMember(LuaLuoChaHaiShiEnterWnd, "EnterBtn")
RegistClassMember(LuaLuoChaHaiShiEnterWnd, "ScrollView")
RegistClassMember(LuaLuoChaHaiShiEnterWnd, "TextTemplate")
RegistClassMember(LuaLuoChaHaiShiEnterWnd, "TextTable")

function LuaLuoChaHaiShiEnterWnd:Awake()
    self.JoinTimeLabel = self.transform:Find("Anchor/Top/TimeInfo/TimeLabel"):GetComponent(typeof(UILabel))
    self.JoinTeamLabel = self.transform:Find("Anchor/Top/TimeInfo/TeamLabel"):GetComponent(typeof(UILabel))
    self.FirstReward = self.transform:Find("Anchor/Top/RewardInfo/FirstReward")
    self.DailyReward = self.transform:Find("Anchor/Top/RewardInfo/DailyReward")
    self.TipBtn = self.transform:Find("Anchor/Top/TipButton").gameObject
    self.EnterBtn = self.transform:Find("Anchor/EnterBtn").gameObject
    self.ScrollView = self.transform:Find("Anchor/BottomRight/ScrollView"):GetComponent(typeof(CUIRestrictScrollView))
    self.TextTable = self.transform:Find("Anchor/BottomRight/ScrollView/TextTable"):GetComponent(typeof(UITable))
    self.TextTemplate = self.transform:Find("Anchor/BottomRight/TextTemplate").gameObject
    self.TextTemplate:SetActive(false)

    UIEventListener.Get(self.TipBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("LuoCha2023_Tips")
    end)

    UIEventListener.Get(self.EnterBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        Gac2Gas.RequestEnterLuoChaBossPlay()
    end)
end

function LuaLuoChaHaiShiEnterWnd:OnQueryLuoChaHaiShi2023InfoResult(curRewardTimes, maxRewardTimes)
    local setting = LuoCha2023_Setting.GetData()
    local joinDesc = setting.JoinDesc
    local rewardItemId = setting.RewardItemId
    local ruleDesc = setting.RuleDesc

    self.JoinTimeLabel.text = joinDesc[0]
    self.JoinTeamLabel.text = joinDesc[1]
    
    self:InitOneItem(self.FirstReward, rewardItemId[0], curRewardTimes, 1, false)
    self:InitOneItem(self.DailyReward, rewardItemId[1], curRewardTimes, maxRewardTimes, true)

    Extensions.RemoveAllChildren(self.TextTable.transform)
    
    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(ruleDesc)
    if info == nil then
        return
    end
    for i = 0, info.paragraphs.Count - 1 do
        local paragraphGo = NGUITools.AddChild(self.TextTable.gameObject, self.TextTemplate)
        paragraphGo:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(paragraphGo, typeof(CTipParagraphItem)):Init(info.paragraphs[i], 4294967295)
    end
    self.ScrollView:ResetPosition()
    self.TextTable:Reposition()
end

function LuaLuoChaHaiShiEnterWnd:Init()
    self:OnQueryLuoChaHaiShi2023InfoResult(LuaLuoChaHaiShiEnterWnd.s_CurRewardTimes, LuaLuoChaHaiShiEnterWnd.s_MaxRewardTimes)
end

function LuaLuoChaHaiShiEnterWnd:InitOneItem(curItem, itemID, gotTimes, limit, isDaily)
    local texture = curItem.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local ItemData = Item_Item.GetData(itemID)
    if ItemData then 
        texture:LoadMaterial(ItemData.Icon) 
    end
    curItem.transform:Find("HasReward").gameObject:SetActive(gotTimes >= limit)
    if isDaily then
        curItem.transform:Find("Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("常规奖励 %d/%d 次"), gotTimes, limit) 
    end

    UIEventListener.Get(texture.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemID, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
    end)
end
