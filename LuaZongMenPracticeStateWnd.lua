local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local NGUITools = import "NGUITools"
local CMainCamera = import "L10.Engine.CMainCamera"

LuaZongMenPracticeStateWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaZongMenPracticeStateWnd, "Hps", "Hps", GameObject)
RegistChildComponent(LuaZongMenPracticeStateWnd, "TeleportHpTemplate", "TeleportHpTemplate", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaZongMenPracticeStateWnd, "m_Id2UI")
RegistClassMember(LuaZongMenPracticeStateWnd, "m_Id2Hp")
RegistClassMember(LuaZongMenPracticeStateWnd, "m_MaxHp")

function LuaZongMenPracticeStateWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.TeleportHpTemplate:SetActive(false)
    self.m_Id2UI = {}
    self.m_Id2Hp = {}
    self.m_MaxHp = tonumber(Menpai_Setting.GetData("PracticeTeleportMaxHp").Value)
end

function LuaZongMenPracticeStateWnd:Init()
    local id2Hp = LuaZongMenMgr.m_TeleportId2Hp
    if id2Hp then
        for k, v in pairs(id2Hp) do
            self.m_Id2Hp[k] = v
        end
    end
end

function LuaZongMenPracticeStateWnd:OnEnable()
    g_ScriptEvent:AddListener("SectXiuxingUpdateChuansongmenHp", self, "OnSectXiuxingUpdateChuansongmenHp")
end

function LuaZongMenPracticeStateWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SectXiuxingUpdateChuansongmenHp", self, "OnSectXiuxingUpdateChuansongmenHp")
end

--@region UIEvent

--@endregion UIEvent

function LuaZongMenPracticeStateWnd:Update()
    for k, v in pairs(self.m_Id2Hp) do
        self:UpdateHpPos(k, v)
    end
end

function LuaZongMenPracticeStateWnd:OnSectXiuxingUpdateChuansongmenHp(engineId, hp)
    self.m_Id2Hp[engineId] = hp
    self:UpdateHpPos(engineId, hp)
end

function LuaZongMenPracticeStateWnd:UpdateHpPos(engineId, hp)
    local ui = self.m_Id2UI[engineId]
    local co = CClientObjectMgr.Inst:GetObject(engineId)

    if co and co.RO then
        if ui == nil then
            ui = NGUITools.AddChild(self.Hps, self.TeleportHpTemplate)
            ui:SetActive(true)
            self.m_Id2UI[engineId] = ui
        end
    else
        if ui then
            ui.transform.localPosition = CommonDefs.op_Multiply_Vector3_Single(Vector3.up, 3000)
        end
        return
    end

    local hpSlider  = ui:GetComponent(typeof(UISlider))
    local hpLab     = ui.transform:Find("HpLab"):GetComponent(typeof(UILabel))
    hpSlider.value  = hp / self.m_MaxHp * 0.275
    hpLab.text      = SafeStringFormat3("%d/%d", hp, self.m_MaxHp)

    local vec3 = co.RO.transform.position
    vec3.y = vec3.y + 3.4
    local viewPos = CMainCamera.Main:WorldToViewportPoint(vec3)
    local screenPos = nil
    if viewPos and viewPos.z > 0 and viewPos.x > 0 and viewPos.x < 1 and viewPos.y > 0 and viewPos.y < 1 then
        screenPos = CMainCamera.Main:WorldToScreenPoint(vec3)
    else  
        screenPos = CommonDefs.op_Multiply_Vector3_Single(Vector3.up, 3000)
    end

    if screenPos ~= nil then
        screenPos.z = 0
        local topWorldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)
        local lastPos = ui.transform.position
        if lastPos.x ~= topWorldPos.x or lastPos.y ~= topWorldPos.y then
            ui.transform.position = topWorldPos
        end
    end
end