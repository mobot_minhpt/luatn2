-- Auto Generated!!
local CChargeWnd = import "L10.UI.CChargeWnd"
local CChargeWnd_Gift = import "L10.UI.CChargeWnd_Gift"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
--local Charge_VipInfo = import "L10.Game.Charge_VipInfo"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local Engine = import "L10.Engine"
local UInt16 = import "System.UInt16"
local UInt32 = import "System.UInt32"
CChargeWnd_Gift.m_OnChargePropUpdated_CS2LuaHook = function (this) 
    this.m_TableView:ReloadData(true, true)
    if this.m_DelayTick ~= nil then
        invoke(this.m_DelayTick)
    end
    this.m_DelayTick = Engine.CTickMgr.RegisterUpdate(DelegateFactory.Action_uint(function (delta) 
        this.m_TableView:ScrollToRow(this:GetUnRecieveGiftRow())
    end), Engine.ETickType.Once)
end
CChargeWnd_Gift.m_Init_CS2LuaHook = function (this) 
    CommonDefs.ListClear(this.m_VipIds)
    --Charge_VipInfo.Foreach(DelegateFactory.Action_uint_Charge_VipInfo(function (id, data) 
    Charge_VipInfo.Foreach(function (id, data) 
        if data.Status > 0 then
            return
        end
        CommonDefs.ListAdd(this.m_VipIds, typeof(UInt32), id)
    end)
    this.m_TableView:ReloadData(false, false)
    if this.m_DelayTick ~= nil then
        invoke(this.m_DelayTick)
    end
    this.m_DelayTick = Engine.CTickMgr.RegisterUpdate(DelegateFactory.Action_uint(function (delta) 
        this.m_TableView:ScrollToRow(this:GetUnRecieveGiftRow())
    end), Engine.ETickType.Once)
end
CChargeWnd_Gift.m_GetUnRecieveGiftRow_CS2LuaHook = function (this) 
    local ret = 0
    if CClientMainPlayer.Inst ~= nil then
        local vipInfo = CClientMainPlayer.Inst.ItemProp.Vip
        if vipInfo ~= nil then
            do
                local i = 1
                while i <= CChargeWnd.MAX_VIP_LEVEL and i <= vipInfo.Level do
                    if not CommonDefs.DictContains(vipInfo.Level2GiftReceivedTime, typeof(UInt16), i) then
                        return i - 1
                    end
                    i = i + 1
                end
            end
        end
    end
    return ret
end
