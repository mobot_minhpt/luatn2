local QnButton = import "L10.UI.QnButton"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local DelegateFactory = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UISprite = import "UISprite"
local CommonDefs = import "L10.Game.CommonDefs"
local LocalString = import "LocalString"
local String = import "System.String"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local Money = import "L10.Game.Money"
local QnLabel = import "L10.UI.QnLabel"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local CUITexture = import "L10.UI.CUITexture"
local IdPartition = import "L10.Game.IdPartition"
local CItem = import "L10.Game.CItem"
local CEquipment = import "L10.Game.CEquipment"
local CItemMgr = import "L10.Game.CItemMgr"
local Extensions = import "Extensions"
local NGUITools = import "NGUITools"
local UIGrid = import "UIGrid"
local UIScrollView = import "UIScrollView"
local QnCheckBox = import "L10.UI.QnCheckBox"
local Collider = import "UnityEngine.Collider"
local NGUIMath = import "NGUIMath"
local UIRoot = import "UIRoot"
local Screen = import "UnityEngine.Screen"
local UITexture = import "UITexture"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"

LuaFashionPreviewTryToBuyWnd = class()
RegistChildComponent(LuaFashionPreviewTryToBuyWnd,"qnCostAndOwnMoney",  CCurentMoneyCtrl)
RegistChildComponent(LuaFashionPreviewTryToBuyWnd,"buyBtn", QnButton)
RegistChildComponent(LuaFashionPreviewTryToBuyWnd,"buyArea", GameObject)
RegistChildComponent(LuaFashionPreviewTryToBuyWnd,"templateItem", GameObject)
RegistChildComponent(LuaFashionPreviewTryToBuyWnd,"disableSprite", UISprite)
RegistChildComponent(LuaFashionPreviewTryToBuyWnd,"grid", UIGrid)
RegistChildComponent(LuaFashionPreviewTryToBuyWnd,"scrollView", UIScrollView)

RegistClassMember(LuaFashionPreviewTryToBuyWnd,"curSelectableSequence")
RegistClassMember(LuaFashionPreviewTryToBuyWnd,"curCostMoney")
RegistClassMember(LuaFashionPreviewTryToBuyWnd,"isShowOneItem")
RegistClassMember(LuaFashionPreviewTryToBuyWnd,"bg")
RegistClassMember(LuaFashionPreviewTryToBuyWnd,"m_OwnedShopItems")
RegistClassMember(LuaFashionPreviewTryToBuyWnd,"m_ConfirmMessageList")

function LuaFashionPreviewTryToBuyWnd:Init()
    self.bg = self.transform:Find("Wnd_Bg_Secondary_2"):GetComponent(typeof(UITexture))

    self.curSelectableSequence = {}
    for k, v in pairs( LuaShopMallFashionPreviewMgr.selectableSequence) do
        self.curSelectableSequence[k] = v
    end

    local count = 0
    for itemID,shopMallTemlate in pairs(self.curSelectableSequence) do
        if shopMallTemlate.Price > 0 then
            count = count + 1
        end
    end

    self:InitShopRoot()

    self.isShowOneItem = count == 1

    if self.isShowOneItem then
        self.scrollView.gameObject:SetActive(false)
        self.templateItem:SetActive(true)
    end

    self:UpdateCostMoney()
    self.buyBtn.OnClick = DelegateFactory.Action_QnButton(function (btn)
        self:OnBuyButtonClick()
    end)

end

function LuaFashionPreviewTryToBuyWnd:UpdateCostMoney()
    self.curCostMoney = 0
    local useBindJade = false
    for itemID, shopMallTemlate in pairs(self.curSelectableSequence) do
        if shopMallTemlate.CanUseBindJade > 0 then
            useBindJade = true
        end
        self.curCostMoney = self.curCostMoney + shopMallTemlate.Price
    end
    self.qnCostAndOwnMoney:SetType(useBindJade and EnumMoneyType.LingYu_WithBind or EnumMoneyType.LingYu, EnumPlayScoreKey.NONE, true)
    self.qnCostAndOwnMoney:SetCost(self.curCostMoney)

    self.buyBtn:GetComponent(typeof(Collider)).enabled = self.curCostMoney ~= 0
    self.disableSprite.enabled = self.curCostMoney == 0
end

function LuaFashionPreviewTryToBuyWnd:InitShopRoot()
    self.m_OwnedShopItems = LuaShopMallFashionPreviewMgr:GetOwnedShopItemsInfo()
    Extensions.RemoveAllChildren(self.grid.transform)
    local sortList = {}
    for itemID,shopMallTemlate in pairs(self.curSelectableSequence) do
        if shopMallTemlate.Price > 0 then
            table.insert(sortList,shopMallTemlate)
        end
    end
    table.sort(sortList, function(a, b)
        local subCategoryA = self:GetSubCategory(a.ItemId)
        local subCategoryB = self:GetSubCategory(b.ItemId)
        if subCategoryA == subCategoryB then
            return a.ItemId < b.ItemId
        else
            return subCategoryA < subCategoryB
        end
    end)

    for index,shopMallTemlate in ipairs(sortList) do
        local obj = NGUITools.AddChild(self.grid.gameObject, self.templateItem)
        self:InitShopItem(obj, shopMallTemlate)

        if index == 1 then
            self:InitShopItem(self.templateItem, shopMallTemlate)
        end
    end

    self.templateItem:SetActive(false)
    self.grid:Reposition()
    self.scrollView:ResetPosition()

    if #sortList > 2 then
        self:UpdateWndHeight()
    end
end

function LuaFashionPreviewTryToBuyWnd:GetSubCategory(itemId)
    local subCategory = 0
    local mallData = Mall_LingYuMall.GetData(itemId)
    if mallData then
        subCategory = mallData.SubCategory
    else
        local limitData = Mall_LingYuMallLimit.GetData(itemId)
        if limitData then
            subCategory = limitData.SubCategory
        end
    end
    return subCategory
end

function LuaFashionPreviewTryToBuyWnd:UpdateWndHeight()
    local top = math.abs(self.scrollView.panel.topAnchor.absolute)
    local bottom = math.abs(self.scrollView.panel.bottomAnchor.absolute)
    local bounds = NGUIMath.CalculateRelativeWidgetBounds(self.grid.transform)
    local totalHeight = top + bottom + bounds.size.y
    local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    local virtualScreenHeight = Screen.height * scale
    totalHeight = (totalHeight > virtualScreenHeight) and virtualScreenHeight or totalHeight
    self.bg.height = totalHeight
    self.scrollView.panel:ResetAndUpdateAnchors()
    self.scrollView:ResetPosition()
end

function LuaFashionPreviewTryToBuyWnd:InitShopItem(itemGo, data)
    local templateId = data.ItemId
    local showDiscount = data.Discount
    local isOwned = self.m_OwnedShopItems[templateId]
    local discountInfo
    if data.Discount > 0.05 and not data.PreSell then
        if CommonDefs.IS_VN_CLIENT then
            showDiscount = showDiscount * 10
            discountInfo = String.Format(LocalString.GetString("{0:N1}折"), showDiscount)
        elseif CommonDefs.IS_KR_CLIENT then
            showDiscount = showDiscount * 10
            discountInfo = String.Format("{0:N1}%", showDiscount)
        else
            discountInfo = String.Format(LocalString.GetString("{0:N1}折"), showDiscount)
        end
    end
    local discountLabel = itemGo.transform:Find("Discount"):GetComponent(typeof(QnLabel))
    if not String.IsNullOrEmpty(discountInfo) then
        discountLabel.Text = discountInfo
    end
    discountLabel.gameObject:SetActive(not String.IsNullOrEmpty(discountInfo))

    itemGo.transform:Find("Sprite"):GetComponent(typeof(UISprite)).spriteName = Money.GetIconName(EnumMoneyType.LingYu)

    local btn = itemGo:GetComponent(typeof(QnSelectableButton))
    btn:SetSelected(true)
    btn.OnButtonSelected = DelegateFactory.Action_bool(function (isSelected)
        self:OnItemSelected(btn, data, isSelected)
    end)

    local item = CItemMgr.Inst:GetItemTemplate(templateId)
    local texture = itemGo.transform:Find("Texture"):GetComponent(typeof(CUITexture))
    texture:LoadMaterial(item.Icon)

    itemGo.transform:Find("Name"):GetComponent(typeof(QnLabel)).Text = item.Name
    itemGo.transform:Find("Price"):GetComponent(typeof(QnLabel)).Text = tostring(data.Price)

    local disableSprite = itemGo.transform:Find("Texture/DisableSprite"):GetComponent(typeof(UISprite))
    disableSprite.enabled = false
    if IdPartition.IdIsItem(templateId) then
        disableSprite.enabled = not CItem.GetMainPlayerIsFit(templateId, true)
    elseif IdPartition.IdIsEquip(templateId) then
        disableSprite.enabled = not CEquipment.GetMainPlayerIsFit(templateId, true)
    end

    itemGo.transform:Find("Owner").gameObject:SetActive(isOwned)
    local avalibleCountLabel = itemGo.transform:Find("Texture/AvalibleCount"):GetComponent(typeof(QnLabel))
    avalibleCountLabel.Text = System.String.Format(LocalString.GetString("限购{0}"), data.AvalibleCount)
    avalibleCountLabel.gameObject:SetActive(data.AvalibleCount >= 0) 
    local soldoutSprite = itemGo.transform:Find("Texture/Soldout")
    soldoutSprite.gameObject:SetActive(data.AvalibleCount == 0)
    itemGo:SetActive(true)
end

function LuaFashionPreviewTryToBuyWnd:OnItemSelected(btn, data, isSelected)
    self.curSelectableSequence[data.ItemId] = isSelected and data or nil

    btn.transform:Find("QnCheckBox"):GetComponent(typeof(QnCheckBox)):SetSelected(isSelected, true)
    self:UpdateCostMoney()
end

function LuaFashionPreviewTryToBuyWnd:OnBuyButtonClick()
    if self.curCostMoney <= 0 then
        return
    end

    if LuaShopMallMgr.TryCheckEnoughJade(self.curCostMoney, self.qnCostAndOwnMoney.m_Type == EnumMoneyType.LingYu_WithBind) then
        self.m_ConfirmMessageList = {}
        for ItemID,item in pairs(self.curSelectableSequence) do
            if not System.String.IsNullOrEmpty(item.ConfirmMessage) then
                table.insert(self.m_ConfirmMessageList, item.ConfirmMessage)
            end
        end
        self:ConfirmBuyMallItem()
    end
end

function LuaFashionPreviewTryToBuyWnd:ConfirmBuyMallItem()
    if self.m_ConfirmMessageList and #self.m_ConfirmMessageList > 0 then
        local message = self.m_ConfirmMessageList[1]
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage(message), DelegateFactory.Action(function ()
            table.remove(self.m_ConfirmMessageList, 1)
            self:ConfirmBuyMallItem()
        end), nil, nil, nil, false)
    else
        self:BuyAllMallItems()
    end
end

function LuaFashionPreviewTryToBuyWnd:BuyAllMallItems()
    Gac2Gas.OneKeyBuyMallItemBegin()
    for ItemID,item in pairs(self.curSelectableSequence) do
        if item.AvalibleCount >= 0 then 
            RegisterTickOnce(function ()
                LuaShopMallFashionPreviewMgr:BuyMallItem(item, EShopMallRegion_lua.ELingyuMallLimit, 1)
                CUIManager.CloseUI(CLuaUIResources.FashionPreviewTryToBuyWnd)
            end, 100)
        else
            Gac2Gas.OneKeyBuyMallItem(ItemID)
        end
    end
    Gac2Gas.OneKeyBuyMallItemEnd()
end
