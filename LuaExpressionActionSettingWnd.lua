local DelegateFactory = import "DelegateFactory"
local CExpressionActionMgr = import "L10.Game.CExpressionActionMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local PlayerSettings = import "L10.Game.PlayerSettings"
local CCommonLuaWnd = import "L10.UI.CCommonLuaWnd"
local QnCheckBoxGroup = import "L10.UI.QnCheckBoxGroup"
local UITable = import "UITable"
local UIRoot=import "UIRoot"
local Screen=import "UnityEngine.Screen"
local NGUIMath=import "NGUIMath"
local Mathf = import "UnityEngine.Mathf"
local UIScrollView=import "UIScrollView"
local UIWidget = import "UIWidget"

CLuaExpressionActionSettingWnd = class()
RegistClassMember(CLuaExpressionActionSettingWnd, "settingsGroup")
RegistClassMember(CLuaExpressionActionSettingWnd, "anchor")
RegistClassMember(CLuaExpressionActionSettingWnd, "bg")
RegistClassMember(CLuaExpressionActionSettingWnd, "detailSettings")
RegistClassMember(CLuaExpressionActionSettingWnd, "settingSave")
RegistClassMember(CLuaExpressionActionSettingWnd, "settingVales")
RegistClassMember(CLuaExpressionActionSettingWnd, "m_Wnd")
RegistClassMember(CLuaExpressionActionSettingWnd, "table")

RegistChildComponent(CLuaExpressionActionSettingWnd, "ScrollView", UIScrollView)

function CLuaExpressionActionSettingWnd:Awake()
    self.m_Wnd = self.gameObject:GetComponent(typeof(CCommonLuaWnd))
    self.settingsGroup = self.transform:Find("Anchor/ScrollView/CheckBoxes"):GetComponent(typeof(QnCheckBoxGroup))
    self.table = self.transform:Find("Anchor/ScrollView/CheckBoxes"):GetComponent(typeof(UITable))
    self.anchor = self.transform:Find("Anchor")
    self.bg = self.transform:Find("Anchor/Bg"):GetComponent(typeof(UISprite))

    self.detailSettings = {
        {title = LocalString.GetString("拒绝他人公主抱")  ,propertyName = "ForbidPrincessCarry"},
        {title = LocalString.GetString("屏蔽他人壁咚特效"),propertyName = "ForbidKabeDonFx"},
        {title = LocalString.GetString("拒绝下雨自动开伞"),propertyName = "ForbidAutoUmbrellaWhenRain"},
        {title = LocalString.GetString("拒绝他人求婚")    ,propertyName = "ForbidProposal"},
        {title = LocalString.GetString("屏蔽他人演奏音效"),propertyName = "ForbidOthersZouYue"},
        {title = LocalString.GetString("拒绝他人摸头杀")  ,propertyName = "ForbidOthersMotousha"},
        {title = LocalString.GetString("拒绝他人举高高")  ,propertyName = "ForbidOthersJuGaoGao"},
        {title = LocalString.GetString("拒绝他人亲吻")    ,propertyName = "ForbidOthersKiss"},
        {title = LocalString.GetString("拒绝他人放风筝")  ,propertyName = "ForbidOthersFangFengZheng"},
        {title = LocalString.GetString("拒绝他人双人滑")  ,propertyName = "ForbidOthersShuangRenHua"},
        {title = LocalString.GetString("拒绝他人相依相偎"),propertyName = "ForbidOthersXiangYiXiangWei"}
    }

    self.settingSave = {
        self.OnSaveNotAllowEmbrace,
        self.OnSaveForbidKabeDonFx,
        self.OnSaveForbidAutoUmbrellaWhenRain,
        self.OnSaveForbidProposal,
        self.OnSaveForbidOthersZouYue,
        self.OnSaveForbidOthersMotousha,
        self.OnSaveForbidOthersJuGaoGao,
        self.OnSaveForbidOthersKiss,
        self.OnSaveForbidOthersFangFengZheng,
        self.OnSaveForbidOthersShuangRenHua,
        self.OnSaveForbidOthersXiangYiXiangWei
    }

    self.settingVales = {
        self.OnGetNotAllowEmbrace,
        self.OnGetForbidKabeDonFx,
        self.OnGetForbidAutoUmbrellaWhenRain,
        self.OnGetForbidProposal,
        self.OnGetForbidOthersZouYue,
        self.OnGetForbidOthersMotousha,
        self.OnGetForbidOthersJuGaoGao,
        self.OnGetForbidOthersKiss,
        self.OnGetForbidOthersFangFengZheng,
        self.OnGetForbidOthersShuangRenHua,
        self.OnGetForbidOthersXiangYiXiangWei
    }


end

function CLuaExpressionActionSettingWnd:Init( )
    local detailtitles = {}

    for i,v in ipairs(self.detailSettings) do
        table.insert(detailtitles,v.title)
    end

    --初始化选项内容
    self.settingsGroup:InitWithOptions(Table2Array(detailtitles,MakeArrayClass(cs_string)), true, true)

    --初始化选项是否被选中
    for i=1, #self.detailSettings do
        self.settingsGroup.m_CheckBoxes[i-1].Selected = self.settingVales[i]()
    end

    self:AdjustPosition(CExpressionActionMgr.targetCenterPos, CExpressionActionMgr.targetSize)
end

function CLuaExpressionActionSettingWnd:OnEnable()
    self.settingsGroup.OnSelect = DelegateFactory.Action_QnCheckBox_int(function(checkBox, index)
        self:OnSetDetail(checkBox, index)
    end) 
end

function CLuaExpressionActionSettingWnd:OnDisable()
    self.settingsGroup.OnSelect = nil
end

function CLuaExpressionActionSettingWnd:Update()
    self.m_Wnd:ClickThroughToClose()
end

function CLuaExpressionActionSettingWnd:AdjustPosition(worldPos, targetSize)
    

    self.table:Reposition()
    local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    local virtualScreenHeight = Screen.height * scale
    local contentTopPadding = Mathf.Abs(self.ScrollView.panel.topAnchor.absolute)
    local contentBottomPadding = Mathf.Abs(self.ScrollView.panel.bottomAnchor.absolute)
    local totalHeight = NGUIMath.CalculateRelativeWidgetBounds(self.table.transform).size.y + (self.table.padding.y * 2) + contentTopPadding + contentBottomPadding + (self.ScrollView.panel.clipSoftness.y * 2)
    local displayWndHeight = Mathf.Clamp(totalHeight, contentTopPadding + contentBottomPadding + 80, virtualScreenHeight - 100)
    self.bg:GetComponent(typeof(UIWidget)).height = Mathf.CeilToInt(displayWndHeight)
    self.ScrollView.panel:ResetAndUpdateAnchors()
    self.ScrollView:ResetPosition()

    local localPos = self.anchor.parent:InverseTransformPoint(worldPos);
    local selfBounds = NGUIMath.CalculateRelativeWidgetBounds(self.bg.transform);
    self.anchor.localPosition = Vector3(localPos.x, 0, 0);

end

function CLuaExpressionActionSettingWnd:OnSetDetail(checkBox, index)
    for i=1, #self.detailSettings do
        if index == i-1 then
            local value = self.settingsGroup.m_CheckBoxes[index].Selected
            self.settingSave[i](self, value)
        end
    end
end

-- 公主抱
function CLuaExpressionActionSettingWnd:OnGetNotAllowEmbrace()
    --return CClientMainPlayer.Inst.RelationshipProp.NotAllowEmbrace == 1
    return CClientMainPlayer.Inst.RelationshipProp.SettingInfo:GetBit(EnumPropertyRelationshipSettingBit.NotAllowEmbrace)
end

function CLuaExpressionActionSettingWnd:OnSaveNotAllowEmbrace(value)
    local notAllowEmbrace = 0
    if value then notAllowEmbrace = 1 end
    --CClientMainPlayer.Inst.RelationshipProp.NotAllowEmbrace = notAllowEmbrace;
    CClientMainPlayer.Inst.RelationshipProp.SettingInfo:SetBit(EnumPropertyRelationshipSettingBit.NotAllowEmbrace, notAllowEmbrace>0)
    Gac2Gas.RequestSetNotAllowExpressionAction("gongzhubao", notAllowEmbrace);
end

-- 壁咚特效
function CLuaExpressionActionSettingWnd:OnGetForbidKabeDonFx()
    return PlayerSettings.ForbidKabeDonFx;
end

function CLuaExpressionActionSettingWnd:OnSaveForbidKabeDonFx(value)
    PlayerSettings.ForbidKabeDonFx = value
end

-- 自动开伞
function CLuaExpressionActionSettingWnd:OnGetForbidAutoUmbrellaWhenRain()
    --return CClientMainPlayer.Inst.RelationshipProp.NotAllowAutoHoldUmbrella == 1
    return CClientMainPlayer.Inst.RelationshipProp.SettingInfo:GetBit(EnumPropertyRelationshipSettingBit.NotAllowAutoHoldUmbrella)
end

function CLuaExpressionActionSettingWnd:OnSaveForbidAutoUmbrellaWhenRain(value)
    local notAllowAutoHoldUmbrella = 0
    if value then notAllowAutoHoldUmbrella = 1 end
    --CClientMainPlayer.Inst.RelationshipProp.NotAllowAutoHoldUmbrella = notAllowAutoHoldUmbrella
    CClientMainPlayer.Inst.RelationshipProp.SettingInfo:SetBit(EnumPropertyRelationshipSettingBit.NotAllowAutoHoldUmbrella, notAllowAutoHoldUmbrella > 0)
    Gac2Gas.RequestSetNotAllowExpressionAction("autoholdumbrella", notAllowAutoHoldUmbrella)
end

-- 求婚
function CLuaExpressionActionSettingWnd:OnGetForbidProposal()
    --return CClientMainPlayer.Inst.RelationshipProp.NotAllowQiuHun == 1
    return CClientMainPlayer.Inst.RelationshipProp.SettingInfo:GetBit(EnumPropertyRelationshipSettingBit.NotAllowQiuHun)
end

function CLuaExpressionActionSettingWnd:OnSaveForbidProposal(value)
    local notAllowQiuHun = 0
    if value then notAllowQiuHun = 1 end
    --CClientMainPlayer.Inst.RelationshipProp.NotAllowQiuHun = notAllowQiuHun
    CClientMainPlayer.Inst.RelationshipProp.SettingInfo:SetBit(EnumPropertyRelationshipSettingBit.NotAllowQiuHun, notAllowQiuHun > 0)
    Gac2Gas.RequestSetNotAllowExpressionAction("qiuhun", notAllowQiuHun)
end

-- 屏蔽奏乐
function CLuaExpressionActionSettingWnd:OnGetForbidOthersZouYue()
    return PlayerSettings.GetForbidOthersZouYue() == 1
end

function CLuaExpressionActionSettingWnd:OnSaveForbidOthersZouYue(value)
    local forbidOthersZouYue = 0
    if value then forbidOthersZouYue = 1 end
    PlayerSettings.SetForbidOthersZouYue(forbidOthersZouYue)
end

-- 摸头杀
function CLuaExpressionActionSettingWnd:OnSaveForbidOthersMotousha(value)
    local notAllowMotousha = 0
    if value then notAllowMotousha = 1 end
    --CClientMainPlayer.Inst.RelationshipProp.NotAllowMoTouSha = notAllowMotousha
    CClientMainPlayer.Inst.RelationshipProp.SettingInfo:SetBit(EnumPropertyRelationshipSettingBit.NotAllowMoTouSha, notAllowMotousha > 0)
    Gac2Gas.RequestSetNotAllowExpressionAction("motousha", notAllowMotousha)
end

function CLuaExpressionActionSettingWnd:OnGetForbidOthersMotousha()
    --return CClientMainPlayer.Inst.RelationshipProp.NotAllowMoTouSha == 1
    return CClientMainPlayer.Inst.RelationshipProp.SettingInfo:GetBit(EnumPropertyRelationshipSettingBit.NotAllowMoTouSha)
end

-- 举高高
function CLuaExpressionActionSettingWnd:OnSaveForbidOthersJuGaoGao(value)
    local notAllowJuGaoGao = 0
    if value then notAllowJuGaoGao = 1 end
    --CClientMainPlayer.Inst.RelationshipProp.NotAllowJuGaoGao = notAllowJuGaoGao
    CClientMainPlayer.Inst.RelationshipProp.SettingInfo:SetBit(EnumPropertyRelationshipSettingBit.NotAllowJuGaoGao, notAllowJuGaoGao > 0)
    Gac2Gas.RequestSetNotAllowExpressionAction("jugaogao", notAllowJuGaoGao)
end

function CLuaExpressionActionSettingWnd:OnGetForbidOthersJuGaoGao()
    --return CClientMainPlayer.Inst.RelationshipProp.NotAllowJuGaoGao == 1
    return CClientMainPlayer.Inst.RelationshipProp.SettingInfo:GetBit(EnumPropertyRelationshipSettingBit.NotAllowJuGaoGao)
end

-- 亲吻
function CLuaExpressionActionSettingWnd:OnSaveForbidOthersKiss(value)
    local notAllowKiss = 0
    if value then notAllowKiss = 1 end
    --CClientMainPlayer.Inst.RelationshipProp.NotAllowKiss = notAllowKiss
    CClientMainPlayer.Inst.RelationshipProp.SettingInfo:SetBit(EnumPropertyRelationshipSettingBit.NotAllowKiss, notAllowKiss > 0)
    Gac2Gas.RequestSetNotAllowExpressionAction("kiss", notAllowKiss)
end

function CLuaExpressionActionSettingWnd:OnGetForbidOthersKiss()
    --return CClientMainPlayer.Inst.RelationshipProp.NotAllowKiss == 1
    return CClientMainPlayer.Inst.RelationshipProp.SettingInfo:GetBit(EnumPropertyRelationshipSettingBit.NotAllowKiss)
end

-- 放风筝
function CLuaExpressionActionSettingWnd:OnSaveForbidOthersFangFengZheng(value)
    local notAllowFangFengZheng = 0
    if value then notAllowFangFengZheng = 1 end
    CClientMainPlayer.Inst.RelationshipProp.SettingInfo:SetBit(EnumPropertyRelationshipSettingBit.NotAllowFangFengZheng, notAllowFangFengZheng > 0)
    Gac2Gas.RequestSetNotAllowExpressionAction("fangfengzheng", notAllowFangFengZheng)
end

function CLuaExpressionActionSettingWnd:OnGetForbidOthersFangFengZheng()
    return CClientMainPlayer.Inst.RelationshipProp.SettingInfo:GetBit(EnumPropertyRelationshipSettingBit.NotAllowFangFengZheng)
end


-- 双人滑
function CLuaExpressionActionSettingWnd:OnSaveForbidOthersShuangRenHua(value)
    local notAllowShuangRenHua = 0
    if value then notAllowShuangRenHua = 1 end
    CClientMainPlayer.Inst.RelationshipProp.SettingInfo:SetBit(EnumPropertyRelationshipSettingBit.NotAllowShuangRenHua, notAllowShuangRenHua > 0)
    Gac2Gas.RequestSetNotAllowExpressionAction("shuangrenhua", notAllowShuangRenHua)
end

function CLuaExpressionActionSettingWnd:OnGetForbidOthersShuangRenHua()
    return CClientMainPlayer.Inst.RelationshipProp.SettingInfo:GetBit(EnumPropertyRelationshipSettingBit.NotAllowShuangRenHua)
end

-- 相依相偎
function CLuaExpressionActionSettingWnd:OnSaveForbidOthersXiangYiXiangWei(value)
    local notAllow = 0
    if value then notAllow = 1 end
    CClientMainPlayer.Inst.RelationshipProp.SettingInfo:SetBit(EnumPropertyRelationshipSettingBit.NotAllowXiangYiXiangWei, notAllow > 0)
    Gac2Gas.RequestSetNotAllowExpressionAction("xiangyixiangwei", notAllow)
end

function CLuaExpressionActionSettingWnd:OnGetForbidOthersXiangYiXiangWei()
    return CClientMainPlayer.Inst.RelationshipProp.SettingInfo:GetBit(EnumPropertyRelationshipSettingBit.NotAllowXiangYiXiangWei)
end

