local CCommonLuaWnd=import "L10.UI.CCommonLuaWnd"

CLuaPropertySwitchPopupMenu = class()
RegistClassMember(CLuaPropertySwitchPopupMenu,"buttonTemplate")
RegistClassMember(CLuaPropertySwitchPopupMenu,"m_Wnd")

CLuaPropertySwitchPopupMenu.m_Mode = 0 --skill fabao

function CLuaPropertySwitchPopupMenu:Awake()
    self.m_Wnd=self.gameObject:GetComponent(typeof(CCommonLuaWnd))
    self.buttonTemplate = self.transform:Find("Anchor/Background/Item").gameObject
    self.buttonTemplate:SetActive(false)

    local anchor = self.transform:Find("Anchor")
    local grid = self.transform:Find("Anchor/Background/Grid").gameObject
    local background = self.transform:Find("Anchor/Background"):GetComponent(typeof(UISprite))

    local attrGroup = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.AttrGroups,CLuaPropertySwitchWnd.m_GroupIndex)
    if CLuaPropertySwitchPopupMenu.m_Mode==0 then
        background.height = 425
        LuaUtils.SetLocalPosition(anchor,-558,303,0)
        local skillProp = CClientMainPlayer.Inst.SkillProp
        local groups = CClientMainPlayer.Inst.PlayProp.AttrGroups
        local lookup = {}
        CommonDefs.DictIterate(groups, DelegateFactory.Action_object_object(function (k, group)
            if not lookup[group.SkillTemplateIdx] then 
                lookup[group.SkillTemplateIdx]={ tostring(k) } 
            else
                table.insert( lookup[group.SkillTemplateIdx], tostring(k) )
            end
        end))

        local str = {
            LocalString.GetString("一"),
            LocalString.GetString("二"),
            LocalString.GetString("三"),
            LocalString.GetString("四"),
            LocalString.GetString("五"),
        }
        for i=1,5 do
            str[i]=str[i].."·"..(skillProp.SkillTemplateName[i] or LocalString.GetString("新组合"))
            -- if lookup[i] then
            --     str[i] = str[i].." "..table.concat( lookup[i], "," )
            -- end
        end
        
        local activeIdx = attrGroup and attrGroup.SkillTemplateIdx or 1
        for i=1,5 do
            local go = NGUITools.AddChild(grid,self.buttonTemplate)
            go:SetActive(true)
            local bg = go:GetComponent(typeof(UISprite))
            bg.spriteName = i==activeIdx and "common_button_02(2)_highlight" or "common_button_02(2)_normal"

            local label = go.transform:Find("Label"):GetComponent(typeof(UILabel))
            label.text = str[i]

            local label2 = go.transform:Find("Label2"):GetComponent(typeof(UILabel))
            if lookup[i] then
                label2.text = table.concat( lookup[i], "," )
            end

            local lock = go.transform:Find("Lock").gameObject
            local available = CClientMainPlayer.Inst.SkillProp:IsSkillTemplateAvaliable(i)
            lock:SetActive(not available)
            CUICommonDef.SetActive(go,available,true)
            UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(g)
                Gac2Gas.RequestSetSkillTemplateInAttrGroup(CLuaPropertySwitchWnd.m_GroupIndex, i, true)
                CUIManager.CloseUI(CLuaUIResources.PropertySwitchPopupMenu)
            end)
        end
    else
        LuaUtils.SetLocalPosition(anchor,-77,303,0)
        local text = {
            LocalString.GetString("法宝·一"),
            LocalString.GetString("法宝·二"),
            LocalString.GetString("法宝·三"),
        }
        local activeIdx = attrGroup and attrGroup.TalismanSetIdx or 1
        local len = CClientMainPlayer.Inst.ItemProp.MultipleColumTalismanNum
        background.height = len * self.buttonTemplate:GetComponent(typeof(UISprite)).height + 25
        for i=1,len do
            local go = NGUITools.AddChild(grid,self.buttonTemplate)
            go:SetActive(true)
            local bg = go:GetComponent(typeof(UISprite))
            bg.spriteName = i==activeIdx and "common_button_02(2)_highlight" or "common_button_02(2)_normal"

            local label = go.transform:Find("Label"):GetComponent(typeof(UILabel))
            label.text = text[i]
            local lock = go.transform:Find("Lock").gameObject
            lock:SetActive(false)
            if i==2 then
                local available = CClientMainPlayer.Inst.PlayProp:IsOpenSecondaryTalisman()
                CUICommonDef.SetActive(go,available,true)
            end

            UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(g)
                Gac2Gas.RequestSetTalismanSetInAttrGroup(CLuaPropertySwitchWnd.m_GroupIndex,i)
                CUIManager.CloseUI(CLuaUIResources.PropertySwitchPopupMenu)
            end)
        end
    end

end

function CLuaPropertySwitchPopupMenu:Update()
    self.m_Wnd:ClickThroughToClose()
end

