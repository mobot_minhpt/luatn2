local UIEventListener = import "UIEventListener"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CItemMgr = import "L10.Game.CItemMgr"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CEquipChouBingDescTable=import "L10.UI.CEquipChouBingDescTable"
local CBaseEquipmentItem=import "L10.UI.CBaseEquipmentItem"
local CEquipChouBingCost=import "L10.UI.CEquipChouBingCost"


LuaEquipChouBingWnd = class()
RegistClassMember(LuaEquipChouBingWnd,"CHOUBING_RESET_WORD_COST_TIME")
RegistClassMember(LuaEquipChouBingWnd,"m_ItemId")
RegistClassMember(LuaEquipChouBingWnd,"closeButton")
RegistClassMember(LuaEquipChouBingWnd,"resetWordBtn")
RegistClassMember(LuaEquipChouBingWnd,"choubingBtn")
RegistClassMember(LuaEquipChouBingWnd,"newWordCount")
RegistClassMember(LuaEquipChouBingWnd,"equipItem")
RegistClassMember(LuaEquipChouBingWnd,"descTable")
RegistClassMember(LuaEquipChouBingWnd,"choubingCost")
RegistClassMember(LuaEquipChouBingWnd,"choubingCount")
RegistClassMember(LuaEquipChouBingWnd,"countLabel")
RegistClassMember(LuaEquipChouBingWnd,"tipButton")

function LuaEquipChouBingWnd:Awake()
    self.CHOUBING_RESET_WORD_COST_TIME = 100
    self.closeButton = self.transform:Find("Wnd_Bg_Primary_Normal/CloseButton").gameObject
    self.resetWordBtn = self.transform:Find("Anchor/ResetWordBtn").gameObject
    self.choubingBtn = self.transform:Find("Anchor/ChoubingBtn").gameObject
    self.newWordCount = 0
    self.equipItem = self.transform:Find("EquipItem"):GetComponent(typeof(CBaseEquipmentItem))
    self.descTable = self.transform:Find("Anchor/Desc/PreDescTable"):GetComponent(typeof(CEquipChouBingDescTable))
    self.choubingCost = self.transform:Find("Anchor/Cost"):GetComponent(typeof(CEquipChouBingCost))
    self.choubingCount = 0
    self.countLabel = self.transform:Find("Anchor/EndLoglabel"):GetComponent(typeof(UILabel))
    self.tipButton = self.transform:Find("Anchor/TipButton").gameObject


    UIEventListener.Get(self.choubingBtn).onClick = DelegateFactory.VoidDelegate(function(go) 
        self:PreChouBing(go)
    end)
    UIEventListener.Get(self.resetWordBtn).onClick = DelegateFactory.VoidDelegate(function(go) 
        self:PreResetWord(go)
    end)
    UIEventListener.Get(self.tipButton).onClick = DelegateFactory.VoidDelegate(function(go) 
        self:OnClickTipButton(go)
    end)
end

function LuaEquipChouBingWnd:Init( )

    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    local data = CItemMgr.Inst:GetById(equip.itemId)
    self.m_ItemId = equip.itemId
    if data.IsBinded then
        self.choubingCost.onlyCostBindItem = true
        self.choubingCost.isBinded = true
        self.choubingCost:ShowToggle()
    else
        self.choubingCost.onlyCostBindItem = false
        self.choubingCost.isBinded = false
        self.choubingCost:HideToggle()
    end

    if data.Equip.IsGhost then
        self.resetWordBtn:SetActive(true)
        self.countLabel.gameObject:SetActive(true)
    else
        self.resetWordBtn:SetActive(false)
        self.countLabel.gameObject:SetActive(false)
    end

    self.choubingCost.onChange = DelegateFactory.Action_bool(function (enough)
        if enough then
            CUICommonDef.SetActive(self.choubingBtn, true, true)
        else
            CUICommonDef.SetActive(self.choubingBtn, false, true)
        end
    end)
    self:InitInfo()
end
function LuaEquipChouBingWnd:InitInfo( )
    self:UpdateCount()
    self:RefreshPreEquipWordDescription()

    local equipInfo = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    self.equipItem:UpdateData(equipInfo.itemId)

    self.choubingCost:Init()

    self:UpdateResetButton()
end

function LuaEquipChouBingWnd:OnClickTipButton( go)
    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    local data = CItemMgr.Inst:GetById(equip.itemId)
    local count = 0
    if data.Equip.ChouBingData ~= nil then
        count = data.Equip.ChouBingData.ChouBingTimes
    end
    g_MessageMgr:ShowMessage("Equipment_Choubing_Fuyuan_Readme", count, self.CHOUBING_RESET_WORD_COST_TIME)
end
function LuaEquipChouBingWnd:OnEnable( )
    --
    g_ScriptEvent:AddListener("ChouBingSuccess", self, "ChouBingSuccess")
    g_ScriptEvent:AddListener("ResetWordSuccess", self, "ResetWordSuccess")
    g_ScriptEvent:AddListener("SendItem", self, "OnSendItem")
end
function LuaEquipChouBingWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("ChouBingSuccess", self, "ChouBingSuccess")
    g_ScriptEvent:RemoveListener("ResetWordSuccess", self, "ResetWordSuccess")
    g_ScriptEvent:RemoveListener("SendItem", self, "OnSendItem")
end

function LuaEquipChouBingWnd:ChouBingSuccess( args)
    local equipId=args[0]
    if equipId~=self.m_ItemId then return end
    --[ChouBingDetail]
    --VERSION 1
    --1: IsFixedWord UINT8
    --2: OldWordIndex UINT16
    --3: OldWordId UINT32
    --4: NewWordId UINT32
    --5: ChouBingTotalTimes UINT32
    --6: ChouBingTimes UINT32

    --GameSetting_Common.CHOUBING_RESET_WORD_COST_TIME  重置词条消耗的抽冰次数
    --GameSetting_Common.CHOUBING_RESET_WORD_EQUIP_COLOR 哪些颜色的装备可以重置词条
    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    if equip ~= nil then
        local item = CItemMgr.Inst:GetById(equip.itemId)
        self:UpdateNewWordDescription(item)
    end
    self:UpdateResetButton()
end
function LuaEquipChouBingWnd:UpdateNewWordDescription(item)
    local fixedWordList,extWordList = item.Equip:GetFixedWordList(false), item.Equip:GetExtWordList(false)
    local fixedWordMaxTable = CLuaEquipMgr:EquipWordIdIsMaxWord(self.m_ItemId,List2Table(fixedWordList))
    local fixedWordMaxList = Table2List(fixedWordMaxTable, MakeGenericClass(List, Boolean))

    local extWordMaxTable = CLuaEquipMgr:EquipWordIdIsMaxWord(self.m_ItemId,List2Table(extWordList))
    local extWordMaxList = Table2List(extWordMaxTable, MakeGenericClass(List, Boolean))

    local chouBingData = item.Equip.ChouBingData
    local chouBingMaxTable = CLuaEquipMgr:EquipWordIdIsMaxWord(self.m_ItemId,{chouBingData.OldWordId})

    self.descTable:Init(fixedWordList,extWordList,chouBingData,fixedWordMaxList,extWordMaxList,chouBingMaxTable[1])
    self.choubingCount = chouBingData.ChouBingTimes
    self:UpdateCount()
end

function LuaEquipChouBingWnd:UpdateCount()
    self.countLabel.text =string.format(LocalString.GetString("已置换%d次"),self.choubingCount)
end
function LuaEquipChouBingWnd:UpdatePreWordDescription(item)
    local fixedWordList,extWordList = item.Equip:GetFixedWordList(false), item.Equip:GetExtWordList(false)
    local fixedWordMaxTable = CLuaEquipMgr:EquipWordIdIsMaxWord(self.m_ItemId,List2Table(fixedWordList))
    local fixedWordMaxList = Table2List(fixedWordMaxTable, MakeGenericClass(List, Boolean))

    local extWordMaxTable = CLuaEquipMgr:EquipWordIdIsMaxWord(self.m_ItemId,List2Table(extWordList))
    local extWordMaxList = Table2List(extWordMaxTable, MakeGenericClass(List, Boolean))

    self.descTable:Init(fixedWordList,extWordList, nil,fixedWordMaxList,extWordMaxList)
    self.choubingCount = item.Equip.ChouBingData.ChouBingTimes
    self:UpdateCount()
end
function LuaEquipChouBingWnd:RefreshPreEquipWordDescription( )
    local item = CItemMgr.Inst:GetById(self.m_ItemId)
    self:UpdatePreWordDescription(item)
end
function LuaEquipChouBingWnd:ResetWordSuccess( args)
    local equipId=args[0]
    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    if equip ~= nil and equip.itemId == equipId then
        local item = CItemMgr.Inst:GetById(equip.itemId)

        local fixedWordList,extWordList = item.Equip:GetFixedWordList(false), item.Equip:GetExtWordList(false)
        local fixedWordMaxTable = CLuaEquipMgr:EquipWordIdIsMaxWord(self.m_ItemId,List2Table(fixedWordList))
        local fixedWordMaxList = Table2List(fixedWordMaxTable, MakeGenericClass(List, Boolean))

        local extWordMaxTable = CLuaEquipMgr:EquipWordIdIsMaxWord(self.m_ItemId,List2Table(extWordList))
        local extWordMaxList = Table2List(extWordMaxTable, MakeGenericClass(List, Boolean))

        self.descTable:Init(fixedWordList,extWordList, nil,fixedWordMaxList,extWordMaxList)
        self.choubingCount = item.Equip.ChouBingData.ChouBingTimes
        self:UpdateCount()
    end
    self:UpdateResetButton()
end
function LuaEquipChouBingWnd:UpdateResetButton( )
    local cnt = self:GetChouBingCount()
    if cnt < self.CHOUBING_RESET_WORD_COST_TIME then
        CUICommonDef.SetActive(self.resetWordBtn, false, true)
    else
        CUICommonDef.SetActive(self.resetWordBtn, true, true)
    end
end
function LuaEquipChouBingWnd:OnSendItem(args)
    local itemId=args[0]
    if itemId==self.m_ItemId then
        self:InitInfo()
    end
end
function LuaEquipChouBingWnd:PreChouBing( go)
    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    local item = CItemMgr.Inst:GetById(equip.itemId)
    local secondWordSet = item.Equip.HasTwoWordSet and item.Equip.IsSecondWordSetActive or false

    if item.Equip:HasTempWordDataInWordSet(secondWordSet) then
        local msg = g_MessageMgr:FormatMessage("CITIAO_NOTICE")
        -- MessageWndManager.ShowOKCancelMessage(msg, < MakeDelegateFromCSFunction >(self.ChouBing, Action0, self), nil, nil, nil, false)
        g_MessageMgr:ShowOkCancelMessage(msg, function()
            self:ChouBing()
        end, nil, nil, nil, false)

    elseif item.Equip:HasTempWordDataInWordSet(not secondWordSet) then
        local msg = g_MessageMgr:FormatMessage("CITIAO_NOTICE_OTHERSET")
        -- MessageWndManager.ShowOKCancelMessage(msg, < MakeDelegateFromCSFunction >(self.ChouBing, Action0, self), nil, nil, nil, false)
        g_MessageMgr:ShowOkCancelMessage(msg, function()
            self:ChouBing()
        end, nil, nil, nil, false)
    else
        local cnt = self:GetChouBingCount()
        if cnt == 0 then
            local msg = g_MessageMgr:FormatMessage("Equipment_Choubing_First_Confirm")
            MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
                self:ChouBing()
            end), nil, nil, nil, false)
        else
            self:ChouBing()
        end
    end
end
function LuaEquipChouBingWnd:GetChouBingCount()
    local data = CItemMgr.Inst:GetById(self.m_ItemId)
    local count = 0
    if data.Equip.ChouBingData then
        count = data.Equip.ChouBingData.ChouBingTimes
    end
    return count
end

function LuaEquipChouBingWnd:ChouBing()
    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    local item = CItemMgr.Inst:GetById(equip.itemId)
    local newWordId = math.floor(item.Equip:GetChouBingNewWordId() / 100)
    if newWordId > 0 then
        local equipTemplate = CEquipmentProcessMgr.Inst.BaptizingEquipmentTemplate
        local option = CEquipmentProcessMgr.Inst:GetWordOption(equipTemplate.Type, equipTemplate.SubType)
        if option ~= nil then
            if CommonDefs.ArrayFindIndex_uint_Array(option.Word, DelegateFactory.Predicate_uint(function (p)
                return p == newWordId
            end)) > - 1 then
                local msg = g_MessageMgr:FormatMessage("Better_ChouBing_CiTiao_Confirm")
                MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
                    self:RequestChouBing()
                end), nil, nil, nil, false)
                return
            end
        end
    end
    self:RequestChouBing()
end
function LuaEquipChouBingWnd:RequestChouBing( )
    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    local data = CItemMgr.Inst:GetById(equip.itemId)
    if data ~= nil then
        if data.IsBinded then
            Gac2Gas.RequestChouBing(EnumToInt(equip.place), equip.pos, equip.itemId, self.choubingCost.onlyCostBindItem)
        else
            Gac2Gas.RequestChouBing(EnumToInt(equip.place), equip.pos, equip.itemId, false)
        end
    end
end
function LuaEquipChouBingWnd:PreResetWord( go)
    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    local item = CItemMgr.Inst:GetById(equip.itemId)
    local secondWordSet = item.Equip.HasTwoWordSet and item.Equip.IsSecondWordSetActive or false

    if item.Equip:HasTempWordDataInWordSet(secondWordSet) then
        local msg = g_MessageMgr:FormatMessage("CITIAO_NOTICE")
        -- MessageWndManager.ShowOKCancelMessage(msg, < MakeDelegateFromCSFunction >(self.ResetWord, Action0, self), nil, nil, nil, false)
        g_MessageMgr:ShowOkCancelMessage(msg, function()
            self:ResetWord()
        end, nil, nil, nil, false)
    elseif item.Equip:HasTempWordDataInWordSet(not secondWordSet) then
        local msg = g_MessageMgr:FormatMessage("CITIAO_NOTICE_OTHERSET")
        -- MessageWndManager.ShowOKCancelMessage(msg, < MakeDelegateFromCSFunction >(self.ResetWord, Action0, self), nil, nil, nil, false)
        g_MessageMgr:ShowOkCancelMessage(msg, function()
            self:ResetWord()
        end, nil, nil, nil, false)
    else
        self:ResetWord()
    end
end
function LuaEquipChouBingWnd:ResetWord( )
    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    local cnt = self:GetChouBingCount()
    local msg = g_MessageMgr:FormatMessage("Equipment_Choubing_Fuyuan_Confirm", cnt, self.CHOUBING_RESET_WORD_COST_TIME)

    MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
        Gac2Gas.RequestResetWord(EnumToInt(equip.place), equip.pos, equip.itemId)
    end), nil, nil, nil, false)
end

