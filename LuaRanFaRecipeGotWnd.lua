local CChatLinkMgr=import "CChatLinkMgr"
local UILabel = import "UILabel"

CLuaRanFaRecipeGotWnd = class()
CLuaRanFaRecipeGotWnd.Path = "ui/ranfa/LuaRanFaRecipeGotWnd"

function CLuaRanFaRecipeGotWnd:Init()
    local designData = RanFaJi_RanFaJi.GetData(CLuaRanFaMgr.m_NewRecipeId)
    if not designData then
        CUIManager.CloseUI(CLuaUIResources.RanFaRecipeGotWnd)
        return
    end
    self.transform:Find("Offset/RecipeNameLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("%s染发配方"), designData.ColorName)
    self.transform:Find("Offset/RecipeNameLabel/Texture"):GetComponent(typeof(UITexture)).color = NGUIText.ParseColor24(designData.ColorRGB, 0)
    self.transform:Find("Offset/TipLabel"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("RanFaJi_Recipe_Got_Tip")
    local index = string.find(designData.FormulaDescription, "r[^r]*$")
    self.transform:Find("Offset/DescLabel"):GetComponent(typeof(UILabel)).text = CChatLinkMgr.TranslateToNGUIText(string.sub(designData.FormulaDescription, 0, index), false)
    UIEventListener.Get(self.transform:Find("Offset/MakeBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ()
        CLuaRanFaMgr:OpenRecipeWnd(CLuaRanFaMgr.m_NewRecipeId)
        CUIManager.CloseUI(CLuaUIResources.RanFaRecipeGotWnd)
    end)
end
