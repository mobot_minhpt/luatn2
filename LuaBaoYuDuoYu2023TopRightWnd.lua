local UITexture = import "UITexture"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local UISlider = import "UISlider"
local CScene = import "L10.Game.CScene"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaBaoYuDuoYu2023TopRightWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaBaoYuDuoYu2023TopRightWnd, "ExplandBtn", "ExplandBtn", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023TopRightWnd, "tipPanel", "tipPanel", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023TopRightWnd, "PhaseInfo", "PhaseInfo", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023TopRightWnd, "Slider", "Slider", UISlider)
RegistChildComponent(LuaBaoYuDuoYu2023TopRightWnd, "BossIcon1", "BossIcon1", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023TopRightWnd, "BossIcon2", "BossIcon2", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023TopRightWnd, "BossIcon3", "BossIcon3", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023TopRightWnd, "RewardInfo", "RewardInfo", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023TopRightWnd, "ConsumeTime", "ConsumeTime", UILabel)
RegistChildComponent(LuaBaoYuDuoYu2023TopRightWnd, "RewardIcon", "RewardIcon", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023TopRightWnd, "GameplayHPBar", "GameplayHPBar", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023TopRightWnd, "TimeSlider", "TimeSlider", UITexture)
RegistChildComponent(LuaBaoYuDuoYu2023TopRightWnd, "divideLine", "divideLine", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023TopRightWnd, "RemainReviveTime", "RemainReviveTime", UILabel)

--@endregion RegistChildComponent end

function LuaBaoYuDuoYu2023TopRightWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

    self:InitWndData()
    self:RefreshConstUI()
    self:InitUIEvent()
    self:RefreshVariableUI()
end

function LuaBaoYuDuoYu2023TopRightWnd:Init()

end

--@region UIEvent

--@endregion UIEvent

function LuaBaoYuDuoYu2023TopRightWnd:InitWndData()
    self.phaseCount = 3
    self.bossIconList = {self.BossIcon1, self.BossIcon2, self.BossIcon3}

    self.bossIconMaterialList = {}
    local BossIconRawData = Double11_BYDLY.GetData().BossIconMaterialList
    local rawDataList = g_LuaUtil:StrSplit(BossIconRawData, ";")
    for i = 1, #rawDataList do
        if rawDataList[i] ~= "" then
            table.insert(self.bossIconMaterialList, rawDataList[i])
        end
    end
    
    self.timeProgressMaxWidth = 356

    self.eliteSeconds = tonumber(g_LuaUtil:StrSplit(Double11_BYDLY.GetData().EliteAwardInfo, ",")[2])
end

function LuaBaoYuDuoYu2023TopRightWnd:InitUIEvent()
    UIEventListener.Get(self.ExplandBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnExplandBtnClick()
    end)

    for i = 1, 3 do
        local bossIcon = self.bossIconList[i]
        UIEventListener.Get(bossIcon).onClick = DelegateFactory.VoidDelegate(function (go)
            g_MessageMgr:ShowMessage("BAOYUDUOLINGYU_PHASE" .. i .. "_TIPS")
        end)
    end
end

function LuaBaoYuDuoYu2023TopRightWnd:OnExplandBtnClick()
    self.tipPanel:SetActive(false)
    self.ExplandBtn.transform.localEulerAngles = Vector3(0, 0, 0)
    CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

function LuaBaoYuDuoYu2023TopRightWnd:OnHideTopAndRightTipWnd()
    self.tipPanel:SetActive(true)
    self.ExplandBtn.transform.localEulerAngles = Vector3(0, 0, 180)
end

function LuaBaoYuDuoYu2023TopRightWnd:RefreshVariableUI()
    if LuaShuangshiyi2023Mgr.m_BaoYuDuoYuPlayInfo then
        --refresh bossIconList
        self.Slider.value = (LuaShuangshiyi2023Mgr.m_BaoYuDuoYuPlayInfo.stage-1) / (self.phaseCount-1)
        for i = 1, #self.bossIconList do
            local bossIconObj = self.bossIconList[i]
            local bossIconTexture = bossIconObj.transform:Find("Icon"):GetComponent(typeof(UITexture))

            bossIconObj.transform:Find("Highlight").gameObject:SetActive(LuaShuangshiyi2023Mgr.m_BaoYuDuoYuPlayInfo.stage == i)
            bossIconObj.transform:Find("Normal").gameObject:SetActive(LuaShuangshiyi2023Mgr.m_BaoYuDuoYuPlayInfo.stage ~= i)
            bossIconObj.transform:Find("IsFinished").gameObject:SetActive(LuaShuangshiyi2023Mgr.m_BaoYuDuoYuPlayInfo.stage > i)
            Extensions.SetLocalPositionZ(bossIconObj.transform:Find("Normal"), LuaShuangshiyi2023Mgr.m_BaoYuDuoYuPlayInfo.stage > i and -1 or 0)
            
            if LuaShuangshiyi2023Mgr.m_BaoYuDuoYuPlayInfo.stage == i then
                --icon设置为104*104
                bossIconTexture.width = 104
                bossIconTexture.height = 104
            else
                --icon设置为80*80
                bossIconTexture.width = 80
                bossIconTexture.height = 80
            end
        end
        
        self:RefreshTime()
    end
end 

function LuaBaoYuDuoYu2023TopRightWnd:RefreshTime()
    local curtime =  LuaShuangshiyi2023Mgr.m_BaoYuDuoYuPlayInfo.curtime
    local timelimit = LuaShuangshiyi2023Mgr.m_BaoYuDuoYuPlayInfo.timelimit
    local standardTimestamp = CServerTimeMgr.Inst.timeStamp
    local formatTime = math.max(0, timelimit - curtime)
    self.ConsumeTime.text = SafeStringFormat3("[ACF9FF]%02d:%02d", math.floor(formatTime / 60), formatTime % 60)
    self:RefreshTimeSlider(curtime, 1 - curtime / LuaShuangshiyi2023Mgr.m_BaoYuDuoYuPlayInfo.timelimit)
    
    if self.m_countDownTick then
        UnRegisterTick(self.m_countDownTick)
        self.m_countDownTick = nil
    end

    self.m_countDownTick = RegisterTick(function ()
        local nowtimeStamp = CServerTimeMgr.Inst.timeStamp
        local nowtime = curtime + nowtimeStamp - standardTimestamp
        local formatTime = math.max(0, timelimit - nowtime)
        self.ConsumeTime.text = SafeStringFormat3("[ACF9FF]%02d:%02d", math.floor(formatTime / 60), formatTime % 60)
        self:RefreshTimeSlider(nowtime, 1 - nowtime / LuaShuangshiyi2023Mgr.m_BaoYuDuoYuPlayInfo.timelimit)
    end, 1000)
end

function LuaBaoYuDuoYu2023TopRightWnd:OnSyncDouble11BYDLYHeroReliveTimes(reliveTimes)
    local totalReliveTime = Double11_BYDLY.GetData().HeroReliveTimes
    self.RemainReviveTime.text = totalReliveTime-reliveTimes
end

function LuaBaoYuDuoYu2023TopRightWnd:RefreshTimeSlider(curtime, sliderValue)
    self.TimeSlider.width = sliderValue * self.timeProgressMaxWidth

    --refresh rewardInfo
    --英雄模式 且 到了第三阶段
    self.RemainReviveTime.gameObject:SetActive(LuaShuangshiyi2023Mgr.m_BaoYuDuoYuPlayInfo.isHero == true
        and LuaShuangshiyi2023Mgr.m_BaoYuDuoYuPlayInfo.stage == 3)
    self.divideLine:SetActive(LuaShuangshiyi2023Mgr.m_BaoYuDuoYuPlayInfo.isHero == true and curtime < self.eliteSeconds)
    self.RewardIcon:SetActive(LuaShuangshiyi2023Mgr.m_BaoYuDuoYuPlayInfo.isHero == true and curtime < self.eliteSeconds)
end

function LuaBaoYuDuoYu2023TopRightWnd:OnDestroy()
    if self.m_countDownTick then
        UnRegisterTick(self.m_countDownTick)
        self.m_countDownTick = nil
    end
end

function LuaBaoYuDuoYu2023TopRightWnd:RefreshConstUI()
    if LuaShuangshiyi2023Mgr.m_BaoYuDuoYuPlayInfo then
        for i = 1, #self.bossIconList do
            local bossIconObj = self.bossIconList[i]
            local bossIconTexture = bossIconObj.transform:Find("Icon"):GetComponent(typeof(CUITexture))
            bossIconTexture:LoadMaterial(self.bossIconMaterialList[i])
        end

        --eliteSeconds
        self.divideLine.transform:GetComponent(typeof(UITexture)).width = self.eliteSeconds / LuaShuangshiyi2023Mgr.m_BaoYuDuoYuPlayInfo.timelimit * self.timeProgressMaxWidth
        self:OnSyncDouble11BYDLYHeroReliveTimes(LuaShuangshiyi2023Mgr.m_heroReliveTimes and LuaShuangshiyi2023Mgr.m_heroReliveTimes or 0)
    end
end

function LuaBaoYuDuoYu2023TopRightWnd:RefreshUI()
    self:RefreshVariableUI()
    self:RefreshConstUI()
end

function LuaBaoYuDuoYu2023TopRightWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncDouble11BYDLYPlayInfo", self, "RefreshUI")
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("SyncDouble11BYDLYHeroReliveTimes", self, "OnSyncDouble11BYDLYHeroReliveTimes")
end 

function LuaBaoYuDuoYu2023TopRightWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncDouble11BYDLYPlayInfo", self, "RefreshUI")
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("SyncDouble11BYDLYHeroReliveTimes", self, "OnSyncDouble11BYDLYHeroReliveTimes")

end
