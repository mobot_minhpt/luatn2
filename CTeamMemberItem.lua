-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local CPropertyAppearance = import "L10.Game.CPropertyAppearance"
local CTeamMatchMgr = import "L10.Game.CTeamMatchMgr"
local CTeamMemberItem = import "L10.UI.Team.CTeamMemberItem"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CUIManager = import "L10.UI.CUIManager"
local DelegateFactory = import "DelegateFactory"
local Double = import "System.Double"
local EGetPlayerAppearanceContext = import "L10.Game.EGetPlayerAppearanceContext"
local Initialization_Init = import "L10.Game.Initialization_Init"
local L10 = import "L10"
local NGUIText = import "NGUIText"
local Object = import "System.Object"
local Profession = import "L10.Game.Profession"
local Transform_Transform = import "L10.Game.Transform_Transform"
local UIEventListener = import "UIEventListener"
local CPropertySkill = import "L10.Game.CPropertySkill"
local Screen = import "UnityEngine.Screen"
-- !!! 修改prefab时记得同步修改TeamGroupWnd和TeamWnd
CTeamMemberItem.m_Awake_CS2LuaHook = function (this)
    UIEventListener.Get(this.acceptBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        if this.acceptAction ~= nil then
            GenericDelegateInvoke(this.acceptAction, Table2ArrayWithCount({this}, 1, MakeArrayClass(Object)))
        end
    end)
    UIEventListener.Get(this.refuseBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        if this.refuseAction ~= nil then
            GenericDelegateInvoke(this.refuseAction, Table2ArrayWithCount({this}, 1, MakeArrayClass(Object)))
        end
    end)

    this.acceptBtn:SetActive(false)
    this.refuseBtn:SetActive(false)
end
CTeamMemberItem.m_UpdateAppearance_CS2LuaHook = function (this, targetId, context)
    if this.memberData ~= nil and targetId == this.memberData.m_MemberId and context == EGetPlayerAppearanceContext.UpdateTeamMember then
        this.appearanceData = CClientPlayerMgr.Inst:GetPlayerAppearance(this.memberData.m_MemberId)
        if LuaShuJia2021Mgr.IsDaMoWangPlay() and LuaShuJia2021Mgr.isBoss then
            this.modelTexture.mainTexture = CUIManager.CreateModelTexture(System.String.Format("__{0}__", this:GetInstanceID()), this, 180, 0.12, -0.82, 5.5, false, true, 0.3)
        else
        this.modelTexture.mainTexture = CUIManager.CreateModelTexture(System.String.Format("__{0}__", this:GetInstanceID()), this, 180, 0.12, -0.82, 5.5, false, true, 1)
        end
        LuaTeamMemberItem:InitRotationList(System.String.Format("__{0}__", this:GetInstanceID()), 180)
        LuaTeamMemberItem:SetRoleRotate(System.String.Format("__{0}__", this:GetInstanceID()),this)
    end
end
CTeamMemberItem.m_Load_CS2LuaHook = function (this, renderObj)
    if this.memberData == nil then
        return
    end
    if this.appearanceData ~= nil then
        CClientMainPlayer.LoadResource(renderObj, this.appearanceData, true, 1, 0, false, this.appearanceData.ResourceId, false, 0, false, nil)

        if this.appearanceData.ResourceId ~= 0 then
            local scale = 1
            local data = Transform_Transform.GetData(this.appearanceData.ResourceId)
            if data ~= nil then
                scale = data.PreviewScale
            end
            renderObj.Scale = scale
        end
    else
        local init = Initialization_Init.GetData(EnumToInt(this.memberData.m_MemberClass) * 100 + EnumToInt(this.memberData.m_MemberGender))
        if init ~= nil then
            local curAppearanceData = CPropertyAppearance.GetDataFromCustom(this.memberData.m_MemberClass, this.memberData.m_MemberGender, CPropertySkill.GetDefaultYingLingStateByGender(this.memberData.m_MemberGender), init.NewRoleEquip3, nil, 0, init.Head, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, this.memberData.IsInXianShenStatus and 1 or 0, nil, nil, nil, nil, 0, 0, 0, 0, 0, 0, nil)
            CClientMainPlayer.LoadResource(renderObj, curAppearanceData, true, 1, 0, false, 0, false, 0, false, nil)
        end
    end
    if this.transform then
        local panel = this.transform:Find("Panel")
        if panel then panel.gameObject:SetActive(true) end
    end
end
CTeamMemberItem.m_OnClick_CS2LuaHook = function (this)
    do
        local i = 0
        while i < CTeamMemberItem.items.Count do
            CTeamMemberItem.items[i].selectedSprite.gameObject:SetActive(CTeamMemberItem.items[i] == this and this.memberData ~= nil)
            i = i + 1
        end
    end
    if this.clickCallback ~= nil then
        GenericDelegateInvoke(this.clickCallback, Table2ArrayWithCount({this}, 1, MakeArrayClass(Object)))
    end
end
CTeamMemberItem.m_Init_CS2LuaHook = function (this, info, firstInit)
    this.memberData = info
    this.memberInfoRoot:SetActive(info ~= nil)
    this.teamMatchLabel.enabled = (info == nil and CTeamMgr.Inst:MainPlayerIsTeamLeader() and CTeamMatchMgr.Inst.IsTeamMatching)
    if info ~= nil then
        this.leaderIcon.enabled = CTeamMgr.Inst:IsTeamLeader(info.m_MemberId)
        if this.leaderIcon.enabled then
            this.leaderIcon.spriteName =  CTeamMgr.Inst:GetLeaderIcon(info.m_MemberId)
        end
        this.professionIcon.spriteName = Profession.GetIcon(info.m_MemberClass)
        local default
        if info.IsInXianShenStatus then
            default = L10.Game.Constants.ColorOfFeiSheng
        else
            default = NGUIText.EncodeColor24(this.levelLabel.color)
        end
        this.levelLabel.text = System.String.Format("[c][{0}]lv. {1}[-][/c]", default, info.m_MemberLevel)
        this.nameLabel.text = info.m_MemberName
        this.xiuweiLabel.text = NumberComplexToString(NumberTruncate(info.BasicProp.XiuWeiGrade, 1), typeof(Double), "F1")
        --一位小数显示
        this.xiulianLabel.text = NumberComplexToString(NumberTruncate(info.BasicProp.XiuLian, 1), typeof(Double), "F1")
        if firstInit then
            this.modelTexture.mainTexture = nil
            --初始不显示默认角色模型
            CClientPlayerMgr.Inst:RequestGetPlayerAppearance(info.m_MemberId, EGetPlayerAppearanceContext.UpdateTeamMember)
        end
        if this.memberBg and this.memberBorder then
            if CTeamMgr.Inst:IsPlayerInTeam(info.m_MemberId) and CTeamMgr.Inst:ExistSpecialTeamBg() then --在队伍界面而非申请界面
                this.memberBg:Clear()
                this.memberBorder:Clear()
            else
                this.memberBg:LoadMaterial(CTeamMgr.Inst:GetMemberBg(info.MemberBg))
                this.memberBorder:LoadMaterial(CTeamMgr.Inst:GetMemberBorder(info.MemberBg))
            end
        end
    else
        this.leaderIcon.enabled = false
        this.professionIcon.spriteName = nil
        this.levelLabel.text = nil
        this.nameLabel.text = nil
        this.xiuweiLabel.text = nil
        this.xiulianLabel.text = nil
        this.modelTexture.mainTexture = nil
        if this.memberBg and this.memberBorder then
            this.memberBg:LoadMaterial(CTeamMgr.Inst:GetMemberBg(0))
            this.memberBorder:Clear()
        end
    end
end

LuaTeamMemberItem = {}

LuaTeamMemberItem.Identify2Rotation = nil

function LuaTeamMemberItem:InitRotationList(identify, rotation)
    if not self.Identify2Rotation then
        self.Identify2Rotation = {}
    end
    if not self.Identify2Rotation[identify] then
        self.Identify2Rotation[identify] = rotation
    end
end

function LuaTeamMemberItem:SetRoleRotate(identify,textureItem)
    UIEventListener.Get(textureItem.gameObject).onDrag = DelegateFactory.VectorDelegate(function(go,delta) 
        if self.Identify2Rotation and self.Identify2Rotation[identify] then
            local deltaVal = -delta.x / Screen.width * 360
            self.Identify2Rotation[identify] = self.Identify2Rotation[identify] + deltaVal
            CUIManager.SetModelRotation(identify, self.Identify2Rotation[identify])
        end
    end)
end 