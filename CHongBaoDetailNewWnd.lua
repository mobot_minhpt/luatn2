-- Auto Generated!!
local CHongBaoDetailNewItem = import "L10.UI.CHongBaoDetailNewItem"
local CHongBaoDetailNewWnd = import "L10.UI.CHongBaoDetailNewWnd"
local CHongBaoMgr = import "L10.UI.CHongBaoMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CTickMgr = import "L10.Engine.CTickMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local DelegateFactory = import "DelegateFactory"
local EnumHongBaoEvent = import "L10.UI.EnumHongBaoEvent"
local ETickType = import "L10.Engine.ETickType"
local HongBao_System = import "L10.Game.HongBao_System"
local SoundManager = import "SoundManager"
local String = import "System.String"
local System = import "System"
local Vector3 = import "UnityEngine.Vector3"
CHongBaoDetailNewWnd.m_Init_CS2LuaHook = function (this) 
    LuaHongBaoDetailNewWnd:InitDetailWndBg(this)
    this.m_Table.m_DataSource = this

    this.m_MoneyLabel.text = ""
    this.m_DescriptionLabel.text = ""
    this.m_CountLabel.text = ""
    if CHongBaoMgr.s_HongBaoEvent == EnumHongBaoEvent.HuaKui and this.HuakuiHongbaoTex and this.BangHuaHongbaoTex and this.HuakuiObj then
        this.HuakuiHongbaoTex.gameObject:SetActive(true)
        this.BangHuaHongbaoTex.gameObject:SetActive(false)
        this.HuakuiObj:SetActive(true)
    elseif CHongBaoMgr.s_HongBaoEvent == EnumHongBaoEvent.BangHua and this.HuakuiHongbaoTex and this.BangHuaHongbaoTex and this.HuakuiObj then
        this.HuakuiHongbaoTex.gameObject:SetActive(false)
        this.BangHuaHongbaoTex.gameObject:SetActive(true)
        this.HuakuiObj:SetActive(false)
    else
        if this.HuakuiHongbaoTex then
            this.HuakuiHongbaoTex.gameObject:SetActive(false)
        end
        if this.BangHuaHongbaoTex then
            this.BangHuaHongbaoTex.gameObject:SetActive(false)
        end
        if this.HuakuiObj then
            this.HuakuiObj:SetActive(false)
        end
    end
    if not System.String.IsNullOrEmpty(CHongBaoMgr.s_HongBaoId) then
        Gac2Gas.RequestHongBaoDetails(CHongBaoMgr.s_HongBaoId)
    else
        this:Close()
    end
    if this.m_VoiceFlagObj ~= nil then
        this.m_VoiceFlagObj:SetActive(false)
    end
end
CHongBaoDetailNewWnd.m_OnReplyHongBaoDetails_CS2LuaHook = function (this, data) 
    this.m_DetailData = data
    if this.m_VoiceFlagObj ~= nil and data.IsVoiceHongbao then
        this.m_VoiceFlagObj:SetActive(true)
    end
    if this.m_DetailData ~= nil then
        this.m_PlayerNameLabel.text = this.m_DetailData.OwnerName
        this.m_DescriptionLabel.text = this.m_DetailData.HongBaoDescription
        this.m_CountLabel.text = System.String.Format("{0}/{1}", this.m_DetailData.SnatchNumber, this.m_DetailData.TotalCount)
        this.m_MoneyLabel.text = tostring(this.m_DetailData.TotalSilver)
        if this.m_DetailData.SnatchAmount > 0 then
            this.m_SnatchingLabel.gameObject:SetActive(true)
            this.m_SnatchedObj:SetActive(false)
            --m_SnatchingLabel.text = m_DetailData.SnatchRecords[0].Silver.ToString();
            if CommonDefs.DictContains(CHongBaoMgr.Inst.m_SnatchedTime, typeof(String), CHongBaoMgr.s_HongBaoId) and CommonDefs.DictGetValue(CHongBaoMgr.Inst.m_SnatchedTime, typeof(String), CHongBaoMgr.s_HongBaoId) - CServerTimeMgr.Inst.timeStamp < 3 then
                CommonDefs.DictRemove(CHongBaoMgr.Inst.m_SnatchedTime, typeof(String), CHongBaoMgr.s_HongBaoId)
                this:ShowMoney(this.m_DetailData.SnatchAmount)
            else
                this.m_SnatchingLabel.text = tostring(this.m_DetailData.SnatchAmount)
            end
        else
            if this.m_DetailData.SnatchNumber >= this.m_DetailData.TotalCount then
                this.m_SnatchingLabel.gameObject:SetActive(false)
                this.m_SnatchedObj:SetActive(true)
            else
                this.m_SnatchingLabel.gameObject:SetActive(true)
                this.m_SnatchedObj:SetActive(false)
                this.m_SnatchingLabel.text = "0"
            end
        end
        if data.Clazz == 0 then
            this.m_IsSystemHongbao = true
            local key = 0
            if not System.String.IsNullOrEmpty(CHongBaoMgr.s_HongBaoId) then
                local arr = CommonDefs.StringSplit_ArrayChar_StringSplitOptions(CHongBaoMgr.s_HongBaoId, Table2ArrayWithCount({"_"}, 1, MakeArrayClass(String)), System.StringSplitOptions.RemoveEmptyEntries)
                if arr.Length > 0 then
                    local default
                    default, key = System.UInt32.TryParse(arr[arr.Length - 1])
                end
            end
            local ret = HongBao_System.GetData(key)
            if ret ~= nil then
                this.m_ExpressionTex:LoadNPCPortrait(ret.Icon, false)
            end
        else
            this.m_ExpressionTex:LoadNPCPortrait(CUICommonDef.GetPortraitName(data.Clazz, data.Gender, data.Expression), false)
            this.m_ExpressionTxtTex:LoadMaterial(CUICommonDef.GetExpressionTxtPath(data.ExpressionTxt))
        end

        this.m_Table:ReloadData(false, false)
        -- 需要放在判断是否系统红包后
    end
end
CHongBaoDetailNewWnd.m_ShowMoney_CS2LuaHook = function (this, money) 
    local tmp = 1 local interval = math.floor(money / CHongBaoDetailNewWnd.s_MoneyIncDuration) + 1
    this.m_SnatchingHongBaoFx:LoadFx(CUIFxPaths.SnatchingHongBaoFx)
    SoundManager.Inst:PlayOneShot(SoundManager.UISoundEvents.HongBaoSnatchSound, Vector3.zero, nil, 0)
    this.m_ShowMoneyTick = CTickMgr.Register(DelegateFactory.Action(function () 
        if tmp >= money then
            tmp = money
            if this.m_ShowMoneyTick ~= nil then
                invoke(this.m_ShowMoneyTick)
            end
            this.m_SnatchingHongBaoFx:DestroyFx()
        end
        this.m_SnatchingLabel.text = tostring(tmp)
        tmp = tmp + interval
    end), 5, ETickType.Loop)
end
CHongBaoDetailNewWnd.m_ItemAt_CS2LuaHook = function (this, view, row) 
    local item = TypeAs(view:GetFromPool(0), typeof(CHongBaoDetailNewItem))
    if this.m_DetailData ~= nil and this.m_DetailData.SnatchRecords ~= nil and this.m_DetailData.SnatchRecords.Count > row then
        local record = this.m_DetailData.SnatchRecords[row]
        item:UpdateData(record, this.m_IsSystemHongbao)
    end
    return item
end
LuaHongBaoDetailNewWnd = {}
function LuaHongBaoDetailNewWnd:InitDetailWndBg(wnd)
    if CHongBaoMgr.s_HongBaoEvent == EnumHongBaoEvent.HuaKui or CHongBaoMgr.s_HongBaoEvent == EnumHongBaoEvent.BangHua then return end
    local coverType = CLuaHongBaoMgr.m_DetailHongBaoCoverType or 0
    local coverData = HongBao_Cover.GetData(coverType)
    if not coverData then return end
    local colorList = coverData.ColorList
    local bg = wnd.transform:Find("Texture").gameObject:GetComponent(typeof(CUITexture))
    local sprite1 = wnd.transform:Find("Sprite1").gameObject:GetComponent(typeof(UITexture))
    local sprite2 = wnd.transform:Find("Sprite2").gameObject:GetComponent(typeof(UITexture))
    local sprite3 = wnd.transform:Find("Sprite3").gameObject:GetComponent(typeof(UISprite))
    local splitSprite = wnd.transform:Find("QnTableView/Pool/HongbaoDetailItem/Split").gameObject:GetComponent(typeof(UISprite))
    bg:LoadMaterial("UI/Texture/Transparent/Material/"..coverData.DetailResource..".mat")
    sprite1.color = NGUIText.ParseColor24(colorList[0],0)
    sprite2.color = NGUIText.ParseColor24(colorList[1],0)
    sprite3.color = NGUIText.ParseColor24(colorList[2],0)
    splitSprite.color = NGUIText.ParseColor24(colorList[3],0)
end

