local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local QnButton = import "L10.UI.QnButton"
local Extensions = import "Extensions"
local CUITexture = import "L10.UI.CUITexture"

LuaQMPKWuXingAdjustWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaQMPKWuXingAdjustWnd, "Circle", "Circle", GameObject)
RegistChildComponent(LuaQMPKWuXingAdjustWnd, "ConfirmBtn", "ConfirmBtn", QnButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaQMPKWuXingAdjustWnd, "m_SelectedWuXing")
RegistClassMember(LuaQMPKWuXingAdjustWnd, "m_WuXings")
RegistClassMember(LuaQMPKWuXingAdjustWnd, "m_CurWuXing")

function LuaQMPKWuXingAdjustWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    local mainPlayer = (CClientMainPlayer.Inst or {})
    local wuXing = (mainPlayer.BasicProp or {}).MingGe
    self.m_CurWuXing = wuXing
    self.m_SelectedWuXing = wuXing
    self.m_WuXings = {}
    for i = 0, self.Circle.transform.childCount - 1 do
        local item = self.Circle.transform:Find("WuXing".. i+1)
        self:InitWuXing(item, i + 1, wuXing == i + 1)
        table.insert(self.m_WuXings, item)
    end

    self.ConfirmBtn.OnClick = DelegateFactory.Action_QnButton(function (go)
        self:OnConfirmBtnClick()
    end)

    self.ConfirmBtn.Enabled = self.m_SelectedWuXing ~= self.m_CurWuXing
end

function LuaQMPKWuXingAdjustWnd:Init()
end

--@region UIEvent

--@endregion UIEvent

function LuaQMPKWuXingAdjustWnd:InitWuXing(transform, wuxing, selected)
    local btn       = transform:GetComponent(typeof(QnButton))
    local texture   = transform:GetComponent(typeof(UITexture))
    local arrow     = transform:Find("Arrow"):GetComponent(typeof(UITexture))
    local selectedGo= transform:Find("Selected").gameObject

    btn.OnClick = DelegateFactory.Action_QnButton(function (go)
        self:OnWuXingClick(wuxing)
    end)

    local width = selected and 128 or 90
    texture.width = width
    texture.height = width
    Extensions.SetLocalPositionZ(arrow.transform, selected and 0 or -1)
    arrow.alpha = selected and 1 or 0.3
    selectedGo:SetActive(selected)
end

function LuaQMPKWuXingAdjustWnd:RefreshWuXing(transform, selected)
    local arrow     = transform:Find("Arrow"):GetComponent(typeof(UITexture))
    local selectedGo= transform:Find("Selected").gameObject
    local texture   = transform:GetComponent(typeof(UITexture))

    local width = selected and 128 or 90
    texture.width = width
    texture.height = width
    Extensions.SetLocalPositionZ(arrow.transform, selected and 0 or -1)
    arrow.alpha = selected and 1 or 0.3
    selectedGo:SetActive(selected)
end

function LuaQMPKWuXingAdjustWnd:OnWuXingClick(wuXing)
    self.m_SelectedWuXing = wuXing
    for i = 1, #self.m_WuXings do
        self:RefreshWuXing(self.m_WuXings[i], wuXing == i)
    end
    self.ConfirmBtn.Enabled = self.m_SelectedWuXing ~= self.m_CurWuXing
end

function LuaQMPKWuXingAdjustWnd:OnConfirmBtnClick()
    if self.m_SelectedWuXing then
        Gac2Gas.RequestSetQmpkSoulCoreWuXing(self.m_SelectedWuXing)
    end
    CUIManager.CloseUI(CLuaUIResources.QMPKWuXingAdjustWnd)
end