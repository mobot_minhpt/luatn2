local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
CLuaDouDiZhu2020ResultWnd = class()
RegistClassMember(CLuaDouDiZhu2020ResultWnd,"m_BaseNode")
RegistClassMember(CLuaDouDiZhu2020ResultWnd,"m_OpenChestNode")
RegistClassMember(CLuaDouDiZhu2020ResultWnd,"m_ResultNode")
RegistClassMember(CLuaDouDiZhu2020ResultWnd,"m_Chest")
RegistClassMember(CLuaDouDiZhu2020ResultWnd,"m_OpenCount")
RegistClassMember(CLuaDouDiZhu2020ResultWnd,"m_ChestLabel")
RegistClassMember(CLuaDouDiZhu2020ResultWnd,"m_ChestLabel2")

CLuaDouDiZhu2020ResultWnd.s_IsWin = false
CLuaDouDiZhu2020ResultWnd.s_ChestCount = 0
CLuaDouDiZhu2020ResultWnd.s_IsDiZhu= false
CLuaDouDiZhu2020ResultWnd.s_IsPlayOver = false
CLuaDouDiZhu2020ResultWnd.s_AllChestCount = 0
-- CLuaDouDiZhu2020ResultWnd.m_Rank = 0
-- CLuaDouDiZhu2020ResultWnd.m_Score = 0
function CLuaDouDiZhu2020ResultWnd:Init()
    self.m_OpenCount = 0
    self.m_BaseNode = self.transform:Find("Base").gameObject
    self.m_OpenChestNode = self.transform:Find("OpenChest").gameObject
    self.m_ResultNode = self.transform:Find("Result").gameObject
    self.m_BaseNode:SetActive(true)
    self.m_OpenChestNode:SetActive(false)
    self.m_ResultNode:SetActive(false)

    UIEventListener.Get(self.m_BaseNode).onClick = DelegateFactory.VoidDelegate(function(go)
        if not CLuaDouDiZhu2020ResultWnd.s_IsPlayOver then
            CUIManager.CloseUI(CLuaUIResources.DouDiZhu2020ResultWnd)
        else
            if CLuaDouDiZhu2020ResultWnd.s_AllChestCount>0 then
                self.m_BaseNode:SetActive(false)
                self.m_OpenChestNode:SetActive(true)
            else
                CUIManager.CloseUI(CLuaUIResources.DouDiZhu2020ResultWnd)
            end
        end
    end)
    self.m_Chest = self.transform:Find("OpenChest/Chest").gameObject
    self.m_ChestLabel = self.transform:Find("OpenChest/ChestLabel"):GetComponent(typeof(UILabel))
    self.m_ChestLabel2 = self.transform:Find("Result/ChestLabel"):GetComponent(typeof(UILabel))
    self:SetChestCount()
    UIEventListener.Get(self.m_Chest).onClick = DelegateFactory.VoidDelegate(function(go)
        Gac2Gas.RequestQiangXiangZiOpenOneChest()
    end)

    UIEventListener.Get(self.m_ResultNode).onClick = DelegateFactory.VoidDelegate(function(go)
        if CLuaDouDiZhu2020ResultWnd.s_AllChestCount-self.m_OpenCount<=0 then
            CUIManager.CloseUI(CLuaUIResources.DouDiZhu2020ResultWnd)
        else
            self.m_OpenChestNode:SetActive(true)
            self.m_ResultNode:SetActive(false)
            self:SetChestCount()
        end
    end)

    local gongxijinji = self.transform:Find("Base/gongxijinji").gameObject
    local yihantaotai = self.transform:Find("Base/yihantaotai").gameObject
    if not CLuaDouDiZhu2020ResultWnd.s_IsWin then
        gongxijinji:SetActive(false)
        yihantaotai:SetActive(true)
    else
        gongxijinji:SetActive(true)
        local win1 = gongxijinji.transform:Find("Win1").gameObject
        local win2 = gongxijinji.transform:Find("Win2").gameObject
        if CLuaDouDiZhu2020ResultWnd.s_IsDiZhu then
            win1:SetActive(false)
            win2:SetActive(true)
        else
            win1:SetActive(true)
            win2:SetActive(false)
        end
        yihantaotai:SetActive(false)
    end

    local item = self.transform:Find("Base/Item").gameObject
    item:SetActive(false)
    local grid = self.transform:Find("Base/Grid").gameObject
    Extensions.RemoveAllChildren(grid.transform)
    if CLuaDouDiZhu2020ResultWnd.s_ChestCount>0 then
        for i=1,CLuaDouDiZhu2020ResultWnd.s_ChestCount do
            local go = NGUITools.AddChild(grid,item)
            go:SetActive(true)
        end
        grid:GetComponent(typeof(UIGrid)):Reposition()
    end

end

function CLuaDouDiZhu2020ResultWnd:OnDestroy()
    Gac2Gas.RequestQiangXiangZiOpenAllChest()
end

function CLuaDouDiZhu2020ResultWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncQiangXiangZiOpenChestResult", self, "OnSyncQiangXiangZiOpenChestResult")
    
end

function CLuaDouDiZhu2020ResultWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncQiangXiangZiOpenChestResult", self, "OnSyncQiangXiangZiOpenChestResult")

end

function CLuaDouDiZhu2020ResultWnd:OnSyncQiangXiangZiOpenChestResult(remainChestCount, awards)
    self.m_OpenChestNode:SetActive(false)
    self.m_ResultNode:SetActive(true)

    self.m_OpenCount = CLuaDouDiZhu2020ResultWnd.s_AllChestCount - remainChestCount


    local grid = self.m_ResultNode.transform:Find("Grid").gameObject
    Extensions.RemoveAllChildren(grid.transform)
    local awardItem = self.m_ResultNode.transform:Find("AwardItem").gameObject
    awardItem:SetActive(false)
    for k,v in pairs(awards) do
        local go = NGUITools.AddChild(grid,awardItem)
        go:SetActive(true)
        local countLabel = go.transform:Find("CountLabel"):GetComponent(typeof(UILabel))
        countLabel.text = tostring(v)
        local icon = go.transform:Find("Icon"):GetComponent(typeof(CUITexture))
        local data = Item_Item.GetData(k)
        icon:LoadMaterial(data.Icon)
        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(go)
            CItemInfoMgr.ShowLinkItemTemplateInfo(k,false, nil, AlignType.Default, 0, 0, 0, 0)
        end)
    end
    grid:GetComponent(typeof(UIGrid)):Reposition()
    self:SetChestCount()
end

function CLuaDouDiZhu2020ResultWnd:SetChestCount()
    self.m_ChestLabel.text = SafeStringFormat3("%d/%d",self.m_OpenCount,CLuaDouDiZhu2020ResultWnd.s_AllChestCount)
    self.m_ChestLabel2.text = self.m_ChestLabel.text
end