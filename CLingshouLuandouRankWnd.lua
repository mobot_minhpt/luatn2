-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CLingshouLuandouMgr = import "L10.Game.CLingshouLuandouMgr"
local CLingshouLuandouRankWnd = import "L10.UI.CLingshouLuandouRankWnd"
local CLingshouLuandouScore = import "L10.Game.CLingshouLuandouScore"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CommonSimpleInfoListItem = import "L10.UI.CommonSimpleInfoListItem"
local Constants = import "L10.Game.Constants"
local DelegateFactory = import "DelegateFactory"
local String = import "System.String"
CLingshouLuandouRankWnd.m_Init_CS2LuaHook = function (this) 
    this.AdvView.m_DataSource = this
    this.dataList = CLingshouLuandouMgr.Instance.ScoreList
    CommonDefs.ListSort1(this.dataList, typeof(CLingshouLuandouScore), DelegateFactory.Comparison_CLingshouLuandouScore(function (x, y) 
        return - NumberCompareTo(x.TotalScore, y.TotalScore)
    end))
    this.AdvView:ReloadData(false, false)
    this.ConfirmButton:SetActive(CLingshouLuandouMgr.Instance.isOver)
end
CLingshouLuandouRankWnd.m_ItemAt_CS2LuaHook = function (this, view, row) 

    local item = TypeAs(view:GetFromPool(0), typeof(CommonSimpleInfoListItem))
    local score = this.dataList[row]
    local textList = CreateFromClass(MakeGenericClass(List, String))
    CommonDefs.ListAdd(textList, typeof(String), score.PlayerName)
    CommonDefs.ListAdd(textList, typeof(String), tostring(score.NormalScore))
    CommonDefs.ListAdd(textList, typeof(String), tostring(score.ShenshouScore))
    CommonDefs.ListAdd(textList, typeof(String), tostring(score.NianshouScore))
    CommonDefs.ListAdd(textList, typeof(String), tostring(score.TotalScore))
    item:SetText(textList)
    if score.PlayerID == CClientMainPlayer.Inst.Id then
        item:SetLabelColor(Constants.SelfLabelColor)
    else
        item:SetLabelColor(Color.white)
    end
    return item
end
