local QnPopupList = import "L10.UI.QnPopupList"
local QnButton = import "L10.UI.QnButton"
local GameObject = import "UnityEngine.GameObject"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local Color = import "UnityEngine.Color"
local UIBasicSprite = import "UIBasicSprite"
local UISprite = import "UISprite"
local CTickMgr = import "L10.Engine.CTickMgr"
local ETickType = import "L10.Engine.ETickType"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CJingLingMgr = import "L10.Game.CJingLingMgr"
local EnumGender = import "L10.Game.EnumGender"
local EnumClass = import "L10.Game.EnumClass"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EChatPanel = import "L10.Game.EChatPanel"

LuaShenYaoPassingRecordWnd = class()
LuaShenYaoPassingRecordWnd.s_TeamInfo = {}
LuaShenYaoPassingRecordWnd.s_Level = 1
LuaShenYaoPassingRecordWnd.s_NpcId = nil

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShenYaoPassingRecordWnd, "LevelPopupList", "LevelPopupList", QnPopupList)
RegistChildComponent(LuaShenYaoPassingRecordWnd, "NamePopupList", "NamePopupList", QnPopupList)
RegistChildComponent(LuaShenYaoPassingRecordWnd, "NoPlayer", "NoPlayer", GameObject)
RegistChildComponent(LuaShenYaoPassingRecordWnd, "Player_0", "Player_0", GameObject)
RegistChildComponent(LuaShenYaoPassingRecordWnd, "Player_1", "Player_1", GameObject)
RegistChildComponent(LuaShenYaoPassingRecordWnd, "Player_2", "Player_2", GameObject)
RegistChildComponent(LuaShenYaoPassingRecordWnd, "Player_3", "Player_3", GameObject)
RegistChildComponent(LuaShenYaoPassingRecordWnd, "Player_4", "Player_4", GameObject)
RegistChildComponent(LuaShenYaoPassingRecordWnd, "TeamView", "TeamView", GameObject)
RegistChildComponent(LuaShenYaoPassingRecordWnd, "RaycastCheck", "RaycastCheck", GameObject)
RegistChildComponent(LuaShenYaoPassingRecordWnd, "JieShaoBtn", "JieShaoBtn", QnButton)
RegistChildComponent(LuaShenYaoPassingRecordWnd, "GongLveBtn", "GongLveBtn", QnButton)

--@endregion RegistChildComponent end

RegistClassMember(LuaShenYaoPassingRecordWnd, "m_Inited") -- 初始化NamePopupList时，其ChangeTo不用请求数据
RegistClassMember(LuaShenYaoPassingRecordWnd, "m_ShenYaoNpc")
RegistClassMember(LuaShenYaoPassingRecordWnd, "m_Idx2NpcId")
RegistClassMember(LuaShenYaoPassingRecordWnd, "m_CurLevel")
RegistClassMember(LuaShenYaoPassingRecordWnd, "m_CurNpcId")
RegistClassMember(LuaShenYaoPassingRecordWnd, "m_LvArrow")
RegistClassMember(LuaShenYaoPassingRecordWnd, "m_NeArrow")

function LuaShenYaoPassingRecordWnd:Awake()
    self.m_Inited = false
    self.m_ShenYaoNpc = {}
    self.m_Idx2NpcId = {}
    self.m_CurLevel = LuaShenYaoPassingRecordWnd.s_Level
    self.m_CurNpcId = LuaShenYaoPassingRecordWnd.s_NpcId
    self.m_LvArrow = self.LevelPopupList.transform:Find("Arrow"):GetComponent(typeof(UISprite))
    self.m_NeArrow = self.NamePopupList.transform:Find("Arrow"):GetComponent(typeof(UISprite))

    --@region EventBind: Dont Modify Manually!
    if CommonDefs.IS_VN_CLIENT then
        self.JieShaoBtn.gameObject:SetActive(false)
        self.GongLveBtn.gameObject:SetActive(false)
    end
	
	UIEventListener.Get(self.RaycastCheck.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRaycastCheckClick()
	end)


	
	UIEventListener.Get(self.JieShaoBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnJieShaoBtnClick()
	end)


	
	UIEventListener.Get(self.GongLveBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGongLveBtnClick()
	end)


    --@endregion EventBind end

    self.LevelPopupList.OnValueChanged = DelegateFactory.Action_int_string(function (index, name)
		self:OnLevelValueChanged(index, name)
	end)

    self.NamePopupList.OnValueChanged = DelegateFactory.Action_int_string(function (index, name)
		self:OnNameValueChanged(index, name)
	end)

    UIEventListener.Get(self.LevelPopupList.transform:Find("Button").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self.m_LvArrow.flip = CommonDefs.ConvertIntToEnum(typeof(UIBasicSprite.Flip), 2 - EnumToInt(self.m_LvArrow.flip))
        self.RaycastCheck:SetActive(self.m_LvArrow.flip ~= UIBasicSprite.Flip.Nothing or self.m_NeArrow.flip ~= UIBasicSprite.Flip.Nothing)
    end)

    UIEventListener.Get(self.NamePopupList.transform:Find("Button").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self.m_NeArrow.flip = CommonDefs.ConvertIntToEnum(typeof(UIBasicSprite.Flip), 2 - EnumToInt(self.m_NeArrow.flip))
        self.RaycastCheck:SetActive(self.m_LvArrow.flip ~= UIBasicSprite.Flip.Nothing or self.m_NeArrow.flip ~= UIBasicSprite.Flip.Nothing)
    end)

    local levelList = CreateFromClass(MakeGenericClass(List, cs_string))
    local i = 1
    local data = XinBaiLianDong_ShenYaoNpc.GetData(i)
    while data do
        levelList:Add(data.Title)
        self.m_ShenYaoNpc[i] = {}
        self.m_Idx2NpcId[i] = {}
        local index = 0
        for npcId, playId in string.gmatch(data.RandPool, "(%d+),(%d+);?") do
            npcId, playId = tonumber(npcId), tonumber(playId)
            local npcData = NPC_NPC.GetData(npcId)
            self.m_ShenYaoNpc[i][npcId] = { 
                name = npcData and npcData.Name or "Unknown",
                idx = index,
            }
            self.m_Idx2NpcId[i][index] = npcId
            index = index + 1
		end
        i = i + 1
        data = XinBaiLianDong_ShenYaoNpc.GetData(i)
    end

    self.LevelPopupList:Init(levelList:ToArray())
end

function LuaShenYaoPassingRecordWnd:Init()
    self.LevelPopupList:ChangeTo(self.m_CurLevel - 1)
    self.NamePopupList:ChangeTo(self.m_ShenYaoNpc[self.m_CurLevel][self.m_CurNpcId].idx)
    self:RefreshTeamView()

    self.m_Inited = true
end

function LuaShenYaoPassingRecordWnd:OnEnable()
    g_ScriptEvent:AddListener("ShenYao_QueryTeamPassResult", self, "ShenYao_QueryTeamPassResult")
end

function LuaShenYaoPassingRecordWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ShenYao_QueryTeamPassResult", self, "ShenYao_QueryTeamPassResult")
end

function LuaShenYaoPassingRecordWnd:OnWillClose()
    
end

--@region UIEvent


function LuaShenYaoPassingRecordWnd:OnRaycastCheckClick()
    self.LevelPopupList:OnTableItemClick(self.m_CurLevel - 1)
    self.NamePopupList:OnTableItemClick(self.m_ShenYaoNpc[self.m_CurLevel][self.m_CurNpcId].idx)
end


function LuaShenYaoPassingRecordWnd:OnJieShaoBtnClick()
    CJingLingMgr.Inst:ShowJingLingWnd(XinBaiLianDong_ShenYaoDialog.GetData(self.m_CurNpcId).NpcDesc, "o_push", true, false, nil, false)
end

function LuaShenYaoPassingRecordWnd:OnGongLveBtnClick()
    CJingLingMgr.Inst:ShowJingLingWnd(XinBaiLianDong_ShenYaoDialog.GetData(self.m_CurNpcId).NpcStrategy, "o_push", true, false, nil, false)
end


--@endregion UIEvent

function LuaShenYaoPassingRecordWnd:OnLevelValueChanged(index, name)
    self.m_CurLevel = index + 1
    local nameList = {}
    for npcId, npc in pairs(self.m_ShenYaoNpc[self.m_CurLevel]) do
        nameList[npc.idx + 1] = npc.name
    end
    local curNpcId = self.m_CurNpcId
    self.NamePopupList:Init(Table2Array(nameList, MakeArrayClass(cs_string))) -- 执行完可能CurNpcId就变了
    if self.m_ShenYaoNpc[self.m_CurLevel][curNpcId] then
        self.NamePopupList:OnTableItemClick(self.m_ShenYaoNpc[self.m_CurLevel][curNpcId].idx)
    else
        self.NamePopupList:OnTableItemClick(0)
    end
    self.m_LvArrow.flip = UIBasicSprite.Flip.Nothing
    self.RaycastCheck:SetActive(self.m_NeArrow.flip ~= UIBasicSprite.Flip.Nothing)
end

function LuaShenYaoPassingRecordWnd:OnNameValueChanged(index, name)
    if self.m_Inited then
        self.m_CurNpcId = self.m_Idx2NpcId[self.m_CurLevel][index]
        Gac2Gas.ShenYao_QueryTeamPass(self.m_CurLevel, self.m_CurNpcId)
    end
    self.m_NeArrow.flip = UIBasicSprite.Flip.Nothing
    self.RaycastCheck:SetActive(self.m_LvArrow.flip ~= UIBasicSprite.Flip.Nothing)
end

function LuaShenYaoPassingRecordWnd:ShenYao_QueryTeamPassResult(Level, NpcId, info)
    LuaShenYaoPassingRecordWnd.s_TeamInfo = info
    self:RefreshTeamView()
end

function LuaShenYaoPassingRecordWnd:RefreshTeamView()
    local info = LuaShenYaoPassingRecordWnd.s_TeamInfo
    info = info and #info > 0 and info[math.random(1, #info)]
    if not info or #info == 0 then
        self.NoPlayer:SetActive(true)
        self.TeamView:SetActive(false)
    else
        self.NoPlayer:SetActive(false)
        self.TeamView:SetActive(true)
        for i = 1, #info do -- 第一个一定是leader
            local p = self["Player_"..(i-1)]

            p.transform:Find("InfoPanel/EmptyHint").gameObject:SetActive(false)
            p.transform:Find("InfoPanel/LevelLabel"):GetComponent(typeof(UILabel)).text = info[i].Level
            local nameLabel = p.transform:Find("InfoPanel/NameLabel"):GetComponent(typeof(UILabel))
            nameLabel.text = info[i].Name
            nameLabel.color = Color(nameLabel.color.r, nameLabel.color.g, nameLabel.color.b, 1)

            local portrait = p.transform:Find("AvatarPanel/Border/Portrait")
            portrait.gameObject:SetActive(true)
            local cls = CommonDefs.ConvertIntToEnum(typeof(EnumClass), info[i].Class)
            local gender = CommonDefs.ConvertIntToEnum(typeof(EnumGender), info[i].Gender)
            portrait:GetComponent(typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(cls, gender))

            CommonDefs.AddOnClickListener(
                portrait.gameObject, 
                DelegateFactory.Action_GameObject(function()
                    CPlayerInfoMgr.ShowPlayerPopupMenu(info[i].Id, EnumPlayerInfoContext.ShenYaoPassingRecord, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), CPlayerInfoMgr.AlignType.Default)
                end), 
                false
            )
        end
        for i = #info + 1, 5 do 
            local p = self["Player_"..(i-1)]

            p.transform:Find("InfoPanel/EmptyHint").gameObject:SetActive(true)
            p.transform:Find("InfoPanel/LevelLabel"):GetComponent(typeof(UILabel)).text = ""
            local nameLabel = p.transform:Find("InfoPanel/NameLabel"):GetComponent(typeof(UILabel))
            nameLabel.text = LocalString.GetString("无")
            nameLabel.color = Color(nameLabel.color.r, nameLabel.color.g, nameLabel.color.b, 0.5)
            p.transform:Find("AvatarPanel/Border/Portrait").gameObject:SetActive(false)
        end
    end
end
