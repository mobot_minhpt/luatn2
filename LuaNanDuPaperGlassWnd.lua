local GameObject = import "UnityEngine.GameObject"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CCommonUITween = import "L10.UI.CCommonUITween"

LuaNanDuPaperGlassWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaNanDuPaperGlassWnd, "CloseBtn", "CloseBtn", GameObject)
RegistChildComponent(LuaNanDuPaperGlassWnd, "obj101", "obj101", GameObject)
RegistChildComponent(LuaNanDuPaperGlassWnd, "obj102", "obj102", GameObject)
RegistChildComponent(LuaNanDuPaperGlassWnd, "obj103", "obj103", GameObject)
RegistChildComponent(LuaNanDuPaperGlassWnd, "obj104", "obj104", GameObject)
RegistChildComponent(LuaNanDuPaperGlassWnd, "obj201", "obj201", GameObject)
RegistChildComponent(LuaNanDuPaperGlassWnd, "obj202", "obj202", GameObject)
RegistChildComponent(LuaNanDuPaperGlassWnd, "obj203", "obj203", GameObject)
RegistChildComponent(LuaNanDuPaperGlassWnd, "obj301", "obj301", GameObject)

--@endregion RegistChildComponent end
LuaNanDuPaperGlassWnd.paperId = nil
LuaNanDuPaperGlassWnd.elementType = nil
function LuaNanDuPaperGlassWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self:InitUIEvent()
    self:InitWndData()
    self:RefreshVariableUI(LuaNanDuPaperGlassWnd.paperId, LuaNanDuPaperGlassWnd.elementType)
end

function LuaNanDuPaperGlassWnd:Init()

end

--@region UIEvent

--@endregion UIEvent
function LuaNanDuPaperGlassWnd:InitUIEvent()
    UIEventListener.Get(self.CloseBtn).onClick = DelegateFactory.VoidDelegate(function ()
        for k, v in pairs(self.elementType2Obj) do
            if LuaNanDuPaperGlassWnd.elementType and k == LuaNanDuPaperGlassWnd.elementType or false then
                v.transform:GetComponent(typeof(CCommonUITween)):PlayDisapperAnimation()
                break
            end
        end
        RegisterTickOnce(function()
            CUIManager.CloseUI(CLuaUIResources.NanDuPaperGlassWnd)
            g_ScriptEvent:BroadcastInLua("ClosePaperGlassWnd")
        end,180)
    end)
end

function LuaNanDuPaperGlassWnd:InitWndData()
    self.elementType2Obj = {}
    self.elementType2Obj[101] = self.obj101
    self.elementType2Obj[102] = self.obj102
    self.elementType2Obj[103] = self.obj103
    self.elementType2Obj[104] = self.obj104
    self.elementType2Obj[201] = self.obj201
    self.elementType2Obj[202] = self.obj202
    self.elementType2Obj[203] = self.obj203
    self.elementType2Obj[301] = self.obj301
end

function LuaNanDuPaperGlassWnd:RefreshVariableUI(paperId, elementType)
    for k, v in pairs(self.elementType2Obj) do
        v:SetActive(elementType == k)
    end

    NanDuFanHua_NanDuPaper.Foreach(function(key, data)
        if data.PaperID == paperId and data.ElementType == elementType then
            local obj = self.elementType2Obj[elementType]
            if data.NewsDetail ~= "" then
                
                local grid = obj.transform:Find("GameObject/LabelGrid"):GetComponent(typeof(UIGrid))
                local template = obj.transform:Find("GameObject/LineTemplate").gameObject

                local detailInfoList = g_LuaUtil:StrSplit(data.NewsDetail,";")
                for i = 1, #detailInfoList-1 do
                    local lineObj = NGUITools.AddChild(grid.gameObject, template)
                    lineObj.gameObject:SetActive(true)
                    lineObj.transform:GetComponent(typeof(UILabel)).text = detailInfoList[i]
                    lineObj.transform:Find("SeparateLine").gameObject:SetActive(i ~= 1)
                end

                grid:Reposition()
            end
        end
    end)
end
