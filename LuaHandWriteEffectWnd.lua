local Vector3 = import "UnityEngine.Vector3"
local Time = import "UnityEngine.Time"
local CUIFx=import "L10.UI.CUIFx"
local UIRoot = import "UIRoot"
local CThumbnailChatWnd = import "L10.UI.CThumbnailChatWnd"

CLuaHandWriteEffectWnd = class()
RegistClassMember(CLuaHandWriteEffectWnd,"m_Texture")
RegistClassMember(CLuaHandWriteEffectWnd,"m_TextureBG")
RegistClassMember(CLuaHandWriteEffectWnd,"m_Texture_Left")
RegistClassMember(CLuaHandWriteEffectWnd,"m_Texture_Right")

RegistClassMember(CLuaHandWriteEffectWnd,"m_BG")
RegistClassMember(CLuaHandWriteEffectWnd,"m_Fx")
RegistClassMember(CLuaHandWriteEffectWnd,"m_Write")
RegistClassMember(CLuaHandWriteEffectWnd,"m_Coroutine")
RegistClassMember(CLuaHandWriteEffectWnd,"m_Matertial")
RegistClassMember(CLuaHandWriteEffectWnd,"m_MaterialBG")
RegistClassMember(CLuaHandWriteEffectWnd,"m_SkipDelay")
RegistClassMember(CLuaHandWriteEffectWnd,"m_SkipText")
RegistClassMember(CLuaHandWriteEffectWnd,"m_DelayStartTime")
RegistClassMember(CLuaHandWriteEffectWnd,"m_IsFinished")
RegistClassMember(CLuaHandWriteEffectWnd,"m_StartTime")

RegistClassMember(CLuaHandWriteEffectWnd,"m_Tick")


function CLuaHandWriteEffectWnd:Awake()
    self.m_Texture = self.transform:Find("Anchor/Text"):GetComponent(typeof(UITexture))
    self.m_TextureBG = self.transform:Find("Anchor/TextBG"):GetComponent(typeof(UITexture))
    self.m_Texture_Left = self.transform:Find("Anchor/Text_Left"):GetComponent(typeof(UITexture))
    self.m_Texture_Right = self.transform:Find("Anchor/Text_Right"):GetComponent(typeof(UITexture))

    self.m_BG = self.transform:Find("Anchor/BgTexture"):GetComponent(typeof(UITexture))
    self.m_Fx = self.transform:Find("Anchor/Fx"):GetComponent(typeof(CUIFx))
    self.m_Write = nil

--self.m_Coroutine = ?
    self.m_Matertial = nil
    self.m_MaterialBG = nil
    self.m_SkipDelay = false
    self.m_SkipText = false
    self.m_DelayStartTime = 0
    self.m_IsFinished = false
    self.m_StartTime = 0

    UIEventListener.Get(self.m_BG.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnBgMaskClick(go)
    end)
end

function CLuaHandWriteEffectWnd:OnEnable()
    if UIRoot.EnableIPhoneXAdaptation then
        self.m_BG.leftAnchor.absolute = -math.floor(UIRoot.VirtualIPhoneXWidthMargin)
        self.m_BG.rightAnchor.absolute =  math.floor(UIRoot.VirtualIPhoneXWidthMargin)
        self.m_BG:UpdateAnchors()
	end
end

function CLuaHandWriteEffectWnd:Init( )

    self.m_Texture.material = nil
    self.m_Texture_Left.material = nil
    self.m_Texture_Right.material = nil

    self.m_TextureBG.material = nil

    -- if self.m_Coroutine ~= nil then
        self.m_SkipDelay = false
        self.m_SkipText = false
        self.m_IsFinished = false
    --     CCoroutineMgr.Inst:CancelCoroutine(self.m_Coroutine)
    --     self.m_Coroutine = nil
    -- end
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick=nil
    end

    self.m_Write = ZhuJueJuQing_Write.GetData(CLuaHandWriteEffectMgr.WriteId)

    self.m_Fx:DestroyFx()

    if self.m_Write ~= nil then
        -- self.m_Coroutine = CCoroutineMgr.Inst:StartCoroutine(self:LoadMaterialAsync(self.m_Write))
        self:LoadMaterialAsync(self.m_Write)

        if self.m_Write.NeedBG == 1 then
            self.m_BG.alpha = 0.7
        else
            self.m_BG.alpha = 0.01
        end
        if self.m_Write.UIFx and self.m_Write.UIFx~="" then
            self.m_Fx:LoadFx(self.m_Write.UIFx)
        end

        if System.String.IsNullOrEmpty(self.m_Write.UIFx) and System.String.IsNullOrEmpty(self.m_Write.TextMat) then
            if CLuaHandWriteEffectMgr.OnFinishWrite ~= nil then
                CLuaHandWriteEffectMgr.OnFinishWrite()
            end
            CUIManager.CloseUI(CUIResources.HandWriteEffectWnd)
        end
    end
end

function CLuaHandWriteEffectWnd:LoadMaterialAsync(write)
    self.m_DelayStartTime = Time.realtimeSinceStartup
    self.m_StartTime = Time.realtimeSinceStartup
    self.m_Matertial =nil
    self.m_Tick = RegisterTickOnce(function()
        if write.TextMat and write.TextMat~="" then
            local texture = self:GetAlignTexture(write)
            if texture then
                texture:GetComponent(typeof(CUITexture)):LoadMaterial(write.TextMat,false,DelegateFactory.Action_bool(function(b)
                    if b then
                        self.m_Matertial = texture.material
                        texture.material:SetFloat("_StartTime",Time.timeSinceLevelLoad)
                        texture.material:SetFloat("_SpeedX",0.5)--这里不应该写死
                        texture.width = write.Width
                        texture.height = write.Height
                    end
                    self.m_StartTime = Time.realtimeSinceStartup
                end))

                if write.Xaxis~=0 or write.Yaxis~=0 then
                    texture.transform.localPosition = Vector3(write.Xaxis,write.Yaxis,0)
                end
            end
        end
        if write.TextBGMat and write.TextBGMat~="" then
            if self.m_TextureBG then
                self.m_TextureBG:GetComponent(typeof(CUITexture)):LoadMaterial(write.TextBGMat,false,
                    DelegateFactory.Action_bool(function(b)
                        self.m_MaterialBG = self.m_TextureBG.material
                        self.m_TextureBG.width=write.BGWidth
                        self.m_TextureBG.width=write.BGHeight
                    end))
            end
        end
    end, math.floor(write.DelayTime*1000))

end

function CLuaHandWriteEffectWnd:GetAlignTexture( write) 
    repeat
        local default = write.Align
        if default == 0 then
            return self.m_Texture
        elseif default == 1 then
            return self.m_Texture_Left
        elseif default == 2 then
            return self.m_Texture_Right
        else
            return self.m_Texture
        end
    until 1
end
function CLuaHandWriteEffectWnd:OnBgMaskClick( go) 
    if not self.m_SkipDelay and (Time.realtimeSinceStartup - self.m_DelayStartTime) < self.m_Write.DelayTime then
        self.m_SkipDelay = true
    else
        if self.m_Write.TextMat and self.m_Write.TextMat~="" and self.m_Matertial == nil then
            return
        end

        if self:effectFinished() then
            if CLuaHandWriteEffectMgr.OnFinishWrite ~= nil then
                CLuaHandWriteEffectMgr.OnFinishWrite()
            end
            CUIManager.CloseUI(CUIResources.HandWriteEffectWnd)
        else
            if self.m_Matertial ~= nil then
                self.m_Matertial:SetFloat("_SpeedX", 100)
                self:GetAlignTexture(self.m_Write).material = nil
                self:GetAlignTexture(self.m_Write).material = self.m_Matertial
                self.m_SkipText = true
            end
        end
    end
end
function CLuaHandWriteEffectWnd:effectFinished( )
    if self.m_Matertial ~= nil then
        local needTime = 1 / self.m_Matertial:GetFloat("_SpeedX") * self.m_Matertial:GetFloat("_Row")

        if Time.realtimeSinceStartup - self.m_StartTime < needTime then
            return self.m_IsFinished
        else
            return true
        end
    else
        if self.m_Write.UIFx and self.m_Write.UIFx~="" then
            if Time.realtimeSinceStartup - self.m_StartTime < self.m_Write.FxTime then
                return self.m_IsFinished
            else
                return true
            end
        end
    end
    return false
end

function CLuaHandWriteEffectWnd:Update()
    if self.m_Write.NeedSkip == 0 then
        return
    end

    if self.m_SkipText then
        return
    end 
    if self.m_Matertial then
        local needTime = 1/self.m_Matertial:GetFloat("_SpeedX") * self.m_Matertial:GetFloat("_Row");

        if (Time.realtimeSinceStartup - self.m_StartTime) >= (needTime + self.m_Write.NeedSkip) then
            CUIManager.CloseUI(CUIResources.HandWriteEffectWnd)
        end
    end
end

CLuaHandWriteEffectMgr = {}
CLuaHandWriteEffectMgr.WriteId = 0
CLuaHandWriteEffectMgr.OnFinishWrite = nil
function CLuaHandWriteEffectMgr.ShowHandWriteEffect(writeId,onFinishWrite)
    CLuaHandWriteEffectMgr.WriteId = writeId
    CLuaHandWriteEffectMgr.OnFinishWrite = onFinishWrite
    if CUIManager.IsLoaded(CUIResources.HandWriteEffectWnd) then
        CUIManager.CloseUI(CUIResources.HandWriteEffectWnd)
    end
    CUIManager.ShowUI(CUIResources.HandWriteEffectWnd)
end
