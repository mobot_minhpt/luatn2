local UIProgressBar = import "UIProgressBar"
local CButton = import "L10.UI.CButton"
local CGamePlayMgr = import "L10.Game.CGamePlayMgr"
local CScene = import "L10.Game.CScene"

LuaShiMenRuQinTaskView = class()
RegistChildComponent(LuaShiMenRuQinTaskView, "Fx", "Fx", CUIFx)
RegistChildComponent(LuaShiMenRuQinTaskView, "Progress", "Progress", UIProgressBar)
--脚本被复用，以下变量未必存在
RegistClassMember(LuaShiMenRuQinTaskView, "m_CountDownRoot")
RegistClassMember(LuaShiMenRuQinTaskView, "m_GameplayNameLabel")
RegistClassMember(LuaShiMenRuQinTaskView, "m_RemainTimeLabel")
RegistClassMember(LuaShiMenRuQinTaskView, "m_LeaveBtn")

function LuaShiMenRuQinTaskView:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBtnClick()
	end)
	if self.Fx then
		self.Fx:LoadFx("Fx/UI/Prefab/UI_kuang_blue02.prefab")
		local uisprite = self.gameObject:GetComponent(typeof(UISprite))
		local btnWith = uisprite.width/2
        local btnHeight = uisprite.height/2
        local path={ Vector3(btnWith,-btnHeight,0),Vector3(-btnWith,-btnHeight,0),Vector3(-btnWith,btnHeight,0),Vector3(btnWith,btnHeight,0),Vector3(btnWith,-btnHeight,0)}
        local array = Table2Array(path, MakeArrayClass(Vector3))
        LuaUtils.SetLocalPosition(self.Fx.transform,btnWith,-btnHeight,0)
        local tween = LuaTweenUtils.DODefaultLocalPath(self.Fx.transform,array,2)
        LuaTweenUtils.SetTarget(tween,self.Fx.transform)
	end
	self:OnRefreshShiMenRuQinTaskViewVisibleStatus()

	self.m_CountDownRoot = self.transform:Find("CountDown")
	if self.m_CountDownRoot then
		self.m_GameplayNameLabel = self.m_CountDownRoot:Find("TaskName"):GetComponent(typeof(UILabel))
    	self.m_RemainTimeLabel = self.m_CountDownRoot:Find("TaskInfo"):GetComponent(typeof(UILabel))
		self.m_LeaveBtn = self.m_CountDownRoot:Find("LeaveBtn"):GetComponent(typeof(CButton))
		CommonDefs.AddOnClickListener(self.m_LeaveBtn.gameObject, DelegateFactory.Action_GameObject(function(go) CGamePlayMgr.Inst:LeavePlay() end), false)
	end
    --@endregion EventBind end
end

--@region UIEvent

function LuaShiMenRuQinTaskView:OnBtnClick()
	LuaZongMenMgr:GoToGuardZongMen()
	if self.Fx then
		LuaTweenUtils.DOKill(self.Fx.transform, false)
	end
end

function LuaShiMenRuQinTaskView:OnEnable()
	g_ScriptEvent:AddListener("OnSMSWBossInfoUpdate", self, "OnSMSWBossInfoUpdate")
	g_ScriptEvent:AddListener("RefreshShiMenRuQinTaskViewVisibleStatus", self, "OnRefreshShiMenRuQinTaskViewVisibleStatus")
	Gac2Gas.SMSW_QueryPlayInfo()
	if self.m_CountDownRoot then
		g_ScriptEvent:AddListener("MainPlayerCreated", self, "OnMainPlayCreated")
		g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnRemainTimeUpdate")
		g_ScriptEvent:AddListener("CanLeaveGamePlay", self, "OnCanLeaveGamePlay")
		g_ScriptEvent:AddListener("GamePlayRemainMonsterNumUpdate", self, "OnGamePlayRemainMonsterNumUpdate")
		g_ScriptEvent:AddListener("ShowTimeCountDown", self, "OnShowTimeCountDown")
		self:InitCountDown()
	end
end

function LuaShiMenRuQinTaskView:OnDisable()
	g_ScriptEvent:RemoveListener("OnSMSWBossInfoUpdate", self, "OnSMSWBossInfoUpdate")
	g_ScriptEvent:RemoveListener("RefreshShiMenRuQinTaskViewVisibleStatus", self, "OnRefreshShiMenRuQinTaskViewVisibleStatus")
	if self.m_CountDownRoot then
		g_ScriptEvent:RemoveListener("MainPlayerCreated", self, "OnMainPlayCreated")
		g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnRemainTimeUpdate")
		g_ScriptEvent:RemoveListener("CanLeaveGamePlay", self, "OnCanLeaveGamePlay")
		g_ScriptEvent:RemoveListener("GamePlayRemainMonsterNumUpdate", self, "OnGamePlayRemainMonsterNumUpdate")
		g_ScriptEvent:RemoveListener("ShowTimeCountDown", self, "OnShowTimeCountDown")
	end
end

function LuaShiMenRuQinTaskView:OnSMSWBossInfoUpdate()
	local t = LuaZongMenMgr.m_SMSWQueryPlayInfoResultData
	if t and t.leftHp and t.maxHp and t.maxHp ~= 0 then
		self.Progress.value = t.leftHp / t.maxHp
	end
end

function LuaShiMenRuQinTaskView:OnRefreshShiMenRuQinTaskViewVisibleStatus()
	self:OnSMSWBossInfoUpdate()
	local t = LuaZongMenMgr.m_SMSWQueryPlayInfoResultData
	self.Progress.gameObject:SetActive(t and t.leftHp and t.maxHp and t.maxHp ~= 0 and LuaZongMenMgr.m_SMSW_PlayState ~= EnumSMSWPlayState.Reward and LuaZongMenMgr.m_SMSW_PlayState ~= EnumSMSWPlayState.Destroy
	and LuaZongMenMgr.m_SMSW_PlayState ~= EnumSMSWPlayState.Delay)
end

function LuaShiMenRuQinTaskView:InitCountDown()
	if self.m_CountDownRoot == nil then return end
    local mainScene = CScene.MainScene
    if mainScene then
        self.m_GameplayNameLabel.text = mainScene.SceneName
        self.m_RemainTimeLabel.enabled = mainScene.ShowTimeCountDown or mainScene.ShowMonsterCountDown
        self.m_LeaveBtn.gameObject:SetActive(mainScene.ShowLeavePlayButton)
        self:SetRemainTimeVal()
    else
        self.m_GameplayNameLabel.text = ""
        self.m_RemainTimeLabel.enabled = false
        self.m_LeaveBtn.gameObject:SetActive(false)
    end
end

function LuaShiMenRuQinTaskView:OnMainPlayCreated(args)
    self:InitCountDown()
end

function LuaShiMenRuQinTaskView:OnRemainTimeUpdate(args)
    self:SetRemainTimeVal()
end

function LuaShiMenRuQinTaskView:OnCanLeaveGamePlay(args)
	if self.m_CountDownRoot == nil then return end
    local mainScene = CScene.MainScene
    if mainScene then
        self.m_LeaveBtn.gameObject:SetActive(mainScene.ShowLeavePlayButton)
    end
end

function LuaShiMenRuQinTaskView:OnGamePlayRemainMonsterNumUpdate(args)
	if self.m_CountDownRoot == nil then return end
    local mainScene = CScene.MainScene
    if mainScene and mainScene.ShowMonsterCountDown then
        self:SetRemainTimeVal()
    end
end

function LuaShiMenRuQinTaskView:OnShowTimeCountDown(args)
	if self.m_CountDownRoot == nil then return end
    local mainScene = CScene.MainScene
    if mainScene then
        self.m_RemainTimeLabel.enabled =  mainScene.ShowTimeCountDown or mainScene.ShowMonsterCountDown
    end
end

function LuaShiMenRuQinTaskView:SetRemainTimeVal()
	if self.m_CountDownRoot == nil then return end
    local mainScene = CScene.MainScene
    if mainScene then
        if mainScene.ShowTimeCountDown then
            self.m_RemainTimeLabel.text = self:GetRemainTimeText(mainScene.ShowTime)
        else
            self.m_RemainTimeLabel.text = ""
        end
    else
        self.m_RemainTimeLabel.text = ""
    end
end

function LuaShiMenRuQinTaskView:GetRemainTimeText(totalSeconds)
    if totalSeconds<0 then
        totalSeconds = 0
    end
    if totalSeconds>=3600 then
        return cs_string.Format(LocalString.GetString("[ACF9FF]{0:00}:{1:00}:{2:00}[-]"),  
         math.floor(totalSeconds / 3600),
         math.floor((totalSeconds % 3600) / 60), 
         totalSeconds % 60)
    else
        return cs_string.Format(LocalString.GetString("[ACF9FF]{0:00}:{1:00}[-]"), math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end
--@endregion UIEvent

