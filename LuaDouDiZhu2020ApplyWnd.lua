local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
CLuaDouDiZhu2020ApplyWnd = class()
RegistClassMember(CLuaDouDiZhu2020ApplyWnd,"m_ButtonLabel")
RegistClassMember(CLuaDouDiZhu2020ApplyWnd,"m_RankLabel")
RegistClassMember(CLuaDouDiZhu2020ApplyWnd,"m_HonorLabel")
RegistClassMember(CLuaDouDiZhu2020ApplyWnd,"m_ItemTemplate")
RegistClassMember(CLuaDouDiZhu2020ApplyWnd,"m_Tick")
RegistClassMember(CLuaDouDiZhu2020ApplyWnd,"m_CountdownLabel")
RegistClassMember(CLuaDouDiZhu2020ApplyWnd,"m_MyRank")
RegistClassMember(CLuaDouDiZhu2020ApplyWnd,"m_MyHonor")

function CLuaDouDiZhu2020ApplyWnd:Init()
    Gac2Gas.RequestQiangXiangZiSignUpInfo()
end

function CLuaDouDiZhu2020ApplyWnd:Awake()
    self.m_MyRank = 0
    self.m_MyHonor = 0
    if CClientMainPlayer.Inst then
        local portraitTexture = self.transform:Find("PlayerInfo/Portrait"):GetComponent(typeof(CUITexture))
        portraitTexture:LoadNPCPortrait(CClientMainPlayer.Inst.PortraitName,false)
    end

    self.m_CountdownLabel = self.transform:Find("CountdownLabel"):GetComponent(typeof(UILabel))
    self.m_CountdownLabel.text = "00:00"


    self.m_ItemTemplate = self.transform:Find("AwardItem").gameObject
    self.m_ItemTemplate:SetActive(false)

    local descLabel = self.transform:Find("Desc/DescLabel"):GetComponent(typeof(UILabel))
    descLabel.text = g_MessageMgr:FormatMessage("DouDiZhu2020_Desc")

    local timeLabel = self.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
    timeLabel.text = g_MessageMgr:FormatMessage("DouDiZhu2020_Time")

    self.m_RankLabel = self.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    self.m_RankLabel.text = "0"
    self.m_HonorLabel = self.transform:Find("HonorLabel"):GetComponent(typeof(UILabel))
    self.m_HonorLabel.text = "0"

    local applyButton = self.transform:Find("ApplyButton").gameObject
    self.m_ButtonLabel = applyButton.transform:Find("Label"):GetComponent(typeof(UILabel))
    UIEventListener.Get(applyButton).onClick = DelegateFactory.VoidDelegate(function(go)
        if self.m_ButtonLabel.text == LocalString.GetString("报名") then
            Gac2Gas.RequestSignUpQiangXiangZi()
        else
            Gac2Gas.RequestCancelSignUpQiangXiangZi()
        end
    end)


    local rankButton = self.transform:Find("RankButton").gameObject
    UIEventListener.Get(rankButton).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CLuaUIResources.DouDiZhu2020RankWnd)
    end)


end

function CLuaDouDiZhu2020ApplyWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncQiangXiangZiSignUpInfo", self, "OnSyncQiangXiangZiSignUpInfo")
    g_ScriptEvent:AddListener("SyncQiangXiangZiSignUpResult", self, "OnSyncQiangXiangZiSignUpResult")
    
end
function CLuaDouDiZhu2020ApplyWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncQiangXiangZiSignUpInfo", self, "OnSyncQiangXiangZiSignUpInfo")
    g_ScriptEvent:RemoveListener("SyncQiangXiangZiSignUpResult", self, "OnSyncQiangXiangZiSignUpResult")
end

function CLuaDouDiZhu2020ApplyWnd:OnSyncQiangXiangZiSignUpInfo(isSignUp,awards,myRank,honor,nextRefreshTime)
    self.m_MyRank = myRank
    self.m_MyHonor = honor

    self:OnSyncQiangXiangZiSignUpResult(isSignUp)
    self.m_RankLabel.text = tostring(myRank)
    self.m_HonorLabel.text = tostring(honor)

    local grid = self.transform:Find("Awards")
    
    Extensions.RemoveAllChildren(grid)
    for i=1,#awards,2 do
        local k = awards[i]
        local v = awards[i+1]
        -- end
    -- for k,v in pairs(awards) do
        local go = NGUITools.AddChild(grid.gameObject,self.m_ItemTemplate)
        go:SetActive(true)
        local countLabel = go.transform:Find("CountLabel"):GetComponent(typeof(UILabel))
        countLabel.text = tostring(v)
        local icon = go.transform:Find("Icon"):GetComponent(typeof(CUITexture))
        local data = Item_Item.GetData(k)
        icon:LoadMaterial(data.Icon)
        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(go)
            CItemInfoMgr.ShowLinkItemTemplateInfo(k,false, nil, AlignType.Default, 0, 0, 0, 0)
        end)
    end
    grid:GetComponent(typeof(UIGrid)):Reposition()

    if  self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
    if nextRefreshTime>0 then
        local now = CServerTimeMgr.Inst.timeStamp
        if nextRefreshTime>now then
            self.m_Tick = RegisterTick(function()
                local leftSecond = nextRefreshTime-CServerTimeMgr.Inst.timeStamp
                if leftSecond>=0 then
                    local hour = math.floor(leftSecond/60/60)
                    local minute = math.floor(leftSecond%3600/60)
                    local second = math.floor(leftSecond%60)
                    self.m_CountdownLabel.text = SafeStringFormat3("%02d:%02d:%02d",hour,minute,second)
                else
                    UnRegisterTick(self.m_Tick)
                    self.m_Tick = nil
                    self.m_CountdownLabel.text = "00:00"
                    Gac2Gas.RequestQiangXiangZiSignUpInfo()
                end
            end,1000)
        end
    end
    

end
function CLuaDouDiZhu2020ApplyWnd:OnSyncQiangXiangZiSignUpResult(isSignUp)
    -- print("OnSyncQiangXiangZiSignUpResult",isSignUp)
    self.m_ButtonLabel.text = isSignUp and LocalString.GetString("取消报名") or LocalString.GetString("报名")
end

function CLuaDouDiZhu2020ApplyWnd:OnDestroy()
    if  self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end

end