LuaGlobalMatchMgr = {}
LuaGlobalMatchMgr.playData = {}

function LuaGlobalMatchMgr:UpdatePlayStatus(playId, bIsPlayOpen, bIsPlayOpenMatch, ExtraInfo_U)
    LuaGlobalMatchMgr.playData[playId] = {isPlayOpen = bIsPlayOpen, isPlayOpenMatch = bIsPlayOpenMatch,
                                            extraInfo_U = ExtraInfo_U}
end