require ("auto_transform/defines")
require ("auto_transform/Assets/Scripts/UI/Main/Package/CPackageView")
-- require ("auto_transform/Assets/Scripts/UI/Main/CItemInfoWnd")
-- require ("auto_transform/Assets/Scripts/UI/Main/ItemAndEquipTipWnd/CEquipInfoWnd")
require ("auto_transform/Assets/Scripts/UI/PersonalSpace/CPersonalSpaceWnd")
require ("auto_transform/Assets/Scripts/UI/Main/Base/SkillButtonBoard/CSkillButtonBoardWnd")
require ("auto_transform/Assets/Scripts/UI/Main/Chat/CChatInputWnd")
-- require ("auto_transform/Assets/Scripts/UI/Main/ItemAndEquipTipWnd/CEquipTip")
require ("auto_transform/Assets/Scripts/UI/House/CConstructHouseWnd")
require ("auto_transform/Assets/Scripts/UI/House/CXiangfangPrivalegeManageWnd")
require ("auto_transform/Assets/Scripts/UI/Main/Package/CQianKunDaiInputView")
require ("auto_transform/Assets/Scripts/UI/CMiniMapPopWnd")
require ("auto_transform/Assets/Scripts/UI/Main/Package/CQianKunDaiCompoundView")
require ("auto_transform/Assets/Scripts/UI/House/CWeixiuItemList")
require ("auto_transform/Assets/Scripts/UI/DataMgr/CPlayerInfoMgr")
require ("auto_transform/Assets/Scripts/UI/PersonalSpace/CPersonalSpaceDetailMoment")
require ("auto_transform/Assets/Scripts/UI/Main/Friend/CFriendContactListView")
require ("auto_transform/Assets/Scripts/UI/Auction/CAuctionWnd")
require ("auto_transform/Assets/Scripts/UI/Main/Chat/CChatWnd")
require ("auto_transform/Assets/Scripts/UI/Main/Chat/CMiniChatMgr")
require ("auto_transform/Assets/Scripts/UI/DataMgr/MessageWndManager")
-- require ("auto_transform/Assets/Scripts/UI/Guild/CGuildMainWndMemberWnd")
-- require ("auto_transform/Assets/Scripts/UI/Welfare/CWelfareWnd")
require ("auto_transform/Assets/Scripts/UI/PersonalSpace/CPersonalSpaceLeaveMessageView")
require ("auto_transform/Assets/Scripts/UI/Main/SkillWnd/CSkillUpgradeBottomView")
require ("auto_transform/Assets/Scripts/UI/PersonalSpace/CPersonalSpaceGMWnd")
require ("auto_transform/Assets/Scripts/UI/Main/Base/CRightMenuWnd")
require ("auto_transform/Assets/Scripts/UI/House/CHouseInfoWnd")
require ("auto_transform/Assets/Scripts/UI/Main/Friend/CFriendChatView")
require ("auto_transform/Assets/Scripts/UI/Main/ZuoQi/CMapiFanyuWnd")
require ("auto_transform/Assets/Scripts/UI/Main/ZuoQi/CMapiDetailInfoWnd")
require ("auto_transform/Assets/Scripts/UI/Main/ZuoQi/CMapiEquipItemCell")
require ("auto_transform/Assets/Scripts/UI/Main/ZuoQi/CMapiLingfuItemCell")
require ("auto_transform/Assets/Scripts/UI/Main/ZuoQi/CMapiLookUpWnd")
require ("auto_transform/Assets/Scripts/UI/Main/ZuoQi/CMapiModelTextureLoader")
require ("auto_transform/Assets/Scripts/UI/Main/ZuoQi/CMapiTianshuWnd")
require ("auto_transform/Assets/Scripts/UI/Main/ZuoQi/CMapiView")
require ("auto_transform/Assets/Scripts/UI/Main/ZuoQi/CMapiViewSimpleItem")
require ("auto_transform/Assets/Scripts/UI/Auction/CAuctionBidWnd")
require ("auto_transform/Assets/Scripts/UI/Auction/CAuctionItem")
require ("auto_transform/Assets/Scripts/UI/Auction/CAuctionOnShelfWnd")
require ("auto_transform/Assets/Scripts/UI/Auction/CSelfAuctionShopWnd")
require ("auto_transform/Assets/Scripts/UI/AutoDrug/CAutoDrugMidWnd")
require ("auto_transform/Assets/Scripts/UI/AutoDrug/CAutoDrugWnd")
require ("auto_transform/Assets/Scripts/UI/AutoDrug/CAutoDrugSlider")
require ("auto_transform/Assets/Scripts/UI/AutoDrug/CDrugItemList")
require ("auto_transform/Assets/Scripts/UI/AutoDrug/CDrugItemTemplate")
require ("auto_transform/Assets/Scripts/UI/BiWuDaHui/CBiWuDaHuiMgr")
require ("auto_transform/Assets/Scripts/UI/BiWuDaHui/CBWDHResultItem")
require ("auto_transform/Assets/Scripts/UI/BiWuDaHui/CBWDHSelectPlayerItem")
require ("auto_transform/Assets/Scripts/UI/BiWuDaHui/CBWDHResultWnd")
require ("auto_transform/Assets/Scripts/UI/BiWuDaHui/CBWDHScoreItem")
-- require ("auto_transform/Assets/Scripts/UI/BiWuDaHui/CBWDHMatchWnd")
-- require ("auto_transform/Assets/Scripts/UI/BiWuDaHui/CBWDHMatchItem")
require ("auto_transform/Assets/Scripts/UI/BiWuDaHui/CBWDHSelectPlayerWnd")
require ("auto_transform/Assets/Scripts/UI/BiWuDaHui/CBWDHScoreWnd")
-- require ("auto_transform/Assets/Scripts/UI/BiWuDaHui/CDanmuWnd")
require ("auto_transform/Assets/Scripts/UI/BiWuZhaoQing/CBWZQAttendChooseWnd")
require ("auto_transform/Assets/Scripts/UI/BiWuZhaoQing/CBWZQAttendSureWnd")
require ("auto_transform/Assets/Scripts/UI/BiWuZhaoQing/CBWZQWatchWnd")
require ("auto_transform/Assets/Scripts/UI/BiWuDaHui/CBWDHWatchWnd")
require ("auto_transform/Assets/Scripts/UI/Main/SkillWnd/COtherSkillDescItem")
require ("auto_transform/Assets/Scripts/UI/Main/SkillWnd/CPlayerSkillInfoWnd")
require ("auto_transform/Assets/Scripts/UI/Main/SkillWnd/CCommonProgressWnd")
require ("auto_transform/Assets/Scripts/UI/Main/SkillWnd/CSkillAcquisitionWnd")
require ("auto_transform/Assets/Scripts/UI/Main/SkillWnd/COtherSkillDetailView")
-- require ("auto_transform/Assets/Scripts/UI/Main/SkillWnd/CSkillInfoWnd")
require ("auto_transform/Assets/Scripts/UI/Main/SkillWnd/CSkillGroupButton")
require ("auto_transform/Assets/Scripts/UI/Main/SkillWnd/CSkillItemCell")
require ("auto_transform/Assets/Scripts/UI/Main/SkillWnd/CSkillTemplateUnlockDescItem")
require ("auto_transform/Assets/Scripts/UI/Main/SkillWnd/CSkillUpgradeDescItem")
-- require ("auto_transform/Assets/Scripts/UI/Main/SkillWnd/CSkillTip")

require ("auto_transform/Assets/Scripts/UI/Main/SkillWnd/CSkillUpgradeTopView")
require ("auto_transform/Assets/Scripts/UI/Main/SkillWnd/CTianFuTabItem")
require ("auto_transform/Assets/Scripts/UI/Main/SkillWnd/CTianFuSkillItemCell")
require ("auto_transform/Assets/Scripts/UI/Main/SkillWnd/CTianFuListItem")
-- require ("auto_transform/Assets/Scripts/UI/Main/SkillWnd/COtherSkillMasterView")
require ("auto_transform/Assets/Scripts/UI/Main/LivingSkill/CLivingMakeConsumeItem")
require ("auto_transform/Assets/Scripts/UI/Main/LivingSkill/CLivingSkillBaseDisplay")
require ("auto_transform/Assets/Scripts/UI/Main/LivingSkill/CLivingItemDisplay")
require ("auto_transform/Assets/Scripts/UI/Main/LivingSkill/CLivingSkillCountInputWnd")
require ("auto_transform/Assets/Scripts/UI/Main/LivingSkill/CLivingSkillEffectDisplay")
require ("auto_transform/Assets/Scripts/UI/Main/LivingSkill/CLivingSkillItemSection")
require ("auto_transform/Assets/Scripts/UI/Main/LivingSkill/CLivingSkillItem")
require ("auto_transform/Assets/Scripts/UI/Main/LivingSkill/CLivingSkillUpgradeCost")
require ("auto_transform/Assets/Scripts/UI/Main/LivingSkill/CLivingSkillItemTable")
require ("auto_transform/Assets/Scripts/UI/Main/LivingSkill/CLivingSkillQuickMakeMgr")
require ("auto_transform/Assets/Scripts/UI/Main/LivingSkill/CLivingSurpriseItem")
require ("auto_transform/Assets/Scripts/UI/Main/LivingSkill/CLivingSkillUseWnd")
require ("auto_transform/Assets/Scripts/UI/Main/LivingSkill/CLivingItem")
require ("auto_transform/Assets/Scripts/UI/Main/Friend/CCommonPlayerListItem")
require ("auto_transform/Assets/Scripts/UI/Main/Friend/CFriendGroupMenu")
require ("auto_transform/Assets/Scripts/UI/Main/Friend/CFriendGroupMsgSettingWnd")
require ("auto_transform/Assets/Scripts/UI/Main/Friend/CFriendRequestListView")
require ("auto_transform/Assets/Scripts/UI/Main/Friend/CFriendRecentListView")
require ("auto_transform/Assets/Scripts/UI/Main/Friend/CFriendWnd")
require ("auto_transform/Assets/Scripts/UI/Main/Friend/CFriendView")
require ("auto_transform/Assets/Scripts/UI/Main/Friend/CFriendSearchListItem")
require ("auto_transform/Assets/Scripts/UI/Main/Friend/CLeavingStatusCtrl")
require ("auto_transform/Assets/Scripts/UI/Main/Friend/CLeavingStatusSettingWnd")
require ("auto_transform/Assets/Scripts/UI/Main/FriendWnd/CFriendGroupModifyListItem")
require ("auto_transform/Assets/Scripts/UI/Main/FriendWnd/CCommonListButtonItem")
require ("auto_transform/Assets/Scripts/UI/Main/FriendWnd/CFriendListItem")
require ("auto_transform/Assets/Scripts/UI/Main/FriendWnd/CFriendGroupModifyWnd")
require ("auto_transform/Assets/Scripts/UI/Main/FriendWnd/CFriendPopupMenuItem")
require ("auto_transform/Assets/Scripts/UI/Main/Friend/CFriendSearchView")
-- require ("auto_transform/Assets/Scripts/UI/Guild/CGuildBenifitItem")
require ("auto_transform/Assets/Scripts/UI/Guild/CGuildChangeMottoWnd")
require ("auto_transform/Assets/Scripts/UI/Guild/CCreateGuildWnd")
-- require ("auto_transform/Assets/Scripts/UI/Guild/CGuildActivityItem")
-- require ("auto_transform/Assets/Scripts/UI/Guild/CGuildHistoryWnd")
require ("auto_transform/Assets/Scripts/UI/Guild/CGuildCreationItem")
-- require ("auto_transform/Assets/Scripts/UI/Guild/CGuildItem")
-- require ("auto_transform/Assets/Scripts/UI/Guild/CGuildMainWnd")
-- require ("auto_transform/Assets/Scripts/UI/Guild/CGuildMainWndActivityWnd")
-- require ("auto_transform/Assets/Scripts/UI/Guild/CGuildMainWndBenifitWnd")
-- require ("auto_transform/Assets/Scripts/UI/Guild/CGuildJobChangeWnd")
-- require ("auto_transform/Assets/Scripts/UI/Guild/CGuildMainWndTabContent_Info")
require ("auto_transform/Assets/Scripts/UI/Guild/CGuildRankWnd")
-- require ("auto_transform/Assets/Scripts/UI/Guild/CGuildRenameWnd")
require ("auto_transform/Assets/Scripts/UI/Guild/CGuildSortButton")
require ("auto_transform/Assets/Scripts/UI/Guild/CGuildWelfareWnd")
-- require ("auto_transform/Assets/Scripts/UI/Guild/CGuildWnd")
-- require ("auto_transform/Assets/Scripts/UI/Guild/CTabelTemplate_GuildMember")
-- require ("auto_transform/Assets/Scripts/UI/Guild/CTabelTemplate_GuildMemberApply")
-- require ("auto_transform/Assets/Scripts/UI/Guild/CJoinGuildWnd")
require ("auto_transform/Assets/Scripts/UI/Guild/CReplyGuildWnd")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildApplyMessage/CGuildApplyMessageWnd")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildChallenge/CGCSituationGuildDetailView")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildChallenge/CGCGuildTemplate")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildChallenge/CGCGuildListView")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildChallenge/CGCSituationPlayerTemplate")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildChallenge/CGuildChallengeWnd")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildChallenge/CGuildChallengeOccupy")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildChallenge/CGuildChallengeTimeSelectWnd")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildChallenge/CGuildChallengeOccupyMapInfo")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildChallenge/CGuildChallengeSituationWnd")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildDevelop/CGuildDevelopDetailView_Academy")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildDevelop/CGuildDevelopDetailView_Main")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildDevelop/CGuildDevelopWnd")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildDevelop/CGuildDevelopDetailView_House")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildDevelop/CGuildDevelopDetailView_Bank")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildEnemy/CGuildSetEnemyConfirmWnd")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildEnemy/CGuildEnemyWnd")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildEnemy/CGuildEnemyListItem")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildEnemy/CGuildEnemyListView")
-- require ("auto_transform/Assets/Scripts/UI/Guild/GuildHistroyWnd/CGuildHistoryItem")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildMemberExport/CGuildMemberChartWnd")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildMemberExport/CBarChartXAixsItem")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildMemberExport/CPieChartView")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildMemberExport/CBarChartView")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildMemberExport/CGuildMemberExportWnd")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildMemberExport/CGuildMemberDataPickerWnd")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildMemberWnd/CGuildInvitationWnd")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildMemberWnd/CGuildInvitationItem")
-- require ("auto_transform/Assets/Scripts/UI/Guild/GuildPower/CGuildPowerSettingTableViewCell")
-- require ("auto_transform/Assets/Scripts/UI/Guild/GuildPower/CGuildPowerSettingWnd")
-- require ("auto_transform/Assets/Scripts/UI/Guild/GuildPower/CGuildPowerSettingItem")
-- require ("auto_transform/Assets/Scripts/UI/Guild/GuildPower/CGuildPowerSettingMasterView")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildTrainWnd/CGuildTrainCategoryList")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildTrainWnd/CGuildTrainTopTabs")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildTrainWnd/CGuildTrainSettingItemList")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildTrainWnd/CGuildTrainCategoryTemplate")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildTrainWnd/CGuildTrainSettingWnd")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildTrainWnd/CGuildTrainItemCell")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildTrainWnd/CGuildTrainWindows")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildTrainWnd/CGuildTrainWnd")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildTrainWnd/CGuildTrainDetailView")
require ("auto_transform/Assets/Scripts/UI/GuildLeague/CComboWnd")
require ("auto_transform/Assets/Scripts/UI/GuildLeague/CGuildLeagueBattleReportWnd")
-- require ("auto_transform/Assets/Scripts/UI/GuildLeague/CGuildLeagueBattleResultWnd")
require ("auto_transform/Assets/Scripts/UI/GuildLeague/CShenShouDieEffectWnd")
-- require ("auto_transform/Assets/Scripts/UI/GuildLeague/CGuildLeagueResultLoopUpItem")
require ("auto_transform/Assets/Scripts/UI/GuildLeague/CGuildLeagueBattleReportItem")
require ("auto_transform/Assets/Scripts/UI/GuildLeague/CGuildLeagueGuildInfoItem")
-- require ("auto_transform/Assets/Scripts/UI/GuildLeague/CGuildLeagueResultLookUpWnd")
require ("auto_transform/Assets/Scripts/UI/GuildLeague/CGuildLeagueInfoLookUpWnd")
require ("auto_transform/Assets/Scripts/UI/GuildLeague/CGuildLeagueBattleResultItem")
require ("auto_transform/Assets/Scripts/UI/GuildLeague/CGuildLeagueBattleResultInfoPane")
require ("auto_transform/Assets/Scripts/UI/GuildLeague/CGuildLeagueGroupItem")
require ("auto_transform/Assets/Scripts/UI/GuildLeague/CGuildLeagueBattleStateWnd")
require ("auto_transform/Assets/Scripts/UI/GuildLeague/GuildLeagueConvene/CGuildLeagueConveneWnd")
require ("auto_transform/Assets/Scripts/UI/GuildLeague/GuildLeagueConvene/CGuildLeagueConveneItem")
require ("auto_transform/Assets/Scripts/UI/GuildLeague/GuildLeagueConvene/CGuildLeagueConveneSortMgr")
require ("auto_transform/Assets/Scripts/UI/GuildLeague/GuildPreLeague/CGetGuildPreLeagueTaskPointWnd")
require ("auto_transform/Assets/Scripts/UI/GuildLeague/GuildPreLeague/CGuildPreLeagueInfoItem")
require ("auto_transform/Assets/Scripts/UI/GuildLeague/GuildPreLeague/CGuildPreLeagueInfoWnd")
require ("auto_transform/Assets/Scripts/UI/Auction/CAuctionHistoryItem")
require ("auto_transform/Assets/Scripts/UI/Auction/CAuctionShelfDetailWnd")
require ("auto_transform/Assets/Scripts/UI/Auction/CAuditLingyuDetailWnd")
require ("auto_transform/Assets/Scripts/UI/Auction/CAuditLingyuItem")
require ("auto_transform/Assets/Scripts/UI/Auction/CSelfAuctionShopItem")
require ("auto_transform/Assets/Scripts/UI/Auction/CAuditLingyuWnd")
require ("auto_transform/Assets/Scripts/UI/DataMgr/CPopupMenuInfoMgr")
-- require ("auto_transform/Assets/Scripts/UI/DataMgr/CNPCShopInfoMgr")
require ("auto_transform/Assets/Scripts/UI/DataMgr/CSkillInfoMgr")
require ("auto_transform/Assets/Scripts/UI/DataMgr/CShopMallMgr")
require ("auto_transform/Assets/Scripts/UI/DataMgr/CItemInfoMgr")
require ("auto_transform/Assets/Scripts/UI/ZuoQi/CHuanLingDrawWnd")
require ("auto_transform/Assets/Scripts/UI/ZuoQi/CHuanLingConsumption")
require ("auto_transform/Assets/Scripts/UI/ZuoQi/CHuanLingShareWnd")
require ("auto_transform/Assets/Scripts/UI/ZuoQi/CMapiPreviewWnd")
require ("auto_transform/Assets/Scripts/UI/ZuoQi/CHuanLingResult")
require ("auto_transform/Assets/Scripts/UI/ZuoQi/CHuanLingColor")
require ("auto_transform/Assets/Scripts/UI/ZuoQi/CHuanLingLineDrawer")
require ("auto_transform/Assets/Scripts/UI/ZuoQi/CHuanLingWnd")
require ("auto_transform/Assets/Scripts/UI/Main/Base/CMiniMap")
require ("auto_transform/Assets/Scripts/UI/Main/TaskWnd/CTaskSubmitItem")
require ("auto_transform/Assets/Scripts/UI/Main/TaskWnd/CTaskItemSubmitWnd")
require ("auto_transform/Assets/Scripts/UI/Main/TaskWnd/CTaskMasterView")
require ("auto_transform/Assets/Scripts/UI/QianKunDai/CQianKunDaiCostWnd")
require ("auto_transform/Assets/Scripts/UI/QianKunDai/CQianKunDaiRecipeWnd")
require ("auto_transform/Assets/Scripts/UI/ProfessionTransfer/CEquipmentHoleTransferOptionsItem")
require ("auto_transform/Assets/Scripts/UI/ProfessionTransfer/CCommonItemSelectCell")
require ("auto_transform/Assets/Scripts/UI/ProfessionTransfer/CEquipmentHoleTransferOptionsWnd")
require ("auto_transform/Assets/Scripts/UI/ProfessionTransfer/CCommonItemSelectWnd")
require ("auto_transform/Assets/Scripts/UI/ProfessionTransfer/CEquipmentHoleTransferWnd")
require ("auto_transform/Assets/Scripts/UI/ProfessionTransfer/CHoleTransferItem")
require ("auto_transform/Assets/Scripts/UI/ProfessionTransfer/CProfessionOptionsWnd")
require ("auto_transform/Assets/Scripts/UI/ProfessionTransfer/CTalismanTransferOptionsItem")
require ("auto_transform/Assets/Scripts/UI/ProfessionTransfer/CTalismanTransferItemCell")
require ("auto_transform/Assets/Scripts/UI/ProfessionTransfer/CTalismanTransferWnd")
require ("auto_transform/Assets/Scripts/UI/ProfessionTransfer/CTalismanTransferOptionsWnd")
require ("auto_transform/Assets/Scripts/UI/Main/ZuoQi/CLingfuBaptizeWnd")
require ("auto_transform/Assets/Scripts/UI/Main/ZuoQi/CLingfuQianghuaWnd")
require ("auto_transform/Assets/Scripts/UI/Main/ZuoQi/CInviteFanyuFriendItem")
require ("auto_transform/Assets/Scripts/UI/Main/ZuoQi/CInviteFanyuWnd")
require ("auto_transform/Assets/Scripts/UI/Main/ZuoQi/CLingfuView")
require ("auto_transform/Assets/Scripts/UI/Main/ZuoQi/CSelectAddMapiWnd")
require ("auto_transform/Assets/Scripts/UI/Main/ZuoQi/CSelectMapiWnd")
require ("auto_transform/Assets/Scripts/UI/Main/ZuoQi/CVehicleList")
require ("auto_transform/Assets/Scripts/UI/Main/ZuoQi/CSelectMapiItem")
require ("auto_transform/Assets/Scripts/UI/Main/ZuoQi/CVehicleMainWnd")
require ("auto_transform/Assets/Scripts/UI/Main/ZuoQi/CZuoQiView")
require ("auto_transform/Assets/Scripts/UI/Main/ZuoQi/CYumaView")
require ("auto_transform/Assets/Scripts/UI/Main/ZuoQi/CVehicleWnd")
require ("auto_transform/Assets/Scripts/UI/Main/ZuoQi/CVehicleTemplate")
-- require ("auto_transform/Assets/Scripts/UI/QingShu/CQingShuLookupItem")
-- require ("auto_transform/Assets/Scripts/UI/QingShu/CQingShuMgr")
-- require ("auto_transform/Assets/Scripts/UI/QingShu/CQingShuBaseItem")
-- require ("auto_transform/Assets/Scripts/UI/QingShu/CQingShuLookupWnd")
-- require ("auto_transform/Assets/Scripts/UI/QingShu/CQingShuRankWnd")
-- require ("auto_transform/Assets/Scripts/UI/QingShu/CWriteQingShuWnd")
require ("auto_transform/Assets/Scripts/UI/QingShu/CSearchPlayerWnd")
-- require ("auto_transform/Assets/Scripts/UI/QingShu/CQingShuRankItem")
require ("auto_transform/Assets/Scripts/UI/GongXun/CGongXunExchangeWnd")
require ("auto_transform/Assets/Scripts/UI/GongXun/CGongXunWnd")
require ("auto_transform/Assets/Scripts/UI/PVP/CMPTZRankGroupView")
require ("auto_transform/Assets/Scripts/UI/PVP/CMPTZRankDetailView")
require ("auto_transform/Assets/Scripts/UI/PVP/CMPTZRankMasterView")
require ("auto_transform/Assets/Scripts/UI/PVP/CMPTZRankListItem")
require ("auto_transform/Assets/Scripts/UI/PVP/CMPTZSettlementWnd")
require ("auto_transform/Assets/Scripts/UI/PVP/CPVPPromotionAwardItem")
require ("auto_transform/Assets/Scripts/UI/PVP/CPVPRecordListItem")
require ("auto_transform/Assets/Scripts/UI/PVP/CPVPPromotionWnd")
require ("auto_transform/Assets/Scripts/UI/PVP/CPVPSkillPreferenceTopView")
require ("auto_transform/Assets/Scripts/UI/PVP/CPVPRecordWnd")
require ("auto_transform/Assets/Scripts/UI/PVP/CPVPSkillPreferenceBottomView")
require ("auto_transform/Assets/Scripts/UI/JingLing/CContactAssistantAnswerWnd")
require ("auto_transform/Assets/Scripts/UI/JingLing/CContactAssistantChatView")
require ("auto_transform/Assets/Scripts/UI/JingLing/CAssistantEvaluateWnd")
require ("auto_transform/Assets/Scripts/UI/JingLing/CContactAssistantPunishExplainView")
require ("auto_transform/Assets/Scripts/UI/JingLing/CContactAssistantGameSetView")
require ("auto_transform/Assets/Scripts/UI/JingLing/CContactAssistantCheckWnd")
require ("auto_transform/Assets/Scripts/UI/JingLing/CJingLingAssistView")
require ("auto_transform/Assets/Scripts/UI/JingLing/CContactAssistantWnd")
require ("auto_transform/Assets/Scripts/UI/JingLing/CJingLingChatView")
require ("auto_transform/Assets/Scripts/UI/JingLing/CJingLingHistoryMenu")
require ("auto_transform/Assets/Scripts/UI/JingLing/CQuickAskImageItem")
require ("auto_transform/Assets/Scripts/UI/Main/Team/CTeamDamageStatItem")
require ("auto_transform/Assets/Scripts/UI/Main/Chat/CGuildCCMemberItem")
require ("auto_transform/Assets/Scripts/UI/Main/Team/CTeamDamageStatWnd")
require ("auto_transform/Assets/Scripts/UI/Main/Team/CTeamApplyTabContent")
require ("auto_transform/Assets/Scripts/UI/Main/Team/CTeamFightDataItem")
require ("auto_transform/Assets/Scripts/UI/Main/Team/CTeamInfoTabContent")
require ("auto_transform/Assets/Scripts/UI/Main/Team/CTeamInviteDlg")
require ("auto_transform/Assets/Scripts/UI/Main/Team/CTeamFightDataWnd")
require ("auto_transform/Assets/Scripts/UI/Main/Team/CTeamMatchActivityItem")
require ("auto_transform/Assets/Scripts/UI/Main/Team/CTeamMatchButton")
require ("auto_transform/Assets/Scripts/UI/Main/Team/CTeamMatchActivityTableView")
require ("auto_transform/Assets/Scripts/UI/Main/Team/CTeamMatchWnd")
require ("auto_transform/Assets/Scripts/UI/Main/Team/CTeamMatchLevelPicker")
require ("auto_transform/Assets/Scripts/UI/Main/Team/CTeamQuickJoinActivityItem")
require ("auto_transform/Assets/Scripts/UI/Main/Team/CTeamMemberItem")
require ("auto_transform/Assets/Scripts/UI/Main/Team/CTeamQuickJoinMasterView")
require ("auto_transform/Assets/Scripts/UI/Main/Team/CTeamQuickJoinDetailItem")
require ("auto_transform/Assets/Scripts/UI/Main/Team/CTeamQuickJoinWnd")
require ("auto_transform/Assets/Scripts/UI/Main/Team/CTeamQuickJoinDetailView")
require ("auto_transform/Assets/Scripts/UI/Main/Team/CTeamWnd")
require ("auto_transform/Assets/Scripts/UI/ZhuJueJuQing/CHandDrawWnd")
require ("auto_transform/Assets/Scripts/UI/ZhuJueJuQing/CItemChosenWnd")
require ("auto_transform/Assets/Scripts/UI/ZhuJueJuQing/CDangongBulletAndBird")
require ("auto_transform/Assets/Scripts/UI/ZhuJueJuQing/CFadeEffect")
require ("auto_transform/Assets/Scripts/UI/ZhuJueJuQing/CZhuJueJuQingLetterWnd")
require ("auto_transform/Assets/Scripts/UI/ZhuJueJuQing/CDanGongWnd")
require ("auto_transform/Assets/Scripts/UI/ZhuJueJuQing/CZhuJueJuQingMgr")
require ("auto_transform/Assets/Scripts/UI/ZhongQiu/CZhongQiuPrayWnd")
require ("auto_transform/Assets/Scripts/UI/ZhongQiu/CZhongQiuSelectFriendItem")
require ("auto_transform/Assets/Scripts/UI/ZhongQiu/CZhongQiuSelectFriendWnd")
require ("auto_transform/Assets/Scripts/UI/ZhongQiu/CYBZDResultItem")
require ("auto_transform/Assets/Scripts/UI/ZhongQiu/CYBZDResultWnd")
require ("auto_transform/Assets/Scripts/UI/ZhanLong/CZhanLongCheckWnd")
require ("auto_transform/Assets/Scripts/UI/ZhanLong/CZhanLongItemWnd")
require ("auto_transform/Assets/Scripts/UI/YuanDan/CYuanDanConcertRankItem")
require ("auto_transform/Assets/Scripts/UI/YuanDan/CYuanDanConcertRankWnd")
require ("auto_transform/Assets/Scripts/UI/YuanDan/CYuanDanConcertWnd")
require ("auto_transform/Assets/Scripts/UI/YuanDan/CYuanDanLotteryResultWnd")
require ("auto_transform/Assets/Scripts/UI/YuanDan/CYuanDanQiFuProgressWnd")
require ("auto_transform/Assets/Scripts/UI/YuanDan/CYuanDanQiangDanProgressWnd")
require ("auto_transform/Assets/Scripts/UI/YuanDan/CYuanDanQiangDanRankItem")
require ("auto_transform/Assets/Scripts/UI/YuanDan/CYuanDanQiangDanSituationWnd")
require ("auto_transform/Assets/Scripts/UI/YuanDan/CYuanDanQiangDanStartWnd")
require ("auto_transform/Assets/Scripts/UI/YuanDan/CYuanDanYaoQianWnd")
require ("auto_transform/Assets/Scripts/UI/YinLiangDuoBao/CDuoBaoMgr")
require ("auto_transform/Assets/Scripts/UI/YinLiangDuoBao/CDuoBaoPreviewer")
require ("auto_transform/Assets/Scripts/UI/YinLiangDuoBao/CDuoBaoWnd")
require ("auto_transform/Assets/Scripts/UI/YinLiangDuoBao/CDuoBaoingWnd")
require ("auto_transform/Assets/Scripts/UI/YinLiangDuoBao/CDuobaoResultWnd")
require ("auto_transform/Assets/Scripts/UI/YinLiangDuoBao/CDuobaoTableItem")
require ("auto_transform/Assets/Scripts/UI/YinLiangDuoBao/CMyDuoBaoWnd")
require ("auto_transform/Assets/Scripts/UI/YinLiangDuoBao/CMyDuobaoItem")
require ("auto_transform/Assets/Scripts/UI/Xinyi/CXinyiJingliItem")
require ("auto_transform/Assets/Scripts/UI/Xinyi/CXinyiWnd")
require ("auto_transform/Assets/Scripts/UI/XialvPK/CXialvPKResultPlayerTemplate")
require ("auto_transform/Assets/Scripts/UI/XialvPK/CXialvPKResultWenshi")
require ("auto_transform/Assets/Scripts/UI/XialvPK/CXialvPKResultWnd")
require ("auto_transform/Assets/Scripts/UI/XialvPK/CXialvPKResultWushi")
require ("auto_transform/Assets/Scripts/UI/XialvPK/CXialvPKSkillItem")
require ("auto_transform/Assets/Scripts/UI/XialvPK/CXialvPKTempSkillLeftView")
require ("auto_transform/Assets/Scripts/UI/XialvPK/CXialvPKTempSkillMiddleView")
require ("auto_transform/Assets/Scripts/UI/XialvPK/CXialvPKTempSkillRightView")
require ("auto_transform/Assets/Scripts/UI/XialvPK/CXialvPKTempSkillWnd")
require ("auto_transform/Assets/Scripts/UI/XialvPK/CXialvPKWenshi")
require ("auto_transform/Assets/Scripts/UI/XialvPK/CXialvPKWenshiPlayerTemplate")
require ("auto_transform/Assets/Scripts/UI/XialvPK/CCommonRuleWnd")
require ("auto_transform/Assets/Scripts/UI/WorldEvent/CWorldEventMgr")
require ("auto_transform/Assets/Scripts/UI/WorldEvent/CWorldEventVoiceItem")
require ("auto_transform/Assets/Scripts/UI/WorldEvent/CWorldEventVoiceWnd")
require ("auto_transform/Assets/Scripts/UI/Welfare/CCallBackFriendTemplate")
require ("auto_transform/Assets/Scripts/UI/Welfare/CCallBackFriendWnd")
require ("auto_transform/Assets/Scripts/UI/Welfare/CChargeActivityAwardTemplate")
require ("auto_transform/Assets/Scripts/UI/Welfare/CChargeActivityWindow")
require ("auto_transform/Assets/Scripts/UI/Welfare/CFirstChargeGiftCell")
require ("auto_transform/Assets/Scripts/UI/Welfare/CFirstChargeGiftWindow")
require ("auto_transform/Assets/Scripts/UI/Welfare/CFirstChargeLevelTemplate")
require ("auto_transform/Assets/Scripts/UI/Welfare/CFirstChargeWnd")
require ("auto_transform/Assets/Scripts/UI/Welfare/CGreenHandGiftWnd")
require ("auto_transform/Assets/Scripts/UI/Welfare/CHuahunMonthCardReturnWindow")
require ("auto_transform/Assets/Scripts/UI/Welfare/CHuiliuWnd")
require ("auto_transform/Assets/Scripts/UI/Welfare/CMonthCardReturnWindow")
require ("auto_transform/Assets/Scripts/UI/Welfare/CMultiExpDetailview")
require ("auto_transform/Assets/Scripts/UI/Welfare/COtherWelfareTemplate")
require ("auto_transform/Assets/Scripts/UI/Welfare/COtherWelfareWindow")
require ("auto_transform/Assets/Scripts/UI/Welfare/CQnRetuanDetailView")
require ("auto_transform/Assets/Scripts/UI/Welfare/CQnReturnAwardTemplate")
require ("auto_transform/Assets/Scripts/UI/Welfare/CQnReturnConfirm")
require ("auto_transform/Assets/Scripts/UI/Welfare/CQnReturnTemplate")
require ("auto_transform/Assets/Scripts/UI/Welfare/CQnReturnWindow")
require ("auto_transform/Assets/Scripts/UI/Welfare/CSchoolInvitationWnd")
require ("auto_transform/Assets/Scripts/UI/Welfare/CSignItemCell")
require ("auto_transform/Assets/Scripts/UI/Welfare/CWelfareBonusAwardTemplate")
require ("auto_transform/Assets/Scripts/UI/Welfare/CWelfareBonusTemplate")
require ("auto_transform/Assets/Scripts/UI/Welfare/SignScrollView")
require ("auto_transform/Assets/Scripts/UI/WorldHistory/CWorldHistoryWnd")
require ("auto_transform/Assets/Scripts/UI/WeekendGameplay/CQianshijingWnd")
require ("auto_transform/Assets/Scripts/UI/WeekendGameplay/CShishijingWnd")
require ("auto_transform/Assets/Scripts/UI/WeekendGameplay/CWeekendPlayBossTemplate")
require ("auto_transform/Assets/Scripts/UI/WeekendGameplay/CWeekendPlayRankWnd")
require ("auto_transform/Assets/Scripts/UI/WeekendFight/CWeekendFightMgr")
require ("auto_transform/Assets/Scripts/UI/WeekendFight/CWeekendFightRankItem")
require ("auto_transform/Assets/Scripts/UI/WeekendFight/CWeekendFightRankWnd")
require ("auto_transform/Assets/Scripts/UI/WeekendFight/CWeekendFightTopRightWnd")
require ("auto_transform/Assets/Scripts/UI/WeekendFight/CWeekendFightView")
require ("auto_transform/Assets/Scripts/UI/Valentine/CValentineConfirmWnd")
require ("auto_transform/Assets/Scripts/UI/Valentine/CValentineExchangeWnd")
require ("auto_transform/Assets/Scripts/UI/Valentine/CValentineQAWnd")
require ("auto_transform/Assets/Scripts/UI/Valentine/CValentineRoseSendWnd")
require ("auto_transform/Assets/Scripts/UI/Valentine/CValentineMgr")
require ("auto_transform/Assets/Scripts/UI/Trade/CTradeBuySellPlayerInfo")
require ("auto_transform/Assets/Scripts/UI/Trade/CTradeHongbaoWnd")
require ("auto_transform/Assets/Scripts/UI/Trade/CTradeMessageBox")
require ("auto_transform/Assets/Scripts/UI/Trade/CTradePresentedDetailView")
require ("auto_transform/Assets/Scripts/UI/Trade/CTradePresentedWnd")
require ("auto_transform/Assets/Scripts/UI/Trade/CTradeSendChooseWnd")
require ("auto_transform/Assets/Scripts/UI/Trade/CTradeTip")
require ("auto_transform/Assets/Scripts/UI/Trade/CTradeWnd")
require ("auto_transform/Assets/Scripts/UI/Trade/CTradeDetailView")
require ("auto_transform/Assets/Scripts/UI/Title/CTitleDetailView")
require ("auto_transform/Assets/Scripts/UI/Title/CTitleRowTemplate")
require ("auto_transform/Assets/Scripts/UI/Title/CTitleTableView")
require ("auto_transform/Assets/Scripts/UI/TipWnd/CMessageTipWnd")
require ("auto_transform/Assets/Scripts/UI/TipWnd/CTipParagraphItem")
require ("auto_transform/Assets/Scripts/UI/Talisman/CTalismanBaptizeDetailView")
-- require ("auto_transform/Assets/Scripts/UI/Talisman/CTalismanBaptizeItemList")
require ("auto_transform/Assets/Scripts/UI/Talisman/CTalismanBaptizeListTemplate")
-- require ("auto_transform/Assets/Scripts/UI/Talisman/CTalismanBaptizeWindow")
require ("auto_transform/Assets/Scripts/UI/Talisman/CTalismanBaptizeWordCell")
require ("auto_transform/Assets/Scripts/UI/Talisman/CTalismanEquipView")
require ("auto_transform/Assets/Scripts/UI/Talisman/CTalismanFuzhuDetailView")
require ("auto_transform/Assets/Scripts/UI/Talisman/CTalismanFuzhuListView")
require ("auto_transform/Assets/Scripts/UI/Talisman/CTalismanFuzhuWordTemplate")
require ("auto_transform/Assets/Scripts/UI/Talisman/CTalismanItemCell")
require ("auto_transform/Assets/Scripts/UI/Talisman/CTalismanMenu")
require ("auto_transform/Assets/Scripts/UI/Talisman/CTalismanMenuTemplate")
require ("auto_transform/Assets/Scripts/UI/Talisman/CTalismanPackageView")
require ("auto_transform/Assets/Scripts/UI/Talisman/CTalismanPositionTemplate")
require ("auto_transform/Assets/Scripts/UI/Talisman/CTalismanWnd")
require ("auto_transform/Assets/Scripts/UI/Talisman/CTalismanWordAutoBaptizeConditionWnd")
require ("auto_transform/Assets/Scripts/UI/Talisman/CTalismanWordBaptizeItemInfo")
require ("auto_transform/Assets/Scripts/UI/Talisman/CTalismanWordBaptizeWnd")
require ("auto_transform/Assets/Scripts/UI/Talisman/CTalismanWordBaptizeWordList")
require ("auto_transform/Assets/Scripts/UI/Talisman/CXianjiaComposeWnd")
require ("auto_transform/Assets/Scripts/UI/Talisman/CXianjiaRawMaterial")
require ("auto_transform/Assets/Scripts/UI/Talisman/CXianjiaSelectTemplate")
require ("auto_transform/Assets/Scripts/UI/Talisman/CXianjiaSelectWnd")
require ("auto_transform/Assets/Scripts/UI/Talisman/CXianjiaUpgradeDetailView")
-- require ("auto_transform/Assets/Scripts/UI/Talisman/CXianjiaUpgradeWindow")
require ("auto_transform/Assets/Scripts/UI/Talisman/CTalismanWordAutoBaptizeWnd")
require ("auto_transform/Assets/Scripts/UI/SpringFestival/CSpringFestivalNianshouCheckWnd")
require ("auto_transform/Assets/Scripts/UI/SpringFestival/CSpringFestivalNianshouGameMapWnd")
require ("auto_transform/Assets/Scripts/UI/SpringFestival/CSpringFestivalNianshouGameStateWnd")
require ("auto_transform/Assets/Scripts/UI/SpringFestival/CSpringFestivalNianshouOpenWnd")
require ("auto_transform/Assets/Scripts/UI/SpringFestival/CSpringFestivalNianshouResultWnd")
require ("auto_transform/Assets/Scripts/UI/SnowBallFight/CSnowBallFightInfoWnd")
require ("auto_transform/Assets/Scripts/UI/SnowBallFight/CSnowBallMgr")
require ("auto_transform/Assets/Scripts/UI/SnowBallFight/CSnowBallOperateWnd")
require ("auto_transform/Assets/Scripts/UI/SnowBallFight/CSnowBallResultWnd")
require ("auto_transform/Assets/Scripts/UI/SnowBallFight/CSnowBallStatusView")
require ("auto_transform/Assets/Scripts/UI/SnowBallFight/CSnowBallTopRightWnd")
require ("auto_transform/Assets/Scripts/UI/ShopMall/CLinyuShopPopupMenu")
require ("auto_transform/Assets/Scripts/UI/ShopMall/CLinyuShopWnd")
require ("auto_transform/Assets/Scripts/UI/ShopMall/CQnPointExchangeWnd")
require ("auto_transform/Assets/Scripts/UI/ShopMall/CShopMallGoodsItem")
require ("auto_transform/Assets/Scripts/UI/ShopMall/CShopMallGoodsItem2")
require ("auto_transform/Assets/Scripts/UI/ShopMall/CYuanbaoBuyTableView")
require ("auto_transform/Assets/Scripts/UI/ShopMall/CYuanbaoMarketBuy")
require ("auto_transform/Assets/Scripts/UI/ShopMall/CYuanbaoMarketPackageView")
require ("auto_transform/Assets/Scripts/UI/ShopMall/CYuanbaoMarketSell")
require ("auto_transform/Assets/Scripts/UI/ShopMall/CYuanbaoMarketWnd")
require ("auto_transform/Assets/Scripts/UI/ShopMall/CYuanbaoShopWnd")
require ("auto_transform/Assets/Scripts/UI/ShiTu/CShiTuAskWnd")
require ("auto_transform/Assets/Scripts/UI/ShiTu/CShiTuChooseShifuWnd")
require ("auto_transform/Assets/Scripts/UI/ShiTu/CShiTuChooseTudiWnd")
require ("auto_transform/Assets/Scripts/UI/ShiTu/CShiTuQAWnd")
require ("auto_transform/Assets/Scripts/UI/ShiTu/CShiTuRecordButton")
require ("auto_transform/Assets/Scripts/UI/ShanHeTu/CShanHeTuLocationItem")
require ("auto_transform/Assets/Scripts/UI/ShanHeTu/CShanHeTuWnd")
require ("auto_transform/Assets/Scripts/UI/Setting/CSettingWnd_BlockSetting")
require ("auto_transform/Assets/Scripts/UI/Setting/CUnlockScreenWnd")
require ("auto_transform/Assets/Scripts/UI/SelfieWnd/CTiezhiItem")
require ("auto_transform/Assets/Scripts/UI/SecondPwd/CSecondPwdChangeWnd")
require ("auto_transform/Assets/Scripts/UI/SecondPwd/CSecondPwdConfirmWnd")
require ("auto_transform/Assets/Scripts/UI/SecondPwd/CSecondPwdContentWnd")
require ("auto_transform/Assets/Scripts/UI/SecondPwd/CSecondPwdMgr")
require ("auto_transform/Assets/Scripts/UI/SecondPwd/CSecondPwdSetWnd")
require ("auto_transform/Assets/Scripts/UI/SecondPwd/CSecondPwdVerifyWnd")
require ("auto_transform/Assets/Scripts/UI/SecondPwd/CSecondPwdWnd")
require ("auto_transform/Assets/Scripts/UI/Schedule/CScheduleAlertCtl")
-- require ("auto_transform/Assets/Scripts/UI/Schedule/CScheduleHuoliTip")
-- require ("auto_transform/Assets/Scripts/UI/Schedule/CScheduleHuoliTipItem")
require ("auto_transform/Assets/Scripts/UI/Rank/CJieBaiRankPlayerTemplate")
require ("auto_transform/Assets/Scripts/UI/Rank/CJieBaiRankInfoWnd")
require ("auto_transform/Assets/Scripts/UI/Rank/CRankClassTemplate")
require ("auto_transform/Assets/Scripts/UI/Rank/CRankData")
require ("auto_transform/Assets/Scripts/UI/Rank/CRankDetailView")
require ("auto_transform/Assets/Scripts/UI/Rank/CRankItemTemplate")
require ("auto_transform/Assets/Scripts/UI/Rank/CRankMainPlayerInfo")
require ("auto_transform/Assets/Scripts/UI/Rank/CRankMasterView")
require ("auto_transform/Assets/Scripts/UI/Rank/CRankTabBar")
require ("auto_transform/Assets/Scripts/UI/Rank/CRankTableBody")
require ("auto_transform/Assets/Scripts/UI/Rank/CRankTableHeader")
require ("auto_transform/Assets/Scripts/UI/BiWuDaHui/CBWDHChampionLookupItem")
require ("auto_transform/Assets/Scripts/UI/BiWuDaHui/CBWDHChampionLookupWnd")
require ("auto_transform/Assets/Scripts/UI/BiWuDaHui/CBWDHChampionMemberItem")
require ("auto_transform/Assets/Scripts/UI/BiWuDaHui/CBWDHChampionMemberWnd")
require ("auto_transform/Assets/Scripts/UI/BiWuDaHui/CBWDHStartWnd")
require ("auto_transform/Assets/Scripts/UI/BiWuDaHui/CBWDHWatchGroupItem")
require ("auto_transform/Assets/Scripts/UI/BiWuDaHui/CBWDHWatchItem")
require ("auto_transform/Assets/Scripts/UI/Guild/GuildLianShen/CLianShenSkillItem")
require ("auto_transform/Assets/Scripts/UI/AutoDrug/CDrugHMPItemCell")
require ("auto_transform/Assets/Scripts/UI/BiWuZhaoQing/CBWZQAttendWnd")
require ("auto_transform/Assets/Scripts/UI/BiWuZhaoQing/CBWZQOpenWnd")
require ("auto_transform/Assets/Scripts/UI/BiWuZhaoQing/CBWZQRiseWnd")
require ("auto_transform/Assets/Scripts/UI/BiWuZhaoQing/CBWZQSuccWnd")
require ("auto_transform/Assets/Scripts/UI/Baby/CBabyClothWnd")
require ("auto_transform/Assets/Scripts/Game/PersonalSpace/CPersonalSpaceMgr")
require ("auto_transform/Assets/Scripts/UI/Main/ChuangGuan/CChuangGuanRankWnd")
require ("auto_transform/Assets/Scripts/UI/Main/ChuangGuan/CChuangGuanRankItem")
require ("auto_transform/Assets/Scripts/UI/Main/ChuangGuan/CChuangGuanMainWnd")
require ("auto_transform/Assets/Scripts/UI/PersonalSpace/CPersonalSpaceHotMomentsView")
require ("auto_transform/Assets/Scripts/UI/Charge/CChargeGitItem")
require ("auto_transform/Assets/Scripts/UI/Charge/CChargeGuideCtrl")
require ("auto_transform/Assets/Scripts/UI/Charge/CChargeItem")
require ("auto_transform/Assets/Scripts/UI/Charge/CChargeMonthCardWnd")
require ("auto_transform/Assets/Scripts/UI/Charge/CChargePrivilegeItem")
require ("auto_transform/Assets/Scripts/UI/Charge/CChargeWnd")
require ("auto_transform/Assets/Scripts/UI/Charge/CChargeWnd_Gift")
require ("auto_transform/Assets/Scripts/UI/Charge/CChargeWnd_Privilege")
require ("auto_transform/Assets/Scripts/UI/Charge/CGiftTemplate")
require ("auto_transform/Assets/Scripts/UI/Christmas/CChristmasCardEditWnd")
require ("auto_transform/Assets/Scripts/UI/Christmas/CChristmasCardItem")
require ("auto_transform/Assets/Scripts/UI/Christmas/CChristmasCardMgr")
require ("auto_transform/Assets/Scripts/UI/Christmas/CChristmasCardPopupMenu")
require ("auto_transform/Assets/Scripts/UI/Christmas/CChristmasCardWnd")
require ("auto_transform/Assets/Scripts/UI/Christmas/CChristmasTreeRankWnd")
require ("auto_transform/Assets/Scripts/UI/Christmas/CHongLvSkillWnd")
require ("auto_transform/Assets/Scripts/UI/Christmas/CHongLvTemple")
require ("auto_transform/Assets/Scripts/UI/Christmas/CWebTexture")
require ("auto_transform/Assets/Scripts/UI/Chujia/CAnswerHuaYuanMessageWnd")
require ("auto_transform/Assets/Scripts/UI/Chujia/CPutuanPopupMenu")
require ("auto_transform/Assets/Scripts/UI/Chujia/CRequestHuaYuanMessageWnd")
require ("auto_transform/Assets/Scripts/UI/Chujia/ChujiaItem")
require ("auto_transform/Assets/Scripts/UI/Chujia/ChujiaListWnd")
require ("auto_transform/Assets/Scripts/UI/CuJu/CCuJuMgr")
require ("auto_transform/Assets/Scripts/UI/DaDiZi/CDaDiZiBattleMatchWnd")
require ("auto_transform/Assets/Scripts/UI/DaDiZi/CDaDiZiBuffIcon")
require ("auto_transform/Assets/Scripts/UI/DaDiZi/CDaDiZiMgr")
require ("auto_transform/Assets/Scripts/UI/DaDiZi/CDaDiZiStageNode")
require ("auto_transform/Assets/Scripts/UI/DaDiZi/CDaDiZiStateWnd")
require ("auto_transform/Assets/Scripts/UI/DaDiZi/CDaDiZiWatchItem")
require ("auto_transform/Assets/Scripts/UI/DaDiZi/CDaDiZiWatchStateItem")
require ("auto_transform/Assets/Scripts/UI/DaDiZi/CDaDiZiWatchStateWnd")
require ("auto_transform/Assets/Scripts/UI/DaDiZi/CDaDiZiWatchWnd")
require ("auto_transform/Assets/Scripts/UI/DuoHun/CFoeListWnd")
require ("auto_transform/Assets/Scripts/UI/DuoHun/CFoeSelectPlayerTemplate")
require ("auto_transform/Assets/Scripts/UI/DuoHun/CFoeSelectWnd")
require ("auto_transform/Assets/Scripts/UI/DuoHun/CHunPoWnd")
require ("auto_transform/Assets/Scripts/UI/DuoHun/CQianliyanWnd")
require ("auto_transform/Assets/Scripts/UI/DuoHun/CZhaoHunWnd")
require ("auto_transform/Assets/Scripts/UI/DyeWnd/CDyeWnd")
require ("auto_transform/Assets/Scripts/UI/ExpertTeam/CExpertTeamAnswerWnd")
require ("auto_transform/Assets/Scripts/UI/ExpertTeam/CExpertTeamAskDetailWnd")
require ("auto_transform/Assets/Scripts/UI/ExpertTeam/CExpertTeamAskHisWnd")
require ("auto_transform/Assets/Scripts/UI/ExpertTeam/CExpertTeamAskWnd")
require ("auto_transform/Assets/Scripts/UI/ExpertTeam/CExpertTeamPersonalInfoWnd")
require ("auto_transform/Assets/Scripts/UI/ExpertTeam/CExpertTeamQuestionWnd")
require ("auto_transform/Assets/Scripts/UI/ExpertTeam/CExpertTeamRankWnd")
require ("auto_transform/Assets/Scripts/UI/ExpertTeam/CExpertTeamWnd")
require ("auto_transform/Assets/Scripts/UI/Expression/CEditImageHandler")
require ("auto_transform/Assets/Scripts/UI/Expression/CExpressionCustomizeItem")
require ("auto_transform/Assets/Scripts/UI/Expression/CExpressionCustomizeWnd")
require ("auto_transform/Assets/Scripts/UI/Expression/CExpressionItem")
require ("auto_transform/Assets/Scripts/UI/Expression/CExpressionMgr")
require ("auto_transform/Assets/Scripts/UI/Expression/CExpressionWnd")
require ("auto_transform/Assets/Scripts/UI/ExpressionAction/CExpressionActionItem")
require ("auto_transform/Assets/Scripts/UI/ExpressionAction/CExpressionActionLoader")
require ("auto_transform/Assets/Scripts/UI/ExpressionAction/CExpressionActionView")
require ("auto_transform/Assets/Scripts/UI/ExpressionAction/CExpressionEquipSelectItem")
require ("auto_transform/Assets/Scripts/UI/ExpressionAction/CExpressionEquipSelectLoader")
require ("auto_transform/Assets/Scripts/UI/ExpressionAction/CExpressionEquipSelectWnd")
require ("auto_transform/Assets/Scripts/UI/ExpressionAction/CHugInvitationItem")
require ("auto_transform/Assets/Scripts/UI/ExpressionAction/CHugInvitationWnd")
require ("auto_transform/Assets/Scripts/UI/ExpressionAction/CKabeDonMessageBox")
require ("auto_transform/Assets/Scripts/UI/ExpressionAction/CSelectExpressionItem")
require ("auto_transform/Assets/Scripts/UI/ExpressionAction/CSelectExpressionWnd")
require ("auto_transform/Assets/Scripts/UI/ExpressionAction/CThumbnailExpressionItem")
require ("auto_transform/Assets/Scripts/UI/ExpressionAction/CThumbnailChatWnd")
require ("auto_transform/Assets/Scripts/UI/ExpressionAction/CThumbnailExpressionView")
require ("auto_transform/Assets/Scripts/UI/FamilyTree/CFamilyItem")
require ("auto_transform/Assets/Scripts/UI/FeiSheng/CConstellationWnd")
--OK: require ("auto_transform/Assets/Scripts/UI/FeiSheng/CRotateCameraPanel")
--OK: require ("auto_transform/Assets/Scripts/UI/FeiSheng/CStarItem")
require ("auto_transform/Assets/Scripts/UI/FeiSheng/CXingGuanItem")
--OK: require ("auto_transform/Assets/Scripts/UI/FeiSheng/CXingGuanShengJiWnd")
require ("auto_transform/Assets/Scripts/UI/FeiSheng/CXingGuanWnd")
--OK: require ("auto_transform/Assets/Scripts/UI/FeiSheng/CXingMangTemplate")
require ("auto_transform/Assets/Scripts/UI/Freight/CCargoListItemCell")
require ("auto_transform/Assets/Scripts/UI/Freight/CFreightBaseItemCell")
-- require ("auto_transform/Assets/Scripts/UI/Freight/CFreightCargoInfo")
-- require ("auto_transform/Assets/Scripts/UI/Freight/CFreightCargoList")
require ("auto_transform/Assets/Scripts/UI/Freight/CFreightEquipSubmitWnd")
require ("auto_transform/Assets/Scripts/UI/Freight/CFreightEquipmentCell")
require ("auto_transform/Assets/Scripts/UI/Freight/CFreightEquipments")
require ("auto_transform/Assets/Scripts/UI/Freight/CFreightTaskWnd")
-- require ("auto_transform/Assets/Scripts/UI/Freight/CFreightTransport")
-- require ("auto_transform/Assets/Scripts/UI/Freight/CFreightWnd")
-- require ("auto_transform/Assets/Scripts/UI/Freight/CTransportAwardItemCell")
require ("auto_transform/Assets/Scripts/UI/ftfTransaction/CftfTApplyList")
require ("auto_transform/Assets/Scripts/UI/ftfTransaction/CftfTApplyListTpl")
require ("auto_transform/Assets/Scripts/UI/ftfTransaction/CftfTransactionListWnd")
require ("auto_transform/Assets/Scripts/UI/GaoChang/CGaoChangEnterWnd")
require ("auto_transform/Assets/Scripts/UI/GaoChang/CGaoChangResultWnd")
require ("auto_transform/Assets/Scripts/UI/Guideline/CGuidelineDetailListItem")
require ("auto_transform/Assets/Scripts/UI/Guideline/CGuidelineWnd")
require ("auto_transform/Assets/Scripts/UI/Guideline/CLevelUpGuideDetailView")
require ("auto_transform/Assets/Scripts/UI/Guideline/CLevelUpGuideMasterView")
require ("auto_transform/Assets/Scripts/UI/GuildQueen/CGuildQueenLevelItem")
require ("auto_transform/Assets/Scripts/UI/GuildQueen/CGuildQueenMgr")
require ("auto_transform/Assets/Scripts/UI/GuildQueen/CGuildQueenStateWnd")
require ("auto_transform/Assets/Scripts/UI/GuildQueen/CGuildQueenWnd")
require ("auto_transform/Assets/Scripts/UI/HideAndSeek/CHideAndSeekBigTypeWnd")
require ("auto_transform/Assets/Scripts/UI/HideAndSeek/CHideAndSeekHiderResultItem")
require ("auto_transform/Assets/Scripts/UI/HideAndSeek/CHideAndSeekRankItem")
require ("auto_transform/Assets/Scripts/UI/HideAndSeek/CHideAndSeekRankWnd")
require ("auto_transform/Assets/Scripts/UI/HideAndSeek/CHideAndSeekSeekerResultItem")
require ("auto_transform/Assets/Scripts/UI/HideAndSeek/CHideAndSeekSkillItem")
require ("auto_transform/Assets/Scripts/UI/HideAndSeek/CHideAndSeekSmallTypeItem")
require ("auto_transform/Assets/Scripts/UI/HideAndSeek/CHideAndSeekSmallTypeWnd")
require ("auto_transform/Assets/Scripts/UI/HideAndSeek/CHideAndSeekStateWnd")
require ("auto_transform/Assets/Scripts/UI/HongBaoWnd/CAwardGiftWnd")
require ("auto_transform/Assets/Scripts/UI/HongBaoWnd/CGuildHongbaoSetting")
require ("auto_transform/Assets/Scripts/UI/HongBaoWnd/CHongBaoDetailItem")
require ("auto_transform/Assets/Scripts/UI/HongBaoWnd/CHongBaoDetailNewItem")
require ("auto_transform/Assets/Scripts/UI/HongBaoWnd/CHongBaoDetailNewWnd")
require ("auto_transform/Assets/Scripts/UI/HongBaoWnd/CHongBaoDetailWnd")
require ("auto_transform/Assets/Scripts/UI/HongBaoWnd/CHongBaoEffect")
-- require ("auto_transform/Assets/Scripts/UI/HongBaoWnd/CHongBaoItem")
--OK: require ("auto_transform/Assets/Scripts/UI/HongBaoWnd/CHongBaoMgr")
-- require ("auto_transform/Assets/Scripts/UI/HongBaoWnd/CHongBaoRain")
-- require ("auto_transform/Assets/Scripts/UI/HongBaoWnd/CHongBaoWnd")
require ("auto_transform/Assets/Scripts/UI/HongBaoWnd/CHongbaoHistoryWnd")
require ("auto_transform/Assets/Scripts/UI/HongBaoWnd/CHongbaoHistroyItem")
require ("auto_transform/Assets/Scripts/UI/HorseRace/CGuildHorseRaceApplyListView")
require ("auto_transform/Assets/Scripts/UI/HorseRace/CGuildHorseRaceApplyPlayerTemplate")
require ("auto_transform/Assets/Scripts/UI/HorseRace/CGuildHorseRaceApplyWnd")
require ("auto_transform/Assets/Scripts/UI/HorseRace/CGuildHorseRaceChampionWnd")
require ("auto_transform/Assets/Scripts/UI/HorseRace/CGuildHorseRaceMainGroupTemplate")
require ("auto_transform/Assets/Scripts/UI/HorseRace/CGuildHorseRaceMainListView")
require ("auto_transform/Assets/Scripts/UI/HorseRace/CGuildHorseRaceMainPlayerTemplate")
require ("auto_transform/Assets/Scripts/UI/HorseRace/CGuildHorseRaceMainWnd")
require ("auto_transform/Assets/Scripts/UI/HorseRace/CGuildHorseRaceWatchMemberList")
require ("auto_transform/Assets/Scripts/UI/HorseRace/CGuildHorseRaceWatchStateWnd")
require ("auto_transform/Assets/Scripts/UI/HorseRace/CHorseRaceBaseWnd")
require ("auto_transform/Assets/Scripts/UI/HorseRace/CHorseRaceRankTemplate")
require ("auto_transform/Assets/Scripts/UI/HorseRace/CHorseRaceRankWnd")
require ("auto_transform/Assets/Scripts/UI/HorseRace/CHorseRaceSelectDetailView")
require ("auto_transform/Assets/Scripts/UI/HorseRace/CHorseRaceSelectListView")
require ("auto_transform/Assets/Scripts/UI/HorseRace/CHorseRaceSelectMapiTemplate")
require ("auto_transform/Assets/Scripts/UI/HorseRace/CHorseRaceSelectWnd")
-- require ("auto_transform/Assets/Scripts/UI/HousePuppet/CCPPuppetInfoWnd")
-- require ("auto_transform/Assets/Scripts/UI/HousePuppet/CPlayerPuppetInfoWnd")
require ("auto_transform/Assets/Scripts/UI/HousePuppet/CPlayerPuppetInviteWnd")
require ("auto_transform/Assets/Scripts/UI/HousePuppet/CPuppetAppearanceWnd")
require ("auto_transform/Assets/Scripts/UI/HousePuppet/CPuppetClothespressDressList")
-- require ("auto_transform/Assets/Scripts/UI/HousePuppet/CPuppetDetailPropertySection")
require ("auto_transform/Assets/Scripts/UI/HousePuppet/CPuppetExpressionActionItem")
require ("auto_transform/Assets/Scripts/UI/HousePuppet/CPuppetExpressionActionView")
require ("auto_transform/Assets/Scripts/UI/HousePuppet/CPuppetFashionAcquireWnd")
require ("auto_transform/Assets/Scripts/UI/HousePuppet/CPuppetFashionClothesWindow")
require ("auto_transform/Assets/Scripts/UI/HousePuppet/CPuppetFashionTabenDetailView")
require ("auto_transform/Assets/Scripts/UI/HousePuppet/CPuppetFightDataWnd")
require ("auto_transform/Assets/Scripts/UI/HousePuppet/CPuppetInteractWnd")
require ("auto_transform/Assets/Scripts/UI/HousePuppet/CPuppetOtherFightDataWnd")
require ("auto_transform/Assets/Scripts/UI/HousePuppet/CPuppetQueryWnd")
require ("auto_transform/Assets/Scripts/UI/HousePuppet/CPuppetSetGossipWnd")
require ("auto_transform/Assets/Scripts/UI/HousePuppet/CXiangfangCPPuppetTemplate")
require ("auto_transform/Assets/Scripts/UI/HousePuppet/CXiangfangPlayerPuppetTemplate")
require ("auto_transform/Assets/Scripts/UI/HuluBrothers/CHuluBrothersChooseWnd")
require ("auto_transform/Assets/Scripts/UI/HuluBrothers/CHuluBrothersRankItem")
require ("auto_transform/Assets/Scripts/UI/HuluBrothers/CHuluBrothersRankWnd")
require ("auto_transform/Assets/Scripts/UI/HuluBrothers/CHuluBrothersStartWnd")
require ("auto_transform/Assets/Scripts/UI/InviteFriend/CInviteFriendRankWnd")
require ("auto_transform/Assets/Scripts/UI/InviteFriend/CInviteFriendTotalInviteWnd")
require ("auto_transform/Assets/Scripts/UI/InviteFriend/CInviteFriendWnd")
require ("auto_transform/Assets/Scripts/UI/JieBai/CJieBaiChangeNameWnd")
require ("auto_transform/Assets/Scripts/UI/JieBai/CJieBaiSelfWnd")
require ("auto_transform/Assets/Scripts/UI/JieBai/CJieBaiWnd")
require ("auto_transform/Assets/Scripts/UI/JXYS/CJXYSBossItem")
require ("auto_transform/Assets/Scripts/UI/JXYS/CJXYSComicWnd")
require ("auto_transform/Assets/Scripts/UI/JXYS/CJXYSBossWnd")
require ("auto_transform/Assets/Scripts/UI/KeJu/CKeJuCCLive")
require ("auto_transform/Assets/Scripts/UI/KeJu/CKeJuGambleWnd")
require ("auto_transform/Assets/Scripts/UI/KeJu/CKeJuHuishi")
require ("auto_transform/Assets/Scripts/UI/KeJu/CKeJuQuestionTemplate")
require ("auto_transform/Assets/Scripts/UI/KeJu/CKeJuReplayWnd")
require ("auto_transform/Assets/Scripts/UI/KeJu/CKeJuWnd")
require ("auto_transform/Assets/Scripts/UI/KeJu/CKeJuXiangshi")
require ("auto_transform/Assets/Scripts/UI/KouDao/CJingYingKouDaoProgressWnd")
require ("auto_transform/Assets/Scripts/UI/KouDao/CKouDaoSituationRankListItem")
require ("auto_transform/Assets/Scripts/UI/LiaoLuoWan/CTanBaoWnd")
require ("auto_transform/Assets/Scripts/UI/LingShou/CLingShouBabyModelTextureLoader")
-- require ("auto_transform/Assets/Scripts/UI/LingShou/CLingShouItem")//CLingShouSwitchSkillTableItem继承了该组件，会有问题，暂时注释掉
require ("auto_transform/Assets/Scripts/UI/LingShou/CLingShouModelTextureLoader")
require ("auto_transform/Assets/Scripts/UI/LingShou/CLingShouMorphWnd")
require ("auto_transform/Assets/Scripts/UI/LingShou/CLingShouSkillSlot")
require ("auto_transform/Assets/Scripts/UI/LingShou2/CLingShouAutoWashWnd")
require ("auto_transform/Assets/Scripts/UI/LingShou2/CLingShouPinZhi")
require ("auto_transform/Assets/Scripts/UI/LingShou2/CLingShouPurchaseWnd")
require ("auto_transform/Assets/Scripts/UI/LingShou2/CLingShouSkills")
require ("auto_transform/Assets/Scripts/UI/LingShou2/CLingShouUnlockWnd")
require ("auto_transform/Assets/Scripts/UI/LingShou2/CLingShouWashCost")
require ("auto_transform/Assets/Scripts/UI/LingShou2/CLingShouYuanShenSelector")
require ("auto_transform/Assets/Scripts/UI/LingShou2/CLingShouYuanShenSelectorItem")
require ("auto_transform/Assets/Scripts/UI/LingShou2/CLingshouOverViewModels")
require ("auto_transform/Assets/Scripts/UI/LingShou2/CLingshouOverviewDetailView")
require ("auto_transform/Assets/Scripts/UI/LingShou2/CLingshouOverviewItemCell")
require ("auto_transform/Assets/Scripts/UI/LingShou2/CLingshouOverviewListView")
--OK: require ("auto_transform/Assets/Scripts/UI/LingShou/CLingShouPropDisplay")
require ("auto_transform/Assets/Scripts/UI/LingShou/CLingShouWashResultWnd")
require ("auto_transform/Assets/Scripts/UI/LingShou2/SwitchSkill/CLingShouSkillSwitchSelectItem")
require ("auto_transform/Assets/Scripts/UI/LingShou2/SwitchSkill/CLingShouSkillSwitchSelectWnd")
require ("auto_transform/Assets/Scripts/UI/LingShou2/SwitchSkill/CLingShouSkillSwitchSuccessWnd")
require ("auto_transform/Assets/Scripts/UI/LingShou2/SwitchSkill/CLingShouSwitchSkillLingShou")
require ("auto_transform/Assets/Scripts/UI/LingShou2/SwitchSkill/CLingShouSwitchSkillMgr")
require ("auto_transform/Assets/Scripts/UI/LingShou2/SwitchSkill/CLingShouSwitchSkillSlot")
--require ("auto_transform/Assets/Scripts/UI/LingShou2/SwitchSkill/CLingShouSwitchSkillTableItem")
--require ("auto_transform/Assets/Scripts/UI/LingShou2/SwitchSkill/CLingShouSwitchSkillWnd")
require ("auto_transform/Assets/Scripts/UI/LingShou2/SwitchSkill/CLingShouSwitchSkills")
require ("auto_transform/Assets/Scripts/UI/LingshouLuandou/CLingshouLuandouProgressWnd")
require ("auto_transform/Assets/Scripts/UI/LingshouLuandou/CLingshouLuandouRankWnd")
require ("auto_transform/Assets/Scripts/UI/LingshouLuandou/CLingshouLuandouWnd")
require ("auto_transform/Assets/Scripts/UI/Marriage/CBaziCheckWnd")
require ("auto_transform/Assets/Scripts/UI/Marriage/CBaziCoupleInfo")
require ("auto_transform/Assets/Scripts/UI/MergeServerVote/CMergeServerVoteWnd")
require ("auto_transform/Assets/Scripts/UI/MiniMap/CMiniMapGuildLeagueBossTemplate")
require ("auto_transform/Assets/Scripts/UI/MiniMap/CMiniMapGuildLeagueFlagTemplate")
require ("auto_transform/Assets/Scripts/UI/NationalDayWnd/CNationalDayMgr")
require ("auto_transform/Assets/Scripts/UI/NationalDayWnd/CNationalDayRankWnd")
require ("auto_transform/Assets/Scripts/UI/NationalDayWnd/CNationalDayWnd")
require ("auto_transform/Assets/Scripts/UI/PlayerShop/CPlayerShopBill")
require ("auto_transform/Assets/Scripts/UI/PlayerShop/CPlayerShopBillItem")
require ("auto_transform/Assets/Scripts/UI/PlayerShop/CPlayerShopBuyTableView")
require ("auto_transform/Assets/Scripts/UI/PlayerShop/CPlayerShopItem")
require ("auto_transform/Assets/Scripts/UI/PlayerShop/CPlayerShopMgr")
require ("auto_transform/Assets/Scripts/UI/PlayerShop/CPlayerShopNumInputBox")
require ("auto_transform/Assets/Scripts/UI/PlayerShop/CPlayerShopShareWnd")
require ("auto_transform/Assets/Scripts/UI/PlayerShop/CPlayerShopTemplateItem")
require ("auto_transform/Assets/Scripts/UI/PlayerShop/CPlayerShopWnd")
require ("auto_transform/Assets/Scripts/UI/PlayerShop/CPlayerShop_Bankrupt")
require ("auto_transform/Assets/Scripts/UI/PlayerShop/CPlayerShop_DeletePlace")
require ("auto_transform/Assets/Scripts/UI/PlayerShop/CPlayerShop_EquipSearch")
require ("auto_transform/Assets/Scripts/UI/PlayerShop/CPlayerShop_Focus")
require ("auto_transform/Assets/Scripts/UI/PlayerShop/CPlayerShop_Frozen")
require ("auto_transform/Assets/Scripts/UI/PlayerShop/CPlayerShop_FurnitureSearch")
require ("auto_transform/Assets/Scripts/UI/PlayerShop/CPlayerShop_Info")
require ("auto_transform/Assets/Scripts/UI/PlayerShop/CPlayerShop_ItemInfo")
require ("auto_transform/Assets/Scripts/UI/PlayerShop/CPlayerShop_Open")
require ("auto_transform/Assets/Scripts/UI/PlayerShop/CPlayerShop_Operate")
require ("auto_transform/Assets/Scripts/UI/PlayerShop/CPlayerShop_OtherBuyGuide")
require ("auto_transform/Assets/Scripts/UI/PlayerShop/CPlayerShop_Publicity")
require ("auto_transform/Assets/Scripts/UI/PlayerShop/CPlayerShop_Sell")
require ("auto_transform/Assets/Scripts/UI/PlayerShop/CPlayerShop_Shelf")
require ("auto_transform/Assets/Scripts/UI/PlayerShop/CPlayerShop_TalismanSearch")
require ("auto_transform/Assets/Scripts/UI/PlayerShop/CPlayerShop_UnShelf")
require ("auto_transform/Assets/Scripts/UI/PlayerShop/CPlayerShop_ZhushabiSearch")
require ("auto_transform/Assets/Scripts/UI/PlayerShop/CplayerShop_NormalSearch")
require ("auto_transform/Assets/Scripts/UI/PlayerShop/CPlayerShop_Buy")
require ("auto_transform/Assets/Scripts/UI/PK/CPKRuleBoard")
require ("auto_transform/Assets/Scripts/UI/PK/CPetPKSettingWnd")
require ("auto_transform/Assets/Scripts/UI/QuanMinPK/CQMPKMatchNodeItem")
require ("auto_transform/Assets/Scripts/UI/QuanMinPK/CQMPKMemberInfoItem")
require ("auto_transform/Assets/Scripts/UI/QuanMinPK/CQMPKScoreItem")
require ("auto_transform/Assets/Scripts/UI/QuanMinPK/CQMPKScoreWnd")
require ("auto_transform/Assets/Scripts/UI/QuanMinPK/CQMPKTitleTemplate")
require ("auto_transform/Assets/Scripts/UI/QuanMinPK/CQMPKWordsListTemplate")
require ("auto_transform/Assets/Scripts/UI/QuanMinPK/CQMPKZhanDuiItemCopy")
require ("auto_transform/Assets/Scripts/UI/QuanMinPK/CQMPKZhanDuiMemberListWnd")
require ("auto_transform/Assets/Scripts/UI/PlayerInfoWnd/CPlayerAppearanceInfoWnd")
require ("auto_transform/Assets/Scripts/UI/PlayerInfoWnd/CPlayerTalismanInfoWnd")
require ("auto_transform/Assets/Scripts/UI/PlayerInfoWnd/CPlayerTalismanItemCell")
require ("auto_transform/Assets/Scripts/UI/PlayerMainStoryQuestion/CPlayerMainStoryQuestionWnd")
require ("auto_transform/Assets/Scripts/UI/PlayerMainStoryQuestion/CPlayerMainStoryShareWnd")
require ("auto_transform/Assets/Scripts/UI/PlayerProDif/CPlayerProDifShowItem")
require ("auto_transform/Assets/Scripts/UI/PlayerProDif/CPlayerProDifShowWnd")
require ("auto_transform/Assets/Scripts/UI/QingQiu/CQingQiuBaoWeiStartWnd")
require ("auto_transform/Assets/Scripts/UI/QingQiu/CQingQiuExchangeWnd")
require ("auto_transform/Assets/Scripts/UI/QingQiu/CQingQiuGiftWnd")
require ("auto_transform/Assets/Scripts/UI/QingQiu/CQingQiuMgr")
require ("auto_transform/Assets/Scripts/UI/QingQiu/CQingQiuQiYuWnd")
require ("auto_transform/Assets/Scripts/UI/QingQiu/CQingQiuScoreDetailView")
require ("auto_transform/Assets/Scripts/UI/QingQiu/CQingQiuScoreItem")
require ("auto_transform/Assets/Scripts/UI/QingQiu/CQingQiuScoreRankWnd")
require ("auto_transform/Assets/Scripts/UI/QingQiu/CQingQiuShiJianWnd")
require ("auto_transform/Assets/Scripts/UI/QingQiu/CQingQiuSituationDetailView")
require ("auto_transform/Assets/Scripts/UI/QingQiu/CQingQiuZhanBuWnd")
require ("auto_transform/Assets/Scripts/UI/QingYuan/CQingYuanRecommendItem")
require ("auto_transform/Assets/Scripts/UI/QingYuan/CQingYuanRecommendWnd")
require ("auto_transform/Assets/Scripts/UI/QiXi/CQueQiaoEventWnd")
require ("auto_transform/Assets/Scripts/UI/QiXi/CQueQiaoPoemWnd")
require ("auto_transform/Assets/Scripts/UI/PersonalSpace/CPersonalSpaceSendMomentWnd")
-- require ("auto_transform/Assets/Scripts/Game/House/CClientFurnitureMgr")
require ("auto_transform/Assets/Scripts/UI/Main/Base/TaskAndTeam/CTaskListItem")
require ("auto_transform/Assets/Scripts/UI/Main/Package/CPackagePlayerEquipmentView")
require ("auto_transform/Assets/Scripts/UI/Main/Base/TaskAndTeam/CTaskAndTeamWnd")
require ("auto_transform/Assets/Scripts/UI/Main/MainPlayerWnd/CPlayerPropertyView")
require ("auto_transform/Assets/Scripts/UI/Main/MainPlayerWnd/CMainPlayerEquipmentFrame")

require ("auto_transform/Assets/Scripts/Game/Precious/CPreciousMgr")

-- require ("auto_transform/Assets/Scripts/UI/Equipment/Baptize/CEquipWordAutoBaptizeWnd")
require ("auto_transform/Assets/Scripts/UI/Equipment/Baptize/CEquipWordBaptizePane")
require ("auto_transform/Assets/Scripts/UI/Equipment/CAutoBaptizeConditionItem")
-- require ("auto_transform/Assets/Scripts/UI/Equipment/Baptize/CEquipWordAutoBaptizeConditionWnd")

--打造
require ("auto_transform/Assets/Scripts/UI/Equipment/Baptize/CEquipBaptizeDescItem")
require ("auto_transform/Assets/Scripts/UI/Equipment/Baptize/CEquipBaptizeDetailView")
-- require ("auto_transform/Assets/Scripts/UI/Equipment/Baptize/CEquipChouBingWnd")
--NO require ("auto_transform/Assets/Scripts/UI/Equipment/IntensifySwitch/CEquipIntensifySwitchMasterView")
require ("auto_transform/Assets/Scripts/UI/Equipment/EquipProcessWnd/CItemComposeWnd")
require ("auto_transform/Assets/Scripts/UI/Equipment/Intensify/CEquipIntensifyWnd")
require ("auto_transform/Assets/Scripts/UI/Equipment/Intensify/CEquipAutoIntensifyItem")
require ("auto_transform/Assets/Scripts/UI/Equipment/Intensify/CEquipHandIntensifyItem")
require ("auto_transform/Assets/Scripts/UI/Equipment/Intensify/CEquipHandIntensifyWnd")
require ("auto_transform/Assets/Scripts/UI/Equipment/Intensify/CEquipIntensifyBaseItem")
require ("auto_transform/Assets/Scripts/UI/Equipment/Intensify/CEquipIntensifyCost")
require ("auto_transform/Assets/Scripts/UI/Equipment/Intensify/CEquipIntensifyDetailView")
require ("auto_transform/Assets/Scripts/UI/Equipment/Baptize/CEquipBaptizeAttributeItem")
require ("auto_transform/Assets/Scripts/UI/Equipment/Baptize/CEquipBaptizeJadeCost")
require ("auto_transform/Assets/Scripts/UI/Equipment/Baptize/CEquipBaptizeJingPing")
require ("auto_transform/Assets/Scripts/UI/Equipment/Baptize/CEquipBaseBaptizeNianZhuCost")
require ("auto_transform/Assets/Scripts/UI/Equipment/Baptize/CEquipBaseBaptizePane")
require ("auto_transform/Assets/Scripts/UI/Equipment/Baptize/CEquipChouBingCost")
require ("auto_transform/Assets/Scripts/UI/Equipment/Baptize/CEquipChouBingPane")

require ("auto_transform/Assets/Scripts/UI/DataSheet/CUIDataSheetRow")
require ("auto_transform/Assets/Scripts/UI/House/CHousePopupMenu")
require ("auto_transform/Assets/Scripts/UI/Common/NewUI/CShopMallItemPreviewer")
require ("auto_transform/Assets/Scripts/UI/House/CHouseMsgItemWnd")
require ("auto_transform/Assets/Scripts/UI/Main/Package/CPackageItemCell")
-- require ("auto_transform/Assets/Scripts/Game/Freight/CFreightMgr")
require ("auto_transform/Assets/Scripts/UI/Login/CServerWnd")
require ("auto_transform/Assets/Scripts/UI/HideAndSeek/CHideAndSeekStartWnd")
require ("auto_transform/Assets/Scripts/Game/Object/Equipment/CEquipmentProcessMgr")
require ("ui/yuanxiao2021/LuaGamePlayHpMgr")
require ("ui/CUIManager")
require ("auto_transform/Assets/Scripts/UI/Main/Base/CJoystickWnd")
require ("auto_transform/Assets/Scripts/UI/Main/Base/MainPlayerFrame")
require ("auto_transform/Assets/Scripts/UI/Main/GuanNing/CCenterCountdownWnd")
require ("ui/main/LuaPackageMoneyInfoView")
require ("auto_transform/Assets/Scripts/UI/CClientObjInteractionWnd")