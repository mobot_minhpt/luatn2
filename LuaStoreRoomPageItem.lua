local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UIGrid = import "UIGrid"
local EnumWarehouseItemCellLockStatus = import "L10.UI.EnumWarehouseItemCellLockStatus"

local CWarehousePackageItemCell = import "L10.UI.CWarehousePackageItemCell"

LuaStoreRoomPageItem = class()

--@region RegistChildComponent: Dont Modify Manually!

--@endregion RegistChildComponent end
RegistChildComponent(LuaStoreRoomPageItem, "ItemCell", "ItemCell", GameObject)
RegistClassMember(LuaStoreRoomPageItem,"mGrid")

function LuaStoreRoomPageItem:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self:InitClassMerber()
end

function LuaStoreRoomPageItem:Init()

end

function LuaStoreRoomPageItem:InitClassMerber()
    self.mGrid = self.transform:Find("Grid"):GetComponent(typeof(UIGrid))
end

function LuaStoreRoomPageItem:InitPage(pageIndex)--0 1 2

    Extensions.RemoveAllChildren(self.mGrid.transform)

    local setting = StoreRoom_Setting.GetData()
    local roomCount = setting.RoomPosCountPerPage
    local totalUnlockCount = LuaStoreRoomMgr:GetTotalValidStoreRoomPosCount()
    local curPageUnlockCount = totalUnlockCount - (pageIndex * roomCount)
	for i=1,roomCount,1 do
		local go = CUICommonDef.AddChild(self.mGrid.gameObject, self.ItemCell)
        go:SetActive(true)
        local cell = CommonDefs.GetComponent_GameObject_Type(go, typeof(CWarehousePackageItemCell))
        local itemId = nil
        if i <= curPageUnlockCount then--unlock
            isLocked = false
            cell:Init(itemId, EnumWarehouseItemCellLockStatus.Unlocked)
        else--lock
            isLocked = true
            cell:Init(itemId, EnumWarehouseItemCellLockStatus.Locked)
        end
        cell.OnItemClickDelegate = DelegateFactory.Action_CWarehousePackageItemCell(
				function(item_cell)
					self:ItemClick(item_cell,isLocked,i-1)
				end
			)
		cell.OnItemDoubleClickDelegate = DelegateFactory.Action_CWarehousePackageItemCell(
				function(item_cell)
					self:OnItemDoubleClick(item_cell,isLocked,i-1)
				end
		)
		
	end

	self.mGrid:Reposition()
end

function LuaStoreRoomPageItem:InitItem(item,isLock)

end

function LuaStoreRoomPageItem:ItemClick(item_cell,isLocked,pos)
    print("click",isLocked)
    if isLocked then
        --打开解锁界面
        LuaStoreRoomMgr:OpenUnlockPosWnd()
    else
        --取出Tip
    end
end

function LuaStoreRoomPageItem:OnItemDoubleClick(item_cell,isLocked,pos)
    print("double click",isLocked)
    if isLocked then
        return
    end
    Gac2Gas.MoveItemFromStoreRoom2Bag(pos,item_cell.ItemId)
end


--@region UIEvent

--@endregion UIEvent

