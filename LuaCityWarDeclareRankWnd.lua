local UIGrid = import "UIGrid"
local Extensions = import "Extensions"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local NGUITools = import "NGUITools"
local Constants = import "L10.Game.Constants"

CLuaCityWarDeclareRankWnd = class()
CLuaCityWarDeclareRankWnd.Path = "ui/citywar/LuaCityWarDeclareRankWnd"

RegistClassMember(CLuaCityWarDeclareRankWnd, "m_Tick")

function CLuaCityWarDeclareRankWnd:Awake()
	UIEventListener.Get(self.transform:Find("Info/TipBtn").gameObject).onClick = LuaUtils.VoidDelegate(function()
		g_MessageMgr:ShowMessage("CITYWAR_XUANZHAN_QUEUE_INTERFACE_HELP_TIP")
	end)
end

function CLuaCityWarDeclareRankWnd:Init()
	self.transform:Find("Info/TopTipLabel"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("CITYWAR_XUANZHAN_QUEUE_INTERFACE_TOP_TIP", #CLuaCityWarMgr.PriorityMatchGroupDataTable, CLuaCityWarMgr.PriorityMatchGuildName)
	if self.m_Tick then
		UnRegisterTick(self.m_Tick)
		self.m_Tick = nil
	end
	local label = self.transform:Find("Info/BottomLabel"):GetComponent(typeof(UILabel))
	local time = CLuaCityWarMgr.PriorityMatchLeftTime
	label.text = LocalString.GetString("宣战排队倒计时 ")..SafeStringFormat3(LocalString.GetString("%02d:%02d"), math.floor(time / 60), math.floor(time % 60))
	self.m_Tick = RegisterTickWithDuration(function()
		time = time - 1
		label.text = LocalString.GetString("宣战排队倒计时 ")..SafeStringFormat3(LocalString.GetString("%02d:%02d"), math.floor(time / 60), math.floor(time % 60))
	end, 1000, time * 1000)
	self.transform:Find("Info/OpBtn/Label"):GetComponent(typeof(UILabel)).text = CLuaCityWarMgr.PriorityMatchInGroup and LocalString.GetString("取消宣战") or LocalString.GetString("发起宣战")
	UIEventListener.Get(self.transform:Find("Info/OpBtn").gameObject).onClick = LuaUtils.VoidDelegate(function()
		if CLuaCityWarMgr.PriorityMatchInGroup then
			Gac2Gas.CancelMirrorWar(CLuaCityWarMgr.PriorityMatchGuildId)
		else
			Gac2Gas.RequestDeclareMirrorWarInfo(CLuaCityWarMgr.PriorityMatchGuildId, CLuaCityWarMgr.PriorityMatchGuildName, CLuaCityWarMgr.PriorityMatchMapId, CLuaCityWarMgr.PriorityMatchTempalteId)
		end
	end)
	self:InitData()
end

function CLuaCityWarDeclareRankWnd:InitData()
	local templateObj = self.transform:Find("Rank/AdvView/Pool/Template").gameObject
	templateObj:SetActive(false)
	local grid = self.transform:Find("Rank/AdvView/Scroll View/Grid"):GetComponent(typeof(UIGrid))
	Extensions.RemoveAllChildren(grid.transform)
	for i = 1, #CLuaCityWarMgr.PriorityMatchGroupDataTable do
		local obj = NGUITools.AddChild(grid.gameObject, templateObj)
		obj:SetActive(true)
		self:InitItem(obj.transform, CLuaCityWarMgr.PriorityMatchGroupDataTable[i] , i)
	end
	grid:Reposition()
end

function CLuaCityWarDeclareRankWnd:InitItem(rootTrans, data, index)
	local transNameTbl = {"GuildNameLabel", "ServerNameLabel", "GuildGradeLabel", "GuildPeopleLabel", "ProgressLabel"}
	local valueTbl = {data.GuildName, data.ServerName, data.Score, CityWar_Map.GetData(data.MapId).Name..LocalString.GetString("主城"), data.MapLevel}
	for i = 1, #transNameTbl do
		rootTrans:Find(transNameTbl[i]):GetComponent(typeof(UILabel)).text = valueTbl[i]
	end

	rootTrans:GetComponent(typeof(UISprite)).spriteName = index % 2 == 0 and Constants.GreyItemBgSpriteName or Constants.WhiteItemBgSpriteName

	local rankSprite = rootTrans:Find("RankSprite"):GetComponent(typeof(UISprite))
	rankSprite.gameObject:SetActive(index <= 1)
	if index > 1 then
		rootTrans:Find("RankLabel"):GetComponent(typeof(UILabel)).text = index
	else
		local spriteNames = {"Rank_No.1", "Rank_No.2png", "Rank_No.3png"}
		rankSprite.spriteName = spriteNames[index]
	end
end


function CLuaCityWarDeclareRankWnd:OnDestroy()
	if self.m_Tick then
		UnRegisterTick(self.m_Tick)
		self.m_Tick = nil
	end
end
