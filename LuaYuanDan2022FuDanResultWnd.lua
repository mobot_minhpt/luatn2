local GameObject = import "UnityEngine.GameObject"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
LuaYuanDan2022FuDanResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaYuanDan2022FuDanResultWnd, "Template1", "Template1", GameObject)
RegistChildComponent(LuaYuanDan2022FuDanResultWnd, "Template2", "Template2", GameObject)
RegistChildComponent(LuaYuanDan2022FuDanResultWnd, "Template3", "Template3", GameObject)
RegistChildComponent(LuaYuanDan2022FuDanResultWnd, "Template4", "Template4", GameObject)
RegistChildComponent(LuaYuanDan2022FuDanResultWnd, "WinSprite2", "WinSprite2", CUITexture)

--@endregion RegistChildComponent end

function LuaYuanDan2022FuDanResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaYuanDan2022FuDanResultWnd:Init()
    self:InitTemplate(self.Template1, LuaYuanDan2022Mgr.m_FuDanPlayResultData[1])
    self:InitTemplate(self.Template2, LuaYuanDan2022Mgr.m_FuDanPlayResultData[2])
    self:InitTemplate(self.Template3, LuaYuanDan2022Mgr.m_FuDanPlayResultData[3])
    self:InitTemplate(self.Template4, LuaYuanDan2022Mgr.m_FuDanPlayResultData[4])
    local ret = LuaYuanDan2022Mgr.m_FuDanPlayResult
    Extensions.SetLocalPositionZ(self.WinSprite2.transform, ret == EnumPlayResult.eLose and -1 or 0)
    self.WinSprite2:LoadMaterial(SafeStringFormat3("UI/Texture/Transparent/Material/liuyipengpengche_%s.mat",ret == EnumPlayResult.eDraw and "ping" or (ret == EnumPlayResult.eVictory and "sheng" or "bai")))

    if CommonDefs.IS_HMT_CLIENT or CommonDefs.IS_VN_CLIENT then
        self.transform:Find("Anchor/Bg/Label").gameObject:SetActive(false)
    end
end

--@region UIEvent

--@endregion UIEvent
function LuaYuanDan2022FuDanResultWnd:InitTemplate(go,data)
    local portrait = go.transform:Find("Portrait"):GetComponent(typeof(CUITexture))
    local titleLabel = go.transform:Find("TitleLabel"):GetComponent(typeof(UILabel))
    local nameLabel = go.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local desLabel = go.transform:Find("DesLabel"):GetComponent(typeof(UILabel))
    portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(data.class, data.gender, -1))
    titleLabel.text = data.isPicker and LocalString.GetString("【孵蛋手】") or LocalString.GetString("【捣蛋手】")
    nameLabel.text = data.name
    desLabel.text = data.isPicker and SafeStringFormat3(LocalString.GetString("成功孵化福蛋%d个"),data.eggs) or 
        SafeStringFormat3(LocalString.GetString("有效干扰对手%d次"),data.interrupt)
end
