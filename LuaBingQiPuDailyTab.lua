local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UITabBar = import "L10.UI.UITabBar"
local UILabel = import "UILabel"
local CCommonSelector = import "L10.UI.CCommonSelector"
local GameObject = import "UnityEngine.GameObject"
local UISprite = import "UISprite"
local MessageMgr = import "L10.Game.MessageMgr"
local AlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local EnumQualityType = import "L10.Game.EnumQualityType"
local QualityColor = import "L10.Game.QualityColor"
local DateTime = import "System.DateTime"
local CultureInfo = import "System.Globalization.CultureInfo"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaBingQiPuDailyTab = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaBingQiPuDailyTab, "SubTabs", "SubTabs", UITabBar)
RegistChildComponent(LuaBingQiPuDailyTab, "StageLabel", "StageLabel", UILabel)
RegistChildComponent(LuaBingQiPuDailyTab, "DropdownBtn", "DropdownBtn", CCommonSelector)
RegistChildComponent(LuaBingQiPuDailyTab, "NoDailyHint", "NoDailyHint", UILabel)
RegistChildComponent(LuaBingQiPuDailyTab, "Item1", "Item1", GameObject)
RegistChildComponent(LuaBingQiPuDailyTab, "Item2", "Item2", GameObject)
RegistChildComponent(LuaBingQiPuDailyTab, "Item3", "Item3", GameObject)
RegistChildComponent(LuaBingQiPuDailyTab, "SponsorBtn", "SponsorBtn", UISprite)
RegistChildComponent(LuaBingQiPuDailyTab, "ChangeBtn", "ChangeBtn", UISprite)
RegistChildComponent(LuaBingQiPuDailyTab, "CommitBtn", "CommitBtn", UISprite)
RegistChildComponent(LuaBingQiPuDailyTab, "RuleBtn", "RuleBtn", UISprite)
RegistChildComponent(LuaBingQiPuDailyTab, "HintLabel", "HintLabel", UILabel)

--@endregion RegistChildComponent end

RegistClassMember(LuaBingQiPuDailyTab,"m_TabIndex")
RegistClassMember(LuaBingQiPuDailyTab,"m_CurEquipType")
RegistClassMember(LuaBingQiPuDailyTab,"m_EquipTypes")
RegistClassMember(LuaBingQiPuDailyTab, "m_DailyInfo")

function LuaBingQiPuDailyTab:Awake()
    --@region EventBind: Dont Modify Manually!
	self.SubTabs.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
	    self:OnSubTabsTabChange(index)
	end)

	UIEventListener.Get(self.SponsorBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSponsorBtnClick()
	end)

	UIEventListener.Get(self.ChangeBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChangeBtnClick()
	end)

	UIEventListener.Get(self.CommitBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCommitBtnClick()
	end)

	UIEventListener.Get(self.RuleBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleBtnClick()
	end)

    --@endregion EventBind end

	self.m_EquipTypes={}
	table.insert(self.m_EquipTypes,0)
	table.insert(self.m_EquipTypes,1)
	local menuList = CreateFromClass(MakeGenericClass(List, String))
	CommonDefs.ListAdd(menuList,typeof(String),LocalString.GetString("全部类型"))

	CommonDefs.ListAdd(menuList,typeof(String),LocalString.GetString("武器"))
	BingQiPu_EquipType.Foreach(function (key,value)
		if value.FstTab ~= 1 then
			table.insert(self.m_EquipTypes,value.FstTab)
			CommonDefs.ListAdd(menuList, typeof(String), value.SecTabName)
		end
	end)
	self.DropdownBtn:SetColumCount(2)
	self.DropdownBtn:SetPopupAlignType(AlignType.Top)
	self.DropdownBtn:SetMenuItemStyle(EnumPopupMenuItemStyle.Light)
	self.DropdownBtn:Init(menuList, DelegateFactory.Action_int(function ( index )
        local menuName = menuList[index]
        self.DropdownBtn:SetName(menuName)
		self:OnSelectEquipType(index)
    end))
end

function LuaBingQiPuDailyTab:Init()
	self.m_TabIndex = 0
	self.m_CurEquipType = 0
	self.SubTabs:ChangeTab(0,true)

	self:RequireRecommendData()
	
	LuaBingQiPuMgr.QueryBQPPraiseCount()
	LuaBingQiPuMgr.QueryBQPStage()
	LuaBingQiPuMgr.QueryBQPSubmitted()
end

function LuaBingQiPuDailyTab:RequireRecommendData()
	local isPrecious = self.m_TabIndex == 1
	local equipType = self.m_CurEquipType
	LuaBingQiPuMgr.QueryBQPRecommendEquip(isPrecious,equipType)
end

function LuaBingQiPuDailyTab:OnEnable()
	g_ScriptEvent:AddListener("UpdateBQPDaily", self, "UpdateBQPDaily")
	g_ScriptEvent:AddListener("UpdateBQPStatus",self, "UpdateBQPStatus")
	g_ScriptEvent:AddListener("BQPPraiseDone",self, "BQPPraiseDone")
end

function LuaBingQiPuDailyTab:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateBQPDaily", self, "UpdateBQPDaily")
	g_ScriptEvent:RemoveListener("UpdateBQPStatus",self, "UpdateBQPStatus")
	g_ScriptEvent:RemoveListener("BQPPraiseDone",self, "BQPPraiseDone")
end

function LuaBingQiPuDailyTab:BQPPraiseDone(equipId)
	local items = {self.Item1,self.Item2,self.Item3}
	for i=1,#items do
		if i<= #self.m_DailyInfo then
			local data = self.m_DailyInfo[i]
			if data.equipId == equipId then
				data.isPraised = 1
				self:InitItem(items[i],data)
				local Fx = items[i].transform:Find("EquipScore/Fx"):GetComponent(typeof(CUIFx))
				Fx.gameObject:SetActive(true)
				Fx:LoadFx("fx/ui/prefab/UI_bingqipu_dianzan.prefab")
			end
		end
	end
	self:UpdatePrise()
end

function LuaBingQiPuDailyTab:UpdateBQPDaily()
	if self.m_TabIndex == 0 then
		self.m_DailyInfo = LuaBingQiPuMgr.m_DailyInfo
	elseif self.m_TabIndex == 1 then
		self.m_DailyInfo = LuaBingQiPuMgr.m_DailyInfo2 --infos元素结构equipId,templateId,tag1,isPraised,score,ownerId,ownerName,equip(CCommonItem)
	end
	self:UpdateItems()
end

function LuaBingQiPuDailyTab:UpdateBtns()
	if LuaBingQiPuMgr.m_StageStatus ~= EnumBingQiPuStage.eSubmit then
		if self:HaveCommitedEquip() then--如果有参选装备
			local lb = LuaGameObject.GetChildNoGC(self.CommitBtn.transform,"Label").label
			lb.text = LocalString.GetString("我的参选")
			self.CommitBtn.gameObject:SetActive(true)
			self.SponsorBtn.gameObject:SetActive(true)
		else
			self.CommitBtn.gameObject:SetActive(false)
			self.SponsorBtn.gameObject:SetActive(false)
		end
	else
		local lb = LuaGameObject.GetChildNoGC(self.CommitBtn.transform,"Label").label
		lb.text = LocalString.GetString("登记兵器")
		self.CommitBtn.gameObject:SetActive(true)
		self.SponsorBtn.gameObject:SetActive(true)
	end
end

function LuaBingQiPuDailyTab:UpdateItems()
	--local datas = self:GetCurEquipTypeDatas()
	self:ShowItems(self.m_DailyInfo)
end

function LuaBingQiPuDailyTab:ShowItems(datas)
	local items = {self.Item1,self.Item2,self.Item3}
	for i = 1,3 do
		local ctrl = items[i]
		if i > #datas then 
			ctrl:SetActive(false)
		else
			self:InitItem(ctrl,datas[i])
			ctrl:SetActive(true)
		end
	end
	self.NoDailyHint.gameObject:SetActive(#datas == 0)
	if #datas < 3 then
		g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("没有更多推荐的兵器啦，请稍后再进行尝试"))
	end
end

function LuaBingQiPuDailyTab:InitItem(go,info)
	local ItemNameLabel = go.transform:Find("EquipInfo/ItemNameLabel"):GetComponent(typeof(UILabel))
	local AvailableLevelLabel = go.transform:Find("EquipInfo/AvailableLevelLabel"):GetComponent(typeof(UILabel))
	local CategoryLabel = go.transform:Find("EquipInfo/CategoryLabel"):GetComponent(typeof(UILabel))

	local EquipIconTexture_WA = go.transform:Find("BigIcon/IconTexture_WA"):GetComponent(typeof(CUITexture))
	local EquipIconTexture_Others = go.transform:Find("BigIcon/IconTexture_Others"):GetComponent(typeof(CUITexture))

	local EquipScore = go.transform:Find("EquipScore/Label"):GetComponent(typeof(UILabel))

	local OwnerName = go.transform:Find("OwnerName"):GetComponent(typeof(UILabel))
	local Tag1 = go.transform:Find("Tag1/Label"):GetComponent(typeof(UILabel))

	local ChatBtn = go.transform:Find("ChatButton")
	local MoreBtn = go.transform:Find("MoreBtn")

	local Fx = go.transform:Find("EquipScore/Fx"):GetComponent(typeof(CUIFx))

	local template = EquipmentTemplate_Equip.GetData(info.templateId)
	if not template then return end

	-- 名称
	if String.IsNullOrEmpty(info.equipName) then
		ItemNameLabel.text = template.Name
	else
		ItemNameLabel.text = info.equipName
	end
	ItemNameLabel.color = QualityColor.GetRGBValue(CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), template.Color))

	-- 等级
	local levelstr = ""
    local level = 0
    if CClientMainPlayer.Inst ~= nil then
        level = CClientMainPlayer.Inst.FinalMaxLevelForEquip
    end
    if template.Grade == 0 then
        levelstr = ""
    elseif template.Grade > level then
        levelstr = System.String.Format(LocalString.GetString("[c][FF0000]{0}级[-][/c]  "), template.Grade)
    else
        levelstr = System.String.Format(LocalString.GetString("[c][FFFFFF]{0}级[-][/c]  "), template.Grade)
    end

    -- 类型
	local typestr = ""
    local subTypeTemplate = EquipmentTemplate_SubType.GetData(template.Type * 100 + template.SubType)
    local blankSpace = CommonDefs.IS_VN_CLIENT and " " or ""
    if subTypeTemplate ~= nil then
    	if template.Type == EnumBodyPosition_lua.Weapon then
            local default
            if template.BothHand == 1 then
                default = LocalString.GetString("双手")
            else
                default = LocalString.GetString("单手")
            end
            typestr = System.String.Format("{0}{1}({2})", subTypeTemplate.Name, blankSpace, default)
        else
            typestr = subTypeTemplate.Name
        end
    end
	AvailableLevelLabel.text = levelstr..typestr

    -- 装备大图标
	local icon = template.BigIcon
	if info.isShenBing then
		local iconData = ShenBing_EquipIcon.GetData(template.ShenBingType)
        if iconData then
            icon = iconData.BigIcon
		end
	end
	if template.Type == EnumBodyPosition_lua.Weapon or template.Type == EnumBodyPosition_lua.Armour or template.Type == EnumBodyPosition_lua.Shield then
		EquipIconTexture_WA:LoadMaterial(icon)
		EquipIconTexture_WA.gameObject:SetActive(true)
		EquipIconTexture_Others.gameObject:SetActive(false)
	else
		EquipIconTexture_Others:LoadMaterial(icon)
		EquipIconTexture_Others.gameObject:SetActive(true)
		EquipIconTexture_WA.gameObject:SetActive(false)
	end

	local onClickIcon = function (go)
    	LuaBingQiPuMgr.QueryBQPDetailInfo(info.equipId)
    end
	CommonDefs.AddOnClickListener(MoreBtn.gameObject,DelegateFactory.Action_GameObject(onClickIcon),false)
    CommonDefs.AddOnClickListener(EquipIconTexture_WA.gameObject,DelegateFactory.Action_GameObject(onClickIcon),false)
    CommonDefs.AddOnClickListener(EquipIconTexture_Others.gameObject,DelegateFactory.Action_GameObject(onClickIcon),false)

	-- 总评分
    EquipScore.text = SafeStringFormat3(LocalString.GetString("人气  %d"), info.score)

    -- 玩家名称
    OwnerName.text = SafeStringFormat3(LocalString.GetString("[ACF9FF]主人[-]  %s"), info.ownerName)

	-- 标签
	if self.m_TabIndex == 0 then
    	local tag1Data = BingQiPu_EquipmentTag.GetData(info.tag1)
		if tag1Data then
    		Tag1.text =  tag1Data.VoteNum
		else
			Tag1.text = ""
		end
	else
		local tag1Data = BingQiPu_TreasureTag.GetData(info.tag1)
		if tag1Data then
    		Tag1.text = tag1Data.TreasureVoteNum 
		else
			Tag1.text = ""
		end
	end

	--点赞按钮
	local chatbtnmask = LuaGameObject.GetChildNoGC(ChatBtn,"SpriteMask").gameObject
	chatbtnmask:SetActive(info.isPraised > 0)
	Fx.gameObject:SetActive(false)
	local onPraisebtn = function (go)
		if info.isPraised > 0 then
			g_MessageMgr:ShowMessage("BQP_PRAISE_NO_TIMES")
			return 
		end
		if LuaBingQiPuMgr.m_LikesLeft <= 0 then 
			g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("点赞次数不足"))
			return 
		end
		LuaBingQiPuMgr.QueryBQPPraise(info.equipId,info.templateId,true)
	end
	CommonDefs.AddOnClickListener(ChatBtn.gameObject,DelegateFactory.Action_GameObject(onPraisebtn),false)
end

function LuaBingQiPuDailyTab:UpdateBQPStatus()
	self:UpdateBtns()
	self:UpdateDes()
	self:UpdatePrise()
end

function LuaBingQiPuDailyTab:UpdatePrise()
	self.HintLabel.text =  LuaBingQiPuMgr.GetLikeStr()
end

function LuaBingQiPuDailyTab:UpdateDes()
	local setting = BingQiPu_Setting.GetData()
	local cult = CultureInfo.CurrentCulture
	local nowTime = CServerTimeMgr.Inst:GetZone8Time()

	if LuaBingQiPuMgr.m_StageStatus == EnumBingQiPuStage.eSubmit then
		local commitStr = setting.commitPeriod
		local commitTime = CommonDefs.StringSplit_ArrayChar(commitStr, ";")
		local commitStartTime = DateTime.ParseExact(commitTime[0], "yyyy-MM-dd", cult)
		local commitEndTime = DateTime.ParseExact(commitTime[1], "yyyy-MM-dd", cult)
		local str = LocalString.GetString("兵器[00FF00]登记中（%s-%s）[-]")
		local st = commitStartTime:ToString("MM.dd")
		local ed = commitEndTime:ToString("MM.dd")
		self.StageLabel.text = SafeStringFormat3(str, st, ed)
	elseif LuaBingQiPuMgr.m_StageStatus == EnumBingQiPuStage.eVote then
		local voteStr = setting.votePeriod
		local voteTime = CommonDefs.StringSplit_ArrayChar(voteStr, ";")
		local voteStartTime = DateTime.ParseExact(voteTime[0], "yyyy-MM-dd", cult)
		local voteEndTime = DateTime.ParseExact(voteTime[1], "yyyy-MM-dd", cult)
		local str = LocalString.GetString("兵器[00FF00]评选中（%s-%s）[-]")
		local st = voteStartTime:ToString("MM.dd")
		local ed = voteEndTime:ToString("MM.dd")
		self.StageLabel.text = SafeStringFormat3(str, st, ed)
	--elseif LuaBingQiPuMgr.m_StageStatus == EnumBingQiPuStage.eClose then
	else
		self.StageLabel.text = ""
	end
end

function LuaBingQiPuDailyTab:ShowEquipInfoWnd()
	CUIManager.ShowUI(CLuaUIResources.BQPEquipInfoWnd)
end

function LuaBingQiPuDailyTab:OnSelectEquipType(idx)
	local type = self.m_EquipTypes[idx+1]
	if self.m_CurEquipType == type then return end
	self.m_CurEquipType = type
	self:RequireRecommendData()
end

function LuaBingQiPuDailyTab:HaveCommitedEquip()
	if self.m_TabIndex == 0 and #LuaBingQiPuMgr.m_EquipsCommited > 0 then
		return true
	end
	return false
end

function LuaBingQiPuDailyTab:PriseEquip(equipId)
	if self.m_DailyInfo == nil then return end 
	for i=1,#self.m_DailyInfo do
		if self.m_DailyInfo[i].equipId == equipId then 
			self.m_DailyInfo[i].isPraised = true

		end
	end
end
--[[
function LuaBingQiPuDailyTab:GetCurEquipTypeDatas()
	local datas = {}
	for i=1,#self.m_DailyInfo do
		local data = self.m_DailyInfo[i]
		local tid = data.templateId
		local tdata = EquipmentTemplate_Equip.GetData(tid)
		if tdata then
			local type = tdata.Type
			if type == self.m_CurEquipType or self.m_CurEquipType == 0 then
				table.insert(datas,data)
			end
		end
	end
	return datas
end
]]
--@region UIEvent

function LuaBingQiPuDailyTab:OnSubTabsTabChange(index)
	self.m_TabIndex = index
	if self.m_TabIndex == 0 then
		if LuaBingQiPuMgr.m_DailyInfo == nil then
			self:RequireRecommendData()
		else
			self.m_DailyInfo = LuaBingQiPuMgr.m_DailyInfo
			self:UpdateItems()
		end
	end
	if self.m_TabIndex == 1 then
		if LuaBingQiPuMgr.m_DailyInfo2 == nil then
			self:RequireRecommendData()
		else
			self.m_DailyInfo = LuaBingQiPuMgr.m_DailyInfo2
			self:UpdateItems()
		end
	end

	self.DropdownBtn.gameObject:SetActive(self.m_TabIndex == 0)
	self:UpdateBtns()
end

function LuaBingQiPuDailyTab:OnSponsorBtnClick()
	Gac2Gas.QuerySponsorRank()
end

function LuaBingQiPuDailyTab:OnChangeBtnClick()
	self:RequireRecommendData()
end

function LuaBingQiPuDailyTab:OnCommitBtnClick()
	if LuaBingQiPuMgr.m_StageStatus == EnumBingQiPuStage.eSubmit then
		CUIManager.ShowUI(CLuaUIResources.BQPCommitEquipWnd)
	else
		CUIManager.ShowUI(CLuaUIResources.BQPCommitEquipWnd)
	end
end

function LuaBingQiPuDailyTab:OnRuleBtnClick()
	MessageMgr.Inst:ShowMessage("BQP_DAILY_TIPS", {})
end

--@endregion UIEvent

