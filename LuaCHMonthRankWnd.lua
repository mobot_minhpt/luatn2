local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local AlignType = import "CPlayerInfoMgr+AlignType"
local CButton = import "L10.UI.CButton"
local Object = import "System.Object"

LuaCHMonthRankWnd = class()

RegistChildComponent(LuaCHMonthRankWnd, "ReceiveTypeBtn", CButton)
RegistChildComponent(LuaCHMonthRankWnd, "GiveTypeBtn", CButton)
RegistChildComponent(LuaCHMonthRankWnd, "TableView", QnAdvanceGridView)
RegistChildComponent(LuaCHMonthRankWnd, "StoreBtn", GameObject)
RegistChildComponent(LuaCHMonthRankWnd, "BottomLabel", UILabel)
RegistChildComponent(LuaCHMonthRankWnd, "RuleBtn", GameObject)
RegistChildComponent(LuaCHMonthRankWnd, "ThirdHeadLabel", UILabel)

RegistClassMember(LuaCHMonthRankWnd, "m_RankTypeBtnTbl")
RegistClassMember(LuaCHMonthRankWnd, "m_RankType")
RegistClassMember(LuaCHMonthRankWnd, "m_RankList")

function LuaCHMonthRankWnd:Awake()
    UIEventListener.Get(self.ReceiveTypeBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnTypeBtnClick(EnumClubHouseRankType.Recv)
    end)
    UIEventListener.Get(self.GiveTypeBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnTypeBtnClick(EnumClubHouseRankType.Give)
    end)

    UIEventListener.Get(self.StoreBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnStoreBtnClick()
    end)
    UIEventListener.Get(self.RuleBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnRuleBtnClick()
    end)
end

function LuaCHMonthRankWnd:Init()
    self.BottomLabel.text = g_MessageMgr:FormatMessage("ClubHouse_Month_Rank_Bottom_Tip")
    self.m_RankList = {}
    -- 设置排名显示
    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(function()
        return #self.m_RankList
    end, function(item, index)
        item:SetBackgroundTexture(index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)
        self:InitItem(item, self.m_RankList[index + 1])
    end)
    -- 设置选中状态，显示玩家信息
    self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function (index)
        local data = self.m_RankList[index + 1]
        if data.AccId then
            local extraInfo = CreateFromClass(MakeGenericClass(Dictionary, String, Object))
            CommonDefs.DictAdd_LuaCall(extraInfo, "AccId", data.AccId)
            CommonDefs.DictAdd_LuaCall(extraInfo, "AppName", data.AppName)
            CPlayerInfoMgr.ShowPlayerPopupMenu(data.PlayerId, EnumPlayerInfoContext.ChatRoom, EChatPanel.Undefined, 
                nil, nil, extraInfo, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
        end
    end)
    -- 不同类型的排行榜
    self.m_RankTypeBtnTbl = {}
    table.insert(self.m_RankTypeBtnTbl, {btn = self.ReceiveTypeBtn, type = EnumClubHouseRankType.Recv})
    table.insert(self.m_RankTypeBtnTbl, {btn = self.GiveTypeBtn, type = EnumClubHouseRankType.Give})
    self:OnTypeBtnClick(EnumClubHouseRankType.Recv)
end

function LuaCHMonthRankWnd:ClubHouse_QueryRank_Result(RankType, RankData_U, MyValue)
    if RankType == self.m_RankType then
        for _, typeInfo in pairs(self.m_RankTypeBtnTbl) do
            typeInfo.btn.Selected = RankType == typeInfo.type
        end
        self.m_RankList = g_MessagePack.unpack(RankData_U) or {}
        local myData = {}
        myData.PlayerId = CClientMainPlayer.Inst.Id
        myData.PlayerName = CClientMainPlayer.Inst.Name
        myData.Value = MyValue
        myData.Rank = 0
        myData.showMyTip = true
        for i, data in pairs(self.m_RankList) do
            self.m_RankList[i].Rank = i
            self.m_RankList[i].showMyTip = false
            if data.PlayerId == myData.PlayerId then
                myData.Rank = i
            end
        end
        table.insert(self.m_RankList, 1, myData)
        self.TableView:ReloadData(true, false)
        self.StoreBtn.gameObject:SetActive(self.m_RankType == EnumClubHouseRankType.Recv)
    end
end

function LuaCHMonthRankWnd:InitItem(item, data)
    -- 排名
    local rankLabel = item.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    local rankImage = item.transform:Find("RankImage"):GetComponent(typeof(UISprite))
    local rankImgList = {g_sprites.RankFirstSpriteName, g_sprites.RankSecondSpriteName, g_sprites.RankThirdSpriteName}
    rankImage.spriteName = rankImgList[data.Rank] 
    rankLabel.text = data.Rank > 0 and tostring(data.Rank) or LocalString.GetString("未上榜")
    -- 其他信息
    item.transform:Find("My").gameObject:SetActive(data.showMyTip)
    local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local valueLabel = item.transform:Find("ValueLabel"):GetComponent(typeof(UILabel))
    nameLabel.color = data.showMyTip and Color.green or Color.white
    valueLabel.color = data.showMyTip and Color.green or Color.white
    nameLabel.text = data.PlayerName
    valueLabel.text = tostring(data.Value)
    -- 排行榜类型图标
    local valueIcon = item.transform:Find("ValueLabel/Icon"):GetComponent(typeof(CUITexture))
    local path = nil
    if self.m_RankType == EnumClubHouseRankType.Give then
        path = "UI/Texture/Transparent/Material/clubhouse_liwuzhi.mat"
        self.ThirdHeadLabel.text = LocalString.GetString("本月赠送积分")
    else
        path = "UI/Texture/Transparent/Material/clubhouse_xinyizhi.mat"
        self.ThirdHeadLabel.text = LocalString.GetString("本月茶室名望度")
    end
    valueIcon:LoadMaterial(path)
end

--@region UIEvent

function LuaCHMonthRankWnd:OnTypeBtnClick(type)
    self.m_RankType = type
    Gac2Gas.ClubHouse_QueryRank(self.m_RankType)
end

function LuaCHMonthRankWnd:OnStoreBtnClick()
	CLuaNPCShopInfoMgr.ShowScoreShop("ClubHouseSYLX")
end

function LuaCHMonthRankWnd:OnRuleBtnClick()
    g_MessageMgr:DisplayMessage("ClubHouse_Month_Rank_Tip")
end

--@endregion UIEvent

function LuaCHMonthRankWnd:OnEnable()
    g_ScriptEvent:AddListener("ClubHouse_QueryRank_Result", self, "ClubHouse_QueryRank_Result")
end

function LuaCHMonthRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ClubHouse_QueryRank_Result", self, "ClubHouse_QueryRank_Result")
end