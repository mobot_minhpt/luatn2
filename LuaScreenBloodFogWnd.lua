local UIPanel = import "UIPanel"
local UITexture = import "UITexture"
local TextureFormat = import "UnityEngine.TextureFormat"
local Texture2D = import "UnityEngine.Texture2D"
local CMainCamera = import "L10.Engine.CMainCamera"
local Screen = import "UnityEngine.Screen"
local UIRoot = import "UIRoot"

LuaScreenBloodFogWnd = class()

RegistChildComponent(LuaScreenBloodFogWnd, "m_Fx","Fx", UITexture)

RegistClassMember(LuaScreenBloodFogWnd,"m_ScreenShot")
RegistClassMember(LuaScreenBloodFogWnd,"m_FxTick")
RegistClassMember(LuaScreenBloodFogWnd,"m_Width")
RegistClassMember(LuaScreenBloodFogWnd,"m_Height")
RegistClassMember(LuaScreenBloodFogWnd,"m_WipedPixelCount")
RegistClassMember(LuaScreenBloodFogWnd,"m_TotalPixelCount")
RegistClassMember(LuaScreenBloodFogWnd,"m_BrushSize")
RegistClassMember(LuaScreenBloodFogWnd,"m_IsStartWipe")
RegistClassMember(LuaScreenBloodFogWnd,"m_TexScaleX")
RegistClassMember(LuaScreenBloodFogWnd,"m_TexScaleY")

function LuaScreenBloodFogWnd:OnEnable()
    self.m_Panel = self.gameObject:GetComponent(typeof(UIPanel))
    self.m_Panel.IgnoreIphoneXMargin = true
end

function LuaScreenBloodFogWnd:Init()
    LuaTweenUtils.TweenFloat(0, 1, 2, function(value)
        self.m_Panel.alpha = value
    end)
    self:CancelFxTick()
    self.m_FxTick = RegisterTickOnce(function()
        self:ScreenShot()
    end, 3000)
    self.m_WipedPixelCount = 0
    self.m_BrushSize = 16
end

function LuaScreenBloodFogWnd:Update()
    if not self.m_IsStartWipe then return end
    if Input.GetMouseButton(0) then
        self:WipePoint(Input.mousePosition)
        if (self.m_WipedPixelCount / self.m_TotalPixelCount) > 0.5 then
            self:FinishTask()
        end
    end
end

function LuaScreenBloodFogWnd:OnDisable()
    self:CancelFxTick()
end

function LuaScreenBloodFogWnd:OnDestroy()
    if self.m_ScreenShot then
        GameObject.Destroy(self.m_ScreenShot)
        self.m_ScreenShot = nil
    end
end

function LuaScreenBloodFogWnd:CancelFxTick()
    if self.m_FxTick then
        UnRegisterTick(self.m_FxTick)
        self.m_FxTick = nil
    end
end

function LuaScreenBloodFogWnd:ScreenShot()
    if CMainCamera.Main then
        local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
        local virtualScreenWidth = Screen.width * CUIManager.UIMainCamera.rect.width * scale
        local virtualScreenHeight = Screen.height * CUIManager.UIMainCamera.rect.height * scale
        self.m_Width = 256
        self.m_Height = 256
        self.m_TexScaleX = virtualScreenWidth / self.m_Width
        self.m_TexScaleY = virtualScreenHeight / self.m_Height
        self.m_TotalPixelCount = self.m_Width * self.m_Height
        self.m_ScreenShot = Texture2D(self.m_Width,  self.m_Height , TextureFormat.ARGB32, false)
        self.m_ScreenShot:SetPixels32(self.m_Fx.mTexture:GetPixels32())
        self.m_ScreenShot:Apply(false, false)
        self.m_Fx.mainTexture = self.m_ScreenShot
    end
    self.m_IsStartWipe = true
end

function LuaScreenBloodFogWnd:WipePoint(screenPos)
    local worldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)
    local localPos = self.m_Fx.transform:InverseTransformPoint(worldPos)
    local halfWith = self.m_Width / 2
    local halfHeight = self.m_Height / 2
    localPos = Vector3(localPos.x / self.m_TexScaleX, localPos.y / self.m_TexScaleY, 0)
    if (localPos.x > -halfWith) and (localPos.x < halfWith) and (localPos.y > - halfHeight) and (localPos.y < halfHeight) then
        for i = localPos.x - self.m_BrushSize, localPos.x + self.m_BrushSize do
            for j = localPos.y - self.m_BrushSize, localPos.y + self.m_BrushSize do
                if (math.pow(i - localPos.x, 2) + math.pow(j - localPos.y, 2)) <= math.pow(self.m_BrushSize, 2) then
                    if ((i + halfWith) < self.m_Width) and ((j + halfHeight) < self.m_Height )then
                        if ((i + halfWith) >= 0) and ((j + halfHeight) >= 0 )then
                            local col = self.m_ScreenShot:GetPixel(i + halfWith, j + halfHeight)
                            if col.a > 0.001 then
                                self.m_WipedPixelCount = self.m_WipedPixelCount + 1
                                col = Color(0,0,0,0)
                                self.m_ScreenShot:SetPixel(i + halfWith, j + halfHeight, col)
                            end
                        end
                    end
                end
            end
        end
    end
    self.m_ScreenShot:Apply(false, false)
end

function LuaScreenBloodFogWnd:FinishTask()
    Gac2Gas.FinishEventTask(CLuaTaskMgr.m_ScreenBloodFogWnd_TaskId,"WipeScreenBloodFog")
    CUIManager.CloseUI(CLuaUIResources.ScreenBloodFogWnd)
end