local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local UISprite = import "UISprite"
local TweenAlpha = import "TweenAlpha"
local UILabel = import "UILabel"
local CUIFx = import "L10.UI.CUIFx"
local TimeEvent = import "L10.Game.TimeEvent"
local GameObject = import "UnityEngine.GameObject"

LuaYingHuoWeiGuangWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaYingHuoWeiGuangWnd, "BlackBg", "BlackBg", UISprite)
RegistChildComponent(LuaYingHuoWeiGuangWnd, "TweenAlpha", "TweenAlpha", TweenAlpha)
RegistChildComponent(LuaYingHuoWeiGuangWnd, "Progress", "Progress", UISprite)
RegistChildComponent(LuaYingHuoWeiGuangWnd, "HintLabel", "HintLabel", UILabel)
RegistChildComponent(LuaYingHuoWeiGuangWnd, "LabelSpawn", "LabelSpawn", TimeEvent)
RegistChildComponent(LuaYingHuoWeiGuangWnd, "LabelTemplate", "LabelTemplate", GameObject)
RegistChildComponent(LuaYingHuoWeiGuangWnd, "LightFx", "LightFx", CUIFx)
RegistChildComponent(LuaYingHuoWeiGuangWnd, "ProgressBG", "ProgressBG", GameObject)
RegistChildComponent(LuaYingHuoWeiGuangWnd, "Light", "Light", GameObject)
RegistChildComponent(LuaYingHuoWeiGuangWnd, "ClickFx", "ClickFx", CUIFx)

--@endregion RegistChildComponent end

RegistClassMember(LuaYingHuoWeiGuangWnd,"m_Width")
RegistClassMember(LuaYingHuoWeiGuangWnd,"m_Height")
RegistClassMember(LuaYingHuoWeiGuangWnd,"m_Time")
RegistClassMember(LuaYingHuoWeiGuangWnd,"m_HintTick")
RegistClassMember(LuaYingHuoWeiGuangWnd,"m_BackTick")
RegistClassMember(LuaYingHuoWeiGuangWnd,"m_Progress")

function LuaYingHuoWeiGuangWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.Light.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLightClick(go)
	end)

    --@endregion EventBind end
end

function LuaYingHuoWeiGuangWnd:Init()
    self.m_Progress = 0
    self.m_Time = Time.time
    local panel = self.gameObject:GetComponent(typeof(UIPanel))
    self.m_Width = panel.width * 0.8
    self.m_Height = panel.height * 0.8

    if CLuaTaskMgr.YingHuoWeiGuangType == 2 then
        self:Complete()
    else
        --未点击计时
        self:StartHintTick()
    end
end

function LuaYingHuoWeiGuangWnd:OnDestroy()
    self:CancelBackTick()
    self:CancelHintTick()
end

function LuaYingHuoWeiGuangWnd:OnDisable()
    self:CancelBackTick()
    self:CancelHintTick()
end

function LuaYingHuoWeiGuangWnd:AddProgress(delta)
    local value = self.m_Progress+delta
    if value > 10 then value = 10 end
    if value < 0 then value = 0 end
    if value == self.m_Progress then return end
    self.m_Progress = value
    self.Progress.fillAmount = self.m_Progress/10

    --萤火大小变化
    local scale = self.Light.transform.localScale
    scale.x = scale.x * 1.1
    scale.y = scale.x
    scale.z = scale.x
    self.Light.transform.localScale = scale

    if delta > 0 then
        self.ClickFx:LoadFx("Fx/UI/Prefab/UI_yinghuochong_dianji.prefab")
        self.ClickFx.gameObject:SetActive(false)
        self.ClickFx.gameObject:SetActive(true)
    end

    if self.m_Progress >= 10 then
        self:Complete()
    end
end

function LuaYingHuoWeiGuangWnd:OnClickFire()
    local time = Time.time
    if time - self.m_Time < 1 then return end
    self.m_Time = time
    self.HintLabel.gameObject:SetActive(false)
    self.ProgressBG:SetActive(true)

    --消息提示计时，提示与否有回溯计时控制
    self:CancelHintTick()

    --开始回溯计时
    self:StartBackTick()

    self:AddProgress(1)
end

function LuaYingHuoWeiGuangWnd:StartBackTick()
    self:CancelBackTick()
    self.m_BackTick = RegisterTick(function()
        if self.m_Progress <= 0 then 
            self:CancelBackTick()
        else
            self:AddProgress(-1)
            self.HintLabel.gameObject:SetActive(true)
        end
    end,3000)
end

function LuaYingHuoWeiGuangWnd:CancelBackTick()
    UnRegisterTick(self.m_BackTick)
    self.m_BackTick = nil
end

function LuaYingHuoWeiGuangWnd:StartHintTick()
    self:CancelHintTick()
    self.m_HintTick = RegisterTickOnce(function()
        self.HintLabel.gameObject:SetActive(true)
        self.ProgressBG:SetActive(true)
    end,5000)
end

function LuaYingHuoWeiGuangWnd:CancelHintTick()
    UnRegisterTick(self.m_HintTick)
    self.m_HintTick = nil
end

--[[
    @desc: 点击成功动画
    author:Codee
    time:2021-10-21 15:59:06
    @return:
]]
function LuaYingHuoWeiGuangWnd:Complete()
    
    --取消回缩计时
    self:CancelBackTick()

    --隐藏进度条和火焰
    self.ProgressBG:SetActive(false)
    self.Light:SetActive(false)
    
    --显示特效LightFx
    self.LightFx.gameObject:SetActive(true)
    
    --背景延迟一段时间后变白色
    local tween = TweenColor.Begin(self.BlackBg.gameObject,0.2,Color.white)
    tween.delay = 1
    local tweenFinish = function()
        self.LightFx.gameObject:SetActive(false)
        --播放文字
        self:StartWordAnim()
    end
    CommonDefs.AddEventDelegate(tween.onFinished, DelegateFactory.Action(tweenFinish))
end

function LuaYingHuoWeiGuangWnd:StartWordAnim()
    local words = {}
    ZhuJueJuQing_Lightning.Foreach(function(k,v)
        if CLuaTaskMgr.YingHuoWeiGuangTaskID == v.TaskID then
            table.insert(words,{Des = v.Gossips,Voice = v.GossipVoice})
        end
    end)

    self.LabelSpawn.OnFinish = DelegateFactory.Action(function()
        local tween = TweenColor.Begin(self.BlackBg.gameObject,0.2,Color.black)
        local tf = function()
            self.LabelSpawn.gameObject:SetActive(false)
            local tween2 = TweenAlpha.Begin(self.BlackBg.gameObject,0.2,0)
            tween2.delay = 2
            local tweenFinish = function()
                Gac2Gas.ClickScreenLightningDone(CLuaTaskMgr.YingHuoWeiGuangTaskID)
                CUIManager.CloseUI(CLuaUIResources.YingHuoWeiGuangWnd)
            end
            CommonDefs.AddEventDelegate(tween2.onFinished, DelegateFactory.Action(tweenFinish))
        end
        CommonDefs.AddEventDelegate(tween.onFinished, DelegateFactory.Action(tf))

    end)

    self.LabelSpawn:DoTime(DelegateFactory.Action_int(function(index)
        self:SpawLabel(index,words)
    end))
end
function LuaYingHuoWeiGuangWnd:SpawLabel(index,words)
    local temp = self.LabelTemplate
        local go = GameObject.Instantiate(temp)
        local trans = go.transform
        trans:SetParent(self.LabelSpawn.transform)
        trans.localScale = Vector3.one
        local label = go:GetComponent(typeof(UILabel))
        local windex = index+1
        if windex > #words then
            windex = math.random(1,#words)
        end
        label.text = words[windex].Des
        if index < 12 then
            SoundManager.Inst:PlayOneShot(words[windex].Voice, Vector3.zero, nil, 0)
        end

        local pos = trans.localPosition
        local rot = trans.localEulerAngles
        pos.x = math.random(-self.m_Width,self.m_Width)
        pos.y = math.random(-self.m_Height,self.m_Height)
        if index < 5 then
            pos.x = pos.x * 0.5
            pos.y = pos.y * 0.5
        end
        pos.z = 0
        rot.x = 0
        rot.y = 0
        rot.z = math.random(-180,180)
        trans.localPosition = pos
        trans.localEulerAngles = rot
        go:SetActive(true)
end

--@region UIEvent

function LuaYingHuoWeiGuangWnd:OnLightClick(go)
    self:OnClickFire()
end

--@endregion UIEvent

