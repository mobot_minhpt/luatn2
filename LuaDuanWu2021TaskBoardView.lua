local CScene = import "L10.Game.CScene"
local CGamePlayMgr = import "L10.Game.CGamePlayMgr"

LuaDuanWu2021TaskBoardView = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaDuanWu2021TaskBoardView, "LeaveBtn", "LeaveBtn", GameObject)
RegistChildComponent(LuaDuanWu2021TaskBoardView, "RuleBtn", "RuleBtn", GameObject)
RegistChildComponent(LuaDuanWu2021TaskBoardView, "TimeLabel", "TimeLabel", UILabel)

--@endregion RegistChildComponent end

function LuaDuanWu2021TaskBoardView:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.LeaveBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeaveBtnClick()
	end)


	
	UIEventListener.Get(self.RuleBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleBtnClick()
	end)


    --@endregion EventBind end
end

function LuaDuanWu2021TaskBoardView:Init()

end

--@region UIEvent

function LuaDuanWu2021TaskBoardView:OnLeaveBtnClick()
    CGamePlayMgr.Inst:LeavePlay()
end

function LuaDuanWu2021TaskBoardView:OnRuleBtnClick()
	LuaDuanWu2021Mgr:ShowOffWorldPlay2TipWnd()
end

--@endregion UIEvent

function LuaDuanWu2021TaskBoardView:OnEnable()
	g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
end

function LuaDuanWu2021TaskBoardView:OnDisable()
	g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
end

function LuaDuanWu2021TaskBoardView:GetRemainTimeText(totalSeconds)
    if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        return SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end

function LuaDuanWu2021TaskBoardView:OnSceneRemainTimeUpdate(args)
    if CScene.MainScene then
        if CScene.MainScene.ShowTimeCountDown then
            self.TimeLabel.text = self:GetRemainTimeText(CScene.MainScene.ShowTime)
            self.TimeLabel.text = self:GetRemainTimeText(CScene.MainScene.ShowTime)
        else
            self.TimeLabel.text = nil
        end
    else
        self.TimeLabel.text = nil
    end
end