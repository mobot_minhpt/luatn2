local GameObject = import "UnityEngine.GameObject"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CButton = import "L10.UI.CButton"
local CAuctionMgr = import "L10.Game.CAuctionMgr"
local CItem = import "L10.Game.CItem"
local QualityColor = import "L10.Game.QualityColor"
local EnumQualityType = import "L10.Game.EnumQualityType"

LuaNewAuctionBidWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaNewAuctionBidWnd, "IconContainer", "IconContainer", CUITexture)
RegistChildComponent(LuaNewAuctionBidWnd, "NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaNewAuctionBidWnd, "AuctionPriceLabel", "AuctionPriceLabel", UILabel)
RegistChildComponent(LuaNewAuctionBidWnd, "YiKouPriceLabel", "YiKouPriceLabel", UILabel)
RegistChildComponent(LuaNewAuctionBidWnd, "QiPaiPriceLabel", "QiPaiPriceLabel", UILabel)
RegistChildComponent(LuaNewAuctionBidWnd, "QnIncreseAndDecreaseButton", "QnIncreseAndDecreaseButton", QnAddSubAndInputButton)
RegistChildComponent(LuaNewAuctionBidWnd, "AddPriceTitleLabel", "AddPriceTitleLabel", UILabel)
RegistChildComponent(LuaNewAuctionBidWnd, "BidButton", "BidButton", CButton)
RegistChildComponent(LuaNewAuctionBidWnd, "BuyFixButton", "BuyFixButton", CButton)
RegistChildComponent(LuaNewAuctionBidWnd, "NumSelectButton", "NumSelectButton", QnAddSubAndInputButton)
RegistChildComponent(LuaNewAuctionBidWnd, "NumDetailLabel", "NumDetailLabel", UILabel)
RegistChildComponent(LuaNewAuctionBidWnd, "FixPriceRoot", "FixPriceRoot", GameObject)
RegistChildComponent(LuaNewAuctionBidWnd, "MyBidButton", "MyBidButton", GameObject)
RegistChildComponent(LuaNewAuctionBidWnd, "QuetionButton", "QuetionButton", GameObject)
RegistChildComponent(LuaNewAuctionBidWnd, "ShuaxinButton", "ShuaxinButton", GameObject)
RegistChildComponent(LuaNewAuctionBidWnd, "PriceLabel", "PriceLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaNewAuctionBidWnd, "EnumPaiMaiExtraValue")
RegistClassMember(LuaNewAuctionBidWnd, "m_ItemInfo")
RegistClassMember(LuaNewAuctionBidWnd, "m_ItemPaiMaiId") -- 物品的PaiMaiId
RegistClassMember(LuaNewAuctionBidWnd, "m_ItemName")     -- 物品的名称
RegistClassMember(LuaNewAuctionBidWnd, "m_ItemPlayId")   -- 物品的玩法Id
RegistClassMember(LuaNewAuctionBidWnd, "m_LastPrice")
RegistClassMember(LuaNewAuctionBidWnd, "m_Min")
RegistClassMember(LuaNewAuctionBidWnd, "m_Max")
RegistClassMember(LuaNewAuctionBidWnd, "m_Step")

function LuaNewAuctionBidWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.BidButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBidButtonClick()
	end)


	
	UIEventListener.Get(self.BuyFixButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBuyFixButtonClick()
	end)


	
	UIEventListener.Get(self.MyBidButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMyBidButtonClick()
	end)


	
	UIEventListener.Get(self.QuetionButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnQuetionButtonClick()
	end)


	
	UIEventListener.Get(self.ShuaxinButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShuaxinButtonClick()
	end)


    --@endregion EventBind end

	self.QnIncreseAndDecreaseButton.onValueChanged = DelegateFactory.Action_uint(function(v)
		self:OnPriceChanged(v)
    end)
	self.NumSelectButton.onValueChanged = DelegateFactory.Action_uint(function(v)
		self:OnNumChanged(v)
    end)

    self.EnumPaiMaiExtraValue = {
        eNormal = 1,  -- 刷新当前可拍
        eClose = 2,   -- 关闭窗口
        eRefresh = 3, -- 关闭窗口，并刷新拍卖界面当前页面
    }
end

function LuaNewAuctionBidWnd:Init()
    local itemInfo = LuaAuctionMgr.m_CurSelectItem
    self.m_ItemPaiMaiId = nil
    self.m_ItemPlayId = nil
    if itemInfo.PlayId == LuaAuctionMgr.HengYuDangKouGamePlayId then -- 横屿荡寇接口，有额外信息
        self.m_ItemPaiMaiId = itemInfo.AuctionID
        self.m_ItemPlayId = itemInfo.PlayId
    end
    self.m_ItemInfo = itemInfo
    if self.m_ItemInfo == nil then
        CUIManager.CloseUI(CLuaUIResources.NewAuctionBidWnd)
        return
    end
    local itemTemplate = Item_Item.GetData(self.m_ItemInfo.TemplateID)
    if itemTemplate then
        self.IconContainer:LoadMaterial(itemTemplate.Icon)
        self.m_ItemName = itemTemplate.Name
        self.NameLabel.text = itemTemplate.Name
        self.NameLabel.color = QualityColor.GetRGBValue(CItem.GetQualityType(self.m_ItemInfo.TemplateID))
    else -- 不是物品，查询装备
        local equipment = EquipmentTemplate_Equip.GetData(self.m_ItemInfo.TemplateID)
        if equipment then
            self.IconContainer:LoadMaterial(equipment.Icon)
            self.m_ItemName = equipment.Name
            self.NameLabel.text = equipment.Name
            self.NameLabel.color = QualityColor.GetRGBValue(CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), equipment.Color))
        end
    end
	if self.m_ItemInfo.FixPrice > 0 then
        self.FixPriceRoot.gameObject:SetActive(true)
        self.YiKouPriceLabel.text = self.m_ItemInfo.FixPrice
        self.BuyFixButton.gameObject:SetActive(true)
    else
        self.FixPriceRoot.gameObject:SetActive(false)
        self.BuyFixButton.gameObject:SetActive(false)
    end
	self.AuctionPriceLabel.text = self.m_ItemInfo.Price
    self.QiPaiPriceLabel.text = self.m_ItemInfo.StartPrice
    self.m_Step = self.m_ItemInfo.StartPrice * 0.05
    local max = self.m_ItemInfo.StartPrice + self.m_Step  * 4
    if self.m_ItemInfo.FixPrice > 0 then
        max = self.m_ItemInfo.FixPrice
    end
    self.m_Min = self.m_ItemInfo.StartPrice + self.m_Step 
    self.m_Max = max
    local step = math.ceil(self.m_Step)
    self.QnIncreseAndDecreaseButton:SetMinMax(self.m_Min, self.m_Max, step)
    self.PriceLabel.text = ""
    self.QnIncreseAndDecreaseButton:SetValue(self.m_ItemInfo.CurrentLowestPrice + step, true)
    self.NumSelectButton:SetMinMax(1, self.m_ItemInfo.Count, 1)
    self.NumSelectButton:SetValue(1,true)
    self.NumDetailLabel.text = SafeStringFormat3(LocalString.GetString("当前出价可拍%s"),self.m_ItemInfo.Count)
    self.BidButton.Enabled = self.m_ItemInfo.Count > 0
end

function LuaNewAuctionBidWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSendPaimaiSystemItemByPrice", self, "OnSendPaimaiSystemItemByPrice")
    --g_ScriptEvent:AddListener("OnPlayerRequestBetSystemResult", self, "OnPlayerRequestBetSystemResult")
end

function LuaNewAuctionBidWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnSendPaimaiSystemItemByPrice", self, "OnSendPaimaiSystemItemByPrice")
    --g_ScriptEvent:RemoveListener("OnPlayerRequestBetSystemResult", self, "OnPlayerRequestBetSystemResult")
end

function LuaNewAuctionBidWnd:OnSendPaimaiSystemItemByPrice(itemTemplateId, oneShootPrice, currentlowestPrice, takeoffPrice, price, hasCount, extraValue)
    if extraValue and extraValue ~= self.EnumPaiMaiExtraValue.eNormal then
        if extraValue == self.EnumPaiMaiExtraValue.eRefresh then
            LuaAuctionWnd:OnRefreshClick() -- 拍卖界面刷新
        end
        CUIManager.CloseUI(CLuaUIResources.NewAuctionBidWnd)
        return
    end
    local curNum = self.NumSelectButton:GetValue()
    self.NumSelectButton:SetMinMax(1, hasCount, 1)
    self.NumSelectButton:SetValue(math.min(math.max(1, curNum),hasCount),true)
    self.NumDetailLabel.text = SafeStringFormat3(LocalString.GetString("当前出价可拍%s"),hasCount)
    self.BidButton.Enabled = hasCount > 0
    local step = math.ceil(self.m_Step)
    self.QnIncreseAndDecreaseButton:SetMinMax(self.m_Min, oneShootPrice, step)
    self.QnIncreseAndDecreaseButton:SetValue(price, false)
    self.PriceLabel.text = price
    local z = (price - self.m_ItemInfo.StartPrice)
    if z > 0 then
        local percentage = math.floor((z * 20 / (self.m_ItemInfo.StartPrice)) + 0.5)
        self.AddPriceTitleLabel.text = System.String.Format(LocalString.GetString("加价{0}%"), percentage * 5)
        local lowestz = (currentlowestPrice - self.m_ItemInfo.StartPrice)
        local lowestpercentage = math.floor((lowestz * 20 / (self.m_ItemInfo.StartPrice)) + 0.5)
        if percentage <= lowestpercentage + 1 then -- 不允许减到当前最低价
            self.QnIncreseAndDecreaseButton:SetMinMax(price, oneShootPrice, step)
            self.QnIncreseAndDecreaseButton:SetValue(price, false)
        end
    end
    self.m_LastPrice = price
end

-- function LuaNewAuctionBidWnd:OnPlayerRequestBetSystemResult(bOk, itemTemplateId, price, targetType, betCount)

-- end

--@region UIEvent

function LuaNewAuctionBidWnd:OnBidButtonClick()
    if self.m_ItemInfo == nil then
        return
    end
    local price = tonumber(self.PriceLabel.text)
    local count = self.NumSelectButton:GetValue()
    local itemId = self.m_ItemInfo.TemplateID
    local msg = g_MessageMgr:FormatMessage("NewAuctionBidWnd_Confirm", price, count, self.m_ItemName)
    MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function()
        if self.m_ItemPaiMaiId then -- 有额外信息，采用新接口
            Gac2Gas.RequestGamePlayPaiMaiBet(self.m_ItemPaiMaiId, itemId, price, count)
        else
            Gac2Gas.RequestPaimaiBetSystemItem(itemId, price, count)
        end
    end), nil, LocalString.GetString("竞拍"), LocalString.GetString("取消"), false)
end

function LuaNewAuctionBidWnd:OnBuyFixButtonClick()
    if self.m_ItemInfo == nil then
        return
    end
    self.QnIncreseAndDecreaseButton:SetValue(self.m_ItemInfo.FixPrice, true)
    self:OnPriceChanged(self.m_ItemInfo.FixPrice)
end

function LuaNewAuctionBidWnd:OnPriceChanged(v)
	if self.m_ItemInfo == nil then
        return
    end
    local z = (v - self.m_ItemInfo.StartPrice)
    if z > 0 then
        local percentage = math.floor((z * 20 / (self.m_ItemInfo.StartPrice)) + 0.5)
        self.AddPriceTitleLabel.text = System.String.Format(LocalString.GetString("加价{0}%"), percentage * 5)
        --local step = math.floor((percentage + 1) * self.m_Step - z + 0.5)
        --self.QnIncreseAndDecreaseButton:SetMinMax(self.m_Min, self.m_Max, step)
    end
    if self.m_LastPrice ~= v then
        self.m_LastPrice = v
        self:OnShuaxinButtonClick()
    end
end

function LuaNewAuctionBidWnd:OnNumChanged(v)
end

function LuaNewAuctionBidWnd:OnMyBidButtonClick()
    if self.m_ItemPlayId then -- 有额外信息，采用新接口
        Gac2Gas.QueryGamePlayPaiMaiMyItems(self.m_ItemPlayId)
    else
        Gac2Gas.QueryPaimaiMySystemItems()
    end
end


function LuaNewAuctionBidWnd:OnQuetionButtonClick()
    g_MessageMgr:ShowMessage("NewAuctionBidWnd_ReadMe")
end

function LuaNewAuctionBidWnd:OnShuaxinButtonClick()
    if self.m_ItemInfo == nil then
        return
    end
    if self.m_ItemPaiMaiId then
        Gac2Gas.QueryGamePlayPaiMaiItemByPrice(self.m_ItemPaiMaiId, self.m_ItemInfo.TemplateID, self.QnIncreseAndDecreaseButton:GetValue())
    else
        Gac2Gas.QueryPaimaiSystemItemByPrice(self.m_ItemInfo.TemplateID, self.QnIncreseAndDecreaseButton:GetValue())
    end
end


--@endregion UIEvent

