local CClientHouseMgr=import "L10.Game.CClientHouseMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"

CLuaWinterHouseUnlockListWnd = class()
RegistClassMember(CLuaWinterHouseUnlockListWnd, "m_LastWinterHouseSwitchTime")
RegistClassMember(CLuaWinterHouseUnlockListWnd, "m_Tick")
RegistClassMember(CLuaWinterHouseUnlockListWnd, "m_ButtonLabel")
RegistClassMember(CLuaWinterHouseUnlockListWnd, "m_UnlockButton")
RegistClassMember(CLuaWinterHouseUnlockListWnd, "m_Text")

function CLuaWinterHouseUnlockListWnd:Awake()
    local button1 = self.transform:Find("Background/Node/Button1").gameObject
    local button2 = self.transform:Find("Background/Node/Button2").gameObject

    UIEventListener.Get(button1).onClick = DelegateFactory.VoidDelegate(function(go)
        CLuaWinterHouseZswLookupWnd.s_Index = 0
        CUIManager.ShowUI(CLuaUIResources.WinterHouseZswLookupWnd)
    end)
    UIEventListener.Get(button2).onClick = DelegateFactory.VoidDelegate(function(go)
        CLuaWinterHouseZswLookupWnd.s_Index = 1
        CUIManager.ShowUI(CLuaUIResources.WinterHouseZswLookupWnd)
    end)

    local go1 = self.transform:Find("Background/Node/Title2/Button01").gameObject
    local go2 = self.transform:Find("Background/Node/Title2/Button02").gameObject
    UIEventListener.Get(go1).onClick = DelegateFactory.VoidDelegate(function(g)
        --开启雪景
        local msg = g_MessageMgr:FormatMessage("Open_SnowFurniture_Confirm")
        g_MessageMgr:ShowOkCancelMessage(msg,function()
            Gac2Gas.RequestSwitchFurnitureWinterAppearanceByTemplateId(-1,1)
        end,nil,nil,nil,false)
    end)
    UIEventListener.Get(go2).onClick = DelegateFactory.VoidDelegate(function(g)
        --关闭雪景
        local msg = g_MessageMgr:FormatMessage("Close_SnowFurniture_Confirm")
        g_MessageMgr:ShowOkCancelMessage(msg,function()
            Gac2Gas.RequestSwitchFurnitureWinterAppearanceByTemplateId(-1,0)
        end,nil,nil,nil,false)
    end)
end

function CLuaWinterHouseUnlockListWnd:Init()

    self.m_UnlockButton = FindChild(self.transform,"UnlockButton").gameObject
    self.m_ButtonLabel = self.m_UnlockButton.transform:Find("Label"):GetComponent(typeof(UILabel))
    local prop = CClientHouseMgr.Inst.mFurnitureProp
    if prop then
        self.m_LastWinterHouseSwitchTime = prop.LastWinterHouseSwitchTime
    end
    -- print(prop.WinterSceneStyle)
    if prop and prop.IsEnableWinterScene and prop.WinterSceneStyle==0 then
        self.m_Text = LocalString.GetString("关闭")
    else
        self.m_Text = LocalString.GetString("开启")
    end
    self.m_ButtonLabel.text=self.m_Text

    -- print(CServerTimeMgr.Inst.timeStamp,self.m_LastWinterHouseSwitchTime)
    if CServerTimeMgr.Inst.timeStamp-self.m_LastWinterHouseSwitchTime<600 then
        CUICommonDef.SetActive(self.m_UnlockButton,false,true)

        local totalSeconds = math.floor(600 - CServerTimeMgr.Inst.timeStamp + self.m_LastWinterHouseSwitchTime)
        if self.m_Tick then
            UnRegisterTick(self.m_Tick)
        end
        self:UpdateTime()
        self.m_Tick = RegisterTickWithDuration(function ( ... )
            self:UpdateTime()
        end, 1000, totalSeconds * 1000)

    else
        CUICommonDef.SetActive(self.m_UnlockButton,true,true)
    end

    UIEventListener.Get(self.m_UnlockButton).onClick = DelegateFactory.VoidDelegate(function(go)
        if CClientHouseMgr.Inst:IsCurHouseRoomer() then
            g_MessageMgr:ShowMessage("WINTER_OPEN_FANGKE_NOT_ALLOW")
            return
        end
        local msg = g_MessageMgr:FormatMessage("Winter_Switch_Confirm")
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function()
            Gac2Gas.RequestSwitchWinterSceneAtHome()
        end), nil, nil, nil, false)
    end)
end

function CLuaWinterHouseUnlockListWnd:UpdateTime()
    local totalSeconds = math.floor(600 - CServerTimeMgr.Inst.timeStamp + self.m_LastWinterHouseSwitchTime)

    if totalSeconds <= 0 then
        UnRegisterTick(self.m_Tick)
        self.m_ButtonLabel.text = self.m_Text
        CUICommonDef.SetActive(self.m_UnlockButton,true,true)
    else
        self.m_ButtonLabel.text= SafeStringFormat3( "%02d:%02d",math.floor(totalSeconds/60),totalSeconds%60 )
	end
end

function CLuaWinterHouseUnlockListWnd:OnDestroy()
    if self.m_Tick then
		UnRegisterTick(self.m_Tick)
		self.m_Tick = nil
	end
end
