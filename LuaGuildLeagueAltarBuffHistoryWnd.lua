local DelegateFactory  = import "DelegateFactory"
local QnTableView = import "L10.UI.QnTableView"
local CChatLinkMgr = import "CChatLinkMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local UICamera = import "UICamera"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local ChatPlayerLink = import "CChatLinkMgr+ChatPlayerLink"

LuaGuildLeagueAltarBuffHistoryWnd = class()

RegistClassMember(LuaGuildLeagueAltarBuffHistoryWnd, "m_QnTableView")
RegistClassMember(LuaGuildLeagueAltarBuffHistoryWnd, "m_EmptyLabel")
RegistClassMember(LuaGuildLeagueAltarBuffHistoryWnd, "m_DefBuffRemainTimesLabel")
RegistClassMember(LuaGuildLeagueAltarBuffHistoryWnd, "m_AttBuffRemainTimesLabel")
RegistClassMember(LuaGuildLeagueAltarBuffHistoryWnd, "m_DefBuffRemainTimesValueLabel")
RegistClassMember(LuaGuildLeagueAltarBuffHistoryWnd, "m_AttBuffRemainTimesValueLabel")

RegistClassMember(LuaGuildLeagueAltarBuffHistoryWnd, "defBuffName")
RegistClassMember(LuaGuildLeagueAltarBuffHistoryWnd, "attBuffName")
RegistClassMember(LuaGuildLeagueAltarBuffHistoryWnd, "m_DataTbl")

function LuaGuildLeagueAltarBuffHistoryWnd:Awake()
    self.m_QnTableView = self.transform:Find("Anchor/QnTableView"):GetComponent(typeof(QnTableView))
    self.m_EmptyLabel = self.transform:Find("Anchor/EmptyLabel").gameObject
    self.m_DefBuffRemainTimesLabel = self.transform:Find("Anchor/Bottom/DefBuffRemainTimes"):GetComponent(typeof(UILabel))
    self.m_AttBuffRemainTimesLabel = self.transform:Find("Anchor/Bottom/AttBuffRemainTimes"):GetComponent(typeof(UILabel))
    self.m_DefBuffRemainTimesValueLabel = self.transform:Find("Anchor/Bottom/DefBuffRemainTimes/ValueLabel"):GetComponent(typeof(UILabel))
    self.m_AttBuffRemainTimesValueLabel = self.transform:Find("Anchor/Bottom/AttBuffRemainTimes/ValueLabel"):GetComponent(typeof(UILabel))
    self.m_QnTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return #self.m_DataTbl
        end,
        function(item, index)
            self:InitItem(item,index)
        end)
    self.m_DataTbl = {}
    self.m_EmptyLabel:SetActive(true)
end

function LuaGuildLeagueAltarBuffHistoryWnd:Init()
    self.defBuffName = Buff_Buff.GetData(GuildLeague_Setting.GetData().DefBuffCls*100+1).Name
    self.attBuffName = Buff_Buff.GetData(GuildLeague_Setting.GetData().AttBuffCls*100+1).Name
    local info = LuaGuildLeagueMgr.m_AltarBuffHistoryInfo
    self.m_DefBuffRemainTimesLabel.text = SafeStringFormat3(LocalString.GetString("%s[c][acf8ff]剩余[-]"), self.defBuffName)
    self.m_AttBuffRemainTimesLabel.text = SafeStringFormat3(LocalString.GetString("%s[c][acf8ff]剩余[-]"), self.attBuffName)
    self.m_DefBuffRemainTimesValueLabel.text = tostring(info.defBuffLeftCount)
    self.m_AttBuffRemainTimesValueLabel.text = tostring(info.attBuffLeftCount)
    self.m_DataTbl = info.historyTbl
    self.m_QnTableView:ReloadData(false, false)
    self.m_EmptyLabel:SetActive(#self.m_DataTbl==0)
end

function LuaGuildLeagueAltarBuffHistoryWnd:InitItem(item,row)
    local timeLabel = item.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
    local contentLabel = item.transform:Find("ContentLabel"):GetComponent(typeof(UILabel))
    local data = self.m_DataTbl[row+1]
    local dtTime =  CServerTimeMgr.ConvertTimeStampToZone8Time(data.time)
    timeLabel.text = ToStringWrap(dtTime, "yyyy-MM-dd HH:mm")
    local playerLink = ChatPlayerLink.GenerateLink(data.playerId, data.playerName).displayTag
    if data.isDefBuff then
        contentLabel.text = SafeStringFormat3(LocalString.GetString("%s在守护神坛处领取了[519fff]%s[-]"), playerLink, self.defBuffName)
    else
        contentLabel.text = SafeStringFormat3(LocalString.GetString("%s在激战神坛处领取了[ff7070]%s[-]"), playerLink, self.attBuffName)
    end
    UIEventListener.Get(contentLabel.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
        local url = contentLabel:GetUrlAtPosition(UICamera.lastWorldPosition)
        CChatLinkMgr.ProcessLinkClick(url, nil)
    end)
end

