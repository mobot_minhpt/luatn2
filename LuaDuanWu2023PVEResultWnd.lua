local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local Item_Item              = import "L10.Game.Item_Item"
local CRankData              = import "L10.UI.CRankData"

LuaDuanWu2023PVEResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDuanWu2023PVEResultWnd, "killBossNum", "KillBossNum", UILabel)
RegistChildComponent(LuaDuanWu2023PVEResultWnd, "rankNum", "RankNum", UILabel)
RegistChildComponent(LuaDuanWu2023PVEResultWnd, "rankButton", "RankButton", GameObject)
RegistChildComponent(LuaDuanWu2023PVEResultWnd, "returnAwardTemplate", "ReturnAwardTemplate", GameObject)
RegistChildComponent(LuaDuanWu2023PVEResultWnd, "grid", "Grid", UIGrid)
RegistChildComponent(LuaDuanWu2023PVEResultWnd, "reason", "Reason", UILabel)
--@endregion RegistChildComponent end

function LuaDuanWu2023PVEResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.rankButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRankButtonClick()
	end)
    --@endregion EventBind end
	self.returnAwardTemplate:SetActive(false)
end

function LuaDuanWu2023PVEResultWnd:OnEnable()
	g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaDuanWu2023PVEResultWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaDuanWu2023PVEResultWnd:OnRankDataReady()
    local myInfo = CRankData.Inst.MainPlayerRankInfo
    self.rankNum.text = myInfo.Rank > 0 and myInfo.Rank or LocalString.GetString("未上榜")
	self.rankButton.transform:GetComponent(typeof(UIWidget)):ResetAndUpdateAnchors()
end


function LuaDuanWu2023PVEResultWnd:Init()
	local info = LuaDuanWu2023Mgr.pveResultInfo
	self.killBossNum.text = info.bossNumber
	local setting = Duanwu2023_EndlessChallengeSetting.GetData()
	local baseKillBossNum, baseRewardTimesLimit = string.match(setting.BaseDailyReward, "(%d+);(%d+);%d+")
	local advancedKillBossNum, advancedRewardTimesLimit = string.match(setting.AdvancedDailyReward, "(%d+);(%d+);%d+")

	Extensions.RemoveAllChildren(self.grid.transform)
	if info.isGetAdvancedDailyReward or info.isGetBaseDailyReward then
		if info.isGetBaseDailyReward then
			local child = NGUITools.AddChild(self.grid.gameObject, self.returnAwardTemplate)
			child:SetActive(true)
			child.transform:GetComponent(typeof(CQnReturnAwardTemplate)):Init(Item_Item.GetData(setting.BaseDailyRewardItemId), 0)
			child.transform:Find("Target"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("击败%d"), baseKillBossNum)
		end
		if info.isGetAdvancedDailyReward then
			local child = NGUITools.AddChild(self.grid.gameObject, self.returnAwardTemplate)
			child:SetActive(true)
			child.transform:GetComponent(typeof(CQnReturnAwardTemplate)):Init(Item_Item.GetData(setting.AdvancedDailyRewardItemId), 0)
			child.transform:Find("Target"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("击败%d"), advancedKillBossNum)
		end
		self.grid:Reposition()
		self.reason.text = ""
	else
		local baseAwarded = CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eEndlessChallengeBaseDailyAwardTimes) >= tonumber(baseRewardTimesLimit)
		local advancedAwarded = CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eEndlessChallengeAdvancedDailyAwardTimes) >= tonumber(advancedRewardTimesLimit)
		if baseAwarded and advancedAwarded then
			self.reason.text = LocalString.GetString("今日奖励次数\n已用完")
		elseif not baseAwarded then
			self.reason.text = SafeStringFormat3(LocalString.GetString("击败%d个BOSS\n可得下级奖励"), baseKillBossNum)
		elseif not advancedAwarded then
			self.reason.text = SafeStringFormat3(LocalString.GetString("击败%d个BOSS\n可得下级奖励"), advancedKillBossNum)
		end
	end

	self.rankNum.text = "-"
	self.rankButton.transform:GetComponent(typeof(UIWidget)):ResetAndUpdateAnchors()
	Gac2Gas.QueryRank(setting.RankId)
end

--@region UIEvent

function LuaDuanWu2023PVEResultWnd:OnRankButtonClick()
	CUIManager.ShowUI(CLuaUIResources.DuanWu2023PVERankWnd)
end

--@endregion UIEvent
