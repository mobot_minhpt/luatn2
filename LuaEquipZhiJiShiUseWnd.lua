require("common/common_include")
local CItemChooseMgr=import "L10.UI.CItemChooseMgr"
local CItemCountUpdate=import "L10.UI.CItemCountUpdate"
local CItemAccessListMgr=import "L10.UI.CItemAccessListMgr"
local UITable=import "UITable"
local CUITexture=import "L10.UI.CUITexture"
local Item_Item=import "L10.Game.Item_Item"
local CItemMgr=import "L10.Game.CItemMgr"
local EquipmentTemplate_Equip=import "L10.Game.EquipmentTemplate_Equip"
local Word_Word=import "L10.Game.Word_Word"
local UIWidget=import "UIWidget"
local Convert=import "System.Convert"
local Gac2Gas=import "L10.Game.Gac2Gas"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local MessageWndManager=import "L10.UI.MessageWndManager"
local MessageMgr=import "L10.Game.MessageMgr"
local CTooltipAlignType=import "L10.UI.CTooltip+AlignType"

CLuaEquipZhiJiShiUseWnd=class()
RegistClassMember(CLuaEquipZhiJiShiUseWnd,"m_SelectEquipButton")
RegistClassMember(CLuaEquipZhiJiShiUseWnd,"m_SelectedEquip")
RegistClassMember(CLuaEquipZhiJiShiUseWnd,"m_ZhiHuanButton")
RegistClassMember(CLuaEquipZhiJiShiUseWnd,"m_TipLabel")
RegistClassMember(CLuaEquipZhiJiShiUseWnd,"m_TipButton")
RegistClassMember(CLuaEquipZhiJiShiUseWnd,"m_SelectedEquipId")
RegistClassMember(CLuaEquipZhiJiShiUseWnd,"m_ZhiJiShi")

RegistClassMember(CLuaEquipZhiJiShiUseWnd,"m_DescTable")
RegistClassMember(CLuaEquipZhiJiShiUseWnd,"m_DescItem")

RegistClassMember(CLuaEquipZhiJiShiUseWnd,"m_NeedCount")
RegistClassMember(CLuaEquipZhiJiShiUseWnd,"m_CountUpdate")

function CLuaEquipZhiJiShiUseWnd:Init()
    self.m_NeedCount=0
    self.m_TipLabel=FindChild(self.transform,"TipLabel"):GetComponent(typeof(UILabel))
    self.m_DescTable=FindChild(self.transform,"DescTable"):GetComponent(typeof(UITable))
    self.m_DescItem=FindChild(self.transform,"DescItem").gameObject
    self.m_DescItem:SetActive(false)

    self.m_SelectEquipButton=FindChild(self.transform,"SelectEquipButton").gameObject
    UIEventListener.Get(self.m_SelectEquipButton).onClick=LuaUtils.VoidDelegate(function(go)

        CItemChooseMgr.Inst.ItemIds=CLuaEquipZhouNianQingMgr.GetEquipList()
        CItemChooseMgr.Inst.OnChoose=DelegateFactory.Action_string(function(p)
            self.m_SelectedEquipId=p
            self:OnChooseEquip()
        end)
        CItemChooseMgr.ShowWnd(LocalString.GetString("选择装备"))
    end)

    self.m_TipButton=FindChild(self.transform,"TipButton").gameObject
    UIEventListener.Get(self.m_TipButton).onClick=LuaUtils.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("Equi_ZhiHuan_ZhiJiShi_Readme",{})
    end)


    self.m_ZhiHuanButton=FindChild(self.transform,"ZhiHuanButton").gameObject
    UIEventListener.Get(self.m_ZhiHuanButton).onClick=LuaUtils.VoidDelegate(function(go)
        if self.m_SelectedEquipId then
            local itemInfo = CItemMgr.Inst:GetItemInfo(self.m_SelectedEquipId)
            if itemInfo then
                local place=Convert.ToUInt32(itemInfo.place)
                local pos=itemInfo.pos
                local equip = CItemMgr.Inst:GetById(self.m_SelectedEquipId)
                local secondWordSet = equip.Equip.HasTwoWordSet and equip.Equip.IsSecondWordSetActive or false
                local hasTempData = equip.Equip:HasTempWordDataInWordSet(secondWordSet)
                local hasTempDataOtherSet = equip.Equip:HasTempWordDataInWordSet(not secondWordSet)

                local function request()
                    if hasTempData then
                        local str=MessageMgr.Inst:FormatMessage("CITIAO_NOTICE",{})
                        MessageWndManager.ShowOKCancelMessage(str,DelegateFactory.Action(function()
                            Gac2Gas.RequestUpgradeEquipWord(place,pos,self.m_SelectedEquipId)
                        end),nil,nil,nil,false)
                    elseif hasTempDataOtherSet then
                        local str=MessageMgr.Inst:FormatMessage("CITIAO_NOTICE_OTHERSET",{})
                        MessageWndManager.ShowOKCancelMessage(str,DelegateFactory.Action(function()
                            Gac2Gas.RequestUpgradeEquipWord(place,pos,self.m_SelectedEquipId)
                        end),nil,nil,nil,false)
                    else
                        Gac2Gas.RequestUpgradeEquipWord(place,pos,self.m_SelectedEquipId)
                    end
                end

                if equip.Equip.IsBinded then
                    request()
                else
                    local str=MessageMgr.Inst:FormatMessage("Equi_ZhiHuan_ZhiJiShi_Bind_Confirm",{})
                    MessageWndManager.ShowOKCancelMessage(str,DelegateFactory.Action(function()
                        request()
                    end),nil,nil,nil,false)
                end
            end
        else
            g_MessageMgr:ShowMessage("Double_EquipWords_Need_Select_Equip",{})
        end
    end)



    self.m_SelectedEquip=FindChild(self.transform,"SelectedEquip").gameObject
    self:InitSelectedEquip()

    self.m_ZhiJiShi=FindChild(self.transform,"ZhiJiShi").gameObject
    self:InitZhiJiShi()
end

function CLuaEquipZhiJiShiUseWnd:InitZhiJiShi()
    -- CLuaEquipZhouNianQingMgr.m_ZhiJiShiItemId
    local tf=self.m_ZhiJiShi.transform
    local templateId=CLuaEquipZhouNianQingMgr.m_ZhiJiShiItemId
    local template = Item_Item.GetData(templateId)

    local icon=FindChild(tf,"Icon"):GetComponent(typeof(CUITexture))
    icon:LoadMaterial(template.Icon)

    FindChild(tf,"NameLabel"):GetComponent(typeof(UILabel)).text=template.Name
    local mask=FindChild(tf,"Mask").gameObject
    self.m_CountUpdate=tf:GetComponent(typeof(CItemCountUpdate))
    self.m_CountUpdate.templateId=templateId
    self.m_CountUpdate.excludeExpireTime=true
    -- local needCount=1
    self.m_NeedCount=0
    local function OnCountChange(val)
        if val>=self.m_NeedCount then
            mask:SetActive(false)
            self.m_CountUpdate.format=SafeStringFormat3("{0}/%d",self.m_NeedCount)
        else
            mask:SetActive(true)
            self.m_CountUpdate.format=SafeStringFormat3("[ff0000]{0}[-]/%d",self.m_NeedCount)
        end
    end
    self.m_CountUpdate.onChange=DelegateFactory.Action_int(OnCountChange)
    self.m_CountUpdate:UpdateCount()

    --点击支机石
    UIEventListener.Get(self.m_ZhiJiShi).onClick=LuaUtils.VoidDelegate(function(go)
        if self.m_CountUpdate.count>=self.m_NeedCount then
            CItemInfoMgr.ShowLinkItemTemplateInfo(templateId,false,nil,CItemInfoMgr.AlignType.Default,0,0,0,0)
        else
            CItemAccessListMgr.Inst:ShowItemAccessInfo(templateId, false, tf, CTooltipAlignType.Right)
        end
    end)

end

function CLuaEquipZhiJiShiUseWnd:InitSelectedEquip()
    local tf=self.m_SelectedEquip.transform
    local spriteMark=FindChild(tf,"EquipMark"):GetComponent(typeof(UISprite))
    spriteMark.spriteName = nil
    local icon=FindChild(tf,"EquipIcon"):GetComponent(typeof(CUITexture))
    icon:Clear()

    local label=FindChild(tf,"NameLabel"):GetComponent(typeof(UILabel))
    label.text=nil

    local qualitySprite=FindChild(tf,"QualitySprite"):GetComponent(typeof(UISprite))
    qualitySprite.spriteName="common_itemcell_border"
end

function CLuaEquipZhiJiShiUseWnd:OnChooseEquip()
    if self.m_SelectedEquipId then
        local itemInfo = CItemMgr.Inst:GetItemInfo(self.m_SelectedEquipId)
        if itemInfo then
            local place=Convert.ToUInt32(itemInfo.place)
            local pos=itemInfo.pos
            Gac2Gas.RequestUpgradeEquipWordCost(place,pos,self.m_SelectedEquipId)
        end

        local tf=self.m_SelectedEquip.transform
        local spriteMark=FindChild(tf,"EquipMark"):GetComponent(typeof(UISprite))
        local icon=FindChild(tf,"EquipIcon"):GetComponent(typeof(CUITexture))
        -- icon:Clear()

        local equip = CItemMgr.Inst:GetById(self.m_SelectedEquipId)--CCommonItem
        spriteMark.spriteName = equip.BindOrEquipCornerMark
        
        local template = EquipmentTemplate_Equip.GetData(equip.TemplateId)
        icon:LoadMaterial(template.Icon)

        local label=FindChild(tf,"NameLabel"):GetComponent(typeof(UILabel))
        -- label.text=template.Name
        label.text=equip.Equip.DisplayName
        label.color=equip.Equip.DisplayColor

        local qualitySprite=FindChild(tf,"QualitySprite"):GetComponent(typeof(UISprite))
        qualitySprite.spriteName=CUICommonDef.GetItemCellBorder(equip.Equip.QualityType)

        self.m_TipLabel.enabled=false

        self:InitDescTable()
    end
end




function CLuaEquipZhiJiShiUseWnd:InitDescTable(oldWordId,newWordId)
    CUICommonDef.ClearTransform(self.m_DescTable.transform)

    local item=CItemMgr.Inst:GetById(self.m_SelectedEquipId)
    local color=item.Equip.DisplayColor


    local list1=item.Equip:GetFixedWordList(false)
    local list2=item.Equip:GetExtWordList(false)

    local parent=self.m_DescTable.gameObject

    local t={}
    for i=1,list1.Count do
        table.insert( t,list1[i-1] )
    end
    for i=1,list2.Count do
        table.insert( t,list2[i-1] )
    end


    local needCheck=oldWordId and true or false
    for i,v in ipairs(t) do
        if needCheck and v==newWordId then
            needCheck=false
            local go=NGUITools.AddChild(parent,self.m_DescItem)
            go:SetActive(true)
            self:InitOldDescItem(go,oldWordId)
        end

        local go=NGUITools.AddChild(parent,self.m_DescItem)
        go:SetActive(true)

        self:InitDescItem(go,v,color)
    end

    self.m_DescTable:Reposition()
end

function CLuaEquipZhiJiShiUseWnd:InitDescItem(go,wordId,color)
    local tf=go.transform
    local nameLabel=FindChild(tf,"NameLabel"):GetComponent(typeof(UILabel))
    local descLabel=FindChild(tf,"DescLabel"):GetComponent(typeof(UILabel))
    local bgWidget=go:GetComponent(typeof(UIWidget))
    local lineSprite=FindChild(tf,"LineSprite"):GetComponent(typeof(UISprite))

    nameLabel.color=color
    descLabel.color=color

    local data = Word_Word.GetData(wordId)
    if data then
        nameLabel.text=SafeStringFormat3( "[%s]",data.Name )
        descLabel.text=data.Description
    end

    descLabel:UpdateNGUIText()
    lineSprite.enabled = true
    bgWidget.height = descLabel.localSize.y + 20
end

function CLuaEquipZhiJiShiUseWnd:InitOldDescItem(go,wordId)
    local tf=go.transform
    local nameLabel=FindChild(tf,"NameLabel"):GetComponent(typeof(UILabel))
    local descLabel=FindChild(tf,"DescLabel"):GetComponent(typeof(UILabel))
    local bgWidget=go:GetComponent(typeof(UIWidget))
    local lineSprite=FindChild(tf,"LineSprite"):GetComponent(typeof(UISprite))
    -- nameLabel.color=color
    -- descLabel.color=color

    local data = Word_Word.GetData(wordId)
    if data then
        nameLabel.text=SafeStringFormat3( "[s][c][888888][%s][-][/c][/s]",data.Name )
        descLabel.text=SafeStringFormat3( "[s][c][888888]%s[-][/c][/s]",data.Description )
    end

    descLabel:UpdateNGUIText()
    lineSprite.enabled = false
    bgWidget.height = descLabel.localSize.y
end
function CLuaEquipZhiJiShiUseWnd:OnEnable()
    g_ScriptEvent:AddListener("UpgradeEquipWordSuccess", self, "OnUpgradeEquipWordSuccess")
    g_ScriptEvent:AddListener("RequestUpgradeEquipWordCostReturn", self, "OnRequestUpgradeEquipWordCostReturn")
    
end
function CLuaEquipZhiJiShiUseWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpgradeEquipWordSuccess", self, "OnUpgradeEquipWordSuccess")
    g_ScriptEvent:RemoveListener("RequestUpgradeEquipWordCostReturn", self, "OnRequestUpgradeEquipWordCostReturn")

end

function CLuaEquipZhiJiShiUseWnd:OnRequestUpgradeEquipWordCostReturn(equipId,count)
    if equipId==self.m_SelectedEquipId then
        self.m_NeedCount=count
        self.m_CountUpdate:UpdateCount()
    end
end

function CLuaEquipZhiJiShiUseWnd:OnUpgradeEquipWordSuccess(equipId,oldWordId,newWordId)
    if equipId==self.m_SelectedEquipId then
        self:InitDescTable(oldWordId,newWordId)
    end
end

return CLuaEquipZhiJiShiUseWnd
