local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CExpressionActionMgr = import "L10.Game.CExpressionActionMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local CStatusMgr = import "L10.Game.CStatusMgr"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local DelegateFactory = import "DelegateFactory"
local EPropStatus = import "L10.Game.EPropStatus"
local Object = import "System.Object"
local UIEventListener = import "UIEventListener"
local Time = import "UnityEngine.Time"
local CAS_Expression = import "L10.Game.CAS_Expression"
local CActionStateMgr = import "L10.Game.CActionStateMgr"
local EnumActionState = import "L10.Game.EnumActionState"

local QnNewSlider = import "L10.UI.QnNewSlider"
local QnRadioBox = import "L10.UI.QnRadioBox"
local BabyMgr = import "L10.Game.BabyMgr"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CClientOtherPlayer = import "L10.Game.CClientOtherPlayer"
local Expression_Define = import "L10.Game.Expression_Define"
local MessageWndManager = import "L10.UI.MessageWndManager"
local EnumGender = import "L10.Game.EnumGender"
local CAniFastClipMgr = import "L10.Engine.CAniFastClipMgr"

CLuaScreenCaptureExpressionView = class()
CLuaScreenCaptureExpressionView.Path = "ui/screencapture/LuaScreenCaptureExpressionView"
RegistClassMember(CLuaScreenCaptureExpressionView, "m_Slider")
RegistClassMember(CLuaScreenCaptureExpressionView, "m_AniTotalTime")
RegistClassMember(CLuaScreenCaptureExpressionView, "m_DeltaTime")
RegistClassMember(CLuaScreenCaptureExpressionView, "m_Paused")
RegistClassMember(CLuaScreenCaptureExpressionView, "m_PauseSprite")
RegistClassMember(CLuaScreenCaptureExpressionView, "m_CurrentTabIndex")

-- Expression ID Table
RegistClassMember(CLuaScreenCaptureExpressionView, "m_BabyExpIdTable")
RegistClassMember(CLuaScreenCaptureExpressionView, "m_OtherPlayerExpIdTable")
RegistClassMember(CLuaScreenCaptureExpressionView, "m_PlayerExpIdTable")
RegistClassMember(CLuaScreenCaptureExpressionView, "m_AdvGridView")

-- log
RegistClassMember(CLuaScreenCaptureExpressionView, "m_bUseExpression")
RegistClassMember(CLuaScreenCaptureExpressionView, "m_bUseOtherExpression")

function CLuaScreenCaptureExpressionView:OnEnable()
    g_ScriptEvent:AddListener("JoyStickDragging", self, "MainPlayerMove")
    g_ScriptEvent:AddListener("PlayerDragJoyStick", self, "MainPlayerMove")
    g_ScriptEvent:AddListener("OnClientObjectSelected", self, "OnClientObjectSelected")
    g_ScriptEvent:AddListener("TargetAgreeToGroupPhoto", self, "TargetAgreeToGroupPhoto")
    g_ScriptEvent:AddListener("OnRideOnOffVehicle", self, "MainPlayerMove")
    g_ScriptEvent:AddListener("OnGetFuxiPlayerOwnData", self, "OnGetFuxiPlayerOwnData")
    g_ScriptEvent:AddListener("OnExpressionActionMgrLoadAppendClipFinished", self, "OnExpressionActionMgrLoadAppendClipFinished")
    g_ScriptEvent:AddListener("OnSendFashionTransformState", self, "OnSendFashionTransformState")
    -- Awake里调用InitFuxiExpression时未Enable,会收不到回调,这里主动调用
    self:OnGetFuxiPlayerOwnData()
    self:OnSendFashionTransformState()
end

function CLuaScreenCaptureExpressionView:OnDisable()
    g_ScriptEvent:RemoveListener("JoyStickDragging", self, "MainPlayerMove")
    g_ScriptEvent:RemoveListener("PlayerDragJoyStick", self, "MainPlayerMove")
    g_ScriptEvent:RemoveListener("OnClientObjectSelected", self, "OnClientObjectSelected")
    g_ScriptEvent:RemoveListener("TargetAgreeToGroupPhoto", self, "TargetAgreeToGroupPhoto")
    g_ScriptEvent:RemoveListener("OnRideOnOffVehicle", self, "MainPlayerMove")
    g_ScriptEvent:RemoveListener("OnGetFuxiPlayerOwnData", self, "OnGetFuxiPlayerOwnData")
    g_ScriptEvent:RemoveListener("OnExpressionActionMgrLoadAppendClipFinished", self, "OnExpressionActionMgrLoadAppendClipFinished")
    g_ScriptEvent:RemoveListener("OnSendFashionTransformState", self, "OnSendFashionTransformState")
end

function CLuaScreenCaptureExpressionView:Awake()
    self.m_Paused = true
    self.m_Slider = self.transform:Find("SliderBG"):GetComponent(typeof(QnNewSlider))
    self.m_Slider.gameObject:SetActive(false)
    self.m_PauseSprite = self.m_Slider.transform:Find("PauseButton/Sprite"):GetComponent(typeof(UISprite))
    self.m_Slider.OnValueChanged = DelegateFactory.Action_float(function(value)
        self:OnSliderValueChanged(value)
    end)
    UIEventListener.Get(self.m_Slider.transform:Find("PauseButton").gameObject).onClick = LuaUtils.VoidDelegate(function()
        self:PauseOrPlay(not self.m_Paused)
    end)
    self.m_AdvGridView = self.transform:Find("Bg"):GetComponent(typeof(QnAdvanceGridView))

    -- Init Table
    self.m_BabyExpIdTable = {}
    local ret, babyInfo = CommonDefs.DictTryGet_LuaCall(BabyMgr.Inst.BabyDictionary, CLuaScreenCaptureMgr.CurrentBabyId)
    if ret then
        local expressionIdList = BabyMgr.Inst:GetValidBabySingleExpression(babyInfo)
        for i = 0, expressionIdList.Count - 1 do
            table.insert(self.m_BabyExpIdTable, expressionIdList[i])
        end
    end

    self.m_FuxiExpIdTable = {}
    self:InitPlayerExpIdTable()
    self:OnClientObjectSelected({[0] = CClientMainPlayer.Inst.Target})

    -- Init QnRadioBox
    local radioBox = self.transform:Find("QnRadioBox"):GetComponent(typeof(QnRadioBox))
    radioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn, index)
      self.m_CurrentTabIndex = index
      self.m_AdvGridView:ReloadData(true,false)
      self:PauseOrPlay(true)
    end)

    local getNumFunc = function()
        if self.m_CurrentTabIndex == 0 then
            return #self.m_PlayerExpIdTable + #self.m_FuxiExpIdTable
        elseif self.m_CurrentTabIndex == 1 then
            return #self.m_OtherPlayerExpIdTable + #self.m_FuxiExpIdTable
        elseif self.m_CurrentTabIndex == 2 then
            return #self.m_BabyExpIdTable
        end
    end
    local initItemFunc = function(item, index)
        self:InitItem(item, index)
    end

    self.m_AdvGridView.m_DataSource = DefaultTableViewDataSource.Create(getNumFunc,initItemFunc)
    self.m_AdvGridView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)

    radioBox:ChangeTo(0, true)

    self:InitOtherPlayerExpression()
    self:InitBabyExpression()
    self:InitFuxiExpression()
end

function CLuaScreenCaptureExpressionView:InitPlayerExpIdTable()
    self.m_PlayerExpIdTable = {}
    self.m_OtherPlayerExpIdTable = {}
    local classId = EnumToInt(CClientMainPlayer.Inst.Class) * 100 + EnumToInt(CClientMainPlayer.Inst.AppearanceGender)
    local isMonster = CClientMainPlayer.Inst.AppearanceGender == EnumGender.Monster
    local fashionSpecialExpressionId = CExpressionActionMgr.Inst:GetCurFashionSpecialExpression()
    if fashionSpecialExpressionId ~= 0 then
        table.insert(self.m_PlayerExpIdTable,1, fashionSpecialExpressionId)
    end
    Expression_Show.Foreach(function(k, v)
        local needAdd = false
        if v.PaiZhaoEnable > 0 then
            if isMonster then
                if v.YingLingYaoExpression > 0 then
                    needAdd = true
                end
            elseif not (v.LockGroup ~= 0 and CExpressionActionMgr.Inst.UnLockGroupInfo[v.LockGroup] == 0) and (not v.RaceCheck or CommonDefs.HashSetContains(v.RaceCheck, TypeOfUInt32, classId)) then
                needAdd = true
            end
        end
        if needAdd then
            table.insert(self.m_PlayerExpIdTable, k)
            table.insert(self.m_OtherPlayerExpIdTable, k)
        end
    end)
end 

function CLuaScreenCaptureExpressionView:OnSelectAtRow(index)
    if self.m_CurrentTabIndex == 0 then
        self:OnMainPlayerExpressionClick(index)
    elseif self.m_CurrentTabIndex == 1 then
        self:OnOtherPlayerExpressionClick(index)
    else
        self:OnBabyExpressionClick(self.m_BabyExpIdTable[index + 1])
    end
end

function CLuaScreenCaptureExpressionView:InitItem(item, index)
    local data, fuxiData
    if self.m_CurrentTabIndex == 2 then
        data = Expression_BabyShow.GetData(self.m_BabyExpIdTable[index + 1])
    elseif self.m_CurrentTabIndex == 1 then
        if self.m_OtherPlayerExpIdTable[index + 1] then
            data = Expression_Show.GetData(self.m_OtherPlayerExpIdTable[index + 1])
        else 
            fuxiData = self.m_FuxiExpIdTable[index + 1 - #self.m_OtherPlayerExpIdTable]
        end
    else
        if self.m_PlayerExpIdTable[index + 1] then
            data = Expression_Show.GetData(self.m_PlayerExpIdTable[index + 1])
        else 
            fuxiData = self.m_FuxiExpIdTable[index + 1 - #self.m_PlayerExpIdTable]
        end
    end
    local icon      = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local namelb    = item.transform:Find("Name"):GetComponent(typeof(UILabel))
    local bignamelb = item.transform:Find("Icon/LbName"):GetComponent(typeof(UILabel))
    local fuxiTag   = item.transform:Find("Icon/FuxiTag").gameObject

    if data then
        icon:LoadMaterial(data.Icon)
        bignamelb.text = ""
        namelb.text = data.ExpName
        fuxiTag:SetActive(false)
    else 
        if fuxiData == nil then
            icon:LoadMaterial("")
            bignamelb.gameObject:SetActive(false)
            namelb.text = ""
            fuxiTag:SetActive(false)
        else
            if fuxiData.outtime or fuxiData.status == EnumFuxiDanceMotionStatus.eDeleted then
                icon:LoadMaterial("")
                bignamelb.gameObject:SetActive(false)
                namelb.text = ""
                local path= GameSetting_Client.GetData().ExpireItemIconPath
                icon:LoadMaterial(path)
            else
                icon:LoadMaterial("")
                bignamelb.gameObject:SetActive(true)
                if fuxiData.motionName == nil then 
                    bignamelb.text = ""
                    namelb.text = ""
                else  
                    bignamelb.text = CommonDefs.StringAt(fuxiData.motionName,0)
                    namelb.text = fuxiData.motionName
                end
            end
            fuxiTag:SetActive(true)
        end
    end
end

function CLuaScreenCaptureExpressionView:OnDestroy()
    for k, v in pairs(self.m_Object2ExpressionIdTable) do
        if k and k.RO then
            local data = Expression_Define.GetData(v)
            if data then
                CExpressionActionMgr.HandleWeaponAndFxOnLeave(k, data)
            end
            CActionStateMgr.Inst:ShowActionState(k, EnumActionState.Idle, {})
        end
    end

    if CClientMainPlayer.Inst then
        CClientMainPlayer.Inst:LeaveOfflineExpressionActionById(CClientMainPlayer.PrevOfflineExpressionID)
		CClientMainPlayer.Inst:LeaveOfflineExpressionAction()
    end

    if self.m_CurrentBabyClientObject then
      CExpressionActionMgr.HandleWeaponAndFxOnLeave(self.m_CurrentBabyClientObject, self.m_CurrentBabyExpressionDesignData)
    end
end

function CLuaScreenCaptureExpressionView:PauseOrPlay(pause)
    self.m_Paused = pause
    self.m_PauseSprite.spriteName = self.m_Paused and Constants.Common_Play_Icon or Constants.Common_Pause_Icon
end

function CLuaScreenCaptureExpressionView:StartOfflineExpression()
    CClientMainPlayer.Inst:LeavePrevOfflineExpressionAction()
    if not self:CanDoThisActionOffline(CExpressionActionMgr.ExpressionActionID) then
        return false
    end
    CClientMainPlayer.Inst:RequestShowExpressionAction(0)

    self.m_AniTotalTime = CClientMainPlayer.Inst:GetExpressionAnimationTime(CExpressionActionMgr.ExpressionActionID)
    local zhankuangMaskCheck = CExpressionActionMgr.GetZhanKuangLiangXiangMaskCheck(CClientMainPlayer.Inst)
    if (self.m_AniTotalTime <= 0 or not zhankuangMaskCheck) and CExpressionActionMgr.ExpressionActionID == CAS_Expression.s_LiangXiangExpressionId  then
        g_MessageMgr:ShowMessage("Expression_LiangXiang_No_Animation")
		return false
     end
    if self.m_AniTotalTime <= 0 then
      return false
    end
    self:PauseOrPlay(false)
    self.m_DeltaTime = 0

    return CExpressionActionMgr.Inst:DoExpressionAction(true, true, 0)
end

function CLuaScreenCaptureExpressionView:StartOfflineFuxiExpression()
    CClientMainPlayer.Inst:LeavePrevOfflineExpressionAction()
    if not self:CanDoThisActionOffline(CExpressionActionMgr.ExpressionActionID) then
        return false
    end
    CClientMainPlayer.Inst:RequestShowExpressionAction(0)

    self.m_AniTotalTime = CAniFastClipMgr.Inst:GetAppendClipLengthByMotionId(CClientMainPlayer.Inst, CExpressionActionMgr.FuxiExpressionMotionID)

    self:PauseOrPlay(false)
    self.m_DeltaTime = 0

    return CExpressionActionMgr.Inst:DoExpressionAction(true, true, 0)
end

function CLuaScreenCaptureExpressionView:CanDoThisActionOffline(id)
    local expression = Expression_Define.GetData(id)
    if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.m_IsInHangZhouWater then
        if expression.HangZhouWaterCanBeUsed then
            return true
        else
            g_MessageMgr:ShowMessage("HANGZHOUWATER_EXPRESSION_CANNOTBEUSE")
            return false
        end
    end
    if expression.ExpressionWhenRide == 0 and CZuoQiMgr.IsOnZuoqi() then
        g_MessageMgr:ShowMessage("EXPRESSION_NOT_SHOW_ON_RIDE")
        return false
    end
    if expression.ExpressionWhenBoat == 0 then
        if (CStatusMgr.Inst:GetStatus(CClientMainPlayer.Inst, EPropStatus.GetId("TianGongGeShip")) == 1) or (CStatusMgr.Inst:GetStatus(CClientMainPlayer.Inst, EPropStatus.GetId("QingQiuTour")) == 1) then
            g_MessageMgr:ShowMessage("EXPRESSION_NOT_SHOW_ON_RIDE")
            return false
        end
    end
    return true
end
function CLuaScreenCaptureExpressionView:Update()
    if self.m_Paused then return end
    if self.m_DeltaTime > self.m_AniTotalTime then
        self.m_DeltaTime = self.m_AniTotalTime
        self:PauseOrPlay(true)
    end
    self.m_Slider.m_Value = self.m_DeltaTime / self.m_AniTotalTime
    self.m_Slider:SetPercentage()
    self.m_DeltaTime = self.m_DeltaTime + Time.deltaTime
end

function CLuaScreenCaptureExpressionView:OnSliderValueChanged(value)
    self.m_DeltaTime = value * self.m_AniTotalTime
    if self.m_CurrentTabIndex == 0 then
        CExpressionActionMgr.Inst:DoExpressionAction(true, true, value, false, self.m_Paused)
    elseif self.m_CurrentTabIndex == 1 then
        if self.m_CurrentExpressionId == CAS_Expression.s_FuxiExpressionId then
            self:ProcessOtherPlayerFuxiExpressionOffline()
            return
        end
        self:ShowOtherPlayerExpressionOffline()
    else
        if self.m_CurrentBabyRenderObject then
            CActionStateMgr.Inst:ShowActionState(self.m_CurrentBabyClientObject, EnumActionState.Idle, {})
            CExpressionActionMgr.HandleWeaponAndFxOnLeave(self.m_CurrentBabyClientObject, self.m_CurrentBabyExpressionDesignData)
            CExpressionActionMgr.HandleWeaponAndFxOnEnter(self.m_CurrentBabyClientObject, self.m_CurrentBabyExpressionDesignData)
            self.m_CurrentBabyRenderObject:Preview(self.m_CurrentBabyExpressionAniName, self.m_DeltaTime)
        end
    end
end

function CLuaScreenCaptureExpressionView:ProcessOtherPlayerFuxiExpressionOffline()
    local aniName = self.m_CurrentOtherPlayerObject:GetFuxiDanceName(self.m_CurrentFuxiMotionId)
    if CAniFastClipMgr.Inst:GetAppendClipByAniName(aniName) ~= nil then
        self:ShowOtherPlayerFuxiExpressionOffline(aniName)
        return
    elseif CAniFastClipMgr.Inst:IsLoadedAppend(aniName) then
        return 
    else 
        CAniFastClipMgr.Inst:LoadAppendClip(aniName, DelegateFactory.Action_CAnimationFastClip(function(clip)
            self.m_AniTotalTime = CAniFastClipMgr.Inst:GetAppendClipLengthByMotionId(self.m_CurrentOtherPlayerObject, self.m_CurrentFuxiMotionId)
            self.m_DeltaTime = 0
            self.m_Slider.m_Value = 0
            self.m_Slider.gameObject:SetActive(true)
            self:ShowOtherPlayerFuxiExpressionOffline(aniName)
        end))
        return
    end
end

function CLuaScreenCaptureExpressionView:MainPlayerMove(argv)
  self.gameObject:SetActive(false)
  if self.m_CurrentBabyClientObject then
    CExpressionActionMgr.HandleWeaponAndFxOnLeave(self.m_CurrentBabyClientObject, self.m_CurrentBabyExpressionDesignData);
  end
end

-- Baby Expression
RegistClassMember(CLuaScreenCaptureExpressionView, "m_CurrentBabyExpressionId")
RegistClassMember(CLuaScreenCaptureExpressionView, "m_CurrentBabyExpressionAniName")
RegistClassMember(CLuaScreenCaptureExpressionView, "m_CurrentBabyClientObject")
RegistClassMember(CLuaScreenCaptureExpressionView, "m_CurrentBabyRenderObject")
RegistClassMember(CLuaScreenCaptureExpressionView, "m_CurrentBabyExpressionDesignData")
function CLuaScreenCaptureExpressionView:InitBabyExpression()
    local haveBaby = CLuaScreenCaptureMgr.CurrentBabyEngineId and CLuaScreenCaptureMgr.CurrentBabyEngineId > 0
    local radioBoxRootObj = self.transform:Find("QnRadioBox").gameObject
    radioBoxRootObj.transform:Find("Baby").gameObject:SetActive(haveBaby)

    self.m_CurrentBabyExpressionId = 0
    self.m_CurrentBabyExpressionAniName = ""
    self.m_CurrentBabyClientObject = nil
    self.m_CurrentBabyRenderObject = nil

    if not haveBaby then return end

    local babyObject = CClientObjectMgr.Inst:GetObject(CLuaScreenCaptureMgr.CurrentBabyEngineId)
    self.m_CurrentBabyClientObject = babyObject
    self.m_CurrentBabyRenderObject = babyObject and babyObject.RO or nil
end

function CLuaScreenCaptureExpressionView:OnBabyExpressionClick(id)
  self.m_CurrentBabyExpressionId = id
  self.m_CurrentBabyExpressionDesignData = Expression_Define.GetData(id)

  if not self.m_CurrentBabyRenderObject then return end
  local data = Expression_Define.GetData(id)
  if data then
    self.m_CurrentBabyExpressionAniName = data.AniName
    self.m_AniTotalTime = self.m_CurrentBabyRenderObject:GetAniLength(data.AniName)
    self:PauseOrPlay(false)
    self.m_DeltaTime = 0
    self.m_Slider.gameObject:SetActive(true)
  end
end

-- Other Player
RegistClassMember(CLuaScreenCaptureExpressionView, "m_Object2PreExpressionIdTable")
RegistClassMember(CLuaScreenCaptureExpressionView, "m_CurrentOtherPlayerObject")
RegistClassMember(CLuaScreenCaptureExpressionView, "m_Object2ExpressionIdTable")
RegistClassMember(CLuaScreenCaptureExpressionView, "m_CurrentPreExpressionId")
RegistClassMember(CLuaScreenCaptureExpressionView, "m_CurrentExpressionId")
RegistClassMember(CLuaScreenCaptureExpressionView, "m_CurrentFuxiMotionId")
RegistClassMember(CLuaScreenCaptureExpressionView, "m_HaveRightsIdTable")

function CLuaScreenCaptureExpressionView:InitOtherPlayerExpression()
    self.m_Object2PreExpressionIdTable = {}
    self.m_Object2ExpressionIdTable = {}
    self.m_HaveRightsIdTable = {}
end

function CLuaScreenCaptureExpressionView:TargetAgreeToGroupPhoto(playerId, bUnLockXiaoYaoExpression)
    if not self.m_HaveRightsIdTable[playerId] then
        self.m_HaveRightsIdTable[playerId] = bUnLockXiaoYaoExpression and 1 or 0
    end
end

function CLuaScreenCaptureExpressionView:GetExpressionAnimationTime(clientObject, expressionId)
    if not clientObject or not clientObject.RO then return 0 end
    if expressionId == CExpressionActionMgr.s_LiangXiangExpressionId then
        local liangxiangAniName = CExpressionActionMgr.GetLiangXiangExpressionAniName(clientObject)
        local zhankuangMaskCheck = CExpressionActionMgr.GetZhanKuangLiangXiangMaskCheck(clientObject)
        if not liangxiangAniName or not string.match(liangxiangAniName, "liangxiang") or not zhankuangMaskCheck  then
           g_MessageMgr:ShowMessage("Expression_LiangXiang_No_Animation")
           return 0
       else
           return clientObject.RO:GetAniLength(liangxiangAniName)
       end
    end
    local data = Expression_Define.GetData(expressionId)
    if not data then return 0 end
    local animationName = data.AniName
    local len = clientObject.RO:GetAniLength(animationName)
    if data.NextExpressionID > 0 then
        len = len + self:GetExpressionAnimationTime(clientObject, data.NextExpressionID)
    end
    return len
end

function CLuaScreenCaptureExpressionView:OnMainPlayerExpressionClick(index)
    local expressionId = self.m_PlayerExpIdTable[index + 1] or CAS_Expression.s_FuxiExpressionId
    local expressionResult = true
    if expressionId ~= CAS_Expression.s_FuxiExpressionId then
        local data = Expression_Define.GetData(expressionId)
        if CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp.ResourceId > 0 and not CClientMainPlayer.Inst.RO:IsAniExist(data.AniName) then
            g_MessageMgr:ShowMessage("EXPRESSION_NO_ANI")
            return
        end

        CExpressionActionMgr.ExpressionActionID = expressionId
        expressionResult = self:StartOfflineExpression()
        if not expressionResult then
            self:PauseOrPlay(true)
        end
        self.m_Slider.gameObject:SetActive(expressionResult)
    else 
        -- 自定义动作
        local fuxiData = self.m_FuxiExpIdTable[index + 1 - #self.m_PlayerExpIdTable]
        if CClientMainPlayer.Inst.AppearanceGender == EnumGender.Monster or CClientMainPlayer.Inst.AppearanceProp.ResourceId > 0 then
            g_MessageMgr:ShowMessage("EXPRESSION_NO_ANI")
            return
        end
        if fuxiData.outtime or fuxiData.status == EnumFuxiDanceMotionStatus.eDeleted then
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("该动作已过期！"))
        else
            CExpressionActionMgr.ExpressionActionID = expressionId
            CExpressionActionMgr.FuxiExpressionMotionID = fuxiData.motionId
            local expressionResult = true
            expressionResult = self:StartOfflineFuxiExpression()
            if not expressionResult then
                self:PauseOrPlay(true)
            end
            self.m_Slider.gameObject:SetActive(expressionResult)
        end
    end

    if not self.m_bUseExpression then
        self.m_bUseExpression = true
        local dict = CreateFromClass(MakeGenericClass(Dictionary, String, Object))
        CommonDefs.DictAdd(dict, typeof(String), "UseMainPlayerExpression", typeof(Object), self.m_bUseExpression)
        Gac2Gas.RequestRecordClientLog("ScreenCaptureClientLog", MsgPackImpl.pack(dict))
    end
end

function CLuaScreenCaptureExpressionView:OnOtherPlayerExpressionClick(index)
    local id = self.m_OtherPlayerExpIdTable[index + 1] or CAS_Expression.s_FuxiExpressionId

    if (not self.m_SelectedOtherPlayerObject) or (not TypeAs(self.m_SelectedOtherPlayerObject, typeof(CClientOtherPlayer))) then
        g_MessageMgr:ShowMessage("Please_Pick_OtherPlayer")
        return
    end

    if not self.m_HaveRightsIdTable[self.m_SelectedOtherPlayerObject.PlayerId] then
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("GroupPhoto_Ask_Confirm"), DelegateFactory.Action(function()
            Gac2Gas.RequestGroupPhotoWithOther(self.m_SelectedOtherPlayerObject.PlayerId, id)
        end), nil, nil, nil, false)
        return
    end

    local isMonster = self.m_SelectedOtherPlayerObject.AppearanceGender == EnumGender.Monster
    if isMonster and self.m_HaveRightsIdTable[self.m_SelectedOtherPlayerObject.PlayerId] == 0 then
        g_MessageMgr:ShowMessage("CUSTOM_STRING1", LocalString.GetString("该玩家尚未获得阿乌表情动作的使用权"))
        return
    end

    if id ~= CAS_Expression.s_FuxiExpressionId then
        self.m_AniTotalTime = self:GetExpressionAnimationTime(self.m_SelectedOtherPlayerObject, id)
        if self.m_AniTotalTime <= 0 then return end

        self.m_CurrentOtherPlayerObject = self.m_SelectedOtherPlayerObject
        local preExpId = self.m_Object2ExpressionIdTable[self.m_CurrentOtherPlayerObject]
        if preExpId then
            self.m_Object2PreExpressionIdTable[self.m_CurrentOtherPlayerObject] = preExpId
        end
        self.m_Object2ExpressionIdTable[self.m_CurrentOtherPlayerObject] = id
        self.m_CurrentPreExpressionId = preExpId
        self.m_CurrentExpressionId = id

        self:PauseOrPlay(false)
        self.m_DeltaTime = 0
        self.m_Slider.gameObject:SetActive(true)
    else 
        -- 自定义动作
        local fuxiData = self.m_FuxiExpIdTable[index + 1 - #self.m_OtherPlayerExpIdTable]
        if CClientMainPlayer.Inst.AppearanceGender == EnumGender.Monster or CClientMainPlayer.Inst.AppearanceProp.ResourceId > 0 then
            g_MessageMgr:ShowMessage("EXPRESSION_NO_ANI")
            return
        end
        if fuxiData.outtime or fuxiData.status == EnumFuxiDanceMotionStatus.eDeleted then
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("该动作已过期！"))
            return
        else
            local expressionResult = true
            
            self.m_CurrentOtherPlayerObject = self.m_SelectedOtherPlayerObject
            self.m_AniTotalTime = CAniFastClipMgr.Inst:GetAppendClipLengthByMotionId(self.m_CurrentOtherPlayerObject, fuxiData.motionId)
            local preExpId = self.m_Object2ExpressionIdTable[self.m_CurrentOtherPlayerObject]
            if preExpId then
                self.m_Object2PreExpressionIdTable[self.m_CurrentOtherPlayerObject] = preExpId
            end
            self.m_Object2ExpressionIdTable[self.m_CurrentOtherPlayerObject] = id
            self.m_CurrentPreExpressionId = preExpId
            self.m_CurrentExpressionId = id
            self.m_CurrentFuxiMotionId = fuxiData.motionId

            local aniName = self.m_CurrentOtherPlayerObject:GetFuxiDanceName(self.m_CurrentFuxiMotionId)
            local isLoaded = CAniFastClipMgr.Inst:GetAppendClipByAniName(aniName) ~= nil
            self:PauseOrPlay(false)
            self.m_DeltaTime = 0
            self.m_Slider.gameObject:SetActive(isLoaded)
        end
    end

    if not self.m_bUseOtherExpression then
        self.m_bUseOtherExpression = true
        local dict = CreateFromClass(MakeGenericClass(Dictionary, String, Object))
		CommonDefs.DictAdd(dict, typeof(String), "UseOtherPlayerExpression", typeof(Object), self.m_bUseOtherExpression)
		Gac2Gas.RequestRecordClientLog("ScreenCaptureClientLog", MsgPackImpl.pack(dict))
    end
end

function CLuaScreenCaptureExpressionView:ShowOtherPlayerExpressionOffline()
    if (not self.m_SelectedOtherPlayerObject) or (not TypeAs(self.m_SelectedOtherPlayerObject, typeof(CClientOtherPlayer))) then return end
    local ro = self.m_SelectedOtherPlayerObject.RO
    if not ro then return end
    if self.m_SelectedOtherPlayerObject ~= self.m_CurrentOtherPlayerObject or self.m_SelectedOtherPlayerObject.IsMoving or self.m_SelectedOtherPlayerObject.IsCasting then
        self:PauseOrPlay(true)
        if self.m_CurrentOtherPlayerObject then
            local curData = self.m_CurrentExpressionId and Expression_Define.GetData(self.m_CurrentExpressionId)
            if curData then
                CExpressionActionMgr.HandleWeaponAndFxOnLeave(self.m_CurrentOtherPlayerObject, curData)
            end
        end
        return
    end

    local data = Expression_Define.GetData(self.m_CurrentExpressionId)
    if not data then return end
    local aniName = data.AniName
    if self.m_CurrentExpressionId == CExpressionActionMgr.s_LiangXiangExpressionId then
        local liangxiangAniName = CExpressionActionMgr.GetLiangXiangExpressionAniName(self.m_SelectedOtherPlayerObject);
        if not liangxiangAniName or liangxiangAniName == "" then
           return
       else
           aniName = liangxiangAniName
       end
    end
    local time = self.m_DeltaTime
    local curAniLen = ro:GetAniLength(aniName)
    while time > curAniLen do
        time = time - curAniLen
        local nextData = Expression_Define.GetData(data.NextExpressionID)
        if not nextData then return end
        data = nextData
        aniName = nextData.AniName
        curAniLen = ro:GetAniLength(aniName)
    end
    local preData = self.m_CurrentPreExpressionId and Expression_Define.GetData(self.m_CurrentPreExpressionId)
    if preData then
        CExpressionActionMgr.HandleWeaponAndFxOnLeave(self.m_SelectedOtherPlayerObject, preData)
        self.m_CurrentPreExpressionId = nil
    end
    CActionStateMgr.Inst:ShowActionState(self.m_SelectedOtherPlayerObject, EnumActionState.Idle, {})

    if time <= 0.1 then
        CExpressionActionMgr.HandleWeaponAndFxOnEnter(self.m_SelectedOtherPlayerObject, data)
    end
    ro:Preview(aniName, time)
end

function CLuaScreenCaptureExpressionView:ShowOtherPlayerFuxiExpressionOffline(fuxiAniName)
    if (not self.m_SelectedOtherPlayerObject) or (not TypeAs(self.m_SelectedOtherPlayerObject, typeof(CClientOtherPlayer))) then return end
    local ro = self.m_SelectedOtherPlayerObject.RO
    if not ro then return end
    if self.m_SelectedOtherPlayerObject ~= self.m_CurrentOtherPlayerObject or self.m_SelectedOtherPlayerObject.IsMoving or self.m_SelectedOtherPlayerObject.IsCasting then
        self:PauseOrPlay(true)
        if self.m_CurrentOtherPlayerObject then
            local curData = self.m_CurrentExpressionId and Expression_Define.GetData(self.m_CurrentExpressionId)
            if curData then
                CExpressionActionMgr.HandleWeaponAndFxOnLeave(self.m_CurrentOtherPlayerObject, curData)
            end
        end
        return
    end

    local data = Expression_Define.GetData(self.m_CurrentExpressionId)
    if not data then return end
    local aniName = fuxiAniName

    local time = self.m_DeltaTime
    local curAniLen = ro:GetAniLength(aniName)

    local preData = self.m_CurrentPreExpressionId and Expression_Define.GetData(self.m_CurrentPreExpressionId)
    if preData then
        CExpressionActionMgr.HandleWeaponAndFxOnLeave(self.m_SelectedOtherPlayerObject, preData)
        self.m_CurrentPreExpressionId = nil
    end
    CActionStateMgr.Inst:ShowActionState(self.m_SelectedOtherPlayerObject, EnumActionState.Idle, {})

    if time <= 0.1 then
        CExpressionActionMgr.HandleWeaponAndFxOnEnter(self.m_SelectedOtherPlayerObject, data)
    end
    ro:Preview(aniName, time)
end

function CLuaScreenCaptureExpressionView:OnClientObjectSelected(argv)
    local clientObject = argv[0]
    if (not clientObject) or (not TypeAs(clientObject, typeof(CClientOtherPlayer))) then
        if self.m_SelectedOtherPlayerObject then
            self.m_SelectedOtherPlayerObject:ShowSelectedFX(false, false)
        end
         self.m_SelectedOtherPlayerObject = nil 
    return end
    self.m_OtherPlayerExpIdTable = {}
    local classId = EnumToInt(clientObject.Class) * 100 + EnumToInt(clientObject.AppearanceGender)
    local isMonster = clientObject.AppearanceGender == EnumGender.Monster
    Expression_Show.Foreach(function(k, v)
        local needAdd = false
        if v.PaiZhaoEnable > 0 then
            if isMonster then
                if v.YingLingYaoExpression > 0 then
                    needAdd = true
                end
            elseif not (v.LockGroup ~= 0 and CExpressionActionMgr.Inst.UnLockGroupInfo[v.LockGroup] == 0) and (not v.RaceCheck or CommonDefs.HashSetContains(v.RaceCheck, TypeOfUInt32, classId)) then
                needAdd = true
            end
        end
        if needAdd then
            table.insert(self.m_OtherPlayerExpIdTable, k)
        end
    end)

    if self.m_CurrentTabIndex == 1 then
        self.m_AdvGridView:ReloadData(true,false)
        self:PauseOrPlay(true)
    end

    if self.m_SelectedOtherPlayerObject then
        self.m_SelectedOtherPlayerObject:ShowSelectedFX(false, false)
    end
    clientObject:ShowSelectedFX(true, false)
    
    self.m_SelectedOtherPlayerObject = clientObject
end

RegistClassMember(CLuaScreenCaptureExpressionView, "m_FuxiExpIdTable")

function CLuaScreenCaptureExpressionView:InitFuxiExpression()
    if not LuaFuxiAniMgr.IsOpen or LuaFuxiAniMgr.IsFuxiLock() then
        return
    end
    LuaFuxiAniMgr.InitFuxiData()
end

function CLuaScreenCaptureExpressionView:OnGetFuxiPlayerOwnData()
    if not LuaFuxiAniMgr.IsOpen or LuaFuxiAniMgr.IsFuxiLock() then
        return
    end
    self.m_FuxiExpIdTable = {}

    if LuaFuxiAniMgr.FuxiData then
        for i=1,#LuaFuxiAniMgr.FuxiData do
            if LuaFuxiAniMgr.FuxiData[i].motionId ~= 0 then --过滤未上传的动作
                table.insert(self.m_FuxiExpIdTable, LuaFuxiAniMgr.FuxiData[i])
            end
        end
    end
    
    self.m_AdvGridView:ReloadData(true,false)
end

function CLuaScreenCaptureExpressionView:OnExpressionActionMgrLoadAppendClipFinished()
    self.m_AniTotalTime = CAniFastClipMgr.Inst:GetAppendClipLengthByMotionId(CClientMainPlayer.Inst, CExpressionActionMgr.FuxiExpressionMotionID)
    self:PauseOrPlay(false)
    self.m_DeltaTime = 0
    self.m_Slider.m_Value = 0
    self.m_Slider.gameObject:SetActive(true)
end

function CLuaScreenCaptureExpressionView:OnSendFashionTransformState()
    self:InitPlayerExpIdTable()
    self.m_AdvGridView:ReloadData(true,false)
end
