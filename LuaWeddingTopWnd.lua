local UILabel            = import "UILabel"
local DelegateFactory    = import "DelegateFactory"
local EnumGender         = import "L10.Game.EnumGender"
local CServerTimeMgr     = import "L10.Game.CServerTimeMgr"
local CTopAndRightTipWnd = import "L10.UI.CTopAndRightTipWnd"
local CClientMainPlayer  = import "L10.Game.CClientMainPlayer"
local UIEventListener    = import "UIEventListener"
local MessageWndManager  = import "L10.UI.MessageWndManager"
local LuaUtils           = import "LuaUtils"
local CUIManager         = import "L10.UI.CUIManager"
local TweenPosition      = import "TweenPosition"
local UIBasicSprite      = import "UIBasicSprite"
local CScene             = import "L10.Game.CScene"
local CEffectMgr         = import "L10.Game.CEffectMgr"
local EnumWarnFXType     = import "L10.Game.EnumWarnFXType"
local CClientNpc         = import "L10.Game.CClientNpc"
local CClientObjectMgr   = import "L10.Game.CClientObjectMgr"
local CButton            = import "L10.UI.CButton"
local Vector2            = import "UnityEngine.Vector2"
local Vector3            = import "UnityEngine.Vector3"
local UIScrollView       = import "UIScrollView"

LuaWeddingTopWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaWeddingTopWnd, "detailTween")
RegistClassMember(LuaWeddingTopWnd, "arrow")
RegistClassMember(LuaWeddingTopWnd, "scrollView")
RegistClassMember(LuaWeddingTopWnd, "grid")
RegistClassMember(LuaWeddingTopWnd, "dragRegion")
RegistClassMember(LuaWeddingTopWnd, "bottom")
RegistClassMember(LuaWeddingTopWnd, "description")
RegistClassMember(LuaWeddingTopWnd, "leftButton")
RegistClassMember(LuaWeddingTopWnd, "leftButtonLabel")
RegistClassMember(LuaWeddingTopWnd, "leftLabel")
RegistClassMember(LuaWeddingTopWnd, "centerButton")
RegistClassMember(LuaWeddingTopWnd, "centerButtonLabel")
RegistClassMember(LuaWeddingTopWnd, "centerButtonPoint")
RegistClassMember(LuaWeddingTopWnd, "centerLabel")
RegistClassMember(LuaWeddingTopWnd, "rightButton")
RegistClassMember(LuaWeddingTopWnd, "rightButtonLabel")
RegistClassMember(LuaWeddingTopWnd, "rightLabel")

RegistClassMember(LuaWeddingTopWnd, "expandButton")
RegistClassMember(LuaWeddingTopWnd, "baiTangButton")
RegistClassMember(LuaWeddingTopWnd, "baiTangLabel")

RegistClassMember(LuaWeddingTopWnd, "dragStartX") -- 拖拽开始的x坐标
RegistClassMember(LuaWeddingTopWnd, "stages")
RegistClassMember(LuaWeddingTopWnd, "goingStage") -- 当前进行到哪一个阶段
RegistClassMember(LuaWeddingTopWnd, "selectedStage") -- 选择了哪一个阶段
RegistClassMember(LuaWeddingTopWnd, "gender") -- 玩家性别
RegistClassMember(LuaWeddingTopWnd, "partnerGender") -- 伴侣性别
RegistClassMember(LuaWeddingTopWnd, "isGroomOrBride")

RegistClassMember(LuaWeddingTopWnd, "baiTangProgress") -- 拜堂进入到了哪一个阶段
RegistClassMember(LuaWeddingTopWnd, "countDownTick") -- 通用的倒计时
RegistClassMember(LuaWeddingTopWnd, "endTimeStamp")
RegistClassMember(LuaWeddingTopWnd, "showPreGameTick") -- 进入副本一段时间自动下拉界面
RegistClassMember(LuaWeddingTopWnd, "msgTick") -- 提示间隔
RegistClassMember(LuaWeddingTopWnd, "baiTangShowFxTick1") -- 拜堂特效
RegistClassMember(LuaWeddingTopWnd, "baiTangShowFxTick2")
RegistClassMember(LuaWeddingTopWnd, "caiGuangFx") -- 彩光特效
RegistClassMember(LuaWeddingTopWnd, "xianNiaoFx") -- 仙鸟特效
RegistClassMember(LuaWeddingTopWnd, "yanHuaFx") -- 烟花特效

function LuaWeddingTopWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

	self:InitUIComponents()
	self:InitEventListener()
	self:InitActive()
end

-- 初始化UI组件
function LuaWeddingTopWnd:InitUIComponents()
    local anchor = self.transform:Find("Anchor")
	self.detailTween = anchor:Find("Detail"):GetComponent(typeof(TweenPosition))
	self.arrow = anchor:Find("Arrow"):GetComponent(typeof(UITexture))
	self.scrollView = anchor:Find("Stage/ScrollView")
	self.grid = self.scrollView:Find("Grid")
	self.dragRegion = anchor:Find("Stage/DragRegion").gameObject

	self.description = anchor:Find("Detail/Description"):GetComponent(typeof(UILabel))
	self.bottom = anchor:Find("Detail/Bottom")
	local left = self.bottom:Find("Left")
	local center = self.bottom:Find("Center")
	local right = self.bottom:Find("Right")
	self.leftButton = left:Find("Button").gameObject
	self.leftButtonLabel = self.leftButton.transform:Find("Label"):GetComponent(typeof(UILabel))
	self.leftLabel = left:Find("Label"):GetComponent(typeof(UILabel))
	self.centerButton = center:Find("Button").gameObject
	self.centerButtonLabel = self.centerButton.transform:Find("Label"):GetComponent(typeof(UILabel))
	self.centerButtonPoint = self.centerButton.transform:Find("Point").gameObject
	self.centerLabel = center:Find("Label"):GetComponent(typeof(UILabel))
	self.rightButton = right:Find("Button").gameObject
	self.rightButtonLabel = self.rightButton.transform:Find("Label"):GetComponent(typeof(UILabel))
	self.rightLabel = right:Find("Label"):GetComponent(typeof(UILabel))

	self.baiTangLabel = self.transform:Find("RightButton/BaiTangLabel"):GetComponent(typeof(UILabel))
	self.baiTangButton = self.baiTangLabel.transform:Find("BaiTangButton").gameObject
	self.expandButton = self.transform:Find("ExpandButton").gameObject
end

-- 初始化点击响应
function LuaWeddingTopWnd:InitEventListener()
	UIEventListener.Get(self.leftButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeftButtonClick()
	end)

	UIEventListener.Get(self.centerButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCenterButtonClick()
	end)

	UIEventListener.Get(self.rightButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRightButtonClick()
	end)

	UIEventListener.Get(self.baiTangButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBaiTangButtonClick()
	end)

	UIEventListener.Get(self.arrow.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnArrowClick()
	end)

	UIEventListener.Get(self.expandButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnExpandButtonClick()
	end)
end

-- 初始化active
function LuaWeddingTopWnd:InitActive()
	self.bottom.gameObject:SetActive(false)
	self.baiTangLabel.gameObject:SetActive(false)
end


function LuaWeddingTopWnd:OnEnable()
	g_ScriptEvent:AddListener("SyncNewWeddingSceneInfo", self, "OnSyncSceneInfo")
	g_ScriptEvent:AddListener("SyncNewWeddingStageInfo", self, "OnSyncNewWeddingStageInfo")
	g_ScriptEvent:AddListener("SyncPartnerSkipNewWeddingPreGame", self, "OnSyncPartnerSkipPreGame")
	g_ScriptEvent:AddListener("SyncPartnerStartNewWeddingPreGame", self, "OnSyncPartnerStartPreGame")
	g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
	g_ScriptEvent:AddListener("ShowNewWeddingBaiTangButton", self, "OnShowBaiTangButton")
	g_ScriptEvent:AddListener("SyncPartnerSkipNewWeddingYiXianQian", self, "OnSyncPartnerSkipYiXianQian")
	g_ScriptEvent:AddListener("SyncPartnerSkipNewWeddingNaoDongFang", self, "OnSyncPartnerSkipNaoDongFang")
	g_ScriptEvent:AddListener("SyncPartnerEndNewWedding", self, "OnSyncPartnerEndWedding")
	g_ScriptEvent:AddListener("NewWeddingSyncBaiTangEndFx", self, "OnSyncBaiTangEndFx")
	g_ScriptEvent:AddListener("NewWeddingSyncPregameRightAnswer", self, "OnSyncPregameRightAnswer")
	g_ScriptEvent:AddListener("NewWeddingSyncSignupDone", self, "OnSyncSignupDone")
end

function LuaWeddingTopWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SyncNewWeddingSceneInfo", self, "OnSyncSceneInfo")
	g_ScriptEvent:RemoveListener("SyncNewWeddingStageInfo", self, "OnSyncNewWeddingStageInfo")
	g_ScriptEvent:RemoveListener("SyncPartnerSkipNewWeddingPreGame", self, "OnSyncPartnerSkipPreGame")
	g_ScriptEvent:RemoveListener("SyncPartnerStartNewWeddingPreGame", self, "OnSyncPartnerStartPreGame")
	g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
	g_ScriptEvent:RemoveListener("ShowNewWeddingBaiTangButton", self, "OnShowBaiTangButton")
	g_ScriptEvent:RemoveListener("SyncPartnerSkipNewWeddingYiXianQian", self, "OnSyncPartnerSkipYiXianQian")
	g_ScriptEvent:RemoveListener("SyncPartnerSkipNewWeddingNaoDongFang", self, "OnSyncPartnerSkipNaoDongFang")
	g_ScriptEvent:RemoveListener("SyncPartnerEndNewWedding", self, "OnSyncPartnerEndWedding")
	g_ScriptEvent:RemoveListener("NewWeddingSyncBaiTangEndFx", self, "OnSyncBaiTangEndFx")
	g_ScriptEvent:RemoveListener("NewWeddingSyncPregameRightAnswer", self, "OnSyncPregameRightAnswer")
	g_ScriptEvent:RemoveListener("NewWeddingSyncSignupDone", self, "OnSyncSignupDone")
end

-- 同步场景信息
function LuaWeddingTopWnd:OnSyncSceneInfo()
	self:UpdateDetailBottom()
end

-- 同步阶段信息
function LuaWeddingTopWnd:OnSyncNewWeddingStageInfo()
	self:UpdateStage()

	local stage = LuaWeddingIterationMgr.stage
	local stageInfo = LuaWeddingIterationMgr.stageInfo
	if stage ~= 1 or stageInfo[1] ~= "Init" then
		self.msgTick = self:ClearTick(self.msgTick)
	end
end

-- 同步伴侣跳过试真心
function LuaWeddingTopWnd:OnSyncPartnerSkipPreGame()
	local str = self:GetPartnerDisplay()
	local message = g_MessageMgr:FormatMessage("WEDDING_PARTNER_CHOOSE_SKIP_PREGAME", str)
	MessageWndManager.ShowOKCancelMessage(message,
		DelegateFactory.Action(function ()
			Gac2Gas.RequestSkipNewWeddingPreGame()
		end),
		DelegateFactory.Action(function ()
			Gac2Gas.RequestPostponeNewWeddingPreGame()
		end), LocalString.GetString("跳过"),LocalString.GetString("再想想"), false)

	self.msgTick = self:ClearTick(self.msgTick)
end

-- 同步伴侣开启试真心
function LuaWeddingTopWnd:OnSyncPartnerStartPreGame()
	local str = self:GetPartnerDisplay()
	local message = g_MessageMgr:FormatMessage("WEDDING_OPEN_SHIZHENXIN", str)
	MessageWndManager.ShowOKCancelMessage(message,
		DelegateFactory.Action(function ()
			Gac2Gas.RequestStartNewWeddingPreGame()
		end),
		DelegateFactory.Action(function ()
			Gac2Gas.RequestPostponeNewWeddingPreGame()
		end), LocalString.GetString("开启"),LocalString.GetString("再想想"), false)

	self.msgTick = self:ClearTick(self.msgTick)
end

-- 婚前游戏同步正确的数量
function LuaWeddingTopWnd:OnSyncPregameRightAnswer(rightCount)
	local stage = LuaWeddingIterationMgr.stage
	local stageInfo = LuaWeddingIterationMgr.stageInfo
	if stage == 1 and stageInfo[1] == "AnswerQuestion" then
		local totalCount = WeddingIteration_Setting.GetData().WeddingSceneFakeBrideCount
		self.leftLabel.text = SafeStringFormat3(LocalString.GetString("已答对题目 %d/%d"), rightCount, totalCount)
	end
end

-- 同步伴侣请求跳过姻缘一线牵
function LuaWeddingTopWnd:OnSyncPartnerSkipYiXianQian()
	local str = self:GetPartnerDisplay()
	local message = g_MessageMgr:FormatMessage("WEDDING_PARTNER_CHOOSE_SKIP_YIXIANQIAN", str)
	MessageWndManager.ShowOKCancelMessage(message,
		DelegateFactory.Action(function ()
			Gac2Gas.NewWeddingRequestSkipYiXianQian()
		end),
		DelegateFactory.Action(function ()
			Gac2Gas.RequestPostponeNewWeddingYiXianQian()
		end), LocalString.GetString("跳过"),LocalString.GetString("再想想"), false)
end

-- 同步伴侣请求跳过闹洞房
function LuaWeddingTopWnd:OnSyncPartnerSkipNaoDongFang()
	local str = self:GetPartnerDisplay()
	local message = g_MessageMgr:FormatMessage("WEDDING_PARTNER_CHOOSE_SKIP_NAODONGFANG", str)
	MessageWndManager.ShowOKCancelMessage(message,
		DelegateFactory.Action(function ()
			Gac2Gas.NewWeddingRequestSkipNaoDongFang()
		end),
		DelegateFactory.Action(function ()
			Gac2Gas.RequestPostponeNewWeddingNaoDongFang()
		end), LocalString.GetString("跳过"),LocalString.GetString("再想想"), false)
end

-- 获取伴侣的姓名显示，新郎/新娘+姓名
function LuaWeddingTopWnd:GetPartnerDisplay()
	local str
	local sceneInfo = LuaWeddingIterationMgr.sceneInfo

	if self.partnerGender == EnumGender.Male then
		str = SafeStringFormat3(LocalString.GetString("[519FFF]新郎[-]%s"), sceneInfo.groomName)
	else
		str = SafeStringFormat3(LocalString.GetString("[FF5050]新娘[-]%s"), sceneInfo.brideName)
	end
	return str
end

function LuaWeddingTopWnd:OnHideTopAndRightTipWnd()
    LuaUtils.SetLocalRotation(self.expandButton.transform, 0, 0, 180)
end

-- 显示拜堂按钮
function LuaWeddingTopWnd:OnShowBaiTangButton(progress)
	if progress < 1 then
		self.baiTangLabel.gameObject:SetActive(false)
		return
	end

	local baiTangButtonDisplay = WeddingIteration_Setting.GetData().BaiTangButtonDisplay
	local display = LuaWeddingIterationMgr:Array2Tbl(baiTangButtonDisplay)
	self.baiTangProgress = progress
	self.baiTangLabel.gameObject:SetActive(true)
	self.baiTangLabel.text = display[progress]
end

-- 伴侣请求结束婚礼
function LuaWeddingTopWnd:OnSyncPartnerEndWedding()
	local message = g_MessageMgr:FormatMessage("WEDDING_PARTNER_CHOOSE_END_WEDDING")
	MessageWndManager.ShowOKCancelMessage(message,
		DelegateFactory.Action(function ()
			Gac2Gas.NewWeddingRequestEndWedding()
		end),
		DelegateFactory.Action(function ()
			Gac2Gas.NewWeddingRequestNotEndWedding()
		end), LocalString.GetString("结束婚礼"),LocalString.GetString("不结束"), false)
end

-- 拜堂结束特效
function LuaWeddingTopWnd:OnSyncBaiTangEndFx(stage)
	if stage == 3 then
		self:ShowBaiTangEndFx()
	end
end

-- 同步报名结果
function LuaWeddingTopWnd:OnSyncSignupDone()
	self.centerButton.transform:GetComponent(typeof(CButton)).Enabled = false
end


function LuaWeddingTopWnd:Init()
	self.detailTween.transform.localPosition = self.detailTween.to
	self.arrow.flip = UIBasicSprite.Flip.Vertically

	self:InitClassMembers()
	self:InitStage()
	self:UpdateStage()
end

-- 初始化成员变量
function LuaWeddingTopWnd:InitClassMembers()
	self.goingStage = -1
	self.selectedStage = -1

	self.gender = CClientMainPlayer.Inst.Gender
	self.partnerGender = self.gender == EnumGender.Male and EnumGender.Female or EnumGender.Male
end

-- 初始化阶段
function LuaWeddingTopWnd:InitStage()
	self.stages = {}
	WeddingIteration_Stage.Foreach(function(id, data)
		local item = self.grid:GetChild(id - 1)
		local label = item:Find("Label"):GetComponent(typeof(UILabel))
		label.text = LocalString.GetString(data.Name)
		item:Find("Selected").gameObject:SetActive(false)
		item:Find("Going").gameObject:SetActive(false)

		UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			self:OnStageClick(id)
		end)

		self.stages[id] = item
	end)

	self:SetStageColor()
end

-- 居中
function LuaWeddingTopWnd:CenterOn(stageId)
	if self.stages[stageId] == nil then return end

	local panel = self.scrollView:GetComponent(typeof(UIPanel))

	local x = 106
	if stageId <= 4 then
		x = 106
	else
		x = -383
	end
	Extensions.SetLocalPositionX(self.scrollView, x)
	panel.clipOffset = Vector2(-x + 80, panel.clipOffset.y)
end


-- 更新阶段
function LuaWeddingTopWnd:UpdateStage()
	self:UpdateGoingStage(LuaWeddingIterationMgr.stage)
	self:UpdateDetailBottom()
end

-- 改变进行中的阶段
function LuaWeddingTopWnd:UpdateGoingStage(id)
	if id <= 0 then
		self.goingStage = -1
		return
	end

	-- 之前的置为false
	self:SetGoingActive(false)

	self:CenterOn(id)
	self.goingStage = id

	if id == 1 then
		self:InitShowPreGame()
	else
		self:UpdateSelectedStage(id)
	end

	-- 现在的置为true
	self:SetGoingActive(true)
end


-- 切换选择
function LuaWeddingTopWnd:UpdateSelectedStage(id)
	if id == 1 then
		self.showPreGameTick = self:ClearTick(self.showPreGameTick)
	end

	-- 之前的置为false
	self:SetSelectedActive(false)

	-- 现在不选
	if id <= 0 then
		self:ShowCloseDetailWnd(false)
		self.selectedStage = -1
		self:SetStageColor()
		return
	end

	self:ShowCloseDetailWnd(true)
	self.selectedStage = id
	self.description.text = WeddingIteration_Stage.GetData(id).BriefDescription
	self.bottom.gameObject:SetActive(self.selectedStage == self.goingStage)

	-- 现在的置为true
	self:SetSelectedActive(true)
	self:SetStageColor()
end

-- 设置Selected状态
function LuaWeddingTopWnd:SetSelectedActive(active)
	if self.selectedStage > 0 and self.stages[self.selectedStage] then
		local item = self.stages[self.selectedStage].transform
		item:Find("Selected").gameObject:SetActive(active)
	end
end

-- 设置Going状态
function LuaWeddingTopWnd:SetGoingActive(active)
	if self.goingStage > 0 and self.stages[self.goingStage] then
		local item = self.stages[self.goingStage].transform
		local going = item:Find("Going")
		going.gameObject:SetActive(active)
		local cuiFx = going:Find("Fx"):GetComponent(typeof(CUIFx))
		cuiFx.ScrollView = self.scrollView:GetComponent(typeof(UIScrollView))

		if active then
			cuiFx:DestroyFx()
			cuiFx:LoadFx(g_UIFxPaths.WeddingStageGoingFxPath)
		end

		self:SetStageColor()
	end
end

-- 更新细节底部显示
function LuaWeddingTopWnd:UpdateDetailBottom()
	local stage = LuaWeddingIterationMgr.stage
	local stageInfo = LuaWeddingIterationMgr.stageInfo

	local isGroom, isBride
	self.isGroomOrBride, isGroom, isBride = LuaWeddingIterationMgr:IsGroomOrBride()

	if #stageInfo == 0 or self.isGroomOrBride == nil then
		self:SetBottomActive(false, false, false, false, false, false)
		return
	end

	if stage == 1 then
		self:UpdatePreGameStage(stageInfo)
	elseif stage == 3 then
		self:UpdateFeastStage(isGroom, stageInfo)
	elseif stage == 4 then
		self:UpdateFlowerBallStage(isBride, stageInfo)
	elseif stage == 5 then
		self:UpdateYiXianQianStage(stageInfo)
	elseif stage == 6 then
		self:UpdateDongFangStage(stageInfo)
	elseif stage == 7 then
		self:UpdateEndStage()
	else
		self:SetBottomActive(false, false, false, false, false, false)
	end
end

-- 设置底部组件的active
function LuaWeddingTopWnd:SetBottomActive(leftButtonActive, leftLabelActive, centerButtonActive,
	centerLabelActive, rightButtonActive, rightLabelActive)
	self.leftButton:SetActive(leftButtonActive)
	self.leftLabel.gameObject:SetActive(leftLabelActive)
	self.centerButton:SetActive(centerButtonActive)
	self.centerLabel.gameObject:SetActive(centerLabelActive)
	self.rightButton:SetActive(rightButtonActive)
	self.rightLabel.gameObject:SetActive(rightLabelActive)

	if centerButtonActive then
		self.centerButton.transform:GetComponent(typeof(CButton)).Enabled = true
	end
end

-- 婚前仪式
function LuaWeddingTopWnd:UpdatePreGameStage(stageInfo)
	local subStage = stageInfo[1]
	local totalCount = WeddingIteration_Setting.GetData().WeddingSceneFakeBrideCount
	if not self:UpdateStageCommon(stageInfo) then
		if subStage == "SetAnswer" then
			self:SetBottomActive(false, true, false, false, false, false)
			self.leftLabel.text = SafeStringFormat3(LocalString.GetString("已答对题目 %d/%d"), 0, totalCount)
		elseif subStage == "AnswerQuestion" then
			self:SetBottomActive(false, true, false, false, false, true)
			local rightCount = stageInfo[3]
			self.leftLabel.text = SafeStringFormat3(LocalString.GetString("已答对题目 %d/%d"), rightCount, totalCount)
			self.endTimeStamp = stageInfo[2]
			self:StartCountDown()
		elseif subStage == "End" then
			self:SetBottomActive(false, false, false, true, false, false)
			self.centerLabel.text = LocalString.GetString("试真心已结束")
		end
	end
end

-- 酒席
function LuaWeddingTopWnd:UpdateFeastStage(isGroom, stageInfo)
	if not isGroom then
		self:SetBottomActive(false, false, false, false, false, false)
		return
	end

	local subStage = stageInfo[1]
	if subStage == "Init" then
		self:SetBottomActive(false, false, true, false, false, false)
		self.centerButtonLabel.text = LocalString.GetString("开启酒席")
		self.centerButtonPoint:SetActive(false)
	else
		self:SetBottomActive(false, false, false, false, false, false)
	end
end

-- 抛花球
function LuaWeddingTopWnd:UpdateFlowerBallStage(isBride, stageInfo)
	local subStage = stageInfo[1]
	if subStage == "Init" and isBride then
		self:SetBottomActive(true, false, false, false, true, false)
		self.leftButtonLabel.text = LocalString.GetString("跳过此步")
		self.rightButtonLabel.text = LocalString.GetString("开启玩法")
	elseif subStage == "Opened" then
		self:SetBottomActive(false, false, false, false, false, true)
		self.endTimeStamp = stageInfo[2]
		self:StartCountDown()
	else
		self:SetBottomActive(false, false, false, false, false, false)
	end
end

-- 姻缘一线牵
function LuaWeddingTopWnd:UpdateYiXianQianStage(stageInfo)
	if not self:UpdateStageCommon(stageInfo) then
		self:SetBottomActive(false, false, false, false, false, false)
	end
end

-- 闹洞房
function LuaWeddingTopWnd:UpdateDongFangStage(stageInfo)
	local subStage = stageInfo[1]
	if subStage == "Init" then
		if self.isGroomOrBride then
			self:SetBottomActive(true, false, false, false, true, false)
			self.leftButtonLabel.text = LocalString.GetString("跳过此步")
			self.rightButtonLabel.text = LocalString.GetString("开启婚房")
		else
			self:SetBottomActive(false, false, false, true, false, false)
			self.centerLabel.text = LocalString.GetString("等待新郎、新娘开启婚房")
		end
	elseif subStage == "SceneCreated" then
		if self.isGroomOrBride then
			self:SetBottomActive(false, false, false, false, false, false)
		else
			self:SetBottomActive(false, false, true, false, false, false)
			self.centerButtonLabel.text = LocalString.GetString("进入婚房")
			self.centerButtonPoint:SetActive(false)
		end
	else
		self:SetBottomActive(false, false, false, false, false, false)
	end
end

-- 结束婚礼
function LuaWeddingTopWnd:UpdateEndStage()
	if self.isGroomOrBride then
		self:SetBottomActive(false, false, true, false, false, false)
		self.centerButtonLabel.text = LocalString.GetString("结束婚礼")
		self.centerButtonPoint:SetActive(false)
	else
		self:SetBottomActive(false, false, false, false, false, false)
	end
end

-- 共有的处理
function LuaWeddingTopWnd:UpdateStageCommon(stageInfo)
	local subStage = stageInfo[1]
	if subStage == "Init" then
		if self.isGroomOrBride then
			self:SetBottomActive(true, false, false, false, true, false)
			self.leftButtonLabel.text = LocalString.GetString("跳过此步")
			self.rightButtonLabel.text = LocalString.GetString("开启玩法")
		else
			self:SetBottomActive(false, false, false, true, false, false)
			self.centerLabel.text = LocalString.GetString("等待新郎、新娘开启")
		end
		return true
	elseif subStage == "Signup" then
		if self.isGroomOrBride then
			self:SetBottomActive(false, true, false, false, false, true)
			self.leftLabel.text = LocalString.GetString("宾客报名参加中...")
		else
			self:SetBottomActive(false, false, true, false, false, true)
			self.centerButtonLabel.text = LocalString.GetString("参加玩法")

			local cButton = self.centerButton.transform:GetComponent(typeof(CButton))
			if stageInfo[3] and stageInfo[3] == 1 then
				self.centerButtonPoint:SetActive(false)
				cButton.Enabled = false
			else
				self.centerButtonPoint:SetActive(true)
				cButton.Enabled = true
			end
		end
		self.endTimeStamp = stageInfo[2]
		self:StartCountDown()
		return true
	end
	return false
end

-- 开始倒计时
function LuaWeddingTopWnd:StartCountDown()
	if not self.endTimeStamp then
		self.rightLabel.text = ""
		return
	end

	self:UpdateTimeOnce()
	self:ClearTick(self.countDownTick)
	self.countDownTick = RegisterTick(function()
		self:UpdateTimeOnce()
	end, 1000)
end

-- 更新一次时间
function LuaWeddingTopWnd:UpdateTimeOnce()
	local count = math.floor(self.endTimeStamp - CServerTimeMgr.Inst.timeStamp)
	count = count <= 0 and 0 or count

	local stage = LuaWeddingIterationMgr.stage
	local stageInfo = LuaWeddingIterationMgr.stageInfo
	if #stageInfo == 0 then return end

	local subStage = stageInfo[1]

	if subStage == "Signup" and stage == 1 or stage == 5 then
		self.rightLabel.text = SafeStringFormat3(LocalString.GetString("%d秒后开始"), count)
	elseif (subStage == "AnswerQuestion" and stage == 1) or (subStage == "Opened" and stage == 4) then
		local timeStr = self:GetTimeShow(count, false)
		self.rightLabel.text = SafeStringFormat3(LocalString.GetString("剩余时间 %s"), timeStr)
	end

	if count <= 0 then
		self.countDownTick = self:ClearTick(self.countDownTick)
	end
end

-- 获取显示的时间
function LuaWeddingTopWnd:GetTimeShow(count, showHour)
	local hour = math.floor(count / 3600)
	local minute = math.floor((count - 3600 * hour) / 60)
	local second = math.floor(count - 3600 * hour - 60 * minute)

	if showHour then
		return SafeStringFormat3(LocalString.GetString("%02d:%02d:%02d"), hour, minute, second)
	else
		return SafeStringFormat3(LocalString.GetString("%02d:%02d"), minute, second)
	end
end

-- 根据性别返回称谓
function LuaWeddingTopWnd:GetAppellation(gender)
	local maleStr = LocalString.GetString("新郎")
	local femaleStr = LocalString.GetString("新娘")

	if gender == EnumGender.Male then
		return maleStr
	else
		return femaleStr
	end
end

-- 进入副本之后等待一段时间再下拉婚前仪式开启/跳过界面
function LuaWeddingTopWnd:InitShowPreGame()
	local passTime = WeddingIteration_Setting.GetData().WeddingTotalTime - CScene.MainScene.ShowTime
	local waitTime = WeddingIteration_Setting.GetData().ShowPreGameWaitTime
	local subStage = LuaWeddingIterationMgr.stageInfo[1]
	self.isGroomOrBride = LuaWeddingIterationMgr:IsGroomOrBride()

	if not self.isGroomOrBride then
		self:UpdateSelectedStage(1)
		return
	end

	if subStage ~= "Init" then
		self:UpdateSelectedStage(1)
	elseif passTime >= waitTime then
		self:UpdateShowPreGame()
	else
		self:ClearTick(self.showPreGameTick)
		self.showPreGameTick = RegisterTickOnce(function ()
			self:UpdateShowPreGame()
		end, (waitTime - passTime) * 1000)
	end
end

function LuaWeddingTopWnd:UpdateShowPreGame()
	local stage = LuaWeddingIterationMgr.stage
	local stageInfo = LuaWeddingIterationMgr.stageInfo

	if stage ~= 1 then return end
	if stageInfo[1] == "Init" then
		if self.isGroomOrBride then
			self:UpdateSelectedStage(1)
			self:ClearTick(self.msgTick)
			self.msgTick = RegisterTick(function ()
				g_MessageMgr:ShowMessage("WEDDING_SCENE_OPEN_PREGAME_TIP")
			end, WeddingIteration_Setting.GetData().PreGameNotifyMessageInterval * 1000)
		end
	end
end

-- 播放拜堂特效
function LuaWeddingTopWnd:ShowBaiTangEndFx()
	local showTimeTbl = LuaWeddingIterationMgr:Array2Tbl(WeddingIteration_Setting.GetData().BaiTangShowFxStageInfo)
	self.baiTangShowFxTick1 = self:ClearTick(self.baiTangShowFxTick1)
	self.baiTangShowFxTick2 = self:ClearTick(self.baiTangShowFxTick2)

	local xianNiaoFxId = WeddingIteration_Setting.GetData().XianNiaoHuanRaoFxId
	local caiGuangFxId = WeddingIteration_Setting.GetData().CaiGuangJiangLinFxId
	local yanHuaFxId = WeddingIteration_Setting.GetData().YanHuaZhaQiFxId

	local npc = self:GetEmptyNpc()
	if npc then
		self.xianNiaoFx = CEffectMgr.Inst:AddObjectFX(xianNiaoFxId, npc.RO, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3(0, -60, 0), nil)
		self.caiGuangFx = CEffectMgr.Inst:AddObjectFX(caiGuangFxId, npc.RO, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
	end

	self.baiTangShowFxTick1 = RegisterTickOnce(function ()
		self.xianNiaoFx = self:ClearFx(self.xianNiaoFx)
		local emptyNpc = self:GetEmptyNpc()
		if emptyNpc then
			self.yanHuaFx = CEffectMgr.Inst:AddObjectFX(yanHuaFxId, npc.RO, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
		end
	end,  showTimeTbl[2] * 1000)

	self.baiTangShowFxTick2 = RegisterTickOnce(function ()
		self.caiGuangFx = self:ClearFx(self.caiGuangFx)
		self.yanHuaFx = self:ClearFx(self.yanHuaFx)
	end,  showTimeTbl[3] * 1000)
end

-- 获取空NPC
function LuaWeddingTopWnd:GetEmptyNpc()
	local npcId = WeddingIteration_Setting.GetData().BaiTangEmptyNpcId
	local npc = nil

	CClientObjectMgr.Inst:ForeachObject(DelegateFactory.Action_CClientObject(function(obj)
		if TypeIs(obj, typeof(CClientNpc)) then
			if obj and obj.TemplateId == npcId then
				npc = obj
			end
		end
    end))
	return npc
end

-- 清除计时器
function LuaWeddingTopWnd:ClearTick(tick)
	if tick then
		UnRegisterTick(tick)
	end
	return nil
end

-- 清除特效
function LuaWeddingTopWnd:ClearFx(fx)
	if fx then
		fx:Destroy()
	end
	return nil
end

-- 关闭界面时清除计时器
function LuaWeddingTopWnd:OnDestroy()
	self:ClearTick(self.countDownTick)
	self:ClearTick(self.showPreGameTick)
	self:ClearTick(self.baiTangShowFxTick1)
	self:ClearTick(self.baiTangShowFxTick2)
	self:ClearTick(self.msgTick)

	self:ClearFx(self.xianNiaoFx)
	self:ClearFx(self.caiGuangFx)
	self:ClearFx(self.yanHuaFx)

	LuaWeddingIterationMgr:ClearData()
end

-- 显示收起描述
function LuaWeddingTopWnd:ShowCloseDetailWnd(isShow)
	if isShow then
		self.arrow.flip = UIBasicSprite.Flip.Nothing
		self.detailTween:PlayReverse()
	else
		self.arrow.flip = UIBasicSprite.Flip.Vertically
		self.detailTween:PlayForward()
	end
end

-- 设置各个阶段的颜色
function LuaWeddingTopWnd:SetStageColor()
	for i = 1, #self.stages do
		local label = self.stages[i].transform:Find("Label"):GetComponent(typeof(UILabel))
		if i == self.goingStage then
			if i == self.selectedStage then
				label.color = NGUIText.ParseColor24("993030", 0)
			else
				label.color = NGUIText.ParseColor24("FFFFFF", 0)
			end
		else
			label.color = NGUIText.ParseColor24("FCCD9C", 0)
		end
	end
end

--@region UIEvent

function LuaWeddingTopWnd:OnStageClick(id)
	if self.selectedStage == id then
		self:UpdateSelectedStage(-1)
	else
		self:UpdateSelectedStage(id)
	end
end

function LuaWeddingTopWnd:OnArrowClick()
	if self.arrow.flip == UIBasicSprite.Flip.Nothing then
		self:UpdateSelectedStage(-1)
	else
		if self.goingStage > 0 then
			self:UpdateSelectedStage(self.goingStage)
		else
			self:UpdateSelectedStage(1)
		end
	end
end

function LuaWeddingTopWnd:OnExpandButtonClick()
	LuaUtils.SetLocalRotation(self.expandButton.transform, 0, 0, 0)
	CTopAndRightTipWnd.showPackage = false
	CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

function LuaWeddingTopWnd:OnLeftButtonClick()
	local stage = LuaWeddingIterationMgr.stage
	local stageInfo = LuaWeddingIterationMgr.stageInfo

	if #stageInfo == 0 then return end

	local subStage = stageInfo[1]
	if subStage ~= "Init" then return end

	if stage == 1 then
		self.msgTick = self:ClearTick(self.msgTick)
		Gac2Gas.RequestSkipNewWeddingPreGame()
	elseif stage == 4 then
		Gac2Gas.NewWeddingRequestSkipXiuQiu()
	elseif stage == 5 then
		Gac2Gas.NewWeddingRequestSkipYiXianQian()
	elseif stage == 6 then
		Gac2Gas.NewWeddingRequestSkipNaoDongFang()
	end
end

function LuaWeddingTopWnd:OnCenterButtonClick()
	self.centerButtonPoint:SetActive(false)
	local stage = LuaWeddingIterationMgr.stage
	local stageInfo = LuaWeddingIterationMgr.stageInfo

	if #stageInfo == 0 then return end

	local subStage = stageInfo[1]
	if stage == 1 then
		if subStage == "Signup" then
			Gac2Gas.RequestSignupWeddingPregame()
		end
	elseif stage == 3 then
		if subStage == "Init" then
			LuaWeddingIterationMgr:OpenFeastSelectWnd()
		end
	elseif stage == 5 then
		if subStage == "Signup" then
			Gac2Gas.NewWeddingRequestSignupYiXianQian()
		end
	elseif stage == 6 then
		if subStage == "SceneCreated" then
			Gac2Gas.NewWeddingRequestEnterNaoDongFang()
		end
	elseif stage == 7 then
		Gac2Gas.NewWeddingRequestEndWedding()
	end
end

function LuaWeddingTopWnd:OnRightButtonClick()
	local stage = LuaWeddingIterationMgr.stage
	local stageInfo = LuaWeddingIterationMgr.stageInfo

	if #stageInfo == 0 then return end

	local subStage = stageInfo[1]
	if subStage ~= "Init" then return end

	if stage == 1 then
		self.msgTick = self:ClearTick(self.msgTick)
		local msg = g_MessageMgr:FormatMessage("WEDDING_START_PREGAME_CONFIRM")
		MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
			Gac2Gas.RequestStartNewWeddingPreGame()
		end), nil, nil, nil, false)
	elseif stage == 4 then
		Gac2Gas.NewWeddingRequestStartXiuQiu()
	elseif stage == 5 then
		LuaWeddingIterationMgr.openedGameplay = "YiXianQian"
		CUIManager.ShowUI(CLuaUIResources.WeddingOpenGameplayWnd)
	elseif stage == 6 then
		LuaWeddingIterationMgr.openedGameplay = "NaoDongFang"
		CUIManager.ShowUI(CLuaUIResources.WeddingOpenGameplayWnd)
	end
end

function LuaWeddingTopWnd:OnBaiTangButtonClick()
	if not self.baiTangProgress then return end

	Gac2Gas.RequestNewWeddingBaiTang(self.baiTangProgress)
	self.baiTangLabel.gameObject:SetActive(false)
end

--@endregion UIEvent
