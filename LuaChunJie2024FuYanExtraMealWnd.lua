local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CCurrentMoneyCtrl      = import "L10.UI.CCurentMoneyCtrl"
local EnumMoneyType          = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey       = import "L10.Game.EnumPlayScoreKey"

LuaChunJie2024FuYanExtraMealWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChunJie2024FuYanExtraMealWnd, "roundTime", "RoundTime", UILabel)
RegistChildComponent(LuaChunJie2024FuYanExtraMealWnd, "normalFoodRoot", "NormalFood", Transform)
RegistChildComponent(LuaChunJie2024FuYanExtraMealWnd, "DIYFoodRoot", "DIYFood", Transform)
--@endregion RegistChildComponent end

RegistClassMember(LuaChunJie2024FuYanExtraMealWnd, "tabs")
RegistClassMember(LuaChunJie2024FuYanExtraMealWnd, "normalFood_Template")
RegistClassMember(LuaChunJie2024FuYanExtraMealWnd, "normalFood_Grid")
RegistClassMember(LuaChunJie2024FuYanExtraMealWnd, "normalFood_SoldOut")
RegistClassMember(LuaChunJie2024FuYanExtraMealWnd, "normalFood_NotSoldOut")
RegistClassMember(LuaChunJie2024FuYanExtraMealWnd, "normalFood_AddSubButton")
RegistClassMember(LuaChunJie2024FuYanExtraMealWnd, "normalFood_CurrentMoney")
RegistClassMember(LuaChunJie2024FuYanExtraMealWnd, "normalFood_Button")
RegistClassMember(LuaChunJie2024FuYanExtraMealWnd, "normalFood_Name")
RegistClassMember(LuaChunJie2024FuYanExtraMealWnd, "normalFood_Icon")

RegistClassMember(LuaChunJie2024FuYanExtraMealWnd, "DIYFood_Template")
RegistClassMember(LuaChunJie2024FuYanExtraMealWnd, "DIYFood_Tables")
RegistClassMember(LuaChunJie2024FuYanExtraMealWnd, "DIYFood_CreateButton")
RegistClassMember(LuaChunJie2024FuYanExtraMealWnd, "DIYFood_FinishCreate")
RegistClassMember(LuaChunJie2024FuYanExtraMealWnd, "DIYFood_RandomButton")
RegistClassMember(LuaChunJie2024FuYanExtraMealWnd, "DIYFood_SelectMaterialWnd")
RegistClassMember(LuaChunJie2024FuYanExtraMealWnd, "DIYFood_SelectMaterialWnd_Bg")
RegistClassMember(LuaChunJie2024FuYanExtraMealWnd, "DIYFood_SelectMaterialWnd_Type")
RegistClassMember(LuaChunJie2024FuYanExtraMealWnd, "DIYFood_SelectMaterialWnd_Tip")
RegistClassMember(LuaChunJie2024FuYanExtraMealWnd, "DIYFood_SelectMaterialWnd_Grid")
RegistClassMember(LuaChunJie2024FuYanExtraMealWnd, "DIYFood_SelectMaterialWnd_Template")
RegistClassMember(LuaChunJie2024FuYanExtraMealWnd, "DIYFood_SelectEffectWnd")
RegistClassMember(LuaChunJie2024FuYanExtraMealWnd, "DIYFood_Bg")

RegistClassMember(LuaChunJie2024FuYanExtraMealWnd, "DIYFood_Finished")
RegistClassMember(LuaChunJie2024FuYanExtraMealWnd, "DIYFood_UnFinish")

RegistClassMember(LuaChunJie2024FuYanExtraMealWnd, "tabId")
RegistClassMember(LuaChunJie2024FuYanExtraMealWnd, "selectNormalFoodId")
RegistClassMember(LuaChunJie2024FuYanExtraMealWnd, "foodType2LimitCount")
RegistClassMember(LuaChunJie2024FuYanExtraMealWnd, "foodType2Name")
RegistClassMember(LuaChunJie2024FuYanExtraMealWnd, "foodType2Choices")
RegistClassMember(LuaChunJie2024FuYanExtraMealWnd, "selectDIYFoodIds")

function LuaChunJie2024FuYanExtraMealWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitTabs()
    self:InitNormalFoodComponents()
    self:InitDIYFoodComponents()
end

function LuaChunJie2024FuYanExtraMealWnd:InitTabs()
    local anchor = self.transform:Find("Anchor")
    local tabRoot = anchor:Find("Tabs")
    self.tabs = {}
    for i = 1, 2 do
        local child = tabRoot:GetChild(i - 1)
        local normal = child:Find("Normal").gameObject
        local highlight = child:Find("Highlight").gameObject
        normal:SetActive(true)
        highlight:SetActive(false)
        UIEventListener.Get(normal).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnTabClick(i)
        end)

        table.insert(self.tabs, {normal = normal, highlight = highlight})
    end
end

function LuaChunJie2024FuYanExtraMealWnd:InitNormalFoodComponents()
    self.normalFood_Template = self.normalFoodRoot:Find("Template").gameObject
    self.normalFood_Template:SetActive(false)
    self.normalFood_Grid = self.normalFoodRoot:Find("Grid"):GetComponent(typeof(UIGrid))
    self.normalFood_SoldOut = self.normalFoodRoot:Find("Right/SoldOut").gameObject
    self.normalFood_NotSoldOut = self.normalFoodRoot:Find("Right/NotSoldOut").gameObject
    self.normalFood_AddSubButton = self.normalFoodRoot:Find("Right/NotSoldOut/AddSubButton"):GetComponent(typeof(QnAddSubAndInputButton))
    self.normalFood_AddSubButton:SetMinMax(1, 99, 1)
    self.normalFood_AddSubButton:SetValue(1, false)
    self.normalFood_CurrentMoney = self.normalFoodRoot:Find("Right/NotSoldOut/CurrentMoney"):GetComponent(typeof(CCurrentMoneyCtrl))
    self.normalFood_CurrentMoney:SetType(EnumMoneyType.YinLiang, EnumPlayScoreKey.NONE, true)
    local settingData = ChunJie2024_FuYanSetting.GetData()
    self.normalFood_CurrentMoney:SetCost(settingData.ExtraFoodSilver)
    self.normalFood_Button = self.normalFoodRoot:Find("Right/Button").gameObject
    self.normalFood_Name = self.normalFoodRoot:Find("Right/NotSoldOut/Name"):GetComponent(typeof(UILabel))
    self.normalFood_Icon = self.normalFoodRoot:Find("Right/NotSoldOut/Icon"):GetComponent(typeof(CUITexture))
    self.normalFoodRoot:Find("Right/NotSoldOut/Tip/Silver"):GetComponent(typeof(UILabel)).text = 520000

    UIEventListener.Get(self.normalFood_Button).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnOKButtonClick()
    end)
end

function LuaChunJie2024FuYanExtraMealWnd:InitDIYFoodComponents()
    self.foodType2LimitCount = {
        [1] = 1,
        [2] = 3,
        [3] = 1,
        [4] = 4,
    }
    self.foodType2Name = {
        [1] = LocalString.GetString("主菜"),
        [2] = LocalString.GetString("配菜"),
        [3] = LocalString.GetString("厨具"),
        [4] = LocalString.GetString("佐料"),
    }
    self.selectDIYFoodIds = {}
    for i = 1, 4 do
        self.selectDIYFoodIds[i] = {}
    end

    local materialRoot = self.DIYFoodRoot:Find("Left/Material")
    self.DIYFood_Template = materialRoot:Find("Template").gameObject
    self.DIYFood_Template:SetActive(false)
    self.DIYFood_Tables = {}
    for i = 1, 4 do
        local grid = materialRoot:Find(tostring(i) .. "/Grid"):GetComponent(typeof(UIGrid))

        local limitCount = self.foodType2LimitCount[i]
        Extensions.RemoveAllChildren(grid.transform)
        for j = 1, limitCount do
            local child = NGUITools.AddChild(grid.gameObject, self.DIYFood_Template)
            child:SetActive(true)

            child.transform:Find("Icon").gameObject:SetActive(false)
            child.transform:Find("AddButton").gameObject:SetActive(true)

            UIEventListener.Get(child).onClick = DelegateFactory.VoidDelegate(function (go)
                self:OnDIYFoodClick(i, j, go)
            end)
        end
        grid:Reposition()

        table.insert(self.DIYFood_Tables, grid)
    end

    self.DIYFood_CreateButton = self.DIYFoodRoot:Find("Left/CreateButton").gameObject
    UIEventListener.Get(self.DIYFood_CreateButton).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnCreateFoodButtonClick()
    end)
    self.DIYFood_FinishCreate = self.DIYFoodRoot:Find("Left/FinishCreate").gameObject
    self.DIYFood_RandomButton = self.DIYFoodRoot:Find("Left/RandomButton").gameObject
    UIEventListener.Get(self.DIYFood_RandomButton).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnRandomButtonClick()
    end)

    self.DIYFood_SelectMaterialWnd = self.DIYFoodRoot:Find("SelectMaterialWnd").gameObject
    self.DIYFood_SelectMaterialWnd:SetActive(false)
    self.DIYFood_SelectMaterialWnd_Bg = self.DIYFood_SelectMaterialWnd.transform:Find("Bg"):GetComponent(typeof(UIWidget))
    self.DIYFood_SelectMaterialWnd_Template = self.DIYFood_SelectMaterialWnd.transform:Find("Bg/Template").gameObject
    self.DIYFood_SelectMaterialWnd_Template:SetActive(false)
    self.DIYFood_SelectMaterialWnd_Grid = self.DIYFood_SelectMaterialWnd.transform:Find("Bg/Grid"):GetComponent(typeof(UIGrid))
    self.DIYFood_SelectMaterialWnd_Type = self.DIYFood_SelectMaterialWnd.transform:Find("Bg/Type"):GetComponent(typeof(UILabel))
    self.DIYFood_SelectMaterialWnd_Tip = self.DIYFood_SelectMaterialWnd.transform:Find("Bg/Tip"):GetComponent(typeof(UILabel))
    self.DIYFood_SelectEffectWnd = self.DIYFoodRoot:Find("SelectEffectWnd").gameObject
    self.DIYFood_SelectEffectWnd:SetActive(false)
    self.DIYFood_Bg = self.DIYFoodRoot:Find("Bg").gameObject
    self.DIYFood_Bg:SetActive(false)
    UIEventListener.Get(self.DIYFood_Bg).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnDIYFoodBgClick()
    end)

    self.foodType2Choices = {}
    ChunJie2024_FuYanExtraDIYFood.Foreach(function(key, data)
        local foodType = data.FoodType
        if not self.foodType2Choices[foodType] then
            self.foodType2Choices[foodType] = {}
        end

        table.insert(self.foodType2Choices[foodType], key)
    end)

    self.DIYFood_Finished = self.DIYFoodRoot:Find("Right/Finish").gameObject
    self.DIYFood_UnFinish = self.DIYFoodRoot:Find("Right/UnFinish").gameObject
    self.DIYFood_Finished:SetActive(false)
    self.DIYFood_UnFinish:SetActive(true)
end


function LuaChunJie2024FuYanExtraMealWnd:Init()
    local settingData = ChunJie2024_FuYanSetting.GetData()
	self.roundTime.text = SafeStringFormat3(LocalString.GetString("[FF5050]今天[-] %s结束"), settingData.CaiMaiEndTime)

    self.tabId = 0
    self:OnTabClick(1)

    self:InitNormalFoodGrid()
end

function LuaChunJie2024FuYanExtraMealWnd:InitNormalFoodGrid()
    Extensions.RemoveAllChildren(self.normalFood_Grid.transform)
    ChunJie2024_FuYanExtraNormalFood.Foreach(function(id, data)
        local child = NGUITools.AddChild(self.normalFood_Grid.gameObject, self.normalFood_Template)
        child:SetActive(true)

        local iconMatPath = "UI/Texture/Item_Other/Material/" .. data.Icon
        local icon = child.transform:Find("Icon")
        icon:GetComponent(typeof(CUITexture)):LoadMaterial(iconMatPath)

        child.transform:Find("Highlight").gameObject:SetActive(false)
        child.transform:Find("SoldOut").gameObject:SetActive(false)

        UIEventListener.Get(icon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnNormalFoodClick(id)
        end)
    end)

    self.normalFood_Grid:Reposition()

    self.selectNormalFoodId = 0
    self:OnNormalFoodClick(1)
end

function LuaChunJie2024FuYanExtraMealWnd:UpdateNormalRightInfo()
    local id = self.selectNormalFoodId
    self.normalFood_SoldOut:SetActive(false)
    self.normalFood_NotSoldOut:SetActive(true)

    local data = ChunJie2024_FuYanExtraNormalFood.GetData(id)
    self.normalFood_Name.text = data.Name
    local iconMatPath = "UI/Texture/Item_Other/Material/" .. data.Icon
    self.normalFood_Icon:LoadMaterial(iconMatPath)
end

function LuaChunJie2024FuYanExtraMealWnd:UpdateDIYRightInfo()
    -- local data = ChunJie2024_FuYanExtraNormalFood.GetData(id)
    -- self.normalFood_Name.text = data.Name
    -- local iconMatPath = "UI/Texture/Item_Other/Material/" .. data.Icon
    -- self.normalFood_Icon:LoadMaterial(iconMatPath)
end

function LuaChunJie2024FuYanExtraMealWnd:OnDIYFoodClick(i, j, go)
    self.DIYFood_SelectMaterialWnd:SetActive(true)
    self.DIYFood_Bg:SetActive(true)
    local currentChoiceId = self.selectDIYFoodIds[i][j] or 0

    self.DIYFood_SelectMaterialWnd_Type.text = self.foodType2Name[i]
    self.DIYFood_SelectMaterialWnd_Tip.text = LocalString.GetString("(必选1种)")
    local totalChoices = self.foodType2Choices[i]
    local totalAvailChoices = {}
    for _, choiceId in ipairs(totalChoices) do
        local included = true
        for pos, choiceId1 in ipairs(self.selectDIYFoodIds[i]) do
            if pos ~= j and choiceId1 == choiceId then
                included = false
                break
            end
        end
        if included then table.insert(totalAvailChoices, choiceId) end
    end

    self:AddChild(self.DIYFood_SelectMaterialWnd_Grid.transform, #totalAvailChoices, self.DIYFood_SelectMaterialWnd_Template)
    for id, choiceId in ipairs(totalAvailChoices) do
        local child = self.DIYFood_SelectMaterialWnd_Grid.transform:GetChild(id - 1)
        local data = ChunJie2024_FuYanExtraDIYFood.GetData(choiceId)
        local iconMatPath = "UI/Texture/Item_Other/Material/" .. data.Icon
        child:Find("Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(iconMatPath)
        child:Find("Label"):GetComponent(typeof(UILabel)).text = data.Name
        child:Find("Selected").gameObject:SetActive(choiceId == currentChoiceId)

        UIEventListener.Get(child.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnMaterialSelect(i, j, choiceId)
        end)
    end
    self.DIYFood_SelectMaterialWnd_Grid:Reposition()

    local bounds = NGUIMath.CalculateRelativeWidgetBounds(self.DIYFood_SelectMaterialWnd_Grid.transform)
    local height = bounds.size.y + 130
    self.DIYFood_SelectMaterialWnd_Bg.height = height

    local b = NGUIMath.CalculateRelativeWidgetBounds(self.DIYFood_SelectMaterialWnd.transform, go.transform)
    local newX = b.center.x + 80
    local newY = b.center.y + 100
    if newY - height <= -530 then
        newY = height - 530
    end
    LuaUtils.SetLocalPosition(self.DIYFood_SelectMaterialWnd_Bg.transform, newX, newY, 0)
end

function LuaChunJie2024FuYanExtraMealWnd:AddChild(parent, count, template)
    local childCount = parent.childCount
    if childCount < count then
        for i = childCount + 1, count do
            NGUITools.AddChild(parent.gameObject, template)
        end
    end

    childCount = parent.childCount
    for i = 1, childCount do
        local child = parent:GetChild(i - 1).gameObject
        child:SetActive(i <= count)
    end
end

function LuaChunJie2024FuYanExtraMealWnd:UpdateDIYFood(i, j)
    local choiceId = self.selectDIYFoodIds[i][j]
    local child = self.DIYFood_Tables[i].transform:GetChild(j - 1)
    local addButton = child:Find("AddButton").gameObject
    local icon = child:Find("Icon"):GetComponent(typeof(CUITexture))
    addButton:SetActive(choiceId == nil)
    icon.gameObject:SetActive(choiceId ~= nil)
    if choiceId then
        local iconMatPath = "UI/Texture/Item_Other/Material/" .. ChunJie2024_FuYanExtraDIYFood.GetData(choiceId).Icon
        icon:LoadMaterial(iconMatPath)
    end
end

--@region UIEvent

function LuaChunJie2024FuYanExtraMealWnd:OnTabClick(i)
    if self.tabId > 0 then
        self.tabs[self.tabId].normal:SetActive(true)
        self.tabs[self.tabId].highlight:SetActive(false)
    end

    self.tabId = i
    self.tabs[i].normal:SetActive(false)
    self.tabs[i].highlight:SetActive(true)

    self.normalFoodRoot.gameObject:SetActive(i == 1)
    self.DIYFoodRoot.gameObject:SetActive(i == 2)
end

function LuaChunJie2024FuYanExtraMealWnd:OnNormalFoodClick(id)
    if self.selectNormalFoodId > 0 then
        self.normalFood_Grid.transform:GetChild(self.selectNormalFoodId - 1):Find("Highlight").gameObject:SetActive(false)
    end

    self.selectNormalFoodId = id
    self.normalFood_Grid.transform:GetChild(id - 1):Find("Highlight").gameObject:SetActive(true)
    self:UpdateNormalRightInfo()
end

function LuaChunJie2024FuYanExtraMealWnd:OnOKButtonClick()

end

function LuaChunJie2024FuYanExtraMealWnd:OnDIYFoodBgClick()
    self.DIYFood_Bg:SetActive(false)
    self.DIYFood_SelectEffectWnd:SetActive(false)
    self.DIYFood_SelectMaterialWnd:SetActive(false)
end

function LuaChunJie2024FuYanExtraMealWnd:OnMaterialSelect(i, j, choiceId)
    self.DIYFood_Bg:SetActive(false)
    self.DIYFood_SelectMaterialWnd:SetActive(false)
    local currentChoiceId = self.selectDIYFoodIds[i][j] or 0
    if currentChoiceId ~= choiceId then
        self.selectDIYFoodIds[i][j] = choiceId
        self:UpdateDIYFood(i, j)
    end
end

function LuaChunJie2024FuYanExtraMealWnd:OnRandomButtonClick()
    for i = 1, 4 do
        self.selectDIYFoodIds[i] = {}
        local limitCount = self.foodType2LimitCount[i]
        local count = math.random(1, limitCount)

        local choices = self.foodType2Choices[i]
        local tempChoices = {}
        for _, choiceId in ipairs(choices) do
            table.insert(tempChoices, choiceId)
        end
        for j = 1, limitCount do
            if j <= count then
                local randomId = math.random(#tempChoices)
                self.selectDIYFoodIds[i][j] = tempChoices[randomId]
                table.remove(tempChoices, randomId)
            end
            self:UpdateDIYFood(i, j)
        end
    end
end

function LuaChunJie2024FuYanExtraMealWnd:OnCreateFoodButtonClick()

end

--@endregion UIEvent
