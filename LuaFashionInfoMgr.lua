

LuaFashionInfoMgr = {}
LuaFashionInfoMgr.m_FashionItemInfo = nil

function LuaFashionInfoMgr.ShowFashionTemplateInfo(templateId, alignType, worldCenterX, worldCenterY, targetSizeX, targetSizeY) 
    local fashionData = Fashion_Fashion.GetData(templateId)
    if fashionData == nil or System.String.IsNullOrEmpty(fashionData.Description) then
        return
    end

    LuaFashionInfoMgr.m_FashionItemInfo = {}
    LuaFashionInfoMgr.m_FashionItemInfo.templateId = templateId
    LuaFashionInfoMgr.m_FashionItemInfo.alignType = alignType
    LuaFashionInfoMgr.m_FashionItemInfo.targetSize = Vector2(targetSizeX, targetSizeY)
    LuaFashionInfoMgr.m_FashionItemInfo.targetCenterPos = Vector3(worldCenterX, worldCenterY)
    CUIManager.ShowUI(CLuaUIResources.FashionInfoWnd)
end