local bServer = IsRunningServerCode()

if bServer then
	function CPhysicsAIShip:GetObject()
		return g_PhysicsMgr:GetObjByPhysicsObjId(self.m_POId)
	end

	function CPhysicsAIShip:OnEnterLeaveSteering(steering, bEnter)
		if not steering then return end
		local steeringId = steering.Id

		local funcMap = bEnter and g_PhysicsMgr.m_OnEnterSteerFunc or g_PhysicsMgr.m_OnLeaveSteerFunc
		if not funcMap[steeringId] then return end

		local obj = self:GetObject()
		if not obj then return end

		-- info = { FuncName, EventParamTbl }
		for i, info in ipairs(funcMap[steeringId]) do
			if obj[info[1]] then
				obj[info[1]](obj, steering, unpack(info[2]))
			end
		end
	end
else
	function CPhysicsAIShip:GetObject()
        return self.m_ClientPhysicsObjectHandler
	end
	function GetDistance2_XY(x1, y1, x2, y2)
		local dx = x1 - x2;
		local dy = y1 - y2;
		return dx * dx + dy * dy;
	end
	function GetDistance_XY(x1, y1, x2, y2)
		return math.sqrt(GetDistance2_XY(x1, y1, x2, y2))
	end

	-- 客户端的得保证重复做没问题
	function CPhysicsAIShip:OnEnterLeaveSteering(steering, bEnter)
		if not steering then return end
		local steeringId = steering.Id
		local func = nil
		if bEnter then
			func = LuaClientPhysicsMgr.GetOnEnterSteerFunc(steeringId)
		else
			func = LuaClientPhysicsMgr.GetOnLeaveSteerFunc(steeringId)
		end
		if not func then return end

		local obj = self:GetObject()
		if not obj then return end

		-- info = { FuncName, EventParamTbl }
		for i, info in ipairs(func) do
			if obj[info[1]] then
				obj[info[1]](obj, steering, unpack(info[2]))
			end
		end
	end
end


--[-[
--	obj:GetPhysicsAIParams()
--	obj:SetPhysicsMoveDeltaPosition(dx, dy)
--	obj:SetPhysicsDirection(rad)
--	obj:SetPhysicsPos(x, y, dir)
--	obj:GetPhysicsPosv()
--	obj:GetPhysicsForwardVector()
--	obj:GetPhysicsVelocity()
--
--	obj:GetNearestObstacle()
--]-]

function CPhysicsAIShip:Ctor(poId, templateId)
	self.m_POId = poId
	self.m_TemplateId = templateId
    self.m_TimeStepInSecond = 0.05
	self.m_Mass = 1

	self.m_MaxSpeed = 2
	self.m_MulSpeed = 1
	self.m_AccelerationRate = 5
	self.m_DecelerationRate = 8
	-- max angular velocity
	self.m_MaxW = 90 / 180 * math.pi
	self.m_CanMove = true

	-- read from box2d
	self.m_Pos = { x = 0, y = 0 }
	self.m_Velocity = { x = 0, y = 0 }
	self.m_Forward = { x = 0, y = 0 }
	self.m_CurrentSpeed = 0
	self.m_TargetSpeed = 0
	self.m_CurrentOrientation = 0
	self.m_TargetOrientation = 0
	self.m_CurrentAngularVelocity = 0

	self.m_bAvoidCollision = false
	self.m_AC_MinTimeToCollision = 1
	self.m_Steering = nil

	self.m_LastForce = { x = 0, y = 0 }

	self:LoadDesignData()
end

function CPhysicsAIShip:LoadDesignData()
	local shipDesign = GetNavalWarShipDesignData(self.m_TemplateId)
	if shipDesign then
		self.m_AccelerationRate = shipDesign.accelerationRate
		self.m_DecelerationRate = shipDesign.decelerationRate
		self.m_AC_MinTimeToCollision = shipDesign.collisionPrediction
	end

	local shapeDesign = GetPhysicsShapeDesignData(self.m_TemplateId)
	InitPhysicsObject_M_I(self, shapeDesign)

	local objDesign = GetPhysicsObjectDesignData(self.m_TemplateId)
	if objDesign then
		self.m_MaxSpeed = objDesign.speed
		local maxW = objDesign.w <= 0 and 90 or objDesign.w
		self.m_MaxW = maxW * PHYSICS_DEG_TO_RAD
	end
end

function CPhysicsAIShip:GetControllerType()
	return EnumPhysicsController.ePhysicsAIShip
end

-- TODO m_MaxSpeed, m_MaxW, IsActive
function CPhysicsAIShip:GetSnapShotDataTbl()
	return {
		self.m_TemplateId,
		self.m_Steering,
		self.m_TargetSpeed,
		self.m_TargetOrientation,
		self.m_MulSpeed,
	}
end
--解析t，并塞到snapshot中
function CPhysicsAIShip.ParseSnapShotDataTbl(snapshot,t)
	snapshot.templateId = t[1]
	snapshot.steering = t[2]
	snapshot.targetSpeed = t[3]
	snapshot.targetOrientation = t[4]
	snapshot.mulSpeed = t[5]
end

function CPhysicsAIShip:ProcessCmd()
end

function CPhysicsAIShip:Step(currframeId)
	if not self.m_CanMove then return end
	local obj = self:GetObject()
	if not obj then return end

	if not self:LoadParams(obj, currframeId) then return end

	if currframeId % 5 == 0 then
		self:CalculateForces(currframeId)
	end
	self:ApplySteeringForce(obj, self.m_TimeStepInSecond, currframeId)
end

function CPhysicsAIShip:LoadParams(obj, currframeId)
	local x, y, anglex, angley, vx, vy, angle, speed, w = obj:GetPhysicsAIParams()
	
	if not (x and y and anglex and angley and vx and vy and angle and speed and w) then
		return
	end
	self.m_Pos.x = x
	self.m_Pos.y = y
	self.m_Forward.x = anglex
	self.m_Forward.y = angley
	self.m_Velocity.x = vx
	self.m_Velocity.y = vy
	-- angle in degree
	self.m_CurrentOrientation = math.floor(angle)
	self.m_CurrentSpeed = speed
	-- w in degree
	self.m_CurrentAngularVelocity = w

	if EnablePhysicsAIShipOutputLog then
		local str = string.format("LoadParams,,%s,,%s,,%s,||,%s,||,%s,,%s\n",
			currframeId, x, y, self.m_CurrentOrientation, self.m_CurrentSpeed, self.m_CurrentAngularVelocity)
		local str2 = string.format("Steering,,%s,,%s,||,%s,,%s,,%s,||,%s,,%s\n",
			currframeId, self.m_Steering.poId or 0,
			self.m_Steering.x or 0, self.m_Steering.y or 0, self.m_Steering.dir or 0,
			self.m_Steering.Id, self.m_Steering.beginFrame)

		if bServer then
			local file = io.open("K:/L10Mainland/artist/PhysicsAIShip_server.log", "a")
			file:write(str)
			file:write(str2)
			file:close()
		else
			local File =import "System.IO.File"
			File.AppendAllText("K:/L10Mainland/artist/PhysicsAIShip_client.log", str);
			File.AppendAllText("K:/L10Mainland/artist/PhysicsAIShip_client.log", str2);
		end
	end

	return true
end

function CPhysicsAIShip:SetMaxSpeed(maxSpeed)
	self.m_MaxSpeed = maxSpeed
end
function CPhysicsAIShip:SetMaxW(maxW)
	self.m_MaxW = maxW * PHYSICS_DEG_TO_RAD
end

function CPhysicsAIShip:SetMulSpeed(mulSpeed)
	self.m_MulSpeed = mulSpeed
end

function CPhysicsAIShip:GetMaxSpeed()
	return self.m_MaxSpeed * math.max(0, self.m_MulSpeed)
end

function CPhysicsAIShip:ResetSpeed()
	local objDesign = GetPhysicsObjectDesignData(self.m_TemplateId)
	if objDesign then
		self.m_MaxSpeed = objDesign.speed
	else
		self.m_MaxSpeed = 2
	end

	local shipDesign = GetNavalWarShipDesignData(self.m_TemplateId)
	if shipDesign then
		local maxW = shipDesign.maxAngularVelocity <= 0 and 90 or shipDesign.maxAngularVelocity
		self.m_MaxW = maxW * PHYSICS_DEG_TO_RAD
	else
		self.m_MaxW = 90 * PHYSICS_DEG_TO_RAD
	end

	self.m_CanFire = nil
end

-- there are two ways to set force:
-- 1.	SetForce(x, y)
-- 2.	use self.m_LastForce as a temporary vector for calculation
--		and store the final result in self.m_LastForce.
function CPhysicsAIShip:SetForce(x, y)
	self.m_LastForce.x = x
	self.m_LastForce.y = y
end

-- steering = {
--     Id = steeringId,
--     beginFrame = frameId,
--     oriAI = oriAI
-- }
function CPhysicsAIShip:CheckSteeringDuration(currframeId)
	local designData = GetPhysicsAIDesignData(self.m_Steering.Id)
	if not designData then return end

	if designData.duration <= 0 then return end
	if not self.m_Steering.beginFrame then return end

	local frames = currframeId - self.m_Steering.beginFrame
	if frames <= designData.duration then return end

	-- steering end
	if designData.nextSteer > 0 then
		local newSteering = {}
		for key, value in pairs(self.m_Steering) do
			newSteering[key] = value
		end
		newSteering.Id = designData.nextSteer
		newSteering.beginFrame = currframeId
		self:AddSteer(newSteering)
	else
		self:AddSteer(self.m_Steering.oriAI)
	end
end

function CPhysicsAIShip:CalculateForce(currframeId)
	if not self.m_Steering then return end

	self:CheckSteeringDuration(currframeId)

	if not self.m_Steering then return end
	local designData = GetPhysicsAIDesignData(self.m_Steering.Id)
	if not designData then return end

	self[EnumPhysicsAICmdFuncName[designData.cmdType]](self, currframeId)
end

-- calculate the forces
-- should run in lower frequency
function CPhysicsAIShip:CalculateForces(currframeId)
	-- if self.m_MaxSpeed < 0.01 or self.m_MaxForce < 0.01 then
	--     return
	-- end

	self:SetForce(0, 0)
	self:CalculateForce(currframeId)

	if self.m_bAvoidCollision then
		self:SteerToAvoidObstacles(self.m_AC_MinTimeToCollision)
	end

	-- Enforce speed limit.  Steering behaviors are expected to return a
    -- final desired velocity, not a acceleration, so we apply them directly.
	-- 22-11-22 the process of force sounds like unnecessary, why not apply to velocity directly?
	local maxSpeed = self:GetMaxSpeed()
	-- Update vehicle velocity
	local force2 = self.m_LastForce.x * self.m_LastForce.x + self.m_LastForce.y * self.m_LastForce.y
	self.m_TargetSpeed = force2 >= maxSpeed * maxSpeed and maxSpeed or math.sqrt(force2)

	self.m_TargetOrientation = math.atan2(self.m_LastForce.y, self.m_LastForce.x)
end


-- Applies a steering force to this vehicle
-- And turns the vehicle towards his velocity vector
-- @param elapsedTime	How long has elapsed since the last update
function CPhysicsAIShip:ApplySteeringForce(obj, elapsedTime, currframeId)
	-- Calculates how much the agent's position should change
	-- Notice that we clamp the target speed and not the speed itself, 
	-- because the vehicle's maximum speed might just have been lowered
	-- and we don't want its actual speed to suddenly drop
	local targetSpeed = self.m_TargetSpeed
	targetSpeed = targetSpeed > 0 and targetSpeed or 0
	local maxSpeed = self:GetMaxSpeed()
	targetSpeed = targetSpeed < maxSpeed and targetSpeed or maxSpeed

	if Mathf_Approximately(self.m_CurrentSpeed, targetSpeed) then
		self.m_CurrentSpeed = targetSpeed
	else
		local rate = targetSpeed > self.m_CurrentSpeed and self.m_AccelerationRate or self.m_DecelerationRate
		self.m_CurrentSpeed = Mathf_Lerp(self.m_CurrentSpeed, targetSpeed, elapsedTime * rate)
	end
	

	local accelerationX = self.m_Forward.x * self.m_CurrentSpeed * elapsedTime
	local accelerationY = self.m_Forward.y * self.m_CurrentSpeed * elapsedTime
	obj:SetPhysicsMoveDeltaPosition(accelerationX, accelerationY)

	-- if targetSpeed of agent is around zero, whe don't need to 
	-- change orientation. If we do recalculate orientation,
	-- it may make the agent wobble.
	if Mathf_Approximately(targetSpeed, 0) then return end

	local angleDiff = (self.m_TargetOrientation - self.m_CurrentOrientation * PHYSICS_DEG_TO_RAD)
	while angleDiff < -math.pi do angleDiff = angleDiff + 2 * math.pi end
	while angleDiff >= math.pi do angleDiff = angleDiff - 2 * math.pi end

	-- -10 ~ 10 degree
	local desiredW
	if -PhysicsAIShipWThreshold < angleDiff and angleDiff < PhysicsAIShipWThreshold then
		desiredW = self.m_MaxW * angleDiff / (PhysicsAIShipWThreshold * math.pi)
	else
		local sign = angleDiff > 0 and 1 or -1
		desiredW = self.m_MaxW * sign
	end

	local deltaW = desiredW - self.m_CurrentAngularVelocity * PHYSICS_DEG_TO_RAD
	local impulse = self.m_I * deltaW
	if deltaW ~= 0 and not Mathf_Approximately(deltaW, 0) then
		obj:ApplyPhysicsAngularImpulse(impulse)
	end

	if EnablePhysicsAIShipOutputLog then
		local current = self.m_CurrentOrientation
		local currW = self.m_CurrentAngularVelocity
		local target = self.m_TargetOrientation
		local angleDiff = angleDiff
 
		while current < -180 do current = current + 360 end
		while current >= 180 do current = current - 360 end

		local str = string.format("ApplySteeringForce,,%s,,%s,,%s,||,%s,,%s,||,%s,,%s\n",
			currframeId, current, currW,
			target, impulse,
			self.m_CurrentSpeed, targetSpeed)
		

		if bServer then
			local file = io.open("K:/L10Mainland/artist/PhysicsAIShip_server.log", "a")
			file:write(str)
			file:close()
		else
			local File =import "System.IO.File"
			File.AppendAllText("K:/L10Mainland/artist/PhysicsAIShip_client.log", str);
		end
	end
end

-------------------------------------------------------------------------------
-- add steer

-- TODO 服务器保证的是调用此处是真的要结束上一个Steering并开启新的
-- 客户端需要留意快照的处理
function CPhysicsAIShip:AddSteer(steeringInfo)
	self:OnEnterLeaveSteering(self.m_Steering, false)
	self:ResetSpeed()
	--clone
	if steeringInfo and next(steeringInfo) then
		self.m_Steering = {}
		for k, v in pairs(steeringInfo) do
			self.m_Steering[k] = v
		end
	else
		self.m_Steering = nil
	end
	self:OnEnterLeaveSteering(self.m_Steering, true)
end

function CPhysicsAIShip:AddSteerWithRecover(steeringInfo)
	steeringInfo.oriAI = self.m_Steering
	self:AddSteer(steeringInfo)
end

-------------------------------------------------------------------------------
-- steer auxiliary

-- Returns x, y to seek a target position
function CPhysicsAIShip:GetSeekVector(x, y, tx, ty, considerVelocity)
	if not considerVelocity then
		return tx - x, ty - y
	else
		return tx - x - self.m_Velocity.x, ty - y - self.m_Velocity.y
	end
end

function CPhysicsAIShip:PredictTargetFuturePos(x, y, tx, ty, dist, tvx, tvy, tfx, tfy, maxPredictionTime)
	if Mathf_Approximately(dist, 0) then
		return tx, ty
	end
	local unitOffsetX = (tx - x) / dist
	local unitOffsetY = (ty - y) / dist
	local targetPosX = tx
	local targetPosY = ty
	if tvx and tvy and tvx * tvx + tvy * tvy > 1 then
		-- how parallel are the paths of "this" and the target
		-- (1 means parallel, 0 is pependicular, -1 is anti-parallel)
		local parallelness = Vector2_Dot(self.m_Forward.x, self.m_Forward.y, tfx, tfy)
		-- how "forward" is the direction to the target
		-- (1 means dead ahead, 0 is directly to the side, -1 is straight back)
		local forwardness = Vector2_Dot(self.m_Forward.x, self.m_Forward.y, unitOffsetX, unitOffsetY)
		local directTravelTime
		if Mathf_Approximately(self.m_CurrentSpeed, 0) then
			directTravelTime = 100
		else
			directTravelTime = dist / self.m_CurrentSpeed
		end
		-- While we could parametrize this value, if we care about forward/backwards
		-- these values are appropriate enough.
		local f = IntervalComparison(forwardness, -0.707, 0.707)
		local p = IntervalComparison(parallelness, -0.707, 0.707)

		local timeFactor = 0
		-- Break the pursuit into nine cases, the cross product of the
		-- target being [ahead, aside, or behind] us and heading
		-- [parallel, perpendicular, or anti-parallel] to us.
		if f == 1 and p == 1 then
			timeFactor = 4
		elseif f == 1 and p == 0 then
			timeFactor = 1.8
		elseif f == 1 and p == -1 then
			timeFactor = 0.85
		elseif f == 0 and p == 1 then
			timeFactor = 1
		elseif f == 0 and p == 0 then
			timeFactor = 0.8
		elseif f == 0 and p == -1 then
			timeFactor = 4
		elseif f == -1 and p == 1 then
			timeFactor = 0.5
		elseif f == -1 and p == 0 then
			timeFactor = 2
		elseif f == -1 and p == -1 then
			timeFactor = 2
		end

		-- estimated time until intercept of target
		local et = directTravelTime * timeFactor
		-- estimated position of target at intercept
		local et1 = (et > maxPredictionTime) and maxPredictionTime or et

		targetPosX = tx + tvx * et1
		targetPosY = ty + tvy * et1
	end
	return targetPosX, targetPosY
end

-- @return distance, unitOffsetX, unitOffsetY
function CPhysicsAIShip:GetDistAndUnitOffset(x, y, targetPosX, targetPosY)
	local dist = GetDistance_XY(x, y, targetPosX, targetPosY)
	if Mathf_Approximately(dist, 0) then
		return 0, 0, 0
	end
	return dist, (targetPosX - x) / dist, (targetPosY - y) / dist
end

-- Returns a vector points to intersection of
--  * circle centered on targetPos
--  and
--  * vertical line perpendicular to unioffset through targetPos
function CPhysicsAIShip:GetSurroundVector(unitOffsetX, unitOffsetY, dist, radius)
	-- get perpendicular vector, to measure the right dirction for ship to turn
	local verticalX, verticalY = Vector2_Perpendicular(self.m_Forward.x, self.m_Forward.y, unitOffsetX, unitOffsetY)
	if Mathf_Approximately(verticalX, 0) and Mathf_Approximately(verticalY, 0) then
		-- forward vector is approximately parallel to the offset
		verticalX = -self.m_Forward.y
		verticalY = self.m_Forward.x
	else
		verticalX, verticalY = Vector2_Normalize(verticalX, verticalY)
	end

	if dist <= radius then
		local maxSpeed = self:GetMaxSpeed()
		return verticalX * maxSpeed, verticalY * maxSpeed
	end

	-- vertical * r + unitOffset * dist
	return verticalX * radius + unitOffsetX * dist, verticalY * radius + unitOffsetY * dist
end

-------------------------------------------------------------------------------
-- 1. steer for pursuit

-- @param minAvoidCollisionDist when distance to target less then this, turn off avoid collision
function CPhysicsAIShip:SteerToPursuit(currframeId)
	local designData = GetPhysicsAIDesignData(self.m_Steering.Id)
	if not designData then return end

	local targetPOId = self.m_Steering.poId
	local maxPredictionTime = designData.maxPredictionTime
	local acceptableDistance = designData.radius
	local minAvoidCollisionDist = designData.minAvoidCollisionDist
	if not (targetPOId and maxPredictionTime and acceptableDistance and minAvoidCollisionDist) then
		return
	end
	local obj = self:GetObject()
	local target = GetObjByPhysicsObjId(targetPOId)
	if not (obj and target and obj.m_SceneId == target.m_SceneId) then return end

	local x, y = self.m_Pos.x, self.m_Pos.y
	local tx, ty = target:GetPhysicsPosv()
	if not (x and y and tx and ty) then return end

	local dist = GetDistance_XY(x, y, tx, ty)
	if dist <= acceptableDistance then return end

	if dist > minAvoidCollisionDist then
		self.m_bAvoidCollision = true
	else
		self.m_bAvoidCollision = false
	end

	local tvx, tvy = target:GetPhysicsVelocity()
	local tfx, tfy = target:GetPhysicsForwardVector()
	local targetPosX, targetPosY = self:PredictTargetFuturePos(x, y, tx, ty, dist, tvx, tvy, tfx, tfy, maxPredictionTime)

	local fx, fy = self:GetSeekVector(x, y, targetPosX, targetPosY)
	self:SetForce(fx, fy)
end

-------------------------------------------------------------------------------
-- steer for obstacles avoidance

-- @param minTimeToCollision  how far forward in time should the ship predict obstacle
function CPhysicsAIShip:SteerToAvoidObstacles(minTimeToCollision)
	local obj = self:GetObject()
	if not obj then return end

	local minDistanceToCollision = self.m_CurrentSpeed * minTimeToCollision
	if minDistanceToCollision < 0.5 then return end
	local obstaclesCenterX, obstaclesCenterY, intersectX, intersectY = obj:GetNearestObstacle(minDistanceToCollision)
	-- when a nearest intersection was not found
	if not (obstaclesCenterX and obstaclesCenterY and intersectX and intersectY) then return end

	-- compute avoidance steering force: take offset from obstacle to me,
    -- take the component of that which is perpendicular to my forward
	-- direction, set length to maxForce, add a bit of forward component 
	local offsetX = self.m_Pos.x - obstaclesCenterX
	local offsetY = self.m_Pos.y - obstaclesCenterY
	-- make avoidance vector a alia of LastForce
	local maxSpeed = self:GetMaxSpeed()
	local fx, fy = Vector2_Perpendicular(offsetX, offsetY, self.m_Forward.x, self.m_Forward.y)
	if Mathf_Approximately(fx, 0) and Mathf_Approximately(fy, 0) then
		fx = - self.m_Forward.y
		fy = self.m_Forward.x
	else
		fx, fy = Vector2_Normalize(fx, fy)
		fx = fx + self.m_Forward.x * 0.75
		fy = fy + self.m_Forward.y * 0.75
	end
	self:SetForce(fx * maxSpeed, fy * maxSpeed)
end

-------------------------------------------------------------------------------
-- 2. steer for surround

-- @param radius  ship will surround target between radius ~ 2 * radius
function CPhysicsAIShip:SteerToSurrounding(currframeId)
	local designData = GetPhysicsAIDesignData(self.m_Steering.Id)
	if not designData then return end

	local targetPOId = self.m_Steering.poId
	local maxPredictionTime = designData.maxPredictionTime
	local radius = designData.radius
	if not (targetPOId and maxPredictionTime and radius) then
		return
	end

	local obj = self:GetObject()
	local target = GetObjByPhysicsObjId(targetPOId)
	if not (obj and target and obj.m_SceneId == target.m_SceneId) then return end

	local x, y = self.m_Pos.x, self.m_Pos.y
	local tx, ty = target:GetPhysicsPosv()
	if not (x and y and tx and ty) then return end

	local dist = GetDistance_XY(x, y, tx, ty)
	if Mathf_Approximately(dist, 0) then
		-- if ship is extremely close to target, stay
		return
	end

	self.m_bAvoidCollision = false

	local tvx, tvy = target:GetPhysicsVelocity()
	local tfx, tfy = target:GetPhysicsForwardVector()
	local targetPosX, targetPosY = self:PredictTargetFuturePos(x, y, tx, ty, dist, tvx, tvy, tfx, tfy, maxPredictionTime)
	local dist, unitX, unitY = self:GetDistAndUnitOffset(x, y, targetPosX, targetPosY)

	if dist == 0 then
		-- if ship is extremely close to target future position, stay
		return
	elseif dist > 2 * radius then
		self:SetForce(targetPosX - x, targetPosY - y)
		return
	end

	-- Try fire cannon
	local forwardness = Vector2_Dot(self.m_Forward.x, self.m_Forward.y, unitX, unitY)
	local canfire = -0.5 < forwardness and forwardness < 0.5 or false
	if canfire ~= self.m_CanFire then
		self.m_CanFire = canfire 
		if obj.AIEvent then
			obj:AIEvent("SetPhysicsShipCanFire", self.m_CanFire)
		end
	end

	local fx, fy = self:GetSurroundVector(unitX, unitY, dist, radius)
	self:SetForce(fx, fy)
end

-------------------------------------------------------------------------------
-- 3. steer for targetPos

function CPhysicsAIShip:SteerToTargetPos(currframeId)
	local designData = GetPhysicsAIDesignData(self.m_Steering.Id)
	if not designData then return end

	local tx, ty, dir, acceptableDistance = self.m_Steering.x, self.m_Steering.y, self.m_Steering.dir, designData.radius
	if not (tx and ty and acceptableDistance) then return end

	local x, y = self.m_Pos.x, self.m_Pos.y
	if not (x and y and tx and ty) then return end

	if acceptableDistance > 0 then
		local dist = GetDistance2_XY(x, y, tx, ty)
		if dist <= acceptableDistance * acceptableDistance then
			self.m_bAvoidCollision = false
			if not dir or -5 < self.m_CurrentOrientation - dir and self.m_CurrentOrientation - dir < 5 then
				local obj = self:GetObject()
				if obj and obj.AIEvent and not self.m_CanFire then
					self.m_CanFire = true
					obj:AIEvent("SteerReachTargetPos", tx, ty)
				end
				return
			end
			local rad = dir * PHYSICS_DEG_TO_RAD
			self:SetForce(0.1 * math.cos(rad), 0.1 * math.sin(rad))
			return
		end
	end

	self.m_bAvoidCollision = true

	self:SetForce(tx - x, ty - y)
end

-------------------------------------------------------------------------------
-- 4. steer for convoy

-- @param radius  ship will surround target between radius ~ 2 * radius
function CPhysicsAIShip:SteerToConvoy(currframeId)
	local designData = GetPhysicsAIDesignData(self.m_Steering.Id)
	if not designData then return end

	local targetPOId, maxPredictionTime, radius = self.m_Steering.poId, designData.maxPredictionTime, designData.radius
	if not (targetPOId and maxPredictionTime and radius) then return end

	local obj = self:GetObject()
	local target = GetObjByPhysicsObjId(targetPOId)
	if not (obj and target and obj.m_SceneId == target.m_SceneId) then return end

	local x, y = self.m_Pos.x, self.m_Pos.y
	local tx, ty = target:GetPhysicsPosv()
	if not (x and y and tx and ty) then return end

	local dist = GetDistance_XY(x, y, tx, ty)
	if Mathf_Approximately(dist, 0) then
		-- if ship is extremely close to target, stay
		return
	end

	local minAvoidCollisionDist = designData.minAvoidCollisionDist
	if dist > minAvoidCollisionDist then
		self.m_bAvoidCollision = true
	else
		self.m_bAvoidCollision = false
	end

	local tvx, tvy = target:GetPhysicsVelocity()
	local tfx, tfy = target:GetPhysicsForwardVector()
	local targetPosX, targetPosY = self:PredictTargetFuturePos(x, y, tx, ty, dist, tvx, tvy, tfx, tfy, maxPredictionTime)
	local dist, unitX, unitY = self:GetDistAndUnitOffset(x, y, targetPosX, targetPosY)

	if dist > 3 * radius then
		self:SetForce(targetPosX - x, targetPosY - y)
		return
	end

	-- Try fire cannon
	local forwardness = Vector2_Dot(self.m_Forward.x, self.m_Forward.y, unitX, unitY)
	local canfire = dist <= 3 * radius and -0.5 < forwardness and forwardness < 0.5 or false
	if canfire ~= self.m_CanFire then
		self.m_CanFire = canfire 
		if obj.AIEvent then
			obj:AIEvent("SetPhysicsShipCanFire", self.m_CanFire)
		end
	end

	local parallelness = Vector2_Dot(self.m_Forward.x, self.m_Forward.y, tfx, tfy)
	if parallelness > 0.707 and -0.5 < forwardness and forwardness < 0.5 then
		self:SetForce(tvx, tvy)
		return
	else
		local leftX, leftY = -tfy, tfx
		local rightness = Vector2_Dot(leftX, leftY, unitX, unitY)
		local mul = rightness >= 0 and -radius or radius
		leftX, leftY = leftX * mul, leftY * mul

		local seakVectorX, seakVectorY = self:GetSeekVector(x, y, targetPosX + leftX, targetPosY + leftY) 
		
		if -0.3 < seakVectorX and seakVectorX < 0.3 and -0.3 < seakVectorY and seakVectorY < 0.3 then
			self:SetForce(tfx * 0.01, tfx * 0.01)
		else
			self:SetForce(seakVectorX, seakVectorY)
		end
	end
end

-------------------------------------------------------------------------------
-- 5. steer for forward

function CPhysicsAIShip:SteerToForward(currframeId)
	local designData = GetPhysicsAIDesignData(self.m_Steering.Id)
	if not designData then return end
	self.m_bAvoidCollision = false

	local maxSpeed = self:GetMaxSpeed()
	self:SetForce(self.m_Forward.x * maxSpeed, self.m_Forward.y * maxSpeed)
end

-------------------------------------------------------------------------------
-- 6. steer for stay still

function CPhysicsAIShip:SteerToStayStill(currframeId)
end

-------------------------------------------------------------------------------
-- 7. steer for turn in place

function CPhysicsAIShip:SteerToTurnInPlace(currframeId)
	local designData = GetPhysicsAIDesignData(self.m_Steering.Id)
	if not designData then return end

	self.m_MaxSpeed = 0.1
	self.m_bAvoidCollision = false
	local tx, ty = self.m_Steering.x, self.m_Steering.y
	if not (tx and ty) then return end

	local x, y = self.m_Pos.x, self.m_Pos.y
	self:SetForce(tx - x, ty - y)
end

-------------------------------------------------------------------------------
-- 8. steer for clockwise turn in place

function CPhysicsAIShip:SteerToTurnClockwiseInPlace(currframeId)
	local designData = GetPhysicsAIDesignData(self.m_Steering.Id)
	if not designData then return end

	self.m_MaxSpeed = 0.1
	self.m_bAvoidCollision = false

	local fx, fy = self.m_Forward.x, self.m_Forward.y
	self:SetForce(-fy, fx)
end

-------------------------------------------------------------------------------
-- 9. steer for follow

function CPhysicsAIShip:SteerToFollow(currframeId)
	local designData = GetPhysicsAIDesignData(self.m_Steering.Id)
	if not designData then return end

	local targetPOId, maxPredictionTime, radius = self.m_Steering.poId, designData.maxPredictionTime, designData.radius
	if not (targetPOId and maxPredictionTime and radius) then return end

	local obj = self:GetObject()
	local target = GetObjByPhysicsObjId(targetPOId)
	if not (obj and target and obj.m_SceneId == target.m_SceneId) then return end

	local x, y = self.m_Pos.x, self.m_Pos.y
	local dx, dy = self.m_Steering.dx or 0, self.m_Steering.dy or 0
	local tx, ty = target:PhysicsPredictFuturePosition(maxPredictionTime, dx, dy)
	if not (x and y and tx and ty) then return end

	local dist = GetDistance_XY(x, y, tx, ty)
	if dist <= radius then
		self.m_bAvoidCollision = false

		local tfx, tfy = target:GetPhysicsForwardVector()
		local parallelness = Vector2_Dot(self.m_Forward.x, self.m_Forward.y, tfx, tfy)
		if parallelness > 0.707 then return end

		self:SetForce(0.1 * tfx, 0.1 * tfy)
		return
	end

	self.m_bAvoidCollision = true

	self:SetForce(tx - x, ty - y)
end

-------------------------------------------------------------------------------
--for client only
function CPhysicsAIShip:SyncServerSnapshot(shot)
	if bServer then return end
	local add = true
	if self.m_Steering and shot.steering then
		--相同的steering不处理
		if self.m_Steering.Id==shot.steering.Id and self.m_Steering.beginFrame==shot.steering.beginFrame then
			add = false
		end
	end
	if add then
		self:AddSteer(shot.steering)
	end
	self.m_TargetSpeed = shot.targetSpeed
	--这个参数是5帧更新一次的，所以需要专门记录一下
	self.m_TargetOrientation = shot.targetOrientation
	self.m_MulSpeed = shot.mulSpeed
	if shot.maxSpeed then
		self.m_MaxSpeed = shot.maxSpeed
	end
	if shot.maxW then
		self.m_MaxW = shot.maxW
	end
end
function CPhysicsAIShip:RecordSnapshot(shot)
	shot.targetSpeed = self.m_TargetSpeed
	shot.targetOrientation = self.m_TargetOrientation
	shot.mulSpeed = self.m_MulSpeed
	shot.maxSpeed = self.m_MaxSpeed
	shot.maxW = self.m_MaxW

	if self.m_Steering then
		shot.steering = {}
		for key, value in pairs(self.m_Steering) do
			shot.steering[key] = value
		end
	else
		shot.steering = nil
	end
end
