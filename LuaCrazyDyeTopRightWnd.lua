--import
local GameObject			= import "UnityEngine.GameObject"
local Vector3               = import "UnityEngine.Vector3"
local CPos				    = import "L10.Engine.CPos"
local CEffectMgr		    = import "L10.Game.CEffectMgr"
local EnumWarnFXType        = import "L10.Game.EnumWarnFXType"

local UILabel               = import "UILabel"
local DelegateFactory		= import "DelegateFactory"

local CUIManager            = import "L10.UI.CUIManager"
local CUIResources          = import "L10.UI.CUIResources"

local CommonDefs			= import "L10.Game.CommonDefs"

local CClientMonster = import "L10.Game.CClientMonster"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local Helloween_Setting = import "L10.Game.Halloween_Setting"

--define
LuaCrazyDyeTopRightWnd = class()

--RegistChildComponent
RegistChildComponent(LuaCrazyDyeTopRightWnd, "ExpandButton",    GameObject)
RegistChildComponent(LuaCrazyDyeTopRightWnd, "CountLabel1",	    UILabel)
RegistChildComponent(LuaCrazyDyeTopRightWnd, "Content",		    GameObject)

--RegistClassMember
RegistClassMember(LuaCrazyDyeTopRightWnd,"MonsterEngineId")

--@region flow function

function LuaCrazyDyeTopRightWnd:Init()

end

function LuaCrazyDyeTopRightWnd:OnEnable()
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("SyncDyePlayScore", self, "RefreshScore")
    g_ScriptEvent:AddListener("SyncDyeSceneOverView", self, "RefreshScore")
    local btnclick = function(go)
        self:OnExpandButtonClick()
    end
    CommonDefs.AddOnClickListener(self.ExpandButton, DelegateFactory.Action_GameObject(btnclick), false)
    
    g_ScriptEvent:AddListener("ClientObjCreate", LuaCrazyDyeTopRightWnd, "OnClientObjCreate")
    g_ScriptEvent:AddListener("ClientObjDestroy", LuaCrazyDyeTopRightWnd, "OnClientObjDestroy")
    g_ScriptEvent:AddListener("SyncDyePreGenMonster", LuaCrazyDyeTopRightWnd, "CreateMonsterEffect")
end

function LuaCrazyDyeTopRightWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("SyncDyePlayScore", self, "RefreshScore")
    g_ScriptEvent:RemoveListener("SyncDyeSceneOverView", self, "RefreshScore")
    
    g_ScriptEvent:RemoveListener("ClientObjCreate", LuaCrazyDyeTopRightWnd, "OnClientObjCreate")
    g_ScriptEvent:RemoveListener("ClientObjDestroy", LuaCrazyDyeTopRightWnd, "OnClientObjDestroy")
    g_ScriptEvent:RemoveListener("SyncDyePreGenMonster", LuaCrazyDyeTopRightWnd, "CreateMonsterEffect")
end

function LuaCrazyDyeTopRightWnd:OnClientObjCreate(args)
    local data = Helloween_Setting.GetData()
    local obj = CClientObjectMgr.Inst:GetObject(args[0])
    if obj and TypeIs(obj, typeof(CClientMonster)) then
        if obj.TemplateId == data.RanSeBossId then
            self.MonsterEngineId = args[0]      --  如果是boss则记录当前EngineId
            CUIManager.ShowUI("CrazyDyeBossStateWnd")
        end
    end
end

function LuaCrazyDyeTopRightWnd:OnClientObjDestroy(args)
    if self.MonsterEngineId and self.MonsterEngineId == args[0] then        --boss销毁时关闭BossStateWnd
        CUIManager.CloseUI("CrazyDyeBossStateWnd")
    end
end

--@endregion

function LuaCrazyDyeTopRightWnd:OnExpandButtonClick()
	self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
	self.Content:SetActive(false)
    CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)


end

function LuaCrazyDyeTopRightWnd:OnHideTopAndRightTipWnd()
    self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
	-- self.Content:SetActive(true)
end

function LuaCrazyDyeTopRightWnd:RefreshScore(score)
    self.CountLabel1.text = tostring(score)
end

function LuaCrazyDyeTopRightWnd:CreateMonsterEffect(posX, posY, duration, fxType)
    local fxid = 0 
    if fxType == 1 then
        fxid = 88801313
    elseif fxType == 2 then
        fxid = 88801312
    end
    local pos = CPos(posX*64,posY*64)
    local fx = CEffectMgr.Inst:AddWorldPositionFX(fxid,pos,0,0,1,-1,EnumWarnFXType.None,nil,0,0, nil)
end
