local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local CButton = import "L10.UI.CButton"
local GameObject = import "UnityEngine.GameObject"

LuaColiseumWeeklyRewardInfoWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaColiseumWeeklyRewardInfoWnd, "RewardNumLabel", "RewardNumLabel", UILabel)
RegistChildComponent(LuaColiseumWeeklyRewardInfoWnd, "Condition1Label", "Condition1Label", UILabel)
RegistChildComponent(LuaColiseumWeeklyRewardInfoWnd, "Progress1Label", "Progress1Label", UILabel)
RegistChildComponent(LuaColiseumWeeklyRewardInfoWnd, "Condition2Label", "Condition2Label", UILabel)
RegistChildComponent(LuaColiseumWeeklyRewardInfoWnd, "Progress2Label", "Progress2Label", UILabel)
RegistChildComponent(LuaColiseumWeeklyRewardInfoWnd, "Button", "Button", CButton)
RegistChildComponent(LuaColiseumWeeklyRewardInfoWnd, "RedDot", "RedDot", GameObject)

--@endregion RegistChildComponent end

function LuaColiseumWeeklyRewardInfoWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.Button.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButtonClick()
	end)


    --@endregion EventBind end
end

function LuaColiseumWeeklyRewardInfoWnd:Init()
    local setting = Arena_Setting.GetData()
    self.RewardNumLabel.text = setting.WeeklyShopScoreReward
    self:InitConditions(setting.WeeklyShopScoreRewardNeedBattleTimesWord,setting.WeeklyShopScoreRewardNeedBattleTimes,LuaColiseumMgr.m_WeeklyBattleTimes,
        setting.WeeklyShopScoreRewardNeedWinTimesWord,setting.WeeklyShopScoreRewardNeedWinTimes,LuaColiseumMgr.m_WeeklyWinTimes)
    self:OnSendArenaInfo()
end

function LuaColiseumWeeklyRewardInfoWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSendArenaInfo", self, "OnSendArenaInfo")
end

function LuaColiseumWeeklyRewardInfoWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnSendArenaInfo", self, "OnSendArenaInfo")
end

function LuaColiseumWeeklyRewardInfoWnd:OnSendArenaInfo()
    self.RedDot:SetActive(LuaColiseumMgr.m_WeeklyScoreRewardState == 1)
    self.Button.Enabled = LuaColiseumMgr.m_WeeklyScoreRewardState ~= 2
end
--@region UIEvent

function LuaColiseumWeeklyRewardInfoWnd:OnButtonClick()
    if LuaColiseumMgr.m_WeeklyScoreRewardState == 0 then
        g_MessageMgr:ShowMessage("ColiseumWeeklyRewardInfoWnd_Condition_Failed")
        return
    end
    Gac2Gas.RequestGetWeeklyArenaScoreReward()
end


--@endregion UIEvent
function LuaColiseumWeeklyRewardInfoWnd:InitConditions(str1,conditionValue1,curConditionValue1,str2,conditionValue2,curConditionValue2)
    self.Condition1Label.text = str1
    self.Condition2Label.text = str2
    self.Progress1Label.text = SafeStringFormat3("%d/%d",curConditionValue1 ,conditionValue1)
    self.Progress2Label.text = SafeStringFormat3("%d/%d",curConditionValue2 ,conditionValue2)
    self.Progress1Label.color = (curConditionValue1 < conditionValue1) and NGUIText.ParseColor24("ff5050", 0) or NGUIText.ParseColor24("00ff60", 0)
    self.Progress2Label.color = (curConditionValue2 < conditionValue2) and NGUIText.ParseColor24("ff5050", 0) or NGUIText.ParseColor24("00ff60", 0)
end
