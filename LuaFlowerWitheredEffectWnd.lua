require("common/common_include")

local CBaseWnd = import "L10.UI.CBaseWnd"
local UIPanel = import "UIPanel"
local CUIFx = import "L10.UI.CUIFx"
local Time = import "UnityEngine.Time"
local CommonDefs = import "L10.Game.CommonDefs"
local CUITexture = import "L10.UI.CUITexture"
local UITweener = import "UITweener"

LuaFlowerWitheredEffectWnd = class()
RegistClassMember(LuaFlowerWitheredEffectWnd,"m_CloseButton")
RegistClassMember(LuaFlowerWitheredEffectWnd,"m_FlowerTexture")
RegistClassMember(LuaFlowerWitheredEffectWnd,"m_FlowerTweenAlpha")
RegistClassMember(LuaFlowerWitheredEffectWnd,"m_MagicFx")
RegistClassMember(LuaFlowerWitheredEffectWnd,"m_PressRoot")
RegistClassMember(LuaFlowerWitheredEffectWnd,"m_ProgressBar")
RegistClassMember(LuaFlowerWitheredEffectWnd,"m_LongPressButton")

RegistClassMember(LuaFlowerWitheredEffectWnd,"m_FlowerTbl")
RegistClassMember(LuaFlowerWitheredEffectWnd,"m_FlowerIndex")
RegistClassMember(LuaFlowerWitheredEffectWnd,"m_Pressing")
RegistClassMember(LuaFlowerWitheredEffectWnd,"m_PressDuration")
RegistClassMember(LuaFlowerWitheredEffectWnd,"m_PressStartTime")

RegistClassMember(LuaFlowerWitheredEffectWnd,"m_FlowerAniDuration")
RegistClassMember(LuaFlowerWitheredEffectWnd,"m_FlowerAniFadeInTime")
RegistClassMember(LuaFlowerWitheredEffectWnd,"m_FlowerAniFadeOutTime")

function LuaFlowerWitheredEffectWnd:Awake()
    self.gameObject:GetComponent(typeof(UIPanel)).IgnoreIphoneXMargin = true
end

function LuaFlowerWitheredEffectWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaFlowerWitheredEffectWnd:Init()
	self.m_CloseButton = self.transform:Find("Anchor/CloseButton").gameObject
	CommonDefs.AddOnClickListener(self.m_CloseButton, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)

	self.m_FlowerTexture = self.transform:Find("Anchor/FlowerTexture"):GetComponent(typeof(CUITexture))
	self.m_FlowerTweenAlpha = self.m_FlowerTexture.gameObject:GetComponent(typeof(UITweener))
	self.m_MagicFx = self.transform:Find("Anchor/MagicFx"):GetComponent(typeof(CUIFx))
	self.m_PressRoot = self.transform:Find("Anchor/PressRoot").gameObject
	self.m_ProgressBar = self.m_PressRoot.transform:Find("ProgressBar"):GetComponent(typeof(UIWidget))
	self.m_LongPressButton = self.m_PressRoot.transform:Find("Button").gameObject

	self.m_FlowerTweenAlpha:SetOnFinished(DelegateFactory.Callback(function()
			self:OnTeenAlphaFinished()
		end))
	self.m_FlowerTweenAlpha.enabled = false
	self.m_ProgressBar.fillAmount = 0

	self.m_FlowerTbl = {}
	table.insert(self.m_FlowerTbl, SafeStringFormat3("UI/Texture/Transparent/Material/flower_open.mat"))
	table.insert(self.m_FlowerTbl, SafeStringFormat3("UI/Texture/Transparent/Material/flower_die.mat"))
	table.insert(self.m_FlowerTbl, SafeStringFormat3("UI/Texture/Transparent/Material/flower_died.mat"))

	CommonDefs.AddOnPressListener(self.m_LongPressButton,DelegateFactory.Action_GameObject_bool(function (go, pressed)
		self:OnButtonPress(go, pressed)
	end),false)

	self.m_PressStartTime = 0
	self.m_PressDuration = CLuaTaskMgr.m_FlowerWitheredEffectPressDuration
	
	self.m_FlowerIndex = 1
   	self.m_FlowerTexture:LoadMaterial(self.m_FlowerTbl[self.m_FlowerIndex])

	self.m_FlowerAniDuration = 2
	self.m_FlowerAniFadeInTime = 0.6
	self.m_FlowerAniFadeOutTime = 0.6

	g_MessageMgr:ShowMessage("Flower_Withered_Effect_Press_Tip")
end

--Begin Press 逻辑
function LuaFlowerWitheredEffectWnd:OnButtonPress(go, pressed)
	if pressed then
		self:OnPressStart()
	else
		self:OnPressEnd()
	end
end

function LuaFlowerWitheredEffectWnd:CheckLongPress()
	if self.m_Pressing then
		local duration = Time.realtimeSinceStartup - self.m_PressStartTime
		self.m_ProgressBar.fillAmount = duration/self.m_PressDuration
		if duration>self.m_PressDuration then
			self:OnPressSuccess()
		end

		if UICamera.lastHit.collider == nil or UICamera.lastHit.collider.gameObject~=self.m_LongPressButton then
			self:OnPressEnd()
		end
	end
end

function LuaFlowerWitheredEffectWnd:OnPressStart()
	self.m_Pressing = true
	self.m_PressStartTime = Time.realtimeSinceStartup
	self.m_MagicFx:LoadFx("fx/ui/prefab/UI_changanhua_kuwei.prefab")
end

function LuaFlowerWitheredEffectWnd:OnPressEnd()
	self.m_Pressing = false
	self.m_ProgressBar.fillAmount = 0
	self.m_MagicFx:DestroyFx()
end

function LuaFlowerWitheredEffectWnd:OnPressSuccess()
	self.m_Pressing = false
	g_MessageMgr:ShowMessage("Flower_Withered_Effect_Press_Success")
	self.m_PressRoot:SetActive(false)
	self.m_MagicFx:DestroyFx()
	self:FadeOut()
end
--End Press 逻辑

function LuaFlowerWitheredEffectWnd:Update()
	self:CheckLongPress()
end

function LuaFlowerWitheredEffectWnd:OnComplete()
	CLuaTaskMgr.CompleteFlowerWitheredEffectTask()
	self:Close()
end

function LuaFlowerWitheredEffectWnd:OnTeenAlphaFinished()
	
	if self.m_FlowerTweenAlpha.value < 0.01 then -- fade out over
		if self:HasNextFlower() then
			self:ShowNextFlower()
		else
			self:OnComplete()
		end
	else -- fade in over
		self:FadeOut()
	end
end

function LuaFlowerWitheredEffectWnd:HasNextFlower()
	return self.m_FlowerTbl and self.m_FlowerIndex and self.m_FlowerTbl[self.m_FlowerIndex + 1]
end

function LuaFlowerWitheredEffectWnd:ShowNextFlower()
	if not self:HasNextFlower() then return end
	self.m_FlowerIndex = self.m_FlowerIndex + 1
	self.m_FlowerTexture:LoadMaterial(self.m_FlowerTbl[self.m_FlowerIndex])
	self:FadeIn()
end

function LuaFlowerWitheredEffectWnd:FadeIn()
	self.m_FlowerTweenAlpha.delay = 0
	self.m_FlowerTweenAlpha.duration = self.m_FlowerAniFadeInTime
	self.m_FlowerTweenAlpha.from = 0
	self.m_FlowerTweenAlpha.to = 1
	self.m_FlowerTweenAlpha:ResetToBeginning()
	self.m_FlowerTweenAlpha:PlayForward()
end

function LuaFlowerWitheredEffectWnd:FadeOut()
	self.m_FlowerTweenAlpha.delay = math.max(0, self.m_FlowerAniDuration - self.m_FlowerAniFadeInTime - self.m_FlowerAniFadeOutTime)
	self.m_FlowerTweenAlpha.duration = self.m_FlowerAniFadeOutTime
	self.m_FlowerTweenAlpha.from = 1
	self.m_FlowerTweenAlpha.to = 0
	self.m_FlowerTweenAlpha:ResetToBeginning()
	self.m_FlowerTweenAlpha:PlayForward()
end
