local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CGamePlayMgr = import "L10.Game.CGamePlayMgr"
local CScene = import "L10.Game.CScene"

LuaHalloween2021View = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHalloween2021View, "TaskDesc", "TaskDesc", UILabel)
RegistChildComponent(LuaHalloween2021View, "LeaveBtn", "LeaveBtn", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaHalloween2021View, "m_Sprite")
RegistClassMember(LuaHalloween2021View, "m_BtnPosDevY")
RegistClassMember(LuaHalloween2021View, "m_SpriteHeightDevY")
RegistClassMember(LuaHalloween2021View, "m_GetHuaCheMapPosTick")

function LuaHalloween2021View:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.LeaveBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeaveBtnClick()
	end)


    --@endregion EventBind end
end

function LuaHalloween2021View:Init()
	self:CancelTick()
	self.m_GetHuaCheMapPosTick = RegisterTick(function ()
		self:RequestHuaCheMapPos()
	end,3000)
	self.m_Sprite = self.gameObject:GetComponent(typeof(UISprite))
	self.m_SpriteHeightDevY = self.m_Sprite.height - self.TaskDesc.height
	self.m_BtnPosDevY = self.LeaveBtn.transform.localPosition.y + self.m_Sprite.height
	self:OnStateUpdate()
end

function LuaHalloween2021View:RequestHuaCheMapPos()
	if not LuaHalloween2021Mgr:IsInYouHuaChePlay() then return end
	Gac2Gas.RequestQueryHuaCheMapPos()
end

function LuaHalloween2021View:CancelTick()
	if self.m_GetHuaCheMapPosTick then
		UnRegisterTick(self.m_GetHuaCheMapPosTick)
		self.m_GetHuaCheMapPosTick = nil
	end
end

function LuaHalloween2021View:OnEnable()
	g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
	g_ScriptEvent:AddListener("OnSyncHalloweenYouHuaChePlayInfo", self, "OnSyncHalloweenYouHuaChePlayInfo")
	g_ScriptEvent:AddListener("OnUpdateHuaCheMapPos", self, "OnSceneRemainTimeUpdate")
	g_ScriptEvent:AddListener("OnJieLiangYuanSyncPlayInfo", self, "OnJieLiangYuanSyncPlayInfo")
end

function LuaHalloween2021View:OnDisable()
	g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
	g_ScriptEvent:RemoveListener("OnSyncHalloweenYouHuaChePlayInfo", self, "OnSyncHalloweenYouHuaChePlayInfo")
	g_ScriptEvent:RemoveListener("OnUpdateHuaCheMapPos", self, "OnSceneRemainTimeUpdate")
	g_ScriptEvent:RemoveListener("OnJieLiangYuanSyncPlayInfo", self, "OnJieLiangYuanSyncPlayInfo")
	self:CancelTick()
end

--@region UIEvent

function LuaHalloween2021View:OnLeaveBtnClick()
	CGamePlayMgr.Inst:LeavePlay()
end

--@endregion UIEvent

function LuaHalloween2021View:OnSyncHalloweenYouHuaChePlayInfo()
	self:OnStateUpdate()
end

function LuaHalloween2021View:OnJieLiangYuanSyncPlayInfo()
	self:OnStateUpdate()
end


function LuaHalloween2021View:OnSceneRemainTimeUpdate(args)
	if not LuaHalloween2021Mgr:IsInYouHuaChePlay() then 
		if LuaHalloween2021Mgr:IsInJieLiangYuanPlay() and 
		((LuaHalloween2021Mgr.jieYuanState == EnumJieLiangYuanStage.eFindXinNiang) or (LuaHalloween2021Mgr.jieYuanState == EnumJieLiangYuanStage.eKaiBaoXiang)) then 
			self:OnStateUpdate()
		end
		return 
	end
	if LuaHalloween2021Mgr.HuaCheState ~= 2 then return end
	local totalSeconds = 0
    if CScene.MainScene then
		totalSeconds = CScene.MainScene.ShowTime
    end
	local timeStr = SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
	local posStr = SafeStringFormat3("(%02d,%02d)", math.floor(LuaHalloween2021Mgr.flowerParadeX), math.floor(LuaHalloween2021Mgr.flowerParadeY))
	self.TaskDesc.text = g_MessageMgr:FormatMessage("Halloween2021_FlowerParadeView_Text2",posStr,timeStr)
	self:UpdateView()
end

function LuaHalloween2021View:UpdateView()
	self.m_Sprite.height = self.TaskDesc.height + self.m_SpriteHeightDevY
	Extensions.SetLocalPositionY(self.LeaveBtn.transform,self.m_BtnPosDevY - self.m_Sprite.height)
end

function LuaHalloween2021View:OnStateUpdate()
	if LuaHalloween2021Mgr:IsInJieLiangYuanPlay() then
		if LuaHalloween2021Mgr.jieYuanState <= EnumJieLiangYuanStage.eNpcPuzzle then
			self.TaskDesc.text = g_MessageMgr:FormatMessage(LuaHalloween2021Mgr.isSpy and "Halloween2021_JieYuan1_SpyText_NpcPuzzle" or "Halloween2021_JieYuan1_Text_NpcPuzzle",self:GetPuzzleText())
		elseif LuaHalloween2021Mgr.jieYuanState == EnumJieLiangYuanStage.ePickPuzzle then
			self.TaskDesc.text = g_MessageMgr:FormatMessage(LuaHalloween2021Mgr.isSpy and "Halloween2021_JieYuan1_SpyText_PickPuzzle" or "Halloween2021_JieYuan1_Text_PickPuzzle",self:GetPuzzleText())
		elseif LuaHalloween2021Mgr.jieYuanState == EnumJieLiangYuanStage.eFindXinNiang then
			self:UpdateJieYuanTextWithTime(LuaHalloween2021Mgr.isSpy and "Halloween2021_JieYuan2_SpyText" or "Halloween2021_JieYuan2_Text")
		elseif LuaHalloween2021Mgr.jieYuanState == EnumJieLiangYuanStage.eKaiBaoXiang then
			self:UpdateJieYuanTextWithTime("Halloween2021_JieYuan3_Text")
		else
			self.TaskDesc.text = g_MessageMgr:FormatMessage("Halloween2021_JieYuan4_Text")
		end
	elseif LuaHalloween2021Mgr:IsInYouHuaChePlay() then
		if LuaHalloween2021Mgr.HuaCheState == 1 then
			self.TaskDesc.text = g_MessageMgr:FormatMessage("Halloween2021_FlowerParadeView_Text1")
		elseif LuaHalloween2021Mgr.HuaCheState == 2 then
			self:OnSceneRemainTimeUpdate()
		end
	end
	self:UpdateView()
end

function LuaHalloween2021View:GetPuzzleText()
	local msg = ""
	for index,t in pairs(LuaHalloween2021Mgr.jieyuanPuzzlesInfo) do
		local data = Halloween2021_Puzzle.GetData(t.id)
		if data then
			msg = SafeStringFormat3("%s%s(%d/1)",msg..((index == 1) and "" or "\n"), data.Desc,t.finished and "1" or "0")
		end
	end
	return msg
end

function LuaHalloween2021View:UpdateJieYuanTextWithTime(msg)
	if CScene.MainScene then
		local leaveTime = CScene.MainScene.ShowTime
		local timeStr = SafeStringFormat3("%02d:%02d", math.floor(leaveTime / 60), leaveTime % 60)
		self.TaskDesc.text = g_MessageMgr:FormatMessage(msg,timeStr)
	end
end
