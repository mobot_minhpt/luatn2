local DelegateFactory  = import "DelegateFactory"
local CScene=import "L10.Game.CScene"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local LocalString = import "LocalString"
local Message_Message = import "L10.Game.Message_Message"
local MessageMgr = import "L10.Game.MessageMgr"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"

LuaLiuYi2021TangGuoMgr = class()

-- 保存player数据 以防止切换场景时访问不到CClientMainPlayer
LuaLiuYi2021TangGuoMgr.m_Name = nil
LuaLiuYi2021TangGuoMgr.m_Id = nil
LuaLiuYi2021TangGuoMgr.m_Class = nil
LuaLiuYi2021TangGuoMgr.m_Gender = nil
LuaLiuYi2021TangGuoMgr.m_Level = nil
LuaLiuYi2021TangGuoMgr.m_ServerName = nil
-- 奖励界面信息
LuaLiuYi2021TangGuoMgr.m_BabyType = nil
LuaLiuYi2021TangGuoMgr.m_Rank = nil
LuaLiuYi2021TangGuoMgr.m_Score = nil
-- 倒计时
LuaLiuYi2021TangGuoMgr.m_CountDown = nil
LuaLiuYi2021TangGuoMgr.m_TickTime = nil
LuaLiuYi2021TangGuoMgr.m_Tick = nil
-- patch策划数据
LuaLiuYi2021TangGuoMgr.m_PatchAction = nil

function LuaLiuYi2021TangGuoMgr:IsInGameplay()
    if CScene.MainScene then
        local gamePlayId=CScene.MainScene.GamePlayDesignId
        if gamePlayId == LiuYi2021_Setting.GetData().GameplayId then
            return true
        end
    end
    return false
end

function LuaLiuYi2021TangGuoMgr:SetSceneMode()
	if CScene.MainScene then
        local gamePlayId=CScene.MainScene.GamePlayDesignId
        if gamePlayId == LiuYi2021_Setting.GetData().GameplayId then
            CScene.MainScene.ShowLeavePlayButton = true
            CScene.MainScene.ShowTimeCountDown = true
            CScene.MainScene.ShowMonsterCountDown = false
        end
    end
	if CClientMainPlayer.Inst then
        LuaLiuYi2021TangGuoMgr.m_Name = CClientMainPlayer.Inst.Name
        LuaLiuYi2021TangGuoMgr.m_Id = CClientMainPlayer.Inst.Id
        LuaLiuYi2021TangGuoMgr.m_Class = CClientMainPlayer.Inst.Class
        LuaLiuYi2021TangGuoMgr.m_Gender = CClientMainPlayer.Inst.Gender
        LuaLiuYi2021TangGuoMgr.m_Level = CClientMainPlayer.Inst.Level
        LuaLiuYi2021TangGuoMgr.m_ServerName = CClientMainPlayer.Inst:GetMyServerName()
    end
end

function LuaLiuYi2021TangGuoMgr:ShowResultWnd(babyType, rank, score)
	self.m_BabyType = babyType
	self.m_Rank = rank
	self.m_Score = score
	CUIManager.ShowUI(CLuaUIResources.LiuYi2021TangGuoResultWnd)
end

function LuaLiuYi2021TangGuoMgr:ShowTipAndChoose(countDown)
    self:PatchMsg()
	self.m_CountDown = countDown
    self.m_TickTime = 0
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end

    g_MessageMgr:ShowMessage("LiuYi_2021_TangGuo_Rule_Desc")
    self.m_Tick = RegisterTick(function()
        self.m_CountDown = self.m_CountDown - 1
        self.m_TickTime = self.m_TickTime + 1
        if not CUIManager.IsLoaded(CUIResources.MessageTipWnd) then
            UnRegisterTick(self.m_Tick)
            CUIManager.ShowUI(CLuaUIResources.LiuYi2021TangGuoChooseWnd)
        end

        if self.m_TickTime >=5 then
            CUIManager.CloseUI(CUIResources.MessageTipWnd)
        end
    end, 1000)
end

function LuaLiuYi2021TangGuoMgr:PatchMsg()
    MessageMgr.s_ShowErrorMessage = true
    Message_Message.PatchField(54000019, "Message", g_MessageMgr:FormatMessage("Liuyi2021_TangGuo_Move_Tip"))

    if self.m_PatchAction == nil then
        self.m_PatchAction = DelegateFactory.Action(function()
            self:PatchBackMsg()
        end)
        EventManager.AddListenerInternal(EnumEventType.MainPlayerDestroyed, self.m_PatchAction)
    end
end

function LuaLiuYi2021TangGuoMgr:PatchBackMsg()
    MessageMgr.s_ShowErrorMessage = false
    Message_Message.PatchField(54000019, "Message", LocalString.GetString("在%s状态下不能这样做"))

    if self.m_PatchAction ~= nil then
        EventManager.RemoveListenerInternal(EnumEventType.MainPlayerDestroyed, self.m_PatchAction)
        self.m_PatchAction = nil
    end
end

function LuaLiuYi2021TangGuoMgr:AddTangGuoWuFx()
    local itemPath = LiuYi2021_Setting.GetData().TangGuoSceneItemPath
    local obj = CRenderScene.Inst and CRenderScene.Inst.transform:Find("Models/Buildings/jz_liuyi_001_001")
    if obj then
        local fx = LiuYi2021_Setting.GetData().TangGuoSceneFx
        CEffectMgr.Inst:AddWorldPositionFX(fx[0], obj.transform.position, 0, 0, 1, -1, EnumWarnFXType.None, nil, 0, 0, nil)
    end
end
