local UISprite   = import "UISprite"
local UITable    = import "UITable"
local CUITexture = import "L10.UI.CUITexture"
local UILabel    = import "UILabel"
local GameObject = import "UnityEngine.GameObject"

LuaBuffInfoWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaBuffInfoWnd, "Background", "Background", UISprite)
RegistChildComponent(LuaBuffInfoWnd, "LabelTemplate", "LabelTemplate", GameObject)
RegistChildComponent(LuaBuffInfoWnd, "Table", "Table", UITable)
RegistChildComponent(LuaBuffInfoWnd, "SkillIcon", "SkillIcon", CUITexture)
RegistChildComponent(LuaBuffInfoWnd, "NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaBuffInfoWnd, "StatueLabel", "StatueLabel", UILabel)

--@endregion RegistChildComponent end

function LuaBuffInfoWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaBuffInfoWnd:Init()
    local buffData = Buff_Buff.GetData(LuaBuffInfoWndMgr.m_BuffTemplateId)
    if buffData then
        self.NameLabel.text = buffData.Name
        self.SkillIcon:LoadMaterial(buffData.Icon)
        self:AddDesc(buffData.Display)
    end

    if LuaBuffInfoWndMgr.m_isEnable then
        self.StatueLabel.text = LocalString.GetString("[00FF60]生效中")
    else
        CUICommonDef.SetActive(self.SkillIcon.gameObject, false, true)
        self.StatueLabel.text = LocalString.GetString("未生效")
        self.StatueLabel.color = Color(1,1,1,0.5)
    end

    -- 额外显示信息
    for i=1, #LuaBuffInfoWndMgr.m_ExtraInfoList do
        self:AddDesc(LuaBuffInfoWndMgr.m_ExtraInfoList[i])
    end

    self.Table:Reposition()

    -- 调整高度
    local b = NGUIMath.CalculateRelativeWidgetBounds(self.Table.transform)
    self.Background.height = b.size.y + 20
    LuaUtils.SetLocalPositionY(self.transform:Find("Anchor"), self.Background.height / 2)
end

function LuaBuffInfoWnd:AddDesc(desc)
    local g = NGUITools.AddChild(self.Table.gameObject, self.LabelTemplate)
    g:SetActive(true)
    g:GetComponent(typeof(UILabel)).text = desc
end

--@region UIEvent

--@endregion UIEvent

