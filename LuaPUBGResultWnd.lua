require("3rdParty/ScriptEvent")
require("common/common_include")

LuaPUBGResultWnd = class()

RegistChildComponent(LuaPUBGResultWnd, "ShareBtn", GameObject)
RegistChildComponent(LuaPUBGResultWnd, "ResultRankLabel", UILabel)
RegistChildComponent(LuaPUBGResultWnd, "TotalTeamCountLabel", UILabel)
RegistChildComponent(LuaPUBGResultWnd, "PlayerInfoItem1", GameObject)
RegistChildComponent(LuaPUBGResultWnd, "PlayerInfoItem2", GameObject)
RegistChildComponent(LuaPUBGResultWnd, "PlayerInfoItem3", GameObject)

RegistClassMember(LuaPUBGResultWnd, "PlayerInfos")
RegistClassMember(LuaPUBGResultWnd, "PlayerInfoItems")
RegistClassMember(LuaPUBGResultWnd, "PlayerSize")

function LuaPUBGResultWnd:Init()
    self.PlayerSize = 3
    self.PlayerInfoItems = {}
    table.insert(self.PlayerInfoItems, self.PlayerInfoItem1)
    table.insert(self.PlayerInfoItems, self.PlayerInfoItem2)
    table.insert(self.PlayerInfoItems, self.PlayerInfoItem3)

    CommonDefs.AddOnClickListener(self.ShareBtn, DelegateFactory.Action_GameObject(function (go)
        self:OnShareBtnClicked(go)
    end), false)

    self:InitPlayerInfos()
    self:UpdateBest()
end

function LuaPUBGResultWnd:InitPlayerInfos()
    for i = 1, self.PlayerSize do
        if i > #LuaPUBGMgr.m_ResultTeamInfos then
            self.PlayerInfoItems[i]:SetActive(false)
        else
            self.PlayerInfoItems[i]:SetActive(true)
            self:InitPlayerInfoItem(self.PlayerInfoItems[i], LuaPUBGMgr.m_ResultTeamInfos[i])
        end
    end
    self.ResultRankLabel.text = tostring(LuaPUBGMgr.m_ResultRank)
    self.TotalTeamCountLabel.text = "/"..tostring(LuaPUBGMgr.m_ResultTotalTeamCounter)
end

-- {name, gender, class, score, totalScorem, killNum, dps, liveDuration}
function LuaPUBGResultWnd:InitPlayerInfoItem(item, info)
    if not item then return end

    local Portrait = item.transform:Find("Portrait"):GetComponent(typeof(CUITexture))
    local PlayerNameLabel = item.transform:Find("PlayerNameLabel"):GetComponent(typeof(UILabel))
    local ScoreLabel = item.transform:Find("ScoreLabel"):GetComponent(typeof(UILabel))
    local KillLabel = item.transform:Find("KillLabel"):GetComponent(typeof(UILabel))
    local DPSLabel = item.transform:Find("DPSLabel"):GetComponent(typeof(UILabel))
    local TimeLabel = item.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
    local Best = item.transform:Find("Best").gameObject

    if not info then return end

    local portraitName = CUICommonDef.GetPortraitName(info.class, info.gender, 0)
    Portrait:LoadNPCPortrait(portraitName, false)

    PlayerNameLabel.text = tostring(info.name)
    ScoreLabel.text = SafeStringFormat3("+%d(%d)", info.score, info.totalScorem)
    KillLabel.text = tostring(info.killNum)
    DPSLabel.text = tostring(info.dps)
    TimeLabel.text = self:Second2Minus(info.liveDuration)
    Best:SetActive(false)
end

function LuaPUBGResultWnd:UpdateBest()
    local bestCounter = 1
    if #LuaPUBGMgr.m_ResultTeamInfos > 1 then
        for i = 2, #LuaPUBGMgr.m_ResultTeamInfos do
            if LuaPUBGMgr.m_ResultTeamInfos[i].score > LuaPUBGMgr.m_ResultTeamInfos[bestCounter].score then
                bestCounter = i
            end
        end
    end
    for j = 1, #self.PlayerInfoItems do
        self:UpdateBestItem(self.PlayerInfoItems[j], j == bestCounter)
    end
end

function LuaPUBGResultWnd:UpdateBestItem(item, isBest)
    local Best = item.transform:Find("Best").gameObject
    Best:SetActive(isBest)
end

function LuaPUBGResultWnd:Second2Minus(time)
	local minutes = math.floor(time / 60)
	local seconds = time - minutes * 60
	return SafeStringFormat3("%02d:%02d", minutes, seconds)
end

function LuaPUBGResultWnd:OnShareBtnClicked(go)
    CUICommonDef.CaptureScreenAndShare()
end

function LuaPUBGResultWnd:OnEnable()
    
end

function LuaPUBGResultWnd:OnDisable()
    
end

return LuaPUBGResultWnd