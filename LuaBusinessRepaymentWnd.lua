local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CButton = import "L10.UI.CButton"
local CNumberKeyBoardMgr = import "CNumberKeyBoardMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"

LuaBusinessRepaymentWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaBusinessRepaymentWnd, "TotallPriceLab", "TotallPriceLab", UILabel)
RegistChildComponent(LuaBusinessRepaymentWnd, "QnIncreseAndDecreaseButton", "QnIncreseAndDecreaseButton", GameObject)
RegistChildComponent(LuaBusinessRepaymentWnd, "CancelBtn", "CancelBtn", CButton)
RegistChildComponent(LuaBusinessRepaymentWnd, "OKBtn", "OKBtn", CButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaBusinessRepaymentWnd, "m_DecreaseButton")
RegistClassMember(LuaBusinessRepaymentWnd, "m_IncreaseButton")
RegistClassMember(LuaBusinessRepaymentWnd, "m_InputLab")
RegistClassMember(LuaBusinessRepaymentWnd, "m_InputNum")
RegistClassMember(LuaBusinessRepaymentWnd, "m_InputNumMax")
RegistClassMember(LuaBusinessRepaymentWnd, "m_TipLab")

function LuaBusinessRepaymentWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_DecreaseButton   = self.QnIncreseAndDecreaseButton.transform:Find("DecreaseButton"):GetComponent(typeof(QnButton))
    self.m_IncreaseButton   = self.QnIncreseAndDecreaseButton.transform:Find("IncreaseButton"):GetComponent(typeof(QnButton))
    self.m_InputLab         = self.QnIncreseAndDecreaseButton.transform:Find("InputLab"):GetComponent(typeof(UILabel))
    self.m_TipLab           = self.transform:Find("Anchor/Tip"):GetComponent(typeof(UILabel))

    UIEventListener.Get(self.m_DecreaseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDecreaseButtonClick()
	end)

	UIEventListener.Get(self.m_IncreaseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnIncreaseButtonClick()
	end)

	UIEventListener.Get(self.m_InputLab.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnInputLabClick()
	end)

    UIEventListener.Get(self.CancelBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCancelBtnClick()
	end)

	UIEventListener.Get(self.OKBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOKBtnClick()
	end)
    
    self.m_TipLab.text = g_MessageMgr:FormatMessage("BUSINESS_REPAYMENT_TIP", tonumber(SafeStringFormat3("%.2f", Business_Setting.GetData().OwingRate * 100)))
end

function LuaBusinessRepaymentWnd:Init()
    self.TotallPriceLab.text = SafeStringFormat3("%.f", LuaBusinessMgr.m_Debt)
    self.m_InputNum = math.min( math.floor(LuaBusinessMgr.m_OwnMoney / 2), LuaBusinessMgr.m_Debt)
	self.m_InputNum = math.min(self.m_InputNum, System.Int32.MaxValue - 2)
    self.m_InputNumMax = math.min(LuaBusinessMgr.m_OwnMoney, LuaBusinessMgr.m_Debt)
	self.m_InputNumMax = math.max(0, self.m_InputNumMax)
	self.m_InputNumMax = math.min(self.m_InputNumMax, System.Int32.MaxValue - 2)
    self:SetInputNum(self.m_InputNum)
end

--@region UIEvent

--@endregion UIEvent

function LuaBusinessRepaymentWnd:OnDecreaseButtonClick()
    self:SetInputNum(self.m_InputNum - 1)
end

function LuaBusinessRepaymentWnd:OnIncreaseButtonClick()
    self:SetInputNum(self.m_InputNum + 1)
end

function LuaBusinessRepaymentWnd:OnInputLabClick()
    CNumberKeyBoardMgr.ShowNumberKeyBoardWithRange(0, self.m_InputNumMax, self.m_InputNum, 100, DelegateFactory.Action_int(function (val) 
		self.m_InputLab.text = val
    end), DelegateFactory.Action_int(function (val) 
        self:SetInputNum(val)
    end), self.m_InputLab, AlignType.Right, true)
end

function LuaBusinessRepaymentWnd:SetInputNum(num)
	if num < 0 then
		num = 0
	end

	if num > self.m_InputNumMax then
		num = self.m_InputNumMax
	end

	self.m_InputNum = num
	self.m_InputLab.text = num

	self.m_IncreaseButton.Enabled = self.m_InputNumMax - num > 0
	self.m_DecreaseButton.Enabled = num > 0
	if self.m_InputNumMax - num < 0 then
		if self.m_IncreaseTick ~= nil then
			UnRegisterTick(self.m_IncreaseTick)
			self.m_IncreaseTick = nil
		end
	end

	if num < 0 then
		if self.m_DecreaseTick ~= nil then
			UnRegisterTick(self.m_DecreaseTick)
			self.m_DecreaseTick = nil
		end
	end
end

function LuaBusinessRepaymentWnd:OnCancelBtnClick()
    CUIManager.CloseUI(CLuaUIResources.BusinessRepaymentWnd)
end

function LuaBusinessRepaymentWnd:OnOKBtnClick()
	Gac2Gas.TradeSimulationReturnMoney(self.m_InputNum)
    CUIManager.CloseUI(CLuaUIResources.BusinessRepaymentWnd)
end