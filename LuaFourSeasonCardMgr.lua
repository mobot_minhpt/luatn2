local CGuideMgr=import "L10.Game.Guide.CGuideMgr"
local Animation = import "UnityEngine.Animation"
local CConversationMgr = import "L10.Game.CConversationMgr"

LuaFourSeasonCardMgr = {}

LuaFourSeasonCardMgr.Music = {
    bgm = "event:/L10/L10_music/MUS_sishixi_mono",
    lose = "event:/L10/L10_UI/SiShiXi/SiShiXi_Lose",
    tie = "event:/L10/L10_UI/SiShiXi/SiShiXi_Tie",
    win = "event:/L10/L10_UI/SiShiXi/SiShiXi_Win",
    collect = "event:/L10/L10_UI/SiShiXi/card_collect", -- 收牌
    deal = "event:/L10/L10_UI/SiShiXi/card_deal", -- 发牌
    play = "event:/L10/L10_UI/SiShiXi/click_deal", -- 出牌点击
    info = "event:/L10/L10_UI/SiShiXi/click_info", -- 信息点击
    crack = "event:/L10/L10_UI/SiShiXi/card_crack", -- 结算碎裂
    epic = "event:/L10/L10_UI/SiShiXi/normal_epic",  -- 普通牌变珍稀
    showup = "event:/L10/L10_UI/SiShiXi/epic_showup", -- 珍稀牌生效
    pile = "event:/L10/L10_UI/SiShiXi/click_epic", -- 珍稀牌堆点击
}

-- 玩家玩法数据对应Index
EnumFSC_PlayDataKey = {
    FinishGuiding = 1,           -- 是否完成引导，true/false/nil
    WinNpcTimes = 2,             -- 战胜npc次数，{ npcId = 战胜npc次数 }，为nil表示未战胜过npc，无某个npc数据表示未战胜该npc
    SpCards = 3,                 -- 已收集珍稀牌，{ cardId = true }，为nil表示未收集过珍稀牌
    ForbidPvpExpiredTime = 4,    -- 禁止pvp时间，nil或0或时间戳
}

-- 同步四时戏玩法主界面数据
function LuaFourSeasonCardMgr:QueryPlayWndDataResult(playData, rankData, selfData, npcData)
    self.RankData =  g_MessagePack.unpack(rankData)
    self.SelfData =  g_MessagePack.unpack(selfData)
    self.NpcData =  g_MessagePack.unpack(npcData)
    self:CalculateRank()
    g_ScriptEvent:BroadcastInLua("FSC_QueryPlayWndDataResult", g_MessagePack.unpack(playData), self.RankData, self.SelfData, self.NpcData)
end

-- 请求排行榜数据
-- rankData：玩家排行榜，每个排名格式参考selfData
-- selfData：本人排行榜数据，字段包括：playerId, name, updateTime, overdueTime, score
-- npcData：npc分数，格式：for npcId, { score = 分数, updateTime = 更新时间 } in pairs(npcData) do ... end
LuaFourSeasonCardMgr.RankData = {}
LuaFourSeasonCardMgr.SelfData = {}
LuaFourSeasonCardMgr.NpcData = {}
function LuaFourSeasonCardMgr:QueryRankDataResult(rankData, selfData, npcData)
    self.RankData = g_MessagePack.unpack(rankData)
    self.SelfData = g_MessagePack.unpack(selfData)
    self.NpcData = g_MessagePack.unpack(npcData)
    self:CalculateRank()
    CUIManager.ShowUI(CLuaUIResources.ZhouNianQingFSCRankWnd)
end

LuaFourSeasonCardMgr.RankList = {}
function LuaFourSeasonCardMgr:CalculateRank()
    self.RankList = {}

    local npcLs = {}
    for id, v in pairs(self.NpcData) do 
        table.insert(npcLs, {
            id = id,
            score = v.score,
            updateTime = v.updateTime
        })
    end
    local cmp = function(a, b)
        if a.score ~= b.score then
            return a.score > b.score
        else
            return a.updateTime < b.updateTime 
        end
    end
    table.sort(npcLs, cmp)
    
    local idx = 1
    for k, v in ipairs(self.RankData) do
        if v.playerId == self.SelfData.playerId then
            self.SelfData.rank = k
        end
        v.rank = k
        while idx <= #npcLs and cmp(npcLs[idx], v) do
            table.insert(self.RankList, {
                ID = npcLs[idx].id,
                Name = NPC_NPC.GetData(npcLs[idx].id).Name,
                Rank = LocalString.GetString("—"), 
                Value = npcLs[idx].score,
                IsNPC = true
            })
            idx = idx + 1
        end
        table.insert(self.RankList, {
            ID = v.playerId,
            Name = v.name,
            Rank = k, 
            Value = v.score
        })
    end
    while idx <= #npcLs do
        table.insert(self.RankList, {
            ID = npcLs[idx].id,
            Name = NPC_NPC.GetData(npcLs[idx].id).Name,
            Rank = LocalString.GetString("—"), 
            Value = npcLs[idx].score,
            IsNPC = true
        })
        idx = idx + 1
    end
end

-- 同步节日界面要显示的珍稀牌数量
function LuaFourSeasonCardMgr:QueryFestivalWndDataResult(specialCardNum)
    g_ScriptEvent:BroadcastInLua("FSC_QueryFestivalWndDataResult", specialCardNum)
end

function LuaFourSeasonCardMgr:ShowGuideDialog(callback)
    local dialog = Dialog_TaskDialog.GetData(3952)
    if dialog then
        CConversationMgr.Inst:OpenStoryDialog(dialog.Dialog, DelegateFactory.Action(function ()
            if callback then
                callback()
            end
        end))
    end
end

--#region 邀请

-- 同步附近可邀请打牌玩家
function LuaFourSeasonCardMgr:SyncNearByPlayers(inviteList_U, lastInviteExpiredTime)
    LuaFSCInviteWnd.s_List = g_MessagePack.unpack(inviteList_U)
    LuaFSCInviteWnd.s_LastInviteExpiredTime = lastInviteExpiredTime
    CUIManager.ShowUI(CLuaUIResources.FSCInviteWnd)
end

-- 邀请玩家成功（发给邀请者）
function LuaFourSeasonCardMgr:InviteSuccess(currentInviteExpiredTime)
    g_MessageMgr:ShowMessage("FSC_SEND_INVITATION_HINT")
    LuaFSCInviteWnd.s_LastInviteExpiredTime = currentInviteExpiredTime
    g_ScriptEvent:BroadcastInLua("FSC_InviteSuccess")
end

-- 收到打牌邀请
function LuaFourSeasonCardMgr:ReceiveInvitation(inviterId, inviterName, currentInviteExpiredTime)
    LuaFSCInviteDlg.s_Info = {
        inviterId = inviterId, 
        inviterName = inviterName, 
        currentInviteExpiredTime = currentInviteExpiredTime,
    }
    CUIManager.ShowUI(CLuaUIResources.FSCInviteDlg)
end

-- 邀请其他玩家打牌被拒绝
function LuaFourSeasonCardMgr:OnInvitationRejected(playerId, playerName, rejectExpiredTime)
    g_MessageMgr:ShowMessage("FSC_REJECTED_INVITATION_HINT")
    g_ScriptEvent:BroadcastInLua("FSC_OnInvitationRejected", playerId, playerName, rejectExpiredTime)
end

--#endregion 邀请



--#region 对局

-- 玩家状态
EnumFSC_PlayerStatus = {
    ePrepare = 0,    -- 未准备 
    eNormal = 1,     -- 已准备，正常对局
    eHangUp = 2,     -- 挂机
    eLeave = 3,      -- 离开
}

-- 珍稀牌来源
EnumFSC_SpCardSrcType = {
    Self = 1,        -- 已拥有
    Borrowed = 2,    -- npc借的（引导局）
    Replicate = 3,    -- 复制技能复制的
}

-- 珍稀牌状态
-- 珍稀牌是仅自己可见的一种特殊增益
EnumFSC_SpCardStatus = {
    NotUsed = 1,        -- 未发动
    Used = 2,           -- 已发动
    Interrupted = 3,    -- 被禁
}

-- 卡牌特殊状态
EnumFSC_CardStatus = {
    Empty = 1,
    Back = 2,
    Detail = 3,
    NotInEffect = 4,
    Banned = 5,
    Copy = 6,
    NotInHand = 7, -- for headshot
}

-- 普通牌局会先发SyncPlayerInfo, 再发SyncPrepareInfo和SyncCardInfo
-- 引导局会在发SyncPlayerInfo之前先发SyncGuidingStage

-- 同步引导局引导阶段
LuaFourSeasonCardMgr.GuidingStage = nil
LuaFourSeasonCardMgr.WaitForNxtGuide = false
function LuaFourSeasonCardMgr:SyncGuidingStage(guidingStage)
    self.GuidingStage = guidingStage
    self.WaitForNxtGuide = false
    UnRegisterTick(self.GuideTick)
    if guidingStage > 1 and guidingStage ~= 3 then
        self:TryTriggerGuide()
    end
end 

LuaFourSeasonCardMgr.GuideTick = nil
LuaFourSeasonCardMgr.EnableOpInGuide = nil
function LuaFourSeasonCardMgr:TryTriggerGuide(args)
    UnRegisterTick(self.GuideTick)
    if self.GuidingStage == 1 then
        self.EnableOpInGuide = false
        CGuideMgr.Inst:StartGuide(EnumGuideKey.FSC_Guide1, 0)
        self.GuideTick = RegisterTick(function()
            if not CGuideMgr.Inst:IsInPhase(EnumGuideKey.FSC_Guide1) then
                UnRegisterTick(self.GuideTick)
                self:ShowGuideDialog(function()
                    g_ScriptEvent:BroadcastInLua("FSC_LevelUpHandCard", 0)
                    self.GuideTick = RegisterTickOnce(function()
                        self.EnableOpInGuide = true
                        CGuideMgr.Inst:StartGuide(EnumGuideKey.FSC_Guide2, 0)
                        self.WaitForNxtGuide = true
                    end, 1800)
                end)
            end
        end, 33)
    elseif self.GuidingStage == 2 then
        self.EnableOpInGuide = false
        self.GuideTick = RegisterTick(function()
            if self.EnableOperation and self.bIsOnMove then
                UnRegisterTick(self.GuideTick)
                self.GuideTick = nil
                self.EnableOpInGuide = true
                CGuideMgr.Inst:StartGuide(EnumGuideKey.FSC_Guide3, 0)
                self.WaitForNxtGuide = true
            end
        end, 33)
    elseif self.GuidingStage == 3 then
        self.EnableOpInGuide = false
        local anim = args and args[1]
        Gac2Gas.Anniv2023FSC_QueryCollectedCards(LuaFourSeasonCardMgr.SelfInfo.gameId)
        if anim then
            self.GuideTick = RegisterTick(function()
                if CommonDefs.IsNull(anim) then
                    UnRegisterTick(self.GuideTick)
                    self.GuideTick = nil
                end
                local animState = anim["fsccombination_in"]
                if animState and anim:IsPlaying("fsccombination_in") and animState.time / animState.length > 0.5 then
                    UnRegisterTick(self.GuideTick)
                    anim.enabled = false
                    CGuideMgr.Inst:StartGuide(EnumGuideKey.FSC_Guide4, 0)
                    self.GuideTick = RegisterTick(function()
                        if not CGuideMgr.Inst:IsInSubPhase(0) then
                            anim.enabled = true
                            self.EnableOpInGuide = true
                        end
                        if not CGuideMgr.Inst:IsInPhase(EnumGuideKey.FSC_Guide4) then
                            UnRegisterTick(self.GuideTick)
                            self.GuideTick = nil
                            if self.GuidingStage == 3 then
                                self.WaitForNxtGuide = true
                            end
                        end
                    end, 33)
                end
            end, 33)
        end
    elseif self.GuidingStage == 4 then
        self.EnableOpInGuide = false
        self.GuideTick = RegisterTick(function()
            if self.bIsOnMove then
                UnRegisterTick(self.GuideTick)
                self.GuideTick = nil
                self.EnableOpInGuide = true
                CGuideMgr.Inst:StartGuide(EnumGuideKey.FSC_Guide5, 0)
                self.WaitForNxtGuide = true
            end
        end, 33)
    elseif self.GuidingStage == 5 then
        self.EnableOpInGuide = nil
        self.WaitForNxtGuide = true
        RegisterTickOnce(function()
            self.GuidingStage = nil
        end, 5000)
    end
end

-- 同步对局玩家信息
-- selfInfo：己方信息, { 是否玩家(true), 局内id, 名字, 职业, 性别, 分数, 玩家状态 }
-- opponentInfo：对方信息, 如果是玩家格式同上, npc格式：{ 是否玩家(false), 局内id, npcId, 分数, 玩家状态 }
LuaFourSeasonCardMgr.SelfInfo = {}
LuaFourSeasonCardMgr.OpponentInfo = {}
function LuaFourSeasonCardMgr:SyncPlayerInfo(selfInfo, opponentInfo)
    local tbl = {g_MessagePack.unpack(selfInfo), g_MessagePack.unpack(opponentInfo)}
    local res = {}
    for i = 1, 2 do
        local info = tbl[i]
        if info[1] then
            res[i] = {
                isPlayer = true,
                gameId = info[2], -- gameId=1表示先手
                id = info[3],
                name = info[4],
                cls = info[5],
                sex = info[6],
                score = info[7],
                status = info[8],
            }
        else
            res[i] = {
                isPlayer = false,
                gameId = info[2],
                npcId = info[3],
                score = info[4],
                status = info[5],
            }
        end
    end
    self.SelfInfo = res[1]
    self.OpponentInfo = res[2]

    g_ScriptEvent:BroadcastInLua("FSC_SyncPlayerInfo")
end

-- 同步准备信息，此时应该打开准备界面
-- specialCards_U：珍稀卡，格式：for cardId, _ in pairs(specialCards) do ... end
-- selfStatus：己方玩家状态
-- opponentStatus：对方玩家状态
-- countDownEndTime：倒计时结束时间，时间戳
function LuaFourSeasonCardMgr:SyncPrepareInfo(specialCards_U, selfStatus, opponentStatus, countDownEndTime)
    LuaFSCPrepareRareCardWnd.s_SpecialCards = g_MessagePack.unpack(specialCards_U)
    LuaFSCPrepareRareCardWnd.s_CountDownEndTime = countDownEndTime
    self.SelfInfo.status = selfStatus
    self.OpponentInfo.status = opponentStatus
    CUIManager.ShowUI(CLuaUIResources.FSCPrepareRareCardWnd)
end

-- 同步牌局信息，此时应该打开牌局界面
-- boardCards：公共牌，list
-- handCards：己方手牌，list
-- selfSpecialCards：己方珍稀牌，格式：for cardId, { 珍稀牌来源, 珍稀牌状态 } in pairs(selfSpecialCards) do ... end
-- selfLastCard：己方牌堆第一张牌
-- opponentCardNum：对方手牌数
-- opponentShowedCards：对方明牌手牌，格式：for cardId, _ in pairs(opponentShowedCards) do ... end
-- opponentLastCard：对方牌堆第一张牌
LuaFourSeasonCardMgr.BoardCards = {}
LuaFourSeasonCardMgr.HandCards = {}
LuaFourSeasonCardMgr.SelfLastCard = 0
LuaFourSeasonCardMgr.OpponentCardNum = 0
LuaFourSeasonCardMgr.OpponentShowedCards = {}
LuaFourSeasonCardMgr.OpponentLastCard = 0
LuaFourSeasonCardMgr.SelfSpecialCards = {}
LuaFourSeasonCardMgr.CollectedCards = {}

LuaFourSeasonCardMgr.SelfCardPileNum = 0
LuaFourSeasonCardMgr.OppoCardPileNum = 0
--LuaFourSeasonCardMgr.GameStart = false -- 发完牌, 对局正式开始
function LuaFourSeasonCardMgr:SyncCardInfo(boardCards, handCards, selfSpecialCards, selfLastCard, opponentCardNum, opponentShowedCards, opponentLastCard, selfCollectedCards, opponentCollectedCards)
    self.BoardCards = g_MessagePack.unpack(boardCards)
    self.HandCards = g_MessagePack.unpack(handCards)
    self.SelfLastCard = selfLastCard
    self.OpponentCardNum = opponentCardNum
    self.OpponentShowedCards = g_MessagePack.unpack(opponentShowedCards)
    self.OpponentLastCard = opponentLastCard
    self.SelfSpecialCards = g_MessagePack.unpack(selfSpecialCards)
    
    self.SelfCardPileNum = 0
    self.OppoCardPileNum = 0
    self.CollectedCards[self.SelfInfo.gameId] = g_MessagePack.unpack(selfCollectedCards)
    self.CollectedCards[self.OpponentInfo.gameId] = g_MessagePack.unpack(opponentCollectedCards)
    for _, __ in pairs(self.CollectedCards[self.SelfInfo.gameId]) do
        self.SelfCardPileNum = self.SelfCardPileNum + 1
    end
    for _, __ in pairs(self.CollectedCards[self.OpponentInfo.gameId]) do
        self.OppoCardPileNum = self.OppoCardPileNum + 1
    end

    self.EnableOperation = false

    CGuideMgr.Inst:EndCurrentPhase()
    CConversationMgr.Inst:CloseDialog()

    SoundManager.Inst:ForceSetBgMusic(self.Music.bgm)

    CUIManager.CloseUI(CLuaUIResources.FSCPrepareRareCardWnd)
    CUIManager.CloseUI(CLuaUIResources.FSCInviteWnd)
    CUIManager.ShowUI(CLuaUIResources.FSCInGameWnd)
end

-- 同步牌局对战结果
function LuaFourSeasonCardMgr:SyncPlayResult(score, result, bNewRecord, extraData_U)
    LuaFSCResultWnd.s_Score = score
    LuaFSCResultWnd.s_Result = result
    LuaFSCResultWnd.s_bNewRecord = bNewRecord
    LuaFSCResultWnd.s_ExtraData = g_MessagePack.unpack(extraData_U)

    local shot = ""
    if result == 0 then
        shot = self.Music.win
    elseif result == 1 then
        shot = self.Music.lose
    else
        shot = self.Music.tie
    end
    SoundManager.Inst:PlayOneShot(shot, Vector3.zero, nil, 0)

    CUIManager.ShowUI(CLuaUIResources.FSCResultWnd)
end

function LuaFourSeasonCardMgr:OnLeaveGame()
    self.bIsOnMove = nil
    self.IsDealCards = false
    --self.GameStart = false
    self.CachedAddNewCombo = nil
    self.MaxCardDepth = 0
    self.SelfCardPileNum = 0
    self.OppoCardPileNum = 0

    self.EnableOpInGuide = nil
    self.GuidingStage = nil
    UnRegisterTick(self.GuideTick)
    self.GuideTick = nil
    CGuideMgr.Inst:EndCurrentPhase()
    CConversationMgr.Inst:CloseDialog()

    SoundManager.Inst:ForceSetBgMusic(nil)
end

function LuaFourSeasonCardMgr:CloseGameWnd()
    self:OnLeaveGame()
    CUIManager.CloseUI(CLuaUIResources.FSCCardDetailWnd)
    CUIManager.CloseUI(CLuaUIResources.FSCCardPileWnd)
    CUIManager.CloseUI(CLuaUIResources.FSCInGameWnd)
    CUIManager.CloseUI(CLuaUIResources.FSCInviteWnd)
    CUIManager.CloseUI(CLuaUIResources.FSCPrepareRareCardWnd)
end

-- 己方完成出牌
-- usedCardId：出了哪张手牌
-- boardCardId：匹配了哪张公共牌
-- handCards_U：当前己方手牌，list
-- boardCards_U：当前公共牌，list
-- lastCollectedCard：己方牌堆第一张牌
function LuaFourSeasonCardMgr:OnSelfUseCard(usedCardId, boardCardId, handCards_U, boardCards_U, lastCollectedCard)
    g_ScriptEvent:BroadcastInLua("FSC_OnSelfUseCard", usedCardId, boardCardId)
    self.HandCards = g_MessagePack.unpack(handCards_U)
    self.BoardCards = g_MessagePack.unpack(boardCards_U)
    self.SelfLastCard = lastCollectedCard
    self.SelfCardPileNum = self.SelfCardPileNum + 2
end

-- 对方完成出牌
-- usedCardId：出了哪张手牌
-- boardCardId：匹配了哪张公共牌
-- opponentCardNum：对方当前手牌数
-- boardCards_U：当前公共牌，list
-- lastCollectedCard：对方牌堆第一张牌
function LuaFourSeasonCardMgr:OnOpponentUseCard(usedCardId, boardCardId, opponentCardNum, boardCards_U, lastCollectedCard)
    g_ScriptEvent:BroadcastInLua("FSC_OnOpponentUseCard", usedCardId, boardCardId)
    self.OpponentCardNum = opponentCardNum
    self.BoardCards = g_MessagePack.unpack(boardCards_U)
    self.OpponentLastCard = lastCollectedCard
    self.OppoCardPileNum = self.OppoCardPileNum + 2
    self.OpponentShowedCards[usedCardId] = nil
end

-- 己方完成换牌
-- giveUpCardId：弃了哪张手牌
-- newHandCardId：换到哪张手牌
-- handCards_U：当前手牌，list
-- boardCards_U：当前公共牌，list
function LuaFourSeasonCardMgr:OnSelfReplaceCard(giveUpCardId, newHandCardId, handCards_U, boardCards_U)
    local data =  g_MessagePack.unpack(boardCards_U)
    g_ScriptEvent:BroadcastInLua("FSC_OnSelfReplaceCard", giveUpCardId, newHandCardId, data[1], data[2], data[3])
    self.HandCards = g_MessagePack.unpack(handCards_U)
    self.BoardCards = data[3]
end

-- 对方完成换牌
-- giveUpCardId：弃了哪张手牌
-- opponentCardNum：对方当前手牌数
-- boardCards_U：当前公共牌，list
function LuaFourSeasonCardMgr:OnOpponentReplaceCard(giveUpCardId, opponentCardNum, boardCards_U)
    local data =  g_MessagePack.unpack(boardCards_U)
    g_ScriptEvent:BroadcastInLua("FSC_OnOpponentReplaceCard", giveUpCardId, opponentCardNum, data[1], data[2], data[3])
    self.BoardCards = data[3]
    self.OpponentShowedCards[giveUpCardId] = nil
end

-- 补充了一张新的公共牌
-- cardId：新公共牌
-- boardCards_U：当前公共牌，list
function LuaFourSeasonCardMgr:OnAddBoardCard(cardId, boardCards_U)
    self.BoardCards = g_MessagePack.unpack(boardCards_U)
    g_ScriptEvent:BroadcastInLua("FSC_OnAddBoardCard", cardId)
end

-- [obsolete][已经不发了, 洗牌和换牌绑定]
-- 公共牌重新洗牌
-- boardCards_U：当前公共牌，list
function LuaFourSeasonCardMgr:OnReShuffleBoardCard(boardCards_U)
    -- 洗牌一定是在换牌之后的同一帧进行的
    --self.BoardCards = g_MessagePack.unpack(boardCards_U)
    --g_ScriptEvent:BroadcastInLua("FSC_OnReShuffleBoardCard")
end

-- 发牌
-- opponentCardNum：对方手牌数
-- handCards_U：自己手牌，list
-- boardCards_U：公共牌，list
LuaFourSeasonCardMgr.IsDealCards = false
function LuaFourSeasonCardMgr:OnDealCards(opponentCardNum, handCards_U, boardCards_U)
    self.OpponentCardNum = opponentCardNum
    self.HandCards = g_MessagePack.unpack(handCards_U)
    self.BoardCards = g_MessagePack.unpack(boardCards_U)
    self.IsDealCards = true
    g_ScriptEvent:BroadcastInLua("FSC_OnDealCards")
end

-- 回合开始
-- bIsOnMove：是否己方回合
-- countDownEndTime：倒计时结束时间，0表示无倒计时
LuaFourSeasonCardMgr.bIsOnMove = nil
LuaFourSeasonCardMgr.CountDownEndTime = 0
function LuaFourSeasonCardMgr:StartRound(bIsOnMove, countDownEndTime)
    self.bIsOnMove = bIsOnMove
    self.CountDownEndTime = countDownEndTime
    g_ScriptEvent:BroadcastInLua("FSC_StartRound")
end

-- 同步分数
-- selfScore：己方分数
-- opponentScore：对方分数
function LuaFourSeasonCardMgr:UpdateScore(selfScore, opponentScore)
    self.SelfInfo.score = selfScore
    self.OpponentInfo.score = opponentScore
    g_ScriptEvent:BroadcastInLua("FSC_UpdateScore")
end

-- 己方形成新组合
-- cardId：新收集的牌
-- comboId：组合id
-- score：加完组合分后，当前己方分数
-- countDownEndTime：弹窗关闭时间戳
LuaFourSeasonCardMgr.CachedAddNewCombo = nil -- 适配引导局dx而界面未打开的情况
function LuaFourSeasonCardMgr:StartAddNewCombo(cardsUD, comboId, score, countDownEndTime)
    local cards = g_MessagePack.unpack(cardsUD)
    self.CachedAddNewCombo = {
        cards, comboId, score, countDownEndTime
    }
    g_ScriptEvent:BroadcastInLua("FSC_StartAddNewCombo", cards, comboId, score, countDownEndTime)
end

-- 己方发动珍稀牌技能(非禁用、复制类的普通技能, 只需要展示生效)
-- cardId：珍稀牌id
-- effectId：技能id
-- countDownEndTime：弹窗关闭时间戳
function LuaFourSeasonCardMgr:StartSpCardEffect(cardId, effectId, countDownEndTime)
    g_ScriptEvent:BroadcastInLua("FSC_StartSpCardEffect", cardId, effectId, countDownEndTime)
end

-- 发动珍稀牌技能结束(进行技能表现)
-- index：发动者局内id
-- cardId：珍稀牌id
-- effectId：技能id
-- extreData_U：附加数据，一般是nil；看牌技能是看到的手牌，list，（如果是己方发动应该将对方手牌置成明牌）；复制和禁技是被复制和被禁的牌，list
function LuaFourSeasonCardMgr:SpCardEffectEnd(index, cardId, effectId, extreData_U)
    local skillData = ZhouNianQing2023_PreciousSkill.GetData(effectId)
    if skillData.Type == 2 then -- 翻牌
        g_ScriptEvent:BroadcastInLua("FSC_TurnOverCards", index, cardId, effectId, g_MessagePack.unpack(extreData_U))
    elseif skillData.Type == 4 then -- 禁用
        g_ScriptEvent:BroadcastInLua("FSC_BanCards", index, cardId, effectId, g_MessagePack.unpack(extreData_U))
    elseif skillData.Type == 5 then -- 复制
        g_ScriptEvent:BroadcastInLua("FSC_CopyCards", index, cardId, effectId, g_MessagePack.unpack(extreData_U))
    else
        g_ScriptEvent:BroadcastInLua("FSC_SpCardEffectEnd", index, cardId, effectId)
    end
end

-- 对方开始发动珍稀牌技能，只有复制和禁技会发，表示等对方选牌
-- cardId：牌id
-- effectId：技能id
-- countDownEndTime：倒计时结束时间戳
function LuaFourSeasonCardMgr:OpponentStartSkill(cardId, effectId, countDownEndTime)
    g_ScriptEvent:BroadcastInLua("FSC_OpponentStartSkill", cardId, effectId, countDownEndTime)
end

-- 己方发动复制技能(展示生效后需要选择目标)
-- cardId：发动牌id
-- effectId：技能id
-- card2Effects_U：可选牌，格式：for cardId, { 可复制技能id, ... } in pairs(card2Effects) do ... end
-- countDownEndTime：倒计时结束时间
function LuaFourSeasonCardMgr:StartReplicate(cardId, effectId, card2Effects_U, countDownEndTime)
    g_ScriptEvent:BroadcastInLua("FSC_StartReplicate", cardId, effectId, g_MessagePack.unpack(card2Effects_U), countDownEndTime)
end

-- 己方发动禁技技能(展示生效后需要选择目标)
-- cardId：发动牌id
-- effectId：技能id
-- card2Effects_U：可选牌，格式：for cardId, { 可禁技能id, ... } in pairs(card2Effects) do ... end
-- countDownEndTime：倒计时结束时间
function LuaFourSeasonCardMgr:StartInterrupt(cardId, effectId, card2Effects_U, countDownEndTime)
    g_ScriptEvent:BroadcastInLua("FSC_StartInterrupt", cardId, effectId, g_MessagePack.unpack(card2Effects_U), countDownEndTime)
end

-- 同步己方/对方已收集卡牌
-- queryIndex：查询哪个玩家，局内id
-- collectedCards：已收集卡牌，{ cardId = 收集顺序（数字小表示先收集） }
-- collectedCombos：已收集组合，{ 组合id = true }
LuaFourSeasonCardMgr.CollectedCombos = {}
function LuaFourSeasonCardMgr:SendCollectedCardsData(queryIndex, collectedCards, collectedCombos)
    self.CollectedCards[queryIndex] = g_MessagePack.unpack(collectedCards)
    self.CollectedCombos[queryIndex] = g_MessagePack.unpack(collectedCombos)
    g_ScriptEvent:BroadcastInLua("FSC_SendCollectedCardsData", queryIndex,  self.CollectedCards[queryIndex], self.CollectedCombos[queryIndex])
end

function LuaFourSeasonCardMgr:SendSpecialCardsData(specialCards)
    self.SelfSpecialCards = g_MessagePack.unpack(specialCards)
    LuaFSCPrepareRareCardWnd.s_SpecialCards = self.SelfSpecialCards
    LuaFSCPrepareRareCardWnd.s_CountDownEndTime = 0
    CUIManager.ShowUI(CLuaUIResources.FSCPrepareRareCardWnd)
end

-- 同步玩家状态，玩家准备好、中途挂机、离开会发
-- index：局内id
-- status：玩家状态，同EnumFSC_PlayerStatus
function LuaFourSeasonCardMgr:SyncPlayerStatus(index, status)
    if LuaFourSeasonCardMgr.SelfInfo.gameId == index then
        LuaFourSeasonCardMgr.SelfInfo.status = status
    elseif LuaFourSeasonCardMgr.OpponentInfo.gameId == index then
        LuaFourSeasonCardMgr.OpponentInfo.status = status
    end
    g_ScriptEvent:BroadcastInLua("FSC_SyncPlayerInfo")
end

--#endregion 对局

LuaFourSeasonCardMgr.MaxCardDepth = 0

LuaFourSeasonCardMgr.CardCombineCards = {}
LuaFourSeasonCardMgr.Card2Combinations = {}
function LuaFourSeasonCardMgr:GetCardCombination(id)
    if not self.CardCombineCards[id] or not self.Card2Combinations[id] then
        for i = 1, ZhouNianQing2023_Cards.GetDataCount() do
            self.CardCombineCards[i] = {}
            self.Card2Combinations[i] = {}
        end
        ZhouNianQing2023_CardCombination.Foreach(function(k, v)
            self.Card2Combinations[v.CardId[0]][k] = true
            for i = 1, v.CardId.Length - 1 do
                self.Card2Combinations[v.CardId[i]][k] = true
                for j = 0, i - 1 do
                    self.CardCombineCards[v.CardId[i]][v.CardId[j]] = true
                    self.CardCombineCards[v.CardId[j]][v.CardId[i]] = true
                end
            end
        end)
    end
    return self.CardCombineCards[id], self.Card2Combinations[id]
end

LuaFourSeasonCardMgr.CardData = {}
function LuaFourSeasonCardMgr:GetCardData(id)
    if not self.CardData[id] then
        self.CardData[id] = ZhouNianQing2023_Cards.GetData(id)
    end
    return self.CardData[id]
end

LuaFourSeasonCardMgr.EnableOperation = false
function LuaFourSeasonCardMgr:SetEnableOperation(enabled)
    if enabled ~= self.EnableOperation then
        if not enabled then
            g_ScriptEvent:BroadcastInLua("FSC_DisableOperation")
        end
        self.EnableOperation = enabled
    end
end

function LuaFourSeasonCardMgr:CheckPlayCard(cardId) -- 判断是否能出牌(即是否有手牌和指定公共牌匹配)
    local hand = self.HandCards
    if cardId and cardId > 0 then
        local data = self:GetCardData(cardId)
        if hand and data then
            for _, id in ipairs(hand) do
                if data.Season == self:GetCardData(id).Season then
                    return true
                end
            end
        end
    else
        local board = self.BoardCards
        if board and hand then
            local cache = {}
            for _, id in ipairs(board) do
                cache[self:GetCardData(id).Season] = true
            end
            for _, id in ipairs(hand) do
                if cache[self:GetCardData(id).Season] then
                    return true
                end
            end
        end
    end
    return false
end

function LuaFourSeasonCardMgr:GetPureName(name)
    if not name then return "" end
    if StringAt(name, 1) == LocalString.GetString("【") then
        name = CommonDefs.StringSubstring1(name, 1)
    end
    local len = CommonDefs.StringLength(name)
    if StringAt(name, len) == LocalString.GetString("】") then
        name = CommonDefs.StringSubstring2(name, 0, len - 1)
    end
    if CommonDefs.StringLength(name) == 2 then
        name = StringAt(name, 1).." "..StringAt(name, 2)
    end
    return name
end

function LuaFourSeasonCardMgr:GetSubject(name)
    if not name then return "" end
    local idx = nil
    if CommonDefs.IS_VN_CLIENT then
        idx = CommonDefs.StringIndexOf_String(name, LocalString.GetString("-"))
    else
        idx = CommonDefs.StringIndexOf_String(name, LocalString.GetString("·"))
    end    if idx then
        return CommonDefs.StringSubstring1(name, idx + 1)
    end
end

function LuaFourSeasonCardMgr:GetSeasonName(season)
    if season == 1 then
        return "Spring"
    elseif season == 2 then
        return "Summer"
    elseif season == 3 then
        return "Autumn"
    elseif season == 4 then
        return "Winter"
    end
end

function LuaFourSeasonCardMgr:GetSeasonColor(season)
    if season == 1 then
        return NGUIText.ParseColor24("c8da77", 0)
    elseif season == 2 then
        return NGUIText.ParseColor24("ff7f6f", 0)
    elseif season == 3 then
        return NGUIText.ParseColor24("ffa756", 0)
    elseif season == 4 then
        return NGUIText.ParseColor24("70b1e8", 0)
    end
end

function LuaFourSeasonCardMgr:GetEnemyName()
    local info = self.OpponentInfo
    if info.isPlayer then
        return info.name
    elseif info.npcId then
        local npc = NPC_NPC.GetData(info.npcId)
        if npc then
            return npc.Name
        end
    end
end
