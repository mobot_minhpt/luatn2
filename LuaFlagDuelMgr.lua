local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local PlayerSettings = import "L10.Game.PlayerSettings"
local CChatLinkMgr = import "CChatLinkMgr"

LuaFlagDuelMgr={}
LuaFlagDuelMgr.IsInFlagDuel = nil
LuaFlagDuelMgr.UpdateAction = nil
LuaFlagDuelMgr.HideSkillEffectSetting = nil
LuaFlagDuelMgr.HideShenbingEffectSetting = nil
LuaFlagDuelMgr.VisiblePlayerLimitSetting = nil

function LuaFlagDuelMgr:TryTriggerFlagDuel(playerDict)
    if self.UpdateAction == nil then
        self.UpdateAction = DelegateFactory.Action(function()
            self:OnMainPlayerPlayPropUpdate()
        end)
    end

    if not self.IsInFlagDuel then
        if CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.PlayId == 51102130 then
            self.IsInFlagDuel = true
            self:SetPlayerFx()
            EventManager.AddListenerInternal(EnumEventType.MainPlayerPlayPropUpdate, self.UpdateAction)
        end
    end
end

function LuaFlagDuelMgr:SetPlayerFx()
    self.HideSkillEffectSetting = PlayerSettings.HideSkillEffect
    self.HideShenbingEffectSetting = PlayerSettings.HideShenbingEffect
    if self.HideSkillEffectSetting then
        PlayerSettings.HideSkillEffect = false
    end

    if self.HideShenbingEffectSetting then
        PlayerSettings.HideShenbingEffect = false
    end

    self.VisiblePlayerLimitSetting = PlayerSettings.VisiblePlayerLimit
    if self.VisiblePlayerLimitSetting == JueDou_Setting.GetData().ChaQiVisiblePlayerLimit then
        self.VisiblePlayerLimitSetting = -1
    else
        PlayerSettings.VisiblePlayerLimit = JueDou_Setting.GetData().ChaQiVisiblePlayerLimit
    end
end

function LuaFlagDuelMgr:ResetPlayerFx()
    if self.HideSkillEffectSetting then
        PlayerSettings.HideSkillEffect = true
    end

    if self.HideShenbingEffectSetting then
        PlayerSettings.HideShenbingEffect = true
    end

    if self.VisiblePlayerLimitSetting >= 0 then
        PlayerSettings.VisiblePlayerLimit = self.VisiblePlayerLimitSetting
    end
end

function LuaFlagDuelMgr:OnMainPlayerPlayPropUpdate()
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.PlayId ~= 51102130 and self.IsInFlagDuel then
        self.IsInFlagDuel = false
        self:ResetPlayerFx()
        EventManager.RemoveListenerInternal(EnumEventType.MainPlayerPlayPropUpdate, self.UpdateAction)
    end
end


LuaFlagDuelMgr.QieCuoMessageInfo = nil

function LuaFlagDuelMgr:ShowQieCuoMessage(pid,msg,job,gender,lv,power,teams,timeout,isnml)
    LuaFlagDuelMgr.QieCuoMessageInfo = {}
    LuaFlagDuelMgr.QieCuoMessageInfo.PlayerID = pid
    LuaFlagDuelMgr.QieCuoMessageInfo.PlayerLv = lv
    LuaFlagDuelMgr.QieCuoMessageInfo.PlayerClass = job
    LuaFlagDuelMgr.QieCuoMessageInfo.PlayerGender = gender
    LuaFlagDuelMgr.QieCuoMessageInfo.Msg = msg
    LuaFlagDuelMgr.QieCuoMessageInfo.Power = power
    LuaFlagDuelMgr.QieCuoMessageInfo.Teams = teams
    LuaFlagDuelMgr.QieCuoMessageInfo.TimeOut = timeout
    LuaFlagDuelMgr.QieCuoMessageInfo.IsNml = isnml
    CUIManager.ShowUI(CLuaUIResources.QieCuoMessageBox)
end

function LuaFlagDuelMgr:SendQieCuoInviteToPlayer(msg, challengerId, bTeam, teamInfo_U)
	local cpid = challengerId
	local cjob = 0
	local cgender = 0

	local timeout = 15

	local isnml = false
	local teams = {}
	local tlv = 0
	local tep = 0
	local memcount = 0
    local zpcount = 0
	local list = MsgPackImpl.unpack(teamInfo_U)
    local index = 2
	for i=0,list.Count-1 do
		local memdatalist = list[i]

		local pid = memdatalist[0]
		local job = memdatalist[1]
		local lv = memdatalist[2]
		local xfst = memdatalist[3]
		local esore = memdatalist[4]

        local gender = 0
        if memdatalist.Count >= 6 then
            gender = memdatalist[5]
        end

		if esore > 0 then
			tep  = tep + esore
			zpcount = zpcount + 1
		end

        if lv > 0 then
            tlv  = tlv + lv
            memcount = memcount + 1
        end 

		if xfst == 0 then --有一个人不为仙身
			isnml = true
		end
        
		if pid == challengerId then --队长的数据
			cjob = job
			cgender = gender
            teams[1] = job
		else
			teams[index] = job
            index = index + 1
		end
	end

	if not bTeam then
		timeout = 10
		teams = nil
	end

    if zpcount == 0 then 
        zpcount = 1 
    end

    if memcount == 0 then
        memcount = 1
    end

    local lv = math.ceil(tlv / memcount)
	local eq = math.ceil(tep / zpcount)

    msg = CChatLinkMgr.TranslateToNGUIText(msg)
	self:ShowQieCuoMessage(cpid,msg,cjob,cgender,lv,eq,teams,timeout,isnml)
end
