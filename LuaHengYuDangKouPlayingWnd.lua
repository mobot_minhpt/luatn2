local UITexture = import "UITexture"
local CUITexture = import "L10.UI.CUITexture"

local CButton = import "L10.UI.CButton"

local UILabel = import "UILabel"

local UISprite = import "UISprite"

local UIProgressBar = import "UIProgressBar"

local UIScrollView = import "UIScrollView"

local GameObject = import "UnityEngine.GameObject"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"

local UIWidget = import "UIWidget"
local QnButton = import "L10.UI.QnButton"
local QnButtonState = import "L10.UI.QnButton+QnButtonState"
local Ease = import "DG.Tweening.Ease"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local NormalCamera = import "L10.Engine.CameraControl.NormalCamera"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CJoystickWnd = import "L10.UI.CJoystickWnd"

LuaHengYuDangKouPlayingWnd = class()
RegistClassMember(LuaHengYuDangKouPlayingWnd, "Type")                -- 副本类型
RegistClassMember(LuaHengYuDangKouPlayingWnd, "MonsterInfo")         -- 怪物信息
RegistClassMember(LuaHengYuDangKouPlayingWnd, "ProgressWidth")       -- 进度条长度
RegistClassMember(LuaHengYuDangKouPlayingWnd, "ObjectList")          -- Panel对象列表
RegistClassMember(LuaHengYuDangKouPlayingWnd, "ObjectCount")         -- Panel对象列表实际个数
RegistClassMember(LuaHengYuDangKouPlayingWnd, "MonsterList")         -- Monster对象列表
RegistClassMember(LuaHengYuDangKouPlayingWnd, "MonsterCount")        -- Monster对象实际个数
RegistClassMember(LuaHengYuDangKouPlayingWnd, "CountDownTime")       -- 倒计时当前秒数
RegistClassMember(LuaHengYuDangKouPlayingWnd, "CountDownTotalTime")  -- 倒计时总秒数
RegistClassMember(LuaHengYuDangKouPlayingWnd, "EndTimeStamp")        -- 倒计时结束时的时间戳
RegistClassMember(LuaHengYuDangKouPlayingWnd, "NumberLabel")         -- 击败小怪数标签
RegistClassMember(LuaHengYuDangKouPlayingWnd, "NowNumber")           -- 当前显示击败小怪数量
RegistClassMember(LuaHengYuDangKouPlayingWnd, "NumberTickTask")      -- 显示小怪数量定时任务
RegistClassMember(LuaHengYuDangKouPlayingWnd, "SceneInfo")           -- 场景信息
RegistClassMember(LuaHengYuDangKouPlayingWnd, "IsPanelInit")         -- 面板是否已经初始化
RegistClassMember(LuaHengYuDangKouPlayingWnd, "IsInXiaoGuai")        -- 是否处于小怪模式
RegistClassMember(LuaHengYuDangKouPlayingWnd, "IsBossCountDown")     -- boss模式倒计时是否打开
RegistClassMember(LuaHengYuDangKouPlayingWnd, "IsXiaoGuaiCountDown") -- 小怪模式倒计时是否打开

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHengYuDangKouPlayingWnd, "ScrollView", "Panel", UIScrollView)
RegistChildComponent(LuaHengYuDangKouPlayingWnd, "ProgressBar", "ProgressBar", UIProgressBar)
RegistChildComponent(LuaHengYuDangKouPlayingWnd, "Foreground", "Foreground", UISprite)
RegistChildComponent(LuaHengYuDangKouPlayingWnd, "Background", "Background", UISprite)
RegistChildComponent(LuaHengYuDangKouPlayingWnd, "ExpandButton", "ExpandButton", CButton)
RegistChildComponent(LuaHengYuDangKouPlayingWnd, "MonsterHighlight", "MonsterHighlight", GameObject)
RegistChildComponent(LuaHengYuDangKouPlayingWnd, "WndBg", "WndBg", UITexture)
RegistChildComponent(LuaHengYuDangKouPlayingWnd, "CountDown", "CountDown", UIProgressBar)
RegistChildComponent(LuaHengYuDangKouPlayingWnd, "CountDownLabel", "Label", UILabel)
RegistChildComponent(LuaHengYuDangKouPlayingWnd, "BossHP", "BossHP", UITexture)

--@endregion RegistChildComponent end

function LuaHengYuDangKouPlayingWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

    -- 取消渐进的视角缩放，直接缩放到指定比例
    NormalCamera.Inst:UnregisterPinchTick()
    local playid = HengYuDangKou_Setting.GetData().GamePlayId
    local pinch = Gameplay_Gameplay.GetData(playid).CameraPinch
    CameraFollow.Inst:SetPinchIn(pinch, false, false)
    CameraFollow.Inst:EnableZoom(false)
    CJoystickWnd.Instance:SetMountAndCaptureButtonVisible(true) -- 显示Joystick
end
-- 初始化怪物头像
function LuaHengYuDangKouPlayingWnd:SetMonsterImage(go, index)
    local image = go.transform:Find("Image/MonsterImage"):GetComponent(typeof(CUITexture))
    image:LoadMaterial(self.MonsterInfo[self.Type][index].MonsterSIcon)
end
-- 初始化2+3+1面板
function LuaHengYuDangKouPlayingWnd:InitPanel()
    if self.IsPanelInit then return end
    self.IsPanelInit = true -- 防止多次初始化
    self.ObjectList = {}
    self.MonsterList = {}
    local count = 0 -- 统计Monster对象数
    for i = 0, 7 do
        local go = self.ScrollView.transform:GetChild(i).gameObject
        if string.find(go.name, "Monster") then
            count = count + 1
            go.name = count -- 方便识别是第几个怪物
            -- 点击怪物显示对应信息面板
            UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(g)
                LuaHengYuDangKouMgr.m_ClickId = tonumber(g.name)
                CUIManager.ShowUI(CLuaUIResources.HengYuDangKouMonsterInfoWnd)
            end)
            table.insert(self.MonsterList, go)
        end
        table.insert(self.ObjectList, go)
    end
    -- 点击当前怪物显示对应信息面板
    UIEventListener.Get(self.MonsterHighlight).onClick = DelegateFactory.VoidDelegate(function(g)
        LuaHengYuDangKouMgr.m_ClickId = self.SceneInfo.progress
        CUIManager.ShowUI(CLuaUIResources.HengYuDangKouMonsterInfoWnd)
    end)
    local ProgressWidth = self.MonsterList[6].transform.localPosition.x - self.MonsterList[1].transform.localPosition.x
    self.ObjectCount = #self.ObjectList
    self.MonsterCount = 6
    -- 普通模式不显示小怪
    if self.Type == EnumHengYuDangKouStage.eNormal then
        self.ObjectList[7]:SetActive(false)
        self.ObjectList[8]:SetActive(false)
        ProgressWidth = self.MonsterList[5].transform.localPosition.x - self.MonsterList[1].transform.localPosition.x
        self.ObjectCount = self.ObjectCount - 2
        self.MonsterCount = self.MonsterCount - 1
    end
    self.ProgressWidth = ProgressWidth
    self.Foreground.width = ProgressWidth
    self.Background.width = ProgressWidth + 2
    self.WndBg.width = (self.Type == EnumHengYuDangKouStage.eHard and 764 or 628)
    self.ScrollView:ResetPosition()
    -- 修改怪物头像
    for i = 1, self.MonsterCount do
        self:SetMonsterImage(self.MonsterList[i], i)
    end
end
-- Monster样式
-- go 需要设置样式的怪物对象 | diff 怪物难度等级 | index 需要设置样式的怪物下标 | now 挑战中的怪物
function LuaHengYuDangKouPlayingWnd:SetMonsterStatus(go, diff, index, now)
    -- 是否是当前怪物
    go:SetActive(index ~= now)
    if index == now then
        self.MonsterHighlight.transform.position = go.transform.position
        self:SetMonsterImage(self.MonsterHighlight, index)
        self.MonsterHighlight:SetActive(true)
    end
    local nowgo = (index == now and self.MonsterHighlight or go) -- 实际显示的怪物对象
    -- 头像是否置灰
    local MonsterImage = nowgo.transform:Find("Image/MonsterImage")
    MonsterImage:GetComponent(typeof(QnButton)).Enabled = (index >= now)
    -- 是否打勾
    if index ~= now then
        local Jibai = nowgo.transform:Find("Jibai")
        Jibai.gameObject:SetActive(index < now)
    end
    -- 背景按钮显示方式
    if index < now then -- 此时是列表中的 go
        local Lock = nowgo.transform:Find("Bottom/Lock")
        local Done_Normal = nowgo.transform:Find("Bottom/Done_Normal")
        local Difficulty = nowgo.transform:Find("Bottom/Difficulty")
        if diff then
            Lock.gameObject:SetActive(false)
            Done_Normal.gameObject:SetActive(false)
            Difficulty.gameObject:SetActive(true)
            for i = 1, 3 do
                Difficulty:GetChild(i - 1).gameObject:SetActive(i == diff)
            end
        else
            Lock.gameObject:SetActive(false)
            Done_Normal.gameObject:SetActive(true)
            Difficulty.gameObject:SetActive(false)
        end
    elseif index == now then -- 此时是 MonsterHighlight
        local Difficulty = nowgo.transform:Find("Bottom/Difficulty")
        if diff then
            Difficulty.gameObject:SetActive(true)
            for i = 1, 3 do
                Difficulty:GetChild(i - 1).gameObject:SetActive(i == diff)
            end
        else
            Difficulty.gameObject:SetActive(false)
        end
    else -- 此时是列表中的 go
        local Lock = nowgo.transform:Find("Bottom/Lock")
        local Done_Normal = nowgo.transform:Find("Bottom/Done_Normal")
        local Difficulty = nowgo.transform:Find("Bottom/Difficulty")
        Lock.gameObject:SetActive(true)
        Done_Normal.gameObject:SetActive(false)
        if Difficulty then Difficulty.gameObject:SetActive(false) end
    end
end
-- 显示进度
function LuaHengYuDangKouPlayingWnd:ShowProgress(now)
    -- 进度条
    local nowwidth = 0
    if self.MonsterList then
        local index = now > self.MonsterCount and self.MonsterCount or now
        nowwidth = self.MonsterList[index].transform.localPosition.x - self.MonsterList[1].transform.localPosition.x
    end
    self.ProgressBar.value = nowwidth / self.ProgressWidth
    -- Panel内容
    self.MonsterHighlight:SetActive(false)
    for i = 1, self.MonsterCount do
        local diff = nil
        if self.Type == EnumHengYuDangKouStage.eHard then
            if i < now then
                diff = self.SceneInfo.difficulty[i]
            elseif i == now and now < self.MonsterCount then
                diff = self.SceneInfo.curDifficulty
            end
        end
        self:SetMonsterStatus(self.MonsterList[i], diff, i, now)
    end
    -- 滚动到Panel对应阶段位置
    local rate = 0
    if now <= 2 then
        rate = 0
    elseif now <= 5 and self.Type == EnumHengYuDangKouStage.eHard then
        rate = 0.64 -- 让第四个怪物处于中间位置
    else
        rate = 1
    end
    self.ScrollView:InvalidateBounds()
    self.ScrollView:SetDragAmount(rate, rate, false)
end
-- 启动小怪模式
function LuaHengYuDangKouPlayingWnd:StartXiaoGuai()
    if self.IsInXiaoGuai then return end
    if self.Type == EnumHengYuDangKouStage.eNormal then return end
    self.IsInXiaoGuai = true -- 防止多次进入小怪阶段
    -- 显示击败数
    self.MonsterHighlight.transform:Find("Bottom").gameObject:SetActive(false)
    self.MonsterHighlight.transform:Find("Image/MonsterImage").gameObject:SetActive(false)
    self.MonsterHighlight.transform:Find("Image/CountLabel").gameObject:SetActive(true)
    self:ShowProgress(self.MonsterCount)
    -- 显示小怪数量定时任务
    self.NumberLabel = self.MonsterHighlight.transform:Find("Image/CountLabel/Number"):GetComponent(typeof(UILabel))
    self.NowNumber = LuaHengYuDangKouMgr.m_KillCount
    self.NumberLabel.text = self.NowNumber
    self.NumberTickTask = RegisterTick(function()
        LuaTweenUtils.DOKill(self.NumberLabel.transform, false)
        if not LuaHengYuDangKouMgr.m_KillCount then
            self.NumberLabel.text = "0"
            return
        end
        self.NowNumber = self.NowNumber and self.NowNumber or LuaHengYuDangKouMgr.m_KillCount
        if self.NowNumber < LuaHengYuDangKouMgr.m_KillCount then
            self.NowNumber = self.NowNumber + 1
            LuaTweenUtils.TweenScaleXY(self.NumberLabel.transform, 1.3, 1.3, 0.04, Ease.InCirc)
        else
            self.NowNumber = LuaHengYuDangKouMgr.m_KillCount
            LuaTweenUtils.TweenScaleXY(self.NumberLabel.transform, 1, 1, 0.04, Ease.InCirc)
        end
        self.NumberLabel.text = self.NowNumber
    end, 50)
end
-- 当前倒计时秒数
function LuaHengYuDangKouPlayingWnd:GetLeftTime()
    return math.floor(self.EndTimeStamp - CServerTimeMgr.Inst.timeStamp + 0.5)
end
-- 启动倒计时
function LuaHengYuDangKouPlayingWnd:StartCountDown(totaltime, endtimestamp)
    self.CountDownTime = 0
    self.CountDownTotalTime = totaltime -- 总时间
    self.EndTimeStamp = endtimestamp -- 结束时间戳
    self:OnSceneRemainTimeUpdate()
end
function LuaHengYuDangKouPlayingWnd:Init()
    -- 获取怪物信息
    self.MonsterInfo = LuaHengYuDangKouMgr:InitMonsterInfo()
    -- 右侧按钮绑定功能窗口
    UIEventListener.Get(self.ExpandButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(p)
        self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
        CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
    end)
    -- 状态变量初始化
    self.EndTimeStamp = nil
    self.IsPanelInit = false
    self.IsInXiaoGuai = false
    self.IsBossCountDown = false
    self.IsXiaoGuaiCountDown = false
    -- 加载场景信息
    self:SyncHengYuDangKouSceneInfo()
    self:SyncHengYuDangKouBossHp(LuaHengYuDangKouMgr.m_curHp, LuaHengYuDangKouMgr.m_fullHp)
end
-- 功能窗口隐藏时翻转右侧按钮
function LuaHengYuDangKouPlayingWnd:OnHideTopAndRightTipWnd()
    self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
end
-- 同步场景信息
function LuaHengYuDangKouPlayingWnd:SyncHengYuDangKouSceneInfo()
    if not LuaHengYuDangKouMgr.m_SceneInfo then return end -- 还未获取到场景信息
    LuaCommonGamePlayTaskViewMgr:OnUpdateTitleAndContent() -- 刷新左侧信息
    self.SceneInfo = LuaHengYuDangKouMgr.m_SceneInfo
    self.Type = self.SceneInfo.stage
    if not self.IsPanelInit then self:InitPanel() end -- 初始化面板
    if not self.IsPanelInit then return end           -- 还没有初始化面板
    -- 不在boss2不显示血条
    if self.SceneInfo.progress ~= 2 then
        self.BossHP.gameObject:SetActive(false)
    end
    -- 是否有boss狂暴倒计时
    if self.SceneInfo.bossCountdownEndTs and self.SceneInfo.bossCountdownDuration then
        self.IsBossCountDown = true
        self:StartCountDown(self.SceneInfo.bossCountdownDuration, self.SceneInfo.bossCountdownEndTs)
    else
        self.IsBossCountDown = false
    end
    if self.Type == EnumHengYuDangKouStage.eHard and self.SceneInfo.progress >= self.MonsterCount then
        self:StartXiaoGuai()
        if self.SceneInfo.extraMonsterEndTime then
            self.IsXiaoGuaiCountDown = true
            local totaltime = HengYuDangKou_Countdown.GetData("HardModeExtraTime").Time
            self:StartCountDown(totaltime, self.SceneInfo.extraMonsterEndTime)
        end
    else
        self:ShowProgress(self.SceneInfo.progress)
    end
    -- 只有第一次主动弹出怪物信息窗口
    if not LuaHengYuDangKouMgr.m_IsMonsterInfoShow then
        LuaHengYuDangKouMgr.m_IsMonsterInfoShow = true -- 防止反复弹出
        LuaHengYuDangKouMgr.m_ClickId = self.SceneInfo.progress
        CUIManager.ShowUI(CLuaUIResources.HengYuDangKouMonsterInfoWnd)
    end
end
-- 同步boss血量
function LuaHengYuDangKouPlayingWnd:SyncHengYuDangKouBossHp(curHp, fullHp)
    -- 不在boss2不显示血条
    if not self.SceneInfo or self.SceneInfo.progress ~= 2 then
        self.BossHP.gameObject:SetActive(false)
        return
    end
    if fullHp and fullHp > 0 and curHp then
        self.BossHP.gameObject:SetActive(true)
        local rate = curHp / fullHp
        if rate < 0 then rate = 0 end
        if rate > 1 then rate = 1 end
        -- 血条并不是完整的，缺失了angle的角度，需要进行换算
        local angle = 90
        local rmin = angle / 2 / 360
        local rmax = 1 - rmin
        rate = rmin + (rmax - rmin) * rate
        self.BossHP.fillAmount = rate
    else
        self.BossHP.gameObject:SetActive(false)
    end
end
-- 场景倒计时更新
function LuaHengYuDangKouPlayingWnd:OnSceneRemainTimeUpdate()
    if not self.EndTimeStamp or not self.IsXiaoGuaiCountDown and not self.IsBossCountDown then return end
    local nowtime = self:GetLeftTime()
    if nowtime > self.CountDownTotalTime then nowtime = self.CountDownTotalTime end
    if nowtime < 0 then nowtime = 0 end
    self.CountDownTime = nowtime
    self.CountDown.value = nowtime / self.CountDownTotalTime
    local minute = math.floor(nowtime / 60)
    local second = nowtime % 60
    self.CountDownLabel.text = SafeStringFormat(LocalString.GetString("倒计时 %d:%02d"), minute, second)
end
function LuaHengYuDangKouPlayingWnd:OnEnable()
    g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("SyncHengYuDangKouSceneInfo", self, "SyncHengYuDangKouSceneInfo")
    g_ScriptEvent:AddListener("SyncHengYuDangKouBossHp", self, "SyncHengYuDangKouBossHp")
    EventManager.Broadcast(EnumEventType.MainPlayerPlayPropUpdate) -- 广播：特殊的副本倒计时位置
end
function LuaHengYuDangKouPlayingWnd:OnDisable()
    if self.NumberTickTask then
        UnRegisterTick(self.NumberTickTask)
    end
    if self.NumberLabel then
        LuaTweenUtils.DOKill(self.NumberLabel.transform, false)
    end
    g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("SyncHengYuDangKouSceneInfo", self, "SyncHengYuDangKouSceneInfo")
    g_ScriptEvent:RemoveListener("SyncHengYuDangKouBossHp", self, "SyncHengYuDangKouBossHp")
    LuaHengYuDangKouMgr.m_SceneInfo = nil
    LuaHengYuDangKouMgr.m_MonsterInfo = nil
    LuaHengYuDangKouMgr.m_KillCount = nil
    LuaHengYuDangKouMgr.m_curHp = 0
    LuaHengYuDangKouMgr.m_fullHp = 0
end
-- 倒计时跟踪当前图标位置
function LuaHengYuDangKouPlayingWnd:CountDownTrackHighlight()
    local vec = self.CountDown.transform.position
    local x = self.MonsterHighlight.transform.position.x
    self.CountDown.transform.position = Vector3(x, vec.y, vec.z)
    x = self.CountDown.transform.position.x
    local lx = self.ScrollView.panel.worldCorners[0].x
    local rx = self.ScrollView.panel.worldCorners[2].x
    if lx < x and x < rx then -- 在面板范围内才显示倒计时
        self.CountDown.gameObject:SetActive(true)
    else
        self.CountDown.gameObject:SetActive(false)
    end
end
function LuaHengYuDangKouPlayingWnd:Update()
    if self.IsXiaoGuaiCountDown then
        if self.CountDownTime > 0 then
            self:CountDownTrackHighlight()
        else
            self.CountDown.gameObject:SetActive(false)
        end
    else
        if self.IsBossCountDown then
            self:CountDownTrackHighlight()
        else
            self.CountDown.gameObject:SetActive(false)
        end
    end
end

--@region UIEvent

--@endregion UIEvent

