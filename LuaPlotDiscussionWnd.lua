local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CUITexture = import "L10.UI.CUITexture"
local UISprite = import "UISprite"
local QnTableView = import "L10.UI.QnTableView"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UIInput = import "UIInput"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local AlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local UILabelOverflow = import "UILabel+Overflow"
local CChatInputMgr = import "L10.UI.CChatInputMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local CChatInputMgrEParentType = import "L10.UI.CChatInputMgr+EParentType"
local DefaultChatInputListener = import "L10.UI.DefaultChatInputListener"
local CChatInputLink = import "CChatInputLink"
local Constants = import "L10.Game.Constants"
local CQnSymbolParser = import "CQnSymbolParser"
local Time = import "UnityEngine.Time"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local TweenPosition = import "TweenPosition"
local CChatLinkMgr = import "CChatLinkMgr"
local UICamera = import "UICamera"
local CButton = import "L10.UI.CButton"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"

LuaPlotDiscussionWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaPlotDiscussionWnd, "CardNameLabel", "CardNameLabel", UILabel)
RegistChildComponent(LuaPlotDiscussionWnd, "CardSuiPianTexture", "CardSuiPianTexture", GameObject)
RegistChildComponent(LuaPlotDiscussionWnd, "CardPurpleTexture", "CardPurpleTexture", GameObject)
RegistChildComponent(LuaPlotDiscussionWnd, "CardBlueTexture", "CardBlueTexture", GameObject)
RegistChildComponent(LuaPlotDiscussionWnd, "CardRedTexture", "CardRedTexture", GameObject)
RegistChildComponent(LuaPlotDiscussionWnd, "CardOwnerTexture", "CardOwnerTexture", CUITexture)
RegistChildComponent(LuaPlotDiscussionWnd, "CardTitleLabel", "CardTitleLabel", UILabel)
RegistChildComponent(LuaPlotDiscussionWnd, "CollectionSprite", "CollectionSprite", UISprite)
RegistChildComponent(LuaPlotDiscussionWnd, "CollectionLabel", "CollectionLabel", UILabel)
RegistChildComponent(LuaPlotDiscussionWnd, "SortBtn", "SortBtn", QnSelectableButton)
RegistChildComponent(LuaPlotDiscussionWnd, "SortbyLabel", "SortbyLabel", UILabel)
RegistChildComponent(LuaPlotDiscussionWnd, "SortSprite", "SortSprite", UISprite)
RegistChildComponent(LuaPlotDiscussionWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaPlotDiscussionWnd, "NoneDiscussionLabel", "NoneDiscussionLabel", UILabel)
RegistChildComponent(LuaPlotDiscussionWnd, "CommentView", "CommentView", GameObject)
RegistChildComponent(LuaPlotDiscussionWnd, "DetailView", "DetailView", GameObject)
RegistChildComponent(LuaPlotDiscussionWnd, "DetailViewItem", "DetailViewItem", GameObject)
RegistChildComponent(LuaPlotDiscussionWnd, "DetailViewScrollView", "DetailViewScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaPlotDiscussionWnd, "SendMsgButton", "SendMsgButton", GameObject)
RegistChildComponent(LuaPlotDiscussionWnd, "EmotionButton", "EmotionButton", GameObject)
RegistChildComponent(LuaPlotDiscussionWnd, "StatusText", "StatusText", UIInput)
RegistChildComponent(LuaPlotDiscussionWnd, "SendDiscussionView", "SendDiscussionView", GameObject)
RegistChildComponent(LuaPlotDiscussionWnd, "DetailViewSendMsgButton", "DetailViewSendMsgButton", GameObject)
RegistChildComponent(LuaPlotDiscussionWnd, "DetailViewEmotionButton", "DetailViewEmotionButton", GameObject)
RegistChildComponent(LuaPlotDiscussionWnd, "DetailViewStatusText", "DetailViewStatusText", UIInput)
RegistChildComponent(LuaPlotDiscussionWnd, "SendDiscussionViewRebackButton", "SendDiscussionViewRebackButton", GameObject)
RegistChildComponent(LuaPlotDiscussionWnd, "DetailViewMessageBarTemplate", "DetailViewMessageBarTemplate", GameObject)
RegistChildComponent(LuaPlotDiscussionWnd, "NewMessageTip", "NewMessageTip", CButton)
RegistChildComponent(LuaPlotDiscussionWnd, "NewMessageView", "NewMessageView", GameObject)
RegistChildComponent(LuaPlotDiscussionWnd, "DetailViewRebackButton", "DetailViewRebackButton", GameObject)
RegistChildComponent(LuaPlotDiscussionWnd, "NewMessageViewRebackButton", "NewMessageViewRebackButton", GameObject)
RegistChildComponent(LuaPlotDiscussionWnd, "ClearNewMessageButton", "ClearNewMessageButton", GameObject)
RegistChildComponent(LuaPlotDiscussionWnd, "NewMessageTableView", "NewMessageTableView", QnTableView)
RegistChildComponent(LuaPlotDiscussionWnd, "QnNumButton", "QnNumButton", QnAddSubAndInputButton)
RegistChildComponent(LuaPlotDiscussionWnd, "Collection", "Collection", GameObject)
RegistChildComponent(LuaPlotDiscussionWnd, "ShowMoreDetailMessagesNode", "ShowMoreDetailMessagesNode", GameObject)
RegistChildComponent(LuaPlotDiscussionWnd, "UnLockLabel", "UnLockLabel", UILabel)
--@endregion RegistChildComponent end

RegistClassMember(LuaPlotDiscussionWnd,"m_SortIndex")
RegistClassMember(LuaPlotDiscussionWnd,"m_CommentsData")
RegistClassMember(LuaPlotDiscussionWnd,"m_CommentSubHeight")
RegistClassMember(LuaPlotDiscussionWnd,"m_MessageFoldLineNumber")
RegistClassMember(LuaPlotDiscussionWnd,"m_DefaultChatInputListener")
RegistClassMember(LuaPlotDiscussionWnd,"m_ExistingLinks")
RegistClassMember(LuaPlotDiscussionWnd,"m_LastSendTime")
RegistClassMember(LuaPlotDiscussionWnd,"m_TweenPositon")
RegistClassMember(LuaPlotDiscussionWnd,"m_DetailViewMessages")
RegistClassMember(LuaPlotDiscussionWnd,"m_ReplayPlayerInfo")
RegistClassMember(LuaPlotDiscussionWnd,"m_NewCommentsData")
RegistClassMember(LuaPlotDiscussionWnd,"m_IsDetailViewRebackNewMessageView")
RegistClassMember(LuaPlotDiscussionWnd,"m_PageIndex")
RegistClassMember(LuaPlotDiscussionWnd,"m_PageSize")
RegistClassMember(LuaPlotDiscussionWnd,"m_CurCommentId")
RegistClassMember(LuaPlotDiscussionWnd,"m_DetailViewPageIndex")
RegistClassMember(LuaPlotDiscussionWnd,"m_DetailViewPageSize")
RegistClassMember(LuaPlotDiscussionWnd,"m_IsReadAll")
function LuaPlotDiscussionWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	self.SortBtn.OnButtonSelected = DelegateFactory.Action_bool(function (selected )
        self:OnSelectSortsClick(selected)
    end)

	UIEventListener.Get(self.SendMsgButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSendMsgButtonClick()
	end)
	self:InitDefaultChatInputListener()
	UIEventListener.Get(self.EmotionButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnEmotionButtonClick()
	end)

	UIEventListener.Get(self.DetailViewSendMsgButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDetailViewSendMsgButtonClick()
	end)

	UIEventListener.Get(self.DetailViewEmotionButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDetailViewEmotionButtonClick()
	end)

	UIEventListener.Get(self.SendDiscussionViewRebackButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSendDiscussionViewRebackButtonClick()
	end)

	UIEventListener.Get(self.NewMessageTip.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnNewMessageTipClick()
	end)

	UIEventListener.Get(self.DetailViewRebackButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDetailViewRebackButtonClick()
	end)

	UIEventListener.Get(self.NewMessageViewRebackButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnNewMessageViewRebackButtonClick()
	end)

	UIEventListener.Get(self.ClearNewMessageButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnClearNewMessageButtonClick()
	end)

	UIEventListener.Get(self.Collection.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCollectionButtonClick()
	end)

	UIEventListener.Get(self.ShowMoreDetailMessagesNode.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShowMoreDetailMessagesNodeClick()
	end)

    --@endregion EventBind end
end

--@region DefaultChatInputListener
function LuaPlotDiscussionWnd:InitDefaultChatInputListener()
	self.m_ExistingLinks =  CreateFromClass(MakeGenericClass(Dictionary, Int32, CChatInputLink))
	self.m_DefaultChatInputListener = DefaultChatInputListener()
	self.m_DefaultChatInputListener:SetOnInputEmoticonFunc(function (prefix)
		self:OnInputEmoticon(prefix)
	end)
	self.m_DefaultChatInputListener:SetOnInputItemFunc(function (item)
		self:OnInputItemLink(item)
	end)
	self.m_DefaultChatInputListener:SetOnInputBabyFunc(function (babyId, babyName)
		self:OnInputBabyLink(babyId, babyName)
	end)
	self.m_DefaultChatInputListener:SetOnLingShouFunc(function (lingshouId, lingshouName)
		self:OnInputLingShouLink(lingshouId, lingshouName)
	end)
	self.m_DefaultChatInputListener:SetOnInputWeddingItemFunc(function (item)
		self:OnInputWeddingItemLink(item)
	end)
	self.m_DefaultChatInputListener:SetOnInputAchievementFunc(function (achievementId)
		self:OnInputAchievement(achievementId)
	end)
end

function LuaPlotDiscussionWnd:GetCurStatusText()
	return self.DetailView.gameObject.activeSelf and self.DetailViewStatusText or self.StatusText
end

function LuaPlotDiscussionWnd:GetSendMsg()
	local input = self:GetCurStatusText()
	local sendInfo = input.value
	if System.String.IsNullOrEmpty(sendInfo) then
        g_MessageMgr:ShowMessage("ENTER_TEXT_TIP")
        return
    end
	input.value = CQnSymbolParser.FilterExceededEmoticons(StringTrim(sendInfo), Constants.MaxEmotionNum)
	local msg = input.value
	if self.m_LastSendTime and Time.realtimeSinceStartup - self.m_LastSendTime < 1 then
        return
    end
	self.m_LastSendTime = Time.realtimeSinceStartup
	local sendText = CChatInputLink.Encapsulate(msg, self.m_ExistingLinks)
	sendText = CWordFilterMgr.Inst:DoFilterOnSendPersonalSpace(sendText, true)
	return sendText
end

function LuaPlotDiscussionWnd:OnInputEmoticon(prefix)
	local input = self:GetCurStatusText()
	input.text = SafeStringFormat3("%s#%s",input.text, prefix) 
end

function LuaPlotDiscussionWnd:OnInputItemLink(item)
	local text = nil
	local input = self:GetCurStatusText()
	text = CChatInputLink.AppendItemLink(input.value, item, self.m_ExistingLinks)
	input.text = text
end

function LuaPlotDiscussionWnd:OnInputBabyLink(babyId, babyName)
	local text = nil
	local input = self:GetCurStatusText()
	text = CChatInputLink.AppendBabyLink(input.value, babyId, babyName, self.m_ExistingLinks)
	input.text = text
end

function LuaPlotDiscussionWnd:OnInputLingShouLink(lingshouId, lingshouName)
	local text = nil
	local input = self:GetCurStatusText()
	text = CChatInputLink.AppendLingShouLink(input.value, lingshouId, lingshouName, self.m_ExistingLinks)
	input.text = text
end

function LuaPlotDiscussionWnd:OnInputWeddingItemLink(item)
	local text = nil
	local input = self:GetCurStatusText()
	text = CChatInputLink.AppendWeddingItemLink(input.value, item, self.m_ExistingLinks)
	input.text = text
end

function LuaPlotDiscussionWnd:OnInputAchievement(achievementId)
	local text = nil
	local input = self:GetCurStatusText()
	text = CChatInputLink.AppendAchievementLink(input.value, achievementId, self.m_ExistingLinks)
	input.text = text
end

--@endregion DefaultChatInputListener

function LuaPlotDiscussionWnd:OnEnable()
	g_ScriptEvent:AddListener("OnPlotAllNewNotification",self,"OnPlotAllNewNotification") 
	g_ScriptEvent:AddListener("OnPlotRequestCardsLikeInfo",self,"OnPlotRequestCardsLikeInfo") 
	g_ScriptEvent:AddListener("OnPlotNotificationNum", self, "OnPlotNotificationNum") 

	g_ScriptEvent:AddListener("OnPlotRequestReadAllCards", self, "OnPlotRequestReadAllCards") 
	g_ScriptEvent:AddListener("OnPlotRequestGiveCommentThumbsUp", self, "OnPlotRequestGiveCommentThumbsUp") 
	g_ScriptEvent:AddListener("OnPlotDeleteCommentDetailMessage", self, "OnPlotDeleteCommentDetailMessage") 
	g_ScriptEvent:AddListener("OnPlotAddCommentDetailMessage", self, "OnPlotAddCommentDetailMessage") 
	g_ScriptEvent:AddListener("OnPlotDeleteCardComment", self, "OnPlotDeleteCardComment") 

	g_ScriptEvent:AddListener("OnPlotAddCardComment", self, "OnPlotAddCardComment") 
	g_ScriptEvent:AddListener("OnPlotCardCommentsList", self, "OnPlotCardCommentsList") 
	g_ScriptEvent:AddListener("OnPlotDiscussionCardFavorited", self, "OnPlotDiscussionCardFavorited") 
	g_ScriptEvent:AddListener("OnPlotCommentFavorPlayInfoUpdate", self, "OnPlotCommentFavorPlayInfoUpdate") 
	g_ScriptEvent:AddListener("OnPlotCommentMessagesUpdate", self, "OnPlotCommentMessagesUpdate") 
	LuaPlotDiscussionMgr:RequestForNotificationNum()
end

function LuaPlotDiscussionWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnPlotAllNewNotification",self,"OnPlotAllNewNotification") 
	g_ScriptEvent:RemoveListener("OnPlotRequestCardsLikeInfo",self,"OnPlotRequestCardsLikeInfo") 
	g_ScriptEvent:RemoveListener("OnPlotNotificationNum", self, "OnPlotNotificationNum")

	g_ScriptEvent:RemoveListener("OnPlotRequestReadAllCards", self, "OnPlotRequestReadAllCards") 
	g_ScriptEvent:RemoveListener("OnPlotRequestGiveCommentThumbsUp", self, "OnPlotRequestGiveCommentThumbsUp") 
	g_ScriptEvent:RemoveListener("OnPlotDeleteCommentDetailMessage", self, "OnPlotDeleteCommentDetailMessage") 
	g_ScriptEvent:RemoveListener("OnPlotAddCommentDetailMessage", self, "OnPlotAddCommentDetailMessage") 
	g_ScriptEvent:RemoveListener("OnPlotDeleteCardComment", self, "OnPlotDeleteCardComment") 

	g_ScriptEvent:RemoveListener("OnPlotAddCardComment", self, "OnPlotAddCardComment")
	g_ScriptEvent:RemoveListener("OnPlotCardCommentsList", self, "OnPlotCardCommentsList")
	g_ScriptEvent:RemoveListener("OnPlotDiscussionCardFavorited", self, "OnPlotDiscussionCardFavorited")
	g_ScriptEvent:RemoveListener("OnPlotCommentFavorPlayInfoUpdate", self, "OnPlotCommentFavorPlayInfoUpdate")
	g_ScriptEvent:RemoveListener("OnPlotCommentMessagesUpdate", self, "OnPlotCommentMessagesUpdate")
	LuaPlotDiscussionMgr.m_NewCommentsData = {}
end

function LuaPlotDiscussionWnd:OnPlotAllNewNotification()
	self.m_NewCommentsData = LuaPlotDiscussionMgr.m_NewCommentsData
	self.CommentView.gameObject:SetActive(false)
	self.NewMessageView.gameObject:SetActive(true)
	self.NewMessageTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return #self.m_NewCommentsData
        end,
        function(item,index) self:InitNewMessageItem(item,index) end
    )
	self.NewMessageTableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        self:OnSelectNewMessageViewAtRow(row)
    end)
	self.NewMessageTableView:ReloadData(true, false)
end

function LuaPlotDiscussionWnd:OnPlotRequestCardsLikeInfo()
	self.CollectionSprite.spriteName = LuaPlotDiscussionMgr.m_IsLike and "personalspacewnd_heart_2" or "personalspacewnd_heart_1"
	self.CollectionLabel.text = LuaPlotDiscussionMgr.m_LikeNum
end

function LuaPlotDiscussionWnd:OnPlotNotificationNum(count)
	self:InitNewReceiveComments(count)
end

function LuaPlotDiscussionWnd:OnPlotRequestReadAllCards()
	if self.m_IsReadAll then return end
	self.m_IsReadAll = true
	g_MessageMgr:ShowMessage("PlotDiscussionWnd_ClearNewMessages")
end

function LuaPlotDiscussionWnd:OnPlotRequestGiveCommentThumbsUp(commentId, isMyFavor)
	if self.m_CurCommentId ~= commentId then return end
	LuaPlotDiscussionMgr:RequestCardCommentLikeUsers(commentId)
end

function LuaPlotDiscussionWnd:OnPlotDeleteCommentDetailMessage(commentId)
	--print(self.m_CurCommentId,commentId)
	if self.m_CurCommentId ~= commentId then return end
	LuaPlotDiscussionMgr:RequestCommentDetailMessages(commentId, self.m_DetailViewPageIndex, self.m_DetailViewPageSize)
end

function LuaPlotDiscussionWnd:OnPlotAddCommentDetailMessage(commentId, msgsCount)
	--print(self.m_CurCommentId,commentId)
	if self.m_CurCommentId ~= commentId then return end
	LuaPlotDiscussionMgr:RequestCommentDetailMessages(commentId, self.m_DetailViewPageIndex, self.m_DetailViewPageSize)
end

function LuaPlotDiscussionWnd:OnPlotDeleteCardComment()
	LuaPlotDiscussionMgr:RequestCardCommentsList(LuaPlotDiscussionMgr.m_CardId, self.m_SortIndex == 1 and "hot" or "new", self.m_PageIndex, self.m_PageSize)
end

function LuaPlotDiscussionWnd:OnPlotAddCardComment()
	LuaPlotDiscussionMgr:RequestCardCommentsList(LuaPlotDiscussionMgr.m_CardId, self.m_SortIndex == 1 and "hot" or "new", 1, self.m_PageSize)
end

function LuaPlotDiscussionWnd:OnPlotCardCommentsList(commentCount, page)
	self.NoneDiscussionLabel.gameObject:SetActive(commentCount == 0)
	local maxPage = math.ceil(commentCount / self.m_PageSize)
	self.CollectionSprite.spriteName = LuaPlotDiscussionMgr.m_IsLike and "personalspacewnd_heart_2" or "personalspacewnd_heart_1"
	self.CollectionLabel.text = LuaPlotDiscussionMgr.m_LikeNum
	self.QnNumButton:SetMinMax(1, maxPage, 1)
	self.QnNumButton:SetValue(page, false)
	self.TableView:ReloadData(true, false)
end

function LuaPlotDiscussionWnd:InitNewReceiveComments(count)
	if count and count > 0 then
		self.SortBtn.gameObject:SetActive(false)
		self.NewMessageTip.gameObject:SetActive(true)
		self.NewMessageTip.Text = SafeStringFormat3(LocalString.GetString("你有%d条新消息"),count)
	end
end

function LuaPlotDiscussionWnd:OnPlotDiscussionCardFavorited(cardId, isMyFavor)
	if cardId ~= 0 then
		if LuaPlotDiscussionMgr.m_HaoYiXingData then
			if LuaPlotDiscussionMgr.m_HaoYiXingData.id == cardId then
				LuaPlotDiscussionMgr.m_IsLike = isMyFavor
			end
		end
		if LuaPlotDiscussionMgr.m_YaoGuiHuData then
			if LuaPlotDiscussionMgr.m_YaoGuiHuData.GroupID == cardId then
				LuaPlotDiscussionMgr.m_IsLike = isMyFavor
			end
		end
	end
	self.CollectionSprite.spriteName = LuaPlotDiscussionMgr.m_IsLike and "personalspacewnd_heart_2" or "personalspacewnd_heart_1"
end

function LuaPlotDiscussionWnd:OnPlotCommentFavorPlayInfoUpdate(id, isMyFavor, likeCount)
	--print(id, isMyFavor, likeCount)
	for i = 1,#LuaPlotDiscussionMgr.m_CommentsData do
		local t = LuaPlotDiscussionMgr.m_CommentsData[i]
		if t.id == id then
			local item = self.TableView:GetItemAtRow(i - 1)
			if item then
				if self.DetailView.gameObject.activeSelf and self.m_CurCommentId == id then
					self:InitDetailView(t, false)
				else
					self:InitCommentFavorIcon(item, isMyFavor, likeCount)
				end
			end
			break
		end
	end
end

function LuaPlotDiscussionWnd:OnPlotCommentMessagesUpdate(id)
	for i = 1,#LuaPlotDiscussionMgr.m_CommentsData do
		local t = LuaPlotDiscussionMgr.m_CommentsData[i]
		if t.id == id then
			--print(#t.msgs)
			local item = self.TableView:GetItemAtRow(i - 1)
			if item then
				if self.DetailView.gameObject.activeSelf and self.m_CurCommentId == id then
					self:InitDetailView(t, false)
				else
					self:InitMessageNumLabel(item, t)
				end
			end
			break
		end
	end
end

function LuaPlotDiscussionWnd:Start()
	self:InitLeftView()
	self:InitRightView()
	self:InitCommentView()
	self:InitNewReceiveComments()
end

function LuaPlotDiscussionWnd:InitLeftView()
	self.CardNameLabel.text = LuaPlotDiscussionMgr.m_Title
	self.CardPurpleTexture.gameObject:SetActive((not LuaPlotDiscussionMgr.m_HaoYiXingData) or (LuaPlotDiscussionMgr.m_HaoYiXingData.tcgdata.Type == 3 or LuaPlotDiscussionMgr.m_HaoYiXingData.tcgdata.Type == 4))
	self.CardBlueTexture.gameObject:SetActive(false)
	self.CardRedTexture.gameObject:SetActive(LuaPlotDiscussionMgr.m_HaoYiXingData and (LuaPlotDiscussionMgr.m_HaoYiXingData.tcgdata.Type == 2))
	self.CardOwnerTexture:LoadMaterial(LuaPlotDiscussionMgr.m_CardRes)
	self.CardOwnerTexture.uiTexture.width = LuaPlotDiscussionMgr.m_HaoYiXingData and 285 or 225
	self.CardOwnerTexture.uiTexture.height = LuaPlotDiscussionMgr.m_HaoYiXingData and 340 or 268
	self.CardTitleLabel.text = LuaPlotDiscussionMgr.m_AchievementName
	self.CardTitleLabel.gameObject:SetActive(not String.IsNullOrEmpty(LuaPlotDiscussionMgr.m_AchievementName))
	self.CollectionSprite.spriteName = LuaPlotDiscussionMgr.m_IsLike and "personalspacewnd_heart_2" or "personalspacewnd_heart_1"
	self.CollectionLabel.text = LuaPlotDiscussionMgr.m_LikeNum

	self.CardOwnerTexture.gameObject:SetActive(LuaPlotDiscussionMgr.m_ShowCardOwnerTexture)
	self.CardSuiPianTexture.gameObject:SetActive(LuaPlotDiscussionMgr.m_ShowSuiPianTexture)
	Extensions.SetLocalPositionZ(self.CardOwnerTexture.transform, LuaPlotDiscussionMgr.m_ShowCardOwnerTextureLocalPositionZ)
	if LuaPlotDiscussionMgr.m_HaoYiXingData then
		self.UnLockLabel.text = CUICommonDef.TranslateToNGUIText(g_MessageMgr:FormatMessage("PlotDiscussionWnd_HaoYiXingCard_Unlocked" ,LuaPlotDiscussionMgr.m_Title))
	elseif LuaPlotDiscussionMgr.m_YaoGuiHuData then
		if cs_string.IsNullOrEmpty(LuaPlotDiscussionMgr.m_YaoGuiHuData.UnLockText) then
			local data = Task_Task.GetData(LuaPlotDiscussionMgr.m_YaoGuiHuData.taskId)
			if data then
				self.UnLockLabel.text = CUICommonDef.TranslateToNGUIText(g_MessageMgr:FormatMessage("PlotDiscussionWnd_YaoGuiHuCard_Unlocked",data.Display))
			end
		else
			self.UnLockLabel.text = CUICommonDef.TranslateToNGUIText(LuaPlotDiscussionMgr.m_YaoGuiHuData.UnLockText)
		end
	end
	self.UnLockLabel.gameObject:SetActive(true)
end

function LuaPlotDiscussionWnd:InitRightView()
	self.CommentView.gameObject:SetActive(true)
	self.DetailView.gameObject:SetActive(false)
	self.NewMessageView.gameObject:SetActive(false)
	self.NewMessageTip.gameObject:SetActive(false)
	self.NoneDiscussionLabel.gameObject:SetActive(false)
	self.StatusText.gameObject:SetActive(false)
	self.QnNumButton:SetValue(1, false)
	self.QnNumButton.onValueChanged = DelegateFactory.Action_uint(function(page)
		self:OnPageChange(page)
	  end)
	self.m_PageSize = 10
	self.m_PageIndex = 1
end

--@region CommentView
function LuaPlotDiscussionWnd:InitCommentView()
	self.m_DetailViewMessages = {}
	self.SortBtn.gameObject:SetActive(true)
	self.NewMessageTip.gameObject:SetActive(false)
	self.m_MessageFoldLineNumber = 10
	local poolItem = self.TableView.m_Pool.transform:GetChild(0):GetComponent(typeof(UISprite))
	local messageLabel =  FindChild(poolItem.transform,"MessageLabel"):GetComponent(typeof(UILabel))
	self.m_CommentSubHeight = poolItem.height - messageLabel.height
	self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() 
            return #LuaPlotDiscussionMgr.m_CommentsData
        end,
        function(item,index) self:InitComment(item,index) end
    )
	self.NoneDiscussionLabel.gameObject:SetActive(#LuaPlotDiscussionMgr.m_CommentsData == 0)
	self:OnChooseSortClicked(1)
end

function LuaPlotDiscussionWnd:InitComment(item, index)
	local data = LuaPlotDiscussionMgr.m_CommentsData[index + 1]

	local class = data.class
	local gender = data.gender
	local name = data.playerName
	local lv = data.lv
	local msg = data.msg
	local playerId = data.playerId
	local hasFeiSheng = data.hasFeiSheng
	local favorNum = data.likeCount
	local isMyFavor = data.isMyFavor
	local createTime = data.createTime
	local id = data.id

	local mainSprite = item:GetComponent(typeof(UISprite))
	local icon = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
	local table = item.transform:Find("Table"):GetComponent(typeof(UITable))
	local nameLabel = item.transform:Find("Table/TopView/NameLabel"):GetComponent(typeof(UILabel))
	local lvLabel = item.transform:Find("Table/TopView/NameLabel/LvLabel"):GetComponent(typeof(UILabel))
	local moreBtnLabel = item.transform:Find("Table/MoreBtn"):GetComponent(typeof(UILabel))
	local favorButton = item.transform:Find("Table/BottomView/FavorButton").gameObject
	local messageButton = item.transform:Find("Table/BottomView/MessageButton").gameObject
	local deleteButton = item.transform:Find("Table/BottomView/DeleteButton").gameObject
	local timeLabel = item.transform:Find("Table/BottomView/TimeLabel"):GetComponent(typeof(UILabel))
	local messageLabel = item.transform:Find("Table/MessageLabel"):GetComponent(typeof(UILabel))

	deleteButton.gameObject:SetActive(CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == playerId)
	icon:LoadNPCPortrait(CUICommonDef.GetPortraitName(class, gender, -1), false)
	nameLabel.text = name
	nameLabel.color = (CClientMainPlayer.Inst and playerId == CClientMainPlayer.Inst.Id) and NGUIText.ParseColor24("00ff60", 0) or Color.white
	lvLabel.text = SafeStringFormat3("Lv.%d",lv)
	lvLabel.color = hasFeiSheng and NGUIText.ParseColor24("fe7900", 0) or Color.white
	self:InitCommentFavorIcon(item, isMyFavor, favorNum)
	self:InitMessageNumLabel(item, data)
	timeLabel.text = self:GetTimeText(createTime)
	messageLabel.overflowMethod = UILabelOverflow.ResizeHeight
	messageLabel.text = CChatLinkMgr.TranslateToNGUIText(msg)
	local isShowMoreBtn = messageLabel.height > messageLabel.fontSize * self.m_MessageFoldLineNumber
	moreBtnLabel.gameObject:SetActive(isShowMoreBtn)
	self:ResetComment(moreBtnLabel, messageLabel, table, mainSprite, isShowMoreBtn, isShowMoreBtn, false)
	
	UIEventListener.Get(moreBtnLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMoreBtnLabelClick(moreBtnLabel, messageLabel, table, mainSprite)
	end)
	UIEventListener.Get(favorButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnFavorBtnLabelClick(data)
	end)
	UIEventListener.Get(messageButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMessageButtonClick(data)
	end)
	UIEventListener.Get(deleteButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDeleteMyDiscussionButtonClick(data)
	end)
	UIEventListener.Get(messageLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMessageLabelClick(messageLabel)
	end)
	UIEventListener.Get(icon.gameObject).onClick = DelegateFactory.VoidDelegate(function (p) 
        self:OnIconbuttonClick(playerId, id, 0, msg)
    end)
end

function LuaPlotDiscussionWnd:InitCommentFavorIcon(item, isMyFavor, favorNum)
	local favorIcon = item.transform:Find("Table/BottomView/FavorButton/FavorIcon").gameObject
	local favorIconBg = item.transform:Find("Table/BottomView/FavorButton/FavorIconBg").gameObject
	local favorNumLabel = item.transform:Find("Table/BottomView/FavorButton/FavorNumLabel"):GetComponent(typeof(UILabel))
	favorIcon.gameObject:SetActive(isMyFavor)
	favorIconBg.gameObject:SetActive(not isMyFavor)
	favorNumLabel.text = favorNum
end

function LuaPlotDiscussionWnd:InitMessageNumLabel(item, data)
	local messageNumLabel = item.transform:Find("Table/BottomView/MessageButton/MessageNumLabel"):GetComponent(typeof(UILabel))
	local messageNum = data.messageNum
	messageNumLabel.text = messageNum
end

function LuaPlotDiscussionWnd:ResetComment(moreBtnLabel, messageLabel, table, mainSprite, isShowMoreBtn, isResetComment, needReposition)
	messageLabel.overflowMethod = isResetComment and UILabelOverflow.ClampContent or UILabelOverflow.ResizeHeight
	if isResetComment then
		messageLabel.height = messageLabel.fontSize * self.m_MessageFoldLineNumber
	end
	moreBtnLabel.text = isResetComment and LocalString.GetString("全文") or LocalString.GetString("收起")
	table:Reposition()
	local height = self.m_CommentSubHeight + messageLabel.height
	if not isShowMoreBtn then
		height = height - moreBtnLabel.fontSize - table.padding.y
	end
	mainSprite.height = height
	if needReposition then
		self.TableView.m_Table:Reposition()
	end
end

--@endregion CommentView

--@region DetailView

function LuaPlotDiscussionWnd:InitDetailView(data, updatePos)
	self:ClearDetailViewMessages()
	self.DetailViewMessageBarTemplate.gameObject:SetActive(false)
	self.SendDiscussionView.gameObject:SetActive(false)
	self.DetailView.gameObject:SetActive(true)
	local item = self.DetailViewItem

	local class = data.class
	local gender = data.gender
	local name = data.playerName
	local lv = data.lv
	local msg = data.msg
	local playerId = data.playerId
	local hasFeiSheng = data.hasFeiSheng
	local favorNum = data.likeCount
	local isMyFavor = data.isMyFavor
	local messageNum = data.messageNum
	local createTime = data.createTime
	local id = data.id
	self.m_CurCommentId = id
	self.ShowMoreDetailMessagesNode.gameObject:SetActive((self.m_PageIndex * self.m_PageSize) < messageNum)

	local bottomView = item.transform:Find("Table/BottomView").gameObject
	bottomView.gameObject:SetActive(true)
	local icon = self.DetailView.transform:Find("Icon"):GetComponent(typeof(CUITexture))
	local table = item.transform:Find("Table"):GetComponent(typeof(UITable))
	local nameLabel = self.DetailView.transform:Find("TopView/NameLabel"):GetComponent(typeof(UILabel))
	local lvLabel = self.DetailView.transform:Find("TopView/NameLabel/LvLabel"):GetComponent(typeof(UILabel))
	local messageLabel = item.transform:Find("Table/MessageLabel"):GetComponent(typeof(UILabel))
	local favorButton = item.transform:Find("Table/BottomView/FavorButton").gameObject
	
	local messageButton = item.transform:Find("Table/BottomView/MessageButton").gameObject
	local messageNumLabel = item.transform:Find("Table/BottomView/MessageButton/MessageNumLabel"):GetComponent(typeof(UILabel))
	local timeLabel = item.transform:Find("Table/BottomView/TimeLabel"):GetComponent(typeof(UILabel))

	icon:LoadNPCPortrait(CUICommonDef.GetPortraitName(class, gender, -1), false)
	nameLabel.text = name
	nameLabel.color = (CClientMainPlayer.Inst and playerId == CClientMainPlayer.Inst.Id) and NGUIText.ParseColor24("00ff60", 0) or Color.white
	lvLabel.text = SafeStringFormat3("Lv.%d",lv)
	lvLabel.color = hasFeiSheng and NGUIText.ParseColor24("fe7900", 0) or Color.white
	self:InitCommentFavorIcon(item, isMyFavor, favorNum)
	messageNumLabel.text = messageNum
	timeLabel.text = self:GetTimeText(createTime)
	messageLabel.overflowMethod = UILabelOverflow.ResizeHeight
	messageLabel.text = CChatLinkMgr.TranslateToNGUIText(msg)
	UIEventListener.Get(messageLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMessageLabelClick(messageLabel)
	end)
	UIEventListener.Get(favorButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnFavorBtnLabelClick(data)
	end)

	local tweenPositon = bottomView:GetComponent(typeof(TweenPosition))
	
	UIEventListener.Get(messageButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		self.m_ReplayPlayerInfo = {playerName = data.playerName}
	    self:OnDetailViewMessageButtonClick(tweenPositon)
	end)

	UIEventListener.Get(icon.gameObject).onClick = DelegateFactory.VoidDelegate(function (p) 
        self:OnIconbuttonClick(playerId, id, 0, msg)
    end)

	self:InitDetailViewMessages(table,tweenPositon,data)
	table:Reposition()
	if updatePos then
		self.DetailViewScrollView:ResetPosition()
	end
end

function LuaPlotDiscussionWnd:InitDetailViewMessages(uitable,tweenPositon,data)
	self.m_DetailViewMessages = {}
	local msgs= data.msgs 
	if data.favorPlayInfo and #data.favorPlayInfo > 0 then
		self:InitDetailViewStarInfoMessageLabel(uitable,data,tweenPositon)
	end
	--print(#msgs)
	for i = 1, #msgs do
		local t = msgs[i]
		self:InitDetailViewMessageTemplate(t,uitable,data,tweenPositon,i)
	end
end

function LuaPlotDiscussionWnd:InitDetailViewStarInfoMessageLabel(uitable,data,tweenPositon)
	local obj = NGUITools.AddChild(uitable.gameObject,self.DetailViewMessageBarTemplate.gameObject)
	obj.gameObject:SetActive(true)
	obj.transform:Find("FavorIcon").gameObject:SetActive(true)
	obj.transform:Find("CommentButton").gameObject:SetActive(false)
	obj.transform:Find("DeleteButton").gameObject:SetActive(false)
	local label = obj.transform:Find("Text"):GetComponent(typeof(UILabel))
	label.text = ""
	for i = 1, #data.favorPlayInfo do
		local t = data.favorPlayInfo[i]
		local playerName = CPersonalSpaceMgr.GeneratePlayerName(t.playerId, t.playerName)
		label.text = CChatLinkMgr.TranslateToNGUIText(label.text .. playerName)
	end
	Extensions.SetLocalPositionY(obj.transform:Find("Divide").transform, label.transform.localPosition.y - label.height - 10)
	UIEventListener.Get(label.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
		self:OnDetailViewMessageLabelClick(label,data,nil,tweenPositon)
	end)
	table.insert(self.m_DetailViewMessages,{obj = obj.gameObject, id = 0})
end

function LuaPlotDiscussionWnd:InitDetailViewMessageTemplate(t,uitable,data,tweenPositon,index)
	local playerName = CPersonalSpaceMgr.GeneratePlayerName(t.playerId, t.playerName)
	local replyPlayerName =  CPersonalSpaceMgr.GeneratePlayerName(t.replyPlayerId, t.replyPlayerName)
	local obj = NGUITools.AddChild(uitable.gameObject,self.DetailViewMessageBarTemplate.gameObject)
	obj.gameObject:SetActive(true)
	obj.transform:Find("FavorIcon").gameObject:SetActive(false)
	obj.transform:Find("CommentButton").gameObject:SetActive(index == 1)
	local msg = (t.replyPlayerId == 0) and 
		SafeStringFormat3(LocalString.GetString("%s%s"),playerName,t.msg) or
		SafeStringFormat3(LocalString.GetString("%s回复%s%s"),playerName,replyPlayerName,t.msg) 
	local msglabel = obj.transform:Find("Text"):GetComponent(typeof(UILabel))
	msglabel.text = CChatLinkMgr.TranslateToNGUIText(msg)
	Extensions.SetLocalPositionY(obj.transform:Find("Divide").transform, msglabel.transform.localPosition.y - msglabel.height - 10)
	local deleteBtn = obj.transform:Find("DeleteButton").gameObject
	deleteBtn:SetActive(CClientMainPlayer.Inst and t.playerId == CClientMainPlayer.Inst.Id)
	UIEventListener.Get(deleteBtn).onClick = DelegateFactory.VoidDelegate(function (go)
		self:OnDeleteDetailViewMessageButtonClick(self.m_CurCommentId, t)
	end)
	-- UIEventListener.Get(obj.transform:Find("CommentButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	-- 	self:OnCommentDetailViewMessageButtonClick(data, t,tweenPositon)
	-- end)
	UIEventListener.Get(msglabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
		self:OnDetailViewMessageLabelClick(msglabel,data,t,tweenPositon)
	end)
	table.insert(self.m_DetailViewMessages,{obj = obj.gameObject, id = t.id})
end

function LuaPlotDiscussionWnd:ClearDetailViewMessages()
	for i = 1,#self.m_DetailViewMessages do
		local t = self.m_DetailViewMessages[i]
		if t then
			local obj = t.obj
			if obj then
				GameObject.DestroyImmediate(obj.gameObject)
			end
		end
	end
end

--@endregion DetailView

--@region NewMessageView

function LuaPlotDiscussionWnd:InitNewMessageView()
	LuaPlotDiscussionMgr:RequestAllNewNotification()
end

function LuaPlotDiscussionWnd:OnSelectNewMessageViewAtRow(row)
	local t = self.m_NewCommentsData[row + 1]
	for index,data in pairs(LuaPlotDiscussionMgr.m_CommentsData) do
		--print(t.id , data.id)
		if t.id == data.id then
			self.NewMessageView.gameObject:SetActive(false)
			self.m_IsDetailViewRebackNewMessageView = true
			self:OnMessageButtonClick(data)
			break
		end
	end
end

function LuaPlotDiscussionWnd:InitNewMessageItem(item,index)
	local data = self.m_NewCommentsData[index + 1]
	local class = data.class
	local gender = data.gender
	local name = data.playerName
	local lv = data.lv
	local hasFeiSheng = data.hasFeiSheng
	local isFavor = data.isFavor
	local msg = data.msg
	local comment = data.comment

	local icon = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
	local table = item.transform:Find("Table"):GetComponent(typeof(UITable))
	local nameLabel = item.transform:Find("Table/TopView/NameLabel"):GetComponent(typeof(UILabel))
	local lvLabel = item.transform:Find("Table/TopView/NameLabel/LvLabel"):GetComponent(typeof(UILabel))
	local messageLabel = item.transform:Find("Table/MessageLabel"):GetComponent(typeof(UILabel))
	local favorView = item.transform:Find("Table/FavorButtonView").gameObject
	local discussionLabel = item.transform:Find("Table/DiscussionText"):GetComponent(typeof(UILabel))

	icon:LoadNPCPortrait(CUICommonDef.GetPortraitName(class, gender, -1), false)
	nameLabel.text = name
	lvLabel.text = SafeStringFormat3("Lv.%d",lv)
	lvLabel.color = hasFeiSheng and NGUIText.ParseColor24("fe7900", 0) or Color.white
	favorView.gameObject:SetActive(isFavor)
	messageLabel.gameObject:SetActive(not isFavor)
	messageLabel.text = CChatLinkMgr.TranslateToNGUIText(msg)
	discussionLabel.text = CChatLinkMgr.TranslateToNGUIText(comment)
	self:CompressMsg(messageLabel)
	self:CompressMsg(discussionLabel)
	table:Reposition()

	UIEventListener.Get(messageLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMessageLabelClick(messageLabel)
	end)
	UIEventListener.Get(discussionLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMessageLabelClick(discussionLabel)
	end)
	UIEventListener.Get(icon.gameObject).onClick = DelegateFactory.VoidDelegate(function (p) 
        self:OnIconbuttonClick(data.roleId, data.id, data.commentId, comment)
    end)
end

function LuaPlotDiscussionWnd:CompressMsg(label)
	local default, str = label:Wrap(label.text)
	local len = CommonDefs.StringIndexOf_String(str,"\n") - 10
	len = len > 0 and len or CommonDefs.StringLength(str)
    if not default then
        label.text = CChatLinkMgr.TranslateToNGUIText(CommonDefs.StringSubstring2(str, 0, len) .. "...")
    end
end

--@endregion NewMessageView

function LuaPlotDiscussionWnd:GetTimeText(createTime)
	local infoTime = createTime / 1000
    local nowTime = CServerTimeMgr.Inst.timeStamp
    local disTime = nowTime - infoTime
    if disTime < 0 then
        disTime = 0
    end

    if disTime < 60 then
        return LocalString.GetString("刚刚")
    elseif disTime < 3600 then
        return math.floor(disTime / 60) .. LocalString.GetString("分钟前")
    elseif disTime < 86400 --[[24 * 3600]] then
        return math.floor(disTime / 3600) .. LocalString.GetString("小时前")
    else
        return CPersonalSpaceMgr.GetTimeString(infoTime)
    end
end

--@region UIEvent

function LuaPlotDiscussionWnd:OnSelectSortsClick(selected)
	local textArray = {LocalString.GetString("按热度排序"),LocalString.GetString("按时间排序")}
    Extensions.SetLocalRotationZ(self.SortSprite.transform, selected and 180 or 0)
    if not selected then return end
    local t = {}
    for k,text in pairs(textArray) do
        local item=PopupMenuItemData(text,DelegateFactory.Action_int(function (idx)
            self.SortBtn.Text = textArray[idx + 1]
            self:OnChooseSortClicked(idx + 1)
        end),false,nil)
        table.insert(t, item)
    end
    local array=Table2Array(t,MakeArrayClass(PopupMenuItemData))
    CPopupMenuInfoMgr.ShowPopupMenu(array, self.SortBtn.m_Label.transform, AlignType.Bottom,1,DelegateFactory.Action(function()
        Extensions.SetLocalRotationZ(self.SortSprite.transform, 0)
    end),600,true,296)
end

function LuaPlotDiscussionWnd:OnChooseSortClicked(sortIndex)
	self.m_SortIndex = sortIndex
	LuaPlotDiscussionMgr:RequestCardCommentsList(LuaPlotDiscussionMgr.m_CardId, sortIndex == 1 and "hot" or "new", self.m_PageIndex, self.m_PageSize)
end

function LuaPlotDiscussionWnd:OnSendMsgButtonClick()
	LuaPlotDiscussionMgr:ShowSendPlotDiscussionWnd(self.m_SortIndex == 1 and "hot" or "new", self.m_PageSize)
end

function LuaPlotDiscussionWnd:OnEmotionButtonClick()
	CChatInputMgr.ShowChatInputWnd(CChatInputMgrEParentType.PersonalSpace, self.m_DefaultChatInputListener, self.EmotionButton.gameObject, EChatPanel.Undefined, 0, 0)
end

function LuaPlotDiscussionWnd:OnDetailViewSendMsgButtonClick()
	local sendText = self:GetSendMsg()

	LuaPlotDiscussionMgr:RequestAddCommentDetailMessage(self.m_CurCommentId, sendText, self.m_ReplayPlayerInfo.id)
end

function LuaPlotDiscussionWnd:OnDetailViewEmotionButtonClick()
	CChatInputMgr.ShowChatInputWnd(CChatInputMgrEParentType.PersonalSpace, self.m_DefaultChatInputListener, self.EmotionButton.gameObject, EChatPanel.Undefined, 0, 0)
end

function LuaPlotDiscussionWnd:OnSendDiscussionViewRebackButtonClick()
	self.SendDiscussionView.gameObject:SetActive(false)
	if self.m_TweenPositon then
		self.m_TweenPositon:PlayReverse()
	end
end

function LuaPlotDiscussionWnd:OnNewMessageTipClick()
	-- if not self.m_NewCommentsData then
	-- 	return
	-- end
	-- if #self.m_NewCommentsData == 1 then
	-- 	self:OnSelectNewMessageViewAtRow(0)
	-- 	return
	-- end
	self.NewMessageTip.gameObject:SetActive(false)
	self:InitNewMessageView()
end

function LuaPlotDiscussionWnd:OnDetailViewRebackButtonClick()
	self.CommentView.gameObject:SetActive(true)
	self.DetailView.gameObject:SetActive(false)
	self:ClearDetailViewMessages()
	if self.m_IsDetailViewRebackNewMessageView then
		self:InitNewMessageView()
	else
		self:InitCommentView()
	end
end

function LuaPlotDiscussionWnd:OnNewMessageViewRebackButtonClick()
	self.CommentView.gameObject:SetActive(true)
	self.NewMessageView.gameObject:SetActive(false)
	self:InitCommentView()
	self.m_IsDetailViewRebackNewMessageView = false
end

function LuaPlotDiscussionWnd:OnClearNewMessageButtonClick()
	LuaPlotDiscussionMgr.m_NewCommentsData = {}
	self:OnNewMessageViewRebackButtonClick()
	LuaPlotDiscussionMgr:RequestDelAllNewNotification()
end

function LuaPlotDiscussionWnd:OnMoreBtnLabelClick(moreBtnLabel, messageLabel, table, mainSprite)
	local isResetComment = moreBtnLabel.text == LocalString.GetString("全文")
	self:ResetComment(moreBtnLabel, messageLabel, table, mainSprite, true, not isResetComment, true)
end

function LuaPlotDiscussionWnd:OnFavorBtnLabelClick(data)
	local isMyFavor = data.isMyFavor
	local id = data.id
	self.m_CurCommentId = id
	for i = 1,#LuaPlotDiscussionMgr.m_CommentsData do
		local t = LuaPlotDiscussionMgr.m_CommentsData[i]
		if t.id == id then
			isMyFavor = data.isMyFavor
			break
		end
	end
	if isMyFavor then
		LuaPlotDiscussionMgr:CancelRequestGiveCommentThumbsUp(id)
	else
		LuaPlotDiscussionMgr:RequestGiveCommentThumbsUp(id)
	end
end

function LuaPlotDiscussionWnd:OnDeleteMyDiscussionButtonClick(commentsData)
	MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("JuQing_Comment_Delete"),DelegateFactory.Action(function()
		LuaPlotDiscussionMgr:RequestDeleteCardComment(commentsData.id)
	end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
end

function LuaPlotDiscussionWnd:OnMessageButtonClick(commentsData)
	local messageNum = commentsData.messageNum
	self.CommentView.gameObject:SetActive(false)
	self:InitDetailView(commentsData, true)
	LuaPlotDiscussionMgr:RequestCardCommentLikeUsers(commentsData.id)
	self.m_DetailViewPageIndex = 1
	self.m_DetailViewPageSize = 10
	LuaPlotDiscussionMgr:RequestCommentDetailMessages(commentsData.id, self.m_DetailViewPageIndex, self.m_DetailViewPageSize)
end

function LuaPlotDiscussionWnd:OnDetailViewMessageButtonClick(tweenPositon)
	if self.m_ReplayPlayerInfo then
		local input = self:GetCurStatusText()
		input.text = ""
		input.defaultText = SafeStringFormat3(LocalString.GetString("回复%s"),self.m_ReplayPlayerInfo.playerName)
	end
	self.m_TweenPositon = tweenPositon
	self.SendDiscussionView.gameObject:SetActive(true)
	tweenPositon.from = Vector3(tweenPositon.from.x,tweenPositon.transform.localPosition.y,0)
	tweenPositon.to = Vector3(tweenPositon.to.x,tweenPositon.transform.localPosition.y,0)
	tweenPositon:PlayForward()
end

function LuaPlotDiscussionWnd:OnDeleteDetailViewMessageButtonClick(commentId, t)
	--TODO、
	MessageWndManager.ShowConfirmMessage(LocalString.GetString('确定删除这条评论?'), 10, false, DelegateFactory.Action(function ()
		LuaPlotDiscussionMgr:RequestDeleteCommentDetailMessage(t.id,commentId)
	end), nil)
end

function LuaPlotDiscussionWnd:OnCommentDetailViewMessageButtonClick(commentsData, t,tweenPositon)
	self.m_ReplayPlayerInfo = t
	self:OnDetailViewMessageButtonClick(tweenPositon)
	--print(commentsData.id, t.id,t.playerName)

end

function LuaPlotDiscussionWnd:OnDetailViewMessageLabelClick(msglabel,data,t,tweenPositon)
	local index = msglabel:GetCharacterIndexAtPosition(UICamera.lastWorldPosition, true)
	if index == 0 then
		index = - 1
	end
	LuaPlotDiscussionMgr.m_ComplaintInfo = {notionId = data.id, commentId = t and t.id or 0, content = t and t.msg or data.msg}
	local url = msglabel:GetUrlAtCharacterIndex(index)
	if url then
		url = string.gsub(url, "PlayerClick", "PlotDiscussionPlayerClick")
	end
	if url ~= nil and CPersonalSpaceMgr.ProcessLinkClick(url) then
	elseif t then
		self:OnCommentDetailViewMessageButtonClick(data, t,tweenPositon)
	end
end

function LuaPlotDiscussionWnd:OnPageChange(page)
	self.m_PageIndex = page
	LuaPlotDiscussionMgr:RequestCardCommentsList(LuaPlotDiscussionMgr.m_CardId, self.m_SortIndex == 1 and "hot" or "new", self.m_PageIndex, self.m_PageSize)
end

function LuaPlotDiscussionWnd:OnCollectionButtonClick()
	local id = LuaPlotDiscussionMgr.m_CardId
	if LuaPlotDiscussionMgr.m_CardId2Like[id] then
        LuaPlotDiscussionMgr:RequestCancelGiveCardThumbsUp(id)
    else
        LuaPlotDiscussionMgr:RequestGiveCardThumbsUp(id)
    end
end

function LuaPlotDiscussionWnd:OnShowMoreDetailMessagesNodeClick()
	self.m_DetailViewPageSize = self.m_DetailViewPageSize + self.m_PageSize
	LuaPlotDiscussionMgr:RequestCommentDetailMessages(self.m_CurCommentId, self.m_DetailViewPageIndex, self.m_DetailViewPageSize)
end

function LuaPlotDiscussionWnd:OnMessageLabelClick(label)
	local url = label:GetUrlAtPosition(UICamera.lastWorldPosition)
	if url ~= nil then
		CChatLinkMgr.ProcessLinkClick(url, nil)
	end
end

function LuaPlotDiscussionWnd:OnIconbuttonClick(playerId, notionId, commentId, content)
	LuaPlotDiscussionMgr:ShowComplaintPopupMenu(playerId, notionId, commentId, content)
end
--@endregion UIEvent