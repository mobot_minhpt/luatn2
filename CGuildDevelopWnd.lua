-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CGuildDevelopWnd = import "L10.UI.CGuildDevelopWnd"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
CGuildDevelopWnd.m_OnClose_CS2LuaHook = function (this, go) 
    this:Close()
    if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst:IsInGuild() then
        CUIManager.ShowUI(CUIResources.GuildMainWnd)
    end
end
