local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local CTeamMgr               = import "L10.Game.CTeamMgr"
local CMessageTipMgr         = import "L10.UI.CMessageTipMgr"
local CTipParagraphItem      = import "L10.UI.CTipParagraphItem"
local Item_Item              = import "L10.Game.Item_Item"
local CServerTimeMgr         = import "L10.Game.CServerTimeMgr"
local CRankData              = import "L10.UI.CRankData"

LuaDuanWu2023PVEEnterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDuanWu2023PVEEnterWnd, "needTeam", "Team", UILabel)
RegistChildComponent(LuaDuanWu2023PVEEnterWnd, "needLevel", "Level", UILabel)
RegistChildComponent(LuaDuanWu2023PVEEnterWnd, "textTable", "TextTable", UITable)
RegistChildComponent(LuaDuanWu2023PVEEnterWnd, "textTemplate", "TextTemplate", GameObject)
RegistChildComponent(LuaDuanWu2023PVEEnterWnd, "rankButton", "RankButton", GameObject)
RegistChildComponent(LuaDuanWu2023PVEEnterWnd, "rankNum", "RankNum", UILabel)
RegistChildComponent(LuaDuanWu2023PVEEnterWnd, "affix", "Affix", UILabel)
RegistChildComponent(LuaDuanWu2023PVEEnterWnd, "baseReturnAward", "BaseReward", CQnReturnAwardTemplate)
RegistChildComponent(LuaDuanWu2023PVEEnterWnd, "advancedReturnAward", "AdvancedReward", CQnReturnAwardTemplate)
RegistChildComponent(LuaDuanWu2023PVEEnterWnd, "enterButton", "EnterButton", GameObject)
RegistChildComponent(LuaDuanWu2023PVEEnterWnd, "extraRewardDesc", "ExtraRewardDesc", UILabel)
--@endregion RegistChildComponent end

RegistClassMember(LuaDuanWu2023PVEEnterWnd, "tick")
RegistClassMember(LuaDuanWu2023PVEEnterWnd, "affixMessage")

function LuaDuanWu2023PVEEnterWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.rankButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRankButtonClick()
	end)

	UIEventListener.Get(self.affix.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAffixClick()
	end)

	UIEventListener.Get(self.enterButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnEnterButtonClick()
	end)
    --@endregion EventBind end
	self.textTemplate:SetActive(false)
end

function LuaDuanWu2023PVEEnterWnd:OnEnable()
	g_ScriptEvent:AddListener("TeamInfoChange", self, "UpdateNeedTeam")
	g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaDuanWu2023PVEEnterWnd:OnDisable()
	g_ScriptEvent:RemoveListener("TeamInfoChange", self, "UpdateNeedTeam")
	g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaDuanWu2023PVEEnterWnd:OnRankDataReady()
	local myInfo = CRankData.Inst.MainPlayerRankInfo
	local rankStr = myInfo.Rank > 0 and myInfo.Rank or LocalString.GetString("未上榜")
	self.rankNum.text = SafeStringFormat3(LocalString.GetString("当前排名：%s"), rankStr)
end

function LuaDuanWu2023PVEEnterWnd:Init()
	local setting = Duanwu2023_EndlessChallengeSetting.GetData()
	self.needTeam.text = SafeStringFormat3(LocalString.GetString("%d人组队"), setting.TeamSizeLimit)
	self.needLevel.text = SafeStringFormat3(LocalString.GetString("≥%d级"), setting.LevelLimit)
	self.needLevel.color = CClientMainPlayer.Inst.Level >= setting.LevelLimit and Color.white or NGUIText.ParseColor24("FF5050", 0)
	self:UpdateNeedTeam()
	self:InitRule()
	self:InitAffix()
	self:InitDailyReward()

	local rewardRank = {}
	for rank in string.gmatch(setting.RankRewardInfos, "(%d+)[%d,%s]*") do
		table.insert(rewardRank, tonumber(rank))
	end
	self.extraRewardDesc.text = SafeStringFormat3(LocalString.GetString("前%d/前%d名可得额外奖励"), rewardRank[1], rewardRank[2])
	self.rankNum.text = LocalString.GetString("当前排名：-")
	Gac2Gas.QueryRank(setting.RankId)
end

function LuaDuanWu2023PVEEnterWnd:UpdateNeedTeam()
	local isTeamOk = CTeamMgr.Inst.TotalMemebersCount >= Duanwu2023_EndlessChallengeSetting.GetData().TeamSizeLimit
	self.needTeam.color = isTeamOk and Color.white or NGUIText.ParseColor24("FF5050", 0)
end

function LuaDuanWu2023PVEEnterWnd:InitRule()
	Extensions.RemoveAllChildren(self.textTable.transform)
	local msg = g_MessageMgr:FormatMessage("DUANWU_2023_PVE_DESC")
	if System.String.IsNullOrEmpty(msg) then return end

    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)
    if info == nil then return end
    do
        local i = 0
        while i < info.paragraphs.Count do
            local paragraphGo = CUICommonDef.AddChild(self.textTable.gameObject, self.textTemplate)
            paragraphGo:SetActive(true)
			paragraphGo.transform:GetComponent(typeof(CTipParagraphItem)):Init(info.paragraphs[i], 4294967295)
            i = i + 1
        end
    end
    self.textTable:Reposition()
end

function LuaDuanWu2023PVEEnterWnd:InitDailyReward()
	local setting = Duanwu2023_EndlessChallengeSetting.GetData()
	local killBossNum, rewardTimesLimit = string.match(setting.BaseDailyReward, "(%d+);(%d+);%d+")
	self.baseReturnAward:Init(Item_Item.GetData(setting.BaseDailyRewardItemId), 0)
	local awarded = CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eEndlessChallengeBaseDailyAwardTimes) >= tonumber(rewardTimesLimit)
	self.baseReturnAward.transform:Find("Awarded").gameObject:SetActive(awarded)
	self.baseReturnAward.transform:Find("Target"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("击败%d"), killBossNum)

	killBossNum, rewardTimesLimit = string.match(setting.AdvancedDailyReward, "(%d+);(%d+);%d+")
	self.advancedReturnAward:Init(Item_Item.GetData(setting.AdvancedDailyRewardItemId), 0)
	awarded = CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eEndlessChallengeAdvancedDailyAwardTimes) >= tonumber(rewardTimesLimit)
	self.advancedReturnAward.transform:Find("Awarded").gameObject:SetActive(awarded)
	self.advancedReturnAward.transform:Find("Target"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("击败%d"), killBossNum)
end

function LuaDuanWu2023PVEEnterWnd:InitAffix()
	local date = CServerTimeMgr.Inst:GetZone8Time()
	local year = date.Year
	local month = date.Month
	local day = date.Day

	Duanwu2023_EndlessChallengeBossBuff.Foreach(function(id, data)
		local y, m, d = string.match(data.Date, "(%d+)-(%d+)-(%d+)")
		if tonumber(y) == year and tonumber(m) == month and tonumber(d) == day then
			self.affixMessage = data.Message
			local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(g_MessageMgr:FormatMessage(data.Message))
			self.affix.text = SafeStringFormat3("[%s]", info.title)
		end
	end)
end

--@region UIEvent

function LuaDuanWu2023PVEEnterWnd:OnRankButtonClick()
	CUIManager.ShowUI(CLuaUIResources.DuanWu2023PVERankWnd)
end

function LuaDuanWu2023PVEEnterWnd:OnAffixClick()
	if self.affixMessage then g_MessageMgr:ShowMessage(self.affixMessage) end
end

function LuaDuanWu2023PVEEnterWnd:OnEnterButtonClick()
	Gac2Gas.EnterEndlessChallengePlay()
end

--@endregion UIEvent
