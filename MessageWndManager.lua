local MessageWndManager = import "L10.UI.MessageWndManager"
local TeamConfirmType = import "L10.UI.MessageWndManager+TeamConfirmType"
local CTeamFollowMgr = import "L10.Game.CTeamFollowMgr"

MessageWndManager.m_hookShowTeamFollowConfirm = function(text, aliveDuration, onOKButtonClickDelegate, onCancelButtonClickDelegate, okLabel, cancelLabel)
    if okLabel == nil then
        okLabel = LocalString.GetString("确定")
    end  
    if cancelLabel == nil then
        cancelLabel = LocalString.GetString("取消")
    end
    local teamConfirmInfo = MessageWndManager.TeamConfirmInfo
    teamConfirmInfo.text = text
    teamConfirmInfo.aliveDuration = aliveDuration
    teamConfirmInfo.okLabel = okLabel
    teamConfirmInfo.cancelLabel = cancelLabel
    teamConfirmInfo.onOKButtonClick = onOKButtonClickDelegate
    teamConfirmInfo.onCancelButtonClick = onCancelButtonClickDelegate
    teamConfirmInfo.type = TeamConfirmType.FollowConfirm
    teamConfirmInfo.okBtnDefaultSelected = true
    MessageWndManager.TeamConfirmInfo = teamConfirmInfo
    if LuaCommonSideDialogMgr.m_IsOpen then
        LuaCommonSideDialogMgr:ClearSameKeyData("TeamFollowConfirm")
        LuaCommonSideDialogMgr:ShowDialog(text, "TeamFollowConfirm",
            LuaCommonSideDialogMgr:GetBtnInfo(cancelLabel, function()
                GenericDelegateInvoke(onCancelButtonClickDelegate, nil)
            end, false, true, 0, false, true), 
            LuaCommonSideDialogMgr:GetBtnInfo(okLabel, function()
                GenericDelegateInvoke(onOKButtonClickDelegate, nil)
            end, true, true, aliveDuration, true),
            LuaCommonSideDialogMgr:GetCheckBoxInfo(LocalString.GetString("以后自动响应跟随"), function ()
                return CTeamFollowMgr.Inst:GetAutoAcceptTeamFllowRequest()
            end, function ()
                CTeamFollowMgr.Inst:SetAutoAcceptTeamFollowRequest(not CTeamFollowMgr.Inst:GetAutoAcceptTeamFllowRequest())
            end, "OnAutoAcceptTeamFollowValueChanged"))
        return 
    end
    CUIManager.ShowUI("TeamConfirmWnd")
end

MessageWndManager.m_hookShowAddFriendConfirm = function(text, aliveDuration, onOKButtonClickDelegate, onCancelButtonClickDelegate, okLabel, cancelLabel)
    if okLabel == nil then
        okLabel = LocalString.GetString("确定")
    end  
    if cancelLabel == nil then
        cancelLabel = LocalString.GetString("取消")
    end
    local teamConfirmInfo = MessageWndManager.TeamConfirmInfo
    teamConfirmInfo.text = text
    teamConfirmInfo.aliveDuration = aliveDuration
    teamConfirmInfo.okLabel = okLabel
    teamConfirmInfo.cancelLabel = cancelLabel
    teamConfirmInfo.onOKButtonClick = onOKButtonClickDelegate
    teamConfirmInfo.onCancelButtonClick = onCancelButtonClickDelegate
    teamConfirmInfo.type = TeamConfirmType.AddFriendConfirm
    teamConfirmInfo.okBtnDefaultSelected = false
    MessageWndManager.TeamConfirmInfo = teamConfirmInfo
    if LuaCommonSideDialogMgr.m_IsOpen then
        LuaCommonSideDialogMgr:ShowDialog(text, "AddFriendConfirm",
            LuaCommonSideDialogMgr:GetBtnInfo(cancelLabel, function()
                GenericDelegateInvoke(onCancelButtonClickDelegate, nil)
            end, false, false, aliveDuration, true, true), 
            LuaCommonSideDialogMgr:GetBtnInfo(okLabel, function()
                GenericDelegateInvoke(onOKButtonClickDelegate, nil)
            end, true, true))
        return 
    end
    CUIManager.ShowUI("TeamConfirmWnd")
end

MessageWndManager.m_hookShowChangeLeaderConfirm = function(text, aliveDuration, onOKButtonClickDelegate, onCancelButtonClickDelegate, okLabel, cancelLabel)
    if okLabel == nil then
        okLabel = LocalString.GetString("确定")
    end  
    if cancelLabel == nil then
        cancelLabel = LocalString.GetString("取消")
    end
    local teamConfirmInfo = MessageWndManager.TeamConfirmInfo
    teamConfirmInfo.text = text
    teamConfirmInfo.aliveDuration = aliveDuration
    teamConfirmInfo.okLabel = okLabel
    teamConfirmInfo.cancelLabel = cancelLabel
    teamConfirmInfo.onOKButtonClick = onOKButtonClickDelegate
    teamConfirmInfo.onCancelButtonClick = onCancelButtonClickDelegate
    teamConfirmInfo.type = TeamConfirmType.RequestChangeLeaderConfirm
    teamConfirmInfo.okBtnDefaultSelected = true
    MessageWndManager.TeamConfirmInfo = teamConfirmInfo
    if LuaCommonSideDialogMgr.m_IsOpen then
        LuaCommonSideDialogMgr:ShowDialog(text, "ChangeLeaderConfirm",
            LuaCommonSideDialogMgr:GetBtnInfo(cancelLabel, function()
                GenericDelegateInvoke(onCancelButtonClickDelegate, nil)
            end, false, false, 0, false, true), 
            LuaCommonSideDialogMgr:GetBtnInfo(okLabel, function()
                GenericDelegateInvoke(onOKButtonClickDelegate, nil)
            end, true, true, aliveDuration, true))
        return 
    end
    CUIManager.ShowUI("TeamConfirmWnd")
end

MessageWndManager.m_hookShowYumaWithFriendConfirm = function(text, aliveDuration, onOKButtonClickDelegate, onCancelButtonClickDelegate, okLabel, cancelLabel)
    if okLabel == nil then
        okLabel = LocalString.GetString("同意")
    end  
    if cancelLabel == nil then
        cancelLabel = LocalString.GetString("拒绝")
    end
    local teamConfirmInfo = MessageWndManager.TeamConfirmInfo
    teamConfirmInfo.text = text
    teamConfirmInfo.aliveDuration = aliveDuration
    teamConfirmInfo.okLabel = okLabel
    teamConfirmInfo.cancelLabel = cancelLabel
    teamConfirmInfo.onOKButtonClick = onOKButtonClickDelegate
    teamConfirmInfo.onCancelButtonClick = onCancelButtonClickDelegate
    teamConfirmInfo.type = TeamConfirmType.RequestYumaWithFriendConfirm
    teamConfirmInfo.okBtnDefaultSelected = false
    MessageWndManager.TeamConfirmInfo = teamConfirmInfo
    if LuaCommonSideDialogMgr.m_IsOpen then
        LuaCommonSideDialogMgr:ShowDialog(text, "YumaWithFriendConfirm",
            LuaCommonSideDialogMgr:GetBtnInfo(cancelLabel, function()
                GenericDelegateInvoke(onCancelButtonClickDelegate, nil)
            end, false, false, aliveDuration, true, true), 
            LuaCommonSideDialogMgr:GetBtnInfo(okLabel, function()
                GenericDelegateInvoke(onOKButtonClickDelegate, nil)
            end, true, true))
        return 
    end
    CUIManager.ShowUI("TeamConfirmWnd")
end

MessageWndManager.m_hookShowDefaultConfirm = function(text, aliveDuration, onOKButtonClickDelegate, onCancelButtonClickDelegate, okLabel, cancelLabel)
    if okLabel == nil then
        okLabel = LocalString.GetString("同意")
    end  
    if cancelLabel == nil then
        cancelLabel = LocalString.GetString("拒绝")
    end
    local teamConfirmInfo = MessageWndManager.TeamConfirmInfo
    teamConfirmInfo.text = text
    teamConfirmInfo.aliveDuration = aliveDuration
    teamConfirmInfo.okLabel = okLabel
    teamConfirmInfo.cancelLabel = cancelLabel
    teamConfirmInfo.onOKButtonClick = onOKButtonClickDelegate
    teamConfirmInfo.onCancelButtonClick = onCancelButtonClickDelegate
    teamConfirmInfo.type = TeamConfirmType.Default
    teamConfirmInfo.okBtnDefaultSelected = false
    MessageWndManager.TeamConfirmInfo = teamConfirmInfo
    if LuaCommonSideDialogMgr.m_IsOpen then
        LuaCommonSideDialogMgr:ShowDialog(text, "DefaultConfirm",
            LuaCommonSideDialogMgr:GetBtnInfo(cancelLabel, function()
                GenericDelegateInvoke(onCancelButtonClickDelegate, nil)
            end, false, false, aliveDuration, true, true), 
            LuaCommonSideDialogMgr:GetBtnInfo(okLabel, function()
                GenericDelegateInvoke(onOKButtonClickDelegate, nil)
            end, true, true))
        return 
    end
    CUIManager.ShowUI("TeamConfirmWnd")
end