require("common/common_include")
local CUITexture=import "L10.UI.CUITexture"
local CCharacterModelTextureLoader=import "L10.UI.CCharacterModelTextureLoader"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CLoginMgr=import "L10.Game.CLoginMgr"
local CTeamMgr=import "L10.Game.CTeamMgr"
CLuaDuanWuDaZuoZhanResultWnd=class()
RegistClassMember(CLuaDuanWuDaZuoZhanResultWnd,"m_ShareButton")
RegistClassMember(CLuaDuanWuDaZuoZhanResultWnd,"m_RankButton")
RegistClassMember(CLuaDuanWuDaZuoZhanResultWnd,"m_ItemTemplate")
RegistClassMember(CLuaDuanWuDaZuoZhanResultWnd,"m_TeamScoreLabel")
RegistClassMember(CLuaDuanWuDaZuoZhanResultWnd,"m_TeamTimeLabel")
-- RegistClassMember(CLuaDuanWuDaZuoZhanResultWnd,"m_ModelLoader")

-- RegistClassMember(CLuaDuanWuDaZuoZhanResultWnd,"m_PlayerTextures")

function CLuaDuanWuDaZuoZhanResultWnd:Init()
    -- self.m_PlayerTextures={}
    self.m_ShareButton=FindChild(self.transform,"ShareButton").gameObject
    self.m_RankButton=FindChild(self.transform,"RankButton").gameObject
    self.m_ItemTemplate=FindChild(self.transform,"ItemTemplate").gameObject
    self.m_ItemTemplate:SetActive(false)
    -- self.m_ModelLoader = DefaultModelTextureLoader.Create()

    self.m_TeamScoreLabel=FindChild(self.transform,"TeamScoreLabel"):GetComponent(typeof(UILabel))
    self.m_TeamTimeLabel=FindChild(self.transform,"TeamTimeLabel"):GetComponent(typeof(UILabel))
    self.m_TeamScoreLabel.text=tostring(CLuaDuanWuDaZuoZhanMgr.m_TeamScore)

    local minute=math.floor(CLuaDuanWuDaZuoZhanMgr.m_PlayTimeCount/60)
    local second=CLuaDuanWuDaZuoZhanMgr.m_PlayTimeCount%60
    self.m_TeamTimeLabel.text=SafeStringFormat3("%02d:%02d",minute,second)

    UIEventListener.Get(self.m_ShareButton).onClick=LuaUtils.VoidDelegate(function(go)
		CUICommonDef.CaptureScreenAndShare()
    end)
    UIEventListener.Get(self.m_RankButton).onClick=LuaUtils.VoidDelegate(function(go)
        CUIManager.ShowUI("DuanWuDaZuoZhanRankWnd")
    end)

    local parent=FindChild(self.transform,"Grid").gameObject
    for i,v in ipairs(CLuaDuanWuDaZuoZhanMgr.m_PlayerInfos) do
        local go=NGUITools.AddChild(parent,self.m_ItemTemplate)
        self:InitItem(go.transform,v)
        go:SetActive(true)
    end
    parent:GetComponent(typeof(UIGrid)):Reposition()

    local playerInfoNode=FindChild(self.transform,"playerInfoNode")
    if CClientMainPlayer.Inst then
		playerInfoNode.gameObject:SetActive(true)
        playerInfoNode:Find("name"):GetComponent(typeof(UILabel)).text = CClientMainPlayer.Inst.Name
        local myGameServer = CLoginMgr.Inst:GetSelectedGameServer()
        playerInfoNode:Find("server"):GetComponent(typeof(UILabel)).text = myGameServer.name
        playerInfoNode:Find("ID"):GetComponent(typeof(UILabel)).text = CClientMainPlayer.Inst.Id
        playerInfoNode:Find("icon"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(CClientMainPlayer.Inst.Class, CClientMainPlayer.Inst.Gender,-1),false)
        playerInfoNode:Find("icon/lv"):GetComponent(typeof(UILabel)).text = CClientMainPlayer.Inst.Level
	else
		playerInfoNode.gameObject:SetActive(false)
    end
    
end

function CLuaDuanWuDaZuoZhanResultWnd:InitItem(tf,info)
    local loader=FindChild(tf,"Texture"):GetComponent(typeof(CCharacterModelTextureLoader))
    loader:Init(info.id,info.class,info.gender)
    FindChild(tf,"NameLabel"):GetComponent(typeof(UILabel)).text=info.name
    FindChild(tf,"ScoreLabel"):GetComponent(typeof(UILabel)).text=SafeStringFormat3(LocalString.GetString("[ACF8FF]粽子[-] %d"),info.score)
    local leaderMark=FindChild(tf,"LeaderIcon").gameObject
    if CTeamMgr.Inst:IsTeamLeader(info.id) then
        leaderMark:SetActive(true)
    else
        leaderMark:SetActive(false)
    end

    local bestMark=FindChild(tf,"Best").gameObject
    if info.id==CLuaDuanWuDaZuoZhanMgr.m_MvpPlayerId then
        bestMark:SetActive(true)
    else
        bestMark:SetActive(false)
    end
end

return CLuaDuanWuDaZuoZhanResultWnd
