local GameObject = import "UnityEngine.GameObject"
local CUICommonDef = import "L10.UI.CUICommonDef"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local Vector3 = import "UnityEngine.Vector3"
local CTopAndRightTipWnd=import "L10.UI.CTopAndRightTipWnd"
local CItemMgr = import "L10.Game.CItemMgr"

LuaDoubleOneQLDTopRightWnd = class()
RegistChildComponent(LuaDoubleOneQLDTopRightWnd,"rightBtn", GameObject)
RegistChildComponent(LuaDoubleOneQLDTopRightWnd,"hpLabel", UILabel)
RegistChildComponent(LuaDoubleOneQLDTopRightWnd,"itemNode1", GameObject)
RegistChildComponent(LuaDoubleOneQLDTopRightWnd,"itemNode2", GameObject)
RegistChildComponent(LuaDoubleOneQLDTopRightWnd,"itemNodeTemplate", GameObject)
RegistChildComponent(LuaDoubleOneQLDTopRightWnd,"itemNodeFather", GameObject)

RegistClassMember(LuaDoubleOneQLDTopRightWnd, "speItemTable")

function LuaDoubleOneQLDTopRightWnd:Close()
	--CUIManager.CloseUI(CLuaUIResources.)
end

function LuaDoubleOneQLDTopRightWnd:OnEnable()
	g_ScriptEvent:AddListener("DoubleOneQLDItemInfoRefresh", self, "InitItem")
	g_ScriptEvent:AddListener("DoubleOneQLDHpRefresh", self, "InitHp")
end

function LuaDoubleOneQLDTopRightWnd:OnDisable()
	g_ScriptEvent:RemoveListener("DoubleOneQLDItemInfoRefresh", self, "InitItem")
	g_ScriptEvent:RemoveListener("DoubleOneQLDHpRefresh", self, "InitHp")
end

function LuaDoubleOneQLDTopRightWnd:Init()
	--local onCloseClick = function(go)
    --self:Close()
	--end
	--CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)
	UIEventListener.Get(self.rightBtn).onClick=LuaUtils.VoidDelegate(function(go)
		LuaUtils.SetLocalRotation(self.rightBtn.transform,0,0,0)
		CTopAndRightTipWnd.showPackage = false
		CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
	end)

	self.itemNodeTemplate:SetActive(false)

	self:InitSpeItem()
	self:InitItem()
	self:InitHp()
end

function LuaDoubleOneQLDTopRightWnd:InitSpeItem()
	self.speItemTable = {21020455,21020454}
end

function LuaDoubleOneQLDTopRightWnd:InitItem()
	CUICommonDef.ClearTransform(self.itemNodeFather.transform)
	self.itemNode1.transform:Find('text'):GetComponent(typeof(UILabel)).text = 0
	self.itemNode2.transform:Find('text'):GetComponent(typeof(UILabel)).text = 0
	local itemWidth = -65
	local itemHeight = -65
	local count = 1
	if LuaDoubleOne2019Mgr.QLDItem then
		for i,v in ipairs(LuaDoubleOne2019Mgr.QLDItem) do
			local itemId = v[1]
			local item = CItemMgr.Inst:GetItemTemplate(itemId)
			if v[1] == self.speItemTable[1] then
				self.itemNode1.transform:Find('text'):GetComponent(typeof(UILabel)).text = v[2]
				self.itemNode1.transform:Find('node'):GetComponent(typeof(CUITexture)):LoadMaterial(item.Icon)
			elseif v[1] == self.speItemTable[2] then
				self.itemNode2.transform:Find('text'):GetComponent(typeof(UILabel)).text = v[2]
				self.itemNode2.transform:Find('node'):GetComponent(typeof(CUITexture)):LoadMaterial(item.Icon)
			else
				local go = NGUITools.AddChild(self.itemNodeFather, self.itemNodeTemplate)
				go:SetActive(true)
				if count <= 10 then
					go.transform.localPosition = Vector3((count-1)*itemWidth,0,0)
				else
					--go.transform.localPosition = Vector3((i-1)*itemWidth,itemHeight,0)
					go.transform.localPosition = Vector3((count-1)*itemWidth,0,0)
				end
				go:GetComponent(typeof(CUITexture)):LoadMaterial(item.Icon)
				count = count + 1
			end
		end
	end
end

function LuaDoubleOneQLDTopRightWnd:InitHp()
	if LuaDoubleOne2019Mgr.QLDHp then
		self.hpLabel.text = LuaDoubleOne2019Mgr.QLDHp
	else
		self.hpLabel.text = 0
	end
end

return LuaDoubleOneQLDTopRightWnd
