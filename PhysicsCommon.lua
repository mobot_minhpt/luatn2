PHYSICS_PI = 3.14159265358979323846264338327950288419716939937510
PHYSICS_DEG_TO_RAD = 1 / 180 * PHYSICS_PI
PHYSICS_RAD_TO_DEG = 1 / PHYSICS_PI * 180

function Vector2_Dot(x1, y1, x2, y2)
	return x1 * x2 + y1 * y2
end

function Vector2_CrossProduct(x1, y1, x2, y2)
	return x1 * y2 - y1 * x2
end

function Vector2_Distance(x1, y1, x2, y2)
	local dx = x2 - x1
	local dy = y2 - y1
	return math.sqrt(dx * dx + dy * dy)
end

function IntervalComparison(x, lowerBound, upperBound)
	if x < lowerBound then return -1 end
	if x > upperBound then return 1 end
	return 0
end

function Vector2_Normalize(x, y)
	local l = math.sqrt(x * x + y * y)
	if l == 0 then return 0, 0 end
	return x/l, y/l
end

function Mathf_Approximately(val1, val2)
	local diff = val2 - val1
	return -0.001 < diff and diff < 0.001
end

function Mathf_Lerp(a, b, t)
	return a + (b - a) * t
end

function Vector2_Lerp(a, b, t)
	local ret = { x=0, y=0 }
	ret.x = a.x + (b.x - a.x) * t
	ret.y = a.y + (b.y - b.y) * t
	return ret
end

-- @return component of vector perpendicular to a unit basis vector
-- (IMPORTANT NOTE: assumes "basis" has unit magnitude (length==1))
function Vector2_Perpendicular(vx, vy, basisX, basisY)
	-- dot(v, basis)
	local projection = vx * basisX + vy * basisY
	-- v - project(v, basis)
	return vx - projection * basisX, vy - projection * basisY
end

if IsRunningServerCode() then
	function GetObjByPhysicsObjId(poId)
		local obj = g_PhysicsMgr:GetObjByPhysicsObjId(poId)
		if obj and obj.m_EngineObject then
			return obj
		end
	end
else
	function GetObjByPhysicsObjId(poId)
		local obj =  g_PhysicsObjectHandlers[poId]
		return obj
	end
end

if IsRunningServerCode() then
	function GetPhysicsShapeDesignData(templateId)
		local objDesign = Physics_Object[templateId]
		if not objDesign then return end

		local shapeDesign = Physics_ObjectShape[objDesign.shape]
		if not shapeDesign then return end
		
		return shapeDesign
	end

	function GetPhysicsObjectDesignData(templateId)
		local objDesign = Physics_Object[templateId]
		if not objDesign then return end

		return objDesign
	end

	function GetNavalWarShipDesignData(templateId)
		local objDesign = NavalWar_Ship[templateId]
		if not objDesign then return end

		return objDesign
	end

	function GetPhysicsAIDesignData(steeringId)
		if not steeringId then return end
		local design = Physics_AI[steeringId]
		return design
	end

	function GetPhysicsSpeedCmdDesignData(speedCmdId)
		local design = speedCmdId and Physics_SpeedCmd[speedCmdId]
		return design
	end
else
	function GetPhysicsShapeDesignData(templateId)
		local objDesign = Physics_Object.GetData(templateId)
		if not objDesign then return end

		local shapeDesign = Physics_ObjectShape.GetData(objDesign.shape)
		if not shapeDesign then return end
		
		return shapeDesign
	end

	function GetPhysicsObjectDesignData(templateId)
		local objDesign = Physics_Object.GetData(templateId)
		if not objDesign then return end

		return objDesign
	end

	function GetNavalWarShipDesignData(templateId)
		local objDesign = NavalWar_Ship.GetData(templateId)
		if not objDesign then return end

		return objDesign
	end

	function GetPhysicsAIDesignData(steeringId)
		if not steeringId then return end
		local design = Physics_AI.GetData(steeringId)
		return design
	end

	function GetPhysicsSpeedCmdDesignData(speedCmdId)
		local design = speedCmdId and Physics_SpeedCmd.GetData(speedCmdId)
		return design
	end
end

function InitPhysicsObject_M_I(controller, shapeDesign)
	if shapeDesign and shapeDesign.shapeType == 1 then
		local height = shapeDesign.height
		local radius = shapeDesign.radius
		local rectMass = 2 * radius * height * shapeDesign.density
		local circleMass = PHYSICS_PI * radius * radius * shapeDesign.density
		controller.m_Mass = circleMass + rectMass
		local rectInertia = rectMass * (height * height + radius * radius) / 12.0
		local circleInertia = circleMass * (0.5 * radius * radius + height * height * 0.25)
		controller.m_I = rectInertia + circleInertia
	elseif shapeDesign and shapeDesign.shapeType == 2 then
		local radius = shapeDesign.radius
		controller.m_Mass = (PHYSICS_PI * radius * radius) * shapeDesign.density
		controller.m_I = controller.m_Mass * 0.5 * radius * radius
	else
		LogCallContext_lua()
		controller.m_Mass = 1
		controller.m_I = 1
	end
end
