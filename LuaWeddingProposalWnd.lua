local DelegateFactory = import "DelegateFactory"
local CWordFilterMgr  = import "L10.Game.CWordFilterMgr"

LuaWeddingProposalWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaWeddingProposalWnd, "propose")
RegistClassMember(LuaWeddingProposalWnd, "proposeName")
RegistClassMember(LuaWeddingProposalWnd, "proposeDesc")
RegistClassMember(LuaWeddingProposalWnd, "proposeOKButton")
RegistClassMember(LuaWeddingProposalWnd, "proposeCancelButton")

RegistClassMember(LuaWeddingProposalWnd, "refuse")
RegistClassMember(LuaWeddingProposalWnd, "refuseTitle")
RegistClassMember(LuaWeddingProposalWnd, "refuseInput")
RegistClassMember(LuaWeddingProposalWnd, "refuseConfirmButton")

function LuaWeddingProposalWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
    self:InitEventListener()
    self:InitActive()
end

-- 初始化UI组件
function LuaWeddingProposalWnd:InitUIComponents()
    self.propose = self.transform:Find("Propose")
    self.proposeName = self.propose:Find("Name"):GetComponent(typeof(UILabel))
    self.proposeDesc = self.propose:Find("Label"):GetComponent(typeof(UILabel))
    self.proposeOKButton = self.propose:Find("OkButton").gameObject
    self.proposeCancelButton = self.propose:Find("CancelButton").gameObject

    self.refuse = self.transform:Find("Refuse")
    self.refuseConfirmButton = self.refuse:Find("ConfirmButton").gameObject
    self.refuseTitle = self.refuse:Find("Title"):GetComponent(typeof(UILabel))
    self.refuseInput = self.refuse:Find("Input"):GetComponent(typeof(UIInput))
end

-- 初始化按键响应
function LuaWeddingProposalWnd:InitEventListener()
	UIEventListener.Get(self.proposeOKButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnProposeOKButtonClick()
	end)

    UIEventListener.Get(self.proposeCancelButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnProposeCancelButtonClick()
	end)

    UIEventListener.Get(self.refuseConfirmButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRefuseConfirmButtonClick()
	end)
end

function LuaWeddingProposalWnd:InitActive()
    self.propose.gameObject:SetActive(true)
    self.refuse.gameObject:SetActive(false)
end


function LuaWeddingProposalWnd:Init()
    local info = LuaWeddingIterationMgr.proposalInfo
    self.proposeName.text = info.playerName
    self.proposeDesc.text = g_MessageMgr:FormatMessage("QIUHUN_IFO", info.itemName)
end

--@region UIEvent

function LuaWeddingProposalWnd:OnProposeOKButtonClick()
    if not CClientMainPlayer.Inst then return end
    local info = LuaWeddingIterationMgr.proposalInfo
    Gac2Gas.PlayerAcceptEngage(info.playerId, info.playerName, info.itemName)
    CUIManager.CloseUI(CLuaUIResources.WeddingProposalWnd)
end

function LuaWeddingProposalWnd:OnProposeCancelButtonClick()
    self.propose.gameObject:SetActive(false)
    self.refuse.gameObject:SetActive(true)
    self.refuseTitle.text = g_MessageMgr:FormatMessage("REFUSE_QIUHUN_IFO", LuaWeddingIterationMgr.proposalInfo.playerName)
end

function LuaWeddingProposalWnd:OnRefuseConfirmButtonClick()
    if not CClientMainPlayer.Inst then return end

    local ret = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(self.refuseInput.value, true)
    local msg = ret.msg

    if msg == nil or ret.shouldBeIgnore then
        return
    end

    local info = LuaWeddingIterationMgr.proposalInfo
    Gac2Gas.PlayerRefuseEngage(info.playerId, info.playerName, info.itemName, msg)
    CUIManager.CloseUI(CLuaUIResources.WeddingProposalWnd)
end

--@endregion UIEvent

