-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CDaDiZiMgr = import "L10.Game.CDaDiZiMgr"
local CDaDiZiWatchItem = import "L10.UI.CDaDiZiWatchItem"
local CDaDiZiWatchWnd = import "L10.UI.CDaDiZiWatchWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local EnumClass = import "L10.Game.EnumClass"
local Int32 = import "System.Int32"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local Profession = import "L10.Game.Profession"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CDaDiZiWatchWnd.m_Awake_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)
    UIEventListener.Get(this.watchBtn).onClick = MakeDelegateFromCSFunction(this.OnClickWatchButton, VoidDelegate, this)
    UIEventListener.Get(this.clsChangeBtn).onClick = MakeDelegateFromCSFunction(this.OnClickClassChangeButton, VoidDelegate, this)
    UIEventListener.Get(this.refreshBtn).onClick = MakeDelegateFromCSFunction(this.OnClickRefreshButton, VoidDelegate, this)

    this.tableView.m_DataSource = this
end
CDaDiZiWatchWnd.m_OnClickClassChangeButton_CS2LuaHook = function (this, go) 
    local items = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
    do
        local i = 0
        while i < this.clsList.Count do
            local item = PopupMenuItemData(Profession.GetFullName(CommonDefs.ConvertIntToEnum(typeof(EnumClass), this.clsList[i])), MakeDelegateFromCSFunction(this.OnChangeClass, MakeGenericClass(Action1, Int32), this), false, nil)
            CommonDefs.ListAdd(items, typeof(PopupMenuItemData), item)
            i = i + 1
        end
    end

    CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(items), go.transform, CPopupMenuInfoMgr.AlignType.Bottom)
end
CDaDiZiWatchWnd.m_OnChangeClass_CS2LuaHook = function (this, index) 
    if index < this.clsList.Count then
        this.watchClass = this.clsList[index]
        this.curClsLabel.text = Profession.GetFullName(CommonDefs.ConvertIntToEnum(typeof(EnumClass), this.watchClass))

        Gac2Gas.QueryShouXiDaDiZiBiWuStageDetails(this.watchClass, 0)
    end
end
CDaDiZiWatchWnd.m_OnClickWatchButton_CS2LuaHook = function (this, go) 
    if this.mData ~= nil then
        local index = this.tableView.currentSelectRow
        if index >= 0 and index < this.mData.Count then
            --观战rpc
            local playid = this.mData[index].playId
            Gac2Gas.RequestWatchShouXiDaDiZiBiWu(playid)

            this:Close()
            CUIManager.CloseUI(CUIResources.DaDiZiBattleMatchWnd)
        end
    end
end
CDaDiZiWatchWnd.m_Init_CS2LuaHook = function (this) 
    this.watchClass = EnumToInt(CDaDiZiMgr.Inst.cls)
    if this.watchClass == 0 then
        this.watchClass = 1
    end

    this:InitClsList()

    this.curClsLabel.text = Profession.GetFullName(CommonDefs.ConvertIntToEnum(typeof(EnumClass), this.watchClass))
    this.stageDescLabel.text = ""
    --CDaDiZiMgr.Inst.GetStageDesc(CDaDiZiMgr.Inst.stage);
    Gac2Gas.QueryShouXiDaDiZiBiWuStageDetails(this.watchClass, 0)
end
CDaDiZiWatchWnd.m_InitClsList_CS2LuaHook = function (this) 
    CommonDefs.ListClear(this.clsList)
    if CClientMainPlayer.Inst ~= nil then
        CommonDefs.ListAdd(this.clsList, typeof(UInt32), EnumToInt(CClientMainPlayer.Inst.Class))
    end
    local maxClass = EnumToInt(EnumClass.Undefined)-1
    for i = 1, maxClass do
        local cls = i
        if not CommonDefs.ListContains(this.clsList, typeof(UInt32), cls) then
            CommonDefs.ListAdd(this.clsList, typeof(UInt32), cls)
        end
    end
end
CDaDiZiWatchWnd.m_OnReplyShouXiDaDiZiBiWuStageDetails_CS2LuaHook = function (this, zhiye, currentStage, stage, data) 
    this.mData = data
    this.stageDescLabel.text = CDaDiZiMgr.Inst:GetStageDesc(currentStage)

    this.tableView:ReloadData(true, false)
    this.tableView:SetSelectRow(0, true)
end
CDaDiZiWatchWnd.m_NumberOfRows_CS2LuaHook = function (this, view) 
    if this.mData ~= nil then
        return this.mData.Count
    end
    return 0
end
CDaDiZiWatchWnd.m_ItemAt_CS2LuaHook = function (this, view, row) 
    if row < this.mData.Count then
        local cmp = TypeAs(view:GetFromPool(0), typeof(CDaDiZiWatchItem))
        if cmp ~= nil then
            cmp:Init(this.mData[row], row)
            return cmp
        end
    end
    return nil
end
