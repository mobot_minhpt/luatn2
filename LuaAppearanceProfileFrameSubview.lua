local CUITexture = import "L10.UI.CUITexture"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local EnumGender = import "L10.Game.EnumGender"
local UISprite = import "UISprite"
local LuaTweenUtils = import "LuaTweenUtils"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"

LuaAppearanceProfileFrameSubview = class()

RegistChildComponent(LuaAppearanceProfileFrameSubview,"m_ProfileFrameItem", "CommonItemCell", GameObject)
RegistChildComponent(LuaAppearanceProfileFrameSubview,"m_ItemDisplay", "AppearanceCommonItemDisplayView", CCommonLuaScript)
RegistChildComponent(LuaAppearanceProfileFrameSubview,"m_MainPlayerProfileFrame", "MainPlayerProfileFrame", GameObject)
RegistChildComponent(LuaAppearanceProfileFrameSubview,"m_ContentGrid", "ContentGrid", UIGrid)
RegistChildComponent(LuaAppearanceProfileFrameSubview,"m_ContentScrollView", "ContentScrollView", UIScrollView)

RegistClassMember(LuaAppearanceProfileFrameSubview, "m_AllFrames")
RegistClassMember(LuaAppearanceProfileFrameSubview, "m_SelectedDataId")

function LuaAppearanceProfileFrameSubview:Awake()
end

function LuaAppearanceProfileFrameSubview:Init()
    self:PlayAppearAnimation()
    self:SetDefaultSelection()
    self:LoadData()
end

function LuaAppearanceProfileFrameSubview:PlayAppearAnimation()
    self.m_MainPlayerProfileFrame:SetActive(true)
    LuaTweenUtils.TweenAlpha(self.m_MainPlayerProfileFrame.transform:GetComponent(typeof(UIWidget)), 0, 1, 0.8)                                                                           
    local portraitTexture = self.m_MainPlayerProfileFrame.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    portraitTexture:LoadNPCPortrait(CClientMainPlayer.Inst and CUICommonDef.GetDieKeSpecialPortraitName(CClientMainPlayer.Inst) or "")
end

function LuaAppearanceProfileFrameSubview:LoadData()
    self.m_AllFrames = LuaAppearancePreviewMgr:GetAllProfileFrameInfo(false)

    Extensions.RemoveAllChildren(self.m_ContentGrid.transform)
    self.m_ContentGrid.gameObject:SetActive(true)

    for i = 1, # self.m_AllFrames do
        local child = CUICommonDef.AddChild(self.m_ContentGrid.gameObject, self.m_ProfileFrameItem)
        child:SetActive(true)
        self:InitItem(child, self.m_AllFrames[i])
    end
    self.m_ContentGrid:Reposition()
    self.m_ContentScrollView:ResetPosition()
    self:InitSelection()
end

function LuaAppearanceProfileFrameSubview:SetDefaultSelection()
    self.m_SelectedDataId = LuaAppearancePreviewMgr:GetCurrentInUseProfileFrame()
end

function LuaAppearanceProfileFrameSubview:InitSelection()
    if self.m_SelectedDataId == 0 then
        self:OnItemClick(nil)
        return
    end

    local childCount = self.m_ContentGrid.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentGrid.transform:GetChild(i).gameObject
        local appearanceData = self.m_AllFrames[i+1]
        if appearanceData.id == self.m_SelectedDataId then
            self:OnItemClick(childGo)
            break
        end
    end
end

function LuaAppearanceProfileFrameSubview:InitItem(itemGo, appearanceData)
    local iconTexture = itemGo.transform:GetComponent(typeof(CUITexture))
    local disableGo = itemGo.transform:Find("Disabled").gameObject
    local lockedGo = itemGo.transform:Find("Locked").gameObject
    local cornerGo = itemGo.transform:Find("Corner").gameObject
    local selectedGo = itemGo.transform:Find("Selected").gameObject
    iconTexture:LoadMaterial(appearanceData.icon)
    
    if CClientMainPlayer.Inst then
        lockedGo:SetActive(not LuaAppearancePreviewMgr:MainPlayerHasProfileFrame(appearanceData.id))
        cornerGo:SetActive(LuaAppearancePreviewMgr:IsCurrentInUseProfileFrame(appearanceData.id))
    else
        lockedGo:SetActive(false)
        cornerGo:SetActive(false)
    end
    selectedGo:SetActive(self.m_SelectedDataId and self.m_SelectedDataId==appearanceData.id)
    disableGo:SetActive(lockedGo.activeSelf or LuaAppearancePreviewMgr:IsProfileFrameExpired(appearanceData.id))
    UIEventListener.Get(itemGo).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnItemClick(go)
    end)
end

function LuaAppearanceProfileFrameSubview:OnItemClick(go)
    local childCount = self.m_ContentGrid.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentGrid.transform:GetChild(i).gameObject
        if childGo == go then
            childGo.transform:Find("Selected").gameObject:SetActive(true)
            self.m_SelectedDataId = self.m_AllFrames[i+1].id
        else
            childGo.transform:Find("Selected").gameObject:SetActive(false)
        end
    end
    self:UpdateItemDisplay()
end

function LuaAppearanceProfileFrameSubview:UpdateItemDisplay()
    self.m_ItemDisplay.gameObject:SetActive(true)
    local frameTexture = self.m_MainPlayerProfileFrame.transform:Find("Icon/Frame"):GetComponent(typeof(CUITexture))
    local frameNameLabel = self.m_MainPlayerProfileFrame.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local frameId = self.m_SelectedDataId and self.m_SelectedDataId or 0
    if frameId==0 then
        frameTexture:Clear()
        frameNameLabel.text = ""
        self.m_ItemDisplay:Clear()
        return
    end
    local appearanceData = nil
    for i=1,#self.m_AllFrames do
        if self.m_AllFrames[i].id == frameId then
            appearanceData = self.m_AllFrames[i]
            break
        end
    end
    frameTexture:LoadMaterial(appearanceData.icon)
    frameNameLabel.text = appearanceData.name
    
    local icon = appearanceData.icon
    local disabled = true
    local locked = true
    local buttonTbl = {}
    local exist = LuaAppearancePreviewMgr:MainPlayerHasProfileFrame(appearanceData.id)
    local inUse = LuaAppearancePreviewMgr:IsCurrentInUseProfileFrame(appearanceData.id)
    local expired = exist and LuaAppearancePreviewMgr:IsProfileFrameExpired(appearanceData.id) or false
    locked = not exist
    disabled = not exist
    if exist then
        if inUse then
            table.insert(buttonTbl, {text=LocalString.GetString("脱下"), isYellow=false, action=function(go) self:OnRemoveButtonClick() end})
        elseif expired then
            table.insert(buttonTbl, {text=LocalString.GetString("丢弃"), isYellow=false, action=function(go) self:OnDiscardButtonClick() end})
        else
            table.insert(buttonTbl, {text=LocalString.GetString("更换"), isYellow=false, action=function(go) self:OnApplyButtonClick() end})
        end
    else
        table.insert(buttonTbl, {text=LocalString.GetString("获取"), isYellow=true, action=function(go) self:OnMoreButtonClick(go) end})
    end

    if appearanceData.isPermanent then
        self.m_ItemDisplay:Init(icon, appearanceData.name, LuaAppearancePreviewMgr:GetProfileFrameConditionText(appearanceData.id), SafeStringFormat3(LocalString.GetString("风尚度+%d"), appearanceData.fashionability), true, disabled, locked, buttonTbl)
    else
        self.m_ItemDisplay:Init(icon, appearanceData.name, LuaAppearancePreviewMgr:GetProfileFrameConditionText(appearanceData.id), "", true, disabled, locked, buttonTbl)
    end
end

function LuaAppearanceProfileFrameSubview:OnApplyButtonClick()
    local exist = LuaAppearancePreviewMgr:MainPlayerHasProfileFrame(self.m_SelectedDataId)
    if exist then
        LuaAppearancePreviewMgr:RequestSetProfileFrame(self.m_SelectedDataId)
    end
end

function LuaAppearanceProfileFrameSubview:OnDiscardButtonClick()
    local exist = LuaAppearancePreviewMgr:MainPlayerHasProfileFrame(self.m_SelectedDataId)
    if exist then
        LuaAppearancePreviewMgr:RequestDiscardProfileFrame(self.m_SelectedDataId)
    end
end

function LuaAppearanceProfileFrameSubview:OnRemoveButtonClick()
    LuaAppearancePreviewMgr:RequestSetProfileFrame(0)
end

function LuaAppearanceProfileFrameSubview:OnMoreButtonClick(go)
    local frameId = self.m_SelectedDataId and self.m_SelectedDataId or 0
    local appearanceData = nil
    for i=1,#self.m_AllFrames do
        if self.m_AllFrames[i].id == frameId then
            appearanceData = self.m_AllFrames[i]
            break
        end
    end
    if appearanceData then
        LuaItemAccessListMgr:ShowItemAccessInfoAtLeft(appearanceData.itemGetId, true, go.transform)
    end
end

function  LuaAppearanceProfileFrameSubview:OnEnable()
    g_ScriptEvent:AddListener("UnlockMainPlayerProfileFrame", self, "OnUnlockMainPlayerProfileFrame")
    g_ScriptEvent:AddListener("UpdateMainPlayerProfileInfo", self, "OnUpdateMainPlayerProfileInfo")
end

function  LuaAppearanceProfileFrameSubview:OnDisable()
    g_ScriptEvent:RemoveListener("UnlockMainPlayerProfileFrame", self, "OnUnlockMainPlayerProfileFrame")
    g_ScriptEvent:RemoveListener("UpdateMainPlayerProfileInfo", self, "OnUpdateMainPlayerProfileInfo")
end

function LuaAppearanceProfileFrameSubview:OnUnlockMainPlayerProfileFrame(args)
    self:LoadData()
end

function LuaAppearanceProfileFrameSubview:OnUpdateMainPlayerProfileInfo()
    self:SetDefaultSelection()
    self:LoadData()
end
