-- Auto Generated!!
local BiWuSelectPlayerResult = import "L10.Game.BiWuSelectPlayerResult"
local BiWuStageTeamMemberInfo = import "L10.Game.BiWuStageTeamMemberInfo"
local CBiWuDaHuiMgr = import "L10.Game.CBiWuDaHuiMgr"
local CBWDHSelectPlayerItem = import "L10.UI.CBWDHSelectPlayerItem"
local CBWDHSelectPlayerWnd = import "L10.UI.CBWDHSelectPlayerWnd"
local CMiddleNoticeMgr = import "L10.UI.CMiddleNoticeMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CQMJJMgr = import "L10.Game.CQMJJMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local Gac2Gas = import "L10.Game.Gac2Gas"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
local UIEventListener = import "UIEventListener"
local UInt64 = import "System.UInt64"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CBWDHSelectPlayerWnd.m_Awake_CS2LuaHook = function (this) 
    this.resultGo:SetActive(false)
    UIEventListener.Get(this.closeButton).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)
    UIEventListener.Get(this.helpBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        g_MessageMgr:ShowMessage("BIWU_COMPETING_ORDERING_INTERFACE")
    end)
    UIEventListener.Get(this.resetBtn).onClick = MakeDelegateFromCSFunction(this.ResetSelection, VoidDelegate, this)
    UIEventListener.Get(this.confirmBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        local select = CreateFromClass(MakeGenericClass(List, Object))
        local cmps = CommonDefs.GetComponentsInChildren_Component_Type(this.leftGrid, typeof(CBWDHSelectPlayerItem))
        --按选择排序
        CommonDefs.ArraySort_CBWDHSelectPlayerItem_Array(cmps, DelegateFactory.Comparison_CBWDHSelectPlayerItem(function (p1, p2) 
            local sel1 = p1:GetSelection()
            local sel2 = p2:GetSelection()
            if sel1 > sel2 then
                return - 1
            elseif sel1 < sel2 then
                return 1
            else
                return 0
            end
        end))
        local count = 0
        do
            local i = 0
            while i < cmps.Length do
                if cmps[i]:GetSelection() > 0 then
                    CommonDefs.ListAdd(select, typeof(Int32), cmps[i]:GetSelection())
                    CommonDefs.ListAdd(select, typeof(UInt64), cmps[i].playerId)
                    count = count + 1
                end
                i = i + 1
            end
        end
        if (count < cmps.Length and count == 3) or count == cmps.Length then
            if CBiWuDaHuiMgr.Inst.inBiWu or CBiWuDaHuiMgr.Inst.inBiWuPrepare then
                Gac2Gas.UpdateBiWuSelectPlayerIdList(LuaBiWuDaHuiMgr.myTeamId, MsgPackImpl.pack(select))
            elseif CQMJJMgr.Inst.inQMJJ or CQMJJMgr.Inst.inQMJJPrepare then
                Gac2Gas.UpdateQMJJSelectPlayerIdList(LuaBiWuDaHuiMgr.myTeamId, MsgPackImpl.pack(select))
            end
        else
            CMiddleNoticeMgr.ShowMiddleNotice(System.String.Format(LocalString.GetString("你需要选择{0}人"), cmps.Length <= 3 and cmps.Length or 3), 1)
        end
    end)
end
CBWDHSelectPlayerWnd.m_ResetSelection_CS2LuaHook = function (this, go) 
    local cmps = CommonDefs.GetComponentsInChildren_Component_Type(this.leftGrid, typeof(CBWDHSelectPlayerItem))
    local count = math.min(3, cmps.Length)
    do
        local i = 0
        while i < count do
            cmps[i]:SetSection(i)
            i = i + 1
        end
    end
end
CBWDHSelectPlayerWnd.m_InitTeamInfo_CS2LuaHook = function (teamId, list, info) 
    LuaBiWuDaHuiMgr.myTeamId = teamId
    --记录队长id
    CommonDefs.ListIterate(info, DelegateFactory.Action_object(function (___value) 
        local item = ___value
        if item.teamId == LuaBiWuDaHuiMgr.myTeamId then
            LuaBiWuDaHuiMgr.myTeamLeaderId = math.floor(item.leaderId)
        end
    end))
    CBWDHSelectPlayerWnd.myTeamInfoList = CreateFromClass(MakeGenericClass(List, BiWuStageTeamMemberInfo))
    CBWDHSelectPlayerWnd.opponentTeamInfoList = CreateFromClass(MakeGenericClass(List, BiWuStageTeamMemberInfo))
    CommonDefs.ListIterate(list, DelegateFactory.Action_object(function (___value) 
        local item = ___value
        if item.teamId == LuaBiWuDaHuiMgr.myTeamId then
            CommonDefs.ListAdd(CBWDHSelectPlayerWnd.myTeamInfoList, typeof(BiWuStageTeamMemberInfo), item)
        else
            CommonDefs.ListAdd(CBWDHSelectPlayerWnd.opponentTeamInfoList, typeof(BiWuStageTeamMemberInfo), item)
        end
    end))
    local comparer = DelegateFactory.Comparison_BiWuStageTeamMemberInfo(function (p1, p2) 
        if p1.equipScore > p2.equipScore then
            return - 1
        elseif p1.equipScore < p2.equipScore then
            return 1
        else
            return 0
        end
    end)

    CommonDefs.ListSort1(CBWDHSelectPlayerWnd.myTeamInfoList, typeof(BiWuStageTeamMemberInfo), comparer)
    CommonDefs.ListSort1(CBWDHSelectPlayerWnd.opponentTeamInfoList, typeof(BiWuStageTeamMemberInfo), comparer)

    CBWDHSelectPlayerWnd.teamInfoList = info
end
CBWDHSelectPlayerWnd.m_Init_CS2LuaHook = function (this) 

    CUICommonDef.SetActive(this.resetBtn, true, true)
    CUICommonDef.SetActive(this.confirmBtn, true, true)
    this.start = true

    this.myTeamNameLabel.text = ""
    this.opponentTeamNameLabel.text = ""
    if CBWDHSelectPlayerWnd.teamInfoList.Count == 2 then
        if CBWDHSelectPlayerWnd.teamInfoList[0].teamId == LuaBiWuDaHuiMgr.myTeamId then
            this.myTeamNameLabel.text = CBWDHSelectPlayerWnd.teamInfoList[0].teamName 
            this.opponentTeamNameLabel.text = CBWDHSelectPlayerWnd.teamInfoList[1].teamName
        else
            this.myTeamNameLabel.text = CBWDHSelectPlayerWnd.teamInfoList[1].teamName
            this.opponentTeamNameLabel.text = CBWDHSelectPlayerWnd.teamInfoList[0].teamName
        end
    end
    if CBiWuDaHuiMgr.Inst.selectionList.Count > 0 then
        this:OnUpdateBiWuSelectPlayerResult(CBiWuDaHuiMgr.Inst.selectionList)
        CommonDefs.ListClear(CBiWuDaHuiMgr.Inst.selectionList)
    elseif CBiWuDaHuiMgr.Inst.updateSelectionList.Count > 0 then
        this:OnUpdateBiWuTeamSelectResult(CBiWuDaHuiMgr.Inst.updateSelectionList)
        CommonDefs.ListClear(CBiWuDaHuiMgr.Inst.updateSelectionList)
    else
        if not LuaBiWuDaHuiMgr.IsLeader() then
            CUICommonDef.SetActive(this.resetBtn, false, true)
            CUICommonDef.SetActive(this.confirmBtn, false, true)
        end

        Extensions.RemoveAllChildren(this.leftGrid.transform)
        Extensions.RemoveAllChildren(this.rightGrid.transform)
        if CBWDHSelectPlayerWnd.myTeamInfoList == nil or CBWDHSelectPlayerWnd.opponentTeamInfoList == nil then
            return
        end
        do
            local i = 0
            while i < CBWDHSelectPlayerWnd.myTeamInfoList.Count do
                local item = this.pool:GetFromPool(0)
                item.transform.parent = this.leftGrid.transform
                item.transform.localScale = Vector3.one
                item.gameObject:SetActive(true)
                CommonDefs.GetComponent_GameObject_Type(item, typeof(CBWDHSelectPlayerItem)):Init(CBWDHSelectPlayerWnd.myTeamInfoList[i], i)
                i = i + 1
            end
        end
        do
            local i = 0
            while i < CBWDHSelectPlayerWnd.opponentTeamInfoList.Count do
                local item = this.pool:GetFromPool(1)
                item.transform.parent = this.rightGrid.transform
                item.transform.localScale = Vector3.one
                item.gameObject:SetActive(true)
                CommonDefs.GetComponent_GameObject_Type(item, typeof(CBWDHSelectPlayerItem)):Init(CBWDHSelectPlayerWnd.opponentTeamInfoList[i], i)
                i = i + 1
            end
        end

        this.leftGrid:Reposition()
        this.rightGrid:Reposition()
        --默认选择
        this:ResetSelection(nil)
    end
end
CBWDHSelectPlayerWnd.m_Update_CS2LuaHook = function (this) 
    if this.start then
        local serverTime = CServerTimeMgr.Inst:GetZone8Time()
        if CommonDefs.op_LessThanOrEqual_DateTime_DateTime(this.nextTickTime, serverTime) then
            if this.elapseSecond >= CBWDHSelectPlayerWnd.count then
                this.start = false
            end
            this.countdownLabel.text = System.String.Format(LocalString.GetString("倒计时{0}s"), CBWDHSelectPlayerWnd.count - this.elapseSecond)

            this.elapseSecond = this.elapseSecond + 1
            this.nextTickTime = CBWDHSelectPlayerWnd.startTime:AddSeconds(this.elapseSecond)
        end
    end
end
CBWDHSelectPlayerWnd.m_OnUpdateBiWuSelectPlayerResult_CS2LuaHook = function (this, result) 
    if result == nil then
        return
    end
    CUICommonDef.SetActive(this.resetBtn, false, true)
    CUICommonDef.SetActive(this.confirmBtn, false, true)

    this.phaseGo:SetActive(false)
    this.resultGo:SetActive(true)

    CUICommonDef.ClearTransform(this.leftGrid.transform)
    CUICommonDef.ClearTransform(this.rightGrid.transform)

    if CBWDHSelectPlayerWnd.myTeamInfoList == nil or CBWDHSelectPlayerWnd.opponentTeamInfoList == nil then
        return
    end

    do
        local i = 0
        while i < CBWDHSelectPlayerWnd.myTeamInfoList.Count do
            local item = this.pool:GetFromPool(2)
            item.transform.parent = this.leftGrid.transform
            item.transform.localScale = Vector3.one
            item.gameObject:SetActive(true)
            local info = CBWDHSelectPlayerWnd.myTeamInfoList[i]
            local find = CommonDefs.ListFind(result, typeof(BiWuSelectPlayerResult), DelegateFactory.Predicate_BiWuSelectPlayerResult(function (p) 
                return p.playerId == info.playerId
            end))
            CommonDefs.GetComponent_GameObject_Type(item, typeof(CBWDHSelectPlayerItem)):InitResult(CBWDHSelectPlayerWnd.myTeamInfoList[i], find, i)
            i = i + 1
        end
    end
    do
        local i = 0
        while i < CBWDHSelectPlayerWnd.opponentTeamInfoList.Count do
            local item = this.pool:GetFromPool(3)
            item.transform.parent = this.rightGrid.transform
            item.transform.localScale = Vector3.one
            item.gameObject:SetActive(true)
            local info = CBWDHSelectPlayerWnd.opponentTeamInfoList[i]
            local find = CommonDefs.ListFind(result, typeof(BiWuSelectPlayerResult), DelegateFactory.Predicate_BiWuSelectPlayerResult(function (p) 
                return p.playerId == info.playerId
            end))
            CommonDefs.GetComponent_GameObject_Type(item, typeof(CBWDHSelectPlayerItem)):InitResult(CBWDHSelectPlayerWnd.opponentTeamInfoList[i], find, i)
            i = i + 1
        end
    end

    this.leftGrid:Reposition()
    this.rightGrid:Reposition()
end
CBWDHSelectPlayerWnd.m_OnUpdateBiWuTeamSelectResult_CS2LuaHook = function (this, result) 
    if result == nil then
        return
    end

    CUICommonDef.SetActive(this.resetBtn, false, true)
    CUICommonDef.SetActive(this.confirmBtn, false, true)


    CUICommonDef.ClearTransform(this.leftGrid.transform)
    do
        local i = 0
        while i < CBWDHSelectPlayerWnd.myTeamInfoList.Count do
            local item = this.pool:GetFromPool(2)
            item.transform.parent = this.leftGrid.transform
            item.transform.localScale = Vector3.one
            item.gameObject:SetActive(true)

            local info = CBWDHSelectPlayerWnd.myTeamInfoList[i]
            local find = CommonDefs.ListFind(result, typeof(BiWuSelectPlayerResult), DelegateFactory.Predicate_BiWuSelectPlayerResult(function (p) 
                return p.playerId == info.playerId
            end))
            CommonDefs.GetComponent_GameObject_Type(item, typeof(CBWDHSelectPlayerItem)):InitResult(CBWDHSelectPlayerWnd.myTeamInfoList[i], find, i)
            i = i + 1
        end
    end
    this.leftGrid:Reposition()
end
