-- Auto Generated!!
local CBWDHChampionLookupItem = import "L10.UI.CBWDHChampionLookupItem"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local DelegateFactory = import "DelegateFactory"
local EnumClass = import "L10.Game.EnumClass"
local LocalString = import "LocalString"
local Profession = import "L10.Game.Profession"
CBWDHChampionLookupItem.m_Init_CS2LuaHook = function (this, info, row) 
    this.mInfo = info
    this.nameLabel.text = info.leaderName
    this.orderLabel.text = System.String.Format(LocalString.GetString("第{0}届"), row + 1)

    CommonDefs.ListIterate(info.members, DelegateFactory.Action_object(function (___value) 
        local item = ___value
        if item.name == info.leaderName then
            this.clsSprite.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), item.role))
            return
        end
    end))

    if row % 2 == 0 then
        this:SetBackgroundTexture(Constants.NewEvenBgSprite)
    else
        this:SetBackgroundTexture(Constants.NewOddBgSprite)
    end
end
