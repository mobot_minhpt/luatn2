-- Auto Generated!!
local CButton = import "L10.UI.CButton"
local CItemMgr = import "L10.Game.CItemMgr"
local CMarketItemInfo = import "L10.Game.CMarketItemInfo"
local CommonDefs = import "L10.Game.CommonDefs"
local CYuanbaoMarketMgr = import "L10.UI.CYuanbaoMarketMgr"
local CYuanbaoMarketSell = import "L10.UI.CYuanbaoMarketSell"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CYuanbaoMarketSell.m_Init_CS2LuaHook = function (this, itemid)
    this.ItemInfo = nil
    this.mPackageWnd.OnPackageItemClick = MakeDelegateFromCSFunction(this.OnPackageItemClick, MakeGenericClass(Action2, String, UInt32), this)
    this.mPackageWnd:Init(itemid)
    this.numInput.onValueChanged = MakeDelegateFromCSFunction(this.OnNumInputValueChanged, MakeGenericClass(Action1, UInt32), this)
    --if (string.IsNullOrEmpty(itemid))
    --{
    --    previewer.UpdateYuanbaoMarketItem("");
    --    numInput.SetMinMax(0, 0);
    --    numInput.SetValue(0);
    --    moneyCtrl.SetCost(0);
    --    rightTips.SetActive(true);
    --    sellButton.GetComponent<CButton>().Enabled = false;
    --}
end
CYuanbaoMarketSell.m_OnEnable_CS2LuaHook = function (this) 
    EventManager.AddListenerInternal(EnumEventType.OneMarketItemInfoUpdate, MakeDelegateFromCSFunction(this.OneMarketItemInfoUpdate, MakeGenericClass(Action1, CMarketItemInfo), this))
    EventManager.AddListenerInternal(EnumEventType.PreSellMarketItemDone, MakeDelegateFromCSFunction(this.OnPreSellDone, MakeGenericClass(Action3, UInt32, UInt32, UInt32), this))
    EventManager.AddListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
    UIEventListener.Get(this.sellButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.sellButton).onClick, MakeDelegateFromCSFunction(this.OnSellButtonClicked, VoidDelegate, this), true)
end
CYuanbaoMarketSell.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListenerInternal(EnumEventType.OneMarketItemInfoUpdate, MakeDelegateFromCSFunction(this.OneMarketItemInfoUpdate, MakeGenericClass(Action1, CMarketItemInfo), this))
    EventManager.RemoveListenerInternal(EnumEventType.PreSellMarketItemDone, MakeDelegateFromCSFunction(this.OnPreSellDone, MakeGenericClass(Action3, UInt32, UInt32, UInt32), this))
    EventManager.RemoveListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
    UIEventListener.Get(this.sellButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.sellButton).onClick, MakeDelegateFromCSFunction(this.OnSellButtonClicked, VoidDelegate, this), false)
    CYuanbaoMarketMgr.ClearYuanbaoMarketSellData()
end
CYuanbaoMarketSell.m_OnPackageItemClick_CS2LuaHook = function (this, itemId, pos)
    if System.String.IsNullOrEmpty(itemId) then
        this.previewer:UpdateYuanbaoMarketItem("")
        this.numInput:SetMinMax(0, 0, 1)
        this.numInput:SetValue(0, true)
        this.moneyCtrl:SetCost(0)
        this.rightTips:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(this.sellButton, typeof(CButton)).Enabled = false
        this.ItemInfo = nil
        return
    end
    local item = CItemMgr.Inst:GetById(itemId)
    if item ~= nil then
        this.ItemId = itemId
        this.rightTips:SetActive(false)
        CommonDefs.GetComponent_GameObject_Type(this.sellButton, typeof(CButton)).Enabled = true
        local notbindCount, bindCount
        bindCount, notbindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(item.TemplateId)
        this.numInput:SetMinMax(1, notbindCount, 1)
        this.numInput:SetValue(1, true)
        Gac2Gas.RequestMarketItem(item.TemplateId)
        this.previewer:UpdateYuanbaoMarketItem(itemId)
    end
end
CYuanbaoMarketSell.m_OnSendItem_CS2LuaHook = function (this, itemId)    if itemId == this.ItemId then
        local item = CItemMgr.Inst:GetById(itemId)
        local notbindCount = 0 local bindCount = 0
        if item ~= nil then
            bindCount, notbindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(item.TemplateId)
        else
            notbindCount = 0
        end
        if notbindCount + bindCount > 0 then
            this.numInput:SetMinMax(1, notbindCount, 1)
            this.numInput:SetValue(1, true)
        else
            this.numInput:SetMinMax(0, 0, 1)
            this.numInput:SetValue(0, true)
            this.ItemInfo = nil
        end
        this.previewer:UpdateYuanbaoMarketItem(itemId)
    end
end
CYuanbaoMarketSell.m_OnSellButtonClicked_CS2LuaHook = function (this, go)
    if this.mPackageWnd.SelectedItemId == nil then
        return
    end
    local item = CItemMgr.Inst:GetById(this.mPackageWnd.SelectedItemId)
    if item ~= nil then
        if item.IsItem then
            if item.Item.isSell2Market then
                --if (item.Item.Count <= 3)
                --{
                --    string content = MessageMgr.Inst.FormatMessage("SELL_ITEM_CONFIRM", new object[] { item.Name });
                --    MessageWndManager.ShowOKCancelMessage(content, () =>
                --    {
                --        //TODO 出售给系统
                --        if (item != null)
                --        {
                --            Gac2Gas.SellMarketItem(item.TemplateId, 1);
                --        }
                --        Gac2Gas.RequestMarketItem(item.TemplateId);
                --    });
                --}
                --else
                --{
                --    //预售进行价格保护
                --    CUICommonDef.PreSellContext = PreSellContextType.YuanBaoMarket;
                --    Gac2Gas.PreSellMarketItem(item.TemplateId, item.Item.Count);
                --}
                Gac2Gas.SellMarketItem(item.TemplateId, this.numInput:GetValue())
            end
        elseif item.IsEquip then
            Gac2Gas.SellMarketItem(item.TemplateId, 1)
        end
    end

    --Gac2Gas.RequestMarketItem(item.TemplateId);
end
