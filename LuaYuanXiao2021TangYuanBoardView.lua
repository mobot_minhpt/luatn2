local CGamePlayMgr = import "L10.Game.CGamePlayMgr"
local CScene = import "L10.Game.CScene"

LuaYuanXiao2021TangYuanBoardView = class()

RegistChildComponent(LuaYuanXiao2021TangYuanBoardView, "m_TimeLabel","TimeLabel", UILabel)
RegistChildComponent(LuaYuanXiao2021TangYuanBoardView, "m_TaskDesc","TaskDesc", UILabel)
RegistChildComponent(LuaYuanXiao2021TangYuanBoardView, "m_RuleBtn","RuleBtn", GameObject)
RegistChildComponent(LuaYuanXiao2021TangYuanBoardView, "m_LeaveBtn","LeaveBtn", GameObject)
RegistChildComponent(LuaYuanXiao2021TangYuanBoardView, "m_CountLabel","CountLabel", UILabel)

function LuaYuanXiao2021TangYuanBoardView:OnEnable()
    UIEventListener.Get(self.m_LeaveBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        CGamePlayMgr.Inst:LeavePlay()
    end)
    UIEventListener.Get(self.m_RuleBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("YuanXiao2021_YuanXiao_Introduction")
    end)

    self:OnPlayerCountUpdate()
    self.m_TaskDesc.text = g_MessageMgr:FormatMessage("YuanXiao2021_TangYuan_BoardView")
    
    g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
    g_ScriptEvent:AddListener("TangYuanPlayerCountUpdate", self, "OnPlayerCountUpdate")
end

function LuaYuanXiao2021TangYuanBoardView:OnDisable()
    g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
    g_ScriptEvent:RemoveListener("TangYuanPlayerCountUpdate", self, "OnPlayerCountUpdate")
end

function LuaYuanXiao2021TangYuanBoardView:OnPlayerCountUpdate()
    local playerCount = LuaYuanXiao2021Mgr.tangYuanPlayerCount
    self.m_CountLabel.text = tostring(playerCount)
end

function LuaYuanXiao2021TangYuanBoardView:OnSceneRemainTimeUpdate(args)
    if CScene.MainScene then
        if CScene.MainScene.ShowTimeCountDown then
            self.m_TimeLabel.text = self:GetRemainTimeText(CScene.MainScene.ShowTime)
        else
            self.m_TimeLabel.text = nil
        end
    else
        self.m_TimeLabel.text = nil
    end
end

function LuaYuanXiao2021TangYuanBoardView:GetRemainTimeText(totalSeconds)
    if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        return SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end