local CTeamMgr = import "L10.Game.CTeamMgr"
local CUIManager = import "L10.UI.CUIManager"
local MsgPackImpl=import "MsgPackImpl"
local Object=import "System.Object"
local Task_Task = import "L10.Game.Task_Task"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CTaskMgr = import "L10.Game.CTaskMgr"
CLuaTaskMgr = class()

function CLuaTaskMgr.ShowNeedEventsDescription(taskId)
    local task = Task_Task.GetData(taskId)
    if not task then return end
	if task.GamePlay == "AdvancedRanFaJi" then
		return true
    elseif task.DisplayNeedEvents==1 then
        return true
    end
	return false
end

function CLuaTaskMgr.NeedChangeDescription(taskId)
	return Task_Customized.Exists(taskId) and  Task_Customized.GetData(taskId).CustomizeDescription > 0
end

function CLuaTaskMgr.NeedCustomizeTrackInfo(taskId)
	return Task_Customized.Exists(taskId) and  Task_Customized.GetData(taskId).CustomizeTrackInfo > 0
end

function CLuaTaskMgr.GetCustomizedDescription(taskId)
	if Valentine2019_QYSJTask.Exists(taskId) then
	    local data = Valentine2019_QYSJTask.GetData(taskId)
        if CTeamMgr.Inst:MainPlayerIsTeamLeader() then
            return data.PlayerAID
        elseif CTeamMgr.Inst:TeamExists() then
            return data.PlayerBID
        else
        	return Task_Task.GetData(taskId).TaskDescription
	    end
	end

	return ""
end

function CLuaTaskMgr.GetCustomizedSimpleDescription(taskId)
	if Valentine2019_QYSJTask.Exists(taskId) then
	    local data = Valentine2019_QYSJTask.GetData(taskId)
        if CTeamMgr.Inst:MainPlayerIsTeamLeader() then
            return data.PlayerAID
        elseif CTeamMgr.Inst:TeamExists() then
            return data.PlayerBID
        else
        	return Task_Task.GetData(taskId).TaskDescription
	    end
	end

	return ""
end

function CLuaTaskMgr.GetCustomizedTrackInfo(taskId)
	if Valentine2019_QYSJTask.Exists(taskId) then
	    local data = Valentine2019_QYSJTask.GetData(taskId)
        if CTeamMgr.Inst:MainPlayerIsTeamLeader() then
            return data.PlayerAID
        elseif CTeamMgr.Inst:TeamExists() then
            return data.PlayerBID
        else
        	return Task_Task.GetData(taskId).TaskDescription
	    end
	end

	return ""
end

function CLuaTaskMgr.IsBagSpaceNotEnough(task)
    if not task then return false end
	return task.BagSpaceNotEnough > 0
end

function CLuaTaskMgr.GetBagSpaceNotEnoughDescription(task)
    return LocalString.GetString("[c][ffff00]当前任务包裹满，请先清理包裹后再点击此处进行任务[-][/c]")
end

function CLuaTaskMgr.RequestGetTaskItem(taskId)
    Gac2Gas.RequestGetTaskItem(taskId)
end
-------------------------------------
-- 可接任务开放状态查询 BEGIN
-------------------------------------
function CLuaTaskMgr.RequestTaskOpenTimeCheck(taskId)
    Gac2Gas.RequestTaskOpenTimeCheck(taskId)
end
function CLuaTaskMgr.OnRequestTaskOpenTimeCheckResult(taskId, bResult, openTimeDesc)
    if not bResult then
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", openTimeDesc)
    else
        CLuaTaskMgr.GoToAcceptTask(taskId)
    end
end

function CLuaTaskMgr.GoToAcceptTask(taskId)
    local template = Task_Task.GetData(taskId)
    if template and CClientMainPlayer.Inst ~= nil then
        if CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.TaskProp.CurrentTasks, taskId) then
            local task = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.TaskProp.CurrentTasks, taskId)
            if not task.IsFailed and not CTaskMgr.Inst:IsFestivalTaskTimeOut(task) then
                if CTaskMgr.Inst:IsTrackingToDoTask(taskId) then
                    return
                end
                CTaskMgr.Inst:TrackToDoTask(taskId)
                CUIManager.CloseUI("TaskWnd")
            end
        else
            if CTaskMgr.Inst:IsTrackingToDoTask(taskId) then
                return
            end
            CTaskMgr.Inst:TrackToDoTask(taskId)
            CUIManager.CloseUI("TaskWnd")
        end
    end
end
-------------------------------------
-- 可接任务开放状态查询 END
-------------------------------------

--用于处理MainPlayer FinishedTask信息，该方法仅用于为C#下面的Dict赋值，整个应用生命周期限调用一次
function CLuaTaskMgr.GetTaskId2BitSetInfo()
    local dict = CreateFromClass(MakeGenericClass(Dictionary, uint, int))
    for taskId,bitsetId in pairs(Task_IdMap) do
        CommonDefs.DictAdd_LuaCall(dict, taskId, bitsetId)
    end
    Task_IdMap = {} --使用完毕清除所占内存
    return dict
end

CLuaTaskMgr.m_FloweWithBloodEffectWndName = ""
CLuaTaskMgr.m_FloweWithBloodEffectTaskId = 0
CLuaTaskMgr.m_FloweWithBloodEffectSubtitleId = 0

CLuaTaskMgr.m_DriveAwayGhostWndName = ""
CLuaTaskMgr.m_DriveAwayGhostTaskId = 0
CLuaTaskMgr.m_DriveAwayGhostNeedCount = 0
CLuaTaskMgr.m_DriveAwayGhostTotalSeconds = 0

CLuaTaskMgr.m_FlowerWitheredEffectWndName = ""
CLuaTaskMgr.m_FlowerWitheredEffectTaskId = 0
CLuaTaskMgr.m_FlowerWitheredEffectPressDuration = 0

CLuaTaskMgr.m_LeafWithMemoryWnd_Name = ""
CLuaTaskMgr.m_LeafWithMemoryWnd_TaskId = 0
CLuaTaskMgr.m_LeafWithMemoryWnd_FadeAlphaTime = 0
CLuaTaskMgr.m_LeafWithMemoryWnd_ColorId = 1

CLuaTaskMgr.m_PanZhuangWnd_TaskId = 0
CLuaTaskMgr.m_PanZhuangWnd_EffectType = 1
CLuaTaskMgr.m_PanZhuangWnd_DrawTime = 1

CLuaTaskMgr.m_PourWineWnd_TaskId = 0
CLuaTaskMgr.m_TiaoXiangPlayWnd_TaskId = 0

CLuaTaskMgr.m_EyeCatchTheDog_TaskId = 0
CLuaTaskMgr.m_EyeCatchTheDog_NPCTemplateId = 0

CLuaTaskMgr.m_CouncilWnd_TaskId = 0
CLuaTaskMgr.m_DebitNote_TaskId = 0
CLuaTaskMgr.m_DebitNoteState = 0
CLuaTaskMgr.m_ScreenBloodFogWnd_TaskId = 0

CLuaTaskMgr.m_PetCat_Type = 1
CLuaTaskMgr.m_PetCatWnd_TaskId = 0

CLuaTaskMgr.m_MakeClothesWnd_TaskId = 0
CLuaTaskMgr.m_CocoonBreakWnd_TaskId = 0
CLuaTaskMgr.m_MakeClothesWnd_ConditionId = 0
CLuaTaskMgr.m_HeShengZiJinWnd_TaskId = 0

CLuaTaskMgr.m_OpenJinNang_TaskId = 0
CLuaTaskMgr.m_WanPiLingShou_TaskId = 0

CLuaTaskMgr.m_WipeScreenTearsWnd_TaskId = 0

function CLuaTaskMgr.ShowPetCatWnd(type, taskId)
    if not taskId then return end
    CLuaTaskMgr.m_PetCat_Type = type
    CLuaTaskMgr.m_PetCatWnd_TaskId = taskId
    CUIManager.ShowUI("PetCatWnd")
end

function CLuaTaskMgr.ShowLeafWithMemoryWnd(wndName, taskId, fadeAlphaTime, colorId)
    if not taskId then return end
    CLuaTaskMgr.m_LeafWithMemoryWnd_Name = wndName
    CLuaTaskMgr.m_LeafWithMemoryWnd_TaskId = taskId
    CLuaTaskMgr.m_LeafWithMemoryWnd_FadeAlphaTime = fadeAlphaTime
    CLuaTaskMgr.m_LeafWithMemoryWnd_ColorId = colorId
    CUIManager.ShowUI("LeafWithMemoroyWnd")
end

function CLuaTaskMgr.ShowPanZhuangWnd(taskId, effectType, drawTime)
    if not taskId then return end
    CLuaTaskMgr.m_PanZhuangWnd_TaskId = taskId
    CLuaTaskMgr.m_PanZhuangWnd_EffectType = effectType
    CLuaTaskMgr.m_PanZhuangWnd_DrawTime = drawTime
    CUIManager.ShowUI(CLuaUIResources.PanZhuangWnd)
end

function CLuaTaskMgr.ShowPourWineWnd(taskId)
    if not taskId then return end
    CLuaTaskMgr.m_PourWineWnd_TaskId = taskId
    CUIManager.ShowUI(CLuaUIResources.PourWineWnd)
end

function CLuaTaskMgr.ShowTiaoXiangPlayWnd(taskId)
    if not taskId then return end
    CLuaTaskMgr.m_TiaoXiangPlayWnd_TaskId = taskId
    LuaTiaoXiangPlayMgr.InitCookBook(taskId)
    CUIManager.ShowUI(CLuaUIResources.TiaoXiangPlayWnd)
end

function CLuaTaskMgr.ShowOpenJinNangWnd(taskId)
    if not taskId then return end
    CLuaTaskMgr.m_OpenJinNang_TaskId = taskId
    CUIManager.ShowUI(CLuaUIResources.ErHaOpenJinNangWnd)
end

function CLuaTaskMgr.ShowWanPiLingShouWnd(taskId)
    if not taskId then return end
    CLuaTaskMgr.m_WanPiLingShou_TaskId = taskId
    CUIManager.ShowUI(CLuaUIResources.InsectTaskFindLingShouWnd)
end

function CLuaTaskMgr.ShowEyeCatchTheDogWnd(taskId, npcTemplateId)
    if not taskId then return end
    if CUIManager.IsLoaded(CLuaUIResources.EyeCatchTheDogWnd) then return end
    CLuaTaskMgr.m_EyeCatchTheDog_TaskId = taskId
    CLuaTaskMgr.m_EyeCatchTheDog_NPCTemplateId = npcTemplateId
    if CUIManager.instance then
        CUIManager.instance:CloseAllPopupViews()
    end
    CUIManager.ShowUI(CLuaUIResources.EyeCatchTheDogWnd)
end

function CLuaTaskMgr.CompleteLeafWithMemoryWnd()
    Gac2Gas.FinishEventTask(CLuaTaskMgr.m_LeafWithMemoryWnd_TaskId, CLuaTaskMgr.m_LeafWithMemoryWnd_Name)
end

function CLuaTaskMgr.ShowFlowerWithBloodEffectWnd(wndName, taskId, subtitleId)
    if not taskId or not subtitleId then return end
    CLuaTaskMgr.m_FloweWithBloodEffectWndName = wndName
    CLuaTaskMgr.m_FloweWithBloodEffectTaskId = taskId
    CLuaTaskMgr.m_FloweWithBloodEffectSubtitleId = subtitleId
    CUIManager.ShowUI("FlowerWithBloodEffectWnd")
end

function CLuaTaskMgr.ShowDriveAwayGhostWnd(wndName, taskId, needCount, totalSeconds)
    if not taskId then return end
    CLuaTaskMgr.m_DriveAwayGhostWndName = wndName
    CLuaTaskMgr.m_DriveAwayGhostTaskId = taskId
    CLuaTaskMgr.m_DriveAwayGhostNeedCount = needCount
    CLuaTaskMgr.m_DriveAwayGhostTotalSeconds = totalSeconds
    CUIManager.ShowUI("DriveAwayGhostWnd")
end

function CLuaTaskMgr.ShowFlowerWitheredEffectWnd(wndName, taskId, pressDuration)
    if not taskId then return end
    CLuaTaskMgr.m_FlowerWitheredEffectWndName = wndName
    CLuaTaskMgr.m_FlowerWitheredEffectTaskId = taskId
    CLuaTaskMgr.m_FlowerWitheredEffectPressDuration = pressDuration
    CUIManager.ShowUI("FlowerWitheredEffectWnd")
end

function CLuaTaskMgr.CompleteFloweWithBloodEffectTask()
    Gac2Gas.FinishEventTask(CLuaTaskMgr.m_FloweWithBloodEffectTaskId, CLuaTaskMgr.m_FloweWithBloodEffectWndName)
end

function CLuaTaskMgr.CompleteDriveAwayChostTask()
    Gac2Gas.FinishEventTask(CLuaTaskMgr.m_DriveAwayGhostTaskId, CLuaTaskMgr.m_DriveAwayGhostWndName)
end

function CLuaTaskMgr.CompleteFlowerWitheredEffectTask()
    Gac2Gas.FinishEventTask(CLuaTaskMgr.m_FlowerWitheredEffectTaskId, CLuaTaskMgr.m_FlowerWitheredEffectWndName)
end

function CLuaTaskMgr.IsOpenJuQingWndTaskEvent(eventName)
    -- eventName对应OpenWnd action里面的窗口名
    return eventName == "FloweWithBloodEffectWnd"
        or eventName == "DriveAwayGhostWnd"
        or eventName == "FlowerWitheredEffectWnd"
		or eventName == "FallingDownTreasure"
		or eventName == "Lumbering"
		or eventName == "BoilMedicine"
		or eventName == "PuSongLingHandWriteEffect"
        or eventName == "LeafWithMemoryWnd"
        or eventName == "chasesteps"
        or eventName == "zuanmuquhuo"
        or eventName == "shanzhenmeiwei"
end

function CLuaTaskMgr.RequestOpenEventTask(taskId, eventName)
    --约定好EventName 等于 WndName
    local wndName = eventName
    Gac2Gas.RequestOpenEventTask(taskId, eventName, wndName)
end

CLuaTaskMgr.m_TakePhotoRelatedTaskId = 0
CLuaTaskMgr.m_TakePhotoRelatedEventName = 0
function CLuaTaskMgr.TaskTriggerTakePhoto(taskId, eventName)
    CLuaTaskMgr.m_TakePhotoRelatedTaskId = taskId or 0
    CLuaTaskMgr.m_TakePhotoRelatedEventName = eventName
    if ((CUIManager.instance or {}).popupUIStack or {}).Count == 0 then
        CUIManager.ShowUI("ScreenCaptureWnd")
    end
end

--通用的拍照任务完成
function CLuaTaskMgr.OnCaptureScreenDone()
    --处理一些回调
    if CLuaTaskMgr.m_TakePhotoRelatedTaskId>0 then
        local id = CLuaTaskMgr.m_TakePhotoRelatedTaskId
        CLuaTaskMgr.m_TakePhotoRelatedTaskId = 0
        local empty = CreateFromClass(MakeGenericClass(List, Object))
        empty = MsgPackImpl.pack(empty)
        Gac2Gas.FinishClientTaskEvent(id, CLuaTaskMgr.m_TakePhotoRelatedEventName, empty)
    end
end
--通用的拍照界面关闭
function CLuaTaskMgr.OnCaptureScreenWndClose(captureHappened)
    if captureHappened then
        CLuaTaskMgr.OnCaptureScreenDone()
    end

    --此处清一些变量
    CLuaTaskMgr.m_TakePhotoRelatedTaskId = 0
end
-------------------------------------
-- 隐藏任务追踪栏的任务
-------------------------------------

CLuaTaskMgr.m_TempHidenTasksInTrackPanel = {}

function CLuaTaskMgr.OnMainPlayerCreated()
    CLuaTaskMgr.m_TempHidenTasksInTrackPanel = {}
end

function CLuaTaskMgr.UpdateTempHiddenTasksInTrackPanel(taskId, visible)
    if visible and CLuaTaskMgr.m_TempHidenTasksInTrackPanel[taskId] ~= nil then
        CLuaTaskMgr.m_TempHidenTasksInTrackPanel[taskId] = nil
        g_ScriptEvent:BroadcastInLua("OnUpdateTempHiddenTasksInTrackPanel")
    elseif not visible and CLuaTaskMgr.m_TempHidenTasksInTrackPanel[taskId] == nil then
        CLuaTaskMgr.m_TempHidenTasksInTrackPanel[taskId] = true
        g_ScriptEvent:BroadcastInLua("OnUpdateTempHiddenTasksInTrackPanel")
    end
end

function CLuaTaskMgr.IsHideTaskListItem(taskId)
    return CLuaTaskMgr.m_TempHidenTasksInTrackPanel[taskId] or
        ZhongYuanJie2020Mgr:IsHideTaskListItem(taskId) or
        Task_Task.GetData(taskId).HideInTaskList > 0
end

function CLuaTaskMgr.HiddenInTaskTrackPanel(taskId, hiddenIds)
    if hiddenIds and hiddenIds[taskId] then
        local taskTemplate = Task_Task.GetData(taskId)
        return taskTemplate.DisplayNode ~= 1 --非主线任务才允许隐藏
    else
        return false
    end
end

CLuaTaskMgr.ScreenFadeIn = 0    -- 屏幕渐入时间
CLuaTaskMgr.ScreenFadeOut = 0   -- 屏幕渐出时间
CLuaTaskMgr.ScreenDuration = 0  -- 屏幕持续时间
CLuaTaskMgr.ScreenColor = ""     -- 屏幕颜色
function CLuaTaskMgr.ShowScreenTransition(data)
    CLuaTaskMgr.ScreenFadeIn = data.fadeIn
    CLuaTaskMgr.ScreenFadeOut = data.fadeOut
    CLuaTaskMgr.ScreenDuration = data.duration
    CLuaTaskMgr.ScreenColor = data.color
    CUIManager.ShowUI(CLuaUIResources.ScreenTransitionWnd)
end
----------
--一些C#下面的方法在Lua进行Hook
----------
CTaskMgr.m_hookCanTrackToDoTaskInCrossServer = function(this)
	return  (CClientMainPlayer.Inst and not CClientMainPlayer.Inst.IsCrossServerPlayer) or  LuaGuildTerritorialWarsMgr:IsPeripheryPlay()
end