local UILabel = import "UILabel"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"

LuaSnowAdventurePropertyDetailWnd = class()
LuaSnowAdventurePropertyDetailWnd.snowmanType = nil

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSnowAdventurePropertyDetailWnd, "PropertyDetailTips", "PropertyDetailTips", UILabel)
RegistChildComponent(LuaSnowAdventurePropertyDetailWnd, "PropertyContent", "PropertyContent", GameObject)
RegistChildComponent(LuaSnowAdventurePropertyDetailWnd, "PropertyPair", "PropertyPair", GameObject)

--@endregion RegistChildComponent end

function LuaSnowAdventurePropertyDetailWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self:RefreshProperty()
end

function LuaSnowAdventurePropertyDetailWnd:Init()

end

function LuaSnowAdventurePropertyDetailWnd:RefreshProperty()
    self.PropertyDetailTips.text = g_MessageMgr:FormatMessage("SnowAdventure_Prop_Tips")
    
    local snowmanType = LuaSnowAdventurePropertyDetailWnd.snowmanType
    local level = LuaHanJia2023Mgr.snowAdventureData.snowmanInfo[snowmanType]
    local sourceProp = LuaHanJia2023Mgr:GetSnowmanProp(snowmanType, 1)
    local targetProp = LuaHanJia2023Mgr:GetSnowmanProp(snowmanType, level)
    
    self.attributeList = {}
    local childCount = self.PropertyContent.transform.childCount
    if childCount < #sourceProp then
        for i = childCount+1, #sourceProp do
            local attributeGo = CommonDefs.Object_Instantiate(self.PropertyPair)
            attributeGo.transform.parent = self.PropertyContent.transform
            attributeGo.transform.localScale = Vector3.one
            attributeGo:SetActive(true)
            table.insert(self.attributeList, attributeGo)
        end
    end
    for i = 1, #self.attributeList do
        local nameLabel = self.attributeList[i].transform:Find("Name"):GetComponent(typeof(UILabel))
        local srcLabel = self.attributeList[i].transform:Find("SourceValue"):GetComponent(typeof(UILabel))
        local buffLabel = self.attributeList[i].transform:Find("BuffLevel"):GetComponent(typeof(UILabel))
        nameLabel.text = sourceProp[i].name
        if #sourceProp[i].value == 1 then
            --name: A
            srcLabel.text = sourceProp[i].value[1]
            buffLabel.text = targetProp[i].value[1] - sourceProp[i].value[1]
        elseif #sourceProp[i].value == 2 then
            --name: A~B
            srcLabel.text = sourceProp[i].value[1] .. "-" .. sourceProp[i].value[2]
            buffLabel.text = (targetProp[i].value[1] - sourceProp[i].value[1]) .. "-" .. (targetProp[i].value[2] - sourceProp[i].value[2])
        end
    end
end

--@region UIEvent

--@endregion UIEvent

