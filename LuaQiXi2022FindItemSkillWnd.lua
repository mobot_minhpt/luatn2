local Vector2 = import "UnityEngine.Vector2"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UISlider = import "UISlider"

LuaQiXi2022FindItemSkillWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaQiXi2022FindItemSkillWnd, "Button", "Button", GameObject)
RegistChildComponent(LuaQiXi2022FindItemSkillWnd, "Filled", "Filled", GameObject)
RegistChildComponent(LuaQiXi2022FindItemSkillWnd, "ProgressBar", "ProgressBar", UISlider)
RegistChildComponent(LuaQiXi2022FindItemSkillWnd, "FilledFx", "FilledFx", CUIFx)

--@endregion RegistChildComponent end

function LuaQiXi2022FindItemSkillWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.Button.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButtonClick()
	end)


    --@endregion EventBind end
end

function LuaQiXi2022FindItemSkillWnd:Init()
	self:OnUpdateDistance(LuaQiXi2022Mgr.m_XW_Distance)
end

function LuaQiXi2022FindItemSkillWnd:OnEnable()
	g_ScriptEvent:AddListener("SFEQ_XW_UpdateDistance", self, "OnUpdateDistance")
end

function LuaQiXi2022FindItemSkillWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SFEQ_XW_UpdateDistance", self, "OnUpdateDistance")
end

function LuaQiXi2022FindItemSkillWnd:OnUpdateDistance(distance)
	local l,r = QiXi2022_Setting.GetData().XunwuDistance[0], QiXi2022_Setting.GetData().XunwuDistance[1]
	local v = (distance - l) / (r - l)
	self.ProgressBar.value = v
	self.Filled.gameObject:SetActive(v >= 1)
	if v>= 1 then
		self.FilledFx:DestroyFx()
		self.FilledFx:LoadFx("fx/ui/prefab/UI_mgd_suifeng_jindutiaoman.prefab")
	end
end

--@region UIEvent

function LuaQiXi2022FindItemSkillWnd:OnButtonClick()
	if self.ProgressBar.value < 1 then
		g_MessageMgr:ShowMessage("QiXi2022FindItemSkill_Failed")
		return
	end
	if CClientMainPlayer.Inst then
		local skillId = QiXi2022_Setting.GetData().Gameplay3TempSkill
		CClientMainPlayer.Inst:TryCastSkill(skillId, true, Vector2.zero)
	end
end

--@endregion UIEvent

