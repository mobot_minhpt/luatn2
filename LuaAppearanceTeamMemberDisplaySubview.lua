local CUITexture = import "L10.UI.CUITexture"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local Profession = import "L10.Game.Profession"
local UISprite = import "UISprite"
local NGUIText = import "NGUIText"
local Constants = import "Constants"
local CCharacterModelTextureLoader = import "L10.UI.CCharacterModelTextureLoader"
local Double = import "System.Double"

LuaAppearanceTeamMemberDisplaySubview = class()
RegistChildComponent(LuaAppearanceTeamMemberDisplaySubview,"m_ItemNameLabel", "ItemNameLabel", UILabel)
RegistChildComponent(LuaAppearanceTeamMemberDisplaySubview,"m_ModelTexture", "Texture", CCharacterModelTextureLoader)
RegistChildComponent(LuaAppearanceTeamMemberDisplaySubview,"m_SwitchButton", "SwitchButton", CButton)
RegistChildComponent(LuaAppearanceTeamMemberDisplaySubview,"m_TeamBgRoot", "TeamBgRoot", GameObject)
RegistChildComponent(LuaAppearanceTeamMemberDisplaySubview,"m_MemberInfoRoot", "MemberInfoRoot", GameObject)
RegistChildComponent(LuaAppearanceTeamMemberDisplaySubview,"m_ProfessionIcon", "TypeIcon", UISprite)
RegistChildComponent(LuaAppearanceTeamMemberDisplaySubview,"m_LevelLabel", "levelLabel", UILabel)
RegistChildComponent(LuaAppearanceTeamMemberDisplaySubview,"m_NameLabel", "nameLabel", UILabel)
RegistChildComponent(LuaAppearanceTeamMemberDisplaySubview,"m_XiuWeiLabel", "xiuweiLabel", UILabel)
RegistChildComponent(LuaAppearanceTeamMemberDisplaySubview,"m_XiuLianLabel", "xiulianLabel", UILabel)
RegistChildComponent(LuaAppearanceTeamMemberDisplaySubview,"m_MemberBg", "MemberBg", CUITexture)
RegistChildComponent(LuaAppearanceTeamMemberDisplaySubview,"m_MemberBorder", "MemberBorder", CUITexture)

RegistClassMember(LuaAppearanceTeamMemberDisplaySubview,"m_TeamBg")
RegistClassMember(LuaAppearanceTeamMemberDisplaySubview,"m_ShowMemberBg")

function LuaAppearanceTeamMemberDisplaySubview:Awake()
    UIEventListener.Get(self.m_SwitchButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnSwitchButtonClick() end)
    self.m_TeamBg = self.m_TeamBgRoot.transform:GetComponent(typeof(CUITexture))
end

function LuaAppearanceTeamMemberDisplaySubview:Init()
    self.m_ShowMemberBg = true
    self:OnShowMemberBgChanged()

    local mainplayer = CClientMainPlayer.Inst
    if mainplayer then
        self.m_ProfessionIcon.spriteName =  mainplayer and Profession.GetIcon(mainplayer.Class) or ""
        local color = mainplayer and mainplayer.IsInXianShenStatus and Constants.ColorOfFeiSheng or NGUIText.EncodeColor24(self.m_LevelLabel.color)
        self.m_LevelLabel.text = System.String.Format("[c][{0}]lv. {1}[-][/c]", color, mainplayer and mainplayer.Level or 0)
        self.m_NameLabel.text =  mainplayer and mainplayer.Name or ""
        self.m_XiuWeiLabel.text = NumberComplexToString(NumberTruncate(mainplayer and mainplayer.BasicProp.XiuWeiGrade or 0, 1), typeof(Double), "F1")
        self.m_XiuLianLabel.text = NumberComplexToString(NumberTruncate(mainplayer and mainplayer.BasicProp.XiuLian or 0, 1), typeof(Double), "F1")
        self.m_ModelTexture:Init(mainplayer.Id, EnumToInt(mainplayer.Class), EnumToInt(mainplayer.Gender))
    end
end

function LuaAppearanceTeamMemberDisplaySubview:OnSwitchButtonClick()
    self.m_ShowMemberBg = not self.m_ShowMemberBg
    self:OnShowMemberBgChanged()
end

function LuaAppearanceTeamMemberDisplaySubview:OnShowMemberBgChanged()
    self.m_MemberInfoRoot:SetActive(self.m_ShowMemberBg)
    self.m_TeamBgRoot:SetActive(not self.m_ShowMemberBg)
    self.m_SwitchButton.Text = self.m_ShowMemberBg and LocalString.GetString("全队") or LocalString.GetString("个人")
end

function LuaAppearanceTeamMemberDisplaySubview:SetContent(itemName, memberBg, memberBorder, teamBg)
    self.m_ItemNameLabel.text = itemName
    self.m_MemberBg:LoadMaterial(memberBg)
    self.m_MemberBorder:LoadMaterial(memberBorder)
    self.m_TeamBg:LoadMaterial(teamBg)
    if teamBg~=nil and teamBg~="" then
        self.m_SwitchButton.gameObject:SetActive(true)
    else
        self.m_SwitchButton.gameObject:SetActive(false)
        self.m_ShowMemberBg = true
        self:OnShowMemberBgChanged()
    end
end