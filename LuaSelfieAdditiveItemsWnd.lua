require("3rdParty/ScriptEvent")
require("common/common_include")
require("ui/selfie/LuaSelfieAdditiveItem")
local UISprite = import "UISprite"
local UIGrid = import "UIGrid"
local UITable = import "UITable"
local UIScrollView = import "UIScrollView"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local Color = import "UnityEngine.Color"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local PaiZhao_TieZhi = import "L10.Game.PaiZhao_TieZhi"
local PaiZhao_QiPao = import "L10.Game.PaiZhao_QiPao"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local PaiZhao_Setting = import "L10.Game.PaiZhao_Setting"
local PaiZhao_LvJing = import "L10.Game.PaiZhao_LvJing"
local CPostEffectMgr = import "L10.Engine.CPostEffectMgr"
local CPostProcessingMgr = import "L10.Engine.PostProcessing.CPostProcessingMgr"
CLuaSelfieAdditiveItemsWnd=class()
RegistClassMember(CLuaSelfieAdditiveItemsWnd,"CloseButton")
RegistClassMember(CLuaSelfieAdditiveItemsWnd,"Grid")
RegistClassMember(CLuaSelfieAdditiveItemsWnd,"ScrollView")
RegistClassMember(CLuaSelfieAdditiveItemsWnd,"ItemPrefab")
RegistClassMember(CLuaSelfieAdditiveItemsWnd,"TiezhiButton")
RegistClassMember(CLuaSelfieAdditiveItemsWnd,"LvjingButton")
RegistClassMember(CLuaSelfieAdditiveItemsWnd,"QipaoButton")
RegistClassMember(CLuaSelfieAdditiveItemsWnd,"ItemTable")
RegistClassMember(CLuaSelfieAdditiveItemsWnd,"currentIndex")
RegistClassMember(CLuaSelfieAdditiveItemsWnd,"colorItemTable")
RegistClassMember(CLuaSelfieAdditiveItemsWnd,"colorTable")
RegistClassMember(CLuaSelfieAdditiveItemsWnd,"ColorItemPrefab")
function CLuaSelfieAdditiveItemsWnd:Awake()
	self.currentIndex = -1
	local tzs = self.TiezhiButton:GetComponent(typeof(UISprite))
	local ljs = self.LvjingButton:GetComponent(typeof(UISprite))
	local qps = self.QipaoButton:GetComponent(typeof(UISprite))
	local onTabGOClick = function(go)
		if go == self.TiezhiButton then 
			self:TabClick(0)	
			tzs.spriteName = "common_tab_02_highlight"
			ljs.spriteName = "common_tab_02_normal"
			qps.spriteName = "common_tab_02_normal"
		elseif go == self.LvjingButton then 
			self:TabClick(1)
			tzs.spriteName = "common_tab_02_normal"
			ljs.spriteName = "common_tab_02_highlight"
			qps.spriteName = "common_tab_02_normal"
		elseif go == self.QipaoButton then 
			self:TabClick(2)
			tzs.spriteName = "common_tab_02_normal"
			ljs.spriteName = "common_tab_02_normal"
			qps.spriteName = "common_tab_02_highlight"
		end
	end
	self:TabClick(0)
	tzs.spriteName = "common_tab_02_highlight"
	ljs.spriteName = "common_tab_02_normal"
	qps.spriteName = "common_tab_02_normal"

	local onCloseClick = function(go)
		self.gameObject:SetActive(false)
	end
	local onValueChange = function (v)
		self:OnValueChange(v)
	end
	CommonDefs.AddOnClickListener(self.CloseButton,DelegateFactory.Action_GameObject(onCloseClick),false)
	CommonDefs.AddOnClickListener(self.TiezhiButton,DelegateFactory.Action_GameObject(onTabGOClick),false)
	CommonDefs.AddOnClickListener(self.LvjingButton,DelegateFactory.Action_GameObject(onTabGOClick),false)
	CommonDefs.AddOnClickListener(self.QipaoButton,DelegateFactory.Action_GameObject(onTabGOClick),false)
end
function CLuaSelfieAdditiveItemsWnd:OnEnable()
	g_ScriptEvent:AddListener("OnAddPhotoFilterEffectComplete", self, "HighlightCurrentFX")
	g_ScriptEvent:AddListener("SyncUnLockPaiZhaoLvJing", self, "OnSyncUnLockPaiZhaoLvJing")
end
function CLuaSelfieAdditiveItemsWnd:OnDisable()
	EventManager.BroadcastInternalForLua(EnumEventType.OnTiezhiItemEditEnable,{0} )
	g_ScriptEvent:RemoveListener("OnAddPhotoFilterEffectComplete", self, "HighlightCurrentFX")
	g_ScriptEvent:RemoveListener("SyncUnLockPaiZhaoLvJing", self, "OnSyncUnLockPaiZhaoLvJing")
end
function CLuaSelfieAdditiveItemsWnd:TabClick(index, forceRefresh)
	if not forceRefresh and self.currentIndex == index then
		return
	end
	self.ItemTable = {}
	self.currentIndex  = index
	CUICommonDef.ClearTransform(self.Grid.transform)
	
	local onItemClick = function(id, density)
		g_ScriptEvent:BroadcastInLua("SelfieAddItem", id, self.currentIndex, density)
		if self.currentIndex == 0 then
			local tiezhiData = PaiZhao_TieZhi.GetData(id)
			self:ShowColorItems(tiezhiData.Color == 1)
		elseif self.currentIndex == 1 then
			--self:HighlightCurrentFX()
		end
		
	end
	local generateItem = function(go,typeindex,id)
		local controller = CLuaSelfieAdditiveItem:new()
		controller:AttachToGo(go)
		controller.onClick = onItemClick
		controller:SetData(id,typeindex)
		return controller
	end
	if 	self.currentIndex == 0 then 
		for i=1,PaiZhao_TieZhi.GetDataCount() do 
			local controller = CLuaSelfieAdditiveItem:new()
			local item = CUICommonDef.AddChild(self.Grid,self.ItemPrefab)
			item:SetActive(true)
			local c = generateItem(item,self.currentIndex,i)
			self.ItemTable[i]=c
		end
	elseif 	self.currentIndex == 1 then
		for i=1,PaiZhao_LvJing.GetDataCount()  do 
			if PaiZhao_LvJing.GetData(i).ValidInCapture == 1 then
				local controller = CLuaSelfieAdditiveItem:new()
				local item = CUICommonDef.AddChild(self.Grid,self.ItemPrefab)
				item:SetActive(true)
				local c = generateItem(item,self.currentIndex,i)
				self.ItemTable[i] = c
			end
		end
	elseif 	self.currentIndex == 2 then
		for i=1,PaiZhao_QiPao.GetDataCount()  do 
			local controller = CLuaSelfieAdditiveItem:new()
			local item = CUICommonDef.AddChild(self.Grid,self.ItemPrefab)
			item:SetActive(true)
			local c = generateItem(item,self.currentIndex,i)
			self.ItemTable[i] = c
		end
	end
	
	local grid = self.Grid:GetComponent(typeof(UIGrid))
	grid:Reposition()
	local uiscrollView = self.ScrollView:GetComponent(typeof(UIScrollView))
	uiscrollView:SetDragAmount(0,0,false)
	
	if self.currentIndex == 1 then 
		self:ShowColorItems(false)
		self:HighlightCurrentFX()
	elseif self.currentIndex == 2 then
		
	end
end
function CLuaSelfieAdditiveItemsWnd:ShowColorItems(flag)
	if not flag then 
		self.ColorChoose:SetActive(false)
		return 
	end
	self.ColorChoose:SetActive(true)
	if self.ColorTable.transform.childCount > 0 then
		return
	end
	local click = function(go)
		self:OnColorItemClick(go)
	end
	self.colorItemTable = {}
	self.colorTable = {}
	local grid = self.ColorTable:GetComponent(typeof(UITable))
	local colordata = PaiZhao_Setting.GetData().Hulu_color
	for i=0,colordata.Length-1 do
		local item = CUICommonDef.AddChild(self.ColorTable,self.ColorItemPrefab)
		item:SetActive(true)
		local sprite = item:GetComponent(typeof(UISprite))
		local color = Color(colordata[i][0]/255,colordata[i][1]/255,colordata[i][2]/255)
		sprite.color = color
		self.colorItemTable[i] = item
		self.colorTable[i] = color
		CommonDefs.AddOnClickListener(item,DelegateFactory.Action_GameObject(click),false)
	end
	grid:Reposition()
end
function CLuaSelfieAdditiveItemsWnd:OnColorItemClick( go)
	for i=0,#self.colorItemTable do 
		local item = self.colorItemTable[i]:GetComponent(typeof(QnSelectableButton))
		if self.colorItemTable[i] == go then
			item:SetSelected(true)
			EventManager.BroadcastInternalForLua(EnumEventType.OnTiezhiModifyColorIndex,{i})
		else
			item:SetSelected(false)
		end	
	end
end
function CLuaSelfieAdditiveItemsWnd:HighlightCurrentFX()
	for i=1,#self.ItemTable do 
		local item = self.ItemTable[i]
		if CPostProcessingMgr.s_NewPostProcessingOpen then
			local postEffectCtrl = CPostProcessingMgr.Instance.MainController
			item:Highlight(item.id == postEffectCtrl.PhotoEffectID)
		else
			item:Highlight(item.id == CPostEffectMgr.Instance.PhotoEffectID)
		end
	end
end
function CLuaSelfieAdditiveItemsWnd:OnSyncUnLockPaiZhaoLvJing()
	if self.currentIndex == 1 then
		self:TabClick(1, true)
	end
end
return CLuaSelfieAdditiveItemsWnd





