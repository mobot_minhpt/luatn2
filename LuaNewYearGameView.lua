require("common/common_include")
local UILabel=import "UILabel"

LuaNewYearGameView=class()
RegistClassMember(LuaNewYearGameView,"m_TaskName")
RegistClassMember(LuaNewYearGameView,"m_Score")
RegistClassMember(LuaNewYearGameView,"m_Attack")
RegistClassMember(LuaNewYearGameView,"m_TeamScore")
RegistClassMember(LuaNewYearGameView,"m_PerText")
RegistClassMember(LuaNewYearGameView,"m_TipBtn")

function LuaNewYearGameView:Awake()
    self.m_TaskName=FindChild(self.transform,"TaskName"):GetComponent(typeof(UILabel))
    self.m_TaskName.text=nil
    self.m_Score=FindChild(self.transform,"score"):GetComponent(typeof(UILabel))
    self.m_Score.text=nil
    self.m_Attack=FindChild(self.transform,"attack"):GetComponent(typeof(UILabel))
    self.m_Attack.text=nil
    self.m_TeamScore=FindChild(self.transform,"teamscore"):GetComponent(typeof(UILabel))
    self.m_TeamScore.text=nil
    self.m_PerText=FindChild(self.transform,"perText"):GetComponent(typeof(UILabel))
    self.m_PerText.text=nil
    self.m_TipBtn = FindChild(self.transform,"tipBtn").gameObject

    local onTipClick = function(go)
      g_MessageMgr:ShowMessage('YuanDanPlayTip')
    end

    CommonDefs.AddOnClickListener(self.m_TipBtn,DelegateFactory.Action_GameObject(onTipClick),false)
end

--断线重连
function LuaNewYearGameView:Init()
  self:Refresh()
end

function LuaNewYearGameView:Refresh()
  if LuaNewYear2019Mgr.NewYearGameShowIndex then
    self.m_TaskName.text = SafeStringFormat3(LocalString.GetString("七层玲珑塔-第%s层"),LuaNewYear2019Mgr.NewYearGameShowIndex)
  end
  if LuaNewYear2019Mgr.NewYearGameScore then
    self.m_Score.text = LuaNewYear2019Mgr.NewYearGameScore
  end
  if LuaNewYear2019Mgr.NewYearGameAttack then
    self.m_Attack.text = LuaNewYear2019Mgr.NewYearGameAttack .. '%'
  end
  if LuaNewYear2019Mgr.NewYearGameTeamScore then
    self.m_TeamScore.text = LuaNewYear2019Mgr.NewYearGameTeamScore
  end
  if LuaNewYear2019Mgr.NewYearGamePer then
    if tonumber(LuaNewYear2019Mgr.NewYearGameShowIndex) == 7 then
      self.m_PerText.text = g_MessageMgr:FormatMessage('NYC_TASK_DESCRIPTION')--LocalString.GetString("挑战二郎真君赢取奖励")
    else
      self.m_PerText.text = SafeStringFormat3(LocalString.GetString("有%s%%的概率进入下一层"),LuaNewYear2019Mgr.NewYearGamePer)
    end
  end
end


function LuaNewYearGameView:OnEnable()
    g_ScriptEvent:AddListener("NewYearGameRefresh", self, "Refresh")

end
function LuaNewYearGameView:OnDisable()
    g_ScriptEvent:RemoveListener("NewYearGameRefresh", self, "Refresh")
end

return LuaNewYearGameView
