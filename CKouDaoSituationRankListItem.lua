-- Auto Generated!!
local CKouDaoSituationRankListItem = import "L10.UI.CKouDaoSituationRankListItem"
local Profession = import "L10.Game.Profession"
CKouDaoSituationRankListItem.m_Init_CS2LuaHook = function (this, rank, cls, playerName, value, percentage) 

    this.rankLabel.text = tostring(rank)
    this.iconSprite.spriteName = Profession.GetIcon(cls)
    this.nameLabel.text = playerName
    this.valueLabel.text = tostring(value)
    this.percentageLabel.text = System.String.Format("{0}%", math.floor(percentage * 100 + 0.5))
    this.progressBar.value = percentage
end
