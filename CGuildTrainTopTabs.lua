-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CGuildTrainMgr = import "L10.Game.CGuildTrainMgr"
local CGuildTrainTopTabs = import "L10.UI.CGuildTrainTopTabs"
local CommonDefs = import "L10.Game.CommonDefs"
local Extensions = import "Extensions"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local Object = import "System.Object"
local QnButton = import "L10.UI.QnButton"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local UILabel = import "UILabel"
CGuildTrainTopTabs.m_Init_CS2LuaHook = function (this) 
    CommonDefs.ListClear(this.tabList)
    Extensions.RemoveAllChildren(this.table.transform)
    this.tabTemplate:SetActive(false)
    --if (CGuildTrainMgr.Inst.trainCategoryList == null || CGuildTrainMgr.Inst.trainCategoryList.Count == 0)
    --    Gac2Gas.
    do
        local i = 0
        while i < CGuildTrainMgr.Inst.trainCategoryList.Count do
            local tab = NGUITools.AddChild(this.table.gameObject, this.tabTemplate)
            tab:SetActive(true)
            CommonDefs.GetComponentInChildren_Component_Type(tab.transform, typeof(UILabel)).text = string.gsub(CGuildTrainMgr.Inst.trainCategoryList[i].categoryName, LocalString.GetString("修炼"), "")
            local button = CommonDefs.GetComponent_GameObject_Type(tab, typeof(QnSelectableButton))
            button:SetSelected(false, false)
            button.OnClick = CommonDefs.CombineListner_Action_QnButton(button.OnClick, MakeDelegateFromCSFunction(this.OnButtonClicked, MakeGenericClass(Action1, QnButton), this), true)
            CommonDefs.ListAdd(this.tabList, typeof(QnSelectableButton), button)
            i = i + 1
        end
    end
    this.table:Reposition()
    this:OnButtonClicked(this.tabList[0])

    this.xiulianLabel.text = ""
    if CClientMainPlayer.Inst ~= nil then
        this.xiulianLabel.text = tostring(NumberTruncate(CClientMainPlayer.Inst.BasicProp.XiuLian, 1))
    end
end
CGuildTrainTopTabs.m_OnButtonClicked_CS2LuaHook = function (this, button) 
    do
        local i = 0
        while i < this.tabList.Count do
            this.tabList[i]:SetSelected(button == this.tabList[i], false)
            if button == this.tabList[i] and this.OnTabChanged ~= nil then
                GenericDelegateInvoke(this.OnTabChanged, Table2ArrayWithCount({i}, 1, MakeArrayClass(Object)))
            end
            i = i + 1
        end
    end
end
CGuildTrainTopTabs.m_OnMainPlayerBasicPropUpdate_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst ~= nil then
        this.xiulianLabel.text = tostring(NumberTruncate(CClientMainPlayer.Inst.BasicProp.XiuLian, 1))
    end
end
