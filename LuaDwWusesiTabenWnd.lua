local String					= import "System.String"
local CItemInfoMgr				= import "L10.UI.CItemInfoMgr"
local AlignType					= import "L10.UI.CItemInfoMgr+AlignType"
local CItemChooseMgr			= import "L10.UI.CItemChooseMgr"
local CUITexture				= import "L10.UI.CUITexture"
local MessageWndManager			= import "L10.UI.MessageWndManager"
local CUIManager                = import "L10.UI.CUIManager"
local CUICommonDef              = import "L10.UI.CUICommonDef"
local CItemMgr					= import "L10.Game.CItemMgr"
local EnumItemPlace				= import "L10.Game.EnumItemPlace"
local CClientMainPlayer         = import "L10.Game.CClientMainPlayer"
local CommonDefs                = import "L10.Game.CommonDefs"
local DelegateFactory		    = import "DelegateFactory"
local UIEventListener		    = import "UIEventListener"
local UILabel                   = import "UILabel"
local UISprite                  = import "UISprite"
local LocalString               = import "LocalString"

--五色丝拓本界面
CLuaDwWusesiTabenWnd = class()

RegistClassMember(CLuaDwWusesiTabenWnd,"instruction")
RegistClassMember(CLuaDwWusesiTabenWnd,"okButton")
RegistClassMember(CLuaDwWusesiTabenWnd,"cancelButton")
RegistClassMember(CLuaDwWusesiTabenWnd,"weddingring")
RegistClassMember(CLuaDwWusesiTabenWnd,"equip")
RegistClassMember(CLuaDwWusesiTabenWnd,"m_TargetId")--目标道具id

CLuaDwWusesiTabenWnd.OriID = nil --五色丝道具id

function CLuaDwWusesiTabenWnd:Awake()
    self.m_TargetId = nil
    self.instruction = self.transform:Find("Anchor/Instruction"):GetComponent(typeof(UILabel))
    self.okButton = self.transform:Find("Anchor/OKButton").gameObject
    self.cancelButton = self.transform:Find("Anchor/CancelButton").gameObject

    UIEventListener.Get(self.okButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnOkButtonClick(go)
    end)
end

function CLuaDwWusesiTabenWnd:Init()
    self:InitTabenInfo(CLuaDwWusesiTabenWnd.OriID)
    self:InitTargetInfo(nil)
end

function CLuaDwWusesiTabenWnd:OnOkButtonClick( go) 
    if self.m_TargetId == nil then
        g_MessageMgr:ShowMessage("Taben_Equip_Material_Not_Exist")
    else
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("WUSESI_TABEN_CONFIRM"), 
            DelegateFactory.Action(function() self:DoTaben() end),nil, nil, nil, false)
    end
end
function CLuaDwWusesiTabenWnd:DoTaben( )
    local s = CItemMgr.Inst:GetItemInfo(CLuaDwWusesiTabenWnd.OriID)
    local t = CItemMgr.Inst:GetItemInfo(self.m_TargetId)
	--发送服务器
    Gac2Gas.RequestCombianWuSeSiAndBangle(s.pos,s.itemId,t.pos,t.itemId)
    CUIManager.CloseUI("DwWusesiTabenWnd")
end

function CLuaDwWusesiTabenWnd:InitTabenInfo(itemId)

	local ring = CItemMgr.Inst:GetById(itemId)
	if ring == nil then return end

    local transform = FindChild(self.transform,"WeddingRing")
    local icon = transform:Find("Texture"):GetComponent(typeof(CUITexture))
    local quality = transform:Find("quality"):GetComponent(typeof(UISprite))
    local bind = transform:Find("Bind"):GetComponent(typeof(UISprite))
    local name = transform:Find("Name"):GetComponent(typeof(UILabel))

    

    icon:LoadMaterial(ring.Icon)
	if ring.Equip ~= nil then
		quality.spriteName = CUICommonDef.GetItemCellBorder(ring.Equip.QualityType)
	end
    bind.spriteName = ring.BindOrEquipCornerMark
    name.text = ring.ColoredName
    

    UIEventListener.Get(transform.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        CItemInfoMgr.ShowLinkItemInfo(ring, false, nil, AlignType.Default, 0, 0, 0, 0, 0) 
    end)
end

function CLuaDwWusesiTabenWnd:InitTargetInfo(itemId)
    local transform = FindChild(self.transform,"AimRing")
    local icon = transform:Find("Texture"):GetComponent(typeof(CUITexture))
    local quality = transform:Find("quality"):GetComponent(typeof(UISprite))
    local bind = transform:Find("Bind"):GetComponent(typeof(UISprite))
    local add = transform:Find("Add").gameObject
    local name = transform:Find("Name"):GetComponent(typeof(UILabel))

    UIEventListener.Get(transform.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OpenEquipSelectWnd()
    end)

    if not itemId then
        icon.material = nil
        add:SetActive(true)
        quality.spriteName = CUICommonDef.GetItemCellBorder()
        bind.spriteName = ""
        name.text = "[c][acf8ff]"..LocalString.GetString("添加手镯")
        return
    end

    local ring = CItemMgr.Inst:GetById(itemId)
    add:SetActive(false)

    icon:LoadMaterial(ring.Icon)
    quality.spriteName = CUICommonDef.GetItemCellBorder(ring.Equip.QualityType)
    bind.spriteName = ring.BindOrEquipCornerMark
    name.text = ring.ColoredName
end

function CLuaDwWusesiTabenWnd:OpenEquipSelectWnd( )
    local equipList = CreateFromClass(MakeGenericClass(List, String))

    local bagSize = CClientMainPlayer.Inst.ItemProp:GetPlaceSize(EnumItemPlace.Bag)
    do
        local i = 1
        while i <= bagSize do
            local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
            if not (id==nil or id=="") then
                local equip = CItemMgr.Inst:GetById(id)
                local templateId = equip.TemplateId

                if equip ~= nil and equip.IsBinded and equip.IsEquip then
                    local template = EquipmentTemplate_Equip.GetData(templateId)
                    if template.Type == EnumBodyPosition_lua.Bracelet then
                        CommonDefs.ListAdd(equipList, typeof(String), equip.Id)
                    end
                end
            end
            i = i + 1
        end
    end
    if equipList.Count > 0 then
        CItemChooseMgr.Inst.ItemIds=equipList
        CItemChooseMgr.Inst.OnChoose=DelegateFactory.Action_string(function(itemId)
            self.m_TargetId = itemId
            local ring = CItemMgr.Inst:GetById(itemId)
            if not (ring and ring.IsEquip) then return end
        
            local equipment = EquipmentTemplate_Equip.GetData(ring.TemplateId)
            if not (equipment ~= nil and equipment.Type == EnumBodyPosition_lua.Bracelet) then
                return
            end

            self:InitTargetInfo(itemId)
        end)
        CItemChooseMgr.ShowWnd(LocalString.GetString("选择要制作拓本的手镯"))
    else
        g_MessageMgr:ShowMessage("WUSESI_TABEN_HAVE_NO_BRACELET")
    end
end
