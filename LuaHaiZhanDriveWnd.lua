local UICamera=import "UICamera"
local CCinemachineCtrl=import "L10.Game.CCinemachineCtrl"
local EasyTouch=import "EasyTouch"
local Collider=import "UnityEngine.Collider"
local KeyCode=import "UnityEngine.KeyCode"
local CCinemachineMgr=import "L10.Game.CCinemachineMgr"
local CCommonUIFxWnd=import "L10.UI.CCommonUIFxWnd"
local Ease = import "DG.Tweening.Ease"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local NGUIMath=import "NGUIMath"
local CStatusMgr = import "L10.Game.CStatusMgr"
local PlayerSettings=import "L10.Game.PlayerSettings"
local CPhysicsTimeMgr=import "L10.Engine.CPhysicsTimeMgr"
local CPhysicsSyncMgr=import "L10.Engine.CPhysicsSyncMgr"
local EPropStatus = import "L10.Game.EPropStatus"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Color=import "UnityEngine.Color"
local CCustomHpAndMpBar=import "L10.UI.CCustomHpAndMpBar"
local CCinemachineMgr=import "L10.Game.CCinemachineMgr"
local MeshRenderer = import "UnityEngine.MeshRenderer"
local Renderer = import "UnityEngine.Renderer"
local CPhysicsObjectMgr=import "L10.Engine.CPhysicsObjectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CEffectMgr = import "L10.Game.CEffectMgr"
local CMainCamera = import "L10.Engine.CMainCamera"
local CClientMonster=import "L10.Game.CClientMonster"
local CClientObjectMgr=import "L10.Game.CClientObjectMgr"
local Component=import "UnityEngine.Component"
local Transform = import "UnityEngine.Transform"
local CSkillButtonInfo=import "L10.UI.CSkillButtonInfo"
local Vector2 = import "UnityEngine.Vector2"
local Vector3=import "UnityEngine.Vector3"
local Screen=import "UnityEngine.Screen"
local Input=import "UnityEngine.Input"
local Plane=import "UnityEngine.Plane"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local WindowsShortCutKey=import "L10.Engine.WindowsShortCutKey"
local GameObject = import "UnityEngine.GameObject"
local Time=import "UnityEngine.Time"
local QnRadioBox = import "L10.UI.QnRadioBox"

local CMainCamera  =import "L10.Engine.CMainCamera"
local BoxCollider2D=import "UnityEngine.BoxCollider2D"
local PrimitiveType=import "UnityEngine.PrimitiveType"
local CRenderScene=import "L10.Engine.Scene.CRenderScene"
local MeshRenderer=import "UnityEngine.MeshRenderer"
local CPhysicsInputMgr=import "L10.Engine.CPhysicsInputMgr"
local CCinemachineCtrl_HaiZhan =import "L10.Game.CCinemachineCtrl_HaiZhan"
local CUIGameObjectPool=import "L10.UI.CUIGameObjectPool"
local UILongPressButton=import "L10.UI.UILongPressButton"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"

LuaHaiZhanDriveWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHaiZhanDriveWnd, "DriveSkills", "DriveSkills", Transform)
RegistChildComponent(LuaHaiZhanDriveWnd, "BattleSkills", "BattleSkills", Transform)
RegistChildComponent(LuaHaiZhanDriveWnd, "PickSkills", "PickSkills", Transform)
RegistChildComponent(LuaHaiZhanDriveWnd, "LeftButton", "LeftButton", GameObject)
RegistChildComponent(LuaHaiZhanDriveWnd, "RightButton", "RightButton", GameObject)
RegistChildComponent(LuaHaiZhanDriveWnd, "Arrow", "Arrow", GameObject)

RegistChildComponent(LuaHaiZhanDriveWnd, "Joystick", "Joystick", GameObject)
RegistChildComponent(LuaHaiZhanDriveWnd, "Joystick2", "Joystick2", GameObject)
RegistChildComponent(LuaHaiZhanDriveWnd, "TargetMark", "TargetMark", GameObject)
RegistChildComponent(LuaHaiZhanDriveWnd, "HeadPool", "HeadPool", CUIGameObjectPool)
RegistChildComponent(LuaHaiZhanDriveWnd, "Heads", "Heads", Transform)
RegistChildComponent(LuaHaiZhanDriveWnd, "TargetLeft", "TargetLeft", UIWidget)
RegistChildComponent(LuaHaiZhanDriveWnd, "TargetRight", "TargetRight", UIWidget)
RegistChildComponent(LuaHaiZhanDriveWnd, "TargetCenter", "TargetCenter", UIWidget)
RegistChildComponent(LuaHaiZhanDriveWnd, "DragArea", "DragArea", GameObject)
RegistChildComponent(LuaHaiZhanDriveWnd, "Rudder", "Rudder", Transform)
RegistChildComponent(LuaHaiZhanDriveWnd, "Rudder1", "Rudder1", UIWidget)
RegistChildComponent(LuaHaiZhanDriveWnd, "Rudder2", "Rudder2", UIWidget)
RegistChildComponent(LuaHaiZhanDriveWnd, "TargetInvalid", "TargetInvalid", GameObject)
RegistChildComponent(LuaHaiZhanDriveWnd, "TargetValid", "TargetValid", GameObject)
RegistChildComponent(LuaHaiZhanDriveWnd, "BattleSkillArrow", "BattleSkillArrow", Transform)
RegistChildComponent(LuaHaiZhanDriveWnd, "TmpSkillButton", "TmpSkillButton", GameObject)
RegistChildComponent(LuaHaiZhanDriveWnd, "TmpSkill", "TmpSkill", Transform)
RegistChildComponent(LuaHaiZhanDriveWnd, "StartPickButton", "StartPickButton", GameObject)
RegistChildComponent(LuaHaiZhanDriveWnd, "PickButton", "PickButton", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaHaiZhanDriveWnd, "m_DriveTick")
RegistClassMember(LuaHaiZhanDriveWnd, "m_IsPressedLeft")
RegistClassMember(LuaHaiZhanDriveWnd, "m_IsPressedRight")
RegistClassMember(LuaHaiZhanDriveWnd, "m_RotateSpeed")
RegistClassMember(LuaHaiZhanDriveWnd, "m_LastMoveDir")
RegistClassMember(LuaHaiZhanDriveWnd, "m_Plane")
RegistClassMember(LuaHaiZhanDriveWnd, "m_IsDriver")
RegistClassMember(LuaHaiZhanDriveWnd, "m_ZhanChuanTemplateId")
RegistClassMember(LuaHaiZhanDriveWnd, "m_DriveSkillRadioBox")
RegistClassMember(LuaHaiZhanDriveWnd, "m_DriveSpeedButton")
RegistClassMember(LuaHaiZhanDriveWnd, "m_BattleSkillRadioBox")
RegistClassMember(LuaHaiZhanDriveWnd, "m_BattleSkillIdx")
RegistClassMember(LuaHaiZhanDriveWnd, "m_RotateAngle")
RegistClassMember(LuaHaiZhanDriveWnd, "m_ArrowTarget")
RegistClassMember(LuaHaiZhanDriveWnd, "m_FxGo")
RegistClassMember(LuaHaiZhanDriveWnd, "m_CDSprites")
RegistClassMember(LuaHaiZhanDriveWnd, "m_CanShot")
RegistClassMember(LuaHaiZhanDriveWnd, "m_CameraTick")
RegistClassMember(LuaHaiZhanDriveWnd, "m_IsFreeze")
RegistClassMember(LuaHaiZhanDriveWnd, "m_BattleFireButton")

RegistClassMember(LuaHaiZhanDriveWnd, "m_SpeedCmdCDFrame")
RegistClassMember(LuaHaiZhanDriveWnd, "m_DriveButtons")
RegistClassMember(LuaHaiZhanDriveWnd, "m_DriveButtonCDSprites")
RegistClassMember(LuaHaiZhanDriveWnd, "m_DriveButtonColliders")

function LuaHaiZhanDriveWnd:Awake()
    self.m_SpeedCmdCDFrame = {}
    --0~3
    for i = 1, 4 do
        self.m_SpeedCmdCDFrame[i-1] =  Physics_SpeedCmd.GetData(i-1).cdFrame
    end
   

    self.TmpSkillButton:SetActive(false)
    self.m_IsFreeze = false
    --舵的显示
    self.Rudder1.enabled = true
    self.Rudder2.enabled = false

    self.m_BattleSkillIdx = 0
    self.m_HeadItems = {}
    self.m_ArrowTarget = 0
    self.m_RotateAngle = 0
    self.m_ZhanChuanTemplateId = 18007144
    self.m_Plane = Plane(Vector3.up,Vector3(0,6.6,0))
    self.TargetMark:SetActive(false)
    self.TargetValid:SetActive(false)
    self.TargetInvalid:SetActive(false)

    self.m_BattleFireButton = self.BattleSkills:Find("1stButton").gameObject
    UIEventListener.Get(self.m_BattleFireButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:CastBattleSkill(1)
    end)


    self.m_DriveSkillRadioBox = self.DriveSkills.gameObject:GetComponent(typeof(QnRadioBox))
    self.m_DriveSpeedButton = self.DriveSkills:Find("1stButton").gameObject
    UIEventListener.Get(self.m_DriveSpeedButton).onClick = DelegateFactory.VoidDelegate(function(g)
        self:CastDriveSkill(1)
    end)

    --行驶的4个按钮，1~4
    self.m_DriveButtons = {}
    for i = 1,3 do
        self.m_DriveButtons[i] = self.m_DriveSkillRadioBox.m_RadioButtons[i-1]
    end
    self.m_DriveButtons[4] = self.m_DriveSpeedButton
    self.m_DriveButtonCDSprites = {}
    self.m_DriveButtonColliders = {}
    for i = 1, 4 do
        self.m_DriveButtonCDSprites[i] = self.m_DriveButtons[i].transform:Find("Mask/CD"):GetComponent(typeof(UISprite))
        self.m_DriveButtonColliders[i] = self.m_DriveButtons[i]:GetComponent(typeof(Collider))
    end


    self.StartPickButton:SetActive(true)
    self.PickButton:SetActive(false)
    UIEventListener.Get(self.StartPickButton).onClick = DelegateFactory.VoidDelegate(function(g)
        CUIManager.ShowUI(CLuaUIResources.HaiZhanPickWnd)
        self.StartPickButton:SetActive(false)
        self.PickButton:SetActive(true)
    end)
    UIEventListener.Get(self.PickButton).onClick = DelegateFactory.VoidDelegate(function(g)
        self.StartPickButton:SetActive(true)
        self.PickButton:SetActive(false)
	    g_ScriptEvent:BroadcastInLua("HaiZhanPickMat")
    end)

    local btns = self.m_DriveSkillRadioBox.m_RadioButtons
    for i=1,btns.Length do
        local btn = btns[i-1]
        btn.OnButtonSelected_02 = DelegateFactory.Action_QnSelectableButton_bool(function(btn,b)
            local icon = FindChild(btn.transform,"Icon"):GetComponent(typeof(UITexture))
            icon.alpha = b and 1 or 0.5
        end)
    end

    self.m_BattleSkillRadioBox =  self.BattleSkills.gameObject:GetComponent(typeof(QnRadioBox))
    local btns = self.m_BattleSkillRadioBox.m_RadioButtons
    local ids = NavalWar_Setting.GetData().PlayerCannonTips
    for i=1,btns.Length do
        local btn = btns[i-1]
        local cmp = btn:GetComponent(typeof(UILongPressButton))
        cmp.OnLongPressDelegate = DelegateFactory.Action(function()
            local id = ids[i-1]
            local b1 = NGUIMath.CalculateRelativeWidgetBounds(btn.transform)
            local height = b1.size.y
            local worldCenterY = btn.transform:TransformPoint(b1.center).y
            local width = b1.size.x
            local worldCenterX = btn.transform:TransformPoint(b1.center).x
            CItemInfoMgr.ShowLinkItemTemplateInfo(id, false, nil, AlignType.Top,worldCenterX, worldCenterY, width, height)
        end)
    end


    if LuaHaiZhanMgr.IsPvpPlay() then
        for i = 1, 5 do
            local item = self.m_BattleSkillRadioBox.m_RadioButtons[i-1]
            local icon = FindChild(item.transform,"Icon"):GetComponent(typeof(CUITexture))
            icon:LoadMaterial(Item_Item.GetData(ids[i-1]).Icon)
        end
    else
        local btnPos = {{-329,74},{-292,232},{-137,313}}
        self.m_BattleSkillRadioBox:SetVisibleGroup(Table2Array({0,1,2},MakeArrayClass(int)),false)
        for i = 1, 3 do
            local item = self.m_BattleSkillRadioBox.m_RadioButtons[i-1]
            item.transform.localPosition = Vector3(btnPos[i][1],btnPos[i][2],0)
            local icon = FindChild(item.transform,"Icon"):GetComponent(typeof(CUITexture))
            icon:LoadMaterial(Item_Item.GetData(ids[i-1]).Icon)
        end
    end

    UIEventListener.Get(self.DragArea).onDrag = DelegateFactory.VectorDelegate(function(go,delta)
        local finger1 = EasyTouch.GetFingerStatic(0)
        local finger2 = EasyTouch.GetFingerStatic(1)
        local finger = nil
        if finger1 and finger1.pickedObject == self.DragArea then
            finger = finger1
        elseif finger2 and finger2.pickedObject == self.DragArea then
            finger = finger2
        end

        if not finger then return end

        local screenPos = finger.position
        screenPos = Vector3(screenPos.x,screenPos.y,0)

        local wp = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)
        local relPos = self.DragArea.transform:InverseTransformPoint(wp)

        relPos.y = math.max(0,relPos.y)
        local angle = math.atan(relPos.x/relPos.y)*180/math.pi
        self.m_RotateAngle = -angle
        if angle==0 then return end
        CPhysicsInputMgr.Inst.inputLevel = self:Angle2RotateLevel(angle)
        if relPos.x>0 then
            CPhysicsInputMgr.Inst.m_IsPressedLeft_View = false
            CPhysicsInputMgr.Inst.m_IsPressedRight_View = true
        elseif relPos.x<0 then
            CPhysicsInputMgr.Inst.m_IsPressedLeft_View = true
            CPhysicsInputMgr.Inst.m_IsPressedRight_View = false
        end
    end)
    UIEventListener.Get(self.DragArea).onPress = DelegateFactory.BoolDelegate(function (go, isPressed)
        if isPressed then
            local screenPos = nil
            if CommonDefs.IsInMobileDevice() then
                if not UICamera.currentTouch then return end
                local pos =  UICamera.currentTouch.pos
                screenPos = Vector3(pos.x,pos.y,0)
            else
                screenPos = Input.mousePosition
            end

            local wp = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)
            local relPos = self.DragArea.transform:InverseTransformPoint(wp)
            local angle = math.atan(relPos.x/relPos.y)*180/math.pi
            self.m_RotateAngle = -angle
            if angle==0 then return end
            CPhysicsInputMgr.Inst.inputLevel = self:Angle2RotateLevel(angle)
            if relPos.x>0 then
                CPhysicsInputMgr.Inst.m_IsPressedLeft_View = false
                CPhysicsInputMgr.Inst.m_IsPressedRight_View = true
            elseif relPos.x<0 then
                CPhysicsInputMgr.Inst.m_IsPressedLeft_View = true
                CPhysicsInputMgr.Inst.m_IsPressedRight_View = false
            end

            self.Rudder1.enabled = false
            self.Rudder2.enabled = true
        else
            self.m_RotateAngle = 0

            CPhysicsInputMgr.Inst.m_IsPressedLeft_View = false
            CPhysicsInputMgr.Inst.m_IsPressedRight_View = false

            self.Rudder1.enabled = true
            self.Rudder2.enabled = false

            
            if self.m_ArrowTarget ~= 0 then
                if CGuideMgr.Inst:IsInPhase(EnumGuideKey.HaiZhan_2) and CGuideMgr.Inst:IsInSubPhase(0) then -- 转动船舵引导
                    CGuideMgr.Inst:NextSubPhase()
                end
            end
        end
    end)
    
    self.m_RotateSpeed = NavalWar_Setting.GetData().RudderSpeed
    self.m_LastMoveDir = 0

    if CRenderScene.Inst and CRenderScene.Inst.TerrainFast then
        if CRenderScene.Inst.TerrainFast.transform.childCount>0 then
            local terrain = CRenderScene.Inst.TerrainFast.transform:GetChild(0)
            terrain:GetComponent(typeof(MeshRenderer)).enabled = false
        end
    end
end
function LuaHaiZhanDriveWnd:Angle2RotateLevel(angle)
    angle = math.abs(angle)
    if angle >0 and angle<=22.5 then
        return 1
    elseif angle>22.5 and angle<=45 then
        return 2
    elseif angle>45 and angle<=67.5 then
        return 3
    else
        return 4
    end
end


function LuaHaiZhanDriveWnd:Start()
    self.m_DriveSkillRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,i)
        self:CastDriveSkill(i+2)
    end)
    self.m_BattleSkillRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,i)
        self:CastBattleSkill(i+2)
    end)
end

function LuaHaiZhanDriveWnd:DoChangeSpeedAni()
    local driverId = CClientMainPlayer.Inst.AppearanceProp.DriverId
    if not g_PhysicsObjectHandlers[driverId] then return end
    local po = g_PhysicsObjectHandlers[driverId].m_PhysicsObject
    if po and po.RO then
        local ro = po.RO
        ro.SepAniEndCallback = nil
        ro:SetSepSkeleton("Bone02",0.15)
        ro:DoSepAni("zhuandong01",false,0,1,0.2,true,1)
        ro.SepAniEndCallback = DelegateFactory.Action(function()
            ro:SetSepSkeleton(nil,0.15)
            ro.SepAniEndCallback = nil
        end)
    end
end

function LuaHaiZhanDriveWnd:Init()
    self:OnSyncNavalWarSeatInfo(LuaHaiZhanMgr.s_SeatInfo)

    local driverId = 0
    if CClientMainPlayer.Inst then
        driverId = math.abs(CClientMainPlayer.Inst.AppearanceProp.DriverId)
    end
    --现有的都创建一下
    local objs = {}
    CommonDefs.DictIterate(CPhysicsObjectMgr.Inst.objects,DelegateFactory.Action_object_object(function(key,val)
        if key ~= driverId then--自己不需要
            table.insert(objs,val.EngineId)
        end
    end))

    for index, engineId in ipairs(objs) do
        self:CreateHud(engineId)
    end
    self:OnSyncNavalWarMaterials(LuaHaiZhanMgr.s_CntInfo,LuaHaiZhanMgr.s_BoardCnt,LuaHaiZhanMgr.s_TmpSkills)

    if LuaHaiZhanMgr:IsInTrainingGameplay() then
        RegisterTickOnce(function()
            if not CUIManager.IsLoaded(CUIResources.JuQingDialogWnd) then
                EventManager.BroadcastInternalForLua(EnumEventType.Guide_ChangeView, {"UIRoot"})
            end
        end, 33)
    end
end

--@region UIEvent
function LuaHaiZhanDriveWnd:RegisterMoveTick()
    if self.m_DriveTick then return end

    -- print("RegisterMoveTick")
    -- self.m_DriveTick = RegisterTick(function()
    --     self:DoMove()
    -- end,2000)
end
function LuaHaiZhanDriveWnd:UnregisterMoveTick()
    if self.m_DriveTick then
        UnRegisterTick(self.m_DriveTick)
        self.m_DriveTick = nil
    end
end
function LuaHaiZhanDriveWnd:DoMove()
    local player = CClientMainPlayer.Inst
    if player then
        local v = math.pi/180*player.Dir
        local dir = Vector3(math.cos( v ), 0, math.sin( v ));
        -- print("OnPlayerDragJoyStick")
        player:StopMove()
        self.m_LastMoveDir = player.Dir
        player:OnPlayerDragJoyStick(dir)
    end
end

function LuaHaiZhanDriveWnd:HandleInput()
    if LuaHaiZhanMgr.s_MyRole==1 then
        local isFreeze = true
        if CClientMainPlayer.Inst then
            local driverId = CClientMainPlayer.Inst.AppearanceProp.DriverId
            local po = g_PhysicsObjectHandlers[driverId]
            if po then
                isFreeze = po.m_Core.m_IsFreeze
            end
        end
        if not isFreeze then
            if Input.GetKeyDown(KeyCode.Alpha1) then
                self:CastDriveSkill(1)
            elseif Input.GetKeyDown(KeyCode.Alpha2) then
                self.m_DriveSkillRadioBox:ChangeTo(0,true)
            elseif Input.GetKeyDown(KeyCode.Alpha3) then
                self.m_DriveSkillRadioBox:ChangeTo(1,true)
            elseif Input.GetKeyDown(KeyCode.Alpha4) then
                self.m_DriveSkillRadioBox:ChangeTo(2,true)
            end
        end
    elseif LuaHaiZhanMgr.s_MyRole==2 then
        if Input.GetKeyDown(KeyCode.Alpha1) then
            self:CastBattleSkill(1)
        elseif Input.GetKeyDown(KeyCode.Alpha2) then
            self.m_BattleSkillRadioBox:ChangeTo(0,true)
        elseif Input.GetKeyDown(KeyCode.Alpha3) then
            self.m_BattleSkillRadioBox:ChangeTo(1,true)
        elseif Input.GetKeyDown(KeyCode.Alpha4) then
            self.m_BattleSkillRadioBox:ChangeTo(2,true)
        else
            if LuaHaiZhanMgr.IsPvpPlay() then
                if Input.GetKeyDown(KeyCode.Alpha5) then
                    self.m_BattleSkillRadioBox:ChangeTo(3,true)
                elseif Input.GetKeyDown(KeyCode.Alpha6) then
                    self.m_BattleSkillRadioBox:ChangeTo(4,true)
                end
            end
        end
    end
end
function LuaHaiZhanDriveWnd:CastDriveSkill(idx)
    local go = nil
    if idx==1 then
        go = self.m_DriveSpeedButton
    else
        go = self.m_DriveSkillRadioBox.m_RadioButtons[idx-2]
    end
    if not go:GetComponent(typeof(Collider)).enabled then return end

    if idx==1 then
        if LuaHaiZhanMgr.s_MyRole==1 then
            local frame = LuaHaiZhanMgr.s_CmdCDInfo[3] or 0
            local curFrame = CPhysicsSyncMgr.Inst.frameId
            if curFrame>frame then
                CPhysicsInputMgr.Inst:SetSpeedCmd(3)
                g_MessageMgr:ShowMessage("NAVALWAR_CHANGE_SPEED_RHSH")
                CCommonUIFxWnd.AddUIFx(89000161, 5, true, false)
                self:DoChangeSpeedAni()
            end
        end
    else
        --2、3、4转成0、1、2
        local i = idx-2
        local driverId = CClientMainPlayer.Inst.AppearanceProp.DriverId
        if not g_PhysicsObjectHandlers[driverId] then return end

		if 0 <= i and i <= 2 then
            local frame = LuaHaiZhanMgr.s_CmdCDInfo[i] or 0
            local curFrame = CPhysicsSyncMgr.Inst.frameId
            if curFrame>frame then
			    CPhysicsInputMgr.Inst:SetSpeedCmd(i)
            end
        end
        self:DoChangeSpeedAni()

        if i==0 then
            g_MessageMgr:ShowMessage("NAVALWAR_CHANGE_SPEED_STOP")
        elseif i==1 then
            g_MessageMgr:ShowMessage("NAVALWAR_CHANGE_SPEED_HALF")
        elseif i==2 then
            g_MessageMgr:ShowMessage("NAVALWAR_CHANGE_SPEED_FULL")
        end
    end
end

function LuaHaiZhanDriveWnd:GetPlayerCannonIds()
    if LuaHaiZhanMgr.IsPvpPlay() then
        return NavalWar_Setting.GetData().PvpPlayerCannonIds
    else
        return NavalWar_Setting.GetData().PlayerCannonIds
    end
end
function LuaHaiZhanDriveWnd:CastBattleSkill(idx)
    if idx==1 then
        if not self.m_BattleFireButton:GetComponent(typeof(Collider)).enabled then return end
        if not self.m_CanShot then
            g_MessageMgr:ShowMessage("HaiZhan_Leave_Shooting_Range")
            return
        end
        local ids = self:GetPlayerCannonIds()
        local id = ids[self.m_BattleSkillRadioBox.CurrentSelectIndex*2]
        if id then
            LuaHaiZhanMgr.SendBullet(id)
        end
    else
        --2、3、4转成0、1、2
        local i = idx-2
        if self.m_BattleSkillIdx~=i then
            self.m_BattleSkillIdx = i
            if i==0 then
                g_MessageMgr:ShowMessage("HaiZhan_Change_Bullet_1")
            elseif i==1 then
                g_MessageMgr:ShowMessage("HaiZhan_Change_Bullet_2")
            elseif i==2 then
                g_MessageMgr:ShowMessage("HaiZhan_Change_Bullet_3")
            elseif i==3 then
                g_MessageMgr:ShowMessage("HaiZhan_Change_Bullet_4")
            elseif i==4 then
                g_MessageMgr:ShowMessage("HaiZhan_Change_Bullet_5")
            end
        end
        local btns = self.m_BattleSkillRadioBox.m_RadioButtons
        local pos1 = btns[i].transform.position
        local pos2 = self.BattleSkillArrow.position
        self.BattleSkillArrow.transform.right=Vector3(pos1.x-pos2.x,pos1.y-pos2.y,0)

        local cameraCtrl = nil
        local go = CCinemachineMgr.GetActiveCamera()
        if go then
            cameraCtrl = go:GetComponent(typeof(CCinemachineCtrl_HaiZhan))
        end
        if cameraCtrl then
            if i==2 then
                if self.m_CameraTick then LuaTweenUtils.Kill(self.m_CameraTick, false) self.m_CameraTick=nil end
                self.m_CameraTick = LuaTweenUtils.TweenFloat(cameraCtrl.yOffset, 1.2, 0.5, function (val) cameraCtrl.yOffset = val end,
                    function() self.m_CameraTick=nil end,Ease.Linear)
            else
                if self.m_CameraTick then LuaTweenUtils.Kill(self.m_CameraTick, false) self.m_CameraTick=nil end
                self.m_CameraTick = LuaTweenUtils.TweenFloat(cameraCtrl.yOffset, 0.8, 0.5, function (val) cameraCtrl.yOffset = val end,
                    function() self.m_CameraTick=nil end,Ease.Linear)
            end
        end
    end
end

--@endregion UIEvent
local rudderAngle = 0
function LuaHaiZhanDriveWnd:Update()
    if not CClientMainPlayer.Inst then return end
    self:UpdateTargetSpeed()
    self:HandleInput()

    self:OnUpdateCooldown()
    --卡顿的时候，可能角度会跳变，所以这里做一个限制
    local deltaTime = math.min(Time.deltaTime*5,0.2)

    local v = LuaTransformAccess.GetLocalEulerAnglesZ(self.Arrow.transform)
    if v>180 then
        v=v-360
    end

    self.m_ArrowTarget = 0
    if self.m_RotateAngle~=0 then
        self.m_ArrowTarget = self.m_RotateAngle
    else
        local pressed = true
        if CPhysicsInputMgr.Inst:IsPressedLeft() then
            self.m_ArrowTarget = 22.5*CPhysicsInputMgr.Inst.inputLevel--22.5=90/4 分4档
        elseif CPhysicsInputMgr.Inst:IsPressedRight() then
            self.m_ArrowTarget = -22.5*CPhysicsInputMgr.Inst.inputLevel
        else
            pressed = false
            self.m_ArrowTarget = 0
        end
        if pressed and CGuideMgr.Inst:IsInPhase(EnumGuideKey.HaiZhan_2) and CGuideMgr.Inst:IsInSubPhase(0) then -- 转动船舵引导
            CGuideMgr.Inst:NextSubPhase()
        end
    end

    if v~=self.m_ArrowTarget then
        if self.m_ArrowTarget<0 then
            LuaTransformAccess.SetLocalEulerAngles(self.Arrow.transform,0,0,v*(1-deltaTime)+self.m_ArrowTarget*deltaTime)
        elseif self.m_ArrowTarget>0 then
            LuaTransformAccess.SetLocalEulerAngles(self.Arrow.transform,0,0,v*(1-deltaTime)+self.m_ArrowTarget*deltaTime)
        else
            if v>1 or v<-1 then
                LuaTransformAccess.SetLocalEulerAngles(self.Arrow.transform,0,0,v*(1-deltaTime*3))
            else
                LuaTransformAccess.SetLocalEulerAngles(self.Arrow.transform,0,0,0)
            end
        end
    end
    --右转
    
    if self.m_ArrowTarget >0 then
        rudderAngle = rudderAngle+1*CPhysicsInputMgr.Inst.inputLevel
        LuaTransformAccess.SetLocalEulerAngles(self.Rudder.transform,0,0,rudderAngle)
    elseif self.m_ArrowTarget <0 then
        rudderAngle = rudderAngle-1*CPhysicsInputMgr.Inst.inputLevel
        LuaTransformAccess.SetLocalEulerAngles(self.Rudder.transform,0,0,rudderAngle)
    else
        if rudderAngle>1 or rudderAngle<-1 then
            rudderAngle = rudderAngle*(1-deltaTime*3)
            LuaTransformAccess.SetLocalEulerAngles(self.Rudder.transform,0,0,rudderAngle)
        else
            --一定误差内回正
            rudderAngle = 0
            LuaTransformAccess.SetLocalEulerAngles(self.Rudder.transform,0,0,0)
        end
    end
    local driverId = CClientMainPlayer.Inst.AppearanceProp.DriverId
    local po = g_PhysicsObjectHandlers[driverId]
    if po then
        if po.m_Core.m_IsFreeze then
            if not self.m_IsFreeze then
                CUICommonDef.SetActive(self.DriveSkills.gameObject, false, true)
                self.m_IsFreeze = true
            end
        else
            if self.m_IsFreeze then
                CUICommonDef.SetActive(self.DriveSkills.gameObject, true, true)
                self.m_IsFreeze = false
            end
        end
    end

    local driver = CClientObjectMgr.Inst:GetObject(math.abs(driverId))
    --ro可能没有加载出来，需要等
    if driver and driver.m_PhysicsObject and driver.m_PhysicsObject.RO then
        local tf = driver.m_PhysicsObject.RO.transform

        local go = CCinemachineMgr.GetActiveCamera()
        if go then
            local ctrl = go:GetComponent(typeof(CCinemachineCtrl_HaiZhan))
            if ctrl.traceTarget ~=tf then
                ctrl:SetFollowObject(tf)
            end
        end
        --炮手显示弹道特效
        if LuaHaiZhanMgr.s_MyRole==2 then
            local fx = driver.m_PhysicsObject.RO:GetFxById(88804016)
            if not fx then
                fx = CEffectMgr.Inst:AddObjectFX(88804016, driver.m_PhysicsObject.RO, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, 
                    DelegateFactory.Action_GameObject(function(go)
                        self.m_FxGo = go
                    end))
                driver.m_PhysicsObject.RO:AddFX(88804016, fx)
            end
        else
            driver.m_PhysicsObject.RO:RemoveFX(88804016)
            self.m_FxGo = nil
        end

        --更新弹道特效的颜色
        if self.m_FxGo and not CommonDefs.IsNull(self.m_FxGo) and CMainCamera.Main then
            local dirx,diry,dirz = LuaTransformAccess.GetRight(CMainCamera.Main.transform)
            -- local v2 = Vector2(-dir.z,dir.x)--视角往前的向量
            local v2x,v2y = dirx,dirz

            local pos = self.m_FxGo.transform.position
            LuaTransformAccess.LookAt(self.m_FxGo.transform, pos.x+v2x, pos.y, pos.z+v2y)

            local forwardx,forwardy = -dirz,dirx
            local rightx,righty,rightz = LuaTransformAccess.GetRight(driver.m_PhysicsObject.transform)
            local dot = forwardx*rightx+forwardy*rightz

            local mat = self.m_FxGo.transform:GetChild(0):GetComponent(typeof(MeshRenderer)).sharedMaterial
            local mat2 = self.m_FxGo.transform:GetChild(1):GetComponent(typeof(Renderer)).sharedMaterial
            if math.abs( dot )<0.5 then--0~1
                self.m_CanShot = false
                LuaMaterialAccess.SetColor(mat,"_TintColor",202/255,12/255,0,100/255)
                LuaMaterialAccess.SetColor(mat,"_UVColor",82/255,43/255,41/255,40/255)
                LuaMaterialAccess.SetColor(mat2,"_TintColor",1,0,0,1)

                self.TargetValid:SetActive(false)
                self.TargetInvalid:SetActive(true)
            else
                self.m_CanShot = true
                LuaMaterialAccess.SetColor(mat,"_TintColor",0,1,0,1)
                LuaMaterialAccess.SetColor(mat,"_UVColor",41/255,82/255,73/255,40/255)
                LuaMaterialAccess.SetColor(mat2,"_TintColor",0,147/255,31/255,1)

                self.TargetValid:SetActive(true)
                self.TargetInvalid:SetActive(false)
            end
        end
    end
end

function LuaHaiZhanDriveWnd:OnEnable()
    CCinemachineCtrl_HaiZhan.joystick = self.DragArea

    WindowsShortCutKey.s_MaxRotateAngle=5
    WindowsShortCutKey.s_IsRudderMode=true
    CUIManager.instance:HideUI(InitializeList(CreateFromClass(MakeGenericClass(List, String)), String, "Joystick", "SkillButtonBoard"))
    g_ScriptEvent:AddListener("SyncNavalWarSeatInfo", self, "OnSyncNavalWarSeatInfo")

    g_ScriptEvent:AddListener("ClientObjCreate", self, "OnClientObjCreate")
    g_ScriptEvent:AddListener("ClientObjDestroy", self, "OnClientObjDestroy")
    g_ScriptEvent:AddListener("CreateShip", self, "OnCreateShip")
    g_ScriptEvent:AddListener("SyncNavalWarMaterials",self,"OnSyncNavalWarMaterials")
    g_ScriptEvent:AddListener("OnRightMenuWndExpandedStatusChanged", self, "OnRightMenuWndExpandedStatusChanged")
    -- g_ScriptEvent:AddListener("Guide_ChangeView", self, "OnChangeView")

    g_ScriptEvent:AddListener("HaiZhanCameraDrag", self, "OnHaiZhanCameraDrag")
end
function LuaHaiZhanDriveWnd:OnDisable()
    CCinemachineCtrl_HaiZhan.joystick = nil
    CCinemachineMgr.CloseCamera("Cinemachine/haizhan.prefab")
    CCinemachineMgr.CloseCamera("Cinemachine/haizhan_aim.prefab")

    WindowsShortCutKey.s_MaxRotateAngle=180
    WindowsShortCutKey.s_IsRudderMode=false

    CUIManager.instance:ShowUI(InitializeList(CreateFromClass(MakeGenericClass(List, String)), String, "Joystick", "SkillButtonBoard"))
    g_ScriptEvent:RemoveListener("SyncNavalWarSeatInfo", self, "OnSyncNavalWarSeatInfo")

    g_ScriptEvent:RemoveListener("ClientObjCreate", self, "OnClientObjCreate")
    g_ScriptEvent:RemoveListener("ClientObjDestroy", self, "OnClientObjDestroy")
    g_ScriptEvent:RemoveListener("CreateShip", self, "OnCreateShip")
    g_ScriptEvent:RemoveListener("SyncNavalWarMaterials",self,"OnSyncNavalWarMaterials")
    g_ScriptEvent:RemoveListener("OnRightMenuWndExpandedStatusChanged", self, "OnRightMenuWndExpandedStatusChanged")
    -- g_ScriptEvent:RemoveListener("Guide_ChangeView", self, "OnChangeView")
    
    g_ScriptEvent:RemoveListener("HaiZhanCameraDrag", self, "OnHaiZhanCameraDrag")
end

function LuaHaiZhanDriveWnd:OnHaiZhanCameraDrag()
    if CGuideMgr.Inst:IsInPhase(EnumGuideKey.HaiZhan_5) and CGuideMgr.Inst:IsInSubPhase(0) then -- 滑动屏幕引导
        CGuideMgr.Inst:NextSubPhase()
    end
end

-- function LuaHaiZhanDriveWnd:OnChangeView(args)
--     -- print("OnChangeView",args[0])
-- end
function LuaHaiZhanDriveWnd:OnRightMenuWndExpandedStatusChanged()
    if PlayerSettings.RightMenuExpanded then
        self.transform:Find("BottomRight").gameObject:SetActive(false)
    else
        self.transform:Find("BottomRight").gameObject:SetActive(true)
    end
end

function LuaHaiZhanDriveWnd:OnSyncNavalWarMaterials(cntInfo,boardCnt,tmpSkills)
    for i = 1, 5 do
        self.m_BattleSkillRadioBox:SetTitle(i-1,"0")
    end
    if cntInfo then
        local lookup = {}
        local ids = self:GetPlayerCannonIds()
        for i = 1, ids.Length,2 do
            lookup[ids[i-1]] = (i-1)/2+1
        end
        for key, value in pairs(cntInfo) do
            if lookup[key] then
                self.m_BattleSkillRadioBox:SetTitle(lookup[key]-1,tostring(value))
            end
        end
    end
    if tmpSkills then
        local cnt = 0
        local list = {}
        for key, value in pairs(tmpSkills) do
            --大于0才显示
            if value>0 then
                cnt = cnt+1
                table.insert(list,key)
            end
        end
        local curCnt = self.TmpSkill.childCount
        if cnt>curCnt then
            --需要新建
            for i = 1, cnt-curCnt do
                local go = NGUITools.AddChild(self.TmpSkill.gameObject,self.TmpSkillButton)
            end
            self.TmpSkill:GetComponent(typeof(UIGrid)):Reposition()
        end
        for index = 1, self.TmpSkill.childCount do
            local value = self.TmpSkill:GetChild(index-1).gameObject
            if index>cnt then
                value:SetActive(false)
            else
                value:SetActive(true)
                local id = list[index]
                local icon = value.transform:Find("Icon"):GetComponent(typeof(CUITexture))
                icon:LoadSkillIcon(Skill_AllSkills.GetData(id).SkillIcon)
                local label = value.transform:Find("Label"):GetComponent(typeof(UILabel))
                label.text = tostring(tmpSkills[id])
                UIEventListener.Get(value).onClick = DelegateFactory.VoidDelegate(function(go)
                    self:CastTmpSkill(id)
                end)
            end
        end
    else
        --全部隐藏
        for index = 1, self.TmpSkill.childCount do
            local value = self.TmpSkill:GetChild(index-1).gameObject
            value:SetActive(false)
        end
    end
end

function LuaHaiZhanDriveWnd:CastTmpSkill(id)
    local dir = CMainCamera.Main.transform.right
    local v = Vector2(-dir.z,dir.x).normalized
    local dis=10
    local driverId = CClientMainPlayer.Inst.AppearanceProp.DriverId
    local po = g_PhysicsObjectHandlers[driverId]
    if po then
        local pos = po.m_PhysicsObject.transform.position
        local x,y = pos.x+v.x*dis,pos.z+v.y*dis
        Gac2Gas.RequestCastNavalTempSkill(id,math.floor(x*64),math.floor(y*64))
    end
end

local lastIdx = 0
function LuaHaiZhanDriveWnd:UpdateTargetSpeed()
    local driverId = CClientMainPlayer.Inst.AppearanceProp.DriverId
    if not g_PhysicsObjectHandlers[driverId] then return end

    local po = g_PhysicsObjectHandlers[driverId]
    local tid = po.m_Core.m_TemplateId
    local speed = Physics_Object.GetData(tid).speed
    local targetSpeed = po.m_Core.m_TargetSpeed
    local idx = 0
    --浮点数有一定的误差
    if targetSpeed>=speed-0.000001 then
        idx = 2
    elseif targetSpeed>=speed/2-0.000001 then
        idx = 1
    else
        idx = 0
    end
    if idx~=lastIdx then
        self.m_DriveSkillRadioBox:ChangeTo(idx,false)
        lastIdx = idx
    end
end

function LuaHaiZhanDriveWnd:OnCreateShip(physicsObjectId)
    self:CreateHud(math.abs(physicsObjectId))
end

function LuaHaiZhanDriveWnd:OnClientObjCreate(args)
    local engineId = args[0]
    self:CreateHud(engineId)
end
-- 自己不显示血条
-- 目前只需要显示怪物船的血条
function LuaHaiZhanDriveWnd:CreateHud(engineId)
    local obj = CClientObjectMgr.Inst:GetObject(engineId)
    if not obj then return end
    if not TypeIs(obj,typeof(CClientMonster)) then return end

    if self.Heads.transform:Find(tostring(engineId)) then return end

    local design = NavalWar_NameVersion.GetData(obj.TemplateId)
    if not design then return end

    local type=design.NameBoardType
    if design.NameBoardType==3 then type=0 end
    local item = self.HeadPool:GetFromPool(type)
    item.name = tostring(engineId)
    item.transform.parent = self.Heads.transform
    item.transform.localScale = Vector3.one
    item:SetActive(true)

    local luaScript = CommonDefs.GetComponent_GameObject_Type(item, typeof(CCommonLuaScript))
    if luaScript then luaScript:Init({engineId,obj.TemplateId}) end
end
function LuaHaiZhanDriveWnd:OnClientObjDestroy(args)
    local engineId = args[0]

    local tf = self.Heads:Find(tostring(engineId))
    if tf then
        self.HeadPool:Recycle(tf.gameObject)
    end
end

function LuaHaiZhanDriveWnd:UpdateDriveColldown(curFrame,idx)
    local frame = LuaHaiZhanMgr.s_CmdCDInfo[idx] or 0
    local cd = self.m_SpeedCmdCDFrame[idx]
    if cd==0 then return end

    local sprite = self.m_DriveButtonCDSprites[idx+1]
    local collider = self.m_DriveButtonColliders[idx+1]
    if curFrame>=frame then
        sprite.fillAmount = 0
    elseif curFrame<frame then
        local fillAmount = (frame-curFrame)/cd
        sprite.fillAmount = fillAmount
    end
    if sprite.fillAmount==0 then
        if self.m_IsFreeze then
            collider.enabled = false
        else
            collider.enabled = true
        end
    else
        collider.enabled = false
    end
end
function LuaHaiZhanDriveWnd:OnUpdateCooldown()
    local myPos = LuaHaiZhanMgr.s_MyPos

    if myPos==1 then
        if self.DriveSkills.gameObject.activeSelf then
            local curFrame = CPhysicsSyncMgr.Inst.frameId

            --刷新cd
            for i = 1, 4 do
                self:UpdateDriveColldown(curFrame,i-1)
            end
        end
        if LuaHaiZhanMgr.s_CDInfo and LuaHaiZhanMgr.s_CDInfo[1] then
            --临时技能的cd
            local cdInfo = LuaHaiZhanMgr.s_CDInfo[1]

            local timeStamp =CPhysicsTimeMgr.Inst.serverTime
            local time0 = cdInfo[1]
            local duration = cdInfo[2]

            local fillAmount = 0
            for index = 1, self.TmpSkill.childCount do
                local go = self.TmpSkill:GetChild(index-1).gameObject
                local sprite = go.transform:Find("CD"):GetComponent(typeof(UISprite))
                if timeStamp>time0 then
                    if timeStamp-time0<duration then
                        fillAmount = 1-(timeStamp-time0)/duration
                    else
                        fillAmount = 0
                    end
                else
                    fillAmount = 1
                end
                sprite.fillAmount = fillAmount

                if fillAmount==0 then
                    go:GetComponent(typeof(Collider)).enabled = true
                else
                    go:GetComponent(typeof(Collider)).enabled = false
                end
            end
        end
    end

    if myPos==2 or myPos==3 or myPos==4 then
        local cdIdx = myPos
        if LuaHaiZhanMgr.s_CDInfo and LuaHaiZhanMgr.s_CDInfo[cdIdx] then
            local cdInfo = LuaHaiZhanMgr.s_CDInfo[cdIdx]
            local timeStamp =CPhysicsTimeMgr.Inst.serverTime
            local time0 = cdInfo[1]
            local duration = cdInfo[2]

            local fillAmount = 0
            local go = self.m_BattleFireButton
            if go.activeSelf then
                local sprite = go.transform:Find("Mask/CD"):GetComponent(typeof(UISprite))
                if timeStamp>time0 then
                    if timeStamp-time0<duration then
                        fillAmount = 1-(timeStamp-time0)/duration
                    else
                        fillAmount = 0
                    end
                else
                    fillAmount = 1
                end
                sprite.fillAmount = fillAmount
            end
            if fillAmount==0 then
                go:GetComponent(typeof(Collider)).enabled = true
            else
                go:GetComponent(typeof(Collider)).enabled = false
            end
            self:UpdateTargetCooldown(1-fillAmount)
        end
    end
    if myPos==5 then
        if LuaHaiZhanMgr.s_CDInfo and LuaHaiZhanMgr.s_CDInfo[myPos] then
            local cdInfo = LuaHaiZhanMgr.s_CDInfo[myPos]
            local timeStamp =CPhysicsTimeMgr.Inst.serverTime
            local time0 = cdInfo[1]
            local duration = cdInfo[2]

            local fillAmount = 0
            local go = self.StartPickButton
            if go.activeSelf then
                local sprite = go.transform:Find("CD"):GetComponent(typeof(UISprite))
                if timeStamp>time0 then
                    if timeStamp-time0<duration then
                        fillAmount = 1-(timeStamp-time0)/duration
                    else
                        fillAmount = 0
                    end
                else
                    fillAmount = 1
                end
                sprite.fillAmount = fillAmount

                if fillAmount==0 then
                    go:GetComponent(typeof(Collider)).enabled = true
                else
                    go:GetComponent(typeof(Collider)).enabled = false
                end
            end
        end
    end
end
function LuaHaiZhanDriveWnd:UpdateTargetCooldown(amount)
    self.TargetLeft.fillAmount = amount
    self.TargetRight.fillAmount = amount
    self.TargetCenter.fillAmount = amount
    if amount<0.95 then
        self.TargetLeft.color = Color.yellow
        self.TargetRight.color = Color.yellow
        self.TargetCenter.color = Color.yellow
    else
        self.TargetLeft.color = Color.green
        self.TargetRight.color = Color.green
        self.TargetCenter.color = Color.green
    end
end

function LuaHaiZhanDriveWnd:OnGetInOffCombatVehicle()
    self:OnRideOnOffVehicle()
end

function LuaHaiZhanDriveWnd:OnSyncNavalWarSeatInfo(seatInfo)
    if not seatInfo then return end
    if not CClientMainPlayer.Inst then return end

    if LuaHaiZhanMgr.s_MyRole==1 then
        self.TargetMark:SetActive(false)

        self:RegisterMoveTick()
        
        self.DriveSkills.gameObject:SetActive(true)
        self.BattleSkills.gameObject:SetActive(false)
        self.PickSkills.gameObject:SetActive(false)
        self.Joystick:SetActive(true)
        self.Joystick2:SetActive(false)

        self:ChangeCamera("Cinemachine/haizhan.prefab")
    elseif LuaHaiZhanMgr.s_MyRole==2 then
        --hnpc395_yujing 攻击范围特效
        self.TargetMark:SetActive(true)
        
        self:UnregisterMoveTick()

        self.DriveSkills.gameObject:SetActive(false)
        self.BattleSkills.gameObject:SetActive(true)
        self.PickSkills.gameObject:SetActive(false)
        self.m_CDSprites = {}


        self.Joystick:SetActive(false)
        self.Joystick2:SetActive(false)

        local leftButton = self.Joystick2.transform:Find("LeftButton").gameObject
        local rightButton = self.Joystick2.transform:Find("RightButton").gameObject
        UIEventListener.Get(leftButton).onClick = DelegateFactory.VoidDelegate(function(go)
            local v = CameraFollow.Inst.targetRZY
            local v2 = Vector3(v.x+5,v.y,v.z)
            CameraFollow.Inst.targetRZY = v2
            CameraFollow.Inst.RZY = v2
            CameraFollow.Inst.CurCameraParam:ApplyRZY(v2)
        end)
        UIEventListener.Get(rightButton).onClick = DelegateFactory.VoidDelegate(function(go)
            local v = CameraFollow.Inst.targetRZY
            local v2 = Vector3(v.x-5,v.y,v.z)
            CameraFollow.Inst.targetRZY = v2
            CameraFollow.Inst.RZY = v2
            CameraFollow.Inst.CurCameraParam:ApplyRZY(v2)
        end)

        self:ChangeCamera("Cinemachine/haizhan_aim.prefab")
    else
        self.TargetMark:SetActive(false)
        self.DriveSkills.gameObject:SetActive(false)
        self.BattleSkills.gameObject:SetActive(false)
        if LuaHaiZhanMgr.s_MyRole==3 then
            self.StartPickButton:SetActive(true)
            self.PickButton:SetActive(false)
            self.PickSkills.gameObject:SetActive(true)
        else
            self.PickSkills.gameObject:SetActive(false)
        end
        self.Joystick:SetActive(false)
        self.Joystick2:SetActive(false)
        self:UnregisterMoveTick()

        self:ChangeCamera("Cinemachine/haizhan.prefab")
    end
end
function LuaHaiZhanDriveWnd:ChangeCamera(path)
    local loaded = CCinemachineMgr.GetCamera(path)
    local go = CCinemachineMgr.GetActiveCamera()
    --当前就是这个相机
    if loaded and go then
        if loaded==go then
            return
        end
    end
    local x,y
    if go then
        local ctrl = go:GetComponent(typeof(CCinemachineCtrl))
        if ctrl then
            x = ctrl.m_FreeCamera.m_XAxis.Value
            y = ctrl.m_FreeCamera.m_YAxis.Value
        end
    end

    CCinemachineMgr.OpenCamera(path,DelegateFactory.Action_object(function(obj)
        self:CameraSetFollowObject(x,y)
    end))
end

function LuaHaiZhanDriveWnd:CameraSetFollowObject(x,y)
    local go = CCinemachineMgr.GetActiveCamera()
    if go then
        local ctrl = go:GetComponent(typeof(CCinemachineCtrl_HaiZhan))
        if x and y then
            ctrl:SetCinemachineXYZ(x,y,-1)
        else
            --如果之前没有相机，则使用主角的方向
            local tf = CClientMainPlayer.Inst.RO.transform
            ctrl:SetCinemachineXYZ(tf.eulerAngles.y,0.4,-1)
        end
    end
end
function LuaHaiZhanDriveWnd:OnDestroy()
    if self.m_DriveTick then UnRegisterTick(self.m_DriveTick) self.m_DriveTick=nil end
    for i = self.Heads.childCount,1,-1 do
        self.HeadPool:Recycle(self.Heads:GetChild(i-1).gameObject)
    end
    if self.m_CameraTick then LuaTweenUtils.Kill(self.m_CameraTick, false) self.m_CameraTick=nil end
end

function LuaHaiZhanDriveWnd:GetGuideGo(methodName)
    if methodName == "Get1stButton" then
        return self.DriveSkills:Find("1stButton").gameObject
    elseif methodName == "Get2rdButton" then
        return self.DriveSkills:Find("2rdButton").gameObject
    elseif methodName == "Get3thButton" then
        return self.DriveSkills:Find("3thButton").gameObject
    elseif methodName == "Get4thButton" then
        return self.DriveSkills:Find("4thButton").gameObject
    elseif methodName == "GetJoystick" then
        return self.Joystick.transform:Find("DragArea").gameObject
    elseif methodName == "GetBattle1stButton" then
        return self.BattleSkills:Find("1stButton").gameObject
    elseif methodName == "GetBattle2rdButton" then
        return self.BattleSkills:Find("2rdButton").gameObject
    elseif methodName == "GetBattle3thButton" then
        return self.BattleSkills:Find("3thButton").gameObject
    elseif methodName == "GetBattle4thButton" then
        return self.BattleSkills:Find("4thButton").gameObject
    elseif methodName == "GetTargetMark" then
        return self.TargetMark
    end
    return nil
end