local UILabel = import "UILabel"
local QnRadioBox = import "L10.UI.QnRadioBox"
local GameObject = import "UnityEngine.GameObject"
local EnumClass = import "L10.Game.EnumClass"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local Constants = import "L10.Game.Constants"
local AlignType = import "CPlayerInfoMgr+AlignType"
local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"
local UITableTween = import "L10.UI.UITableTween"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"

LuaPengDaoRankWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaPengDaoRankWnd, "RadioBox", "RadioBox", QnRadioBox)
RegistChildComponent(LuaPengDaoRankWnd, "TabTemplate", "TabTemplate", GameObject)
RegistChildComponent(LuaPengDaoRankWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaPengDaoRankWnd, "CountDownLabel", "CountDownLabel", UILabel)
RegistChildComponent(LuaPengDaoRankWnd, "Item1", "Item1", GameObject)
RegistChildComponent(LuaPengDaoRankWnd, "Item2", "Item2", GameObject)
RegistChildComponent(LuaPengDaoRankWnd, "Item3", "Item3", GameObject)
RegistChildComponent(LuaPengDaoRankWnd, "Item4", "Item4", GameObject)
RegistChildComponent(LuaPengDaoRankWnd, "Item5", "Item5", GameObject)
RegistChildComponent(LuaPengDaoRankWnd, "Item6", "Item6", GameObject)
RegistChildComponent(LuaPengDaoRankWnd, "NoneRankLabel", "NoneRankLabel", UILabel)
RegistChildComponent(LuaPengDaoRankWnd, "MonthLabel", "MonthLabel", UILabel)
RegistChildComponent(LuaPengDaoRankWnd, "MonthTitleLabel", "MonthTitleLabel", UILabel)
RegistChildComponent(LuaPengDaoRankWnd, "RewardView", "RewardView", GameObject)
RegistChildComponent(LuaPengDaoRankWnd, "RefreshBtn", "RefreshBtn", GameObject)
RegistChildComponent(LuaPengDaoRankWnd, "HideRewardsLabel", "HideRewardsLabel", UILabel)
--@endregion RegistChildComponent end

RegistClassMember(LuaPengDaoRankWnd,"m_List")
RegistClassMember(LuaPengDaoRankWnd,"m_SelectClass")
RegistClassMember(LuaPengDaoRankWnd,"m_AwardId2CurObjs")
RegistClassMember(LuaPengDaoRankWnd,"m_Rank")
RegistClassMember(LuaPengDaoRankWnd,"m_IsLastMonth")

function LuaPengDaoRankWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    UIEventListener.Get(self.RefreshBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnRefreshBtnClicked()
    end)
    --@endregion EventBind end
end

function LuaPengDaoRankWnd:Init()
    self.CountDownLabel.text = ""
    self.HideRewardsLabel.gameObject:SetActive(false)
    self:InitMonth(false)
    self:InitClassRadioBox()
    self:InitAwardItems()
    self:InitTableView()
end

function LuaPengDaoRankWnd:InitClassRadioBox()
    self.TabTemplate.gameObject:SetActive(false)
    Extensions.RemoveAllChildren(self.RadioBox.m_Table.transform)
    local len = 13
	self.RadioBox.m_RadioButtons = CreateFromClass(MakeArrayClass(QnSelectableButton), len)
    local matPathArr = {
        "login_profession_new_sheshou","login_profession_new_jiashi","login_profession_new_daoke","login_profession_new_xiake",
        "login_profession_new_fangshi","login_profession_new_yishi","login_profession_new_meizhe","login_profession_new_yiren",
        "login_profession_new_yanshi","login_profession_new_huahun","login_profession_new_yingling","login_profession_new_dieke",
        "login_profession_new_zhankuang",
    }
    local texNameArr = {
        "pengdaorankwnd_zhiye_sheshou","pengdaorankwnd_zhiye_jiashi","pengdaorankwnd_zhiye_daoke",
        "pengdaorankwnd_zhiye_xiake","pengdaorankwnd_zhiye_fangshi","pengdaorankwnd_zhiye_yishi",
        "pengdaorankwnd_zhiye_meizhe","pengdaorankwnd_zhiye_yiren","pengdaorankwnd_zhiye_yanshi",
        "pengdaorankwnd_zhiye_huahun","pengdaorankwnd_zhiye_yingling","pengdaorankwnd_zhiye_dieke",
        "pengdaorankwnd_zhiye_zhankuang",
    }
    for i = 1, len do
        local go = NGUITools.AddChild(self.RadioBox.m_Table.gameObject, self.TabTemplate.gameObject)
        go:SetActive(true)
        go.transform:Find("Normal"):GetComponent(typeof(CUITexture)):LoadMaterial(SafeStringFormat3("UI/Texture/Transparent/Material/%s.mat",matPathArr[i]) )
        go.transform:Find("Label"):GetComponent(typeof(CUITexture)):LoadMaterial(SafeStringFormat3("UI/Texture/Transparent/Material/%s.mat",texNameArr[i]) )
        self.RadioBox.m_RadioButtons[i - 1] = go:GetComponent(typeof(QnSelectableButton))
		self.RadioBox.m_RadioButtons[i - 1].OnClick = MakeDelegateFromCSFunction(self.RadioBox.On_Click, MakeGenericClass(Action1, QnButton), self.RadioBox)
    end
    self.RadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnRadioBoxSelected(btn,index)
    end)
	self.RadioBox.m_Table:Reposition()
    local scrollView = self.RadioBox.gameObject:GetComponent(typeof(UIScrollView))
    scrollView:ResetPosition()
    self.RadioBox:ChangeTo(CClientMainPlayer.Inst and (EnumToInt(CClientMainPlayer.Inst.Class) - 1) or 0 , true)
end

function LuaPengDaoRankWnd:InitTableView()
    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_List
        end,
        function(item, index)
            item:SetBackgroundTexture(index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)

            self:InitItem(item, index)
        end
    )
	self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
		self:OnSelectAtRow(row)
    end)
end

function LuaPengDaoRankWnd:InitAwardItems()
    local itemArr = {{self.Item1, self.Item2},{self.Item3, self.Item4},{self.Item5, self.Item6}}
    local labelArr = {
        self.RewardView.transform:Find("Label1"):GetComponent(typeof(UILabel)),
        self.RewardView.transform:Find("Label2"):GetComponent(typeof(UILabel)),
        self.RewardView.transform:Find("Label3"):GetComponent(typeof(UILabel))
    }
    self.m_AwardId2CurObjs = {}
    PengDaoFuYao_Award.Foreach(function (k,v)
        labelArr[k].text = v.StartNum == v.EndNum and SafeStringFormat3(LocalString.GetString("第%d名"),v.StartNum) or SafeStringFormat3(LocalString.GetString("第%d-%d名"),v.StartNum,v.EndNum)
        local item1, item2 = itemArr[k][1], itemArr[k][2]
        local cur1, cur2 = item1.transform:Find("Cur"), item2.transform:Find("Cur")
        local icon1, icon2 = item1.transform:Find("IconTexture"):GetComponent(typeof(CUITexture)) , item2.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
        item1.gameObject:SetActive(false)
        item2.gameObject:SetActive(false)
        self.m_AwardId2CurObjs[k] = {cur1, cur2}
        if v.AwardItem and v.AwardItem.Length >= 1 then
            local itemId = v.AwardItem[0]
            local itemData1 = Item_Item.GetData(itemId)
            if itemData1 then
                item1.gameObject:SetActive(true)
                icon1:LoadMaterial(itemData1.Icon)
                cur1.gameObject:SetActive(false)
                UIEventListener.Get(item1.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
                    CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType2.Default, 0, 0, 0, 0)
                end)
            end
        end
        if v.AwardItem and v.AwardItem.Length >= 2 then
            local itemId = v.AwardItem[1]
            local itemData2= Item_Item.GetData(itemId)
            if itemData2 then
                item2.gameObject:SetActive(true)
                icon2:LoadMaterial(itemData2.Icon)
                cur2.gameObject:SetActive(false)
                UIEventListener.Get(item2.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
                    CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType2.Default, 0, 0, 0, 0)
                end)
            end
        end
    end)
    self.m_Rank = 0
end

function LuaPengDaoRankWnd:InitMonth(isLast)
    self.m_IsLastMonth = isLast
    self.MonthTitleLabel.text = self.m_IsLastMonth and LocalString.GetString("上月") or LocalString.GetString("当月")
    local seasonId = self.m_IsLastMonth and LuaPengDaoMgr.m_LastSeasonId or LuaPengDaoMgr.m_SeasonId
    local seasonData = PengDaoFuYao_Season.GetData(seasonId)
    self.MonthLabel.text = seasonData.Month
    self.RewardView.gameObject:SetActive(not self.m_IsLastMonth)
    self.HideRewardsLabel.gameObject:SetActive(self.m_IsLastMonth)
    local lastSeasonData = PengDaoFuYao_Season.GetData(LuaPengDaoMgr.m_LastSeasonId)
    self.RefreshBtn.gameObject:SetActive(lastSeasonData ~= nil)
    self.CountDownLabel.text = seasonData.AwardTimeContext
end

function LuaPengDaoRankWnd:RefreshAwardItems()
    for id, arr in pairs(self.m_AwardId2CurObjs) do
        local cur1, cur2 =  arr[1], arr[2]
        local data = PengDaoFuYao_Award.GetData(id)
        cur1.gameObject:SetActive(self.m_Rank >= data.StartNum and self.m_Rank <= data.EndNum)
        cur2.gameObject:SetActive(self.m_Rank >= data.StartNum and self.m_Rank <= data.EndNum)
    end
end

function LuaPengDaoRankWnd:InitItem(item, index)
    local rankLabel = item.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
	local rankImage = item.transform:Find("RankLabel/RankImage"):GetComponent(typeof(UISprite))
    local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local pliesLabel = item.transform:Find("PliesLabel"):GetComponent(typeof(UILabel))
    local timeLabel = item.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
    local scoreLabel = item.transform:Find("ScoreLabel"):GetComponent(typeof(UILabel))
    local myObj = item.transform:Find("My")
    local hardLabel1 = item.transform:Find("HardLabel1"):GetComponent(typeof(UILabel))
    local hardLabel2 = item.transform:Find("HardLabel2"):GetComponent(typeof(UILabel))
    local hardLabel3 = item.transform:Find("HardLabel3"):GetComponent(typeof(UILabel))

    local t = self.m_List[index + 1]

    myObj.gameObject:SetActive(t.isMy)
    rankLabel.text = ""
    rankImage.spriteName = ""
    if t.rank==1 then
        rankImage.spriteName = Constants.RankFirstSpriteName
    elseif t.rank==2 then
        rankImage.spriteName = Constants.RankSecondSpriteName
    elseif t.rank==3 then
        rankImage.spriteName = Constants.RankThirdSpriteName
    else
        if t.rank > 0 then
            rankLabel.text = t.rank
        else
            rankLabel.text = LocalString.GetString("未上榜")
        end
    end
    nameLabel.text = t.data.name
    pliesLabel.text = t.data.PassedLevel
    timeLabel.text = t.data.UsedTime and SafeStringFormat3(LocalString.GetString("%02d:%02d"), math.floor((t.data.UsedTime % 3600) / 60), t.data.UsedTime % 60) or ""
    scoreLabel.text = t.data.Score
    hardLabel1.text = t.data.Difficulty1 and t.data.Difficulty1 or "-"
    hardLabel2.text = t.data.Difficulty2 and t.data.Difficulty2 or "-"
    hardLabel3.text = t.data.Difficulty3 and t.data.Difficulty3 or "-"
    local color = t.isMy and NGUIText.ParseColor24("1dff28", 0) or Color.white
    rankLabel.color = color
    nameLabel.color = color
    pliesLabel.color = color
    timeLabel.color = color
    scoreLabel.color = color
    hardLabel1.color = color
    hardLabel2.color = color
    hardLabel3.color = color
end

function LuaPengDaoRankWnd:OnEnable()
	g_ScriptEvent:AddListener("SeasonRank_QueryRankResult", self, "OnRankData")
end

function LuaPengDaoRankWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SeasonRank_QueryRankResult", self, "OnRankData")
end

function LuaPengDaoRankWnd:OnRankData(rankType, seasonId, selfDataU, rankDataU)
    self.m_List = {}
	local selfData = g_MessagePack.unpack(selfDataU)
    self.m_Rank = 0
    local rankData = g_MessagePack.unpack(rankDataU)
    if rankData then
        for rank, data in pairs(rankData) do
            table.insert(self.m_List, {rank = rank, data = data})
            if CClientMainPlayer.Inst and data.playerId == CClientMainPlayer.Inst.Id then
                table.insert(self.m_List,{rank = rank, data = selfData, isMy = true})
                self.m_Rank = rank
            end
        end
    end
    if selfData and self.m_Rank == 0 and CClientMainPlayer.Inst then
        selfData.name = CClientMainPlayer.Inst.Name
        table.insert(self.m_List,{rank = 0, data = selfData, isMy = true})
    end   
    table.sort(self.m_List, function (a, b)
        if a.isMy then
            return true
        elseif b.isMy then
            return false
        else
            return a.rank < b.rank
        end
    end)     
    self.TableView:ReloadData(true, false)
    self.TableView.m_Grid.transform:GetComponent(typeof(UITableTween)):Reset()
    self.TableView:ScrollToRow(0)
    self.TableView.m_Grid:Reposition()
    self.NoneRankLabel.gameObject:SetActive(#self.m_List == 0)
    self:RefreshAwardItems()
end

--@region UIEvent
function LuaPengDaoRankWnd:OnRadioBoxSelected(btn,index)
    self.m_SelectClass = index + 1
    for i = 0, self.RadioBox.m_RadioButtons.Length - 1 do
        local btn = self.RadioBox.m_RadioButtons[i]
        btn.transform:Find("Normal"):GetComponent(typeof(UITexture)).alpha = (index ~= i) and 0.6 or 1
        btn.transform:Find("Label"):GetComponent(typeof(UITexture)).alpha = (index ~= i) and 0.6 or 1
    end
    if CClientMainPlayer.Inst then
        local seasonId = self.m_IsLastMonth and LuaPengDaoMgr.m_LastSeasonId or LuaPengDaoMgr.m_SeasonId
		Gac2Gas.SeasonRank_QueryRank(2200 + self.m_SelectClass, seasonId)
	end
end

function LuaPengDaoRankWnd:OnRefreshBtnClicked()
    self:InitMonth(not self.m_IsLastMonth)
    if CClientMainPlayer.Inst then
		Gac2Gas.SeasonRank_QueryRank(2200 + self.m_SelectClass, self.m_IsLastMonth and LuaPengDaoMgr.m_LastSeasonId or LuaPengDaoMgr.m_SeasonId)
	end
end

function LuaPengDaoRankWnd:OnSelectAtRow(row)
    local t = self.m_List[row + 1]
    CPlayerInfoMgr.ShowPlayerPopupMenu(t.data.playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, Vector3.zero, AlignType.Default)
end
--@endregion UIEvent

