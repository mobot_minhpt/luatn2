local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CUIFx = import "L10.UI.CUIFx"

LuaScrollTaskMakeCombGameTaskView = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaScrollTaskMakeCombGameTaskView, "MakingComb", "MakingComb", GameObject)
RegistChildComponent(LuaScrollTaskMakeCombGameTaskView, "FinishedComb", "FinishedComb", GameObject)
RegistChildComponent(LuaScrollTaskMakeCombGameTaskView, "FinishedFx", "FinishedFx", CUIFx)
RegistChildComponent(LuaScrollTaskMakeCombGameTaskView, "OptionTemplate", "OptionTemplate", GameObject)
RegistChildComponent(LuaScrollTaskMakeCombGameTaskView, "Shape", "Shape", GameObject)
RegistChildComponent(LuaScrollTaskMakeCombGameTaskView, "Material", "Material", GameObject)
RegistChildComponent(LuaScrollTaskMakeCombGameTaskView, "Decorate", "Decorate", GameObject)
RegistChildComponent(LuaScrollTaskMakeCombGameTaskView, "ConfirmButton", "ConfirmButton", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaScrollTaskMakeCombGameTaskView, "m_Shapes")
RegistClassMember(LuaScrollTaskMakeCombGameTaskView, "m_Materials")
RegistClassMember(LuaScrollTaskMakeCombGameTaskView, "m_Decorates")
RegistClassMember(LuaScrollTaskMakeCombGameTaskView, "m_CurShapeIndex")
RegistClassMember(LuaScrollTaskMakeCombGameTaskView, "m_CurMaterialIndex")
RegistClassMember(LuaScrollTaskMakeCombGameTaskView, "m_CurDecorateIndex")
RegistClassMember(LuaScrollTaskMakeCombGameTaskView, "m_ShapeTitleName")
RegistClassMember(LuaScrollTaskMakeCombGameTaskView, "m_MaterialTitleName")
RegistClassMember(LuaScrollTaskMakeCombGameTaskView, "m_DecorateTitleName")

RegistClassMember(LuaScrollTaskMakeCombGameTaskView, "m_MakePhase")
RegistClassMember(LuaScrollTaskMakeCombGameTaskView, "m_ShowPhase")
RegistClassMember(LuaScrollTaskMakeCombGameTaskView, "m_ShowWidget")
RegistClassMember(LuaScrollTaskMakeCombGameTaskView, "m_IsPlaying")
RegistClassMember(LuaScrollTaskMakeCombGameTaskView, "m_TaskId")

function LuaScrollTaskMakeCombGameTaskView:Awake()
    self:InitComponents()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_Shapes = {LocalString.GetString("半月"),
                    LocalString.GetString("马蹄"),
                    LocalString.GetString("扇面")}
    self.m_Materials = {LocalString.GetString("银"),
                    LocalString.GetString("金"),
                    LocalString.GetString("木"),
                    LocalString.GetString("玉"),}
    self.m_Decorates = {LocalString.GetString("繁花"),
                    LocalString.GetString("彩蝶"),
                    LocalString.GetString("吊坠"),
                    LocalString.GetString("翡翠"),}
    self.m_ShapeTitleName = LocalString.GetString("形状")
    self.m_MaterialTitleName = LocalString.GetString("材质")
    self.m_DecorateTitleName = LocalString.GetString("装饰")

    self.m_MakePhase:SetActive(true)
    self.m_ShowPhase:SetActive(false)
    self.OptionTemplate:SetActive(false)

    self:InitOptions()
end

function LuaScrollTaskMakeCombGameTaskView:InitComponents()
    self.m_MakePhase = self.transform:Find("MakePhase").gameObject
    self.m_ShowPhase = self.transform:Find("ShowPhase").gameObject
    self.m_ShowWidget= self.transform:Find("ShowPhase/Widget").gameObject
end

function LuaScrollTaskMakeCombGameTaskView:Start()
    UIEventListener.Get(self.ConfirmButton).onClick = DelegateFactory.VoidDelegate(function(p)
        self:OnConfirmButtonClick(p)
    end)

    UIEventListener.Get(self.m_ShowWidget).onClick = DelegateFactory.VoidDelegate(function(p)
        self:OnShowWidgetClick(p)
    end)
end

function LuaScrollTaskMakeCombGameTaskView:InitGame(gameId, taskId, callback)
    self:LoadDesignData(gameId)

    self.m_CurShapeIndex = math.random(#self.m_Shapes)
    self.m_CurMaterialIndex = math.random(#self.m_Materials)
    self.m_CurDecorateIndex = math.random(#self.m_Decorates)
    self:RefreshOptions()
    self:RefreshComb()

    self.m_TaskId = taskId
    self.m_IsPlaying = true
    self.m_Callback = callback
end

function LuaScrollTaskMakeCombGameTaskView:OnEnable()
    g_ScriptEvent:AddListener("OnSaveCombDataSuccess", self, "OnSaveCombDataSuccess")
end

function LuaScrollTaskMakeCombGameTaskView:OnDisable()
    g_ScriptEvent:RemoveListener("OnSaveCombDataSuccess", self, "OnSaveCombDataSuccess")
end

--@region UIEvent

--@endregion UIEvent

function LuaScrollTaskMakeCombGameTaskView:LoadDesignData(gameId)
    
end

function LuaScrollTaskMakeCombGameTaskView:InitOptions()
    self:InitOption(self.Shape, self.m_ShapeTitleName, self.m_Shapes, function(index)
        self.m_CurShapeIndex = index
        self:RefreshComb()
    end)

    self:InitOption(self.Material, self.m_MaterialTitleName, self.m_Materials, function(index)
        self.m_CurMaterialIndex = index
        self:RefreshComb()
    end)

    self:InitOption(self.Decorate, self.m_DecorateTitleName, self.m_Decorates, function(index)
        self.m_CurDecorateIndex = index
        self:RefreshComb()
    end)
end

function LuaScrollTaskMakeCombGameTaskView:InitOption(sectionGo, optionTitleName, optionNames, selectCallBack)
    local grid = sectionGo.transform:Find("Grid"):GetComponent(typeof(UIGrid))
    local titleLab = sectionGo.transform:Find("Title"):GetComponent(typeof(UILabel))

    titleLab.text = optionTitleName

    NGUITools.DestroyChildren(grid.transform)
    for i = 1, #optionNames do
        local optionGo = NGUITools.AddChild(grid.gameObject, self.OptionTemplate)
        local lab = optionGo:GetComponent(typeof(UILabel))
        local selectGo = optionGo.transform:Find("Selected").gameObject

        optionGo:SetActive(true)
        UIEventListener.Get(optionGo).onClick = DelegateFactory.VoidDelegate(function(p)
            self:RefreshOption(sectionGo, i)
            selectCallBack(i)
        end)
        lab.text = optionNames[i]
        selectGo:SetActive(false)
    end

    grid:Reposition()
end

function LuaScrollTaskMakeCombGameTaskView:OnOptionClick(sectionGo, index)
    for i = 0, sectionGo.transform.childCount - 1 do
        local optionTf = sectionGo.transform:GetChild(i)
        local selectGo = optionTf:Find("Selected").gameObject
        selectGo:SetActive(i == index - 1)
    end
end

function LuaScrollTaskMakeCombGameTaskView:RefreshOptions()
    self:RefreshOption(self.Shape, self.m_CurShapeIndex)
    self:RefreshOption(self.Material, self.m_CurMaterialIndex)
    self:RefreshOption(self.Decorate, self.m_CurDecorateIndex)
end

function LuaScrollTaskMakeCombGameTaskView:RefreshOption(sectionGo, curOptionIndex)
    local grid = sectionGo.transform:Find("Grid"):GetComponent(typeof(UIGrid))
    for i = 0, grid.transform.childCount - 1 do
        local optionTf = grid.transform:GetChild(i)
        local selectGo = optionTf:Find("Selected").gameObject
        selectGo:SetActive(i == curOptionIndex - 1)
    end
end

function LuaScrollTaskMakeCombGameTaskView:RefreshComb()
    local combTexture = self.MakingComb.transform:Find("Comb"):GetComponent(typeof(UITexture))
    self:RefreshCombTexture(combTexture)
end

function LuaScrollTaskMakeCombGameTaskView:RefreshShowComb()
    local combTexture = self.FinishedComb.transform:Find("Comb"):GetComponent(typeof(UITexture))
    self:RefreshCombTexture(combTexture)
end

function LuaScrollTaskMakeCombGameTaskView:RefreshCombTexture(combTexture)
    local combInfo ={
        Shape = self.m_CurShapeIndex,
        Material = self.m_CurMaterialIndex,
        Decorate = self.m_CurDecorateIndex
    }
    LuaScrollTaskMgr:GenCombTex(combTexture, combInfo)
end

function LuaScrollTaskMakeCombGameTaskView:OnConfirmButtonClick(p)
    local msg = g_MessageMgr:FormatMessage("ScrollTaskMakeCombGame_MakeSure")
    g_MessageMgr:ShowOkCancelMessage(msg, function()
        Gac2Gas.RequestSaveCombData(self.m_TaskId, self.m_CurMaterialIndex, self.m_CurDecorateIndex, self.m_CurShapeIndex)
        end, nil, nil, nil, false)
end

function LuaScrollTaskMakeCombGameTaskView:OnShowWidgetClick(p)
    UnRegisterTick(self.m_Tick)
    self:OnGameFinish()
end

function LuaScrollTaskMakeCombGameTaskView:OnSaveCombDataSuccess()
    self:RefreshShowComb()
    self.m_MakePhase:SetActive(false)
    self.m_ShowPhase:SetActive(true)

    self:OnSuccess()
end

function LuaScrollTaskMakeCombGameTaskView:OnSuccess()
    UnRegisterTick(self.m_Tick)
    self.m_Tick = RegisterTickOnce(function()
        self:OnGameFinish()
    end, self.m_FinishDelay or 3000)
end

function LuaScrollTaskMakeCombGameTaskView:OnGameFinish()
    if self.m_IsPlaying then
        self.m_IsPlaying = false
        if self.m_Callback then
            self.m_Callback()
        end
    end
end

function LuaScrollTaskMakeCombGameTaskView:OnDestroy()
    UnRegisterTick(self.m_Tick)
end
