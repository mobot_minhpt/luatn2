local GameObject = import "UnityEngine.GameObject"

local UILabel = import "UILabel"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaStarBiwuChampionWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaStarBiwuChampionWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaStarBiwuChampionWnd, "NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaStarBiwuChampionWnd, "Button", "Button", GameObject)
RegistChildComponent(LuaStarBiwuChampionWnd, "Fx", "Fx", CUIFx)
--@endregion RegistChildComponent end

function LuaStarBiwuChampionWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.Button.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButtonClick()
	end)


    --@endregion EventBind end
end

function LuaStarBiwuChampionWnd:Init()
    self:InitTitle()
	self.NameLabel.text = CLuaStarBiwuMgr.championZhanduiName
	self.Button.gameObject:SetActive(false)
	self.Fx:LoadFx("fx/ui/prefab/UI_Guanjunzhanshi.prefab")
end

function LuaStarBiwuChampionWnd:InitTitle()
	local nowTime = CServerTimeMgr.Inst.timeStamp
	local autopatchData = nil
	StarBiWuShow_AutoPatch.Foreach(function(key, data) 
		local startTime = CServerTimeMgr.Inst:GetTimeStampByStr(data.StartTime)
		if startTime < nowTime then
			local endTime = CServerTimeMgr.Inst:GetTimeStampByStr(data.EndTime)
			if endTime > nowTime then
				autopatchData = StarBiWuShow_AutoPatch.GetData(key)
			end
		end
	end)
	if autopatchData then
		self.TitleLabel.text = string.gsub(autopatchData.Desc, LocalString.GetString("跨服明星赛"), "")
	end
end
--@region UIEvent

function LuaStarBiwuChampionWnd:OnButtonClick()
	Gac2Gas.QueryStarBiwuZongJueSaiOverView(0)
    --CUIManager.ShowUI(CLuaUIResources.StarBiwuTopFourWnd)
end

--@endregion UIEvent

