local LuaGameObject=import "LuaGameObject"

CTickMgr = import "L10.Engine.CTickMgr"
ETickType = import "L10.Engine.ETickType"
CBlowDetectMgr = import "L10.Game.CBlowDetectMgr"

CBlowBubblesWnd = class()
RegistClassMember(CBlowBubblesWnd, "m_CountLabel")
RegistClassMember(CBlowBubblesWnd, "m_Tick")
RegistClassMember(CBlowBubblesWnd, "m_Count")
RegistClassMember(CBlowBubblesWnd, "m_BlowCount")
RegistClassMember(CBlowBubblesWnd, "m_WaterMaterial")
RegistClassMember(CBlowBubblesWnd, "m_WaterAmount")
RegistClassMember(CBlowBubblesWnd, "m_WaterTex")
RegistClassMember(CBlowBubblesWnd, "m_Fx")
RegistClassMember(CBlowBubblesWnd, "m_IsFirstBlow")
RegistClassMember(CBlowBubblesWnd, "m_TipLabel")

function CBlowBubblesWnd:Init()
	self.m_IsFirstBlow = true
	-- 总共四次吹起
	self.m_BlowCount = 4
	self.m_WaterTex = LuaGameObject.GetChildNoGC(self.transform, "Top").texture
	self.m_Fx = LuaGameObject.GetChildNoGC(self.transform, "FX").uiFx
	self.m_TipLabel = self.transform:Find("Root/TipLabel").gameObject
	self.m_WaterMaterial = self.m_WaterTex.material
	self:SetWaterAmount(0)
	self.m_CountLabel = LuaGameObject.GetChildNoGC(self.transform, "Count").label
	self.m_Count = 10
	self.m_CountLabel.text = self.m_Count
	self.m_Tick = CTickMgr.Register(DelegateFactory.Action(function ()
		if not self.m_IsFirstBlow then
			g_MessageMgr:ShowMessage("PLEASE_BLOW_BUBBLE_MORE")
		end
		self.m_Count = self.m_Count - 1
		self.m_CountLabel.text = self.m_Count
		if self.m_Count <= 0 then
			if self.m_Tick then
				invoke(self.m_Tick)
				self.m_Tick = nil
			end
			CUIManager.CloseUI(CUIResources.BlowBubblesWnd)
		end
	end), 1000, ETickType.Loop)

	local g = LuaGameObject.GetChildNoGC(self.transform, "Bottom").gameObject
	UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
		self:BlowSuccess()
	end)
	self:StartDetect()
end

function CBlowBubblesWnd:StartDetect()
	if not CUIManager.IsLoaded(CUIResources.BlowBubblesWnd) then
		return
	end
	CBlowDetectMgr.Inst:StartDetectWithoutWnd(self.m_Count * 1000, DelegateFactory.Action_bool(function (result)
			if result then
				self:BlowSuccess()
			end
			self:StartDetect()
		end), false)
end

function CBlowBubblesWnd:BlowSuccess()
	if self.m_IsFirstBlow then
		self.m_Fx:LoadFx("fx/ui/prefab/UI_chuipaoao_fx01.prefab")
		self.m_IsFirstBlow = false
		self.m_TipLabel:SetActive(false)
	end
	self:SetWaterAmount(self.m_WaterAmount + 1.0 / self.m_BlowCount)
	if self.m_WaterAmount >= 1 then
		CUIManager.CloseUI(CUIResources.BlowBubblesWnd)
	end
	--g_MessageMgr:ShowMessage("PLEASE_BLOW_BUBBLE_HARDER")
end

-- 调整材质参数设置进度
function CBlowBubblesWnd:SetWaterAmount(amount)
	self.m_WaterAmount = amount
	self.m_WaterMaterial:SetFloat("_Progress", amount)
	self.m_WaterTex:RemoveFromPanel()
	self.m_WaterTex:SetDirty()
end

function CBlowBubblesWnd:OnDestroy()
	if self.m_Tick then
		invoke(self.m_Tick)
		self.m_Tick = nil
	end
	CBlowDetectMgr.Inst:StopDetect(false)
	Gac2Gas.FinishBlowBubble(self.m_WaterAmount, CLuaLiuYiMgr.ItemPlace, CLuaLiuYiMgr.ItemPos, CLuaLiuYiMgr.ItemId)
end
return CBlowBubblesWnd
