
local CHongBaoMgr = import "L10.UI.CHongBaoMgr"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local CUICommonDef = import "L10.UI.CUICommonDef"
local EnumRecordContext = import "L10.Game.EnumRecordContext"
local EnumVoicePlatform = import "L10.Game.EnumVoicePlatform"
local CRecordingInfoMgr = import "L10.UI.CRecordingInfoMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local DelegateFactory = import "DelegateFactory"


CLuaSnatchVoiceHongBaoWnd = class()
CLuaSnatchVoiceHongBaoWnd.Path = "ui/hongbao/LuaSnatchVoiceHongBaoWnd"

function CLuaSnatchVoiceHongBaoWnd:Init( ... )
	self:InitBg()
	self.transform:Find("Anchor/Name"):GetComponent(typeof(UILabel)).text = CLuaHongBaoMgr.m_HongbaoInfo.m_OwnerName
	self.transform:Find("Anchor/Content"):GetComponent(typeof(UILabel)).text = CLuaHongBaoMgr.m_HongbaoInfo.m_Content
	self.transform:Find("Anchor/Portrait"):GetComponent(typeof(CUITexture)):LoadPortrait(CUICommonDef.GetPortraitName(CLuaHongBaoMgr.m_HongbaoInfo.m_Class, CLuaHongBaoMgr.m_HongbaoInfo.m_Gender, -1), true)
	CommonDefs.AddOnPressListener(self.transform:Find("Anchor/VoiceBtn").gameObject, DelegateFactory.Action_GameObject_bool(function(go, pressed) self:OnVoiceButtonPress(go, pressed) end), true)
	UIEventListener.Get(self.transform:Find("Anchor/Detail").gameObject).onClick = LuaUtils.VoidDelegate(function ()
		CLuaHongBaoMgr.ShowHongBaoDetailWnd(CLuaHongBaoMgr.m_HongbaoInfo.m_HongbaoId, CLuaHongBaoMgr.m_HongbaoInfo.m_EventType,CLuaHongBaoMgr.m_HongbaoInfo.m_HongBaoCoverType);
	end)
end

function CLuaSnatchVoiceHongBaoWnd:OnEnable()
	g_ScriptEvent:AddListener("OnVoiceTranslateFinished", self, "OnVoiceTranslateFinished")
end

function CLuaSnatchVoiceHongBaoWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnVoiceTranslateFinished", self, "OnVoiceTranslateFinished")
end

function CLuaSnatchVoiceHongBaoWnd:OnVoiceButtonPress(go, pressed)
	if pressed then
        CRecordingInfoMgr.ShowRecordingWnd(EnumRecordContext.HongBao, EChatPanel.Guild, go, EnumVoicePlatform.Default, false);
    else
        CRecordingInfoMgr.CloseRecordingWnd(not go:Equals(CUICommonDef.SelectedUI))
    end
end

function CLuaSnatchVoiceHongBaoWnd:FilterTranslation(translation)
	if not translation then return "" end
	local i, len = 1, #translation
	local ret = {}
	while i <= len do
		local c = string.byte(translation, i)
		if c >= 0xF0 then
			i = i + 4
		elseif c >= 0xE0 then
			-- chinese word
			if c >= 228 and c <= 233 then
				table.insert(ret, c)
				table.insert(ret, string.byte(translation, i + 1))
				table.insert(ret, string.byte(translation, i + 2))
			end
			i = i + 3
		elseif c >= 0xC0 then
			i = i + 2
		else
			-- digit
			if c >= 48 and c <= 57 then table.insert(ret, c) end
			i = i + 1
		end
	end
	return string.char(unpack(ret, 1, #ret))
end

function CLuaSnatchVoiceHongBaoWnd:OnVoiceTranslateFinished(args)
	local context = args[0]
	local voiceId = args[1]
	local result = args[2]
	if context == EnumRecordContext.HongBao then
		Gac2Gas.RequestSnatchHongBao(CLuaHongBaoMgr.m_HongbaoInfo.m_HongbaoId, EnumToInt(CHongBaoMgr.m_HongbaoType), self:FilterTranslation(result))
        Gac2Gas.RequestRecentHongBaoInfo(EnumToInt(CHongBaoMgr.m_HongbaoType))
   	end
end

function CLuaSnatchVoiceHongBaoWnd:InitBg()
	local coverType = CLuaHongBaoMgr.m_HongbaoInfo.m_HongBaoCoverType or 0
    local coverData = HongBao_Cover.GetData(coverType)
    if not coverData then return end
	local bg = self.transform:Find("Bg").gameObject:GetComponent(typeof(CUITexture))
	bg:LoadMaterial("UI/Texture/Transparent/Material/"..coverData.VoiceResource..".mat")
end
