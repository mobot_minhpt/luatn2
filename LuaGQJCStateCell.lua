require("common/common_include")

local CUIFx = import "L10.UI.CUIFx"
local NGUIText = import "NGUIText"
local TweenAlpha = import "TweenAlpha"
local UISprite = import "UISprite"
local CUIFxPaths = import "L10.UI.CUIFxPaths"

LuaGQJCStateCell = class()

RegistClassMember(LuaGQJCStateCell,"m_Transform")

RegistClassMember(LuaGQJCStateCell, "m_CellId")	-- ID
RegistClassMember(LuaGQJCStateCell, "m_PositionId") -- 用于grid显示
RegistClassMember(LuaGQJCStateCell, "m_Level")	-- 级别(1-5)
RegistClassMember(LuaGQJCStateCell, "m_Cleared")	-- 1完成, 0未完成
RegistClassMember(LuaGQJCStateCell, "m_ShrinkState")	-- 是否完成(会变) 0, 1, 2 NoShrink, Shrinking, Shrinked
RegistClassMember(LuaGQJCStateCell, "m_BossState")	-- boss状态 0, 1, 2 NoActive, Activating, Actived

RegistClassMember(LuaGQJCStateCell, "m_ClearMark")
RegistClassMember(LuaGQJCStateCell, "m_ClosedMark")
RegistClassMember(LuaGQJCStateCell, "m_ClosedMarkTween")
RegistClassMember(LuaGQJCStateCell, "m_BossMark")
RegistClassMember(LuaGQJCStateCell, "m_BossFx")
RegistClassMember(LuaGQJCStateCell, "m_BG")

RegistClassMember(LuaGQJCStateCell, "m_NormalBGColor")
RegistClassMember(LuaGQJCStateCell, "m_ClosedBGColor")

function LuaGQJCStateCell:Init(transform, cellId, positionId, level, cleared, shrinkState, bossState)
	self.m_Transform = transform
	self.m_CellId = cellId
	self.m_PositionId = positionId
	self.m_Level = level

	self.m_Cleared = -1
	self.m_ShrinkState = -1
	self.m_BossState = -1

	self:InitClassMembers()
	self:InitValues()
	self:UpdateCell(cleared, shrinkState, bossState)

end

function LuaGQJCStateCell:InitClassMembers()
	self.m_ClearMark = self.m_Transform:Find("ClearMark").gameObject
	self.m_ClosedMark = self.m_Transform:Find("ClosedMark").gameObject
	self.m_ClosedMarkTween = self.m_Transform:Find("ClosedMark"):GetComponent(typeof(TweenAlpha))

	self.m_BossMark = self.m_Transform:Find("BossMark"):GetComponent(typeof(UISprite))
	self.m_BossFx = self.m_Transform:Find("BossFx"):GetComponent(typeof(CUIFx))
	self.m_BossFx.gameObject:SetActive(false)
	self.m_BG = self.m_Transform:Find("BG"):GetComponent(typeof(UISprite))
end

function LuaGQJCStateCell:InitValues()
	self.m_NormalBGColor = NGUIText.ParseColor32("0BD6FFB4", 0)
	self.m_ClosedBGColor = NGUIText.ParseColor32("FFFFFF64", 0)

	if self.m_Level == 5 then
		self.m_BossMark.gameObject:SetActive(true)
	else
		self.m_BossMark.gameObject:SetActive(false)
	end
end

function LuaGQJCStateCell:UpdateCell(cleared, shrinkState, bossState)

	if self.m_Cleared ~= cleared or self.m_ShrinkState ~= shrinkState then
		self.m_ClearMark:SetActive(cleared == 1 and shrinkState ~= 2) -- 已经关闭的不需要显示通关
		self.m_Cleared = cleared
	end
	
	if self.m_ShrinkState ~= shrinkState then -- 防止多次闪烁
		self.m_ShrinkState = shrinkState
		if shrinkState == 0 then
			self.m_ClosedMarkTween.enabled = false
			self.m_ClosedMark:SetActive(false)
			self.m_BG.color = self.m_ClosedBGColor
		elseif shrinkState == 1 then
			self.m_ClosedMark:SetActive(true)
			-- 开始闪烁
			self.m_ClosedMarkTween.enabled = true
		--elseif shrinkState == 2 then
			--self.m_ClosedMark:SetActive(true)
			--self.m_ClosedMarkTween.enabled = false
			--self.m_BG.color = self.m_ClosedBGColor
		end
	end

	if shrinkState == 2 then
		self.m_ClosedMark:SetActive(true)
		self.m_ClosedMarkTween.enabled = false
		self.m_BG.color = self.m_ClosedBGColor
	end
	
	if self.m_Level == 5 and self.m_BossState ~= bossState then
		self.m_BossState = bossState
		if bossState == 0 then
			self.m_BossMark.alpha = 0.3
		elseif bossState == 1 then
			self.m_BossMark.alpha = 1
			-- 特效闪烁 UI_boss_jihuo
			self.m_BossFx:LoadFx(CUIFxPaths.GQJCBossShowUpFx)
			self.m_BossFx.gameObject:SetActive(true)
		elseif bossState == 2 then
			self.m_BossMark.alpha = 1
		end
	end

end

function LuaGQJCStateCell:GetPos()
	return self.m_Transform.localPosition
end

function LuaGQJCStateCell:GetPositionId()
	return self.m_PositionId
end

function LuaGQJCStateCell:OnEnable()
	
end

function LuaGQJCStateCell:OnDisable()
	if CLuaGuoQingJiaoChangMgr.m_MapCells[self.m_CellId] then
		CLuaGuoQingJiaoChangMgr.m_MapCells[self.m_CellId]= nil
	end
end


return LuaGQJCStateCell