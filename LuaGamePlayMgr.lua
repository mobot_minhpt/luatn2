local CScene=import "L10.Game.CScene"
local CGamePlayMgr = import "L10.Game.CGamePlayMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CUIManager = import "L10.UI.CUIManager"
local WatermarkHelper = import "WatermarkHelper"
local SdkU3d = import "NtUniSdk.Unity3d.SdkU3d"
local ChannelDefine = import "L10.Game.ChannelDefine"
local NativeTools = import "L10.Engine.NativeTools"
local CLoginMgr = import "L10.Game.CLoginMgr"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CSkillButtonBoardWnd = import "L10.UI.CSkillButtonBoardWnd"
local CClientLingShou = import "L10.Game.CClientLingShou"
local CClientPet = import "L10.Game.CClientPet"
local CClientOtherPlayer = import "L10.Game.CClientOtherPlayer"
local CHeadInfoWnd = import "L10.UI.CHeadInfoWnd"
local CPlayerHeadInfoItem = import "L10.UI.CPlayerHeadInfoItem"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local CRenderObject = import "L10.Engine.CRenderObject"
local CAnimationMgr = import "L10.Game.CAnimationMgr"
local EnumObjectType = import "L10.Game.EnumObjectType"
local EShadowType = import "L10.Engine.EShadowType"
local CClientNpc = import "L10.Game.CClientNpc"
local NPC_NPC = import "L10.Game.NPC_NPC"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CEffectMgr = import "L10.Game.CEffectMgr"

LuaGamePlayMgr = {}

if rawget(_G, "LuaGamePlayMgr") then
    g_ScriptEvent:RemoveListener("ClientObjCreate", LuaGamePlayMgr, "OnClientObjCreate")
    g_ScriptEvent:RemoveListener("ClientObjDestroy", LuaGamePlayMgr, "OnClientObjDestroy")
end

g_ScriptEvent:AddListener("ClientObjCreate", LuaGamePlayMgr, "OnClientObjCreate")
g_ScriptEvent:AddListener("ClientObjDestroy", LuaGamePlayMgr, "OnClientObjDestroy")

LuaGamePlayMgr.s_CurrentNpcDict = nil

function LuaGamePlayMgr:OnClientObjCreate(args)
    if self.s_CurrentNpcDict then
        local engineId = args[0]
        local obj = CClientObjectMgr.Inst:GetObject(engineId)
        if obj and obj.ObjectType == EnumObjectType.NPC then
            local npc = self.s_CurrentNpcDict[obj.TemplateId]
            if npc and npc.showOutOfSight then
                if not CommonDefs.IsNull(npc.RO) then
                    npc.RO:Destroy()
                end
            end
        end
    end
end

function LuaGamePlayMgr:OnClientObjDestroy(args)
    if self.s_CurrentNpcDict then
        local engineId = args[0]
        local obj = CClientObjectMgr.Inst:GetObject(engineId)
        if obj and obj.ObjectType == EnumObjectType.NPC then
            local npc = self.s_CurrentNpcDict[obj.TemplateId]
            if npc and npc.showOutOfSight then
                if not CommonDefs.IsNull(npc.RO) then
                    npc.RO:Destroy()
                end
                npc.RO = self:CreateLocalNpc(npc)
            end
        end
    end
end

function LuaGamePlayMgr:InitCurrentNpcInfo()
    if self.s_GamePlayId and not self.s_CurrentNpcDict then
        self.s_CurrentNpcDict = {}
        local playData = Gameplay_Gameplay.GetData(self.s_GamePlayId)
        if playData and playData.ShowOutOfSightNPC then
            for i = 0, playData.ShowOutOfSightNPC.Length - 1 do
                local npc = playData.ShowOutOfSightNPC[i]
                --local id, x, z, dir, pixelPos = string.match(playData.ShowOutOfSightNPC[i], "(%d+),(%d+),(%d+),(%d+),(.+)")
                local id, x, z, dir, pixelPos = npc[0], npc[1], npc[2], npc[3], npc[4] 
                local px, pz = string.match(pixelPos, "(%d+)p(%d+)")
                x = x + px / 64
                z = z + pz / 64
                self.s_CurrentNpcDict[id] = {}
                self.s_CurrentNpcDict[id].id = id
                self.s_CurrentNpcDict[id].x = x
                self.s_CurrentNpcDict[id].z = z
                self.s_CurrentNpcDict[id].dir = dir
                self.s_CurrentNpcDict[id].showOutOfSight = true

                local y
                if CRenderScene.Inst and CScene.MainScene then
                    --if CScene.MainScene:Is3DScene() then
                    --    y = CRenderScene.Inst:SampleLogicHeightWithFloor(x, z, math.floor(pos.y))
                    --else
                        y = CRenderScene.Inst:SampleLogicHeight(x, z)
                    --end
                end
                --y += LogicHeight + GetExtraLogicHeight() + GetWaterHeight(pos.y)
                self.s_CurrentNpcDict[id].y = y or 0
            end
        end

        local exist = {}
        CClientObjectMgr.Inst:ForeachObject(DelegateFactory.Action_CClientObject(function(obj)
            if obj.ObjectType == EnumObjectType.NPC then
                exist[obj.TemplateId] = true
            end
        end))

        for id, npc in pairs(self.s_CurrentNpcDict) do
            if npc.showOutOfSight then
                if not CommonDefs.IsNull(npc.RO) then
                    npc.RO:Destroy()
                end
                if not exist[id] then
                    npc.RO = self:CreateLocalNpc(npc)
                end
            end
        end
    end
end

function LuaGamePlayMgr:CreateLocalNpc(npc)
    local defaultNpc = "assets/res/character/npc/hnpc251/prefab/hnpc251_01.prefab"
    local data = NPC_NPC.GetData(npc.id)
    if data and CRenderScene.Inst then
        local obj = CreateFromClass(GameObject, data.Name)
        --obj:SetActive(false)
        obj.transform.parent = CRenderScene.Inst.transform
        local ro = obj:AddComponent(typeof(CRenderObject))
        ro.IsImportant = false
        ro.HasFeisheng = false
        ro.IsKickOuting = false
        ro.CanBeJob = false
        ro.Selectable = false

        obj.transform.position = Vector3(npc.x, npc.y, npc.z)
        LuaUtils.SetLocalRotationY(obj.transform, 90 - npc.dir)
        LuaUtils.SetLocalScale(obj.transform, data.Scale, data.Scale, data.Scale)

        ro:LoadMain(CClientNpc.GetNPCPrefabPath(data), defaultNpc, false, data.IsSpecial, false)
        if data.RotationXZ and data.RotationXZ.Length == 2 then
            ro:SetRotationXZ(data.RotationXZ[0], data.RotationXZ[1])
        end
        local ani = CAnimationMgr.Inst:GetAniName("stand")
        ro:DoAni(ani, true, 0, 1, 0, true, 1)
        if data.FX then
            for i = 0, data.FX.Length - 1 do
                local fx = CEffectMgr.Inst:AddObjectFX(data.FX[i], ro)
                ro:AddFX(data.FX[i], fx)
            end
        end
        if data.RealTimeShadow == 1 then ro.ShadowType = EShadowType.RealTime
        elseif data.RealTimeShadow > 1 then ro.ShadowType = EShadowType.None
        else ro.ShadowType = EShadowType.Simple end
        ro.Frozen = data.DisableAni
        return ro
    end
end


LuaGamePlayMgr.s_TaskStageGamePlayId = nil
LuaGamePlayMgr.s_TaskSategTitle = nil
LuaGamePlayMgr.s_TaskContent = nil

--记录一下gameplay id，方便全局访问
LuaGamePlayMgr.s_GamePlayId = 0
LuaGamePlayMgr.s_SceneTemplateId = 0

--当前运行的playmgr，一个mgr可能关联多个gameplayid，比如海战，有船内玩法和海上玩法。
LuaGamePlayMgr.s_CurrentPlayMgr = nil
LuaGamePlayMgr.s_PlayId2PlayMgr = nil
LuaGamePlayMgr.s_SceneTemplateId2PlayMgr = nil

LuaGamePlayMgr.m_OpenWatermarkTbl = {[1003] = true, [564] = true, [500] = true}
--@desc 方便在lua的gamemgr中监听login事件
CGamePlayMgr.m_OnPlayerLoginFunc = function (mgr)
    LuaProfessionTransferMgr.OnPlayerLogin()
    LuaHuiLiuMgr.OnPlayerLogin()
    LuaRecommendGiftMgr.OnPlayerLogin()
    LuaWelfareMgr.OnPlayerLogin()
    CLuaQMPKMgr.OnPlayerLogin()

    LuaGamePlayMgr:OnPlayerLogin()
    LuaTopAndRightMenuWndMgr:OnPlayerLogin()
    LuaCommonGamePlayTaskViewMgr:OnPlayerLogin()
    LuaHanJia2023Mgr:OnPlayerLogin()
    LuaYiRenSiMgr:OnPlayerLogin()
    LuaDaFuWongMgr:OnPlayerLogin()
    LuaShuangshiyi2023Mgr:OnPlayerLogin()
    LuaSEASdkMgr:OnPlayerLogin()
end

function LuaGamePlayMgr:OnPlayerLogin()
    --互娱云游戏版本开启60帧高帧率模式
    if CommonDefs.IsAndroidPlatform() then
        local SystemInfo = import "UnityEngine.SystemInfo"
        if string.find(SystemInfo.deviceModel, "-ncgqp$") then
            local Setting = import "L10.Engine.Setting"
            local NativeTools = import "L10.Engine.NativeTools"
            local PlayerSettings = import "L10.Game.PlayerSettings"
            Setting.eDefaultFPS = 60
            Setting.eLowFPS = 30
            Application.targetFrameRate = 60
            PlayerSettings.UpdateFrameRate(PlayerSettings.HighFrameRateEnabled)
        end
    end
end

function LuaGamePlayMgr:OnMainPlayerCreated()
    g_AutoChangeToTaskTab = false

    local playId = 0
    local sceneTemplateId=0
    if CScene.MainScene then
        playId = CScene.MainScene.GamePlayDesignId
        self.s_GamePlayId = playId
        sceneTemplateId = CScene.MainScene.SceneTemplateId
        self.s_SceneTemplateId = sceneTemplateId
	end

    self:InitCurrentNpcInfo()

    --进入玩法才初始化
    if playId>0 then TryRegisterAllGamePlay() end

    if playId>0 and self.s_PlayId2PlayMgr[playId] then
        local mgrName = self.s_PlayId2PlayMgr[playId]
        local playMgr = _G[mgrName]
        if playMgr then
            self.s_CurrentPlayMgr = playMgr
            if playMgr.OnMainPlayerCreated then
                playMgr:OnMainPlayerCreated(playId)
            end
        end
    end

    TryRegisterAllGamePlay_CommonScene()
    if self.s_SceneTemplateId2PlayMgr[sceneTemplateId] then
        local mgrName = self.s_SceneTemplateId2PlayMgr[sceneTemplateId]
        local playMgr = _G[mgrName]
        if playMgr then
            self.s_CurrentScenePlayMgr = playMgr
            if playMgr.OnMainPlayerCreated then
                playMgr:OnMainPlayerCreated(sceneTemplateId)
            end
        end
    end

    -------------------------------------------------------------------------------------------
    self:AddWatermark()
    --师徒捡垃圾任务关注一下buff信息的变化，用于更新头顶显示，出于性能考虑不用所有场景都监听该消息
    g_ScriptEvent:RemoveListener("OnBuffInfoUpdate", self, "OnBuffInfoUpdate")
    if LuaShiTuMgr:IsInJianLaJiPlay() or 
            LuaZongMenMgr:IsInZongMenScene() or 
            (self.s_CurrentPlayMgr or {}).OnBuffInfoUpdate or
            LuaShuangshiyi2023Mgr.IsInBaoYuDuoYuPlay() then
        g_ScriptEvent:AddListener("OnBuffInfoUpdate", self, "OnBuffInfoUpdate")
    end
end
function LuaGamePlayMgr:OnMainPlayerDestroyed()
    self.s_GamePlayId = 0
    local playMgr = self.s_CurrentPlayMgr
    if playMgr and playMgr.OnMainPlayerDestroyed then
        playMgr:OnMainPlayerDestroyed()
    end

    -- 清空玩法数据 BEGIN --
    self.MiniMapMarkInfos = {}

    self.NameBoardInfos = {}
    self.PlayerNameBoardInfos = {}

    self.TitleBoardInfos = {}
    self.PlayerTitleBoardInfos = {}

    self.PlayerHeadSpeIconInfos = {}
    self.NpcHeadSpeIconInfos = {}

    self.SkillButtonBoardState = {}
    -- 清空玩法数据 END --

    if self.s_CurrentNpcDict then
        for _, npc in pairs(self.s_CurrentNpcDict) do
            if not CommonDefs.IsNull(npc.RO) then
                npc.RO:Destroy()
            end
        end
        self.s_CurrentNpcDict = nil
    end
    self.s_CurrentPlayMgr = nil
end

--#region 玩法数据结构

--[[ 
    设置指定引擎对象的地图图标
    使用说明：参数doUpdate=true立即更新单个图标 or 先Set全部图标数据后再Refresh
    info格式：{
        -- 参数名 = 不设置时的默认值
        res = "",                                   -- 资源名称
        x = 0, y = 0,                               -- Grid坐标
        offset = { {x=0,y=0},{0,0} },               -- 图标偏移, 单位Pixel, [1]=小地图参数, [2]=大地图参数
        rotZ = 0,                                   -- 旋转角度
        size = { {width=32,height=32},{64,64} },    -- 图标大小, [1]=小地图参数, [2]=大地图参数
        scale = 1,                                  -- 图标缩放
        color,                                      -- 图标颜色, 无默认值, 设置后才修改
        alpha,                                      -- 图标不透明度, 无默认值, 设置后才修改
        depth,                                      -- 图标UIWidget的depth, 无默认值, 设置后才修改
        show = true,                                -- 是否显示
    }
--]]
LuaGamePlayMgr.MiniMapMarkInfos = {} 
function LuaGamePlayMgr:UpdateMiniMapMark(engineId)
    g_ScriptEvent:BroadcastInLua("UpdateMiniMapMark", engineId, self.MiniMapMarkInfos[engineId])
end
function LuaGamePlayMgr:RefreshMiniMapMarks() -- 同时刷新小地图和大地图
    g_ScriptEvent:BroadcastInLua("RefreshMiniMapMarks", self.MiniMapMarkInfos)
end
function LuaGamePlayMgr:RefreshMiniMapPopMarks() -- 只刷新大地图
    g_ScriptEvent:BroadcastInLua("RefreshMiniMapPopMarks", self.MiniMapMarkInfos)
end
function LuaGamePlayMgr:GetMiniMapMarkInfo(engineId)
    return engineId and self.MiniMapMarkInfos[engineId]
end
function LuaGamePlayMgr:SetMiniMapMarkInfo(engineId, info, doUpdate)
    self.MiniMapMarkInfos[engineId] = info
    if doUpdate then
        self:UpdateMiniMapMark(engineId)
    end
end

--[[ 
    设置指定引擎对象的Name
    使用说明：doUpdate=true立即修改 or 存住数据等创建时生效(创建时将优先使用该信息)
    info格式：{
        -- 参数名 = 不设置时的默认值
        show = true,                                    -- 是否显示
        displayName = "",                               -- 文本内容 
        fontSize = defaultNameFontSize,                 -- 字体大小
        fontColor = obj.NameDisplayColor,               -- 字体颜色  
        bgName =                                        -- 背景的spriteName
            player: MAIN_PLAYER_ATTACKABLE              -- 玩家Name的默认背景
            other: "headinfownd_lingshou_attack_namebg",-- 其它(宠物、灵兽)Name的默认背景             
        bgVisible = false,                              -- 背景是否可见
        syncTargetWnd = true,                           -- 是否同步修改CurrentTarget显示的Name
        onClickTargetWnd = nil,                         -- 点击CurrentTargetWnd的头像时调用

        lingShouInfo = nil,                             -- 灵兽的NameInfo，格式与上述info格式一致，对所有灵兽生效
        petInfo = nil,                                  -- 宠物的NameInfo，格式与上述info格式一致，对所有宠物生效
        
        hideShadowPetName = false,                      -- 分身Pet是否隐藏Name(分身比角色先创建时头顶信息会使用PetHeadInfoItem)
    }
--]]
LuaGamePlayMgr.NameBoardInfos = {}
function LuaGamePlayMgr:GetNameBoardInfo(engineId)
    return engineId and self.NameBoardInfos[engineId]
end
function LuaGamePlayMgr:SetNameBoardInfo(engineId, info, doUpdate)
    self.NameBoardInfos[engineId] = info
    if doUpdate then
        EventManager.BroadcastInternalForLua(EnumEventType.ClientObjChangeName, {engineId})
        CClientObjectMgr.Inst:ForeachObject(DelegateFactory.Action_CClientObject(function(obj)
            if (TypeIs(obj, typeof(CClientLingShou)) or TypeIs(obj, typeof(CClientPet))) and obj.m_OwnerEngineId == engineId then
                EventManager.BroadcastInternalForLua(EnumEventType.ClientObjChangeName, {obj.EngineId})
                if TypeIs(obj, typeof(CClientPet)) and obj.IsShadow then 
                    --处理分身的情况
                    -- 自己的分身类型是Pet且使用PetHeadInfoItem
                    -- 别人的分身类型开始也是Pet，但在创建头顶信息时使用的是PlayerHeadInfoItem
                    -- 别人的分身在InitHeadInfo的时候会被转换为其Owner（类型变成OtherPlayer）
                    -- 但有时候可能没有InitHeadInfo而先UpdateName，此时出现一种特殊情况，分身还是Pet，但头顶信息是PlayerHeadInfoItem
                    -- PlayerHeadInfoItem里isShadow=true时FollowId~=objId不能刷新显示, 需要特殊处理一下
                    local headInfo = CommonDefs.DictGetValue_LuaCall(CHeadInfoWnd.Instance.headInfoDict, obj.EngineId)
                    if headInfo then
                        headInfo = TypeAs(headInfo, typeof(CPlayerHeadInfoItem))
                        if headInfo then
                            headInfo.isShadow = false
                            headInfo:UpdateName(obj.EngineId)
                            headInfo:UpdateTitleBoard(obj.EngineId)
                            headInfo.isShadow = true
                        end
                    end
                end
            end
        end))
    end
end

--[[ 
    设置指定玩家的Name
    使用说明：doUpdate=true立即修改 or 存住数据等创建时生效
    info格式：参考NameBoardInfo，但不处理lingShouInfo和petInfo
--]]
LuaGamePlayMgr.PlayerNameBoardInfos = {}
function LuaGamePlayMgr:GetPlayerNameBoardInfo(playerId)
    return playerId and self.PlayerNameBoardInfos[playerId]
end
function LuaGamePlayMgr:SetPlayerNameBoardInfo(playerId, info, doUpdate)
    self.PlayerNameBoardInfos[playerId] = info
    if doUpdate then
        local player = CClientPlayerMgr.Inst:GetPlayer(playerId)
        if player then
            EventManager.BroadcastInternalForLua(EnumEventType.ClientObjChangeName, {player.EngineId})
        end
    end
end

--[[ 
    设置指定引擎对象的Title
    使用说明：doUpdate=true立即修改 or 存住数据等创建时生效(创建时将优先使用该信息)
    info格式：{
        -- 参数名 = 不设置时的默认值
        show = true,                                    -- 是否显示
        displayName = "",                               -- 文本内容 
        fontSize = defaultTitleFontSize,                -- 字体大小
        fontColor = NGUIText.ParseColor24("ffffff", 0), -- 字体颜色  
        bgName = DEFAULT_TITLEBOARD_SPRITE,             -- 玩家Title背景的spriteName (宠物、灵兽等其它Title不设置默认bg)
        bgVisible = false,                              -- 背景是否可见

        lingShouInfo = nil,                             -- 灵兽的TitleInfo，格式与上述info格式一致，对所有灵兽生效
        petInfo = nil,                                  -- 宠物的TitleInfo，格式与上述info格式一致，对所有宠物生效
    }
--]]
LuaGamePlayMgr.TitleBoardInfos = {}
function LuaGamePlayMgr:GetTitleBoardInfo(engineId)
    return engineId and self.TitleBoardInfos[engineId]
end
function LuaGamePlayMgr:SetTitleBoardInfo(engineId, info, doUpdate)
    self.TitleBoardInfos[engineId] = info
    if doUpdate then
        EventManager.BroadcastInternalForLua(EnumEventType.ClientObjChangeName, {engineId})
        CClientObjectMgr.Inst:ForeachObject(DelegateFactory.Action_CClientObject(function(obj)
            if (TypeIs(obj, typeof(CClientLingShou)) or TypeIs(obj, typeof(CClientPet))) and obj.m_OwnerEngineId == engineId then
                EventManager.BroadcastInternalForLua(EnumEventType.ClientObjChangeName, {obj.EngineId})
                if TypeIs(obj, typeof(CClientPet)) and obj.IsShadow then  
                    --处理分身的情况
                    -- 自己的分身类型是Pet且使用PetHeadInfoItem
                    -- 别人的分身类型开始也是Pet，但在创建头顶信息时使用的是PlayerHeadInfoItem
                    -- 别人的分身在InitHeadInfo的时候会被转换为其Owner（类型变成OtherPlayer）
                    -- 但有时候可能没有InitHeadInfo而先UpdateName，此时出现一种特殊情况，分身还是Pet，但头顶信息是PlayerHeadInfoItem
                    -- PlayerHeadInfoItem里isShadow=true时FollowId~=objId不能刷新显示, 需要特殊处理一下
                    local headInfo = CommonDefs.DictGetValue_LuaCall(CHeadInfoWnd.Instance.headInfoDict, obj.EngineId)
                    if headInfo then
                        headInfo = TypeAs(headInfo, typeof(CPlayerHeadInfoItem))
                        if headInfo then
                            headInfo.isShadow = false
                            headInfo:UpdateName(obj.EngineId)
                            headInfo:UpdateTitleBoard(obj.EngineId)
                            headInfo.isShadow = true
                        end
                    end
                end
            end
        end))
    end
end

--[[ 
    设置指定玩家的Title
    使用说明：doUpdate=true立即修改 or 存住数据等创建时生效
    info格式：参考TitleBoardInfo，但不处理lingShouInfo和petInfo
--]]
LuaGamePlayMgr.PlayerTitleBoardInfos = {}
function LuaGamePlayMgr:GetPlayerTitleBoardInfo(playerId)
    return playerId and self.PlayerTitleBoardInfos[playerId]
end
function LuaGamePlayMgr:SetPlayerTitleBoardInfo(playerId, info, doUpdate)
    self.PlayerTitleBoardInfos[playerId] = info
    if doUpdate then
        local player = CClientPlayerMgr.Inst:GetPlayer(playerId)
        if player then
            EventManager.BroadcastInternalForLua(EnumEventType.OnTitleUpdate, {player.EngineId})
        end
    end
end

--[[ 
    设置玩家头顶气泡信息
   {
        -- 参数名 = 不设置时的默认值
        fontSize = defaultTitleFontSize,                -- 字体大小
    }
--]]
LuaGamePlayMgr.PlayerSpeakBubbleInfos = {}
function LuaGamePlayMgr:GetPlayerSpeakBubbleInfo(engineId)
    return engineId and self.PlayerSpeakBubbleInfos[engineId]
end
function LuaGamePlayMgr:SetPlayerSpeakBubbleInfo(engineId, info)
    self.PlayerSpeakBubbleInfos[engineId] = info
end

--[[
设置玩家头顶名字坐标的图标显示
参数playerHeadSpeIconInfos为一个dict，key是playerId或engineId，value是一个控制显示内容的dict，例如
playerHeadSpeIconInfos[playerId] = {
    atlas = "SubUI", -- 可选"MainUI"或"SubUI"，不支持其他图集，不设置默认为"SubUI"
    spriteName = "", -- 图集中资源名
    label = "1", -- 文本，不设置默认为空
    labelColor = NGUIText.ParseColor24("FFFFFF", 0), -- 文本颜色，不设置默认为白色
}
参数useEngineId表示playerHeadSpeIconInfos的key是playerId还是engineId
]]
LuaGamePlayMgr.PlayerHeadSpeIconInfos = {}

function LuaGamePlayMgr:GetPlayerHeadSpeIconInfo(playerId, engineId)
    local dict = self.PlayerHeadSpeIconInfos.dict
    if dict then
        if self.PlayerHeadSpeIconInfos.useEngineId then
            return engineId and dict[engineId]
        else
            return playerId and dict[playerId]
        end
    end
end

function LuaGamePlayMgr:SetPlayerHeadSpeIconInfos(playerHeadSpeIconInfos, useEngineId, doUpdate)
    self.PlayerHeadSpeIconInfos = {dict = playerHeadSpeIconInfos, useEngineId = useEngineId}
    if doUpdate then
        EventManager.BroadcastInternalForLua(EnumEventType.UpdatePlayerHeadInfoSepIcon, {true})
    end
end

--[[
设置NPC头顶名字坐标的图标显示
参数npcHeadSpeIconInfos为一个dict，key是engineId，value是一个控制显示内容的dict，结构等同上面的playerHeadSpeIconInfos
]]
LuaGamePlayMgr.NpcHeadSpeIconInfos = {}

function LuaGamePlayMgr:GetNpcHeadSpeIconInfo(engineId)
    return engineId and self.NpcHeadSpeIconInfos[engineId]
end

function LuaGamePlayMgr:SetNpcHeadSpeIconInfos(npcHeadSpeIconInfos, doUpdate)
    self.NpcHeadSpeIconInfos = npcHeadSpeIconInfos
    if doUpdate then
        EventManager.BroadcastInternalForLua(EnumEventType.UpdateNpcHeadInfoSepIcon, {})
    end
end

--[[ 
    设置技能栏显示状态
    使用说明：doUpdate=true立即修改 or 存住数据等技能栏初始化时加载设置的状态(可能被后续其它操作修改)
    state格式：{ 
        -- 以下参数不设置时，不会修改原本的显示
        show,                   -- 显示技能栏
        showGuaJiBtn,           -- 显示挂机按钮
        showSwitchTargetBtn,    -- 显示切换目标按钮
        showHpRecoverBtn,       -- 显示使用回血药按钮
    }
--]]
LuaGamePlayMgr.SkillButtonBoardState = {}
function LuaGamePlayMgr:UpdateSkillButtonBoardState()
    g_ScriptEvent:BroadcastInLua("UpdateSkillButtonBoardState", self.SkillButtonBoardState)
end
function LuaGamePlayMgr:SetSkillButtonBoardState(state, doUpdate)
    self.SkillButtonBoardState = type(state) == "table" and state or {}
    if doUpdate then
        self:UpdateSkillButtonBoardState()
    end
end

--@endregion


function LuaGamePlayMgr:AddWatermark()
    --在1003 564 500服以及所有的iOS平台和PC平台试投放盲水印功能
    local serverGroupId = CLoginMgr.Inst.ServerGroupId
    local bOpen = self.m_OpenWatermarkTbl[serverGroupId] and true or false
    if CommonDefs.IsIOSPlatform() or CommonDefs.Is_PC_PLATFORM() then
        bOpen = true
    elseif SdkU3d.getChannel() == ChannelDefine.HUAWEI and NativeTools.GetAndroidAPILevel()<28 then
        bOpen = false
    end
    
    local script = CUIManager.instance.MainCamera.gameObject:GetComponent(typeof(WatermarkHelper))
    if script == nil then
        if bOpen then
            CUIManager.instance.MainCamera.gameObject:AddComponent(typeof(WatermarkHelper))
        end
    else
        script.enabled = bOpen
    end
end

function LuaGamePlayMgr:PlayerEnterGamePlay(designPlayId, sceneTemplateId, extraDataUD)
    if designPlayId == LuaHanJiaMgr:GetXueJingKuangHuanPlayId() then
        LuaHanJiaMgr:ShowXueJingKuangHuanImageRuleWnd()
    elseif designPlayId == YuanXiao_TangYuan2022Setting.GetData().GamePlayId then--狂奔的汤圆
        LuaTangYuan2022Mgr.ShowImageRuleWnd()
    elseif designPlayId == QingMing2022_PVESetting.GetData().GameplayId then
        LuaQingMing2022Mgr:EnterYHPZ()
    end
end

function LuaGamePlayMgr:UpdateYiTiaoLongTaskFinishedTimes(taskId, round, subTimes, todayTimes)
    g_ScriptEvent:BroadcastInLua("UpdateYiTiaoLongTaskFinishedTimes", taskId, round, subTimes, todayTimes)
end

function LuaGamePlayMgr:UpdateShiTuQingTaskFinishedTimes(taskId, subTimes)
    g_ScriptEvent:BroadcastInLua("UpdateShiTuQingTaskFinishedTimes", taskId, subTimes)
end

--通用的获取剩余时间显示文本的方法，主要用户任务栏区域玩法自定义显示倒计时之用
function LuaGamePlayMgr:GetRemainTimeText(totalSeconds)
    if totalSeconds <= 1 then
        return ""
    end
    local time
    if totalSeconds >= 3600 then
        time = SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        time = SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
    return time
end

function LuaGamePlayMgr:OnBuffInfoUpdate(args)
    local engineId = args[0]
    if LuaShiTuMgr:IsInJianLaJiPlay() then
        EventManager.BroadcastInternalForLua(EnumEventType.RefreshHeadInfoVisible,{engineId})
    end

    if (self.s_CurrentPlayMgr or {}).OnBuffInfoUpdate then
        EventManager.BroadcastInternalForLua(EnumEventType.RefreshHeadInfoVisible,{engineId})
        self.s_CurrentPlayMgr:OnBuffInfoUpdate(args)
    end

    if LuaShuangshiyi2023Mgr.IsInBaoYuDuoYuPlay() then
        EventManager.BroadcastInternalForLua(EnumEventType.RefreshHeadInfoVisible,{engineId})
    end
end

LuaGamePlayMgr.s_PlayId2SelectionWnd = nil

LuaGamePlayMgr.s_CurSessionId = nil
LuaGamePlayMgr.s_CurEnterGameplayId = nil
LuaGamePlayMgr.s_SelectionConfirmMsg = nil
LuaGamePlayMgr.s_SelectionCountdown = nil
LuaGamePlayMgr.s_SelectionCountdownType = nil
function LuaGamePlayMgr:ShowEnterSelectionWnd(sessionId, playId, msg, timeout, countdownType)
    self.s_CurSessionId = sessionId
    self.s_CurEnterGameplayId = playId
    self.s_SelectionConfirmMsg = msg
    self.s_SelectionCountdown = timeout
    self.s_SelectionCountdownType = countdownType

    local wnd = self.s_PlayId2SelectionWnd[playId]
    if wnd then
        CUIManager.ShowUI(CLuaUIResources[wnd])
    end
end
