local Profession=import "L10.Game.Profession"
CLuaRemoteDouDiZhuApplyListWnd = class()
RegistClassMember(CLuaRemoteDouDiZhuApplyListWnd,"m_Players")
RegistClassMember(CLuaRemoteDouDiZhuApplyListWnd,"m_NoApply")


function CLuaRemoteDouDiZhuApplyListWnd:Init()
    CLuaDouDiZhuMgr.UpdateRemoteDouDiZhuApplyAlert(false)

    self.m_NoApply = self.transform:Find("NoApplyLabel").gameObject
    self.m_NoApply:SetActive(true)
    self.m_Players = {}
    self.m_TableView = self.transform:Find("TableView"):GetComponent(typeof(QnTableView))

    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create(function()
            return #self.m_Players
        end,
        function(item,row)
            self:InitItem(item,row)
        end)

    Gac2Gas.QueryApplyJoinRemoteDouDiZhuPlayerList()

    local clearButton =  self.transform:Find("ClearButton").gameObject
    UIEventListener.Get(clearButton).onClick = DelegateFactory.VoidDelegate(function(go)
        Gac2Gas.ClearApplyJoinRemoteDouDiZhuPlayerList()
        self.m_Players={}
        self.m_NoApply:SetActive(true)
        self.m_TableView:ReloadData(true,false)
        g_ScriptEvent:BroadcastInLua("ApplyJoinRemoteDouDiZhuPlayerListAlert")
    end)
end

function CLuaRemoteDouDiZhuApplyListWnd:OnEnable()
    g_ScriptEvent:AddListener("QueryApplyJoinRemoteDouDiZhuPlayerListResult", self, "OnQueryApplyJoinRemoteDouDiZhuPlayerListResult")
    g_ScriptEvent:AddListener("ApprovePlayerJoinRemoteDouDiZhuSuccess", self, "OnApprovePlayerJoinRemoteDouDiZhuSuccess")
    g_ScriptEvent:AddListener("DisapprovePlayerJoinRemoteDouDiZhuSuccess", self, "OnDisapprovePlayerJoinRemoteDouDiZhuSuccess")
    
end

function CLuaRemoteDouDiZhuApplyListWnd:OnDisable()
    g_ScriptEvent:RemoveListener("QueryApplyJoinRemoteDouDiZhuPlayerListResult", self, "OnQueryApplyJoinRemoteDouDiZhuPlayerListResult")
    g_ScriptEvent:RemoveListener("ApprovePlayerJoinRemoteDouDiZhuSuccess", self, "OnApprovePlayerJoinRemoteDouDiZhuSuccess")
    g_ScriptEvent:RemoveListener("DisapprovePlayerJoinRemoteDouDiZhuSuccess", self, "OnDisapprovePlayerJoinRemoteDouDiZhuSuccess")
    
end

function CLuaRemoteDouDiZhuApplyListWnd:OnApprovePlayerJoinRemoteDouDiZhuSuccess(playerId)
    self:RemovePlayer(playerId)
end

function CLuaRemoteDouDiZhuApplyListWnd:RemovePlayer(playerId)
    local index = -1
    for i,v in ipairs(self.m_Players) do
        if v.playerId == playerId then
            index=i
            break
        end
    end
    if index>0 then
        table.remove( self.m_Players,index )
    end
    -- if #self.m_Players>0 then
        self.m_TableView:ReloadData(true,false)
    -- else
        -- CUIManager.CloseUI(CLuaUIResources.RemoteDouDiZhuApplyListWnd)
    -- end
end

function CLuaRemoteDouDiZhuApplyListWnd:OnDisapprovePlayerJoinRemoteDouDiZhuSuccess(playerId)
    self:RemovePlayer(playerId)
end
function CLuaRemoteDouDiZhuApplyListWnd:InitItem(item,row)
    local tf = item.transform
    local playerInfo = self.m_Players[row+1]
    local icon = tf:Find("Icon"):GetComponent(typeof(CUITexture))

    local portraitName = CUICommonDef.GetPortraitName(playerInfo.class, playerInfo.gender, playerInfo.expression)
    icon:LoadNPCPortrait(portraitName,false)

    local clsSprite = tf:Find("ClsSprite"):GetComponent(typeof(UISprite))
    clsSprite.spriteName = Profession.GetIconByNumber(playerInfo.class)
    
    local nameLabel = tf:Find("NameLabel"):GetComponent(typeof(UILabel))
    nameLabel.text = playerInfo.name

    local refuseButton = tf:Find("RefuseButton").gameObject
    local acceptButton = tf:Find("AcceptButton").gameObject

    local playerId = playerInfo.playerId
    UIEventListener.Get(refuseButton).onClick = DelegateFactory.VoidDelegate(function(g)
        Gac2Gas.DisapproveJoinRemoteDouDiZhu(playerId)--拒绝
        -- Gac2Gas.QueryApplyJoinRemoteDouDiZhuPlayerList()
        --清空 等服务器返回
        -- self.m_Players = {}
        -- self.m_TableView:ReloadData(true,false)
    end)

    UIEventListener.Get(acceptButton).onClick = DelegateFactory.VoidDelegate(function(g)
        Gac2Gas.ApproveJoinRemoteDouDiZhu(playerId)--接受
        -- Gac2Gas.QueryApplyJoinRemoteDouDiZhuPlayerList()
        -- self.m_Players = {}
        -- self.m_TableView:ReloadData(true,false)
    end)
end

function CLuaRemoteDouDiZhuApplyListWnd:OnQueryApplyJoinRemoteDouDiZhuPlayerListResult(t)
    self.m_Players = t

    if #self.m_Players>0 then
        self.m_NoApply:SetActive(false)
    else
        self.m_NoApply:SetActive(true)
        --不显示红点
        g_ScriptEvent:BroadcastInLua("ApplyJoinRemoteDouDiZhuPlayerListAlert")
    end
    self.m_TableView:ReloadData(true,false)
end