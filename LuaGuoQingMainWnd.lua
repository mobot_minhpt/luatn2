local UILabel = import "UILabel"

local GameObject = import "UnityEngine.GameObject"
local CUIManager = import "L10.UI.CUIManager"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local DateTime = import "System.DateTime"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Animator = import "UnityEngine.Animator"
local CTaskMgr = import "L10.Game.CTaskMgr"

LuaGuoQingMainWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGuoQingMainWnd, "WanFaMingCheng", "WanFaMingCheng", GameObject)
RegistChildComponent(LuaGuoQingMainWnd, "WanFaMingCheng2", "WanFaMingCheng2", GameObject)
RegistChildComponent(LuaGuoQingMainWnd, "JieRiLiBao", "JieRiLiBao", GameObject)
RegistChildComponent(LuaGuoQingMainWnd, "JingYanYu", "JingYanYu", GameObject)
RegistChildComponent(LuaGuoQingMainWnd, "RiChangLiBao", "RiChangLiBao", GameObject)
RegistChildComponent(LuaGuoQingMainWnd, "Time_Label", "Time_Label", UILabel)

--@endregion RegistChildComponent end

RegistClassMember(LuaGuoQingMainWnd, "expRainActivityID")
RegistClassMember(LuaGuoQingMainWnd, "dailyGiftActivityID")
RegistClassMember(LuaGuoQingMainWnd, "mainOpenTimeDisplay")
RegistClassMember(LuaGuoQingMainWnd, "mainAnimator")
RegistClassMember(LuaGuoQingMainWnd, "MainPVPOpenTimeDisplay")
RegistClassMember(LuaGuoQingMainWnd, "pvptaskId")
function LuaGuoQingMainWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    Gac2Gas.QueryJinLuHunYuanZhanOpenStatus()
    local setting = GuoQing2022_Setting.GetData()
	self.expRainActivityID = setting.ExpRainActivityID
    self.dailyGiftActivityID = setting.DailyGiftActivityID
    self.mainOpenTimeDisplay = setting.MainOpenTimeDisplay
    self.MainPVPOpenTimeDisplay = setting.MainPVPOpenTimeDisplay
	self.pvptaskId = setting.MainPVPTaskID
    UIEventListener.Get(self.WanFaMingCheng).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOpenPvEButtonClick(go)
	end)
    UIEventListener.Get(self.WanFaMingCheng2).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOpenPvPButtonClick(go)
	end)
    UIEventListener.Get(self.JieRiLiBao).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOpenJieRiLiBaoButtonClick(go)
	end)
    UIEventListener.Get(self.JingYanYu).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOpenJingYanYuButtonClick(go)
	end)
    UIEventListener.Get(self.RiChangLiBao).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOpenRiChangLiBaoButtonClick(go)
	end)
    self.Time_Label.text = self.mainOpenTimeDisplay
    self.mainAnimator = self.transform:GetComponent(typeof(Animator))
    self.WanFaMingCheng2.transform:Find("Time"):GetComponent(typeof(UILabel)).text = self.MainPVPOpenTimeDisplay

    
    
    
--[[
    if self.pvptaskId and CTaskMgr.Inst:CheckTaskTime(self.pvptaskId) then
        renClose:SetActive(false)
        renOpen:SetActive(true)
        Bg_Vfx:SetActive(true)
    else
        renClose:SetActive(true)
        renOpen:SetActive(false)
        Bg_Vfx:SetActive(false)
    end
]]--
end

function LuaGuoQingMainWnd:Init()
    

end

function  LuaGuoQingMainWnd:OnEnable()
    
    g_ScriptEvent:AddListener("ReplyJinLuHunYuanZhanOpenStatus", self, "OnJinLuHunYuanZhanOpenStatus")
end

function LuaGuoQingMainWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ReplyJinLuHunYuanZhanOpenStatus", self, "OnJinLuHunYuanZhanOpenStatus")
end
--@region UIEvent

function  LuaGuoQingMainWnd:OnJinLuHunYuanZhanOpenStatus(status)
    local renClose = FindChild(self.transform,"renClose").gameObject
    local renOpen = FindChild(self.transform,"renOpen").gameObject
    local Bg_Vfx = self.WanFaMingCheng2.transform:Find("Bg_Vfx").gameObject
    if status == true then
        renClose:SetActive(false)
        renOpen:SetActive(true)
        Bg_Vfx:SetActive(true)
    else
        renClose:SetActive(true)
        renOpen:SetActive(false)
        Bg_Vfx:SetActive(false)
    end
    self.mainAnimator.enabled = true
end


function  LuaGuoQingMainWnd:OnOpenJingYanYuButtonClick(go)
    local thisactivityId = self.expRainActivityID
    local schedule = Task_Schedule.GetData(thisactivityId)
    local thistaskiId = schedule.TaskID[0]
    CLuaScheduleMgr.selectedScheduleInfo={
        activityId = thisactivityId,
        taskId = thistaskiId,

        TotalTimes = 0,--不显示次数
        DailyTimes = 0,--不显示活力
        FinishedTimes = 0,
        
        ActivityTime = schedule.ExternTip,
        ShowJoinBtn = false -- 等级不够就不显示参加按钮了
    }
    CLuaScheduleInfoWnd.s_UseCustomInfo = true
    CUIManager.ShowUI(CLuaUIResources.ScheduleInfoWnd)
end
function  LuaGuoQingMainWnd:OnOpenRiChangLiBaoButtonClick(go)
    local thisactivityId = self.dailyGiftActivityID
    local schedule = Task_Schedule.GetData(thisactivityId)
    local thistaskiId = schedule.TaskID[0]
    CLuaScheduleMgr.selectedScheduleInfo={
        activityId = thisactivityId,
        taskId = thistaskiId,

        TotalTimes = 0,--不显示次数
        DailyTimes = 0,--不显示活力
        FinishedTimes = 0,
        ActivityTime = schedule.ExternTip,
        ShowJoinBtn = false -- 等级不够就不显示参加按钮了
    }
    CLuaScheduleInfoWnd.s_UseCustomInfo = true
    CUIManager.ShowUI(CLuaUIResources.ScheduleInfoWnd)
end
function  LuaGuoQingMainWnd:OnOpenPvEButtonClick(go)
    CUIManager.ShowUI(CLuaUIResources.GuoQingPvEWnd)
end
function  LuaGuoQingMainWnd:OnOpenPvPButtonClick(go)
    self.mainAnimator:Play("guoqingmainwnd_hide", 0, 0) 
    CUIManager.ShowUI(CLuaUIResources.GuoQingPvPWnd)
    --RegisterTickOnce(function()  
    --    
    --    CUIManager.ShowUI(CLuaUIResources.GuoQingPvPWnd)
	--end, 150)
    
end
function  LuaGuoQingMainWnd:OnOpenJieRiLiBaoButtonClick(go)
    CShopMallMgr.ShowLingyuLiBaoShop()
end
--@endregion UIEvent

