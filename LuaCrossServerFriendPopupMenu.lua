local CCommonLuaWnd=import "L10.UI.CCommonLuaWnd"
local NGUIText = import "NGUIText"
local NGUIMath = import "NGUIMath"
local Extensions = import "Extensions"
local Constants = import "L10.Game.Constants"
local CExpressionMgr = import "L10.Game.CExpressionMgr"
local CUITexture=import "L10.UI.CUITexture"
local CButton=import "L10.UI.CButton"

LuaCrossServerFriendPopupMenu = class()
RegistClassMember(LuaCrossServerFriendPopupMenu,"m_BasicInfoRoot")
RegistClassMember(LuaCrossServerFriendPopupMenu,"m_Background")
RegistClassMember(LuaCrossServerFriendPopupMenu,"m_Portrait")
RegistClassMember(LuaCrossServerFriendPopupMenu,"m_ProfileFrame")
RegistClassMember(LuaCrossServerFriendPopupMenu,"m_ExpressionTxt")
RegistClassMember(LuaCrossServerFriendPopupMenu,"m_NameLabel")
RegistClassMember(LuaCrossServerFriendPopupMenu,"m_LevelLabel")
RegistClassMember(LuaCrossServerFriendPopupMenu,"m_ServerNameLabel")
RegistClassMember(LuaCrossServerFriendPopupMenu,"m_GuildNameLabel")
RegistClassMember(LuaCrossServerFriendPopupMenu,"m_SignatureLabel")
RegistClassMember(LuaCrossServerFriendPopupMenu,"m_ButtonTemplate")
RegistClassMember(LuaCrossServerFriendPopupMenu,"m_ImageButtonTemplate")
RegistClassMember(LuaCrossServerFriendPopupMenu,"m_Grid")
RegistClassMember(LuaCrossServerFriendPopupMenu,"m_Buttons")
RegistClassMember(LuaCrossServerFriendPopupMenu,"m_Wnd")


function LuaCrossServerFriendPopupMenu:Awake()
    self.m_BasicInfoRoot = self.transform:Find("Anchor/Background/BasicInfo").gameObject
    self.m_Background = self.transform:Find("Anchor/Background"):GetComponent(typeof(UISprite))
    self.m_Portrait = self.transform:Find("Anchor/Background/BasicInfo/Border/Portrait"):GetComponent(typeof(CUITexture))
    self.m_ProfileFrame = self.transform:Find("Anchor/Background/BasicInfo/Border/ProfileFrame"):GetComponent(typeof(CUITexture))
    self.m_ExpressionTxt = self.transform:Find("Anchor/Background/BasicInfo/Border/ExpressionTxt"):GetComponent(typeof(CUITexture))
    self.m_NameLabel = self.transform:Find("Anchor/Background/BasicInfo/NameLabel"):GetComponent(typeof(UILabel))
    self.m_LevelLabel = self.transform:Find("Anchor/Background/BasicInfo/LevelLabel"):GetComponent(typeof(UILabel))
    self.m_ServerNameLabel = self.transform:Find("Anchor/Background/BasicInfo/ServerNameLabel/ServerName"):GetComponent(typeof(UILabel))
    self.m_GuildNameLabel = self.transform:Find("Anchor/Background/BasicInfo/GuildNameLabel/GuildName"):GetComponent(typeof(UILabel))
    self.m_SignatureLabel = self.transform:Find("Anchor/Background/BasicInfo/SignatureLabel"):GetComponent(typeof(UILabel))
    self.m_ButtonTemplate = self.transform:Find("Anchor/TemplateButton1").gameObject
    self.m_ImageButtonTemplate = self.transform:Find("Anchor/TemplateButton2").gameObject
    self.m_Grid = self.transform:Find("Anchor/Background/ButtonsGrid"):GetComponent(typeof(UIGrid))
    self.m_Wnd=self.gameObject:GetComponent(typeof(CCommonLuaWnd))

    self.m_Buttons = {}
    self.m_ButtonTemplate:SetActive(false)
    self.m_ImageButtonTemplate:SetActive(false)
end

function LuaCrossServerFriendPopupMenu:Init( )
    self:LoadItems()
end

function LuaCrossServerFriendPopupMenu:LoadItems( )
    local info = g_PopupMenuMgr.m_CrossServerFriendInfo
    local portraitName = info.portraitName or CUICommonDef.GetPortraitName(info.cls, info.gender, info.expression)
    self.m_Portrait:LoadNPCPortrait(portraitName, false)
    if self.m_ProfileFrame ~= nil and CExpressionMgr.EnableProfileFrame then
        self.m_ProfileFrame:LoadMaterial(CUICommonDef.GetProfileFramePath(info.frame))
    end
    self.m_ExpressionTxt:LoadMaterial(CUICommonDef.GetExpressionTxtPath(info.expressionTxt))

    local name = info.playerName
    if LuaLiangHaoMgr.LiangHaoEnabled() and info.lianghaoIcon~=nil then
        name = name..info.lianghaoIcon
    end
    self.m_NameLabel.text = name
    local default
    if info.xianfanstatus>0 then
        default = Constants.ColorOfFeiSheng
    else
        default = NGUIText.EncodeColor24(self.m_LevelLabel.color)
    end

    self.m_LevelLabel.text = SafeStringFormat3("[c][%s]%s[-][/c]", default, info.level)
    self.m_ServerNameLabel.text = info.serverName
    self.m_GuildNameLabel.text = info.guildName
    if info.signature~="" and info.signature~=nil then
        self.m_SignatureLabel.text = LocalString.GetString("暂无签名");
    else
        self.m_SignatureLabel.text = info.signature
    end

    Extensions.RemoveAllChildren(self.m_Grid.transform)
    self.m_Buttons={}

    for __,data in pairs(g_PopupMenuMgr.m_CrossServerFriendPopupMenuItems) do
        local tempalte
        if not System.String.IsNullOrEmpty(data.imageName) then
            tempalte = self.m_ImageButtonTemplate
        else
            tempalte = self.m_ButtonTemplate
        end
        local instance = CUICommonDef.AddChild(self.m_Grid.gameObject, tempalte)
        instance:SetActive(true)

        local button = instance:GetComponent(typeof(CButton))
        local label = instance.transform:Find("Label"):GetComponent(typeof(UILabel))
        local iconTransform = instance.transform:Find("Sprite")
        local iconSprite = iconTransform and iconTransform:GetComponent(typeof(UISprite)) or nil


        label.text = data.text
        if iconSprite then
            if not System.String.IsNullOrEmpty(data.imageName) then
                iconSprite.enabled = true
                iconSprite.spriteName = data.imageName
            else
                iconSprite.enabled = false
            end
        end

        CommonDefs.AddOnClickListener(instance, DelegateFactory.Action_GameObject(function(go) self:OnMenuItemClick(go) end), false)

        table.insert(self.m_Buttons, {go=instance,action=data.action})
        if data.action == nil then
            button.Enabled=false
        end
    end

    self.m_Grid:Reposition()
    self.m_Background.height = math.floor(NGUIMath.CalculateRelativeWidgetBounds(self.m_Grid.transform).size.y) + math.floor(NGUIMath.CalculateRelativeWidgetBounds(self.m_BasicInfoRoot.transform).size.y) + 50
    local posRelativeToWndRoot = CUICommonDef.AdjustPosition(info.alignType, self.m_Background.width, self.m_Background.height, info.targetWorldPos, info.targetWidth, info.targetHeight)
    posRelativeToWndRoot.y = posRelativeToWndRoot.y + self.m_Background.height*0.5
    self.m_Background.transform.localPosition = posRelativeToWndRoot
end


function LuaCrossServerFriendPopupMenu:OnMenuItemClick(go) 
    self:Close()
    for __,item in pairs(self.m_Buttons) do
        if item.go == go then
            local info = g_PopupMenuMgr.m_CrossServerFriendInfo
            item.action(info.playerId, info.playerName)
            break
        end
    end
end

function LuaCrossServerFriendPopupMenu:Update()
    self.m_Wnd:ClickThroughToClose()
end

function LuaCrossServerFriendPopupMenu:Close()
    self.m_Wnd:Close()
end
