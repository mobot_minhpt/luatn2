local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UISlider = import "UISlider"
local CButton = import "L10.UI.CButton"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local UIGrid = import "UIGrid"
local CQnSymbolParser = import "CQnSymbolParser"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Extensions = import "Extensions"
local DateTime = import "System.DateTime"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local EnumQMPKPlayStage = import "L10.Game.EnumQMPKPlayStage"
local Item_Item = import "L10.Game.Item_Item"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CUIFx = import "L10.UI.CUIFx"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local PathMode = import "DG.Tweening.PathMode"
local PathType = import "DG.Tweening.PathType"
local Ease = import "DG.Tweening.Ease"
local Vector4 = import "UnityEngine.Vector4"
LuaScheduleQMPKPage = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaScheduleQMPKPage, "Progress", "Progress", CUIRestrictScrollView)
RegistChildComponent(LuaScheduleQMPKPage, "AwardOverviewButton", "AwardOverviewButton", CButton)
RegistChildComponent(LuaScheduleQMPKPage, "AgendaButton", "AgendaButton", CButton)
RegistChildComponent(LuaScheduleQMPKPage, "TodayNoticeLab", "TodayNoticeLab", UILabel)
RegistChildComponent(LuaScheduleQMPKPage, "TomorrowNoticeLab", "TomorrowNoticeLab", UILabel)
RegistChildComponent(LuaScheduleQMPKPage, "MatchButton", "MatchButton", CButton)
RegistChildComponent(LuaScheduleQMPKPage, "HandBookButton", "HandBookButton", CButton)
RegistChildComponent(LuaScheduleQMPKPage, "GoToButton", "GoToButton", CButton)
RegistChildComponent(LuaScheduleQMPKPage, "Calendar", "Calendar", GameObject)
RegistChildComponent(LuaScheduleQMPKPage, "TopFour", "TopFour", GameObject)
RegistChildComponent(LuaScheduleQMPKPage, "TopFourGrid", "TopFourGrid", GameObject)
RegistChildComponent(LuaScheduleQMPKPage, "PhaseGrid", "PhaseGrid", UIGrid)
RegistChildComponent(LuaScheduleQMPKPage, "AddPointNoticeLab", "AddPointNoticeLab", UILabel)
RegistChildComponent(LuaScheduleQMPKPage, "DetailButton", "DetailButton", CButton)
RegistChildComponent(LuaScheduleQMPKPage, "AwardProgress", "AwardProgress", GameObject)
RegistChildComponent(LuaScheduleQMPKPage, "AwardTemplate", "AwardTemplate", GameObject)
RegistChildComponent(LuaScheduleQMPKPage, "AwardRoot", "AwardRoot", GameObject)
RegistChildComponent(LuaScheduleQMPKPage, "AwardProgressBar", "AwardProgressBar", UISlider)
RegistChildComponent(LuaScheduleQMPKPage, "ProgressBar", "ProgressBar", UISlider)

--@endregion RegistChildComponent end
RegistClassMember(LuaScheduleQMPKPage, "m_TodayNotice")
RegistClassMember(LuaScheduleQMPKPage, "m_TomorrowNotice")
RegistClassMember(LuaScheduleQMPKPage, "m_RedDot")
RegistClassMember(LuaScheduleQMPKPage, "m_IsSignUp")
RegistClassMember(LuaScheduleQMPKPage, "m_IsFinish")
RegistClassMember(LuaScheduleQMPKPage, "m_ProgressNodes")
RegistClassMember(LuaScheduleQMPKPage, "m_ProgressNodeInfos")
RegistClassMember(LuaScheduleQMPKPage, "m_WhiteColor")
RegistClassMember(LuaScheduleQMPKPage, "m_YellowColor")
RegistClassMember(LuaScheduleQMPKPage, "m_GrayColor")
RegistClassMember(LuaScheduleQMPKPage, "m_SpriteColor")
RegistClassMember(LuaScheduleQMPKPage, "m_SpriteLow")
RegistClassMember(LuaScheduleQMPKPage, "m_SpriteHigh")
RegistClassMember(LuaScheduleQMPKPage, "m_SliderFore")
RegistClassMember(LuaScheduleQMPKPage, "m_SliderBack")
RegistClassMember(LuaScheduleQMPKPage, "m_PhaseTemplate")
RegistClassMember(LuaScheduleQMPKPage, "m_PhasesCal")
RegistClassMember(LuaScheduleQMPKPage, "m_StarButton")
RegistClassMember(LuaScheduleQMPKPage, "m_ReportButton")
RegistClassMember(LuaScheduleQMPKPage, "m_CurTitle")
RegistClassMember(LuaScheduleQMPKPage, "m_AwardProgressViewList")
RegistClassMember(LuaScheduleQMPKPage, "m_AwardProgressMaxNum")
RegistClassMember(LuaScheduleQMPKPage, "m_MainFx")
function LuaScheduleQMPKPage:Awake()
    self.m_TodayNotice      = self.Calendar.transform:Find("TodayNotice").gameObject
    self.m_TomorrowNotice   = self.Calendar.transform:Find("TomorrowNotice").gameObject
    self.m_RedDot           = self.HandBookButton.transform:Find("RedDot").gameObject
    self.m_WhiteColor       = NGUIText.ParseColor24("fff6b8", 0)
    self.m_YellowColor      = NGUIText.ParseColor24("ffd74b", 0)
    self.m_GrayColor        = NGUIText.ParseColor24("a98e75", 0)
    self.m_SpriteColor      = NGUIText.ParseColor24("fff481", 0)
    self.m_SpriteLow        = 18
    self.m_SpriteHigh       = 28
    self.m_SliderFore       = self.ProgressBar.transform:Find("Foreground"):GetComponent(typeof(UISprite))
    self.m_SliderBack       = self.ProgressBar.transform:Find("Background"):GetComponent(typeof(UISprite))
    self.m_PhaseTemplate    = self.Progress.transform:Find("PhaseTemplate").gameObject
    self.m_StarButton       = self.transform:Find("StarButton").gameObject
    self.m_ReportButton     = self.transform:Find("ReportButton").gameObject
    self.m_AwardProgressViewList = nil
    self.m_AwardProgressMaxNum = 0
    self.m_MainFx = self.transform:Find("CUIFx").gameObject
    self.m_MainFx.gameObject:SetActive(true)
    self.TopFour:SetActive(false)
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    UIEventListener.Get(self.m_StarButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        CUIManager.ShowUI(CLuaUIResources.QMPKStarWnd)
	end)

    UIEventListener.Get(self.m_ReportButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        CLuaShareMgr.ShareQMPKZhanBao()
	end)

    UIEventListener.Get(self.AwardOverviewButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        CUIManager.ShowUI(CLuaUIResources.QMPKRewardWnd)
	end)

    UIEventListener.Get(self.AgendaButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        CUIManager.ShowUI(CLuaUIResources.QMPKAgendaWnd)
	end)

    UIEventListener.Get(self.DetailButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        g_MessageMgr:ShowMessage("QMPK_HuoLi2BuffList_Tip")
	end)

    UIEventListener.Get(self.MatchButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        if string.find(self.m_CurTitle, LocalString.GetString("积分赛")) or string.find(self.m_CurTitle, LocalString.GetString("热身赛")) then 
            Gac2Gas.QueryQmpkWatchList()
        elseif string.find(self.m_CurTitle, LocalString.GetString("淘汰赛")) then 
            CQuanMinPKMgr.Inst.m_WatchListWndType = EnumQMPKPlayStage.eTaoTaiSai
            CUIManager.ShowUI(CLuaUIResources.QMPKTaoTaiSaiMatchRecordWnd)
        elseif string.find(self.m_CurTitle, LocalString.GetString("总决赛")) then
            CQuanMinPKMgr.Inst.m_WatchListWndType = EnumQMPKPlayStage.eZongJueSai
            CUIManager.ShowUI(CLuaUIResources.QMPKTaoTaiSaiMatchRecordWnd)
        end
	end)

    UIEventListener.Get(self.HandBookButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        if CLuaQMPKMgr.IsPlayerSignUp() then
            CUIManager.ShowUI(CLuaUIResources.QMPKHandBookWnd)
        else
            g_MessageMgr:ShowMessage("QMPK_Cannot_Open_HandBook")
        end
	end)

    UIEventListener.Get(self.GoToButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        if self.m_IsFinish then
            CUIManager.ShowUI(CLuaUIResources.QMPKTopFourWnd)
            return
        end
        if self.m_IsSignUp then
            Gac2Gas.QueryQmpkCreateRoleInfo()
        else
            CUIManager.ShowUI(CLuaUIResources.QMPKRegisterWnd)
        end
	end)


end

function LuaScheduleQMPKPage:Start()
    CLuaQMPKMgr.SetQMPKPageClicked()
    EventManager.Broadcast(EnumEventType.UpdateScheduleAlert)

    self:ProcessIsSignUp()
    self:ProcessAgenda()
    self:ProcessHandBookRedDot()
    self:OnUpdatePageInfo()
    Gac2Gas.QueryQmpkZongJueSaiOverView2()
end

function LuaScheduleQMPKPage:OnEnable()
    g_ScriptEvent:AddListener("UpdateTempPlayDataWithKey", self, "OnUpdateTempPlayDataWithKey")
    g_ScriptEvent:AddListener("ProcessHandBookRedDot", self, "OnSendQmpkHandbookReceiveAwardInfo")
    g_ScriptEvent:AddListener("ReplyQmpkZongJueSaiOverView2", self, "OnReplyQmpkZongJueSaiOverView2")
    g_ScriptEvent:AddListener("ReplyQmpkActivityPageMeedInfo", self, "OnUpdatePageInfo")
end

function LuaScheduleQMPKPage:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateTempPlayDataWithKey", self, "OnUpdateTempPlayDataWithKey")
    g_ScriptEvent:RemoveListener("ProcessHandBookRedDot", self, "OnSendQmpkHandbookReceiveAwardInfo")
    g_ScriptEvent:RemoveListener("ReplyQmpkZongJueSaiOverView2", self, "OnReplyQmpkZongJueSaiOverView2")
    g_ScriptEvent:RemoveListener("ReplyQmpkActivityPageMeedInfo", self, "OnUpdatePageInfo")
end

function LuaScheduleQMPKPage:InitAwardProgress()
    local awardProgressList = QuanMinPK_Setting.GetData().SignUpNum2ItemId

    self.m_AwardProgressViewList = {}
    self.AwardTemplate.gameObject:SetActive(false)
    Extensions.RemoveAllChildren(self.AwardRoot.transform)
    for i = 0,awardProgressList.Length - 1 do
        local num = awardProgressList[i][0]
        local itemId = awardProgressList[i][1]
        if num > self.m_AwardProgressMaxNum then self.m_AwardProgressMaxNum = num end
        local Item = CUICommonDef.AddChild(self.AwardRoot.gameObject,self.AwardTemplate.gameObject)
        local ItemCell = Item.transform:Find("ItemCell").gameObject
        Item.gameObject:SetActive(true)
        local Icon = Item.transform:Find("ItemCell/IconTexture"):GetComponent(typeof(CUITexture))
        local Mask = Item.transform:Find("ItemCell/Mask").gameObject
        local CUIFx = Item.transform:Find("ItemCell/CUIFx"):GetComponent(typeof(CUIFx))
        local CountLabel = Item.transform:Find("CountLabel"):GetComponent(typeof(UILabel))
        local itemData = Item_Item.GetData(itemId)
        CUIFx:DestroyFx()
		CUIFx.gameObject:SetActive(false)
        Mask.gameObject:SetActive(false)
        CUIFx.gameObject:SetActive(false)
        CountLabel.text = tostring(num)
        Icon:LoadMaterial(itemData.Icon)
        self.m_AwardProgressViewList[num] = {
            ItemView = Item,
            Mask = Mask,
            Num = num,
            CUIFx = CUIFx,
            ItemId = itemId,
        }
        UIEventListener.Get(ItemCell.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnProgressItemClick(num)
        end)
    end
    if self.m_AwardProgressMaxNum <= 0 then return end
    local maxLength = (self.AwardProgressBar.transform:Find("Foreground"):GetComponent(typeof(UITexture)).width) * 0.9
    for k,v in pairs(self.m_AwardProgressViewList) do
        local maxNum = self.m_AwardProgressMaxNum
        local curNum = k
        local item = v.ItemView
        LuaUtils.SetLocalPosition(item.transform, maxLength * (curNum / maxNum), 0, 0)
    end

    self:UpdateAwardProgress()
end

function LuaScheduleQMPKPage:UpdateAwardProgress()
    if not self.m_AwardProgressViewList then self:InitAwardProgress() end
    if not self.m_AwardProgressViewList or not CLuaQMPKMgr.m_QmpkActivityPageInfo then return end
    local receiveList = CLuaQMPKMgr.m_QmpkActivityPageInfo.receiveList
    local maxNum = CLuaQMPKMgr.m_QmpkActivityPageInfo.totalSignUpNum
    for k,v in pairs(self.m_AwardProgressViewList) do
        local curNum = k
        v.Mask.gameObject:SetActive(receiveList[curNum])
        v.CUIFx.gameObject:SetActive(curNum <= maxNum and not receiveList[curNum])
        if (curNum <= maxNum and  not receiveList[curNum] ) then
            LuaTweenUtils.DOKill(v.CUIFx.transform, true)
            v.CUIFx.gameObject:SetActive(true)
            local b = NGUIMath.CalculateRelativeWidgetBounds(v.ItemView.transform:Find("ItemCell/IconTexture").transform)
            local waypoints = CUICommonDef.GetWayPoints(Vector4(b.min.x, b.max.y + 6, b.size.x + 5, b.size.y + 8), 1, true)
            v.CUIFx:LoadFx(CUIFxPaths.CommonBlueLineFxPath)
            v.CUIFx.transform.localPosition = waypoints[0]
            LuaTweenUtils.DOLuaLocalPath(v.CUIFx.transform, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, - 1, Ease.Linear)
        else
            v.CUIFx.gameObject:SetActive(false)
            v.CUIFx:DestroyFx()
            LuaTweenUtils.DOKill(v.CUIFx.transform, true)
        end
        --LuaUtils.SetLocalPositionZ(v.ItemView.transform,curNum > maxNum and -1 or 0 )
        v.ItemView.transform:Find("ItemCell/Background_kelingqu").gameObject:SetActive(curNum <= maxNum)
    end
    self.AwardProgressBar.value = (maxNum * 0.9) / (self.m_AwardProgressMaxNum)
end

function LuaScheduleQMPKPage:OnProgressItemClick(num)
    if not self.m_AwardProgressViewList or not CLuaQMPKMgr.m_QmpkActivityPageInfo then return end
    local data = self.m_AwardProgressViewList[num]
    local receiveList = CLuaQMPKMgr.m_QmpkActivityPageInfo.receiveList
    local totalNum = CLuaQMPKMgr.m_QmpkActivityPageInfo.totalSignUpNum
    if num <= totalNum and not receiveList[num] then  -- 可以领但还未领取
        Gac2Gas.ReceiveQmpkSignUpNumAward(num)
    else
        local id = data.ItemId
        CItemInfoMgr.ShowLinkItemTemplateInfo(id, false, nil, AlignType.Default, 0, 0, 0, 0)
    end
end

--@region UIEvent

--@endregion UIEvent

function LuaScheduleQMPKPage:OnUpdatePageInfo()
    if not CLuaQMPKMgr.m_QmpkActivityPageInfo then return end
    local huoli = CLuaQMPKMgr.m_QmpkActivityPageInfo.huoLi
    local huoliList = QuanMinPK_Setting.GetData().HuoLi2BuffId
    local level = 0
    for i = 0,huoliList.Length - 1 do
        if huoli >= tonumber(huoliList[i][0]) then
            buffId = tonumber(huoliList[i][1])
            level = buffId % 100
        end
    end
    self.AddPointNoticeLab.text = SafeStringFormat3(LocalString.GetString("%d级"),level)
    self:UpdateAwardProgress()
end

function LuaScheduleQMPKPage:OnUpdateTempPlayDataWithKey()
    self:ProcessIsSignUp()
end

function LuaScheduleQMPKPage:OnSendQmpkHandbookReceiveAwardInfo()
    self:ProcessHandBookRedDot()
end

function LuaScheduleQMPKPage:OnReplyQmpkZongJueSaiOverView2(isFinish, tab)
    self.m_IsFinish = isFinish == 1
    self.Calendar:SetActive(not self.m_IsFinish)
    self.TopFour:SetActive(self.m_IsFinish)
    self.m_MainFx.gameObject:SetActive(not self.m_IsFinish)
	if self.m_IsFinish then
        self.GoToButton.Text = LocalString.GetString("本届四强")
        for i = 0, self.TopFourGrid.transform.childCount - 1 do 
            local item = self.TopFourGrid.transform:GetChild(i)
            local name = item.transform:Find("TeamNameLab"):GetComponent(typeof(UILabel))
            name.text = tab[i + 1] and tab[i + 1].zhanduiTitle
        end
    end
end


function LuaScheduleQMPKPage:ProcessIsSignUp()
    self.m_IsSignUp = CLuaQMPKMgr.IsPlayerSignUp()
    self.GoToButton.Text = self.m_IsSignUp and LocalString.GetString("进入比赛服") or LocalString.GetString("我要参加")
    self.HandBookButton.gameObject:SetActive(true)
end

function LuaScheduleQMPKPage:ProcessHandBookRedDot()
    local isAward = CLuaQMPKMgr.m_IsHandBookCanRecive or CLuaQMPKMgr.FindNextMail() ~= nil
    self.m_RedDot:SetActive(isAward)
end

function LuaScheduleQMPKPage:ProcessAgenda()
    local todayStr = LocalString.GetString("暂无")
    local tomorrowStr = LocalString.GetString("暂无")
    local now = CServerTimeMgr.Inst:GetZone8Time()
    local formatted_date = tonumber(self:FormatDate(now))
    local foundKey = nil
    local curPhaseNode = 1
    local not_start = false
    local no_today = false
    local no_tomorrow = false
    
    self.m_PhasesCal = {}
    self.m_ProgressNodeInfos = {}
    QuanMinPK_Schedule.Foreach(function (key, data)
        self.m_PhasesCal[data.ProgressNode] = true
        local progressNode = data.ProgressNode
        local isInProgress = false
        local isPassed = false
        local data_date = nil
        if(data.Date and data.Date ~= "")then
            data_date = tonumber(self:FormatDataDate(tostring(data.Date)))
            if(formatted_date == data_date)then
                todayStr = CQnSymbolParser.ConvertQnTextToNGUIText(data.Description)
                foundKey = key
                isInProgress = true
            elseif(formatted_date < data_date)then
                not_start = true
            else
                isPassed = true
            end
        end
        if progressNode > 0 then
            if isInProgress or isPassed then
                curPhaseNode = progressNode
            end
            if not self.m_ProgressNodeInfos[progressNode] then
                self.m_ProgressNodeInfos[progressNode] = {NodeTitle = data.NodeTitle, NodeTime = data.NodeTime, DateTime = data.Date ~= "" and DateTime.Parse(tostring(data.Date)) or nil}
            end
            if isInProgress then
                self.m_CurTitle = data.NodeTitle
                self.m_ProgressNodeInfos[progressNode].InPorgress = true
            end
            if isPassed then
                self.m_ProgressNodeInfos[progressNode].Passed = true
            end
        end
    end)

    if foundKey then
        local data = QuanMinPK_Schedule.GetData(foundKey + 1)
        if data and data.Date and data.Date ~= "" then
            tomorrowStr = CQnSymbolParser.ConvertQnTextToNGUIText(data.Description)
        else
            no_tomorrow = true
        end
    else
        no_today = true
    end
    
    self.TodayNoticeLab.text = todayStr
    self.TomorrowNoticeLab.text = tomorrowStr
    self.m_TomorrowNotice:SetActive(false)
    --Extensions.SetLocalPositionY(self.m_TodayNotice.transform, no_tomorrow and -42.4 or -23.9)

    -- 子进度
    local subProgress = 0
    local curInfo = self.m_ProgressNodeInfos[curPhaseNode]
    local nextInfo = self.m_ProgressNodeInfos[curPhaseNode + 1]
    if curInfo and curInfo.Passed and nextInfo then
        local curDate = curInfo.DateTime
        local nextDate = nextInfo.DateTime
        if curDate and nextDate then
            subProgress = CServerTimeMgr.Inst:DayDiff(now, curDate) / CServerTimeMgr.Inst:DayDiff(nextDate, curDate)
        end
    end

    local watchEnable = self.m_CurTitle and (string.find(self.m_CurTitle, LocalString.GetString("积分赛")) or string.find(self.m_CurTitle, LocalString.GetString("热身赛")) or
        string.find(self.m_CurTitle, LocalString.GetString("总决赛")) or string.find(self.m_CurTitle, LocalString.GetString("淘汰赛")))
    self.MatchButton.gameObject:SetActive(watchEnable)

    self:InitPhases(curPhaseNode, subProgress)
end

function LuaScheduleQMPKPage:FormatDate(_date)
    local return_val = _date.Year
    if(_date.Month < 10)then
        return_val = return_val.."0"
    end
    return_val = return_val.._date.Month
    if(_date.Day < 10)then
        return_val = return_val.."0"
    end
    return_val = return_val.._date.Day
    return return_val
end

function LuaScheduleQMPKPage:FormatDataDate(_date)
    if(_date and _date ~= "")then
        local return_val = ""
        local words = {}
        for w in string.gmatch(_date, "%d+") do
            table.insert(words, w)
        end
        return_val = words[1]..words[2]..words[3]
        return return_val
    end
end

function LuaScheduleQMPKPage:InitPhases(curPhaseNode, subProgress)
    self.m_PhaseTemplate:SetActive(false)
    local nodeNum = #self.m_PhasesCal
    self.m_ProgressNodes = {}
    Extensions.RemoveAllChildren(self.PhaseGrid.transform)
    for i = 1, nodeNum do
        local go = NGUITools.AddChild(self.PhaseGrid.gameObject, self.m_PhaseTemplate)
        go:SetActive(true)
        table.insert(self.m_ProgressNodes, go.transform)
    end

    local sliderWidth = (nodeNum - 1) * 190
    self.m_SliderFore.width = sliderWidth
    self.m_SliderBack.width = sliderWidth
    for i = 1, #self.m_ProgressNodes do
        self:InitPhase(self.m_ProgressNodes[i], self.m_ProgressNodeInfos[i], i == nodeNum)
    end
    self.ProgressBar.value = (curPhaseNode - 1 + subProgress) / (#self.m_ProgressNodes - 1)
    self.PhaseGrid:Reposition()
    local curNode = self.m_ProgressNodes[curPhaseNode]
    if curNode then
        CUICommonDef.SetFullyVisible(curNode.gameObject, self.Progress)
    end
end

function LuaScheduleQMPKPage:InitPhase(item, info, isLast)
    local nameLab   = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local timeLab   = item.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
    local stateIcon = item.transform:Find("StateIcon"):GetComponent(typeof(UITexture))
    local cupIcon   = item.transform:Find("CupIcon").gameObject

    nameLab.text = info.NodeTitle
    timeLab.text = info.NodeTime
    local labColor = nil
    local iconWidth = nil
    local iconColor = nil
    if info.InPorgress then
        -- 进行中
        labColor = self.m_YellowColor
        iconWidth = self.m_SpriteHigh
        iconColor = self.m_SpriteColor
    elseif info.Passed then
        -- 已完成
        labColor = self.m_GrayColor
        iconWidth = self.m_SpriteLow
        iconColor = self.m_SpriteColor
    else
        -- 未开始
        labColor = self.m_WhiteColor
        iconWidth = self.m_SpriteLow
        iconColor = self.m_GrayColor
    end

    nameLab.color = labColor
    timeLab.color = labColor
    stateIcon.color = iconColor
    stateIcon.width = iconWidth
    stateIcon.height = iconWidth
    cupIcon:SetActive(isLast)
end