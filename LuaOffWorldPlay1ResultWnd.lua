local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CUICommonDef = import "L10.UI.CUICommonDef"
local ShareMgr = import "ShareMgr"

LuaOffWorldPlay1ResultWnd = class()

RegistChildComponent(LuaOffWorldPlay1ResultWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaOffWorldPlay1ResultWnd, "score", "score", UILabel)
RegistChildComponent(LuaOffWorldPlay1ResultWnd, "desc1", "desc1", GameObject)
RegistChildComponent(LuaOffWorldPlay1ResultWnd, "desc2", "desc2", GameObject)
RegistChildComponent(LuaOffWorldPlay1ResultWnd, "desc3", "desc3", GameObject)
RegistChildComponent(LuaOffWorldPlay1ResultWnd, "fullTip", "fullTip", GameObject)
RegistChildComponent(LuaOffWorldPlay1ResultWnd, "node1", "node1", GameObject)
RegistChildComponent(LuaOffWorldPlay1ResultWnd, "node2", "node2", GameObject)
RegistChildComponent(LuaOffWorldPlay1ResultWnd, "node3", "node3", GameObject)
RegistChildComponent(LuaOffWorldPlay1ResultWnd, "shareBtn", "shareBtn", GameObject)
RegistChildComponent(LuaOffWorldPlay1ResultWnd, "Victory", "Victory", GameObject)

RegistClassMember(LuaOffWorldPlay1ResultWnd, "singleScoreTable")

--LuaOffWorldPassMgr.GameResult = {playInfo[0], playInfo[1], playInfo[2], playInfo[3], playerInfo[0], playerInfo[1], playerInfo[2]}
function LuaOffWorldPlay1ResultWnd:Awake()

end

function LuaOffWorldPlay1ResultWnd:InitSingleScoreData()
  self.singleScoreTable = {}
  OffWorldPass_JiaoShanReward.Foreach(function(k,v)
    if not self.singleScoreTable[v.stage] then
      self.singleScoreTable[v.stage] = {}
    end
    local data = {}
    data.hpMax = v.hpMax
    data.hpMin = v.hpMin
    data.grade = v.grade
    data.result = v.result
    data.credit = v.credit
    self.singleScoreTable[v.stage][v.grade] = data
  end)
end

function LuaOffWorldPlay1ResultWnd:InitSingleNode()
  local IconTable = {"UI/Texture/Transparent/Material/wangshujingshi_1.mat","UI/Texture/Transparent/Material/wangshujingshi_2.mat",
  "UI/Texture/Transparent/Material/wangshujingshi_3.mat"}

  local nodeTable = {self.node1,self.node2,self.node3}
  for i,v in ipairs(nodeTable) do
    local node = v
    local guanka = OffWorldPass_JiaoShan.GetData(i)
    local resultHp = LuaOffWorldPassMgr.GameResult[i]
    local percent = resultHp / 100
    node.transform:Find('grid/HPItem'):GetComponent(typeof(UISprite)).width = 591 * percent
    node.transform:Find('desc1'):GetComponent(typeof(UILabel)).text = guanka.name
    node.transform:Find('precent'):GetComponent(typeof(UILabel)).text = resultHp..'%'

    local gradeData = self.singleScoreTable[i]
    local gradeColorTable = {'64A6D5','D55DE2','FFE300'}
    local grade = 1
    local gradeString = nil
    local gradeColor = nil
    local count = 0
    for j,k in ipairs(gradeData) do
      if resultHp >= k.hpMin  and resultHp <= k.hpMax then
        if k.grade >= grade then
          grade = k.grade
          gradeString = k.result
          gradeColor = gradeColorTable[j]
          count = k.credit
        end
      end
    end
    node.transform:Find('count'):GetComponent(typeof(UILabel)).text = count
    node.transform:Find('icon'):GetComponent(typeof(CUITexture)):LoadMaterial(IconTable[grade])
    node.transform:Find('libao/text'):GetComponent(typeof(UILabel)).text = gradeString
    node.transform:Find('libao'):GetComponent(typeof(UISprite)).color = NGUIText.ParseColor(gradeColor, 0)
  end
  
end

function LuaOffWorldPlay1ResultWnd:InitScoreInfo()
  local realGet = LuaOffWorldPassMgr.GameResult[6]
  local theoryGet = LuaOffWorldPassMgr.GameResult[4]
  if realGet < theoryGet then
    self.desc1:SetActive(true)
    self.desc1.transform:Find('Label'):GetComponent(typeof(UILabel)).text = theoryGet
    self.desc2.transform:Find('Bg').gameObject:SetActive(false)
    self.fullTip:SetActive(true)
  else
    self.desc1:SetActive(false)
    self.desc2.transform:Find('Bg').gameObject:SetActive(true)
    self.fullTip:SetActive(false)
  end

  self.score.text = realGet
  self.desc2.transform:Find('Label'):GetComponent(typeof(UILabel)).text = tostring(tonumber(LuaOffWorldPassMgr.GameResult[5]) + realGet)
  self.desc3.transform:Find('Label'):GetComponent(typeof(UILabel)).text = OffWorldPass_Setting.GetData().MaxScorePerWeekJiaoShan

  local level = 3
  local totalScoreString = OffWorldPass_Setting.GetData().JiaoShanFinalScore
  local totalScoreTable = g_LuaUtil:StrSplit(totalScoreString,";")
  for i,v in ipairs(totalScoreTable) do
    local t = g_LuaUtil:StrSplit(v,",")
    local bonusLevel = tonumber(t[1])
    if theoryGet >= tonumber(t[2]) then
      if bonusLevel < level then
        level = bonusLevel
      end
    end
  end

  local materialTable = {"UI/Texture/Transparent/Material/offworldplay1resultwnd_wenzi_01.mat","UI/Texture/Transparent/Material/offworldplay1resultwnd_wenzi_02.mat",
  "UI/Texture/Transparent/Material/offworldplay1resultwnd_wenzi_03.mat"}
  self.Victory:GetComponent(typeof(CUITexture)):LoadMaterial(materialTable[level])
end

function LuaOffWorldPlay1ResultWnd:Init()
  local onCloseClick = function(go)
    CUIManager.CloseUI(CLuaUIResources.OffWorldPlay1ResultWnd)
  end
  CommonDefs.AddOnClickListener(self.CloseBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

  local onShareClick = function(go)
    CUICommonDef.CaptureScreen("screenshot", true, true, self.shareBtn, DelegateFactory.Action_string_bytes(function(path,bytes)
      ShareMgr.ShareLocalImage2PersonalSpace(path, nil)
    end))
  end
  CommonDefs.AddOnClickListener(self.shareBtn,DelegateFactory.Action_GameObject(onShareClick),false)

  self:InitScoreInfo()
  self:InitSingleScoreData()
  self:InitSingleNode()
end

--@region UIEvent

--@endregion
