require("common/common_include")
local LuaGameObject=import "LuaGameObject"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local Profession=import "L10.Game.Profession"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CPlayerInfoMgr=import "CPlayerInfoMgr"
local EnumPlayerInfoContext=import "L10.Game.EnumPlayerInfoContext"
local EChatPanel=import "L10.Game.EChatPanel"
local AlignType=import "CPlayerInfoMgr+AlignType"

CLuaGuanNingFriendRankView=class()
RegistClassMember(CLuaGuanNingFriendRankView,"m_PlayerItems")
RegistClassMember(CLuaGuanNingFriendRankView,"m_TableViewDataSource")
RegistClassMember(CLuaGuanNingFriendRankView,"m_TableView")
RegistClassMember(CLuaGuanNingFriendRankView,"m_PlayerInfos")
RegistClassMember(CLuaGuanNingFriendRankView,"m_RankInfos")
RegistClassMember(CLuaGuanNingFriendRankView,"m_SortFlags")
RegistClassMember(CLuaGuanNingFriendRankView,"m_SortRadioBox")

function CLuaGuanNingFriendRankView:Init(tf)
    self.m_SortFlags={ -1,-1,-1,-1,-1 }
    self.m_SortRadioBox=LuaGameObject.GetChildNoGC(tf,"Header").qnRadioBox
    self.m_SortRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(p1,p2)
        local index=p2+1
        self.m_SortFlags[index] = -self.m_SortFlags[index]
        for i,v in ipairs(self.m_SortFlags) do
            if i~=index then
                self.m_SortFlags[i]=-1
            end
        end
        p1:SetSortTipStatus(self.m_SortFlags[index]<0)
        self:Sort(index)
        self.m_TableView:ReloadData(true,false)
    end)

    self.m_PlayerItems={}
    self.m_PlayerItems[1]=LuaGameObject.GetChildNoGC(tf,"PlayerItem1").transform
    self.m_PlayerItems[2]=LuaGameObject.GetChildNoGC(tf,"PlayerItem2").transform
    self.m_PlayerItems[3]=LuaGameObject.GetChildNoGC(tf,"PlayerItem3").transform
    self.m_PlayerItems[4]=LuaGameObject.GetChildNoGC(tf,"PlayerItem4").transform
    --初始化
    for i,v in ipairs(self.m_PlayerItems) do
        self:InitPlayerItem(v,nil,i)
    end

    self.m_TableView=LuaGameObject.GetLuaGameObjectNoGC(tf).tableView


    self.m_PlayerInfos={}
    self.m_RankInfos={}

end
function CLuaGuanNingFriendRankView:OnEnable()
    self.m_SortRadioBox:ChangeTo(-1)
    self.m_SortFlags={ -1,-1,-1,-1,-1 }--初始化
end
function CLuaGuanNingFriendRankView:Sort(index)
    local myId=0
    if CClientMainPlayer.Inst then
        myId=CClientMainPlayer.Inst.Id
    end
    local function compare_rank(a,b)
        local aid=a.playerId
        local bid=b.playerId
        if aid==myId then
            return true
        elseif bid==myId then
            return false
        else
            if self.m_RankInfos[aid]<self.m_RankInfos[bid] then
                return self.m_SortFlags[index]>0
            elseif self.m_RankInfos[aid]>self.m_RankInfos[bid] then
                return self.m_SortFlags[index]<0
            else
                return aid<bid
            end
        end
    end
    local function compare_class(a,b)
        local aid=a.playerId
        local bid=b.playerId
        if aid==myId then
            return true
        elseif bid==myId then
            return false
        else
            local aclazz=a.clazz
            local bclazz=b.clazz
            if aclazz<bclazz then
                return self.m_SortFlags[index]>0
            elseif aclazz>bclazz then
                return self.m_SortFlags[index]<0
            else
                return aid<bid
            end
        end
    end
    local function compare_zongfen(a,b)
        local aid=a.playerId
        local bid=b.playerId
        if aid==myId then
            return true
        elseif bid==myId then
            return false
        else
            local ascore=a.totalScore
            local bscore=b.totalScore
            if ascore<bscore then
                return self.m_SortFlags[index]<0
            elseif ascore>bscore then
                return self.m_SortFlags[index]>0
            else
                return aid<bid
            end
        end
    end
    local function compare_junfen(a,b)
        local aid=a.playerId
        local bid=b.playerId
        if aid==myId then
            return true
        elseif bid==myId then
            return false
        else
            local ajunfen=0
            if a.joinNum>0 then ajunfen = a.totalScore/a.joinNum end
            local bjunfen=0
            if b.joinNum>0 then bjunfen = b.totalScore/b.joinNum end
            if ajunfen<bjunfen then
                return self.m_SortFlags[index]<0
            elseif ajunfen>bjunfen then
                return self.m_SortFlags[index]>0
            else
                return aid<bid
            end
        end
    end
    local function compare_win(a,b)
        local aid=a.playerId
        local bid=b.playerId
        if aid==myId then
            return true
        elseif bid==myId then
            return false
        else
            local awin=a.victoryNum
            local bwin=b.victoryNum
            if awin<bwin then
                return self.m_SortFlags[index]<0
            elseif awin>bwin then
                return self.m_SortFlags[index]>0
            else
                return aid<bid
            end
        end
    end
    if index==1 then
        table.sort(self.m_PlayerInfos, compare_rank)
    elseif index==2 then
        table.sort(self.m_PlayerInfos, compare_class)
    elseif index==3 then
        table.sort(self.m_PlayerInfos, compare_zongfen)
    elseif index==4 then
        table.sort(self.m_PlayerInfos, compare_junfen)
    elseif index==5 then
        table.sort(self.m_PlayerInfos, compare_win)
    end
end

function  CLuaGuanNingFriendRankView:InitItem(item,index)
    local default = index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite
    item:SetBackgroundTexture(default)

    local tf=item.transform
    local nameLabel=LuaGameObject.GetChildNoGC(tf,"NameLabel").label
    local classSprite=LuaGameObject.GetChildNoGC(tf,"ClassSprite").sprite
    local zongfen=LuaGameObject.GetChildNoGC(tf,"Zongfen").label
    local changjunfen=LuaGameObject.GetChildNoGC(tf,"Changjunfen").label
    local shengchang=LuaGameObject.GetChildNoGC(tf,"Shengchang").label
    local paiming=LuaGameObject.GetChildNoGC(tf,"Paiming").label
    local data=self.m_PlayerInfos[index+1]
    if data then
        nameLabel.text=data.name
        zongfen.text=tostring(data.totalScore)
        if data.joinNum==0 then
            changjunfen.text="0"
        else
            changjunfen.text=tostring(math.floor(data.totalScore/data.joinNum))
        end
        shengchang.text=tostring(data.victoryNum)
        paiming.text=tostring(self.m_RankInfos[data.playerId])
        classSprite.spriteName=Profession.GetIconByNumber(data.clazz)
    else
        nameLabel.text=""
        zongfen.text=""
        changjunfen.text=""
        shengchang.text=""
        paiming.text=""
        classSprite.spriteName=""
    end

    local myId=0
    if CClientMainPlayer.Inst then
        myId=CClientMainPlayer.Inst.Id
    end
    if myId==data.playerId then
        item:SetBackgroundTexture("common_bg_mission_background_highlight")
        LuaUtils.SetUIWidgetColor(nameLabel,0,255,0,255)
        LuaUtils.SetUIWidgetColor(zongfen,0,255,0,255)
        LuaUtils.SetUIWidgetColor(changjunfen,0,255,0,255)
        LuaUtils.SetUIWidgetColor(shengchang,0,255,0,255)
        LuaUtils.SetUIWidgetColor(paiming,0,255,0,255)
    else
        LuaUtils.SetUIWidgetColor(nameLabel,255,255,255,255)
        LuaUtils.SetUIWidgetColor(zongfen,255,255,255,255)
        LuaUtils.SetUIWidgetColor(changjunfen,255,255,255,255)
        LuaUtils.SetUIWidgetColor(shengchang,255,255,255,255)
        LuaUtils.SetUIWidgetColor(paiming,255,255,255,255)
    end
end

function CLuaGuanNingFriendRankView:InitPlayerItem(tf,data,index)
    local btn=LuaGameObject.GetChildNoGC(tf,"Portrait").gameObject
    local portrait=LuaGameObject.GetChildNoGC(tf,"Portrait").cTexture
    local nameLabel=LuaGameObject.GetChildNoGC(tf,"NameLabel").label
    local scoreLabel=LuaGameObject.GetChildNoGC(tf,"ScoreLabel").label
    local myId=0
    if CClientMainPlayer.Inst then
        myId=CClientMainPlayer.Inst.Id
    end
    if data and myId==data.playerId then
        LuaUtils.SetUIWidgetColor(nameLabel,0,255,0,255)
    else
        LuaUtils.SetUIWidgetColor(nameLabel,255,255,255,255)
    end    

    if data then
        portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(data.clazz,data.gender,-1),false)
        nameLabel.text=data.name
        if index==1 then
            scoreLabel.text=SafeStringFormat3(LocalString.GetString("本周累计击杀:%s"),data.totalKill)
        elseif index==2 then
            scoreLabel.text=SafeStringFormat3(LocalString.GetString("本周累计助攻:%s"),data.totalHelp)
        elseif index==3 then
            scoreLabel.text=SafeStringFormat3(LocalString.GetString("本周累计复活:%s"),data.totalRelive)
        elseif index==4 then
            scoreLabel.text=SafeStringFormat3(LocalString.GetString("本周最高连斩:%s"),data.maxLianZhan)
        end
        CommonDefs.AddOnClickListener(btn,DelegateFactory.Action_GameObject(function(go)
            if data then
                CPlayerInfoMgr.ShowPlayerPopupMenu(data.playerId, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,AlignType.Default)
            end
        end),false)
    else
        portrait:Clear()
        nameLabel.text=LocalString.GetString("暂无")
        LuaUtils.SetUIWidgetColor(nameLabel,150,150,150,255)
        if index==1 then
            scoreLabel.text=SafeStringFormat3(LocalString.GetString("本周累计击杀:%s"),"－")
        elseif index==2 then
            scoreLabel.text=SafeStringFormat3(LocalString.GetString("本周累计助攻:%s"),"－")
        elseif index==3 then
            scoreLabel.text=SafeStringFormat3(LocalString.GetString("本周累计复活:%s"),"－")
        elseif index==4 then
            scoreLabel.text=SafeStringFormat3(LocalString.GetString("本周最高连斩:%s"),"－")
        end
        CommonDefs.AddOnClickListener(btn,DelegateFactory.Action_GameObject(function(go) end),false)
    end
end


function CLuaGuanNingFriendRankView:GetJiShaBest()
    local ret=nil
    local maxJiSha=0
    if #self.m_PlayerInfos>0 then
        for i,v in ipairs(self.m_PlayerInfos) do
            if v.totalKill>0 and v.totalKill>maxJiSha then
                maxJiSha=v.totalKill
                ret=v
            end
        end
    end
    return ret    
end
function CLuaGuanNingFriendRankView:GetZhuGongBest()
    local ret=nil
    local maxZhuGong=0
    if #self.m_PlayerInfos>0 then
        for i,v in ipairs(self.m_PlayerInfos) do
            if v.totalHelp>0 and v.totalHelp>maxZhuGong then
                maxZhuGong=v.totalHelp
                ret=v
            end
        end
    end
    return ret  
end
function CLuaGuanNingFriendRankView:GetFuHuoBest()
    local ret=nil
    local maxFuHuo=0
    if #self.m_PlayerInfos>0 then
        for i,v in ipairs(self.m_PlayerInfos) do
            if v.totalRelive>0 and v.totalRelive>maxFuHuo then
                maxFuHuo=v.totalRelive
                ret=v
            end
        end
    end
    return ret
end
function CLuaGuanNingFriendRankView:GetLianZhanBest()
    local ret=nil
    local maxLianZhan=0
    if #self.m_PlayerInfos>0 then
        for i,v in ipairs(self.m_PlayerInfos) do
             if v.maxLianZhan>0 and v.maxLianZhan>maxLianZhan then
                maxLianZhan=v.maxLianZhan
                ret=v
            end
        end
    end
    return ret
end
function CLuaGuanNingFriendRankView:OnQueryGuanNingRankDataResult( rank )
    self.m_PlayerInfos=rank
    -- for i=1,#rank do
    --     table.insert( self.m_PlayerInfos,data[i-1] )
    -- end
    local myId=0
    if CClientMainPlayer.Inst then
        myId=CClientMainPlayer.Inst.Id
    end
    local function compare(a,b)
        if a.totalScore>b.totalScore then
            return true
        elseif a.totalScore<b.totalScore then
            return false
        else
            return a.playerId<b.playerId
        end
    end
    local function compare2(a,b)
        --自己排第一位
        if a.playerId==myId then
            return true
        elseif b.playerId==myId then
            return false
        else
            return compare(a,b)
        end
    end

    table.sort(self.m_PlayerInfos, compare)
    self.m_RankInfos={}
    for i,v in ipairs(self.m_PlayerInfos) do
        self.m_RankInfos[v.playerId]=i
    end


    --把自己放在第一位
    table.sort(self.m_PlayerInfos, compare2)

    self:InitPlayerItem(self.m_PlayerItems[1],self:GetJiShaBest(),1)
    self:InitPlayerItem(self.m_PlayerItems[2],self:GetZhuGongBest(),2)
    self:InitPlayerItem(self.m_PlayerItems[3],self:GetFuHuoBest(),3)
    self:InitPlayerItem(self.m_PlayerItems[4],self:GetLianZhanBest(),4)

    local function InitItem(item,index)

        self:InitItem(item,index)
    end
    self.m_TableViewDataSource=DefaultTableViewDataSource.CreateByCount(#self.m_PlayerInfos,InitItem)
    self.m_TableView.m_DataSource=self.m_TableViewDataSource
    local function OnSelect(index)
        self:OnSelectAtRow(index)
    end
    
    self.m_TableView.OnSelectAtRow=DelegateFactory.Action_int(OnSelect)
    self.m_TableView:ReloadData(true,false)
end
function CLuaGuanNingFriendRankView:OnSelectAtRow(index)
    local data=self.m_PlayerInfos[index+1]
    if data then
        CPlayerInfoMgr.ShowPlayerPopupMenu(data.playerId, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,AlignType.Default)
    end
end
return CLuaGuanNingFriendRankView
