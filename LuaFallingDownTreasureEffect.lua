require("common/common_include")

local Random = import "UnityEngine.Random"
local Object = import "UnityEngine.Object"
local UITexture = import "UITexture"
local Time = import "UnityEngine.Time"
local UICamera = import "UICamera"
local Physics = import "UnityEngine.Physics"
local CUIFx = import "L10.UI.CUIFx"

LuaFallingDownTreasureEffect = class()

RegistClassMember(LuaFallingDownTreasureEffect, "m_ToyTexture")
RegistClassMember(LuaFallingDownTreasureEffect, "m_ToyType")
RegistClassMember(LuaFallingDownTreasureEffect, "m_TargetTexture")
RegistClassMember(LuaFallingDownTreasureEffect, "m_HitFx")

RegistClassMember(LuaFallingDownTreasureEffect, "m_Rotation")
RegistClassMember(LuaFallingDownTreasureEffect, "m_Scale")
RegistClassMember(LuaFallingDownTreasureEffect, "m_DropSpeed")
RegistClassMember(LuaFallingDownTreasureEffect, "m_LargenSpeed")

RegistClassMember(LuaFallingDownTreasureEffect, "m_LifeTime")
RegistClassMember(LuaFallingDownTreasureEffect, "m_Fin")
RegistClassMember(LuaFallingDownTreasureEffect, "m_Score")

function LuaFallingDownTreasureEffect:Init(args)
	if args and args.Length > 0 then
		self.m_Score = args[0]
	else
		self.m_Score = 0
	end
end

function LuaFallingDownTreasureEffect:OnEnable()
	self.m_ToyTexture = self.transform:GetComponent(typeof(UITexture))
	self.m_TargetTexture = self.transform.parent.parent:Find("Player/PlayerTexture").gameObject
	self.m_HitFx = self.transform:Find("HitFx"):GetComponent(typeof(CUIFx))

	self.m_DropSpeed = Random.Range(500 - 100, 500 + 100)
	self.m_LargenSpeed = 0.1
	self.m_Rotation = Random.Range(-30, 30)
	self.m_Scale = Random.Range(0.4, 0.6)
	self.m_HitFx:DestroyFx()

	self.m_ToyTexture.transform.localScale = Vector3(self.m_Scale, self.m_Scale, self.m_Scale)
	self.m_ToyTexture.transform.localEulerAngles = Vector3(0, 0, self.m_Rotation)

	self.m_Fin = false
	self.m_LifeTime = 5

	Object.Destroy(self.gameObject, self.m_LifeTime)
end

function LuaFallingDownTreasureEffect:Update()
	if not self.m_Fin then
		local newPos = Vector3(self.transform.localPosition.x, self.transform.localPosition.y - self.m_DropSpeed * Time.deltaTime,  0)
		self.transform.localPosition = newPos

		if not LuaZhuJueJuQingMgr.m_FallingDownTreasureEnd then
			local screenPos = UICamera.currentCamera:WorldToScreenPoint(self.transform.position)
			local targetPos = UICamera.currentCamera:WorldToScreenPoint(self.m_TargetTexture.transform.position)
			local ray = UICamera.currentCamera:ScreenPointToRay(Vector3(screenPos.x, screenPos.y, 0))
      		local hits = Physics.RaycastAll(ray, 99999,  UICamera.currentCamera.cullingMask)
      		for i = 0, hits.Length - 1 do
        		if hits[i].collider.gameObject == self.m_TargetTexture then
          			if not self.m_Score then
          			else
          				if self.m_Score ~= 0 then
          					self.m_HitFx:LoadFx("Fx/UI/Prefab/UI_jieyuanbao.prefab")
          					LuaZhuJueJuQingMgr.AddScore(self.m_Score)
          				else
          					LuaZhuJueJuQingMgr.AddFrozenStatus()
          				end
          				self.m_Fin = true
          				Object.Destroy(self.gameObject, 0.5)
          			end
        		end
      		end
		end

		local newScale =self.transform.localScale.x + self.m_LargenSpeed * Time.deltaTime
		self.m_ToyTexture.transform.localScale = Vector3(newScale, newScale, newScale)
	end
end

return LuaFallingDownTreasureEffect
