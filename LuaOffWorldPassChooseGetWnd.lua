local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local Quaternion = import "UnityEngine.Quaternion"
local CUIManager = import "L10.UI.CUIManager"
local UIVerticalLabel = import "UIVerticalLabel"
local CFashionMgr = import "L10.Game.CFashionMgr"

LuaOffWorldPassChooseGetWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaOffWorldPassChooseGetWnd, "CloseBtn", "CloseBtn", GameObject)
RegistChildComponent(LuaOffWorldPassChooseGetWnd, "weapon", "weapon", GameObject)
RegistChildComponent(LuaOffWorldPassChooseGetWnd, "Label", "Label", GameObject)
RegistChildComponent(LuaOffWorldPassChooseGetWnd, "name", "name", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaOffWorldPassChooseGetWnd, "m_ModelTextureLoader")
RegistClassMember(LuaOffWorldPassChooseGetWnd, "m_ModelName")
RegistClassMember(LuaOffWorldPassChooseGetWnd, "weaponRo")
RegistClassMember(LuaOffWorldPassChooseGetWnd, "swShowArgs")

function LuaOffWorldPassChooseGetWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaOffWorldPassChooseGetWnd:Init()
  local onCloseClick = function(go)
    CUIManager.CloseUI(CLuaUIResources.OffWorldPassReward2Wnd)
  end
  local swData = LuaOffWorldPassMgr.GetSWWeaponData(LuaOffWorldPassMgr.ShowFashionId)
  local nameList = UIVerticalLabel.SplitText(swData[2],1)
  local nameTable = {}
  for i=1,nameList.Count do
    table.insert(nameTable ,nameList[i-1])
  end
  local name = table.concat( nameTable, "\n" )
  self.name:GetComponent(typeof(UILabel)).text = name

  self:InitSWModel({weaponType = swData[0], wuxing = 0, realwuxing = swData[1]})
end

--@region UIEvent

--@endregion UIEvent
function LuaOffWorldPassChooseGetWnd:InitSWModel(data)
  local equipmentId = nil
  local weaponTexture = nil
  OffWorldPass_Fashion.Foreach(function(i,v)
    if v.type == data.weaponType and v.wuxing == data.wuxing then
      equipmentId = v.equipmentId
      weaponTexture = v.weaponTexture
    end
  end)

  if not equipmentId or not weaponTexture then
    return
  end

  local equipData = EquipmentTemplate_Equip.GetData(equipmentId)

  local name = equipData.Name
  local colorTable = {
    LocalString.GetString('[c][fdab2c]金属性%s[-][/c]'),
    LocalString.GetString('[c][15b600]木属性%s[-][/c]'),
    LocalString.GetString('[c][0189b6]水属性%s[-][/c]'),
    LocalString.GetString('[c][d04500]火属性%s[-][/c]'),
    LocalString.GetString('[c][bf6700]土属性%s[-][/c]'),
  }
  self.Label:GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString('恭喜获得%s'),SafeStringFormat3(colorTable[data.realwuxing],name))

  local m_FakeModel = self.weapon:GetComponent(typeof(CUITexture))
  m_FakeModel:LoadMaterial(weaponTexture)

  -- self.m_ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
  --   self:LoadModel(ro, equipData.Prefab)
  -- end)
  -- self.m_ModelName = "_LuaOffWorldPassChooseGetWnd_"
  -- self.swShowArgs = LuaOffWorldPassMgr.GetWeaponShowArgs(data)
  -- --m_FakeModel.mainTexture = CUIManager.CreateModelTexture(self.m_ModelName, self.m_ModelTextureLoader,90,0,-0.3,3.04,false,true,1,true)
  -- m_FakeModel.mainTexture = CUIManager.CreateModelTexture(self.m_ModelName, self.m_ModelTextureLoader,self.swShowArgs[1],self.swShowArgs[2],self.swShowArgs[3],self.swShowArgs[4],false,true,1,true)
	-- local onDrag = function(go,delta)
	-- 	self.weaponRo.transform:Rotate(Vector3.right,(delta.x+delta.y+delta.z))
	-- end
	-- CommonDefs.AddOnDragListener(self.weapon,DelegateFactory.Action_GameObject_Vector2(onDrag),false)
end

function LuaOffWorldPassChooseGetWnd:LoadModel(ro, prefabname)
    ro:LoadMain(prefabname, nil, false, false)
		--ro.transform.localRotation = Quaternion.Euler(0, 90, -90)
		ro.transform.localRotation = Quaternion.Euler(self.swShowArgs[5],self.swShowArgs[6],self.swShowArgs[7])
    self.weaponRo = ro
end

function LuaOffWorldPassChooseGetWnd:OnDestroy()
	CUIManager.DestroyModelTexture(self.m_ModelName)

  msg = g_MessageMgr:FormatMessage('GET_NEW_SW_WEAPON_FASHION')
  MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
    LuaOffWorldPassMgr.FirstOpenClothesWnd = true
    LuaAppearancePreviewMgr:CloseAppearanceWnd()
    CFashionMgr.Inst:ShowAppearanceWnd(LuaOffWorldPassMgr.ShowFashionId)
  end), nil, nil, nil, false)
end
