--cs和lua可共同访问的数组
-- LuaCSharpArr = require("arraccess/LuaCSharpArr")
Utility = import "L10.Engine.Utility"
function BeginSample(name)
	if DEVELOPMENT_BUILD or UNITY_EDITOR then
		Utility.BeginSample(name)
	end
end
function EndSample()
	if DEVELOPMENT_BUILD or UNITY_EDITOR then
		Utility.EndSample()
	end
end

--UnityEngine
Application = import "UnityEngine.Application"
RuntimePlatform = import "UnityEngine.RuntimePlatform"
Vector3 = import "UnityEngine.Vector3"
Vector2 = import "UnityEngine.Vector2"
GameObject = import "UnityEngine.GameObject"
Transform = import "UnityEngine.Transform"
Input = import "UnityEngine.Input"
Time = import "UnityEngine.Time"
Mathf = import "UnityEngine.Mathf"
Color = import "UnityEngine.Color"

SoundManager=import "SoundManager"
L10=import "L10"
System = import "System"

--NGUI
UIEventListener = import "UIEventListener"
UICamera = import "UICamera"
UIInput=import "UIInput"
UISlider = import "UISlider"
UIToggle=import "UIToggle"

UnityEngine = import "UnityEngine"
NGUIMath = import "NGUIMath"
NGUIText = import "NGUIText"

--Game Code
Utility = import "L10.Engine.Utility"
CUIManager = import "L10.UI.CUIManager"
CUIResources = import "L10.UI.CUIResources"
CIndirectUIResources = import "L10.UI.CIndirectUIResources"
CUICommonDef = import "L10.UI.CUICommonDef"
DelegateFactory = import "DelegateFactory"
CLogMgr = import "L10.CLogMgr"
CommonDefs = import "L10.Game.CommonDefs"
Constants = import "L10.Game.Constants"
LocalString=import "LocalString"
NGUITools=import "NGUITools"
LuaTweenUtils=import "LuaTweenUtils"
LuaUtils=import "LuaUtils"
MsgPackImpl = import "MsgPackImpl"

UILabel=import "UILabel"
UISprite=import "UISprite"
UIWidget=import "UIWidget"
UIGrid=import "UIGrid"
UITable=import "UITable"
UITexture=import "UITexture"
UIScrollView=import "UIScrollView"
UIBasicSprite=import "UIBasicSprite"

UIScrollViewIndicator=import "UIScrollViewIndicator"
CClientMainPlayer=import "L10.Game.CClientMainPlayer"

CUITexture=import "L10.UI.CUITexture"
UIPanel=import "UIPanel"
NGUIText=import "NGUIText"

CCommonLuaWnd=import "L10.UI.CCommonLuaWnd"
CCommonLuaScript=import "L10.UI.CCommonLuaScript"
DefaultItemActionDataSource = import "L10.UI.DefaultItemActionDataSource"
DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
QnTableView=import "L10.UI.QnTableView"
CUIFx = import "L10.UI.CUIFx"
QnRadioBox=import "L10.UI.QnRadioBox"
QnTabView=import "L10.UI.QnTabView"
MessageWndManager = import "L10.UI.MessageWndManager"
QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
QnButton = import "L10.UI.QnButton"
Debug = import "UnityEngine.Debug"

LuaTransformAccess = import "LuaInterface.LuaTransformAccess"
LuaRigidbody2DAccess = import "LuaInterface.LuaRigidbody2DAccess"
LuaMaterialAccess = import "LuaInterface.LuaMaterialAccess"

EnumTowerDefenseGameStaus = {
	ePreparing = 0,
	eRunning = 1,
	eEnd = 2,
	eEndShowWnd = 3,
}
CLuaUIFxPaths={
	LingShouZhunHunZhengFx = "Fx/UI/Prefab/UI_lingshouzhunhunzhengxiaoshi.prefab",
	DouDiZhuFeiJi = "fx/ui/prefab/doudizhu_feiji.prefab",
	DouDiZhuZhaDan = "fx/ui/prefab/UI_doudizhu_zhadan.prefab",
	DouDiZhuHuoJian = "fx/ui/prefab/UI_doudizhu_huojian.prefab",

	DouDiZhuJinBi="fx/ui/prefab/UI_jinbi.prefab",

	MeiWeiShaBingFx = "Fx/UI/Prefab/UI_caibingsha_meiwei.prefab",
	HeiAnShaBingFx = "Fx/UI/Prefab/UI_caibingsha_heian.prefab",

	QTESingleClickFx = "Fx/UI/Prefab/UI_qte_click_01.prefab",
	QTEConsecutiveClickFx = "Fx/UI/Prefab/UI_qte_click_02.prefab",
	QTESlideFx = "Fx/UI/Prefab/UI_qte_JianTou_01.prefab",
	QTESuccessFx = "Fx/UI/Prefab/UI_EQT_chenggong.prefab",
	QTEFailFx = "Fx/UI/Prefab/UI_EQT_shibai.prefab",
}

CLuaUISoundEvents={
	DouDiZhuFaPai="event:/L10/DouDiZhu/FaPai",
	DouDiZhuDiZhuMao="event:/L10/DouDiZhu/DiZhuMao",
	DouDiZhuFeiJi="event:/L10/DouDiZhu/FeiJi",
	DouDiZhuZhaDan="event:/L10/DouDiZhu/ZhaDan",
	DouDiZhuHuoJian="event:/L10/DouDiZhu/HuoJian",
	AnQiDaoFire="event:/L10/SFX/SFX_AQD_CannonFire",
	AnQiDaoHit="event:/L10/SFX/SFX_AQD_CannonHit",
}

LuaEnumItemPlace = {
	Bag = 1,
	Body = 2,
	Task = 3,
	LingShou = 4,
	Recycle = 5,
	ZuoQi = 6,
}
LuaEnumLingShouQuality={
	Ji = 1,
	Wu = 2,
	Ding = 3,
	Bing = 4,
	Yi = 5,
	Jia = 6,
}
LuaEnumLingShouType = {
	PAttack = 1,
	MAttack = 2,
	PCorDefend = 3,
	MCorDefend = 4,
	PAType = 5,
	MAType = 6,
}
LuaEnumLingShouNature = {
	None = 0,
	Brave = 1,
	Smart = 2,
	Obstinate = 3,
	Careful = 4,
	Loyalty = 5
}
--窗口状态
ItemListWndType={
	LingShouMarriage = 1,
	Common=2
}
EnumBabyWashRequestState = {
    eNone = 0,
    eNormal = 1,
    eWaitResult=2,
    eResult = 3,
    eWaitDelta=4,
}

EnumSecretShopType = {
	eUnknown = 0,
	eMapiShop = 1,
	eTianQiShop = 2,
}
EnumGamePlayType={
	eNone=0,
	eDuanWuDaZuoZhan=1,--端午大作战
}

EnumBabyStatus = {
	eBorn = 1,		-- 婴儿阶段
	eChild = 2,		-- 幼儿阶段
	eYoung = 3,		-- 少年阶段
}

EnumMPTYSJump = {
	eLow = 1,
	ePerfect = 2,
	eHigh = 3,
	eNormal = 4,
}


luaJson = require("3rdParty/LuaJson")
require "callback"
require "chatlinkactioncallback"
require "clientactioncallback"
require "upgradenotifycallback"

require "event"



LuaEnumPlayScoreKey = {
	NONE = 0,
	MPTZ = 1,
	GNJC = 2,
	GNJC_TOTAL = 3,
	BiWuStage1 = 4,
	BiWuStage2 = 5,
	BiWuStage3 = 6,
	BiWuStage4 = 7,
	BiWuStage5 = 8,
	QYJF = 9,
	DuoBao = 10,
	BangGong = 11,
	--12已经被GIFT，客户端用掉了，不能用
	InviteHuiLiu = 13,
	JiaYuanTuZhi = 14,
	JiaYuanTuZhiSpecialScore = 15,
	YuanXiao = 16,
	PGQY = 17,
	QingQiu = 18,
	NationalPve = 19,
	-- 20已经被YuMa，客户端用掉了，不能用
	Halloween = 21,
	DouHunTrainSubmit = 22,
	JiaYuanJiFen = 23,
	DuoMaoMao = 24,
	DuoMaoMaoTotal = 25,
	QianKunDai = 26,
	QMQD = 27,
	HanJia = 28,
	Valentine = 29,
	YuanXiaoTotal = 30,
	NSDZZ = 31,
	XZS = 32,
	CuJu = 33,
	QmpkJingCai2019 = 34,
	WuCaiShaBing = 35,
	SHTY = 36,
	TangYuan = 37,
	QingMing2019 = 38,
	LabourDay2019 = 39,
	LiuYi2019 = 40,
	SanXingGuZhanChang = 44,
	ButterflyWorldEventScore = 50,
	YueLi = 51,
	HouseCompetition = 52,
	SpokesmanHaiTangHua = 54,
	HongJingPing = 55,
	LanJingPing = 56,
	NewHuiLiuHuiGui = 57,
	TangYuan2021 = 58,
	HouseFishing = 59,
	FishingScore = 60,
	SeaFishingGameplayScore = 61,
	JXYSScore = 62,--降雪元霜积分
	PengDaoFuYao = 63,
	ClubHouseSYLX = 64, -- 室雅兰香积分
	AppearanceScore = 65, -- 外观积分
	StarBiWuScore = 66, -- 跨服明星赛积分
	-- 注意!!!不能超过80

	-- 后面的id没有直接存在PlayScore数组里面，但是为了方便记运营日志。也加上key
	MAPIJIFEN = 1001,
	TIANQIJIFEN = 1002,
}
LuaEnumHoleStatus = {
    availableWithGem = 1,
    availableWithoutGem = 2,
    disable = 3,
}

--身上装备的位置，对应equipment 的类型
-- 11 12 是右边的戒指和项链
LuaEnumBodyPosition = {
	Weapon = 1,
	Shield = 2,
	Headwear = 3,
	Clothes = 4,
	Belt = 5,
	Gloves = 6,
	Shoes = 7,
	LeftRing = 8,
	LeftBracelet = 9,
	Necklace = 10,
	RightRing = 11,
	RightBracelet = 12,
	BackPendant = 13, -- 背饰
	HiddenWeapon = 14, -- 暗器
	Beaded = 15, -- 串珠

	TalismanQian = 17,--乾
	TalismanXun = 18, --巽
	TalismanKan = 19, --坎
	TalismanGen = 20, --艮
	TalismanKun = 21, --坤
	TalismanZhen = 22,--震
	TalismanLi = 23,  -- 离
	TalismanDui = 24, --兑

	--第二套法宝
	TalismanQian2 = 25,--乾2
	TalismanXun2 = 26, --巽2
	TalismanKan2 = 27, --坎2
	TalismanGen2 = 28, --艮2
	TalismanKun2 = 29, --坤2
	TalismanZhen2 = 30,--震2
	TalismanLi2 = 31,  -- 离2
	TalismanDui2 = 32, --兑2

	--第三套法宝
	TalismanQian3 = 33,--乾3
	TalismanXun3 = 34, --巽3
	TalismanKan3 = 35, --坎3
	TalismanGen3 = 36, --艮3
	TalismanKun3 = 37, --坤3
	TalismanZhen3 = 38,--震3
	TalismanLi3 = 39,  -- 离3
	TalismanDui3 = 40, --兑3
}


LuaEnumAttrGroupItem = {
	Any = 0,
	Name = 1,
	SkillTemplate = 2,
	TalismanSet = 3,
	BodyEquipWordSetIdx = 4,
	BodyEquipHoleSetIdx = 5,
	LingShou = 6,
	AssistLingShou = 7,
	BindedPartnerLingShou = 8,
}

EnumPropertyRelationshipSettingBit = {
    NotAllowEmbrace = 1,
    NotAllowQiuHun = 2,
    NotAllowAutoHoldUmbrella = 3,
    NotAllowMoTouSha = 4,
    NotAllowJuGaoGao = 5,
    NotAllowKiss = 6,
  	RefuseAddCrossFriend = 7,
	RefuseCrossStrangerMessage = 8,
	RefuseGroupPhoto = 9,
	NotAllowFangFengZheng = 10,
	NotAllowShuangRenHua = 11,
	NotAllowXiangYiXiangWei = 12,
}

EnumLiangHaoPrivilegeType = {
	HideIcon = 1,
	RefuseAddCrossFriend = 2,
	RefuseCrossStrangerMessage = 3,
	ShowDigitIcon = 4,
}

EnumSyncLiangHaoInfoReason = {
	eGet = 1,
	eRenew = 2,
	eGiveUp = 3,
	eDelete = 4,
}

LuaEnumSyncNpcHaoGanDuInfoReason = {
	eReceiveLetter = 1,
	eReadLetter = 2,
	eSendLetter = 3,
	eBuyZengSongNum = 4,
	eZengSongGift = 5,
	eAddHaoGanDuByAction = 6,
	eNewLetterCanSend = 7,
}

LuaEnumFurnitureSubType = {
	eUnknown = 0,
  eChuangta = 1,
  eChugui = 2,
  eZhuoan = 3,
  eZuoju = 4,
  ePingfeng = 5,
  eZawu = 6,
  eGuashi = 7,
  eQinggong = 8,
  eChuanjiabao = 9,
  eXiaojing = 10,
  eHuamu = 11,
  eZhengwu = 12,
  eCangku = 13,
  eXiangfang = 14,
  eMiaopu = 15,
  eNpc = 16,
  eWeiqiang = 17,
  eLingshou = 18,
  ePuppet = 19,
  eMapi = 20,
}
LuaEnumNpcHaoGanDuLetterType = {
	eNpcSend2Player = 1,
	ePlayerReplyNpc = 2,
	ePlayerSend2Npc = 3,
	eNpcReplyPlayer = 4,
}

--[[
 * Converts an RGB color value to HSV. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSV_color_space.
 * Assumes r, g, and b are contained in the set [0, 255] and
 * returns h, s, and v in the set [0, 1].
 *
 * @param   Number  r       The red color value
 * @param   Number  g       The green color value
 * @param   Number  b       The blue color value
 * @return  Array           The HSV representation
]]
function rgb2hsv(r, g, b)
	--r, g, b = r / 255, g / 255, b / 255
	local max, min = math.max(r, g, b), math.min(r, g, b)
	local h, s, v
	v = max

	local d = max - min
	if max == 0 then s = 0 else s = d / max end

	if max == min then
	  h = 0 -- achromatic
	else
	  if max == r then
		h = (g - b) / d
		if g < b then h = h + 6 end
	  elseif max == g then h = (b - r) / d + 2
	  elseif max == b then h = (r - g) / d + 4
	  end
	  h = h / 6
	end

	return h, s, v
  end

  --[[
   * Converts an HSV color value to RGB. Conversion formula
   * adapted from http://en.wikipedia.org/wiki/HSV_color_space.
   * Assumes h, s, and v are contained in the set [0, 1] and
   * returns r, g, and b in the set [0, 255].
   *
   * @param   Number  h       The hue
   * @param   Number  s       The saturation
   * @param   Number  v       The value
   * @return  Array           The RGB representation
  ]]
  function hsv2rgb(h, s, v)
	local r, g, b

	local i = math.floor(h * 6);
	local f = h * 6 - i;
	local p = v * (1 - s);
	local q = v * (1 - f * s);
	local t = v * (1 - (1 - f) * s);

	i = i % 6

	if i == 0 then r, g, b = v, t, p
	elseif i == 1 then r, g, b = q, v, p
	elseif i == 2 then r, g, b = p, v, t
	elseif i == 3 then r, g, b = p, q, v
	elseif i == 4 then r, g, b = t, p, v
	elseif i == 5 then r, g, b = v, p, q
	end

	return r, g, b
	--return r * 255, g * 255, b * 255
  end
