local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local CPlayerInfoMgr=import "CPlayerInfoMgr"
local EnumPlayerInfoContext=import "L10.Game.EnumPlayerInfoContext"
local EChatPanel=import "L10.Game.EChatPanel"
local AlignType=import "CPlayerInfoMgr+AlignType"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local QnTableView=import "L10.UI.QnTableView"


CLuaLianLianKanRankWnd=class()
RegistClassMember(CLuaLianLianKanRankWnd,"m_MyRankLabel")
RegistClassMember(CLuaLianLianKanRankWnd,"m_MyNameLabel")
RegistClassMember(CLuaLianLianKanRankWnd,"m_MyTimeLabel")
RegistClassMember(CLuaLianLianKanRankWnd,"m_MyScoreLabel")

RegistClassMember(CLuaLianLianKanRankWnd,"m_TableView")
RegistClassMember(CLuaLianLianKanRankWnd,"m_RankList")


function CLuaLianLianKanRankWnd:Init()
    self.m_MyRankLabel=FindChild(self.transform,"MyRankLabel"):GetComponent(typeof(UILabel))
    self.m_MyNameLabel=FindChild(self.transform,"MyNameLabel"):GetComponent(typeof(UILabel))
    self.m_MyTimeLabel=FindChild(self.transform,"MyTimeLabel"):GetComponent(typeof(UILabel))
    self.m_MyScoreLabel=FindChild(self.transform,"MyScoreLabel"):GetComponent(typeof(UILabel))

    self.m_RankList={}
    if CClientMainPlayer.Inst then
        self.m_MyNameLabel.text= CClientMainPlayer.Inst.Name
    end
    self.m_MyRankLabel.text=LocalString.GetString("未上榜")
    self.m_MyTimeLabel.text="—"
    self.m_MyScoreLabel.text="—"

    self.m_TableView=FindChild(self.transform,"TableView"):GetComponent(typeof(QnTableView))
    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_RankList
        end,
        function(item,index)
            if index % 2 == 1 then
                item:SetBackgroundTexture("common_textbg_02_dark")
            else
                item:SetBackgroundTexture("common_textbg_02_light")
            end
            self:InitItem(item,index,self.m_RankList[index+1])
        end)

    self.m_TableView.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)

    Gac2Gas.QueryLianLianKan2018Rank()
end

function CLuaLianLianKanRankWnd:OnSelectAtRow(row)
    local data=self.m_RankList[row+1]
    if data then
        CPlayerInfoMgr.ShowPlayerPopupMenu(data.id, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,AlignType.Default)
    end
end
function CLuaLianLianKanRankWnd:InitItem(item,index,info)
    local tf=item.transform
    local timeLabel=FindChild(tf,"TimeLabel"):GetComponent(typeof(UILabel))
    timeLabel.text=""
    local nameLabel=FindChild(tf,"NameLabel"):GetComponent(typeof(UILabel))
    nameLabel.text=" "
    local rankLabel=FindChild(tf,"RankLabel"):GetComponent(typeof(UILabel))
    rankLabel.text=""
    local rankSprite=FindChild(tf,"RankImage"):GetComponent(typeof(UISprite))
    rankSprite.spriteName=""
    local scoreLabel=FindChild(tf,"ScoreLabel"):GetComponent(typeof(UILabel))
    scoreLabel.text=tostring(info.score)
    
    -- local info=self.m_RankList[index]

    local rank=index+1
    if rank==1 then
        rankSprite.spriteName="Rank_No.1"
    elseif rank==2 then
        rankSprite.spriteName="Rank_No.2png"
    elseif rank==3 then
        rankSprite.spriteName="Rank_No.3png"
    else
        rankLabel.text=tostring(rank)
    end

    nameLabel.text=info.name

    timeLabel.text=self:FormatTime(info.time)
end
function CLuaLianLianKanRankWnd:FormatTime(time)
    local minute=math.floor(time/60)
    local second=time%60
    return SafeStringFormat3(LocalString.GetString("%02d分%02d秒"),minute,second)
end
function CLuaLianLianKanRankWnd:OnEnable()
    g_ScriptEvent:AddListener("QueryLianLianKan2018RankReturn", self, "OnQueryLianLianKan2018RankReturn")
end
function CLuaLianLianKanRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("QueryLianLianKan2018RankReturn", self, "OnQueryLianLianKan2018RankReturn")
end

function CLuaLianLianKanRankWnd:OnQueryLianLianKan2018RankReturn(data,inRank, selfScore, selfCostTime)
    if selfScore<0 then inRank=false end
    if inRank then
        self.m_MyRankLabel.text=nil
        self.m_MyTimeLabel.text=self:FormatTime(selfCostTime)
        self.m_MyScoreLabel.text=tostring(selfScore)
    else
        self.m_MyRankLabel.text=LocalString.GetString("未上榜")
        self.m_MyTimeLabel.text="—"
        self.m_MyScoreLabel.text="—"
    end
    local myId=0
    if CClientMainPlayer.Inst then
        self.m_MyNameLabel.text= CClientMainPlayer.Inst.Name
        myId=CClientMainPlayer.Inst.Id
    end

    for i,v in ipairs(data) do
        if v.id==myId then
            self.m_MyRankLabel.text=tostring(i)
            break
        end
    end

    self.m_RankList=data
    self.m_TableView:ReloadData(true,false)
end