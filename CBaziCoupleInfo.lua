-- Auto Generated!!
local CBaziCoupleInfo = import "L10.UI.CBaziCoupleInfo"
local CBaZiMgr = import "L10.Game.CBaZiMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local EnumGender = import "L10.Game.EnumGender"
local Extensions = import "Extensions"
local LocalString = import "LocalString"
local MsgPackImpl = import "MsgPackImpl"
local NGUITools = import "NGUITools"
local Object = import "System.Object"
local StringBuilder = import "System.Text.StringBuilder"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
CBaziCoupleInfo.m_Init_CS2LuaHook = function (this) 
    local baZiItem = LuaWeddingIterationMgr.baZiItem

    if baZiItem.Item.ExtraVarData.Data == nil then
        return
    end
    local data = baZiItem.Item.ExtraVarData.Data
    local list = TypeAs(MsgPackImpl.unpack(data), typeof(MakeGenericClass(List, Object)))
    if list == nil or list.Count < 2 then
        return
    end
    local info = nil
    do
        local i = 0
        while i < list.Count do
            local item = TypeAs(list[i], typeof(MakeGenericClass(List, Object)))
            if ToStringWrap(item[2]) == tostring((EnumToInt(this.gender))) then
                info = item
                break
            end
            i = i + 1
        end
    end

    this.nameLabel.text = ToStringWrap(info[1])
    local default
    default, this.birthYear = System.Int32.TryParse(ToStringWrap(info[3]))
    local extern
    extern, this.birthMonth = System.Int32.TryParse(ToStringWrap(info[4]))
    local ref
    ref, this.birthDay = System.Int32.TryParse(ToStringWrap(info[5]))
    local out
    out, this.birthHour = System.Int32.TryParse(ToStringWrap(info[6]))
    if default and extern and ref and out then
        this.birthLabel.text = CommonDefs.String_Format_String_ArrayObject(LocalString.GetString("{0}年{1}月{2}日{3}时生"), {this.birthYear, this.birthMonth, this.birthDay, this.birthHour})
    end
    this:InitBaziTime()
    this:GetBaziInfo()
end
CBaziCoupleInfo.m_InitBaziTime_CS2LuaHook = function (this) 
    this.baziResult = CBaZiMgr.Inst:GetBaZiByYMDH(this.BirthYear, this.BirthMonth, this.BirthDay, this.BirthHour)
    if this.baziResult == nil then
        return
    end
    Extensions.RemoveAllChildren(this.table.transform)
    this.timeTemplate:SetActive(false)
    local labels = CreateFromClass(MakeArrayClass(UILabel), 8)
    for i = 0, 7 do
        local instance = NGUITools.AddChild(this.table.gameObject, this.timeTemplate)
        instance:SetActive(true)
        local label = CommonDefs.GetComponentInChildren_GameObject_Type(instance, typeof(UILabel))
        if label ~= nil then
            labels[i] = label
        end
        local sprite = CommonDefs.GetComponent_GameObject_Type(instance, typeof(UISprite))
        if sprite ~= nil then
            local default
            if i % 2 == 0 then
                default = this.spriteName1
            else
                default = this.spriteName2
            end
            sprite.spriteName = default
        end
    end
    labels[0].text = this.baziResult.year.tian
    labels[1].text = this.baziResult.month.tian
    labels[2].text = this.baziResult.day.tian
    labels[3].text = this.baziResult.hour.tian
    labels[4].text = this.baziResult.year.di
    labels[5].text = this.baziResult.month.di
    labels[6].text = this.baziResult.day.di
    labels[7].text = this.baziResult.hour.di

    this.table:Reposition()
end
CBaziCoupleInfo.m_GetBaziInfo_CS2LuaHook = function (this) 
    this.info = CBaZiMgr.Inst:GetBaZiInfo(this.baziResult)
    if this.info == nil then
        return
    end
    local builder = NewStringBuilderWraper(StringBuilder)
    local default
    if this.gender == EnumGender.Male then
        default = LocalString.GetString("男")
    else
        default = LocalString.GetString("女")
    end
    builder:AppendFormat(LocalString.GetString("此{0}八字{1}，"), default, this.info.QiangRuo)
    builder:AppendFormat(LocalString.GetString("五行{0}旺，"), this.info.MaxWuXing)
    builder:AppendFormat(LocalString.GetString("用神为{0}，"), this.info.YongShen)
    builder:AppendFormat(LocalString.GetString("忌神为{0}"), this.info.JiShen)
    this.descLabel.text = ToStringWrap(builder)
end
