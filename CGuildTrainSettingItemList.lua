-- Auto Generated!!
local CGuildTrainMgr = import "L10.Game.CGuildTrainMgr"
local CGuildTrainSettingItemCell = import "L10.UI.CGuildTrainSettingItemCell"
local CGuildTrainSettingItemList = import "L10.UI.CGuildTrainSettingItemList"
local CommonDefs = import "L10.Game.CommonDefs"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local NGUITools = import "NGUITools"
local QnButton = import "L10.UI.QnButton"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
CGuildTrainSettingItemList.m_Init_CS2LuaHook = function (this, index) 
    this.categoryIndex = index
    Extensions.RemoveAllChildren(this.table.transform)
    CommonDefs.ListClear(this.trainItemList)
    this.trainItemCell:SetActive(false)
    local baseInfoList = CGuildTrainMgr.Inst.trainCategoryList[index].trainItemList
    do
        local i = 0
        while i < baseInfoList.Count do
            local trainItem = NGUITools.AddChild(this.table.gameObject, this.trainItemCell)
            CommonDefs.GetComponent_GameObject_Type(trainItem, typeof(CGuildTrainSettingItemCell)):Init(baseInfoList[i].name, baseInfoList[i].hasSelected)
            CommonDefs.GetComponent_GameObject_Type(trainItem, typeof(QnSelectableButton)).OnClick = CommonDefs.CombineListner_Action_QnButton(CommonDefs.GetComponent_GameObject_Type(trainItem, typeof(QnSelectableButton)).OnClick, MakeDelegateFromCSFunction(this.OnTrainItemClicked, MakeGenericClass(Action1, QnButton), this), true)
            CommonDefs.ListAdd(this.trainItemList, typeof(GameObject), trainItem)
            trainItem:SetActive(true)
            i = i + 1
        end
    end
    this.table:Reposition()
end
CGuildTrainSettingItemList.m_OnTrainItemClicked_CS2LuaHook = function (this, go) 
    do
        local i = 0
        while i < this.trainItemList.Count do
            if this.trainItemList[i] == go.gameObject then
                local b = CommonDefs.GetComponent_GameObject_Type(this.trainItemList[i], typeof(QnSelectableButton)):isSeleted()
                if not CGuildTrainMgr.Inst:ChangeTrainItemStatus(this.categoryIndex, i, b) then
                    CommonDefs.GetComponent_GameObject_Type(this.trainItemList[i], typeof(QnSelectableButton)):SetSelected(not b, false)
                else
                    CGuildTrainMgr.Inst.trainCategoryList[this.categoryIndex].trainItemList[i].hasSelected = b
                    if this.OnSettingChanged ~= nil then
                        invoke(this.OnSettingChanged)
                    end
                end
            end
            i = i + 1
        end
    end
end
