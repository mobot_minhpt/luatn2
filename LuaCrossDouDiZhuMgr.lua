
CLuaCrossDouDiZhuMgr = {}

--打开本场积分排行界面
function CLuaCrossDouDiZhuMgr.OpenInGameRankWnd()
	CLuaCrossDouDiZhuRankWnd.m_IsAllRank = false
	CUIManager.ShowUI(CLuaUIResources.CrossDouDiZhuRankWnd)
end

function CLuaCrossDouDiZhuMgr.GetJiangJin(silver)
	local t = {}
	local raw = DouDiZhu_CrossSetting.GetData().JueSaiPlayRankReward
	for rank, itemId, ratio in string.gmatch(raw, "(%d+),(%d+),([%d.]+);?") do
		rank, itemId, ratio = tonumber(rank), tonumber(itemId), tonumber(ratio)
        table.insert( t,math.floor(ratio*silver) )
	end
	return t
end

