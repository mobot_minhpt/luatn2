local CLogMgr = import "L10.CLogMgr"
local TaskNPCLocation = import "L10.Game.TaskNPCLocation"
local PublicMap_TeleportInfo = import "L10.Game.PublicMap_TeleportInfo"
local PublicMap_TeleportFxType = import "L10.Game.PublicMap_TeleportFxType"
local CPublicMapMgr = import "L10.Game.CPublicMapMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CrossMapPathInfo = import "L10.Game.PathInfo"
local TaskConditionType = import "L10.Game.TaskConditionType"
local TaskLocation = import "L10.Game.TaskLocation"
local CBinaryDesignDataReader2 = import "BinaryDesignData.CBinaryDesignDataReader2"
local CPos = import "L10.Engine.CPos"
local LocationPatchInfo = import "L10.Game.LocationPatchInfo"
local EnumObjectType = import "L10.Game.EnumObjectType"
local Guide_KeyValuePair = import "L10.Game.Guide_KeyValuePair"

GacCityReplaceHelperFunction = class()
GacCityReplacePostAction = class()

local function _debug_print(...)
    if CLuaCityReplaceHelperMgr.m_VerboseLog then
    end
end

-- 注意下replace函数不要在表里取出来的数据上改，要重新创建一个对象patch上去。

--#region Common Replace Function

function GacCityReplaceHelperFunction.Patch_Common_Internal(tablename, key, fieldName, value)
    if not key or key == "" then
        if _G[tablename] then
            _G[tablename].PatchField(fieldName, value)
        end
    else
        if _G[tablename] then
            _G[tablename].PatchField(tonumber(key), fieldName, value)
        end
    end
end

function GacCityReplaceHelperFunction.GetOriginalData(tablename, key, fieldName)
    local reader = CBinaryDesignDataReader2.Inst:GetReaderByName(tablename)

    if not reader then
        CLogMgr.LogError("Restore_Common_Internal failed for " .. tablename)
        return
    end

    local value
    if not key or key == "" then
        value = reader:GetRawData(true)[fieldName]
    else
        value = reader:GetRawData(tonumber(key), true)[fieldName]
    end

    return value
end

function GacCityReplaceHelperFunction.Restore_Common_Internal(tablename, key, fieldName)
    local value = GacCityReplaceHelperFunction.GetOriginalData(tablename, key, fieldName)
    GacCityReplaceHelperFunction.Patch_Common_Internal(tablename, key, fieldName, value)
end

function GacCityReplaceHelperFunction.Restore_Common(helper, id, tablename, key, fieldInfo)
    GacCityReplaceHelperFunction.Restore_Common_Internal(tablename, key, helper:GetClientName(tablename, fieldInfo))
end

function GacCityReplaceHelperFunction.Replace_Common(helper, id, tablename, key, fieldInfo)
    GacCityReplaceHelperFunction.Patch_Common_Internal(tablename, key, helper:GetClientName(tablename, fieldInfo), fieldInfo.ReplaceValue)
end

--#endregion

--#region TaskNPCLocation

function GacCityReplaceHelperFunction.Restore_TaskNPCLocation(helper, id, tablename, key, fieldInfo)
    GacCityReplaceHelperFunction.Restore_Common_Internal(tablename, key, helper:GetClientName(tablename, fieldInfo))
end

function GacCityReplaceHelperFunction.Replace_TaskNPCLocation(helper, id, tablename, key, fieldInfo)
    local location

    local sceneTemplateId, x, y = string.match(fieldInfo.ReplaceValue, "(%d+),(%d+),(%d+)")
    sceneTemplateId, x, y = tonumber(sceneTemplateId), tonumber(x), tonumber(y)
    if sceneTemplateId and x and y then
        location = TaskNPCLocation()
        location.sceneTemplateId = sceneTemplateId
        location.targetX = x
        location.targetY = y
    end
    GacCityReplaceHelperFunction.Patch_Common_Internal(tablename, key, helper:GetClientName(tablename, fieldInfo), location)
end

--#endregion

--#region Task_Task_UseItem

function GacCityReplaceHelperFunction.Restore_Task_Task_UseItem(helper, id, tablename, key, fieldInfo)
    GacCityReplaceHelperFunction.Restore_Common_Internal(tablename, key, "UseItem")
    GacCityReplaceHelperFunction.Restore_Common_Internal(tablename, key, "NeedUseItem")
end

function GacCityReplaceHelperFunction.Replace_Task_Task_UseItem(helper, id, tablename, key, fieldInfo)
    local dict = CreateFromClass(MakeGenericClass(Dictionary, String, UInt32))

    for sceneTemplateId, x, y, itemTemplateId, count in string.gmatch(fieldInfo.ReplaceValue, "(%d+),(%d+),(%d+),(%d+),(%d+);?") do
        local v = tonumber(count)
        local k = table.concat({sceneTemplateId, x, y, itemTemplateId}, ",")
        if k and v then
            CommonDefs.DictAdd(dict, typeof(String), k, typeof(UInt32), v)
            _debug_print("Replace_Task_Task_UseItem", key, k, v)
        end
    end

    GacCityReplaceHelperFunction.Patch_Common_Internal(tablename, key, "UseItem", fieldInfo.ReplaceValue)
    GacCityReplaceHelperFunction.Patch_Common_Internal(tablename, key, "NeedUseItem", dict)
end

--#endregion

--#region Task_Task_Locations

function GacCityReplaceHelperFunction.Restore_Task_Task_Locations(helper, id, tablename, key, fieldInfo)
    GacCityReplaceHelperFunction.Restore_Common_Internal(tablename, key, "Locations")
    GacCityReplaceHelperFunction.Restore_Common_Internal(tablename, key, "TaskLocations")
end

function GacCityReplaceHelperFunction.Replace_Task_Task_Locations(helper, id, tablename, key, fieldInfo)
    local locations = CreateFromClass(MakeGenericClass(List, TaskLocation))
    local oldLocations = GacCityReplaceHelperFunction.GetOriginalData(tablename, key, "TaskLocations")

    local modifyTbl = {}
    local index = 0
    for locstr in string.gmatch(fieldInfo.ReplaceValue, "([^;]+)") do
        local linkType, _, _, x, y = string.match(locstr, "(%a+)%s*=%s*([^,]*)%s*,%s*(%d+)%s*,%s*(%d+)%s*,%s*(%d+)")
        x, y = tonumber(x), tonumber(y)
        if x and y then
            modifyTbl[index] = {x, y}
        end
        index = index + 1
    end

    for i = 0, oldLocations.Length - 1 do
        local loc = CreateFromClass(TaskLocation)
        local oldLoc = oldLocations[i]
        loc.taskId = oldLoc.taskId
        loc.trackInfo = oldLoc.trackInfo
        loc.sceneTemplateId = oldLoc.sceneTemplateId
        loc.targetX = oldLoc.targetX
        loc.targetY = oldLoc.targetY
        loc.ConditionObjectId = oldLoc.ConditionObjectId
        loc.targetObjectType = oldLoc.targetObjectType
        loc.destEvent = oldLoc.destEvent
        loc.conditionType = oldLoc.conditionType
        loc.InteractWithNpcId = oldLoc.InteractWithNpcId
        loc.sceneId = oldLoc.sceneId
        loc.eventName = oldLoc.eventName
        loc.wndName = oldLoc.wndName
        loc.selectIndex = oldLoc.selectIndex
        loc.dynamicSelection = oldLoc.dynamicSelection
        loc.itemId = oldLoc.itemId
        if modifyTbl[i] then
            loc.targetX = modifyTbl[i][1]
            loc.targetY = modifyTbl[i][2]
            _debug_print("Replace_Task_Task_Locations", key, index, loc.targetX, loc.targetY )
        end
		CommonDefs.ListAdd(locations, typeof(TaskLocation), loc)
    end

    GacCityReplaceHelperFunction.Patch_Common_Internal(tablename, key, "Locations", fieldInfo.ReplaceValue)
    GacCityReplaceHelperFunction.Patch_Common_Internal(tablename, key, "TaskLocations", CommonDefs.ListToArray(locations))
end

--#endregion

--#region GameplayItem_YiZhangLocations_Locations

function GacCityReplaceHelperFunction.Restore_GameplayItem_YiZhangLocations_Locations(helper, id, tablename, key, fieldInfo)
    GacCityReplaceHelperFunction.Restore_Common_Internal(tablename, key, helper:GetClientName(tablename, fieldInfo))
end

function GacCityReplaceHelperFunction.Replace_GameplayItem_YiZhangLocations_Locations(helper, id, tablename, key, fieldInfo)
	local list = CreateFromClass(MakeGenericClass(List, MakeArrayClass(Int32)))

    for x, y, z in string.gmatch(fieldInfo.ReplaceValue, "(%d+),(%d+),(%d+);?") do
        x, y, z = tonumber(x), tonumber(y), tonumber(z)
        local pos = CreateFromClass(MakeGenericClass(List, Int32))
		CommonDefs.ListAdd(pos, typeof(Int32), x)
		CommonDefs.ListAdd(pos, typeof(Int32), y)
		CommonDefs.ListAdd(pos, typeof(Int32), z)
		CommonDefs.ListAdd(list, typeof(MakeArrayClass(Int32)), CommonDefs.ListToArray(pos))
    end
    GacCityReplaceHelperFunction.Patch_Common_Internal(tablename, key, helper:GetClientName(tablename, fieldInfo), CommonDefs.ListToArray(list))
end

--#endregion

--#region PublicMap_PublicMap_TeleportFxType

function GacCityReplaceHelperFunction.Restore_PublicMap_PublicMap_TeleportFxType(helper, id, tablename, key, fieldInfo)
    GacCityReplaceHelperFunction.Restore_Common_Internal(tablename, key, helper:GetClientName(tablename, fieldInfo))
end

function GacCityReplaceHelperFunction.Replace_PublicMap_PublicMap_TeleportFxType(helper, id, tablename, key, fieldInfo)
	local list = CreateFromClass(MakeGenericClass(List, PublicMap_TeleportFxType))

    for srcx, srcy, fxType in string.gmatch(fieldInfo.ReplaceValue, "(%d+),(%d+),(%d+);?") do
        srcx, srcy, fxType= tonumber(srcx), tonumber(srcy), tonumber(fxType)
        local p = PublicMap_TeleportFxType()
        p.SrcX = srcx
        p.SrcY = srcy
        p.SrcZ = 0
        p.FxType = fxType
		CommonDefs.ListAdd(list, typeof(PublicMap_TeleportFxType), p)
    end

    GacCityReplaceHelperFunction.Patch_Common_Internal(tablename, key, helper:GetClientName(tablename, fieldInfo), CommonDefs.ListToArray(list))
end

--#endregion

--#region PublicMap_PublicMap_Teleport

function GacCityReplaceHelperFunction.Restore_PublicMap_PublicMap_Teleport(helper, id, tablename, key, fieldInfo)
    GacCityReplaceHelperFunction.Restore_Common_Internal(tablename, key, helper:GetClientName(tablename, fieldInfo))
    GacCityReplaceHelperFunction.Restore_Common_Internal("CityReplacHelperGen_MapInfo", key, helper:GetClientName(tablename, fieldInfo))
end

function GacCityReplaceHelperFunction.Replace_PublicMap_PublicMap_Teleport(helper, id, tablename, key, fieldInfo)
	local list = CreateFromClass(MakeGenericClass(List, PublicMap_TeleportInfo))

    for srcx, srcy, destSceneId, destx, desty in string.gmatch(fieldInfo.ReplaceValue, "(%d+),(%d+),(%d+),(%d+),(%d+);?") do
        srcx, srcy, destSceneId, destx, desty = tonumber(srcx), tonumber(srcy), tonumber(destSceneId), tonumber(destx), tonumber(desty)
        local p = PublicMap_TeleportInfo()
        p.SrcX = srcx
        p.SrcY = srcy
        p.SrcZ = 0
        p.DestSceneId = destSceneId
        p.DestX = destx
        p.DestY = desty
        p.DestZ = 0
		CommonDefs.ListAdd(list, typeof(PublicMap_TeleportInfo), p)
    end

    GacCityReplaceHelperFunction.Patch_Common_Internal(tablename, key, helper:GetClientName(tablename, fieldInfo), CommonDefs.ListToArray(list))
    GacCityReplaceHelperFunction.Patch_Common_Internal("CityReplacHelperGen_MapInfo", key, helper:GetClientName(tablename, fieldInfo), fieldInfo.ReplaceValue)
end

--#endregion

--#region CrossMapPathHelper

function GacCityReplaceHelperFunction.Restore_CrossMapPathHelper(helper, id, tablename, key, fieldInfo)
    GacCityReplaceHelperFunction.Restore_Common_Internal("CityReplacHelperGen_MapInfo", key, helper:GetClientName(tablename, fieldInfo))
end

function GacCityReplaceHelperFunction.Replace_CrossMapPathHelper(helper, id, tablename, key, fieldInfo)
    GacCityReplaceHelperFunction.Patch_Common_Internal("CityReplacHelperGen_MapInfo", key, helper:GetClientName(tablename, fieldInfo), fieldInfo.ReplaceValue)
end

--#endregion

--#region Dialog_Dialog_Choice

function GacCityReplaceHelperFunction.Restore_Dialog_Dialog_Choice(helper, id, tablename, key, fieldInfo)
    if CityReplacHelperGen_TeleportDialog.GetData(tonumber(key)) then
        GacCityReplaceHelperFunction.Restore_Common_Internal("CityReplacHelperGen_TeleportDialog", key, "TargetMaps")
    end
end

function GacCityReplaceHelperFunction.Replace_Dialog_Dialog_Choice(helper, id, tablename, key, fieldInfo)
    key = tonumber(key)
    if CityReplacHelperGen_TeleportDialog.GetData(key) then
        local setting = CityReplacHelperGen_Setting.GetData()
        local guild_city_id =  setting.GuildCityId
        local guild_city_entrance_x = setting.GuildCityEntranceX
        local guild_city_entrance_y = setting.GuildCityEntranceY
        
        local choice_str = fieldInfo.ReplaceValue
        local targets = CreateFromClass(MakeGenericClass(List, MakeArrayClass(uint)))
        local choice_index = 0
        for line in string.gmatch(choice_str or "", "([^\n]+)") do
            local display, action = string.match(line, "([^|]+)|([^|]+)")
            if action then
                local target_map_id, target_x, target_y, target_z = string.match(action, "Teleport%(%{%s*(%d+)%s*,%s*(%d+)%s*,%s*(%d+)%s*,?%s*(%d*)%s*%}%)")
                target_map_id, target_x, target_y, target_z = tonumber(target_map_id), tonumber(target_x), tonumber(target_y), tonumber(target_z) or 0
                if target_map_id and target_x and target_y and target_z then
                    CommonDefs.ListAdd(targets, typeof(MakeArrayClass(uint)), Table2Array({target_map_id, target_x, target_y, target_z, choice_index, 0}, MakeArrayClass(uint)))
                end

                target_map_id, target_x, target_y, target_z = string.match(action, "TeleportToAmusementPark%(%{%s*(%d+)%s*,%s*(%d+)%s*,%s*(%d+)%s*,?%s*(%d*)%s*%}%)")
                target_map_id, target_x, target_y, target_z = tonumber(target_map_id), tonumber(target_x), tonumber(target_y), tonumber(target_z) or 0
                if target_map_id and target_x and target_y and target_z then
                    CommonDefs.ListAdd(targets, typeof(MakeArrayClass(uint)), Table2Array({target_map_id, target_x, target_y, target_z, choice_index, 0}, MakeArrayClass(uint)))
                end

                local bGuildTeleport = string.match(action, "GuildCity()")
                if bGuildTeleport then
                    CommonDefs.ListAdd(targets, typeof(MakeArrayClass(uint)), Table2Array({guild_city_id, guild_city_entrance_x, guild_city_entrance_y, 0, choice_index, 1}, MakeArrayClass(uint)))
                end
            end
            choice_index = choice_index + 1
        end
        GacCityReplaceHelperFunction.Patch_Common_Internal("CityReplacHelperGen_TeleportDialog", key, "TargetMaps", CommonDefs.ListToArray(targets))
    end
end

--#endregion

--#region StringToUintArray

function GacCityReplaceHelperFunction.Restore_StringToUintArray(helper, id, tablename, key, fieldInfo)
    GacCityReplaceHelperFunction.Restore_Common_Internal(tablename, key, helper:GetClientName(tablename, fieldInfo))
end

function GacCityReplaceHelperFunction.Replace_StringToUintArray(helper, id, tablename, key, fieldInfo)
    local ary = CreateFromClass(MakeGenericClass(List, UInt32))
    for val in string.gmatch(fieldInfo.ReplaceValue, "(%d+),?") do
        val =  tonumber(val)
        if val then
            CommonDefs.ListAdd(ary, typeof(UInt32), val)
        end
    end
    GacCityReplaceHelperFunction.Patch_Common_Internal(tablename, key, helper:GetClientName(tablename, fieldInfo), CommonDefs.ListToArray(ary))
end

--#endregion

--#region StringToUintArray

function GacCityReplaceHelperFunction.Restore_StringToUintUintArray(helper, id, tablename, key, fieldInfo)
    GacCityReplaceHelperFunction.Restore_Common_Internal(tablename, key, helper:GetClientName(tablename, fieldInfo))
end

function GacCityReplaceHelperFunction.Replace_StringToUintUintArray(helper, id, tablename, key, fieldInfo)

    local res = CreateFromClass(MakeGenericClass(List, MakeArrayClass(uint)))
    for part in string.gmatch(fieldInfo.ReplaceValue, "([^;]+);?") do
        if part then
            local ary = CreateFromClass(MakeGenericClass(List, UInt32))
            for val in string.gmatch(part, "(%d+),?") do
                val =  tonumber(val)
                if val then
                    CommonDefs.ListAdd(ary, typeof(UInt32), val)
                end
            end
            CommonDefs.ListAdd(res, typeof(MakeArrayClass(uint)), CommonDefs.ListToArray(ary))
        end
    end
    GacCityReplaceHelperFunction.Patch_Common_Internal(tablename, key, helper:GetClientName(tablename, fieldInfo), CommonDefs.ListToArray(res))
end

--#endregion

--#region GuideKeyValuePair

function GacCityReplaceHelperFunction.Restore_GuideKeyValuePair(helper, id, tablename, key, fieldInfo)
    GacCityReplaceHelperFunction.Restore_Common_Internal(tablename, key, helper:GetClientName(tablename, fieldInfo))
end

function GacCityReplaceHelperFunction.Replace_GuideKeyValuePair(helper, id, tablename, key, fieldInfo)
    local res = CreateFromClass(MakeGenericClass(List, Guide_KeyValuePair))
    for part in string.gmatch(fieldInfo.ReplaceValue, "([^;]+);?") do
        if part then
            local tbl = {}
            for v in string.gmatch(part, "([^=]+)") do
                table.insert(tbl, v)
            end
            if #tbl == 2 then
                local p = Guide_KeyValuePair()
                p.Key = StringTrim(tbl[1])
                p.Val = StringTrim(tbl[2])
                CommonDefs.ListAdd(res, typeof(Guide_KeyValuePair), p)
            elseif #tbl == 1 then
                local p = Guide_KeyValuePair()
                p.Key = StringTrim(tbl[1])
                p.Val = ""
                CommonDefs.ListAdd(res, typeof(Guide_KeyValuePair), p)
            end
        end
    end
    if res.Count > 0 then
        GacCityReplaceHelperFunction.Patch_Common_Internal(tablename, key, helper:GetClientName(tablename, fieldInfo), CommonDefs.ListToArray(res))
    else
        GacCityReplaceHelperFunction.Patch_Common_Internal(tablename, key, helper:GetClientName(tablename, fieldInfo), nil)
    end
end

--#endregion

function GacCityReplacePostAction.ReloadLocation(replaces)

    -- 1. 先找出哪些mapId受了影响
    local affectedMapIdTbl = {}
    for _, replaceInfo in ipairs(replaces) do
        local id, tablename, key, fieldInfo = replaceInfo[1], replaceInfo[2], replaceInfo[3], replaceInfo[4]
        local fieldName = fieldInfo.FieldName
        if tablename == "PublicMap_PublicMap"  and (fieldName == "NPC" or fieldName == "Monster" or fieldName == "Pick" or fieldName == "Temple")then
            key = tonumber(key)
            affectedMapIdTbl[key] = 1
        else
            CLogMgr.LogError("ReloadLocation for bad field " .. tablename .. "_" .. fieldName)
        end
    end

    local function _create_location_obj(location, _type, _npc_id, _map_id, _x, _y, _z)
        location[_map_id] = location[_map_id] or {}
        location[_map_id][_type] = location[_map_id][_type]  or {}
        location[_map_id][_type][_npc_id] = location[_map_id][_type][_npc_id] or {}
        table.insert(location[_map_id][_type][_npc_id], {_x, _y, _z})
    end

    local _TYPE_NPC = "NPC"
    local _TYPE_MONSTER = "Monster"
    local _TYPE_PICK = "Pick"
    local _TYPE_TEMPLE = "Temple"
    local locationUpdateTbl = {}

    for mapId, _ in pairs(affectedMapIdTbl) do
        local mapInfo = CityReplacHelperGen_MapInfo.GetData(mapId)

        local npc_str = mapInfo and mapInfo.NPC
        local monster_str = mapInfo and mapInfo.Monster
        local pick_str = mapInfo and mapInfo.Pick
        local temple_str = mapInfo and mapInfo.Temple

        if npc_str then
            for npcId, x, y in string.gmatch(npc_str, "(%d+),(%d+),(%d+),(%d+),?(%d*)p?(%d*)") do
                npcId, x, y = tonumber(npcId), tonumber(x), tonumber(y)
                if npcId and x and y then
                    _create_location_obj(locationUpdateTbl, _TYPE_NPC, npcId, mapId, x, y, 0)
                end
            end
        end

        if monster_str then
            for x, y, id in string.gmatch(monster_str, "(%d+),(%d+),(%d+),(%d+),(%d+),(%d+),(.-);?") do
                id, x, y = tonumber(id), tonumber(x), tonumber(y)
                if id and x and y then
                    _create_location_obj(locationUpdateTbl, _TYPE_MONSTER, id, mapId, x, y, 0)
                end
            end
        end

        if pick_str then
            for x, y, id in string.gmatch(pick_str, "(%d+),(%d+),(%d+),(%d+),(%d+),(%d+)") do
                id, x, y = tonumber(id), tonumber(x), tonumber(y)
                if id and x and y then
                    _create_location_obj(locationUpdateTbl, _TYPE_PICK, id, mapId, x, y, 0)
                end
            end
        end

        if temple_str then
            for x, y, id in string.gmatch(temple_str, "(%d+),(%d+),(%d+),(%d+),(%d+),(%d+)") do
                id, x, y = tonumber(id), tonumber(x), tonumber(y)
                if id and x and y then
                    _create_location_obj(locationUpdateTbl, _TYPE_TEMPLE, id, mapId, x, y, 0)
                end
            end
        end
    end

    local _StrObjectType2Int = {
        NPC = 3,
        Monster = 2,
        Pick = 7,
        Temple = 8,
    }

    -- 分类
    local toAdd = {}
    local toUpdate = {}
    local toDelete  = {}

    for mapId, info in pairs(locationUpdateTbl) do
        for strObjectType, objectInfo in pairs(info) do
            local intObjectType = _StrObjectType2Int[strObjectType]
            local mapObjects = CPublicMapMgr.Instance:GetMapObjects(mapId, EnumFromInt(EnumObjectType, intObjectType))
            local originalObjects = {}
            CommonDefs.DictIterate(mapObjects, DelegateFactory.Action_object_object(function (objectId, list)
                if  objectInfo[objectId] then
                    table.insert(toUpdate, {intObjectType, objectId, mapId, objectInfo[objectId]})
                else
                    table.insert(toDelete, {intObjectType, objectId, mapId})
                end
                originalObjects[objectId] = 1
            end))

            for id, posList in pairs(objectInfo) do
                if not originalObjects[id] then
                    table.insert(toAdd, {intObjectType, id, mapId, posList})
                end
            end
        end
    end


    GacCityReplacePostAction.UpdateLocation(toAdd, toUpdate, toDelete)
    GacCityReplacePostAction.FixTaskConditionLocation(toAdd, toUpdate, toDelete)
end


function GacCityReplacePostAction.UpdateLocation(toAdd, toUpdate, toDelete)

    local function _add_location_patch_info(map_id, type, objectId, posTbl)
        local list = CreateFromClass(MakeGenericClass(List, CPos))
        for i, _ in pairs(posTbl or {}) do
            local p = CreateFromClass(CPos, posTbl[i][1], posTbl[i][2], posTbl[i][3] or 0)
            CommonDefs.ListAdd(list, typeof(CPos), p)
        end
        local patchInfo = LocationPatchInfo()
        patchInfo.map_id = map_id
        patchInfo.type = type
        patchInfo.objectId = objectId
        patchInfo.posList = list
	    CommonDefs.ListAdd(CPublicMapMgr.Instance.m_LocationPatchInfo, typeof(LocationPatchInfo), patchInfo)
    end

    for _, info in pairs(toAdd) do
        local objectType, id, mapId, val = unpack(info)
        _add_location_patch_info(mapId, objectType, id, val)
    end

    for _, info in pairs(toUpdate) do
        local objectType, id, mapId, val = unpack(info)
        _add_location_patch_info(mapId, objectType, id, val)
    end

    for _, info in pairs(toDelete) do
        local objectType, id, mapId, val = unpack(info)
        _add_location_patch_info(mapId, objectType, id, val)
    end

    CPublicMapMgr.Instance:ApplyLocationPatchInfo()
end

function GacCityReplacePostAction.FixTaskConditionLocation(toAdd, toUpdate, toDelete)
    -- 修一下Task中的ConditionLocation，如果Task没有定义Location的话，会
    -- 根据UseItem、KillMonster、FindNpc等条件，自动生成一个Location。
    -- UseItem跟Location没关系，在替换UseItem的地方修。
    -- 自动生成的TaskLocation用到了Location表的数据，因此需要修改这些Task的定义。


    -- 哪些object的位置变了，并取第一个位置为任务寻路位置。
    -- toAdd只会出现在开启替换，新场景增加npc。这种情况下任务的寻路位置是正确的。
    -- toDelete只会出现在关闭替换，新npc被去掉。这种情况下任务的寻路是没有意义的


    local locationChangedObjectTbl = {}
    for _, info in pairs(toAdd) do
        local objectType, id, mapId, posList = unpack(info)
        locationChangedObjectTbl[id] = {1, mapId, posList[1][1], posList[1][2], posList[1][3]}
    end

    for _, info in pairs(toUpdate) do
        local objectType, id, mapId, posList = unpack(info)
        locationChangedObjectTbl[id] = {2, mapId, posList[1][1], posList[1][2], posList[1][3]}
    end

    for _, info in pairs(toDelete) do
        local objectType, id, mapId, posList = unpack(info)
        locationChangedObjectTbl[id] = {3, 0, 0, 0, 0}
    end

    -- 取到所有依赖于对象Location生成TaskLocations的task

    CityReplacHelperGen_Task.Foreach(function(taskId, data)
        if not data.ConditionObject then return end
        local td = Task_Task.GetData(taskId)
        if not td then return end

        local bChanged = false

        for t = 0, data.ConditionObject.Length - 1 do
            local conditionObjectId = data.ConditionObject[t]
            if locationChangedObjectTbl[conditionObjectId] then
                local info = locationChangedObjectTbl[conditionObjectId]
                local reason, mapId, x, y, z = info[1], info[2], info[3], info[4], info[5]
                if td and td.TaskLocations then
                    for i = 0, td.TaskLocations.Length - 1 do
                        local loc = td.TaskLocations[i]
                        if loc.ConditionObjectId == conditionObjectId then
                            local oldSceneTemplateId, oldx, oldy = loc.sceneTemplateId, loc.targetX, loc.targetY
                            loc.sceneTemplateId = mapId
                            loc.targetX = x
                            loc.targetY = y
                            bChanged = true
                            _debug_print(SafeStringFormat3("UpdateTaskConditionLocation %s-%s-%s-%s :%s,,%s,,%s -> %s,,%s,,%s", taskId, i, loc.ConditionObjectId, reason, oldSceneTemplateId, oldx, oldy,
                                                                                mapId, x, y))
                        end
                    end
                end
            end
        end

        if bChanged then
            Task_Task.PatchField(taskId, "TaskLocations", td.TaskLocations)
        end
    end)
end


function GacCityReplacePostAction.ReloadCrossMapPath(replaces)
    -- 有传送选项的DialogId -> Dialog所挂NPC在的MapId

    local affectedMapIdTbl = {}
    for _, replaceInfo in ipairs(replaces) do
        local id, tablename, key, fieldInfo = replaceInfo[1], replaceInfo[2], replaceInfo[3], replaceInfo[4]
        local fieldName = fieldInfo.FieldName
        if tablename == "PublicMap_PublicMap" and (fieldName == "NPC" or fieldName == "Teleport" or fieldName == "AssistantTeleport")then
            key = tonumber(key)
            affectedMapIdTbl[key] = 1
        elseif tablename == "Dialog_Dialog" and fieldName == "Choice" then
            local dialogId = tonumber(key)
            local teleportdata = CityReplacHelperGen_TeleportDialog.GetData(dialogId)
            if teleportdata and teleportdata.SourceMaps then
                for i = 0, teleportdata.SourceMaps.Length - 1 do
                    affectedMapIdTbl[teleportdata.SourceMaps[i]] = 1
                end
            end
        else
            CLogMgr.LogError("ReloadCrossMapPath for bad field " .. tablename .. "_" .. fieldName)
        end
    end

    local mapId2PathInfoTbl = {}
    local function _create_crossmap_node(_map_id, _x, _y, _z, _target_map_id, _target_x, _target_y, _target_z, _npc_id, _dialog_id, _choice_index, _type)
        _debug_print("createnode", _map_id, _x, _y, _z, _target_map_id, _target_x, _target_y, _target_z, _npc_id, _dialog_id, _choice_index, _type)
        mapId2PathInfoTbl[_map_id] = mapId2PathInfoTbl[_map_id] or CreateFromClass(MakeGenericClass(List, CrossMapPathInfo))
        local path = CrossMapPathInfo()
        path.mapTemplateId = _map_id
        path.x = _x
        path.y = _y
        path.z = _z
        path.targetMapId = _target_map_id
        path.targetPosX = _target_x
        path.targetPosY = _target_y
        path.targetPosZ = _target_z
        path.npcId = _npc_id or 0
        path.dialogId = _dialog_id or 0
        path.choiceIndex = _choice_index or 0
        path.type = _type or "normal"
        CommonDefs.ListAdd(mapId2PathInfoTbl[_map_id], typeof(CrossMapPathInfo), path)
    end

    for mapId, _ in pairs(affectedMapIdTbl) do
        local mapInfo = CityReplacHelperGen_MapInfo.GetData(mapId)
        local npc_str = mapInfo and mapInfo.NPC
        local teleport_str = mapInfo and mapInfo.Teleport
        local assistant_teleport_str = mapInfo and mapInfo.AssistantTeleport
        _debug_print("affect mapId ", mapId, npc_str, teleport_str, assistant_teleport_str)

        for x, y, target_map_id, target_x, target_y, target_z in string.gmatch(teleport_str or "", "(%d+),(%d+),(%d+),(%d+),(%d+),?(%d*)") do
            x, y, target_map_id, target_x, target_y, target_z = tonumber(x), tonumber(y), tonumber(target_map_id), tonumber(target_x), tonumber(target_y), tonumber(target_z) or 0
            if x and y and target_map_id and target_x and target_y and target_z then
                _create_crossmap_node(mapId, x, y, 0, target_map_id, target_x, target_y, target_z)
            end
        end

        for x, y, target_map_id, target_x, target_y in string.gmatch(assistant_teleport_str or "", "(%d+),(%d+),(%d+),(%d+),(%d+)") do
            x, y, target_map_id, target_x, target_y = tonumber(x), tonumber(y), tonumber(target_map_id), tonumber(target_x), tonumber(target_y)
            if x and y and target_map_id and target_x and target_y then
                _create_crossmap_node(mapId, x, y, 0, target_map_id, target_x, target_y, 0, nil, nil, nil, "assistant")
            end
        end

        for npcId, x, y in string.gmatch(npc_str or "", "(%d+),(%d+),(%d+),(%d+),?(%d*)p?(%d*)") do
            npcId, x, y = tonumber(npcId), tonumber(x), tonumber(y)
            local npcData = npcId and NPC_NPC.GetData(npcId)
            local dialogId = npcData.DialogId
            if dialogId ~= 0 then
                local teleportInfo = CityReplacHelperGen_TeleportDialog.GetData(dialogId)
                local choices = teleportInfo and teleportInfo.TargetMaps
                if choices then
                    for i = 0, choices.Length - 1 do
                        local target_map_id, target_x, target_y, target_z, choice_index, isGuild = choices[i][0], choices[i][1], choices[i][2], choices[i][3], choices[i][4], choices[i][5]
                        if target_map_id and target_x and target_y and target_z and choice_index then
                            _create_crossmap_node(mapId, x, y, 0, target_map_id, target_x, target_y, target_z, npcId, dialogId, choice_index, isGuild == 1 and "guild" or nil)
                        end
                    end
                end
            end
        end
    end

    for mapId, pathList in pairs(mapId2PathInfoTbl) do
        CommonDefs.DictSet(CPublicMapMgr.Instance.m_CrossMapPathPatchInfo, typeof(UInt32), mapId, typeof(MakeGenericClass(List, CrossMapPathInfo)), pathList)
    end
    CPublicMapMgr.Instance:ApplyCrossMapPathPatchInfo()
end

function GacCityReplacePostAction.FixUseItemTaskConditionLocation(replaces)

    -- 哪些Task收了影响
    local affectedTaskTbl = {}
    for _, replaceInfo in ipairs(replaces) do
        local id, tablename, key, fieldInfo = replaceInfo[1], replaceInfo[2], replaceInfo[3], replaceInfo[4]
        local fieldName = fieldInfo.FieldName
        if tablename == "Task_Task"  and fieldName == "UseItem" then
            key = tonumber(key)
            affectedTaskTbl[key] = 1
        else
            CLogMgr.LogError("FixUseItemTaskConditionLocation for bad field " .. tablename .. "_" .. fieldName)
        end
    end

    local eUseItem = EnumToInt(TaskConditionType.UseItem)

    for taskId, _ in pairs(affectedTaskTbl) do

        -- 是需要修正的ConditionalLocationTask
        if CityReplacHelperGen_Task.GetData(taskId) then
            local td = Task_Task.GetData(taskId)

            -- 这里有个强假设，即UseItem只改了场景和坐标；其他都没变。
            local newPosList = {}
            for sceneTemplateId, x, y, itemTemplateId, _ in string.gmatch(td.UseItem, "(%d+),(%d+),(%d+),(%d+),(%d+);?") do
                sceneTemplateId, x, y, itemTemplateId = tonumber(sceneTemplateId), tonumber(x), tonumber(y), tonumber(itemTemplateId)
                table.insert(newPosList, {sceneTemplateId, x, y, itemTemplateId})
            end

            local bChanged = false

            for i = 0, td.TaskLocations.Length -1 do
                local loc = td.TaskLocations[i]
                local newPos = newPosList[i + 1]
                if loc.conditionType == eUseItem and loc.ConditionObjectId == newPos[4] then
                    local oldSceneTemplateId, oldx, oldy = loc.sceneTemplateId, loc.targetX, loc.targetY
                    loc.sceneTemplateId = newPos[1]
                    loc.targetX = newPos[2]
                    loc.targetY = newPos[3]
                    bChanged = true
                    _debug_print(SafeStringFormat3("FixUseItemTaskConditionLocation %s-%s-%s :%s,,%s,,%s -> %s,,%s,,%s", taskId, i, loc.ConditionObjectId, oldSceneTemplateId, oldx, oldy,
                                                                        loc.sceneTemplateId, loc.targetX, loc.targetY))
                else
                    CLogMgr.LogError("FixUseItemTaskConditionLocation Error!")
                end
            end

            if bChanged then
                Task_Task.PatchField(taskId, "TaskLocations",  td.TaskLocations)
            end
        end
    end
end

local GameSetting_Client_Wapper = import "L10.Game.GameSetting_Client_Wapper"
local GameSetting_Client = import "L10.Game.GameSetting_Client"

function GacCityReplacePostAction.ReloadNewPlayerFirstTaskId()
    GameSetting_Client_Wapper.Inst.m_NewPlayerFirstTaskId = GameSetting_Client.GetData().NewPlayerFirstTaskId
end
