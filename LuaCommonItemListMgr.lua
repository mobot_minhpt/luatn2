CLuaCommonItemListMgr={}

CLuaCommonItemListMgr.m_DataList={}
CLuaCommonItemListMgr.m_AlignType=nil
CLuaCommonItemListMgr.m_TargetTransform=nil
CLuaCommonItemListMgr.m_Callback=nil
CLuaCommonItemListMgr.m_LongPressShift=false
CLuaCommonItemListMgr.m_LongPressShiftDuration=5.0
CLuaCommonItemListMgr.m_LongPressMaxCallbackPerSecond=20
CLuaCommonItemListMgr.m_ItemDescriptionHander={}


function CLuaCommonItemListMgr.ShowWnd(dataList,tf,alignType,longPressShift,callback)
    CLuaCommonItemListMgr.m_DataList=dataList
    CLuaCommonItemListMgr.m_AlignType=alignType
    CLuaCommonItemListMgr.m_TargetTransform=tf
    CLuaCommonItemListMgr.m_LongPressShift=longPressShift
    CLuaCommonItemListMgr.m_Callback=callback

    CUIManager.ShowUI(CLuaUIResources.CommonItemListWnd)
end

function CLuaCommonItemListMgr.GetCustomDescription(templateId)
    if CLuaCommonItemListMgr.m_ItemDescriptionHander[templateId] then
        return CLuaCommonItemListMgr.m_ItemDescriptionHander[templateId]
    end
end

function CLuaCommonItemListMgr.SetCustomDescription(templateId,desc)
    CLuaCommonItemListMgr.m_ItemDescriptionHander[templateId]=desc
end
function CLuaCommonItemListMgr.RemoveCustomDescription(templateId)
    CLuaCommonItemListMgr.m_ItemDescriptionHander[templateId]=nil
end

