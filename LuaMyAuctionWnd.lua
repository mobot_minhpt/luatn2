local UIScrollViewIndicator = import "UIScrollViewIndicator"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UITable = import "UITable"
local GameObject = import "UnityEngine.GameObject"
local UISprite = import "UISprite"
local UIRoot = import "UIRoot"
local Screen = import "UnityEngine.Screen"
local Time = import "UnityEngine.Time"

LuaMyAuctionWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaMyAuctionWnd, "ScrollView", "ScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaMyAuctionWnd, "Table", "Table", UITable)
RegistChildComponent(LuaMyAuctionWnd, "Template", "Template", GameObject)
RegistChildComponent(LuaMyAuctionWnd, "Background", "Background", UISprite)
RegistChildComponent(LuaMyAuctionWnd, "ScrollViewIndicator", "ScrollViewIndicator", UIScrollViewIndicator)

--@endregion RegistChildComponent end
RegistClassMember(LuaMyAuctionWnd, "m_InitialHeight")
RegistClassMember(LuaMyAuctionWnd, "m_SubTitleLabelHeight")
RegistClassMember(LuaMyAuctionWnd, "m_SubGridCellHeight")
RegistClassMember(LuaMyAuctionWnd, "m_Tick")

function LuaMyAuctionWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaMyAuctionWnd:Init()
    local subTitleLabel = self.Template.transform:Find("TitleLabel"):GetComponent(typeof(UILabel))
    local subGrid = self.Template.transform:Find("Grid"):GetComponent(typeof(UIGrid))
    self.m_SubTitleLabelHeight = subTitleLabel.height
    self.m_SubGridCellHeight = subGrid.cellHeight
    self.m_InitialHeight = self.Background.height - self.m_SubTitleLabelHeight - self.m_SubGridCellHeight - self.Table.padding.y
    self.Template.gameObject:SetActive(false)
    self.ScrollViewIndicator.gameObject:SetActive(false)
    self:OnSendPaimaiMySystemItemsEnd()
end

function LuaMyAuctionWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSendPaimaiMySystemItemsEnd", self, "OnSendPaimaiMySystemItemsEnd")
end

function LuaMyAuctionWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnSendPaimaiMySystemItemsEnd", self, "OnSendPaimaiMySystemItemsEnd")
    self:CancelTick()
end

function LuaMyAuctionWnd:OnSendPaimaiMySystemItemsEnd()
    local height = self.m_InitialHeight
    Extensions.RemoveAllChildren(self.Table.transform)

    for TemplateId,list in pairs(LuaAuctionMgr.m_PaimaiMySystemItems) do
        for i = 1, #list do
            height = height + self.m_SubGridCellHeight
        end
        height = height + self.Table.padding.y + self.m_SubTitleLabelHeight
    end
    height = height + self.Table.padding.y
    local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    local virtualScreenHeight = Screen.height * scale
    self.Background.height = math.min(height,virtualScreenHeight * 0.7)
    self.Background:ResizeCollider()

    self:CancelTick()
    self.m_Tick = RegisterTickOnce(function ()
        for TemplateId,list in pairs(LuaAuctionMgr.m_PaimaiMySystemItems) do
            local go = NGUITools.AddChild(self.Table.gameObject, self.Template.gameObject)
            go:SetActive(true)
    
            local itemData = Item_Item.GetData(TemplateId)
            if not itemData then -- 不是物品，查询装备
                itemData = EquipmentTemplate_Equip.GetData(TemplateId)
            end
    
            local titleLabel = go.transform:Find("TitleLabel"):GetComponent(typeof(UILabel))
            local grid = go.transform:Find("Grid"):GetComponent(typeof(UIGrid))
            local subTemplate = go.transform:Find("SubTemplate")
            subTemplate.gameObject:SetActive(false)
            titleLabel.text = itemData.Name
            table.sort(list, function (a, b)
                return a.Price < b.Price
            end)
            for i = 1, #list do
                local data = list[i]
                local obj = NGUITools.AddChild(grid.gameObject, subTemplate.gameObject)
                obj:SetActive(true)
    
                local priceLabel = obj.transform:Find("PriceLabel"):GetComponent(typeof(UILabel))
                local numLabel = obj.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
    
                priceLabel.text = SafeStringFormat3(LocalString.GetString("单价%s"), data.Price)
                numLabel.text = SafeStringFormat3(LocalString.GetString("数量%s"), data.ItemCount)
            end
            
            grid:Reposition()
        end
        self.ScrollView.gameObject:SetActive(true)
        self.Table:Reposition()
        self.ScrollView:ResetPosition()
        self.ScrollViewIndicator.gameObject:SetActive(false)
        self.ScrollViewIndicator.gameObject:SetActive(true)
    end,100)
end

function LuaMyAuctionWnd:CancelTick()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick) 
        self.m_Tick = nil
    end
end

--@region UIEvent

--@endregion UIEvent

