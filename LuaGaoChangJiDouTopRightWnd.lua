local CTopAndRightTipWnd = import "L10.UI.CTopAndRightTipWnd"
local CMainCamera = import "L10.Engine.CMainCamera"
local CCommonUIFxWnd = import "L10.UI.CCommonUIFxWnd"

LuaGaoChangJiDouTopRightWnd = class()

RegistClassMember(LuaGaoChangJiDouTopRightWnd, "ExpandButton")
RegistClassMember(LuaGaoChangJiDouTopRightWnd, "ScoreLabel")
RegistClassMember(LuaGaoChangJiDouTopRightWnd, "RankLabel")
RegistClassMember(LuaGaoChangJiDouTopRightWnd, "ChestRemain")
RegistClassMember(LuaGaoChangJiDouTopRightWnd, "MonsterRemain")


function LuaGaoChangJiDouTopRightWnd:Awake()
    self.ExpandButton = self.transform:Find("Anchor/Offset/ExpandButton").gameObject
    self.ScoreLabel = self.transform:Find("Anchor/Offset/Panel/ScoreRank/Score/Label"):GetComponent(typeof(UILabel))
    self.RankLabel = self.transform:Find("Anchor/Offset/Panel/ScoreRank/Rank/Label"):GetComponent(typeof(UILabel))
    self.ChestRemain = self.transform:Find("Anchor/Offset/Panel/Remaining/Chest/Remain/Label"):GetComponent(typeof(UILabel))
    self.MonsterRemain = self.transform:Find("Anchor/Offset/Panel/Remaining/Monster/Remain/Label"):GetComponent(typeof(UILabel))

    UIEventListener.Get(self.ExpandButton).onClick = LuaUtils.VoidDelegate(function (go)
	    self:OnExpandButtonClick()
	end)
end

function LuaGaoChangJiDouTopRightWnd:Init()
    self.transform:GetComponent(typeof(UIPanel)).depth = 1
end

function LuaGaoChangJiDouTopRightWnd:OnEnable()
    self:SyncGaoChangJiDouCrossPlayInfo()
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("SyncGaoChangJiDouCrossPlayInfo", self, "SyncGaoChangJiDouCrossPlayInfo")
    g_ScriptEvent:AddListener("GaoChangJiDouOnPlayerPick", self, "GaoChangJiDouOnPlayerPick")
end

function LuaGaoChangJiDouTopRightWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("SyncGaoChangJiDouCrossPlayInfo", self, "SyncGaoChangJiDouCrossPlayInfo")
    g_ScriptEvent:RemoveListener("GaoChangJiDouOnPlayerPick", self, "GaoChangJiDouOnPlayerPick")
end

function LuaGaoChangJiDouTopRightWnd:GaoChangJiDouOnPlayerPick(pos)
    local moveTime = 1
    local screenPos = CMainCamera.Main:WorldToScreenPoint(pos)
    screenPos.z = 0
    local nguiWorldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)
    CCommonUIFxWnd.Instance:PlayLineAni(nguiWorldPos, self.ScoreLabel.transform.position, moveTime, DelegateFactory.Action(function()end))
end

function LuaGaoChangJiDouTopRightWnd:OnExpandButtonClick()
    LuaUtils.SetLocalRotation(self.ExpandButton.transform, 0, 0, 0)
	CTopAndRightTipWnd.showPackage = false
	CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

function LuaGaoChangJiDouTopRightWnd:OnHideTopAndRightTipWnd()
    self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
end

function LuaGaoChangJiDouTopRightWnd:SyncGaoChangJiDouCrossPlayInfo()
    self.ScoreLabel.text = LuaGaoChangJiDouMgr.MyInfo.score or 0
    self.RankLabel.text = LuaGaoChangJiDouMgr.MyInfo.rank and LuaGaoChangJiDouMgr.PlayerNum > 0 and LuaGaoChangJiDouMgr.MyInfo.rank.."/"..LuaGaoChangJiDouMgr.PlayerNum or "0/0"
    self.ChestRemain.text = LuaGaoChangJiDouMgr.PickNum or 0
    self.MonsterRemain.text = LuaGaoChangJiDouMgr.MonsterNum or 0
end