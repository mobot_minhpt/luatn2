local Extensions = import "Extensions"
local UISlider = import "UISlider"

LuaDuiDuiLePlayInfos = class()

RegistChildComponent(LuaDuiDuiLePlayInfos,"m_TeamTotalScoreLabel","TeamTotalScoreLabel", UILabel)
RegistChildComponent(LuaDuiDuiLePlayInfos,"m_Grid","Grid", UIGrid)
RegistChildComponent(LuaDuiDuiLePlayInfos,"m_PlayerInfoTemplate","PlayerInfoTemplate", GameObject)

RegistClassMember(LuaDuiDuiLePlayInfos,"m_PlayerInfosComponents")

function LuaDuiDuiLePlayInfos:Start()
    self.m_TeamTotalScoreLabel.text = String.Format(LocalString.GetString("队伍总分 {0}"), LuaYuanXiao2020Mgr:GetTeamScores())

    Extensions.RemoveAllChildren(self.m_Grid.transform)

    self.m_PlayerInfosComponents = {}
    self.m_PlayerInfoTemplate:SetActive(false)
    self:LoadPlayerInfosData()
end

function LuaDuiDuiLePlayInfos:OnEnable()
    g_ScriptEvent:AddListener("YuanXiao2020_UpdatePlayerInfos", self, "LoadPlayerInfosData")
end

function LuaDuiDuiLePlayInfos:OnDisable()
    g_ScriptEvent:RemoveListener("YuanXiao2020_UpdatePlayerInfos", self, "LoadPlayerInfosData")
end

function LuaDuiDuiLePlayInfos:LoadPlayerInfosData(data)
    self.m_TeamTotalScoreLabel.text = String.Format(LocalString.GetString("队伍总分 {0}"), LuaYuanXiao2020Mgr:GetTeamScores())

    local playerInfos = LuaYuanXiao2020Mgr:GetPlayerInfos()

    if nil == self.m_PlayerInfosComponents then
        self.m_PlayerInfosComponents = {}
    end
    if #playerInfos > #self.m_PlayerInfosComponents then
        self:InitPlayerInfosComponents()
    end
    for i = 1,#playerInfos do
        local t = self.m_PlayerInfosComponents[i]
        t.NameLabel.text = playerInfos[i].Name
        local weight = (playerInfos[i].Weight < 0) and 0 or playerInfos[i].Weight
        t.ScoreLabel.text =  SafeStringFormat3("%.1f%s", weight, LocalString.GetString("两"))
        t.Slider.value = playerInfos[i].SliderValue
    end
end

function LuaDuiDuiLePlayInfos:InitPlayerInfosComponents()
    local playerInfos = LuaYuanXiao2020Mgr:GetPlayerInfos()
    for i = #self.m_PlayerInfosComponents + 1,#playerInfos do
        local obj = NGUITools.AddChild(self.m_Grid.gameObject, self.m_PlayerInfoTemplate)
        obj:SetActive(true)
        local t = {}
        t.NameLabel = obj.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
        t.ScoreLabel = obj.transform:Find("ScoreLabel"):GetComponent(typeof(UILabel))
        t.Slider = obj.transform:Find("ScoreLabel/Slider"):GetComponent(typeof(UISlider))
        table.insert(self.m_PlayerInfosComponents, t)
    end
    self.m_Grid:Reposition()
end