local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UIGrid = import "UIGrid"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local Extensions = import "Extensions"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local Animator = import "UnityEngine.Animator"

LuaWuLiangLevelWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaWuLiangLevelWnd, "RuleDescBtn", "RuleDescBtn", GameObject)
RegistChildComponent(LuaWuLiangLevelWnd, "PassedInfoLabel", "PassedInfoLabel", UILabel)
RegistChildComponent(LuaWuLiangLevelWnd, "LevelLabel", "LevelLabel", UILabel)
RegistChildComponent(LuaWuLiangLevelWnd, "DifficultyInfo", "DifficultyInfo", GameObject)
RegistChildComponent(LuaWuLiangLevelWnd, "ConditionLabel", "ConditionLabel", UILabel)
RegistChildComponent(LuaWuLiangLevelWnd, "RewardInfo", "RewardInfo", GameObject)
RegistChildComponent(LuaWuLiangLevelWnd, "ChallengeBtn", "ChallengeBtn", GameObject)
RegistChildComponent(LuaWuLiangLevelWnd, "PassedTeamInfoBtn", "PassedTeamInfoBtn", GameObject)
RegistChildComponent(LuaWuLiangLevelWnd, "ScrollView", "ScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaWuLiangLevelWnd, "ItemRoot", "ItemRoot", GameObject)
RegistChildComponent(LuaWuLiangLevelWnd, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaWuLiangLevelWnd, "Leifengta_zhong6", "Leifengta_zhong6", GameObject)
RegistChildComponent(LuaWuLiangLevelWnd, "Leifengta_zhong1", "Leifengta_zhong1", GameObject)
RegistChildComponent(LuaWuLiangLevelWnd, "Leifengta_di", "Leifengta_di", GameObject)
RegistChildComponent(LuaWuLiangLevelWnd, "Leifengta_ding", "Leifengta_ding", GameObject)
RegistChildComponent(LuaWuLiangLevelWnd, "Leifengta", "Leifengta", GameObject)
RegistChildComponent(LuaWuLiangLevelWnd, "BgRoot", "BgRoot", GameObject)
RegistChildComponent(LuaWuLiangLevelWnd, "PaihangbangBtn", "PaihangbangBtn", GameObject)
RegistChildComponent(LuaWuLiangLevelWnd, "FakeItemDown", "FakeItemDown", GameObject)
RegistChildComponent(LuaWuLiangLevelWnd, "FakeItemUp", "FakeItemUp", GameObject)

--@endregion RegistChildComponent end

function LuaWuLiangLevelWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.RuleDescBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleDescBtnClick()
	end)


	
	UIEventListener.Get(self.ChallengeBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChallengeBtnClick()
	end)


	
	UIEventListener.Get(self.PassedTeamInfoBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnPassedTeamInfoBtnClick()
	end)


	
	UIEventListener.Get(self.PaihangbangBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnPaihangbangBtnClick()
	end)


    --@endregion EventBind end

	local p1 = self.transform:GetComponent(typeof(UIPanel))
	local p2 = self.ScrollView.transform:GetComponent(typeof(UIPanel))
	p1.IgnoreIphoneXMargin = true
	p2.IgnoreIphoneXMargin = true

	self.transform:Find("GameObject").localPosition = Vector3(0,0,0)
end

function LuaWuLiangLevelWnd:Init()
	local p2 = self.ScrollView.transform:GetComponent(typeof(UIPanel))
	p2:ResetAndUpdateAnchors()

	self:InitTabView()

	-- 遍历找到最低没通关的
	self.m_PassedTable = LuaWuLiangMgr.passPlayIdTbl
	local level, difficult = self:GetInitLevel()

	self:SetLevelPassState()

	self.m_CurrentDifficult = difficult
	-- 设置层数
	self:SetLevel(level, true)
end

function LuaWuLiangLevelWnd:InitTabView()
	self.ItemRoot:SetActive(false)
	Extensions.RemoveAllChildren(self.Grid.transform)

	self.m_LevelItem = {}
	for i = 1, 1 do
		local fake = NGUITools.AddChild(self.Grid.gameObject, self.FakeItemUp)
		local widget = fake:GetComponent(typeof(UIWidget))
		widget.height = 300
        fake:SetActive(true)
	end

	-- 顶部
	for i = 1, 1 do
		local g = NGUITools.AddChild(self.Grid.gameObject, self.Leifengta_ding)
        g:SetActive(true)
	end

	-- 中部6-25
    for i= 25, 6, -1 do
        local g = NGUITools.AddChild(self.Grid.gameObject, self.Leifengta_zhong6)
        g:SetActive(true)

		local normal = g.transform:Find("Normal").gameObject
        UIEventListener.Get(normal).onClick = DelegateFactory.VoidDelegate(function (go)
			self:SetLevel(i)
		end)	

		local highlight = g.transform:Find("Highlight").gameObject
        UIEventListener.Get(highlight).onClick = DelegateFactory.VoidDelegate(function (go)
			self:SetLevel(i)
		end)

		self.m_LevelItem[i] = g
    end

	-- 新手关
    for i=5, 2, -1 do
        local g = NGUITools.AddChild(self.Grid.gameObject, self.Leifengta_zhong1)

		UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function (go)
			self:SetLevel(i)
		end)
        g:SetActive(true)
		self.m_LevelItem[i] = g

		self:SetLevelHightLight(i, false)
    end

	-- 底部
	for i = 1, 1 do
		local g = NGUITools.AddChild(self.Grid.gameObject, self.Leifengta_di)

		UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function (go)
			self:SetLevel(i)
		end)

        g:SetActive(true)
		self.m_LevelItem[i] = g
	end
    self.Grid:Reposition()
    self.ScrollView:AlignToEnd()
end

-- 获取初始楼层
function LuaWuLiangLevelWnd:GetInitLevel()

	local sum = 0
	local pass = 0
	for i =1, 25 do
		for j = 1, 3 do
			local data = XinBaiLianDong_WuLiangPlay.GetData(i*100 + j)
			if data then
				if self.m_PassedTable[data.GameplayId] then
					pass = pass + 1
				end
				sum = sum + 1
			end
		end
	end
	self.PassedInfoLabel.text = pass.."/".. sum
	
	for j =1, 3 do
		for i = 1, 25 do
			local data = XinBaiLianDong_WuLiangPlay.GetData(i*100 + j)
			if data then
				if not self.m_PassedTable[data.GameplayId] then
					return i,j
				end
			end
		end
	end

	-- 全部通关了
	return 25,3
end

-- 设置楼层的通关信息
function LuaWuLiangLevelWnd:SetLevelPassState()
	for i = 2, 5 do
		local item = self.m_LevelItem[i]
		local h1 = item.transform:Find("men1/men1_highlight")
		local h2 = item.transform:Find("Leifengta_zhong1_highlight/men1/men1_highlight")

		if self:HasDoorPassed(i,1) then
			h1.gameObject:SetActive(true)
			h2.gameObject:SetActive(true)
		else
			h1.gameObject:SetActive(false)
			h2.gameObject:SetActive(false)
		end
	end

	for i = 6, 25 do
		local item = self.m_LevelItem[i]

		local h1List = {}
		table.insert(h1List, item.transform:Find("Highlight/men1/men1_highlight"))
		table.insert(h1List, item.transform:Find("Highlight/men2/men1_highlight"))
		table.insert(h1List, item.transform:Find("Highlight/men3/men1_highlight"))

		local h2List = {}
		table.insert(h2List, item.transform:Find("Normal/men1/men1_highlight"))
		table.insert(h2List, item.transform:Find("Normal/men2/men1_highlight"))
		table.insert(h2List, item.transform:Find("Normal/men3/men1_highlight"))


		for j=1,3 do
			if self:HasDoorPassed(i,j) then
				h1List[j].gameObject:SetActive(true)
				h2List[j].gameObject:SetActive(true)
			else
				h1List[j].gameObject:SetActive(false)
				h2List[j].gameObject:SetActive(false)
			end
		end
		
	end
end

-- 设置楼层的高亮信息
function LuaWuLiangLevelWnd:SetLevelHightLight(level, isHightLight)
	if not level then
		return level
	end

	local item = self.m_LevelItem[level]

	if level >=6 and level <= 25 then
		local ani = item:GetComponent(typeof(Animator))
		ani:SetBool("IsOpen", isHightLight)
	elseif level <=5 and level >= 2 then
		local h = item.transform:Find("Leifengta_zhong1_highlight").gameObject
		h:SetActive(isHightLight)

		local ani = item:GetComponent(typeof(Animator))
		ani:SetBool("IsOpen", isHightLight)
	elseif level == 1 then
		
	end
end

-- 查询是否通关
function LuaWuLiangLevelWnd:HasDoorPassed(level ,diff)
	local data = XinBaiLianDong_WuLiangPlay.GetData(level*100 + diff)
	if data then
		if self.m_PassedTable[data.GameplayId] then
			return true
		else
			return false
		end
	end

	return false
end

-- 更新背景
function LuaWuLiangLevelWnd:Update()
	local posY = self.ScrollView.transform.localPosition.y

	local targetY = (posY - 4500) / 2.1

	self.BgRoot.transform.localPosition = Vector3(0, targetY, 0)
end

function LuaWuLiangLevelWnd:SetLevel(level, force)
	-- 当前层数
	if self.m_CurrentLevel and self.m_CurrentLevel == level then
		return
	elseif self.m_CurrentLevel then
		self:SetLevelHightLight(self.m_CurrentLevel, false)
	end

	local lvItem = self.m_LevelItem[level]

	if lvItem and force then
		CUICommonDef.SetFullyVisible(lvItem.gameObject, self.ScrollView)
	end

	self.m_CurrentLevel = level
	self:SetLevelHightLight(self.m_CurrentLevel, true)

	self.LevelLabel.text = SafeStringFormat3(LocalString.GetString("第%s层"), Extensions.ConvertToChineseString(level))

	local label = self.ChallengeBtn.transform:Find("Label"):GetComponent(typeof(UILabel))
	label.text = SafeStringFormat3(LocalString.GetString("进入第%s层"), Extensions.ConvertToChineseString(level))

	-- 设置难度选中
	for i = 1, 3 do
		local item = self.DifficultyInfo.transform:Find(tostring(i)).gameObject

		-- 小于5层 只有一个难度
		if level <= 5 and i >= 2 then
			item:SetActive(false)
		else
			item:SetActive(true)
		end

		-- 设置回调
		UIEventListener.Get(item).onClick = DelegateFactory.VoidDelegate(function (go)
			self:SetDifficult(i)
		end)
	end

	-- 设置当前难度
	if self.m_CurrentDifficult > 1 and self.m_CurrentLevel <=5 then
		self.m_CurrentDifficult = 1
	end
	self:SetDifficult(self.m_CurrentDifficult)
end

function LuaWuLiangLevelWnd:SetDifficult(difficult)
	-- 当前难度
	self.m_CurrentDifficult = difficult

	-- 设置高亮显示
	for i=1, 3 do
		local item = self.DifficultyInfo.transform:Find(tostring(i))
		local fx = item:Find("fx_nandu")

		local ani = item:GetComponent(typeof(Animator))
		ani.enabled = true
		ani:SetBool("IsOpen", difficult == i)
	end

	local data = XinBaiLianDong_WuLiangPlay.GetData(self.m_CurrentLevel*100 + difficult)
	
	-- 设置奖励物品
	local rewardList = data.ClientRewardItemId
	for i=1, 3 do
		local item = self.RewardInfo.transform:Find(tostring(i))
		local icon = item:Find("IconTexture"):GetComponent(typeof(CUITexture))

		if i <= rewardList.Length then
			item.gameObject:SetActive(true)
			local itemId = rewardList[i-1]
			local itemData = Item_Item.GetData(itemId)

			icon:LoadMaterial(itemData.Icon)

			UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
				CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
			end)
		else
			item.gameObject:SetActive(false)
		end
	end

	-- 设置前置玩法
	local desStr = LocalString.GetString("通关")
	local prePlayId = data.PrevPlay
	local difficultStr = {LocalString.GetString("苦境"), LocalString.GetString("集境"), LocalString.GetString("灭境")}

	local isUnLock = true
	if prePlayId then
		for i=0, prePlayId.Length-1 do
			local playId = prePlayId[i]
			local id = XinBaiLianDong_WuLiangPlay.GetDataBySubKey("GameplayId", playId).Id
			
			local level = math.floor(id/100)
			local difficult = id % 100

			if not self:HasDoorPassed(level, difficult) then
				isUnLock = false
			end

			local levelStr = SafeStringFormat3(LocalString.GetString("第%s层"), Extensions.ConvertToChineseString(level))
			desStr = SafeStringFormat3(LocalString.GetString("%s %s·%s"), desStr, difficultStr[difficult], levelStr)
		end
	end

	if desStr == LocalString.GetString("通关") then
		desStr = LocalString.GetString("无")
	end

	

	local desLabel = self.ConditionLabel.transform:Find("Label"):GetComponent(typeof(UILabel))

	if self:HasDoorPassed(self.m_CurrentLevel , self.m_CurrentDifficult) then
		-- 已通关
		desLabel.text = LocalString.GetString("已通关")
		self.ConditionLabel.text = ""
	elseif isUnLock then
		desLabel.text = LocalString.GetString("已解锁")
		self.ConditionLabel.text = ""
	else
		desLabel.text = LocalString.GetString("解锁条件")
		self.ConditionLabel.text = desStr
	end
end

function LuaWuLiangLevelWnd:GetGuideGo(methodName)
    if methodName == "GetLevel2" then
        if self.m_LevelItem and self.m_LevelItem[2] then
			local g = self.m_LevelItem[2].gameObject
        	return g
		end
    elseif methodName == "GetEnterBtn" then
        return self.ChallengeBtn
	end
	return nil
end

--@region UIEvent

function LuaWuLiangLevelWnd:OnRuleDescBtnClick()
	g_MessageMgr:ShowMessage("WuLiang_Level_Wnd_Desc")
end

function LuaWuLiangLevelWnd:OnChallengeBtnClick()
	-- 从准备场景来查找
	local data = XinBaiLianDong_WuLiangPrepare.GetData(self.m_CurrentLevel)
	Gac2Gas.WuLiangShenJing_EnterPrepare(data.GameplayId)
end

function LuaWuLiangLevelWnd:OnPassedTeamInfoBtnClick()
	local level = self.m_CurrentLevel*100 + self.m_CurrentDifficult
	LuaWuLiangMgr:OpenPassedTeamInfoWnd(level)
end

function LuaWuLiangLevelWnd:OnPaihangbangBtnClick()
	CUIManager.ShowUI(CLuaUIResources.WuLiangRankWnd)
end


--@endregion UIEvent

