local CUIResources = import "L10.UI.CUIResources"
local CScene = import "L10.Game.CScene"
local WindowsShortCutKey = import "L10.Engine.WindowsShortCutKey"
local CCityWarMgr = import "L10.Game.CCityWarMgr"
if CommonDefs.Is_UNITY_STANDALONE_WIN() and CommonDefs.Is_PC_PLATFORM() and (not CommonDefs.Is_L10_Marketing())then
    WindowsShortCutKey.m_hookOpenMinimapPopWnd = function (this)
        if CCityWarMgr.Inst:TryOpenMap() then return end
        if CScene.MainScene then
            local gamePlayId = CScene.MainScene.GamePlayDesignId
            if gamePlayId == 51101465 then return end
            if gamePlayId == DaFuWeng_Setting.GetData().GamePlayId then return end
        end
        CUIManager.ShowOrCloseUI(CUIResources.MiniMapPopWnd)
    end
end
