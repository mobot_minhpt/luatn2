local QnTableView = import "L10.UI.QnTableView"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UITabBar = import "L10.UI.UITabBar"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local UIGrid = import "UIGrid"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local AlignType = import "CPlayerInfoMgr+AlignType"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CChatHelper = import "L10.UI.CChatHelper"
local CGroupIMMgr = import "L10.Game.CGroupIMMgr"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Animator = import "UnityEngine.Animator"
local NativeTools = import "L10.Engine.NativeTools"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local CShiTuMgr = import "L10.Game.CShiTuMgr"

LuaShiTuDirectoryWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShiTuDirectoryWnd, "Tabs", "Tabs", UITabBar)
RegistChildComponent(LuaShiTuDirectoryWnd, "RightBtnsView", "RightBtnsView", GameObject)
RegistChildComponent(LuaShiTuDirectoryWnd, "ShareButton", "ShareButton", GameObject)
RegistChildComponent(LuaShiTuDirectoryWnd, "ChatButton", "ChatButton", GameObject)
RegistChildComponent(LuaShiTuDirectoryWnd, "BottomLabel", "BottomLabel", UILabel)
RegistChildComponent(LuaShiTuDirectoryWnd, "FirstView", "FirstView", GameObject)
RegistChildComponent(LuaShiTuDirectoryWnd, "SecondView", "SecondView", GameObject)
RegistChildComponent(LuaShiTuDirectoryWnd, "BigHeadTemplate", "BigHeadTemplate", GameObject)
RegistChildComponent(LuaShiTuDirectoryWnd, "SmallHeadTemplate", "SmallHeadTemplate", GameObject)
RegistChildComponent(LuaShiTuDirectoryWnd, "PageGrid", "PageGrid", UIGrid)
RegistChildComponent(LuaShiTuDirectoryWnd, "PageTemplate", "PageTemplate", GameObject)
RegistChildComponent(LuaShiTuDirectoryWnd, "PageTemplate_wu", "PageTemplate_wu", GameObject)
RegistChildComponent(LuaShiTuDirectoryWnd, "TableView", "TableView", QnTableView)

--@endregion RegistChildComponent end
RegistClassMember(LuaShiTuDirectoryWnd, "m_HanZhangJiData")
RegistClassMember(LuaShiTuDirectoryWnd, "m_IsTudiView")
RegistClassMember(LuaShiTuDirectoryWnd, "m_Animator")
RegistClassMember(LuaShiTuDirectoryWnd, "m_IsInit")
RegistClassMember(LuaShiTuDirectoryWnd, "m_IsCreatingShiTuGroupIm")

function LuaShiTuDirectoryWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ShareButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareButtonClick()
	end)


	
	UIEventListener.Get(self.ChatButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChatButtonClick()
	end)


    --@endregion EventBind end
end

function LuaShiTuDirectoryWnd:OnEnable()
	g_ScriptEvent:AddListener("SendShiMenQiuXuePuInfo", self, "OnSendShiMenQiuXuePuInfo")
	g_ScriptEvent:AddListener("SendShiMenHanZhangJiInfo", self, "OnSendShiMenHanZhangJiInfo")
	g_ScriptEvent:AddListener("ModifyChuShiJiYuSucc", self, "OnModifyChuShiJiYuSucc")
	g_ScriptEvent:AddListener("MainPlayerJoinGroupIM", self, "OnMainPlayerJoinGroupIM")
end

function LuaShiTuDirectoryWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SendShiMenQiuXuePuInfo", self, "OnSendShiMenQiuXuePuInfo")
	g_ScriptEvent:RemoveListener("SendShiMenHanZhangJiInfo", self, "OnSendShiMenHanZhangJiInfo")
	g_ScriptEvent:RemoveListener("ModifyChuShiJiYuSucc", self, "OnModifyChuShiJiYuSucc")
	g_ScriptEvent:RemoveListener("MainPlayerJoinGroupIM", self, "OnMainPlayerJoinGroupIM")
end

function LuaShiTuDirectoryWnd:Init()
	self.m_IsTudiView = LuaShiTuMgr.m_ShiTuMainWndTabIndex == 0 
	self.Tabs.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
	    self:OnTabChange(index)
	end)
	self.Tabs:ChangeTab(0, false)
	self.BottomLabel.text = g_MessageMgr:FormatMessage("ShiTuDirectoryWnd_BottomLabel")
	self.ShareButton.gameObject:SetActive(not LuaShiTuMgr.s_ShowOtherPlayerDirectoryWnd and not CommonDefs.IS_VN_CLIENT)
	self.ChatButton.gameObject:SetActive(not LuaShiTuMgr.s_ShowOtherPlayerDirectoryWnd)
	self.m_Animator = self.transform:GetComponent(typeof(Animator))
	self:InitFirstView()
	self:InitSecondView()
end

function LuaShiTuDirectoryWnd:OnMainPlayerJoinGroupIM(args)
    local id = args[0]
    local im = CClientMainPlayer.Inst and CClientMainPlayer.Inst.RelationshipProp.JoinedGroupIM[id]
    if im and im.Type == CGroupIMMgr.GroupIMType_ShiTu and self.m_IsCreatingShiTuGroupIm then
        CChatHelper.ShowGroupChat(id)
        self.m_IsCreatingShiTuGroupIm = false
    end
end

function LuaShiTuDirectoryWnd:OnSendShiMenQiuXuePuInfo(myIndex1, myIndex2)
	if self.m_IsInit then
		self.m_Animator:Play("shitudirectorywnd_firstview_tab_open")
	end
	self.m_IsInit = true
	local topLabelTextArr = {LocalString.GetString("第一代"),LocalString.GetString("第二代"),LocalString.GetString("第三代")}
	local mainPersonalTitleArr = {LocalString.GetString("师公"),LocalString.GetString("师父")}
	if myIndex1 == 2 then
		table.remove(mainPersonalTitleArr,1)
	end
	Extensions.RemoveAllChildren(self.PageGrid.transform)
	local maxIndex = #LuaShiTuMgr.m_QiuXuePuInfo
	for i = 1,#LuaShiTuMgr.m_QiuXuePuInfo do
		local obj = NGUITools.AddChild(self.PageGrid.gameObject, self.PageTemplate.gameObject)
		obj.gameObject:SetActive(true)
		local topLabel = obj.transform:Find("TopLabel"):GetComponent(typeof(UILabel))
		local childGrid = obj.transform:Find("ChildGrid"):GetComponent(typeof(UIGrid))
		local waiMenRoot = obj.transform:Find("WaiMenRoot")
		local line = obj.transform:Find("Line"):GetComponent(typeof(UIWidget))

		topLabel.text = topLabelTextArr[i]
		Extensions.RemoveAllChildren(childGrid.transform)
		local isLastObj = i == maxIndex
		waiMenRoot.gameObject:SetActive(false)	

		local num = 0
		local t = LuaShiTuMgr.m_QiuXuePuInfo[i]
		if t.mainId then
			local bigObj = NGUITools.AddChild(childGrid.gameObject, self.BigHeadTemplate.gameObject)
			self:InitBigHeadTemplate(bigObj, t.relationInfo, t.mainId)
			local titleLabel = bigObj.transform:Find("TitleLabel"):GetComponent(typeof(UILabel))
			local rLine = bigObj.transform:Find("RLine")
			local lLine = bigObj.transform:Find("LLine")
			local myLabel = bigObj.transform:Find("MyLabel")
			titleLabel.text = (CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == t.mainId) and LocalString.GetString("我") or mainPersonalTitleArr[i]
			rLine.gameObject:SetActive(not isLastObj)
			lLine.gameObject:SetActive(i > 1)
			myLabel.gameObject:SetActive(CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == t.mainId)
			titleLabel.gameObject:SetActive(CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id ~= t.mainId)
			num = num + 1
		end

		if t.othertudiInfo then
			for j = 1,#t.othertudiInfo do
				local tudiId, tudiInfo, title, isWaiMen  = t.othertudiInfo[j].tudiId, t.othertudiInfo[j].tudiInfo, t.othertudiInfo[j].title, t.othertudiInfo[j].isWaiMen
				if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == tudiId and not isWaiMen and t.mainId == nil then

					local relationInfo = {}
					relationInfo.shifuName = CClientMainPlayer.Inst.Name
					relationInfo.shifuClass = EnumToInt(CClientMainPlayer.Inst.Class)
					relationInfo.shifuGender = EnumToInt(CClientMainPlayer.Inst.Gender)
					if CClientMainPlayer.Inst.RelationshipProp.WeddingInfo then
						local weddingInfo = CClientMainPlayer.Inst.RelationshipProp:GetBasicInfo(CClientMainPlayer.Inst.RelationshipProp.WeddingInfo.PartnerId)
						relationInfo.xialvId = weddingInfo.ID
						relationInfo.xialvName = weddingInfo.Name
						relationInfo.xialvClass = weddingInfo.Class
						relationInfo.xialvGender = weddingInfo.Gender
					end

					local bigObj = NGUITools.AddChild(childGrid.gameObject, self.BigHeadTemplate.gameObject)
					self:InitBigHeadTemplate(bigObj, relationInfo, CClientMainPlayer.Inst.Id)
					local titleLabel = bigObj.transform:Find("TitleLabel"):GetComponent(typeof(UILabel))
					local rLine = bigObj.transform:Find("RLine")
					local lLine = bigObj.transform:Find("LLine")
					local myLabel = bigObj.transform:Find("MyLabel")
					titleLabel.text = LocalString.GetString("我")
					rLine.gameObject:SetActive(not isLastObj)
					lLine.gameObject:SetActive(i > 1)
					myLabel.gameObject:SetActive(true)
					titleLabel.gameObject:SetActive(false)
					num = num + 1
				end
			end
			for j = 1,#t.othertudiInfo do
				local tudiId, tudiInfo, title, isWaiMen = t.othertudiInfo[j].tudiId, t.othertudiInfo[j].tudiInfo, t.othertudiInfo[j].title, t.othertudiInfo[j].isWaiMen
				if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id ~= tudiId and t.mainId ~=tudiId then
					if not isWaiMen then
						local smallObj = NGUITools.AddChild(childGrid.gameObject, self.SmallHeadTemplate.gameObject)
						self:InitSmallHeadTemplate(smallObj, tudiInfo, tudiId, title)
						num = num + 1
					end
				end
			end
			if isLastObj then
				waiMenRoot.gameObject:SetActive(true)	
				self:InitWaiMenList(waiMenRoot, t.othertudiInfo)
			end
		end
		
		line.width = childGrid.cellHeight * (num - 1)
		childGrid:Reposition()
	end
	local memberSerialNumber = ShiTu_Setting.GetData().MemberSerialNumber
	for i = (maxIndex + 1), 3 do
		local obj = NGUITools.AddChild(self.PageGrid.gameObject, self.PageTemplate_wu.gameObject)
		obj.transform:Find("Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("暂无第%s代"),memberSerialNumber[i -1]) 
		obj.gameObject:SetActive(true)
	end
	self.PageGrid.hideInactive = true
	self.PageGrid:Reposition()
end

function LuaShiTuDirectoryWnd:OnSendShiMenHanZhangJiInfo()
	self.m_Animator:Play("shitudirectorywnd_secondview_tab_open")
	self.m_HanZhangJiData = {}
	for tudiId, tudiInfo in pairs(LuaShiTuMgr.m_HanZhangJiInfo) do
		table.insert(self.m_HanZhangJiData, {info = LuaShiTuMgr.m_HanZhangJiInfo[tudiId], tudiId = tudiId})
  	end
	table.sort(self.m_HanZhangJiData,function (a,b)
		return a.info.beTudiTime < b.info.beTudiTime
	end)
	self.TableView:ReloadData(true, true)
end

function LuaShiTuDirectoryWnd:OnModifyChuShiJiYuSucc(tudiId, jiyuId)
	for i = 1,#self.m_HanZhangJiData do
		local t = self.m_HanZhangJiData[i]
		if t.tudiId == tudiId then
			self.m_HanZhangJiData[i].info.jiyuId = jiyuId
		end
	end
	self.TableView:ReloadData(true, true)
end

function LuaShiTuDirectoryWnd:InitFirstView()
	self.BigHeadTemplate.gameObject:SetActive(false)
	self.SmallHeadTemplate.gameObject:SetActive(false)
	self.PageTemplate.gameObject:SetActive(false)
	Extensions.RemoveAllChildren(self.PageGrid.transform)
end

function LuaShiTuDirectoryWnd:InitWaiMenList(waiMenRoot, othertudiInfo)
	local portraitTemplate = waiMenRoot.transform:Find("PortraitTemplate")
	local waiMenGrid = waiMenRoot.transform:Find("WaiMenGrid"):GetComponent(typeof(UIGrid))

	portraitTemplate.gameObject:SetActive(false)
	Extensions.RemoveAllChildren(waiMenGrid.transform)

	local num = 0
	for i = 1,#othertudiInfo do
		local tudiId, tudiInfo, title, isWaiMen = othertudiInfo[i].tudiId, othertudiInfo[i].tudiInfo, othertudiInfo[i].title, othertudiInfo[i].isWaiMen
		if isWaiMen then
			local obj = NGUITools.AddChild(waiMenGrid.gameObject, portraitTemplate.gameObject)
			obj.gameObject:SetActive(true)
			local portrait = obj:GetComponent(typeof(CUITexture))
			portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName( othertudiInfo[i].tudiClass, othertudiInfo[i].tudiGender, -1), false)
			local my = obj.transform:Find("My")
			my.gameObject:SetActive(CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == tudiId)
			UIEventListener.Get(portrait.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
				CPlayerInfoMgr.ShowPlayerPopupMenu(tudiId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
			end)
			num = num + 1
		end
	end
	waiMenRoot.gameObject:SetActive(num > 0)
	
	waiMenGrid:Reposition()
end

function LuaShiTuDirectoryWnd:InitBigHeadTemplate(obj, relationInfo, playerId)
	obj.gameObject:SetActive(true)

	local portrait = obj.transform:Find("Portrait"):GetComponent(typeof(CUITexture))
	local nameLabel = obj.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
	local partnerHead = obj.transform:Find("PartnerHead")
	local partnerHeadPortrait = obj.transform:Find("PartnerHead/Portrait"):GetComponent(typeof(CUITexture))
	--local partnerHeadNoneLabel = obj.transform:Find("PartnerHead/NoneLabel")

	local shifuName = relationInfo.shifuName
	local shifuClass = relationInfo.shifuClass
	local shifuGender = relationInfo.shifuGender
	local xiaLvId = relationInfo.xialvId
	--local xiaLvName = relationInfo.xialvName
	local xialvClass = relationInfo.xialvClass
	local xialvGender = relationInfo.xialvGender

	portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(shifuClass, shifuGender, -1), false)
	nameLabel.text = shifuName
	partnerHead.gameObject:SetActive(xiaLvId and xiaLvId > 0 and CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id ~= playerId)
	--partnerHeadNoneLabel.gameObject:SetActive(not xiaLvId or xiaLvId == 0)
	if xiaLvId then
		partnerHeadPortrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(xialvClass, xialvGender, -1), false)
	end
	UIEventListener.Get(portrait.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
	end)
	UIEventListener.Get(partnerHeadPortrait.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    CPlayerInfoMgr.ShowPlayerPopupMenu(xiaLvId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
	end)
end

function LuaShiTuDirectoryWnd:InitSmallHeadTemplate(obj, tudiInfo, playerId, title)
	obj.gameObject:SetActive(true)

	local portrait = obj.transform:Find("Portrait"):GetComponent(typeof(CUITexture))
	local titleLabel = obj.transform:Find("TitleLabel"):GetComponent(typeof(UILabel))
	local nameLabel = obj.transform:Find("NameLabel"):GetComponent(typeof(UILabel))

	local tudiName = tudiInfo.tudiName
	local tudiClass = tudiInfo.tudiClass
	local tudiGender = tudiInfo.tudiGender
	local beTudiTime = tudiInfo.beTudiTime

	portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(tudiClass, tudiGender, -1), false)
	nameLabel.text = tudiName
	titleLabel.text = title

	UIEventListener.Get(portrait.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
	end)
end

function LuaShiTuDirectoryWnd:InitSecondView()
	self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return self.m_HanZhangJiData and #self.m_HanZhangJiData or 0
        end,
        function(item,index)
            self:InitItem(item,index)
        end)
	self.TableView:ReloadData(true, true)
end

function LuaShiTuDirectoryWnd:InitItem(item,index)
	local chushiRoot = item.transform:Find("ChuShi")
	local titleLabel = item.transform:Find("TitleLabel"):GetComponent(typeof(UILabel))
	local portrait = item.transform:Find("Portrait"):GetComponent(typeof(CUITexture))
	local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
	local timeLabel = item.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
	local desLabel = item.transform:Find("DesLabel"):GetComponent(typeof(UILabel))
	local notChuShiRoot = item.transform:Find("NotChuShi")
	local itemCell = item.transform:Find("ItemCell")
	local itemIconTexture = item.transform:Find("ItemCell/IconTexture"):GetComponent(typeof(CUITexture))
	local selectDesLabel = item.transform:Find("SelectDesLabel"):GetComponent(typeof(UILabel))
	local bg = item.transform:Find("common_bg_white_Sprite"):GetComponent(typeof(UISprite))
	local myLabel = item.transform:Find("MyLabel")

	local tudiInfo = self.m_HanZhangJiData[index + 1].info
	local tudiId = self.m_HanZhangJiData[index + 1].tudiId
	local tudiName = tudiInfo.tudiName
	local tudiClass = tudiInfo.tudiClass
	local tudiGender = tudiInfo.tudiGender
	local beTudiTime = tudiInfo.beTudiTime
	local isChuShi = tudiInfo.isChuShi
	local jiyuId = tudiInfo.jiyuId
	local chushiTime = tudiInfo.chushiTime

	local chuShiLiData =  ShiTu_ChuShiLi.GetData(tudiInfo.giftId)
	local giftItemId = chuShiLiData and chuShiLiData.GiftItemId or 0
	myLabel.gameObject:SetActive(CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == tudiId)
	titleLabel.gameObject:SetActive(CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id ~= tudiId)

	local itemData = Item_Item.GetData(giftItemId)
	chushiRoot.gameObject:SetActive(isChuShi)
	notChuShiRoot.gameObject:SetActive(not isChuShi)
	if index <= 16 then
		local indexText = ShiTu_Setting.GetData().MemberSerialNumber[index]
		titleLabel.text = SafeStringFormat3(LocalString.GetString("%s徒弟"),indexText) 
	elseif index == 1 then
		titleLabel.text = LocalString.GetString("大徒弟")
	else
		titleLabel.text = LocalString.GetString("小徒弟")
	end
	local data = ShiTu_ChuShiJiYu.GetData(jiyuId)
	desLabel.text = data and data.Content or ""
	selectDesLabel.transform:GetChild(0).gameObject:SetActive(data == nil)
	portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(tudiClass, tudiGender, -1), false)
	nameLabel.text = tudiName
	timeLabel.text = SafeStringFormat3(LocalString.GetString("%s-%s"),CServerTimeMgr.ConvertTimeStampToZone8Time(beTudiTime):ToString("yyyy.MM.dd"),
	isChuShi and CServerTimeMgr.ConvertTimeStampToZone8Time(chushiTime):ToString("yyyy.MM.dd") or LocalString.GetString("至今")) 

	itemIconTexture.gameObject:SetActive(itemData)
	if itemData then
		itemIconTexture:LoadMaterial(itemData.Icon)
	end
	itemCell.gameObject:SetActive(itemData)
	UIEventListener.Get(itemCell.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    CItemInfoMgr.ShowLinkItemTemplateInfo(giftItemId)
	end)
	UIEventListener.Get(selectDesLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		LuaShiTuMgr.m_ShiTuChooseJiYuWnd_TuDiId = self.m_HanZhangJiData[index + 1].tudiId
		CUIManager.ShowUI(CLuaUIResources.ShiTuChooseJiYuWnd)
	end)
	UIEventListener.Get(portrait.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
		CPlayerInfoMgr.ShowPlayerPopupMenu(tudiId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
	end)
	selectDesLabel.gameObject:SetActive(not self.m_IsTudiView)
	selectDesLabel.text = (data == nil) and LocalString.GetString("点击输入寄语") or ""
	-- bg.color = NGUIText.ParseColor24((index % 2 == 1) and "faf4e9" or "ffffff",0) 
end

--@region UIEvent

function LuaShiTuDirectoryWnd:OnTabChange(index)
	self.FirstView.gameObject:SetActive(index == 0)
	self.SecondView.gameObject:SetActive(index == 1)
	if index == 0 then
		Gac2Gas.RequestShiMenQiuXuePuInfo(not self.m_IsTudiView)
	else
		Gac2Gas.RequestShiMenHanZhangJiInfo(not self.m_IsTudiView)
	end
end

function LuaShiTuDirectoryWnd:OnShareButtonClick()
	self.ChatButton:SetActive(false)
	self.ShareButton:SetActive(false)
	local closeBtn = self.transform:Find("CloseButton").gameObject
	closeBtn:SetActive(false)
	CTickMgr.Register(DelegateFactory.Action(function ()
		local filename = Utility.persistentDataPath .. "/screenshot.jpg"
		local data = CUICommonDef.DoCaptureScreen(filename, true, true, false)
		NativeTools.SaveImage(data)
		CTickMgr.Register(DelegateFactory.Action(function ()
			ShareBoxMgr.ImagePath = filename
			ShareBoxMgr.ShareType = EShareType.ShareChatPanelImage2
			CUIManager.ShowUI(CUIResources.ShareBox3)
			self.ChatButton.gameObject:SetActive(true)
			self.ShareButton.gameObject:SetActive(true)
			closeBtn:SetActive(true)
		end), 1000, ETickType.Once)
	end), 200, ETickType.Once)
end

function LuaShiTuDirectoryWnd:OnChatButtonClick()
	local ims = CGroupIMMgr.Inst:GetGroupIMs()
    for i = 0, ims.Count - 1 do
        if ims[i].Type == CGroupIMMgr.GroupIMType_ShiTu then
			if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id ~= ims[i].OwnerId and self.m_IsTudiView then
                CChatHelper.ShowGroupChat(ims[i].GroupIMID)
                return
            elseif CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == ims[i].OwnerId and not self.m_IsTudiView then
                CChatHelper.ShowGroupChat(ims[i].GroupIMID)
                return 
            end
        end
    end
	if not self.m_IsTudiView then
        local msg = g_MessageMgr:FormatMessage("CreateShiTuGroupIm_Confirm")
        MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
            self.m_IsCreatingShiTuGroupIm = true
            Gac2Gas.RequestCreateShiTuGroupIm()
        end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
    else
        if not CSwitchMgr.EnableGroupIM then
            g_MessageMgr:ShowMessage("ShiTuMainWndShiTuView_ChatButtonClick_TuDi_GroupIM_Limit")
            return
		elseif CShiTuMgr.Inst:GetCurrentShiFuId() == 0 then
			g_MessageMgr:ShowMessage("ShowShiTuChat_NoneShiFu")
        else
            Gac2Gas.TryJoinShiMenGroupIM()
        end
    end
end

--@endregion UIEvent

