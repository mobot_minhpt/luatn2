local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UIGrid = import "UIGrid"
local CButton = import "L10.UI.CButton"
local CPopupMenuInfoMgrAlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local Screen = import "UnityEngine.Screen"
local Vector2 = import "UnityEngine.Vector2"
LuaCommonHorizontalPopupMenu = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaCommonHorizontalPopupMenu, "Button", "Button", GameObject)
RegistChildComponent(LuaCommonHorizontalPopupMenu, "ButtonsTable", "ButtonsTable", UIGrid)
RegistChildComponent(LuaCommonHorizontalPopupMenu, "Background", "Background", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaCommonHorizontalPopupMenu, "m_ScrollView")
RegistClassMember(LuaCommonHorizontalPopupMenu, "m_Select")
RegistClassMember(LuaCommonHorizontalPopupMenu, "m_ItemList")
function LuaCommonHorizontalPopupMenu:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_ScrollView = self.transform:Find("Anchor/Background/ScrollView"):GetComponent(typeof(UIScrollView))
    self.Button.gameObject:SetActive(false)
    self.m_Select = 0
    self.m_ItemList = {}
end

function LuaCommonHorizontalPopupMenu:Init()
    if LuaCommonHorizontalPopupMenuMgr.m_BtnTbl then
        self:InitBtnList()
    end
    if LuaCommonHorizontalPopupMenuMgr.m_Target then
        self:AdjustPosition()
    end
    self:LayoutWnd()
    
end
function LuaCommonHorizontalPopupMenu:InitBtnList()
    Extensions.RemoveAllChildren(self.ButtonsTable.transform)
    self.m_Select = LuaCommonHorizontalPopupMenuMgr.m_CurSelect
    local index = 0
    local btnTbl = LuaCommonHorizontalPopupMenuMgr.m_BtnTbl
    local isLeft = LuaCommonHorizontalPopupMenuMgr.m_Anchor == CPopupMenuInfoMgrAlignType.Left
    for i = 1,#btnTbl do
        index = i
        if isLeft then
            index = #btnTbl - i + 1
        end
        self.m_ItemList[index] = {}
        self.m_ItemList[index].text = LocalString.StrH2V(btnTbl[i].text, true)  -- 转成竖版文本
        self.m_ItemList[index].callbackFunc = btnTbl[i].callbackFunc
        if self.m_Select == i then
            self.m_ItemList[index].showHighLight = true
        end
    end

    for k,v in pairs(self.m_ItemList) do
        local btn =  CUICommonDef.AddChild(self.ButtonsTable.gameObject, self.Button)
        btn.gameObject:SetActive(true)
        btn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = v.text
        btn:GetComponent(typeof(CButton)).Selected = v.showHighLight
        UIEventListener.Get(btn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnBtnClick(go,k)
        end)
    end
    if isLeft then
        self.m_ScrollView.contentPivot = UIWidget.Pivot.Right
        self.ButtonsTable.pivot = UIWidget.Pivot.TopRight
    else
        self.m_ScrollView.contentPivot = UIWidget.Pivot.Left
        self.ButtonsTable.pivot = UIWidget.Pivot.TopLeft
    end
    self.ButtonsTable:Reposition()
end

function LuaCommonHorizontalPopupMenu:AdjustPosition()
    local target = LuaCommonHorizontalPopupMenuMgr.m_Target.transform
    local b = NGUIMath.CalculateRelativeWidgetBounds(target)
    local TargetSize = Vector2(b.size.x, b.size.y)
    local TargetCenterPos = target.transform:TransformPoint(b.center)
    local localPos = self.Background.transform.parent:InverseTransformPoint(TargetCenterPos)
    local anchor = LuaCommonHorizontalPopupMenuMgr.m_Anchor
    local bgsprite = self.Background:GetComponent(typeof(UISprite))
    if anchor == CPopupMenuInfoMgrAlignType.Left then
        bgsprite.pivot = UIWidget.Pivot.Right
        self.Background.transform.localPosition = Vector3(localPos.x - TargetSize.x * 0.5 - 10, localPos.y, 0)
    else
        self.Background.transform.localPosition = Vector3(localPos.x + TargetSize.x * 0.5 + 10, localPos.y, 0)
    end

end

function LuaCommonHorizontalPopupMenu:OnBtnClick(go,index)
    if self.m_ItemList and self.m_ItemList[index] then
        if self.m_ItemList[index].callbackFunc then
            self.m_ItemList[index].callbackFunc()
        end
    end
    self:close()
end

function LuaCommonHorizontalPopupMenu:close()
    CUIManager.CloseUI(CLuaUIResources.CommonHorizontalPopupMenu)
end

function LuaCommonHorizontalPopupMenu:LayoutWnd()
    --self.m_ScrollView.panel.bottomAnchor.absolute = 120

    local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    local virtualScreenWidth = Screen.width * scale * CUIManager.UIMainCamera.rect.width
    local virtualScreenHeight = Screen.height * scale

    local contentLeftPadding = math.abs(self.m_ScrollView.panel.leftAnchor.absolute)
    local contentRightPadding = math.abs(self.m_ScrollView.panel.rightAnchor.absolute)

    --计算宽度
    local totalWndWidth = self:TotalWidthOfScrollViewContent() + contentLeftPadding + contentRightPadding + self.m_ScrollView.panel.clipSoftness.x * 2 + 1
    local displayWndWidth = math.min(totalWndWidth, virtualScreenWidth)
    if LuaCommonHorizontalPopupMenuMgr.m_MaxWidth ~= 0 then
        displayWndWidth = math.min(displayWndWidth,LuaCommonHorizontalPopupMenuMgr.m_MaxWidth)
    end
    --设置背景宽度
    self.Background:GetComponent(typeof(UISprite)).width = math.ceil(displayWndWidth)

    -- local rects = CommonDefs.GetComponentsInChildren_Component_Type_Boolean(self.gameObject:GetComponent(typeof(CBaseWnd)), typeof(UIRect), true)
    -- do
    --     local i = 0
    --     while i < rects.Length do
    --         if rects[i].updateAnchors == UIRect.AnchorUpdate.OnEnable or rects[i].updateAnchors == UIRect.AnchorUpdate.OnStart then
    --             rects[i]:ResetAndUpdateAnchors()
    --         end
    --         i = i + 1
    --     end
    -- end
    self.m_ScrollView.transform:GetComponent(typeof(UIPanel)):ResetAndUpdateAnchors()
    self.m_ScrollView:ResetPosition()
    
    -- LuaUtils.SetLocalPosition(self.Background.transform, 0, 0, 0)
    -- self.m_ScrollViewIndicator:Layout()
    -- self.m_Background.transform.localPosition = Vector3.zero
end

function LuaCommonHorizontalPopupMenu:TotalWidthOfScrollViewContent()
    return NGUIMath.CalculateRelativeWidgetBounds(self.ButtonsTable.transform).size.x --+ self.ButtonsTable.padding.x * 2
end
--@region UIEvent

--@endregion UIEvent

LuaCommonHorizontalPopupMenuMgr = class()

LuaCommonHorizontalPopupMenuMgr.m_BtnTbl = nil
LuaCommonHorizontalPopupMenuMgr.m_CurSelect = 0 
LuaCommonHorizontalPopupMenuMgr.m_Anchor = nil  
LuaCommonHorizontalPopupMenuMgr.m_Target = nil
LuaCommonHorizontalPopupMenuMgr.m_MaxWidth = 0

--[[
    btnTable:
    {
        text: 按钮文本
        callbackFunc: 按钮按下的回调
    }
    curSelect: 当前选择按钮高亮
    anchor: CPopupMenuInfoMgr.AlignType , 只有Left和Right有效
    goTrans: 锚点Transform
    maxWidth : 最大宽度，为0表示无限制
]]
function LuaCommonHorizontalPopupMenuMgr:ShowHorizontalPopupMenuWnd(btnTable, curSelect, anchor, target, maxWidth)
    self.m_BtnTbl = btnTable
    self.m_CurSelect = curSelect
    self.m_Anchor = anchor
    self.m_Target = target
    self.m_MaxWidth = maxWidth
    CUIManager.ShowUI("CommonHorizontalPopupMenu")
end