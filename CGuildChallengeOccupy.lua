-- Auto Generated!!
local CGuildChallengeMgr = import "L10.Game.CGuildChallengeMgr"
local CGuildChallengeOccupy = import "L10.UI.CGuildChallengeOccupy"
local CommonDefs = import "L10.Game.CommonDefs"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CTickMgr = import "L10.Engine.CTickMgr"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local EnumEventType = import "EnumEventType"
local ETickType = import "L10.Engine.ETickType"
local EventManager = import "EventManager"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CGuildChallengeOccupy.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.tips).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.tips).onClick, MakeDelegateFromCSFunction(this.OnTipsButtonClick, VoidDelegate, this), false)
    EventManager.RemoveListener(EnumEventType.OnQueryGuildChallengeOccupationInfoReturn, MakeDelegateFromCSFunction(this.OnInitOccupyInfo, Action0, this))

    if this.cancel ~= nil then
        invoke(this.cancel)
    end
end
CGuildChallengeOccupy.m_OnInitOccupyInfo_CS2LuaHook = function (this) 
    this:InitBaseInfo()
    do
        local j = 0
        while j < this.infos.Length do
            local index = - 1
            do
                local i = 0
                while i < CGuildChallengeMgr.Inst.occupationList.Count do
                    if CGuildChallengeMgr.Inst.occupationList[i].templateId == this.infos[j].sceneTemplateId then
                        index = i
                        break
                    end
                    i = i + 1
                end
            end
            if index >= 0 then
                this.infos[j].gameObject:SetActive(true)
                this.infos[j]:Init(index)
            else
                this.infos[j].gameObject:SetActive(false)
            end
            j = j + 1
        end
    end
end
CGuildChallengeOccupy.m_InitBaseInfo_CS2LuaHook = function (this) 
    this.myGuildName.text = CGuildChallengeMgr.Inst.guildName1
    this.targetGuildName.text = CGuildChallengeMgr.Inst.guildName2
    if this.cancel ~= nil then
        invoke(this.cancel)
    end
    this.cancel = CTickMgr.Register(MakeDelegateFromCSFunction(this.OnUpdateCountDown, Action0, this), 1000, ETickType.Loop)
end
CGuildChallengeOccupy.m_OnUpdateCountDown_CS2LuaHook = function (this) 
    local time = CGuildChallengeMgr.Inst.occupationRefreshTime - CServerTimeMgr.Inst.timeStamp
    if time > 1.5 and time < 3.5 then
        this.fx:DestroyFx()
        this.fx:LoadFx(CUIFxPaths.GuildChallengeRefreshOccupyFx)
    elseif time <= 0 and this.cancel ~= nil then
        invoke(this.cancel)
    else
        this:FormatTime(CGuildChallengeMgr.Inst.occupationRefreshTime - CServerTimeMgr.Inst.timeStamp)
    end
    Gac2Gas.GCO_RequestOpenOccupationDetailWnd()
end
CGuildChallengeOccupy.m_FormatNumber_CS2LuaHook = function (this, t) 
    if math.floor(t / 10) >= 1 then
        return tostring(t)
    else
        return System.String.Format("0{0}", t)
    end
end
