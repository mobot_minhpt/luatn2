local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CMiniChatMgr = import "L10.UI.CMiniChatMgr"

LuaCommonSideDialogMgr = {}
LuaCommonSideDialogMgr.m_DialogList = {}
LuaCommonSideDialogMgr.m_IsOpen = true
LuaCommonSideDialogMgr.m_IsInit = false
LuaCommonSideDialogMgr.m_LastPlayerId = 0

function LuaCommonSideDialogMgr:OnMainPlayerCreated()
    local playerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    if playerId ~= self.m_LastPlayerId then
        self.m_LastPlayerId = playerId
        self:Clear()
    end
end

function LuaCommonSideDialogMgr:Clear()
    for _, data in pairs(self.m_DialogList) do
        if data.onCloseAction then
            data.onCloseAction()
        end
    end
    self.m_DialogList = {}
    g_ScriptEvent:BroadcastInLua("OnUpdateCommonSideDialogList")
end

function LuaCommonSideDialogMgr:RefuseAll()
    local newList = {}
    for _, data in pairs(self.m_DialogList) do
        local isCanRefuse = false
        if data.btn1Info and data.btn1Info.isRefuseBtn then
            isCanRefuse = true
            self:OnBtnClicked(data.btn1Info, data)
        elseif data.btn2Info and data.btn2Info.isRefuseBtn then
            isCanRefuse = true
            self:OnBtnClicked(data.btn2Info, data)
        end
        if not isCanRefuse then
            table.insert(newList, data)
        end
    end
    self.m_DialogList = newList
    g_ScriptEvent:BroadcastInLua("OnUpdateCommonSideDialogList")
end

function LuaCommonSideDialogMgr:ClearSameKeyDataWithSelectAction(key, selectAction)
    local newList = {}
    for _, data in pairs(self.m_DialogList) do
        if data.key ~= key then
            table.insert(newList, data)
        elseif data.key == key and selectAction(data) then
            table.insert(newList, data)
        else
            if data.onClearSameKeyDataAction then
                data.onClearSameKeyDataAction(data)
            end
            if data.onCloseAction then
                data.onCloseAction()
            end
        end
    end
    self.m_DialogList = newList
    g_ScriptEvent:BroadcastInLua("OnUpdateCommonSideDialogList")
end

function LuaCommonSideDialogMgr:IsSameDialogData(a, b)
    return (a.showDialogTime == b.showDialogTime) and (a.key == b.key)
end

--数字越小优先级越低
LuaCommonSideDialogMgr.m_key2PriorityMap = {}
LuaCommonSideDialogMgr.m_key2DeleteCountDown = {}
function LuaCommonSideDialogMgr:ShowCommonSideDialogWnd(data)
    if not self.m_IsInit then
        Message_SideDialog.Foreach(function (k,v)
            self.m_key2PriorityMap[k] = v.Priority
            self.m_key2DeleteCountDown[k] = v.DeleteCountDown
        end)
        self.m_IsInit = true
    end
    local isSameDialogData = false
    for i = 1, #self.m_DialogList do
        local dialog = self.m_DialogList[i]
        if self:IsSameDialogData(dialog, data) then
            isSameDialogData = true
        end
    end
    if isSameDialogData then
        return
    end
    table.insert(self.m_DialogList, data)
    self:Sort()
    local storageLimit = Message_Setting.GetData().StorageLimit
    if #self.m_DialogList > storageLimit then
        local newlist = {}
        for i = 1, storageLimit  do
            table.insert(newlist, self.m_DialogList[i])
        end
        self.m_DialogList = newlist
    end

    --音游模式下屏蔽侧边栏
	if CUIManager.IsLoading(CLuaUIResources.MusicPlayHitingWnd) or CUIManager.IsLoaded(CLuaUIResources.MusicPlayHitingWnd) then
		return
	end

    if not CUIManager.IsLoaded(CLuaUIResources.CommonSideDialogWnd) then
        CUIManager.ShowUI(CLuaUIResources.CommonSideDialogWnd)
    else
        g_ScriptEvent:BroadcastInLua("OnUpdateCommonSideDialogList")
    end
end

--msg 提示文字
--key 用以处理互斥类型的提示消息，处理完一条消息自动清理同key提示消息
--btn1Info, btn2Info 按钮信息
--onCloseAction 关闭按钮点击事件
--onClearSameKeyDataAction 互斥消息被同key消息清楚时的回调
function LuaCommonSideDialogMgr:ShowDialog(msg, key, btn1Info, btn2Info, checkBoxInfo, onCloseAction, extraData, onClearSameKeyDataAction, hideCloseBtn)
    self:ShowCommonSideDialogWnd({msg = msg, key = key, btn1Info = btn1Info, btn2Info = btn2Info, checkBoxInfo = checkBoxInfo, onCloseAction = onCloseAction, showDialogTime = CServerTimeMgr.Inst.timeStamp, extraData = extraData, onClearSameKeyDataAction = onClearSameKeyDataAction, hideCloseBtn = hideCloseBtn})
end

--btnName 按钮文字
--btnAction 按钮点击事件
--isYellowBtn true是默认的黄色按钮，false是蓝色按钮
--clearSameKeyAferDoAction true的话执行点击事件后会自动清理同key提示消息
--btnDuration 倒计时
--btnDefaultSelected 是否默认选中该按钮
--isRefuseBtn 为true才会被全部拒绝按钮执行action
function LuaCommonSideDialogMgr:GetBtnInfo(btnName, btnAction, isYellowBtn, clearSameKeyAferDoAction, btnDuration, btnDefaultSelected, isRefuseBtn)
    local data = {btnName = btnName, btnAction = btnAction, isYellowBtn = isYellowBtn, clearSameKeyAferDoAction = clearSameKeyAferDoAction, btnDuration = btnDuration, btnDefaultSelected = btnDefaultSelected, isRefuseBtn = isRefuseBtn}
    return data
end

function LuaCommonSideDialogMgr:OnBtnClicked(btnInfo, data)
	if self:HasData(data) then
        if btnInfo.btnAction then
            btnInfo.btnAction()
        end
        if btnInfo.clearSameKeyAferDoAction then
            self:ClearSameKeyData(data.key)
        else
            self:DeleteDialog(data)
        end
	end
end

function LuaCommonSideDialogMgr:ShowMiniChatNotifyWnd(key, btnInfo, notifyInfo)
    self:ShowCommonSideDialogWnd({key = key, isNotify = true, btn1Info = btnInfo, notifyInfo = notifyInfo, showDialogTime = CServerTimeMgr.Inst.timeStamp, btn2Info = self:GetBtnInfo(LocalString.GetString("忽略"), nil, not btnInfo.isYellowBtn, false)})
end

function LuaCommonSideDialogMgr:GetNotifyInfo(messageName, bgSpriteName, labaIconSpriteName, cornerMark1SpriteName, cornerMark2SpriteName, cornerMark3SpriteName)
    local data = {messageName = messageName, bgSpriteName = bgSpriteName, labaIconSpriteName = labaIconSpriteName, cornerMark1SpriteName = cornerMark1SpriteName, cornerMark2SpriteName = cornerMark2SpriteName, cornerMark3SpriteName = cornerMark3SpriteName,
    notifySenderId = CMiniChatMgr.NotifySenderId, notifySenderName = CMiniChatMgr.NotifySenderName}
    return data
end

--text 勾选项文字
--isSelectAction return 是否选中该选项 checkbox.Selected = isSelectAction()
--onClickAction 点击事件
--evtType 点击后的回调事件 g_ScriptEvent:AddListener("evtType",t,"evtType")
function LuaCommonSideDialogMgr:GetCheckBoxInfo(text, isSelectAction, onClickAction, evtType)
    local data = {text = text, isSelectAction = isSelectAction, onClickAction = onClickAction, evtType = evtType}
    return data
end

function LuaCommonSideDialogMgr:Sort()
    if #self.m_DialogList == 0 then
        return
    end
    table.sort(self.m_DialogList, function (a, b)
        return a.showDialogTime > b.showDialogTime --后出现的消息优先显示
    end)
end

function LuaCommonSideDialogMgr:ClearSameKeyData(key)
    local newlist = {}
    for i, data in pairs(self.m_DialogList) do
        if data.key ~= key then
            table.insert(newlist, data)
        else
            if data.onClearSameKeyDataAction then
                data.onClearSameKeyDataAction(data)
            end
            if data.onCloseAction then
                data.onCloseAction()
            end
        end
    end
    self.m_DialogList = newlist
    self:Sort()
    g_ScriptEvent:BroadcastInLua("OnUpdateCommonSideDialogList")
end

function LuaCommonSideDialogMgr:DeleteDialog(data)
    local newlist = {}
    for i, dialog in pairs(self.m_DialogList) do
        if not self:IsSameDialogData(dialog, data) then
            table.insert(newlist, dialog)
        else
            if data.onCloseAction then
                data.onCloseAction()
            end
        end
    end
    self.m_DialogList = newlist
    self:Sort()
    g_ScriptEvent:BroadcastInLua("OnUpdateCommonSideDialogList")
end

function LuaCommonSideDialogMgr:HasData(data)
    local has = false
    for i, dialog in pairs(self.m_DialogList) do
        if self:IsSameDialogData(dialog, data) then
            has = true
        end
    end
    return has
end

function LuaCommonSideDialogMgr:GetTopDialogData()
    if #self.m_DialogList > 0 then
        local topDialogData = nil
        local messageCountdown = Message_Setting.GetData().MessageCountdown
        for i = 1, #self.m_DialogList do
            if not topDialogData then
                local data = self.m_DialogList[1]
                local duration = nil
                if data.btn1Info and data.btn1Info.btnDuration and data.btn1Info.btnDuration > 0 then
                    duration = data.btn1Info.btnDuration
                elseif data.btn2Info and data.btn2Info.btnDuration and data.btn2Info.btnDuration > 0  then
                    duration = data.btn2Info.btnDuration
                end
                local canShowTop = true
                for j, _data in pairs(self.m_DialogList) do
                    if j ~= i and _data.key == data.key then
                        canShowTop = false --互斥消息只能点击查看后显示
                    end
                end
                canShowTop = canShowTop or data.isNotify
                if canShowTop then
                    if duration then
                        local seconds = data.showDialogTime + duration - CServerTimeMgr.Inst.timeStamp
                        if seconds >= messageCountdown then
                            topDialogData = data --倒计时小于1秒的消息不弹出
                        end
                    else
                        topDialogData = data
                    end
                end
            end
        end
        return topDialogData
    end
    return nil
end

LuaCommonSideDialogMgr.m_DeleteCountDownTick = nil
LuaCommonSideDialogMgr.m_ReadyDeleteDataList = {}
function LuaCommonSideDialogMgr:RegisterDeleteCountDownTick()
    self:CancelDeleteCountDownTick()
    self.m_DeleteCountDownTick = RegisterTick(function ()
        local hasReadyDeleteData = false
        for i = 1, #self.m_DialogList do
            local dialog = self.m_DialogList[i]
            if dialog.countDown == nil then
                dialog.countDown = 0
            end
            dialog.countDown = dialog.countDown + 1
            local countDown = self.m_key2DeleteCountDown[dialog.key]
            if countDown and countDown > 0 then
                if dialog.countDown >= countDown then
                    table.insert(self.m_ReadyDeleteDataList, dialog)
                else
                    hasReadyDeleteData = true
                end
            end
        end
        if #self.m_ReadyDeleteDataList > 0 then
            for _, dialog in pairs(self.m_ReadyDeleteDataList) do
                LuaCommonSideDialogMgr:DeleteDialog(dialog)
            end
            self.m_ReadyDeleteDataList = {}
        end
        if not hasReadyDeleteData then
            self:CancelDeleteCountDownTick()
        end
    end,1000)
end

function LuaCommonSideDialogMgr:CancelDeleteCountDownTick()
    if self.m_DeleteCountDownTick then
        UnRegisterTick(self.m_DeleteCountDownTick)
        self.m_DeleteCountDownTick = nil
    end
    self.m_ReadyDeleteDataList = {}
    for i = 1, #self.m_DialogList do
        local dialog = self.m_DialogList[i]
        dialog.countDown = 0
    end
end