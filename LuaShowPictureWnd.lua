local CUITexture = import "L10.UI.CUITexture"
local UITexture = import "UITexture"
CLuaShowPictureWnd = class()
CLuaShowPictureWnd.m_MatPath = nil
CLuaShowPictureWnd.m_Width = 512
CLuaShowPictureWnd.m_Height = 512

function CLuaShowPictureWnd:Init()
    local texture = self.transform:Find("Texture"):GetComponent(typeof(CUITexture))
    texture:LoadMaterial(CLuaShowPictureWnd.m_MatPath)
    local uitexture = texture:GetComponent(typeof(UITexture))
    uitexture.width = CLuaShowPictureWnd.m_Width
    uitexture.height = CLuaShowPictureWnd.m_Height
end