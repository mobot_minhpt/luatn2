local CYaoGuai = import "L10.Game.CYaoGuai"
local CEffectMgr = import "L10.Game.CEffectMgr"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local EnumWarnFXType =import "L10.Game.EnumWarnFXType"
local EnumQualityType = import "L10.Game.EnumQualityType"
local CTrackMgr = import "L10.Game.CTrackMgr"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"

LuaZhuoYaoMgr = {}
LuaZhuoYaoMgr.m_TuJianMonsterIds = nil
LuaZhuoYaoMgr.m_BagSize = 0
LuaZhuoYaoMgr.m_Exp = 0
LuaZhuoYaoMgr.m_Level = 1
LuaZhuoYaoMgr.m_WarehouseYaoGuaiList = {}
LuaZhuoYaoMgr.m_TipData = nil
LuaZhuoYaoMgr.m_TipLabels = nil
LuaZhuoYaoMgr.m_TipBtnActions = nil
LuaZhuoYaoMgr.m_CatchMosterIdList = {}
LuaZhuoYaoMgr.m_QualityBorderColorList = nil
LuaZhuoYaoMgr.m_ZhuoYaoMainWndTab = 0

function LuaZhuoYaoMgr:Leave()
    self.m_WarehouseYaoGuaiList = {}
    self.m_CatchMosterIdList = {}
end

function LuaZhuoYaoMgr:CatchYaoGuai(monterID)
    Gac2Gas.RequestCatchYaoGuai(monterID)
end

function LuaZhuoYaoMgr:GetFuLongPieceItemAndCatchItem()
    local catchItemIdStr = ZhuoYao_Setting.GetData("CatchItemId").Value
    local arr = g_LuaUtil:StrSplit(catchItemIdStr,";")
    return tonumber(ZhuoYao_Setting.GetData("FuLongItemId").Value), tonumber(arr[1])
end

function LuaZhuoYaoMgr:IsFeatureOpen()
    local isOpen = false
    local taskID = tonumber(ZhuoYao_Setting.GetData("EnableFeatureTaskID").Value) 
    if taskID and CClientMainPlayer.Inst then
        local taskFinished = CClientMainPlayer.Inst.TaskProp:IsMainPlayerTaskFinished(taskID)
        isOpen = taskFinished
    end
    isOpen = isOpen or (CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.MingGe > 0)
    return isOpen and LuaZongMenMgr.m_IsOpen
end

function LuaZhuoYaoMgr:CheckIsCatched(monsterEngineId)
    return self.m_CatchMosterIdList[monsterEngineId]
end

function LuaZhuoYaoMgr:GetAllTuJian()
    if not self:IsFeatureOpen() then return end
    if self.m_TuJianMonsterIds then return end
    self.m_TuJianMonsterIds = {}
    ZhuoYao_TuJian.Foreach(function(id, data)
        local list = data.MonsterIds
        for i = 0, list.Length - 1 do
            local monsterId = list[i]
            self.m_TuJianMonsterIds[monsterId] = id
        end
    end)
end

function LuaZhuoYaoMgr:GetTuJianId(monsterTempId)
    self:GetAllTuJian()
    return self.m_TuJianMonsterIds[monsterTempId]
end

function LuaZhuoYaoMgr:GetCatchRate(tujianData, targetMonsterLevel)
    local rate = 0
    local catchRateFormulaId = tonumber(ZhuoYao_Setting.GetData("CatchRateFormulaId").Value)
    local monsterLevel = tujianData.Fixinglevel == 0 and CClientMainPlayer.Inst.MaxLevel or tujianData.Fixinglevel
    if targetMonsterLevel then
        monsterLevel = targetMonsterLevel
    end
    local formula = AllFormulas.Action_Formula[catchRateFormulaId] and AllFormulas.Action_Formula[catchRateFormulaId].Formula or nil
    if formula and CClientMainPlayer.Inst then
        rate = formula(nil, nil, {CClientMainPlayer.Inst.MaxLevel, self.m_Level, monsterLevel, tujianData.Grade})
    end
    return rate
end

function LuaZhuoYaoMgr:GetHpThreshold(maxhp)
    local formulaId = tonumber(ZhuoYao_Setting.GetData("CatchHpThresholdFormulaId").Value)
    local formula = AllFormulas.Action_Formula[formulaId] and AllFormulas.Action_Formula[formulaId].Formula or nil
    local threshold = 0.5
    if formula and CClientMainPlayer.Inst then
        threshold = formula(nil, nil, {maxhp})
    end
    return threshold
end

function LuaZhuoYaoMgr:ShowTip(data, labels, btnActions)
    self.m_TipData = data
    self.m_TipLabels = labels
    self.m_TipBtnActions = btnActions
    CUIManager.ShowUI(CLuaUIResources.YaoGuaiInfoWnd)
end

function LuaZhuoYaoMgr:GetYaoGuaiQualityBorderColor(tujianData, quality)
    self:GetQualityBorderColorList()
    local resV = (quality - tujianData.QualityRange[0]) / (tujianData.QualityRange[1] - tujianData.QualityRange[0])
    local res = 1
    for i = 1, #self.m_QualityBorderColorList, 2 do
        local v =  tonumber(self.m_QualityBorderColorList[i])
        local colorEnum = self.m_QualityBorderColorList[i + 1]
        if resV <= v then
            res = colorEnum
            break
        end
    end
    local enumtype = CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), res)
    local borderSpriteName = CUICommonDef.GetItemCellBorder(enumtype)
    return borderSpriteName, enumtype
end

function LuaZhuoYaoMgr:CheckYaoGuaiIsHighQuality(tujianData, quality)
    self:GetQualityBorderColorList()
    local resV = quality / tujianData.QualityRange[1]
    local lastIndex = #self.m_QualityBorderColorList
    local v = tonumber(self.m_QualityBorderColorList[lastIndex - 3])
    return resV > v
end

function LuaZhuoYaoMgr:GetQualityBorderColorList()
    if self.m_QualityBorderColorList then return end
    self.m_QualityBorderColorList = {}
    local list = g_LuaUtil:StrSplit(ZhuoYao_Setting.GetData("QualityBorderColor").Value, ";")
    for _,s in pairs(list) do
        local k2v = g_LuaUtil:StrSplit(s, ",")
        table.insert(self.m_QualityBorderColorList, k2v[1])
        table.insert(self.m_QualityBorderColorList, k2v[2])
    end
end

function LuaZhuoYaoMgr:DoGetpathAction(getPath)
    local data = ZhuoYao_GetPathInfo.GetData(getPath)
    if data then
        local action = data.Action
        if action == nil  then return end
        for functionName, parameters in string.gmatch(action, "(%w+)%(([^%s]-)%)") do
            local func = self[functionName]
            if func then
                if nil == parameters then
                    func(self)
                else
                    func(self,g_LuaUtil:StrSplit(parameters,","))
                end
            end
        end
    end
end
----------------------------------------------------------------------------------------
---ClickAction
function LuaZhuoYaoMgr:ShowScheduleInfo(params)
    local activityId = tonumber(params[1]) 
    CLuaScheduleMgr:ShowScheduleInfo(activityId)
end

function LuaZhuoYaoMgr:ShowMessage(params)
    local msgName = string.gsub(params[1], "%\"", "")
    g_MessageMgr:ShowMessage(msgName)
end

function LuaZhuoYaoMgr:FindNpc(params)
    local npcTemplateId, sceneTemplateId, posX, posY = params[1],params[2],params[3],params[4]
    CTrackMgr.Inst:FindNPC(npcTemplateId, sceneTemplateId, posX, posY, nil, nil)
end

--------------------------------------------rpc--------------------------------------------
-- 捉妖
function Gas2Gac.SendYaoGuai(yaoguaiUD, isOnLoad)
    local info = CYaoGuai()
    info:LoadFromString(yaoguaiUD)
    LuaZhuoYaoMgr.m_WarehouseYaoGuaiList[info.Id] = info
    g_ScriptEvent:BroadcastInLua("OnWarehouseYaoGuaiListUpdate")
end

function Gas2Gac.DeleteYaoGuai(yaoguaiId)
    LuaZhuoYaoMgr.m_WarehouseYaoGuaiList[yaoguaiId] =  nil
    g_ScriptEvent:BroadcastInLua("OnWarehouseYaoGuaiListUpdate")
end

function Gas2Gac.IncYaoGuaiBagSucc(currentSize)
    LuaZhuoYaoMgr.m_BagSize = currentSize
    g_ScriptEvent:BroadcastInLua("OnIncYaoGuaiBagSucc",currentSize)
end

function Gas2Gac.TryCatchYaoGuai(monsterEngineId)
    LuaZhuoYaoMgr.m_CatchMosterIdList[monsterEngineId] = true
    g_ScriptEvent:BroadcastInLua("OnTryCatchYaoGuai")
end

function Gas2Gac.CatchYaoGuaiResult(isSucc, monsterEngineId, yaoguaiId)
    if isSucc then
        CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.ZhuoYaoMainWnd)
    end
    local co = CClientObjectMgr.Inst:GetObject(monsterEngineId)
    if co then
        local ro = co.RO
        if ro then
            local fx = CEffectMgr.Inst:AddObjectFX(isSucc and 88801939 or 88801940,ro,0,1,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero)
            ro:AddFX("CatchedYaoGuaiResult", fx)
        end
    end
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.RO then
        local ro = CClientMainPlayer.Inst.RO
        local fx = CEffectMgr.Inst:AddObjectFX(isSucc and 88801958 or 88801959,ro,0,1,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero)
        ro:RemoveFX("CatchedYaoGuaiResult")
        ro:AddFX("CatchedYaoGuaiResult", fx)
        RegisterTickOnce(function()
            ro:RemoveFX("CatchedYaoGuaiResult")
        end, 2000)
    end
    LuaZhuoYaoMgr.m_CatchMosterIdList[monsterEngineId] = nil
end

function Gas2Gac.SyncYaoGuaiTuJian(tujianData)
    local list = MsgPackImpl.unpack(tujianData)
    local catchList = {}
    for i = 0, list.Count - 1, 7 do
        local yaoguaiTempId = list[i]
        local catchedCount = list[i+1]
        local maxQuality = list[i+2]
        local maxQualityLevel = list[i+3]
        local maxQualityCatchedTime = list[i+4]
        local isRewarded = list[i+5]
        local isFullQualityRewarded = list[i+6]
        table.insert(catchList, {yaoguaiTempId = yaoguaiTempId, catchedCount = catchedCount,maxQuality = maxQuality,maxQualityLevel = maxQualityLevel, maxQualityCatchedTime = maxQualityCatchedTime,isRewarded = isRewarded,isFullQualityRewarded = isFullQualityRewarded})
    end
    g_ScriptEvent:BroadcastInLua("OnSyncYaoGuaiTuJian", catchList)
end

function Gas2Gac.SyncZhuoYaoLevelAndExp(level, exp, bagSize)
    LuaZhuoYaoMgr.m_BagSize = bagSize
    LuaZhuoYaoMgr.m_Exp = exp
    LuaZhuoYaoMgr.m_Level = level
    g_ScriptEvent:BroadcastInLua("OnSyncZhuoYaoLevelAndExp", level, exp, bagSize)
end

function Gas2Gac.SyncGetTuJianRewardSucc(yaoguaiTempId)
    g_ScriptEvent:BroadcastInLua("OnSyncGetTuJianRewardSucc", yaoguaiTempId)
end

function Gas2Gac.SyncGetTuJianFullQualityRewardSucc(yaoguaiTempId)
    g_ScriptEvent:BroadcastInLua("OnSyncGetTuJianFullQualityRewardSucc", yaoguaiTempId)
end

function Gas2Gac.SendZhuoYaoLevelUp(oldLevel, newLevel)
    EventManager.Broadcast(EnumEventType.MainPlayerLevelChange)
end

LuaZhuoYaoMgr.m_ZhuoYaoUnlockInfoIte_CanCatchSet = {}
LuaZhuoYaoMgr.m_ZhuoYaoUnlockInfoIte_NoTimesSet = {}
LuaZhuoYaoMgr.m_ZhuoYaoUnlockInfoIte_OtherSet = {}
LuaZhuoYaoMgr.m_ZhuoYaoHuaFuWnd_YaoguaiTempId = 0
LuaZhuoYaoMgr.m_ZhuoYaoHuaFuWnd_ZhuoyaoType = 0
-- 宗派捉妖入口迭代-入口：包裹-妖怪口袋-妖怪捕捉页签 Gac2Gas.QueryZhuoYaoUnlockInfo_Ite
-- level: 捉妖等级
-- exp: 捉妖经验
-- canCatchUd: list, 等级和次数都满足
-- noTimesUd: list, 次数不满足，注意，等级可能也不满足
-- otherUd: list, 次数满足,但有其他条件不满足
function Gas2Gac.SyncZhuoYaoUnlockInfo_Ite(level, exp, canCatchUd, noTimesUd, otherUd)
    -- 当前已解锁的妖怪
    local canCatchList = g_MessagePack.unpack(canCatchUd)
    local noTimesList = g_MessagePack.unpack(noTimesUd)
    local otherList = g_MessagePack.unpack(otherUd)
    LuaZhuoYaoMgr.m_ZhuoYaoUnlockInfoIte_CanCatchSet = {}
    LuaZhuoYaoMgr.m_ZhuoYaoUnlockInfoIte_NoTimesSet = {}
    LuaZhuoYaoMgr.m_ZhuoYaoUnlockInfoIte_OtherSet = {}
    for _, yaoguaiTempId in pairs(canCatchList) do
		LuaZhuoYaoMgr.m_ZhuoYaoUnlockInfoIte_CanCatchSet[yaoguaiTempId] = true
	end
    for _, yaoguaiTempId in pairs(noTimesList) do
		LuaZhuoYaoMgr.m_ZhuoYaoUnlockInfoIte_NoTimesSet[yaoguaiTempId] = true
	end
    for _, yaoguaiTempId in pairs(otherList) do
		LuaZhuoYaoMgr.m_ZhuoYaoUnlockInfoIte_OtherSet[yaoguaiTempId] = true
	end
    g_ScriptEvent:BroadcastInLua("OnSyncZhuoYaoUnlockInfo_Ite", level, exp)
end

-- 玩家初次解锁可捕捉妖怪时 触发引导
function Gas2Gac.SyncTriggerZhuoyaoGuide_Ite()
    CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.ZhuoYaoCaptureView)
end

-- 初次进入画符界面时触发引导
function Gas2Gac.SyncTriggerZhuoyaoHuaFuGuide_Ite()
    CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.ZhuoYaoHuaFuWnd)
end

LuaZhuoYaoMgr.m_YaoGuaiCacthInfoIte = {}
-- 根据妖怪id查询当前选中妖怪的本周捕捉次数信息 Gac2Gas.QueryYaoguaiCatchInfo
-- catchTimes 是分子,本周捕捉次数
-- unlockTimes 是分母,本周解锁次数
function Gas2Gac.SyncYaoGuaiCacthInfo_Ite(yaoguaiTempId, catchTimes, unlockTimes)
    LuaZhuoYaoMgr.m_YaoGuaiCacthInfoIte[yaoguaiTempId] = {catchTimes, unlockTimes}
    --print(yaoguaiTempId, catchTimes, unlockTimes)
    g_ScriptEvent:BroadcastInLua("OnSyncYaoGuaiCacthInfo_Ite", yaoguaiTempId, catchTimes, unlockTimes)
end

-- Gac2Gas.RequestCatchYaoGuai_Ite(yaoguaiTempId, zhuoyaoType) 对应的返回
LuaZhuoYaoMgr.m_CatchYaoGuaiResultInfo = {}
function Gas2Gac.RequestCatchYaoGuaiResult_Ite(bSuccess, yaoguaiTempId, zhuoyaoType, quality, returnItemTemplateId, returnItemCount, bravo, addExp, level, currentExp, originalLevel, originalExp)
    print(bSuccess, yaoguaiTempId, zhuoyaoType, quality, returnItemTemplateId, returnItemCount,bravo, addExp, level, currentExp)
    LuaZhuoYaoMgr.m_CatchYaoGuaiResultInfo = {bSuccess = bSuccess, yaoguaiTempId = yaoguaiTempId, zhuoyaoType = zhuoyaoType,
        quality = quality, returnItemTemplateId = returnItemTemplateId, returnItemCount = returnItemCount,bravo = bravo,
        addExp = addExp, level = level, currentExp = currentExp, lastlevel = originalLevel, lastexp = originalExp}
    g_ScriptEvent:BroadcastInLua("OnRequestCatchYaoGuaiResult_Ite")
end
-- 触发修行玩法的引导
function Gas2Gac.SyncTriggerSectXiuXingGuide_Ite()
    
end