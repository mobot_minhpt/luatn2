local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CButton = import "L10.UI.CButton"
local CUITexture = import "L10.UI.CUITexture"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr.AlignType"
local Extensions = import "Extensions"
local CItem = import "L10.Game.CItem"
local GameObject = import "UnityEngine.GameObject"

LuaGuildLeagueCrossAwardOverviewWnd = class()

RegistClassMember(LuaGuildLeagueCrossAwardOverviewWnd, "m_AwardTexture")
RegistClassMember(LuaGuildLeagueCrossAwardOverviewWnd, "m_SwitchButton")
RegistClassMember(LuaGuildLeagueCrossAwardOverviewWnd, "m_GiftBagItem")
RegistClassMember(LuaGuildLeagueCrossAwardOverviewWnd, "m_TitleTemplate")
RegistClassMember(LuaGuildLeagueCrossAwardOverviewWnd, "m_Table")

RegistClassMember(LuaGuildLeagueCrossAwardOverviewWnd, "m_ShowFirstPlaceSceneTexture")

function LuaGuildLeagueCrossAwardOverviewWnd:Awake()
    self.m_AwardTexture = self.transform:Find("Anchor/Scene/AwardTexture"):GetComponent(typeof(CUITexture))
    self.m_SwitchButton = self.transform:Find("Anchor/Scene/SwitchButton"):GetComponent(typeof(CButton))
    self.m_GiftBagItem = self.transform:Find("Anchor/GiftBag/Item").gameObject
    self.m_TitleTemplate = self.transform:Find("Anchor/Title/Template").gameObject
    self.m_Table = self.transform:Find("Anchor/Title/Table"):GetComponent(typeof(UITable))
    self.m_TitleTemplate:SetActive(false)
    UIEventListener.Get(self.m_SwitchButton.gameObject).onClick = DelegateFactory.VoidDelegate(function() self:OnSwitchButtonClick() end)
    self.m_ShowFirstPlaceSceneTexture = true

    --对第二张图进行加载预热，避免首次切换出现闪烁
    local obj = TypeAs(CommonDefs.Object_Instantiate(self.m_AwardTexture.gameObject), typeof(GameObject))
    obj.transform.parent = self.m_AwardTexture.transform.parent
    local tex = obj.transform:GetComponent(typeof(CUITexture))
    tex.alpha = 0
    tex:LoadMaterial("UI/Texture/Transparent/Material/guildleaguecrossawardwnd_award_02.mat")
end

function LuaGuildLeagueCrossAwardOverviewWnd:Init()
    self:LoadSceneAwardInfo()
    local data = GuildLeagueCross_Setting.GetData()
    self:InitAwardItem(self.m_GiftBagItem.transform, data.GiftBagAwardItemID, nil)
    local spriteNames = {"headinfownd_jiashang_01", "headinfownd_jiashang_02", "headinfownd_jiashang_03"}
    Extensions.RemoveAllChildren(self.m_Table.transform)
    for i=0, data.TitleAwardItemIDs.Length-1 do
        local child = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_TitleTemplate)
        child:SetActive(true)
        self:InitAwardItem(child.transform:Find("Item"), data.TitleAwardItemIDs[i], spriteNames[i+1])
    end
    self.m_Table:Reposition()
end

function LuaGuildLeagueCrossAwardOverviewWnd:LoadSceneAwardInfo()
    if self.m_ShowFirstPlaceSceneTexture then
        self.m_AwardTexture:LoadMaterial("UI/Texture/Transparent/Material/guildleaguecrossawardwnd_award_01.mat")
        self.m_SwitchButton.Text = LocalString.GetString("点击查看亚季军奖励")
    else
        self.m_AwardTexture:LoadMaterial("UI/Texture/Transparent/Material/guildleaguecrossawardwnd_award_02.mat")
        self.m_SwitchButton.Text = LocalString.GetString("点击查看冠军奖励")
    end
end

function LuaGuildLeagueCrossAwardOverviewWnd:OnSwitchButtonClick()
    self.m_ShowFirstPlaceSceneTexture = not self.m_ShowFirstPlaceSceneTexture
    self:LoadSceneAwardInfo()
end

function LuaGuildLeagueCrossAwardOverviewWnd:InitAwardItem(transRoot, templateId, rankIconName)
    local item = Item_Item.GetData(templateId)
    local icon = transRoot:Find("Icon"):GetComponent(typeof(CUITexture))
    icon:LoadMaterial(item.Icon)
    local nameLabel = transRoot:Find("NameLabel")
    if nameLabel then
        nameLabel = nameLabel:GetComponent(typeof(UILabel))
        nameLabel.text = CItem.GetColoredDisplayName(templateId) --传入item会有问题，此类型需要import C# Item_Item
    end
    local rankIcon = transRoot:Find("RankIcon")
    if rankIcon then
        rankIcon = rankIcon:GetComponent(typeof(UISprite))
        rankIcon.spriteName = rankIconName
    end
    UIEventListener.Get(icon.gameObject).onClick = DelegateFactory.VoidDelegate(function() self:OnItemClick(transRoot.gameObject, templateId) end)

end

function LuaGuildLeagueCrossAwardOverviewWnd:OnItemClick(go, itemId)
    CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
end

