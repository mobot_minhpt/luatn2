-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CDrugItemTemplate = import "L10.UI.CDrugItemTemplate"
local CItem = import "L10.Game.CItem"
local CItemMgr = import "L10.Game.CItemMgr"
local Color = import "UnityEngine.Color"
CDrugItemTemplate.m_InitItemInfo_CS2LuaHook = function (this, food) 
    this.Item = food
    this.Name.text = food.Name
    this.Description.text = CommonDefs.StringSplit_ArrayChar(CItem.GetItemDescription(food.ID,true), "#")[0]
    this.Icon:LoadMaterial(food.Icon)

    local default
    local adjustedGradeCheck = CLuaDesignMgr.Item_Item_GetAdjustedGradeCheck(food)
    if adjustedGradeCheck > CClientMainPlayer.Inst.Level then
        default = System.String.Format("lv.{0}", adjustedGradeCheck)
    else
        default = ""
    end
    this.Level.text = default
    this.Level.color = Color.red

    local bindCount, notBindCount
    bindCount, notBindCount = CItemMgr.Inst:CalcItemCountInBagByTemplateId(food.ID)
    this.mask:SetActive(bindCount + notBindCount <= 0)

    this.Amount.text = tostring((bindCount + notBindCount))
    this.Amount.color = bindCount + notBindCount > 0 and Color.green or Color.red
end
CDrugItemTemplate.m_OnSendItem_CS2LuaHook = function (this, itemid) 
    local item = CItemMgr.Inst:GetById(itemid)
    if item ~= nil and item.TemplateId == this.Item.ID then
        this:InitItemInfo(this.Item)
    end
end
CDrugItemTemplate.m_OnSetItemAt_CS2LuaHook = function (this, place, pos, oldId, newId) 
    local oldItem = CItemMgr.Inst:GetById(oldId)
    local newItem = CItemMgr.Inst:GetById(newId)
    if oldItem ~= nil and oldItem.TemplateId == this.Item.ID then
        this:InitItemInfo(this.Item)
    elseif newItem ~= nil and newItem.TemplateId == this.Item.ID then
        this:InitItemInfo(this.Item)
    end
end
