
LuaChunJie2024FuYanBuyWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChunJie2024FuYanBuyWnd, "activityTime", "ActivityTime", UILabel)
RegistChildComponent(LuaChunJie2024FuYanBuyWnd, "roundTime", "RoundTime", UILabel)
RegistChildComponent(LuaChunJie2024FuYanBuyWnd, "rankButton", "RankButton", GameObject)
RegistChildComponent(LuaChunJie2024FuYanBuyWnd, "template", "Template", GameObject)
RegistChildComponent(LuaChunJie2024FuYanBuyWnd, "grid", "Grid", UIGrid)
RegistChildComponent(LuaChunJie2024FuYanBuyWnd, "detailRoot", "Detail", Transform)
RegistChildComponent(LuaChunJie2024FuYanBuyWnd, "allFinished", "AllFinished", GameObject)
--@endregion RegistChildComponent end

function LuaChunJie2024FuYanBuyWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.rankButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRankButtonClick()
	end)

    --@endregion EventBind end
end

function LuaChunJie2024FuYanBuyWnd:Init()
	local settingData = ChunJie2024_FuYanSetting.GetData()
	self.activityTime.text = settingData.ActivityTime
	self.roundTime.text = SafeStringFormat3(LocalString.GetString("[FF5050]今天[-] %s结束"), settingData.CaiMaiEndTime)
end

--@region UIEvent

function LuaChunJie2024FuYanBuyWnd:OnRankButtonClick()
	CUIManager.ShowUI(CLuaUIResources.ChunJie2024FuYanBuyRankWnd)
end

--@endregion UIEvent
