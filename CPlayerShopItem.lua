-- Auto Generated!!
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local Boolean = import "System.Boolean"
local CCJBMgr = import "L10.Game.CCJBMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerShopItem = import "L10.UI.CPlayerShopItem"
local CPlayerShopMgr = import "L10.UI.CPlayerShopMgr"
local LocalString = import "LocalString"
local OnShelfStatus = import "L10.UI.OnShelfStatus"
local QnButton = import "L10.UI.QnButton"
local StringActionKeyValuePair = import "L10.Game.StringActionKeyValuePair"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CPlayerShopItem.m_OnEnable_CS2LuaHook = function (this) 
    if this.m_FocusCheckBox ~= nil then
        this.m_FocusCheckBox.OnValueChanged = CommonDefs.CombineListner_Action_bool(this.m_FocusCheckBox.OnValueChanged, MakeDelegateFromCSFunction(this.OnCheckBoxValueChanged, MakeGenericClass(Action1, Boolean), this), true)
    end
    if this.m_FocusCounterButton ~= nil then
        this.m_FocusCounterButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_FocusCounterButton.OnClick, MakeDelegateFromCSFunction(this.OnFocusCounterButtonClick, MakeGenericClass(Action1, QnButton), this), true)
    end
    UIEventListener.Get(this.m_ItemTexture.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_ItemTexture.gameObject).onClick, MakeDelegateFromCSFunction(this.OnItemTextureClick, VoidDelegate, this), true)
end
CPlayerShopItem.m_OnDisable_CS2LuaHook = function (this) 
    if this.m_FocusCheckBox ~= nil then
        this.m_FocusCheckBox.OnValueChanged = CommonDefs.CombineListner_Action_bool(this.m_FocusCheckBox.OnValueChanged, MakeDelegateFromCSFunction(this.OnCheckBoxValueChanged, MakeGenericClass(Action1, Boolean), this), true)
    end
    if this.m_FocusCounterButton ~= nil then
        this.m_FocusCounterButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_FocusCounterButton.OnClick, MakeDelegateFromCSFunction(this.OnFocusCounterButtonClick, MakeGenericClass(Action1, QnButton), this), false)
    end
    UIEventListener.Get(this.m_ItemTexture.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_ItemTexture.gameObject).onClick, MakeDelegateFromCSFunction(this.OnItemTextureClick, VoidDelegate, this), false)
    if this.m_LeftTimeTick ~= nil then
        UnRegisterTick(this.m_LeftTimeTick)
        this.m_LeftTimeTick = nil
    end
end
CPlayerShopItem.m_OnItemTextureClick_CS2LuaHook = function (this, go) 
    if this.m_CachedData ~= nil and not this.m_DisableClickTexture then
        CItemInfoMgr.ShowLinkItemInfo(this.m_CachedData.Item, true, this, AlignType.Default, 0, 0, 0, 0, 0)
    end
    this:SetSelected(true, false)
end
CPlayerShopItem.m_GetLabelTextByShelfStatus_CS2LuaHook = function (this, status) 
    repeat
        local default = status
        if default == OnShelfStatus.OnShelf then
            return LocalString.GetString("已上架")
        elseif default == OnShelfStatus.OnPublicity then
            return LocalString.GetString("公示期")
        elseif default == OnShelfStatus.OffShelf then
            return LocalString.GetString("已过期")
        end
    until 1
    return ""
end

CPlayerShopItem.m_OnReview_CS2LuaHook = function (this) 
    if this.m_CachedData ~= nil then
        local item = this.m_CachedData.Item

        if item ~= nil and item.IsItem then
            if item.Item.Type == EnumItemType_lua.UnEvaluateChuanjiabao or item.Item.Type == EnumItemType_lua.Chuanjiabao then
                CCJBMgr.Inst:OpenTipShow(item.Id, false)
            elseif item.Item.Type == EnumItemType_lua.Zhuangshiwu then
                CLuaZhuangshiwuPreviewMgr.OpenPreviewWnd(item.TemplateId)
            end
        end
    end
end

CPlayerShopItem.m_GetActionPairs_CS2LuaHook = function (this, itemId, templateId) 
    local actionPairs = CreateFromClass(MakeGenericClass(List, StringActionKeyValuePair))
    local reviewAction = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("预览"), MakeDelegateFromCSFunction(this.OnReview, Action0, this))
    local shareAction = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("分享"), MakeDelegateFromCSFunction(this.OnShare, Action0, this))

    CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), shareAction)

    local item = CItemMgr.Inst:GetById(itemId)
    if item == nil then
        local _item = CPlayerShopMgr.Inst:GetItemById(itemId)
        if _item ~= nil then
            item = _item.Item
        end
    end

    if item ~= nil and item.IsItem then
        if item.Item.Type == EnumItemType_lua.UnEvaluateChuanjiabao or item.Item.Type == EnumItemType_lua.Chuanjiabao then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), reviewAction)
        elseif item.Item.Type == EnumItemType_lua.Zhuangshiwu then
            CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), reviewAction)
        end
    end
    return actionPairs
end
