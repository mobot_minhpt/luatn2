local QnRadioBox = import "L10.UI.QnRadioBox"
local CButton = import "L10.UI.CButton"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CSocialWndMgr = import "L10.UI.CSocialWndMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local QnButton = import "L10.UI.QnButton"

LuaWuYi2023FYZXChooseTempSkillWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaWuYi2023FYZXChooseTempSkillWnd, "TopLabel", "TopLabel", UILabel)
RegistChildComponent(LuaWuYi2023FYZXChooseTempSkillWnd, "ChatButton", "ChatButton", GameObject)
RegistChildComponent(LuaWuYi2023FYZXChooseTempSkillWnd, "QnRadioBox", "QnRadioBox", QnRadioBox)
RegistChildComponent(LuaWuYi2023FYZXChooseTempSkillWnd, "BtnTemplate", "BtnTemplate", GameObject)
RegistChildComponent(LuaWuYi2023FYZXChooseTempSkillWnd, "OkButton", "OkButton", CButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaWuYi2023FYZXChooseTempSkillWnd, "m_CloseWndTick")
RegistClassMember(LuaWuYi2023FYZXChooseTempSkillWnd, "m_CloseWndTime")
RegistClassMember(LuaWuYi2023FYZXChooseTempSkillWnd, "m_SelectIndex")

function LuaWuYi2023FYZXChooseTempSkillWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ChatButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChatButtonClick()
	end)


	
	UIEventListener.Get(self.OkButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOkButtonClick()
	end)


    --@endregion EventBind end
end

function LuaWuYi2023FYZXChooseTempSkillWnd:Init()
	self.TopLabel.text = g_MessageMgr:FormatMessage("WuYi2023FYZXChooseTempSkillWnd_TopLabel")
	self:CancelCloseWndTick()
	self.OkButton.Text = SafeStringFormat3(LocalString.GetString("确定选择(%d)"), 30)
	self.m_CloseWndTime = LuaWuYi2023Mgr.m_TempSkillWndCloseTime
	self.m_CloseWndTick = RegisterTick(function ()
		local coutDownTime = self.m_CloseWndTime - CServerTimeMgr.Inst.timeStamp
		if coutDownTime > 0 then
			self.OkButton.Text = SafeStringFormat3(LocalString.GetString("确定选择(%d)"), coutDownTime)
		else
			self:CancelCloseWndTick()
			CUIManager.CloseUI(CLuaUIResources.WuYi2023FYZXChooseTempSkillWnd)
		end
	end,500)
	self:InitQnRadioBox()
end

function LuaWuYi2023FYZXChooseTempSkillWnd:InitQnRadioBox()
	self.BtnTemplate.gameObject:SetActive(false)
	local len = WuYi2023_TempSkill.GetDataCount()
	self.QnRadioBox.m_RadioButtons = CreateFromClass(MakeArrayClass(QnSelectableButton), len)
	for i = 1, len do
		local data = WuYi2023_TempSkill.GetData(i)
		local skillData = Skill_AllSkills.GetData(data.skillid * 100 + 1)
		local obj = NGUITools.AddChild(self.QnRadioBox.m_Table.gameObject, self.BtnTemplate.gameObject)
		obj:SetActive(true)
		local skillIcon = obj.transform:Find("Mask/Texture"):GetComponent(typeof(CUITexture))
		local nameLabel = obj.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
		local descLabel = obj.transform:Find("DescLabel"):GetComponent(typeof(UILabel))
		if skillData then
			skillIcon:LoadSkillIcon(skillData.SkillIcon)
		end
		nameLabel.text = data.name
		descLabel.text = data.desc
		local button = CommonDefs.GetComponent_GameObject_Type(obj, typeof(QnSelectableButton))
		self.QnRadioBox.m_RadioButtons[i - 1] = button
		self.QnRadioBox.m_RadioButtons[i - 1]:SetSelected(false, false)
		self.QnRadioBox.m_RadioButtons[i - 1].OnClick = MakeDelegateFromCSFunction(self.QnRadioBox.On_Click, MakeGenericClass(Action1, QnButton), self.QnRadioBox)
	end
	self.QnRadioBox.m_Table:Reposition()
	self.QnRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn, index)
		self:OnSelect(btn, index)
	end)
	self.QnRadioBox:ChangeTo(0, true)
end

function LuaWuYi2023FYZXChooseTempSkillWnd:OnDisable()
	self:CancelCloseWndTick()
	self:ChooseTempSkill()
end

function LuaWuYi2023FYZXChooseTempSkillWnd:CancelCloseWndTick()
	if self.m_CloseWndTick then
		UnRegisterTick(self.m_CloseWndTick)
		self.m_CloseWndTick = nil
	end
end

function LuaWuYi2023FYZXChooseTempSkillWnd:ChooseTempSkill()
	local data = WuYi2023_TempSkill.GetData(self.m_SelectIndex + 1)
	if data then
		Gac2Gas.FenYongZhenXianChooseSkill(data.skillid) 
	end
end

--@region UIEvent

function LuaWuYi2023FYZXChooseTempSkillWnd:OnChatButtonClick()
	CSocialWndMgr.ShowChatWnd(EChatPanel.Team)
end

function LuaWuYi2023FYZXChooseTempSkillWnd:OnOkButtonClick()
	CUIManager.CloseUI(CLuaUIResources.WuYi2023FYZXChooseTempSkillWnd)
end

function LuaWuYi2023FYZXChooseTempSkillWnd:OnSelect(btn, index)
	self.m_SelectIndex = index
end
--@endregion UIEvent

