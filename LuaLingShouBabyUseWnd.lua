require("common/common_include")
require("ui/lingshou/baby/LuaLingShouBabyUseItem")

local LuaGameObject=import "LuaGameObject"
local LingShou_Setting=import "L10.Game.LingShou_Setting"
local Constants=import "L10.Game.Constants"
local CItemMgr=import "L10.Game.CItemMgr"
--注入经验 RequestLingShouBabyTakeParentExp
CLuaLingShouBabyUseWnd=class()
RegistClassMember(CLuaLingShouBabyUseWnd,"m_InjectExpButton")
RegistClassMember(CLuaLingShouBabyUseWnd,"m_ItemTemplate")
-- RegistClassMember(CLuaLingShouBabyUseWnd,"m_ItemTable")
RegistClassMember(CLuaLingShouBabyUseWnd,"m_AnchorNode")

function CLuaLingShouBabyUseWnd:Init()
    local g = LuaGameObject.GetChildNoGC(self.transform,"InjectExpButton").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CLuaUIResources.LingShouBabyInjectExpWnd)
    end)
    self.m_AnchorNode=LuaGameObject.GetChildNoGC(self.transform,"Node").transform
    self.m_AnchorNode.localPosition = Vector3(590,180,0)

    self.m_ItemTemplate=LuaGameObject.GetChildNoGC(self.transform,"ItemTemplate").gameObject
    self.m_ItemTemplate:SetActive(false)

    -- self.m_ItemTable={}

    local grid=LuaGameObject.GetChildNoGC(self.transform,"Grid").grid
    local parent=grid.transform
    local itemIds = LingShou_Setting.GetData().FeedItemIds
    for i=1,itemIds.Length do
        local go=NGUITools.AddChild(grid.gameObject,self.m_ItemTemplate)
        go:SetActive(true)
        local item=CLuaLingShouBabyUseItem:new()
        item:Init(go.transform)
        item:InitData(itemIds[i-1])
        -- table.insert(self.m_ItemTable, item)
    end

    local bindItemCountInBag, notBindItemCountInBag=CItemMgr.Inst:CalcItemCountInBagByTemplateId(Constants.BabyFangShengExpItemId)
    local count=bindItemCountInBag+notBindItemCountInBag
    if count>0 then
        local go=NGUITools.AddChild(grid.gameObject,self.m_ItemTemplate)
        go:SetActive(true)
        local item=CLuaLingShouBabyUseItem:new()
        item:Init(go.transform)
        item:InitData(Constants.BabyFangShengExpItemId)
        -- table.insert(self.m_ItemTable, item)
    end
    
    grid:Reposition()
end


return CLuaLingShouBabyUseWnd