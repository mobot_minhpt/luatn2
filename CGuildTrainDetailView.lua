-- Auto Generated!!
local BoolDelegate = import "UIEventListener+BoolDelegate"
local CButton = import "L10.UI.CButton"
local CChatLinkMgr = import "CChatLinkMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CGuildTrainDetailView = import "L10.UI.CGuildTrainDetailView"
local CGuildTrainMgr = import "L10.Game.CGuildTrainMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local NGUITools = import "NGUITools"
local Practice_Practice = import "L10.Game.Practice_Practice"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local Time = import "UnityEngine.Time"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CGuildTrainDetailView.m_Init_CS2LuaHook = function (this, maxLevel, guildTrainInfo) 

    this.baseInfo = guildTrainInfo
    CGuildTrainMgr.Inst.baseInfo = this.baseInfo
    this.maxLevel = maxLevel
    local practiceSkillId = 0
    local trainId = 0
    if guildTrainInfo == nil then
        return
    end
    if CommonDefs.DictContains(CClientMainPlayer.Inst.SkillProp.PracticeLevel, typeof(UInt32), EnumToInt(guildTrainInfo.type)) and CommonDefs.DictGetValue(CClientMainPlayer.Inst.SkillProp.PracticeLevel, typeof(UInt32), EnumToInt(guildTrainInfo.type)) ~= 0 then
        trainId = guildTrainInfo.skillId * 100 + CommonDefs.DictGetValue(CClientMainPlayer.Inst.SkillProp.PracticeLevel, typeof(UInt32), EnumToInt(guildTrainInfo.type)) + 1
        practiceSkillId = trainId - 1
    else
        trainId = guildTrainInfo.skillId * 100 + 1
        practiceSkillId = trainId
    end
    this.practice = Practice_Practice.GetData(trainId)
    local pLevel = 0 local pInExperience = 0
    if this.practice ~= nil then
        this.progress.gameObject:SetActive(true)
        pLevel = this.practice.Level
        pInExperience = this.practice.InExperience
    else
        this.progress.gameObject:SetActive(false)
        local former = Practice_Practice.GetData(trainId - 1)
        if former ~= nil then
            pLevel = former.Level + 1
            pInExperience = 0
        end
    end

    this:InitProgress(pLevel, pInExperience)
    this:InitDesc(practiceSkillId)
    this:InitCost()
    this:InitButton()
end
CGuildTrainDetailView.m_InitProgress_CS2LuaHook = function (this, pLevel, pInExperience) 
    this.titlelabel.text = System.String.Format("{0}    lv.{1}", this.baseInfo.name, pLevel - 1)
    this.maxLevelLabel.text = System.String.Format(LocalString.GetString("等级上限 lv.{0}"), this.maxLevel)
    if CommonDefs.DictContains(CClientMainPlayer.Inst.SkillProp.PracticeExp, typeof(UInt32), EnumToInt(this.baseInfo.type)) then
        if pInExperience ~= 0 then
            this.progress.value = CommonDefs.DictGetValue(CClientMainPlayer.Inst.SkillProp.PracticeExp, typeof(UInt32), EnumToInt(this.baseInfo.type)) / pInExperience
        else
            this.progress.value = 0
        end
        this.progressValue.text = System.String.Format("{0}/{1}", CommonDefs.DictGetValue(CClientMainPlayer.Inst.SkillProp.PracticeExp, typeof(UInt32), EnumToInt(this.baseInfo.type)), pInExperience)
    else
        this.progress.value = 0
        this.progressValue.text = System.String.Format("0/{0}", pInExperience)
    end
end
CGuildTrainDetailView.m_TryShowFeiShengModifyLevel_CS2LuaHook = function (this, practiceSkillId) 
    if CClientMainPlayer.Inst == nil then
        return
    end

    local skillProp = CClientMainPlayer.Inst.SkillProp
    local practiceType = EnumToInt(this.baseInfo.type)
    local exist = CommonDefs.DictContains(skillProp.PracticeLevel, typeof(UInt32), practiceType) and CommonDefs.DictGetValue(CClientMainPlayer.Inst.SkillProp.PracticeLevel, typeof(UInt32), practiceType) > 0
    if exist then
        if CClientMainPlayer.Inst.IsInXianShenStatus then
            local level = CLuaDesignMgr.Practice_Practice_GetFeiShengModifyLevel(practiceSkillId, CClientMainPlayer.Inst.Level)

            local modifySkillLevel = math.floor(practiceSkillId / 100) * 100 + level
            if modifySkillLevel > 0 and level ~= practiceSkillId % 100 then
                local desc = ""
                local skill = Skill_AllSkills.GetData(modifySkillLevel)
                if skill ~= nil then
                    desc = CChatLinkMgr.TranslateToNGUIText(skill.Display, false)
                end
                this:AddDesc(System.String.Format(LocalString.GetString("[c][{0}]修正等级lv.{1}    {2}[-][/c]"), Constants.ColorOfFeiSheng, level, desc))
            end
        end
    end
end
CGuildTrainDetailView.m_InitDesc_CS2LuaHook = function (this, practiceSkillId) 
    Extensions.RemoveAllChildren(this.table.transform)
    this.descTitle:SetActive(false)
    this.descLabel:SetActive(false)


    local skillProp = CClientMainPlayer.Inst.SkillProp
    local praticeType = EnumToInt(this.baseInfo.type)
    local exist = CommonDefs.DictContains(skillProp.PracticeLevel, typeof(UInt32), praticeType) and CommonDefs.DictGetValue(CClientMainPlayer.Inst.SkillProp.PracticeLevel, typeof(UInt32), praticeType) > 0
    if exist then
        this:TryShowFeiShengModifyLevel(practiceSkillId)

        this:AddTitle(LocalString.GetString("当前"))
        local skill = Skill_AllSkills.GetData(practiceSkillId)
        this:AddDesc(CChatLinkMgr.TranslateToNGUIText(skill.Display, false))
    end

    local nextLevelSkillId = exist and practiceSkillId + 1 or practiceSkillId
    local nextLevelSkill = Skill_AllSkills.GetData(nextLevelSkillId)
    if nextLevelSkill ~= nil then
        this:AddTitle(LocalString.GetString("下一级"))
        this:AddDesc(CChatLinkMgr.TranslateToNGUIText(nextLevelSkill.Display, false))
        this:AddTitle(LocalString.GetString("修炼条件"))
        local levelColor = CClientMainPlayer.Inst.MaxLevel >= nextLevelSkill.PlayerLevel and "FFFFFF" or "FF0000"
        local friendColor = CClientMainPlayer.Inst.RelationshipProp.Friends.Count >= this.practice.FriendsAmount and "FFFFFF" or "FF0000"
        local contriColor = CClientMainPlayer.Inst.PlayProp.GuildData.ContributionTotal >= this.practice.RequiredContribution and "FFFFFF" or "FF0000"
        this:AddDesc(CommonDefs.String_Format_String_ArrayObject(LocalString.GetString("角色等级 lv.[c][{3}]{0}[-]      好友数量 [c][{4}]{1}[-]     历史帮贡 [c][{5}]{2}[-]"), {
            this.practice.PlayerLearnLv, 
            this.practice.FriendsAmount, 
            this.practice.RequiredContribution, 
            levelColor, 
            friendColor, 
            contriColor
        }))
    else
        this:AddDesc(LocalString.GetString("已达到修炼最大等级。"))
    end
    this.table:Reposition()
end
CGuildTrainDetailView.m_InitCost_CS2LuaHook = function (this) 
    local pMoney = 0 local pContribute = 0
    if this.practice ~= nil then
        pMoney = this.practice.InMoney
        pContribute = this.practice.InContribution
    end

    if CClientMainPlayer.Inst.FreeSilver > 0 then
        this.yinpiaoCost.text = tostring(pMoney)
        this.yinpiaoOwn.text = tostring(CClientMainPlayer.Inst.FreeSilver)
        this.yinpiaoCtrl:SetActive(true)
        this.yinliangCtrl.gameObject:SetActive(false)
        this.costTips:SetActive(CClientMainPlayer.Inst.FreeSilver < pMoney)
    else
        this.yinpiaoCtrl:SetActive(false)
        this.yinliangCtrl.gameObject:SetActive(true)
        this.yinliangCtrl:SetCost(pMoney)
    end
    this.banggongCtrl:SetCost(pContribute)
end
CGuildTrainDetailView.m_InitButton_CS2LuaHook = function (this) 
    if this.baseInfo.hasSelected then
        this.buttonLabel.text = LocalString.GetString("修炼一次")
        CommonDefs.GetComponent_GameObject_Type(this.button, typeof(CButton)).Enabled = this.practice ~= nil
        this.trainFullButtonLabel.text = LocalString.GetString("一键修炼")
        CommonDefs.GetComponent_GameObject_Type(this.trainFullButton, typeof(CButton)).Enabled = this.practice ~= nil
    else
        this.buttonLabel.text = LocalString.GetString("未开启")
        CommonDefs.GetComponent_GameObject_Type(this.button, typeof(CButton)).Enabled = false
        this.trainFullButtonLabel.text = LocalString.GetString("未开启")
        CommonDefs.GetComponent_GameObject_Type(this.trainFullButton, typeof(CButton)).Enabled = false
    end
end
CGuildTrainDetailView.m_AddTitle_CS2LuaHook = function (this, text) 
    local curInstance = NGUITools.AddChild(this.table.gameObject, this.descTitle)
    curInstance:SetActive(true)
    local curLabel = CommonDefs.GetComponent_GameObject_Type(curInstance, typeof(UILabel))
    if curLabel ~= nil then
        curLabel.text = text
    end
end
CGuildTrainDetailView.m_AddDesc_CS2LuaHook = function (this, text) 
    local curDesc = NGUITools.AddChild(this.table.gameObject, this.descLabel)
    curDesc:SetActive(true)
    local curDescLabel = CommonDefs.GetComponent_GameObject_Type(curDesc, typeof(UILabel))
    if curDescLabel ~= nil then
        curDescLabel.text = text
    end
end
CGuildTrainDetailView.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.button).onPress = CommonDefs.CombineListner_BoolDelegate(UIEventListener.Get(this.button).onPress, MakeDelegateFromCSFunction(this.OnTrainButtonPress, BoolDelegate, this), true)
    UIEventListener.Get(this.button).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.button).onClick, MakeDelegateFromCSFunction(this.OnTrainButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.trainFullButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.trainFullButton).onClick, MakeDelegateFromCSFunction(this.OnTrainFullButtonClick, VoidDelegate, this), true)
    EventManager.AddListener(EnumEventType.MainPlayerUpdateMoney, MakeDelegateFromCSFunction(this.OnUpdateMoney, Action0, this))
    EventManager.AddListener(EnumEventType.GuildTrainExpUpdate, MakeDelegateFromCSFunction(this.OnPlayerPropUpdate, Action0, this))
end
CGuildTrainDetailView.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.button).onPress = CommonDefs.CombineListner_BoolDelegate(UIEventListener.Get(this.button).onPress, MakeDelegateFromCSFunction(this.OnTrainButtonPress, BoolDelegate, this), false)
    UIEventListener.Get(this.button).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.button).onClick, MakeDelegateFromCSFunction(this.OnTrainButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.trainFullButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.trainFullButton).onClick, MakeDelegateFromCSFunction(this.OnTrainFullButtonClick, VoidDelegate, this), false)
    EventManager.RemoveListener(EnumEventType.MainPlayerUpdateMoney, MakeDelegateFromCSFunction(this.OnUpdateMoney, Action0, this))
    EventManager.RemoveListener(EnumEventType.GuildTrainExpUpdate, MakeDelegateFromCSFunction(this.OnPlayerPropUpdate, Action0, this))
end
CGuildTrainDetailView.m_DoTrain_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst == nil then
        return
    end
    if CClientMainPlayer.Inst.FreeSilver > 0 and CClientMainPlayer.Inst.FreeSilver < this.practice.InMoney then
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("SKILL_UPDATE_CONFIRM_MONEY_MIX", this.practice.InMoney - CClientMainPlayer.Inst.FreeSilver), DelegateFactory.Action(function () 
            CGuildTrainMgr.Inst:ConfirmRaceIfNeed(MakeDelegateFromCSFunction(this.Practice, Action0, this))
        end), nil, nil, nil, false)
    else
        CGuildTrainMgr.Inst:ConfirmRaceIfNeed(MakeDelegateFromCSFunction(this.Practice, Action0, this))
    end
end
CGuildTrainDetailView.m_Update_CS2LuaHook = function (this) 

    if CGuildTrainDetailView.OpenLongPressTrain and this.buttonPressed and Time.realtimeSinceStartup - this.btnPressTime > CGuildTrainDetailView.interval and CommonDefs.GetComponent_GameObject_Type(this.button, typeof(CButton)).Enabled then
        this.btnPressTime = Time.realtimeSinceStartup
        this:DoTrain()
    end
end
