-- Auto Generated!!
local CGuildHorseRaceMainPlayerTemplate = import "L10.UI.CGuildHorseRaceMainPlayerTemplate"
local CGuildHorseRaceMgr = import "L10.Game.CGuildHorseRaceMgr"
local NGUIText = import "NGUIText"
CGuildHorseRaceMainPlayerTemplate.m_InitRank_CS2LuaHook = function (this) 
    if this.info.rank > 0 then
        this.rankGo.gameObject:SetActive(true)
        if this.notFinished ~= nil then
            this.notFinished:SetActive(false)
        end
        this.rankLabel.text = tostring(this.info.rank)
        repeat
            local default = this.info.rank
            if default == (1) then
                this.rankGo.color = NGUIText.ParseColor(this.firstColor, 0)
                break
            elseif default == (2) then
                this.rankGo.color = NGUIText.ParseColor(this.secondColor, 0)
                break
            elseif default == (3) then
                this.rankGo.color = NGUIText.ParseColor(this.thirdColor, 0)
                break
            else
                this.rankGo.color = NGUIText.ParseColor(this.otherColor, 0)
                break
            end
        until 1
    else
        this.rankGo.gameObject:SetActive(false)
        if this:NeedShowNotFinish() then
            if this.notFinished ~= nil then
                this.notFinished:SetActive(true)
            end
        else
            if this.notFinished ~= nil then
                this.notFinished:SetActive(false)
            end
        end
    end
end
CGuildHorseRaceMainPlayerTemplate.m_NeedShowNotFinish_CS2LuaHook = function (this) 
    if CGuildHorseRaceMgr.Inst.MPTYSStatus == CGuildHorseRaceMgr.EnumMPTYSStatus.eDataHold then
        return true
    end
    -- 计时赛界面
    if CGuildHorseRaceMgr.Inst.mainStatus <= 3 and EnumToInt(CGuildHorseRaceMgr.Inst.MPTYSStatus) >= EnumToInt(CGuildHorseRaceMgr.EnumMPTYSStatus.ePreelection) then
        return this.info.isFinished == 0
    end
    -- 排名赛界面
    if CGuildHorseRaceMgr.Inst.mainStatus > 3 and EnumToInt(CGuildHorseRaceMgr.Inst.MPTYSStatus) >= EnumToInt(CGuildHorseRaceMgr.EnumMPTYSStatus.eFinals) then
        return this.info.isFinished == 0
    end
    return false
end
