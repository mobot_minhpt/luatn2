local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnTabView = import "L10.UI.QnTabView"
local QnTableView = import "L10.UI.QnTableView"
local UILabel = import "UILabel"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local AlignType = import "CPlayerInfoMgr+AlignType"
local Profession=import "L10.Game.Profession"
local EnumClass = import "L10.Game.EnumClass"
local QnTableItem=import "L10.UI.QnTableItem"
local CGuildSortButton = import "L10.UI.CGuildSortButton"

LuaJuDianBattleContributionWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaJuDianBattleContributionWnd, "ShowArea", "ShowArea", QnTabView)
RegistChildComponent(LuaJuDianBattleContributionWnd, "WeekTableView", "WeekTableView", QnTableView)
RegistChildComponent(LuaJuDianBattleContributionWnd, "SeasonTableView", "SeasonTableView", QnTableView)
RegistChildComponent(LuaJuDianBattleContributionWnd, "WeekBottomLabel", "WeekBottomLabel", UILabel)
RegistChildComponent(LuaJuDianBattleContributionWnd, "Table", "Table", QnRadioBox)

RegistClassMember(LuaJuDianBattleContributionWnd, "m_SortType")
RegistClassMember(LuaJuDianBattleContributionWnd, "m_SortFlags")
RegistClassMember(LuaJuDianBattleContributionWnd, "m_IsSeasonData")

--@endregion RegistChildComponent end

function LuaJuDianBattleContributionWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaJuDianBattleContributionWnd:Init()
    -- 请求数据
    Gac2Gas.GuildJuDianQueryContributionWeekData()
    Gac2Gas.GuildJuDianQueryContributionSeasonData()

    self.m_SortFlags = {}
    self.m_WeekData = {}
    self.m_SeasonData = {}

    self.ShowArea.OnSelect = DelegateFactory.Action_QnTabButton_int(function(btn, index)
        self.m_SortFlags = {}
        for i = 0, self.Table.m_RadioButtons.Length - 1 do
            self.Table.m_RadioButtons[i]:SetSelected(false, false)
        end
        if index == 0 then
            self.WeekTableView.gameObject:SetActive(true)
            self.WeekTableView:ReloadData(true,false)
            self.m_IsSeasonData = false
        else
            self.SeasonTableView.gameObject:SetActive(true)
            self.SeasonTableView:ReloadData(true,false)
            self.m_IsSeasonData = true
        end
    end)

    -- 赛季
    self.SeasonTableView.m_DataSource = DefaultTableViewDataSource.Create(function()
        return #self.m_SeasonData
    end, function(item, index)
        self:InitItem(item, index+1, false)
    end)

    -- 设置选中状态
    self.SeasonTableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
    end)

    -- 每周
    self.WeekTableView.m_DataSource = DefaultTableViewDataSource.Create(function()
        return #self.m_WeekData
    end, function(item, index)
        self:InitItem(item, index+1, true)
    end)

    -- 设置选中状态
    self.WeekTableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
    end)

    self.SeasonTableView:ReloadData(true,false)
    self.WeekTableView:ReloadData(true,false)

    self.WeekBottomLabel.text = LocalString.GetString("列表数据每小时刷新一次")

    -- 显示本周
    self.SeasonTableView.gameObject:SetActive(false)
    self.WeekTableView.gameObject:SetActive(true)

    
    self.Table.OnSelect = DelegateFactory.Action_QnButton_int(function(btn, index)
        self:OnTableSelect(btn, index)
    end)
end

function LuaJuDianBattleContributionWnd:InitItem(item, index, isWeek)
    -- list, 9个数据为一组，分别是  playerId playerName, class, level, xianfanStatus, 击杀数，提交数，棋局时间，战斗评价

    local playerNameLabel = item.transform:Find("PlayerNameLabel"):GetComponent(typeof(UILabel))
    local clzIcon = item.transform:Find("ClzIcon"):GetComponent(typeof(UISprite))
    local levelLabel = item.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
    local killLabel = item.transform:Find("KillLabel"):GetComponent(typeof(UILabel))
    local commitLabel = item.transform:Find("CommitLabel"):GetComponent(typeof(UILabel))
    local timeLabel = item.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
    local commentLabel = item.transform:Find("CommentLabel"):GetComponent(typeof(UILabel))

    local bg = item.transform:GetComponent(typeof(QnTableItem))
    if index %2 == 0 then
        bg.m_NormalSpirte = "common_bg_mission_background_n"
    else
        bg.m_NormalSpirte = "common_bg_mission_background_s"
    end

    local dataList = {}
    if isWeek then
        dataList = self.m_WeekData
    else
        dataList = self.m_SeasonData
    end
    
	-- 玩家自己的处理
    local isMy = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == dataList[index][1]
    item.transform:Find("My").gameObject:SetActive(isMy)
    local labelColor = isMy and Color.green or Color.white
    playerNameLabel.color = labelColor
    killLabel.color = labelColor
    commitLabel.color = labelColor
    timeLabel.color = labelColor
    commentLabel.color = labelColor

    playerNameLabel.text = dataList[index][2]
    clzIcon.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), dataList[index][3]))

    local xianFanStatus = dataList[index][5]
    if xianFanStatus and xianFanStatus > 0 then
        levelLabel.text = "[ff7900]lv." .. dataList[index][4]
    else
        levelLabel.text = "lv." .. dataList[index][4]
    end
    
    killLabel.text =  dataList[index][6]
    commitLabel.text = dataList[index][7]

    local time = dataList[index][8]
    if time>=3600 then
        timeLabel.text = SafeStringFormat3("[ACF9FF]%02d:%02d:%02d", math.floor(time / 3600),math.floor((time % 3600) / 60),  time % 60)
    else
        timeLabel.text = SafeStringFormat3("[ACF9FF]%02d:%02d", math.floor(time / 60), time % 60)
    end
    commentLabel.text = dataList[index][9]
end

function LuaJuDianBattleContributionWnd:ShowPlayerInfo(index, isWeek)
    local dataList = {}
    if isWeek then
        dataList = self.m_WeekData
    else
        dataList = self.m_SeasonData
    end

    local playerId = dataList[index][1]
    CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, "", nil, Vector3.zero, AlignType.Default)
end

function LuaJuDianBattleContributionWnd:OnSetData(data)
    local newdata = {}
    for i = 1, #data, 9 do
        local info = {}
        for j = 0, 8 do
            table.insert(info, data[i + j])
        end
        table.insert(newdata, info)
    end
    return newdata
end

function LuaJuDianBattleContributionWnd:OnSeasonData(data)
    self.m_SeasonData = self:OnSetData(data)
    self:SortList(self.m_SeasonData, nil)
    self.SeasonTableView:ReloadData(true,false)
end

function LuaJuDianBattleContributionWnd:OnWeekData(data)
    self.m_WeekData = self:OnSetData(data)
    self:SortList(self.m_WeekData, nil)
    self.WeekTableView:ReloadData(true,false)
end

function LuaJuDianBattleContributionWnd:OnTableSelect(btn, index)
    local sortButton = TypeAs(btn, typeof(CGuildSortButton))
    if self.m_SortFlags[index + 1] == nil then 
        self.m_SortFlags[index + 1] = -1 
    else
        self.m_SortFlags[index + 1] = -self.m_SortFlags[index + 1]
    end
    sortButton:SetSortTipStatus(self.m_SortFlags[index + 1] > 0)

    if self.m_IsSeasonData then
        self:SortList(self.m_SeasonData, index + 1)
        self.SeasonTableView:ReloadData(true,false)
    else
        self:SortList(self.m_WeekData, index + 1)
        self.WeekTableView:ReloadData(true,false)
    end
end

function LuaJuDianBattleContributionWnd:SortList(list, index)
    self.m_SortType = index
    -- data中数据一次为 1 ID, 2 名称，3 职业，4 等级，5 是否飞升，6 击杀妖魔数，7 灵气提交，8 棋局时间，9 个人评价
    local sortKeyArray = {3, 4, 6, 7, 8, 9} -- 仅罗列排序内容
    local flag = self.m_SortFlags[index]
    local myId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    table.sort(list,function (a, b)
        if myId == a[1] then 
            return true 
        elseif myId == b[1] then 
            return false 
        end
        if index and sortKeyArray[index] then
            local val = a[sortKeyArray[index]] - b[sortKeyArray[index]]
            if val == 0 then return a[1] < b[1] end
            return (val * flag) < 0
        else
            return a[1] < b[1]
        end
    end)
end

function LuaJuDianBattleContributionWnd:OnEnable()
    g_ScriptEvent:AddListener("GuildJuDianSendContributionSeasonData", self, "OnSeasonData")
    g_ScriptEvent:AddListener("GuildJuDianSendContributionWeekData", self, "OnWeekData")
end

function LuaJuDianBattleContributionWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GuildJuDianSendContributionSeasonData", self, "OnSeasonData")
    g_ScriptEvent:RemoveListener("GuildJuDianSendContributionWeekData", self, "OnWeekData")
end


--@region UIEvent

--@endregion UIEvent

