local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
LuaHanJiaMgr = class()

LuaHanJiaMgr.PetAdventureSignUp = false
LuaHanJiaMgr.PetAdventureLeftRewards = 0

LuaHanJiaMgr.PetAdventureInfoList = nil
LuaHanJiaMgr.PetAdventureSelfInfo = nil

LuaHanJiaMgr.PlayerFinalRank = 0
LuaHanJiaMgr.PlayerMonsterID = 0
LuaHanJiaMgr.PlayerAliveTime = 0
LuaHanJiaMgr.PlayerFinalInfo = nil

-------------
----2022 雪镜狂欢 BEGIN 
-------------
LuaHanJiaMgr.m_XueJingKuangHuanPlayId = nil
LuaHanJiaMgr.m_XueJingKuangHuanInfo = {}
LuaHanJiaMgr.m_XueJingKuangHuanResultInfo = {}
LuaHanJiaMgr.m_XueJingKuangHuanTempPlayInfo = {}
LuaHanJiaMgr.m_XueJingKuangHuanPlayInfo = {}

function LuaHanJiaMgr:GetXueJingKuangHuanPlayId()
    if self.m_XueJingKuangHuanPlayId == nil then
        self.m_XueJingKuangHuanPlayId = HanJia2022_XueJingKuangHuanSetting.GetData().GamePlayId
    end
    return self.m_XueJingKuangHuanPlayId
end

function LuaHanJiaMgr:ShowXueJingKuangHuanImageRuleWnd()
    local imagePaths = {
        "UI/Texture/NonTransparent/Material/xuejingkuanghuan_yindao_01.mat",
        "UI/Texture/NonTransparent/Material/xuejingkuanghuan_yindao_02.mat",
        "UI/Texture/NonTransparent/Material/xuejingkuanghuan_yindao_03.mat",
    }
    local msgs = {
        g_MessageMgr:FormatMessage("XueJingKuangHuan_Image_Rule_Desc_1"),
        g_MessageMgr:FormatMessage("XueJingKuangHuan_Image_Rule_Desc_2"),
        g_MessageMgr:FormatMessage("XueJingKuangHuan_Image_Rule_Desc_3"),
    }
    LuaImageRuleMgr:ShowCommonImageRuleWnd(imagePaths, msgs)
end

function LuaHanJiaMgr:OpenXueJingKuangHuanStartWnd()
    Gac2Gas.OpenXueJingKuangHuanSignUpWnd()
end

function LuaHanJiaMgr:RequestSignUpXueJingKuangHuan()
    Gac2Gas.RequestSignUpXueJingKuangHuan()
end

function LuaHanJiaMgr:CancelSignUpXueJingKuangHuan()
    Gac2Gas.RequestCancelSignUpXueJingKuangHuan()
end

function LuaHanJiaMgr:ChangeDirInXueJingKuangHuanMoving(x,z)
    Gac2Gas.ChangeDirInXueJingKuangHuanMoving(x, z)
end

function LuaHanJiaMgr:Gas2Gac_OpenXueJingKuangHuanSignUpWnd(bSignUp, dailyAwardTimes)
    self.m_XueJingKuangHuanInfo.bSignedUp = bSignUp
    self.m_XueJingKuangHuanInfo.dailyAwardTimes = dailyAwardTimes
    CUIManager.ShowUI("XueJingKuangHuanStartWnd")
end

function LuaHanJiaMgr:Gas2Gac_SyncXueJingKuangHuanSignUpResult(bSignUp)
    self.m_XueJingKuangHuanInfo.bSignedUp = bSignUp
    g_ScriptEvent:BroadcastInLua("OnSyncXueJingKuangHuanSignUpResult")
end

function LuaHanJiaMgr:Gas2Gac_SyncXueJingKuangHuanPlayInfo(infoUD)
    local info = MsgPackImpl.unpack(infoUD)
    if not info then return end
    local playInfoTbl = {}
    self.m_XueJingKuangHuanTempPlayInfo = {} --用于刷新头顶图标
    for i = 0, info.Count-1, 6 do
        local data = {
            rank = info[i],
            playerId = info[i+1],
            playerName = info[i+2],
		    class = info[i+3],
		    length = info[i+4],
            isDead = info[i+5],
            isMe = (CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == info[i+1]),
        }
        table.insert(playInfoTbl, data)
        if data.rank == 1 then 
            self.m_XueJingKuangHuanTempPlayInfo[data.playerId] = g_sprites.Common_Huangguan_01
        elseif data.rank == 2 then
            self.m_XueJingKuangHuanTempPlayInfo[data.playerId] = g_sprites.Common_Huangguan_02
        elseif data.rank == 3 then
            self.m_XueJingKuangHuanTempPlayInfo[data.playerId] = g_sprites.Common_Huangguan_03
        end
    end
    EventManager.BroadcastInternalForLua(EnumEventType.UpdatePlayerHeadInfoSepIcon, {true})
    table.sort(playInfoTbl, function(a,b)
        if a.isMe~=b.isMe then
            return a.isMe
        else
            return a.rank<b.rank
        end
    end)

    --如果数量超过5，则最多只显示5个（自己和其他四个玩家）
    if #playInfoTbl>5 then
        local tmp = {}
        for i=1,5 do
            table.insert(tmp, playInfoTbl[i])
        end
        playInfoTbl = tmp
    end
    self.m_XueJingKuangHuanPlayInfo = playInfoTbl
    g_ScriptEvent:BroadcastInLua("OnSyncXueJingKuangHuanPlayInfo", self.m_XueJingKuangHuanPlayInfo)
end

function LuaHanJiaMgr:Gas2Gac_ShowXueJingKuangHuanResultWnd(rank, finalLength, maxLength, historyMaxLength)
    self.m_XueJingKuangHuanResultInfo.rank = rank
    self.m_XueJingKuangHuanResultInfo.finalLength = finalLength
    self.m_XueJingKuangHuanResultInfo.maxLength = maxLength
    self.m_XueJingKuangHuanResultInfo.historyMaxLength = historyMaxLength
    CUIManager.ShowUI("XueJingKuangHuanResultWnd")

    self.m_XueJingKuangHuanTempPlayInfo = {} --清一下数据，避免下次进入副本根据上次残留显示异常
end
-------------
----2022 雪镜狂欢 END
-------------
--@region Gac2Gas

--@desc 萌宠大作战 同步报名结果
function Gas2Gac.SyncHanJiaAdventureSignUpResult(isSignUp)
    LuaHanJiaMgr.PetAdventureSignUp = isSignUp
    g_ScriptEvent:BroadcastInLua("SyncHanJiaAdventureSignUpResult")
end

--@desc 萌宠大作战 打开报名界面返回
function Gas2Gac.OpenHanJiaAdventureSignUpWnd(isSignUp, todayRewardTimes)
    LuaHanJiaMgr.PetAdventureSignUp = isSignUp
    LuaHanJiaMgr.PetAdventureLeftRewards = todayRewardTimes
    if CUIManager.IsLoaded(CLuaUIResources.PetAdventureStartWnd) then
        g_ScriptEvent:BroadcastInLua("SyncHanJiaAdventureSignUpResult")
    else
        CUIManager.ShowUI(CLuaUIResources.PetAdventureStartWnd)
    end
end

--@desc 萌宠大作战 副本中同步玩法信息(每秒)
function Gas2Gac.SyncHanJiaAdventurePlayInfo(infoUD)

    local info = MsgPackImpl.unpack(infoUD)
    if not info then return end

    LuaHanJiaMgr.PetAdventureInfoList = {}
    LuaHanJiaMgr.PetAdventureSelfInfo = {}

    for i = 0, info.Count-1, 7 do
        local rank = info[i]
        local playerId = info[i+1]
        local hpPercent = info[i+2]
        local aliveTime = info[i+3]
        local playerName = info[i+4]
        local isDead = info[i+5]
        local cls = info[i+6]
        local i = {
            rank = rank,
            playerId = playerId,
            hpPercent = hpPercent,
            aliveTime = aliveTime,
            playerName = playerName,
            isDead = isDead,
            cls = cls,
        }
        table.insert(LuaHanJiaMgr.PetAdventureInfoList, i)

        if playerId == CClientMainPlayer.Inst.Id then
            LuaHanJiaMgr.PetAdventureSelfInfo = i
        end
    end
    g_ScriptEvent:BroadcastInLua("SyncHanJiaAdventurePlayInfo")
end

LuaHanJiaMgr.PlayerName = nil
LuaHanJiaMgr.PlayerServerName = nil
LuaHanJiaMgr.PlayerPortraitName = nil
LuaHanJiaMgr.PlayerId = nil

--@desc 萌宠大作战 打开结算界面
function Gas2Gac.ShowHanJiaAdventureResultWnd(rank, aliveTime, monsterId, resultUD)
    
    -- 此处暂存mainplayer的信息，避免一打开界面就传送有可能取不到mainplayer信息的问题
    LuaHanJiaMgr.PlayerName = CClientMainPlayer.Inst.Name
    LuaHanJiaMgr.PlayerServerName = CClientMainPlayer.Inst:GetMyServerName()
    LuaHanJiaMgr.PlayerPortraitName = CClientMainPlayer.Inst.PortraitName
    LuaHanJiaMgr.PlayerId = CClientMainPlayer.Inst.Id

    LuaHanJiaMgr.PlayerFinalRank = rank
    LuaHanJiaMgr.PlayerMonsterID = monsterId
    LuaHanJiaMgr.PlayerAliveTime = aliveTime

    local info = MsgPackImpl.unpack(resultUD)

    if not info then return end

    LuaHanJiaMgr.PlayerFinalInfo = {}

    for i = 0, info.Count-1 , 7 do
        local rank = info[i]
        local playerId = info[i+1]
        local hpPercent = info[i+2]
        local aliveTime = info[i+3]
        local playerName = info[i+4]
        local isDead = info[i+5]
        local cls = info[i+6]

        local i = {
            rank = rank,
            playerId = playerId,
            hpPercent = hpPercent,
            aliveTime = aliveTime,
            playerName = playerName,
            isDead = isDead,
            cls = cls,
        }
    
        table.insert(LuaHanJiaMgr.PlayerFinalInfo, i)
    end
    
    table.sort(LuaHanJiaMgr.PlayerFinalInfo, function(a, b)
        return a.rank < b.rank
    end)

    CUIManager.ShowUI(CLuaUIResources.PetAdventureResultWnd)
    
end

--@endregion


local CScheduleMgr = import "L10.Game.CScheduleMgr"

LuaHanJiaMgr.TcjhMainData = {}
LuaHanJiaMgr.TcjhMainCfg = nil
LuaHanJiaMgr.TcjhRewardData = nil

--@desc: 服务器同步活动界面的红点信息
function Gas2Gac.SyncTianChengHasReward(hasLevelReward, hasTaskReward)
    local isred = hasLevelReward or hasTaskReward
    if isred then
        CScheduleMgr.Inst:SetAlertState(42010059,CScheduleMgr.EnumAlertState.Show,true)
    else
        CScheduleMgr.Inst:SetAlertState(42010059,CScheduleMgr.EnumAlertState.Hide,true)
    end
end

--[[
    @desc: 天成酒壶同步信息
    author:{author}
    time:2020-12-30 14:53:28
    --@curLevel:
	--@curJiuQi:
	--@state:
	--@rewardedLevel:
	--@rewardedVipLevel:
	--@levelLimit:
	--@taskUD: 
    @return:
]]
function Gas2Gac.SyncTianChengData(curLevel, curJiuQi, state, rewardedLevel, rewardedVipLevel, levelLimit, taskUD)
    local tasks = {}
    local taskInfoList = MsgPackImpl.unpack(taskUD)
    if taskInfoList then
        for i = 0, taskInfoList.Count - 1, 3 do
            tasks[#tasks+1] = {
                TaskID      = taskInfoList[i],
                Progress    = taskInfoList[i+1],
                IsRewarded  = taskInfoList[i+2]
            }
        end
    end
    LuaHanJiaMgr.SyncTianChengData(curLevel, curJiuQi, state, rewardedLevel, rewardedVipLevel, tasks,levelLimit)
end

function LuaHanJiaMgr.SyncTianChengData(curLevel, curJiuQi, state,rewardedLevel, rewardedVipLevel, tasks,lvlimit)
    local data = {}
    data.Lv = curLevel          --当前等级
    data.Exp = curJiuQi         --经验值，从1级开始的总值
    data.PassType = state       --0 免费通行证, 1 高级通行证, 2 高级通行证豪华版

    data.RewardNmlLv = rewardedLevel        --已经领取的普通的奖励等级
    data.RewardAdvLv = rewardedVipLevel     --已经领取的高级的奖励等级

    data.TaskData = tasks       --任务数组，{TaskID,Progress,IsRewarded}
    data.MaxLv = lvlimit

    LuaHanJiaMgr.LoadCfg(false)
    
    LuaHanJiaMgr.TcjhMainData = data
    LuaHanJiaMgr.TcjhMainData.Cfg = LuaHanJiaMgr.TcjhMainCfg.Datas
    LuaHanJiaMgr.TcjhMainData.CfgMaxLv = LuaHanJiaMgr.TcjhMainCfg.CfgMaxLv
    
    if CUIManager.IsLoaded(CLuaUIResources.TcjhMainWnd) then
        g_ScriptEvent:BroadcastInLua("OnTcjhDataChange")
    else
        CUIManager.ShowUI(CLuaUIResources.TcjhMainWnd)
    end
end

function LuaHanJiaMgr.LoadCfg(forceupdate)
    if LuaHanJiaMgr.TcjhMainCfg == nil or forceupdate then
        local maxlv = 0
        local cfg = {}               --表数据
        HanJia2021_TianChengLevelReward.Foreach(function (key, item)
            cfg[key] = {
                NeedJiuQi   = item.NeedJiuQi,
                Name        = item.Name,
                Icon        = item.Icon,
                NmlItems = LuaHanJiaMgr.GetItems(item.ItemRewards),
                AdvItems = LuaHanJiaMgr.GetItems(item.VipItemRewards)
            }
            maxlv = key
        end)
        LuaHanJiaMgr.TcjhMainCfg = {}
        LuaHanJiaMgr.TcjhMainCfg.Datas = cfg
        LuaHanJiaMgr.TcjhMainCfg.CfgMaxLv = maxlv
    end
end

function LuaHanJiaMgr.GetItems(str)
    local items = {}
    local op = System.StringSplitOptions.RemoveEmptyEntries
    local splits = Table2ArrayWithCount({";"}, 1, MakeArrayClass(System.String))
    local splits2 = Table2ArrayWithCount({","}, 1, MakeArrayClass(System.String))
    local nmlstrs = CommonDefs.StringSplit_ArrayChar_StringSplitOptions(str, splits, op)
    for i=0,nmlstrs.Length do
        if not System.String.IsNullOrEmpty(nmlstrs[i]) then
            local strs = CommonDefs.StringSplit_ArrayChar_StringSplitOptions(nmlstrs[i], splits2, op)
            local len = strs.Length
            if len == 3 then
                items[i+1]={ItemID = strs[0],Count = strs[1],IsBind=strs[2]}
            end
        end
    end
    return items
end

--[[
    @desc: 天成酒壶等级奖励信息
    author:{author}
    time:2020-12-30 14:54:12
    --@level:
	--@state:
	--@levelLimit:
	--@getItems: 
    @return:
]]
function Gas2Gac.TianChengLevelRewardResult(level, state, levelLimit, getItems)
    local items = {}
    local getItemList = MsgPackImpl.unpack(getItems)
    for i = 0, getItemList.Count - 1, 2 do
        items[#items+1] =  {ItemID = getItemList[i],Count = getItemList[i+1]}
    end

    local lv = level
    local passtype = state
    LuaHanJiaMgr.SyncRewardDatas(items,passtype,lv,levelLimit)
end

function LuaHanJiaMgr.SyncRewardDatas(items,passType,lv,lvlimit)
    LuaHanJiaMgr.LoadCfg(false)

    LuaHanJiaMgr.TcjhRewardData = {}
    LuaHanJiaMgr.TcjhRewardData.Items = items
    LuaHanJiaMgr.TcjhRewardData.PassType = passType
    LuaHanJiaMgr.TcjhRewardData.Lv = lv
    LuaHanJiaMgr.TcjhRewardData.LockItems = nil

    LuaHanJiaMgr.TcjhRewardData.BtnAction = function()
        if passType ~= 0 then
            CUIManager.ShowUI(CLuaUIResources.TcjhBuyLevelWnd)
        else
            CUIManager.ShowUI(CLuaUIResources.TcjhBuyAdvPassWnd)
        end
    end

    LuaHanJiaMgr.TcjhRewardData.CloseAction= function()
        Gac2Gas.QueryTianChengData()
    end

    if passType == 0 then
        local advitemdatas = {}
        local advitemcounts = {}
        local cfg = LuaHanJiaMgr.TcjhMainCfg.Datas      
        for i=1,lv do
            local advitems = cfg[i].AdvItems
            if advitems ~= nil then
                LuaHanJiaMgr.AddItem(advitems[1],advitemdatas,advitemcounts) 
                LuaHanJiaMgr.AddItem(advitems[2],advitemdatas,advitemcounts) 
            end
        end
        if #advitemdatas > 0 then
            LuaHanJiaMgr.TcjhRewardData.LockItems = {}
            for i=1,#advitemdatas+1 do
                local itemid = advitemdatas[i].ItemID
                LuaHanJiaMgr.TcjhRewardData.LockItems[i]={
                    Data = advitemdatas[i],
                    Count = advitemcounts[itemid]
                }
            end
        end
    end

    LuaHanJiaMgr.TcjhMainData.MaxLv = lvlimit
    CUIManager.ShowUI(CLuaUIResources.TcjhRewardWnd)
end

function LuaHanJiaMgr.AddItem(item,items,itemids)
    if item == nil then return end
    if itemids[item.ItemID] == nil then
        itemids[item.ItemID] = item.Count
        items[#items+1] = item
    else
        itemids[item.ItemID] = itemids[item.ItemID] + item.Count
    end
end

----------------HanJia2022-----------

function LuaHanJiaMgr.AddLockItems(itype,lv,advitemdatas,advitemcounts)
    local cfg = LuaHanJiaMgr.TcjhMainCfg.Datas
    for i=1,lv do
        local advitems = nil
        if itype == 1 then 
            advitems = cfg[i].AdvItems
        elseif itype == 2 then 
            advitems = cfg[i].AdvItems2
        end
        if advitems ~= nil then
            LuaHanJiaMgr.AddItem(advitems[1],advitemdatas,advitemcounts) 
        end
    end
end

function LuaHanJiaMgr.TsjzShowReward(items,passType,lv,lvlimit)
    LuaHanJiaMgr.LoadTsjzCfg(false)

    LuaHanJiaMgr.TcjhRewardData = {}
    LuaHanJiaMgr.TcjhRewardData.Items = items
    LuaHanJiaMgr.TcjhRewardData.PassType = passType
    LuaHanJiaMgr.TcjhRewardData.Lv = lv

    LuaHanJiaMgr.TcjhRewardData.Des = ""
    LuaHanJiaMgr.TcjhRewardData.BtnName = ""
    LuaHanJiaMgr.TcjhRewardData.BtnAction = nil
    LuaHanJiaMgr.TcjhRewardData.LockItems = nil

    if passType ~= 3 then 
        LuaHanJiaMgr.TcjhRewardData.BtnName = LocalString.GetString("解锁高级画轴")
        local advitemdatas = {}
        local advitemcounts = {}
        if passType == 0 then
            LuaHanJiaMgr.TcjhRewardData.Des = g_MessageMgr:FormatMessage("TCJH_UNLOCK_DES_1")
            LuaHanJiaMgr.AddLockItems(1,lv,advitemdatas,advitemcounts)
            LuaHanJiaMgr.AddLockItems(2,lv,advitemdatas,advitemcounts)
        elseif passType == 1 then
            LuaHanJiaMgr.TcjhRewardData.Des = g_MessageMgr:FormatMessage("TCJH_UNLOCK_DES_2")
            LuaHanJiaMgr.AddLockItems(2,lv,advitemdatas,advitemcounts)
        elseif passType == 2 then
            LuaHanJiaMgr.TcjhRewardData.Des = g_MessageMgr:FormatMessage("TCJH_UNLOCK_DES_3")
            LuaHanJiaMgr.AddLockItems(1,lv,advitemdatas,advitemcounts)
        end

        if #advitemdatas > 0 then
            LuaHanJiaMgr.TcjhRewardData.LockItems = {}
            for i=1,#advitemdatas do
                local itemid = advitemdatas[i].ItemID
                LuaHanJiaMgr.TcjhRewardData.LockItems[i]={
                    Data = advitemdatas[i],
                    Count = advitemcounts[itemid]
                }
            end
        end

        LuaHanJiaMgr.TcjhRewardData.BtnAction = function()
            if passType == 3 then
                CUIManager.ShowUI(CLuaUIResources.TcjhBuyLevelWnd)
            else
                CUIManager.ShowUI(CLuaUIResources.TsjzBuyAdvPassWnd)
            end
        end
    end
    
    LuaHanJiaMgr.TcjhRewardData.CloseAction= function()
        LuaHanJiaMgr.RequireTsjzData()
    end

    LuaHanJiaMgr.TcjhMainData.MaxLv = lvlimit
    CUIManager.ShowUI(CLuaUIResources.TcjhRewardWnd)
end

function LuaHanJiaMgr.LoadTsjzCfg(forceupdate)
    if LuaHanJiaMgr.TcjhMainCfg == nil or forceupdate then
        local maxlv = 0
        local cfg = {}               --表数据
        HanJia2022_TianShuoLevelReward.Foreach(function (key, item)
             cfg[key] = {
                 NeedJiuQi   = item.NeedJingCui,
                 NmlItems = LuaHanJiaMgr.GetItems(item.ItemRewards1),
                 AdvItems = LuaHanJiaMgr.GetItems(item.ItemRewards2),
                 AdvItems2 = LuaHanJiaMgr.GetItems(item.ItemRewards3)
             }
             maxlv = key
        end)
        LuaHanJiaMgr.TcjhMainCfg = {}
        LuaHanJiaMgr.TcjhMainCfg.Datas = cfg
        LuaHanJiaMgr.TcjhMainCfg.CfgMaxLv = maxlv
    end
end

function LuaHanJiaMgr.SyncTjszData(level, jingcui, state, rewardedLevel, rewardedVip1Level, rewardedVip2Level,rewardLevelLimit,tasks)
    local data = {}
    data.Lv = level          --当前等级
    data.Exp = jingcui         --经验值，从1级开始的总值
    data.PassType = state       --0 免费通行证, 1 高级通行证, 2 高级通行证豪华版

    data.RewardNmlLv = rewardedLevel        --已经领取的普通的奖励等级
    data.RewardAdvLv = rewardedVip1Level     --已经领取的高级的奖励等级
    data.RewardAdvLv2 = rewardedVip2Level

    data.TaskData = tasks       --任务数组，{TaskID,Progress,IsRewarded}
    data.MaxLv = rewardLevelLimit

    LuaHanJiaMgr.LoadTsjzCfg(false)
    
    local passtypeChangeTo = -1
    
    if LuaHanJiaMgr.TcjhMainData then
        local curtype = LuaHanJiaMgr.TcjhMainData.PassType
        if  curtype ~= data.PassType then
            if LuaHanJiaMgr.TcjhMainData.PassType == 0 then
                passtypeChangeTo = data.PassType
            elseif LuaHanJiaMgr.TcjhMainData.PassType == 1 then
                passtypeChangeTo = 2
            elseif LuaHanJiaMgr.TcjhMainData.PassType == 2 then
                passtypeChangeTo = 1
            end 
        end
    end
    LuaHanJiaMgr.TcjhMainData = data
    LuaHanJiaMgr.TcjhMainData.Cfg = LuaHanJiaMgr.TcjhMainCfg.Datas
    LuaHanJiaMgr.TcjhMainData.CfgMaxLv = LuaHanJiaMgr.TcjhMainCfg.CfgMaxLv
    
    if CUIManager.IsLoaded(CLuaUIResources.TsjzMainWnd) then
        g_ScriptEvent:BroadcastInLua("OnTsjzDataChange")
        if passtypeChangeTo > 0 then
            g_ScriptEvent:BroadcastInLua("OnTsjzPassTypeChange",passtypeChangeTo)
        end
    else
        CUIManager.ShowUI(CLuaUIResources.TsjzMainWnd)
    end
end

--@region Network

--请求天朔数据
function LuaHanJiaMgr.RequireTsjzData()
    Gac2Gas.QueryTianShuoData()
end

function LuaHanJiaMgr.RequireTsjzTaskReward(taskid)
    Gac2Gas.RequestGetTianShuoTaskReward(taskid)
end

function LuaHanJiaMgr.RequireTsjzBuyLevel(level)
    Gac2Gas.RequestBuyTianShuoLevel(level)
end

--天朔数据返回
function Gas2Gac.SyncTianShuoData(level, jingcui, state, rewardedLevel, rewardedVip1Level, rewardedVip2Level, rewardLevelLimit, taskUD)
    local tasks = {}
	local taskInfoList = MsgPackImpl.unpack(taskUD)
    if taskInfoList then
        for i = 0, taskInfoList.Count - 1, 3 do
            tasks[#tasks+1] = {
                TaskID      = taskInfoList[i],
                Progress    = taskInfoList[i+1],
                IsRewarded  = taskInfoList[i+2]
			}
        end
    end

    LuaHanJiaMgr.SyncTjszData(level, jingcui, state, rewardedLevel, rewardedVip1Level, rewardedVip2Level,rewardLevelLimit,tasks)
end

function LuaHanJiaMgr.RequireTsjzLevelReward(level)
    Gac2Gas.RequestGetTianShuoLevelReward(level)
end

function Gas2Gac.TianShuoLevelRewardResult(level, vip1Level, vip2Level, state, rewardLevelLimit, itemsUD) 
	local items = {}
    local getItemList = MsgPackImpl.unpack(itemsUD)
    for i = 0, getItemList.Count - 1, 2 do
        items[#items+1] =  {ItemID = getItemList[i],Count = getItemList[i+1]}
    end
    local lv = level
    local passtype = state
    LuaHanJiaMgr.TsjzShowReward(items,passtype,lv,rewardLevelLimit)
end

function Gas2Gac.SyncTianShuoHasReward(hasLevelReward, hasTaskReward)
	local isred = hasLevelReward or hasTaskReward
    if isred then
        CScheduleMgr.Inst:SetAlertState(42010102,CScheduleMgr.EnumAlertState.Show,true)
    else
        CScheduleMgr.Inst:SetAlertState(42010102,CScheduleMgr.EnumAlertState.Hide,true)
    end
end


--@endregion




-------------
----RPC回调
-------------
