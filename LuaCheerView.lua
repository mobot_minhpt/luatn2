local CGameVideoMgr = import "L10.Game.CGameVideoMgr"
local DelegateFactory  = import "DelegateFactory"
local UILongPressButton = import "L10.UI.UILongPressButton"
local CUIFx = import "L10.UI.CUIFx"
local Time = import "UnityEngine.Time"

LuaCheerView = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaCheerView, "m_CheerButton", "CheerButton", UILongPressButton)
RegistChildComponent(LuaCheerView, "m_CheerButtonFx", "CheerButtonFx", CUIFx)
RegistChildComponent(LuaCheerView, "m_TopCheerFx1", "TopCheerFx1", CUIFx)
RegistChildComponent(LuaCheerView, "m_TopCheerFx2", "TopCheerFx2", CUIFx)
RegistChildComponent(LuaCheerView, "m_TopCheerNextLevelFx1", "TopCheerNextLevelFx1", CUIFx)
RegistChildComponent(LuaCheerView, "m_TopCheerNextLevelFx2", "TopCheerNextLevelFx2", CUIFx)
RegistChildComponent(LuaCheerView, "m_CheerButtonFx2", "CheerButtonFx2", CUIFx)
--@endregion RegistChildComponent end
RegistClassMember(LuaCheerView,"m_LastZhuWeiValList")
RegistClassMember(LuaCheerView,"m_Tick")
RegistClassMember(LuaCheerView,"m_FireVal")
RegistClassMember(LuaCheerView,"m_UpArr")
RegistClassMember(LuaCheerView,"m_LastLongPressDelta")

function LuaCheerView:Awake()
    --@region EventBind: Dont Modify Manually!
    self:InitCheerButton()
    --@endregion EventBind end
end

function LuaCheerView:OnEnable()
    self.m_LastLongPressDelta = UILongPressButton.longPressDelta
    UILongPressButton.longPressDelta = 0.5
    g_ScriptEvent:AddListener("OnStarBiwuFightZhuWeiInfo",self,"OnStarBiwuFightZhuWeiInfo")
    g_ScriptEvent:AddListener("OnStarBiwuRequestZhuWei",self,"OnStarBiwuRequestZhuWei")
end

function LuaCheerView:OnDisable()
    UILongPressButton.longPressDelta = self.m_LastLongPressDelta
    CLuaFightingSpiritMgr.m_CheerIndex = -1
    LuaTweenUtils.DOKill(self.m_TopCheerFx1.transform, false)
    LuaTweenUtils.DOKill(self.m_TopCheerFx2.transform, false)
    g_ScriptEvent:RemoveListener("OnStarBiwuFightZhuWeiInfo",self,"OnStarBiwuFightZhuWeiInfo")
    g_ScriptEvent:RemoveListener("OnStarBiwuRequestZhuWei",self,"OnStarBiwuRequestZhuWei")
end
--@region UIEvent

--@endregion UIEvent
function LuaCheerView:InitCheerButton()
    self.m_CheerButton.OnLongPressDelegate = DelegateFactory.Action(function()
        self:OnCheerButtonLongPress()
    end)
    UIEventListener.Get(self.m_CheerButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnCheerButtonClick()
    end)
    self.m_CheerButton.gameObject:SetActive(CLuaStarBiwuMgr:IsInStarBiwuWatch() and not CGameVideoMgr.Inst:IsVideoMode())
end

function LuaCheerView:Update()
    self:UpdateFire()
end

function LuaCheerView:UpdateFire()
    if not self.m_FireVal then return end
    if not self.m_UpArr then return end
    local speed = 1
    if self.m_FireVal < 40 then speed = 10 end
    if #self.m_UpArr == 0 then
        self.m_FireVal = self.m_FireVal - StarBiWuShow_Setting.GetData().ZhuWeiFireDown * Time.deltaTime * speed
        if self.m_FireVal < 5 then 
            self.m_FireVal = 0
        end
    end
    local newArr = {}
    for k, v in pairs(self.m_UpArr) do
        if v > 0 then
            table.insert(newArr, v - Time.deltaTime)
        end
        self.m_FireVal =  self.m_FireVal + StarBiWuShow_Setting.GetData().ZhuWeiFireUp * Time.deltaTime * speed
    end
    self.m_UpArr = newArr
    self.m_FireVal = math.min(100, math.max(0,self.m_FireVal))
    self.m_CheerButtonFx.transform.localScale = Vector3(1,0.01 * self.m_FireVal,1)
end

function LuaCheerView:OnCheerButtonLongPress()
    CUIManager.ShowUI(CLuaUIResources.FightingSpiritCheerWnd)
end

function LuaCheerView:OnCheerButtonClick()
    if CLuaFightingSpiritMgr.m_CheerIndex == -1 then
        CUIManager.ShowUI(CLuaUIResources.FightingSpiritCheerWnd)
    else
        Gac2Gas.StarBiwuRequestZhuWei(CLuaFightingSpiritMgr.m_CheerIndex)
        local fxPaths = {"fx/ui/prefab/UI_zhuwei_Blue.prefab","fx/ui/prefab/UI_zhuwei_Red.prefab","fx/ui/prefab/UI_zhuwei_Blend.prefab"}
        self.m_CheerButtonFx:LoadFx(fxPaths[CLuaFightingSpiritMgr.m_CheerIndex + 1])
        self.m_CheerButtonFx2:DestroyFx()
        self.m_CheerButtonFx2:LoadFx("fx/ui/prefab/UI_juejie_anniudianji.prefab")
        if not self.m_UpArr then self.m_UpArr = {} end
        table.insert(self.m_UpArr,StarBiWuShow_Setting.GetData().ZhuWeiFireTime)
    end
end

function LuaCheerView:OnStarBiwuFightZhuWeiInfo(f1,v1,f2,v2,f3,v3)
    if not self.m_LastZhuWeiValList then
        self.m_LastZhuWeiValList = {0.5, 0.5}
    end
    local arr = {[f1] = v1,[f2] = v2,[f3] = v3}
    local leftVal = arr[EnumCommonForce_lua.eDefend] + arr[EnumCommonForce_lua.eNeutral] / 2
    local rightVal= arr[EnumCommonForce_lua.eAttack] + arr[EnumCommonForce_lua.eNeutral] / 2
    if leftVal > 0  then
        self.m_TopCheerFx1:LoadFx("fx/ui/prefab/UI_zhuweijiacheng_blue01.prefab")
    end
    if rightVal > 0  then
        self.m_TopCheerFx2:LoadFx("fx/ui/prefab/UI_zhuweijiacheng_red01.prefab")
    end
    local formulaId = StarBiWuShow_Setting.GetData().ZhuWeiFormula
    local formula = AllFormulas.Action_Formula[formulaId] and AllFormulas.Action_Formula[formulaId].Formula
    if formula then
        local leftVal = formula(nil, nil, {leftVal})
        local rightVal = formula(nil, nil, {rightVal})
        local nextLevelEffect = StarBiWuShow_Setting.GetData().NextLevelEffect
        local inteval = StarBiWuShow_Setting.GetData().ZhuweiTickInteval
        local tween1 = LuaTweenUtils.TweenFloat(self.m_LastZhuWeiValList[1], leftVal, inteval, function (val)
            for i = 0, nextLevelEffect.Length - 1 do
                local v = nextLevelEffect[i]
                if self.m_TopCheerFx1.transform.localScale.x <= v and v <= val then
                    self.m_TopCheerNextLevelFx1:DestroyFx()
                    self.m_TopCheerNextLevelFx1:LoadFx("fx/ui/prefab/UI_zhuweijiacheng_bluelizi.prefab")
                    break
                end
            end
            self.m_TopCheerFx1.transform.localScale = Vector3(val,1,1)
            self.m_TopCheerFx1.gameObject:SetActive(val > 0)
        end)
        LuaTweenUtils.DOKill(self.m_TopCheerFx1.transform, false)
        LuaTweenUtils.SetTarget(tween1, self.m_TopCheerFx1.transform)
        local tween2 = LuaTweenUtils.TweenFloat(self.m_LastZhuWeiValList[2], rightVal, inteval, function (val)
            local isShowFx2 = false
            for i = 0, nextLevelEffect.Length - 1 do
                local v = nextLevelEffect[i]
                if self.m_TopCheerFx2.transform.localScale.x <= v and v <= val then
                    isShowFx2 = true
                    break
                end
            end
            if isShowFx2 then
                self.m_TopCheerNextLevelFx2:DestroyFx()
                self.m_TopCheerNextLevelFx2:LoadFx("fx/ui/prefab/UI_zhuweijiacheng_redlizi.prefab")
            end
            self.m_TopCheerFx2.transform.localScale = Vector3(val,1,1)
            self.m_TopCheerFx2.gameObject:SetActive(val > 0)
        end)
        LuaTweenUtils.DOKill(self.m_TopCheerFx2.transform, false)
        LuaTweenUtils.SetTarget(tween2, self.m_TopCheerFx2.transform)
        self.m_LastZhuWeiValList = {leftVal, rightVal}
    end
end

function LuaCheerView:OnStarBiwuRequestZhuWei()
    if CLuaFightingSpiritMgr.m_CheerIndex == -1 then return end
    local btnTexture = self.m_CheerButton:GetComponent(typeof(CUITexture))
    local matPaths = {"UI/Texture/Transparent/Material/fightingspiritcheerwnd_zhuwei_blue.mat","UI/Texture/Transparent/Material/fightingspiritcheerwnd_zhuwei_red.mat","UI/Texture/Transparent/Material/fightingspiritcheerwnd_zhuwei_zhongli.mat"}
    btnTexture:LoadMaterial(matPaths[CLuaFightingSpiritMgr.m_CheerIndex + 1])
    self.m_CheerButtonFx:DestroyFx()
    self.m_CheerButtonFx2:DestroyFx()
    self.m_FireVal = 0
    self.m_UpArr = {}
end