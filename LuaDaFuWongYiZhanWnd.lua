local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"

LuaDaFuWongYiZhanWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaDaFuWongYiZhanWnd, "DesLabel", "DesLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongYiZhanWnd, "m_PlayerHead")
function LuaDaFuWongYiZhanWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_PlayerHead = self.transform:Find("Anchor/Content/PlayerHead").gameObject
end

function LuaDaFuWongYiZhanWnd:Init()
    self.DesLabel.text = g_MessageMgr:FormatMessage("DaFuWeng_YiZhanDes")
    SoundManager.Inst:PlayOneShot("event:/L10/L10_UI/DaFuWeng/UI_DaFuWeng_YiZhan", Vector3.zero, nil, 0)
    local totalTime = DaFuWeng_Countdown.GetData(EnumDaFuWengStage.RandomEventPostHouse).Time 
    local CountDown = totalTime
    local endFunc = function() CUIManager.CloseUI(CLuaUIResources.DaFuWongYiZhanWnd) end
	local durationFunc = function(time) end
	LuaDaFuWongMgr:StartCountDownTick(EnumDaFuWengCountDownStage.RandomEvent,CountDown,durationFunc,endFunc)
    self:ShowPlayerHead()
end
function LuaDaFuWongYiZhanWnd:OnStageUpdate(CurStage)
    if CurStage ~= EnumDaFuWengStage.RandomEventPostHouse then
        CUIManager.CloseUI(CLuaUIResources.DaFuWongYiZhanWnd)
    end
end

function LuaDaFuWongYiZhanWnd:ShowPlayerHead()
    if LuaDaFuWongMgr.IsMyRound then
        self.m_PlayerHead.gameObject:SetActive(false)
    else
        self.m_PlayerHead.gameObject:SetActive(true)
        local playerData = LuaDaFuWongMgr.PlayerInfo and LuaDaFuWongMgr.PlayerInfo[LuaDaFuWongMgr.CurPlayerRound]
        local Portrait = self.m_PlayerHead.transform:Find("Portrait"):GetComponent(typeof(CUITexture))
        local bg = self.m_PlayerHead.transform:Find("BG"):GetComponent(typeof(CUITexture))
        local Name = self.m_PlayerHead.transform:Find("Name"):GetComponent(typeof(UILabel))
        if playerData then
            Portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(playerData.class, playerData.gender, -1), false)
			bg:LoadMaterial(LuaDaFuWongMgr:GetHeadBgPath(playerData.round))
			Name.transform:Find("Texture"):GetComponent(typeof(CUITexture)):LoadMaterial(LuaDaFuWongMgr:GetNameBgPath(playerData.round))
            Name.text = playerData.name
        end
    end
end

function LuaDaFuWongYiZhanWnd:OnEnable()
    g_ScriptEvent:AddListener("OnDaFuWengStageUpdate",self,"OnStageUpdate")
end

function LuaDaFuWongYiZhanWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnDaFuWengStageUpdate",self,"OnStageUpdate")
    LuaDaFuWongMgr:EndCountDownTick(EnumDaFuWengCountDownStage.RandomEvent)
end
--@region UIEvent

--@endregion UIEvent

