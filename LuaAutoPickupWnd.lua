local QualityColor = import "L10.Game.QualityColor"
local QnCheckBox = import "L10.UI.QnCheckBox"
local NGUITools = import "NGUITools"
local NGUIText = import "NGUIText"
local Extensions = import "Extensions"
local EnumQualityType = import "L10.Game.EnumQualityType"
local CItemMgr = import "L10.Game.CItemMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CGuideMgr=import "L10.Game.Guide.CGuideMgr"
local UITabBar = import "L10.UI.UITabBar"
local CButton = import "L10.UI.CButton"


CLuaAutoPickupWnd = class()
RegistClassMember(CLuaAutoPickupWnd,"m_TabBar")
RegistClassMember(CLuaAutoPickupWnd,"m_AutoPickView")
RegistClassMember(CLuaAutoPickupWnd,"m_PinnedItemView")
RegistClassMember(CLuaAutoPickupWnd,"m_TabBar")
RegistClassMember(CLuaAutoPickupWnd,"m_PinnedItemTpl")
RegistClassMember(CLuaAutoPickupWnd,"m_PinnedItemGrid")
RegistClassMember(CLuaAutoPickupWnd,"m_PinnedItemDescLabel")
RegistClassMember(CLuaAutoPickupWnd,"m_PinnedOptions")
RegistClassMember(CLuaAutoPickupWnd,"checkBoxTpl")
RegistClassMember(CLuaAutoPickupWnd,"checkBoxTpl2")
RegistClassMember(CLuaAutoPickupWnd,"m_AutoPickGrid")
RegistClassMember(CLuaAutoPickupWnd,"m_AutoPickDescLabel")
RegistClassMember(CLuaAutoPickupWnd,"checkboxes")
RegistClassMember(CLuaAutoPickupWnd,"m_LanZhuangIndex")
RegistClassMember(CLuaAutoPickupWnd,"m_ExtraLanZhuangIndex")
RegistClassMember(CLuaAutoPickupWnd,"equipAutoPickData")
RegistClassMember(CLuaAutoPickupWnd,"itemAutoPickData")

function CLuaAutoPickupWnd:Awake()
    self.m_TabBar = self.transform:Find("Anchor/TabBar"):GetComponent(typeof(UITabBar))
    self.m_AutoPickView = self.transform:Find("Anchor/AutoPick").gameObject
    self.m_PinnedItemView = self.transform:Find("Anchor/PinnedItem").gameObject
    self.m_PinnedItemTpl = self.transform:Find("Anchor/PinnedItem/Button").gameObject
    self.m_PinnedItemGrid = self.transform:Find("Anchor/PinnedItem/Table"):GetComponent(typeof(UIGrid))
    self.m_PinnedItemDescLabel = self.transform:Find("Anchor/PinnedItem/DescLabel"):GetComponent(typeof(UILabel))
    self.checkBoxTpl = self.transform:Find("Anchor/AutoPick/Checkbox").gameObject
    self.checkBoxTpl:SetActive(false)
    self.checkBoxTpl2 = self.transform:Find("Anchor/AutoPick/Checkbox2").gameObject
    self.checkBoxTpl2:SetActive(false)
    self.m_AutoPickGrid = self.transform:Find("Anchor/AutoPick/Table"):GetComponent(typeof(UIGrid))
    self.m_AutoPickDescLabel = self.transform:Find("Anchor/AutoPick/DescLabel"):GetComponent(typeof(UILabel))
    self.checkboxes = {}
    self.equipAutoPickData = {}
    self.itemAutoPickData = {}

    self.equipTypeNameDict = {
        [1] = LocalString.GetString("白装"),
        [2] = LocalString.GetString("黄装"),
        [3] = LocalString.GetString("橙装"),
        [4] = LocalString.GetString("蓝装"),
        [5] = LocalString.GetString("红装"),
        [6] = LocalString.GetString("紫装"),
        [7] = LocalString.GetString("鬼装")
    }
    
    self.m_TabBar.OnTabChange = CommonDefs.CombineListner_Action_GameObject_int(self.m_TabBar.OnTabChange, DelegateFactory.Action_GameObject_int(function(go, index)
        self:OnTabChange(go, index)
    end), true)
end

function CLuaAutoPickupWnd:InitPinnedOptions()
    if self.m_PinnedOptions then return end
    self.m_PinnedOptions = {}

    local orderTypeTbl = {}
    Item_PinOrderType.Foreach(function(key, data)
        if not orderTypeTbl[data.OrderType] then
            orderTypeTbl[data.OrderType] = true
            table.insert(self.m_PinnedOptions, {text=data.Name, isEquip=false, type=data.OrderType, displayOrder = data.DisplayOrder})
        end
    end)
    table.sort(self.m_PinnedOptions, function(a,b)
        if a.displayOrder~=b.displayOrder then
            return a.displayOrder<b.displayOrder
        else
            return a.type<b.type
        end
    end)

    table.insert(self.m_PinnedOptions, 1, {text=LocalString.GetString("不置顶"), isEquip=false, type=0, displayOrder=0})
    table.insert(self.m_PinnedOptions, 2, {text=LocalString.GetString("装备"), isEquip=true, type=0, displayOrder=0})
end

function CLuaAutoPickupWnd:Init()
    self:InitAutoPickView()
    self:InitPinnedItemView()
    self.m_TabBar:ChangeTab(0, false)
end

function CLuaAutoPickupWnd:OnTabChange(go, index)
    if index == 1 then
        self.m_AutoPickView:SetActive(false)
        self.m_PinnedItemView:SetActive(true)
    else
        self.m_AutoPickView:SetActive(true)
        self.m_PinnedItemView:SetActive(false)
    end
end

function CLuaAutoPickupWnd:InitAutoPickView()
    Extensions.RemoveAllChildren(self.m_AutoPickGrid.transform)
    self.m_AutoPickDescLabel.text = g_MessageMgr:FormatMessage("AUTO_PICK_INFORMATION", nil)
    self.equipAutoPickData = {}
    
    EquipmentTemplate_AutoPick.ForeachKey(function (key) 
        local autoPick = EquipmentTemplate_AutoPick.GetData(key)
        if autoPick.CanAdjust == 1 and self.equipTypeNameDict[key] or key==6 then
            local data = {
                key = key,
                text = SafeStringFormat3("[c][%s]%s[-][/c]", NGUIText.EncodeColor24(QualityColor.GetRGBValue(CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), key))), self.equipTypeNameDict[key])
            }
            table.insert(self.equipAutoPickData,data)
        end
    end)
    table.sort( self.equipAutoPickData, function(a,b)
        return a.key>b.key
    end )

    for i=1,#self.equipAutoPickData do
        local v = self.equipAutoPickData[i]
        if v.key==4 then
            table.insert(self.equipAutoPickData,i,{
                key = nil,
                extrakey = 1,
                text = SafeStringFormat3("[c][%s]%s[-][/c]", NGUIText.EncodeColor24(QualityColor.GetRGBValue(CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), 4))), LocalString.GetString("本职业蓝装"))
            })
            break
        end
    end

    self.itemAutoPickData = {}
    Item_AutoPick.ForeachKey(function (key) 
        local autoPick = Item_AutoPick.GetData(key)
        if autoPick.CanAdjust == 1 then
            local data = {
                key = key,
                text = SafeStringFormat3("[c][%s]%s[-][/c]", NGUIText.EncodeColor24(QualityColor.GetRGBValue(EnumQualityType.White)), Item_Type.GetData(key).Name)
            }
            table.insert(self.itemAutoPickData,data)
        end
    end)
    table.sort( self.itemAutoPickData, function(a,b)
        return a.key<b.key
    end )

    self.checkboxes = {}

    for i,v in ipairs(self.equipAutoPickData) do
        local instance = NGUITools.AddChild(self.m_AutoPickGrid.gameObject, v.key==6 and self.checkBoxTpl2 or self.checkBoxTpl)
        instance:SetActive(true)
        local uiCheckBox = CommonDefs.GetComponent_GameObject_Type(instance, typeof(QnCheckBox))
        uiCheckBox.Text = v.text
        if v.key==6 then
            uiCheckBox.Selected = true
            local mask = instance.transform:Find("Mask").gameObject
            UIEventListener.Get(mask).onClick = DelegateFactory.VoidDelegate(function(g)
                g_MessageMgr:ShowMessage("AutoPick_ZiZhuang_Cannot_Cancel")
            end)
        elseif v.extrakey then
            if CClientMainPlayer.Inst ~= nil then
                uiCheckBox.Selected =  CClientMainPlayer.Inst.ItemProp.AutoPickExtra:GetBit(1)
            else
                uiCheckBox.Selected = false
            end
            self.m_ExtraLanZhuangIndex = #self.checkboxes+1
            uiCheckBox.OnValueChanged = DelegateFactory.Action_bool(function(b)
                if self.checkboxes[self.m_LanZhuangIndex].Selected and not b then
                    g_MessageMgr:ShowMessage("AutoPick_ExtraLanZhuang_Cannot_Cancel")
                    self.checkboxes[self.m_ExtraLanZhuangIndex].Selected = true
                end
            end)
        else
            if CClientMainPlayer.Inst ~= nil then
                if CItemMgr.Inst:CanEquipAutoPick(v.key) then
                    uiCheckBox.Selected = true
                else
                    uiCheckBox.Selected = false
                end
            end
            if v.key==4 then
                self.m_LanZhuangIndex = #self.checkboxes+1
                uiCheckBox.OnValueChanged = DelegateFactory.Action_bool(function(b)
                    --勾选“蓝装”会自动勾选“本职业蓝装”
                    if b then
                        self.checkboxes[self.m_ExtraLanZhuangIndex].Selected = true
                    end
                end)
            end
        end

        table.insert(self.checkboxes,uiCheckBox)
    end

    for i,v in ipairs(self.itemAutoPickData) do
        local instance = NGUITools.AddChild(self.m_AutoPickGrid.gameObject, self.checkBoxTpl)
        instance:SetActive(true)
        local uiCheckBox = CommonDefs.GetComponent_GameObject_Type(instance, typeof(QnCheckBox))
        uiCheckBox.Text = v.text
        if CClientMainPlayer.Inst ~= nil then
            if CItemMgr.Inst:CanItemAutoPick(v.key) then
                uiCheckBox.Selected = true
            else
                uiCheckBox.Selected = false
            end
        end
        table.insert(self.checkboxes,uiCheckBox)
    end

    self.m_AutoPickGrid:Reposition()
end

function CLuaAutoPickupWnd:InitPinnedItemView()
    self:InitPinnedOptions()

    Extensions.RemoveAllChildren(self.m_PinnedItemGrid.transform)
    self.m_PinnedItemDescLabel.text = g_MessageMgr:FormatMessage("PACKAGE_PINNED_ITEM_INFORMATION")

    for i,v in ipairs(self.m_PinnedOptions) do
        local child = CUICommonDef.AddChild(self.m_PinnedItemGrid.gameObject, self.m_PinnedItemTpl)
        child:SetActive(true)
        local button = child.transform:GetComponent(typeof(CButton))
        button.Text = v.text
        UIEventListener.Get(child).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnPinnedItemClick(go, i)
        end)
    end
    self.m_PinnedItemGrid:Reposition()
    self:OnSyncSetPackagePinnedItemTypeResult()
end

function CLuaAutoPickupWnd:OnPinnedItemClick(go, index)
    if not CClientMainPlayer.Inst then return end
    local itemProp = CClientMainPlayer.Inst.ItemProp
    local itemType = itemProp.PackagePinnedItemType
    local equipFirst = itemProp.PackagePinnedEquipment>0
    local oldOption = nil
    for i,v in ipairs(self.m_PinnedOptions) do
        if (equipFirst and v.isEquip) or (not equipFirst and not v.isEquip and v.type ==itemType) then
            oldOption = v
            break
        end
    end
    local optionInfo = self.m_PinnedOptions and self.m_PinnedOptions[index]
    if oldOption == optionInfo then return end
    local oldOptionText = oldOption and oldOption.text or ""
    g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("PACKAGE_PINNED_ITEM_CONFIRM", oldOptionText, optionInfo.text), function()
        Gac2Gas.RequestSetPackagePinnedItemType(optionInfo.isEquip, optionInfo.type)
    end, function()

    end, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
end

function CLuaAutoPickupWnd:OnSyncSetPackagePinnedItemTypeResult()
    local childCount = self.m_PinnedItemGrid.transform.childCount
    if childCount ~= #self.m_PinnedOptions then return end
    local equipFirst = false
    local itemType = 0
    if CClientMainPlayer.Inst then
        local itemProp = CClientMainPlayer.Inst.ItemProp
        itemType = itemProp.PackagePinnedItemType
        equipFirst = itemProp.PackagePinnedEquipment>0
    end
    for i,v in ipairs(self.m_PinnedOptions) do
        local child = self.m_PinnedItemGrid.transform:GetChild(i-1).gameObject
        local button = child.transform:GetComponent(typeof(CButton))
        button.Selected = (equipFirst and v.isEquip) or (not equipFirst and not v.isEquip and v.type ==itemType)
    end
end

function CLuaAutoPickupWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncSetPackagePinnedItemTypeResult", self, "OnSyncSetPackagePinnedItemTypeResult")
end

function CLuaAutoPickupWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("SyncSetPackagePinnedItemTypeResult", self, "OnSyncSetPackagePinnedItemTypeResult")
    local m = #self.equipAutoPickData
    local n = #self.itemAutoPickData
    local equipSettings = {}
    local itemSettings = {}
    local extraSetting = {}

    for i,v in ipairs(self.checkboxes) do
        if i <= m then
            local key = self.equipAutoPickData[i].key
            local value = self.checkboxes[i].Selected and 1 or 0
            if key then
                equipSettings[key] = value
            else
                local extrakey = self.equipAutoPickData[i].extrakey
                extraSetting[extrakey] = value
            end
        else
            itemSettings[self.itemAutoPickData[i-m].key] = self.checkboxes[i].Selected and 1 or 0
        end
    end

    local dataStr = ""
    for k,v in pairs(equipSettings) do
        dataStr = dataStr..SafeStringFormat3("%d,%d;",k,v)
    end
    Gac2Gas.SetAutoPickEquip(dataStr)

    dataStr = ""
    for k,v in pairs(itemSettings) do
        dataStr = dataStr..SafeStringFormat3("%d,%d;",k,v)
    end
    Gac2Gas.SetAutoPickItem(dataStr)

    dataStr = ""
    for k,v in pairs(extraSetting) do
        dataStr = dataStr..SafeStringFormat3("%d,%d;",k,v)
    end
    Gac2Gas.SetAutoPickExtra(dataStr)
end



function CLuaAutoPickupWnd:GetGuideGo(methodName)
    local function GetChoice(index)
        local child = self.m_AutoPickGrid.transform:GetChild(index)
        local checkbox = child:GetComponent(typeof(QnCheckBox))
        if checkbox.Selected then
            return checkbox.gameObject
        else
            return nil
        end
    end
    if methodName=="GetChoice5" then
        local rtn= GetChoice(5+1)
        if rtn then
            return rtn
        else
            CGuideMgr.Inst:TriggerGuide(5)
            return nil
        end
    elseif methodName=="GetChoice4" then
        local rtn= GetChoice(4+1)
        if rtn then
            return rtn
        else
            CGuideMgr.Inst:TriggerGuide(6)
            return nil
        end
    elseif methodName=="GetChoice3" then
        local rtn= GetChoice(3+1)
        if rtn then
            return rtn
        else
            CGuideMgr.Inst:EndCurrentPhase()
            return nil
        end
    end
end