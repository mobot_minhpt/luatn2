local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local UIGrid = import "UIGrid"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemMgr = import "L10.Game.CItemMgr"

LuaShiTuWenDaoRewardWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaShiTuWenDaoRewardWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaShiTuWenDaoRewardWnd, "LeftPageButton", "LeftPageButton", GameObject)
RegistChildComponent(LuaShiTuWenDaoRewardWnd, "RightPageButton", "RightPageButton", GameObject)
RegistChildComponent(LuaShiTuWenDaoRewardWnd, "StageLabel", "StageLabel", CUITexture)
RegistChildComponent(LuaShiTuWenDaoRewardWnd, "State1Label", "State1Label", UILabel)
RegistChildComponent(LuaShiTuWenDaoRewardWnd, "State2Label", "State2Label", UILabel)
RegistChildComponent(LuaShiTuWenDaoRewardWnd, "State3Label", "State3Label", UILabel)
RegistChildComponent(LuaShiTuWenDaoRewardWnd, "BottomLabel", "BottomLabel", UILabel)
RegistChildComponent(LuaShiTuWenDaoRewardWnd, "RewardTemplate", "RewardTemplate", GameObject)
RegistChildComponent(LuaShiTuWenDaoRewardWnd, "TeacherGrid", "TeacherGrid", UIGrid)
RegistChildComponent(LuaShiTuWenDaoRewardWnd, "StudentGrid", "StudentGrid", UIGrid)

--@endregion RegistChildComponent end
RegistClassMember(LuaShiTuWenDaoRewardWnd,"m_CurPage")
RegistClassMember(LuaShiTuWenDaoRewardWnd,"m_MaxPageNum")
RegistClassMember(LuaShiTuWenDaoRewardWnd,"m_Data")
RegistClassMember(LuaShiTuWenDaoRewardWnd,"m_CanBeFilledItemIds")
function LuaShiTuWenDaoRewardWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)


	
	UIEventListener.Get(self.LeftPageButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeftPageButtonClick()
	end)


	
	UIEventListener.Get(self.RightPageButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRightPageButtonClick()
	end)


    --@endregion EventBind end
end

function LuaShiTuWenDaoRewardWnd:Init()
	self.RewardTemplate.gameObject:SetActive(false)
	self.m_CurPage = LuaShiTuTrainingHandbookMgr.m_WenDaoStage
	self.m_MaxPageNum = ShiTu_Setting.GetData().WenDaoStageName.Length
	self:InitItemIds()
	self:ShowCurPage()
end

function LuaShiTuWenDaoRewardWnd:OnEnable()
	g_ScriptEvent:AddListener("SendNewShiTuWenDaoInfo", self, "OnSendNewShiTuWenDaoInfo")
end

function LuaShiTuWenDaoRewardWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SendNewShiTuWenDaoInfo", self, "OnSendNewShiTuWenDaoInfo")
end

function LuaShiTuWenDaoRewardWnd:OnSendNewShiTuWenDaoInfo()
	self:ShowCurPage()
end

function LuaShiTuWenDaoRewardWnd:ShowCurPage()
	local wenDaoInfo = LuaShiTuTrainingHandbookMgr.m_WenDaoInfo.wendaoInfo
	local stageInfo = wenDaoInfo and wenDaoInfo[self.m_CurPage]
	self.LeftPageButton.gameObject:SetActive(self.m_CurPage > 1)
	self.RightPageButton.gameObject:SetActive(self.m_CurPage < self.m_MaxPageNum)

	ShiTuCultivate_CultivateNewStage.Foreach(function (k,v)
		if v.Level == LuaShiTuTrainingHandbookMgr.m_WenDaoRewardWndLevel and v.StageId == self.m_CurPage then
			self.m_Data = ShiTuCultivate_CultivateNewStage.GetData(k)
		end
	end)
	self.StageLabel:LoadMaterial(SafeStringFormat3("UI/Texture/Transparent/Material/shituwendao_jieduan_0%d.mat", self.m_CurPage))
	--self.StageLabel.text = ShiTu_Setting.GetData().WenDaoStageName[self.m_CurPage - 1]

	Extensions.RemoveAllChildren(self.TeacherGrid.transform)
	if self.m_Data.ShifuQingyi > 0 then
		local obj = NGUITools.AddChild(self.TeacherGrid.gameObject, self.RewardTemplate.gameObject)
		self:InitItem(obj, {scorekey = EnumPlayScoreKey.QYJF, num = self.m_Data.ShifuQingyi})
	end
	self.TeacherGrid:Reposition()
	
	Extensions.RemoveAllChildren(self.StudentGrid.transform)
	if self.m_Data.TudiQingyi > 0 then
		local obj = NGUITools.AddChild(self.StudentGrid.gameObject, self.RewardTemplate.gameObject)
		self:InitItem(obj, {scorekey = EnumPlayScoreKey.QYJF, num = self.m_Data.TudiQingyi})
	end
	if self.m_Data.TudiGiftLimit > 0 then
		local obj = NGUITools.AddChild(self.StudentGrid.gameObject, self.RewardTemplate.gameObject)
		self:InitItem(obj, {scorekey = EnumPlayScoreKey.GIFT, num = self.m_Data.TudiGiftLimit})
	end
	for i = 0, self.m_Data.TudiItems.Length - 1 do
		local itmeId, num = self.m_Data.TudiItems[i][0],self.m_Data.TudiItems[i][1]
		if num > 0 then
			local obj = NGUITools.AddChild(self.StudentGrid.gameObject, self.RewardTemplate.gameObject)
			self:InitItem(obj, {item = Item_Item.GetData(itmeId), num = num})
		end
	end
	
	local t = {extraAddItem = true, num = (stageInfo and stageInfo.giftItemCount) and stageInfo.giftItemCount or 0}
	if stageInfo and stageInfo.hasGift then
		local obj = NGUITools.AddChild(self.StudentGrid.gameObject, self.RewardTemplate.gameObject)
		if stageInfo.giftItemTemplateId and stageInfo.isItem then
			t.giftItem = Item_Item.GetData(stageInfo.giftItemTemplateId)
			self:InitItem(obj, t)
		elseif stageInfo.giftItemId and not stageInfo.isItem then
			t.giftItemId = stageInfo.giftItemId
			t.giftItem = EquipmentTemplate_Equip.GetData(stageInfo.giftItemTemplateId)
			self:InitItem(obj, t)
		elseif stageInfo.jadeNum then
			t.jadeNum = stageInfo.jadeNum
			self:InitItem(obj, t)
		end
	elseif stageInfo and not stageInfo.hasGift and not stageInfo.rewarded and LuaShiTuTrainingHandbookMgr.m_IsShowForTeacher then
		local obj = NGUITools.AddChild(self.StudentGrid.gameObject, self.RewardTemplate.gameObject)
		self:InitItem(obj, t)
	end

	self.StudentGrid:Reposition()
	local isFinished = true
	for cultivateId, taskInfo in pairs(stageInfo.cultivateTasks or {}) do
		local cultivate = ShiTuCultivate_CultivateNew.GetData(cultivateId)
		local progress = taskInfo[1] or 0
		local rewarded = taskInfo[2] == true
		if progress < cultivate.Num then
            isFinished = false
        end
    end
	local status = 2
	local currentStage = ShiTuCultivate_CultivateNewStage.GetData(LuaShiTuTrainingHandbookMgr.m_WenDaoInfo.currentStage)
	if wenDaoInfo and currentStage and currentStage.StageId < self.m_CurPage then
		status = 3
	elseif stageInfo and isFinished then 
		status = 1
	end
	self.State1Label.gameObject:SetActive(status == 1)
	self.State2Label.gameObject:SetActive(status == 2)
	self.State3Label.gameObject:SetActive(status == 3)
end

function LuaShiTuWenDaoRewardWnd:InitItem(obj, t)
	local addRewardSprite = obj.transform:Find("AddRewardSprite")
	local alertSprite = obj.transform:Find("AlertSprite")
	local reward = obj.transform:Find("Reward"):GetComponent(typeof(CUITexture))
	local numLabel = obj.transform:Find("NumLabel"):GetComponent(typeof(UILabel))

	obj.gameObject:SetActive(true)

	addRewardSprite.gameObject:SetActive(false)
	alertSprite.gameObject:SetActive(false)
	numLabel.text = t.num > 1 and t.num or ""
	if t.jadeNum then
		numLabel.text = t.jadeNum
	end
	
	if t.extraAddItem then
		reward.gameObject:SetActive(t.giftItem or t.jadeNum)
		if t.giftItem then
			reward:LoadMaterial(t.giftItem.Icon)
        else
            reward:LoadMaterial("ui/texture/transparent/material/welfarewnd_lingyu.mat")
		end	
		addRewardSprite.gameObject:SetActive(t.giftItem == nil and t.jadeNum == nil)	
		alertSprite.gameObject:SetActive(true)
		UIEventListener.Get(obj).onClick = DelegateFactory.VoidDelegate(function()
			if t.giftItem == nil and t.jadeNum == nil then
				self:ShowPopupMenu(obj)
			elseif LuaShiTuTrainingHandbookMgr.m_IsShowForTeacher then
				self:ShowTakeOutItemPopupMenu(obj)
			elseif t.giftItemId then
				CItemInfoMgr.ShowLinkItemInfo(t.giftItemId, false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
			elseif t.giftItem then
				CItemInfoMgr.ShowLinkItemTemplateInfo(t.giftItem.ID, false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
			end
		end)
	elseif t.scorekey then 
		reward:LoadMaterial(LuaShiTuTrainingHandbookMgr:GetPlayScore(t.scorekey).ScoreIcon) 
		-- UIEventListener.Get(obj).onClick = DelegateFactory.VoidDelegate(function()
		-- 	CItemInfoMgr.ShowLinkItemTemplateInfo(t.scorekey == EnumPlayScoreKey.QYJF and 21001285 or 0, false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
		-- end)
	elseif t.item then
		reward:LoadMaterial(t.item.Icon) 
		UIEventListener.Get(obj).onClick = DelegateFactory.VoidDelegate(function()
			CItemInfoMgr.ShowLinkItemTemplateInfo(t.item.ID, false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
		end)
	end
end

function LuaShiTuWenDaoRewardWnd:ShowPopupMenu(obj)
	local t = {}
	table.insert(t, PopupMenuItemData(LocalString.GetString("填充道具"),DelegateFactory.Action_int(function (idx)
		self:AddExtraItem()
	end),false,nil))
	if not CommonDefs.IS_VN_CLIENT then
		table.insert(t, PopupMenuItemData(LocalString.GetString("填充灵玉"),DelegateFactory.Action_int(function (idx)
			self:AddExtraLingYu()
		end),false,nil))
	end
	local array = Table2Array(t,MakeArrayClass(PopupMenuItemData))
	CPopupMenuInfoMgr.ShowPopupMenu(array, obj.transform, CPopupMenuInfoMgr.AlignType.Left,1,DelegateFactory.Action(function( ... )
	end),600,true,240)
end

function LuaShiTuWenDaoRewardWnd:ShowTakeOutItemPopupMenu(obj)
	local t = {}
	local itemdata = PopupMenuItemData(LocalString.GetString("取出道具"),DelegateFactory.Action_int(function (idx)
		MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("ShiTuTrainingPlan_TakeOutItem_Confirm"),DelegateFactory.Action(function()
			Gac2Gas.ShiTuWenDaoShiFuAddStageItem(LuaShiTuTrainingHandbookMgr:GetTudiPlayerId(), self:GetCurStageId(), 0, 0, 0,"")
		end),nil,LocalString.GetString("确认"),LocalString.GetString("取消"),false)
	end),false,nil)
	table.insert(t, itemdata)
	local array = Table2Array(t,MakeArrayClass(PopupMenuItemData))
	CPopupMenuInfoMgr.ShowPopupMenu(array, obj.transform, CPopupMenuInfoMgr.AlignType.Left,1,DelegateFactory.Action(function()

	end),600,true,240)
end

function LuaShiTuWenDaoRewardWnd:AddExtraItem(idx, data)
	self:InitItemIds()
    LuaItemChooseWndMgr:ShowItemWithDetailMessageChooseWnd(self.m_CanBeFilledItemIds,function(itemId)
        self:ChooseItem(itemId, data)
    end, LocalString.GetString("选择填充道具"),LocalString.GetString("填充"), function()
        g_MessageMgr:ShowMessage("ShiTuTrainingPlan_ChooseItem_Explanation")
    end)
end

function LuaShiTuWenDaoRewardWnd:InitItemIds()
    self.m_CanBeFilledItemIds =  CreateFromClass(MakeGenericClass(List, cs_string))
    local bagSize = CClientMainPlayer.Inst.ItemProp:GetPlaceSize(EnumItemPlace.Bag)
    for i = 1,bagSize do
        local id = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
        if not cs_string.IsNullOrEmpty(id) then
            local precious = CItemMgr.Inst:GetById(id)
            if precious ~= nil then
                if precious.IsEquip and precious.Equip:IsAllowFace2FaceGift() then
                    CommonDefs.ListAdd(self.m_CanBeFilledItemIds, typeof(cs_string), precious.Id)
                elseif precious.IsItem and precious.Item:IsAllowFace2FaceGift() and not precious.Item:IsExpire() then
                    CommonDefs.ListAdd(self.m_CanBeFilledItemIds, typeof(cs_string), precious.Id)
                end
            end
        end
    end
end

function LuaShiTuWenDaoRewardWnd:AddExtraLingYu(idx, data)
    CLuaNumberInputMgr.ShowNumInputBox(0, 1000, 1, function (val)
        Gac2Gas.ShiTuWenDaoShiFuAddStageItem(LuaShiTuTrainingHandbookMgr:GetTudiPlayerId(), self:GetCurStageId(), val, EnumItemPlace.Bag, 0, "")
    end, LocalString.GetString("请输入填充的灵玉数量"), -1)
end

function LuaShiTuWenDaoRewardWnd:ChooseItem(itemId, data)
    if itemId then
    	local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, itemId)
        Gac2Gas.ShiTuWenDaoShiFuAddStageItem(LuaShiTuTrainingHandbookMgr:GetTudiPlayerId(), self:GetCurStageId(), 0, EnumItemPlace.Bag, pos, itemId)
    else
        g_MessageMgr:ShowMessage("ShiTuTrainingPlan_ChooseItem_NoneItem")
    end
end

function LuaShiTuWenDaoRewardWnd:GetCurStageId()
	return math.modf( (LuaShiTuTrainingHandbookMgr.m_WenDaoInfo.currentStage - 1) / 4 ) * 4 + LuaShiTuTrainingHandbookMgr.m_WenDaoRewardWndCurrentStage + self.m_CurPage - LuaShiTuTrainingHandbookMgr.m_WenDaoStage
end

--@region UIEvent

function LuaShiTuWenDaoRewardWnd:OnTipButtonClick()
	g_MessageMgr:ShowMessage("ShiTuWenDaoRewardWnd_ReadMe")
end

function LuaShiTuWenDaoRewardWnd:OnLeftPageButtonClick()
	self.m_CurPage = self.m_CurPage - 1
	self:ShowCurPage()
end

function LuaShiTuWenDaoRewardWnd:OnRightPageButtonClick()
	self.m_CurPage = self.m_CurPage + 1
	self:ShowCurPage()
end

--@endregion UIEvent

