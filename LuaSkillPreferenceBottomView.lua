local CInputBoxMgr = import "L10.UI.CInputBoxMgr"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CSkillGroupButton = import "L10.UI.CSkillGroupButton"
local Extensions = import "Extensions"
local CSkillTemplateUnlockDescItem = import "L10.UI.CSkillTemplateUnlockDescItem"
local Object = import "System.Object"
local EnumTianFuSkillType = import "L10.Game.EnumTianFuSkillType"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local CSkillMgr = import "L10.Game.CSkillMgr"
local CTianFuSkillItemCell = import "L10.UI.CTianFuSkillItemCell"
local SkillKind = import "L10.Game.SkillKind"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
--local __Skill_AllSkills_Template = import "L10.Game.__Skill_AllSkills_Template"

LuaSkillPreferenceBottomView = class()

RegistChildComponent(LuaSkillPreferenceBottomView,"m_SkillGroupButtonTemplate","SkillGroupButton", GameObject)
RegistChildComponent(LuaSkillPreferenceBottomView,"m_ConditionTemplate","ConditionTemplate", GameObject)
RegistChildComponent(LuaSkillPreferenceBottomView,"m_ReNameBtn","ChangNameBtn", GameObject)
RegistChildComponent(LuaSkillPreferenceBottomView,"m_InfoBtn","InfoBtn", GameObject)
RegistChildComponent(LuaSkillPreferenceBottomView,"m_SettingBtn","SettingBtn", GameObject)
RegistChildComponent(LuaSkillPreferenceBottomView,"m_ReNameBtnBg","SkillSet", GameObject)
RegistChildComponent(LuaSkillPreferenceBottomView,"m_TemplateNameLabel","ValueLabel", UILabel)
RegistChildComponent(LuaSkillPreferenceBottomView,"m_SkillGroupListTable","Table", UITable)
RegistChildComponent(LuaSkillPreferenceBottomView,"m_LockRoot","LockRoot", GameObject)
RegistChildComponent(LuaSkillPreferenceBottomView,"m_UnlockRoot","UnlockRoot", GameObject)
RegistChildComponent(LuaSkillPreferenceBottomView,"m_ConditionTable","ConditionTable", UITable)
RegistChildComponent(LuaSkillPreferenceBottomView,"m_YingLingSwitchRoot","YingLingSwitchRoot", CCommonLuaScript)
RegistChildComponent(LuaSkillPreferenceBottomView,"m_SkillButtonRoot","SkillButtonRoot", CCommonLuaScript)
RegistChildComponent(LuaSkillPreferenceBottomView,"m_TianfuSkillTemplate","TianFuSkillItem", GameObject)
RegistChildComponent(LuaSkillPreferenceBottomView,"m_TianFuDeltaDescLabel","TianFuDeltaDescLabel", UILabel)
RegistChildComponent(LuaSkillPreferenceBottomView,"m_TianfuAdjustBtn","AdjustButton", GameObject)
RegistChildComponent(LuaSkillPreferenceBottomView,"m_TianFuSkillGrid","TianFuSkillGrid", UIGrid)
RegistChildComponent(LuaSkillPreferenceBottomView,"m_TipTopPos","TipTopPos", Transform)


RegistClassMember(LuaSkillPreferenceBottomView,"m_MaxNameByteLength")
RegistClassMember(LuaSkillPreferenceBottomView,"m_InputBoxStringLimit")
RegistClassMember(LuaSkillPreferenceBottomView,"m_IsSkillGroupsInit")
RegistClassMember(LuaSkillPreferenceBottomView,"m_SkillGroupButtons")
RegistClassMember(LuaSkillPreferenceBottomView,"m_SkillGroupCount")
RegistClassMember(LuaSkillPreferenceBottomView,"m_SelectedTianFuIndex")
RegistClassMember(LuaSkillPreferenceBottomView,"m_TianfuItemCells")

function LuaSkillPreferenceBottomView:Awake()
    self.m_SkillGroupButtonTemplate:SetActive(false)
    self.m_ConditionTemplate:SetActive(false)

    UIEventListener.Get(self.m_ReNameBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnRenameButtonClick(go)
    end)
    UIEventListener.Get(self.m_InfoBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnInfoButtonClick(go)
    end)
    UIEventListener.Get(self.m_SettingBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnSettingButtonClick(go)
    end)
    UIEventListener.Get(self.m_ReNameBtnBg).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnRenameButtonClick(go)
    end)

    self.m_MaxNameByteLength = 8
    self.m_InputBoxStringLimit = 32
    self.m_SkillGroupCount = 5

    self.m_TianfuSkillTemplate:SetActive(false)
    self.m_TianFuDeltaDescLabel.gameObject:SetActive(false)

    UIEventListener.Get(self.m_TianfuAdjustBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnTianFuAdjustButtonClick(go)
    end)
end

function LuaSkillPreferenceBottomView:Start()
    self.m_SelectedTianFuIndex = 1
    self:UpdateSkills()
    self:LoadDeltaTianFuSkillInfo()
end

function LuaSkillPreferenceBottomView:OnEnable()
    g_ScriptEvent:AddListener("MainPlayerSkillPropUpdate", self, "OnMainPlayerSkillPropUpdate")
    g_ScriptEvent:AddListener("OnSwitchSkillSetSuccess", self, "OnSwitchSkillSetSuccess")
    self:OnSwitchSkillSetSuccess()
end

function LuaSkillPreferenceBottomView:OnDisable()
    g_ScriptEvent:RemoveListener("MainPlayerSkillPropUpdate", self, "OnMainPlayerSkillPropUpdate")
    g_ScriptEvent:RemoveListener("OnSwitchSkillSetSuccess", self, "OnSwitchSkillSetSuccess")
end

---------------------------------------------------------------------------------------------
---技能组合设置相关
---------------------------------------------------------------------------------------------
function LuaSkillPreferenceBottomView:InitSkillGroupButtons()
    if self.m_IsSkillGroupsInit then
        return
    end
    self.m_IsSkillGroupsInit = true
    self.m_SkillGroupButtons = {}
    for i = 1,self.m_SkillGroupCount do
        local btn = CUICommonDef.AddChild(self.m_SkillGroupListTable.gameObject, self.m_SkillGroupButtonTemplate)
        btn:SetActive(true)
        local button = CommonDefs.GetComponent_GameObject_Type(btn, typeof(CSkillGroupButton))
        button:Init(Extensions.ToChinese((i)), CClientMainPlayer.Inst == nil or not CClientMainPlayer.Inst.SkillProp:IsSkillTemplateAvaliable(i))
        UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnSkillGroupButtonClick(go)
        end)
        table.insert(self.m_SkillGroupButtons, button)
    end
    self.m_SkillGroupListTable:Reposition()
end

function LuaSkillPreferenceBottomView:RefreshRoot(index)
    local mainPlayer = CClientMainPlayer.Inst
    if not mainPlayer then
        return
    end
    local unlock = mainPlayer.SkillProp:IsSkillTemplateAvaliable(index)
    self.m_UnlockRoot:SetActive(unlock)
    self.m_LockRoot:SetActive(not unlock)
    if not unlock then
        Extensions.RemoveAllChildren(self.m_ConditionTable.transform)
        local grade = mainPlayer.MaxLevel
        local xiuwei = mainPlayer.BasicProp.XiuWeiGrade
        local vip = mainPlayer.ItemProp.Vip.Level

        Skill_SkillTemplate.Foreach(function (key, data)
            local go = CUICommonDef.AddChild(self.m_ConditionTable.gameObject, self.m_ConditionTemplate)
            local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CSkillTemplateUnlockDescItem))
            local achieve = (grade >= data.Grade and xiuwei >= data.XiuweiGrade and vip >= data.VipLevel)
            item:Init(data.Condition, achieve)
            go:SetActive(true)
        end)
        self.m_ConditionTable:Reposition()
    end
end
----------------------------------------
---Event
----------------------------------------
function LuaSkillPreferenceBottomView:OnRenameButtonClick(go)
    local mainPlayer = CClientMainPlayer.Inst
    if not mainPlayer then return end

    local text = SafeStringFormat3(LocalString.GetString("名称最多%d个字符(%d个汉字)"), self.m_MaxNameByteLength, math.floor(self.m_MaxNameByteLength / 2))
    if CommonDefs.IsSeaMultiLang() and (LocalString.language == "en" or LocalString.language == "ina") then
        text = SafeStringFormat3(LocalString.GetString("名称最多%d个字母"), self.m_MaxNameByteLength)
    end

    CInputBoxMgr.ShowInputBox(text, DelegateFactory.Action_string(function (newName)
        newName = StringTrim(newName)
        if System.String.IsNullOrEmpty(newName) then
            g_MessageMgr:ShowMessage("SKILL_TEMPLATE_NAME_LEN_ERR")
            return
        end

        if CUICommonDef.GetStrByteLength(newName) > self.m_MaxNameByteLength then
            g_MessageMgr:ShowMessage("SKILL_TEMPLATE_NAME_LEN_ERR")
            return
        end

        if not CWordFilterMgr.Inst:CheckName(newName, LocalString.GetString("技能组合名")) then
            g_MessageMgr:ShowMessage("Name_Violation")
            return
        end

        local activeSkills = CreateFromClass(MakeGenericClass(List, Object))
        local activeSkills2 = CreateFromClass(MakeGenericClass(List, Object))
        local skillProp = mainPlayer.SkillProp
        for i = 1 ,skillProp.NewActiveSkill.Length - 1 do
            CommonDefs.ListAdd(activeSkills, typeof(UInt32), skillProp.NewActiveSkill[i])
        end
        for i = 1 ,skillProp.NewActiveSkill2.Length - 1 do
            CommonDefs.ListAdd(activeSkills2, typeof(UInt32), skillProp.NewActiveSkill2[i])
        end

        local tianfuSkills = CreateFromClass(MakeGenericClass(List, Object))
        skillProp:ForeachOriginalSkillId(DelegateFactory.Action_KeyValuePair_uint_uint(function (kv)
            local skillId = kv.Key
            local skillTemp = Skill_AllSkills.GetData(skillId)
            if skillTemp.Kind == SkillKind_lua.TianFuSkill then
                CommonDefs.ListAdd(tianfuSkills, typeof(UInt32), skillId)
            end
        end))
        Gac2Gas.RequestSaveSkill2Template(skillProp.CurrentTemplateIdx, newName, MsgPackImpl.pack(activeSkills), MsgPackImpl.pack(tianfuSkills), MsgPackImpl.pack(activeSkills2))
    end), self.m_InputBoxStringLimit, true, nil, nil)
end

function LuaSkillPreferenceBottomView:OnInfoButtonClick(go)
    g_MessageMgr:ShowMessage("Skill_Tips_Genghuan_Readme")
end

function LuaSkillPreferenceBottomView:OnSettingButtonClick(go)
    CUIManager.ShowUI(CUIResources.SkillSettingWnd)
end

function LuaSkillPreferenceBottomView:OnSwitchSkillSetSuccess()
    local mainPlayer = CClientMainPlayer.Inst
    if not mainPlayer then
        return
    end

    self:InitSkillGroupButtons()
    self.m_TemplateNameLabel.text = LocalString.TranslateAndFormatText(mainPlayer.SkillProp.CurrentSkillTemplateName)

    for i = 1, self.m_SkillGroupCount do
        local selected = ((i) == mainPlayer.SkillProp.CurrentTemplateIdx)
        self.m_SkillGroupButtons[i].selected = selected
        if selected then
            self:RefreshRoot(i)
        end
    end
end

function LuaSkillPreferenceBottomView:OnMainPlayerSkillPropUpdate()
    if CClientMainPlayer.Inst ~= nil then
        self.m_TemplateNameLabel.text = LocalString.TranslateAndFormatText(CClientMainPlayer.Inst.SkillProp.CurrentSkillTemplateName)
    end
    self:UpdateSkills()
end

function LuaSkillPreferenceBottomView:OnSkillGroupButtonClick(go)
    local mainPlayer = CClientMainPlayer.Inst
    if not mainPlayer then
        return
    end

    for i = 1,self.m_SkillGroupCount do
        local selected = (self.m_SkillGroupButtons[i].gameObject == go)
        self.m_SkillGroupButtons[i].selected = selected
        if selected then
            self:RefreshRoot(i)
            local unlock = mainPlayer.SkillProp:IsSkillTemplateAvaliable(i)
            if unlock then
                Gac2Gas.RequestSwitchSkillTemplate((i))
            end
        end
    end
end

----------------------------------------
---引导相关
----------------------------------------
function LuaSkillPreferenceBottomView:GetSwitchSkill()
    return self.m_SkillButtonRoot:GetSwitchSkill()
end

function LuaSkillPreferenceBottomView:GetTianFuAdjustButton()
    return self.m_TianfuAdjustBtn
end

function LuaSkillPreferenceBottomView:GetSwitchButton()
    return self.m_SkillButtonRoot:GetSwitchButton()
end

function LuaSkillPreferenceBottomView:GetYingLingSwitchSkillStateBtn()
    return self.m_YingLingSwitchRoot:GetYingLingSwitchSkillStateBtn()
end

---------------------------------------------------------------------------------------------
---天赋技能相关
---------------------------------------------------------------------------------------------
function LuaSkillPreferenceBottomView:GetNeedXiuweiGrade(index)
    local type = index
    local needXiuweiGrade = 0
    local default = type
    if default == TianFuNeedXiuweiType.Xiuwei40 then
        needXiuweiGrade = 40
    elseif default == TianFuNeedXiuweiType.Xiuwei60 then
        needXiuweiGrade = 60
    elseif default == TianFuNeedXiuweiType.Xiuwei80 then
        needXiuweiGrade = 80
    elseif default == TianFuNeedXiuweiType.Xiuwei100 then
        needXiuweiGrade = 100
    end
    return needXiuweiGrade
end

--获取神兵给以天赋技能的额外等级
function LuaSkillPreferenceBottomView:LoadDeltaTianFuSkillInfo()
    self.m_TianFuDeltaDescLabel.gameObject:SetActive(false)
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer == nil then
        return
    end
    local prefix = 900 +  EnumToInt(mainplayer.Class)
    local needXiuweiGradesList = {}
    for i = 0, 3 do
        local needXiuweiGrade = self:GetNeedXiuweiGrade(i)
        table.insert(needXiuweiGradesList, needXiuweiGrade)
    end
    local skillProp = mainplayer.SkillProp
    local level = mainplayer.Level
    local temptianfuSkillIds = {}

    local isfind = false
    __Skill_AllSkills_Template.ForeachKey(function (cls)

        if isfind then return end
        if math.floor(cls / 1000) == prefix then
            local skillId = CLuaDesignMgr.Skill_AllSkills_GetSkillId(cls, 1)
            --以(900+ClassId)开头，以01结尾
            local skill = Skill_AllSkills.GetData(skillId)
            if CommonDefs.ConvertIntToEnum(typeof(SkillKind), skill.Kind) == SkillKind.TianFuSkill then
                for i,needXiuweiGrade in ipairs(needXiuweiGradesList) do
                    if needXiuweiGrade == skill.NeedXiuweiLevel then
                        local originWithDeltaLevel = skillProp:GetTianFuSkilleltaLevel(cls, level)
                        if originWithDeltaLevel > 0 then
                            local t = {}
                            t.weight = skill.Weight
                            t.id = skillProp:IsOriginalSkillClsExist(cls) and skillProp:GetOriginalSkillIdByCls(cls) or skillId
                            t.originWithDeltaLevel = originWithDeltaLevel
                            t.name = skill.Name
                            table.insert(temptianfuSkillIds,t)
                            isfind = true
                            break
                        end
                    end
                end
            end
        end
    end)
    table.sort(temptianfuSkillIds,function (a,b)
        if a.Weight == b.Weight then
            return a.id > b.id
        end
        return a.Weight > b.Weight
    end)

    if temptianfuSkillIds and (#temptianfuSkillIds > 0) then
        self.m_TianFuDeltaDescLabel.gameObject:SetActive(true)
        for i, v in ipairs(temptianfuSkillIds) do
            self.m_TianFuDeltaDescLabel.text = g_MessageMgr:FormatMessage("TIANFU_SKILL_EXTRA_DESC", v.name, v.originWithDeltaLevel)
        end
    end
end

function LuaSkillPreferenceBottomView:UpdateSkills()
    self:LoadTianFuSkills()
end

function LuaSkillPreferenceBottomView:LoadTianFuSkills()
    Extensions.RemoveAllChildren(self.m_TianFuSkillGrid.transform)
    self.m_TianfuItemCells ={}

    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer == nil then return end

    local needXiuWeiLevels = CreateFromClass(MakeGenericClass(List, Int32))
    local tianfuSkills = CreateFromClass(MakeGenericClass(Dictionary, Int32, UInt32))
    CSkillMgr.Inst:GetPlayerCurrentTianFuSkills(mainPlayer.Class, mainPlayer.Level, mainPlayer.SkillProp, needXiuWeiLevels, tianfuSkills)

    for i = 0,needXiuWeiLevels.Count - 1 do
        local item = NGUITools.AddChild(self.m_TianFuSkillGrid.gameObject, self.m_TianfuSkillTemplate)
        item:SetActive(true)
        local cell = CommonDefs.GetComponent_GameObject_Type(item, typeof(CTianFuSkillItemCell))
        if CommonDefs.DictContains(tianfuSkills, typeof(Int32), needXiuWeiLevels[i]) then
            local tianfuSkillId = CommonDefs.DictGetValue(tianfuSkills, typeof(Int32), needXiuWeiLevels[i])
            local data = Skill_AllSkills.GetData(tianfuSkillId)
            local cls = math.modf(data.ID / 100)
            local originSkillId = mainPlayer.SkillProp:GetOriginalSkillIdByCls(cls)
            local level = originSkillId % 100
            if mainPlayer.IsInXianShenStatus then
                local feishengLevel = level > 0 and mainPlayer.SkillProp:GetFeiShengModifyLevel(originSkillId, data.Kind, mainPlayer.Level) or 0
                cell:InitFeiSheng(true, feishengLevel)
            else
                cell:InitFeiSheng(false, 0)
            end
            cell:Init(math.modf(data.ID / 100), level, needXiuWeiLevels[i], data.SkillIcon, mainPlayer.BasicProp.XiuWeiGrade < needXiuWeiLevels[i])
        else
            cell:Init(0, 0, needXiuWeiLevels[i], nil, mainPlayer.BasicProp.XiuWeiGrade < needXiuWeiLevels[i])
        end
        UIEventListener.Get(item).onPress = DelegateFactory.BoolDelegate(function (go, isPressed)
            self:OnPressTianFuSkillIcon(item, isPressed, cell.ClassId)
        end)
        local index =#self.m_TianfuItemCells + 1
        UIEventListener.Get(item).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnTianFuSkillItemClick(index)
        end)
        table.insert(self.m_TianfuItemCells, cell)

    end


    self.m_TianFuSkillGrid:Reposition()
    if #self.m_TianfuItemCells > 0 and self.m_SelectedTianFuIndex >= 1 and self.m_SelectedTianFuIndex <= #self.m_TianfuItemCells then
        local cell = self.m_TianfuItemCells[self.m_SelectedTianFuIndex]
        self:OnPressTianFuSkillIcon(cell.gameObject, true, cell.ClassId)
    end

end
----------------------------------------
---Event
----------------------------------------
function LuaSkillPreferenceBottomView:OnTianFuAdjustButtonClick(go)
    CSkillInfoMgr.ShowTianFuSkillWnd(EnumTianFuSkillType.Normal, -1)
end

function LuaSkillPreferenceBottomView:OnPressTianFuSkillIcon(go, pressed, skillClassId)
    if pressed then
        for i = 1,#self.m_TianfuItemCells do
            if go:Equals(self.m_TianfuItemCells[i].gameObject) then
                self.m_SelectedTianFuIndex = i
                self.m_TianfuItemCells[i].Selected = true
            else
                self.m_TianfuItemCells[i].Selected = false
            end
        end
    end
end

function LuaSkillPreferenceBottomView:OnTianFuSkillItemClick(index)
    for i = 1, #self.m_TianfuItemCells do
        if i == index then
            self.m_SelectedTianFuIndex = i
            self.m_TianfuItemCells[i].Selected = true
            if self.m_TianfuItemCells[i].OriginLevel > 0 then
                CSkillInfoMgr.ShowSkillInfoWnd(CLuaDesignMgr.Skill_AllSkills_GetSkillId(self.m_TianfuItemCells[i].ClassId, self.m_TianfuItemCells[i].OriginLevel), self.m_TipTopPos.transform.position, CSkillInfoMgr.EnumSkillInfoContext.MainPlayerSkillPreference, true, 0, 0, nil)
            elseif not self.m_TianfuItemCells[i].Locked then
                CSkillInfoMgr.ShowTianFuSkillWnd(EnumTianFuSkillType.Normal, i)
            end
        else
            self.m_TianfuItemCells[i].Selected = false
        end
    end
end


