local QnTableView=import "L10.UI.QnTableView"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local AlignType=import "CPlayerInfoMgr+AlignType"
local EnumPlayerInfoContext=import "L10.Game.EnumPlayerInfoContext"
local CPlayerInfoMgr=import "CPlayerInfoMgr"
local EChatPanel=import "L10.Game.EChatPanel"



CLuaDuanWuDaZuoZhanRankWnd=class()
RegistClassMember(CLuaDuanWuDaZuoZhanRankWnd,"m_MyRankLabel")
RegistClassMember(CLuaDuanWuDaZuoZhanRankWnd,"m_MyNameLabel")
RegistClassMember(CLuaDuanWuDaZuoZhanRankWnd,"m_MyTimeLabel")
RegistClassMember(CLuaDuanWuDaZuoZhanRankWnd,"m_MyScoreLabel")

RegistClassMember(CLuaDuanWuDaZuoZhanRankWnd,"m_TableViewDataSource")
RegistClassMember(CLuaDuanWuDaZuoZhanRankWnd,"m_TableView")
-- RegistClassMember(CLuaDuanWuDaZuoZhanRankWnd,"m_RankList")
RegistClassMember(CLuaDuanWuDaZuoZhanRankWnd,"m_PlayerInfos")

function CLuaDuanWuDaZuoZhanRankWnd:Init()
    self.m_PlayerInfos={}
    self.m_MyRankLabel=FindChild(self.transform,"MyRankLabel"):GetComponent(typeof(UILabel))
    self.m_MyNameLabel=FindChild(self.transform,"MyNameLabel"):GetComponent(typeof(UILabel))
    self.m_MyTimeLabel=FindChild(self.transform,"MyTimeLabel"):GetComponent(typeof(UILabel))
    self.m_MyScoreLabel=FindChild(self.transform,"MyScoreLabel"):GetComponent(typeof(UILabel))

    FindChild(self.transform,"TitleLabel"):GetComponent(typeof(UILabel)).text=LocalString.GetString("端午大作战排行")
    self.m_MyRankLabel.text=LocalString.GetString("未上榜")
    self.m_MyTimeLabel.text="—"
    self.m_MyScoreLabel.text="—"
    if CClientMainPlayer.Inst then
        self.m_MyNameLabel.text= CClientMainPlayer.Inst.Name
    else
        self.m_MyNameLabel.text=nil
    end

    self.m_TableView=FindChild(self.transform,"TableView"):GetComponent(typeof(QnTableView))
    local function initItem(item,index)
        if index % 2 == 1 then
            item:SetBackgroundTexture("common_textbg_02_dark")
        else
            item:SetBackgroundTexture("common_textbg_02_light")
        end
        self:InitItem(item,index)
    end

    self.m_TableViewDataSource=DefaultTableViewDataSource.CreateByCount(0,initItem)
    self.m_TableView.m_DataSource=self.m_TableViewDataSource
    self.m_TableView.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)

    Gac2Gas.DuanWuDZZ_RequestRankList("")
end
function CLuaDuanWuDaZuoZhanRankWnd:OnSelectAtRow(row)
    local data=self.m_PlayerInfos[row+1]
    if data then
        CPlayerInfoMgr.ShowPlayerPopupMenu(data.playerId, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,AlignType.Default)
    end
end

function CLuaDuanWuDaZuoZhanRankWnd:InitItem(item,index)
    local tf=item.transform
    local timeLabel=FindChild(tf,"TimeLabel"):GetComponent(typeof(UILabel))
    timeLabel.text=""
    local scoreLabel=FindChild(tf,"ScoreLabel"):GetComponent(typeof(UILabel))
    scoreLabel.text=""

    local nameLabel=FindChild(tf,"NameLabel"):GetComponent(typeof(UILabel))
    nameLabel.text=" "
    local rankLabel=FindChild(tf,"RankLabel"):GetComponent(typeof(UILabel))
    rankLabel.text=""
    local rankSprite=FindChild(tf,"RankImage"):GetComponent(typeof(UISprite))
    rankSprite.spriteName=""
    
    local info=self.m_PlayerInfos[index+1]

    local rank=index+1
    if rank==1 then
        rankSprite.spriteName="Rank_No.1"
    elseif rank==2 then
        rankSprite.spriteName="Rank_No.2png"
    elseif rank==3 then
        rankSprite.spriteName="Rank_No.3png"
    else
        rankLabel.text=tostring(rank)
    end

    nameLabel.text=info.name

    timeLabel.text=self:FormatTime(info.duration)

    scoreLabel.text=tostring(info.score)
end
function CLuaDuanWuDaZuoZhanRankWnd:FormatTime(time)
    local minute=math.floor(time/60)
    local second=time%60
    return SafeStringFormat3(LocalString.GetString("%02d分%02d秒"),minute,second)
end

function CLuaDuanWuDaZuoZhanRankWnd:OnEnable()
    g_ScriptEvent:AddListener("DuanWuDZZ_SendOrderedRankListToClient", self, "OnDuanWuDZZ_SendOrderedRankListToClient")
    
end
function CLuaDuanWuDaZuoZhanRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("DuanWuDZZ_SendOrderedRankListToClient", self, "OnDuanWuDZZ_SendOrderedRankListToClient")
end

function CLuaDuanWuDaZuoZhanRankWnd:OnDuanWuDZZ_SendOrderedRankListToClient(playerInfos)
    self.m_PlayerInfos=playerInfos

    local myId=0
    if CClientMainPlayer.Inst then
        myId=CClientMainPlayer.Inst.Id
    end
    
    for i,v in ipairs(self.m_PlayerInfos) do
        if v.playerId==myId then
            self.m_MyRankLabel.text=tostring(i)
            self.m_MyTimeLabel.text=self:FormatTime(v.duration)
            self.m_MyScoreLabel.text=tostring(v.score)
            break
        end
    end

    self.m_TableViewDataSource.count=#self.m_PlayerInfos
    self.m_TableView:ReloadData(true,false)
end
