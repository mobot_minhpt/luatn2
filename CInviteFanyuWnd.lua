-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CIMMgr = import "L10.Game.CIMMgr"
local CInviteFanyuFriendItem = import "L10.UI.CInviteFanyuFriendItem"
local CInviteFanyuWnd = import "L10.UI.CInviteFanyuWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local Double = import "System.Double"
local Extensions = import "Extensions"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
local UInt64 = import "System.UInt64"
local Vector3 = import "UnityEngine.Vector3"
CInviteFanyuWnd.m_OnSendYumaFriendInfo_CS2LuaHook = function (this, friendsInfoUD) 
    if CClientMainPlayer.Inst == nil then
        return
    end

    local friendList = TypeAs(MsgPackImpl.unpack(friendsInfoUD), typeof(MakeGenericClass(List, Object)))
    local friendIdList = CreateFromClass(MakeGenericClass(List, Double))
    do
        local i = 0
        while i < friendList.Count do
            local o = friendList[i]
            local o1 = friendList[i + 1]
            local id = tonumber(o)
            local friendness = tonumber(o1)
            CommonDefs.ListAdd(friendIdList, typeof(Double), id)
            if CClientMainPlayer.Inst ~= nil then
                CommonDefs.DictGetValue(CClientMainPlayer.Inst.RelationshipProp.Friends, typeof(UInt64), math.floor(id)).Friendliness = friendness
            end
            i = i + 2
        end
    end

    CommonDefs.ListSort1(friendIdList, typeof(Double), DelegateFactory.Comparison_double(function (id1, id2) 
        local friendness1 = CIMMgr.Inst:GetFriendliness(math.floor(id1))
        local friendness2 = CIMMgr.Inst:GetFriendliness(math.floor(id2))
        return friendness2 - friendness1
    end))

    local actClick = DelegateFactory.Action_ulong(function (pid) 
        this:TryInviteFriend(pid)
    end)

    Extensions.RemoveAllChildren(this.grid.transform)
    do
        local i = 0
        while i < friendIdList.Count do
            local item = CommonDefs.Object_Instantiate(this.itemTemplate)
            item.transform.parent = this.grid.transform
            item.transform.localScale = Vector3.one
            item.gameObject:SetActive(true)

            local friendId = math.floor(friendIdList[i])
            local friendItem = CommonDefs.GetComponent_GameObject_Type(item, typeof(CInviteFanyuFriendItem))
            CommonDefs.DictAdd(this.ItemDic, typeof(UInt64), friendId, typeof(CInviteFanyuFriendItem), friendItem)
            friendItem:Init(friendId, actClick)
            i = i + 1
        end
    end
    this.grid:Reposition()
    this.scrollView:ResetPosition()
end
