local UIGrid = import "UIGrid"

local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local UITable = import "UITable"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

local IdPartition = import "L10.Game.IdPartition"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"

LuaHengYuDangKouResultWnd = class()
RegistClassMember(LuaHengYuDangKouResultWnd, "m_ResultTbl") -- 结算数据
RegistClassMember(LuaHengYuDangKouResultWnd, "m_AwardList") -- 帮会奖励

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHengYuDangKouResultWnd, "ItemTemplate", "ItemTemplate", GameObject)
RegistChildComponent(LuaHengYuDangKouResultWnd, "AwardGrid", "AwardGrid", UIGrid)
RegistChildComponent(LuaHengYuDangKouResultWnd, "HasGhost", "HasGhost", GameObject)
RegistChildComponent(LuaHengYuDangKouResultWnd, "NoGhost", "NoGhost", GameObject)

--@endregion RegistChildComponent end

function LuaHengYuDangKouResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

    self.m_ResultTbl = LuaHengYuDangKouMgr.m_ResultTbl
    if self.m_ResultTbl and self.m_ResultTbl.ghostId2Count and next(self.m_ResultTbl.ghostId2Count) then
        self.HasGhost:SetActive(true)
        self.NoGhost:SetActive(false)
    else
        self.HasGhost:SetActive(false)
        self.NoGhost:SetActive(true)
    end
end
-- 帮会奖励
function LuaHengYuDangKouResultWnd:ShowAward()
    self.ItemTemplate:SetActive(false)
    Extensions.RemoveAllChildren(self.AwardGrid.transform)
    for i = 1, #self.m_AwardList do
        local itemgo = NGUITools.AddChild(self.AwardGrid.gameObject, self.ItemTemplate)
        itemgo:SetActive(true)
        itemgo.name = tostring(i) -- 编号方便区分
        local itemid = self.m_AwardList[i].id
        local iconpath = nil
        if IdPartition.IdIsItem(itemid) then
            local item = Item_Item.GetData(itemid)
            if item then iconpath = item.Icon end
        elseif IdPartition.IdIsEquip(itemid) then
            local equip = EquipmentTemplate_Equip.GetData(itemid)
            if equip then iconpath = equip.Icon end
        end
        local icon = itemgo:GetComponent(typeof(CUITexture))
        if iconpath and icon then
            icon:LoadMaterial(iconpath)
            UIEventListener.Get(itemgo).onClick = DelegateFactory.VoidDelegate(function(go)
                local index = tonumber(go.name)
                local id = self.m_AwardList[index].id
                if IdPartition.IdIsEquip(id) and LuaHengYuDangKouMgr.m_GhostDict[id] then
                    -- 鬼装详细信息
                    CItemInfoMgr.ShowLinkItemInfo(LuaHengYuDangKouMgr.m_GhostDict[id], false, nil, AlignType.Default, 0, 0, 0, 0, 0)
                else
                    -- 普通信息
                    CItemInfoMgr.ShowLinkItemTemplateInfo(id, false, nil, AlignType.Default, 0, 0, 0, 0)
                end
            end)
        end
        local label = itemgo.transform:Find("CountLabel"):GetComponent(typeof(UILabel))
        if label then
            if self.m_AwardList[i].count and self.m_AwardList[i].count > 1 then
                label.gameObject:SetActive(true)
                label.text = tostring(self.m_AwardList[i].count)
            else
                label.gameObject:SetActive(false)
            end
        end
    end
    self.AwardGrid:Reposition()
end
function LuaHengYuDangKouResultWnd:Init()
    self.m_AwardList = {}
    if not self.m_ResultTbl then
        self:ShowAward()
        return
    end
    -- 鬼装
    if self.m_ResultTbl.ghostId2Count then
        print("ghost") -- zzk debug
        for k, v in pairs(self.m_ResultTbl.ghostId2Count) do
            print(k,v) -- zzk debug
            table.insert(self.m_AwardList, { id = k, count = v })
        end
    end
    -- zzk debug
    for k, v in pairs(LuaHengYuDangKouMgr.m_GhostDict) do
        print("ghost",k) -- zzk debug
    end
    -- 宝匣钥匙
    if self.m_ResultTbl.keyNum and self.m_ResultTbl.keyNum > 0 then
        print("key") -- zzk debug
        print(self.m_ResultTbl.keyNum) -- zzk debug
        local keyitemid = HengYuDangKou_Setting.GetData().KeyItemId
        table.insert(self.m_AwardList, { id = keyitemid, count = self.m_ResultTbl.keyNum })
    end
    -- 金刚钻
    if self.m_ResultTbl.jGZNum and self.m_ResultTbl.jGZNum > 0 then
        print("jgz") -- zzk debug
        print(self.m_ResultTbl.jGZNum) -- zzk debug
        local jgzitemid = HengYuDangKou_Setting.GetData().JinGangZuanItemId
        table.insert(self.m_AwardList, { id = jgzitemid, count = self.m_ResultTbl.jGZNum })
    end
    -- 装备宝箱
    if self.m_ResultTbl.boxItemId2Count then
        print("box") -- zzk debug
        for k, v in pairs(self.m_ResultTbl.boxItemId2Count) do
            print(k,v) -- zzk debug
            table.insert(self.m_AwardList, { id = k, count = v })
        end
    end
    self:ShowAward()
end
function LuaHengYuDangKouResultWnd:OnDisable()
    LuaHengYuDangKouMgr.m_ResultTbl = nil -- 清除数据
    LuaHengYuDangKouMgr.m_GhostDict = {}  -- 清除数据
end

--@region UIEvent

--@endregion UIEvent

