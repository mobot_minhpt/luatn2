local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CButton = import "L10.UI.CButton"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local CSocialWndMgr = import "L10.UI.CSocialWndMgr"
local EChatPanel = import "L10.Game.EChatPanel"

LuaQiXi2022PlotWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaQiXi2022PlotWnd, "InitialView", "InitialView", GameObject)
RegistChildComponent(LuaQiXi2022PlotWnd, "FlowChartView", "FlowChartView", GameObject)
RegistChildComponent(LuaQiXi2022PlotWnd, "LastPageButton", "LastPageButton", GameObject)
RegistChildComponent(LuaQiXi2022PlotWnd, "NextPageButton", "NextPageButton", CButton)
RegistChildComponent(LuaQiXi2022PlotWnd, "InitialViewLeftTexture", "InitialViewLeftTexture", CUITexture)
RegistChildComponent(LuaQiXi2022PlotWnd, "InitialViewRightLabel", "InitialViewRightLabel", UILabel)
RegistChildComponent(LuaQiXi2022PlotWnd, "FlowChartViewBranchItem", "FlowChartViewBranchItem", GameObject)
RegistChildComponent(LuaQiXi2022PlotWnd, "FlowChartViewResultItem", "FlowChartViewResultItem", GameObject)
RegistChildComponent(LuaQiXi2022PlotWnd, "FlowChartViewBranchRoot", "FlowChartViewBranchRoot", GameObject)
RegistChildComponent(LuaQiXi2022PlotWnd, "FlowChartViewResultRoot", "FlowChartViewResultRoot", GameObject)
RegistChildComponent(LuaQiXi2022PlotWnd, "LeftPageLabel", "LeftPageLabel", UILabel)
RegistChildComponent(LuaQiXi2022PlotWnd, "RightPageLabel", "RightPageLabel", UILabel)
RegistChildComponent(LuaQiXi2022PlotWnd, "LeftPageNumLabel", "LeftPageNumLabel", UILabel)
RegistChildComponent(LuaQiXi2022PlotWnd, "RightPageNumLabel", "RightPageNumLabel", UILabel)
RegistChildComponent(LuaQiXi2022PlotWnd, "PageRoot", "PageRoot", GameObject)
RegistChildComponent(LuaQiXi2022PlotWnd, "CountDownLabel", "CountDownLabel", UILabel)
RegistChildComponent(LuaQiXi2022PlotWnd, "TeamChannelButton", "TeamChannelButton", GameObject)
RegistChildComponent(LuaQiXi2022PlotWnd, "LineRoot", "LineRoot", GameObject)
--@endregion RegistChildComponent end
RegistClassMember(LuaQiXi2022PlotWnd, "m_Page")
RegistClassMember(LuaQiXi2022PlotWnd, "m_PageNum")
RegistClassMember(LuaQiXi2022PlotWnd, "m_BranchItems")
RegistClassMember(LuaQiXi2022PlotWnd, "m_ResultItems")
RegistClassMember(LuaQiXi2022PlotWnd, "m_BranchDataList")
RegistClassMember(LuaQiXi2022PlotWnd, "m_ResultDataList")
RegistClassMember(LuaQiXi2022PlotWnd, "m_CloseWndTime")
RegistClassMember(LuaQiXi2022PlotWnd, "m_CloseWndTick")
RegistClassMember(LuaQiXi2022PlotWnd, "m_EndingData")
RegistClassMember(LuaQiXi2022PlotWnd, "m_OptionData")
RegistClassMember(LuaQiXi2022PlotWnd, "m_BranchTreeParentData")
RegistClassMember(LuaQiXi2022PlotWnd, "m_ResultTreeParentData")

function LuaQiXi2022PlotWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.LastPageButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLastPageButtonClick()
	end)


	
	UIEventListener.Get(self.NextPageButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnNextPageButtonClick()
	end)


	
	UIEventListener.Get(self.TeamChannelButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTeamChannelButtonClick()
	end)


    --@endregion EventBind end
end

function LuaQiXi2022PlotWnd:Init()
	self.FlowChartViewBranchItem.gameObject:SetActive(false)
	self.FlowChartViewResultItem.gameObject:SetActive(false)
	self.CountDownLabel.text = ""
	self:CreateFlowChartView()
	Gac2Gas.SFEQ_QueryPlayData()

	if LuaQiXi2022Mgr.m_PlotWndShowFlowChart then
		self.m_PageNum = 0
		self:OnPageChange(1)
		return
	end

	self.m_PageNum = QiXi2022_Background.GetDataCount() 
	local hasOpenQiXi2022PlotWnd = LuaQiXi2022Mgr.m_HasOpenQiXi2022PlotWnd
	self:OnPageChange( hasOpenQiXi2022PlotWnd and (self.m_PageNum + 1) or 1)
	self.m_CloseWndTime = LuaQiXi2022Mgr.m_ClosePlotWndTime
	self:CancelCloseWndTick()
	self.m_CloseWndTick = RegisterTick(function ()
		local duration = math.floor(self.m_CloseWndTime - CServerTimeMgr.Inst.timeStamp) 
		self.CountDownLabel.text = g_MessageMgr:FormatMessage("QiXi2022PlotWnd_CountDown", duration)
		if duration <= 0 then
			CUIManager.CloseUI(CLuaUIResources.QiXi2022PlotWnd)
		end
	end,500)
end

function LuaQiXi2022PlotWnd:OnEnable()
	g_ScriptEvent:AddListener("SFEQ_SyncPlayData", self, "OnSyncPlayData")
end

function LuaQiXi2022PlotWnd:OnDisable()
	self:CancelCloseWndTick()
	LuaQiXi2022Mgr.m_PlotWndShowFlowChart = false
	Gac2Gas.SFEQ_Script_RequestCloseWnd()
	g_ScriptEvent:RemoveListener("SFEQ_SyncPlayData", self, "OnSyncPlayData")
end

function LuaQiXi2022PlotWnd:OnSyncPlayData(data)
	self.m_EndingData, self.m_OptionData = data[1], data[2]
	self:Show()
end

function LuaQiXi2022PlotWnd:CancelCloseWndTick()
	if self.m_CloseWndTick then
		UnRegisterTick(self.m_CloseWndTick)
		self.m_CloseWndTick = nil
	end
end

function LuaQiXi2022PlotWnd:Show()
	local page = self.m_Page

	self.PageRoot.gameObject:SetActive(page <= self.m_PageNum)
	self.InitialView.gameObject:SetActive(page <= self.m_PageNum)
	self.FlowChartView.gameObject:SetActive(page > self.m_PageNum)
	self.LastPageButton.gameObject:SetActive(page > 1)
	self.NextPageButton.gameObject:SetActive(page <= self.m_PageNum)
	if page <= self.m_PageNum then
		self:InitInitialView()
	else
		self:InitFlowChartView()
	end
end

function LuaQiXi2022PlotWnd:InitInitialView()
	local page = self.m_Page
	if page < 1 or page > self.m_PageNum then return end
	self.LeftPageLabel.text = page * 2 - 1
	self.RightPageLabel.text = page * 2
	self.LeftPageNumLabel.text = "/"..self.m_PageNum * 2
	self.RightPageNumLabel.text = "/"..self.m_PageNum * 2
	local data = QiXi2022_Background.GetData(page)
	if page then
		self.InitialViewLeftTexture:LoadMaterial(data.Picture)
		self.InitialViewRightLabel.text = data.Text
	end
end

function LuaQiXi2022PlotWnd:CreateFlowChartView()
	self.m_BranchItems = {}
	for i = 0, self.FlowChartViewBranchRoot.transform.childCount - 1 do
		local name = tostring(i+1)
		local p = self.FlowChartViewBranchRoot.transform:Find(name)
		Extensions.RemoveAllChildren(p.transform)
		local obj = NGUITools.AddChild(p.gameObject, self.FlowChartViewBranchItem.gameObject)
		obj.gameObject:SetActive(true)
		obj.transform.localPosition = Vector3.zero
		table.insert(self.m_BranchItems, obj)
	end
	self.m_ResultItems = {}
	for i = 0, self.FlowChartViewResultRoot.transform.childCount - 1 do
		local p = self.FlowChartViewResultRoot.transform:GetChild(i)
		Extensions.RemoveAllChildren(p.transform)
		local obj = NGUITools.AddChild(p.gameObject, self.FlowChartViewResultItem.gameObject)
		obj.gameObject:SetActive(true)
		obj.transform.localPosition = Vector3.zero
		table.insert(self.m_ResultItems, obj)
	end
	self:InitLineRoot()
end

function LuaQiXi2022PlotWnd:InitFlowChartViewBranchItem(obj, index)
	local data = self.m_BranchDataList[index]
	obj.name = data and data.ID or index
	local aStyle = obj.transform:Find("AStyle")
	local bStyle = obj.transform:Find("BStyle")
	local cStyle = obj.transform:Find("CStyle")
	aStyle.gameObject:SetActive(false)
	bStyle.gameObject:SetActive(false)
	cStyle.gameObject:SetActive(true)
	
	aStyle.transform:Find("Label"):GetComponent(typeof(UILabel)).text = data and data.Name or LocalString.GetString("开端")
	bStyle.transform:Find("Label"):GetComponent(typeof(UILabel)).text = data and data.Name or LocalString.GetString("开端")
	bStyle.transform:Find("Label"):GetComponent(typeof(UILabel)).text = data and data.Name or LocalString.GetString("开端")
	if data == nil then
		aStyle.gameObject:SetActive(true)
		bStyle.gameObject:SetActive(false)
		cStyle.gameObject:SetActive(false)
		return
	end
	local parentIndexArr = self.m_BranchTreeParentData[index]
	local isShowObj = (index <= 3)
	obj.gameObject:SetActive(isShowObj)
	if self.m_OptionData then
		local optionData = self.m_OptionData
		local state = optionData[data.ID]
		for i = 1, #parentIndexArr do
			local parentIndex = parentIndexArr[i]
			local parentData = self.m_BranchDataList[parentIndex]
			local parentRootState = optionData[parentData and parentData.ID or 0]
			isShowObj = isShowObj or (parentRootState == 2)
		end
		obj.gameObject:SetActive(isShowObj)
		aStyle.gameObject:SetActive(state == 2)
		bStyle.gameObject:SetActive(false)
		cStyle.gameObject:SetActive(state == 1 or state == nil)
		for i = 1, #parentIndexArr do
			local parentIndex = parentIndexArr[i]
			self:ShowBranchLine(index, parentIndex)
		end
	end
end

function LuaQiXi2022PlotWnd:InitFlowChartViewResultItem(obj, index)
	local data = self.m_ResultDataList[index]
	local aStyle = obj.transform:Find("AStyle")
	local cStyle = obj.transform:Find("CStyle")
	local dStyle = obj.transform:Find("DStyle")
	aStyle.gameObject:SetActive(false)
	cStyle.gameObject:SetActive(true)
	dStyle.gameObject:SetActive(false)
	local showStyle = cStyle
	local parentIndexList = self.m_ResultTreeParentData[index]
	local showObject = false
	for i = 1,#parentIndexList do
		local parentIndex = parentIndexList[i]
		local parentData = self.m_BranchDataList[parentIndex]
		if self.m_OptionData then
			local optionData = self.m_OptionData
			local parentRootState = optionData[parentData and parentData.ID or 0]
			showObject = showObject or (parentRootState == 2)
		end
	end
	obj.gameObject:SetActive(showObject)
	for i = 1,#parentIndexList do
		local parentIndex = parentIndexList[i]
		self:ShowResultLine(index, parentIndex)
	end
	if self.m_EndingData then
		local endingData = self.m_EndingData
		local hasEnd = endingData[data.Stage]
		local isTrueEnd = string.find(data.EndingName, LocalString.GetString("真"))
		aStyle.gameObject:SetActive(hasEnd and not isTrueEnd)
		cStyle.gameObject:SetActive(false)
		dStyle.gameObject:SetActive(hasEnd and isTrueEnd)
		if hasEnd then
			showStyle = isTrueEnd and dStyle or aStyle
		end
	end
	local label = showStyle.transform:Find("Label"):GetComponent(typeof(UILabel))
	label.text = showStyle == cStyle and "?" or data.EndingName
	local desLabel = showStyle.transform:Find("DesLabel")
	if desLabel then
		desLabel = desLabel:GetComponent(typeof(UILabel))
		desLabel.text = data.EndingText
	end
	local tipButton = showStyle.transform:Find("TipButton")
	if tipButton then
		tipButton.gameObject:SetActive(true)
		UIEventListener.Get(tipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			self:OnTipButtonClick(data)
		end)
	end
	local curResult = showStyle.transform:Find("CurResult")
	if curResult then
		curResult.gameObject:SetActive(not LuaQiXi2022Mgr.m_PlotWndShowFlowChart and data.Stage == LuaQiXi2022Mgr.m_PlotWndResultStage)
	end
end

function LuaQiXi2022PlotWnd:InitFlowChartView()
	local page = self.m_Page
	if page <= self.m_PageNum then return end
	self.m_BranchDataList = {}
	self.m_ResultDataList = {}
	self:InitFlowChartViewBranchItem(self.m_BranchItems[1],1)
	local i = 2
	QiXi2022_SFEQAnswerDescription.ForeachKey(function (k)
		local obj = self.m_BranchItems[i]
		local data = QiXi2022_SFEQAnswerDescription.GetData(k)
		self.m_BranchDataList[i] = data
		self:InitFlowChartViewBranchItem(obj,i)
		i = i + 1
	end)
	local j = 1
	QiXi2022_SFEQTaskDescription.ForeachKey(function (k)
		local data = QiXi2022_SFEQTaskDescription.GetData(k)
		if not String.IsNullOrEmpty(data.EndingName) then
			local obj = self.m_ResultItems[j]
			self.m_ResultDataList[j] = data
			self:InitFlowChartViewResultItem(obj, j)
			j = j + 1
		end
	end)
end

function LuaQiXi2022PlotWnd:ShowBranchLine(child, parent)
	local name = SafeStringFormat3("%dTo%d", parent, child)
	local line = self.LineRoot.transform:Find(name)
	local childRoot = self.m_BranchItems[child]
	local parentRoot = self.m_BranchItems[child]
	local optionData = self.m_OptionData
	local childData = self.m_BranchDataList[child]
	local childRootState = optionData and optionData[childData and childData.ID or 0] or 0
	if line then
		local isShow = childRoot.gameObject.activeSelf and parentRoot.gameObject.activeSelf
		line.gameObject:SetActive(isShow)
		local lineColor = NGUIText.ParseColor24(childRootState == 2 and "A5614C" or "907a74", 0)
		local depth = childRootState == 2 and 5 or 4
		local sprite = line.gameObject:GetComponent(typeof(UISprite))
		sprite.color = lineColor
		sprite.depth = depth
		local sprites = CommonDefs.GetComponentsInChildren_Component_Type(line.transform, typeof(UISprite))
		if sprites then
			for i = 0, sprites.Length - 1 do
				sprite = sprites[i]
				sprite.color = lineColor
				sprite.depth = depth
			end
		end
	end
end

function LuaQiXi2022PlotWnd:ShowResultLine(child, parent)
	local name = SafeStringFormat3("%dR%d", parent, child)
	local line = self.LineRoot.transform:Find(name)
	local childRoot = self.m_ResultItems[child]
	local parentRoot = self.m_BranchItems[child]
	if line then
		local isShow = childRoot.gameObject.activeSelf and parentRoot.gameObject.activeSelf
		line.gameObject:SetActive(isShow)	
	end
end

function LuaQiXi2022PlotWnd:InitLineRoot()
	self.m_BranchTreeParentData = {}
	self.m_ResultTreeParentData = {}
	local childCount = self.LineRoot.transform.childCount
	for i = 1, childCount do
		local line = self.LineRoot.transform:GetChild(i - 1)
		line.gameObject:SetActive(false)
		local lineName = line.name
		local iter = string.gmatch(lineName, "%d+")
		local arr = {}
		for w, v in iter do
			table.insert(arr, tonumber(w))
		end	
		if #arr == 2 then
			if string.find(lineName,"To") then
				if self.m_BranchTreeParentData[arr[2]] == nil then
					self.m_BranchTreeParentData[arr[2]] = {}
				end
				table.insert(self.m_BranchTreeParentData[arr[2]] , arr[1])
			elseif string.find(lineName,"R") then
				if not self.m_ResultTreeParentData[arr[2]] then
					self.m_ResultTreeParentData[arr[2]] = {}
				end
				table.insert(self.m_ResultTreeParentData[arr[2]], arr[1])
			end
		end
	end
end

--@region UIEvent

function LuaQiXi2022PlotWnd:OnLastPageButtonClick()
	if self.m_Page <= 1 then return end
	self:OnPageChange(self.m_Page - 1)
end

function LuaQiXi2022PlotWnd:OnNextPageButtonClick()
	self:OnPageChange(self.m_Page + 1)
end

function LuaQiXi2022PlotWnd:OnPageChange(page)
	if page < 1 then return end
	self.m_Page = page
	self:Show()
end

function LuaQiXi2022PlotWnd:OnTipButtonClick(data)
	LuaQiXi2022Mgr.m_PlotWndResultStage = data.Stage
	LuaQiXi2022Mgr.m_EndingResultData = {}
	CUIManager.ShowUI(CLuaUIResources.QiXi2022PlotResultWnd)
end

function LuaQiXi2022PlotWnd:OnTeamChannelButtonClick()
	CSocialWndMgr.ShowChatWnd(EChatPanel.Team)
end

--@endregion UIEvent

