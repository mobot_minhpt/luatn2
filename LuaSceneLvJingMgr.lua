local CPostEffectMgr = import "L10.Engine.CPostEffectMgr"
local CPostProcessingMgr = import "L10.Engine.PostProcessing.CPostProcessingMgr"

CLuaSceneLvJingMgr = {}

function CLuaSceneLvJingMgr:AddListener()
    g_ScriptEvent:RemoveListener("StartUnloadScene", self, "OnStartUnloadScene")
    g_ScriptEvent:AddListener("StartUnloadScene", self, "OnStartUnloadScene")
end

CLuaSceneLvJingMgr:AddListener()

function CLuaSceneLvJingMgr:OnStartUnloadScene()
    if CPostProcessingMgr.s_NewPostProcessingOpen then
        local postEffectCtrl = CPostProcessingMgr.Instance.MainController
        postEffectCtrl:LoadLvJingEffect(0, 0)
        postEffectCtrl:SetSpliteCharacter(false)
        postEffectCtrl:RemoveScenePhotoFilter()
        postEffectCtrl.RadialBlurEnable = false
    else
        CPostEffectMgr.Instance:DisableAllFilterEffect()
        CPostEffectMgr.Instance:RemoveScenePhotoFilter()
    end
end