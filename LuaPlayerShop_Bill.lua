local CPopupMenuInfoMgrAlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local DelegateFactory  = import "DelegateFactory"
local QnTableView = import "L10.UI.QnTableView"
local UILabel = import "UILabel"
local CPlayerShopPopupMenuInfoMgr = import "L10.UI.CPlayerShopPopupMenuInfoMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CPlayerShopMgr = import "L10.UI.CPlayerShopMgr"
local QnTipButton = import "L10.UI.QnTipButton"
local CPlayerShopData = import "L10.UI.CPlayerShopData"
local CPlayerShopBillData = import "L10.UI.CPlayerShopBillData"

LuaPlayerShop_Bill = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaPlayerShop_Bill, "QnTableView", "QnTableView", QnTableView)
RegistChildComponent(LuaPlayerShop_Bill, "QnButton", "QnButton", QnTipButton)
RegistChildComponent(LuaPlayerShop_Bill, "QnButton2", "QnButton2", QnTipButton)
RegistChildComponent(LuaPlayerShop_Bill, "DescContentLabel", "DescContentLabel", UILabel)
RegistChildComponent(LuaPlayerShop_Bill, "NoneLabel", "NoneLabel", UILabel)
--@endregion RegistChildComponent end
RegistClassMember(LuaPlayerShop_Bill,"m_DateTimeList")
RegistClassMember(LuaPlayerShop_Bill,"m_DateTimeActions")
RegistClassMember(LuaPlayerShop_Bill,"m_DateTimeActions2")
RegistClassMember(LuaPlayerShop_Bill,"m_BillList")
RegistClassMember(LuaPlayerShop_Bill,"m_SelectIndex2")

function LuaPlayerShop_Bill:Awake()
    --@region EventBind: Dont Modify Manually!
	CommonDefs.AddOnClickListener(self.QnButton.gameObject,DelegateFactory.Action_GameObject(function (go)
	    self:OnQnButtonClick()
	end),false)
	CommonDefs.AddOnClickListener(self.QnButton2.gameObject,DelegateFactory.Action_GameObject(function (go)
	    self:OnQnButton2Click()
	end),false)
    --@endregion EventBind end
end

function LuaPlayerShop_Bill:Init()
	local time = CServerTimeMgr.Inst:GetZone8Time()
	self.m_DateTimeList = {}
	self.m_DateTimeActions = {}
	self.m_DateTimeActions2 = {}
	self.m_SelectIndex2 = 0
	for i = 1, 5 do
		table.insert(self.m_DateTimeList,time)
		table.insert(self.m_DateTimeActions,PopupMenuItemData(time:ToString("yyyy-MM-dd"), DelegateFactory.Action_int(function (index) 
			self:OnSelectQueryTime(index)
		end), false, nil))
		time = time:AddDays(-1)
	end
	local action2NameArr = {LocalString.GetString("仅卖出"),LocalString.GetString("仅买入"),LocalString.GetString("显示全部")}
	for i = 1, 3 do
		table.insert(self.m_DateTimeActions2,PopupMenuItemData(action2NameArr[i], DelegateFactory.Action_int(function (index) 
			self.QnButton2.Text = action2NameArr[i]
			self:OnSelectIndex2(index)
		end), false, nil))
	end
	self.DescContentLabel.text = SafeStringFormat3(LocalString.GetString("玩家商店交易需要扣除[ff0000]%d%%[-]的交易税。"),math.ceil(CPlayerShopMgr.Transaction_Tax_Client * 100))
	self.QnTableView.m_DataSource = DefaultTableViewDataSource.Create(
		function() return self.m_BillList and #self.m_BillList or 0 end,
		function(item,row)
			self:InitItem(item,row)
	end)
	self.NoneLabel.gameObject:SetActive(true)
	self.QnButton2.Text = action2NameArr[1]
	self:QueryRecordsOnTime(self.m_DateTimeList[1])
end

function LuaPlayerShop_Bill:OnEnable()
	g_ScriptEvent:AddListener("QueryTradeRecordsResult", self, "OnQueryTradeRecordsResult")
end

function LuaPlayerShop_Bill:OnDisable()
	g_ScriptEvent:RemoveListener("QueryTradeRecordsResult", self, "OnQueryTradeRecordsResult")
end

function LuaPlayerShop_Bill:OnQueryTradeRecordsResult(args)
	local timeformat,shopId,sellbillData,buybillData = args[0],args[1],args[2],args[3]
	self.m_BillList = {}
	for i = 0,sellbillData.Count - 1 do
		table.insert(self.m_BillList,{data = CPlayerShopBillData(sellbillData[i]),isSell = true})
	end
	for i = 0,buybillData.Count - 1 do
		local itemId, num, price = buybillData[i][0],buybillData[i][1],buybillData[i][2]
		table.insert(self.m_BillList,{isSell = false,data = {ItemName = CPlayerShopMgr.Inst:GetName(itemId),ItemCount = num,TotalPrice = price}})
	end
	table.sort(self.m_BillList,function (a,b)
		return a.data.TotalPrice > b.data.TotalPrice 
	end)
	self.NoneLabel.gameObject:SetActive(#self.m_BillList == 0)
	self.QnTableView:ReloadData()
end

function LuaPlayerShop_Bill:InitItem(item,row)
	local data = self.m_BillList[row + 1]
	if data.isSell then
		item.gameObject:SetActive(self.m_SelectIndex2 ~= 1) 
	else
		item.gameObject:SetActive(self.m_SelectIndex2 ~= 0) 
	end
	item.transform:Find("BuyTag").gameObject:SetActive(not data.isSell)
	item.transform:Find("SellTag").gameObject:SetActive(data.isSell)
	item.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = data.data.ItemName
	item.transform:Find("NumLabel"):GetComponent(typeof(UILabel)).text = data.data.ItemCount
	local costLabel = item.transform:Find("CostLabel"):GetComponent(typeof(UILabel))
	costLabel.text = SafeStringFormat3("%s%d",data.isSell and "+" or "-",data.data.TotalPrice)
	costLabel.color = UILabel.GetMoneyColor(data.data.TotalPrice)
end

function LuaPlayerShop_Bill:OnSelectQueryTime(row)
	local time = self.m_DateTimeList[row + 1]
	self:QueryRecordsOnTime(time)
end

function LuaPlayerShop_Bill:OnSelectIndex2(row)
	self.m_SelectIndex2 = row
	self.QnTableView:ReloadData()
end

function LuaPlayerShop_Bill:QueryRecordsOnTime(time)
	self.QnButton.Text = time:ToString("yyyy-MM-dd")
	local timeFormat = time.Year * 10000 + time.Month * 100 + time.Day
	Gac2Gas.QueryTradeRecords(CPlayerShopData.Main.PlayerShopId, timeFormat)
end

--@region UIEvent

function LuaPlayerShop_Bill:OnQnButtonClick()
	-- CPopupMenuInfoMgr.ShowPopupMenu(Table2Array(self.m_DateTimeActions, MakeArrayClass(PopupMenuItemData)), self.QnButton.transform, CPopupMenuInfoMgrAlignType.Bottom,1,DelegateFactory.Action(function ()
    --     self.QnButton:SetTipStatus(false)
    -- end),600,true,300)
	CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenu(Table2Array(self.m_DateTimeActions, MakeArrayClass(PopupMenuItemData)), self.QnButton.transform, CPopupMenuInfoMgrAlignType.Bottom, 1, nil, nil, DelegateFactory.Action(function ()
        self.QnButton:SetTipStatus(false)
    end), 600,300)
end

function LuaPlayerShop_Bill:OnQnButton2Click()
	CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenu(Table2Array(self.m_DateTimeActions2, MakeArrayClass(PopupMenuItemData)), self.QnButton2.transform, CPopupMenuInfoMgrAlignType.Bottom, 1, nil, nil, DelegateFactory.Action(function ()
        self.QnButton2:SetTipStatus(false)
    end), 600,320)
end
--@endregion UIEvent

