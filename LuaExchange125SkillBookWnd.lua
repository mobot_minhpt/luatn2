local LuaTweenUtils = import "LuaTweenUtils"
local Vector3 = import "UnityEngine.Vector3"
local CommonDefs = import "L10.Game.CommonDefs"
local Quaternion = import "UnityEngine.Quaternion"
local Ease = import "DG.Tweening.Ease"
local UIWidget = import "UIWidget"
local CBaseWnd = import "L10.UI.CBaseWnd"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CItem = import "L10.Game.CItem"
local CItemMgr = import "L10.Game.CItemMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Item_Item = import "L10.Game.Item_Item" -- 为了用一下CUICommonDef.GetItemCellBorder方法
local DelegateFactory = import "DelegateFactory"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CUIFx = import "L10.UI.CUIFx"

LuaExchange125SkillBookWnd = class()
RegistClassMember(LuaExchange125SkillBookWnd,"m_Bg1")
RegistClassMember(LuaExchange125SkillBookWnd,"m_Bg2")
RegistClassMember(LuaExchange125SkillBookWnd,"m_SourceItemRoot")
RegistClassMember(LuaExchange125SkillBookWnd,"m_TargetItem")
RegistClassMember(LuaExchange125SkillBookWnd,"m_ExchangeButton")
RegistClassMember(LuaExchange125SkillBookWnd,"m_ReceiveButton")
RegistClassMember(LuaExchange125SkillBookWnd,"m_CenterPivot")

RegistClassMember(LuaExchange125SkillBookWnd,"m_ExchangeFx")

RegistClassMember(LuaExchange125SkillBookWnd,"m_ExchangeId")
RegistClassMember(LuaExchange125SkillBookWnd,"m_ConsumeItemsData")
RegistClassMember(LuaExchange125SkillBookWnd,"m_TargetItemData")

RegistClassMember(LuaExchange125SkillBookWnd,"m_FadeInTweener")

function LuaExchange125SkillBookWnd:Awake()
    self:InitComponents()
end

function LuaExchange125SkillBookWnd:InitComponents()
    self.m_Bg1 = self.transform:Find("Anchor/Bg"):GetComponent(typeof(UIWidget))
    self.m_Bg2 = self.transform:Find("Anchor/Bg2"):GetComponent(typeof(UIWidget))
    self.m_SourceItemRoot = self.transform:Find("Anchor/Items")
    self.m_TargetItem = self.transform:Find("Anchor/TargetItem")
    self.m_ExchangeButton = self.transform:Find("Anchor/ExchangeButton").gameObject
    self.m_ReceiveButton = self.transform:Find("Anchor/ReceiveButton").gameObject
    self.m_CenterPivot = self.transform:Find("Anchor/Bg/CenterPivot")
    self.m_ExchangeFx = self.transform:Find("Anchor/TargetItem/Fx"):GetComponent(typeof(CUIFx))
    self.m_ExchangeButton:SetActive(true)
    self.m_ReceiveButton:SetActive(false)
    self.m_Bg1.gameObject:SetActive(true)
    self.m_Bg2.gameObject:SetActive(true)
    self.m_Bg1.alpha = 1
    self.m_Bg2.alpha = 0

    CommonDefs.AddOnClickListener(self.m_ExchangeButton, DelegateFactory.Action_GameObject(function(go) self:OnExchangeButtonClick(go) end), false)
    CommonDefs.AddOnClickListener(self.m_ReceiveButton, DelegateFactory.Action_GameObject(function(go) self:OnReceiveButtonClick(go) end), false)
end

function LuaExchange125SkillBookWnd:Init()

    local cls = CClientMainPlayer.Inst and EnumToInt(CClientMainPlayer.Inst.Class) or 0
    local data = Exchange_Exchange125Skill.GetData(cls)
    if data == nil then
        self:Close()
        return
    end
    if CClientMainPlayer.Inst.SkillProp:IsOriginalSkillClsExist(data.SkillID) then
        g_MessageMgr:ShowMessage("Exchange_125_Skill_Book_Skill_Already_Learned")
        self:Close()
        return true
    end
    self.m_ExchangeId = data.ExchangeID
    local exchangeInfo = Exchange_Exchange.GetData(data.ExchangeID)
    self.m_ConsumeItemsData = {}
    for i=0,exchangeInfo.ConsumeItems.Length-1 do
        local tmp = exchangeInfo.ConsumeItems[i]
        table.insert(self.m_ConsumeItemsData, {Id=tmp[0], Count=tmp[1], NeedBind=tmp[2]})
    end

    local newItem = exchangeInfo.NewItems[0]
    self.m_TargetItemData = {Id=newItem[0], Count=newItem[1]}
    self:SetSelectedIcon(nil) --默认不选中任何Item
    self:LoadData(true)
end

function LuaExchange125SkillBookWnd:LoadData()
    local dataCount = #self.m_ConsumeItemsData
    for i=0, self.m_SourceItemRoot.childCount-1 do
        local child = self.m_SourceItemRoot:GetChild(i)
        if i<dataCount then
            child.gameObject:SetActive(true)
            self:InitConsumeItem(child, self.m_ConsumeItemsData[i+1])
        else
            child.gameObject:SetActive(false)
        end
    end
    self:InitTargetItem()
end

function LuaExchange125SkillBookWnd:SetSelectedIcon(go)
    for i=0, self.m_SourceItemRoot.childCount-1 do
        local child = self.m_SourceItemRoot:GetChild(i)
        child:Find("Selected").gameObject:SetActive(child.gameObject == go)
    end
    self.m_TargetItem:Find("Selected").gameObject:SetActive(self.m_TargetItem.gameObject == go)
end

function LuaExchange125SkillBookWnd:InitConsumeItem(itemTransform, data)
    local item = Item_Item.GetData(data.Id)
    local numInBag = CItemMgr.Inst:GetItemCount(data.Id)
    itemTransform:Find("ItemIcon"):GetComponent(typeof(CUITexture)):LoadMaterial(item.Icon)
    itemTransform:Find("QualityIcon"):GetComponent(typeof(UISprite)).spriteName = CUICommonDef.GetItemCellBorder(item, nil, false)
    itemTransform:Find("NumLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3( numInBag<data.Count and "[ff0000]%d[-]/%d" or "%d/%d", numInBag, data.Count)
    itemTransform:Find("Mask").gameObject:SetActive(numInBag<data.Count)
    itemTransform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = CItem.GetColoredDisplayName(item)
    CommonDefs.AddOnClickListener(itemTransform.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnSourceItemClick(go, data) end), false)
end

function LuaExchange125SkillBookWnd:InitTargetItem()
    local item = Item_Item.GetData(self.m_TargetItemData.Id)
    self.m_TargetItem:Find("ItemIcon"):GetComponent(typeof(CUITexture)):LoadMaterial(item.Icon)
    self.m_TargetItem:Find("QualityIcon"):GetComponent(typeof(UISprite)).spriteName = CUICommonDef.GetItemCellBorder(item, nil, false)
    self.m_TargetItem:Find("NameLabel"):GetComponent(typeof(UILabel)).text = CItem.GetColoredDisplayName(item)
    CommonDefs.AddOnClickListener(self.m_TargetItem.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnTargetItemClick(go, self.m_TargetItemData) end), false)
end

function LuaExchange125SkillBookWnd:OnEnable()
    g_ScriptEvent:AddListener("SendItem", self, "OnSendItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "OnSetItemAt")
end

function LuaExchange125SkillBookWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendItem", self, "OnSendItem")
    g_ScriptEvent:RemoveListener("SetItemAt", self, "OnSetItemAt")
    if self.m_FadeInTweener then
        LuaTweenUtils.Kill(self.m_FadeInTweener, false)
        self.m_FadeInTweener = nil
    end
end

function LuaExchange125SkillBookWnd:OnSendItem(args)
	local itemId = args[0]
	local item = CItemMgr.Inst:GetById(itemId)

	if not item then return end

	self:LoadData()
end

function LuaExchange125SkillBookWnd:OnSetItemAt(args)
	local place = args[0]
	local pos = args[1]
	local oldItemId = args[2]
	local newItemId = args[3]

	if place ~= EnumItemPlace.Bag then return end

    self:LoadData()
    local item = CItemMgr.Inst:GetById(newItemId)
    if item and item.TemplateId == self.m_TargetItemData.Id then --这里假定包裹里面没有，不然会有问题，由于是通用RPC没有专门通知，借用通用消息来处理一下合成动画
        self:DoAnimation()
    end
end

function LuaExchange125SkillBookWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaExchange125SkillBookWnd:OnSourceItemClick(go, data)
    self:SetSelectedIcon(go)
    if CItemMgr.Inst:GetItemCount(data.Id)<data.Count then
        --显示获取按钮
        local t_name={}
	    t_name[1]=LocalString.GetString("获取") 
	    local t_action={}
	    t_action[1] = function ()
            CItemAccessListMgr.Inst:ShowItemAccessInfo(data.Id, true, nil, CTooltipAlignType.Right)
	    end
        local actionSource=DefaultItemActionDataSource.Create(1,t_action,t_name)
        CItemInfoMgr.ShowLinkItemTemplateInfo(data.Id, false, actionSource, AlignType.Default,0, 0, 0, 0)
    else
        CItemInfoMgr.ShowLinkItemTemplateInfo(data.Id, false, nil, AlignType.Default,0, 0, 0, 0)
    end
end

function LuaExchange125SkillBookWnd:OnTargetItemClick(go, data)
    self:SetSelectedIcon(go)
    CItemInfoMgr.ShowLinkItemTemplateInfo(data.Id, false, nil, AlignType.Default,0, 0, 0, 0)
end

function LuaExchange125SkillBookWnd:OnExchangeButtonClick(go)
    if CItemMgr.Inst:GetItemCount(self.m_TargetItemData.Id)>0 then
        g_MessageMgr:ShowMessage("Exchange_125_Skill_Book_Already_Exist_Book")
        return
    end
    --发送合成请求，之后监听物品变动消息，有物品添加到包裹时，播放合成动画
    Gac2Gas.RequestExchangeItems(self.m_ExchangeId, 1)
end

function LuaExchange125SkillBookWnd:OnReceiveButtonClick(go)
    self:Close()
end

function LuaExchange125SkillBookWnd:DoAnimation()

    -- 隐藏按钮
    self.m_ExchangeButton.gameObject:SetActive(false)
    local tbl = {}
    for i=0, self.m_SourceItemRoot.childCount-1 do
        local child = self.m_SourceItemRoot:GetChild(i)
        local startPos = child.position
        local endPos = self.m_CenterPivot.position
        local tangentPos = self:CalculateCubicBezierTangentPos(startPos, endPos, -80, 1)
        tbl[i+1] = {}
        tbl[i+1].child = child
        tbl[i+1].startPos = startPos
        tbl[i+1].endPos = endPos
        tbl[i+1].tangentPos = tangentPos
    end

    local tweener = LuaTweenUtils.TweenFloat(0, 1, 1.2, function ( val )
        if not self.m_SourceItemRoot then return end
        self.m_SourceItemRoot:GetComponent(typeof(UIWidget)).alpha = 1 - val
        self.m_Bg1.alpha = 1 - val
        for __,data in pairs(tbl) do
            data.child.position = self:CalculateCubicBezierPos(val, data.startPos, data.endPos, data.tangentPos)
            data.child.localScale = Vector3(1-val, 1-val, 1-val)
        end
    end)
    LuaTweenUtils.SetEase(tweener, Ease.InExpo)

    self.m_ReceiveButton:SetActive(true)
    self.m_ReceiveButton.transform.localScale = Vector3.zero
    local fadeInTweener = LuaTweenUtils.TweenFloat(0, 1, 0.2, function ( val )
        if not self.m_Bg2 then return end
        --新背景出现
        self.m_Bg2.alpha = val
        --遮罩消失
        self.m_TargetItem:Find("Mask"):GetComponent(typeof(UIWidget)).alpha = 1 - val
        --整体放大
        self.m_TargetItem.transform.localScale = Vector3(1 + val*0.1, 1 + val*0.1, 1 + val*0.1)
        --收下按钮出现
        self.m_ReceiveButton.transform.localScale = Vector3(val, val, val)
    end, function ()
        self.m_FadeInTweener = nil
        self.m_ExchangeFx:LoadFx("fx/ui/prefab/UI_huanranyixin_cizhui.prefab")
    end)
    LuaTweenUtils.SetEase(fadeInTweener, Ease.InOutQuint)
    LuaTweenUtils.SetDelay(fadeInTweener, 1.2)

    self.m_FadeInTweener = fadeInTweener
end


-- Bezier曲线
function LuaExchange125SkillBookWnd:CalculateCubicBezierPos(t, startPos, endPos, tangentPos)
    return Vector3(self:CalculateCubicBezierValue(t, startPos.x, endPos.x, tangentPos.x),
                    self:CalculateCubicBezierValue(t, startPos.y, endPos.y, tangentPos.y),
                    self:CalculateCubicBezierValue(t, startPos.z, endPos.z, tangentPos.z))
end
function LuaExchange125SkillBookWnd:CalculateCubicBezierValue(t, startVal, endVal, tangentVal)
    return (1 - t) * (1 - t) * startVal + 2 * t * (1 - t) * tangentVal + t * t * endVal
end

function LuaExchange125SkillBookWnd:CalculateCubicBezierTangentPos(startPos, endPos, startAngle, ratio)
    return CommonDefs.op_Multiply_Quaternion_Vector3(Quaternion.AngleAxis(startAngle, Vector3.forward), 
        Vector3((endPos.x-startPos.x)*ratio, (endPos.y-startPos.y)*ratio,(endPos.z-startPos.z)*ratio))
end