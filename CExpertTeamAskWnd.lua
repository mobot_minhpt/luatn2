-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CExpertTeamAskWnd = import "L10.UI.CExpertTeamAskWnd"
local CExpertTeamMgr = import "L10.Game.CExpertTeamMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local DelegateFactory = import "DelegateFactory"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CExpertTeamAskWnd.m_Init_CS2LuaHook = function (this) 
    UIEventListener.Get(this.backBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
        this:Close()
    end)

    UIEventListener.Get(this.sendBtn).onClick = MakeDelegateFromCSFunction(this.SendQuestion, VoidDelegate, this)
    UIEventListener.Get(this.scoreChooseBtn).onClick = MakeDelegateFromCSFunction(this.ScoreChooseBtn, VoidDelegate, this)
    this.totalScoreLabel.text = tostring((CClientMainPlayer.Inst.PlayProp.JingLingExpertInfo.TotalScore - CClientMainPlayer.Inst.PlayProp.JingLingExpertInfo.TotalConsumeScore))
    this.chooseScore = this.scoreArray[0]
end
CExpertTeamAskWnd.m_ScoreChooseBtn_CS2LuaHook = function (this, go) 
    local rows = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
    CommonDefs.ListAdd(rows, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString(tostring(this.scoreArray[0])), MakeDelegateFromCSFunction(this.SocreSetting, MakeGenericClass(Action1, Int32), this), false, nil))
    CommonDefs.ListAdd(rows, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString(tostring(this.scoreArray[1])), MakeDelegateFromCSFunction(this.SocreSetting, MakeGenericClass(Action1, Int32), this), false, nil))
    CommonDefs.ListAdd(rows, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString(tostring(this.scoreArray[2])), MakeDelegateFromCSFunction(this.SocreSetting, MakeGenericClass(Action1, Int32), this), false, nil))
    CommonDefs.ListAdd(rows, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString(tostring(this.scoreArray[3])), MakeDelegateFromCSFunction(this.SocreSetting, MakeGenericClass(Action1, Int32), this), false, nil))
    CommonDefs.ListAdd(rows, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString(tostring(this.scoreArray[4])), MakeDelegateFromCSFunction(this.SocreSetting, MakeGenericClass(Action1, Int32), this), false, nil))
    CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(rows), go.transform, CPopupMenuInfoMgr.AlignType.Right)
end
CExpertTeamAskWnd.m_SendQuestion_CS2LuaHook = function (this, go) 
    if System.String.IsNullOrEmpty(this.questionText.value) then
        g_MessageMgr:ShowMessage("Content_Cannot_Be_Empty")
        return
    end

    if CommonDefs.StringLength(this.questionText.value) > CExpertTeamAskWnd.MaxNum then
        g_MessageMgr:ShowMessage("Hongbao_Input_Limit", CExpertTeamAskWnd.MaxNum)
        return
    end

    local chooseValue = this.chooseScore
    if chooseValue > (CClientMainPlayer.Inst.PlayProp.JingLingExpertInfo.TotalScore - CClientMainPlayer.Inst.PlayProp.JingLingExpertInfo.TotalConsumeScore) then
        g_MessageMgr:ShowMessage("JINGLINGEXPERT_RELEASE_NO_SCORE")
        return
    end
    local sendContent = CExpertTeamMgr.GetSendText(this.questionText.value)

    if System.String.IsNullOrEmpty(sendContent) then
        g_MessageMgr:ShowMessage("Speech_Violation")
        return
    end

    Gac2Gas.ReleaseExpertQuestion(chooseValue, sendContent)
    this:Close()
end
