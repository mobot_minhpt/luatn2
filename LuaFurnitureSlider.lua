local UISlider = import "UISlider"
local ETickType = import "L10.Engine.ETickType"
local EnumFurnitureSliderOT = import "L10.UI.EnumFurnitureSliderOT"
local CZuoanFurnitureMgr = import "L10.UI.CZuoanFurnitureMgr"
local CTickMgr = import "L10.Engine.CTickMgr"
local CFurnitureSliderMgr = import "L10.UI.CFurnitureSliderMgr"
local CClientFurnitureMgr = import "L10.Game.CClientFurnitureMgr"
local QnNewSlider=import "L10.UI.QnNewSlider"


CLuaFurnitureSlider = class()
RegistClassMember(CLuaFurnitureSlider,"Slider")
RegistClassMember(CLuaFurnitureSlider,"FurnitureId")
RegistClassMember(CLuaFurnitureSlider,"m_VerticalSlider")


function CLuaFurnitureSlider:Awake()
    self.Slider = self.transform:Find("Anchor/NumberSlider"):GetComponent(typeof(QnNewSlider))
    self.FurnitureId = 0

    self.m_VerticalSlider = self.transform:Find("Anchor/VerticalSlider"):GetComponent(typeof(QnNewSlider))

    --huamu
    self.m_VerticalSlider.gameObject:SetActive(false)
end

function CLuaFurnitureSlider:Init()
    local enableXuanFu = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.IsOpenHouseXuanFuEdit>0 or false

    --花木类支持竖直方向移动
    if enableXuanFu then
        local furniture = CClientFurnitureMgr.Inst:GetFurniture(CFurnitureSliderMgr.CurFurnitureId)
        if furniture ~= nil then
            local data = Zhuangshiwu_Zhuangshiwu.GetData(furniture.TemplateId)
            if data and data.Type == LuaEnumFurnitureSubType.eHuamu then
                self.m_VerticalSlider.gameObject:SetActive(true)
            end
        end
    end

    self:InitFurniture(CFurnitureSliderMgr.CurFurnitureId)
    self.Slider.OnValueChanged = DelegateFactory.Action_float(function(v)
        self:OnValueChanged(v)
    end)

    self.m_VerticalSlider.OnValueChanged = DelegateFactory.Action_float(function(v)
        self:OnVerticalValueChanged(v)
    end)

end

function CLuaFurnitureSlider:OnDisable( )
    local furniture = CClientFurnitureMgr.Inst:GetFurniture(self.FurnitureId)
    if furniture ~= nil then
        if CClientFurnitureMgr.Inst:IsPenjingFurniture(furniture.TemplateId) then
            CZuoanFurnitureMgr.Inst:ShowHuamuPopupMenu()
        else
            furniture:ShowActionPopupMenu()
        end
    end
end
function CLuaFurnitureSlider:InitFurniture( furnitureId) 
    local furniture = CClientFurnitureMgr.Inst:GetFurniture(furnitureId)
    if furniture ~= nil then
        self.FurnitureId = furnitureId

        if CFurnitureSliderMgr.CurOT == EnumFurnitureSliderOT.eScale or CFurnitureSliderMgr.CurOT == EnumFurnitureSliderOT.ePenzaiHuamuScale then
            self:InitForScale(furniture)
        end

        --可上下移动
        if self.m_VerticalSlider.gameObject.activeSelf then
            local y = furniture.RO.Position.y
            local x = furniture.RO.Position.x
            local z = furniture.RO.Position.z
            local xOffset = x - math.floor(x)
            local zOffset = z - math.floor(z)

            local xPos = math.floor(x) + 0.5
            local zPos = math.floor(z) + 0.5

            local posY = CClientFurnitureMgr.Inst:GetLogicHeightByTemplateId(furniture.TemplateId,xPos, zPos)
            local yOffset = y - posY

            local heightInterval = Zhuangshiwu_Setting.GetData().PlantOffsetInterval
            local percent = (yOffset - heightInterval[0]) / (heightInterval[1] - heightInterval[0])
            percent = math.min(math.max(percent, 0), 1)
            self.m_VerticalSlider:GetComponent(typeof(UISlider)).value = percent
        end
    end
end
function CLuaFurnitureSlider:InitForScale( furniture) 
    local fScale = 1
    if CFurnitureSliderMgr.CurOT == EnumFurnitureSliderOT.eScale then
        fScale = furniture.RO.Scale
    elseif CFurnitureSliderMgr.CurOT == EnumFurnitureSliderOT.ePenzaiHuamuScale and furniture.PenzaiRO ~= nil then
        fScale = furniture.PenzaiRO.Scale
    end

    if furniture.IsWinter then
        fScale = furniture.m_OriginScale
    end

    local scaleInterval = Zhuangshiwu_Setting.GetData().FurnitureScaleInterval
    local percent = 0.5
    if fScale >= 1 then
        percent = percent + ((fScale - 1) / (scaleInterval[0] - 1) * 0.5)
    else
        percent = (fScale - scaleInterval[1]) / (1 - scaleInterval[1])
    end

    CommonDefs.GetComponent_Component_Type(self.Slider, typeof(UISlider)).value = percent
end
function CLuaFurnitureSlider:OnValueChanged( value) 
    local furniture = CClientFurnitureMgr.Inst:GetFurniture(self.FurnitureId)
    if furniture ~= nil then
        local newScale = 1
        local percent = CommonDefs.GetComponent_Component_Type(self.Slider, typeof(UISlider)).value

        local scaleInterval = Zhuangshiwu_Setting.GetData().FurnitureScaleInterval
        if percent >= 0.5 then
            newScale = (percent - 0.5) / 0.5 * (scaleInterval[0] - 1) + 1
        else
            newScale = percent / 0.5 * (1 - scaleInterval[1]) + scaleInterval[1]
        end
        local ui8Scale = CClientFurnitureMgr.GetScaleFloatToUI8(newScale)

        if CFurnitureSliderMgr.CurOT == EnumFurnitureSliderOT.eScale then
            furniture.RO.Scale = newScale
            furniture:UpdateWinterSkinScale(newScale)
        elseif CFurnitureSliderMgr.CurOT == EnumFurnitureSliderOT.ePenzaiHuamuScale then
            furniture.PenzaiHuamuScale = newScale
        end

        self:RegisterScaleTick(self.FurnitureId, ui8Scale)
    end
end

function CLuaFurnitureSlider:OnVerticalValueChanged(value)
    local furniture = CClientFurnitureMgr.Inst:GetFurniture(self.FurnitureId)
    if furniture ~= nil then
        local percent =value
        local heightInterval = Zhuangshiwu_Setting.GetData().PlantOffsetInterval
        local yOffset = heightInterval[0]+(heightInterval[1]-heightInterval[0])*percent

        local y = furniture.RO.Position.y
        local x = furniture.RO.Position.x
        local z = furniture.RO.Position.z

        local xPos = math.floor( x )+0.5
        local zPos = math.floor( z )+0.5

        local posY = CClientFurnitureMgr.Inst:GetLogicHeightByTemplateId(furniture.TemplateId,xPos, zPos)
        furniture.RO.Position = Vector3(x,posY+yOffset,z)

        local xOffset = x-math.floor( x )
        local zOffset = z-math.floor( z )

        if CFurnitureSliderMgr.mHeightRpcTick ~= nil then
            invoke(CFurnitureSliderMgr.mHeightRpcTick)
            CFurnitureSliderMgr.mHeightRpcTick = nil
        end
        CFurnitureSliderMgr.mHeightRpcTick = CTickMgr.Register(DelegateFactory.Action(function()
            Gac2Gas.ShiftFurniture(self.FurnitureId, xOffset, yOffset, zOffset)
        end), 1000, ETickType.Once)
    end
end

function CLuaFurnitureSlider:RegisterScaleTick( fid, scale) 
    self:UnRegisterScaleTick()

    CFurnitureSliderMgr.mScaleRpcTick = CTickMgr.Register(DelegateFactory.Action(function () 
        local fur = CClientFurnitureMgr.Inst:GetFurniture(fid)
        if fur ~= nil then
            if CFurnitureSliderMgr.CurOT == EnumFurnitureSliderOT.eScale then
                Gac2Gas.ScaleFurniture(fid, scale)
            elseif CFurnitureSliderMgr.CurOT == EnumFurnitureSliderOT.ePenzaiHuamuScale then
                Gac2Gas.ScalePenzaiHuamu(fid, scale)
            end
            CFurnitureSliderMgr.mScaleRpcTick = nil
        end
    end), 1000, ETickType.Once)
end
function CLuaFurnitureSlider:UnRegisterScaleTick( )
    if CFurnitureSliderMgr.mScaleRpcTick ~= nil then
        invoke(CFurnitureSliderMgr.mScaleRpcTick)
        CFurnitureSliderMgr.mScaleRpcTick = nil
    end
end


