require("common/common_include")

local UIGrid = import "UIGrid"
local Profession = import "L10.Game.Profession"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local AlignType = import "CPlayerInfoMgr+AlignType"
local Vector3 = import "UnityEngine.Vector3"
local CTeamGroupMgr = import "L10.Game.CTeamGroupMgr"
local Constants = import "L10.Game.Constants"

LuaGuildLeagueInviteWnd = class()

RegistClassMember(LuaGuildLeagueInviteWnd, "InfoGrid")
RegistClassMember(LuaGuildLeagueInviteWnd, "InfoTemplate")
RegistClassMember(LuaGuildLeagueInviteWnd, "InviteButton")

RegistClassMember(LuaGuildLeagueInviteWnd, "PlayerInfos")

function LuaGuildLeagueInviteWnd:Init()
	self:InitClassMembers()
	self:InitValues()

	self:UpdatePLayerInfos(LuaGuildLeagueMgr.m_PlayersToInvite)
end

function LuaGuildLeagueInviteWnd:InitClassMembers()
	self.InfoGrid = self.transform:Find("Anchor/Infos/InfoPanel/InfoGrid"):GetComponent(typeof(UIGrid))
	self.InfoTemplate = self.transform:Find("Anchor/Infos/InfoTemplate").gameObject
	self.InfoTemplate:SetActive(false)
	self.InviteButton = self.transform:Find("Anchor/InviteBtn").gameObject

end

function LuaGuildLeagueInviteWnd:InitValues()

	self.PlayerInfos = {}

	local onInviteButtonClicked = function (go)
		self:OnInviteButtonClicked(go)
	end
	CommonDefs.AddOnClickListener(self.InviteButton, DelegateFactory.Action_GameObject(onInviteButtonClicked), false)

end

function LuaGuildLeagueInviteWnd:OnInviteButtonClicked(go)
	if not CClientMainPlayer.Inst then return end

	if CTeamGroupMgr.Instance:GetGroupLeaderID() == CClientMainPlayer.Inst.Id then
		for i = 1, #self.PlayerInfos, 1 do
			Gac2Gas.InvitePlayerJoinTeamGroup(self.PlayerInfos[i].playerId)
		end
		CUIManager.CloseUI(CLuaUIResources.GuildLeagueInviteWnd)
	else
		g_MessageMgr:ShowMessage("GUILD_LEAGUE_INVITE_NOT_LEADER")
	end
end
	
function LuaGuildLeagueInviteWnd:UpdatePLayerInfos(infos)
	if not infos then return end
	self.PlayerInfos = infos
	LuaGuildLeagueMgr.m_PlayersToInvite = nil

	CUICommonDef.ClearTransform(self.InfoGrid.transform)
	for i = 1, #self.PlayerInfos, 1 do
		local go = NGUITools.AddChild(self.InfoGrid.gameObject, self.InfoTemplate)
		self:InitItem(go, self.PlayerInfos[i], i)
		go:SetActive(true)
	end
	self.InfoGrid:Reposition()

end

function LuaGuildLeagueInviteWnd:InitItem(go, info, index)
	local BG = go.transform:GetComponent(typeof(UISprite))

	local yushu = math.floor((index - 1) / 3)
	local colorIdx = yushu - math.floor(yushu / 2) * 2

	if colorIdx == 1 then
		BG.spriteName = Constants.OddBgSpirite
	else
		BG.spriteName = Constants.EvenBgSprite
	end

	local JobImage = go.transform:Find("JobImage"):GetComponent(typeof(UISprite))
	JobImage.spriteName = nil
	local PlayerNameLabel = go.transform:Find("PlayerNameLabel"):GetComponent(typeof(UILabel))

	JobImage.spriteName = Profession.GetIconByNumber(info.playerJob)
	PlayerNameLabel.text = tostring(info.playerName)

	local onClickedPlayer = function (go)
		CPlayerInfoMgr.ShowPlayerPopupMenu(info.playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, "", nil, Vector3.zero, AlignType.Default)
	end

	CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(onClickedPlayer), false)
end

function LuaGuildLeagueInviteWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateGuildLeagueInvite", self, "UpdatePLayerInfos")
end

function LuaGuildLeagueInviteWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateGuildLeagueInvite", self, "UpdatePLayerInfos")
end



return LuaGuildLeagueInviteWnd
