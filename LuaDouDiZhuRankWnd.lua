local LuaGameObject=import "LuaGameObject"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local CPlayerInfoMgr=import "CPlayerInfoMgr"
local EnumPlayerInfoContext=import "L10.Game.EnumPlayerInfoContext"
local EChatPanel=import "L10.Game.EChatPanel"
local AlignType=import "CPlayerInfoMgr+AlignType"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CGuildMgr = import "L10.Game.CGuildMgr"

local UILabel=import "UILabel"
local QnTableView=import "L10.UI.QnTableView"


CLuaDouDiZhuRankWnd=class()
-- 主玩家相关信息
RegistClassMember(CLuaDouDiZhuRankWnd,"m_MyRankLabel")
RegistClassMember(CLuaDouDiZhuRankWnd,"m_MyNameLabel")
RegistClassMember(CLuaDouDiZhuRankWnd,"m_MyTimeLabel")
RegistClassMember(CLuaDouDiZhuRankWnd,"m_MyScoreLabel")

RegistClassMember(CLuaDouDiZhuRankWnd,"m_TitleLabel")

RegistClassMember(CLuaDouDiZhuRankWnd,"m_TableViewDataSource")
RegistClassMember(CLuaDouDiZhuRankWnd,"m_TableView")
RegistClassMember(CLuaDouDiZhuRankWnd,"m_RankList")


function CLuaDouDiZhuRankWnd:Init()
    local UILabelType=typeof(UILabel)

    self.m_MyRankLabel=FindChild(self.transform,"MyRankLabel"):GetComponent(UILabelType)
    self.m_MyNameLabel=FindChild(self.transform,"MyNameLabel"):GetComponent(UILabelType)
    self.m_MyTimeLabel=FindChild(self.transform,"MyTimeLabel"):GetComponent(UILabelType)
    self.m_MyScoreLabel=FindChild(self.transform,"MyScoreLabel"):GetComponent(UILabelType)

    self.m_TitleLabel=FindChild(self.transform, "TitleLabel"):GetComponent(UILabelType)
    local titleName = LocalString.GetString("积分排行")
    if CLuaDouDiZhuMgr.m_DouDiZhuStage == EnumGuildDouDiZhuFightStage.ePrePlay then
        titleName = SafeStringFormat3(LocalString.GetString("突围赛第%d场积分排行"), CLuaDouDiZhuMgr.m_DouDiZhuRound)
    elseif CLuaDouDiZhuMgr.m_DouDiZhuStage == EnumGuildDouDiZhuFightStage.eFinalPlay then
        titleName = SafeStringFormat3(LocalString.GetString("排位赛第%d场积分排行"), CLuaDouDiZhuMgr.m_DouDiZhuRound)
    end
    self.m_TitleLabel.text=titleName

    FindChild(self.transform, "HeadLabel3"):GetComponent(UILabelType).text=LocalString.GetString("帮会名称")
    FindChild(self.transform, "HeadLabel4"):GetComponent(UILabelType).text=LocalString.GetString("当前积分")

    if CClientMainPlayer.Inst then
        self.m_MyNameLabel.text= CClientMainPlayer.Inst.Name
    end
    self.m_MyRankLabel.text=LocalString.GetString("未上榜")
    self.m_MyTimeLabel.text=LocalString.GetString("—")
    if CClientMainPlayer.Inst:IsInGuild() then
        self.m_MyTimeLabel.text=CGuildMgr.Inst.m_GuildName
    end
    self.m_MyScoreLabel.text=LocalString.GetString("—")

    self.m_TableView=FindChild(self.transform,"TableView"):GetComponent(typeof(QnTableView))
    local function initItem(item,index)
        if index % 2 == 1 then
            item:SetBackgroundTexture("common_textbg_02_dark")
        else
            item:SetBackgroundTexture("common_textbg_02_light")
        end
        self:InitItem(item,index)
    end
    

    self.m_TableViewDataSource=DefaultTableViewDataSource.CreateByCount(0,initItem)
    self.m_TableView.m_DataSource=self.m_TableViewDataSource
    self.m_TableView.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow(row)
    end)
    self:OnRankDataReady()

end

function CLuaDouDiZhuRankWnd:OnSelectAtRow(row)
    local data=self.m_RankList[row+1]
    if data then
        CPlayerInfoMgr.ShowPlayerPopupMenu(data.Id, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,AlignType.Default)
    end
end
function CLuaDouDiZhuRankWnd:InitItem(item,index)
    local tf=item.transform
    local timeLabel=LuaGameObject.GetChildNoGC(tf,"TimeLabel").label
    timeLabel.text=""
    local nameLabel=LuaGameObject.GetChildNoGC(tf,"NameLabel").label
    nameLabel.text=" "
    local rankLabel=LuaGameObject.GetChildNoGC(tf,"RankLabel").label
    rankLabel.text=""
    local rankSprite=LuaGameObject.GetChildNoGC(tf,"RankImage").sprite
    rankSprite.spriteName=""
    local scoreLabel=LuaGameObject.GetChildNoGC(tf,"ScoreLabel").label
    scoreLabel.text=""
    
    local info=self.m_RankList[index+1]
    local rank=info.Rank
    if rank==1 then
        rankSprite.spriteName="Rank_No.1"
    elseif rank==2 then
        rankSprite.spriteName="Rank_No.2png"
    elseif rank==3 then
        rankSprite.spriteName="Rank_No.3png"
    else
        rankLabel.text=tostring(rank)
    end

    nameLabel.text=info.Name
    scoreLabel.text=tostring(info.Value)
    timeLabel.text=info.GuildName
end

function CLuaDouDiZhuRankWnd:OnRankDataReady()
    local myInfo=CLuaDouDiZhuMgr.m_MainPlayerRankData
    if myInfo.Rank>0 then
        self.m_MyRankLabel.text=myInfo.Rank
    else
        self.m_MyRankLabel.text=LocalString.GetString("未上榜")
    end
    self.m_MyNameLabel.text= myInfo.Name
    if CClientMainPlayer.Inst then
        self.m_MyNameLabel.text= CClientMainPlayer.Inst.Name
    end
    self.m_MyScoreLabel.text=tostring(myInfo.Value)
    self.m_MyTimeLabel.text=LocalString.GetString("—")
    if CClientMainPlayer.Inst:IsInGuild() then
        self.m_MyTimeLabel.text=CGuildMgr.Inst.m_GuildName
    end

    if CLuaDouDiZhuMgr.m_FinalBroadcast and myInfo.Rank > 0  and myInfo.Rank <=3 then
        LuaUtils.RankNumWndValue=myInfo.Rank
        CUIManager.ShowUI(CUIResources.RankNumWnd)
    end
    self.m_RankList=CLuaDouDiZhuMgr.m_RankDataList
    self.m_TableViewDataSource.count=#CLuaDouDiZhuMgr.m_RankDataList
    self.m_TableView:ReloadData(true,false)
end
return CLuaDouDiZhuRankWnd
