local NGUIMath = import "NGUIMath"

CLuaGuanNingGroupInfoTip = class()
RegistClassMember(CLuaGuanNingGroupInfoTip,"template")
RegistClassMember(CLuaGuanNingGroupInfoTip,"grid")
RegistClassMember(CLuaGuanNingGroupInfoTip,"nameLabel")
RegistClassMember(CLuaGuanNingGroupInfoTip,"node")
RegistClassMember(CLuaGuanNingGroupInfoTip,"bgSprite")

function CLuaGuanNingGroupInfoTip:Awake()
    self.template = self.transform:Find("Background/Template").gameObject
    self.template:SetActive(false)
    self.grid = self.transform:Find("Background/Node/Grid"):GetComponent(typeof(UIGrid))
    self.nameLabel = self.transform:Find("Background/Node/NameLabel"):GetComponent(typeof(UILabel))
    self.node = self.transform:Find("Background/Node")
    self.bgSprite = self.transform:Find("Background"):GetComponent(typeof(UISprite))

end

function CLuaGuanNingGroupInfoTip:Init( )
    self.nameLabel.text = SafeStringFormat3(LocalString.GetString("跨服关宁%d组"),CLuaGuanNingGroupWnd.m_ServerGroup)
    

    for i,v in ipairs(CLuaGuanNingGroupWnd.m_ServerNames) do
        local go = NGUITools.AddChild(self.grid.gameObject,self.template)
        go:SetActive(true)
        local nameLabel =go.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
        nameLabel.text = v
    end

    self.grid:Reposition()


    local b = NGUIMath.CalculateRelativeWidgetBounds(self.node)
    self.bgSprite.height = math.floor(b.size.y) + 45
    self.node.gameObject:SetActive(false)
    self.node.gameObject:SetActive(true)
end

