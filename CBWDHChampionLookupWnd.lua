-- Auto Generated!!
local CBWDHChampionLookupItem = import "L10.UI.CBWDHChampionLookupItem"
local CBWDHChampionLookupWnd = import "L10.UI.CBWDHChampionLookupWnd"
local CBWDHWatchGroupItem = import "L10.UI.CBWDHWatchGroupItem"
local Int32 = import "System.Int32"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CBWDHChampionLookupWnd.m_Awake_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeBtn).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)

    this.groupView.m_DataSource = this
    this.contentView.m_DataSource = this
    this.groupView.OnSelectAtRow = MakeDelegateFromCSFunction(this.OnSelectAtRow, MakeGenericClass(Action1, Int32), this)
    this.groupView:ReloadData(true, false)
    this.groupView:SetSelectRow(0, true)
end
CBWDHChampionLookupWnd.m_NumberOfRows_CS2LuaHook = function (this, view) 
    if view == this.groupView then
        return 5
        -- CBiWuDaHuiMgr.GroupArray.Length;
    elseif view == this.contentView then
        if this.contentList ~= nil then
            return this.contentList.Count
        end
    end
    return 0
end
CBWDHChampionLookupWnd.m_ItemAt_CS2LuaHook = function (this, view, row) 
    if view == this.groupView then
        local cmp = TypeAs(this.groupView:GetFromPool(0), typeof(CBWDHWatchGroupItem))
        cmp:Init(row + 1)
        return cmp
    elseif view == this.contentView then
        local cmp = TypeAs(this.contentView:GetFromPool(0), typeof(CBWDHChampionLookupItem))
        cmp:Init(this.contentList[row], row)
        return cmp
    end
    return nil
end
