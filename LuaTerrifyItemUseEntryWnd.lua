local QnButton = import "L10.UI.QnButton"

CLuaTerrifyItemUseEntryWnd = class()

RegistClassMember(CLuaTerrifyItemUseEntryWnd, "m_Button")

function CLuaTerrifyItemUseEntryWnd:Awake()
    self:InitComponents()
end

function CLuaTerrifyItemUseEntryWnd:InitComponents()
    self.m_Button = self.transform:Find("Anchor/Button"):GetComponent(typeof(QnButton))
end

function CLuaTerrifyItemUseEntryWnd:Init()
    self.m_Button.OnClick = DelegateFactory.Action_QnButton(function ()
        if CUIManager.IsLoaded(CLuaUIResources.TerrifyItemUseWnd) then
            CUIManager.CloseUI(CLuaUIResources.TerrifyItemUseWnd)
        else
            CUIManager.ShowUI(CLuaUIResources.TerrifyItemUseWnd)
        end
    end)
end