require("common/common_include")

local UIPanel = import "UIPanel"
local CUITexture = import "L10.UI.CUITexture"
local UIGrid = import "UIGrid"
local Baby_Setting = import "L10.Game.Baby_Setting"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local MessageMgr = import "L10.Game.MessageMgr"
local BabyMgr = import "L10.Game.BabyMgr"
local Baby_Character = import "L10.Game.Baby_Character"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Item_Item = import "L10.Game.Item_Item"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"
local CUIFx = import "L10.UI.CUIFx"
local CUIFxPaths = import "L10.UI.CUIFxPaths"


LuaProductionInspectionWnd = class()

RegistClassMember(LuaProductionInspectionWnd, "Page0")
RegistClassMember(LuaProductionInspectionWnd, "Page1")
RegistClassMember(LuaProductionInspectionWnd, "Page2")

RegistClassMember(LuaProductionInspectionWnd, "BabyTexture")
RegistClassMember(LuaProductionInspectionWnd, "InspectFX")
RegistClassMember(LuaProductionInspectionWnd, "InspectTimeLabel")
RegistClassMember(LuaProductionInspectionWnd, "InspectInfoLabel")

RegistClassMember(LuaProductionInspectionWnd, "InspectGrid")
RegistClassMember(LuaProductionInspectionWnd, "InspectTemplate")
RegistClassMember(LuaProductionInspectionWnd, "ShareButton")
RegistClassMember(LuaProductionInspectionWnd, "MedcineButton")
RegistClassMember(LuaProductionInspectionWnd, "InspectButton")
RegistClassMember(LuaProductionInspectionWnd, "BirthButton")
RegistClassMember(LuaProductionInspectionWnd, "InspectStatusLabel")

RegistClassMember(LuaProductionInspectionWnd, "InspectTime")
RegistClassMember(LuaProductionInspectionWnd, "InspectInfos")
RegistClassMember(LuaProductionInspectionWnd, "TotalInfoCount")
RegistClassMember(LuaProductionInspectionWnd, "InspectStatusTick")


function LuaProductionInspectionWnd:Init()
	self:InitClassMembers()
	self:InitValues()

end

function LuaProductionInspectionWnd:InitClassMembers()
	self.Page0 = self.transform:Find("Anchor/Page0"):GetComponent(typeof(UIPanel))
	self.Page1 = self.transform:Find("Anchor/Page1"):GetComponent(typeof(UIPanel))
	self.Page2 = self.transform:Find("Anchor/Page2"):GetComponent(typeof(UIPanel))

	self.Page0.gameObject:SetActive(false)
	self.Page1.gameObject:SetActive(true)
	self.Page2.gameObject:SetActive(true)

	self.BabyTexture = self.transform:Find("Anchor/Page1/BG/BabyTexture"):GetComponent(typeof(CUITexture))
	self.InspectFX = self.transform:Find("Anchor/Page1/BG/InspectFX"):GetComponent(typeof(CUIFx))
	self.InspectTimeLabel = self.transform:Find("Anchor/Page1/BG/InspectInfos/InspectTimeLabel"):GetComponent(typeof(UILabel))
	self.InspectTimeLabel.text = ""
	self.InspectInfoLabel = self.transform:Find("Anchor/Page1/BG/InspectInfos/InspectInfoLabel"):GetComponent(typeof(UILabel))
	self.InspectInfoLabel.text = ""

	self.InspectGrid = self.transform:Find("Anchor/Page2/BG/InspectGrid"):GetComponent(typeof(UIGrid))
	self.InspectTemplate = self.transform:Find("Anchor/Page2/BG/InspectTemplate").gameObject
	self.InspectTemplate:SetActive(false)

	self.ShareButton = self.transform:Find("Anchor/Page2/BG/Buttons/ShareButton").gameObject
	self.MedcineButton = self.transform:Find("Anchor/Page2/BG/Buttons/MedcineButton").gameObject
	self.InspectButton = self.transform:Find("Anchor/Page2/BG/Buttons/InspectButton").gameObject
	self.BirthButton = self.transform:Find("Anchor/Page2/BG/Buttons/BirthButton").gameObject
	self.InspectStatusLabel = self.transform:Find("Anchor/Page2/BG/Buttons/InspectStatusLabel"):GetComponent(typeof(UILabel))
	
end

function LuaProductionInspectionWnd:InitValues()

	self.InspectTime = nil
	self.InspectInfos = nil
	self.TotalInfoCount = 5
	self.InspectFX:DestroyFx()

	self:DestroyInspectStatusTick()
	if CommonDefs.IS_VN_CLIENT then
		self.ShareButton:SetActive(false)
		self.MedcineButton.transform.position = self.ShareButton.transform.position
	end
	local onShareButtonClicked = function (go)
		self:OnShareButtonClicked(go)
	end
	CommonDefs.AddOnClickListener(self.ShareButton, DelegateFactory.Action_GameObject(onShareButtonClicked), false)

	local onMedcineButtonClicked = function (go)
		self:OnMedcineButtonClicked(go)
	end
	CommonDefs.AddOnClickListener(self.MedcineButton, DelegateFactory.Action_GameObject(onMedcineButtonClicked), false)

	local onInspectButtonClicked = function (go)
		self:OnInspectButtonClicked(go)
	end
	CommonDefs.AddOnClickListener(self.InspectButton, DelegateFactory.Action_GameObject(onInspectButtonClicked), false)

	local onBitthButtonClicked = function (go)
		self:OnBirthButtonClicked(go)
	end
	CommonDefs.AddOnClickListener(self.BirthButton, DelegateFactory.Action_GameObject(onBitthButtonClicked), false)

	if LuaBabyMgr.m_InspectOpenType == 2 then
		self:UpdateButtons()
		self.InspectFX:LoadFx(CUIFxPaths.BabyInspectFx)
		
		self.InspectStatusTick = RegisterTickOnce(function ()
			self.InspectStatusLabel.text = LocalString.GetString("产检完成")
			self.InspectFX:DestroyFx()
			self:UpdateInspectInfos(LuaBabyMgr.m_InspectTime, LuaBabyMgr.m_InspectInfo)
			self:UpdateButtons()
		end, 4200)
	else
		self:UpdateInspectInfos(LuaBabyMgr.m_InspectTime, LuaBabyMgr.m_InspectInfo)
		self:UpdateButtons()
	end
end


function LuaProductionInspectionWnd:UpdateInspectInfos(inspectTime, inspectInfos)
	--[[if not inspectTime or not inspectInfos then
		CUIManager.CloseUI(CLuaUIResources.ProductionInspectionWnd)
		return 
	end--]]

	self.InspectTime = inspectTime
	self.InspectInfos = inspectInfos

	LuaBabyMgr.m_InspectTime = nil
	LuaBabyMgr.m_InspectInfo = nil

	if self.InspectTime ~= 0 then
		local inspectDate = CServerTimeMgr.ConvertTimeStampToZone8Time(inspectTime)
		self.InspectTimeLabel.text = inspectDate:ToString("yyyy-MM-dd")
	else
		self.InspectTimeLabel.text = LocalString.GetString("暂未有产检信息")
	end

	local infoCount = #self.InspectInfos

	-- 根据infoCount来确定加载的baby图片
	local texurePath = "UI/Texture/Transparent/Material/ProductionInspectionWnd_baby"
	if infoCount <= 4 then
		texurePath = SafeStringFormat3("%s_%s.mat", texurePath, tostring(infoCount))
	else
		texurePath = SafeStringFormat3("%s.mat", texurePath)
	end
	self.BabyTexture:LoadMaterial(texurePath)

	local setting = Baby_Setting.GetData()
	local yunJianTips = setting.YunJianTips
	if infoCount - 1 >= 0 and infoCount - 1 < yunJianTips.Length then
		self.InspectInfoLabel.text = yunJianTips[infoCount - 1]
	else
		self.InspectInfoLabel.text = ""
	end


	CUICommonDef.ClearTransform(self.InspectGrid.transform)
	for i = 1, self.TotalInfoCount, 1 do
		local go = NGUITools.AddChild(self.InspectGrid.gameObject, self.InspectTemplate)
		self:InitInspectInfoItem(go, i, self.InspectInfos[i])
		go:SetActive(true)
	end
	self.InspectGrid:Reposition()

end

function LuaProductionInspectionWnd:InitInspectInfoItem(go, index, info)
	local Value = go.transform:Find("Value"):GetComponent(typeof(UILabel))

	Value.text = self:GetValueContent(index, info)
end

function LuaProductionInspectionWnd:GetPropertyName(index)

	if index == EnumBabyFeature.HairColor then
		return LocalString.GetString("发色")
	elseif index == EnumBabyFeature.SkinColor then
		return LocalString.GetString("肤色")
	elseif index == EnumBabyFeature.Gender then
		return LocalString.GetString("性别")
	elseif index == EnumBabyFeature.BodyWeight then
		return LocalString.GetString("体重")
	elseif index == EnumBabyFeature.Nature then
		return LocalString.GetString("性格")
	else
		return ""
	end
end

-- info 都是从1开始的
function LuaProductionInspectionWnd:GetValueContent(index, info)
	if not info then return "" end
	local setting = Baby_Setting.GetData()

	if index == EnumBabyFeature.HairColor and info - 1 < setting.BabyHairColor.Length then
		return setting.BabyHairColor[info-1]
	elseif index == EnumBabyFeature.SkinColor and info - 1 < setting.BabySkinColor.Length then
		return setting.BabySkinColor[info-1]
	elseif index == EnumBabyFeature.Gender then
		if info == 0 then
			return LocalString.GetString("八成是个公子")
		elseif info == 1 then
			return LocalString.GetString("八成是个千金")
		end
	elseif index == EnumBabyFeature.BodyWeight then
		local bodyWeightName = setting.BodyWeightName
		if CommonDefs.DictContains_LuaCall(bodyWeightName, info) then
			return bodyWeightName[info]
		else
			return tostring(info)
		end
		
	elseif index == EnumBabyFeature.Nature then
		local character = Baby_Character.GetData(info)
		if character then
			return character.Character
		end
	else
		return ""
	end
end

function LuaProductionInspectionWnd:UpdateButtons()
	if LuaBabyMgr.m_InspectOpenType == 1 then
		-- 宝宝母亲打开
		self.MedcineButton:SetActive(true)
		if CommonDefs.IS_VN_CLIENT then
			self.ShareButton:SetActive(false)
		else
			self.ShareButton:SetActive(true)
		end

		if BabyMgr.Inst:IsReadyToGiveBirth(CClientMainPlayer.Inst.Id) then
			self.BirthButton:SetActive(true)
			self.InspectButton:SetActive(false)
		else
			self.BirthButton:SetActive(false)
			self.InspectButton:SetActive(true)
		end
	elseif LuaBabyMgr.m_InspectOpenType == 2 then
		-- 医生打开
		self.MedcineButton:SetActive(false)
		self.BirthButton:SetActive(false)
		self.InspectButton:SetActive(false)
		self.InspectStatusLabel.gameObject:SetActive(true)

	elseif LuaBabyMgr.m_InspectOpenType == 3 then
		-- 其他玩家链接打开
		self.MedcineButton:SetActive(false)
		self.BirthButton:SetActive(false)
		self.InspectButton:SetActive(false)
		self.InspectStatusLabel.gameObject:SetActive(false)
	end
end


function LuaProductionInspectionWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateProductionInspection", self, "UpdateInspectInfos")
end

function LuaProductionInspectionWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateProductionInspection", self, "UpdateInspectInfos")
	self:DestroyInspectStatusTick()
end


function LuaProductionInspectionWnd:OnShareButtonClicked(go)
	CUICommonDef.CaptureScreenAndShare()
end

function LuaProductionInspectionWnd:OnMedcineButtonClicked(go)
	-- 保胎丸ID待修改
	local msg = g_MessageMgr:FormatMessage("USE_BAO_TAI_WAN")
	local setting = Baby_Setting.GetData()

	LuaItemInfoMgr:ShowItemConsumeWndWithQuickAccess(setting.PregnantHelpId, 1, msg, true, false, LocalString.GetString("服用"), function (enough)
		if enough then
			local bagPos = 0
			local itemid = nil
			local default
				default, bagPos, itemid = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, setting.PregnantHelpId)
			if default then
				Gac2Gas.RequestUseItem(EnumToInt(EnumItemPlace.Bag), bagPos, itemid, "")
				LuaItemInfoMgr:CloseItemConsumeWnd()
			end
		else
			MessageMgr.Inst:ShowMessage("BAO_TAI_WAN_NOT_ENOUGH", {})
		end
	end, nil, false)
end

-- 孕检按钮，显示孕检方式
function LuaProductionInspectionWnd:OnInspectButtonClicked(go)
	-- 21040300
	local templateId = 21040300
	local itemCount = CItemMgr.Inst:GetItemCount(templateId)
	if itemCount > 0 or #self.InspectInfos >= 5 then
		MessageMgr.Inst:ShowMessage("YANGYU_INSPECT_TIPS", {})
	else
		local item = Item_Item.GetData(templateId)
		if not item then return end
		local hint = SafeStringFormat3("[00FF00]%s([-][FF0000]%s[-][00FF00]/1)[-]", item.Name, tostring(itemCount))
		local msg = g_MessageMgr:FormatMessage("BABY_BAOTAIWAN_NOT_ENOUGH", hint)
		MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function()
			CItemAccessListMgr.Inst:ShowItemAccessInfo(templateId, false, self.InspectButton.transform, AlignType.Right)
		end), nil ,LocalString.GetString("购买"), LocalString.GetString("取消"), false)
	end
end

-- 生产按钮，显示生产说明
function LuaProductionInspectionWnd:OnBirthButtonClicked(go)
	MessageMgr.Inst:ShowMessage("YANGYU_GIVE_BIRTH_TIPS", {})
end

function LuaProductionInspectionWnd:DestroyInspectStatusTick()
	if self.InspectStatusTick ~= nil then
      	UnRegisterTick(self.InspectStatusTick)
      	self.InspectStatusTick = nil
    end
end

return LuaProductionInspectionWnd
