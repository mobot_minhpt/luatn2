-- Auto Generated!!
local CButton = import "L10.UI.CButton"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CClientOtherPlayer = import "L10.Game.CClientOtherPlayer"
local CIMMgr = import "L10.Game.CIMMgr"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CTradeMgr = import "L10.Game.CTradeMgr"
local CTradeWnd = import "L10.UI.CTradeWnd"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Gac2Gas = import "L10.Game.Gac2Gas"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local NGUIText = import "NGUIText"
local Profession = import "L10.Game.Profession"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

CTradeWnd.m_Init_CS2LuaHook = function (this) 
    local obj = TypeAs(CClientObjectMgr.Inst:GetObject(CTradeMgr.Inst.targetEngineId), typeof(CClientOtherPlayer))
    if CTradeMgr.Inst.targetEngineId == 0 then
        g_MessageMgr:ShowMessage("OTHER_PLAYER_LEAVE")
        Gac2Gas.CancelTrade()
        return
    end
    local isFriend
    if obj ~= nil then
        local playerId = obj.PlayerId
        isFriend = CIMMgr.Inst:IsMyFriend(playerId)

        if LuaCreditScoreMgr:OpenFace2FaceTradeReminder() then
            LuaCreditScoreMgr:RequestPlayerCreditScoreData(playerId)
        end
    else
        g_MessageMgr:ShowMessage("OTHER_PLAYER_LEAVE")
        Gac2Gas.CancelTrade()
        return
    end
    if CTradeMgr.Inst.IsPlayerSeller then
        this.sellPlayerInfo:Init(LocalString.GetString("我"), "", "", "", Color.white)
        local default
        if isFriend then
            default = LocalString.GetString("好友")
        else
            default = LocalString.GetString("陌生人")
        end
        this.buyPlayerInfo:Init(obj.Name, tostring(obj.Level), Profession.GetFullName(obj.Class), default, isFriend and NGUIText.ParseColor24("73ff2e", 0) or NGUIText.ParseColor24("f35151", 0))
    else
        this.buyPlayerInfo:Init(LocalString.GetString("我"), "", "", "", Color.white)
        local extern
        if isFriend then
            extern = LocalString.GetString("好友")
        else
            extern = LocalString.GetString("陌生人")
        end
        this.sellPlayerInfo:Init(obj.Name, tostring(obj.Level), Profession.GetFullName(obj.Class), extern, isFriend and NGUIText.ParseColor24("73ff2e", 0) or NGUIText.ParseColor24("f35151", 0))
    end
    this.detailView:Init()
end
CTradeWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.tradeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.tradeButton).onClick, MakeDelegateFromCSFunction(this.OnLockBtnClick, VoidDelegate, this), true)
    UIEventListener.Get(this.cancelButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.cancelButton).onClick, MakeDelegateFromCSFunction(this.OnCancelBtnClick, VoidDelegate, this), true)
    EventManager.AddListener(EnumEventType.TargetLockTrade, MakeDelegateFromCSFunction(this.OnTargetLockTrade, Action0, this))
    EventManager.AddListener(EnumEventType.PlayerLockTrade, MakeDelegateFromCSFunction(this.OnPlayerLockTrade, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.GetPlayerCreditScore, MakeDelegateFromCSFunction(this.OnGetPlayerCreditScore, MakeGenericClass(Action2, UInt64, UInt64), this))

end
CTradeWnd.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.tradeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.tradeButton).onClick, MakeDelegateFromCSFunction(this.OnLockBtnClick, VoidDelegate, this), false)
    UIEventListener.Get(this.cancelButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.cancelButton).onClick, MakeDelegateFromCSFunction(this.OnCancelBtnClick, VoidDelegate, this), false)
    EventManager.RemoveListener(EnumEventType.TargetLockTrade, MakeDelegateFromCSFunction(this.OnTargetLockTrade, Action0, this))
    EventManager.RemoveListener(EnumEventType.PlayerLockTrade, MakeDelegateFromCSFunction(this.OnPlayerLockTrade, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.GetPlayerCreditScore, MakeDelegateFromCSFunction(this.OnGetPlayerCreditScore, MakeGenericClass(Action2, UInt64, UInt64), this))

end

CTradeWnd.m_hookOnGetPlayerCreditScore = function(this, pid, creditScore)
    local obj = TypeAs(CClientObjectMgr.Inst:GetObject(CTradeMgr.Inst.targetEngineId), typeof(CClientOtherPlayer))
    if obj ~= nil then
        local playerId = obj.PlayerId
        local name = obj.Name
        if playerId == pid then
            if creditScore < CreditScore_Setting.GetData().TradeNoticeScore then
                g_MessageMgr:ShowMessage("CreditScore_LowNotice_Trade", name)
            end
        end
    end
end

CTradeWnd.m_OnLockBtnClick_CS2LuaHook = function (this, go) 

    if this.lockLabel.text == LocalString.GetString("锁定") then
        if not CTradeMgr.Inst.IsPlayerSeller and CTradeMgr.Inst.playerMoney == 0 then
            g_MessageMgr:ShowMessage("Face_Trade_Require_Silver")
        elseif CTradeMgr.Inst.IsPlayerSeller and CTradeMgr.Inst.playerTradeItem == nil and CTradeMgr.Inst.playerTradeEquip == nil then
            g_MessageMgr:ShowMessage("Face_Trade_Require_Zhenpin")
        else
            if not CTradeMgr.Inst.IsPlayerSeller then
                Gac2Gas.MoveSilverToTradeShelf(CTradeMgr.Inst.playerMoney)
            end
            Gac2Gas.LockTradeShelf()
        end
    elseif CommonDefs.GetComponent_GameObject_Type(this.tradeButton, typeof(CButton)).Enabled then
        if CTradeMgr.Inst.IsPlayerSeller and CTradeMgr.Inst:TradeItemNeedConfirm() then
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Sell_Big_Reconfirm"), MakeDelegateFromCSFunction(this.DoTrade, Action0, this), nil, nil, nil, false)
        elseif not CTradeMgr.Inst.IsPlayerSeller and CTradeMgr.Inst:TradeItemNeedConfirm() then
            MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Buy_Big_Reconfirm"), MakeDelegateFromCSFunction(this.DoTrade, Action0, this), nil, nil, nil, false)
        else
            this:DoTrade()
        end
    end
end
CTradeWnd.m_OnPlayerLockTrade_CS2LuaHook = function (this) 

    this.lockLabel.text = LocalString.GetString("交易")
    CommonDefs.GetComponent_GameObject_Type(this.tradeButton, typeof(CButton)).Enabled = CTradeMgr.Inst.targetLock
    if CTradeMgr.Inst.IsPlayerSeller then
        this.detailView:OnLockItem()
    else
        this.detailView:OnLockMoney()
    end
end
CTradeWnd.m_OnTargetLockTrade_CS2LuaHook = function (this) 

    if CTradeMgr.Inst.playerLock then
        CommonDefs.GetComponent_GameObject_Type(this.tradeButton, typeof(CButton)).Enabled = true
    end
    if CTradeMgr.Inst.IsPlayerSeller then
        this.detailView:ShowMoney()
    else
        this.detailView:OnLockItem()
    end
end
