-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CommonDefs = import "L10.Game.CommonDefs"
local CTeamDamageStatItem = import "L10.UI.CTeamDamageStatItem"
local CTeamDamageStatWnd = import "L10.UI.CTeamDamageStatWnd"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local ETickType = import "L10.Engine.ETickType"
local EventManager = import "EventManager"
local Gac2Gas = import "L10.Game.Gac2Gas"
local L10 = import "L10"
local TeamMemberDamageData = import "L10.UI.TeamMemberDamageData"

CTeamDamageStatWnd.m_OnEnable_CS2LuaHook = function (this) 
    EventManager.AddListenerInternal(EnumEventType.OnTeamDamageStatInfoUpdate, MakeDelegateFromCSFunction(this.OnTeamDamageStatInfoUpdate, MakeGenericClass(Action1, MakeGenericClass(List, TeamMemberDamageData)), this))
    CommonDefs.ListClear(this.dataList)
    this.tableView:Clear()
    this.tableView.dataSource = this
    this:StartTick()
end
CTeamDamageStatWnd.m_StartTick_CS2LuaHook = function (this) 
    this:CancelTick()
    Gac2Gas.RequestTeamDamageInfo()
    this.cancelTick = L10.Engine.CTickMgr.Register(DelegateFactory.Action(function () 
        Gac2Gas.RequestTeamDamageInfo()
    end), 1000, ETickType.Loop)
end
CTeamDamageStatWnd.m_CancelTick_CS2LuaHook = function (this) 
    if this.cancelTick ~= nil then
        invoke(this.cancelTick)
        this.cancelTick = nil
    end
end
CTeamDamageStatWnd.m_OnTeamDamageStatInfoUpdate_CS2LuaHook = function (this, dataList) 
    CommonDefs.ListClear(this.dataList)
    this.tableView:Clear()
    CommonDefs.ListAddRange(this.dataList, dataList)
    this.tableView:LoadData(0, true)
end
CTeamDamageStatWnd.m_CellForRowAtIndex_CS2LuaHook = function (this, index) 
    local cellIdentifier = "RowCell"

    local cell = this.tableView:DequeueReusableCellWithIdentifier(cellIdentifier)
    if cell == nil then
        cell = this.tableView:AllocNewCellWithIdentifier(this.template, cellIdentifier)
    end
    if index >= 0 and index < this.dataList.Count then
        local data = this.dataList[index]
        CommonDefs.GetComponentInChildren_GameObject_Type(cell, typeof(CTeamDamageStatItem)):Init(data, CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Id == data.playerId)
    end

    return cell
end
