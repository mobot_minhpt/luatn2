require("common/common_include")

local CUIFx = import "L10.UI.CUIFx"
local UIGrid = import "UIGrid"

LuaMengHuaLuResultWnd = class()

RegistChildComponent(LuaMengHuaLuResultWnd, "PassLabel", UILabel)
RegistChildComponent(LuaMengHuaLuResultWnd, "ExceedLabel", UILabel)
RegistChildComponent(LuaMengHuaLuResultWnd, "RankBtn", GameObject)
RegistChildComponent(LuaMengHuaLuResultWnd, "ResultFX", CUIFx)
-- 详细信息
RegistChildComponent(LuaMengHuaLuResultWnd, "InfoTemplate", GameObject)
RegistChildComponent(LuaMengHuaLuResultWnd, "InfoGrid", UIGrid)
RegistChildComponent(LuaMengHuaLuResultWnd, "OpenGridLabel", UILabel)
RegistChildComponent(LuaMengHuaLuResultWnd, "KillMonsterLabel", UILabel)
RegistChildComponent(LuaMengHuaLuResultWnd, "KillBossLabel", UILabel)
RegistChildComponent(LuaMengHuaLuResultWnd, "TotalGoldLabel", UILabel)
RegistChildComponent(LuaMengHuaLuResultWnd, "TotalSkillLabel", UILabel)
RegistChildComponent(LuaMengHuaLuResultWnd, "MaxLayerLabel", UILabel)

-- 奖励信息(目前没用到)
RegistChildComponent(LuaMengHuaLuResultWnd, "RewardItem", GameObject)
RegistChildComponent(LuaMengHuaLuResultWnd, "RewardGrid", UIGrid)

function LuaMengHuaLuResultWnd:Init()
	self.InfoTemplate:SetActive(false)
	self.RewardItem:SetActive(false)

	self:UpdateInfos()

	local onRankBtnClicked = function (go)
		self:OnRankBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.RankBtn, DelegateFactory.Action_GameObject(onRankBtnClicked), false)

	self.ResultFX:DestroyFx()

	local result = LuaMengHuaLuMgr.m_ResultLayer-1
	self.PassLabel.text = tostring(result >= 0 and result or 0)
	if LuaMengHuaLuMgr.m_ResultTotalRank > 1 then
		local percent = ((LuaMengHuaLuMgr.m_ResultRank-1) / (LuaMengHuaLuMgr.m_ResultTotalRank-1)) * 100

		if percent <= 10 then
			-- 大烟花
			self.ResultFX:LoadFx("fx/ui/prefab/UI_yangyudilaojiesuan_02.prefab")
		elseif percent <= 50 then
			-- 小烟花
			self.ResultFX:LoadFx("fx/ui/prefab/UI_yangyudilaojiesuan_01.prefab")
		end
		if percent > 50 then
			self.ExceedLabel.text = nil
		else
			local result = 100-percent
			if result == 0 then
				result = 1
			end
			self.ExceedLabel.text = SafeStringFormat3(LocalString.GetString("超过了%d%%的玩家"), math.floor(result))
		end
	else
		-- 只有1个或0个玩家
		self.ExceedLabel.text = LocalString.GetString("超过了99%的玩家")
		self.ResultFX:LoadFx("fx/ui/prefab/UI_yangyudilaojiesuan_02.prefab")
	end
end

function LuaMengHuaLuResultWnd:OnRankBtnClicked()
	CUIManager.ShowUI(CLuaUIResources.MengHualuRankWnd)
end

function LuaMengHuaLuResultWnd:UpdateInfos()
	self.OpenGridLabel.text = tostring(LuaMengHuaLuMgr.m_ResultOpenGridNum)
	self.KillMonsterLabel.text = tostring(LuaMengHuaLuMgr.m_ResultKillMonsterNum)
	self.KillBossLabel.text = tostring(LuaMengHuaLuMgr.m_ResultBossNum)
	self.TotalGoldLabel.text = tostring(LuaMengHuaLuMgr.m_ResultTotalGoldNum)
	self.TotalSkillLabel.text = tostring(LuaMengHuaLuMgr.m_ResultTotalSkillNum)
	self.MaxLayerLabel.text = tostring(LuaMengHuaLuMgr.m_ResultMaxLayer)
end

function LuaMengHuaLuResultWnd:OnEnable()
	
end

function LuaMengHuaLuResultWnd:OnDisable()
	CUIManager.CloseUI(CLuaUIResources.MengHuaLuWnd)
end

function LuaMengHuaLuResultWnd:ClickThroughToClose()
    if Input.GetMouseButtonDown(0) then       
        if (not CUICommonDef.IsOverUI or (UICamera.lastHit.collider and
           not CUICommonDef.IsChild(self.transform, UICamera.lastHit.collider.transform) and
            CUICommonDef.GetNearestPanelDepth(self.transform) > CUICommonDef.GetNearestPanelDepth(UICamera.lastHit.collider.transform))) then
            CUIManager.CloseUI(CLuaUIResources.MengHuaLuResultWnd)
        end
    end
end

return LuaMengHuaLuResultWnd
