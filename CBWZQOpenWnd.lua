-- Auto Generated!!
local CBWZQOpenWnd = import "L10.UI.CBWZQOpenWnd"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local UIEventListener = import "UIEventListener"
CBWZQOpenWnd.m_Init_CS2LuaHook = function (this) 
    UIEventListener.Get(this.watchBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        Gac2Gas.GetZhaoQinGuanZhanInfo()
        this:Close()
    end)

    UIEventListener.Get(this.attendBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        Gac2Gas.GetZhaoQinCallerInfo()
        this:Close()
    end)

    UIEventListener.Get(this.riseBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        CUIManager.ShowUI(CUIResources.BWZQRiseWnd)
        this:Close()
    end)

    UIEventListener.Get(this.closeBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        this:Close()
    end)
end
