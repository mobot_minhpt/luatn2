local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local UILabel = import "UILabel"
local UIPanel = import "UIPanel"
local GameObject = import "UnityEngine.GameObject"
local NGUIText = import "NGUIText"
local LuaTweenUtils = import "LuaTweenUtils"
local Mathf = import "UnityEngine.Mathf"
local Ease = import "DG.Tweening.Ease"
local Extensions = import "Extensions"

LuaLuckyPlayer = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaLuckyPlayer, "LuckyPlayerLabel", "LuckyPlayerLabel", UILabel)
RegistChildComponent(LuaLuckyPlayer, "LuckyPlayerPanel", "LuckyPlayerPanel", UIPanel)
RegistChildComponent(LuaLuckyPlayer, "BgSprite", "BgSprite", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaLuckyPlayer, "m_inited")
RegistClassMember(LuaLuckyPlayer, "m_message")

function LuaLuckyPlayer:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end


function LuaLuckyPlayer:Init()
end

--@region UIEvent

--@endregion UIEvent

function LuaLuckyPlayer:InitWithMsg(message)
    if self.m_inited == false then
        self.m_inited = true
        LuaTweenUtils.DOKill(self.LuckyPlayerLabel.transform, false)
        self.LuckyPlayerPanel.gameObject:SetActive(false)
        self.BgSprite:SetActive(false)
    end
    self.m_message = message
    self:ReceiveMessage()
end

function LuaLuckyPlayer:ReceiveMessage()
    self:InitPlayerMessage()
end

function LuaLuckyPlayer:InitPlayerMessage( )
    self.LuckyPlayerLabel.text = System.String.Format("[c][{0}]{1}[-][/c]", NGUIText.EncodeColor24(self.LuckyPlayerLabel.color), self.m_message)
	self:Play()
end

function LuaLuckyPlayer:Play()
    local speed = 70
    local textLabel = self.LuckyPlayerLabel
    local clip = self.LuckyPlayerPanel
    local bg = self.BgSprite
    if not clip.gameObject.activeSelf then
        clip.gameObject:SetActive(true)
        bg:SetActive(true)
    end
    LuaTweenUtils.DOKill(textLabel.transform,false)
    textLabel:UpdateNGUIText();
    NGUIText.regionWidth = 1000000;

    
    local size = NGUIText.CalculatePrintedSize(textLabel.text);
	textLabel.width = Mathf.CeilToInt(size.x);
			local duration = (textLabel.localSize.x + clip.baseClipRegion.z) / speed
			local startPos = clip.baseClipRegion.x + clip.baseClipRegion.z * 0.5
			local endPos = clip.baseClipRegion.x - clip.baseClipRegion.z * 0.5 - textLabel.localSize.x

            Extensions.SetLocalPositionX(textLabel.transform, startPos)

            local tween = LuaTweenUtils.DOLocalMoveX(textLabel.transform, endPos, duration, true)

            local func = DelegateFactory.TweenCallback(function () 
                clip.gameObject:SetActive(false)
				bg:SetActive(false)
            end)

            CommonDefs.OnComplete_Tweener(CommonDefs.SetEase_Tweener(tween,Ease.Linear), func)
end