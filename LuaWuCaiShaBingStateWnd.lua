require("common/common_include")

local CTopAndRightTipWnd = import "L10.UI.CTopAndRightTipWnd"
local CUITexture = import "L10.UI.CUITexture"

CLuaWuCaiShaBingStateWnd = class()
RegistClassMember(CLuaWuCaiShaBingStateWnd, "m_ExpandButton")
RegistClassMember(CLuaWuCaiShaBingStateWnd, "m_ShowState")

function CLuaWuCaiShaBingStateWnd:Init()
	self.m_ExpandButton = self.transform:Find("Anchor/Tip/ExpandButton").gameObject
	self.m_ShowState = self.transform:Find("State").gameObject

	CommonDefs.AddOnClickListener(self.m_ExpandButton, DelegateFactory.Action_GameObject(function()
		if CUIManager.IsLoaded(CUIResources.TopAndRightTipWnd) then
			self.m_ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)

			CUIManager.CloseUI(CUIResources.TopAndRightTipWnd)

			self.m_ShowState:SetActive(true)
		else
			self.m_ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)

			CTopAndRightTipWnd.showPackage = false
			CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)

			self.m_ShowState:SetActive(false)
		end
	end), false)

	if LuaWuCaiShaBingMgr.m_StateData.playerGroupId then
		local playerGroupId = LuaWuCaiShaBingMgr.m_StateData.playerGroupId
		local submitData = LuaWuCaiShaBingMgr.m_StateData.submitData

		local textureTbl = {"shabing_tao.mat", "shabing_mangguo.mat", "shabing_xigua.mat"}
		local texture = self.transform:Find("State/Progress1/Texture"):GetComponent(typeof(CUITexture))
		texture:LoadMaterial("UI/Texture/Transparent/Material/" .. textureTbl[playerGroupId], false, nil)

		local nameTbl = {LocalString.GetString("桃子"), LocalString.GetString("芒果"), LocalString.GetString("西瓜")}
		local nameLabel = self.transform:Find("State/Progress1/Name"):GetComponent(typeof(UILabel))
		nameLabel.text = nameTbl[playerGroupId]

		self:OnWuCaiShaBingShowState(playerGroupId, submitData)
	end
end

function CLuaWuCaiShaBingStateWnd:OnEnable()
	g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
	g_ScriptEvent:AddListener("WuCaiShaBingShowState", self, "OnWuCaiShaBingShowState")
end

function CLuaWuCaiShaBingStateWnd:OnDisable()
	g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
	g_ScriptEvent:RemoveListener("WuCaiShaBingShowState", self, "OnWuCaiShaBingShowState")

	if CUIManager.IsLoaded(CUIResources.TopAndRightTipWnd) then
		CUIManager.CloseUI(CUIResources.TopAndRightTipWnd)
	end
end

function CLuaWuCaiShaBingStateWnd:OnWuCaiShaBingShowState(playerGroupId, progressData)
	local colorTbl = {"FF4343", "F3D24F", "46EC78"}
	for i = 1, #progressData do
		if i % 2 == 0 then
			local idx = math.floor(i / 2)
			local progressGo = self.transform:Find("State/Progress" .. tostring(idx)).gameObject
				
			local label = progressGo.transform:Find("Label"):GetComponent(typeof(UILabel))
			label.text = SafeStringFormat3("%d/%d", progressData[i - 1], progressData[i])
			
			local progressBar = progressGo.transform:Find("Progress Bar"):GetComponent(typeof(UISlider))
			progressBar.sliderValue = progressData[i] == 0 and 0 or progressData[i - 1] / progressData[i]

			local progressLabel = progressGo.transform:Find("Progress Bar/Foreground"):GetComponent(typeof(UISprite))
			progressLabel.color = NGUIText.ParseColor24(colorTbl[playerGroupId], 0)
		end
	end
end

function CLuaWuCaiShaBingStateWnd:OnHideTopAndRightTipWnd()
	self.m_ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
	self.m_ShowState:SetActive(true)
end

return CLuaWuCaiShaBingStateWnd