local UILabel = import "UILabel"
local UIEventListener = import "UIEventListener"
local MsgPackImpl = import "MsgPackImpl"
local UITable = import "UITable"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local MessageWndManager = import "L10.UI.MessageWndManager"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local QnTabView=import "L10.UI.QnTabView"
local Extensions = import "Extensions"
local QnTableView = import "L10.UI.QnTableView"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local Object=import "System.Object"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CPopupMenuInfoMgrAlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local PlayerSettings = import "L10.Game.PlayerSettings"
local CUIManager = import "L10.UI.CUIManager"
local CUIFx = import "L10.UI.CUIFx"
local QnButton = import "L10.UI.QnButton"
local QnTipButton = import "L10.UI.QnTipButton"
local CPlayerShopPopupMenuInfoMgr = import "L10.UI.CPlayerShopPopupMenuInfoMgr"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgrAlignType = import  "L10.UI.CPopupMenuInfoMgr+AlignType"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CRankData = import "L10.UI.CRankData"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local CPlayerInfoMgrAlignType = import "CPlayerInfoMgr+AlignType"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUITexture = import "L10.UI.CUITexture"
local EnumGender = import "L10.Game.EnumGender"
local CItemMgr = import "L10.Game.CItemMgr"
local CButton = import "L10.UI.CButton"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CChatHelper = import "L10.UI.CChatHelper"
local CSocialWndMgr = import "L10.UI.CSocialWndMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local QnTableItem = import "L10.UI.QnTableItem"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
LuaStarBiwuJingCaiWnd=class()
LuaStarBiwuJingCaiWnd.Path = "ui/starbiwu/LuaStarBiwuJingCaiWnd"

RegistClassMember(LuaStarBiwuJingCaiWnd,"m_currentBetPoolLabel")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_targetDateLabel")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_targetStageNameLabel")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_maxBetValueLabel")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_HuiBaoBeiShuLabel")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_OperateBtn")                     --投注按钮
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_BottomRoot")                     --底部节点
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_FinishLabel")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_ContentRoot")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_ItemTemplate")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_CostAndOwnMoney")               --价格
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_TipButton")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_TabView")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_AdvView")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_RecordTipLabel")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_AwardtButton")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_AwardtLabel")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_tabAlert")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_btnAlert")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_AddSubAndInputButton")
RegistClassMember(LuaStarBiwuJingCaiWnd,"NormalRoot")
RegistClassMember(LuaStarBiwuJingCaiWnd,"NoRpcRoot")
RegistClassMember(LuaStarBiwuJingCaiWnd, "m_FinalRoot")
RegistClassMember(LuaStarBiwuJingCaiWnd, "m_WorldFilterButton")

RegistClassMember(LuaStarBiwuJingCaiWnd,"m_currentBetPool")         --奖池
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_targetDate")             --比赛日
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_targetStageName")        --赛程
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_maxBetValue")            --投注上限
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_canBet")                 --能否参与竞猜
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_phaseBet")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_jingcaiPhaseNum")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_jingcaiHistoryPhaseNum")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_jingcaiItemTblUd")       --竞猜项
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_resultTable")            --竞猜结果
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_canBetValue")              --份数上限（由投注上限和已投注数量决定）
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_XiaZhuUnit")               --每份数量（读表）
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_Cost")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_minXiazhu")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_RecordTable")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_canGetReward")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_canGetFaildReward")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_EffectTick")                 --特效计时器
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_Choice2BetList")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_SingleChoiceList")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_JingCaiResultList")

RegistClassMember(LuaStarBiwuJingCaiWnd,"m_JingCaiTeamName1")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_JingCaiTeamName2")

RegistClassMember(LuaStarBiwuJingCaiWnd,"m_RebackFinalButtonStatus")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_FilterActions")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_RankTemplate")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_RankContentRoot")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_HistoryJingCaiTeamName")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_JueSaiJingCaiPhaseNumBegin")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_JueSaiJingCaiPhaseNumTable")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_JueSaiJingCaiTotalList")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_JingCaiQuanMoney")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_JingCaiQuanLabel")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_ChooseTableView")

RegistClassMember(LuaStarBiwuJingCaiWnd,"m_QuestionTabList")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_CurQuestionInfo")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_CurQuestionIndex")

RegistClassMember(LuaStarBiwuJingCaiWnd,"m_ShowBetPoolTick")
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_JingCaiTeamNameList")

RegistChildComponent(LuaStarBiwuJingCaiWnd,"m_RebackFinalButton","RebackFinalButton", GameObject)
RegistChildComponent(LuaStarBiwuJingCaiWnd,"m_WatchButton","WatchButton", GameObject)
RegistClassMember(LuaStarBiwuJingCaiWnd,"m_QuestionViewAlert")

function LuaStarBiwuJingCaiWnd:Awake()
    self.m_ShowBetPoolTick = nil
    self.m_minXiazhu = 1
    self.m_jingcaiHistoryPhaseNum = 0
    self.NormalRoot = self.transform:Find("QnTabView/JingCai/Normal").gameObject
    self.NormalRoot:SetActive(false)
    self.m_WorldFilterButton = self.NormalRoot.transform:Find("WordFilterButton"):GetComponent(typeof(QnTipButton))
    self.m_WorldFilterButton.gameObject:SetActive(false)
    self.m_RebackFinalButtonStatus = false
    self.m_RebackFinalButton:SetActive(false)
    self.NoRpcRoot = self.transform:Find("QnTabView/JingCai/NoRpc").gameObject
    self.NoRpcRoot:SetActive(true)
    local NoRPCLabel = self.NoRpcRoot.transform:Find("FinishLabel"):GetComponent(typeof(UILabel))
    NoRPCLabel.text = g_MessageMgr:FormatMessage("StarBiWu_JingCaiEmpty")
    self.m_FinalRoot = self.transform:Find("QnTabView/JingCai/Final").gameObject
    self.m_FinalRoot:SetActive(false)
    self.m_currentBetPoolLabel = self.transform:Find("QnTabView/JingCai/Normal/Top/Label/Label"):GetComponent(typeof(UILabel))
    self.m_targetDateLabel = self.transform:Find("QnTabView/JingCai/Normal/Top/Time/Label"):GetComponent(typeof(UILabel))
    self.m_targetStageNameLabel = self.transform:Find("QnTabView/JingCai/Normal/Top/Progress/Label"):GetComponent(typeof(UILabel))
    self.m_maxBetValueLabel = self.transform:Find("QnTabView/JingCai/Normal/Top/Limit/Label"):GetComponent(typeof(UILabel))
    self.m_HuiBaoBeiShuLabel = self.transform:Find("QnTabView/JingCai/Normal/Top/HuiBaoBeiShu/Label"):GetComponent(typeof(UILabel))
    self.m_ContentRoot = self.transform:Find("QnTabView/JingCai/Normal/ContentRoot"):GetComponent(typeof(UITable))
    self.m_BottomRoot = self.transform:Find("QnTabView/JingCai/Normal/Bottom").gameObject
    self.m_CostAndOwnMoney = self.transform:Find("QnTabView/JingCai/Normal/Bottom/QnCostAndOwnMoney"):GetComponent(typeof(CCurentMoneyCtrl))

    self.m_AddSubAndInputButton = self.transform:Find("QnTabView/JingCai/Normal/Bottom/QnIncreaseAndDecreaseButton_3"):GetComponent(typeof(QnAddSubAndInputButton))


    self.m_RankTemplate = self.transform:Find("QnTabView/Rank/Normal/ItemTemplate")
    self.m_RankContentRoot = self.transform:Find("QnTabView/Rank/Normal/ContentRoot")
    self.m_RankTemplate.gameObject:SetActive(false)
    self.m_TabView = self.transform:Find("QnTabView"):GetComponent(typeof(QnTabView))
    self.m_TabView.OnSelect = DelegateFactory.Action_QnTabButton_int(function (go, index)
		self:OnTabChange(go, index)
    end)
    self.m_AdvView = self.transform:Find("QnTabView/JiLu/AdvView"):GetComponent(typeof(QnTableView))
    self.m_RecordTipLabel = self.transform:Find("QnTabView/JiLu/TipLabel").gameObject
    self.m_AwardtButton = self.transform:Find("QnTabView/JiLu/AwardtButton").gameObject
    self.m_AwardtLabel = self.transform:Find("QnTabView/JiLu/AwardtLabel"):GetComponent(typeof(UILabel))
    self.m_AwardtButton:GetComponent(typeof(QnButton)).Enabled = false

    self.m_QuestionViewAlert = self.transform:Find("Wnd_Bg_Primary_Tab/Tabs/Tab2/Alert").gameObject

    self.m_tabAlert = self.transform:Find("Wnd_Bg_Primary_Tab/Tabs/Tab1/Alert").gameObject
    self.m_tabAlert:SetActive(false)
    self.m_btnAlert = self.transform:Find("QnTabView/JiLu/AwardtButton/Alert").gameObject
    self.m_JingCaiQuanLabel = self.transform:Find("QnTabView/JingCai/Normal/JingcaiItemLabel"):GetComponent(typeof(UILabel))
    self.m_JingCaiQuanMoney = 0
    self.m_AddSubAndInputButton.onValueChanged = DelegateFactory.Action_uint(function (value)
        self.m_Cost = self.m_XiaZhuUnit*value
        self.m_CostAndOwnMoney:SetCost(math.max(self.m_XiaZhuUnit*value - self.m_JingCaiQuanMoney,0))
	end)
    
    self.m_FinishLabel = self.transform:Find("QnTabView/JingCai/Normal/FinishLabel").gameObject:GetComponent(typeof(UILabel))
    self.m_ItemTemplate = self.transform:Find("QnTabView/JingCai/Normal/ItemTemplate").gameObject

    self.m_OperateBtn = self.transform:Find("QnTabView/JingCai/Normal/Bottom/OperateBtn").gameObject
    self.m_TipButton = self.transform:Find("QnTabView/JingCai/Normal/TipButton").gameObject

    local getRwardTipButton = self.transform:Find("QnTabView/JiLu/TipButton").gameObject
    UIEventListener.Get(getRwardTipButton).onClick = DelegateFactory.VoidDelegate(function (p)
        g_MessageMgr:ShowMessage("Star_Biwu_JingcaiRecord_Rule")
    end)

    self.m_JueSaiJingCaiPhaseNumTable = nil

    UIEventListener.Get(self.m_TipButton).onClick = DelegateFactory.VoidDelegate(function (p)
        g_MessageMgr:ShowMessage("Star_Biwu_Jingcai_Rule")
    end)
    UIEventListener.Get(self.m_WatchButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        if self.m_jingcaiPhaseNum > 0 and self.m_jingcaiPhaseNum <= 3 then
            CLuaStarBiwuMgr.m_InitShowGroup = 2
            CLuaStarBiwuMgr:OpenStarBiWuDetailWnd(0)
        elseif self.m_jingcaiPhaseNum > 3 and self.m_jingcaiPhaseNum <= 6 then
            CLuaStarBiwuMgr.m_InitShowGroup = 1
            CLuaStarBiwuMgr:OpenStarBiWuDetailWnd(0)
        elseif self.m_jingcaiPhaseNum > 6 then
            CLuaStarBiwuMgr:OpenStarBiWuDetailWnd(1)
        end
    end)
    UIEventListener.Get(self.m_WorldFilterButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnWorldFilterClick()
    end)
    -- UIEventListener.Get(self.m_RebackFinalButton).onClick = DelegateFactory.VoidDelegate(function (p)
    --     self.m_RebackFinalButtonStatus = false
    --     self.m_RebackFinalButton:SetActive(self.m_RebackFinalButtonStatus)
    --     Gac2Gas.StarBiwuQueryCurrentJingCaiItem()
    -- end)
    self:InitQuestionView()
    self:GetJueSaiJingCaiPhaseNumBegin()
    self:RefreshJingCaiQuanItem()
end


function LuaStarBiwuJingCaiWnd:RefreshJingCaiQuanItem()
    local itemid = StarBiWuShow_Setting.GetData().JingCaiCouponItemId
    local count = StarBiWuShow_Setting.GetData().JingCaiCouponPrice
    local curNum = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, itemid)
    self.m_JingCaiQuanMoney = count*curNum
    self.m_JingCaiQuanLabel.text = g_MessageMgr:FormatMessage("StarBiWu_JingCaiQuanDesc",curNum)
end

function LuaStarBiwuJingCaiWnd:GetJueSaiJingCaiPhaseNumBegin()
    StarBiWuShow_FinalStage.Foreach(function(k,v)
        if not self.m_JueSaiJingCaiPhaseNumBegin then
			self.m_JueSaiJingCaiPhaseNumBegin = k
		end
		if self.m_JueSaiJingCaiPhaseNumBegin > k then
			self.m_JueSaiJingCaiPhaseNumBegin = k
		end
    end)
end


function LuaStarBiwuJingCaiWnd:OnTabChange(go, index)
    if index == 0 then
        Gac2Gas.StarBiwuQueryCurrentJingCaiItem()   --查询当前竞猜奖项
    elseif index == 1 then
        Gac2Gas.StarBiwuQueryJingCaiRecord()        --查询竞猜记录
    elseif index == 2 then
        -- 查询竞猜问答的回答情况
        Gac2Gas.QueryStarBiwuJingCaiWenDa()
        self.m_QuestionViewAlert:SetActive(false)
        LuaActivityRedDotMgr:OnRedDotClicked(90)
    elseif index == 3 then
        Gac2Gas.QueryRank(StarBiWuShow_Setting.GetData().JingCaiRankId)
	end
end

function LuaStarBiwuJingCaiWnd:OnEnable()
    g_ScriptEvent:AddListener("StarBiwuSendCurrentJingCaiItem", self, "StarBiwuSendCurrentJingCaiItem")
    g_ScriptEvent:AddListener("StarBiwuSendJingCaiRecord", self, "StarBiwuSendJingCaiRecord")
    g_ScriptEvent:AddListener("StarBiwuJingCaiBetResult", self, "StarBiwuJingCaiBetResult")
    g_ScriptEvent:AddListener("StarBiwuJingCaiGetRewardResult", self, "StarBiwuJingCaiGetRewardResult")
    g_ScriptEvent:AddListener("ReplyStarBiwuZongJueSaiMatch", self, "ReplyStarBiwuZongJueSaiMatch")
    g_ScriptEvent:AddListener("StarBiwuSendJingCaiHistory", self, "StarBiwuSendJingCaiHistory")
    g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
    g_ScriptEvent:AddListener("SendItem", self, "RefreshJingCaiQuanItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "RefreshJingCaiQuanItem")
    g_ScriptEvent:AddListener("OnSyncStarBiwuQuestionInfo", self, "UpdateQuestionViewInfo")
    if CClientMainPlayer.Inst  then
        if PlayerPrefs.GetInt(tostring(CClientMainPlayer.Inst.Id)) ~= 1 then
            g_MessageMgr:ShowMessage("Star_Biwu_Jingcai_Rule")
        end
        PlayerPrefs.SetInt(tostring(CClientMainPlayer.Inst.Id), 1)
    end

end

function LuaStarBiwuJingCaiWnd:OnDisable()
    g_ScriptEvent:RemoveListener("StarBiwuSendCurrentJingCaiItem", self, "StarBiwuSendCurrentJingCaiItem")
    g_ScriptEvent:RemoveListener("StarBiwuSendJingCaiRecord", self, "StarBiwuSendJingCaiRecord")
    g_ScriptEvent:RemoveListener("StarBiwuJingCaiBetResult", self, "StarBiwuJingCaiBetResult")
    g_ScriptEvent:RemoveListener("ReplyStarBiwuZongJueSaiMatch", self, "ReplyStarBiwuZongJueSaiMatch")
    g_ScriptEvent:RemoveListener("StarBiwuJingCaiGetRewardResult", self, "StarBiwuJingCaiGetRewardResult")
    g_ScriptEvent:RemoveListener("StarBiwuSendJingCaiHistory", self, "StarBiwuSendJingCaiHistory")
    g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")
    g_ScriptEvent:RemoveListener("SendItem", self, "RefreshJingCaiQuanItem")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "RefreshJingCaiQuanItem")
    g_ScriptEvent:RemoveListener("OnSyncStarBiwuQuestionInfo", self, "UpdateQuestionViewInfo")

    if self.m_EffectTick ~= nil then
        invoke(self.m_EffectTick)
    end
    if self.m_ShowBetPoolTick then UnRegisterTick(self.m_ShowBetPoolTick) self.m_ShowBetPoolTick = nil end
end
-- 往期竞猜记录查询
function LuaStarBiwuJingCaiWnd:OnWorldFilterClick()
    if not self.m_jingcaiPhaseNum or  self.m_jingcaiPhaseNum <= 0 then return end
    self.m_WorldFilterButton:SetTipStatus(false)
    local curIndex = self.m_jingcaiPhaseNum - self.m_jingcaiHistoryPhaseNum
    -- if self.m_RebackFinalButtonStatus and #self.m_JueSaiJingCaiTotalList >= self.m_JueSaiJingCaiPhaseNumBegin then
    --     local num = #self.m_JueSaiJingCaiTotalList
    --     local index = 0
    --     for i = num,1,-1 do
    --         if self.m_jingcaiHistoryPhaseNum == self.m_JueSaiJingCaiTotalList[i] then
    --             curIndex = index
    --         end
    --         index = index + 1
    --     end
    -- end
	CPlayerShopPopupMenuInfoMgr.ShowPlayerShopPopupMenuWithdefaultSelectedIndex(Table2Array(self.m_FilterActions, MakeArrayClass(PopupMenuItemData)), curIndex , self.m_WorldFilterButton.transform, CPopupMenuInfoMgrAlignType.Bottom, 1, nil, nil, DelegateFactory.Action(function ()
        self.m_WorldFilterButton:SetTipStatus(true)
    end), 600,300)
end
-- 往期竞猜查询
function LuaStarBiwuJingCaiWnd:StarBiwuSendJingCaiHistory(jingcaiItemList,rightChoiceList, jingcaiStage, targetDate, phaseBetPool, choice2BetValueList,resultList,matchInfoList,maxBetValue)
    self.NoRpcRoot:SetActive(false)
    self.m_FinalRoot:SetActive(false)
    self.m_jingcaiItemTblUd = jingcaiItemList
    self.m_rightChice = rightChoiceList
    self.m_targetDate = targetDate
    self.m_currentBetPool = phaseBetPool
    self.m_Choice2BetList = choice2BetValueList
    self.m_JingCaiResultList = resultList
    self.m_maxBetValue = maxBetValue
    local StageName = ""
    local endTime = ""
    local hasJingCai = resultList and resultList[1] or false
    if self.m_jingcaiHistoryPhaseNum < self.m_JueSaiJingCaiPhaseNumBegin then
        StageName = StarBiWuShow_Stage.GetData(self.m_jingcaiHistoryPhaseNum).StageName
        endTime = StarBiWuShow_Stage.GetData(self.m_jingcaiHistoryPhaseNum).EndTime
    else
        StageName = StarBiWuShow_FinalStage.GetData(self.m_jingcaiHistoryPhaseNum).StageName
        endTime = StarBiWuShow_FinalStage.GetData(self.m_jingcaiHistoryPhaseNum).EndTime
    end
    self:ShowCurrentBetPoolAni(self.m_jingcaiHistoryPhaseNum, phaseBetPool)
    self.m_targetStageName = StageName
    self.m_targetStageNameLabel.text = StageName
    --self.m_currentBetPoolLabel.text = LocalString.GetString(self.m_currentBetPool)
    self.m_targetDateLabel.text = LocalString.GetString(self.m_targetDate)
    self.m_maxBetValueLabel.text = LocalString.GetString(self.m_maxBetValue)
    self.m_WorldFilterButton.gameObject:SetActive(true)
    self.m_WorldFilterButton.transform:Find("Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("[ACF8FF]第%d期竞猜[-]"),self.m_jingcaiHistoryPhaseNum)
    self.m_WorldFilterButton.transform:Find("LeftTimeLabel"):GetComponent(typeof(UILabel)).text = self:GetJingCaiLeftTime(endTime)
    self.m_HistoryJingCaiTeamName = {}
    for i = 1,4 do
        self.m_HistoryJingCaiTeamName[i] = {}
        self.m_HistoryJingCaiTeamName[i][1] =  matchInfoList and matchInfoList[i] and matchInfoList[i][1] or ""
        self.m_HistoryJingCaiTeamName[i][2] =  matchInfoList and matchInfoList[i] and matchInfoList[i][2] or ""
    end

    self.m_BottomRoot:SetActive(false)
    self.m_FinishLabel.gameObject:SetActive(true)
    self.m_JingCaiQuanLabel.gameObject:SetActive(false)
    local textArr = LocalString.GetString("该期竞猜结果已揭晓:")
    if not hasJingCai then
        textArr = LocalString.GetString("本期竞猜已截止，请耐心等待比赛结果")
    else
        for i = 1, 4 do
            textArr= textArr .. (self.m_rightChice[i - 1] > 0 and LocalString.GetString("是") or LocalString.GetString("否"))
        end
    end
    self.m_FinishLabel.text = textArr
    self.m_WatchButton.gameObject:SetActive(false)
    
    self:UpdateHistoryJingCaiItem()
end
function LuaStarBiwuJingCaiWnd:UpdateHistoryJingCaiItem()
    self.m_ItemTemplate:SetActive(false)
    self.m_resultTable = {}
    Extensions.RemoveAllChildren(self.m_ContentRoot.transform)
    local item = self.m_ItemTemplate
	local gridList = MsgPackImpl.unpack(self.m_jingcaiItemTblUd)
	if gridList then
        for i = 0, gridList.Count - 1, 1 do
            self.m_resultTable[i+1] = self.m_rightChice[i]
            local LuaStarBiwuJingCaiItem = LuaStarBiwuJingCaiItem:new()
            LuaStarBiwuJingCaiItem:InitHistoryItem(self.m_ContentRoot.gameObject,item,gridList,self.m_rightChice,i,self.m_HistoryJingCaiTeamName[i + 1][1], self.m_HistoryJingCaiTeamName[i + 1][2], self.m_JingCaiResultList )
		end
    end
    if self.m_JingCaiResultList and self.m_JingCaiResultList[1] then
        self.m_HuiBaoBeiShuLabel.text = CLuaStarBiwuMgr:GetHuiBaoBeiShu(gridList.Count, self.m_resultTable, self.m_Choice2BetList, self.m_currentBetPool)
    else
        self.m_HuiBaoBeiShuLabel.text = nil
    end
    self.m_ContentRoot:Reposition()
end
-- 竞猜查询当前竞猜的返回
function LuaStarBiwuJingCaiWnd:StarBiwuSendCurrentJingCaiItem(jingcaiItemTblUd, canBet, targetDate, targetStageName, maxBetValue, currentBetPool, jingcaiPhaseNum,phaseBet,canGetReward,matchtbl, choice2BetList, singleChoiceList, jingcaiShutdownStatus, rightChice, resultList, jingcaiquanCount)
    self.NoRpcRoot:SetActive(false)
    self.m_FinalRoot:SetActive(false)
    self:ShowCurrentBetPoolAni(jingcaiPhaseNum, currentBetPool)
    self.m_jingcaiItemTblUd = jingcaiItemTblUd
    self.m_canBet = canBet
    self.m_jingcaiShutdownStatus = jingcaiShutdownStatus
    self.m_rightChice = rightChice
    self.m_targetDate = targetDate
    self.m_targetStageName = targetStageName
    self.m_maxBetValue = maxBetValue
    self.m_currentBetPool = currentBetPool
    self.m_jingcaiPhaseNum = jingcaiPhaseNum
    self.m_jingcaiHistoryPhaseNum = jingcaiPhaseNum
    self.m_phaseBet = phaseBet
    self.m_canGetReward = canGetReward
    self.m_canGetFaildReward = jingcaiquanCount
    self.m_Choice2BetList = choice2BetList
    self.m_SingleChoiceList = singleChoiceList
    self.m_JingCaiResultList = resultList
    self.m_JingCaiTeamNameList = {}
    for i = 1,4 do
        self.m_JingCaiTeamNameList[i] = {}
        self.m_JingCaiTeamNameList[i][1] =  matchtbl and matchtbl[i] and matchtbl[i][1] or ""
        self.m_JingCaiTeamNameList[i][2] =  matchtbl and matchtbl[i] and matchtbl[i][2] or ""
    end
    -- self.m_JingCaiTeamName1 = matchtbl and matchtbl[0] or ""
    -- self.m_JingCaiTeamName2 = matchtbl and matchtbl[1] or ""

    self.m_XiaZhuUnit = StarBiWuShow_Setting.GetData().XiaZhuUnit

    if  self.m_maxBetValue - self.m_phaseBet == 0 then
        self.m_canBetValue = self.m_minXiazhu
        self.m_OperateBtn.transform:GetComponent(typeof(QnButton)).Enabled = false
    else
        self.m_OperateBtn.transform:GetComponent(typeof(QnButton)).Enabled = true
        self.m_canBetValue = (self.m_maxBetValue - self.m_phaseBet)/self.m_XiaZhuUnit
    end

    self:UpdatePanel()
    self:InitFilterButton()
    self.NormalRoot:SetActive(true)
    self.m_RebackFinalButton:SetActive(false)
    self.m_ContentRoot:Reposition()
end


function LuaStarBiwuJingCaiWnd:UpdatePanel()
    --self.m_currentBetPoolLabel.text = LocalString.GetString(self.m_currentBetPool)
    self.m_targetDateLabel.text = LocalString.GetString(self.m_targetDate)
    self.m_targetStageNameLabel.text = LocalString.GetString(self.m_targetStageName)
    self.m_maxBetValueLabel.text = LocalString.GetString(self.m_maxBetValue)
    self.m_WorldFilterButton.gameObject:SetActive(true)
    local endTime = ""
    if self.m_jingcaiPhaseNum < self.m_JueSaiJingCaiPhaseNumBegin then
        endTime = StarBiWuShow_Stage.GetData(self.m_jingcaiPhaseNum).EndTime
    else
        endTime = StarBiWuShow_FinalStage.GetData(self.m_jingcaiPhaseNum).EndTime
    end
    self.m_WorldFilterButton.transform:Find("Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("[ACF8FF]第%d期竞猜[-]"),self.m_jingcaiPhaseNum)
    self.m_WorldFilterButton.transform:Find("LeftTimeLabel"):GetComponent(typeof(UILabel)).text = self:GetJingCaiLeftTime(endTime)
    self.m_HuiBaoBeiShuLabel.text = ""
    self.m_tabAlert:SetActive(tonumber(self.m_canGetReward) ~= 0 or tonumber(self.m_canGetFaildReward) ~= 0)
    self.m_BottomRoot:SetActive(self.m_canBet)
    self.m_FinishLabel.gameObject:SetActive(not self.m_canBet)
    self.m_JingCaiQuanLabel.gameObject:SetActive(self.m_canBet)
    local textArr = {
        LocalString.GetString("本期竞猜已结束，请耐心等待比赛结果"),
        LocalString.GetString("该期竞猜已截止，比赛正在进行中"),
        LocalString.GetString("该期竞猜结果已揭晓:")
    }
    if self.m_jingcaiShutdownStatus == 3 and self.m_rightChice >= 0 then
        local arr = {}
        local tmp = self.m_rightChice
        for i = 1, 4 do
            arr[5 - i] = math.floor(tmp % 10) 
            tmp = tmp / 10
        end
        for i = 1, 4 do
            textArr[3] = textArr[3] .. (arr[i] > 0 and LocalString.GetString("是") or LocalString.GetString("否"))
        end
    end
    self.m_FinishLabel.text = textArr[self.m_jingcaiShutdownStatus]
    self.m_WatchButton.gameObject:SetActive(not self.m_canBet and self.m_jingcaiShutdownStatus == 2)
    UIEventListener.Get(self.m_OperateBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        for i = 0,#self.m_resultTable do
            if self.m_resultTable[i] == -1 then
                g_MessageMgr:ShowMessage("Star_Biwu_Jingcai_Not_Open")
                return
            end
        end

        local msg = g_MessageMgr:FormatMessage("Star_Biwu_Jingcai_Confirm",self.m_Cost)
        MessageWndManager.ShowConfirmMessage(msg,15,false,DelegateFactory.Action(function ()
            self.m_HuiBaoBeiShuLabel.text = ""
            local cslist = Table2List(self.m_resultTable, MakeGenericClass(List, Object))
            local data = MsgPackImpl.pack(cslist)
            Gac2Gas.StarBiwuRequestJingCaiBet(self.m_jingcaiPhaseNum,self.m_Cost,data)-- 请求竞猜 jingcaiPhaseNum, jingcaiBetCount, jingcaiChoiceTblUd
        end), nil)
    end)
    self.m_Cost = self.m_minXiazhu * self.m_XiaZhuUnit

    self.m_AddSubAndInputButton:SetValue(self.m_minXiazhu, true)
    self.m_AddSubAndInputButton:SetMinMax(self.m_minXiazhu, self.m_canBetValue, 1)

    self:UpdateJingCaiItem()
end

function LuaStarBiwuJingCaiWnd:ShowCurrentBetPoolAni(curPhaseNum, updateBetPool)
    if self.m_ShowBetPoolTick then UnRegisterTick(self.m_ShowBetPoolTick) self.m_ShowBetPoolTick = nil end
    local curBetPool = self.m_currentBetPool
    self.m_currentBetPoolLabel.text = tostring(curBetPool)
    if curPhaseNum == self.m_jingcaiPhaseNum and self.m_jingcaiHistoryPhaseNum == curPhaseNum and updateBetPool > curBetPool  then
        local totalTime = 1000
        local interval = 20
        local delta = math.floor((updateBetPool - curBetPool) / (totalTime / interval))
        self.m_ShowBetPoolTick = RegisterTickWithDuration(function ()
            curBetPool = curBetPool + delta
            self.m_currentBetPoolLabel.text = tostring(curBetPool)
            if curBetPool >= updateBetPool then
                self.m_currentBetPoolLabel.text = tostring(updateBetPool)
            end
        end, interval, totalTime + interval)
    else
        self.m_currentBetPoolLabel.text = tostring(updateBetPool)
    end
end

function LuaStarBiwuJingCaiWnd:GetJingCaiLeftTime(endTime) 
    local curTime = CServerTimeMgr.Inst.timeStamp
    local endStamp = CServerTimeMgr.Inst:GetTimeStampByStr(endTime)
    if curTime > endStamp then
        return LocalString.GetString("[949697]已截止[-]")
    else
        local leftTime = endStamp - curTime
        local day = math.floor(leftTime / (60*60*24))
        local hour = math.floor((leftTime - day*60*60*24) / (60*60))
        if day > 0 then
            return SafeStringFormat(LocalString.GetString("[A1D26E]剩余%d天%d小时[-]"),day,hour)
        elseif hour > 0 then
            return SafeStringFormat(LocalString.GetString("[A1D26E]剩余%d小时[-]"),hour)
        else
            return LocalString.GetString("[A1D26E]剩余不足1小时[-]")
        end
    end
end

function LuaStarBiwuJingCaiWnd:UpdateJingCaiItem()
    self.m_ItemTemplate:SetActive(false)
    self.m_resultTable = {}
    Extensions.RemoveAllChildren(self.m_ContentRoot.transform)
    local item = self.m_ItemTemplate
	local gridList = MsgPackImpl.unpack(self.m_jingcaiItemTblUd)
	if gridList then
        for i = 0, gridList.Count - 1, 1 do
            self.m_resultTable[i+1] = -1
            local LuaStarBiwuJingCaiItem = LuaStarBiwuJingCaiItem:new()
            LuaStarBiwuJingCaiItem:Init(self.m_ContentRoot.gameObject,item,gridList,i, self.m_SingleChoiceList, self.m_JingCaiTeamNameList[i + 1][1], self.m_JingCaiTeamNameList[i + 1][2], self.m_JingCaiResultList,self.m_jingcaiShutdownStatus,self.m_rightChice )
            LuaStarBiwuJingCaiItem.m_Callback = function (i,c)
                self.m_resultTable[i+1] = c
                for i = 0,#self.m_resultTable do
                    if self.m_resultTable[i] == -1 then
                        return
                    end
                end
                self.m_HuiBaoBeiShuLabel.text = CLuaStarBiwuMgr:GetHuiBaoBeiShu(gridList.Count, self.m_resultTable, self.m_Choice2BetList, self.m_currentBetPool)
            end
		end
    end
    if self.m_jingcaiShutdownStatus == 3 and self.m_rightChice >= 0 then
        local arr = {}
        local tmp = self.m_rightChice
        for i = 1, 4 do
            self.m_resultTable[5 - i] = math.floor(tmp % 10) 
            tmp = tmp / 10
        end
        self.m_HuiBaoBeiShuLabel.text = CLuaStarBiwuMgr:GetHuiBaoBeiShu(gridList.Count, self.m_resultTable, self.m_Choice2BetList, self.m_currentBetPool)
    end
    self.m_ContentRoot:Reposition()
end

function LuaStarBiwuJingCaiWnd:StarBiwuJingCaiBetResult(phaseBet)

    self.m_phaseBet = phaseBet
    if  self.m_maxBetValue - self.m_phaseBet == 0 then
        self.m_canBetValue = self.m_minXiazhu
        self.m_OperateBtn.transform:GetComponent(typeof(QnButton)).Enabled = false
    else
        self.m_OperateBtn.transform:GetComponent(typeof(QnButton)).Enabled = true
        self.m_canBetValue = (self.m_maxBetValue - self.m_phaseBet)/self.m_XiaZhuUnit
    end
    self.m_AddSubAndInputButton:SetMinMax(self.m_minXiazhu, self.m_canBetValue, 1)
    self:UpdateJingCaiItem()
end

-- 竞猜查询历史记录的返回
function LuaStarBiwuJingCaiWnd:StarBiwuSendJingCaiRecord(jingcaiRecordForClientUd, eachDataLen,canGetReward,jingcaiquanCount)
    local jingcaiRecordList = MsgPackImpl.unpack(jingcaiRecordForClientUd)
    if not jingcaiRecordList then
      return
    end

    self.m_RecordTable = {}
    for i = 0, jingcaiRecordList.Count - 1, eachDataLen do
      local t = {}
      t.recordIdx       = jingcaiRecordList[i]    -- 序号, recordIdx. 这个数据后面的上行RPC StarBiwuQueryJingCaiRecordDetail 也会用
      t.jingcaiPhaseNum = jingcaiRecordList[i+1]  -- 第几期竞猜
      t.jingcaiBetTime  = jingcaiRecordList[i+2]  -- 竞猜投注时间
      t.jingcaiBetPool  = jingcaiRecordList[i+3]  -- 对应奖池
      t.jingcaiBetCount = jingcaiRecordList[i+4]  -- 竞猜投注数额
      t.jingcaiReward   = jingcaiRecordList[i+5]  -- 竞猜奖励
      t.faildAward     = jingcaiRecordList[i+6]  -- 竞猜失败奖励
      t.hasGetReward    = jingcaiRecordList[i+7]  -- 竞猜奖励是否领取,数据类型,1表示已经领取
      t.bJingcaiRes     = jingcaiRecordList[i+8]  -- 竞猜结果是否揭晓,bool类型
      -- t.faildAward      = jingcaiRecordList[i+4] / 100000 -- 竞猜失败的奖励

      table.insert(self.m_RecordTable,t)
    end

    self.m_canGetReward = canGetReward
    self.m_canGetFaildReward = jingcaiquanCount

    self.m_tabAlert:SetActive(tonumber(self.m_canGetReward) ~= 0 or tonumber(self.m_canGetFaildReward) ~= 0)
    self.m_btnAlert:SetActive(tonumber(self.m_canGetReward) ~= 0 or tonumber(self.m_canGetFaildReward) ~= 0)

    self.m_RecordTipLabel:SetActive(#self.m_RecordTable == 0)
    table.sort(self.m_RecordTable, function(a, b) return a.jingcaiBetTime > b.jingcaiBetTime end)

    CLuaStarBiwuMgr.JingCaiRecordTable = self.m_RecordTable

	self.m_AdvView.m_DataSource=DefaultTableViewDataSource.Create(function()
        return #self.m_RecordTable
    end, function(item, index)
        self:InitRecordItem(item, index)
    end)

    self.m_AdvView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        CLuaStarBiwuMgr.JingCaiRecordCurrentIndex = row + 1

        local selectAction = DelegateFactory.Action_int(function (row)
            CUIManager.ShowUI(CLuaUIResources.StarBiwuJingCaiDetailWnd)
        end)

        local danmuType = PlayerSettings.CCLiveDanMuType

        local item1 = PopupMenuItemData(LocalString.GetString("查看详情"), selectAction, false, nil, EnumPopupMenuItemStyle.Default)
        local tbl = {item1}
        local array = Table2Array(tbl, MakeArrayClass(PopupMenuItemData))
        CPopupMenuInfoMgr.ShowPopupMenuWithDefaultSelectIndex(array, danmuType, self.m_AdvView:GetItemAtRow(row).transform, CPopupMenuInfoMgrAlignType.Right)
    end)

    self.m_AdvView:ReloadData(true,false)
    self.m_AwardtLabel.text = self.m_canGetReward
    if self.m_canGetReward > 0 or self.m_canGetFaildReward > 0 then
        self.m_AwardtButton:GetComponent(typeof(QnButton)).Enabled = true
    else
        self.m_AwardtButton:GetComponent(typeof(QnButton)).Enabled = false
    end
    UIEventListener.Get(self.m_AwardtButton).onClick = DelegateFactory.VoidDelegate(function (p)
        Gac2Gas.StarBiwuRequestGetJingCaiReward()
    end)
end

function LuaStarBiwuJingCaiWnd:InitRecordItem(item,index)
    item.transform:Find("IndexLabel"):GetComponent(typeof(UILabel)).text = self.m_RecordTable[index+1].recordIdx
    item.transform:Find("QiShuLabel"):GetComponent(typeof(UILabel)).text = self.m_RecordTable[index+1].jingcaiPhaseNum
    local dateTime = CServerTimeMgr.ConvertTimeStampToZone8Time(self.m_RecordTable[index+1].jingcaiBetTime)
    item.transform:Find("TimeLabel"):GetComponent(typeof(UILabel)).text = System.String.Format("{0:yyyy-MM-dd}", dateTime)
    item.transform:Find("TotalLabel"):GetComponent(typeof(UILabel)).text = self.m_RecordTable[index+1].jingcaiBetPool
    item.transform:Find("BetLabel"):GetComponent(typeof(UILabel)).text = self.m_RecordTable[index+1].jingcaiBetCount

    if tonumber(self.m_RecordTable[index+1].recordIdx%2) ~= 0 then
        item:SetBackgroundTexture("common_textbg_02_dark")
    else
        item:SetBackgroundTexture("common_textbg_02_light")
    end

    local HaveGot = item.transform:Find("HaveGot").gameObject
    local AwardSprite = item.transform:Find("AwardSprite").gameObject
    local ResultLabel = item.transform:Find("ResultLabel"):GetComponent(typeof(UILabel))
    local FaildAwardTexture = item.transform:Find("FaildAwardTexture").gameObject
    local FaildResult = item.transform:Find("FaildResult"):GetComponent(typeof(UILabel))
    if self.m_RecordTable[index+1].bJingcaiRes then
        if self.m_RecordTable[index+1].jingcaiReward == 0 then
            ResultLabel.text = nil
            HaveGot:SetActive(false)
            FaildAwardTexture:SetActive(true)
            FaildResult.gameObject:SetActive(true)
            FaildResult.text = SafeStringFormat("%.1f",self.m_RecordTable[index+1].faildAward)
            HaveGot:SetActive(self.m_RecordTable[index+1].hasGetReward == 1) 
            AwardSprite:SetActive(false)
        else
            HaveGot:SetActive(self.m_RecordTable[index+1].hasGetReward == 1)
            FaildAwardTexture:SetActive(false)
            ResultLabel.text = self.m_RecordTable[index+1].jingcaiReward
            FaildResult.gameObject:SetActive(false)
            AwardSprite:SetActive(true)
        end
    else
        ResultLabel.text = LocalString.GetString("未揭晓")
        HaveGot:SetActive(false)
        AwardSprite:SetActive(false)
        FaildAwardTexture:SetActive(false)
        FaildResult.gameObject:SetActive(false)
    end
end

function LuaStarBiwuJingCaiWnd:StarBiwuJingCaiGetRewardResult(bSuccess,leftReward)
    if bSuccess then
        local fx = self.m_AwardtButton.transform:Find("FX"):GetComponent(typeof(CUIFx))
        fx:LoadFx("Fx/UI/Prefab/UI_hongbaojingbi.prefab")

        if self.m_EffectTick ~= nil then
            invoke(self.m_EffectTick)
        end

        self.m_EffectTick = RegisterTickOnce(function ()
            fx.gameObject:SetActive(false)
        end, 1650)
    end
end

function LuaStarBiwuJingCaiWnd:InitFilterButton()
    if not self.m_jingcaiPhaseNum or self.m_jingcaiPhaseNum <= 0 then return end
    self.m_FilterActions = {}
    -- if self.m_jingcaiPhaseNum >= self.m_JueSaiJingCaiPhaseNumBegin then
    --     self.m_JueSaiJingCaiTotalList = {}
    --     local jingcaiNum2JueSaiIndex = {}
    --     for i = 1, self.m_JueSaiJingCaiPhaseNumBegin - 1 do
    --         table.insert(self.m_JueSaiJingCaiTotalList,i)
    --     end
    --     if self.m_JueSaiJingCaiPhaseNumTable then
    --         for k,v in pairs(self.m_JueSaiJingCaiPhaseNumTable) do
    --             table.insert(self.m_JueSaiJingCaiTotalList,v)
    --             jingcaiNum2JueSaiIndex[v] = k
    --         end
    --     end
    --     local num = #self.m_JueSaiJingCaiTotalList
    --     for i = num,1,-1  do
    --         local name = SafeStringFormat3(LocalString.GetString("[ACF8FF]第%d期竞猜[-]"),self.m_JueSaiJingCaiTotalList[i])
    --         local endTime = ""
    --         if self.m_JueSaiJingCaiTotalList[i] < self.m_JueSaiJingCaiPhaseNumBegin then
    --             endTime = StarBiWuShow_Stage.GetData(self.m_jingcaiPhaseNum).EndTime
    --         else
    --             endTime = StarBiWuShow_FinalStage.GetData(self.m_jingcaiPhaseNum).EndTime
    --         end
    --         local leftTime = self:GetJingCaiLeftTime(endTime)
    --         table.insert(self.m_FilterActions,PopupMenuItemData(name, DelegateFactory.Action_int(function (index) 
    --             self.m_jingcaiHistoryPhaseNum = self.m_JueSaiJingCaiTotalList[num - index]
    --             if self.m_JueSaiJingCaiTotalList[num - index] < self.m_JueSaiJingCaiPhaseNumBegin then
    --                 Gac2Gas.StarBiwuQueryJingCaiHistory(tonumber(self.m_JueSaiJingCaiTotalList[num - index]))
    --             else
    --                 local juesaiIndex = jingcaiNum2JueSaiIndex[self.m_JueSaiJingCaiTotalList[num - index]]
    --                 Gac2Gas.StarBiwuQueryJueSaiJingCaiItem(juesaiIndex)
    --             end
    --         end), false, nil,0,EnumPopupMenuItemStyle.Special, Table2Array({leftTime}, MakeArrayClass(cs_string))))
    --     end
    -- else
        for i = self.m_jingcaiPhaseNum,1,-1  do
            local name = SafeStringFormat3(LocalString.GetString("[ACF8FF]第%d期竞猜[-]"),i)
            local endTime = ""
            if i < self.m_JueSaiJingCaiPhaseNumBegin then
                endTime = StarBiWuShow_Stage.GetData(i).EndTime
            else
                endTime = StarBiWuShow_FinalStage.GetData(i).EndTime
            end
            local leftTime = self:GetJingCaiLeftTime(endTime)
            table.insert(self.m_FilterActions,PopupMenuItemData(name, DelegateFactory.Action_int(function (index) 
                if index == 0 then
                    Gac2Gas.StarBiwuQueryCurrentJingCaiItem()
                else
                    self.m_jingcaiHistoryPhaseNum = self.m_jingcaiPhaseNum - index
                    Gac2Gas.StarBiwuQueryJingCaiHistory(tonumber(self.m_jingcaiPhaseNum - index))
                end
                
            end), false, nil,0,EnumPopupMenuItemStyle.Special, Table2Array({leftTime}, MakeArrayClass(cs_string))))
        end
    -- end
    
	
end


-- Final JingCai
RegistClassMember(LuaStarBiwuJingCaiWnd, "m_CurrentMatchIndex")
RegistClassMember(LuaStarBiwuJingCaiWnd, "m_SelfZhanduiId")
RegistClassMember(LuaStarBiwuJingCaiWnd, "m_Name1LabelTable")
RegistClassMember(LuaStarBiwuJingCaiWnd, "m_Name2LabelTable")
RegistClassMember(LuaStarBiwuJingCaiWnd, "m_Score1LabelTable")
RegistClassMember(LuaStarBiwuJingCaiWnd, "m_Score2LabelTable")
RegistClassMember(LuaStarBiwuJingCaiWnd, "m_MatchingFxTable")
RegistClassMember(LuaStarBiwuJingCaiWnd, "m_JingCaiBtnTable")

-- For default name
RegistClassMember(LuaStarBiwuJingCaiWnd, "m_MatchIndexTable")
RegistClassMember(LuaStarBiwuJingCaiWnd, "m_PreMatchIsWinTable")

RegistClassMember(LuaStarBiwuJingCaiWnd, "m_RedColor")
RegistClassMember(LuaStarBiwuJingCaiWnd, "m_BlueColor")
RegistClassMember(LuaStarBiwuJingCaiWnd, "m_CurrentDataColor")
RegistClassMember(LuaStarBiwuJingCaiWnd, "m_FinalInited")

function LuaStarBiwuJingCaiWnd:ReplyStarBiwuZongJueSaiMatch(currentMatchPlayIdx, selfZhanduiId, matchDataUd, syncType, canGetReward)
    -- 不显示淘汰赛对阵信息
    if true then return end
    if not self.m_FinalInited then self:InitFinalJingCai() end
    self.NoRpcRoot:SetActive(false)
    self.NormalRoot:SetActive(false)
    self.m_FinalRoot:SetActive(true)
    self.m_RebackFinalButton:SetActive(false)
    self.m_tabAlert:SetActive(canGetReward ~= 0)

    -- 当前正在比赛的playIdx,这个值的范围目前对应的是 0-14,也是观战请求 RequestWatchStarBiwuZongJueSai 的参数
    -- 0表示当前没有正在进行的比赛
    self.m_CurrentMatchIndex = currentMatchPlayIdx

    -- 自己的战队id,0表示自己不处于战队中
    self.m_SelfZhanduiId = selfZhanduiId
    -- 对阵信息
    local matchDataList = MsgPackImpl.unpack(matchDataUd)
    local index = 1
    self.m_JueSaiJingCaiPhaseNumTable = {}
    for i = 0, matchDataList.Count - 1, 6 do
      local attackerZId   = matchDataList[i]    -- 战队id
      local attackerZName = matchDataList[i+1]  -- 战队名
      local attackerPoint = matchDataList[i+2]  -- 战队得分,attackerPoint 和 defenderPoint都是0 就不显示

      local defenderZId   = matchDataList[i+3]  -- 战队id
      local defenderZName = matchDataList[i+4]  -- 战队名
      local defenderPoint = matchDataList[i+5]  -- 战队得分,attackerPoint 和 defenderPoint都是0 就不显示

      self:RefreshZhandui(index, attackerZId, attackerZName, attackerPoint, defenderZId, defenderZName, defenderPoint)
      index = index + 1
    end
end

function LuaStarBiwuJingCaiWnd:RefreshZhandui(index, id1, name1, score1, id2, name2, score2)
    local hasAlpha1 = false
    local hasAlpha2 = false
    if index >= 5 then
        local iindex = (index - 5) * 2 + 1
        if not name1 or name1 == "" then
            name1 = SafeStringFormat3(LocalString.GetString("第%s场%s者"), self.m_MatchIndexTable[iindex], self.m_PreMatchIsWinTable[iindex] == 1 and LocalString.GetString("胜") or LocalString.GetString("败"))
            hasAlpha1 = true
        end

        if not name2 or name2 == "" then
            name2 = SafeStringFormat3(LocalString.GetString("第%s场%s者"), self.m_MatchIndexTable[iindex + 1], self.m_PreMatchIsWinTable[iindex + 1] == 1 and LocalString.GetString("胜") or LocalString.GetString("败"))
            hasAlpha2 = true
        end
    end

    if index == self.m_CurrentMatchIndex then
        self.m_MatchingFxTable[index]:LoadFx("fx/ui/prefab/UI_duizhanzhuangtai.prefab")
        self.m_Score1LabelTable[index].text = ""
        self.m_Score2LabelTable[index].text = ""
        self.m_JingCaiBtnTable[index]:SetActive(false)
    else
        self.m_JingCaiBtnTable[index]:SetActive(false)
        self.m_MatchingFxTable[index]:DestroyFx()
        if score1 == 0 and score2 == 0 then
            self.m_Score1LabelTable[index].text = ""
            self.m_Score2LabelTable[index].text = ""
            self.m_JingCaiBtnTable[index]:SetActive(true)
        else
            self.m_Score1LabelTable[index].text = score1
            self.m_Score2LabelTable[index].text = score2
            self.m_Score1LabelTable[index].color = score1 > score2 and self.m_BlueColor or self.m_RedColor
            self.m_Score2LabelTable[index].color = score1 <= score2 and self.m_BlueColor or self.m_RedColor
            if score1 > score2 then
                hasAlpha2 = true
            else
                hasAlpha1 = true
            end
        end
    end
    self.m_JueSaiJingCaiPhaseNumTable[index] = self:GetJingCaiPhaseNumByJuesaiPlayIdx(index)
    self.m_Name1LabelTable[index].text = name1
    self.m_Name1LabelTable[index].color = id1 == self.m_SelfZhanduiId and Color.green or Color.white
    self.m_Name1LabelTable[index].alpha = hasAlpha1 and 0.3 or 1
    self.m_Name2LabelTable[index].text = name2
    self.m_Name2LabelTable[index].color = id2 == self.m_SelfZhanduiId and Color.green or Color.white
    self.m_Name2LabelTable[index].alpha = hasAlpha2 and 0.3 or 1
end

function LuaStarBiwuJingCaiWnd:InitFinalJingCai()
    self.m_FinalInited = true
    self.m_RedColor = NGUIText.ParseColor24("ec7676", 0)
    self.m_BlueColor = NGUIText.ParseColor24("4a8eff", 0)
    self.m_CurrentDataColor = NGUIText.ParseColor24("fff68f", 0)
    self.m_CurrentMatchIndex = 0
    self.m_SelfZhanduiId = 0
    self.m_Name1LabelTable = {}
    self.m_Name2LabelTable = {}
    self.m_Score2LabelTable =  {}
    self.m_Score1LabelTable = {}
    self.m_MatchingFxTable = {}
    self.m_JingCaiBtnTable = {}

    for i = 1, 14 do
        local rootTrans = self.transform:Find("QnTabView/JingCai/Final/"..i)
        local name1Label = rootTrans:Find("Name1"):GetComponent(typeof(UILabel))
        name1Label.text = ""
        local name2Label = rootTrans:Find("Name2"):GetComponent(typeof(UILabel))
        name2Label.text = ""
        local score1Label = rootTrans:Find("Score1"):GetComponent(typeof(UILabel))
        score1Label.text = ""
        local score2Label = rootTrans:Find("Score2"):GetComponent(typeof(UILabel))
        score2Label.text = ""
        local fx = rootTrans:Find("Fx"):GetComponent(typeof(CUIFx))
        table.insert(self.m_Name1LabelTable, name1Label)
        table.insert(self.m_Name2LabelTable, name2Label)
        table.insert(self.m_Score1LabelTable, score1Label)
        table.insert(self.m_Score2LabelTable, score2Label)
        table.insert(self.m_MatchingFxTable, fx)
        local btn = rootTrans:Find("Btn").gameObject
        UIEventListener.Get(btn).onClick = LuaUtils.VoidDelegate(function()
            self.m_JingCaiTeamName1 = name1Label.text
            self.m_JingCaiTeamName2 = name2Label.text
            self.m_RebackFinalButtonStatus = true
            Gac2Gas.StarBiwuQueryJueSaiJingCaiItem(i)
            end)
        table.insert(self.m_JingCaiBtnTable, btn)
    end
    -- from 5 to 14
    self.m_MatchIndexTable = {1, 2, 3, 4, 1, 2, 3, 4, 5, 8, 6, 7, 7, 8, 9, 10, 11, 12, 11, 13}
    self.m_PreMatchIsWinTable = {0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1}

    local finalTime = StarBiWuShow_Setting.GetData().FinalStageTime
    for i = 1, 3 do
        self.transform:Find("QnTabView/JingCai/Final/Title/"..i.."/Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("%d月%d日比赛"), finalTime[(i - 1) * 2], finalTime[(i - 1) * 2 + 1])
    end
end

function LuaStarBiwuJingCaiWnd:OnRankDataReady()
    if CRankData.Inst.MainPlayerRankInfo and CRankData.Inst.MainPlayerRankInfo.rankId == StarBiWuShow_Setting.GetData().JingCaiRankId then
        if CRankData.Inst.RankList and CRankData.Inst.RankList.Count > 0 then
            self.transform:Find("QnTabView/Rank/NoRpc").gameObject:SetActive(false)
            self.transform:Find("QnTabView/Rank/Normal").gameObject:SetActive(true)
            Extensions.RemoveAllChildren(self.m_RankContentRoot.transform)
            for i = 1,10 do
                local go = CUICommonDef.AddChild(self.m_RankContentRoot.gameObject, self.m_RankTemplate.gameObject)
                local list = CRankData.Inst.RankList
                if i <= list.Count then
                    self:InitRankNodeData(go,list[i-1])
                else
                    self:InitRankNodeData(go,nil)
                end
            end
            self.m_RankContentRoot:GetComponent(typeof(UITable)):Reposition()
        else
            self.transform:Find("QnTabView/Rank/NoRpc").gameObject:SetActive(true)
            self.transform:Find("QnTabView/Rank/Normal").gameObject:SetActive(false)
        end
    end
end

function LuaStarBiwuJingCaiWnd:InitRankNodeData(go,data)
    if not go then return end
    go.gameObject:SetActive(true)
    if not data then 
        go.transform:Find("ZanWuLabel").gameObject:SetActive(true)
        go.transform:Find("Content").gameObject:SetActive(false)
        return
    end
    go.transform:Find("ZanWuLabel").gameObject:SetActive(false)
    go.transform:Find("Content").gameObject:SetActive(true)
    local name = go.transform:Find("Content/Portrait/Name"):GetComponent(typeof(UILabel))
    local server = go.transform:Find("Content/Portrait/Server"):GetComponent(typeof(UILabel))
    local portrait = go.transform:Find("Content/Portrait"):GetComponent(typeof(CUITexture))
    local rankNum = go.transform:Find("Content/Rank"):GetComponent(typeof(UILabel))
    local rankSprite = go.transform:Find("Content/Rank/Rank01"):GetComponent(typeof(UISprite))
    local Num = go.transform:Find("Content/ResultLabel"):GetComponent(typeof(UILabel))
    local rankBg = go.transform:Find("Content/Rank/RankBg")
    rankNum.text = ""
    rankSprite.spriteName = ""
    if data.Rank == 1 then
        rankSprite.spriteName = "headinfownd_jiashang_01"
    elseif data.Rank == 2 then
        rankSprite.spriteName = "headinfownd_jiashang_02"
    elseif data.Rank == 3 then
        rankSprite.spriteName = "headinfownd_jiashang_03"
    else
        rankNum.text = data.Rank
    end
    rankBg.gameObject:SetActive(data.Rank ~= 1 and data.Rank ~= 2 and data.Rank ~= 3)
    name.text = data.Name
    Num.text = self:GetJingCaiRankValue(data.Value)
    server.text = data.serverName
    local job = data.Job
    local extraData = MsgPackImpl.unpack(data.extraData)
    local gender = CommonDefs.ConvertIntToEnum(typeof(EnumGender), extraData[0])
    if CClientMainPlayer.Inst and data.PlayerIndex == CClientMainPlayer.Inst.Id then
        name.color = NGUIText.ParseColor24("00ff60", 0)
    end
    portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(job, gender, -1), false)
    UIEventListener.Get(portrait.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        CPlayerInfoMgr.ShowPlayerPopupMenu(data.PlayerIndex, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, "", nil, Vector3.zero, CPlayerInfoMgrAlignType.Default)
    end)
    
end

function LuaStarBiwuJingCaiWnd:GetJingCaiRankValue(value)
    if value > 10000 then 
        local res =  math.floor(value / 10000)
        return SafeStringFormat3(LocalString.GetString("%d万"),tonumber(res))
    else
        return tostring(value)
    end
end

function LuaStarBiwuJingCaiWnd:GetJingCaiPhaseNumByJuesaiPlayIdx(juesaiPlayIdx)
    if not self.m_JueSaiJingCaiPhaseNumBegin then return 0 end
    local jingcaiPhaseNum = self.m_JueSaiJingCaiPhaseNumBegin + juesaiPlayIdx - 1
	return jingcaiPhaseNum
end

function LuaStarBiwuJingCaiWnd:UpdateQuestionViewInfo(questionList)
    self.m_QuestionList = questionList
    local selectIndex = 0
    local openNum = 0
    for k,v in pairs(questionList) do
        if v.IsOpen == 1 then
            if v.MyChoice == 0 then
                selectIndex = k
            end
            openNum = openNum + 1
        end
    end
    if selectIndex == 0 and openNum > 0 then selectIndex = 1 end
    if not self.m_QuestionTabList then 
        self:InitQuestionView() 
        self.m_ChooseTableView:ReloadData(false,false)
    else
        for k,v in pairs(self.m_QuestionTabList) do
            local data = questionList[k]
            if data then
                local questionStatus = 0
                if data.IsOpen == 1 then
                    questionStatus = data.MyChoice > 0 and 2 or 1
                end
                v.status = questionStatus
                self:InitTabItem(self.m_ChooseTableView:GetItemAtRow(k - 1),k - 1)
            end
        end
    end
    if self.m_CurQuestionInfo == nil then
        self.m_ChooseTableView:SetSelectRow(selectIndex - 1,true)
    else
        self:ShowQuestionView(self.m_CurQuestionIndex - 1)
    end
end

function LuaStarBiwuJingCaiWnd:InitQuestionView()
    self.m_QuestionViewAlert:SetActive(LuaActivityRedDotMgr:IsRedDot(90))
    self.m_ChooseTableView = self.transform:Find("QnTabView/Question/ChooseView"):GetComponent(typeof(QnTableView))

    
    self.m_CurQuestionInfo = nil
    self.m_CurQuestionIndex = 0
    self.transform:Find("QnTabView/Question/TopLabel"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("StarBiWu_JingCaiQuestionTitle")
    local tipBtn = self.transform:Find("QnTabView/Question/TopLabel/TipButton").gameObject
    UIEventListener.Get(tipBtn.gameObject).onClick = LuaUtils.VoidDelegate(function()
        g_MessageMgr:ShowMessage("StarBiWu_JingCaiQuestionTip")
    end)
    local shareBtn = self.transform:Find("QnTabView/Question/ShareButton").gameObject
    UIEventListener.Get(shareBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnShareBtnClick(go)
    end)
    if self.m_QuestionList then
        self.m_QuestionTabList = {}
        for k,v in pairs(self.m_QuestionList) do
            local id = v.QuestionId
            local data = StarBiWuShow_Question.GetData(id)
            local questionStatus = 0
            if v.IsOpen == 1 then
                questionStatus = v.MyChoice > 0 and 2 or 1
            end
            table.insert(self.m_QuestionTabList,{index = id,
                startTime = data.StartTime,
                tabShow = data.Tab,
                question = data.Question,
                analysis = data.Analysis,
                option = {data.option1,data.option2,data.option3,data.option4},
                status = questionStatus})
        end
    end

    self.m_ChooseTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_QuestionTabList
        end,
        function(item, index)
            self:InitTabItem(item, index)
        end
    )
    self.m_ChooseTableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        local data = self.m_QuestionTabList[row + 1]
        local curTab = self.m_ChooseTableView:GetItemAtRow(self.m_CurQuestionIndex - 1)
        if curTab then
            local mark = curTab.transform:Find("Content/Mark"):GetComponent(typeof(UITexture))
            local complate = curTab.transform:Find("Content/Complate"):GetComponent(typeof(UITexture))
            mark.color = NGUIText.ParseColor24("83C9FF", 0)
            complate.color = NGUIText.ParseColor24("83C9FF", 0)
            complate.alpha = 0.6
        end
        if data and data.status > 0 then
            self:ShowQuestionView(row)
        end
    end)
end

function LuaStarBiwuJingCaiWnd:InitTabItem(item,index)
    if not self.m_QuestionTabList then return end
    local data = self.m_QuestionTabList[index + 1]
    if not data then return end
    local content = item.transform:Find("Content")
    item.transform:Find("Content/Label"):GetComponent(typeof(UILabel)).text = data.tabShow
    local mark = item.transform:Find("Content/Mark").gameObject
    local lock = item.transform:Find("Content/Lock").gameObject
    local complate = item.transform:Find("Content/Complate").gameObject
    mark:SetActive(data.status == 2)
    lock:SetActive(data.status == 0)
    complate:SetActive(data.status == 1)
    item.transform:GetComponent(typeof(QnTableItem)).Enabled = data.status > 0
    Extensions.SetLocalPositionZ(content.transform, data.status > 0 and 0 or -1)
end

function LuaStarBiwuJingCaiWnd:ShowQuestionView(index)
    if not self.m_QuestionTabList then return end
    local data = self.m_QuestionTabList[index + 1]
    if not data then return end
    self.m_CurQuestionIndex = index + 1
    self.m_CurQuestionInfo = data
    local curTab = self.m_ChooseTableView:GetItemAtRow(self.m_CurQuestionIndex - 1)
    if curTab then
        local mark = curTab.transform:Find("Content/Mark"):GetComponent(typeof(UITexture))
        local complate = curTab.transform:Find("Content/Complate"):GetComponent(typeof(UITexture))
        mark.color = NGUIText.ParseColor24("1E2B62", 0)
        complate.color = NGUIText.ParseColor24("1E2B62", 0)
        complate.alpha = 0.6
    end
    local questionLabel = self.transform:Find("QnTabView/Question/QuestionLabel"):GetComponent(typeof(UILabel))
    local desLabel = self.transform:Find("QnTabView/Question/DescLabel"):GetComponent(typeof(UILabel))
    local chooseTable = self.transform:Find("QnTabView/Question/ChooseTable")
    local playerSelect = 0
    if self.m_QuestionList then
        playerSelect = tonumber(self.m_QuestionList[index + 1] and self.m_QuestionList[index + 1].MyChoice or 0)
    end
    questionLabel.gameObject:SetActive(true)
    questionLabel.text = data.question
    desLabel.gameObject:SetActive(true)
    desLabel.text = data.status <= 1 and "" or data.analysis
    chooseTable.gameObject:SetActive(true)
    local shareBtn = self.transform:Find("QnTabView/Question/ShareButton").gameObject
    shareBtn.gameObject:SetActive(true)
    local rightAnswer = tonumber(self.m_QuestionList and self.m_QuestionList[index + 1] and self.m_QuestionList[index + 1].RightAnswer or 0)
    for i = 1,4 do
        local chooseItem = chooseTable:Find(i)
        local chooseLabel = chooseItem:Find("ContentLabel"):GetComponent(typeof(UILabel))
        local chooseRight = chooseItem:Find("Right").gameObject
        local chooseWrong = chooseItem:Find("Wrong").gameObject
        local chooseHighLight = chooseItem:Find("HighLight").gameObject
        chooseLabel.text = data.option[i]
        
        chooseHighLight:SetActive(playerSelect == i)
        chooseRight:SetActive(data.status == 2 and rightAnswer == i)
        chooseWrong:SetActive(data.status == 2 and rightAnswer ~= playerSelect and playerSelect == i)
        UIEventListener.Get(chooseItem.gameObject).onClick = LuaUtils.VoidDelegate(function()
            if data.status == 1 then
                -- 答题
                Gac2Gas.RequestStarBiwuJingCaiWenDaSelect(data.index,i)
            end
        end)
    end
end

function LuaStarBiwuJingCaiWnd:OnShareBtnClick(btn)
    local popupList={}
    table.insert( popupList,PopupMenuItemData(LocalString.GetString("分享至世界"), DelegateFactory.Action_int(function(index) 
        self:ShareQuestion(EChatPanel.World)
    end),false, nil, EnumPopupMenuItemStyle.Default))
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst:IsInGuild() then
        table.insert( popupList,PopupMenuItemData(LocalString.GetString("分享至帮会"), DelegateFactory.Action_int(function(index) 
            self:ShareQuestion(EChatPanel.Guild)
        end),false, nil, EnumPopupMenuItemStyle.Default))
    end
    if CTeamMgr.Inst:TeamExists() then
        table.insert( popupList,PopupMenuItemData(LocalString.GetString("分享至队伍"), DelegateFactory.Action_int(function(index) 
            self:ShareQuestion(EChatPanel.Team)
        end),false, nil, EnumPopupMenuItemStyle.Default))
    end
    local array=Table2Array(popupList,MakeArrayClass(PopupMenuItemData))
    local side = CPopupMenuInfoMgr.AlignType.Right
    CPopupMenuInfoMgr.ShowPopupMenu(array,btn.transform, side, 1, nil, 600, true, 220)
end

function LuaStarBiwuJingCaiWnd:ShareQuestion(channel)
    if not self.m_CurQuestionInfo then return end
    local optionLabelArray = {"A.","B.","C.","D."}
    local optionText = ""
    for i = 1,#self.m_CurQuestionInfo.option do
        local optionLabel = optionLabelArray[i]
        local answer = self.m_CurQuestionInfo.option[i]
        optionLabel = optionLabel .. answer
        optionText = optionText .. ((i > 1) and " " or "") .. optionLabel
    end
    local questionText = self.m_CurQuestionInfo.question
    local link = g_MessageMgr:FormatMessage("StarBiWu_Share_Question", questionText, optionText)
    CChatHelper.SendMsgWithFilterOption(channel,link,0, true)
    CSocialWndMgr.ShowChatWnd()
end