local UILabel = import "UILabel"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CTeamGroupMgr = import "L10.Game.CTeamGroupMgr"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
CLuaTeamManagerWnd = class()

RegistClassMember(CLuaTeamManagerWnd,"LeaderLabel")
RegistClassMember(CLuaTeamManagerWnd,"ManagerLabel1")
RegistClassMember(CLuaTeamManagerWnd,"ManagerLabel2")
RegistClassMember(CLuaTeamManagerWnd,"ManagerLabel3")
RegistClassMember(CLuaTeamManagerWnd,"ManagerLabel4")
RegistClassMember(CLuaTeamManagerWnd,"ManagerLabel5")

function CLuaTeamManagerWnd:Awake()
	self.LeaderLabel = self.transform:Find("ContentContainer/LeaderContainer/NameLabel"):GetComponent(typeof(UILabel))
	self.ManagerLabel1 = self.transform:Find("ContentContainer/ManagerContainer1/NameLabel"):GetComponent(typeof(UILabel))
	self.ManagerLabel2 = self.transform:Find("ContentContainer/ManagerContainer2/NameLabel"):GetComponent(typeof(UILabel))
	self.ManagerLabel3 = self.transform:Find("ContentContainer/ManagerContainer3/NameLabel"):GetComponent(typeof(UILabel))
	self.ManagerLabel4 = self.transform:Find("ContentContainer/ManagerContainer4/NameLabel"):GetComponent(typeof(UILabel))
	self.ManagerLabel5 = self.transform:Find("ContentContainer/ManagerContainer5/NameLabel"):GetComponent(typeof(UILabel))
end

function CLuaTeamManagerWnd:OnEnable()
	g_ScriptEvent:AddListener("TeamGroupMemberInfoReceived", self, "show")

end
function CLuaTeamManagerWnd:OnDisable()
	g_ScriptEvent:RemoveListener("TeamGroupMemberInfoReceived", self, "show")
end
function CLuaTeamManagerWnd:Init()
	self:show()
end
function CLuaTeamManagerWnd:show()
	local memberList = CTeamGroupMgr.Instance:GetManagersList()
	local leader= memberList[0]
	self.LeaderLabel.text = leader.Name
	
	local ltable = {}
	ltable[1] = self.LeaderLabel
	ltable[2] = self.ManagerLabel1
	ltable[3] = self.ManagerLabel2
	ltable[4] = self.ManagerLabel3
	ltable[5] = self.ManagerLabel4
	ltable[6] = self.ManagerLabel5
	
	for k,v in pairs(ltable) do
		if memberList.Count >= k then
			v.text =  memberList[k-1].Name
			local onclick = function(go)
				local id = memberList[k-1].PlayerID
				CPlayerInfoMgr.ShowPlayerPopupMenu(id, EnumPlayerInfoContext.TeamGroup, EChatPanel.Undefined, nil, nil, go.transform.position)
			end
			CommonDefs.AddOnClickListener(v.cachedTransform.parent.gameObject,DelegateFactory.Action_GameObject(onclick),false)
		else
			local onclick = function(go)
			end
			CommonDefs.AddOnClickListener(v.cachedTransform.parent.gameObject,DelegateFactory.Action_GameObject(onclick),false)
			v.text =  LocalString.GetString("无")
		end
	end
end








