LuaGetNewPersonalSpaceBgWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGetNewPersonalSpaceBgWnd, "MsgLabel", "MsgLabel", UILabel)
RegistChildComponent(LuaGetNewPersonalSpaceBgWnd, "TestButton", "TestButton", GameObject)

--@endregion RegistChildComponent end

function LuaGetNewPersonalSpaceBgWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.TestButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTestButtonClick()
	end)


    --@endregion EventBind end
end

function LuaGetNewPersonalSpaceBgWnd:Init()
    local msg = ""
    if CLuaPersonalSpaceMgr.m_NewGetBgIds then
        for i = 0, CLuaPersonalSpaceMgr.m_NewGetBgIds.Count - 1 do
            local designData = PersonalSpace_Background.GetData(tonumber(CLuaPersonalSpaceMgr.m_NewGetBgIds[i]))
            if designData then
                msg = msg .. (i == 0 and designData.Name or (","..designData.Name))
            end
        end
    end
    self.MsgLabel.text = g_MessageMgr:FormatMessage("Get_New_PersonalSpace_Bg",msg)
end

--@region UIEvent

function LuaGetNewPersonalSpaceBgWnd:OnTestButtonClick()
    CUIManager.CloseUI(CLuaUIResources.GetNewPersonalSpaceBgWnd)
    CUIManager.ShowUI(CUIResources.PersonalSpaceWnd)
end


--@endregion UIEvent

