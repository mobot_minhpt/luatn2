local Extensions = import "Extensions"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local UIScrollView = import "UIScrollView"

LuaDuiDuiLeStartWnd = class()

RegistChildComponent(LuaDuiDuiLeStartWnd,"m_JoinBtn","JoinBtn", GameObject)
RegistChildComponent(LuaDuiDuiLeStartWnd,"m_TextTable","TextTable", UITable)
RegistChildComponent(LuaDuiDuiLeStartWnd,"m_TextTemplate","TextTemplate", GameObject)
RegistChildComponent(LuaDuiDuiLeStartWnd,"m_RewardNumLabel","RewardNumLabel", UILabel)
RegistChildComponent(LuaDuiDuiLeStartWnd,"m_ScrollView", "ScrollView", UIScrollView)

function LuaDuiDuiLeStartWnd:Init()
    UIEventListener.Get(self.m_JoinBtn).onClick = DelegateFactory.VoidDelegate(function()
        LuaYuanXiao2020Mgr:TakePartInDuiDuiLe()
    end)

    self:InitTextTable()
end

function LuaDuiDuiLeStartWnd:OnEnable()
    g_ScriptEvent:AddListener("YuanXiao2020_DuiDuiLeOpenSignUpWnd", self, "InitRewardNumLabel")
    Gac2Gas.DuiDuiLeOpenSignUpWnd()
end

function LuaDuiDuiLeStartWnd:OnDisable()
    g_ScriptEvent:RemoveListener("YuanXiao2020_DuiDuiLeOpenSignUpWnd", self, "InitRewardNumLabel")
end

function LuaDuiDuiLeStartWnd:InitRewardNumLabel()
    self.m_RewardNumLabel.text = LuaYuanXiao2020Mgr:GetRewardNum()
end

function LuaDuiDuiLeStartWnd:InitTextTable()
    self.m_TextTemplate:SetActive(false)
    Extensions.RemoveAllChildren(self.m_TextTable.transform)
    local msg = g_MessageMgr:FormatMessage("YuanXiao2020_DuiDuiLe_Introduction")
    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)
    if info == nil then
        return
    end
    for i = 0, info.paragraphs.Count - 1 do
        local paragraphGo = NGUITools.AddChild(self.m_TextTable.gameObject, self.m_TextTemplate)
        paragraphGo:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(paragraphGo, typeof(CTipParagraphItem))
                :Init(info.paragraphs[i], 4294967295)
    end
    self.m_ScrollView:ResetPosition()
    self.m_TextTable:Reposition()
end
