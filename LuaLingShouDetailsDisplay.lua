local LingShou_LingShou = import "L10.Game.LingShou_LingShou"
local EPlayerFightProp = import "L10.Game.EPlayerFightProp"
local CUITexture = import "L10.UI.CUITexture"
local CLingShouMgr = import "L10.Game.CLingShouMgr"
local CLingShouBaseMgr = import "L10.Game.CLingShouBaseMgr"


CLuaLingShouDetailsDisplay = class()
RegistClassMember(CLuaLingShouDetailsDisplay,"zhandouliLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"nameLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"titleLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"levelLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"bringLevelLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"corLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"lifeLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"StaLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"StrLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"IntLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"agiLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"pHitLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"mHitLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"fatalLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"attackLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"hpLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"pDefLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"mDefLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"pMissLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"mMissLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"corZiZhiLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"staZiZhiLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"strZiZhiLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"intZiZhiLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"agiZiZhiLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"chengZhangLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"xiuWeiLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"wuXingLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"pFatalLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"pFatalDamageLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"mFatalLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"mFatalDamageLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"IgnoreAntiFireLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"IgnoreAntiThunderLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"IgnoreAntiIceLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"IgnoreAntiPoisonLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"IgnoreAntiWindLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"IgnoreAntiLightLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"IgnoreAntiIllusionLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"IgnoreAntiWaterLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"expSlider")
RegistClassMember(CLuaLingShouDetailsDisplay,"expLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"shouMingSlider")
RegistClassMember(CLuaLingShouDetailsDisplay,"shouMingLabel")
RegistClassMember(CLuaLingShouDetailsDisplay,"zizhiStar")
RegistClassMember(CLuaLingShouDetailsDisplay,"zizhiStar2")
RegistClassMember(CLuaLingShouDetailsDisplay,"shenShouSprite")
RegistClassMember(CLuaLingShouDetailsDisplay,"genderSprite")
RegistClassMember(CLuaLingShouDetailsDisplay,"babyBtn")
RegistClassMember(CLuaLingShouDetailsDisplay,"mLingShouId")
RegistClassMember(CLuaLingShouDetailsDisplay,"mDetails")
RegistClassMember(CLuaLingShouDetailsDisplay,"IsMyLingShou")
RegistClassMember(CLuaLingShouDetailsDisplay,"m_LastZhandouli")
RegistClassMember(CLuaLingShouDetailsDisplay,"m_LastZhandouli2Id")
RegistClassMember(CLuaLingShouDetailsDisplay,"m_LastIsJieBan")
RegistClassMember(CLuaLingShouDetailsDisplay,"m_FxNode")

function CLuaLingShouDetailsDisplay:Awake()
    self.zhandouliLabel = self.transform:Find("ZhandouliLabel"):GetComponent(typeof(UILabel))
    self.nameLabel = self.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    self.titleLabel = self.transform:Find("BaseProperty/TitleLabel"):GetComponent(typeof(UILabel))
    self.levelLabel = self.transform:Find("NameLabel/level"):GetComponent(typeof(UILabel))
    self.bringLevelLabel = self.transform:Find("DetailProperty/Panel/Group1/BringLevel"):GetComponent(typeof(UILabel))
    self.m_QinmiLabel = self.transform:Find("BaseProperty/QinminLabel"):GetComponent(typeof(UILabel))
    self.corLabel = self.transform:Find("DetailProperty/Panel/Group2/CorLabel"):GetComponent(typeof(UILabel))
    self.lifeLabel = self.transform:Find(""):GetComponent(typeof(UILabel))
    self.StaLabel = self.transform:Find("DetailProperty/Panel/Group2/StaLabel"):GetComponent(typeof(UILabel))
    self.StrLabel = self.transform:Find("DetailProperty/Panel/Group2/StrLabel"):GetComponent(typeof(UILabel))
    self.IntLabel = self.transform:Find("DetailProperty/Panel/Group2/IntLabel"):GetComponent(typeof(UILabel))
    self.agiLabel = self.transform:Find("DetailProperty/Panel/Group2/AgiLabel"):GetComponent(typeof(UILabel))
    self.pHitLabel = self.transform:Find("DetailProperty/Panel/Group2/PHitLabel"):GetComponent(typeof(UILabel))
    self.mHitLabel = self.transform:Find("DetailProperty/Panel/Group2/MHitLabel"):GetComponent(typeof(UILabel))
    self.fatalLabel = self.transform:Find(""):GetComponent(typeof(UILabel))
    self.attackLabel = self.transform:Find("BaseProperty/AttackLabel"):GetComponent(typeof(UILabel))
    self.hpLabel = self.transform:Find("BaseProperty/HpLabel"):GetComponent(typeof(UILabel))
    self.pDefLabel = self.transform:Find("BaseProperty/pDefLabel"):GetComponent(typeof(UILabel))
    self.mDefLabel = self.transform:Find("BaseProperty/mDefLabel"):GetComponent(typeof(UILabel))
    self.pMissLabel = self.transform:Find("DetailProperty/Panel/Group2/pMissLabel"):GetComponent(typeof(UILabel))
    self.mMissLabel = self.transform:Find("DetailProperty/Panel/Group2/mMissLabel"):GetComponent(typeof(UILabel))
    self.corZiZhiLabel = self.transform:Find("DetailProperty/Panel/Group1/CorZiZhiLabel"):GetComponent(typeof(UILabel))
    self.staZiZhiLabel = self.transform:Find("DetailProperty/Panel/Group1/StaZiZhiLabel"):GetComponent(typeof(UILabel))
    self.strZiZhiLabel = self.transform:Find("DetailProperty/Panel/Group1/StrZiZhiLabel"):GetComponent(typeof(UILabel))
    self.intZiZhiLabel = self.transform:Find("DetailProperty/Panel/Group1/IntZiZhiLabel"):GetComponent(typeof(UILabel))
    self.agiZiZhiLabel = self.transform:Find("DetailProperty/Panel/Group1/AgiZiZhiLabel"):GetComponent(typeof(UILabel))
    self.chengZhangLabel = self.transform:Find("BaseProperty/ChengZhangLabel"):GetComponent(typeof(UILabel))
    self.xiuWeiLabel = self.transform:Find("BaseProperty/XiuWeiLabel"):GetComponent(typeof(UILabel))
    self.wuXingLabel = self.transform:Find("BaseProperty/WuXingLabel"):GetComponent(typeof(UILabel))
    self.pFatalLabel = self.transform:Find("DetailProperty/Panel/Group2/pFatal"):GetComponent(typeof(UILabel))
    self.pFatalDamageLabel = self.transform:Find("DetailProperty/Panel/Group2/pFatalDamage"):GetComponent(typeof(UILabel))
    self.mFatalLabel = self.transform:Find("DetailProperty/Panel/Group2/mFatal"):GetComponent(typeof(UILabel))
    self.mFatalDamageLabel = self.transform:Find("DetailProperty/Panel/Group2/mFatalDamage"):GetComponent(typeof(UILabel))
    self.IgnoreAntiFireLabel = self.transform:Find("DetailProperty/Panel/Group2/IgnoreAntiFire"):GetComponent(typeof(UILabel))
    self.IgnoreAntiThunderLabel = self.transform:Find("DetailProperty/Panel/Group2/IgnoreAntiThunder"):GetComponent(typeof(UILabel))
    self.IgnoreAntiIceLabel = self.transform:Find("DetailProperty/Panel/Group2/IgnoreAntiIce"):GetComponent(typeof(UILabel))
    self.IgnoreAntiPoisonLabel = self.transform:Find("DetailProperty/Panel/Group2/IgnoreAntiPoison"):GetComponent(typeof(UILabel))
    self.IgnoreAntiWindLabel = self.transform:Find("DetailProperty/Panel/Group2/IgnoreAntiWind"):GetComponent(typeof(UILabel))
    self.IgnoreAntiLightLabel = self.transform:Find("DetailProperty/Panel/Group2/IgnoreAntiLight"):GetComponent(typeof(UILabel))
    self.IgnoreAntiIllusionLabel = self.transform:Find("DetailProperty/Panel/Group2/IgnoreAntiIllusion"):GetComponent(typeof(UILabel))
    self.IgnoreAntiWaterLabel = self.transform:Find("DetailProperty/Panel/Group2/IgnoreAntiWater"):GetComponent(typeof(UILabel))
    self.expSlider = self.transform:Find("BaseProperty/ExpSlider"):GetComponent(typeof(UISlider))
    self.expLabel = self.transform:Find("BaseProperty/ExpSlider/ExpLabel"):GetComponent(typeof(UILabel))
    self.shouMingSlider = self.transform:Find("BaseProperty/ShouMingSlider"):GetComponent(typeof(UISlider))
    self.shouMingLabel = self.transform:Find("BaseProperty/ShouMingSlider/ShouMingLabel"):GetComponent(typeof(UILabel))
    self.zizhiStar = self.transform:Find("BaseProperty/StarLevels")
    self.zizhiStar2 = self.transform:Find("DetailProperty/Panel/Group1/StarLevels (1)")

    self.shenShouSprite = self.transform:Find("ShenShou"):GetComponent(typeof(UISprite))
    self.genderSprite = self.transform:Find("GenderSprite"):GetComponent(typeof(UISprite))
    self.babyBtn = self.transform:Find("BabyBtn").gameObject

    self.m_FxNode = self.transform:Find("ZhandouliLabel/FxNode"):GetComponent(typeof(CUIFx))

    self.mLingShouId = nil
    self.mDetails = nil
    self.IsMyLingShou = true
end

-- Auto Generated!!
function CLuaLingShouDetailsDisplay:Fill( details) 
    local notNull = details ~= nil
    self.mDetails = details
    local default =notNull and details.id or ""

    self.mLingShouId = default
    local pinzhi = self.transform:Find("Pinzhi"):GetComponent(typeof(UISprite))
    if pinzhi ~= nil then
        pinzhi.spriteName = notNull and CLuaLingShouMgr.GetLingShouQualitySprite(CLuaLingShouMgr.GetLingShouQuality(details.data.Props.Quality)) or nil
    end

    self:RefreshZhandouli()

    self.m_QinmiLabel.text = notNull and tostring(details.data.Props.PartnerInfo.Affinity / 10) or "0"

    if self.nameLabel ~= nil then
        local extern
        if notNull then
            extern = details.data.Name
        else
            extern = ""
        end
        self.nameLabel.text = LocalString.TranslateAndFormatText(extern)
    end

    if self.expSlider ~= nil then
        local requiredExp = notNull and CLingShouBaseMgr.GetLevelRequireExp(details.data.Level) or 0
        self.expSlider.value = notNull and (details.data.Exp / requiredExp) or 0
        local ref
        if notNull then
            ref = SafeStringFormat3("%s/%s", CUICommonDef.GetShortExpStr(details.data.Exp), CUICommonDef.GetShortExpStr(requiredExp))
        else
            ref = ""
        end
        self.expLabel.text = ref
    end

    local function initZiZhi(transform,starLevel)
        -- local starLevel = notNull and details.data.Props.ZizhiLevel or 0
        -- local zizhiStar = transform:Find("ZizhiStar")
        local sprites={}
        for i=1,5 do
            local sprite = transform:GetChild(i-1):GetComponent(typeof(UISprite))
            table.insert( sprites,sprite )
            sprite.spriteName = "common_star_70%"
        end
        local c1 = math.floor(starLevel/2)
        local c2 = starLevel%2
        for i=1,c1 do
            -- local sprite = zizhiStar:GetChild(i-1)
            sprites[i].spriteName = "common_star_3_1"
        end
        if c2==1 then
            sprites[c1+1].spriteName = "common_star_3_2"
        end
    end

    if self.zizhiStar ~= nil then
        initZiZhi(self.zizhiStar,notNull and details.data.Props.ZizhiLevel or 0)
    end
    if self.zizhiStar2 ~= nil then
        initZiZhi(self.zizhiStar2,notNull and details.data.Props.ZizhiLevel or 0)
    end

    if self.titleLabel ~= nil then
        local out
        if notNull then
            out = details.data.Name
        else
            out = ""
        end
        self.titleLabel.text = out

        if notNull then
            --string name = CLingShouBaseMgr.GetLingShouName(details.data.TemplateId);
            --string desc = CLingShouBaseMgr.GetLingShouTypeTempalteDesc(details.data.TemplateId);
            self.titleLabel.text = details.FullDesc
            -- string.Format("一只性格[ffba00]{0},{1}[-]的{2}",
            --EnumLingShouNatureName.GetName((EnumLingShouNature)details.data.Props.Nature), /*find.GetTypeDesc()*/desc, name);
        end
    end
    --if (descLabel!=null)
    --{
    --    //descLabel.text = notNull ? CLingShouBaseMgr.GetLingShouDesc(details.data.TemplateId) : "";
    --}

    local improve = ""
    if notNull then
        local add = NumberTruncate((details.fightProperty:GetParam(EPlayerFightProp.WuxingImprove) * 100), 2)
        if add > 0 then
            improve = System.String.Format("[ffed5f](+{0:f1}%)[-]", add)
        end
    end


    if self.levelLabel ~= nil then
        local try
        if notNull then
            try = "Lv." .. tostring(details.data.Level)
        else
            try = ""
        end
        self.levelLabel.text = try
    end

    if self.bringLevelLabel ~= nil then
        self.bringLevelLabel.text = notNull and tostring(CLingShouBaseMgr.GetLingShouBringLevel(details.data.TemplateId)) or ""
    end

    if self.corLabel ~= nil then
        self.corLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.Cor)))) or ""
    end

    if self.lifeLabel ~= nil then
        self.lifeLabel.text = notNull and tostring(details.data.Props.Shouming) or ""
    end

    if self.shenShouSprite ~= nil then
        self.shenShouSprite.enabled = false
    end

    if notNull then
        local data = LingShou_LingShou.GetData(details.data.TemplateId)
        if data ~= nil then
            if self.shouMingLabel ~= nil then
                self.shouMingLabel.text = (tostring(details.data.Props.Shouming) .. "/") .. tostring(data.LifeSpan)
            end
            if self.shouMingSlider ~= nil then
                self.shouMingSlider.value = details.data.Props.Shouming / data.LifeSpan
            end
            if CLuaLingShouMgr.IsShenShou(details.data) then
                if self.shenShouSprite ~= nil then
                    self.shenShouSprite.enabled = true
                end
            end
        end
    else
        if self.shouMingLabel ~= nil then
            self.shouMingLabel.text = ""
        end
        if self.shouMingSlider ~= nil then
            self.shouMingSlider.value = 0
        end
    end


    if self.StaLabel ~= nil then
        self.StaLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.Sta)))) or ""
    end

    if self.StrLabel ~= nil then
        self.StrLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.Str)))) or ""
    end

    if self.IntLabel ~= nil then
        self.IntLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.Int)))) or ""
    end

    if self.agiLabel ~= nil then
        self.agiLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.Agi)))) or ""
    end

    if self.pHitLabel ~= nil then
        self.pHitLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.pHit)))) or ""
    end
    --物理命中

    if self.mHitLabel ~= nil then
        self.mHitLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.mHit)))) or ""
    end
    --法术命中


    if self.fatalLabel ~= nil then
        self.fatalLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.pHit)))) or ""
    end
    --物理命中

    self:InitAttack(details)


    if self.hpLabel ~= nil then
        self.hpLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.HpFull)))) or ""
    end

    if self.pDefLabel ~= nil then
        self.pDefLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.pDef)))) or ""
    end
    if self.mDefLabel ~= nil then
        self.mDefLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.mDef)))) or ""
    end
    if self.pMissLabel ~= nil then
        self.pMissLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.pMiss)))) or ""
    end
    if self.mMissLabel ~= nil then
        self.mMissLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.mMiss)))) or ""
    end
    if self.corZiZhiLabel ~= nil then
        local case
        if notNull then
            case = tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.CorZizhi)))) .. improve
        else
            case = ""
        end
        self.corZiZhiLabel.text = case
    end
    if self.staZiZhiLabel ~= nil then
        local void
        if notNull then
            void = tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.StaZizhi)))) .. improve
        else
            void = ""
        end
        self.staZiZhiLabel.text = void
    end
    if self.strZiZhiLabel ~= nil then
        local byte
        if notNull then
            byte = tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.StrZizhi)))) .. improve
        else
            byte = ""
        end
        self.strZiZhiLabel.text = byte
    end
    if self.intZiZhiLabel ~= nil then
        local char
        if notNull then
            char = tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.IntZizhi)))) .. improve
        else
            char = ""
        end
        self.intZiZhiLabel.text = char
    end
    if self.agiZiZhiLabel ~= nil then
        local uint
        if notNull then
            uint = tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.AgiZizhi)))) .. improve
        else
            uint = ""
        end
        self.agiZiZhiLabel.text = uint
    end


    if self.pFatalLabel ~= nil then
        self.pFatalLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.pFatal)))) or ""
    end
    if self.pFatalDamageLabel ~= nil then
        local lock
        if notNull then
            lock = tostring(((NumberTruncate(details.fightProperty:GetParam(EPlayerFightProp.pFatalDamage), 3) * 100))) .. "%"
        else
            lock = ""
        end
        self.pFatalDamageLabel.text = lock
    end
    if self.mFatalLabel ~= nil then
        self.mFatalLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.mFatal)))) or ""
    end
    if self.mFatalDamageLabel ~= nil then
        local using
        if notNull then
            using = tostring(((NumberTruncate(details.fightProperty:GetParam(EPlayerFightProp.mFatalDamage), 3) * 100))) .. "%"
        else
            using = ""
        end
        self.mFatalDamageLabel.text = using
    end

    if CLingShouMgr.Inst:IsOnBattle(self.mLingShouId) then
        if self.IgnoreAntiFireLabel ~= nil then
            self.IgnoreAntiFireLabel.gameObject:SetActive(true)
            self.IgnoreAntiFireLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.IgnoreAntiFire)))) or ""
        end

        if self.IgnoreAntiThunderLabel ~= nil then
            self.IgnoreAntiThunderLabel.gameObject:SetActive(true)
            self.IgnoreAntiThunderLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.IgnoreAntiThunder)))) or ""
        end

        if self.IgnoreAntiIceLabel ~= nil then
            self.IgnoreAntiIceLabel.gameObject:SetActive(true)
            self.IgnoreAntiIceLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.IgnoreAntiIce)))) or ""
        end

        if self.IgnoreAntiPoisonLabel ~= nil then
            self.IgnoreAntiPoisonLabel.gameObject:SetActive(true)
            self.IgnoreAntiPoisonLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.IgnoreAntiPoison)))) or ""
        end

        if self.IgnoreAntiWindLabel ~= nil then
            self.IgnoreAntiWindLabel.gameObject:SetActive(true)
            self.IgnoreAntiWindLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.IgnoreAntiWind)))) or ""
        end

        if self.IgnoreAntiLightLabel ~= nil then
            self.IgnoreAntiLightLabel.gameObject:SetActive(true)
            self.IgnoreAntiLightLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.IgnoreAntiLight)))) or ""
        end

        if self.IgnoreAntiIllusionLabel ~= nil then
            self.IgnoreAntiIllusionLabel.gameObject:SetActive(true)
            self.IgnoreAntiIllusionLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.IgnoreAntiIllusion)))) or ""
        end

        if self.IgnoreAntiWaterLabel ~= nil then
            self.IgnoreAntiWaterLabel.gameObject:SetActive(true)
            self.IgnoreAntiWaterLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.IgnoreAntiWater)))) or ""
        end
    else
        if self.IgnoreAntiFireLabel ~= nil then
            self.IgnoreAntiFireLabel.gameObject:SetActive(false)
        end

        if self.IgnoreAntiThunderLabel ~= nil then
            self.IgnoreAntiThunderLabel.gameObject:SetActive(false)
        end

        if self.IgnoreAntiIceLabel ~= nil then
            self.IgnoreAntiIceLabel.gameObject:SetActive(false)
        end

        if self.IgnoreAntiPoisonLabel ~= nil then
            self.IgnoreAntiPoisonLabel.gameObject:SetActive(false)
        end

        if self.IgnoreAntiWindLabel ~= nil then
            self.IgnoreAntiWindLabel.gameObject:SetActive(false)
        end

        if self.IgnoreAntiLightLabel ~= nil then
            self.IgnoreAntiLightLabel.gameObject:SetActive(false)
        end

        if self.IgnoreAntiIllusionLabel ~= nil then
            self.IgnoreAntiIllusionLabel.gameObject:SetActive(false)
        end

        if self.IgnoreAntiWaterLabel ~= nil then
            self.IgnoreAntiWaterLabel.gameObject:SetActive(false)
        end
    end




    if self.chengZhangLabel ~= nil then
        local fixed
        if notNull then
            fixed = System.String.Format("{0}({1})", CUICommonDef.ConvertDouble2String(details.data.Props.Chengzhang, 3), CLingShouBaseMgr.GetGrowRemark(details.data.Props.Chengzhang))
        else
            fixed = ""
        end
        self.chengZhangLabel.text = fixed
    end
    if self.xiuWeiLabel ~= nil then
        local const
        if notNull then
            const = tostring(math.floor(details.data.Props.Xiuwei))
        else
            const = ""
        end
        self.xiuWeiLabel.text = const
    end
    if self.wuXingLabel ~= nil then
        local object
        if notNull then
            object = details.strWuxing
        else
            object = ""
        end
        self.wuXingLabel.text = object
    end
    --public QnProgressBar expBar;

    if self.genderSprite ~= nil then
        self:RefreshYinYang(details)
    end
    if self.babyBtn ~= nil then
        self:RefreshBabyBtn(details)
    end
end
function CLuaLingShouDetailsDisplay:RefreshZhandouli( )
    local notNull = self.mDetails ~= nil
    if self.zhandouliLabel ~= nil then
        
        local bindPartenerLingShouId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.BindedPartenerLingShouId or nil
        local qichangId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.JieBanQiChangId or 0
        local isJieBan = bindPartenerLingShouId == self.mLingShouId
        self.zhandouliLabel.text = notNull and tostring(isJieBan and CLuaLingShouMgr.GetLingShouPartnerZhandouli(self.mDetails.data,self.mDetails.fightProperty,qichangId) or CLuaLingShouMgr.GetLingShouZhandouli(self.mDetails.data)) or ""
        self.zhandouliLabel.transform:Find("Label"):GetComponent(typeof(UILabel)).text =isJieBan and LocalString.GetString("结伴战斗力") or LocalString.GetString("出战战斗力")

        local curZhandouli = self.zhandouliLabel.text
        if curZhandouli == "" then
            curZhandouli = 0
        end
        self.m_FxNode:DestroyFx()
        if self.m_LastZhandouli then
            if tonumber(curZhandouli) > tonumber(self.m_LastZhandouli) and self.mDetails.id == self.m_LastZhandouli2Id and self.m_LastIsJieBan == isJieBan then
                self.m_FxNode:LoadFx("Fx/UI/Prefab/UI_dagongshouce_kekaiqicishu.prefab")
            end
        end
        self.m_LastZhandouli = curZhandouli
        self.m_LastZhandouli2Id = notNull and self.mDetails.id
        self.m_LastIsJieBan = isJieBan
    end
end
function CLuaLingShouDetailsDisplay:OnClickBabyButton( go) 
    CLuaLingShouMgr.m_TempLingShouInfo = self.mDetails
    CLuaLingShouMgr.m_IsMyLingShou = self.IsMyLingShou
    if self.IsMyLingShou then
        CUIManager.ShowUI(CUIResources.LingShouBabyWnd)
    else
        CUIManager.ShowUI(CUIResources.LingShouOtherBabyWnd)
    end
end
function CLuaLingShouDetailsDisplay:InitAttack( details) 
    local notNull = details ~= nil
    if self.attackLabel ~= nil then
        if notNull then
            local default = details.data.Props.Type--CommonDefs.ConvertIntToEnum(typeof(EnumLingShouType), details.data.Props.Type)
            if default == LuaEnumLingShouType.PAttack or default == LuaEnumLingShouType.PCorDefend or default == LuaEnumLingShouType.PAType then
                self.attackLabel.text = SafeStringFormat3("%d-%d", math.floor(details.fightProperty:GetParam(EPlayerFightProp.pAttMin)), math.floor(details.fightProperty:GetParam(EPlayerFightProp.pAttMax)))
                -- break
            elseif default == LuaEnumLingShouType.MAttack or default == LuaEnumLingShouType.MCorDefend or default == LuaEnumLingShouType.MAType then
                self.attackLabel.text = SafeStringFormat3("%d-%d", math.floor(details.fightProperty:GetParam(EPlayerFightProp.mAttMin)), math.floor(details.fightProperty:GetParam(EPlayerFightProp.mAttMax)))
                -- break
            end
        else
            self.attackLabel.text = ""
        end
    end
end
function CLuaLingShouDetailsDisplay:OnEnable( )
    self.m_FxNode:DestroyFx()
    self.m_LastZhandouli = nil
    g_ScriptEvent:AddListener("UpdateLingShouOverview", self, "UpdateLingShouOverview")
    g_ScriptEvent:AddListener("UpdateLingShouExp", self, "UpdateLingShouExp")
    g_ScriptEvent:AddListener("UpdateLingShouBabyInfo", self, "OnUpdateLingShouBabyInfo")
    g_ScriptEvent:AddListener("UpdateLingShouMarryInfo", self, "OnUpdateLingShouMarryInfo")
    g_ScriptEvent:AddListener("UpdateLingShouExItemUsedTime", self, "OnUpdateLingShouExItemUsedTime")
    -- EventManager.AddListenerInternal(EnumEventType.UpdateLingShouOverview, < MakeDelegateFromCSFunction >(self.UpdateLingShouOverview, MakeGenericClass(Action1, String), self))
    -- EventManager.AddListenerInternal(EnumEventType.UpdateLingShouExp, < MakeDelegateFromCSFunction >(self.UpdateLingShouExp, MakeGenericClass(Action1, String), self))
    -- EventManager.AddListenerInternal(EnumEventType.UpdateLingShouBabyInfo, < MakeDelegateFromCSFunction >(self.OnUpdateLingShouBabyInfo, MakeGenericClass(Action1, String), self))
    -- EventManager.AddListenerInternal(EnumEventType.UpdateLingShouMarryInfo, < MakeDelegateFromCSFunction >(self.OnUpdateLingShouMarryInfo, MakeGenericClass(Action1, String), self))
end
function CLuaLingShouDetailsDisplay:OnDisable( )
    g_ScriptEvent:RemoveListener("UpdateLingShouOverview", self, "UpdateLingShouOverview")
    g_ScriptEvent:RemoveListener("UpdateLingShouExp", self, "UpdateLingShouExp")
    g_ScriptEvent:RemoveListener("UpdateLingShouBabyInfo", self, "OnUpdateLingShouBabyInfo")
    g_ScriptEvent:RemoveListener("UpdateLingShouMarryInfo", self, "OnUpdateLingShouMarryInfo")
    g_ScriptEvent:RemoveListener("UpdateLingShouExItemUsedTime", self, "OnUpdateLingShouExItemUsedTime")
    -- EventManager.RemoveListenerInternal(EnumEventType.UpdateLingShouOverview, < MakeDelegateFromCSFunction >(self.UpdateLingShouOverview, MakeGenericClass(Action1, String), self))
    -- EventManager.RemoveListenerInternal(EnumEventType.UpdateLingShouExp, < MakeDelegateFromCSFunction >(self.UpdateLingShouExp, MakeGenericClass(Action1, String), self))
    -- EventManager.RemoveListenerInternal(EnumEventType.UpdateLingShouBabyInfo, < MakeDelegateFromCSFunction >(self.OnUpdateLingShouBabyInfo, MakeGenericClass(Action1, String), self))
    -- EventManager.RemoveListenerInternal(EnumEventType.UpdateLingShouMarryInfo, < MakeDelegateFromCSFunction >(self.OnUpdateLingShouMarryInfo, MakeGenericClass(Action1, String), self))
end
function CLuaLingShouDetailsDisplay:OnUpdateLingShouMarryInfo( args ) 
    local lingshouId=args[0]
    if self.mLingShouId == lingshouId then
        local details = CLingShouMgr.Inst:GetLingShouDetails(lingshouId)
        if details ~= nil then
            self:RefreshYinYang(details)
        end
    end
end
function CLuaLingShouDetailsDisplay:RefreshYinYang( details) 
    if self.genderSprite == nil then
        return
    end

    local show = false
    if details ~= nil then
        local marryInfo = details.data.Props.MarryInfo
        local gender = details.data.Props.MarryInfo.Gender
        if marryInfo.AdultStartTime > 0 then
            if gender == 0 then
                show = false
            else
                if gender == 1 then
                    self.genderSprite.spriteName = "lingshoujiehun_yang"--CLingShouDetailsDisplay.YangSpriteName
                elseif gender == 2 then
                    self.genderSprite.spriteName = "lingshoujiehun_yin"--CLingShouDetailsDisplay.YingSpriteName
                elseif gender == 3 then
                    self.genderSprite.spriteName = "lingshoujiehun_yinyang"--CLingShouDetailsDisplay.YinYangSpriteName
                end
                show = true
            end
        else
            show = false
        end
    end
    if show then
        self.genderSprite.gameObject:SetActive(true)
        UIEventListener.Get(self.genderSprite.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            CLuaLingShouMgr.m_TempLingShouInfo = self.mDetails
            CLuaLingShouMgr.m_IsMyLingShou = self.IsMyLingShou
            CUIManager.ShowUI(CUIResources.LingShouMarryInfoWnd)
        end)
    else
        self.genderSprite.gameObject:SetActive(false)
        UIEventListener.Get(self.genderSprite.gameObject).onClick = nil
    end
end
function CLuaLingShouDetailsDisplay:OnUpdateLingShouBabyInfo( args ) 
    local lingshouId = args[0]
    if self.mLingShouId == lingshouId then
        local details = CLingShouMgr.Inst:GetLingShouDetails(lingshouId)
        if details ~= nil then
            self.mDetails = details
            self:RefreshZhandouli()
            self:RefreshBabyBtn(details)
            --如果宝宝等级或者资质变化，则再次请求一次灵兽数据
            if details.dirty then
                CLingShouMgr.Inst:RequestLingShouDetails(self.mLingShouId)
            end
        end
    end
end
function CLuaLingShouDetailsDisplay:RefreshBabyBtn( details) 
    if self.babyBtn ~= nil then
        UIEventListener.Get(self.babyBtn).onClick = nil
        local headIcon = CommonDefs.GetComponent_Component_Type(self.babyBtn.transform:Find("Icon"), typeof(CUITexture))
        headIcon:Clear()
        if details ~= nil then
            local babyInfo = details.data.Props.Baby
            if babyInfo.BornTime > 0 then
                self.babyBtn:SetActive(true)
                local babyData = LingShouBaby_BabyTemplate.GetData(babyInfo.TemplateId)
                if babyData ~= nil then
                    headIcon:LoadNPCPortrait(babyData.Portrait, false)
                else
                    headIcon:Clear()
                end
                UIEventListener.Get(self.babyBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnClickBabyButton(go) end)
            else
                self.babyBtn:SetActive(false)
            end
        else
            self.babyBtn:SetActive(false)
        end
    end
end
function CLuaLingShouDetailsDisplay:UpdateLingShouOverview( args ) 
    local lingshouId = args[0]
    if self.mLingShouId == lingshouId then
        local data = CLingShouMgr.Inst:GetLingShouOverview(lingshouId)
        if data ~= nil then
            self.levelLabel.text = "Lv." .. tostring(data.level)
            self.nameLabel.text = LocalString.TranslateAndFormatText(data.name)
            self:UpdateLingShouExp(lingshouId)
        end
    end
end
function CLuaLingShouDetailsDisplay:UpdateLingShouExp( args ) 
    local lingshouId = args[0]
    if self.mLingShouId == lingshouId then
        local details = CLingShouMgr.Inst:GetLingShouDetails(lingshouId)
        if details ~= nil then
            if self.expSlider ~= nil then
                local requiredExp = CLingShouBaseMgr.GetLevelRequireExp(details.data.Level)
                self.expSlider.value = (details.data.Exp / requiredExp)
                self.expLabel.text = SafeStringFormat3("%s/%s", CUICommonDef.GetShortExpStr(details.data.Exp), CUICommonDef.GetShortExpStr(requiredExp))
            end
        end
    end
end

function CLuaLingShouDetailsDisplay:OnUpdateLingShouExItemUsedTime(playerId, lingshouId, exChengzhang, exProps)
    --print("CLuaLingShouDetailsDisplay OnUpdateLingShouExItemUsedTime",playerId, lingshouId, exChengzhang, exProps)
    if self.mLingShouId == lingshouId then
        Gac2Gas.RequestLingShouDetails(lingshouId, 0)
    end
end

