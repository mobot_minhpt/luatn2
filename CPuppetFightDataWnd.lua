-- Auto Generated!!
local CFightStatic = import "L10.Game.CFightStatic"
local CHousePuppetMgr = import "L10.Game.CHousePuppetMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPuppetFightDataWnd = import "L10.UI.CPuppetFightDataWnd"
local CUICommonDef = import "L10.UI.CUICommonDef"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local SkillData = import "L10.UI.CPuppetFightDataWnd+SkillData"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CPuppetFightDataWnd.m_OnEnable_CS2LuaHook = function (this)
    EventManager.AddListenerInternal(EnumEventType.HouseQueryPuppetFightInfo, MakeDelegateFromCSFunction(this.ReplyTeamFightData, MakeGenericClass(Action1, CFightStatic), this))
    UIEventListener.Get(this.m_CloseBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_CloseBtn).onClick, MakeDelegateFromCSFunction(this.CloseWnd, VoidDelegate, this), true)
    UIEventListener.Get(this.m_TipBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_TipBtn).onClick, MakeDelegateFromCSFunction(this.ShowTip, VoidDelegate, this), true)
    this.m_PlayerItemObj:SetActive(false)
end
CPuppetFightDataWnd.m_ReplyTeamFightData_CS2LuaHook = function (this, data)
    if data.Damage.Count > 0 or data.PetDamage.Count > 0 then
        this.m_TotalNode:SetActive(true)
    end

    CHousePuppetMgr.Inst.SaveFightStatic = data

    local dif = data.CPDifficulty
    if dif >= 0 and dif <= 4 then
        this.m_DiffNode:SetActive(true)
        this.m_DiffLabel.text = CPuppetFightDataWnd.DiffName[dif]
    else
        this.m_DiffNode:SetActive(false)
    end

    Extensions.RemoveAllChildren(this.m_Table.transform)
    local dataDic = data.Damage
    local outputInfo = data.OutputInfo

    local skillList = CreateFromClass(MakeGenericClass(List, SkillData))
    local totalDamage = 0
    CommonDefs.DictIterate(dataDic, DelegateFactory.Action_object_object(function (___key, ___value)
        local p = {}
        p.Key = ___key
        p.Value = ___value
        if p.Value > 0 then
            totalDamage = totalDamage + p.Value
        end

        if p.Key > 1 then
            local skillInfo = Skill_AllSkills.GetData(p.Key * 100 + 1)
            local name = skillInfo.Name
            local d = CreateFromClass(SkillData)
            d.damage = p.Value
            d.name = name
            d.skillId = p.Key
            CommonDefs.ListAdd(skillList, typeof(SkillData), d)
        end
    end))

    CommonDefs.ListSort1(skillList, typeof(SkillData), DelegateFactory.Comparison_SkillData(function (o1, o2)
        return (math.floor(o2.damage) - math.floor(o1.damage))
    end))

    --if(dataDic.ContainsKey(1))
    --{
    --    SkillData d = new SkillData();
    --    d.damage = dataDic[1];
    --    d.name = PetName;
    --    d.skillId = 1;
    --    skillList.Add(d);
    --}

    if CommonDefs.DictContains(dataDic, typeof(UInt32), 0) then
        local d = CreateFromClass(SkillData)
        d.damage = CommonDefs.DictGetValue(dataDic, typeof(UInt32), 0)
        d.name = CPuppetFightDataWnd.OtherName
        d.skillId = 0
        CommonDefs.ListAdd(skillList, typeof(SkillData), d)
    end

    local petDataDic = data.PetDamage
    CommonDefs.DictIterate(petDataDic, DelegateFactory.Action_object_object(function (___key, ___value)
        local p = {}
        p.Key = ___key
        p.Value = ___value
        if p.Value > 0 then
            totalDamage = totalDamage + p.Value
        end
    end))

    do
        local i = 0 local cnt = skillList.Count
        while i < cnt do
            local sData = skillList[i]
            if sData.damage > 0 then
                local obj = NGUITools.AddChild(this.m_Table.gameObject, this.m_PlayerItemObj)
                obj:SetActive(true)

                local num1 = obj.transform:Find("text1").gameObject
                local num2 = obj.transform:Find("text2").gameObject
                local num3 = obj.transform:Find("text3").gameObject
                local num4 = obj.transform:Find("text4").gameObject
                local num5 = obj.transform:Find("text5").gameObject
                local num6 = obj.transform:Find("text6").gameObject

                if CommonDefs.DictContains(outputInfo, typeof(UInt32), sData.skillId) then
                    local otherInfo = CommonDefs.DictGetValue(outputInfo, typeof(UInt32), sData.skillId)

                    CommonDefs.GetComponent_GameObject_Type(num1, typeof(UILabel)).text = sData.name
                    if otherInfo.AttackTimes > 0 then
                        CommonDefs.GetComponent_GameObject_Type(num2, typeof(UILabel)).text = tostring(round2(otherInfo.HitTimes * 100 / otherInfo.AttackTimes, 1)) .. "%"
                        if otherInfo.HitTimes > 0 then
                          CommonDefs.GetComponent_GameObject_Type(num3, typeof(UILabel)).text = tostring(round2(otherInfo.FatalTimes * 100 / otherInfo.HitTimes, 1)) .. "%"
                        else
                          CommonDefs.GetComponent_GameObject_Type(num3, typeof(UILabel)).text = "0%"
                        end
                    else
                        CommonDefs.GetComponent_GameObject_Type(num2, typeof(UILabel)).text = "0%"
                        CommonDefs.GetComponent_GameObject_Type(num3, typeof(UILabel)).text = "0%"
                    end

                    CommonDefs.GetComponent_GameObject_Type(num4, typeof(UILabel)).text = tostring(otherInfo.FatalTimes)
                    CommonDefs.GetComponent_GameObject_Type(num5, typeof(UILabel)).text = tostring((math.floor(sData.damage)))
                    if totalDamage <= 0 then
                        CommonDefs.GetComponent_GameObject_Type(num6, typeof(UILabel)).text = ""
                    else
                        CommonDefs.GetComponent_GameObject_Type(num6, typeof(UILabel)).text = tostring(round2(sData.damage * 100 / totalDamage, 1)) .. "%"
                    end
                else
                    CommonDefs.GetComponent_GameObject_Type(num1, typeof(UILabel)).text = sData.name
                    CommonDefs.GetComponent_GameObject_Type(num2, typeof(UILabel)).text = ""
                    CommonDefs.GetComponent_GameObject_Type(num3, typeof(UILabel)).text = ""
                    CommonDefs.GetComponent_GameObject_Type(num4, typeof(UILabel)).text = ""
                    CommonDefs.GetComponent_GameObject_Type(num5, typeof(UILabel)).text = tostring((math.floor(sData.damage)))
                    if totalDamage <= 0 then
                        CommonDefs.GetComponent_GameObject_Type(num6, typeof(UILabel)).text = ""
                    else
                        CommonDefs.GetComponent_GameObject_Type(num6, typeof(UILabel)).text = tostring(round2(sData.damage * 100 / totalDamage, 1)) .. "%"
                    end
                end
            end
            i = i + 1
        end
    end
    this.m_Table:Reposition()
    this.m_ScrollView:ResetPosition()

    local portrait = CUICommonDef.GetPortraitName(data.AttackerClass, data.AttackerGender, -1)
    this.m_PlayerIcon:LoadNPCPortrait(portrait, false)
    this.m_PlayerName.text = data.AttackerName
    this.m_TotalDamage.text = tostring((math.floor(totalDamage)))
    this.m_TotalTime.text = tostring((math.floor(data.Duration))) .. LocalString.GetString("秒")
    --m_PlayerLevel.text = data.AttackerClass
    if data.Duration > 0 then
        this.m_Dps.text = tostring((math.floor((totalDamage / data.Duration))))
    else
        this.m_Dps.text = ""
    end

    CHousePuppetMgr.Inst.SaveFightStatic = data
    UIEventListener.Get(this.m_CheckOtherInfoBtn).onClick = MakeDelegateFromCSFunction(this.CheckOtherInfo, VoidDelegate, this)
end
