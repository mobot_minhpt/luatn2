local CScene = import "L10.Game.CScene"

LuaQingMing2021Mgr = {}

LuaQingMing2021Mgr.IsInPlay = function()
	if CScene.MainScene then
		local gamePlayId = CScene.MainScene.GamePlayDesignId
		if gamePlayId == QingMing2021_Setting.GetData().GamePlayId then
			return true
		end
	end
	return false
end
-------------------------------
function Gas2Gac.SyncQingMing2021TuJianInfo(info)

  local gridTable = {}
  local rewardTable = {}

  for i=1,16 do
    local info = bit.band(info, bit.lshift(1, i-1))
    if info >= 1 then
      table.insert(gridTable,1)
    else
      table.insert(gridTable,0)
    end
  end
  for i=17,27 do
    local info = bit.band(info, bit.lshift(1, i-1))
    if info >= 1 then
      table.insert(rewardTable,1)
    else
      table.insert(rewardTable,0)
    end
  end
  for i,v in ipairs(rewardTable) do
  end
  LuaQingMing2021Mgr.CGYJData = {
    gridTable,rewardTable
  }
  if CUIManager.IsLoaded(CLuaUIResources.QingMingCGYJMainWnd) then
    g_ScriptEvent:BroadcastInLua("UpdateQingMingCGYJInfo")
  else
    CUIManager.ShowUI(CLuaUIResources.QingMingCGYJMainWnd)
  end
end

function Gas2Gac.ShowQingMing2021PlaySuccessWnd(leftTime, awardItemId)
  LuaQingMing2021Mgr.GameResult = {leftTime, awardItemId}
  CUIManager.ShowUI(CLuaUIResources.QingMingNHTQResultWnd)
end

function Gas2Gac.SyncQingMing2021PlayTime(playStatus, leftTime, awardLv, showLeftTime, result)
  LuaQingMing2021Mgr.GameStatus = {playStatus, leftTime, awardLv, showLeftTime, result}
  g_ScriptEvent:BroadcastInLua("UpdateQingMingCGYJTopInfo")
end

function Gas2Gac.SyncQingMing2021PlayMonsterCount(monsterCount_U)
  local tbl = MsgPackImpl.unpack(monsterCount_U)
  if tbl and tbl.Count > 0 then
    LuaQingMing2021Mgr.MonsterStatus = {}
    for i=0, tbl.Count-1 do
      local monsterId,monsterNum,monsterMaxNum = tbl[i][0], tbl[i][2] - tbl[i][1], tbl[i][2]
      table.insert(LuaQingMing2021Mgr.MonsterStatus,{monsterId,monsterNum,monsterMaxNum})
    end
    g_ScriptEvent:BroadcastInLua("UpdateQingMingCGYJMonsterInfo")
  end
end
