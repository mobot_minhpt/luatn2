-- Auto Generated!!
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CExpressionAppearanceMgr = import "L10.Game.CExpressionAppearanceMgr"
local CExpressionEquipSelectLoader = import "L10.UI.CExpressionEquipSelectLoader"
local Expression_Define = import "L10.Game.Expression_Define"
local Time = import "UnityEngine.Time"
local Vector3 = import "UnityEngine.Vector3"
local CEffectMgr = import "L10.Game.CEffectMgr"
CExpressionEquipSelectLoader.m_Init_CS2LuaHook = function (this) 
    this.deltaTime = Time.deltaTime / 2
    this.identifier = System.String.Format("__{0}__", this:GetInstanceID())
    this.rotation = 180
    this:LoadModel()
end
CExpressionEquipSelectLoader.m_Load_CS2LuaHook = function (this, renderObj) 
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer ~= nil then
        local expression = Expression_Define.GetData(CExpressionAppearanceMgr.AppearancePreviewID)
        if expression ~= nil then
            local fakeAppearance = mainPlayer.AppearanceProp
            CClientMainPlayer.LoadResource(renderObj, fakeAppearance, true, 1, 0, false, 0, false, expression.ID, false, nil, false)
        end
    end
end
