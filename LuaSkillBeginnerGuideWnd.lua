local UIGrid = import "UIGrid"

local GameObject = import "UnityEngine.GameObject"
local EnumYingLingState = import "L10.Game.EnumYingLingState"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CSkillButtonInfo = import "L10.UI.CSkillButtonInfo"
local Vector2 = import "UnityEngine.Vector2"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CUIFx = import "L10.UI.CUIFx"
local PlayerSettings = import "L10.Game.PlayerSettings"
local Animation = import "UnityEngine.Animation"
local CSkillMgr = import "L10.Game.CSkillMgr"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"

LuaSkillBeginnerGuideWnd = class()
LuaSkillBeginnerGuideWnd.castSkillSequenceId = nil
LuaSkillBeginnerGuideWnd.startSkillTraining = false
--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSkillBeginnerGuideWnd, "Button1", "Button1", GameObject)
RegistChildComponent(LuaSkillBeginnerGuideWnd, "Button2", "Button2", GameObject)
RegistChildComponent(LuaSkillBeginnerGuideWnd, "Button3", "Button3", GameObject)
RegistChildComponent(LuaSkillBeginnerGuideWnd, "Button4", "Button4", GameObject)
RegistChildComponent(LuaSkillBeginnerGuideWnd, "Button5", "Button5", GameObject)
RegistChildComponent(LuaSkillBeginnerGuideWnd, "Button6", "Button6", GameObject)
RegistChildComponent(LuaSkillBeginnerGuideWnd, "Button7", "Button7", GameObject)
RegistChildComponent(LuaSkillBeginnerGuideWnd, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaSkillBeginnerGuideWnd, "SkillSequenceItem", "SkillSequenceItem", GameObject)
RegistChildComponent(LuaSkillBeginnerGuideWnd, "YingLingSkill", "YingLingSkill", GameObject)
RegistChildComponent(LuaSkillBeginnerGuideWnd, "LeftButtonPos", "LeftButtonPos", GameObject)
RegistChildComponent(LuaSkillBeginnerGuideWnd, "RightButtonPos", "RightButtonPos", GameObject)
RegistChildComponent(LuaSkillBeginnerGuideWnd, "ButtonFemale", "ButtonFemale", GameObject)
RegistChildComponent(LuaSkillBeginnerGuideWnd, "ButtonMale", "ButtonMale", GameObject)
RegistChildComponent(LuaSkillBeginnerGuideWnd, "ButtonMonster", "ButtonMonster", GameObject)
RegistChildComponent(LuaSkillBeginnerGuideWnd, "Bg", "Bg", GameObject)

--@endregion RegistChildComponent end

function LuaSkillBeginnerGuideWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    local startSkillTraining = LuaSkillBeginnerGuideWnd.startSkillTraining
    self:InitWndData()
    self:RefreshConstUI()
    self:InitUIEvent()
    self:RecoverSequenceList(false, startSkillTraining)
end

function LuaSkillBeginnerGuideWnd:Init()

end

--@region UIEvent

--@endregion UIEvent

function LuaSkillBeginnerGuideWnd:InitWndData()
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer == nil then
        return
    end
    
    local skillProperty = mainPlayer.SkillProp
    local skillTbl = {}
    skillProperty:ForeachOriginalSkillId(DelegateFactory.Action_KeyValuePair_uint_uint(function (kv)
        local skillId = kv.Key
        local skillCls = math.floor(skillId / 100)
        skillTbl[skillCls] = skillId
    end))
    
    local cfg = BeginnerGuide_CastSkillGuide.GetData(LuaSkillBeginnerGuideWnd.castSkillSequenceId)
    self.skillPositionDict = {}
    local skillPositionRawData = cfg.skillPositionList
    local skillPositionRawList = g_LuaUtil:StrSplit(skillPositionRawData, ";")
    local tempReversePositionDict = {}
    for i = 1, #skillPositionRawList do
        if skillPositionRawList[i] ~= "" then
            local skillPositionRaw = g_LuaUtil:StrSplit(skillPositionRawList[i], ":")
            local positionId = tonumber(skillPositionRaw[1])
            local skillId = tonumber(skillPositionRaw[2])
            local castSkillId = skillTbl[skillId] and skillTbl[skillId] or (skillId * 100 + 1)
            self.skillPositionDict[positionId] = castSkillId
            tempReversePositionDict[castSkillId] = positionId
        end
    end

    self.castSkillSequence = {}
    self.castSkillObjectSequence = {}
    self.castSkillSequence2SkillPosition = {}
    local castSkillSequenceRawData = cfg.CastSkillSequence
    local castSkillSequenceRawList = g_LuaUtil:StrSplit(castSkillSequenceRawData, ";")
    for i = 1, #castSkillSequenceRawList do
        if castSkillSequenceRawList[i] ~= "" then
            local skillId = tonumber(castSkillSequenceRawList[i])
            local castSkillId = skillTbl[skillId] and skillTbl[skillId] or (skillId * 100 + 1)

            table.insert(self.castSkillSequence, castSkillId)

            self.castSkillSequence2SkillPosition[i] = tempReversePositionDict[castSkillId]
        end
    end

    if not System.String.IsNullOrEmpty(cfg.CastSkillSequence2Position) then
        self.castSkillSequence2SkillPosition = {}
        local castSkillSequence2PositionRawData = cfg.CastSkillSequence2Position
        local castSkillSequence2PositionRawList = g_LuaUtil:StrSplit(castSkillSequence2PositionRawData, ";")
        for i = 1, #castSkillSequence2PositionRawList do
            if castSkillSequence2PositionRawList[i] ~= "" then
                table.insert(self.castSkillSequence2SkillPosition, tonumber(castSkillSequence2PositionRawList[i]))
            end
        end
    end
    
    self.replacementSkillCls = {}
    local replacementClsRawData = cfg.skillReplacement
    local replacementClsRawList = g_LuaUtil:StrSplit(replacementClsRawData, ";")
    for i = 1, #replacementClsRawList do
        if replacementClsRawList[i] ~= "" then
            local skillList = g_LuaUtil:StrSplit(replacementClsRawList[i], ":")
            for j = 1, #skillList do
                local skillCls = tonumber(skillList[j])
                if self.replacementSkillCls[skillCls] == nil then
                    self.replacementSkillCls[skillCls] = {}
                end
                for k = 1, #skillList do
                    if k ~= j then
                        self.replacementSkillCls[skillCls][ tonumber(skillList[k]) ] = true
                    end
                end
            end
        end
    end
    
    self.ignoreSkill = {}
    local ignoreClsRawData = cfg.ignoreSkill
    local ignoreClsRawList = g_LuaUtil:StrSplit(ignoreClsRawData, ";")
    for i = 1, #ignoreClsRawList do
        if ignoreClsRawList[i] ~= "" then
            self.ignoreSkill[ tonumber(ignoreClsRawList[i]) ] = true
        end
    end
    
    self.skillButtonList = {}
    for i = 1, 7 do
        table.insert(self.skillButtonList, self["Button" .. i])
    end
    
    self.checkSequenceId = 1
    self.checkSequenceLength = #self.castSkillSequence
    self.checkSequenceRecordTime = 0
    self.transform:Find("Anchor/SkillSequenceTable").gameObject:SetActive(LuaSkillBeginnerGuideWnd.startSkillTraining)
    LuaSkillBeginnerGuideWnd.startSkillTraining = false

    self.m_NeedUpdateCooldown = false
    
    self.colorSystemSkills = {}
    local colorSystemRawData = BeginnerGuide_Setting.GetData().CastSkillGuideColorSkill
    local colorSystemRawList = g_LuaUtil:StrSplit(colorSystemRawData, ";")
    for i = 1, #colorSystemRawList do
        if colorSystemRawList[i] ~= "" then
            local colorData = g_LuaUtil:StrSplit(colorSystemRawList[i], ",")
            self.colorSystemSkills[tonumber(colorData[1])] = tonumber(colorData[2])
        end
    end
    
    self.yinglingTransformSkillCls = {}
    self.yinglingTransformSkillCls[952007] = {contribution = {3, 2}, rightAnswer = 2}
    self.yinglingTransformSkillCls[952008] = {contribution = {3, 2}, rightAnswer = 3}
    self.yinglingTransformSkillCls[952009] = {contribution = {1, 3}, rightAnswer = 1}
    self.yinglingTransformSkillCls[952010] = {contribution = {1, 3}, rightAnswer = 3}
    self.yinglingTransformSkillCls[952011] = {contribution = {1, 2}, rightAnswer = 1}
    self.yinglingTransformSkillCls[952012] = {contribution = {1, 2}, rightAnswer = 2}
end 

function LuaSkillBeginnerGuideWnd:IsYingLingTransformSkill(skillId)
    local skillCls = math.floor(skillId / 100)
    if self.yinglingTransformSkillCls[skillCls] then
        return true
    end
    return false
end

function LuaSkillBeginnerGuideWnd:RefreshConstUI()
    --刷新右下角技能栏
    for i = 1, 7 do
        local skillButtonObj = self.skillButtonList[i]
        local skillId = self.skillPositionDict[i] and self.skillPositionDict[i] or 0
        skillButtonObj:SetActive(skillId ~= 0)
        if skillId ~= 0 then
            local castSkillId = skillId
            local template = Skill_AllSkills.GetData(castSkillId)
            local skillButtonInfo = CommonDefs.GetComponent_GameObject_Type(skillButtonObj, typeof(CSkillButtonInfo))
            skillButtonInfo:LoadIcon(LuaSkillMgr:GetOriginOrReplaceSkillIcon(template), castSkillId, false, true)
            
            local skillCls = math.floor(skillId / 100)
            if self.colorSystemSkills[skillCls] ~= nil then
                local colorList = {[0] = "huahun_bai", [1] = "huahun_hong", [2] = "huahun_lan", [3] = "huahun_huang"}
                skillButtonObj.transform:Find("ColorSystemIcon").gameObject:SetActive(true)
                skillButtonObj.transform:Find("ColorSystemIcon"):GetComponent(typeof(UISprite)).spriteName = colorList[self.colorSystemSkills[skillCls]]
            else
                skillButtonObj.transform:Find("ColorSystemIcon").gameObject:SetActive(false)
            end
            
            UIEventListener.Get(skillButtonObj).onClick = DelegateFactory.VoidDelegate(function (_)
                if CClientMainPlayer.Inst then
                    CClientMainPlayer.Inst:StopAutoCastSkill(true)
                    if self:IsYingLingTransformSkill(castSkillId) then
                        self.YingLingSkill:SetActive(true)
                        self:OpenYingLingSkill(castSkillId)
                    else
                        if CClientMainPlayer.Inst:CheckSkillCDAndAlive(castSkillId) then
                            CClientMainPlayer.Inst:TryCastSkill(castSkillId, true, Vector2.zero)
                        end 
                    end
                end
            end)
        end
    end
    
    --刷新中间的技能输入序列
    self.SkillSequenceItem:SetActive(false)
    Extensions.RemoveAllChildren(self.Grid.transform)
    self.castSkillObjectSequence = {}
    for i = 1, #self.castSkillSequence do
        local skillId = self.castSkillSequence[i]
        local castSkillId = skillId
        local template = Skill_AllSkills.GetData(castSkillId)
        local skillSequenceItem = GameObject.Instantiate(self.SkillSequenceItem)
        skillSequenceItem:SetActive(true)
        skillSequenceItem.transform.parent = self.Grid.transform
        skillSequenceItem.transform.localScale = Vector3.one
        local skillButtonInfo = CommonDefs.GetComponent_GameObject_Type(skillSequenceItem, typeof(CSkillButtonInfo))
        skillButtonInfo:LoadIcon(LuaSkillMgr:GetOriginOrReplaceSkillIcon(template), castSkillId, false, true)

        local skillCls = math.floor(skillId / 100)
        if self.colorSystemSkills[skillCls] ~= nil then
            local colorList = {[0] = "huahun_bai", [1] = "huahun_hong", [2] = "huahun_lan", [3] = "huahun_huang"}
            skillSequenceItem.transform:Find("ColorSystemIcon").gameObject:SetActive(true)
            skillSequenceItem.transform:Find("ColorSystemIcon"):GetComponent(typeof(UISprite)).spriteName = colorList[self.colorSystemSkills[skillCls]]
        else
            skillSequenceItem.transform:Find("ColorSystemIcon").gameObject:SetActive(false)
        end
        
        table.insert(self.castSkillObjectSequence, skillSequenceItem)
    end
    self.Grid:Reposition()
end

function LuaSkillBeginnerGuideWnd:OnEnable()
    self.OnRightMenuChanged = DelegateFactory.Action(function()
        local panel = self.transform:GetComponent(typeof(UIPanel))
        if PlayerSettings.RightMenuExpanded then
            panel.alpha = 0
        else
            panel.alpha = 1
        end
    end)
    EventManager.AddListenerInternal(EnumEventType.OnRightMenuWndExpandedStatusChanged, self.OnRightMenuChanged)
    CUIManager.instance:HideUI(InitializeList(CreateFromClass(MakeGenericClass(List, String)), String, "", "SkillButtonBoard"))
    g_ScriptEvent:AddListener("MainPlayerCastSkillSuccess", self, "OnMainPlayerCastSkillSuccess")
    g_ScriptEvent:AddListener("UpdateCooldown", self, "OnUpdateCooldown")
    g_ScriptEvent:AddListener("MainPlayerSkillPropUpdate", self, "OnMainPlayerSkillPropUpdate")
    g_ScriptEvent:AddListener("UpdateSkillBeginnerGuideSkillBoard", self, "OnUpdateSkillBeginnerGuideSkillBoard")
    g_ScriptEvent:AddListener("UpdateYingLingState", self, "UpdateYingLingState")
    g_ScriptEvent:AddListener("UpdateSkillGuideSequence", self, "OnUpdateSkillGuideSequence")
    g_ScriptEvent:AddListener("StartSkillTraining", self, "OnStartSkillTraining")
    self:RegisterTick()
end

function LuaSkillBeginnerGuideWnd:OnStartSkillTraining(castSkillSequenceId)
    self.transform:Find("Anchor/SkillSequenceTable").gameObject:SetActive(true)
    LuaSkillBeginnerGuideWnd.startSkillTraining = false
    self:SetSkillHintFxState(self.castSkillSequence2SkillPosition[self.checkSequenceId], true)
end

function LuaSkillBeginnerGuideWnd:OnUpdateSkillGuideSequence(castSkillSequenceId)
    LuaSkillBeginnerGuideWnd.castSkillSequenceId = castSkillSequenceId

    self:InitWndData()
    self:RefreshConstUI()
    for i = 1, #self.castSkillObjectSequence do
        if self.castSkillObjectSequence[self.checkSequenceId] then
            self.castSkillObjectSequence[self.checkSequenceId].transform:GetComponent(typeof(Animation)):Stop()
        end

        self.castSkillObjectSequence[i].transform:Find("ClickFx/right").gameObject:SetActive(false)
        self.castSkillObjectSequence[i].transform:Find("ClickFx/wrong").gameObject:SetActive(false)
        self.castSkillObjectSequence[i].transform:Find("FailIcon").gameObject:SetActive(false)
        self.castSkillObjectSequence[i].transform:Find("PassIcon").gameObject:SetActive(false)

        self:SetSequenceHintFxState(i, false)
    end

    --刷新右下角技能栏
    for i = 1, 7 do
        self:SetSkillClickFxState(i, 0)
        self:SetSkillHintFxState(i, false)
    end

    self:SetSequenceHintFxState(self.checkSequenceId, true)
    self:SetSkillHintFxState(self.castSkillSequence2SkillPosition[self.checkSequenceId], LuaSkillBeginnerGuideWnd.startSkillTraining)
    LuaSkillBeginnerGuideWnd.startSkillTraining = false
    --self:SetSkillSequenceGridPosition(self.checkSequenceId)

    self.Grid.transform.localPosition = Vector3(-124 - 110*1, -3, 0)
    self.transform:Find("Anchor/SkillSequenceTable").gameObject:SetActive(false)
    LuaSkillBeginnerGuideWnd.startSkillTraining = false
end

function LuaSkillBeginnerGuideWnd:OnDisable()
    EventManager.RemoveListenerInternal(EnumEventType.OnRightMenuWndExpandedStatusChanged, self.OnRightMenuChanged)
    CUIManager.instance:ShowUI(InitializeList(CreateFromClass(MakeGenericClass(List, String)), String, "", "SkillButtonBoard"))
    g_ScriptEvent:RemoveListener("MainPlayerCastSkillSuccess", self, "OnMainPlayerCastSkillSuccess")
    g_ScriptEvent:RemoveListener("UpdateCooldown", self, "OnUpdateCooldown")
    g_ScriptEvent:RemoveListener("MainPlayerSkillPropUpdate", self, "OnMainPlayerSkillPropUpdate")
    g_ScriptEvent:RemoveListener("UpdateSkillBeginnerGuideSkillBoard", self, "OnUpdateSkillBeginnerGuideSkillBoard")
    g_ScriptEvent:RemoveListener("UpdateYingLingState", self, "UpdateYingLingState")
    g_ScriptEvent:RemoveListener("UpdateSkillGuideSequence", self, "OnUpdateSkillGuideSequence")
    g_ScriptEvent:RemoveListener("StartSkillTraining", self, "OnStartSkillTraining")

    if self.m_UpdateCoolDownTick then
        UnRegisterTick(self.m_UpdateCoolDownTick)
        self.m_UpdateCoolDownTick = nil
    end

    if self.m_CountDown3Tick then
        UnRegisterTick(self.m_CountDown3Tick)
        self.m_CountDown3Tick = nil
    end
    
    if self.m_HideSelfUITick then
        UnRegisterTick(self.m_HideSelfUITick)
        self.m_HideSelfUITick = nil
    end

    if self.m_RecoveryTick then
        UnRegisterTick(self.m_RecoveryTick)
        self.m_RecoveryTick = nil
    end
    
    if self.m_AutoMoveGirdTick then
        UnRegisterTick(self.m_AutoMoveGirdTick)
        self.m_AutoMoveGirdTick = nil
    end
end

function LuaSkillBeginnerGuideWnd:OnMainPlayerCastSkillSuccess(args)
    if (self.transform:Find("Anchor/SkillSequenceTable").gameObject.activeSelf) then
        local castSkillId = tonumber(args[0])
        local template = Skill_AllSkills.GetData(castSkillId)
        if template.Category ~= 2 and template.Category ~= 0 then
            --避免子技能的类型打断技能序列的判断
            local castSkillCls = math.floor(castSkillId / 100)
            if self.ignoreSkill[castSkillCls] then
                return
            end
            self:CastSkillSuccess(castSkillId)
        end 
    end
end

function LuaSkillBeginnerGuideWnd:OnUpdateCooldown()
    self.m_NeedUpdateCooldown = true
end

function LuaSkillBeginnerGuideWnd:RegisterTick()
    self:CancelTick()
    self.m_UpdateCoolDownTick = RegisterTick(function ( ... )
        self:OnTick()
    end, 50)
end

function LuaSkillBeginnerGuideWnd:CancelTick()
    if self.m_UpdateCoolDownTick then
        UnRegisterTick(self.m_UpdateCoolDownTick)
        self.m_UpdateCoolDownTick = nil
    end
end

function LuaSkillBeginnerGuideWnd:OnTick()
    if self.m_NeedUpdateCooldown then
        self.m_NeedUpdateCooldown = false

        local mainplayer = CClientMainPlayer.Inst
        if not mainplayer then
            return
        end
        local cooldownProp = mainplayer.CooldownProp
        local globalRemain = cooldownProp:GetServerRemainTime(1000)
        local globalTotal = cooldownProp:GetServerTotalTime(1000)
        local globalPercent = (globalTotal == 0) and 0 or (globalRemain / globalTotal)
        for i = 1, 7 do
            local skillButtonObj = self.skillButtonList[i]
            local skillButtonInfo = CommonDefs.GetComponent_GameObject_Type(skillButtonObj, typeof(CSkillButtonInfo))
            local skillId = skillButtonInfo.skillTemplateID
            local remain = cooldownProp:GetServerRemainTime(skillId)
            local total = cooldownProp:GetServerTotalTime(skillId)
            local percent = total == 0 and 0 or remain/total
            if globalRemain > remain then
                percent = globalPercent
            end 
            skillButtonInfo.CDLeft = percent
        end
    end
end

function LuaSkillBeginnerGuideWnd:RefreshSkillIcon()
    --刷新右下角技能栏
    if CClientMainPlayer.Inst then
        for i = 1, 7 do
            local skillButtonObj = self.skillButtonList[i]
            local skillId = self.skillPositionDict[i] and self.skillPositionDict[i] or 0
            skillButtonObj:SetActive(skillId ~= 0)
            if skillId ~= 0 then
                local castSkillId = skillId
                castSkillId = CClientMainPlayer.Inst.SkillProp:GetAlternativeSkillIdWithDelta(skillId, CClientMainPlayer.Inst.Level)
                local template = Skill_AllSkills.GetData(castSkillId)
                local skillButtonInfo = CommonDefs.GetComponent_GameObject_Type(skillButtonObj, typeof(CSkillButtonInfo))
                skillButtonInfo:LoadIcon(LuaSkillMgr:GetOriginOrReplaceSkillIcon(template), castSkillId, false, true)

                local skillCls = math.floor(skillId / 100)
                if self.colorSystemSkills[skillCls] ~= nil then
                    local colorList = {[0] = "huahun_bai", [1] = "huahun_hong", [2] = "huahun_lan", [3] = "huahun_huang"}
                    skillButtonObj.transform:Find("ColorSystemIcon").gameObject:SetActive(true)
                    skillButtonObj.transform:Find("ColorSystemIcon"):GetComponent(typeof(UISprite)).spriteName = colorList[self.colorSystemSkills[skillCls]]
                else
                    skillButtonObj.transform:Find("ColorSystemIcon").gameObject:SetActive(false)
                end
                
                UIEventListener.Get(skillButtonObj).onClick = DelegateFactory.VoidDelegate(function (_)
                    CClientMainPlayer.Inst:StopAutoCastSkill(true)
                    if self:IsYingLingTransformSkill(castSkillId) then
                        self.YingLingSkill:SetActive(true)
                        self:OpenYingLingSkill(castSkillId)
                    else
                        if CClientMainPlayer.Inst:CheckSkillCDAndAlive(castSkillId) then
                            CClientMainPlayer.Inst:TryCastSkill(castSkillId, true, Vector2.zero)
                        end 
                    end
                end)
            end
        end
    end
end

function LuaSkillBeginnerGuideWnd:CastSkillSuccess(skillId)
    
    if self.checkSequenceId > self.checkSequenceLength then
        return
    end

    if self.m_CountDown3Tick then
        UnRegisterTick(self.m_CountDown3Tick)
        self.m_CountDown3Tick = nil
    end

    --self:SetSkillClickFxState(self.castSkillSequence2SkillPosition[self.checkSequenceId], true)
    self:SetSkillHintFxState(self.castSkillSequence2SkillPosition[self.checkSequenceId], false)
    self:SetSequenceHintFxState(self.checkSequenceId, false)
    --self:RefreshSkillIcon()
    
    local nowTimestamp = CServerTimeMgr.Inst.timeStamp
    if self.checkSequenceId == 1 then
        --首技能, 不检查时间
        local compareSkillCls = math.floor(self.castSkillSequence[self.checkSequenceId] / 100)
        local skillCls = math.floor(skillId / 100)
        local isReplaceSkill = false
        if self.replacementSkillCls[compareSkillCls] then
            if self.replacementSkillCls[compareSkillCls][skillCls] then
                isReplaceSkill = true
            end
        end
        
        if self.castSkillSequence[self.checkSequenceId] == skillId or isReplaceSkill then
            
            --正确
            --做一些正确的表现之后, 切换到下一个技能
            self.checkSequenceRecordTime = nowTimestamp
            self:SetSkillClickFxState(self.castSkillSequence2SkillPosition[self.checkSequenceId], 1)
            self.castSkillObjectSequence[self.checkSequenceId].transform:GetComponent(typeof(Animation)):Play("skillbeginnerguidewnd_right")
            self.checkSequenceId = self.checkSequenceId + 1

            if self.checkSequenceId > self.checkSequenceLength then
                self.m_HideSelfUITick = RegisterTickOnce(function ()
                    Gac2Gas.FinishSkillTraining(LuaSkillBeginnerGuideWnd.castSkillSequenceId)
                    self.transform:Find("Anchor/SkillSequenceTable").gameObject:SetActive(false)
                    LuaSkillBeginnerGuideWnd.startSkillTraining = false
                    --CUIManager.CloseUI(CLuaUIResources.SkillBeginnerGuideWnd)
                end,  1000)
                return
            end
            self:SetSkillHintFxState(self.castSkillSequence2SkillPosition[self.checkSequenceId], true)
            self:SetSequenceHintFxState(self.checkSequenceId, true)
            
            self.m_CountDown3Tick = RegisterTickOnce(function ()
                self:CastSkillSuccess(0)
            end,  3000)
        else

            --做一些错误的表现之后, 重置技能序列
            self.checkSequenceRecordTime = 0
            self:SetSkillClickFxState(self.castSkillSequence2SkillPosition[self.checkSequenceId], -1)
            self.castSkillObjectSequence[self.checkSequenceId].transform:GetComponent(typeof(Animation)):Play("skillbeginnerguidewnd_wrong")
            self.checkSequenceId = 1

            if self.m_RecoveryTick then
                UnRegisterTick(self.m_RecoveryTick)
                self.m_RecoveryTick = nil
            end
            self.m_RecoveryTick = RegisterTickOnce(function ()
                self:RecoverSequenceList(true)
            end,  1000)
        end
    else
        --检查时间, 检查技能
        local compareSkillCls = math.floor(self.castSkillSequence[self.checkSequenceId] / 100)
        local skillCls = math.floor(skillId / 100)
        local isReplaceSkill = false
        if self.replacementSkillCls[compareSkillCls] then
            if self.replacementSkillCls[compareSkillCls][skillCls] then
                isReplaceSkill = true
            end
        end
        if (self.castSkillSequence[self.checkSequenceId] == skillId or isReplaceSkill)
                and (nowTimestamp - self.checkSequenceRecordTime) <= 3 then
            
            --正确
            --做一些正确的表现之后, 切换到下一个技能
            self.checkSequenceRecordTime = nowTimestamp
            self:SetSkillClickFxState(self.castSkillSequence2SkillPosition[self.checkSequenceId], 1)
            self.castSkillObjectSequence[self.checkSequenceId].transform:GetComponent(typeof(Animation)):Play("skillbeginnerguidewnd_right")
            self.checkSequenceId = self.checkSequenceId + 1

            if self.checkSequenceId > self.checkSequenceLength then
                self.m_HideSelfUITick = RegisterTickOnce(function ()
                    Gac2Gas.FinishSkillTraining(LuaSkillBeginnerGuideWnd.castSkillSequenceId)
                    self.transform:Find("Anchor/SkillSequenceTable").gameObject:SetActive(false)
                    LuaSkillBeginnerGuideWnd.startSkillTraining = false
                    --CUIManager.CloseUI(CLuaUIResources.SkillBeginnerGuideWnd)
                end,  1000)
                return
            end
            self:SetSkillHintFxState(self.castSkillSequence2SkillPosition[self.checkSequenceId], true)
            self:SetSequenceHintFxState(self.checkSequenceId, true)

            if self.checkSequenceId % 5 == 0 then
                self:SetSkillSequenceGridPosition(self.checkSequenceId)
            end
            
            self.m_CountDown3Tick = RegisterTickOnce(function ()
                self:CastSkillSuccess(0)
            end,  3000)
        else
            --g_MessageMgr:ShowCustomMsg("技能 错误\n" .. tostring(self.checkSequenceId) .. "\n" .. tostring(self.castSkillSequence[self.checkSequenceId])
            --        .. "\n" .. tostring(skillId) .. "\n" .. tostring(nowTimestamp - self.checkSequenceRecordTime))
            
            --做一些错误的表现之后, 重置技能序列
            self.checkSequenceRecordTime = 0
            self:SetSkillClickFxState(self.castSkillSequence2SkillPosition[self.checkSequenceId], -1)
            self.castSkillObjectSequence[self.checkSequenceId].transform:GetComponent(typeof(Animation)):Play("skillbeginnerguidewnd_wrong")

            self.checkSequenceId = 1

            if self.m_RecoveryTick then
                UnRegisterTick(self.m_RecoveryTick)
                self.m_RecoveryTick = nil
            end
            self.m_RecoveryTick = RegisterTickOnce(function ()
                self:RecoverSequenceList(true)
            end,  1000)
        end
    end
end

function LuaSkillBeginnerGuideWnd:RecoverSequenceList(sendFailRpc, showSkillHintFxState)
    if showSkillHintFxState == nil then
        showSkillHintFxState = true
    end
    for i = 1, #self.castSkillObjectSequence do
        self.castSkillObjectSequence[self.checkSequenceId].transform:GetComponent(typeof(Animation)):Stop()

        self.castSkillObjectSequence[i].transform:Find("ClickFx/right").gameObject:SetActive(false)
        self.castSkillObjectSequence[i].transform:Find("ClickFx/wrong").gameObject:SetActive(false)
        self.castSkillObjectSequence[i].transform:Find("FailIcon").gameObject:SetActive(false)
        self.castSkillObjectSequence[i].transform:Find("PassIcon").gameObject:SetActive(false)

        self:SetSequenceHintFxState(i, false)
    end

    --刷新右下角技能栏
    for i = 1, 7 do
        self:SetSkillClickFxState(i, 0)
        self:SetSkillHintFxState(i, false)
    end
    
    self:SetSequenceHintFxState(self.checkSequenceId, true)
    if showSkillHintFxState then
        self:SetSkillHintFxState(self.castSkillSequence2SkillPosition[self.checkSequenceId], true)
    end
    self:SetSkillSequenceGridPosition(1)

    if sendFailRpc then
        Gac2Gas.FailSkillTraining(LuaSkillBeginnerGuideWnd.castSkillSequenceId)
    end
end

function LuaSkillBeginnerGuideWnd:SetSkillSequenceGridPosition(sequenceId)
    if self.m_AutoMoveGirdTick then
        UnRegisterTick(self.m_AutoMoveGirdTick)
    end
    
    self.m_AutoMoveGirdTick = RegisterTickOnce(function ()
        self.Grid.transform.localPosition = Vector3(-124 - 110*sequenceId, -3, 0)
    end,  1000)
end

function LuaSkillBeginnerGuideWnd:SetSkillHintFxState(skillPosition, isOn)
    if self.skillButtonList[skillPosition] then
        local fxComp = self.skillButtonList[skillPosition].transform:Find("HintFx")
        fxComp.gameObject:SetActive(isOn)
    end
end

function LuaSkillBeginnerGuideWnd:SetSkillClickFxState(skillPosition, isOn)
    if self.skillButtonList[skillPosition] then
        local fxComp = self.skillButtonList[skillPosition].transform:Find("ClickFx")
        fxComp.transform:Find("right").gameObject:SetActive(false)
        fxComp.transform:Find("wrong").gameObject:SetActive(false)
        fxComp.transform:Find("right").gameObject:SetActive(isOn == 1)
        fxComp.transform:Find("wrong").gameObject:SetActive(isOn == -1)
    end
end

function LuaSkillBeginnerGuideWnd:SetSequenceHintFxState(sequenceId, isOn)
    if self.castSkillObjectSequence[sequenceId] then
        local fxComp = self.castSkillObjectSequence[sequenceId].transform:Find("HintFx")
        fxComp.gameObject:SetActive(isOn)
    end
end

function LuaSkillBeginnerGuideWnd:OnMainPlayerSkillPropUpdate()
    self:RefreshSkillIcon()
end 

function LuaSkillBeginnerGuideWnd:OnUpdateSkillBeginnerGuideSkillBoard(skillCls1, skillCls2, skillCls3, skillCls4, skillCls5)
    --刷新右下角技能栏
    self.skillPositionDict = {}
    self.skillPositionDict[1] = skillCls1 == 0 and 0 or skillCls1 * 100 + 1
    self.skillPositionDict[2] = skillCls2 == 0 and 0 or skillCls2 * 100 + 1
    self.skillPositionDict[3] = skillCls3 == 0 and 0 or skillCls3 * 100 + 1
    self.skillPositionDict[4] = skillCls4 == 0 and 0 or skillCls4 * 100 + 1
    self.skillPositionDict[5] = skillCls5 == 0 and 0 or skillCls5 * 100 + 1
    
    for i = 1, 7 do
        local skillButtonObj = self.skillButtonList[i]
        local skillId = self.skillPositionDict[i] and self.skillPositionDict[i] or 0
        skillButtonObj:SetActive(skillId ~= 0)
        if skillId ~= 0 then
            local castSkillId = skillId
            local template = Skill_AllSkills.GetData(castSkillId)
            local skillButtonInfo = CommonDefs.GetComponent_GameObject_Type(skillButtonObj, typeof(CSkillButtonInfo))
            skillButtonInfo:LoadIcon(LuaSkillMgr:GetOriginOrReplaceSkillIcon(template), castSkillId, false, true)

            local skillCls = math.floor(skillId / 100)
            if self.colorSystemSkills[skillCls] ~= nil then
                local colorList = {[0] = "huahun_bai", [1] = "huahun_hong", [2] = "huahun_lan", [3] = "huahun_huang"}
                skillButtonObj.transform:Find("ColorSystemIcon").gameObject:SetActive(true)
                skillButtonObj.transform:Find("ColorSystemIcon"):GetComponent(typeof(UISprite)).spriteName = colorList[self.colorSystemSkills[skillCls]]
            else
                skillButtonObj.transform:Find("ColorSystemIcon").gameObject:SetActive(false)
            end
            
            UIEventListener.Get(skillButtonObj).onClick = DelegateFactory.VoidDelegate(function (_)
                if CClientMainPlayer.Inst then
                    CClientMainPlayer.Inst:StopAutoCastSkill(true)
                    if self:IsYingLingTransformSkill(castSkillId) then
                        self.YingLingSkill:SetActive(true)
                        self:OpenYingLingSkill(castSkillId)
                    else
                        if CClientMainPlayer.Inst:CheckSkillCDAndAlive(castSkillId) then
                            CClientMainPlayer.Inst:TryCastSkill(castSkillId, true, Vector2.zero)
                        end
                    end
                end
            end)
        end
    end
end 


function LuaSkillBeginnerGuideWnd:InitUIEvent()
    CommonDefs.AddOnClickListener(self.ButtonMale, DelegateFactory.Action_GameObject(function(go) self:OnButtonClick(go, EnumYingLingState.eMale) end), false)
    CommonDefs.AddOnClickListener(self.ButtonFemale, DelegateFactory.Action_GameObject(function(go) self:OnButtonClick(go, EnumYingLingState.eFemale) end), false)
    CommonDefs.AddOnClickListener(self.ButtonMonster, DelegateFactory.Action_GameObject(function(go) self:OnButtonClick(go, EnumYingLingState.eMonster) end), false)
end

function LuaSkillBeginnerGuideWnd:OnButtonClick(go, state)
    local isOnTrainState = self.transform:Find("Anchor/SkillSequenceTable").gameObject.activeSelf
    
    if self.rightAnswer and self.rightAnswer == EnumToInt(state) or false then
    local skillId = self.castSkillSequence[self.checkSequenceId] and self.castSkillSequence[self.checkSequenceId] or 0
        if self:IsYingLingTransformSkill(skillId) then
            Gac2Gas.SkillTrainingYingLingTransform(EnumToInt(state))

            if isOnTrainState then
                self:CastSkillSuccess(skillId)
            end
            self.YingLingSkill:SetActive(false) 
        else
            Gac2Gas.SkillTrainingYingLingTransform(EnumToInt(state))
            if isOnTrainState then
                self:CastSkillSuccess(0)
            end
            self.YingLingSkill:SetActive(false)
        end
    else
        --变身成错误的方向了
        Gac2Gas.SkillTrainingYingLingTransform(EnumToInt(state))
        if isOnTrainState then
            self:CastSkillSuccess(0)
        end
        self.YingLingSkill:SetActive(false)
    end
end

function LuaSkillBeginnerGuideWnd:UpdateYingLingState(args)
    if not args then return end

    local engineId = args[0]
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer and mainplayer.EngineId == engineId then
        self:InitDisplay()
    end
end

function LuaSkillBeginnerGuideWnd:OpenYingLingSkill(yinglingTransformSkillId)
    self:InitDisplay(yinglingTransformSkillId)
end

function LuaSkillBeginnerGuideWnd:Update()
    if not CUICommonDef.CheckPressWhenButtonDown(self.YingLingSkill.transform, true) then
        self.YingLingSkill:SetActive(false)
    end
end

function LuaSkillBeginnerGuideWnd:InitDisplay(yinglingTransformSkillId)
    local mainplayer = CClientMainPlayer.Inst
    if not mainplayer then return end
    local skillCls = math.floor(yinglingTransformSkillId / 100)
    if not self.yinglingTransformSkillCls[skillCls] then
        return
    end
    local skillContribution = self.yinglingTransformSkillCls[skillCls].contribution
    self.rightAnswer = self.yinglingTransformSkillCls[skillCls].rightAnswer
    --总共三种影灵状态 男1 女2 妖3, 和必为6
    local yinglingState = 6 - skillContribution[1] - skillContribution[2]
    
    self.Bg.transform:GetComponent(typeof(CUITexture)):LoadMaterial(SafeStringFormat3("UI/Texture/Transparent/Material/raw_yinglingskill_0%d.mat", yinglingState) , false)

    self.ButtonMale.gameObject:SetActive(false)
    self.ButtonFemale.gameObject:SetActive(false)
    self.ButtonMonster.gameObject:SetActive(false)
    for i = 1, 2 do
        local pos = (i == 1) and self.LeftButtonPos or self.RightButtonPos
        local state = skillContribution[i]
        local btn
        if 1 == state then btn = self.ButtonMale
        elseif 2 == state then btn = self.ButtonFemale
        elseif 3 == state then btn = self.ButtonMonster
        else return end
        btn.transform:SetParent(pos.transform)
        btn.transform.localPosition = Vector3.zero
        btn.gameObject:SetActive(true)
    end
end
