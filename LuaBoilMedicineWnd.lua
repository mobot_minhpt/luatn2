require("common/common_include")

local UISlider = import "UISlider"
local Time = import "UnityEngine.Time"
local CUIFx = import "L10.UI.CUIFx"
local NGUIText = import "NGUIText"
local CUITexture = import "L10.UI.CUITexture"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local UIPanel = import "UIPanel"

LuaBoilMedicineWnd = class()

RegistChildComponent(LuaBoilMedicineWnd, "FireBehaviorSlider", UISlider)
RegistChildComponent(LuaBoilMedicineWnd, "FireFx", CUIFx)
RegistChildComponent(LuaBoilMedicineWnd, "SmokeFx", CUIFx)
RegistChildComponent(LuaBoilMedicineWnd, "EnhancedSmokeFx", CUIFx)
RegistChildComponent(LuaBoilMedicineWnd, "EnhancedFireFx", CUIFx)
RegistChildComponent(LuaBoilMedicineWnd, "Wood", GameObject)
RegistChildComponent(LuaBoilMedicineWnd, "Foreground", UISprite)
RegistChildComponent(LuaBoilMedicineWnd, "ShineFx", CUIFx)
RegistChildComponent(LuaBoilMedicineWnd, "SandClock", CUITexture)
RegistChildComponent(LuaBoilMedicineWnd, "ResultFx", CUIFx)
RegistChildComponent(LuaBoilMedicineWnd, "Fail", GameObject)
RegistChildComponent(LuaBoilMedicineWnd, "GuideLayer", UIPanel)

RegistClassMember(LuaBoilMedicineWnd, "m_TotalTime")
RegistClassMember(LuaBoilMedicineWnd, "m_InitProgress")
RegistClassMember(LuaBoilMedicineWnd, "m_TotalProgress")
RegistClassMember(LuaBoilMedicineWnd, "m_ClickProgress")
RegistClassMember(LuaBoilMedicineWnd, "m_CurrentProgress")
RegistClassMember(LuaBoilMedicineWnd, "m_ValidProgress")

RegistClassMember(LuaBoilMedicineWnd, "m_FadeProgress")
RegistClassMember(LuaBoilMedicineWnd, "m_FailTime")

RegistClassMember(LuaBoilMedicineWnd, "m_CountDownTick")
RegistClassMember(LuaBoilMedicineWnd, "m_TimeCounter")

RegistClassMember(LuaBoilMedicineWnd, "m_EndGame")
RegistClassMember(LuaBoilMedicineWnd, "m_FireStatus")
RegistClassMember(LuaBoilMedicineWnd, "m_DyingOutTime")

function LuaBoilMedicineWnd:Init()
	self.ResultFx:DestroyFx()
	self.Fail:SetActive(false)
	self.SandClock:LoadMaterial("UI/Texture/Transparent/Material/Jianyao_shalou_1.mat")

	local setting = ZhuJueJuQing_Setting.GetData()
	self.m_TotalTime = setting.BoilMedicineTotalTime
	self.m_InitProgress = setting.BoilMedicineInitProgress
	self.m_TotalProgress = setting.BoilMedicineTotalProgress

	self.m_ClickProgress = setting.BoilMedicineClickProgress
	self.m_FadeProgress = setting.BoilMedicineFadeProgress
	self.m_FailTime = setting.BoilMedicineFailTime

	self.m_EndGame = true
	self.m_FireStatus = EnumFireStatus.eInit
	self.m_ValidProgress = {}

	for i = 0, setting.BoilMedicineValidProgress.Length-1 do
		table.insert(self.m_ValidProgress, setting.BoilMedicineValidProgress[i])
	end

	CommonDefs.AddOnClickListener(self.Wood, DelegateFactory.Action_GameObject(function (go)
        self:OnWoodClicked(go)
    end), false)

    self.SmokeFx:LoadFx("Fx/UI/Prefab/UI_jianyao_zhengchangyan.prefab")
	self.FireFx:LoadFx("Fx/UI/Prefab/UI_jianyao_xiaohuo.prefab")
	self.EnhancedSmokeFx:LoadFx("Fx/UI/Prefab/UI_jianyao_julieyan.prefab")
	self.EnhancedFireFx:LoadFx("Fx/UI/Prefab/UI_jianyao_dahuo.prefab")


	self:TryStartGameWithGuide()
	--self:BeginGame()
end

function LuaBoilMedicineWnd:BeginGame()
	self.m_DyingOutTime = 0
	self.m_ScorchTime = 0
	self.m_EndGame = false

	self.m_CurrentProgress = self.m_InitProgress

	self.FireBehaviorSlider.value = self.m_CurrentProgress / self.m_TotalProgress
	self:DestroyCountDownTick()
	self.m_CountDownTick = RegisterTick(function ()
		self.m_TimeCounter = self.m_TimeCounter + 1
		local leftTime = self.m_TotalTime - self.m_TimeCounter
		if leftTime < 0 then
			self:EndGame(true)
		else
			self:LoadSandClock(self.m_TotalTime - leftTime)
		end
	end, 1000)

end

function LuaBoilMedicineWnd:LoadSandClock(passedTime)
	local index = 0
	local interval = self.m_TotalTime / 5
	for i = 1, 6 do
		if passedTime >= interval * (i-1) and passedTime <= interval * i then
			index = i
		end
	end

	local matPath = SafeStringFormat3("UI/Texture/Transparent/Material/Jianyao_shalou_%s.mat", tostring(index))
	self.SandClock:LoadMaterial(matPath)
end


function LuaBoilMedicineWnd:OnWoodClicked(go)
	self:TryEndGuide()
	if self.m_EndGame then return end
	self.m_CurrentProgress = self.m_CurrentProgress + self.m_ClickProgress
end

function LuaBoilMedicineWnd:DestroyCountDownTick()
	self.m_TimeCounter = 0
	if self.m_CountDownTick then
		UnRegisterTick(self.m_CountDownTick)
		self.m_CountDownTick = nil
	end
end

function LuaBoilMedicineWnd:EndGame(success)
	if self.m_EndGame then return end

	self.m_EndGame = true
	self:DestroyCountDownTick()

	if success then
		self.ResultFx:DestroyFx()
		self.ResultFx:LoadFx(CUIFxPaths.TaskFinishedFxPath)
		Gac2Gas.FinishEventTask(LuaZhuJueJuQingMgr.m_BoilMedicineTaskId, "BoilMedicine")
	else
		self.Fail:SetActive(true)
	end
	RegisterTickOnce(function ()
		CUIManager.CloseUI(CLuaUIResources.BoilMedicineWnd)
	end, 2000)
end

function LuaBoilMedicineWnd:OnEnable()
	
end

function LuaBoilMedicineWnd:OnDisable()
	self:DestroyCountDownTick()
end

function LuaBoilMedicineWnd:Update()
	if self.m_EndGame then return end

	self.m_CurrentProgress = self.m_CurrentProgress - Time.deltaTime * self.m_FadeProgress
	self.FireBehaviorSlider.value = self.m_CurrentProgress / self.m_TotalProgress
	self:CheckFireStatus()
	self:CheckFireTime()
end

function LuaBoilMedicineWnd:CheckFireStatus()
	if self.m_CurrentProgress < self.m_ValidProgress[1] and self.m_FireStatus ~= EnumFireStatus.eDyingOut then
		self:EnterDyingOut()

	elseif self.m_CurrentProgress < self.m_ValidProgress[2] and self.m_CurrentProgress >= self.m_ValidProgress[1] and self.m_FireStatus ~= EnumFireStatus.eNormal then
		self:EnterNormal()

	elseif self.m_CurrentProgress >= self.m_ValidProgress[2] and self.m_FireStatus ~= EnumFireStatus.eScorch then
		self:EnterScorch()
		
	end
end

function LuaBoilMedicineWnd:CheckFireTime()
	if self.m_FireStatus == EnumFireStatus.eDyingOut then
		self.m_DyingOutTime = self.m_DyingOutTime + Time.deltaTime
	elseif self.m_FireStatus == EnumFireStatus.eScorch then
		self.m_ScorchTime = self.m_ScorchTime + Time.deltaTime
	end

	if self.m_DyingOutTime >= self.m_FailTime or self.m_ScorchTime >= self.m_FailTime then
		self:EndGame(false)
	end
end

function LuaBoilMedicineWnd:EnterDyingOut()
	self.m_DyingOutTime = 0
	self.m_ScorchTime = 0

	self.m_FireStatus = EnumFireStatus.eDyingOut
	self.SmokeFx.gameObject:SetActive(false)
	self.FireFx.gameObject:SetActive(false)
	self.EnhancedSmokeFx.gameObject:SetActive(false)
	self.EnhancedFireFx.gameObject:SetActive(false)

	self.Foreground.color = NGUIText.ParseColor24("ffaf14", 0)
	self.ShineFx:DestroyFx()
	self.ShineFx:LoadFx("fx/ui/prefab/qinghongyupei_jindutiaoyujing_fx01.prefab")

	g_MessageMgr:ShowMessage("Boil_Medicine_Dying_Out")
end

function LuaBoilMedicineWnd:EnterNormal()
	self.m_DyingOutTime = 0
	self.m_ScorchTime = 0

	self.m_FireStatus = EnumFireStatus.eNormal

	self.EnhancedSmokeFx.gameObject:SetActive(false)
	self.EnhancedFireFx.gameObject:SetActive(false)

	self.FireFx.gameObject:SetActive(true)
	self.SmokeFx.gameObject:SetActive(true)

	self.Foreground.color = NGUIText.ParseColor24("0fc458", 0)
	self.ShineFx:DestroyFx()
end

function LuaBoilMedicineWnd:EnterScorch()
	self.m_DyingOutTime = 0
	self.m_ScorchTime = 0

	self.m_FireStatus = EnumFireStatus.eScorch

	self.EnhancedSmokeFx:LoadFx("Fx/UI/Prefab/UI_jianyao_julieyan.prefab")
	self.EnhancedFireFx:LoadFx("Fx/UI/Prefab/UI_jianyao_dahuo.prefab")

	self.Foreground.color = NGUIText.ParseColor24("c52005", 0)
	self.ShineFx:DestroyFx()
	self.ShineFx:LoadFx("fx/ui/prefab/qinghongyupei_jindutiaoyujing_fx01.prefab")

	g_MessageMgr:ShowMessage("Boil_Medicine_Scorch")

	self.FireFx.gameObject:SetActive(false)
	self.SmokeFx.gameObject:SetActive(false)
end

function LuaBoilMedicineWnd:TryStartGameWithGuide()
	local should_guide = CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.BoilMedicineWnd)
	if(should_guide)then
		self.GuideLayer.depth = CUIManager.GUIDE_MASK_DEPTH + 1
		self:EnterNormal()
	else
		self:BeginGame()
	end
end

function LuaBoilMedicineWnd:TryEndGuide()
	if(L10.Game.Guide.CGuideMgr.Inst:IsInPhase(EnumGuideKey.BoilMedicineWnd))then
		L10.Game.Guide.CGuideMgr.Inst:EndCurrentPhase()
		self.GuideLayer.depth = self.gameObject:GetComponent(typeof(UIPanel)).depth + 1
		self:BeginGame()
	end
end

function LuaBoilMedicineWnd:GetGuideGo(methodName)
	if(methodName == "GetWood")then
		return self.Wood
	end
	return nil
end

return LuaBoilMedicineWnd