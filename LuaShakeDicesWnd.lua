require("3rdParty/ScriptEvent")
require("common/common_include")
--require("design/Message/Dialog")

local LuaGameObject=import "LuaGameObject"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local Gac2Gas = import "L10.Game.Gac2Gas"
local DelegateFactory = import "DelegateFactory"
local CommonDefs = import "L10.Game.CommonDefs"
local CTickMgr = import "L10.Engine.CTickMgr"
local ETickType = import "L10.Engine.ETickType"
local CYaoYiYaoMgr = import "L10.Game.CYaoYiYaoMgr"
local MessageMgr = import "L10.Game.MessageMgr"
local Camera = import "UnityEngine.Camera"
local RenderTexture = import "UnityEngine.RenderTexture"
local CShakeDice = import "L10.UI.CShakeDice"
local LocalString = import "LocalString"
local SoundManager = import "SoundManager"
local Vector3 = import "UnityEngine.Vector3"
local MessageWndManager = import "L10.UI.MessageWndManager"
local RenderTextureFormat = import "UnityEngine.RenderTextureFormat"
local RenderTextureReadWrite = import "UnityEngine.RenderTextureReadWrite"

CLuaShakeDicesWnd=class()
RegistClassMember(CLuaShakeDicesWnd, "m_DrunkSpriteList")
RegistClassMember(CLuaShakeDicesWnd, "m_WineSpriteList")
RegistClassMember(CLuaShakeDicesWnd, "m_ShakeButton")
RegistClassMember(CLuaShakeDicesWnd, "m_BetButton")
RegistClassMember(CLuaShakeDicesWnd, "m_BigOfficers")
RegistClassMember(CLuaShakeDicesWnd, "m_SmallOfficers")
RegistClassMember(CLuaShakeDicesWnd, "m_BigFailedTex")
RegistClassMember(CLuaShakeDicesWnd, "m_SmallFailedTex")
RegistClassMember(CLuaShakeDicesWnd, "m_DisplayResultTick")
RegistClassMember(CLuaShakeDicesWnd, "m_ShakeDiceAni")
RegistClassMember(CLuaShakeDicesWnd, "m_TopTex")
RegistClassMember(CLuaShakeDicesWnd, "m_IsWaitDice")
RegistClassMember(CLuaShakeDicesWnd, "m_DicesSum")
RegistClassMember(CLuaShakeDicesWnd, "m_FailedTimes")
RegistClassMember(CLuaShakeDicesWnd, "m_IsPlayEnded")
RegistClassMember(CLuaShakeDicesWnd, "m_IsRoundEnded")
RegistClassMember(CLuaShakeDicesWnd, "m_IsOfficerWin")
RegistClassMember(CLuaShakeDicesWnd, "m_IsOfficerDrunk")
RegistClassMember(CLuaShakeDicesWnd, "m_IsAllWin")
RegistClassMember(CLuaShakeDicesWnd, "m_ReadyBetLabel")
RegistClassMember(CLuaShakeDicesWnd, "m_IsFirstOneFailed")
RegistClassMember(CLuaShakeDicesWnd, "m_OfficerPortrait")
RegistClassMember(CLuaShakeDicesWnd, "m_IsPlaySuccess")
RegistClassMember(CLuaShakeDicesWnd, "m_RoundNum")

function CLuaShakeDicesWnd:Init()
    local g = LuaGameObject.GetChildNoGC(self.transform, "CloseButton").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        MessageWndManager.ShowOKCancelMessage(MessageMgr.Inst:FormatMessage("DiceLeaveConfirm", {}), DelegateFactory.Action(function ()
            CUIManager.CloseUI(CUIResources.ShakeDicesWnd)
            end), nil, nil, nil, false)
    end)
    self.m_DrunkSpriteList = {}
    self.m_WineSpriteList = {}
    self.m_ReadyBetLabel = {}
    self.m_OfficerPortrait = {}
    local index = {"", "1", "2"}
    local officersRoot = LuaGameObject.GetChildNoGC(self.transform, "Officers").transform
    for _, value in pairs(index) do
        local officer = LuaGameObject.GetChildNoGC(officersRoot, "Officer"..value).transform
        local drunkSprite = LuaGameObject.GetChildNoGC(officer, "Drunk").gameObject
        drunkSprite:SetActive(false)
        table.insert(self.m_DrunkSpriteList, drunkSprite)
        local label = LuaGameObject.GetChildNoGC(officer, "Label").gameObject
        table.insert(self.m_ReadyBetLabel, label)
        local wineSprites = {}
        for _, val in pairs(index) do
            local obj = LuaGameObject.GetChildNoGC(officer, "Wine"..val).sprite
            --obj:SetActive(false)
            table.insert(wineSprites, obj)
        end
        table.insert(self.m_WineSpriteList, wineSprites)
        local portrait = LuaGameObject.GetChildNoGC(officer, "Portrait").cTexture
        table.insert(self.m_OfficerPortrait, portrait)
    end

    self.m_ShakeButton = LuaGameObject.GetChildNoGC(self.transform, "ShakeBtn").qnButton
    self.m_ShakeButton.Enabled = false
    self.m_ShakeButton.OnClick = DelegateFactory.Action_QnButton(
        function ( ... )
            self.m_TopTex:SetActive(true)
            CYaoYiYaoMgr.Inst:ShowWnd(MessageMgr.Inst:FormatMessage("Shake_Dices_Tip", {}), MessageMgr.Inst:FormatMessage("Shake_Dices_Tip_PC", {}))
            self.m_IsWaitDice = true
            SoundManager.Inst:PlayOneShot(SoundManager.UISoundEvents.DicePutSound, Vector3.zero, nil, 0)
        end)
    self.m_BetButton = LuaGameObject.GetChildNoGC(self.transform, "BetBtn").qnButton
    self.m_BetButton.OnClick = DelegateFactory.Action_QnButton(
        function ( ... )
            Gac2Gas.DiceBeginBet()
        end)
    local g = LuaGameObject.GetChildNoGC(self.transform, "TipBtn").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        MessageMgr.Inst:ShowMessage("Shake_Dices_Rule", {})
    end)

    local bigRoot = LuaGameObject.GetChildNoGC(self.transform, "Big").transform
    self.m_BigOfficers = {}
    for _, value in pairs(index) do
        local officer = LuaGameObject.GetChildNoGC(bigRoot, "Texture"..value).gameObject
        officer:SetActive(false)
        table.insert(self.m_BigOfficers, officer)
    end
    self.m_BigFailedTex = LuaGameObject.GetChildNoGC(bigRoot, "Failed").gameObject
    self.m_BigFailedTex:SetActive(false)

    local smallRoot = LuaGameObject.GetChildNoGC(self.transform, "Small").transform
    self.m_SmallOfficers = {}
    for _, value in pairs(index) do
        local officer = LuaGameObject.GetChildNoGC(smallRoot, "Texture"..value).gameObject
        officer:SetActive(false)
        table.insert(self.m_SmallOfficers, officer)
    end
    self.m_SmallFailedTex = LuaGameObject.GetChildNoGC(smallRoot, "Failed").gameObject
    self.m_SmallFailedTex:SetActive(false)

    local rt = RenderTexture(512, 512, 24, RenderTextureFormat.ARGB32, RenderTextureReadWrite.Default)
    LuaGameObject.GetChildNoGC(self.transform, "Camera").gameObject:GetComponent(typeof(Camera)).targetTexture = rt
    self.m_ShakeDiceAni = LuaGameObject.GetChildNoGC(self.transform, "Camera").gameObject:GetComponent(typeof(CShakeDice))
    LuaGameObject.GetChildNoGC(self.transform, "Dice").texture.mainTexture = rt

    self.m_TopTex = LuaGameObject.GetChildNoGC(self.transform, "Top").gameObject
    self.m_TopTex:SetActive(false)

    self.m_IsWaitDice = false
    self.m_IsPlayEnded = false
    self.m_IsRoundEnded = false
    self.m_IsAllWin = false
    self.m_IsFirstOneFailed = false
    self.m_IsOfficerDrunk = {false, false, false}
    self.m_RoundNum = 0
end

function CLuaShakeDicesWnd:ResetDice()
    for _, value in pairs(self.m_SmallOfficers) do
        value:SetActive(false)
    end
    for _, value in pairs(self.m_BigOfficers) do
        value:SetActive(false)
    end
    for _, value in pairs(self.m_ReadyBetLabel) do
        value:SetActive(true)
    end
    self.m_BetButton.Enabled = true
    self.m_BigFailedTex:SetActive(false)
    self.m_SmallFailedTex:SetActive(false)
    if self.m_IsPlayEnded then
        return
    end
    if self.m_IsAllWin then
        CLuaJuQingDialogMgr.ShowSerialDialog(Dialog_GameMechanicDialog.GetData(4).Message)
    else
        local winCnt = 0
        for _, value in pairs(self.m_IsOfficerWin) do
            if value then
                winCnt = winCnt + 1
            end
        end
        if winCnt == 3 then
            CLuaJuQingDialogMgr.ShowSerialDialog(Dialog_GameMechanicDialog.GetData(5).Message)
        else
            if winCnt == 0 then
                CLuaJuQingDialogMgr.ShowSerialDialog(Dialog_GameMechanicDialog.GetData(3).Message)
            else
                if winCnt == 2 and math.random(0, 2) > 1 then
                    CLuaJuQingDialogMgr.ShowSerialDialog(Dialog_GameMechanicDialog.GetData(2).Message)
                end
                if winCnt == 1 and math.random(0, 2) > 1 then
                    CLuaJuQingDialogMgr.ShowSerialDialog(Dialog_GameMechanicDialog.GetData(1).Message)
                end
            end
        end
    end
    self.m_IsAllWin = false
end

function CLuaShakeDicesWnd:EndBet( args )
    SoundManager.Inst:PlayOneShot(SoundManager.UISoundEvents.DiceChipSound, Vector3.zero, nil, 0)
    local bet = args[0]
    local bigIndex = 1
    local smallIndex = 1
    local index = {"A", "B", "C"}
    local name = {LocalString.GetString("甲"), LocalString.GetString("乙"), LocalString.GetString("丙")}
    self.m_IsOfficerWin = {}
    if bet ~= nil then
        for key, value in pairs(index) do
            if CommonDefs.DictContains(bet, TypeOfString, value) then
                table.insert(self.m_IsOfficerWin, bet[value])
                if bet[value] then
                    self.m_BigOfficers[bigIndex]:SetActive(true)
                    LuaGameObject.GetChildNoGC(self.m_BigOfficers[bigIndex].transform, "Name").label.text = name[key]
                    if self.m_IsOfficerDrunk[key] then
                        LuaGameObject.GetChildNoGC(self.m_BigOfficers[bigIndex].transform, "Portrait").cTexture:LoadNPCPortrait("hnpc395_02", false)
                    else
                        LuaGameObject.GetChildNoGC(self.m_BigOfficers[bigIndex].transform, "Portrait").cTexture:LoadNPCPortrait("hnpc395", false)
                    end
                    bigIndex = bigIndex + 1
                else
                    self.m_SmallOfficers[smallIndex]:SetActive(true)
                    LuaGameObject.GetChildNoGC(self.m_SmallOfficers[smallIndex].transform, "Name").label.text = name[key]
                    if self.m_IsOfficerDrunk[key] then
                        LuaGameObject.GetChildNoGC(self.m_SmallOfficers[smallIndex].transform, "Portrait").cTexture:LoadNPCPortrait("hnpc395_02", false)
                    else
                        LuaGameObject.GetChildNoGC(self.m_SmallOfficers[smallIndex].transform, "Portrait").cTexture:LoadNPCPortrait("hnpc395", false)
                    end
                    smallIndex = smallIndex + 1
                end
            end
        end
    end
    for _, value in pairs(self.m_ReadyBetLabel) do
        value:SetActive(false)
    end
    self.m_BetButton.Enabled = false
    self.m_ShakeButton.Enabled = true
    self.m_IsRoundEnded = false
end

function CLuaShakeDicesWnd:EndDice( args )
    SoundManager.Inst:PlayOneShot(SoundManager.UISoundEvents.DiceRollSound, Vector3.zero, nil, 0)
    local sum = args[0] + args[1] + args[2]
    if args[0] == args[1] and args[1] == args[2] then
        self.m_IsAllWin = true
    end
    for key, value in pairs(self.m_IsOfficerWin) do
        if self.m_IsAllWin then
            self.m_IsOfficerWin[key] = false
        else
            self.m_IsOfficerWin[key] = (value and sum > 10) or (value == false and sum <= 10)
        end
    end
    local failedTimes = args[3]
    self.m_TopTex:SetActive(false)
    self.m_ShakeDiceAni.Inst:ShakeDice(args[0], args[1], args[2])
    CTickMgr.Register(DelegateFactory.Action(function ()
        self:EndCurrentRound(args[0], args[1], args[2], sum, failedTimes)
    end), 1000, ETickType.Once)
end

function CLuaShakeDicesWnd:EndCurrentRound(a, b, c, sum, failedTimes)
    self.m_RoundNum = self.m_RoundNum + 1
    local str = LocalString.GetString("大")
    if self.m_IsAllWin then
        str = LocalString.GetString("大小通吃")
    elseif sum <= 10 then
        str = LocalString.GetString("小")
    end
    local name = {LocalString.GetString("甲"), LocalString.GetString("乙"), LocalString.GetString("丙")}
    if self.m_IsPlayEnded and not self.m_IsPlaySuccess then
        CUIManager.CloseUI(CUIResources.ShakeDicesWnd)
        CLuaJuQingDialogMgr.ShowSerialDialog(Dialog_GameMechanicDialog.GetData(9).Message)
        return
    else
        MessageMgr.Inst:ShowMessage("CUSTOM_STRING2", SafeStringFormat3(LocalString.GetString("%d.%d.%d， %d点，%s!"), a, b, c, sum, str))
        local win
        local lose
        for key, value in pairs(self.m_IsOfficerWin) do
            if value then
                if not win then
                    win = name[key]
                else
                    win = win..LocalString.GetString("，")..name[key]
                end
            else
                if not lose then
                    lose = name[key]
                else
                    lose = lose..LocalString.GetString("，")..name[key]
                end
            end
        end
        if lose then
            MessageMgr.Inst:ShowMessage("DiceWin", {lose})
        end
        if win then
            MessageMgr.Inst:ShowMessage("DiceLoseMoney", {win})
        end
    end
    local index = {"A", "B", "C"}
    local failedOfficer = 0
    if failedTimes ~= nil then
        for key, value in pairs(index) do
            if CommonDefs.DictContains(failedTimes, TypeOfString, value) then
                for i = 1, failedTimes[value] do
                    --self.m_WineSpriteList[key][i]:SetActive(true)
                    self.m_WineSpriteList[key][i].spriteName = "shakedice_jiu_01"
                end
                if failedTimes[value] >= 3 and not self.m_IsOfficerDrunk[key] then
                    self.m_DrunkSpriteList[key]:SetActive(true)
                    self.m_IsOfficerDrunk[key] = true
                    self.m_OfficerPortrait[key]:LoadNPCPortrait("hnpc395_02", false)
                    failedOfficer = failedOfficer + 1
                end
            end
        end
    end
    self.m_ShakeButton.Enabled = false
    if self.m_IsAllWin then
        self.m_BigFailedTex:SetActive(true)
        self.m_SmallFailedTex:SetActive(true)
    else
        if sum <= 10 then
            self.m_BigFailedTex:SetActive(true)
        else
            self.m_SmallFailedTex:SetActive(true)
        end
    end
    if self.m_DisplayResultTick ~= nil then
        invoke(self.m_DisplayResultTick)
    end
    self.m_DisplayResultTick = CTickMgr.Register(DelegateFactory.Action(function ()
        self.m_DisplayResultTick = nil
        self:ResetDice()
        if self.m_IsPlayEnded then
            CUIManager.CloseUI(CUIResources.ShakeDicesWnd)
            CLuaJuQingDialogMgr.ShowSerialDialog(Dialog_GameMechanicDialog.GetData(7).Message)
        else
            if failedOfficer == 1 and not self.m_IsFirstOneFailed then
                self.m_IsFirstOneFailed = true
                CLuaJuQingDialogMgr.ShowSerialDialog(Dialog_GameMechanicDialog.GetData(6).Message)
            end
            if self.m_RoundNum == 8 then
                CLuaJuQingDialogMgr.ShowSerialDialog(Dialog_GameMechanicDialog.GetData(8).Message)
                self.m_RoundNum = -10000
            end
        end
        self.m_IsRoundEnded = true
    end), 3000, ETickType.Once)
    self.m_IsWaitDice = false
end

function CLuaShakeDicesWnd:EndPlay( args )
    self.m_IsPlayEnded = true
    self.m_IsPlaySuccess = args[0]
    if self.m_IsRoundEnded then
        if not self.m_IsPlaySuccess then

        end
        CUIManager.CloseUI(CUIResources.ShakeDicesWnd)
    end
end

function CLuaShakeDicesWnd:ShakeDevice(args)
    if self.m_IsWaitDice then
        Gac2Gas.DiceBeginDice()
    end
end

function CLuaShakeDicesWnd:OnEnable()
    g_ScriptEvent:AddListener("DiceEndBet", self, "EndBet")
    g_ScriptEvent:AddListener("DiceEndDice", self, "EndDice")
    g_ScriptEvent:AddListener("DicePlayResult", self, "EndPlay")
    g_ScriptEvent:AddListener("PlayerShakeDevice", self, "ShakeDevice")
end

function CLuaShakeDicesWnd:OnDisable()
    g_ScriptEvent:RemoveListener("DiceEndBet", self, "EndBet")
    g_ScriptEvent:RemoveListener("DiceEndDice", self, "EndDice")
    g_ScriptEvent:RemoveListener("DicePlayResult", self, "EndPlay")
    g_ScriptEvent:RemoveListener("PlayerShakeDevice", self, "ShakeDevice")
end

function CLuaShakeDicesWnd:OnDestroy()
    if self.m_DisplayResultTick ~= nil then
        invoke(self.m_DisplayResultTick)
    end
end


return CLuaShakeDicesWnd
