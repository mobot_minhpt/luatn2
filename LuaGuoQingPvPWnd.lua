local UILabel = import "UILabel"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local PopupMenuItemData=import "L10.UI.PopupMenuItemData"
local AlignType=import "L10.UI.CPopupMenuInfoMgr+AlignType"
local CPopupMenuInfoMgr=import "L10.UI.CPopupMenuInfoMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
local EnumPlayTimesKey = import "L10.Game.EnumPlayTimesKey"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local Item_Item = import "L10.Game.Item_Item"
local CUITexture = import "L10.UI.CUITexture"
local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"


LuaGuoQingPvPWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGuoQingPvPWnd, "SignupButton", "SignupButton", GameObject)
RegistChildComponent(LuaGuoQingPvPWnd, "CancelButton", "CancelButton", GameObject)
RegistChildComponent(LuaGuoQingPvPWnd, "RemainCount", "RemainCount", UILabel)
RegistChildComponent(LuaGuoQingPvPWnd, "HotSkills", "HotSkills", GameObject)
RegistChildComponent(LuaGuoQingPvPWnd, "Time_Content", "Time_Content", UILabel)
RegistChildComponent(LuaGuoQingPvPWnd, "Content", "Content", UILabel)
RegistChildComponent(LuaGuoQingPvPWnd, "canyujiang", "canyujiang", GameObject)
RegistChildComponent(LuaGuoQingPvPWnd, "youshengjiang", "youshengjiang", GameObject)
RegistChildComponent(LuaGuoQingPvPWnd, "MoreContentButton", "MoreContentButton", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaGuoQingPvPWnd,"gameplayId")
RegistClassMember(LuaGuoQingPvPWnd,"isMatching")
RegistClassMember(LuaGuoQingPvPWnd,"awarlimit")
RegistClassMember(LuaGuoQingPvPWnd,"joinRewardItemID")
RegistClassMember(LuaGuoQingPvPWnd,"winRewardItemID")
RegistClassMember(LuaGuoQingPvPWnd,"HotSkillData")
function LuaGuoQingPvPWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    RegisterTickOnce(function()      
       CUIManager.CloseUI(CLuaUIResources.GuoQingMainWnd)
	end, 200)
    --CUIManager.CloseUI(CLuaUIResources.GuoQingMainWnd)
    local setting = GuoQing2022_JinLuHunYuanZhan.GetData()
	self.gameplayId = setting.GameplayId
    self.awarlimit = setting.AwardLimit
    self.joinRewardItemID = setting.JoinRewardItemID
    self.winRewardItemID = setting.WinRewardItemID

    UIEventListener.Get(self.SignupButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSignupButtonClick(go)
	end)
    UIEventListener.Get(self.CancelButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCancelButtonClick(go)
	end)
    UIEventListener.Get(self.canyujiang).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCanyujiangButtonClick(go)
	end)
    UIEventListener.Get(self.youshengjiang).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnYoushengjiangButtonClick(go)
	end)
    UIEventListener.Get(self.MoreContentButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMoreContentButtonClick(go)
	end)
    self.HotSkillData = {}
    self.HotSkills:SetActive(false)
    local hotskillid = {"hot1","hot2","hot3"}
    for i,hotid in pairs(hotskillid) do
        local hotskillobj = self.HotSkills.transform:Find(hotid).gameObject
        UIEventListener.Get(hotskillobj).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnHotButtonClick(go)
        end)
    end
    Gac2Gas.QueryJinLuHunYuanZhanHotSkill()

end

function LuaGuoQingPvPWnd:Init()

    self.Time_Content.text = g_MessageMgr:FormatMessage("JinLuHunYuanZhan_Open_Time")
    self.Content.text = g_MessageMgr:FormatMessage("JinLuHunYuanZhan_Brief_Rule")
    
    --CItemInfoMgr.ShowLinkItemTemplateInfo(templateId, false, nil, AlignType2.Default, 0, 0, 0, 0)
    --local itemt = Item_Item.GetData(TemplateId)
    self.CancelButton:SetActive(false)
    self.SignupButton:SetActive(true)
    self.RemainCount.gameObject:SetActive(true)




    local canyujiang_icon = self.canyujiang.transform:Find("icon"):GetComponent(typeof(CUITexture))
    canyujiang_icon:LoadMaterial(Item_Item.GetData(self.joinRewardItemID).Icon)

    local youshengjiang_icon = self.youshengjiang.transform:Find("icon"):GetComponent(typeof(CUITexture))
    youshengjiang_icon:LoadMaterial(Item_Item.GetData(self.winRewardItemID).Icon)

    if self.gameplayId and CClientMainPlayer.Inst then
        Gac2Gas.GlobalMatch_RequestCheckSignUp(self.gameplayId, CClientMainPlayer.Inst.Id)
    end
    local playtimes = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eJinLuHunYuanZhanTimes) or 0
    if playtimes > self.awarlimit then
        playtimes = self.awarlimit
    end
    self.RemainCount.text = SafeStringFormat3("%s(%s/%s)",LocalString.GetString("今日已获得奖励次数"),tostring(playtimes),tostring(self.awarlimit))
    
end

--@region UIEvent

function LuaGuoQingPvPWnd:OnMoreContentButtonClick(go)
    g_MessageMgr:ShowMessage("JinLuHunYuanZhan_Complete_Rule")
end
function LuaGuoQingPvPWnd:OnCanyujiangButtonClick(go)
    CItemInfoMgr.ShowLinkItemTemplateInfo(self.joinRewardItemID, false, nil, AlignType2.Default, 0, 0, 0, 0)
end
function LuaGuoQingPvPWnd:OnYoushengjiangButtonClick(go)
    CItemInfoMgr.ShowLinkItemTemplateInfo(self.winRewardItemID, false, nil, AlignType2.Default, 0, 0, 0, 0)
end

function LuaGuoQingPvPWnd:OnHotButtonClick(go)

    for i = 1, #self.HotSkillData do
        if self.HotSkillData[i][1] == go then
            if self.HotSkillData[i][2] ~= nil then
                CSkillInfoMgr.ShowSkillInfoWnd(self.HotSkillData[i][2], true, 0, 0, nil)
            end
            break
        end
    end

end

function LuaGuoQingPvPWnd:OnCancelButtonClick(go)
    self.SignupButton:SetActive(true)
    self.RemainCount.gameObject:SetActive(true)
    self.CancelButton:SetActive(false)
    Gac2Gas.GlobalMatch_RequestCancelSignUp(self.gameplayId)
end

function LuaGuoQingPvPWnd:OnEnable()
    g_ScriptEvent:AddListener("GlobalMatch_CheckInMatchingResult", self, "OnGlobalMatch_CheckInMatchingResult")
    g_ScriptEvent:AddListener("GlobalMatch_SignUpPlayResult", self, "OnGlobalMatch_SignUpPlayResult")
    g_ScriptEvent:AddListener("GlobalMatch_SignUpPlayResultWithInfo", self, "OnGlobalMatch_SignUpPlayResultWithInfo")
    
    g_ScriptEvent:AddListener("GlobalMatch_CancelSignUpResult", self, "OnGlobalMatch_CancelSignUpResult")
    g_ScriptEvent:AddListener("ReplyJinLuHunYuanZhanHotSkill", self, "OnReplyJinLuHunYuanZhanHotSkill")
    g_ScriptEvent:AddListener("GlobalMatch_CheckInMatchingResultWithInfo", self, "OnGlobalMatch_CheckInMatchingResultWithInfo")
    
end

function LuaGuoQingPvPWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GlobalMatch_CheckInMatchingResult", self, "OnGlobalMatch_CheckInMatchingResult")
    g_ScriptEvent:RemoveListener("GlobalMatch_SignUpPlayResult", self, "OnGlobalMatch_SignUpPlayResult")
    g_ScriptEvent:RemoveListener("GlobalMatch_SignUpPlayResultWithInfo", self, "OnGlobalMatch_SignUpPlayResultWithInfo")
    g_ScriptEvent:RemoveListener("GlobalMatch_CancelSignUpResult", self, "OnGlobalMatch_CancelSignUpResult")

    g_ScriptEvent:RemoveListener("ReplyJinLuHunYuanZhanHotSkill", self, "OnReplyJinLuHunYuanZhanHotSkill")
    g_ScriptEvent:RemoveListener("GlobalMatch_CheckInMatchingResultWithInfo", self, "OnGlobalMatch_CheckInMatchingResultWithInfo")
end

function LuaGuoQingPvPWnd:OnState(isMatching)
	self.isMatching = isMatching
    if isMatching then
        self.SignupButton:SetActive(false)
        self.RemainCount.gameObject:SetActive(false)
        self.CancelButton:SetActive(true)
    else
        self.SignupButton:SetActive(true)
        self.RemainCount.gameObject:SetActive(true)
        self.CancelButton:SetActive(false)
    end

end

function LuaGuoQingPvPWnd:OnReplyJinLuHunYuanZhanHotSkill(data)
    if data ~= nil then
        local hotskillobjid = {"hot1","hot2","hot3"}
        for i=1, #data do
            if i > 3 then
                break
            end
            local hotskillobj = self.HotSkills.transform:Find(hotskillobjid[i]).gameObject
            local skillicon = Skill_AllSkills.GetData(data[i])
            local Icon = hotskillobj.transform:Find("SkillIcon"):GetComponent(typeof(CUITexture))
            if skillicon ~= nil then
                Icon:LoadSkillIcon(skillicon.SkillIcon)
                table.insert(self.HotSkillData,{hotskillobj,data[i]})
            else
                Icon:Clear()
                table.insert(self.HotSkillData,{hotskillobj,nil})
            end       
            
        end
        self.HotSkills:SetActive(true)
    end
end
function LuaGuoQingPvPWnd:OnGlobalMatch_CheckInMatchingResultWithInfo(playerId, playId, isInMatching, resultStr, info)
    if CClientMainPlayer.Inst == nil or playerId ~= CClientMainPlayer.Inst.Id or playId ~= self.gameplayId then
        return
    end

    if isInMatching then
        local findingtext = self.CancelButton.transform:Find("Finding"):GetComponent(typeof(UILabel))
        if info.Mode == 1 then
            findingtext.text = LocalString.GetString("休闲模式匹配中...")
        else
            findingtext.text = LocalString.GetString("竞技模式匹配中...")
        end
    end

    self:OnState(isInMatching)
end

function LuaGuoQingPvPWnd:OnGlobalMatch_CheckInMatchingResult(playerId, playId, isInMatching, resultStr)
    if CClientMainPlayer.Inst == nil or playerId ~= CClientMainPlayer.Inst.Id or playId ~= self.gameplayId then
        return
    end
    self:OnState(isInMatching)
    
end

function LuaGuoQingPvPWnd:OnGlobalMatch_CancelSignUpResult(playId, success)
	if success then
		self:OnState(false)
	end
end

function LuaGuoQingPvPWnd:OnGlobalMatch_SignUpPlayResultWithInfo(playId, success, info)

    if success then
        local findingtext = self.CancelButton.transform:Find("Finding"):GetComponent(typeof(UILabel))
        if info.Mode == 1 then
            findingtext.text = LocalString.GetString("休闲模式匹配中...")
        else
            findingtext.text = LocalString.GetString("竞技模式匹配中...")
        end
        self:OnState(true)
    end
end
function LuaGuoQingPvPWnd:OnGlobalMatch_SignUpPlayResult(playId, success)

    if success then
        self:OnState(true)
    end
end

function LuaGuoQingPvPWnd:OnSignupButtonClick(go)
   
    local function Action(index)
        local findingtext = self.CancelButton.transform:Find("Finding"):GetComponent(typeof(UILabel))
        if index==0 then
            --休闲模式
            findingtext.text = LocalString.GetString("休闲模式匹配中...")
            Gac2Gas.GlobalMatch_RequestSignUpWithExtra(self.gameplayId, g_MessagePack.pack({Mode = 1}))
            
        else
           --竞技模式
           findingtext.text = LocalString.GetString("竞技模式匹配中...")
           Gac2Gas.GlobalMatch_RequestSignUpWithExtra(self.gameplayId, g_MessagePack.pack({Mode = 2}))
           
        end
    end
    local selectItem=DelegateFactory.Action_int(Action)

    local t={}
    local item1=PopupMenuItemData(LocalString.GetString("休闲模式"),selectItem,false,nil)
    table.insert(t, item1)
    local item2=PopupMenuItemData(LocalString.GetString("竞技模式"),selectItem,false,nil)
    table.insert(t, item2)

    local array=Table2Array(t,MakeArrayClass(PopupMenuItemData))
    CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, AlignType.Top,1,nil,600,true,360)
	
end
--@endregion UIEvent

