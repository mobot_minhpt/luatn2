local CButton = import "L10.UI.CButton"
local UITable = import "UITable"
local UIScrollView = import "UIScrollView"
local UIGrid = import "UIGrid"
local CUITexture = import "L10.UI.CUITexture"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local Huiliu_Setting = import "L10.Game.Huiliu_Setting"
local CPayMgr = import "L10.Game.CPayMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"

LuaHuiLiuFundWindow = class()

RegistChildComponent(LuaHuiLiuFundWindow, "BuyBtn", CButton)
RegistChildComponent(LuaHuiLiuFundWindow, "HintLabel", UILabel)
RegistChildComponent(LuaHuiLiuFundWindow, "PriceLabel", UILabel)

RegistChildComponent(LuaHuiLiuFundWindow, "FundItem", GameObject)
RegistChildComponent(LuaHuiLiuFundWindow, "FundTable", UITable)
RegistChildComponent(LuaHuiLiuFundWindow, "Scrollview", UIScrollView)

RegistClassMember(LuaHuiLiuFundWindow, "FundInfos")
RegistClassMember(LuaHuiLiuFundWindow, "m_CountDownTick")

function LuaHuiLiuFundWindow:Init()
    self.FundItem:SetActive(false)
    self:CancelTick()
    
    self.HintLabel.text = g_MessageMgr:FormatMessage("HuiLiu_Fund_Hint")
    self.FundInfos = {}

    local huiliuCID = Huiliu_Setting.GetData("HuiLiuFundChargeID").Value
    local charge = Charge_Charge.GetData(huiliuCID)
    if not charge then return end

    self.PriceLabel.text = SafeStringFormat3("%sRMB", tostring(charge.Price))

    Gac2Gas.QueryHuiLiuFund()
    -- 取消小红点
    Gac2Gas.IgnoreHuiLiuFundAlert()
end

--@desc 回流基金整体更新
function LuaHuiLiuFundWindow:SyncHuiLiuFundInfo()
    self:UpdateButBtnStatus()
    self:UpdateFundInfo()
end

--@desc 更新基金列表信息
function LuaHuiLiuFundWindow:UpdateFundInfo()
    self.FundInfos = {}

    if not LuaHuiLiuMgr.FundProgressInfo then return end

    Huiliu_FundTask.ForeachKey(function (key)
        local data = Huiliu_FundTask.GetData(key)

        local info = LuaHuiLiuMgr.FundProgressInfo[key]
        table.insert(self.FundInfos, {
            ID = data.ID,
            Desc = data.Desc,
            CurrentProgress = (info and info.progress) or 0,
            NeedProgress = data.NeedProgress,
            AwardItem = data.AwardItem,
            IsFinished =  (info and info.isFinished) or false,
            IsTaken = (info and info.isAwarded) or false,
        })
    end)

    -- 排序
    -- 已经领取的放到最后
    -- 已经完成但是未领取的放到最前面
    -- 其他按照ID排序
    table.sort(self.FundInfos, function ( a, b )

        if a.IsTaken and not b.IsTaken then
            return false
        elseif not a.IsTaken and b.IsTaken then
            return true
        else
            if a.IsFinished and not b.IsFinished then
                return true
            elseif not a.IsFinished and b.IsFinished then
                return false
            end
        end
        return a.ID < b.ID
    end)

    CUICommonDef.ClearTransform(self.FundTable.transform)

    for i = 1, #self.FundInfos do
        local info = self.FundInfos[i]
        local go = NGUITools.AddChild(self.FundTable.gameObject, self.FundItem)

        self:InitFundItem(go, info)
        go:SetActive(true)
    end

    self.FundTable:Reposition()
    self.Scrollview:ResetPosition()
end

--@desc 初始化基金Item
function LuaHuiLiuFundWindow:InitFundItem(item, info)
    if not item or not info then return end
    local fundConditionLabel = item.transform:Find("FundConditionLabel"):GetComponent(typeof(UILabel))
    local grid = item.transform:Find("AwardList/Grid"):GetComponent(typeof(UIGrid))
    local award = item.transform:Find("AwardList/Award").gameObject
    award:SetActive(false)

    local status = item.transform:Find("Status").gameObject
    local getBonusBtn = item.transform:Find("Status/GetBonusBtn"):GetComponent(typeof(CButton))
    local progressLabel = item.transform:Find("Status/ProgressLabel"):GetComponent(typeof(UILabel))
    local gotStatus = item.transform:Find("Status/GotStatus").gameObject
    status:SetActive(false)

    fundConditionLabel.text = info.Desc

    -- 奖励信息
    CUICommonDef.ClearTransform(grid.transform)
    for i = 0, info.AwardItem.Length-1, 3 do
        local itemId = info.AwardItem[i]
        local count = info.AwardItem[i+1]
        local isBind = info.AwardItem[i+2] == 1

        local go = NGUITools.AddChild(grid.gameObject, award)
        self:InitFundAwardItem(go, itemId, count, isBind)
        go:SetActive(true)
    end

    grid:Reposition()

    -- 基金状态
    status:SetActive(LuaHuiLiuMgr.FundPurchased)
    if not info.IsFinished then
        -- 进度未达成
        gotStatus:SetActive(false)
        getBonusBtn.gameObject:SetActive(false)
        progressLabel.gameObject:SetActive(true)
        progressLabel.text = SafeStringFormat3("%d/%d", info.CurrentProgress, info.NeedProgress)
    else
        progressLabel.gameObject:SetActive(false)
        progressLabel.text = nil
        if info.IsTaken then
            gotStatus:SetActive(true)
            getBonusBtn.gameObject:SetActive(false)
        else
            gotStatus:SetActive(false)
            getBonusBtn.gameObject:SetActive(true)

            CommonDefs.AddOnClickListener(getBonusBtn.gameObject, DelegateFactory.Action_GameObject(function ( go )
                Gac2Gas.GetHuiLiuFundTaskAward(info.ID)
            end), false)
        end
    end
end

--@desc 初始化基金奖励Item
function LuaHuiLiuFundWindow:InitFundAwardItem(go, itemId, count, isBind)
    if not go then return end

    local icon = go.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local amount = go.transform:Find("Amount"):GetComponent(typeof(UILabel))
    local quality = go.transform:Find("Quality"):GetComponent(typeof(UISprite))
    local bind = go.transform:Find("Bind").gameObject

    local item = Item_Item.GetData(itemId)
    if not item then return end

    icon:LoadMaterial(item.Icon)
    amount.text = tostring(count)
    bind:SetActive(isBind)
    quality.spriteName = CUICommonDef.GetItemCellBorder(item.NameColor)

    CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(function (gameobject)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
    end), false)
end

--@desc 更新购买按钮状态
function LuaHuiLiuFundWindow:UpdateButBtnStatus()
    self:CancelTick()

    CommonDefs.AddOnClickListener(self.BuyBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
        self:OnBuyBtnClicked(go)
    end), false)

    -- 基金是否购买
    if LuaHuiLiuMgr.FundPurchased then
        self.BuyBtn.Enabled = false
        self.BuyBtn.Text = LocalString.GetString("已购买")
    else
        self.BuyBtn.Enabled = true
        self.BuyBtn.Text = LocalString.GetString("购买")

        -- 倒计时
        self.m_CountDownTick = RegisterTick(function ()
            if LuaHuiLiuMgr.FundExpiredTime > CServerTimeMgr.Inst.timeStamp then
                self.BuyBtn.Text = SafeStringFormat3(LocalString.GetString("购买(余%s)"), self:ParseTime())
                self.BuyBtn.Enabled = true
            else
                self.BuyBtn.Text = self:ParseTime()
                self.BuyBtn.Enabled = false
                self:CancelTick()
            end
        end, 1000)
    end
    
end

--@desc 购买按钮的点击事件
function LuaHuiLiuFundWindow:OnBuyBtnClicked(go)
    local msg = g_MessageMgr:FormatMessage("HuiLiu_Buy_Second_Confirm")
    MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
        local pid = Huiliu_Setting.GetData("HuiLiuFundPID").Value
        if not pid then return end
        CPayMgr.Inst:BuyProduct(CPayMgr.Inst:PIDConverter(pid), 0)
    end), nil, nil, nil, false)
end

function LuaHuiLiuFundWindow:OnEnable()
    g_ScriptEvent:AddListener("SyncHuiLiuFundInfo", self, "SyncHuiLiuFundInfo")
    g_ScriptEvent:AddListener("UpdateHuiLiuFundTaskProgress", self, "UpdateFundInfo")
    g_ScriptEvent:AddListener("GetHuiLiuFundTaskAwardSuccess", self, "UpdateFundInfo")
end

function LuaHuiLiuFundWindow:OnDisable()
    g_ScriptEvent:RemoveListener("SyncHuiLiuFundInfo", self, "SyncHuiLiuFundInfo")
    g_ScriptEvent:RemoveListener("UpdateHuiLiuFundTaskProgress", self, "UpdateFundInfo")
    g_ScriptEvent:RemoveListener("GetHuiLiuFundTaskAwardSuccess", self, "UpdateFundInfo")
    self:CancelTick()
end

function LuaHuiLiuFundWindow:CancelTick()
    if self.m_CountDownTick then
		UnRegisterTick(self.m_CountDownTick)
		self.m_CountDownTick = nil
	end
end

--@desc 将可购买的剩余时间处理为玩家可理解的形式
function LuaHuiLiuFundWindow:ParseTime()
    local leftTime = LuaHuiLiuMgr.FundExpiredTime - CServerTimeMgr.Inst.timeStamp
    if leftTime <= 0 then
        return LocalString.GetString("已过期")
    end

    if leftTime < 24 * 3600 then
        local hour = math.floor(leftTime / 3600)
        local minute = math.floor(leftTime % 3600 / 60)
        return SafeStringFormat3("%d:%d", hour, minute)
    else
        local days = math.floor(leftTime / (24 * 3600))
        return SafeStringFormat3(LocalString.GetString("%d天"), days)
    end
end

return LuaHuiLiuFundWindow