local DelegateFactory  = import "DelegateFactory"
local UIScrollView  = import "UIScrollView"
local UITable = import "UITable"
local CButton = import "L10.UI.CButton"
local UITabBar = import "L10.UI.UITabBar"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Extensions = import "Extensions"
local UIWidget = import "UIWidget"

LuaGuildLeagueCrossGambleWnd = class()
RegistChildComponent(LuaGuildLeagueCrossGambleWnd, "m_TabBar", "TabBar", UITabBar)
RegistChildComponent(LuaGuildLeagueCrossGambleWnd, "m_MainView", "MainView", GameObject)
RegistChildComponent(LuaGuildLeagueCrossGambleWnd, "m_RecordView", "RecordView", GameObject)

--mainview
RegistChildComponent(LuaGuildLeagueCrossGambleWnd, "m_GambleTypeTable", "GambleTypeTable", UITable)
RegistChildComponent(LuaGuildLeagueCrossGambleWnd, "m_GambleTypeTemplate", "GambleTypeTemplate", GameObject)
RegistChildComponent(LuaGuildLeagueCrossGambleWnd, "m_RewardPoolLabel", "RewardPoolLabel", UILabel)
RegistChildComponent(LuaGuildLeagueCrossGambleWnd, "m_RewardTipLabel", "RewardTipLabel", UILabel)
RegistChildComponent(LuaGuildLeagueCrossGambleWnd, "m_RewardPoolButton", "RewardPoolButton", GameObject)
RegistChildComponent(LuaGuildLeagueCrossGambleWnd, "m_ServerTemplate", "ServerTemplate", GameObject)
RegistChildComponent(LuaGuildLeagueCrossGambleWnd, "m_RankTemplate", "RankTemplate", GameObject)
RegistChildComponent(LuaGuildLeagueCrossGambleWnd, "m_MainScrollView", "MainScrollView", UIScrollView)
RegistChildComponent(LuaGuildLeagueCrossGambleWnd, "m_MainTable", "MainTable", UITable)
RegistChildComponent(LuaGuildLeagueCrossGambleWnd, "m_GambleButton", "GambleButton", CButton)
RegistChildComponent(LuaGuildLeagueCrossGambleWnd, "m_InfoButton", "InfoButton", GameObject)
RegistChildComponent(LuaGuildLeagueCrossGambleWnd, "m_GambleDescLabel", "GambleDescLabel", UILabel)
--recordview
RegistChildComponent(LuaGuildLeagueCrossGambleWnd, "m_RecordTemplate", "RecordTemplate", GameObject)
RegistChildComponent(LuaGuildLeagueCrossGambleWnd, "m_RecordScrollview", "RecordScrollView", UIScrollView)
RegistChildComponent(LuaGuildLeagueCrossGambleWnd, "m_RecordTable", "RecordTable", UITable)
RegistChildComponent(LuaGuildLeagueCrossGambleWnd, "m_TotalRewardLabel", "TotalRewardLabel", UILabel)
RegistChildComponent(LuaGuildLeagueCrossGambleWnd, "m_GetRewardRoot", "GetRewardRoot", GameObject)
RegistChildComponent(LuaGuildLeagueCrossGambleWnd, "m_GetRewardButton", "GetRewardButton", CButton)
RegistChildComponent(LuaGuildLeagueCrossGambleWnd, "m_GetRewardDescLabel", "GetRewardDescLabel", UILabel)
RegistChildComponent(LuaGuildLeagueCrossGambleWnd, "m_GetRewardButtonAlert", "ButtonAlert", GameObject)
RegistChildComponent(LuaGuildLeagueCrossGambleWnd, "m_RecordTabAlert", "Alert", GameObject)
RegistChildComponent(LuaGuildLeagueCrossGambleWnd, "m_NoBetRecordLabel", "NoBetRecordLabel", GameObject)

RegistClassMember(LuaGuildLeagueCrossGambleWnd, "m_IsInRankGamblePage") --竞猜大类：冠军竞猜、本服竞猜, 标记当前是否为本服竞猜页面，默认false
RegistClassMember(LuaGuildLeagueCrossGambleWnd, "m_GamblePageChanged") --是否从冠军竞猜切换到了本服竞猜或者相反
RegistClassMember(LuaGuildLeagueCrossGambleWnd, "m_CurGambleType")
RegistClassMember(LuaGuildLeagueCrossGambleWnd, "m_CurServerInfo")

function LuaGuildLeagueCrossGambleWnd:Awake()
    self.m_GambleTypeTemplate:SetActive(false)
    self.m_ServerTemplate:SetActive(false)
    self.m_RankTemplate:SetActive(false)
    self.m_RecordTemplate:SetActive(false)
    self.m_TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(
        function(go, index)
            self:OnTabChange(go, index)
        end
    )

    UIEventListener.Get(self.m_RewardPoolButton.gameObject).onClick = DelegateFactory.VoidDelegate(function() self:OnRewardPoolButtonClick() end)
    UIEventListener.Get(self.m_GambleButton.gameObject).onClick = DelegateFactory.VoidDelegate(function() self:OnGambleButtonClick() end)
    UIEventListener.Get(self.m_InfoButton.gameObject).onClick = DelegateFactory.VoidDelegate(function() self:OnInfoButtonClick() end)
    UIEventListener.Get(self.m_GetRewardButton.gameObject).onClick = DelegateFactory.VoidDelegate(function() self:OnGetRewardButtonClick() end)
end

function LuaGuildLeagueCrossGambleWnd:Init()
    self.m_TabBar:ChangeTab(self.m_TabBar.SelectedIndex>-1 and self.m_TabBar.SelectedIndex or 0, false)
    self:CheckIfShowGetAwardAlert()
end

function LuaGuildLeagueCrossGambleWnd:OnTabChange(go, index)
    if index == 0 then
        self.m_MainView.gameObject:SetActive(true)
        self.m_RecordView.gameObject:SetActive(false)
        self:ShowMainView(false)
    elseif index == 1 then
        self.m_MainView.gameObject:SetActive(true)
        self.m_RecordView.gameObject:SetActive(false)
        self:ShowMainView(true)
    elseif index == 2 then
        self.m_MainView.gameObject:SetActive(false)
        self.m_RecordView.gameObject:SetActive(true)
        self:ShowRecordView()
    end
end

function LuaGuildLeagueCrossGambleWnd:ShowMainView(isInRankGamblePage)
    self:InitGambleType(isInRankGamblePage, LuaGuildLeagueCrossMgr.m_GambleInfo.openGambleTypeTbl)
end

function LuaGuildLeagueCrossGambleWnd:InitGambleType(isInRankGamblePage, openGambleTypeTbl)
    self.m_GamblePageChanged = self.m_IsInRankGamblePage~=isInRankGamblePage
    self.m_IsInRankGamblePage = isInRankGamblePage
    local n = self.m_GambleTypeTable.transform.childCount
    for i=0,n-1 do
        self.m_GambleTypeTable.transform:GetChild(i).gameObject:SetActive(false)
    end
    local designDataCount = GuildLeagueCross_GambleCron.GetDataCount()
    local idx = 0
    local first = isInRankGamblePage and EnumGuildLeagueCrossGambleType.eRank1 or EnumGuildLeagueCrossGambleType.eFirst
    local last = isInRankGamblePage and EnumGuildLeagueCrossGambleType.eRank3 or EnumGuildLeagueCrossGambleType.eFourth
    for i=first, last do
        local child = nil
		if idx<n then
			child = self.m_GambleTypeTable.transform:GetChild(idx).gameObject
		else
			child = CUICommonDef.AddChild(self.m_GambleTypeTable.gameObject, self.m_GambleTypeTemplate)
		end
		idx = idx+1
        child:SetActive(true)

        local bgSprite = child.transform:GetComponent(typeof(UIWidget))
        local nameLabel = child.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
        local timeLabel = child.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
        local data = GuildLeagueCross_GambleCron.GetData(i)
        local openTimestamp = CServerTimeMgr.Inst:GetTimeStampByStr(data.OpenTime)
        local closeTimestamp =  CServerTimeMgr.Inst:GetTimeStampByStr(data.CloseTime)
        local cnIndex = Extensions.ConvertToChineseString(i)
        local status = LuaGuildLeagueCrossMgr:GetGambleTypeStatus(i)
        if status == EnumGuildLeagueCrossGambleStatus.eClosed then -- 封盘
            nameLabel.text = SafeStringFormat3(LocalString.GetString("第%s场"),cnIndex)
            timeLabel.text = LocalString.GetString("已封盘")
            timeLabel.color = NGUIText.ParseColor24("ffffff", 0)   
            bgSprite.alpha = 1
            nameLabel.alpha = 1
            timeLabel.alpha = 0.7
        elseif status == EnumGuildLeagueCrossGambleStatus.eOpen then --进行中
            nameLabel.text = SafeStringFormat3(LocalString.GetString("第%s场(进行中)"),cnIndex)
            timeLabel.text = LuaGuildLeagueCrossMgr:FormatTimestamp(closeTimestamp, LocalString.GetString("MM月dd日 HH:mm ")) .. LocalString.GetString("截止")
            timeLabel.color = NGUIText.ParseColor24("00ab2c", 0)
            bgSprite.alpha = 1
            nameLabel.alpha = 1
            timeLabel.alpha = 1
        else --未开始
            nameLabel.text = SafeStringFormat3(LocalString.GetString("第%s场"),cnIndex)
            timeLabel.text = LuaGuildLeagueCrossMgr:FormatTimestamp(openTimestamp, LocalString.GetString("MM月dd日 HH:mm ")) .. LocalString.GetString("开始")
            timeLabel.color = NGUIText.ParseColor24("ffffff", 0)
            bgSprite.alpha = 0.3 --子节点继承透明度
            nameLabel.alpha = 1
            timeLabel.alpha = 1
        end
        UIEventListener.Get(child.gameObject).onClick = DelegateFactory.VoidDelegate(function() self:OnGameTypeSelected(child.gameObject, isInRankGamblePage, i) end)
    end
    self.m_GambleTypeTable:Reposition()
    local openGambleType = LuaGuildLeagueCrossMgr:GetOpenGamble(isInRankGamblePage, openGambleTypeTbl)
    if openGambleType>=1 and openGambleType<=designDataCount then
        self:OnGameTypeSelected(self.m_GambleTypeTable.transform:GetChild(self:GetButtonIdxByGambleType(isInRankGamblePage, openGambleType)).gameObject, isInRankGamblePage, openGambleType)
    else
        self:OnGameTypeSelected(self.m_GambleTypeTable.transform:GetChild(0).gameObject, isInRankGamblePage, isInRankGamblePage and EnumGuildLeagueCrossGambleType.eRank1 or EnumGuildLeagueCrossGambleType.eFirst)
    end
end

function LuaGuildLeagueCrossGambleWnd:GetButtonIdxByGambleType(isInRankGamblePage, openGambleType)
    return (isInRankGamblePage and openGambleType-EnumGuildLeagueCrossGambleType.eFourth or openGambleType)-1
end

function LuaGuildLeagueCrossGambleWnd:GetGambleTypeByButtonIdx(isInRankGamblePage, idx)
    return (isInRankGamblePage and idx+EnumGuildLeagueCrossGambleType.eFourth or idx)+1
end

function LuaGuildLeagueCrossGambleWnd:OnGameTypeSelected(go, isInRankGamblePage, gambleType)
    local n = self.m_GambleTypeTable.transform.childCount
    local btnIdx = self:GetButtonIdxByGambleType(isInRankGamblePage, gambleType)
    for i=0,n-1 do
        if i == btnIdx and self.m_GambleTypeTable.transform:GetChild(i).gameObject.activeSelf then
            if LuaGuildLeagueCrossMgr:GetGambleTypeStatus(gambleType) == EnumGuildLeagueCrossGambleStatus.eNotStarted then
                g_MessageMgr:ShowMessage("GLC_GAMBLE_CURRENT_CHOSEN_NOT_OPEN")
                return
            end
        end
    end

    for i=0,n-1 do
        local child = self.m_GambleTypeTable.transform:GetChild(i)
        local button = child:GetComponent(typeof(CButton))
        button.Selected = false
        if i == btnIdx then
            if LuaGuildLeagueCrossMgr:GetGambleTypeStatus(gambleType) ~= EnumGuildLeagueCrossGambleStatus.eNotStarted then
                button.Selected = true
                self:InitGambleList(isInRankGamblePage, gambleType)
            else
                --上面已经过滤掉，不会进入这里
            end
        end
    end
end

function LuaGuildLeagueCrossGambleWnd:InitGambleList(isInRankGamblePage, gambleType)
    if self.m_GamblePageChanged then
        Extensions.RemoveAllChildren(self.m_MainTable.transform)
        self.m_GamblePageChanged = false
    end
    local n = self.m_MainTable.transform.childCount
    for i=0,n-1 do
        self.m_MainTable.transform:GetChild(i).gameObject:SetActive(false)
    end
    self.m_CurGambleType = gambleType
    self.m_CurServerInfo = nil
    self:UpdateGambleButton()
    self.m_RewardPoolLabel.text = ""
    if gambleType == 0 then return end

    local servers = LuaGuildLeagueCrossMgr.m_GambleInfo.gambleTbl[gambleType]

    local totalSilver = 0
    for i,server in pairs(servers) do
        totalSilver = totalSilver + server.silver
    end
    self.m_RewardPoolLabel.text = tostring(totalSilver)
    local idx = 0
    for i, server in pairs(servers) do
        local child = nil
		if idx<n then
			child = self.m_MainTable.transform:GetChild(idx).gameObject
		else
			child = CUICommonDef.AddChild(self.m_MainTable.gameObject, isInRankGamblePage and self.m_RankTemplate or self.m_ServerTemplate)
		end
		idx = idx+1
        child:SetActive(true)

        self:InitOneServerItem(child, server, totalSilver, gambleType)
    end
    self.m_MainTable:Reposition()
    self.m_MainScrollView:ResetPosition()
    self:InitRewardPoolLabelLineColor()
    self:InitRewardTipLabel(isInRankGamblePage, servers)
end

function LuaGuildLeagueCrossGambleWnd:InitOneServerItem(child, server, totalSilver, gambleType)
    --冠军竞猜多一个查询详情的按钮
    if not LuaGuildLeagueCrossMgr:IsRankGamble(gambleType) then
        local detailButton = child.transform:Find("DetailButton").gameObject
        UIEventListener.Get(detailButton).onClick = DelegateFactory.VoidDelegate(function() 
            LuaGuildLeagueCrossMgr:QueryGLCServerGuildInfo(server.serverId, server.serverName, EnumQueryGLCServerGuildInfoContext.eGamble)
        end)
    end
    
    local serverNameLabel = child.transform:Find("ServerNameLabel"):GetComponent(typeof(UILabel))
    local oddsLabel = child.transform:Find("OddsLabel"):GetComponent(typeof(UILabel))
    local ratioSprite = child.transform:Find("RatioSprite"):GetComponent(typeof(UISprite))

    serverNameLabel.text = LuaGuildLeagueCrossMgr:IsRankGamble(gambleType) and server.aliasName or server.serverName
    oddsLabel.text = SafeStringFormat3(LocalString.GetString("赔率%.2f倍"), server.odds - server.odds%0.01)
    ratioSprite.fillAmount = server.silver/totalSilver

    local button = child:GetComponent(typeof(CButton))
    button.Selected = false
    UIEventListener.Get(child.gameObject).onClick = DelegateFactory.VoidDelegate(function() self:OnServerItemClick(child.gameObject, server, gambleType) end)
    
end

function LuaGuildLeagueCrossGambleWnd:InitRewardTipLabel(isInRankGamblePage, servers)
    self.m_RewardTipLabel.text = isInRankGamblePage and 
            SafeStringFormat3(LocalString.GetString("选择你认为[c][00ff60]%s[-][/c]在本届跨服帮会联赛的最终排名"), servers and servers[1] and servers[1].serverName or "")
            or LocalString.GetString("选择你认为最可能夺冠的服务器")
end

function LuaGuildLeagueCrossGambleWnd:InitRewardPoolLabelLineColor()
    local c = self.m_RewardPoolLabel.color
    local line01 = self.m_RewardPoolLabel.transform:Find("Bg/line01")
    if line01 then line01:GetComponent(typeof(UIWidget)).color = c end
    local line02 = self.m_RewardPoolLabel.transform:Find("Bg/line02")
    if line02 then line02:GetComponent(typeof(UIWidget)).color = c end
    self.m_RewardPoolLabel.effectColor = c
end

function LuaGuildLeagueCrossGambleWnd:OnServerItemClick(go, info, gambleType)
    local n = self.m_MainTable.transform.childCount
    for i=0,n-1 do
        local child = self.m_MainTable.transform:GetChild(i)
        local button = child:GetComponent(typeof(CButton))
        if child.gameObject == go then
            button.Selected = true
            self.m_CurServerInfo = info
            self:UpdateGambleButton()
        else
            button.Selected = false
        end
    end
end

function LuaGuildLeagueCrossGambleWnd:UpdateGambleButton()
    local info = self.m_CurServerInfo
    local status =  LuaGuildLeagueCrossMgr:GetGambleTypeStatus(self.m_CurGambleType)
    local isOpen = status == EnumGuildLeagueCrossGambleStatus.eOpen
    local isClosed = status == EnumGuildLeagueCrossGambleStatus.eClosed
    self.m_GambleButton.Enabled = not isClosed
    self.m_GambleButton.Text = isClosed and  LocalString.GetString("已封盘") or LocalString.GetString("竞  猜")
end

function LuaGuildLeagueCrossGambleWnd:OnRewardPoolButtonClick()
    LuaGuildLeagueCrossMgr:QueryGuildLeagueCrossRewardPoolInfo(EnumZhanLingAwardPoolWndOpenType.eGamble)
end

function LuaGuildLeagueCrossGambleWnd:OnGambleButtonClick()
    local info = self.m_CurServerInfo
    if info then
        if not LuaGuildLeagueCrossMgr:GetGambleTypeStatus(self.m_CurGambleType)==EnumGuildLeagueCrossGambleStatus.eOpen then
            g_MessageMgr:ShowMessage("GLC_GAMBLE_BET_NOT_OPEN_FOR_BET")
            return
        end
        local remaining = LuaGuildLeagueCrossMgr:GetRemainingPortionForGamble(self.m_CurGambleType,  LuaGuildLeagueCrossMgr.m_GambleInfo)
        if remaining>0 then
            local choiceRankOrServerId = LuaGuildLeagueCrossMgr:IsRankGamble(self.m_CurGambleType) and info.choiceRank or info.serverId 
            LuaGuildLeagueCrossMgr:ShowGambleBetWnd(self.m_CurGambleType, choiceRankOrServerId, info.serverName, info.odds)
        else
            g_MessageMgr:ShowMessage("GLC_GAMBLE_BET_PORTION_NOT_ENOUGH")
        end
    else
        g_MessageMgr:ShowMessage("GLC_GAMBLE_BET_NEED_SELECTION")
    end
end

function LuaGuildLeagueCrossGambleWnd:OnInfoButtonClick()
    g_MessageMgr:ShowMessage("GLC_GAMBLE_DESCRIPTION")
end

function LuaGuildLeagueCrossGambleWnd:ShowRecordView()
    local gambleInfo = LuaGuildLeagueCrossMgr.m_GambleInfo
    local hasResult = gambleInfo.championServerId>0
    local totalRewardSilver = 0
    local hasReward = false
    self.m_NoBetRecordLabel:SetActive(#gambleInfo.betRecordTbl==0)
    local n = self.m_RecordTable.transform.childCount
    for i=0,n-1 do
        self.m_RecordTable.transform:GetChild(i).gameObject:SetActive(false)
    end
    local idx = 0
    for i, record in pairs(gambleInfo.betRecordTbl) do
        local child = nil
		if idx<n then
			child = self.m_RecordTable.transform:GetChild(idx).gameObject
		else
			child = CUICommonDef.AddChild(self.m_RecordTable.gameObject, self.m_RecordTemplate)
		end
		idx = idx+1
        child:SetActive(true)

        local timeLabel = child.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
        local serverNameLabel = child.transform:Find("ServerNameLabel"):GetComponent(typeof(UILabel))
        local categoryLabel = child.transform:Find("CategoryLabel"):GetComponent(typeof(UILabel))
        local totalSilverLabel = child.transform:Find("TotalSilverLabel"):GetComponent(typeof(UILabel))
        local oddsLabel = child.transform:Find("OddsLabel"):GetComponent(typeof(UILabel))
        local investmentLabel = child.transform:Find("InvestmentLabel"):GetComponent(typeof(UILabel))
        local getrewardLabel = child.transform:Find("GetRewardLabel"):GetComponent(typeof(UILabel))
        local resultLabel = child.transform:Find("ResultLabel"):GetComponent(typeof(UILabel))

        local isRankGamble = LuaGuildLeagueCrossMgr:IsRankGamble(record.gambleType)

        timeLabel.text = LuaGuildLeagueCrossMgr:FormatTimestamp(record.time, LocalString.GetString("MM月dd日 HH:mm"))
        serverNameLabel.text = isRankGamble and LuaGuildLeagueCrossMgr:GetRankGambleChoiceName(record.serverId) or record.serverName
        categoryLabel.text = isRankGamble and LocalString.GetString("排名竞猜") or LocalString.GetString("冠军竞猜")
        totalSilverLabel.text = SafeStringFormat3(LocalString.GetString("%d万"), record.totalSilver/10000)
        oddsLabel.text = SafeStringFormat3(LocalString.GetString("%.2f倍"), record.odds - record.odds%0.01)
        investmentLabel.text = SafeStringFormat3(LocalString.GetString("%d万"), record.silver/10000)
        if hasResult then
            local betSuccess = false
            if isRankGamble then
                betSuccess = LuaGuildLeagueCrossMgr:GetRankGambleResult(record.gambleType) == record.serverId
            else
                betSuccess =gambleInfo.championServerId == record.serverId
            end
            if betSuccess then
                getrewardLabel.gameObject:SetActive(true)
                getrewardLabel.text = SafeStringFormat3("+%d", math.floor(record.silver*record.odds))
                resultLabel.text = ""
                totalRewardSilver = totalRewardSilver + (record.rewarded and math.floor(record.silver*record.odds) or 0)
                if not record.rewarded then hasReward = true end
            else
                getrewardLabel.gameObject:SetActive(false)
                resultLabel.text = LocalString.GetString("失败")
            end
        else
            getrewardLabel.gameObject:SetActive(false)
            resultLabel.text = LocalString.GetString("未揭晓")
        end
        
        if i % 2 == 1 then
            child:GetComponent(typeof(CButton)):SetBackgroundSprite(g_sprites.OddBgSpirite)
        else
            child:GetComponent(typeof(CButton)):SetBackgroundSprite(g_sprites.EvenBgSprite)
        end
        local button = child:GetComponent(typeof(CButton))
        button.Selected = false
    end
    self.m_RecordTable:Reposition()
    self.m_RecordScrollview:ResetPosition()

    self:InitRecordRootAndAlert(hasResult, totalRewardSilver, hasReward)
end

function LuaGuildLeagueCrossGambleWnd:InitRecordRootAndAlert(hasResult, totalRewardSilver, hasReward)
    self.m_GetRewardRoot:SetActive(hasResult)
    self.m_GetRewardDescLabel.gameObject:SetActive(not hasResult)
    if hasResult then
        self.m_TotalRewardLabel.text = tostring(totalRewardSilver)
    else
        self.m_GetRewardDescLabel.text = g_MessageMgr:FormatMessage("GLC_GAMBLE_GEN_RESULT_TIME_DESC")
    end
    self.m_GetRewardButton.Enabled = hasResult and hasReward
    self.m_GetRewardButton.Text = (hasReward or totalRewardSilver==0) and LocalString.GetString("领取奖励") or LocalString.GetString("已领取") 
end

function LuaGuildLeagueCrossGambleWnd:OnGetRewardButtonClick()
    LuaGuildLeagueCrossMgr:RequestGuildLeagueCrossGambleReward()
end

function LuaGuildLeagueCrossGambleWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSendGuildLeagueCrossGambleRecord", self, "OnSendGuildLeagueCrossGambleRecord")
end

function LuaGuildLeagueCrossGambleWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnSendGuildLeagueCrossGambleRecord", self, "OnSendGuildLeagueCrossGambleRecord")
end

function LuaGuildLeagueCrossGambleWnd:OnSendGuildLeagueCrossGambleRecord()
    self.m_TabBar:ChangeTab(self.m_TabBar.SelectedIndex, false) --刷新一下页面内容
    self:CheckIfShowGetAwardAlert()
end

function LuaGuildLeagueCrossGambleWnd:CheckIfShowGetAwardAlert()
    local gambleInfo = LuaGuildLeagueCrossMgr.m_GambleInfo
    local hasResult = gambleInfo.championServerId>0
    local hasReward = false
    if hasResult then
        for i, record in pairs(gambleInfo.betRecordTbl) do
            local isRankGamble = LuaGuildLeagueCrossMgr:IsRankGamble(record.gambleType)
            if isRankGamble then
                if LuaGuildLeagueCrossMgr:GetRankGambleResult(record.gambleType) == record.serverId then
                    if not record.rewarded then hasReward = true end
                end
            elseif gambleInfo.championServerId == record.serverId then
                if not record.rewarded then hasReward = true end
            end
        end
    end
    self.m_GetRewardButtonAlert:SetActive(hasReward)
    self.m_RecordTabAlert:SetActive(hasReward)
end





