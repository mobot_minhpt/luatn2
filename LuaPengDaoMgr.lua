local CScene=import "L10.Game.CScene"
local CBinaryDesignData_CommonReader2=import "BinaryDesignData.CBinaryDesignData_CommonReader2"


LuaPengDaoMgr = {}
LuaPengDaoMgr.m_ReplaceBlessingId = 0

function LuaPengDaoMgr:PDFY_RewardPlayerScore(score)
    print("PDFY_RewardPlayerScore", score)
end

function LuaPengDaoMgr:PDFY_RequestShuffleBuffResult(shufflePoint, shuffleTimes, shuffleType, buffLevel, tempBuff)
    print("PDFY_RequestShuffleBuffResult", shufflePoint, shuffleTimes, shuffleType, buffLevel, tempBuff)
    if not CClientMainPlayer.Inst then return end
	local info = CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
	info.ShufflePoint = shufflePoint
	info.ShuffleTimes = shuffleTimes
	info.ShuffleType = shuffleType
	info.BuffLevel = buffLevel
	local arr = g_MessagePack.unpack(tempBuff)
	for i = 1,info.TempBuff.Length do
		info.TempBuff[i - 1] = (i <= #arr) and arr[i] or 0
	end
	CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo = info
    g_ScriptEvent:BroadcastInLua("PDFY_RequestShuffleBuffResult",shufflePoint, shuffleTimes, shuffleType, buffLevel, tempBuff)
end

function LuaPengDaoMgr:PDFY_RequestReplaceAllBuffResult(newBuff, shuffleType)
    print("PDFY_RequestReplaceAllBuffResult", newBuff, shuffleType)
    if not CClientMainPlayer.Inst then return end
	local info = CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
	local arr = g_MessagePack.unpack(newBuff)
	for i = 1,info.AllBuff.Length do
		info.AllBuff[i - 1] = (i <= #arr) and arr[i] or 0
	end
	for i = 1,info.TempBuff.Length do
		info.TempBuff[i - 1] =  0
	end
	info.CurrShuffleType = shuffleType
	CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo = info
    g_ScriptEvent:BroadcastInLua("PDFY_RequestReplaceAllBuffResult",newBuff, shuffleType)
end

function LuaPengDaoMgr:PDFY_RequestSkillLevelUpSuccess(id, skillLevel, skillPoint, group, hasGroupPoint)
    print("PDFY_RequestSkillLevelUpSuccess", id, skillLevel, skillPoint, group, hasGroupPoint)
    if not CClientMainPlayer.Inst then return end
	local info = CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
	info.SkillPoint = skillPoint
	CommonDefs.DictSet(info.SkillLevel, typeof(Byte), id, typeof(Byte), skillLevel)
	CommonDefs.DictSet(info.SkillGroupPoint, typeof(Byte), group, typeof(UInt32), hasGroupPoint)
	CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo = info
    g_ScriptEvent:BroadcastInLua("PDFY_RequestSkillLevelUpSuccess",id, skillLevel, skillPoint, group, hasGroupPoint)
end

function LuaPengDaoMgr:PDFY_RequestEquipSkillSuccess(id)
    print("PDFY_RequestEquipSkillSuccess", id)
    if not CClientMainPlayer.Inst then return end
	local info = CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
	info.EquipedSkill = id
	CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo = info
    g_ScriptEvent:BroadcastInLua("PDFY_RequestEquipSkillSuccess",id)
end

function LuaPengDaoMgr:PDFY_RequestResetSkillResult(score, skillPoint)
    print("PDFY_RequestResetSkillResult", score, skillPoint)
    if not CClientMainPlayer.Inst then return end
	local info = CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
	info.SkillPoint = skillPoint
	PengDaoFuYao_OutsiderSkill.ForeachKey(function (k)
		CommonDefs.DictSet(info.SkillLevel, typeof(Byte), k, typeof(Byte), 0)
	end)
	for group = 1, 3 do
		CommonDefs.DictSet(info.SkillGroupPoint, typeof(Byte), group, typeof(UInt32), 0)
	end
    info.EquipedSkill = 0
	CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo = info
    CClientMainPlayer.Inst.PlayProp.Scores[LuaEnumPlayScoreKey.PengDaoFuYao] = score
    g_ScriptEvent:BroadcastInLua("PDFY_RequestResetSkillResult",score, skillPoint)
end

LuaPengDaoMgr.m_MainWndData = {}
LuaPengDaoMgr.m_SeasonId = 0
LuaPengDaoMgr.m_LastSeasonId = 0
function LuaPengDaoMgr:PDFY_SendMainWndData(lastSeasonId, thisSeasonId, rewardData, rankData, selfData)
    self.m_MainWndData = {}
    self.m_SeasonId = thisSeasonId
    self.m_LastSeasonId = lastSeasonId
    self.m_MainWndData.rewardData = g_MessagePack.unpack(rewardData)
    if self.m_MainWndData.rewardData then
        for rewardId, status in pairs(self.m_MainWndData.rewardData) do
            --rewardId -- 奖励id，见PengDaoFuYao_AwardItem
            --status -- 奖励状态，0和nil未发奖，1可领奖，2已领奖
        end
    end
    self.m_MainWndData.rankData = g_MessagePack.unpack(rankData)
    self.m_MainWndData.playerId2RankMap = {}
    if self.m_MainWndData.rankData then
        for rank, data in pairs(self.m_MainWndData.rankData) do
            self.m_MainWndData.playerId2RankMap[data.playerId] = rank
         end
    end
    self.m_MainWndData.selfData = g_MessagePack.unpack(selfData)
    if CUIManager.IsLoaded(CLuaUIResources.PengDaoDialogWnd) then
        g_ScriptEvent:BroadcastInLua("PDFY_SendMainWndData")
    end
    CUIManager.ShowUI(CLuaUIResources.PengDaoDialogWnd)
end

LuaPengDaoMgr.m_ShowNextLevelBlessingInfo = {}
function LuaPengDaoMgr:PDFY_ShowNextLevelBlessing(candidateBlessings, selectedBlessings, extraSkills )
    print("PDFY_ShowNextLevelBlessing", candidateBlessings, selectedBlessings, extraSkills )
    self.m_ShowNextLevelBlessingInfo = {
        candidateBlessings = g_MessagePack.unpack(candidateBlessings), 
        selectedBlessings = g_MessagePack.unpack(selectedBlessings), 
        extraSkills = g_MessagePack.unpack(extraSkills),
    }
    CUIManager.ShowUI(CLuaUIResources.PengDaoSelectBlessingWnd)
end

function LuaPengDaoMgr:PDFY_UpdateKilledMonsterNum(updatedMonsterNum)
    print("PDFY_UpdateKilledMonsterNum", updatedMonsterNum)
end

LuaPengDaoMgr.m_PlayResultInfo = {}
function LuaPengDaoMgr:PDFY_SyncPlayResult(bSuccess, difficulty1, difficulty2, difficulty3, passedLevel, usedTime, score, breakRecord, rewardScore)
    print("PDFY_SyncPlayResult", bSuccess, difficulty1, difficulty2, difficulty3, passedLevel, usedTime, score, breakRecord, rewardScore)
    self.m_PlayResultInfo = {bSuccess, difficulty1, difficulty2, difficulty3, passedLevel, usedTime, score, breakRecord, rewardScore}
    CUIManager.CloseUI(CLuaUIResources.PengDaoSelectBlessingWnd)
    CUIManager.ShowUI(CLuaUIResources.PengDaoResultWnd)
end

function LuaPengDaoMgr:PDFY_GetMonsterRewardSuccess(monsterId, rewardId)
    print("PDFY_GetMonsterRewardSuccess", monsterId, rewardId)
    if not CClientMainPlayer.Inst then return end
    local info = CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
	local rewardArr = {info.FirstReward,info.SecondReward,info.ThirdReward} 
	rewardArr[rewardId]:SetBit(monsterId, true)
    g_ScriptEvent:BroadcastInLua("PDFY_GetMonsterRewardSuccess",monsterId, rewardId)
end

function LuaPengDaoMgr:PDFY_UpdataMaxDifficulty(difficulty)
    print("PDFY_UpdataMaxDifficulty", difficulty)
end

LuaPengDaoMgr.m_TempSkillResultInfo = {}
function LuaPengDaoMgr:PDFY_QueryTempSkillResult(extraSkill_U)
    print("PDFY_QueryTempSkillResult", extraSkill_U)
    self.m_TempSkillResultInfo = g_MessagePack.unpack(extraSkill_U)
    g_ScriptEvent:BroadcastInLua("PDFY_QueryTempSkillResult")
end

function LuaPengDaoMgr:PDFY_SyncShufflePoint(value)
    print("PDFY_SyncShufflePoint", value)
    if not CClientMainPlayer.Inst then return end
	local info = CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
	info.ShufflePoint = value
	CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo = info
    g_ScriptEvent:BroadcastInLua("PDFY_SyncShufflePoint", value)
end

function LuaPengDaoMgr:PDFY_SyncSkillPoint(value)
    print("PDFY_SyncSkillPoint", value)
    if not CClientMainPlayer.Inst then return end
	local info = CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
	info.SkillPoint = value
	CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo = info
    g_ScriptEvent:BroadcastInLua("PDFY_SyncSkillPoint", value)
end

function LuaPengDaoMgr:GetAwardItemData()
    local list = {}
    PengDaoFuYao_AwardItem.ForeachKey(function (k)
        table.insert(list, PengDaoFuYao_AwardItem.GetData(k))
    end)
    table.sort(list,function (a, b)
        if self.m_MainWndData.rewardData then
            local status1 = self.m_MainWndData.rewardData[a.Id]
            local status2 = self.m_MainWndData.rewardData[b.Id]
            if (status1 == 2 or status1 == 1) and (status2 ~= 2 or status2 ~= 1) then
                return false
            elseif (status2 == 2 or status2 == 1) and (status1 ~= 2 or status1 ~= 1) then
                return true
            end
        end
        if a.Stage == b.Stage then
            return a.Difficulty < b.Difficulty
        else
            return a.Stage < b.Stage
        end
    end)
    return list
end

LuaPengDaoMgr.m_PengDaoFuYaoGameplayIdSet = nil
function LuaPengDaoMgr:IsInPlay()
    if self.m_PengDaoFuYaoGameplayIdSet == nil then
        self.m_PengDaoFuYaoGameplayIdSet = {}
        PengDaoFuYao_Gameplay.ForeachKey(function (key)
            self.m_PengDaoFuYaoGameplayIdSet[key] = true
        end)
    end
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        return self.m_PengDaoFuYaoGameplayIdSet[gamePlayId]
	end
    return false
end

function LuaPengDaoMgr:OnMainPlayerCreated()
    if self:IsInPlay() then
        if not CUIManager.IsLoaded(CLuaUIResources.PengDaoPlayWnd) then
            CUIManager.ShowUI(CLuaUIResources.PengDaoPlayWnd)
        end
    else
        if CUIManager.IsLoaded(CLuaUIResources.PengDaoPlayWnd) then
            CUIManager.CloseUI(CLuaUIResources.PengDaoPlayWnd)
        end
    end
end

function LuaPengDaoMgr:OnMainPlayerDestroyed()
    if CUIManager.IsLoaded(CLuaUIResources.PengDaoPlayWnd) then
        CUIManager.CloseUI(CLuaUIResources.PengDaoPlayWnd)
    end
end

function LuaPengDaoMgr:GetSkillDes(skillid, level, isShowNextLevelInfo)
	local data = Skill_AllSkills.GetData(skillid * 100 + level)
	local nextData = Skill_AllSkills.GetData(skillid * 100 + level + 1)
	local fullLevelData = Skill_AllSkills.GetData(skillid * 100 + 70)
	if data == nil then
		return ""
	end
	if isShowNextLevelInfo and nextData then
		local arr1 = data.__Display__Args
		local arr2 = nextData.__Display__Args
		local arr3 = fullLevelData and fullLevelData.__Display__Args or nil
		local newArgs = {}
        if arr1 then
            for i = 0, arr1.Length - 1 do
                local s1, s2, s3 = arr1[i], arr2[i], arr3 and arr3[i] or nil
                newArgs[i + 1] = self:GetNewArg(s1, s2, s3)
            end
        end
		local s = CBinaryDesignData_CommonReader2.FormatExtField(data._CommonData_.Display, Table2Array(newArgs, MakeArrayClass(cs_string)), false)
        return s
	else
		return data.Display
	end
end

function LuaPengDaoMgr:GetNewArg(s1, s2, s3)
	local match = CBinaryDesignData_CommonReader2.s_ArgRegex:Match(s1)
	local sheetName,keyfieldValue1 = match.Groups[1]:ToString(),match.Groups[3]:ToString()
	local s = (s1 == s3) and s1 or SafeStringFormat3("%s(%s)", s1, s2)
	if sheetName == "Buff_Buff" then
		local match2 = CBinaryDesignData_CommonReader2.s_ArgRegex:Match(s2)
		local keyfieldValue2 = match2.Groups[3]:ToString()
		local match3 = CBinaryDesignData_CommonReader2.s_ArgRegex:Match(s3)
		local keyfieldValue3 = match3.Groups[3]:ToString()
		local buffData1 = Buff_Buff.GetData(tonumber(keyfieldValue1))
		local arr1 = buffData1.__Display__Args
		local buffData2 = Buff_Buff.GetData(tonumber(keyfieldValue2))
		if not buffData2 then
			return s
		end
		local arr2 = buffData2.__Display__Args
		local buffData3 = Buff_Buff.GetData(tonumber(keyfieldValue3))
		local arr3 = buffData3 and buffData3.__Display__Args or nil
		local newArgs = {}
		for i = 0, arr1.Length - 1 do
			local a1, a2, a3 = arr1[i], arr2[i], arr3 and arr3[i] or nil
			newArgs[i + 1] = (a1 == a3) and a1 or SafeStringFormat3("%s(%s)", a1, a2)
		end
		s = CBinaryDesignData_CommonReader2.FormatExtField(buffData1._CommonData_.Display, Table2Array(newArgs, MakeArrayClass(cs_string)), false)
	end
	return s
end

function LuaPengDaoMgr:GetMonsterKillNum(id)
	local hasInfo = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
	if hasInfo then
		local info = CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
		local map = id <= 250 and info.KilledNum1 or info.KilledNum2
		local newid = id <= 250 and id or (id - 250)
		local default, killNum = CommonDefs.DictTryGet(map, typeof(Byte), newid, typeof(Byte))
		if self:HasGetMonsterRewarded(id) then
			return 51
		end
		return default and killNum or 0
	end
	return 0
end

function LuaPengDaoMgr:GetMonsterRewarded(id, index)
	local hasInfo = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
	if hasInfo then
		local info = CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
		local rewardArr = {info.FirstReward,info.SecondReward,info.ThirdReward} -- info.ForthReward
		if index then
			return rewardArr[index]:GetBit(id)
		else
			local rewarded = false
			local killNum = self:GetMonsterKillNum(id)
			for index = 1, 3 do 
				local listAwardData = PengDaoFuYao_MonsterListAward.GetData(index)
				rewarded = rewarded or ((killNum >= listAwardData.Number) and not self:GetMonsterRewarded(id, index)) 
			end
			return rewarded
		end
	end
	return false
end

function LuaPengDaoMgr:HasGetMonsterRewarded(id, index)
	local hasInfo = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
	if hasInfo then
		local info = CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
		local rewardArr = {info.FirstReward,info.SecondReward,info.ThirdReward}
		local rewarded = true
		if index then
			return rewardArr[index]:GetBit(id)
		else
			for index = 1, 3 do 
				rewarded = rewarded and rewardArr[index]:GetBit(id)
			end
		end
		return rewarded
	end
	return false
end

function LuaPengDaoMgr:IsMonsterLocked(id)
	local unlocked = self:GetMonsterKillNum(id) > 0 
	for index = 1, 3 do 
		unlocked = unlocked or (self:GetMonsterRewarded(id, index)) 
	end
	return not unlocked
end

function LuaPengDaoMgr:PDFY_RequestMonsterAllRewardResult(updatedMonsterReward_U)
	local hasInfo = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
	if hasInfo then
		local info = CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
		local rewardArr = {info.FirstReward,info.SecondReward,info.ThirdReward}
		local updatedMonsterReward = g_MessagePack.unpack(updatedMonsterReward_U)
		for id, data in pairs(updatedMonsterReward) do
			for _, rewardId in pairs(data) do
				local rewardInfo = rewardArr[rewardId]
				rewardInfo:SetBit(id, true)
				rewardArr[rewardId] = rewardInfo
			end
		end
		info.FirstReward = rewardArr[1]
		info.SecondReward = rewardArr[2]
		info.ThirdReward = rewardArr[3]
		CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo = info
	end
	g_ScriptEvent:BroadcastInLua("PDFY_RequestMonsterAllRewardResult", updatedMonsterReward_U)
end

function LuaPengDaoMgr:PDFY_ShuffleBuffFail()
	g_ScriptEvent:BroadcastInLua("PDFY_ShuffleBuffFail")
end