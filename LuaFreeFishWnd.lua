local UISprite = import "UISprite"
local UILabel = import "UILabel"
local UITexture = import "UITexture"
local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CCenterCountdownWnd = import "L10.UI.CCenterCountdownWnd"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local LuaTweenUtils = import "LuaTweenUtils"
local Ease = import "DG.Tweening.Ease"
local CCommonUIFxWnd = import "L10.UI.CCommonUIFxWnd"
local EnumEventType = import "EnumEventType"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local Animation = import "UnityEngine.Animation"
local BoxCollider = import "UnityEngine.BoxCollider"

LuaFreeFishWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaFreeFishWnd, "FlowerTrack", "FlowerTrack", GameObject)
RegistChildComponent(LuaFreeFishWnd, "FlowerExample", "FlowerExample", GameObject)
RegistChildComponent(LuaFreeFishWnd, "FishBasket", "FishBasket", GameObject)
RegistChildComponent(LuaFreeFishWnd, "FishGroup", "FishGroup", GameObject)
RegistChildComponent(LuaFreeFishWnd, "CurrentFreeText", "CurrentFreeText", UILabel)
RegistChildComponent(LuaFreeFishWnd, "FreeTips", "FreeTips", UILabel)
RegistChildComponent(LuaFreeFishWnd, "StoneButton", "StoneButton", GameObject)
RegistChildComponent(LuaFreeFishWnd, "TimeProgressBar", "TimeProgressBar", UISprite)
RegistChildComponent(LuaFreeFishWnd, "RemainTimeLabel", "RemainTimeLabel", UILabel)
RegistChildComponent(LuaFreeFishWnd, "TimeProgressBarBg", "TimeProgressBarBg", GameObject)

--@endregion RegistChildComponent end

function LuaFreeFishWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self:InitWndData()
    self:RefreshConstUI()
    self:RefreshVariableUI(self.currentFishNumber)
    self:InitUIEvent()
    
end

function LuaFreeFishWnd:Init()

end

--@region UIEvent

--@endregion UIEvent
function LuaFreeFishWnd:OnDestroy()
    self:ClearAllTween()
end

function LuaFreeFishWnd:ClearAllTween()
    LuaTweenUtils.Kill(self.startGameTween, false)
    LuaTweenUtils.Kill(self.miniGameTween, false)
    LuaTweenUtils.Kill(self.generateFlowerAndDropTween, false)

    for _, tween in pairs(self.go2Tween) do
        if tween then
            LuaTweenUtils.Kill(tween, false)
        end
    end
end

function LuaFreeFishWnd:ClearTween()

end

function LuaFreeFishWnd:OnEnable()
    
end

function LuaFreeFishWnd:OnDisable()
    
end

function LuaFreeFishWnd:InitWndData()
    self.fishTotalTime = NanDuFanHua_FreeFishSetting.GetData().FishTotalTime
    self.fishWarningTime = NanDuFanHua_FreeFishSetting.GetData().FishWarningTime
    self.fishTargetNumber = NanDuFanHua_FreeFishSetting.GetData().FishTargetNumber
    self.dropTime = NanDuFanHua_FreeFishSetting.GetData().DropTime
    self.dropRecommendTime = NanDuFanHua_FreeFishSetting.GetData().DropRecommendTime
    self.dropGap = NanDuFanHua_FreeFishSetting.GetData().DropGap
    self.clickPenalty = NanDuFanHua_FreeFishSetting.GetData().ClickPenalty
    self.dropRandomRange = NanDuFanHua_FreeFishSetting.GetData().DropRandomRange

    self.currentFishNumber = 0
    self.catchedFish = {}
    for i = 1, self.fishTargetNumber do
        self.catchedFish[i] = false
    end

    self.RemainTimeLabel.text = LocalString.GetString("剩余") .. self.fishTotalTime .. LocalString.GetString("秒")
    self.bgWidth = self.TimeProgressBarBg.transform:Find("Bg"):GetComponent(typeof(UISprite)).width
    self.TimeProgressBar.width = self.bgWidth
    
    
    self.isRecommend = false
    self.recommendGo = nil
    --类对象池
    self.inactiveFlowers = {}
    self.go2Tween = {}
end

function LuaFreeFishWnd:RefreshConstUI()
    self.FlowerExample:SetActive(false)
    for i = 1, self.fishTargetNumber do
        self.FishGroup.transform:Find(tostring(i)).gameObject:SetActive(false)
    end
    self.transform:Find("fish").gameObject:SetActive(false)
    local countDownTime = 3
    CCenterCountdownWnd.count = countDownTime
    CCenterCountdownWnd.InitStartTime()
    CUIManager.ShowUI(CUIResources.CenterCountdownWnd)

    self.startGameTween = LuaTweenUtils.TweenInt(0, 1, countDownTime, function ( val )
        --doNothing
    end)
    LuaTweenUtils.SetEase(self.startGameTween, Ease.Linear)
    LuaTweenUtils.OnComplete(self.startGameTween, function()
        self:StartGame()
    end)
    
    self:ChangeStoneButtonState(false)
end

function LuaFreeFishWnd:RefreshVariableUI(freeFish, flowerGo)
    if flowerGo then
        self.CurrentFreeText.text = freeFish .. "/" .. self.fishTargetNumber

        local tbl = {}
        for i = 1, self.fishTargetNumber do
            if not self.catchedFish[i] then
                table.insert(tbl, i)
            end
        end
        if #tbl > 0 then
            local randomResult = tbl[math.random(1, #tbl)]
            self.FishGroup.transform:Find(randomResult).gameObject:SetActive(true)
            self.catchedFish[randomResult] = true
        end
    else
        self.CurrentFreeText.text = freeFish .. "/" .. self.fishTargetNumber
    end
end

function LuaFreeFishWnd:InitUIEvent()
    UIEventListener.Get(self.StoneButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self.StoneButton.transform:GetComponent(typeof(BoxCollider)).enabled = false
        RegisterTickOnce(function ()
            if not CommonDefs.IsNull(self.gameObject) then
                self.StoneButton.transform:GetComponent(typeof(BoxCollider)).enabled = true
            end
        end, NanDuFanHua_FreeFishSetting.GetData().ClickPenalty * 1000)
        
        self.StoneButton.transform:Find("Chu"):GetComponent(typeof(Animation)):Play("freefishwnd_chu")
        
        if self.isRecommend then
            if self.recommendGo then
                self.FlowerTrack.transform:GetComponent(typeof(Animation)):Play("freefishwnd_fuwen")
                self.currentFishNumber = self.currentFishNumber + 1
                RegisterTickOnce(function ()
                    if not CommonDefs.IsNull(self.gameObject) then
                        self:RefreshVariableUI(self.currentFishNumber, self.recommendGo)
                    end
                end, 500)
                RegisterTickOnce(function ()
                    if not CommonDefs.IsNull(self.gameObject) then
                        local tween = self.go2Tween[self.recommendGo:GetInstanceID()]
                        LuaTweenUtils.Kill(tween, true)
                        self.transform:Find("fish").gameObject:SetActive(false)
                        self.transform:Find("fish").gameObject:SetActive(true)
                        if self.currentFishNumber >= self.fishTargetNumber then
                            --结束时间
                            LuaTweenUtils.Kill(self.miniGameTween, true)
                            LuaTweenUtils.Kill(self.generateFlowerAndDropTween, false)
                        end
                    end
                end, 200)
            end
        end
    end)
end

function LuaFreeFishWnd:StartGame()
    local duration = self.fishTotalTime
    local commonColor = NGUIText.ParseColor24("274882",0)
    local warningColor = NGUIText.ParseColor24("B52B14",0)
    
    self.miniGameTween = LuaTweenUtils.TweenFloat(duration, 0, duration, function ( val )
        if not CommonDefs.IsNull(self.gameObject) then
            self:ChangeCountdown(val, duration, commonColor, warningColor)
        end
    end)
    LuaTweenUtils.SetEase(self.miniGameTween, Ease.Linear)
    LuaTweenUtils.OnComplete(self.miniGameTween, function()
        self:FinishGame()
    end)

    local flowerIndex = 0
    self.generateFlowerAndDropTween = LuaTweenUtils.TweenFloat(duration, 0, duration, function ( val )
        if not CommonDefs.IsNull(self.gameObject) then
            local passTime = duration - val
            if passTime >= flowerIndex * self.dropGap then
                self:ControlFlower()
                flowerIndex = flowerIndex + 1
            end
        end
    end)
    LuaTweenUtils.SetEase(self.generateFlowerAndDropTween, Ease.Linear)
end

function LuaFreeFishWnd:ControlFlower()
    local go = nil
    if #self.inactiveFlowers == 0 then
        go = CommonDefs.Object_Instantiate(self.FlowerExample)
    else    
        local index = #self.inactiveFlowers
        go = self.inactiveFlowers[index]
        table.remove(self.inactiveFlowers, index)
    end

    if not CommonDefs.IsNull(go) then
        go.transform.parent = self.FlowerTrack.transform
        go.transform.localScale = Vector3.one
        local srcX = (math.random()-0.5)*2*self.dropRandomRange + 150
        local targetY = self.StoneButton.transform.localPosition.y
        go.transform.localPosition = Vector3(srcX, 0, 0)
        go:SetActive(true)
        
        local flowerTween = LuaTweenUtils.TweenFloat(self.dropTime, 0, self.dropTime, function ( val )
            if not CommonDefs.IsNull(self.gameObject) then
                local passTime = self.dropTime - val
                go.transform.localPosition = Vector3(srcX, passTime/self.dropTime*targetY, 0)
                if passTime >= self.dropRecommendTime then
                    self:ChangeStoneButtonState(true, go)
                end
            end
        end)
        LuaTweenUtils.SetEase(flowerTween, Ease.Linear)
        LuaTweenUtils.OnComplete(flowerTween, function()
            if not CommonDefs.IsNull(go) then
                go:SetActive(false)
                table.insert(self.inactiveFlowers, go)
                self:ChangeStoneButtonState(false)            
            end
        end)
        self.go2Tween[go:GetInstanceID()] = flowerTween
    end
end

function LuaFreeFishWnd:FinishGame()
    if CommonDefs.IsNull(self.gameObject) then
        return
    end
    
    for _, tween in pairs(self.go2Tween) do
        if tween then
            LuaTweenUtils.Kill(tween, false)
        end
    end

    if self.currentFishNumber >= self.fishTargetNumber then
        --成功
        EventManager.BroadcastInternalForLua(EnumEventType.OnLoadCommonUIFx, {CUIFxPaths.ShengliFxPath})
        Gac2Gas.WuChengEnFangShengFinish()
        RegisterTickOnce(function ()
            if not CommonDefs.IsNull(self.gameObject) then
                CUIManager.CloseUI(CLuaUIResources.FreeFishWnd)      
            end
        end, 3 * 1000)
    else
        --失败
        EventManager.BroadcastInternalForLua(EnumEventType.OnLoadCommonUIFx, {CUIFxPaths.ShibaiFxPath})
    end
end

function LuaFreeFishWnd:ChangeStoneButtonState(isRecommend, go)
    --@TODO 可能会有资源竞争的问题吗?
    self.isRecommend = isRecommend
    
    if isRecommend then
        self:SetHighLightState(true)
        self.recommendGo = go
    else
        self:SetHighLightState(false)
    end
end

function LuaFreeFishWnd:SetHighLightState(isOn)
    self.StoneButton.transform:Find("vfx_freefishwnd_bo_highlight").gameObject:SetActive(isOn)
    self.StoneButton.transform:Find("Chu/vfx_freefishwnd_chu_highlight").gameObject:SetActive(isOn)
end

function LuaFreeFishWnd:ChangeCountdown(remainValue, duration, commonColor, warningColor)
    local changeColorTime = self.fishWarningTime

    self.RemainTimeLabel.text = LocalString.GetString("剩余") .. math.floor(remainValue) .. LocalString.GetString("秒")
    self.TimeProgressBar.width = remainValue / duration * self.bgWidth
    if remainValue == 0 then
        self.TimeProgressBar.gameObject:SetActive(false)
    end
    
    if remainValue <= changeColorTime then
        --变红
        self.RemainTimeLabel.color = warningColor
        self.TimeProgressBar.spriteName = "common_hpandmp_hp"
    else
        --普通
        self.RemainTimeLabel.color = commonColor
        self.TimeProgressBar.spriteName = "common_hpandmp_mp"
    end
end
