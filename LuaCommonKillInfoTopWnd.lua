local TweenScale = import "TweenScale"
local TweenAlpha = import "TweenAlpha"
local TweenPosition = import "TweenPosition"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local CClientOtherPlayer = import "L10.Game.CClientOtherPlayer"

LuaCommonKillInfoTopWnd = class()

RegistChildComponent(LuaCommonKillInfoTopWnd,"m_LeftPortrait","LeftPortrait", CUITexture)
RegistChildComponent(LuaCommonKillInfoTopWnd,"m_RightPortrait","RightPortrait", CUITexture)
RegistChildComponent(LuaCommonKillInfoTopWnd,"m_LeftNameLabel","LeftNameLabel", UILabel)
RegistChildComponent(LuaCommonKillInfoTopWnd,"m_RightNameLabel","RightNameLabel", UILabel)
RegistChildComponent(LuaCommonKillInfoTopWnd,"m_LeftBorder","LeftBorder", UISprite)
RegistChildComponent(LuaCommonKillInfoTopWnd,"m_RightBorder","RightBorder", UISprite)
RegistChildComponent(LuaCommonKillInfoTopWnd,"m_TweenScale","Tween", TweenScale)
RegistChildComponent(LuaCommonKillInfoTopWnd,"m_TweenPos","Tween", TweenPosition)
RegistChildComponent(LuaCommonKillInfoTopWnd,"m_Fx","Fx", CUIFx)

RegistClassMember(LuaCommonKillInfoTopWnd, "m_LeftBorderColor")
RegistClassMember(LuaCommonKillInfoTopWnd, "m_RightBorderColor")
RegistClassMember(LuaCommonKillInfoTopWnd, "m_LeftNameColor")
RegistClassMember(LuaCommonKillInfoTopWnd, "m_RightNameColor")
RegistClassMember(LuaCommonKillInfoTopWnd, "m_TweenAlpha")
RegistClassMember(LuaCommonKillInfoTopWnd, "m_Queue")
RegistClassMember(LuaCommonKillInfoTopWnd, "m_EnterAniDelay")
RegistClassMember(LuaCommonKillInfoTopWnd, "m_EndAniDelay")
RegistClassMember(LuaCommonKillInfoTopWnd, "m_Tick")

function LuaCommonKillInfoTopWnd:InitComponent()
    self.m_LeftBorderColor = self.m_LeftBorder.color
    self.m_RightBorderColor = self.m_RightBorder.color
    self.m_LeftNameColor = self.m_LeftNameLabel.color
    self.m_RightNameColor = self.m_RightNameLabel.color
    self.m_TweenAlpha = self.gameObject:GetComponent(typeof(TweenAlpha))
    self.m_Queue = {}
    self.m_EnterAniDelay = 0.2
    self.m_EndAniDelay = 0.3
    self.m_TweenScale:AddOnFinished(DelegateFactory.Callback(function ()
        self:CancelTick()
        self.m_Tick = RegisterTick(function () self:CheckInfo() end,500)
    end))
    self.m_TweenPos:AddOnFinished(DelegateFactory.Callback(function ()
        if #self.m_Queue > 0 then
            self:OnInfoEnter()
        end
    end))
end

function LuaCommonKillInfoTopWnd:CancelTick()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
end

function LuaCommonKillInfoTopWnd:OnEnable()
    self:InitComponent()
    g_ScriptEvent:AddListener("KillInfoUpdate", self, "OnKillInfoUpdate")
end

function LuaCommonKillInfoTopWnd:OnDisable()
    g_ScriptEvent:RemoveListener("KillInfoUpdate", self, "OnKillInfoUpdate")
    self:CancelTick()
end

function LuaCommonKillInfoTopWnd:OnKillInfoUpdate(winnerIsLeft, winerPortrait, loserPortrait,winnerName, loserName)

    if #self.m_Queue > 0 then
        self.m_Queue[#self.m_Queue].time = 1
    end

    table.insert(self.m_Queue,{time = 2 ,winerPortrait = winerPortrait,winnerName = winnerName,winnerIsLeft = winnerIsLeft,loserPortrait = loserPortrait,loserName = loserName})

    if #self.m_Queue == 1 then
        self:OnInfoEnter()
    end
end

function LuaCommonKillInfoTopWnd:GetPlayer(playerId)
    local co = CClientPlayerMgr.Inst:GetPlayer(playerId)
    local cop = TypeAs(co, typeof(CClientOtherPlayer))
    if co and (not cop) then
        cop = TypeAs(co, typeof(CClientMainPlayer))
    end
    return cop
end

function LuaCommonKillInfoTopWnd:CheckInfo()
    local length = #self.m_Queue
    if length > 0 then
        self.m_Queue[length].time = self.m_Queue[length].time - 0.5
        local time = self.m_Queue[length].time
        if time <= 0 then
            self:CancelTick()
            table.remove(self.m_Queue,1)
            self:OnInfoEnd()
        end
    end
end

function LuaCommonKillInfoTopWnd:ShowKillInfo(winerPortrait, winnerName, winnerIsLeft, loserPortrait, loserName)
    self.m_LeftPortrait:LoadNPCPortrait(winerPortrait, false)
    self.m_LeftNameLabel.text = winnerName
    self.m_RightPortrait:LoadNPCPortrait(loserPortrait, false)
    self.m_RightNameLabel.text = loserName
    self.m_LeftBorder.color = winnerIsLeft and self.m_LeftBorderColor or self.m_RightBorderColor
    self.m_RightBorder.color = (not winnerIsLeft) and self.m_LeftBorderColor or self.m_RightBorderColor
    self.m_LeftNameLabel.color = winnerIsLeft and self.m_LeftNameColor or self.m_RightNameColor
    self.m_RightNameLabel.color = (not winnerIsLeft) and self.m_LeftNameColor or self.m_RightNameColor
end

function LuaCommonKillInfoTopWnd:OnInfoEnd()
    self.m_TweenPos:ResetToBeginning()
    self.m_TweenPos:PlayForward()
    self.m_TweenAlpha.duration = self.m_EndAniDelay
    self.m_TweenAlpha:PlayReverse()
end

function LuaCommonKillInfoTopWnd:OnInfoEnter()
    self.m_TweenScale:ResetToBeginning()
    self.m_TweenScale:PlayForward()
    self.m_TweenAlpha.duration = self.m_EnterAniDelay
    self.m_TweenAlpha:ResetToBeginning()
    self.m_TweenAlpha:PlayForward()
    self.m_Fx:LoadFx("fx/ui/prefab/UI_jishafankui.prefab")
    local t = self.m_Queue[1]
    self:ShowKillInfo(t.winerPortrait,t.winnerName, t.winnerIsLeft, t.loserPortrait, t.loserName)
end