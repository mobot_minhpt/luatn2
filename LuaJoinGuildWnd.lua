local UIInput = import "UIInput"
local QnButton = import "L10.UI.QnButton"
local CGuildMgr = import "L10.Game.CGuildMgr"
local CGuildApplyMessageWnd = import "L10.UI.CGuildApplyMessageWnd"
local CGuideMessager = import "L10.Game.Guide.CGuideMessager"
local CGuildSortButton = import "L10.UI.CGuildSortButton"
local QnInput = import "L10.UI.QnInput"
local QnLabel = import "L10.UI.QnLabel"
local Extensions = import "Extensions"
local CGuildLeagueMgr = import "L10.Game.CGuildLeagueMgr"
local Time = import "UnityEngine.Time"

CLuaJoinGuildWnd = class()
RegistClassMember(CLuaJoinGuildWnd, "m_GuildList")
RegistClassMember(CLuaJoinGuildWnd, "m_SearchInputField")
RegistClassMember(CLuaJoinGuildWnd, "m_SearchButton")
RegistClassMember(CLuaJoinGuildWnd, "m_OneKeyRequestButton")
RegistClassMember(CLuaJoinGuildWnd, "m_RequestButton")
RegistClassMember(CLuaJoinGuildWnd, "m_ContactMasterButton")
RegistClassMember(CLuaJoinGuildWnd, "m_GuildAim")
RegistClassMember(CLuaJoinGuildWnd, "m_SortButtonGuildId")
RegistClassMember(CLuaJoinGuildWnd, "sortFlagGuildId")
RegistClassMember(CLuaJoinGuildWnd, "m_SortButtonGrade")
RegistClassMember(CLuaJoinGuildWnd, "sortFlagGrade")
RegistClassMember(CLuaJoinGuildWnd, "m_SortButtonNum")
RegistClassMember(CLuaJoinGuildWnd, "sortFlagNum")
RegistClassMember(CLuaJoinGuildWnd, "currentIndex")
RegistClassMember(CLuaJoinGuildWnd, "searchedGuildId")
RegistClassMember(CLuaJoinGuildWnd, "m_GuildInfos")
RegistClassMember(CLuaJoinGuildWnd, "m_GuildLeagueLabel")

function CLuaJoinGuildWnd:Awake()
    self.m_SortRadioBox = self.transform:Find("Offset/GuildList/Header"):GetComponent(typeof(QnRadioBox))

    self.m_GuildList = self.transform:Find("Offset/GuildList/GuildTable"):GetComponent(typeof(QnTableView))
    self.m_SearchInputField = self.transform:Find("Offset/GuildIdInput"):GetComponent(typeof(QnInput))
    self.m_SearchButton = self.transform:Find("Offset/SearchButton"):GetComponent(typeof(QnButton))
    self.m_OneKeyRequestButton = self.transform:Find("Offset/OneKeyRequest_Button"):GetComponent(typeof(QnButton))
    self.m_RequestButton = self.transform:Find("Offset/Request_Button"):GetComponent(typeof(QnButton))
    self.m_ContactMasterButton = self.transform:Find("Offset/ContactMaster_Button"):GetComponent(typeof(QnButton))
    self.m_GuildAim = self.transform:Find("Offset/GuildAim/GuildAimInput/LabelMission"):GetComponent(typeof(QnLabel))
    self.m_SortButtonGuildId = self.transform:Find("Offset/GuildList/Header/GuildHeader_Id"):GetComponent(typeof(QnButton))
    self.sortFlagGuildId = 1
    self.m_SortButtonGrade = self.transform:Find("Offset/GuildList/Header/GuildHeader_Grade"):GetComponent(typeof(QnButton))
    self.sortFlagGrade = 1
    self.m_SortButtonNum = self.transform:Find("Offset/GuildList/Header/GuildHeader_ApplyNum"):GetComponent(typeof(QnButton))
    self.sortFlagNum = 1
    self.currentIndex = -1
    self.searchedGuildId = -1

    self.m_SortFlags = {-1, -1, -1, -1, -1, -1}
    self.m_GuildInfos = {}

    self.m_GuildLeagueLabel = self.transform:Find("Offset/GuildLeagueData/GuildLeagueInput/LabelLeagueInfo"):GetComponent(typeof(QnLabel))
end
function CLuaJoinGuildWnd:Init()
end

function CLuaJoinGuildWnd:GetGuildCount()
    if self.searchedGuildId > 0 then
        local item = self:_GetSearchedItemData()
        if item ~= nil then
            return 1
        else
            g_MessageMgr:ShowMessage("CUSTOM_STRING", LocalString.GetString("该帮会不存在"))
            self.searchedGuildId = -1
            return #self.m_GuildInfos
        end
    else
        return #self.m_GuildInfos
    end
end

function CLuaJoinGuildWnd:InitGuildItem(item, row)
    local itemData = self:_GetItemDataAt(row)
    -- item:UpdateData(itemData)
    local LabelId = item.transform:Find("LabelId"):GetComponent(typeof(UILabel))
    LabelId.text = tostring(itemData.GuildId)
    local LabelName = item.transform:Find("LabelName"):GetComponent(typeof(UILabel))
    LabelName.text = itemData.Name
    local LabelGrade = item.transform:Find("LabelGrade"):GetComponent(typeof(UILabel))
    LabelGrade.text = tostring(itemData.Grade)
    local LabelNum = item.transform:Find("LabelNum"):GetComponent(typeof(UILabel))
    LabelNum.text = (itemData.MemberNum .. "/") .. itemData.MaxSize
    local LabelLeader = item.transform:Find("LabelLeader"):GetComponent(typeof(UILabel))
    LabelLeader.text = itemData.LeaderName
    local LabelActivity = item.transform:Find("LabelActivity"):GetComponent(typeof(UILabel))
    local avgActivity = 0
    if itemData.MemberNum > 0 then
        avgActivity = math.min(math.floor(itemData.Activity / itemData.MemberNum),9999) 
    end
    LabelActivity.text = tostring(avgActivity)

    item:SetBackgroundTexture(row % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)
end

function CLuaJoinGuildWnd:OnGuildItemSelect(row)
    self.currentIndex = row
    local misson = ""
    local LeagueDataText = ""
    if self.searchedGuildId > 0 then
        local item = self:_GetSearchedItemData()
        if item ~= nil then
            misson = item.Mission
            LeagueDataText = self:ShowLeagueText(item)
        end
    elseif self.m_GuildInfos[row + 1] then
        misson = self.m_GuildInfos[row + 1].Mission
        LeagueDataText = self:ShowLeagueText(self.m_GuildInfos[row + 1])
    end
    self.m_GuildAim.Text = misson
    self.m_GuildLeagueLabel.Text = LeagueDataText

    self:SendClickMessage2()
end

function CLuaJoinGuildWnd:OnSearchButtonClick(btn)
    local idStr = self.m_SearchInputField.Text
    local guildId = tonumber(idStr)
    if guildId and guildId > 0 then
        self.searchedGuildId = guildId
        self.m_GuildList:ReloadData(true, false)
        return
    else
        g_MessageMgr:ShowMessage("Guild_Search_NoResult")
    end

    self.searchedGuildId = -1
    self.m_GuildList:ReloadData(true, false)
end

function CLuaJoinGuildWnd:OnSearchInputValueChanged(s)
    if not Extensions.IsNumeric(s) then
        local uiinput =CommonDefs.GetComponent_Component_Type(self.m_SearchInputField, typeof(UIInput)) 
        uiinput.value = Extensions.NumericString(s)
        g_MessageMgr:ShowMessage("Input_Only_Number")
    end
end

function CLuaJoinGuildWnd:OnReportBtnClick()
    local itemData = self:_GetSelectedItemData()
    if itemData ~= nil then
        LuaPlayerReportMgr:ShowGuildReportWnd(itemData.GuildId, itemData.Name, "banghui",
            LocalString.GetString("帮会举报"), LocalString.GetString("举报帮会"))
    else
        g_MessageMgr:ShowMessage("Guild_Report_Need_Choose_Guild")
    end
end

function CLuaJoinGuildWnd:Start()
    self.m_GuildAim.Text = ""
    self.m_GuildLeagueLabel.Text = ""

    local getCountFunc = function()
        return self:GetGuildCount()
    end
    local initFunc = function(item, row)
        self:InitGuildItem(item,row)
    end 
    self.m_GuildList.m_DataSource = DefaultTableViewDataSource.Create(getCountFunc, initFunc)

    self.m_SearchButton.OnClick =DelegateFactory.Action_QnButton(function(btn)
         self:OnSearchButtonClick(btn)   
    end)

    self.m_OneKeyRequestButton.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:OnOneKeyRequestButtonClick(btn)
    end)

    self.m_RequestButton.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:OnRequestButtonClick(btn)
    end
)
    self.m_ContactMasterButton.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:OnContactMasterButtonClick(btn)
    end)

    self.m_GuildList.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        self:OnGuildItemSelect(row)
    end)

    self.m_SearchInputField.OnValueChanged = DelegateFactory.Action_string(function(s)
        self:OnSearchInputValueChanged(s) 
    end)

    local reportbtn = self.transform:Find("Offset/ReportButton").gameObject
    CommonDefs.AddOnClickListener(reportbtn,DelegateFactory.Action_GameObject(function(go)
        self:OnReportBtnClick()
    end),false)

    self.m_SortRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(button, index)
        self:OnSort(button, index)
    end)
    -- self.m_SortRadioBox:ChangeTo(- 1, true)
end

function CLuaJoinGuildWnd:OnSort(button, index)
    -- id 名称 人数 平均活跃度 帮主 等级
    local sortButton = TypeAs(button, typeof(CGuildSortButton))

    self.m_SortFlags[index + 1] = -self.m_SortFlags[index + 1]
    for i, v in ipairs(self.m_SortFlags) do
        if i ~= index + 1 then
            self.m_SortFlags[i] = -1
        end
    end

    sortButton:SetSortTipStatus(self.m_SortFlags[index + 1] < 0)

    local flag = self.m_SortFlags[index + 1]

    table.sort(self.m_GuildInfos,function(a, b)
        local av = 0
        local bv = 0
        if a then
            av = self:GetValue(a,index)
        end
        if b then
            bv = self:GetValue(b,index)
        end
        if av == bv then 
            return a.GuildId < b.GuildId 
        else
            local ret = 0
            if av < bv then
                ret = flag
            else
                ret = -flag
            end
            return ret < 0
        end
    end)

    self.m_GuildList:ReloadData(true, true)
end

function CLuaJoinGuildWnd:GetValue(data,index)
    -- id 名称 人数 平均活跃度 帮主 等级
    if index == 0 or index == 1 then 
        return data.GuildId
    elseif index == 2 then
        return data.MemberNum
    elseif index == 3 then
        local v = 0
        if data.MemberNum > 0 then
            v = data.Activity / data.MemberNum
        end
        return v
    elseif index == 4 then
        return data.LeaderId
    elseif index == 5 then
        return data.Grade
    end
end

function CLuaJoinGuildWnd:OnContactMasterButtonClick(button)
    local itemData = self:_GetSelectedItemData()
    if itemData ~= nil then
        if CClientMainPlayer.Inst.Id == itemData.LeaderId then
            -- 帮主就是本人
            g_MessageMgr:ShowMessage("GUILD_CONTACT_LEADER_SELF")
            return
        end
        Gac2Gas.RequestContactGuildManager(itemData.GuildId)
    end

    --#region Guide
    self:SendClickMessage()
    --#endregion
end
function CLuaJoinGuildWnd:OnRequestButtonClick(button)
    local itemData = self:_GetSelectedItemData()
    if itemData ~= nil then
        local doubleGuildId = tonumber(itemData.GuildId)
        CGuildApplyMessageWnd.GuildId = doubleGuildId
        CGuildApplyMessageWnd.ApplyAsMemberAll = false
        CUIManager.ShowUI(CUIResources.GuildApplyMessageWnd)
    else
        g_MessageMgr:ShowMessage("GUILD_CHOOSE_ONE")
    end
    --#region Guide
    self:SendClickMessage()
    --#endregion
end
function CLuaJoinGuildWnd:OnOneKeyRequestButtonClick(button)
    local doubleGuildId = tonumber(0)
    CGuildApplyMessageWnd.ApplyAsMemberAll = true
    CUIManager.ShowUI(CUIResources.GuildApplyMessageWnd)
    --#region Guide
    self:SendClickMessage()
    --#endregion
end
local lastTime = 0
function CLuaJoinGuildWnd:OnEnable( )
    g_ScriptEvent:AddListener("SendGuildInfo_AllGuilds", self, "OnGuildAllInfoReceived")
    if Time.realtimeSinceStartup-lastTime>10 then
        CGuildMgr.Inst:GetAllGuilds()
        lastTime = Time.realtimeSinceStartup
    else
        RegisterTickOnce(function()
            g_ScriptEvent:BroadcastInLua("SendGuildInfo_AllGuilds")
        end,100)
    end
end
function CLuaJoinGuildWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendGuildInfo_AllGuilds", self, "OnGuildAllInfoReceived")
end

function CLuaJoinGuildWnd:OnGuildAllInfoReceived()
    self.m_GuildInfos = {}
    local list = CGuildMgr.Inst.AllGuildInfo
    for i = 1, list.Count do
        table.insert(self.m_GuildInfos, list[i - 1])
    end
    self.m_GuildList:ReloadData(true, false)
end

function CLuaJoinGuildWnd:_GetItemDataAt(row)
    if self.searchedGuildId > 0 then
        local item = self:_GetSearchedItemData()
        if item ~= nil then
            return item
        end
    else
        return self.m_GuildInfos[row + 1]
    end
end

function CLuaJoinGuildWnd:_GetSearchedItemData()
    for i, v in ipairs(self.m_GuildInfos) do
        if v.GuildId == self.searchedGuildId then
            return v
        end
    end
end

function CLuaJoinGuildWnd:_GetSelectedItemData()
    if self.searchedGuildId > 0 then
        if self.currentIndex >= 0 then
            return self:_GetSearchedItemData()
        end
    end
    return self.m_GuildInfos[self.currentIndex + 1] and self.m_GuildInfos[self.currentIndex + 1] or nil
end

function CLuaJoinGuildWnd:SendClickMessage()
    local region = self.transform:Find("Offset/JoinGuildRegion").gameObject
    local cmp = CommonDefs.GetComponent_GameObject_Type(region, typeof(CGuideMessager))
    if cmp ~= nil then
        CLuaGuildWnd.applied = true
        cmp:Click()
    end
end
function CLuaJoinGuildWnd:SendClickMessage2()
    local region = self.transform:Find("Offset/GuildListRegion").gameObject
    local cmp = CommonDefs.GetComponent_GameObject_Type(region, typeof(CGuideMessager))
    if cmp ~= nil then
        cmp:Click()
    end
end

function CLuaJoinGuildWnd:ShowLeagueText(GuildInfo)
    if GuildInfo.GuildLeagueLevel == 0 or GuildInfo.GuildLeagueRank == 0 or GuildInfo.GuildLeagueJoinCount == 0 then
        return LocalString.GetString("[FFFFFF7F]暂未参与帮会联赛[-]")
    else
        local levelstring = LuaGuildLeagueMgr.GetLevelStr(GuildInfo.GuildLeagueLevel)
        local rankString = Extensions.ConvertToChineseString(GuildInfo.GuildLeagueRank)
        local winRatio = GuildInfo.GuildLeagueWinCount / GuildInfo.GuildLeagueJoinCount * 100
        return CommonDefs.String_Format_String_ArrayObject(
            LocalString.GetString("[FFFF60]{0}[-]联赛第{1}名\n胜率[FFFF60]{2:f2}%[-] ({3}胜{4}负)"),
            {levelstring, rankString, winRatio, GuildInfo.GuildLeagueWinCount, GuildInfo.GuildLeagueJoinCount - GuildInfo.GuildLeagueWinCount}
        )
    end
end
