local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local CBaseWnd = import "L10.UI.CBaseWnd"
local Time = import "UnityEngine.Time"
local CUIWebTexture = import "L10.UI.CUIWebTexture"
local CCommonUITween = import "L10.UI.CCommonUITween"
local EnumPlayMode = import "L10.UI.CCommonUITween+EnumPlayMode"

LuaGMBubblePopupWnd = class()

RegistChildComponent(LuaGMBubblePopupWnd, "m_Icon", "Icon", CUITexture)
RegistChildComponent(LuaGMBubblePopupWnd, "m_TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaGMBubblePopupWnd, "m_ContentLabel", "ContentLabel", UILabel)
RegistChildComponent(LuaGMBubblePopupWnd, "m_ContentImage", "ContentImage", CUIWebTexture)
RegistChildComponent(LuaGMBubblePopupWnd, "m_CountDownWidget", "CountDownWidget", UIWidget)
RegistChildComponent(LuaGMBubblePopupWnd, "m_TipLabel", "TipLabel", UILabel)
RegistChildComponent(LuaGMBubblePopupWnd, "m_WndBg", "Wnd_Bg", GameObject)
RegistChildComponent(LuaGMBubblePopupWnd, "m_CloseButton", "CloseButton", GameObject)

RegistClassMember(LuaGMBubblePopupWnd, "m_BubblePopupInfo")
RegistClassMember(LuaGMBubblePopupWnd, "m_CountDownTick")
RegistClassMember(LuaGMBubblePopupWnd, "m_StartTime")
RegistClassMember(LuaGMBubblePopupWnd, "m_Duration")
RegistClassMember(LuaGMBubblePopupWnd, "m_IsClosing")

function LuaGMBubblePopupWnd:Awake()
    UIEventListener.Get(self.m_WndBg).onClick = DelegateFactory.VoidDelegate(function() self:OnWndBgClick() end)
    UIEventListener.Get(self.m_CloseButton).onClick = DelegateFactory.VoidDelegate(function() self:OnCloseButtonClick() end)
end

function LuaGMBubblePopupWnd:Init()
    self.m_IsClosing = false
    self.m_BubblePopupInfo = LuaGMMgr.m_BubblePopupInfo
    self.m_Icon:LoadMaterial(LuaGMMgr:GetIconByBubblePopupType())
    self.m_TitleLabel.text = self.m_BubblePopupInfo.title
    local imageUrl = self.m_BubblePopupInfo.content_image
    if imageUrl and imageUrl~="" then
        self.m_ContentLabel.text = ""
    else
        self.m_ContentLabel.text = self.m_BubblePopupInfo.content_text
    end
    self.m_ContentImage:Init(imageUrl)
    self.m_TipLabel.text = LuaGMMgr:GetTipByBubblePopupType()

    self.m_StartTime = Time.realtimeSinceStartup
    self.m_Duration = self.m_BubblePopupInfo.duration
    self.m_CountDownWidget.fillAmount = 1
    self:StartCountDownTick()
end

function LuaGMBubblePopupWnd:OnDestroy()
    self:StopCountDownTick()
end

function LuaGMBubblePopupWnd:StartCountDownTick()
    self:StopCountDownTick()
    self.m_CountDownTick = RegisterTick(function()
        local pastTime = Time.realtimeSinceStartup - self.m_StartTime
        if pastTime>=self.m_Duration then
            self:Close()
            return
        end

        self.m_CountDownWidget.fillAmount = math.max(0, self.m_Duration-pastTime) / self.m_Duration
    end, 100)
end

function LuaGMBubblePopupWnd:StopCountDownTick()
    if self.m_CountDownTick then
        UnRegisterTick(self.m_CountDownTick)
        self.m_CountDownTick = nil
    end
end

function LuaGMBubblePopupWnd:OnWndBgClick()
    LuaGMMgr:DoActionByBubblePopupType()
    self:Close() 
end

function LuaGMBubblePopupWnd:OnCloseButtonClick()
    LuaGMMgr:RecordBubblePopupLog(self.m_BubblePopupInfo.type, self.m_BubblePopupInfo.urlOrText, EnumGMBubblePopupActionType.eClose)
    self:Close()
end

function LuaGMBubblePopupWnd:Close()
    if self.m_IsClosing then return end
    self.m_IsClosing = true
    local offsetRoot = self.transform:Find("Anchor/Offset")
    local uiTween = offsetRoot and offsetRoot:GetComponent(typeof(CCommonUITween)) or nil
    if uiTween and uiTween.playMode==EnumPlayMode.PlayOnEnable then
        uiTween:PlayDisapperAnimation(DelegateFactory.Action(function()
            self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
        end))
    else
        self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
    end
end

