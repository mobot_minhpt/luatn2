-- Auto Generated!!
local CConversationMgr = import "L10.Game.CConversationMgr"
local CKeJuMgr = import "L10.Game.CKeJuMgr"
local CKeJuReplayWnd = import "L10.UI.CKeJuReplayWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local CTickMgr = import "L10.Engine.CTickMgr"
local EnumEventType = import "EnumEventType"
local ETickType = import "L10.Engine.ETickType"
local EventManager = import "EventManager"
local KeJu_NPCTalk = import "L10.Game.KeJu_NPCTalk"
local KeJu_Setting = import "L10.Game.KeJu_Setting"
local L10 = import "L10"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CKeJuReplayWnd.m_Init_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this)
    if CKeJuMgr.Inst.replayLastIndex <= 0 then
        this:ReplayFromStart()
    else
        this:ReplayFromContinue()
    end
end
CKeJuReplayWnd.m_ReplayFromStart_CS2LuaHook = function (this) 
    CommonDefs.ListClear(CKeJuMgr.Inst.rankList)
    this.danmuTimestamp = 0
    this:DoTickCancel()
    this.cancelTick = CTickMgr.Register(MakeDelegateFromCSFunction(this.OnTick, Action0, this), 1000, ETickType.Loop)
    this.countDownLabel.transform.localPosition = this.countDownPos2
    this.huishi:InitHuishiCountDown(this.questionTemplate)
    CKeJuMgr.Inst.replayLastIndex = 1
    this:StartDianshiCountDown(KeJu_Setting.GetData().HuiShiPrepareDuration)
end
CKeJuReplayWnd.m_ReplayFromContinue_CS2LuaHook = function (this) 
    CommonDefs.ListClear(CKeJuMgr.Inst.rankList)
    this.countDownLabel.text = ""
    local temp = KeJu_Setting.GetData().HuiShiPrepareDuration + CKeJuMgr.Inst.replayLastIndex * KeJu_Setting.GetData().HuiShiQuestionInterval
    this.danmuTimestamp = math.floor(tonumber(temp or 0))
    this:DoTickCancel()
    this.cancelTick = CTickMgr.Register(MakeDelegateFromCSFunction(this.OnTick, Action0, this), 1000, ETickType.Loop)
    this:RequestQuestion()
    this.huishi:InitHuishi(this.questionTemplate)
end
CKeJuReplayWnd.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListener(EnumEventType.KeJuReplayQuestionSend, MakeDelegateFromCSFunction(this.OnKejuReplayQuestionSend, Action0, this))
    EventManager.RemoveListener(EnumEventType.KeJuHuiShiFinished, MakeDelegateFromCSFunction(this.ShowDialog, Action0, this))
    EventManager.RemoveListener(EnumEventType.KejuReplayEnd, MakeDelegateFromCSFunction(this.OnDianshiEnd, Action0, this))

    this:DoCancel()
    this:DoTickCancel()
    this:DoCancelDianshiInterval()
    this:DoClearData()
end
CKeJuReplayWnd.m_ShowDialog_CS2LuaHook = function (this) 

    this.countDownLabel.transform.localPosition = this.countDownPos2
    this.huishi:InitHuishi(this.questionTemplate)
    this.huishi:DisableCountDown()
    local randomIndex = UnityEngine_Random(1, KeJu_NPCTalk.GetDataCount())
    CConversationMgr.Inst:OpenStoryDialogWithAutoPlay(KeJu_NPCTalk.GetData(randomIndex).Value, true, 3000, nil)

    this:DoCancelDianshiInterval()
    --cancelDianshiInterval = CTickMgr.Register(RequestQuestion, (uint)KeJu_Setting.GetData().DianShiPrepareDuration * 2 * 1000, ETickType.Once);
    CKeJuMgr.Inst.replayLastIndex = 21
    this.countDownLabel.transform.localPosition = this.countDownPos1
    this:StartDianshiCountDown(KeJu_Setting.GetData().DianShiPrepareDuration * 2)
end
CKeJuReplayWnd.m_DoClearData_CS2LuaHook = function (this) 
    if CKeJuMgr.Inst.cancelReplayNext ~= nil then
        invoke(CKeJuMgr.Inst.cancelReplayNext)
    end
    if CKeJuMgr.Inst.cancelReplayAnswer ~= nil then
        invoke(CKeJuMgr.Inst.cancelReplayAnswer)
    end
end
CKeJuReplayWnd.m_StartDianshiCountDown_CS2LuaHook = function (this, count) 

    if this.cancel ~= nil then
        invoke(this.cancel)
    end
    this.cancel = L10.Engine.CTickMgr.Register(MakeDelegateFromCSFunction(this.DianshiCountDown, Action0, this), 1000, ETickType.Loop)
    this.countDown = count
    this:DianshiCountDown()
end
CKeJuReplayWnd.m_DianshiCountDown_CS2LuaHook = function (this) 

    this.countDown = this.countDown - 1
    if this.countDown <= 0 then
        this:DoCancel()
        this.countDownLabel.text = ""
        this:RequestQuestion()
    else
        this.countDownLabel.text = tostring(this.countDown)
    end
end
