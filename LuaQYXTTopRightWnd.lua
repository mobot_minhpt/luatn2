local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local CUIFx = import "L10.UI.CUIFx"
local Vector3 = import "UnityEngine.Vector3"

CLuaQYXTTopRightWnd = class()

RegistChildComponent(CLuaQYXTTopRightWnd, "ExpandButton", GameObject)
RegistChildComponent(CLuaQYXTTopRightWnd, "StageInfo1", GameObject)
RegistChildComponent(CLuaQYXTTopRightWnd, "RoundLabel", UILabel)
RegistChildComponent(CLuaQYXTTopRightWnd, "Flower1", CUITexture)
RegistChildComponent(CLuaQYXTTopRightWnd, "Flower2", CUITexture)
RegistChildComponent(CLuaQYXTTopRightWnd, "StageInfo2", GameObject)
RegistChildComponent(CLuaQYXTTopRightWnd, "ScoreLabel", UILabel)
RegistChildComponent(CLuaQYXTTopRightWnd, "RankLabel", UILabel)
RegistChildComponent(CLuaQYXTTopRightWnd, "ReviveLabel", UILabel)
RegistChildComponent(CLuaQYXTTopRightWnd, "FxNode", CUIFx)
RegistChildComponent(CLuaQYXTTopRightWnd, "StageView", GameObject)

RegistClassMember(CLuaQYXTTopRightWnd,"m_CurStage")
RegistClassMember(CLuaQYXTTopRightWnd,"m_TargetFlowers")
RegistClassMember(CLuaQYXTTopRightWnd,"m_FlowerIcons")
RegistClassMember(CLuaQYXTTopRightWnd,"m_RefreshRoundFx")
RegistClassMember(CLuaQYXTTopRightWnd,"m_CurRound")

function CLuaQYXTTopRightWnd:OnEnable()
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("RefreshQYXTProgressInfo",self,"OnRefreshQYXTProgressInfo")
end

function CLuaQYXTTopRightWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("RefreshQYXTProgressInfo",self,"OnRefreshQYXTProgressInfo")
end

function CLuaQYXTTopRightWnd:Awake()
    self.m_CurStage = 1
    self.m_CurRound = 0
    self.m_TargetFlowers = {}

    self.m_FlowerIcons = {}

    local data = Valentine2020_Setting.GetData()
    for i=0,data.XianHuaNPCIcon.Length-1,2 do
        local id = tonumber(data.XianHuaNPCIcon[i])
        local icon = data.XianHuaNPCIcon[i+1]
        self.m_FlowerIcons[id] = icon
    end
    
    UIEventListener.Get(self.ExpandButton).onClick = DelegateFactory.VoidDelegate(function (p)
        self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
        self.StageView:SetActive(false)
        CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
    end)
end

function CLuaQYXTTopRightWnd:Init()
    Gac2Gas.RequestQYXTProgressInfo()
end

function CLuaQYXTTopRightWnd:RefreshStage1(list)
    if self.m_CurStage ~= 1 then
        self.StageInfo1:SetActive(true)
        self.StageInfo2:SetActive(false)
    end

    local round = list[0]
    local flowerId1 = list[1]
    local isCollected1 = list[2]
    local flowerId2 = list[3]
    local isCollected2 = list[4]

    local icon1 = self.m_FlowerIcons[flowerId1]
    local icon2 = self.m_FlowerIcons[flowerId2]
 
    self.Flower1:LoadMaterial(icon1)
    self.Flower2:LoadMaterial(icon2)
    self.Flower1.transform:Find("Collected").gameObject:SetActive(isCollected1)
    self.Flower2.transform:Find("Collected").gameObject:SetActive(isCollected2)

    if  isCollected1 and isCollected2 then
        self.FxNode:DestroyFx()
        self.FxNode:LoadFx("Fx/UI/Prefab/UI_qingyixiangtong_luncishuaxin.prefab")
    end
    local maxRound = Valentine2020_Setting.GetData().PickMaxRound
    self.RoundLabel.text = SafeStringFormat3(LocalString.GetString("第%d/%d轮"), round,maxRound)
    self.m_CurStage = 1
end

function CLuaQYXTTopRightWnd:RefreshStage2(list)
    if self.m_CurStage ~= 2 then
        self.StageInfo1:SetActive(false)
        self.StageInfo2:SetActive(true)
        self.ReviveLabel.gameObject:SetActive(false)
    end

    local score = list[0]
    local rank = list[1]
    
    self.ScoreLabel.text = score
    self.RankLabel.text = rank
    self.m_CurStage = 2
end

function CLuaQYXTTopRightWnd:OnRefreshQYXTProgressInfo(stage, progressInfoUD)
    local list = MsgPackImpl.unpack(progressInfoUD)
    if stage == 1 then
        self:RefreshStage1(list)
    elseif stage == 2 then
        self:RefreshStage2(list)
    end
end

function CLuaQYXTTopRightWnd:OnHideTopAndRightTipWnd( )
    self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
    self.StageView:SetActive(true)
end
