local GameObject = import "UnityEngine.GameObject"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local QnRadioBox = import "L10.UI.QnRadioBox"

LuaShifuPingJiaWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShifuPingJiaWnd, "ReadMeLabel", "ReadMeLabel", UILabel)
RegistChildComponent(LuaShifuPingJiaWnd, "Button1", "Button1", GameObject)
RegistChildComponent(LuaShifuPingJiaWnd, "Button2", "Button2", GameObject)
RegistChildComponent(LuaShifuPingJiaWnd, "Button3", "Button3", GameObject)

--@endregion RegistChildComponent end

function LuaShifuPingJiaWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.Button1.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButton1Click()
	end)

	UIEventListener.Get(self.Button2.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButton2Click()
	end)

	UIEventListener.Get(self.Button3.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButton3Click()
	end)

    --@endregion EventBind end
end

function LuaShifuPingJiaWnd:Init()
    self.ReadMeLabel.text = g_MessageMgr:FormatMessage("ShifuPingJiaWnd_ReadMe")
end

function LuaShifuPingJiaWnd:OnQnRadioBoxSelected(index)
    Gac2Gas.RequestSetShifuPingJia(index)
    CUIManager.CloseUI(CLuaUIResources.ShifuPingJiaWnd)
end
--@region UIEvent

function LuaShifuPingJiaWnd:OnButton1Click()
    self:OnQnRadioBoxSelected(1)
end

function LuaShifuPingJiaWnd:OnButton2Click()
    self:OnQnRadioBoxSelected(2)
end

function LuaShifuPingJiaWnd:OnButton3Click()
    self:OnQnRadioBoxSelected(3)
end


--@endregion UIEvent

