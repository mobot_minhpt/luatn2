-- Auto Generated!!
local CButton = import "L10.UI.CButton"
local CChatMgr = import "L10.Game.CChatMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CHuanLingShareWnd = import "L10.UI.CHuanLingShareWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local File = import "System.IO.File"
local GameObject = import "UnityEngine.GameObject"
local LoadPicMgr = import "L10.Game.LoadPicMgr"
local Object = import "System.Object"
local String = import "System.String"
local Texture2D = import "UnityEngine.Texture2D"
CHuanLingShareWnd.m_Init_CS2LuaHook = function (this) 
    this.loadingArea:SetActive(true)
    local message = g_MessageMgr:FormatMessage("Pic_Is_Loading")
    if message ~= nil then
        this.loadingHint.text = message
    end

	if CommonDefs.IS_VN_CLIENT then
		this.shareBtn:SetActive(false)
		this.shareInput.gameObject:SetActive(false)
	end
end
CHuanLingShareWnd.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListenerInternal(EnumEventType.OnGetLingFuShareImage, MakeDelegateFromCSFunction(this.OnGetLingFuShareImage, MakeGenericClass(Action1, String), this))

    if this.tex ~= nil then
        GameObject.Destroy(this.tex)
        this.tex = nil
    end
end
CHuanLingShareWnd.m_OnGetLingFuShareImage_CS2LuaHook = function (this, url) 
    this.loadingArea:SetActive(false)
    local picPath = Utility.persistentDataPath .. "/LingFuShare.jpg"
    if File.Exists(picPath) then
        local data = File.ReadAllBytes(picPath)
        this.tex = CreateFromClass(Texture2D, 1334, 750)
        CommonDefs.LoadImage(this.tex,data)
        this.tex:Apply()
        this.tex.name = "ShareTexture"
        this.sharePic.mainTexture = this.tex
    end
    this.m_ShareUrl = url
end
CHuanLingShareWnd.m_OnShareBtnClicked_CS2LuaHook = function (this, go) 

    local shareTexture = TypeAs(this.sharePic.mainTexture, typeof(Texture2D))
    local shareText = this.shareInput.value
    shareText = CChatMgr.Inst:FilterYangYangEmotion(shareText)

    if shareTexture == nil then
        return
    end

    local textureArray = CreateFromClass(MakeArrayClass(System.String), 1)
    CommonDefs.GetComponent_GameObject_Type(this.shareBtn, typeof(CButton)).Enabled = false

    textureArray[0] = this.m_ShareUrl

    -- 先上传
    LoadPicMgr.UploadPic(CPersonalSpaceMgr.Upload_MomentPhotoType,CommonDefs.EncodeToPNG(shareTexture), DelegateFactory.Action_string(function (url) 
        CPersonalSpaceMgr.Inst:AddMoment(shareText, textureArray, nil, DelegateFactory.Action_string_CPersonalSpace_AdvRet(function (textAfterFilter, data) 
            if data.code == 0 then
                if CPersonalSpaceMgr.Inst.OnShareTextComplete ~= nil then
                    GenericDelegateInvoke(CPersonalSpaceMgr.Inst.OnShareTextComplete, Table2ArrayWithCount({shareText}, 1, MakeArrayClass(Object)))
                end

                if CClientMainPlayer.Inst ~= nil then
                    CPersonalSpaceMgr.Inst:OpenPersonalSpaceWnd(CClientMainPlayer.Inst.Id, 0)
                end
            else
                g_MessageMgr:ShowMessage("CUSTOM_STRING2", data.msg)
            end
            if not this then
                return
            end
            g_MessageMgr:ShowMessage("MENGDAO_TUPIAN_SHANGCHUANCHENGGONG")
            EventManager.BroadcastInternalForLua(EnumEventType.PersonalSpaceAddNewMoment, {textAfterFilter, data})
            this:Close()
        end), DelegateFactory.Action(function () 
            if not this then
                return
            end
            CommonDefs.GetComponent_GameObject_Type(this.shareBtn, typeof(CButton)).Enabled = false
            --MessageMgr.Inst.ShowMessage("JINGLING_MENGDAO_SHARE_FAILED");
        end), false)
    end))
end
