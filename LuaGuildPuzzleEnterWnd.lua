local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CRankData = import "L10.UI.CRankData"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CGuildMgr=import "L10.Game.CGuildMgr"

LuaGuildPuzzleEnterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGuildPuzzleEnterWnd, "OpenTimeLabel", "OpenTimeLabel", UILabel)
RegistChildComponent(LuaGuildPuzzleEnterWnd, "StateLabel", "StateLabel", UILabel)
RegistChildComponent(LuaGuildPuzzleEnterWnd, "Middle", "Middle", GameObject)
RegistChildComponent(LuaGuildPuzzleEnterWnd, "EnterBtn", "EnterBtn", GameObject)
RegistChildComponent(LuaGuildPuzzleEnterWnd, "RankBtn", "RankBtn", GameObject)
RegistChildComponent(LuaGuildPuzzleEnterWnd, "GetBtnAlert", "GetBtnAlert", GameObject)
RegistChildComponent(LuaGuildPuzzleEnterWnd, "RewardBtn", "RewardBtn", GameObject)
RegistChildComponent(LuaGuildPuzzleEnterWnd, "RuleBtn", "RuleBtn", GameObject)
RegistChildComponent(LuaGuildPuzzleEnterWnd, "EnterBtnLabel", "EnterBtnLabel", UILabel)
RegistChildComponent(LuaGuildPuzzleEnterWnd, "DetailBtn", "DetailBtn", GameObject)
RegistChildComponent(LuaGuildPuzzleEnterWnd, "Item", "Item", GameObject)

--@endregion RegistChildComponent end

function LuaGuildPuzzleEnterWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.EnterBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnEnterBtnClick()
	end)

	UIEventListener.Get(self.RankBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRankBtnClick()
	end)

	UIEventListener.Get(self.RewardBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRewardBtnClick()
	end)

	UIEventListener.Get(self.RuleBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleBtnClick()
	end)

	UIEventListener.Get(self.DetailBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDetailBtnClick()
	end)

    --@endregion EventBind end
end

function LuaGuildPuzzleEnterWnd:Init()
    self.OpenTimeLabel.text = g_MessageMgr:FormatMessage("Guild_Puzzle_Open_Time")
    self.m_SelectPinTuData = nil
    self.Item:SetActive(false)

    -- 初始化界面
    self:InitMain(CLuaGuildMgr.m_PinTuStage, CLuaGuildMgr.m_PinTuDuration, CLuaGuildMgr.m_PinTuDataList)

    -- 打开界面可能发起请求
    if CLuaGuildMgr.m_PinTuNeedOpenId then
        Gac2Gas.RequestEnterGuildPinTu(CLuaGuildMgr.m_PinTuNeedOpenId)
    end

    if #CLuaGuildMgr.m_PinTuDataList > 0 then
        self:SetDefalutSelected(CLuaGuildMgr.m_PinTuDataList)
    end

    self:InitReward()

    CGuildMgr.Inst:GetMyGuildInfo()
end

function LuaGuildPuzzleEnterWnd:InitReward()
    self.transform:Find("RewardPopUp").gameObject:SetActive(false)

    local itemId = {
		-- 帮贡
		21000620,
		-- 修为银票
		21006698,
		-- 经验
		21000618,
    }

    
    local baseTable = self.transform:Find("RewardPopUp/BaseReward/Table")
    local topTable = self.transform:Find("RewardPopUp/TopReward/Table") 

    local bg = self.transform:Find("RewardPopUp/ClickBg").gameObject
    UIEventListener.Get(bg).onClick = DelegateFactory.VoidDelegate(function()
        self.transform:Find("RewardPopUp").gameObject:SetActive(false)
    end)

	for i=1, 3 do
		local item = baseTable:Find(tostring(i)).gameObject
		UIEventListener.Get(item).onClick = DelegateFactory.VoidDelegate(function()
			CItemInfoMgr.ShowLinkItemTemplateInfo(itemId[i], false, nil, AlignType.Default, 0, 0, 0, 0)
		end)

        local itemData = Item_Item.GetData(itemId[i])
        item.transform:Find("Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(itemData.Icon)
	end

    -- 改为读表
    local topItemId = GuildPinTu_GameSetting.GetData().GuildTitleItemId
    for i=1, 4 do
		local item = topTable:Find(tostring(i)).gameObject
        local itemId = topItemId[i-1]
		UIEventListener.Get(item).onClick = DelegateFactory.VoidDelegate(function()
			CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
		end)

        local itemData = Item_Item.GetData(itemId)
        item.transform:Find("Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(itemData.Icon)
	end
end

function LuaGuildPuzzleEnterWnd:SetDefalutSelected(pinTuDataList)
    local curSelect = 1
    local curSumValue = -1
    for i = 1, 3 do
        local pinTu = pinTuDataList[i]

        if pinTu then
            local sum = self:GetNum(pinTu.setchips)
            if sum < 20 and sum > curSumValue then
                curSelect = i
                curSumValue = sum
            end
        end
    end

    self:SetSelected(curSelect)
end

function LuaGuildPuzzleEnterWnd:SetSelected(curSelect)
    for i =1, 3 do
        self.Middle.transform:Find("Puzzle".. tostring(i).."/Selected").gameObject:SetActive(i == curSelect)
    end
    -- 设置当前选中数据
    self.m_SelectPinTuData = CLuaGuildMgr.m_PinTuDataList[curSelect]
    if self.m_SelectPinTuData then
        local sum = self:GetNum(self.m_SelectPinTuData.setchips)

        if CLuaGuildMgr.m_PinTuStage == EnumGuildPinTuPlayStage.eOpen then
            if sum == 20 then
                self.EnterBtnLabel.text = LocalString.GetString("查看迷卷")
                self.GetBtnAlert:SetActive(false)
            else
                self.EnterBtnLabel.text = LocalString.GetString("进入迷卷")
                self.GetBtnAlert:SetActive(true)
            end
        end
    end
    self.m_SelectIndex = curSelect
end

function LuaGuildPuzzleEnterWnd:InitMain(stage, duration, pinTuDataList)
    self.m_HasPinTuFinish = true
    -- 初始化拼图区域
    for i = 1, 3 do
        local pinTu = pinTuDataList[i]
        self:InitPuzzle(i, pinTu)
    end

    local stateNumLabel = self.StateLabel.transform:Find("Label"):GetComponent(typeof(UILabel))
    stateNumLabel.text = ""

    self.GetBtnAlert:SetActive(false)

    UnRegisterTick(self.m_Tick)
    if stage == EnumGuildPinTuPlayStage.eNotOpen then
        -- 未开启
        CUICommonDef.SetActive(self.EnterBtn.gameObject, false, true)
        self.EnterBtnLabel.text = LocalString.GetString("未开启")
        self.StateLabel.gameObject:SetActive(false)
    elseif stage == EnumGuildPinTuPlayStage.eCountDown then
        -- 倒计时
        CUICommonDef.SetActive(self.EnterBtn.gameObject, false, true)
        self.EnterBtnLabel.text = SafeStringFormat3(LocalString.GetString("%s后开始") ,CUICommonDef.SecondsToTimeString(duration, true))
        self.StateLabel.gameObject:SetActive(false)

        self.m_Tick = RegisterTick(function()
            CLuaGuildMgr.m_PinTuDuration = CLuaGuildMgr.m_PinTuDuration - 1
            if CLuaGuildMgr.m_PinTuDuration < 0 then
                CLuaGuildMgr.m_PinTuDuration = 0
            end
            self.EnterBtnLabel.text = SafeStringFormat3(LocalString.GetString("%s后开始") ,CUICommonDef.SecondsToTimeString(CLuaGuildMgr.m_PinTuDuration, true))
        end, 1000)
    elseif stage == EnumGuildPinTuPlayStage.eOpen then
        -- 开启玩法
        self.GetBtnAlert:SetActive(not self.m_HasPinTuFinish)
        CUICommonDef.SetActive(self.EnterBtn.gameObject, true, true)

        if self.m_HasPinTuFinish then
            self.EnterBtnLabel.text = LocalString.GetString("查看迷卷")
        else
            self.EnterBtnLabel.text = LocalString.GetString("进入迷卷")
        end

        -- 刷新一下
        if self.m_SelectIndex then
            self:SetSelected(self.m_SelectIndex)
        end

        self.StateLabel.gameObject:SetActive(true)
        self.StateLabel.text = CUICommonDef.SecondsToTimeString(duration, true)
        stateNumLabel.text = LocalString.GetString("拼图进行中")

        -- 判断是否已经拼完
        local hasFinsih = true
        for _, data in ipairs(pinTuDataList) do
            local sum = self:GetNum(data.setchips)
            if sum < 20 then
                hasFinsih = false
                break
            end
        end

        if hasFinsih then
            -- 已全部完成拼图的提示
            self.StateLabel.text = ""
            stateNumLabel.text = LocalString.GetString("本月已完成，请等待下月拼图更新")
        else
            self.m_Tick = RegisterTick(function()
                CLuaGuildMgr.m_PinTuDuration = CLuaGuildMgr.m_PinTuDuration - 1
                if CLuaGuildMgr.m_PinTuDuration < 0 then
                    CLuaGuildMgr.m_PinTuDuration = 0
                end
                self.StateLabel.text = CUICommonDef.SecondsToTimeString(CLuaGuildMgr.m_PinTuDuration, true)
            end, 1000)
        end
        
    elseif stage == EnumGuildPinTuPlayStage.eEnd then
        -- 已结束
        CUICommonDef.SetActive(self.EnterBtn.gameObject, false, true)
        self.EnterBtnLabel.text = LocalString.GetString("已结束")
        self.StateLabel.gameObject:SetActive(false)
    end
end

-- index从1开始
function LuaGuildPuzzleEnterWnd:HasSetChip(index, setchips)
    local result = bit.band(bit.lshift(1 ,index-1), setchips)
    return result > 0
end

-- 获取已经拿到的拼图数量
function LuaGuildPuzzleEnterWnd:GetNum(getchips)
    local sum = 0
    for i = 1,20 do
        local result = bit.band(bit.lshift(1 ,i-1), getchips)
        if result > 0 then
            sum = sum + 1
        end
    end
    return sum
end

-- id getchips setchips answered answer finishtime pinTuPlayerCount
function LuaGuildPuzzleEnterWnd:InitPuzzle(index, pinTuData)
    local sum = 0
    local item = self.Middle.transform:Find("Puzzle".. tostring(index)).gameObject

    if pinTuData then
        --拼图图片与偏移
        local id = tonumber(pinTuData.id)

        local picData = GuildPinTu_Picture.GetData(tonumber(pinTuData.id))
        local picRes = picData.Res

        local offset = picData.Offset

        item.transform:Find("Puzzle/Puzzle/PicTable").localPosition = Vector3(offset[0], offset[1], 0)
        self.Middle.transform:Find("Puzzle".. tostring(index).."/Selected").gameObject:SetActive(self.m_SelectPinTuData ~= nil and self.m_SelectPinTuData == CLuaGuildMgr.m_PinTuDataList[index])
        -- 20块拼图都安装上
        local table = item.transform:Find("Puzzle/Puzzle/PicTable"):GetComponent(typeof(UITable))
        Extensions.RemoveAllChildren(table.transform)
        
        for i=1, 20 do
            local g = NGUITools.AddChild(table.gameObject, self.Item)
            g:SetActive(true)

            local texture = g.transform:Find("Puzzle"):GetComponent(typeof(CUITexture))
            local matPath = SafeStringFormat3("UI/Texture/PinTu/material/pintu_%02d_%02d.mat", id,  i)

            if self:HasSetChip(i, pinTuData.setchips) then
                sum = sum + 1
                texture:LoadMaterial(matPath)
            else
                texture:LoadMaterial("")
            end
        end
        table:Reposition()
    else

        item.transform:Find("Puzzle/Puzzle/PicTable").localPosition = Vector3(-300, 400, 0)
        -- 20块拼图都安装上
        local table = item.transform:Find("Puzzle/Puzzle/PicTable"):GetComponent(typeof(UITable))
        Extensions.RemoveAllChildren(table.transform)
        
        for i=1, 20 do
            local g = NGUITools.AddChild(table.gameObject, self.Item)
            g:SetActive(true)

            local texture = g.transform:Find("Puzzle"):GetComponent(typeof(CUITexture))
            texture:LoadMaterial("")
        end
        table:Reposition()
    end

    -- 几个显示
    local percentLabel = item.transform:Find("Other/Info/PercentLabel"):GetComponent(typeof(UILabel))
    local playerCountLabel = item.transform:Find("Other/Info/PlayerCountLabel"):GetComponent(typeof(UILabel))
    local playerCountIcon = item.transform:Find("Other/Info/Icon")
    local residueLabel = item.transform:Find("Other/ResidueLabel"):GetComponent(typeof(UILabel))
    local progressBar = item.transform:Find("Other/ProgressBar"):GetComponent(typeof(UISlider))

    percentLabel.text = SafeStringFormat3("%s%%", tostring(sum * 100 / 20))

    if sum == 20 then
        percentLabel.color = Color(30/255, 143/255, 80/255)
        progressBar.transform:Find("Foreground"):GetComponent(typeof(CUITexture)):LoadMaterial("UI/Texture/Transparent/Material/guildpuzzlewnd_jindutiao_g.mat")
    else
        percentLabel.color = Color(117/255, 94/255, 3/255)
        progressBar.transform:Find("Foreground"):GetComponent(typeof(CUITexture)):LoadMaterial("UI/Texture/Transparent/Material/guildpuzzlewnd_jindutiao_y.mat")
    end
    
    progressBar.value = sum / 20

    if sum < 20 then
        self.m_HasPinTuFinish = false
    end

    -- 玩家数量
    local maxPlayer = GuildPinTu_GameSetting.GetData().MaxPlayerOnePinTu
    local curPlayer = 0
    if pinTuData then
        curPlayer = pinTuData.pinTuPlayerCount
    end

    if curPlayer == maxPlayer then
        -- 红色
        playerCountLabel.text = SafeStringFormat3("[c][FF0000]%s[-][/c]/%s", curPlayer, maxPlayer)
    else
        -- 绿色
        playerCountLabel.text = SafeStringFormat3("[c][1E8F50]%s[-][/c]/%s", curPlayer, maxPlayer)
    end

    if self.m_HasPinTuFinish or pinTuData == nil or CLuaGuildMgr.m_PinTuStage ~= EnumGuildPinTuPlayStage.eOpen then
        -- 未开启或者已完成 不显示人数
        playerCountLabel.gameObject:SetActive(false)
        playerCountIcon.gameObject:SetActive(false)
    else
        playerCountLabel.gameObject:SetActive(true)
        playerCountIcon.gameObject:SetActive(true)
    end
    
    -- 剩余数量
    local residueNum = 0
    if pinTuData then
        residueNum = self:GetNum(pinTuData.getchips)
    end

    if residueNum == 20 then
        residueLabel.text = SafeStringFormat3(LocalString.GetString("剩余碎片 [c][FF0000]%s[-][/c]"), tostring(20 - residueNum))
    else
        residueLabel.text = SafeStringFormat3(LocalString.GetString("剩余碎片 [c][1E8F50]%s[-][/c]"), tostring(20 - residueNum))
    end
    residueLabel.gameObject:SetActive(not self.m_HasPinTuFinish)
    
    local btn = item.transform:Find("Other").gameObject
    UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(function()
        self:SetSelected(index)
    end)
end

function LuaGuildPuzzleEnterWnd:OnQueryGuildPinTuDataResult(stage, duration, pinTuDataList)
    self:InitMain(stage, duration, pinTuDataList)
end

-- 玩家数量变更，进入
function LuaGuildPuzzleEnterWnd:OnPlayerEnterGuildPinTu(playerId, pictureId, pinTuPlayerCount)
    for _, data in ipairs(CLuaGuildMgr.m_PinTuDataList) do
        if data.id == pictureId then
            data.pinTuPlayerCount = pinTuPlayerCount
        end
    end
    self:InitMain(CLuaGuildMgr.m_PinTuStage, CLuaGuildMgr.m_PinTuDuration, CLuaGuildMgr.m_PinTuDataList)
end

-- 玩家数量变更，退出
function LuaGuildPuzzleEnterWnd:OnPlayerLeaveGuildPinTu(playerId, pictureId, pinTuPlayerCount)
    for _, data in ipairs(CLuaGuildMgr.m_PinTuDataList) do
        if data.id == pictureId then
            data.pinTuPlayerCount = pinTuPlayerCount
        end
    end
    self:InitMain(CLuaGuildMgr.m_PinTuStage, CLuaGuildMgr.m_PinTuDuration, CLuaGuildMgr.m_PinTuDataList)
end

-- 游戏开始
function LuaGuildPuzzleEnterWnd:OnGuildPinTuPlayStart()
    CLuaGuildMgr.m_PinTuStage = EnumGuildPinTuPlayStage.eOpen
    self:InitMain(CLuaGuildMgr.m_PinTuStage, CLuaGuildMgr.m_PinTuDuration, CLuaGuildMgr.m_PinTuDataList)
end

-- 游戏结束
function LuaGuildPuzzleEnterWnd:OnGuildPinTuPlayEnd()
    CLuaGuildMgr.m_PinTuStage = EnumGuildPinTuPlayStage.eEnd
    self:InitMain(CLuaGuildMgr.m_PinTuStage, CLuaGuildMgr.m_PinTuDuration, CLuaGuildMgr.m_PinTuDataList)
end

-- 使用碎片成功
-- id getchips setchips answered answer finishtime pinTuPlayerCount
function LuaGuildPuzzleEnterWnd:OnUseGuildPinTuChipSuccess(pictureId, chipId, place)
    for _, data in ipairs(CLuaGuildMgr.m_PinTuDataList) do
        if data.id == pictureId then
            data.setchips = bit.bor(bit.lshift(1 ,place-1), data.setchips)
        end
    end
    self:InitMain(CLuaGuildMgr.m_PinTuStage, CLuaGuildMgr.m_PinTuDuration, CLuaGuildMgr.m_PinTuDataList)
end

-- 返回碎片
function LuaGuildPuzzleEnterWnd:OnReturnGuildPinTuChipSuccess(pictureId, chipId)
    for _, data in ipairs(CLuaGuildMgr.m_PinTuDataList) do
        if data.id == pictureId then
            local place = chipId % 1000
            data.getchips = bit.band(bit.bnot(bit.lshift(1 ,place-1)), data.getchips)
        end
    end
    self:InitMain(CLuaGuildMgr.m_PinTuStage, CLuaGuildMgr.m_PinTuDuration, CLuaGuildMgr.m_PinTuDataList)
end

-- 取得碎片
function LuaGuildPuzzleEnterWnd:OnGetGuildPinTuChipSuccess(pictureId, chipId)
    for _, data in ipairs(CLuaGuildMgr.m_PinTuDataList) do
        if data.id == pictureId then
            local place = chipId % 1000
            data.getchips = bit.bor(bit.lshift(1 ,place-1), data.getchips)
        end
    end
    self:InitMain(CLuaGuildMgr.m_PinTuStage, CLuaGuildMgr.m_PinTuDuration, CLuaGuildMgr.m_PinTuDataList)
end

-- 答题成功
function LuaGuildPuzzleEnterWnd:OnAnswerGuildPinTuQuestionSuccess(pictureId, answered, answer, placeListUD)
    for _, data in ipairs(CLuaGuildMgr.m_PinTuDataList) do
        if data.id == pictureId then
            for i, place in ipairs(placeListUD) do
                data.setchips = bit.bor(bit.lshift(1 ,place-1), data.setchips)
                data.getchips = bit.bor(bit.lshift(1 ,place-1), data.getchips)
            end
        end
    end
    self:InitMain(CLuaGuildMgr.m_PinTuStage, CLuaGuildMgr.m_PinTuDuration, CLuaGuildMgr.m_PinTuDataList)
end

-- 玩家答题
function LuaGuildPuzzleEnterWnd:OnPlayerAnswerGuildPinTuQuestion(playerId, pictureId, placeList, id, getchips, setchips, answered, answer, finishtime)
    for _, data in ipairs(CLuaGuildMgr.m_PinTuDataList) do
        if data.id == pictureId then
            data.setchips = setchips
            data.getchips = getchips
        end
    end
    self:InitMain(CLuaGuildMgr.m_PinTuStage, CLuaGuildMgr.m_PinTuDuration, CLuaGuildMgr.m_PinTuDataList)
end

-- 玩家拼上拼图
function LuaGuildPuzzleEnterWnd:OnPlayerFillGuildPinTu(playerId, pictureId, place, id, getchips, setchips, answered, answer, finishtime)
    for _, data in ipairs(CLuaGuildMgr.m_PinTuDataList) do
        if data.id == pictureId then
            data.setchips = setchips
            data.getchips = getchips
        end
    end
end

function LuaGuildPuzzleEnterWnd:OnEnable()
    g_ScriptEvent:AddListener("QueryGuildPinTuDataResult", self, "OnQueryGuildPinTuDataResult")
    g_ScriptEvent:AddListener("PlayerEnterGuildPinTu", self, "OnPlayerEnterGuildPinTu")
    g_ScriptEvent:AddListener("PlayerLeaveGuildPinTu", self, "OnPlayerLeaveGuildPinTu")
    g_ScriptEvent:AddListener("GuildPinTuPlayStart", self, "OnGuildPinTuPlayStart")
    g_ScriptEvent:AddListener("GuildPinTuPlayEnd", self, "OnGuildPinTuPlayEnd")

    g_ScriptEvent:AddListener("UseGuildPinTuChipSuccess", self, "OnUseGuildPinTuChipSuccess")
    g_ScriptEvent:AddListener("AnswerGuildPinTuQuestionSuccess", self, "OnAnswerGuildPinTuQuestionSuccess")
    g_ScriptEvent:AddListener("PlayerFillGuildPinTu", self, "OnPlayerFillGuildPinTu")
    g_ScriptEvent:AddListener("PlayerAnswerGuildPinTuQuestion", self, "OnPlayerAnswerGuildPinTuQuestion")

    g_ScriptEvent:AddListener("GetGuildPinTuChipSuccess", self, "OnGetGuildPinTuChipSuccess")
    g_ScriptEvent:AddListener("ReturnGuildPinTuChipSuccess", self, "OnReturnGuildPinTuChipSuccess")
end

function LuaGuildPuzzleEnterWnd:OnDisable()
    UnRegisterTick(self.m_Tick)
    CLuaGuildMgr.m_PinTuNeedOpenId = nil
    g_ScriptEvent:RemoveListener("QueryGuildPinTuDataResult", self, "OnQueryGuildPinTuDataResult")
    g_ScriptEvent:RemoveListener("PlayerEnterGuildPinTu", self, "OnPlayerEnterGuildPinTu")
    g_ScriptEvent:RemoveListener("PlayerLeaveGuildPinTu", self, "OnPlayerLeaveGuildPinTu")
    g_ScriptEvent:RemoveListener("GuildPinTuPlayStart", self, "OnGuildPinTuPlayStart")
    g_ScriptEvent:RemoveListener("GuildPinTuPlayEnd", self, "OnGuildPinTuPlayEnd")

    g_ScriptEvent:RemoveListener("UseGuildPinTuChipSuccess", self, "OnUseGuildPinTuChipSuccess")
    g_ScriptEvent:RemoveListener("AnswerGuildPinTuQuestionSuccess", self, "OnAnswerGuildPinTuQuestionSuccess")
    g_ScriptEvent:RemoveListener("PlayerFillGuildPinTu", self, "OnPlayerFillGuildPinTu")
    g_ScriptEvent:RemoveListener("PlayerAnswerGuildPinTuQuestion", self, "OnPlayerAnswerGuildPinTuQuestion")

    g_ScriptEvent:RemoveListener("GetGuildPinTuChipSuccess", self, "OnGetGuildPinTuChipSuccess")
    g_ScriptEvent:RemoveListener("ReturnGuildPinTuChipSuccess", self, "OnReturnGuildPinTuChipSuccess")
end

--@region UIEvent

function LuaGuildPuzzleEnterWnd:OnEnterBtnClick()
    if self.m_SelectPinTuData then
        -- 发出请求
        Gac2Gas.RequestEnterGuildPinTu(self.m_SelectPinTuData.id)
    else
        g_MessageMgr:ShowMessage("Guild_Puzzle_Need_Select_One_Puzzle")
    end
end

function LuaGuildPuzzleEnterWnd:OnRankBtnClick()
    CRankData.Inst.InitRankId = GuildPinTu_GameSetting.GetData().RankId
    if IsOpenNewRankWnd() then
	    CUIManager.ShowUI(CLuaUIResources.NewRankWnd)
	else
		CUIManager.ShowUI(CUIResources.RankWnd)
	end
end

function LuaGuildPuzzleEnterWnd:OnRewardBtnClick()
    self.transform:Find("RewardPopUp").gameObject:SetActive(true)
end

function LuaGuildPuzzleEnterWnd:OnRuleBtnClick()
    g_MessageMgr:ShowMessage("Guild_Puzzle_Rule")
end


function LuaGuildPuzzleEnterWnd:OnDetailBtnClick()
    -- 打开碎片情况
    CUIManager.ShowUI(CLuaUIResources.GuildPuzzleDetailWnd)
end


--@endregion UIEvent

