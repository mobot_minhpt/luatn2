local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local EnumSkillInfoContext = import "L10.UI.CSkillInfoMgr+EnumSkillInfoContext"

LuaXueJingKuangHuanStartWnd = class()

RegistClassMember(LuaXueJingKuangHuanStartWnd, "m_TimeLabel")
RegistClassMember(LuaXueJingKuangHuanStartWnd, "m_RuleScrollView")
RegistClassMember(LuaXueJingKuangHuanStartWnd, "m_RuleTable")
RegistClassMember(LuaXueJingKuangHuanStartWnd, "m_RuleTemplate")
RegistClassMember(LuaXueJingKuangHuanStartWnd, "m_SkillTemplate")
RegistClassMember(LuaXueJingKuangHuanStartWnd, "m_AwardLeftLabel")
RegistClassMember(LuaXueJingKuangHuanStartWnd, "m_ApplyButton")
RegistClassMember(LuaXueJingKuangHuanStartWnd, "m_CancelButton")
RegistClassMember(LuaXueJingKuangHuanStartWnd, "m_MatchingHint")

function LuaXueJingKuangHuanStartWnd:Awake()
    self.m_TimeLabel = self.transform:Find("Anchor/RuleView/BgTexture/TimeSprite/TimeLabel"):GetComponent(typeof(UILabel))
    self.m_RuleScrollView = self.transform:Find("Anchor/RuleView/RuleScrollView"):GetComponent(typeof(UIScrollView))
    self.m_RuleTable = self.transform:Find("Anchor/RuleView/RuleScrollView/RuleTable"):GetComponent(typeof(UITable))
    self.m_RuleTemplate = self.transform:Find("Anchor/RuleView/RuleScrollView/RuleTable/RuleTemplate").gameObject
    self.m_SkillTemplate = self.transform:Find("Anchor/RuleView/RuleScrollView/RuleTable/SkillTemplate").gameObject
    self.m_AwardLeftLabel = self.transform:Find("Anchor/AwardLeftLabel"):GetComponent(typeof(UILabel))
    self.m_ApplyButton = self.transform:Find("Anchor/ApplyButton").gameObject
    self.m_CancelButton = self.transform:Find("Anchor/CancelButton").gameObject
    self.m_MatchingHint = self.transform:Find("Anchor/MatchingHint").gameObject

    UIEventListener.Get(self.m_ApplyButton).onClick = DelegateFactory.VoidDelegate(function() self:OnApplyButtonClick() end)
    UIEventListener.Get(self.m_CancelButton).onClick = DelegateFactory.VoidDelegate(function() self:OnCancelButtonClick() end)
end

function LuaXueJingKuangHuanStartWnd:Init()
    local setting = HanJia2022_XueJingKuangHuanSetting.GetData()
    self.m_TimeLabel.text = g_MessageMgr:Format(setting.XueJingKuangHuan_Open_Time_Desc)
    self.m_AwardLeftLabel.text = tostring(setting.DailyRewardTimesLimit - LuaHanJiaMgr.m_XueJingKuangHuanInfo.dailyAwardTimes)
    --Extensions.RemoveAllChildren(self.m_RuleTable.transform)
    local skills = setting.TempSkills
    self:InitRuleText(self.m_RuleTemplate.transform, g_MessageMgr:Format(setting.XueJingKuangHuan_Open_Rule_Desc))
    self:InitRuleSkill(self.m_SkillTemplate.transform, skills[0], skills[1])
    self.m_RuleTable:Reposition()
    self.m_RuleScrollView:ResetPosition()
    self:UpdateButtonStatus()
end

function LuaXueJingKuangHuanStartWnd:InitRuleText(transRoot, text)
    transRoot:Find("Label"):GetComponent(typeof(UILabel)).text = text
end

function LuaXueJingKuangHuanStartWnd:InitRuleSkill(transRoot, skillId1, skillId2)
    self:InitSkill(transRoot:Find("Skill1"), skillId1)
    self:InitSkill(transRoot:Find("Skill2"), skillId2)
end

function LuaXueJingKuangHuanStartWnd:InitSkill(skillRoot, skillId)
    local icon = skillRoot:Find("Icon"):GetComponent(typeof(CUITexture))
    local skillName = skillRoot:Find("SkillName"):GetComponent(typeof(UILabel))
    local skillDisplay = skillRoot:Find("SkillDisplay"):GetComponent(typeof(UILabel))

    local skill = Skill_AllSkills.GetData(skillId)

    icon:LoadSkillIcon(skill.SkillIcon)
    skillName.text = skill.Name
    skillDisplay.text = skill.Display_jian

    CommonDefs.AddOnClickListener(skillRoot.gameObject, DelegateFactory.Action_GameObject(function (go)
        CSkillInfoMgr.ShowSkillInfoWnd(skillId, false, go.transform.position, EnumSkillInfoContext.Default, true, 0, 0, nil)
    end), false)
end


function LuaXueJingKuangHuanStartWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSyncXueJingKuangHuanSignUpResult", self, "OnSyncXueJingKuangHuanSignUpResult")
end

function LuaXueJingKuangHuanStartWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnSyncXueJingKuangHuanSignUpResult", self, "OnSyncXueJingKuangHuanSignUpResult")
end

function LuaXueJingKuangHuanStartWnd:OnSyncXueJingKuangHuanSignUpResult()
    self:UpdateButtonStatus()
end

function LuaXueJingKuangHuanStartWnd:OnApplyButtonClick()
    LuaHanJiaMgr:RequestSignUpXueJingKuangHuan()
end

function LuaXueJingKuangHuanStartWnd:OnCancelButtonClick()
    LuaHanJiaMgr:CancelSignUpXueJingKuangHuan()
end

function LuaXueJingKuangHuanStartWnd:UpdateButtonStatus()
    local bSignedUp = LuaHanJiaMgr.m_XueJingKuangHuanInfo.bSignedUp or false
    self.m_ApplyButton:SetActive(not bSignedUp)
    self.m_CancelButton:SetActive(bSignedUp)
    self.m_MatchingHint:SetActive(bSignedUp)
end
