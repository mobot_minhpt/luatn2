require("3rdParty/ScriptEvent")
require("common/common_include")

local QnButton = import "L10.UI.QnButton"
local CUITexture = import "L10.UI.CUITexture"
local UIInput = import "UIInput"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local QnNumberInput = import "L10.UI.QnNumberInput"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local Constants = import "L10.Game.Constants"
local MessageMgr = import "L10.Game.MessageMgr"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Cangbaoge_Setting = import "L10.Game.Cangbaoge_Setting"

-- 藏宝阁待售区展示
LuaTreasureHousePutToShowWnd=class()

RegistClassMember(LuaTreasureHousePutToShowWnd, "ToShowBtn")

-- 左边的数据
RegistClassMember(LuaTreasureHousePutToShowWnd, "PlayerTexture")
RegistClassMember(LuaTreasureHousePutToShowWnd, "PlayerLevelLabel")
RegistClassMember(LuaTreasureHousePutToShowWnd, "PlayerNameLabel")
RegistClassMember(LuaTreasureHousePutToShowWnd, "PlayerScoreLabel")
RegistClassMember(LuaTreasureHousePutToShowWnd, "TaglineInput")

-- 右边的数据
RegistClassMember(LuaTreasureHousePutToShowWnd, "ExpectedPriceInput")
RegistClassMember(LuaTreasureHousePutToShowWnd, "ShowDaysInput")
RegistClassMember(LuaTreasureHousePutToShowWnd, "MoneyCtrl")


function LuaTreasureHousePutToShowWnd:Init()
	self:InitClassMember()
	self:UpdateShowInfo()
end

function LuaTreasureHousePutToShowWnd:InitClassMember()

	self.ToShowBtn = self.transform:Find("Anchor/ToShowBtn"):GetComponent(typeof(QnButton))

	self.PlayerTexture = self.transform:Find("Anchor/ContentLeft/PlayerTexture"):GetComponent(typeof(CUITexture))
	self.PlayerLevelLabel = self.transform:Find("Anchor/ContentLeft/PlayerTexture/PlayerLevelLabel"):GetComponent(typeof(UILabel))
	self.PlayerNameLabel = self.transform:Find("Anchor/ContentLeft/PlayerNameLabel"):GetComponent(typeof(UILabel))
	self.PlayerScoreLabel = self.transform:Find("Anchor/ContentLeft/PlayerScoreLabel"):GetComponent(typeof(UILabel))
	self.TaglineInput = self.transform:Find("Anchor/ContentLeft/TagLine/TaglineInput"):GetComponent(typeof(UIInput))

	self.ExpectedPriceInput = self.transform:Find("Anchor/ContentRight/Grid/ExceptedPrice/ExpectedPriceInput"):GetComponent(typeof(QnAddSubAndInputButton))
	self.ShowDaysInput = self.transform:Find("Anchor/ContentRight/Grid/ShowDays/ShowDaysInput"):GetComponent(typeof(QnAddSubAndInputButton))
	self.MoneyCtrl = self.transform:Find("Anchor/ContentRight/Grid/MoneyCtrl"):GetComponent(typeof(CCurentMoneyCtrl))

end

--  更新显示信息
function LuaTreasureHousePutToShowWnd:UpdateShowInfo()
	
	if not CClientMainPlayer.Inst then return end
	-- 玩家信息
	self.PlayerTexture:LoadNPCPortrait(CClientMainPlayer.Inst.PortraitName)

	local color = "FFFFFF"
	if CClientMainPlayer.Inst.IsInXianShenStatus then
		color = Constants.ColorOfFeiSheng
	end
	local levelStr = SafeStringFormat3("[c][%s]%s[-][/c]", color, tostring(CClientMainPlayer.Inst.Level))
	self.PlayerLevelLabel.text = levelStr

	self.PlayerNameLabel.text = CClientMainPlayer.Inst.Name
	self.PlayerScoreLabel.text = SafeStringFormat3(LocalString.GetString("[91FAFF]装评[-] %s"), tostring(CClientMainPlayer.Inst.EquipScore)) 

	-- 期望价格
	local setting = Cangbaoge_Setting.GetData()
	self.ExpectedPriceInput:SetMinMax(setting.MinPrice, setting.MaxPrice, 1)

	-- 展示天数
	self.ShowDaysInput:SetMinMax(LuaTreasureHouseMgr.MinShowDays, LuaTreasureHouseMgr.MaxShowDays, 1)
	local onValueChanged = function (value)
		self:OnShowDaysChange(value)
	end
	self.ShowDaysInput.onValueChanged = DelegateFactory.Action_uint(onValueChanged)
	self.ShowDaysInput:SetValue(1, true)
	self.MoneyCtrl:SetType(4, 0, true) -- 设置为灵玉支付

	local onToShowBtnClicked = function (go)
		self:RequestToShow()
	end
	CommonDefs.AddOnClickListener(self.ToShowBtn.gameObject,DelegateFactory.Action_GameObject(onToShowBtnClicked),false)
end

-- 修改展示天数的回调
function LuaTreasureHousePutToShowWnd:OnShowDaysChange(value)
	-- 调用公式计算每天花费灵玉数
	local setting = Cangbaoge_Setting.GetData()
	local formulaId = setting.PreSaleCostFormula
	local formula = GetFormula(formulaId)
	local dailyCost = formula(nil, nil, {CClientMainPlayer.Inst.EquipScore})

	local needLingyu = value * dailyCost
	self.MoneyCtrl:SetCost(needLingyu)
end

-- 请求展示区展示该号
function LuaTreasureHousePutToShowWnd:RequestToShow()
	-- 检查输入的预期价格
	local numberInput = self.ExpectedPriceInput.transform:Find("QnNumberInput"):GetComponent(typeof(QnNumberInput))
	if not numberInput.Text or numberInput.Text == "" then
		MessageMgr.Inst:ShowMessage("CBG_NO_EXCEPTED_PRICE", {})
		return
	end

	-- 检查口号
	local content = self.TaglineInput.value
	if not content or content == "" then
		MessageMgr.Inst:ShowMessage("CBG_NO_TAGLINE", {})
		return
	end
	local ret = CWordFilterMgr.Inst:DoFilterOnSend(content, nil, nil, true)
	local msg = ret.msg
	if not msg then return end
	if ret.shouldBeIgnore then
        g_MessageMgr:ShowMessage("Speech_Violation")
        return
    end

	-- 检查灵玉是否充足
	if not self.MoneyCtrl.moneyEnough then
		local msg = MessageMgr.Inst:FormatMessage("CBG_NOT_ENOUGH_LINGYU", {})
		MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(
        	function()
        		CShopMallMgr.ShowChargeWnd()
        	end
        	), nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
		return
	end

	local txt = MessageMgr.Inst:FormatMessage("CBG_SHELF_COMFIRM", {self.ExpectedPriceInput:GetValue()})
	MessageWndManager.ShowOKCancelMessage(txt, DelegateFactory.Action(
        function()
        	Gac2Gas.CBG_RequestGameOnShelf(CClientMainPlayer.Inst.Id, self.ExpectedPriceInput:GetValue(), msg, self.ShowDaysInput:GetValue(), "")
        	CUIManager.CloseUI(CLuaUIResources.TreasureHousePutToShowWnd)
        end
        ), nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)

end


function LuaTreasureHousePutToShowWnd:OnEnable()
	--g_ScriptEvent:AddListener("SendBingQiPuRank", self, "UpdateRankInfo")
end

function LuaTreasureHousePutToShowWnd:OnDisable()
	--g_ScriptEvent:RemoveListener("SendBingQiPuRank", self, "UpdateRankInfo")
end



return LuaTreasureHousePutToShowWnd