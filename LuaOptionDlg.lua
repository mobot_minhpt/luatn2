local Vector3 = import "UnityEngine.Vector3"
local EventDelegate = import "EventDelegate"
local UIToggle=import "UIToggle"
local CIndirectUIResources=import "L10.UI.CIndirectUIResources"
local Screen = import "UnityEngine.Screen"
local UIRoot = import "UIRoot"

CLuaOptionDlg = class()
RegistClassMember(CLuaOptionDlg,"ItemTemplate")
RegistClassMember(CLuaOptionDlg,"table")
RegistClassMember(CLuaOptionDlg,"background")

function CLuaOptionDlg:Awake()
    self.ItemTemplate = self.transform:Find("node/Option1/Item").gameObject
    self.ItemTemplate:SetActive(false)
    self.table = self.transform:Find("node/Option1/Table"):GetComponent(typeof(UITable))
    self.background = self.transform:Find("node"):GetComponent(typeof(UISprite))
end

function CLuaOptionDlg:Init()
    local count = CLuaOptionMgr.m_GetCountFunction and CLuaOptionMgr.m_GetCountFunction() or 0
    Extensions.RemoveAllChildren(self.table.transform)
    local height = 30

    for i=1,count do
        local go = NGUITools.AddChild(self.table.gameObject,self.ItemTemplate)
        go:SetActive(true)

        local contentLabel = go.transform:Find("text"):GetComponent(typeof(UILabel))
        contentLabel.text = CLuaOptionMgr.m_GetContentFunction and CLuaOptionMgr.m_GetContentFunction(i) or nil

        local toggle = go.transform:GetComponent(typeof(UIToggle))
        toggle:Set(CLuaOptionMgr.m_GetOptionValueFunction and CLuaOptionMgr.m_GetOptionValueFunction(i) or false)

        if CLuaOptionMgr.m_IsRadioMode then
            toggle.group = 777
        end

        EventDelegate.Add(toggle.onChange,DelegateFactory.Callback(function () 
            if CLuaOptionMgr.m_SetOptionValueFunction then
                CLuaOptionMgr.m_SetOptionValueFunction(i,toggle.value)
            end
        end))

        height = height+80
    end
    self.background.height = height
    local localPos = self.background.transform.parent:InverseTransformPoint(CLuaOptionMgr.m_WorldPosition)
    localPos.z = 0
    local centerX = localPos.x
	local centerY = localPos.y
    local halfBgWidth = self.background.width * 0.5
	local bgHeight = self.background.height
    local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
	local virtualScreenWidth = Screen.width * scale * CUIManager.UIMainCamera.rect.width
	local virtualScreenHeight = Screen.height * scale
	local halfVirtualScreenWidth = virtualScreenWidth * 0.5
	local halfVirtualScreenHeight = virtualScreenHeight * 0.5
	if centerX - halfBgWidth < -halfVirtualScreenWidth then
		centerX = -halfVirtualScreenWidth
	end
	if centerX + halfBgWidth > halfVirtualScreenWidth then
		centerX = halfVirtualScreenWidth - halfBgWidth
	end
	if centerY - bgHeight < -halfVirtualScreenHeight then
		centerY = -halfVirtualScreenHeight + bgHeight
	end
    self.background.transform.localPosition =  Vector3(centerX, centerY , 0);
    self.table:Reposition()
end

function CLuaOptionDlg:OnDestroy()
    CLuaOptionMgr.m_GetCountFunction = nil
    CLuaOptionMgr.m_GetContentFunction = nil
    CLuaOptionMgr.m_GetOptionValueFunction = nil
    CLuaOptionMgr.m_SetOptionValueFunction = nil
    CLuaOptionMgr.m_WorldPosition = Vector3.zero
    CLuaOptionMgr.m_IsRadioMode = false
end

CLuaOptionMgr = {}
CLuaOptionMgr.m_GetCountFunction = nil
CLuaOptionMgr.m_GetContentFunction = nil
CLuaOptionMgr.m_GetOptionValueFunction = nil
CLuaOptionMgr.m_SetOptionValueFunction = nil
CLuaOptionMgr.m_WorldPosition = nil
CLuaOptionMgr.m_IsRadioMode = false --radio模式
function CLuaOptionMgr.ShowDlg(getCountFunc,getContentFunc,getOptionValueFunc,setOptionValueFunc,worldPos,isRadioMode)
    CLuaOptionMgr.m_GetCountFunction = getCountFunc
    CLuaOptionMgr.m_GetContentFunction = getContentFunc
    CLuaOptionMgr.m_GetOptionValueFunction = getOptionValueFunc
    CLuaOptionMgr.m_SetOptionValueFunction = setOptionValueFunc
    CLuaOptionMgr.m_WorldPosition = worldPos and worldPos or Vector3.zero
    CLuaOptionMgr.m_IsRadioMode = isRadioMode and isRadioMode or false
    CUIManager.ShowUI(CIndirectUIResources.OptionDlg)
end
function CLuaOptionMgr.CloseDlg()
    CLuaOptionMgr.m_GetCountFunction = nil
    CLuaOptionMgr.m_GetContentFunction = nil
    CLuaOptionMgr.m_GetOptionValueFunction = nil
    CLuaOptionMgr.m_SetOptionValueFunction = nil
    CLuaOptionMgr.m_WorldPosition = Vector3.zero
    CLuaOptionMgr.m_IsRadioMode = false
    CUIManager.CloseUI(CIndirectUIResources.OptionDlg)
end