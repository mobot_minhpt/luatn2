local QnTableView = import "L10.UI.QnTableView"
local CPlayerInfoMgr=import "CPlayerInfoMgr"
local CRankData=import "L10.UI.CRankData"
local EnumPlayerInfoContext=import "L10.Game.EnumPlayerInfoContext"
local EChatPanel=import "L10.Game.EChatPanel"
local AlignType=import "CPlayerInfoMgr+AlignType"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local Profession = import "L10.Game.Profession"
local State = import "L10.UI.QnTabButton.State"
local QnTabButton = import "L10.UI.QnTabButton"

LuaLuoSpecialZongMenRankWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaLuoSpecialZongMenRankWnd, "TabGrid", "TabGrid", GameObject)
RegistChildComponent(LuaLuoSpecialZongMenRankWnd, "CurrentTitleLabel", "CurrentTitleLabel", UILabel)
RegistChildComponent(LuaLuoSpecialZongMenRankWnd, "CountLabel", "CountLabel", UILabel)
RegistChildComponent(LuaLuoSpecialZongMenRankWnd, "DescLabel", "DescLabel", UILabel)
RegistChildComponent(LuaLuoSpecialZongMenRankWnd, "TableBody", "TableBody", CUIRestrictScrollView)
RegistChildComponent(LuaLuoSpecialZongMenRankWnd, "TableView", "TableView", QnTableView)
--@endregion RegistChildComponent end
RegistClassMember(LuaLuoSpecialZongMenRankWnd,"m_RankList")
RegistClassMember(LuaLuoSpecialZongMenRankWnd,"m_RankId")
RegistClassMember(LuaLuoSpecialZongMenRankWnd,"m_MainPlayerMark")
RegistClassMember(LuaLuoSpecialZongMenRankWnd,"m_TitleList")
RegistClassMember(LuaLuoSpecialZongMenRankWnd,"m_TabList")

function LuaLuoSpecialZongMenRankWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaLuoSpecialZongMenRankWnd:Init()
    --TableView初始化
    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_RankList
        end,
        function(item,index)
            item:SetBackgroundTexture(index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)
            self:InitItem(item,index,self.m_RankList[index+1])
        end)

    self.TableView.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        local data=self.m_RankList[row+1]
        if data then
            CPlayerInfoMgr.ShowPlayerPopupMenu(data.PlayerIndex, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,AlignType.Default)
        end
    end)

    local rankList = Menpai_Spokesman.GetData().RankId
    self.m_TitleList = {}
    self.m_TabList = {}
    -- Tab初始化
    for i=1, 5 do
        local rankData = Rank_Player.GetData(rankList[i-1])
        table.insert(self.m_TitleList, rankData.HeaderName)

        local tab = self.TabGrid.transform:Find(tostring(i)).gameObject
        local tabBtn = tab.transform:GetComponent(typeof(QnTabButton))
        table.insert(self.m_TabList, tabBtn)

        local tabLable = self.TabGrid.transform:Find(tostring(i) .. "/Label"):GetComponent(typeof(UILabel))
        tabLable.text = rankData.TabName
        UIEventListener.Get(tab).onClick = DelegateFactory.VoidDelegate(function()
            self:ChangeTab(i)
        end)
    end

    self.DescLabel.text = g_MessageMgr:FormatMessage("LuoYunXi_Special_Sect_Qualification_Tip")
    -- 请求
    Gac2Gas.QuerySpokesmanSectRankExtra()
    self:ChangeTab(1)
end

function LuaLuoSpecialZongMenRankWnd:ChangeTab(index)
    for i=1, #self.m_TabList do
        self.m_TabList[i]:SetState(index == i and State.Highlighted or State.Normal)
    end

    self.CurrentTitleLabel.text = self.m_TitleList[index]
    local rankId = Menpai_Spokesman.GetData().RankId[index-1]
    -- 测试用 根骨排行
    -- local test = {41000026,41000026,41000029,41000028,rankId}
    -- rankId = test[index]
    self.m_RankId = rankId
    Gac2Gas.QueryRank(rankId)
end


function LuaLuoSpecialZongMenRankWnd:OnEnable()
    g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
    g_ScriptEvent:AddListener("SyncSpokesmanSectRankExtra", self, "OnRankInfo")
    
end

function LuaLuoSpecialZongMenRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")
    g_ScriptEvent:RemoveListener("SyncSpokesmanSectRankExtra", self, "OnRankInfo")
end

function LuaLuoSpecialZongMenRankWnd:OnRankInfo(isSigned, effectCnt)
    if not isSigned then
        self.CountLabel.text = LocalString.GetString("仅报名玩家参与排行")
    else
        self.CountLabel.text = SafeStringFormat3(LocalString.GetString("当前[FFFF00]%d项榜单[-]满足条件"), effectCnt)
    end
end

function LuaLuoSpecialZongMenRankWnd:OnRankDataReady()
    self.m_RankList = {}
    -- 当前玩家信息
    local myInfo = CRankData.Inst.MainPlayerRankInfo
    if myInfo.Rank > 0 then
        self.m_MainPlayerMark = true
        table.insert(self.m_RankList, myInfo)
    else
        self.m_MainPlayerMark = false
    end

    -- 排行榜信息
    for i = 1, CRankData.Inst.RankList.Count do
        table.insert(self.m_RankList, CRankData.Inst.RankList[i - 1])
    end

    self.TableView:ReloadData(true, false)
end

function LuaLuoSpecialZongMenRankWnd:InitItem(item, index, info)
    local tf = item.transform

    local nameLabel = tf:Find("PlayerNameLabel"):GetComponent(typeof(UILabel))
    local rankLabel = tf:Find("Rank"):GetComponent(typeof(UILabel))
    local guildLabel = tf:Find("GuildLabel"):GetComponent(typeof(UILabel))
    local countLabel = tf:Find("CountLabel"):GetComponent(typeof(UILabel))
    local rankSprite = tf:Find("Rank/RankImage"):GetComponent(typeof(UISprite))
    local clsSprite = tf:Find("ClsSprite"):GetComponent(typeof(UISprite))
    local selfIcon = tf:Find("SelfIcon").gameObject

    rankSprite.spriteName = ""
    rankLabel.text = ""
    selfIcon:SetActive(self.m_MainPlayerMark and index == 0)
    if self.m_MainPlayerMark and index == 0 then
        item:SetBackgroundTexture("common_bg_mission_background_highlight")
    end

    local rank = info.Rank
    if rank == 1 then
        rankSprite.spriteName = "Rank_No.1"
    elseif rank == 2 then
        rankSprite.spriteName = "Rank_No.2png"
    elseif rank == 3 then
        rankSprite.spriteName = "Rank_No.3png"
    else
        rankLabel.text = tostring(rank)
    end

    nameLabel.text = info.Name
    countLabel.text = info.Value
    guildLabel.text = info.Guild_Name
    clsSprite.spriteName = Profession.GetIcon(info.Job)
end



--@region UIEvent

--@endregion UIEvent

