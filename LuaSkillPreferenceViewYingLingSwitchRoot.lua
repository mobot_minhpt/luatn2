local QnButton = import "L10.UI.QnButton"

LuaSkillPreferenceViewYingLingSwitchRoot = class()

RegistChildComponent(LuaSkillPreferenceViewYingLingSwitchRoot,"m_Root","Root", GameObject)
RegistChildComponent(LuaSkillPreferenceViewYingLingSwitchRoot,"m_Bg","Bg", GameObject)
RegistChildComponent(LuaSkillPreferenceViewYingLingSwitchRoot,"m_Table","Table", GameObject)

RegistClassMember(LuaSkillPreferenceViewYingLingSwitchRoot,"m_IsInit")
RegistClassMember(LuaSkillPreferenceViewYingLingSwitchRoot,"m_CurStateFlagIcons")
RegistClassMember(LuaSkillPreferenceViewYingLingSwitchRoot,"m_YingLingSwitchStateTable")
RegistClassMember(LuaSkillPreferenceViewYingLingSwitchRoot,"m_SwitchStateHighlightSpriteNameTable")
RegistClassMember(LuaSkillPreferenceViewYingLingSwitchRoot,"m_SwitchStateNormalSpriteNameTable")
RegistClassMember(LuaSkillPreferenceViewYingLingSwitchRoot,"m_IsSwitchingYingLingState")
RegistClassMember(LuaSkillPreferenceViewYingLingSwitchRoot,"m_YingLingSwitchStateBtns")
RegistClassMember(LuaSkillPreferenceViewYingLingSwitchRoot,"m_CurSwitchYingLingState")

function LuaSkillPreferenceViewYingLingSwitchRoot:Awake()

    local mainPlayer = CClientMainPlayer.Inst
    if (mainPlayer == nil) or (not mainPlayer.IsYingLing) then
        self.gameObject:SetActive(false)
        return
    end

    self.m_Bg:SetActive(false)
    self.m_Table:SetActive(false)
    self.m_YingLingSwitchStateTable = {1, 2, 3}
    self.m_YingLingSwitchStateBtns = {}
    table.insert(self.m_YingLingSwitchStateBtns,
            self.m_Root.transform:Find("CurSwitch"):GetComponent(typeof(QnButton)))
    table.insert(self.m_YingLingSwitchStateBtns,
            self.m_Root.transform:Find("Table/SwitchBtn1"):GetComponent(typeof(QnButton)))
    table.insert(self.m_YingLingSwitchStateBtns,
            self.m_Root.transform:Find("Table/SwitchBtn2"):GetComponent(typeof(QnButton)))
    self.m_CurStateFlagIcons = {}
    for i = 1, 3 do
        table.insert(self.m_CurStateFlagIcons, self.m_YingLingSwitchStateBtns[i].transform:Find("CurState").gameObject)
    end
    self.m_SwitchStateHighlightSpriteNameTable = {"skillwnd_yinglingswitchroot_yang_highlight","skillwnd_yinglingswitchroot_yin_highlight","skillwnd_yinglingswitchroot_ying_highlight"}
    self.m_SwitchStateNormalSpriteNameTable = {"skillwnd_yinglingswitchroot_yang_normal","skillwnd_yinglingswitchroot_yin_normal","skillwnd_yinglingswitchroot_ying_normal"}
    self:UpdateStateFlagIcons()
    self.m_IsInit = true
end

function LuaSkillPreferenceViewYingLingSwitchRoot:Start()
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer == nil then
        return
    end

    self.m_CurSwitchYingLingState = LuaSkillMgr.GetDefaultYingLingState()

    self.m_Root:SetActive(false)
    if LuaSkillMgr.CheckMainPlayerYingLingBianShenSkill() then
        self:EnableYingLingBianShenSkill()
    end
end

function LuaSkillPreferenceViewYingLingSwitchRoot:UpdateStateFlagIcons()
    local mainPlayer = CClientMainPlayer.Inst
    if not mainPlayer then return end

    for i = 1, #self.m_YingLingSwitchStateTable do
        self.m_CurStateFlagIcons[i].gameObject:SetActive(self.m_YingLingSwitchStateTable[i] == EnumToInt(mainPlayer.CurYingLingState))
    end
end

function LuaSkillPreferenceViewYingLingSwitchRoot:HideOrShowSwitchRootTable()
    self.m_IsSwitchingYingLingState = not self.m_Table.gameObject.activeSelf
    self.m_Table:SetActive(self.m_IsSwitchingYingLingState)
    self.m_Bg:SetActive(self.m_IsSwitchingYingLingState)
end

function LuaSkillPreferenceViewYingLingSwitchRoot:EnableYingLingBianShenSkill()
    local mainPlayer = CClientMainPlayer.Inst
    if not mainPlayer then return end
    if not self.m_IsInit then
        return
    end

    g_ScriptEvent:BroadcastInLua("SkillPreferenceViewEnableYingLingBianShenSkill")
    CLuaGuideMgr.TryTriggerYingLingGetBianShenSkillGuide()
    self.m_Root:SetActive(true)
    self:SwitchYingLingState(EnumToInt(mainPlayer.CurYingLingState))
    g_ScriptEvent:BroadcastInLua("SkillPreferenceViewLoadYingLingSkillIcons")

    self.m_YingLingSwitchStateBtns[1].OnClick = DelegateFactory.Action_QnButton(function (button)
        self:HideOrShowSwitchRootTable()
    end)
    for i = 2,3 do
        local index = i
        self.m_YingLingSwitchStateBtns[i].OnClick = DelegateFactory.Action_QnButton(function (button)
            self:HideOrShowSwitchRootTable()
            self:SwitchYingLingState(self.m_YingLingSwitchStateTable[index ])
            g_ScriptEvent:BroadcastInLua("SkillPreferenceViewLoadYingLingSkillIcons")
        end)
    end
end

function LuaSkillPreferenceViewYingLingSwitchRoot:SwitchYingLingState(state)
    if not self.m_IsInit then
        return
    end

    local a = state + 1
    local b = math.fmod(a + 1,3)
    if a == 4 then a = 1 end
    if b == 0 then b = 3 end
    self.m_YingLingSwitchStateTable  = {state, a, b}
    self:UpdateStateFlagIcons()

    self.m_YingLingSwitchStateBtns[1].m_BackGround.spriteName = self.m_SwitchStateHighlightSpriteNameTable[state]
    self.m_YingLingSwitchStateBtns[2].m_BackGround.spriteName = self.m_SwitchStateNormalSpriteNameTable[a]
    self.m_YingLingSwitchStateBtns[3].m_BackGround.spriteName = self.m_SwitchStateNormalSpriteNameTable[b]
    self.m_CurSwitchYingLingState = state
    g_ScriptEvent:BroadcastInLua("SkillPreferenceViewSwitchYingLingState", self.m_CurSwitchYingLingState)
end
----------------------------------------
---引导相关
----------------------------------------
function LuaSkillPreferenceViewYingLingSwitchRoot:GetYingLingSwitchSkillStateBtn()
    if self.m_YingLingSwitchStateBtns and #self.m_YingLingSwitchStateBtns > 1 then
        return self.m_YingLingSwitchStateBtns[1].gameObject
    end
    return nil
end