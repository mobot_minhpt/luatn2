local QnExpandListBox = import "L10.UI.QnExpandListBox"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local QnTabView = import "L10.UI.QnTabView"
local GameObject = import "UnityEngine.GameObject"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local CBiWuDaHuiMgr = import "L10.Game.CBiWuDaHuiMgr"

LuaBiWuCrossInfoWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaBiWuCrossInfoWnd, "Tabs", "Tabs", QnTabView)
RegistChildComponent(LuaBiWuCrossInfoWnd, "TableView", "TableView", QnAdvanceGridView)
RegistChildComponent(LuaBiWuCrossInfoWnd, "SaiChengWnd", "SaiChengWnd", GameObject)
RegistChildComponent(LuaBiWuCrossInfoWnd, "TeamSelectBox", "TeamSelectBox", QnExpandListBox)

--@endregion RegistChildComponent end

RegistClassMember(LuaBiWuCrossInfoWnd, "m_tab")
RegistClassMember(LuaBiWuCrossInfoWnd, "m_rankNames")
RegistClassMember(LuaBiWuCrossInfoWnd, "m_group")

function LuaBiWuCrossInfoWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    self.Tabs.OnSelect =
        DelegateFactory.Action_QnTabButton_int(
        function(btn, index)
            self:OnTabsSelected(btn, index)
        end
    )

    --@endregion EventBind end

    self.m_group = CBiWuDaHuiMgr.Inst:GetMyStage()
    if self.m_group <= 0 then
        self.m_group = 1
    end
end

function LuaBiWuCrossInfoWnd:OnEnable()
    g_ScriptEvent:AddListener("OnGetBiWuCrossMatchInfo", self, "OnGetBiWuCrossMatchInfo")
    g_ScriptEvent:AddListener("OnGetBiWuCrossTeamsInfo", self, "OnGetBiWuCrossTeamsInfo")
end

function LuaBiWuCrossInfoWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnGetBiWuCrossMatchInfo", self, "OnGetBiWuCrossMatchInfo")
    g_ScriptEvent:RemoveListener("OnGetBiWuCrossTeamsInfo", self, "OnGetBiWuCrossTeamsInfo")
end

function LuaBiWuCrossInfoWnd:OnGetBiWuCrossTeamsInfo()
    if self.m_tab == 1 then
        self:InitTeamTab()
    end
end

function LuaBiWuCrossInfoWnd:OnGetBiWuCrossMatchInfo()
    if self.m_tab == 0 then
        self:InitBattleTab()
    end
end

function LuaBiWuCrossInfoWnd:Init()
    self.m_rankNames = {
        LocalString.GetString("状元"),
        LocalString.GetString("榜眼"),
        LocalString.GetString("探花")
    }

    local items = {
        LocalString.GetString("新锐组"),
        LocalString.GetString("英武组"),
        LocalString.GetString("神勇组"),
        LocalString.GetString("天罡组"),
        LocalString.GetString("天元组")
    }
    local nameList = CreateFromClass(MakeGenericClass(List, String))
    for i = 1, #items do
        CommonDefs.ListAdd(nameList, typeof(String), items[i])
    end

    self.TeamSelectBox:SetItems(nameList)
    self.TeamSelectBox:SetNameIndex(self.m_group - 1)
    self.TeamSelectBox.ClickCallback =
        DelegateFactory.Action_int(
        function(index)
            self.m_group = index + 1
            if self.m_tab == 0 then
                LuaBiWuCrossMgr.ReqInfoData(self.m_group)
            else
                LuaBiWuCrossMgr.ReqGroupTeams(self.m_group)
            end
        end
    )
end

function LuaBiWuCrossInfoWnd:InitBattleTab()
    local batinfos = LuaBiWuCrossMgr.BattleInfo
    if not batinfos then
        return
    end

    for i = 1, #batinfos do
        local ctrl = FindChild(self.SaiChengWnd.transform, "BattleTeamItem" .. i)
        local info = batinfos[i]
        self:InitBattleCtrl(ctrl, info)
    end

    local setting = BiWuCross_Setting.GetData()
    local rd1timelb = FindChildWithType(self.SaiChengWnd.transform, "Dec1/Label", typeof(UILabel))
    local rd2timelb = FindChildWithType(self.SaiChengWnd.transform, "Dec3/Label", typeof(UILabel))
    rd1timelb.text = setting.Round1TimeStr
    rd2timelb.text = setting.Round2TimeStr
end

function LuaBiWuCrossInfoWnd:InitBattleCtrl(ctrl, info)
    local teamlb1 = FindChildWithType(ctrl, "TeamName1", typeof(UILabel))
    local teamlb2 = FindChildWithType(ctrl, "TeamName2", typeof(UILabel))

    teamlb1.text = info.TeamName1
    teamlb2.text = info.TeamName2

    if info.WinTeamIndex > 0 then
        teamlb1.color = info.WinTeamIndex == info.TeamIndex1 and Color.white or Color.gray
        teamlb2.color = info.WinTeamIndex == info.TeamIndex2 and Color.white or Color.gray
    else
        teamlb1.color = info.TeamIndex1 > 0 and Color.white or Color.gray
        teamlb2.color = info.TeamIndex2 > 0 and Color.white or Color.gray
    end

    UIEventListener.Get(teamlb1.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:ShowTeamWnd(info.TeamIndex1)
        end
    )

    UIEventListener.Get(teamlb2.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:ShowTeamWnd(info.TeamIndex2)
        end
    )
end

function LuaBiWuCrossInfoWnd:InitTeamTab()
    if not LuaBiWuCrossMgr.AllTeamInfos then
        return
    end
    self.TableView.m_DataSource =
        DefaultTableViewDataSource.Create(
        function()
            return #LuaBiWuCrossMgr.AllTeamInfos
        end,
        function(item, index)
            self:InitTeamItem(item, index)
        end
    )

    self.TableView.OnSelectAtRow =
        DelegateFactory.Action_int(
        function(row)
            self:OnSelectTeam(row)
        end
    )
    self.TableView:ReloadData(true, false)
end

function LuaBiWuCrossInfoWnd:InitTeamItem(item, index)
    local rowtrans = FindChild(item.transform, "Row")
    local lbscore = FindChildWithType(rowtrans, "LbScore", typeof(UILabel))
    local lbache = FindChildWithType(rowtrans, "LbAche", typeof(UILabel))
    local lbserver = FindChildWithType(rowtrans, "LbServer", typeof(UILabel))
    local lbteam = FindChildWithType(rowtrans, "LbTeam", typeof(UILabel))
    local btn = FindChild(rowtrans, "InfoBtn")

    local rindex = index + 1
    local data = LuaBiWuCrossMgr.AllTeamInfos[rindex]

    lbscore.text = tostring(data.ZhuangPing)
    lbache.text = self.m_rankNames[data.Rank]
    lbserver.text = data.ServerName
    lbteam.text = data.TeamName

    UIEventListener.Get(btn.gameObject).onClick =
        DelegateFactory.VoidDelegate(
        function(go)
            self:ShowTeamWnd(data.TeamIndex)
        end
    )
end

function LuaBiWuCrossInfoWnd:OnSelectTeam(row)
    local rindex = row + 1
    local data = LuaBiWuCrossMgr.AllTeamInfos[rindex]
    self:ShowTeamWnd(data.TeamIndex)
end

function LuaBiWuCrossInfoWnd:ShowTeamWnd(teamindex)
    if teamindex <= 0 then
        return
    end
    LuaBiWuCrossMgr.ShowTeamWnd(0, teamindex)
end

--@region UIEvent

function LuaBiWuCrossInfoWnd:OnTabsSelected(btn, index)
    self.m_tab = index
    if index == 0 then
        LuaBiWuCrossMgr.ReqInfoData(self.m_group)
    else
        LuaBiWuCrossMgr.ReqGroupTeams(self.m_group)
    end
end

--@endregion UIEvent
