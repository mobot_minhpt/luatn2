local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local Color = import "UnityEngine.Color"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local Animator = import "UnityEngine.Animator"

LuaHuanHunShanZhuangSignUpWnd = class()
LuaHuanHunShanZhuangSignUpWnd.s_OpenWndWithInfo = nil

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end
RegistClassMember(LuaHuanHunShanZhuangSignUpWnd, "WndPanel")
RegistClassMember(LuaHuanHunShanZhuangSignUpWnd, "NormalTab")
RegistClassMember(LuaHuanHunShanZhuangSignUpWnd, "HardTab")
RegistClassMember(LuaHuanHunShanZhuangSignUpWnd, "BottomCnt")
RegistClassMember(LuaHuanHunShanZhuangSignUpWnd, "BottomHint")
RegistClassMember(LuaHuanHunShanZhuangSignUpWnd, "Portrait")
RegistClassMember(LuaHuanHunShanZhuangSignUpWnd, "ModeTitle")
RegistClassMember(LuaHuanHunShanZhuangSignUpWnd, "NormalReward")
RegistClassMember(LuaHuanHunShanZhuangSignUpWnd, "HardReward")


RegistClassMember(LuaHuanHunShanZhuangSignUpWnd, "m_ChallengeTimes")
RegistClassMember(LuaHuanHunShanZhuangSignUpWnd, "m_HelpTimes")
RegistClassMember(LuaHuanHunShanZhuangSignUpWnd, "m_HardTimes")
RegistClassMember(LuaHuanHunShanZhuangSignUpWnd, "m_IsHardMode")

function LuaHuanHunShanZhuangSignUpWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    
    self:InitUI()
    self.transform:GetComponent(typeof(Animator)):Play("huanhunshanzhuangsignupwnd_shown")
end

function LuaHuanHunShanZhuangSignUpWnd:InitUI()
    self.WndPanel = self.transform:GetComponent(typeof(UIPanel))
    --self.WndPanel.alpha = 0

    --self.NormalTab = self.transform:Find("Anchor/BossView/Mode/Normal"):GetComponent(typeof(QnSelectableButton))
    --self.NormalTab.Selectable = false
    --self.HardTab = self.transform:Find("Anchor/BossView/Mode/Hard"):GetComponent(typeof(QnSelectableButton))
    --self.HardTab.Selectable = false
    --self.BottomCnt = self.transform:Find("Anchor/BossView/Bottom/Count")
    --self.BottomHint = self.transform:Find("Anchor/BossView/Bottom/Hint")
    --self.Portrait = self.transform:Find("Anchor/BossView/Portrait")
    --self.PortraitBg = self.transform:Find("Anchor/BossView/Bg")
    --self.ModeTitle = self.transform:Find("Anchor/InfoView/Title/Mode")
    --self.NormalReward = self.transform:Find("Anchor/InfoView/Content/NormalReward")
    --self.HardReward = self.transform:Find("Anchor/InfoView/Content/HardReward")

    --self.transform:Find("Anchor/InfoView/Content/Time/Info"):GetComponent(typeof(UILabel)).text = ShuJia2022_Setting.GetData().HHSZTime
    self.transform:Find("Anchor/InfoView/Content/Desc/Info"):GetComponent(typeof(UILabel)).text = ShuJia2022_Setting.GetData().HHSZTaskDisc
    self.transform:Find("Anchor/InfoView/Hint"):GetComponent(typeof(UILabel)).text = ShuJia2022_Setting.GetData().HHSZTaskHint

    CommonDefs.AddOnClickListener(
        self.transform:Find("Anchor/InfoView/Content/GoButton").gameObject, 
        DelegateFactory.Action_GameObject(function()
            Gac2Gas.RequestEnterHuanHunShanZhuangPlay(true)
        end), 
        false
    )

    CommonDefs.AddOnClickListener(
        self.transform:Find("Anchor/InfoView/Content/AchiButton").gameObject, 
        DelegateFactory.Action_GameObject(function()
            LuaAchievementMgr.m_CurAchieveTab = LocalString.GetString("系统玩法")
            LuaAchievementMgr.m_CurAchieveSubTab = LocalString.GetString("还魂山庄")
            LuaAchievementMgr.m_CurGroupId = 0
            CUIManager.ShowUI("AchievementDetailWnd")
        end), 
        false
    )

    --[[
    CommonDefs.AddOnClickListener(
        self.NormalTab.gameObject, 
        DelegateFactory.Action_GameObject(function()
            self:ChooseMode(true)
        end), 
        false
    )

    CommonDefs.AddOnClickListener(
        self.HardTab.gameObject, 
        DelegateFactory.Action_GameObject(function()
            self:ChooseMode(false)
        end), 
        false
    )
    --]]

    CommonDefs.AddOnClickListener(
        self.transform:Find("Anchor/InfoView/Content/RuleButton").gameObject,
        DelegateFactory.Action_GameObject(function()
            g_MessageMgr:ShowMessage("ShuJia2022_HuanHunShanZhuang_Tips")
        end), 
        false
    )

    -- 用m_InitFxPath才能保证OnLoadFxFinish在CUIFx的Start后触发，填在Prefab里
    --self.transform:Find("Anchor/Fx3").localPosition = Vector3(20, -20, 0)
    --self.transform:Find("Anchor/Fx3"):GetComponent(typeof(CUIFx)).OnLoadFxFinish = DelegateFactory.Action(function()
    --    self.transform:Find("Anchor/Fx3"):GetComponent(typeof(UITexture)).alpha = 0
    --end)
end

function LuaHuanHunShanZhuangSignUpWnd:Init()
    --if LuaHuanHunShanZhuangSignUpWnd.s_OpenWndWithInfo then
    --    self:OnSendHuanHunShanZhuangPlayTimes(unpack(LuaHuanHunShanZhuangSignUpWnd.s_OpenWndWithInfo))
    --    LuaHuanHunShanZhuangSignUpWnd.s_OpenWndWithInfo = nil
    --else
    --    Gac2Gas.RequestHuanHunShanZhuangPlayTimes()
    --end
end

function LuaHuanHunShanZhuangSignUpWnd:Start()

end

function LuaHuanHunShanZhuangSignUpWnd:OnEnable()
    --g_ScriptEvent:AddListener("SendHuanHunShanZhuangPlayTimes", self, "OnSendHuanHunShanZhuangPlayTimes")
end

function LuaHuanHunShanZhuangSignUpWnd:OnDisable()
    --g_ScriptEvent:RemoveListener("SendHuanHunShanZhuangPlayTimes", self, "OnSendHuanHunShanZhuangPlayTimes")
end

-- obsolete
function LuaHuanHunShanZhuangSignUpWnd:InitOneItem(curItem, itemID)
    local texture = curItem.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local ItemData = Item_Item.GetData(itemID)

    if ItemData then texture:LoadMaterial(ItemData.Icon) end

    CommonDefs.AddOnClickListener(
        curItem,
        DelegateFactory.Action_GameObject(
            function(go)
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemID, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
            end
        ),
        false
    )
end

--@region UIEvent
--@endregion UIEvent

-- obsolete
function LuaHuanHunShanZhuangSignUpWnd:OnSendHuanHunShanZhuangPlayTimes(tiaozhanTimes, zhuzhanTimes, hardModeTimes)
    self.m_ChallengeTimes = tiaozhanTimes
    self.m_HelpTimes = zhuzhanTimes
    self.m_HardTimes = hardModeTimes
    if tiaozhanTimes + zhuzhanTimes > 0 then
        self:ChooseMode(true)
    else
        self:ChooseMode(false)
    end

    --self.WndPanel.alpha = 1
end

-- obsolete
function LuaHuanHunShanZhuangSignUpWnd:ChooseMode(normal)
    self.transform:Find("Anchor/Fx2").gameObject:SetActive(not normal)
    self.transform:Find("Anchor/Fx3").gameObject:SetActive(not normal)

    self.m_IsHardMode = not normal

    self.NormalTab:SetSelected(normal)
    local normalLabel = self.NormalTab.transform:Find("Label"):GetComponent(typeof(UILabel))
    normalLabel.fontSize = normal and 56 or 44
    normalLabel.color = normal and NGUIText.ParseColor("fef1ff", 0) or Color(172/255, 248/255, 1, 0.5)
    self.HardTab:SetSelected(not normal)
    local hardLabel = self.HardTab.transform:Find("Label"):GetComponent(typeof(UILabel))
    hardLabel.fontSize = not normal and 56 or 44
    hardLabel.color = not normal and NGUIText.ParseColor("fef1ff", 0) or Color(172/255, 248/255, 1, 0.5)

    self.BottomCnt:Find("Normal").gameObject:SetActive(normal)
    self.BottomCnt:Find("Hard").gameObject:SetActive(not normal)
    self.BottomHint:Find("Normal").gameObject:SetActive(normal)
    self.BottomHint:Find("Hard").gameObject:SetActive(not normal)
    
    self.Portrait:Find("Normal").gameObject:SetActive(normal)
    self.Portrait:Find("Hard").gameObject:SetActive(not normal)
    self.PortraitBg:Find("Normal").gameObject:SetActive(normal)
    self.PortraitBg:Find("Hard").gameObject:SetActive(not normal)

    self.ModeTitle:Find("Normal").gameObject:SetActive(normal)
    self.ModeTitle:Find("Hard").gameObject:SetActive(not normal)

    self.NormalReward.gameObject:SetActive(normal)
    self.HardReward.gameObject:SetActive(not normal)
    
    if normal then
        local cnt1 = self.BottomCnt:Find("Normal/Cnt1"):GetComponent(typeof(UILabel))
        local cnt2 = self.BottomCnt:Find("Normal/Cnt2"):GetComponent(typeof(UILabel))
        cnt1.text = self.m_ChallengeTimes
        cnt2.text = self.m_HelpTimes
        cnt1.color = NGUIText.ParseColor(self.m_ChallengeTimes > 0 and "ffffff" or "ff5050", 0)
        cnt2.color = NGUIText.ParseColor(self.m_HelpTimes > 0 and "ffffff" or "ff5050", 0)

        local items = ShuJia2022_Setting.GetData().EasyPlayRewardItem
        self:InitOneItem(self.NormalReward:Find("Item1").gameObject, items[0])
        self:InitOneItem(self.NormalReward:Find("Item2").gameObject, items[1])
    else
        self.BottomCnt:Find("Hard/Cnt1"):GetComponent(typeof(UILabel)).text = self.m_HardTimes
        
        local items = ShuJia2022_Setting.GetData().HardPlayRewardItem
        self:InitOneItem(self.HardReward:Find("Item1").gameObject, items[0][1])
        self:InitOneItem(self.HardReward:Find("Item2").gameObject, items[1][1])
        self.HardReward:Find("Item1/Cnt"):GetComponent(typeof(UILabel)).text = items[0][0]..LocalString.GetString("次")
        self.HardReward:Find("Item1/Mask").gameObject:SetActive(self.m_HardTimes >= items[0][0])
        self.HardReward:Find("Item1/CheckboxSprite").gameObject:SetActive(self.m_HardTimes >= items[0][0])
        self.HardReward:Find("Item2/Cnt"):GetComponent(typeof(UILabel)).text = items[1][0]..LocalString.GetString("次")
        self.HardReward:Find("Item2/Mask").gameObject:SetActive(self.m_HardTimes >= items[1][0])
        self.HardReward:Find("Item2/CheckboxSprite").gameObject:SetActive(self.m_HardTimes >= items[1][0])
    end
end
