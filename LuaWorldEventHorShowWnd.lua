require("common/common_include")

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CUIManager = import "L10.UI.CUIManager"
local CUITexture = import "L10.UI.CUITexture"
local TweenWidth = import "TweenWidth"

LuaWorldEventHorShowWnd = class()

RegistClassMember(LuaWorldEventHorShowWnd,"CloseBtn")
RegistClassMember(LuaWorldEventHorShowWnd,"ShowTexture")
RegistClassMember(LuaWorldEventHorShowWnd,"ShowBg")

function LuaWorldEventHorShowWnd:Init()
	local onCloseClick = function(go)
    CUIManager.CloseUI(CLuaUIResources.WorldEventHorShowWnd)
	end
	CommonDefs.AddOnClickListener(self.CloseBtn,DelegateFactory.Action_GameObject(onCloseClick),false)
	self.CloseBtn:SetActive(false)

	if LuaWorldEventMgr.HorShowPic then
		self.ShowTexture:GetComponent(typeof(CUITexture)):LoadMaterial("UI/Texture/NonTransparent/Material/"..LuaWorldEventMgr.HorShowPic..".mat",false)
	end
	local showBgUIWidget = self.ShowBg:GetComponent(typeof(UIWidget))
	showBgUIWidget.width = 0
	RegisterTickWithDuration(function ()
		TweenWidth.Begin(showBgUIWidget, 3, 1920)
	end,300,300)

	local showTime = 5500
	if LuaWorldEventMgr.HorShowTime > 0 then
		showTime = LuaWorldEventMgr.HorShowTime * 100
	end

	RegisterTickWithDuration(function ()
		CUIManager.CloseUI(CLuaUIResources.WorldEventHorShowWnd)
	end,showTime,showTime)
end

return LuaWorldEventHorShowWnd
