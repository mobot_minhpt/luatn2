local CChatLinkMgr=import "CChatLinkMgr"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local Main = import "L10.Engine.Main"
--local Json = import "L10.Game.Utils.Json"
local CHTTPForm = import "L10.Game.CHTTPForm"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local JSON = require '3rdParty/json/JSON'
local CUITexture = import "L10.UI.CUITexture"
local CExpressionMgr = import "L10.Game.CExpressionMgr"
local ExpressionHead_ProfileFrame = import "L10.Game.ExpressionHead_ProfileFrame"
local DefaultChatInputListener = import "L10.UI.DefaultChatInputListener"
local md5 = require '3rdParty/md5/md5'
local Vector3 = import "UnityEngine.Vector3"
local CLoginMgr = import "L10.Game.CLoginMgr"
local UISpriteAnimation = import "UISpriteAnimation"
local HTTPHelper = import "L10.Game.HTTPHelper"
local L10 = import "L10"
local Encoding=import "System.Text.Encoding"
local Json = import "L10.Game.Utils.Json"

LuaPersonalSpaceMgrReal = {}
if CommonDefs.IS_CN_CLIENT then
  LuaPersonalSpaceMgrReal.EnableHotTalk = true
else
  LuaPersonalSpaceMgrReal.EnableHotTalk = false
end

LuaPersonalSpaceMgrReal.ShowInfoTopic = nil
LuaPersonalSpaceMgrReal.ShowInfoList = nil
LuaPersonalSpaceMgrReal.ShowInfoPage = nil
LuaPersonalSpaceMgrReal.ShowInfoType = nil
LuaPersonalSpaceMgrReal.ShowInfoTopicDefaultId = nil
LuaPersonalSpaceMgrReal.HotTalkBtnName = 'PSHotTalk'
LuaPersonalSpaceMgrReal.ShowDetailImgTable = nil
LuaPersonalSpaceMgrReal.ShowDetailImgIndex = nil
LuaPersonalSpaceMgrReal.MD5_SALT = '^&*()$#@!!~_($%^##&*+_'

LuaPersonalSpaceMgrReal.MoviePlayUrl = nil
-- 暗恋
LuaPersonalSpaceMgrReal.SlientLoveCallBack = nil
LuaPersonalSpaceMgrReal.SlientLoveRoleId = nil
LuaPersonalSpaceMgrReal.SlientLoveCls = nil
LuaPersonalSpaceMgrReal.SlientLoveGender = nil
LuaPersonalSpaceMgrReal.SlientLoveName = nil
--------------------
LuaPersonalSpaceMgrReal.EnableLongText = true
function LuaPersonalSpaceMgrReal.OpenSendLongText()
  if CPersonalSpaceMgr.Inst:OpenPersonalSpaceCheck() then
    CUIManager.ShowUI(CLuaUIResources.SendLongTextMomentWnd)
  end
end

-------------------

function LuaPersonalSpaceMgrReal.MakeToken(id,time,content)
  local contentMd5 = LuaPersonalSpaceMgrReal.GetHexMd5_Internal(content)
  if not contentMd5 then
    return
  end

  return LuaPersonalSpaceMgrReal.GetHexMd5_Internal(time .. id .. contentMd5 .. LuaPersonalSpaceMgrReal.MD5_SALT)
end

function LuaPersonalSpaceMgrReal.GetHexMd5_Internal(inputString)
  --local strBytes =
  local m = md5.new()
  local ok = pcall(function()
    local strBytes = {}
    for i=1,string.len(inputString) do
      local curByte = string.byte(inputString, i)
      m:update(tostring(curByte))
    end
  end)

  if ok then
    return md5.tohex(m:finish())
  end
end

--------------------

function LuaPersonalSpaceMgrReal.UpdateShowInfoList(data)
  if LuaPersonalSpaceMgrReal.ShowInfoList and data then
    for i,v in pairs(LuaPersonalSpaceMgrReal.ShowInfoList) do
      if v.id == data.id then
        LuaPersonalSpaceMgrReal.ShowInfoList[i] = data
        break
      end
    end
  end
end

function LuaPersonalSpaceMgrReal.CleanBtn(btn)
  UIEventListener.Get(btn).onClick = nil
end

function LuaPersonalSpaceMgrReal.TableToArray(commonItems)
  if commonItems then
    return CommonDefs.ListToArray(CommonDefs.ListGetRange(commonItems, 1, commonItems.Count - 1))
  end
end

function LuaPersonalSpaceMgrReal.GenerateForwardString(forwards)
  local result = ''
  if forwards then
    for i,v in ipairs(forwards) do
      result = result .. CPersonalSpaceMgr.GeneratePlayerName(v.roleinfo.roleid,v.roleinfo.rolename) .. v.text
    end
  end
  return result
end

function LuaPersonalSpaceMgrReal.LoadPortraitPic(photo,clazz,gender,node,expression)
  if not node then
    return
  end
  if photo and photo ~= '' and CPersonalSpaceMgr.EnableSelfPicShow and CPersonalSpaceMgr.CheckPortraitPicAvaliable(photo) then
    local utex = node:GetComponent(typeof(UITexture))
    if not utex then
      return
    end
    local photoUrl = SafeStringFormat3('%s?imageView&thumbnail=%sx%s',photo,utex.width,utex.height)
    CPersonalSpaceMgr.DownLoadPic(photoUrl,utex,utex.height,utex.width,nil,true,false)
  elseif clazz and gender and clazz ~= '' and gender ~= '' then
    local ctex = node:GetComponent(typeof(CUITexture))
    if not ctex then
      return
    end
    ctex:LoadNPCPortrait(CUICommonDef.GetPortraitName(tonumber(clazz),tonumber(gender)))
  end

  local frame = node.transform:Find('frame').gameObject
  local normalframe = node.transform:Find('normalframe').gameObject
  frame:SetActive(false)
  normalframe:SetActive(true)
  if CExpressionMgr.EnableProfileFrame and expression then
    if expression.frame and expression.frame > 0 then
      local data = ExpressionHead_ProfileFrame.GetData(tonumber(expression.frame))
      if data then
        frame:SetActive(true)
        normalframe:SetActive(false)
        frame:GetComponent(typeof(CUITexture)):LoadMaterial(data.ResName)
      end
    end
  end
end

function LuaPersonalSpaceMgrReal.PushData(_url, dataTable, backFunction)
  local url = _url

  local form = CHTTPForm()
  local time = math.floor(CServerTimeMgr.Inst.timeStamp * 1000)
  local skey = CPersonalSpaceMgr.Inst.Token
  local roleid = CClientMainPlayer.Inst.Id
  local serverid = CPersonalSpaceMgr.Inst:GetOriginalServerId()

  form:AddField("time", tostring(time))
  form:AddField("roleid", tostring(roleid))
  --form:AddField("origin_roleid", origin_roleid)
  form:AddField("serverid", tostring(serverid))
  form:AddField("skey", skey)
  if dataTable then
    for i,v in pairs(dataTable) do
      form:AddField(tostring(i),tostring(v))
    end
  end

  Main.Inst:StartCoroutine(CPersonalSpaceMgr.Inst:DoPost(url, form, DelegateFactory.Action_bool_string(function (success, json)
    if success then
      if backFunction then
        local tData = JSON:decode(json)
        backFunction(tData)
      end
    end
  end)))
end

function LuaPersonalSpaceMgrReal.GetPersonalSpaceHotTalkTitle()
  local url = CPersonalSpaceMgr.BASE_URL.."qnm/topic/list"

  local dataTable = {}
  dataTable.page = '1'
  dataTable.pagesize = '10'

  local backFunction = function(tData)
    if tData and tData.code and tData.code == 0 then
      local dataTable = tData.data
      if dataTable then
        LuaPersonalSpaceMgrReal.ShowInfoTopic = dataTable
        g_ScriptEvent:BroadcastInLua("PersonalSpaceHotTalkTopicInfoBack")
      end
    end
  end

  LuaPersonalSpaceMgrReal.PushData(url,dataTable,backFunction)
end

function LuaPersonalSpaceMgrReal.GetPersonalSpaceHotTalkInfo(topicId,sortType,pageNum)
  local url = CPersonalSpaceMgr.BASE_URL.."qnm/topic/moment/list"

  local dataTable = {}
  dataTable.page = pageNum or 1
  dataTable.page_size = '10'
  dataTable.sort = sortType
  dataTable.topicId = topicId

  local backFunction = function(tData)
    if tData and tData.code and tData.code == 0 then
      if tData.data and tData.data.list then
        LuaPersonalSpaceMgrReal.ShowInfoList = tData.data.list
      end
    end
    LuaPersonalSpaceMgrReal.ShowInfoPage = dataTable.page
    LuaPersonalSpaceMgrReal.ShowInfoType = dataTable.sort
    g_ScriptEvent:BroadcastInLua("PersonalSpaceHotTalkInfoBack")
  end

  LuaPersonalSpaceMgrReal.PushData(url,dataTable,backFunction)
end

function LuaPersonalSpaceMgrReal.GetSingleMomentInfo(id,backFunction)
  local url = CPersonalSpaceMgr.BASE_URL..LuaPersonalSpaceDefs.GET_MOMENT_BYID
  local dataTable = {}
  dataTable.id = id

  LuaPersonalSpaceMgrReal.PushData(url,dataTable,backFunction)
end

function LuaPersonalSpaceMgrReal.GetChatListener(chatInput)
  if not chatInput then
    return
  end
  local m_DefaultChatInputListener = DefaultChatInputListener()
  m_DefaultChatInputListener:SetOnInputEmoticonFunc(function (prefix)
    chatInput:AddEmotion(prefix)
  end)
  m_DefaultChatInputListener:SetOnInputItemFunc(function (item)
    chatInput:AddItem(item)
  end)
  m_DefaultChatInputListener:SetOnLingShouFunc(function (lingshouId,lingshouName)
    chatInput:AddLingShou(lingshouId,lingshouName)
  end)
  m_DefaultChatInputListener:SetOnInputBabyFunc(function (babyId,babyName)
    chatInput:AddBaby(babyId,babyName)
  end)
  m_DefaultChatInputListener:SetOnInputWeddingItemFunc(function (item)
    chatInput:AddWeddingItem(item)
  end)
  m_DefaultChatInputListener:SetOnInputAchievementFunc(function (aId)
    chatInput:AddAchievement(aId)
  end)
  return m_DefaultChatInputListener
end

function LuaPersonalSpaceMgrReal.GetComments(comment_id,backFunction)
  local url = CPersonalSpaceMgr.BASE_URL .. LuaPersonalSpaceDefs.GET_COMMENTS
  local dataTable = {}
  dataTable.comment_id = comment_id

  LuaPersonalSpaceMgrReal.PushData(url,dataTable,backFunction)
end

function LuaPersonalSpaceMgrReal.InitDetailPicPanel(imgTable,imgIndex)
  if imgTable and imgIndex then
    LuaPersonalSpaceMgrReal.ShowDetailImgTable = imgTable
    LuaPersonalSpaceMgrReal.ShowDetailImgIndex = imgIndex
    CUIManager.ShowUI(CLuaUIResources.PersonalSpaceDetailPicWnd)
  end
end

function LuaPersonalSpaceMgrReal.SubStringUTF8(str, startIndex, endIndex)
    if startIndex < 0 then
        startIndex = LuaPersonalSpaceMgrReal.SubStringGetTotalIndex(str) + startIndex + 1;
    end

    if endIndex ~= nil and endIndex < 0 then
        endIndex = LuaPersonalSpaceMgrReal.SubStringGetTotalIndex(str) + endIndex + 1;
    end

    if endIndex == nil then
        return string.sub(str, LuaPersonalSpaceMgrReal.SubStringGetTrueIndex(str, startIndex));
    else
        return string.sub(str, LuaPersonalSpaceMgrReal.SubStringGetTrueIndex(str, startIndex), LuaPersonalSpaceMgrReal.SubStringGetTrueIndex(str, endIndex + 1) - 1);
    end
end

function LuaPersonalSpaceMgrReal.SubStringGetTotalIndex(str)
    local curIndex = 0;
    local i = 1;
    local lastCount = 1;
    repeat
        lastCount = LuaPersonalSpaceMgrReal.SubStringGetByteCount(str, i)
        i = i + lastCount;
        curIndex = curIndex + 1;
    until(lastCount == 0);
    return curIndex - 1;
end

function LuaPersonalSpaceMgrReal.SubStringGetTrueIndex(str, index)
    local curIndex = 0;
    local i = 1;
    local lastCount = 1;
    repeat
        lastCount = LuaPersonalSpaceMgrReal.SubStringGetByteCount(str, i)
        i = i + lastCount;
        curIndex = curIndex + 1;
    until(curIndex >= index);
    return i - lastCount;
end

function LuaPersonalSpaceMgrReal.SubStringGetByteCount(str, index)
    local curByte = string.byte(str, index)
    local byteCount = 1
    if curByte == nil then
        byteCount = 0
    elseif curByte>0 and curByte<=127 then
        byteCount = 1
    elseif curByte>=192 and curByte<=223 then
        byteCount = 2
    elseif curByte>=224 and curByte<=239 then
        byteCount = 3
    elseif curByte>=240 and curByte<=247 then
        byteCount = 4
    end
    return byteCount
end

function LuaPersonalSpaceMgrReal.SetTextMore(showText,showLabel)
  local originText = CChatLinkMgr.TranslateToNGUIText(showText)
  showLabel.text = originText
  local result,outerText = showLabel:Wrap(originText)

  if not result then
    outerText = LuaPersonalSpaceMgrReal.SubStringUTF8(outerText, 1, string.len(outerText)) .. '..'
    showLabel.text = outerText
  end
end

function LuaPersonalSpaceMgrReal.GetSpaceGMIcon()
  return 'im_pmsg01'
end


--------------------------------------------------------------------------------------------
local CUITexture = import "L10.UI.CUITexture"
local UITexture = import "UITexture"
function LuaPersonalSpaceMgrReal.SetVideoPlayPic(node,size)
  local signNode = GameObject('videoSign')
  signNode.transform.parent = node.transform
  signNode.transform.localPosition = Vector3.zero
  signNode.transform.localScale = Vector3.one
  local uiTex = signNode:AddComponent(typeof(UITexture))
  local cuiTex = signNode:AddComponent(typeof(CUITexture))
  uiTex.width = size
  uiTex.height = size
  uiTex.depth = 20
  cuiTex:LoadMaterial("UI/Texture/Transparent/Material/mengdao_bofang.mat", false)
end

function LuaPersonalSpaceMgrReal.GetLiangHaoIcon(getMomentItem)
  if not getMomentItem then return "" end
  if not getMomentItem.lianghao then return "" end
  local lianghaoIcon = CUICommonDef.GetLiangHaoIcon(getMomentItem.lianghao.id, getMomentItem.lianghao.expiredtime, getMomentItem.lianghao.hideicon>0, false, getMomentItem.lianghao.showdigit>0)
  if LuaLiangHaoMgr.LiangHaoEnabled() and lianghaoIcon~=nil then
      return lianghaoIcon
  end
  return ""
end

local LocalString = import 'LocalString'
LuaPersonalSpaceMgrReal.EnableUpMoment = true
LuaPersonalSpaceMgrReal.UpMomentString = LocalString.GetString('是否置顶本心情?')
LuaPersonalSpaceMgrReal.CancelUpMomentString = LocalString.GetString('是否取消置顶本心情?')

function LuaPersonalSpaceMgrReal.SetMomentTopicUp(momentId,op,backFunction)
  local url = CPersonalSpaceMgr.BASE_URL.."qnm/moment/cancel_top"
  if op then
    url = CPersonalSpaceMgr.BASE_URL.."qnm/moment/set_top"
  end

  local dataTable = {}
  dataTable.momentId = momentId

  local backFunction = function(tData)
    if tData and tData.code and tData.code == 0 then
      if backFunction then
        backFunction(true)
      end
    else
      if backFunction then
        backFunction(false)
      end
    end
  end

  LuaPersonalSpaceMgrReal.PushData(url,dataTable,backFunction)
end

local CPersonalSpace_CircleGenerateType = import "L10.Game.CPersonalSpace_CircleGenerateType"
local MessageWndManager = import "L10.UI.MessageWndManager"

function LuaPersonalSpaceMgrReal.SetMomentTopicNodeInfo(upBtn,generateType,info,node,wnd)
  if generateType == CPersonalSpace_CircleGenerateType.SelfCircle then
    upBtn:SetActive(true)
    local upIcon = node.transform:Find('upbutton/cancel').gameObject
    if info.is_user_top then
      upIcon:SetActive(true)
      UIEventListener.Get(upBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        MessageWndManager.ShowConfirmMessage(LuaPersonalSpaceMgrReal.CancelUpMomentString, 10, false, DelegateFactory.Action(function ()
          LuaPersonalSpaceMgrReal.SetMomentTopicUp(info.id,false,function(code)
            if code then
              if not wnd.systemPortrait then
                return
              end
              if CPersonalSpaceMgr.Inst.NowShowInputButton ~= nil then
                g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("心情取消置顶成功!"))
                wnd:UpdateListInfo(0)
                CPersonalSpaceMgr.Inst.NowShowInputButton:SetValue(1, true)
              end
            end
          end)
        end), nil)
      end)
    else
      upIcon:SetActive(false)
      UIEventListener.Get(upBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        MessageWndManager.ShowConfirmMessage(LuaPersonalSpaceMgrReal.UpMomentString, 10, false, DelegateFactory.Action(function ()
          LuaPersonalSpaceMgrReal.SetMomentTopicUp(info.id,true,function(code)
            if code then
              if not wnd.systemPortrait then
                return
              end
              if CPersonalSpaceMgr.Inst.NowShowInputButton ~= nil then
                g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("心情置顶成功!"))
                wnd:UpdateListInfo(0)
                CPersonalSpaceMgr.Inst.NowShowInputButton:SetValue(1, true)
              end
            end
          end)
        end), nil)
      end)
    end
  else
    upBtn:SetActive(false)
  end
end

function LuaPersonalSpaceMgrReal.GetNosVideoInfo(videoUrl,backFunction)
  local url = videoUrl.."?vinfo"
  local form = CHTTPForm()
  Main.Inst:StartCoroutine(CPersonalSpaceMgr.Inst:DoPost(url, form, DelegateFactory.Action_bool_string(function (success, json)
    if success then
      if backFunction then
        local tData = JSON:decode(json)
        backFunction(tData)
      end
    end
  end)))
end


function LuaPersonalSpaceMgrReal:SetSlientLove(roleid, name, cls, gender, backFunction)
  self.SlientLoveRoleId = roleid
  self.SlientLoveCls = cls
  self.SlientLoveGender = gender
  self.SlientLoveName = name
  self.SlientLoveCallBack = backFunction
  CUIManager.ShowUI(CLuaUIResources.PersonalSpaceSlientLoveConfirmWnd)
end

function LuaPersonalSpaceMgrReal:CancelSlientLove(roleid,backFunction)
  self.SlientLoveCallBack = backFunction
  g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("MengDao_AnLian_Cancel"), function()
    Gac2Gas.CancelSecretLove(roleid)
  end, nil, nil, nil, false)
end

function LuaPersonalSpaceMgrReal:ComfirmSlientLoveRet(targetId, bIsSuccess, bIsBoth)
  if bIsSuccess and self.SlientLoveCallBack then
    self.SlientLoveCallBack(bIsBoth)
  end
end

function LuaPersonalSpaceMgrReal.CancelPlayerFollow(roleid,backFunction)
  local url = CPersonalSpaceMgr.BASE_URL.."qnm/follow/cancel"

  local dataTable = {}
  dataTable.targetid = roleid

  local backFunction = function(tData)
    if tData and tData.code and tData.code == 0 then
      if backFunction then
        backFunction(true)
      end
    else
      if backFunction then
        backFunction(false)
      end
    end
  end

  LuaPersonalSpaceMgrReal.PushData(url,dataTable,backFunction)
end

function LuaPersonalSpaceMgrReal.SetPlayerFollow(roleid,backFunction)
  local url = CPersonalSpaceMgr.BASE_URL.."qnm/follow/add"

  local dataTable = {}
  dataTable.targetid = roleid

  local backFunction = function(tData)
    if tData and tData.code and tData.code == 0 then
      if backFunction then
        backFunction(true)
      end
    elseif tData and tData.code and tData.code == -1000 then
      g_MessageMgr:ShowMessage('CUSTOM_STRING2',LocalString.GetString('已达到关注上限!'))
    else
      if backFunction then
        backFunction(false)
      end
    end
  end

  LuaPersonalSpaceMgrReal.PushData(url,dataTable,backFunction)
end

function LuaPersonalSpaceMgrReal.GetHistoryData(page,backFunction)
  local url = CPersonalSpaceMgr.BASE_URL.."qnm/follow/follow_list"

  local dataTable = {}
   -- dataTable.roleid = roleid
   dataTable.page = page
  -- dataTable.pageSize = 10
  local backFunction = function(tData)
    if tData and tData.code and tData.code == 0 then
      if backFunction then
        backFunction(tData)
      end
    else
      if backFunction then
        backFunction(nil)
      end
    end
  end

  LuaPersonalSpaceMgrReal.PushData(url,dataTable,backFunction)
end

function LuaPersonalSpaceMgrReal.BatchCancelFollow(roleidTable,backFunction)
  local url = CPersonalSpaceMgr.BASE_URL.."qnm/follow/cancel_batch"
  local roleids = ''
  if not roleidTable then
    return
  end

  local count = 1
  for i,v in pairs(roleidTable) do
    if count == 1 then
      roleids = tostring(i)
    else
      roleids = roleids .. ',' .. tostring(i)
    end
    count = count + 1
  end

  local dataTable = {}
   -- dataTable.roleid = roleid
   dataTable.targetids = roleids
  -- dataTable.pageSize = 10
  local back = function(tData)
    if tData and tData.code and tData.code == 0 then
      if backFunction then
        backFunction(tData)
      end
    else
      if backFunction then
        backFunction(nil)
      end
    end
  end

  LuaPersonalSpaceMgrReal.PushData(url,dataTable,back)
end



LuaPersonalSpaceMgrReal.UnFollowTable = nil

function LuaPersonalSpaceMgrReal.InitUnFollow(showNode)
  local normalNode = showNode.transform:Find('normal').gameObject
  local operateNode = showNode.transform:Find('operate').gameObject
  normalNode:SetActive(true)
  operateNode:SetActive(false)

  local manageBtn = showNode.transform:Find('normal/ManageButton').gameObject
  local unFollowBtn = showNode.transform:Find('operate/UnFollowButton').gameObject
  local backBtn = showNode.transform:Find('operate/BackButton').gameObject

  UIEventListener.Get(manageBtn).onClick = DelegateFactory.VoidDelegate(function (p)
    normalNode:SetActive(false)
    operateNode:SetActive(true)
    LuaPersonalSpaceMgrReal.StartUnFollow()
  end)
  UIEventListener.Get(backBtn).onClick = DelegateFactory.VoidDelegate(function (p)
    local count = 0
    if LuaPersonalSpaceMgrReal.UnFollowTable then
      for i,v in pairs(LuaPersonalSpaceMgrReal.UnFollowTable) do
        if v then
          count = count + 1
        end
      end
    end
    --if count > 0 then
    if true then
      MessageWndManager.ShowOKCancelMessage(LocalString.GetString("确定放弃当前对关注列表的管理吗？确定后，勾选将全部取消哦"), DelegateFactory.Action(function ()
        normalNode:SetActive(true)
        operateNode:SetActive(false)
        LuaPersonalSpaceMgrReal.EndUnFollow()
      end), nil, nil, nil, false)
    else
      normalNode:SetActive(true)
      operateNode:SetActive(false)
      LuaPersonalSpaceMgrReal.EndUnFollow()
    end
  end)
  UIEventListener.Get(unFollowBtn).onClick = DelegateFactory.VoidDelegate(function (p)
    local count = 0
    if LuaPersonalSpaceMgrReal.UnFollowTable then
      for i,v in pairs(LuaPersonalSpaceMgrReal.UnFollowTable) do
        if v then
          count = count + 1
        end
      end
    end
    if count > 0 then
      MessageWndManager.ShowOKCancelMessage(LocalString.GetString("确定取消关注勾选的对象吗？确定后，关注对象将从关注列表消失"), DelegateFactory.Action(function ()
        LuaPersonalSpaceMgrReal.DoUnFollow()
      end), nil, nil, nil, false)
    else
      normalNode:SetActive(true)
      operateNode:SetActive(false)
      LuaPersonalSpaceMgrReal.EndUnFollow()
    end
  end)
end

function LuaPersonalSpaceMgrReal.AddClickBtn(showNode,hideNode,btn,info)
  if not LuaPersonalSpaceMgrReal.ClickTable then
    LuaPersonalSpaceMgrReal.ClickTable = {}
  end
  table.insert(LuaPersonalSpaceMgrReal.ClickTable,{showNode,hideNode,btn,info})

  local arrowNode = btn.transform:Find('node').gameObject
  arrowNode:SetActive(false)
  UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(function (p)
    if arrowNode.activeSelf then
      arrowNode:SetActive(false)
      if LuaPersonalSpaceMgrReal.UnFollowTable then
        LuaPersonalSpaceMgrReal.UnFollowTable[info.roleinfo.roleid] = nil
      end
    else
      arrowNode:SetActive(true)
      if not LuaPersonalSpaceMgrReal.UnFollowTable then
        LuaPersonalSpaceMgrReal.UnFollowTable = {}
      end
      if LuaPersonalSpaceMgrReal.UnFollowTable then
        LuaPersonalSpaceMgrReal.UnFollowTable[info.roleinfo.roleid] = true
      end
    end
  end)
end

function LuaPersonalSpaceMgrReal.StartUnFollow()
  LuaPersonalSpaceMgrReal.UnFollowTable = {}
  if LuaPersonalSpaceMgrReal.ClickTable then
    for i,v in pairs(LuaPersonalSpaceMgrReal.ClickTable) do
      v[1]:SetActive(false)
      v[2]:SetActive(true)
      local arrowNode = v[3].transform:Find('node').gameObject
      arrowNode:SetActive(false)
    end
  end
end

function LuaPersonalSpaceMgrReal.EndUnFollow()
  if LuaPersonalSpaceMgrReal.ClickTable then
    for i,v in pairs(LuaPersonalSpaceMgrReal.ClickTable) do
      v[1]:SetActive(true)
      v[2]:SetActive(false)
    end
  end
  LuaPersonalSpaceMgrReal.UnFollowTable = nil
end

function LuaPersonalSpaceMgrReal.BatchUnFollowBack(tData)
  if tData and tData.code == 0 then
    g_MessageMgr:ShowMessage('CUSTOM_STRING2',LocalString.GetString('批量取消关注成功！'))
  end
  if LuaPersonalSpaceMgrReal.FollowPageBtn then
    LuaPersonalSpaceMgrReal.FollowPageBtn:SetValue(LuaPersonalSpaceMgrReal.FollowPageValue, true)
  end
end

function LuaPersonalSpaceMgrReal.DoUnFollow()
  if LuaPersonalSpaceMgrReal.UnFollowTable then
    LuaPersonalSpaceMgrReal.BatchCancelFollow(LuaPersonalSpaceMgrReal.UnFollowTable,LuaPersonalSpaceMgrReal.BatchUnFollowBack)
  else
    g_MessageMgr:ShowMessage('CUSTOM_STRING2',LocalString.GetString('未选中任何玩家！'))
  end
end

function LuaPersonalSpaceMgrReal.InitFollowItemNode(node,info,cls,gender,alertGoSpace)
  CommonDefs.GetComponent_Component_Type(node.transform:Find("icon"), typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(cls, gender, -1), false)
  CommonDefs.GetComponent_Component_Type(node.transform:Find("lv"), typeof(UILabel)).text = info.roleinfo.grade
  --CommonDefs.GetComponent_Component_Type(node.transform:Find("name"), typeof(UILabel)).text = info.roleinfo.rolename
  CPersonalSpaceMgr.SetTextMore(info.roleinfo.rolename, CommonDefs.GetComponent_Component_Type(node.transform:Find("name"), typeof(UILabel)))
  CPersonalSpaceMgr.Inst:addPlayerLinkBtn(node.transform:Find("icon/button").gameObject, info.roleinfo.roleid, EnumReportId_lua.eDefault, "", nil, 0, 0)
  if info.followingeach then
    node.transform:Find("attentionSign").gameObject:SetActive(true)
  else
    node.transform:Find("attentionSign").gameObject:SetActive(false)
  end
  if alertGoSpace then
    UIEventListener.Get(node.transform:Find("button").gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
      MessageWndManager.ShowOKCancelMessage(LocalString.GetString("确定放弃当前对关注列表的管理前往玩家梦岛主页吗？"), DelegateFactory.Action(function ()
        CPersonalSpaceMgr.Inst:OpenPersonalSpaceWnd(info.roleinfo.roleid, 0)
      end), nil, nil, nil, false)
    end)
  else
    UIEventListener.Get(node.transform:Find("button").gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
      CPersonalSpaceMgr.Inst:OpenPersonalSpaceWnd(info.roleinfo.roleid, 0)
    end)
  end
end

function LuaPersonalSpaceMgrReal.InitFollowedDetailPanel(this, fatherNode, showNode, template, totalData, pageValue, pageBtn)
  LuaPersonalSpaceMgrReal.ClickTable = {}
  LuaPersonalSpaceMgrReal.FollowPageValue = pageValue
  LuaPersonalSpaceMgrReal.FollowPageBtn = pageBtn
  fatherNode:SetActive(false)
  showNode:SetActive(true)

  local emptyNode = showNode.transform:Find("EmptyNode").gameObject
  if not totalData then
    emptyNode:SetActive(true)
    local table = CommonDefs.GetComponent_Component_Type(showNode.transform:Find("ScrollView/Table"), typeof(UITable))
    Extensions.RemoveAllChildren(table.transform)
    return
  end

  showNode.transform:Find('normal').gameObject:SetActive(true)
  showNode.transform:Find('operate').gameObject:SetActive(false)
  showNode.transform:Find('normal/text/num2'):GetComponent(typeof(UILabel)).text = totalData.fanscount
  showNode.transform:Find('normal/text/num1'):GetComponent(typeof(UILabel)).text = totalData.followcount
  showNode.transform:Find('operate/text/num2'):GetComponent(typeof(UILabel)).text = totalData.fanscount
  showNode.transform:Find('operate/text/num1'):GetComponent(typeof(UILabel)).text = totalData.followcount

  local data = totalData.list

  local scrollview = CommonDefs.GetComponent_Component_Type(showNode.transform:Find("ScrollView"), typeof(UIScrollView))
  local table = CommonDefs.GetComponent_Component_Type(showNode.transform:Find("ScrollView/Table"), typeof(UITable))
  Extensions.RemoveAllChildren(table.transform)
  UIEventListener.Get(showNode.transform:Find("normal/BackButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
    fatherNode:SetActive(true)
    showNode:SetActive(false)
  end)

  if #data <= 0 then
    emptyNode:SetActive(true)
  else
    emptyNode:SetActive(false)
  end

  template:SetActive(false)
  do
    local i = 1
    while i <= #data do
      local node = NGUITools.AddChild(table.gameObject, template)
      node:SetActive(true)
      local info = data[i]
      local cls = tonumber(info.roleinfo.clazz)
      local gender = tonumber(info.roleinfo.gender)
      local showNode = node.transform:Find('normal').gameObject
      local hideNode = node.transform:Find('operate').gameObject
      showNode:SetActive(true)
      hideNode:SetActive(false)

      LuaPersonalSpaceMgrReal.InitFollowItemNode(showNode,info,cls,gender)
      LuaPersonalSpaceMgrReal.InitFollowItemNode(hideNode,info,cls,gender,true)

      local unfollowClickNode = hideNode.transform:Find('click').gameObject
      --unfollowClickNode:SetActive(false)
      LuaPersonalSpaceMgrReal.AddClickBtn(showNode,hideNode,unfollowClickNode,info)

      i = i + 1
    end
  end

  table:Reposition()
  scrollview:ResetPosition()

  LuaPersonalSpaceMgrReal.InitUnFollow(showNode)
end

--------wish

function LuaPersonalSpaceMgrReal.GetSelfWishList(roleid,page,backFunction)
  local url = CPersonalSpaceMgr.BASE_URL.."qnm/wishlist/listByRole"

  local dataTable = {}
  -- dataTable.roleid = roleid
  dataTable.targetid = roleid
  dataTable.pageSize = 10
  dataTable.page = page
  local back = function(tData)
    if tData and tData.code and tData.code == 0 then
      if backFunction then
        backFunction(tData)
      end
    else
      if backFunction then
        backFunction(nil)
      end
    end
  end

  LuaPersonalSpaceMgrReal.PushData(url,dataTable,back)
end

function LuaPersonalSpaceMgrReal.GetFriendWishList(page,backFunction)
  local url = CPersonalSpaceMgr.BASE_URL.."qnm/wishlist/list"

  local dataTable = {}
  -- dataTable.roleid = roleid
  dataTable.pageSize = 10
  dataTable.page = page
  local back = function(tData)
    if tData and tData.code and tData.code == 0 then
      if backFunction then
        backFunction(tData)
      end
    else
      if backFunction then
        backFunction(nil)
      end
    end
  end

  LuaPersonalSpaceMgrReal.PushData(url,dataTable,back)
end

function LuaPersonalSpaceMgrReal.GetDetailWishInfo(wishId,backFunction)
  local url = CPersonalSpaceMgr.BASE_URL.."qnm/wishlist/detail"

  local dataTable = {}
  -- dataTable.roleid = roleid
  --dataTable.targetid = roleid
  dataTable.wishId = wishId
  local back = function(tData)
    if tData and tData.code and tData.code == 0 then
      if backFunction then
        backFunction(tData)
        --LuaPersonalSpaceMgrReal.QueryZhuliLimitAndOpenWnd(tData.data)
      end
      --g_ScriptEvent:BroadcastInLua("OpenWishDetailPanel", tData.data)
    elseif tData and tData.code and tData.code == -1005 then
      g_MessageMgr:ShowMessage('CUSTOM_STRING2',LocalString.GetString('该心愿已被遗忘，无法查看'))
      if backFunction then
        backFunction(nil)
      end
    else
      if backFunction then
        backFunction(nil)
      end
    end
  end

  LuaPersonalSpaceMgrReal.PushData(url,dataTable,back)
end

function LuaPersonalSpaceMgrReal.GetSingleDetailWishInfo(wishId,backFunction)
  local url = CPersonalSpaceMgr.BASE_URL.."qnm/wishlist/detail"

  local dataTable = {}
  -- dataTable.roleid = roleid
  --dataTable.targetid = roleid
  dataTable.wishId = wishId
  local back = function(tData)
    if tData and tData.code and tData.code == 0 and tData.data then
      if backFunction then
        backFunction(tData)
      end
      --g_ScriptEvent:BroadcastInLua("OpenWishDetailPanel", tData.data)
      LuaPersonalSpaceMgrReal.QueryZhuliLimitAndOpenWnd(tData.data)
    elseif tData and tData.code and tData.code == -1005 then
      g_MessageMgr:ShowMessage('CUSTOM_STRING2',LocalString.GetString('该心愿已被遗忘#35'))
      if backFunction then
        backFunction(nil)
      end
    else
      if backFunction then
        backFunction(nil)
      end
    end
  end

  LuaPersonalSpaceMgrReal.PushData(url,dataTable,back)
end

function LuaPersonalSpaceMgrReal.DeleteDetailWishComment(wishId,helpId,backFunction)
  local url = CPersonalSpaceMgr.BASE_URL.."qnm/wishlist/help_text/del"

  local dataTable = {}
  -- dataTable.roleid = roleid
  dataTable.wishId = wishId
  dataTable.helpId = helpId
  local back = function(tData)
    if tData and tData.code and tData.code == 0 then
      if backFunction then
        backFunction(tData)
      end
    else
      if backFunction then
        backFunction(nil)
      end
    end
  end

  LuaPersonalSpaceMgrReal.PushData(url,dataTable,back)
end

function LuaPersonalSpaceMgrReal.AddWishComment(wishId,text,backFunction)
  local url = CPersonalSpaceMgr.BASE_URL.."qnm/wishlist/help_text/add"

  local dataTable = {}
  dataTable.wishId = wishId
  dataTable.text = text
  local back = function(tData)
    if tData and tData.code and tData.code == 0 then
      if backFunction then
        backFunction(tData)
      end
    else
      if backFunction then
        backFunction(nil)
      end
    end
  end

  LuaPersonalSpaceMgrReal.PushData(url,dataTable,back)
end

function LuaPersonalSpaceMgrReal.QueryZhuliLimitAndOpenWnd(data)
  local config = PlayConfig_ServerIdEnabled.GetData(CLoginMgr.Inst.ServerGroupId)
  if config and config.Wish == 0 then
    g_MessageMgr:ShowMessage('CUSTOM_STRING2', LocalString.GetString('该服务器当前无法助力'))
  else
    if CClientMainPlayer.Inst then
      if CClientMainPlayer.Inst.Id == data.roleId then
        g_MessageMgr:ShowMessage('CUSTOM_STRING2',LocalString.GetString('无法为自己的心愿助力哦~#35'))
      else
        if data.currentProgress >= data.totalProgress then
          g_MessageMgr:ShowMessage('CUSTOM_STRING2',LocalString.GetString('该心愿已实现#50'))
        else
          local leftTime = data.endTime/1000 - CServerTimeMgr.Inst.timeStamp
          if leftTime < 0 then
            g_MessageMgr:ShowMessage('CUSTOM_STRING2',LocalString.GetString('该心愿已过期#35'))
          else
            LuaWishMgr.CurrentWishData = data
            Gac2Gas.LuaQueryMonthPresentLimit(0)
          end
        end
      end
    end
  end
end

---烟花编辑器
function LuaPersonalSpaceMgrReal.UpdateFireworkPicWithIndex(picUrl,index,backFunction)
  local url = CPersonalSpaceMgr.BASE_URL.."qnm/firework_photo/update"

  local dataTable = {}
  dataTable.url = picUrl
  dataTable.index = index

  local back = function(tData)
    if tData and tData.code and tData.code == 0 then
      if backFunction then
        backFunction(tData)
      end
    else
      if backFunction then
        backFunction(nil)
      end
    end
  end

  LuaPersonalSpaceMgrReal.PushData(url,dataTable,back)
end

function LuaPersonalSpaceMgrReal.RemoveFireworkPicWithIndex(index,backFunction)
  local url = CPersonalSpaceMgr.BASE_URL.."qnm/firework_photo/remove"

  local dataTable = {}
  dataTable.index = index

  local back = function(tData)
    if tData and tData.code and tData.code == 0 then
      if backFunction then
        backFunction(tData)
      end
    else
      if backFunction then
        backFunction(nil)
      end
    end
  end

  LuaPersonalSpaceMgrReal.PushData(url,dataTable,back)
end

function LuaPersonalSpaceMgrReal.GetFireworkPicListByPlayerId(playerId,backFunction)
  local url = CPersonalSpaceMgr.BASE_URL.."qnm/firework_photo/list"

  local dataTable = {}
  dataTable.targetid = playerId

  local back = function(tData)
    if tData and tData.code and tData.code == 0 then
      if backFunction then
        backFunction(tData)
      end
    else
      if backFunction then
        backFunction(nil)
      end
    end
  end

  LuaPersonalSpaceMgrReal.PushData(url,dataTable,back)
end

function LuaPersonalSpaceMgrReal.DeletePersonalSpaceVoice(backFunction)
  local url = CPersonalSpaceMgr.BASE_URL.."qnm/setsignaturevoice"

  local data = {}
  data.url = ''
  data.time = ''

  local dataJson = JSON:encode(data)

  local dataTable = {}
  dataTable.signaturevoice = dataJson

  local back = function(tData)
    if tData and tData.code and tData.code == 0 then
      if backFunction then
        backFunction(tData)
      end
    else
      if backFunction then
        backFunction(nil)
      end
    end
  end

  LuaPersonalSpaceMgrReal.PushData(url,dataTable,back)
end

function LuaPersonalSpaceMgrReal.SetPersonalSpaceVoice(wnd,signatureVoice)
  if not wnd then
    return
  end

  local selfInputNode = wnd.personalInfoNode.transform:Find("Signature/voiceInputNode").gameObject
  local selfModifyNode = wnd.personalInfoNode.transform:Find("Signature/voiceModifyNode").gameObject
  local otherPlayNode = wnd.personalInfoNode.transform:Find("OtherSignature/VoicePlayButton").gameObject

  local VoiceInputButton = wnd.personalInfoNode.transform:Find("Signature/voiceInputNode/VoiceInputButton").gameObject
  VoiceInputButton:SetActive(true)
  local VoiceCheckButton = wnd.personalInfoNode.transform:Find("Signature/voiceModifyNode/VoiceCheckButton").gameObject
  local VoiceDeleteButton = wnd.personalInfoNode.transform:Find("Signature/voiceModifyNode/VoiceDeleteButton").gameObject
  local OtherVoicePlayButton = wnd.personalInfoNode.transform:Find("OtherSignature/VoicePlayButton").gameObject

  local selfPlayAni = wnd.personalInfoNode.transform:Find('Signature/voiceModifyNode/VoiceCheckButton/LoudSpeaker'):GetComponent(typeof(UISpriteAnimation))
  local otherPlayAni = wnd.personalInfoNode.transform:Find('OtherSignature/VoicePlayButton/LoudSpeaker'):GetComponent(typeof(UISpriteAnimation))
  selfPlayAni:ResetToBeginning()
  selfPlayAni:Pause()
  otherPlayAni:ResetToBeginning()
  otherPlayAni:Pause()

  if CommonDefs.IS_HMT_CLIENT or CommonDefs.IS_VN_CLIENT then
    selfInputNode:SetActive(false)
    selfModifyNode:SetActive(false)
    otherPlayNode:SetActive(false)
  else
    if CPersonalSpaceMgr.SignatureVoice then
      if System.String.IsNullOrEmpty(signatureVoice.url) then
        selfInputNode:SetActive(true)
        selfModifyNode:SetActive(false)
        otherPlayNode:SetActive(false)
      else
        selfInputNode:SetActive(false)
        selfModifyNode:SetActive(true)
        otherPlayNode:SetActive(true)

        local backFunction = function()
          local empty = {}
          LuaPersonalSpaceMgrReal.SetPersonalSpaceVoice(wnd,empty)
        end
        UIEventListener.Get(VoiceDeleteButton).onClick = DelegateFactory.VoidDelegate(function (p)
          MessageWndManager.ShowConfirmMessage(LocalString.GetString('确定删除这条语音留言？'), 10, false, DelegateFactory.Action(function()
            LuaPersonalSpaceMgrReal.DeletePersonalSpaceVoice(backFunction)
          end), nil)
        end)
        UIEventListener.Get(VoiceCheckButton).onClick = DelegateFactory.VoidDelegate(function (p)
          CPersonalSpaceMgr.Inst:PlaySpaceVoice(signatureVoice.url)
          selfPlayAni:Play()

          if LuaPersonalSpaceMgrReal.m_PersonalSpaceVoiceTick then
            UnRegisterTick(LuaPersonalSpaceMgrReal.m_PersonalSpaceVoiceTick)
            LuaPersonalSpaceMgrReal.m_PersonalSpaceVoiceTick = nil
          end
          LuaPersonalSpaceMgrReal.m_PersonalSpaceVoiceTick = RegisterTickWithDuration(function ()
            if wnd and otherPlayAni then
              selfPlayAni:ResetToBeginning()
              selfPlayAni:Pause()
            end
          end,signatureVoice.time*1000,signatureVoice.time*1000)
        end)
        VoiceCheckButton.transform:Find('VoiceLengthLabel'):GetComponent(typeof(UILabel)).text = math.ceil(signatureVoice.time) .. LocalString.GetString('秒')
        UIEventListener.Get(OtherVoicePlayButton).onClick = DelegateFactory.VoidDelegate(function (p)
          CPersonalSpaceMgr.Inst:PlaySpaceVoice(signatureVoice.url)
          otherPlayAni:Play()

          if LuaPersonalSpaceMgrReal.m_PersonalSpaceVoiceTick then
            UnRegisterTick(LuaPersonalSpaceMgrReal.m_PersonalSpaceVoiceTick)
            LuaPersonalSpaceMgrReal.m_PersonalSpaceVoiceTick = nil
          end
          LuaPersonalSpaceMgrReal.m_PersonalSpaceVoiceTick = RegisterTickWithDuration(function ()
            if wnd and otherPlayAni then
              otherPlayAni:ResetToBeginning()
              otherPlayAni:Pause()
            end
          end,signatureVoice.time*1000,signatureVoice.time*1000)
        end)
        OtherVoicePlayButton.transform:Find('VoiceLengthLabel'):GetComponent(typeof(UILabel)).text = math.ceil(signatureVoice.time) .. LocalString.GetString('秒')
      end
    else
      selfInputNode:SetActive(false)
      selfModifyNode:SetActive(false)
      otherPlayNode:SetActive(false)
    end
  end
end

function LuaPersonalSpaceMgrReal.GetHomeDecorationPicWithPicIds(picIds,backFunction)

  local roleid=CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
  local url = CPersonalSpaceMgr.BASE_URL.."qnm/home_decorate_photo/get_by_ids"..SafeStringFormat3("?roleid=%s&ids=%s",roleid,picIds)

  local back = function(tData)
    if tData and tData.code and tData.code == 0 then
      if backFunction then
        backFunction(tData)
      end
    else
      if backFunction then
        backFunction(nil)
      end
    end
  end

    L10.Engine.Main.Inst:StartCoroutine(HTTPHelper.NOSDownloadFileData(url, DelegateFactory.Action_bool_byte_Array(function (sign, bytes) 
      if sign then
        local json = Encoding.UTF8:GetString(bytes)
        local dict = Json.Deserialize(json)
        back(dict)
      end
    end)))
end

function LuaPersonalSpaceMgrReal.UpdateHomeDecoretionPicWithIndex(picUrl,index,backFunction,interfaceName)
  local url = CPersonalSpaceMgr.BASE_URL.."qnm/home_decorate_photo/update"

  local dataTable = {}
  dataTable.url = picUrl
  dataTable.index = index

  local back = function(tData)
    if tData and tData.code and tData.code == 0 then
      if backFunction then
        backFunction(tData)
      end
    else
      if backFunction then
        backFunction(nil)
      end
    end
  end
  print(url)
  LuaPersonalSpaceMgrReal.PushData(url,dataTable,back)
end