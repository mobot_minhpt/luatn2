local QnSelectableButton = import "L10.UI.QnSelectableButton"
local CButton = import "L10.UI.CButton"
local UILabel = import "UILabel"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local QnButton = import "L10.UI.QnButton"
local QnCheckBox = import "L10.UI.QnCheckBox"
local DelegateFactory  = import "DelegateFactory"
local AlignType = import "L10.UI.CTooltip+AlignType"
local CNumberKeyBoardMgr = import "CNumberKeyBoardMgr"

LuaFriendChatSettingWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaFriendChatSettingWnd, "AddFriendLimitCheckbox", "AddFriendLimitCheckbox", QnCheckBox)
RegistChildComponent(LuaFriendChatSettingWnd, "FriendLimitInputBtn", "FriendLimitInputBtn", QnButton)
RegistChildComponent(LuaFriendChatSettingWnd, "SwitchButton", "SwitchButton", QnSelectableButton)
RegistChildComponent(LuaFriendChatSettingWnd, "BlueButton", "BlueButton", CButton)
RegistChildComponent(LuaFriendChatSettingWnd, "CloseBtnLabel", "CloseBtnLabel", UILabel)
--RegistChildComponent(LuaFriendChatSettingWnd, "ChatLimitCheckbox", "ChatLimitCheckbox", QnCheckBox)
--RegistChildComponent(LuaFriendChatSettingWnd, "ChatLimitInputBtn", "ChatLimitInputBtn", QnButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaFriendChatSettingWnd, "m_AddFriendLimitLv")
RegistClassMember(LuaFriendChatSettingWnd, "m_AddFriendLimitLvText")
--RegistClassMember(LuaFriendChatSettingWnd, "m_ChatLimitLv")
--RegistClassMember(LuaFriendChatSettingWnd, "m_ChatLimitLvText")
RegistClassMember(LuaFriendChatSettingWnd, "m_InputMax")
RegistClassMember(LuaFriendChatSettingWnd, "m_InputMin")

function LuaFriendChatSettingWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.BlueButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBlueButtonClick()
	end)


    --@endregion EventBind end
end

function LuaFriendChatSettingWnd:Init()
    self.m_InputMax = 160
    self.m_InputMin = 1

    self.m_AddFriendLimitLvText = self.FriendLimitInputBtn.transform:Find("NumLab"):GetComponent(typeof(UILabel))
--    self.m_ChatLimitLvText = self.ChatLimitInputBtn.transform:Find("NumLab"):GetComponent(typeof(UILabel))
    self:InitWndShow()
    self.SwitchButton.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:SetCenterLabel()
    end)
--[[
    self.AddFriendLimitCheckbox.OnValueChanged = DelegateFactory.Action_bool(function (selected)
        self.AddFriendLimitCheckbox:SetSelected(selected, true)
        self:SendSettingInfo()
    end)

    self.ChatLimitCheckbox.OnValueChanged = DelegateFactory.Action_bool(function (selected)
        self.ChatLimitCheckbox:SetSelected(selected, true)
        self:SendSettingInfo()
    end)
]]
    self.FriendLimitInputBtn.OnClick = DelegateFactory.Action_QnButton(function (go)
        self:ShowAddFriendInputWnd()
    end)
--[[   
    self.ChatLimitInputBtn.OnClick = DelegateFactory.Action_QnButton(function (go)
        self:ShowChatInputWnd()
    end)
]]

end

function LuaFriendChatSettingWnd:SetCenterLabel()
    if self.SwitchButton:isSeleted() then
        self.AddFriendLimitCheckbox.gameObject:SetActive(true)
        self.CloseBtnLabel.gameObject:SetActive(false)
    else
        self.AddFriendLimitCheckbox.gameObject:SetActive(false)
        self.CloseBtnLabel.gameObject:SetActive(true)
    end
end

function LuaFriendChatSettingWnd:InitWndShow()
    --self.AddFriendLimitCheckbox:SetSelected(LuaChatMgr.FriendChatSettingInfo.AddFriendSet,true)
    --self.ChatLimitCheckbox:SetSelected(LuaChatMgr.FriendChatSettingInfo.ChatSet, true)
    local MainPlayer = CClientMainPlayer.Inst
    if MainPlayer then
        self.SwitchButton:SetSelected(not (MainPlayer.RelationshipProp.IgnoreAddFriendRequest == 1))
        self.m_AddFriendLimitLvText.text = tostring(MainPlayer.RelationshipProp.AddFriendLvThreashold)
        self.m_AddFriendLimitLv = MainPlayer.RelationshipProp.AddFriendLvThreashold
        if MainPlayer.RelationshipProp.AddFriendLvThreashold < self.m_InputMin then
            self.m_AddFriendLimitLvText.text = tostring(self.m_InputMin)
            self.m_AddFriendLimitLv = self.m_InputMin
        end
        self:SetCenterLabel()
    end
    --self.m_ChatLimitLvText.text = tostring(LuaChatMgr.FriendChatSettingInfo.ChatLv)
    --self.m_ChatLimitLv = LuaChatMgr.FriendChatSettingInfo.ChatLv
end

function LuaFriendChatSettingWnd:ShowAddFriendInputWnd()
    CNumberKeyBoardMgr.ShowNumberKeyBoardWithRange(self.m_InputMin, self.m_InputMax, self.m_AddFriendLimitLv, 100, DelegateFactory.Action_int(function (val) 
        self.m_AddFriendLimitLvText.text = tostring(val)
    end), DelegateFactory.Action_int(function (val)
        self.m_AddFriendLimitLv = val
        if val < self.m_InputMin then
            self.m_AddFriendLimitLv = self.m_InputMin
            self.m_AddFriendLimitLvText.text = tostring(self.m_InputMin)
        end 
        --self:SendSettingInfo()
    end), self.m_AddFriendLimitLvText, AlignType.Right, true)
end
--[[
function LuaFriendChatSettingWnd:ShowChatInputWnd()
    CNumberKeyBoardMgr.ShowNumberKeyBoardWithRange(0, self.m_InputMax, self.m_ChatLimitLv, 100, DelegateFactory.Action_int(function (val) 
        self.m_ChatLimitLvText.text = tostring(val)
    end), DelegateFactory.Action_int(function (val)
        self.m_ChatLimitLv = val
        self:SendSettingInfo()
    end), self.m_ChatLimitLvText, AlignType.Right, true)
end

function LuaFriendChatSettingWnd:SendSettingInfo()
end
]]
--@region UIEvent

function LuaFriendChatSettingWnd:OnBlueButtonClick()
    Gac2Gas.SetIgnoreAddFriendRequest(not self.SwitchButton:isSeleted())
    Gac2Gas.SetAddFriendLvThreashold(self.m_AddFriendLimitLv) 
    CUIManager.CloseUI(CLuaUIResources.FriendChatSettingWnd)
end


--@endregion UIEvent

