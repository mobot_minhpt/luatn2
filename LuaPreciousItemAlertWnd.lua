local CUITexture=import "L10.UI.CUITexture"
local CChatLinkMgr=import "CChatLinkMgr"
local CItem=import "L10.Game.CItem"
--暂时只支持一个物品
CLuaPreciousItemAlertWnd = class()

CLuaPreciousItemAlertWnd.ItemTemplateIds = nil

function CLuaPreciousItemAlertWnd:Init()
    local icon = FindChild(self.transform,"ItemIcon"):GetComponent(typeof(CUITexture))
    local descLabel = FindChild(self.transform,"DescLabel"):GetComponent(typeof(UILabel))
    local itemNameLabel = FindChild(self.transform,"ItemNameLabel"):GetComponent(typeof(UILabel))

    if CLuaPreciousItemAlertWnd.ItemTemplateIds and #CLuaPreciousItemAlertWnd.ItemTemplateIds >0 then
        local itemId = CLuaPreciousItemAlertWnd.ItemTemplateIds[1]
        local itemData = Item_Item.GetData(itemId)
        icon:LoadMaterial(itemData.Icon)
        descLabel.text = CChatLinkMgr.TranslateToNGUIText(CItem.GetItemDescription(itemId,true),false)
        itemNameLabel.text=itemData.Name
    end
end