-- Auto Generated!!
local CJieBaiChangeNameWnd = import "L10.UI.CJieBaiChangeNameWnd"
local CJieBaiMgr = import "L10.Game.CJieBaiMgr"
local CJieBaiWnd = import "L10.UI.CJieBaiWnd"
local DelegateFactory = import "DelegateFactory"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local JieBai_Setting = import "L10.Game.JieBai_Setting"
local LocalString = import "LocalString"
local UIEventListener = import "UIEventListener"
local UIInput = import "UIInput"
CJieBaiChangeNameWnd.m_SubmitInfo_CS2LuaHook = function (this) 
    local beginNameInput = CommonDefs.GetComponent_GameObject_Type(this.BeginNameInputNode, typeof(UIInput))
    local lastNameInput = CommonDefs.GetComponent_GameObject_Type(this.LastNameInputNode, typeof(UIInput))
    local beginName = beginNameInput.value
    local lastName = lastNameInput.value
    if not System.String.IsNullOrEmpty(beginName) and not System.String.IsNullOrEmpty(lastName) then
        if CommonDefs.StringLength(beginName) > CJieBaiChangeNameWnd.MaxTitle1Length then
            g_MessageMgr:ShowMessage("CHUANJIABAO_NAME_LIMIT", LocalString.GetString("结拜称号前缀"), CJieBaiChangeNameWnd.MaxTitle1Length)
            return
        end
        if CommonDefs.StringLength(lastName) > CJieBaiChangeNameWnd.MinTitle2Length then
            g_MessageMgr:ShowMessage("CHUANJIABAO_NAME_LIMIT", LocalString.GetString("结拜称号后缀"), CJieBaiChangeNameWnd.MinTitle2Length)
            return
        end


        local totalName = (beginName .. this.MidNameLabel.text) .. lastName
        if this.MoneyCtrl.moneyEnough then
            if CJieBaiMgr.Inst:CheckJiebaiTitleNameBySelfName(totalName) then
                Gac2Gas.SetJiebaiCommonTitle(totalName)
                this:Close()
            end
        else
            g_MessageMgr:ShowMessage("NotEnough_Silver")
        end
    else
        g_MessageMgr:ShowMessage("PLAYERSHOP_NOT_NO_NAME")
    end
end
CJieBaiChangeNameWnd.m_Init_CS2LuaHook = function (this) 
    if CJieBaiMgr.Inst.sessionData.memberCount > 0 and CJieBaiMgr.Inst.sessionData.memberCount <= CJieBaiWnd.TitleMidName.Length then
        this.MoneyCtrl:SetType(EnumMoneyType.YinPiao, EnumPlayScoreKey.NONE, true)
        this.MoneyCtrl:SetCost(JieBai_Setting.GetData().CommonJieBaiTitleMoney)
        this.MidNameLabel.text = CJieBaiWnd.TitleMidName[CJieBaiMgr.Inst.sessionData.memberCount - 1]

        UIEventListener.Get(this.CancelBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
            this:ClosePanel()
        end)

        UIEventListener.Get(this.CloseBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
            this:ClosePanel()
        end)

        UIEventListener.Get(this.SubmitBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
            this:SubmitInfo()
        end)
    else
        this:ClosePanel()
    end
end
