local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UISprite = import "UISprite"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local CUIFx = import "L10.UI.CUIFx"
local Item_Item = import "L10.Game.Item_Item"
local IdPartition = import "L10.Game.IdPartition"
local EquipmentTemplate_Equip=import "L10.Game.EquipmentTemplate_Equip"
local CEquipment = import "L10.Game.CEquipment"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CItem = import "L10.Game.CItem"
local NGUIText = import "NGUIText"
local GameSetting_Common_Wapper = import "L10.Game.GameSetting_Common_Wapper"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local UIEventListener = import "UIEventListener"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local EnumQualityType = import "L10.Game.EnumQualityType"
local UITexture = import "UITexture"

LuaAwardItemTemplate = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaAwardItemTemplate, "QualitySprite", "QualitySprite", UISprite)
RegistChildComponent(LuaAwardItemTemplate, "DisableSprite", "DisableSprite", UISprite)
RegistChildComponent(LuaAwardItemTemplate, "IconTexture", "IconTexture", CUITexture)
RegistChildComponent(LuaAwardItemTemplate, "ItemNameLabel", "ItemNameLabel", UILabel)
RegistChildComponent(LuaAwardItemTemplate, "SelectedSprite", "SelectedSprite", UITexture)
RegistChildComponent(LuaAwardItemTemplate, "AmountLabel", "AmountLabel", UILabel)
RegistChildComponent(LuaAwardItemTemplate, "BoomAniFx", "BoomAniFx", CUIFx)

--@endregion RegistChildComponent end
RegistClassMember(LuaAwardItemTemplate, "m_templateId")



function LuaAwardItemTemplate:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    UIEventListener.Get(self.IconTexture.gameObject).onClick =  DelegateFactory.VoidDelegate(
        function(go)
            CItemInfoMgr.ShowLinkItemTemplateInfo(self.m_templateId)
        end
    )

end

function LuaAwardItemTemplate:Init(templateId , count , showColor)
    self.m_templateId = templateId
    if count > 1 then
        self.AmountLabel.text = count:ToString()
    else
        self.AmountLabel.text = LocalString.GetString("")
    end

    if IdPartition.IdIsItem(templateId) then
        local item = Item_Item.GetData(templateId)
        self.QualitySprite.spriteName = CUICommonDef.GetItemCellBorder(item)
        self.DisableSprite.enabled = not CItem.GetMainPlayerIsFit(templateId)
        self.IconTexture:LoadMaterial(item.Icon)
        if showColor == true then
            self.ItemNameLabel.text = System.String.Format("[c][{0}]{1}[-][/c]",
            NGUIText.EncodeColor24(GameSetting_Common_Wapper.Inst:GetColor(item.NameColor)),  --物品颜色
            item.Name)
        else
            self.ItemNameLabel.text = System.String.Format("{0}",
            item.Name)
        end
    else
        local equip = EquipmentTemplate_Equip.GetData(templateId)
        if equip then
            self.QualitySprite.spriteName = CUICommonDef.GetItemCellBorder(CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), equip.Color))
            self.DisableSprite.enabled = not CEquipment.GetMainPlayerIsFit(templateId)
            self.IconTexture:LoadMaterial(equip.Icon)
            if showColor then
                self.ItemNameLabel.text = System.String.Format("[c][{0}]{1}[-][/c]",
                NGUIText.EncodeColor(CEquipment.GetColor(templateId)),
                equip.Name)
            else
                self.ItemNameLabel.text = System.String.Format("{0}",
                NGUIText.EncodeColor(CEquipment.GetColor(templateId)),
                equip.Name)
            end
        else
            self.QualitySprite.spriteName = LocalString.GetString("")
            self.DisableSprite.enabled = false
            self.IconTexture:Clear()
            self.ItemNameLabel.text = LocalString.GetString("")
        end
    end
end

function LuaAwardItemTemplate:ShowBoom()
    self.BoomAniFx:DestroyFx();
    self.BoomAniFx:LoadFx(CUIFxPaths.PlayerLevelUpBoomFxPath);
end

function LuaAwardItemTemplate:Selected(value)
    if self.SelectedSprite then
        self.SelectedSprite.enabled = value
    end
end

function LuaAwardItemTemplate:OnDisable()
    self.BoomAniFx.gameObject:SetActive(false)
end
--@region UIEvent

--@endregion UIEvent

