require("common/common_include")
local UILabel=import "UILabel"
local CScene=import "L10.Game.CScene"
local CGamePlayMgr=import "L10.Game.CGamePlayMgr"
local UITable=import "UITable"
local Gac2Gas=import "L10.Game.Gac2Gas"
local CUIFx=import "L10.UI.CUIFx"
local CUIFxPaths=import "L10.UI.CUIFxPaths"
local LuaUtils=import "LuaUtils"
local LuaTweenUtils=import "LuaTweenUtils"
local CGuanNingMgr=import "L10.Game.CGuanNingMgr"
local CChatLinkMgr=import "CChatLinkMgr"

CLuaGuanNingTaskView=class()
RegistClassMember(CLuaGuanNingTaskView,"m_RemainTimeLabel")
RegistClassMember(CLuaGuanNingTaskView,"m_GameplayNameLabel")
RegistClassMember(CLuaGuanNingTaskView,"m_LeaveButton")
RegistClassMember(CLuaGuanNingTaskView,"m_SituationButton")
RegistClassMember(CLuaGuanNingTaskView,"m_Table")

RegistClassMember(CLuaGuanNingTaskView,"m_WuShenRemind")
RegistClassMember(CLuaGuanNingTaskView,"m_WuShenTask")

RegistClassMember(CLuaGuanNingTaskView,"m_UIFx")
RegistClassMember(CLuaGuanNingTaskView,"m_Finish")

function CLuaGuanNingTaskView:Awake()
    self.m_Table=self.transform:GetComponent(typeof(UITable))
    self.m_RemainTimeLabel=FindChild(self.transform,"RemainTimeLabel"):GetComponent(typeof(UILabel))
    self.m_RemainTimeLabel.text=nil

    self.m_GameplayNameLabel=FindChild(self.transform,"TaskName"):GetComponent(typeof(UILabel))

    self.m_LeaveButton=FindChild(self.transform,"LeaveBtn").gameObject 

    self.m_WuShenRemind=FindChild(self.transform,"WuShenRemind").gameObject
    self.m_WuShenTask=FindChild(self.transform,"WuShenTask").gameObject
    self.m_SituationButton=FindChild(self.transform,"SituationBtn").gameObject
    self.m_UIFx=FindChild(self.transform,"Fx"):GetComponent(typeof(CUIFx))

    self.m_Finish=FindChild(self.transform,"Finish").gameObject
    self.m_Finish:SetActive(false)

    
    CommonDefs.AddOnClickListener(self.m_SituationButton,DelegateFactory.Action_GameObject(function(go)
        CUIManager.ShowUI(CUIResources.GuanNingResultWnd)
    end),false)

    CommonDefs.AddOnClickListener(self.m_WuShenRemind,DelegateFactory.Action_GameObject(function(go)
        CUIManager.ShowUI(CUIResources.GuanNingWuShenTaskWnd)
    end),false)
end

--断线重连
function CLuaGuanNingTaskView:Init()

    if CGuanNingMgr.s_UpdateGuanNingChallengeTaskInfo then
        self:OnUpdateGuanNingChallengeTaskInfo()
    end

    -- self.m_WuShenRemind:SetActive(CLuaGuanNingWuShenMgr.s_ShowGuanNingChallengeTaskChoiceRemind)
    -- self.m_WuShenTask:SetActive(CLuaGuanNingWuShenMgr.s_UpdateGuanNingChallengeTaskInfo)
    -- self.m_Table:Reposition()

    --self.m_LeaveButton:SetActive(CScene.MainScene.ShowLeavePlayButton)

    self.m_GameplayNameLabel.text=nil
    if CScene.MainScene then
        self.m_GameplayNameLabel.text=CScene.MainScene.SceneName
    end

    self:OnShowTimeCountDown()

    --一开始全部设置成false 然后跟服务器请求数据
    CLuaGuanNingWuShenMgr.s_ShowGuanNingChallengeTaskChoiceRemind=false
    self:Refresh()
end

function CLuaGuanNingTaskView:Refresh()
    self.m_WuShenRemind:SetActive(CLuaGuanNingWuShenMgr.s_ShowGuanNingChallengeTaskChoiceRemind)
    self.m_WuShenTask:SetActive(CGuanNingMgr.s_UpdateGuanNingChallengeTaskInfo)
    self.m_Table:Reposition()
end


function CLuaGuanNingTaskView:OnEnable()
    self:OnSyncGnjcZhiHuiInfo(CLuaGuanNingMgr.m_CommanderOpen)
    g_ScriptEvent:AddListener("SyncGnjcZhiHuiInfo", self, "OnSyncGnjcZhiHuiInfo")
    g_ScriptEvent:AddListener("MainPlayerCreated", self, "OnMainPlayerCreated")
    g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
    g_ScriptEvent:AddListener("CanLeaveGamePlay", self, "OnCanLeaveGamePlay")
    g_ScriptEvent:AddListener("ShowTimeCountDown", self, "OnShowTimeCountDown")
    g_ScriptEvent:AddListener("ShowGuanNingChallengeTaskChoiceRemind", self, "OnShowGuanNingChalleageTaskChoiceRemind")
    g_ScriptEvent:AddListener("UpdateGuanNingChallengeTaskInfo", self, "OnUpdateGuanNingChallengeTaskInfo")
    
end
function CLuaGuanNingTaskView:OnDisable()
    g_ScriptEvent:RemoveListener("SyncGnjcZhiHuiInfo", self, "OnSyncGnjcZhiHuiInfo")
    g_ScriptEvent:RemoveListener("MainPlayerCreated", self, "OnMainPlayerCreated")
    g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
    g_ScriptEvent:RemoveListener("CanLeaveGamePlay", self, "OnCanLeaveGamePlay")
    g_ScriptEvent:RemoveListener("ShowTimeCountDown", self, "OnShowTimeCountDown")
    g_ScriptEvent:RemoveListener("ShowGuanNingChallengeTaskChoiceRemind", self, "OnShowGuanNingChalleageTaskChoiceRemind")
    g_ScriptEvent:RemoveListener("UpdateGuanNingChallengeTaskInfo", self, "OnUpdateGuanNingChallengeTaskInfo")
end

function CLuaGuanNingTaskView:OnSyncGnjcZhiHuiInfo(bOpen)
    if bOpen then
        FindChild(self.m_LeaveButton.transform, "Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("校场指挥")
        CommonDefs.AddOnClickListener(self.m_LeaveButton,DelegateFactory.Action_GameObject(function(go)
            CUIManager.ShowUI(CLuaUIResources.GuanNingCommandWnd)
        end),false)
    else
        FindChild(self.m_LeaveButton.transform, "Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("离开")
        CommonDefs.AddOnClickListener(self.m_LeaveButton,DelegateFactory.Action_GameObject(function(go)
            CGamePlayMgr.Inst:LeavePlay()
        end),false)
    end
end

function CLuaGuanNingTaskView:OnShowGuanNingChalleageTaskChoiceRemind()
    self.m_WuShenRemind:SetActive(true)
    self.m_WuShenTask:SetActive(false)
    self.m_Table:Reposition()

    self.m_UIFx:DestroyFx()
    self.m_UIFx:LoadFx(CUIFxPaths.CommonYellowLineFxPath)
    LuaTweenUtils.DOKill(self.m_UIFx.FxRoot,false)
    local path={Vector3(180,35,0),Vector3(180,-35,0),Vector3(-180,-35,0),Vector3(-180,35,0),Vector3(180,35,0)}
    local array = Table2Array(path, MakeArrayClass(Vector3))
    LuaUtils.SetLocalPosition(self.m_UIFx.FxRoot,180,35,0)
    LuaTweenUtils.DODefaultLocalPath(self.m_UIFx.FxRoot,array,2)
end


function CLuaGuanNingTaskView:OnUpdateGuanNingChallengeTaskInfo()
    self.m_UIFx:DestroyFx()
    
    local taskId=CLuaGuanNingWuShenMgr.taskId
    local needValue=CLuaGuanNingWuShenMgr.FormatNum(CLuaGuanNingWuShenMgr.needValue)
    local curValue=CLuaGuanNingWuShenMgr.FormatNum(CLuaGuanNingWuShenMgr.curValue)
    local bFinish=CLuaGuanNingWuShenMgr.bFinish
    local awardScore=CLuaGuanNingWuShenMgr.awardScore

    if taskId>0 then
        local designData = GuanNing_ChallengeTask.GetData(taskId)
        local format=CChatLinkMgr.TranslateToNGUIText(designData.TaskDesc)

        self.m_Finish:SetActive(bFinish)
        local c=bFinish and "[00ff00]" or "[ff0000]"
        local str="[c]"..c..curValue.."[-][/c]".."[c][ffffff]/"..needValue.."[-][/c]"

        local tf=self.m_WuShenTask.transform
        local nameLabel=FindChild(tf,"TaskName2"):GetComponent(typeof(UILabel))
        nameLabel.text=designData.TaskName
        local infoLabel=FindChild(tf,"TaskInfo"):GetComponent(typeof(UILabel))
        infoLabel.text=SafeStringFormat3(format,str)

        self.m_WuShenRemind:SetActive(false)
        self.m_WuShenTask:SetActive(true)--触发一次size调整
        local bg=FindChild(tf,"Bg"):GetComponent(typeof(UISprite))
        bg:ResetAndUpdateAnchors()
        local bg2=FindChild(bg.transform,"Sprite"):GetComponent(typeof(UISprite))
        bg2:ResetAndUpdateAnchors()
        self.m_Table:Reposition()
    end
end

function CLuaGuanNingTaskView:OnShowTimeCountDown()
    if CScene.MainScene then
        self.m_RemainTimeLabel.enabled = CScene.MainScene.ShowTimeCountDown or CScene.MainScene.ShowMonsterCountDown
    end
end

function CLuaGuanNingTaskView:OnCanLeaveGamePlay()
    if CScene.MainScene then
        --self.m_LeaveButton:SetActive(CScene.MainScene.ShowLeavePlayButton)
    end
end

--断线重连，请求数据
function CLuaGuanNingTaskView:OnMainPlayerCreated()
    self:Init()
    Gac2Gas.QueryHasGuanNingChallengeTaskChoice()
end

function CLuaGuanNingTaskView:OnSceneRemainTimeUpdate(args)
    if CScene.MainScene then
        if CScene.MainScene.ShowTimeCountDown then
            self.m_RemainTimeLabel.text = self:GetRemainTimeText(CScene.MainScene.ShowTime)
        else
            self.m_RemainTimeLabel.text = nil
        end
    else
        self.m_RemainTimeLabel.text = nil
    end
end
function CLuaGuanNingTaskView:GetRemainTimeText(totalSeconds)
    if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return SafeStringFormat3("[ACF9FF]%02d:%02d:%02d[-]", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        return SafeStringFormat3("[ACF9FF]%02d:%02d[-]", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end

return CLuaGuanNingTaskView
