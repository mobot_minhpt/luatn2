local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"
local CameraMode = import "L10.Engine.CameraControl.CameraMode"
local Vector3 = import "UnityEngine.Vector3"
local MessageWndManager = import "L10.UI.MessageWndManager"
local PlayerSettings = import "L10.Game.PlayerSettings"

local CScene = import "L10.Game.CScene"
local CLightMgr = import "L10.Engine.CLightMgr"

local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CHeadInfoWnd = import "L10.UI.CHeadInfoWnd"
local CSkillButtonBoardWnd = import "L10.UI.CSkillButtonBoardWnd"

local MusicFireworkMgr = import "L10.Game.MusicFireworkMgr"

CLuaQiXiMgr = class()
CLuaQiXiMgr.m_InputTempText = ""

function CLuaQiXiMgr.TryOpenQiXiLiuXingYu3D()
	local liuXingYuCamera = QiXi_QiXi2019.GetData().LiuXingYuCamera
	local r, z, y = liuXingYuCamera[0], liuXingYuCamera[1], liuXingYuCamera[2]
	local vec3 = Vector3(r, z, y)
	if CameraFollow.Inst.CurMode ~= CameraMode.E3DPlus then
		local message = g_MessageMgr:FormatMessage("QIXI2019_LIUXINGYU_OPEN_3DPLUS_CONFIRM")
		MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function()
			CameraFollow.Inst:SetMode(CameraMode.E3DPlus)
			PlayerSettings.Saved3DMode = 2
			CameraFollow.Inst.targetRZY = vec3
			CameraFollow.Inst.CurCameraParam:ApplyRZY(vec3)
		end), nil)
		return
	end
	CameraFollow.Inst.targetRZY = vec3
	CameraFollow.Inst.CurCameraParam:ApplyRZY(vec3)
end

--[[
    @desc: 开启2020七夕模块
    author:{author}
    time:2020-07-21 10:31:00
    @return:
]]
function CLuaQiXiMgr.ModulesStart()
	CLuaQiXiMgr.DelListener()
	CLuaQiXiMgr.AddListener()
	CLuaQiXiMgr.Start2020 = true
	CLuaQiXiMgr.ProcessCamera()
end

function CLuaQiXiMgr.AddListener()
	g_ScriptEvent:AddListener("MainPlayerCreated", CLuaQiXiMgr, "MainPlayerCreated")
	g_ScriptEvent:AddListener("GasDisconnect", CLuaQiXiMgr, "OnDisconnect")
end

function CLuaQiXiMgr.DelListener()
	g_ScriptEvent:RemoveListener("MainPlayerCreated", CLuaQiXiMgr, "MainPlayerCreated")
	g_ScriptEvent:RemoveListener("GasDisconnect", CLuaQiXiMgr, "OnDisconnect")
end

--[[
    @desc: 结束2020七夕模块
    author:{author}
    time:2020-07-21 10:31:30
    @return:
]]
function CLuaQiXiMgr.ModulesEnd()
	CLuaQiXiMgr.DelListener()
	CLuaQiXiMgr.Start2020 = false
	if CameraFollow.Inst then
		CameraFollow.Inst.MaxSpHeight = 0
	end
end

function CLuaQiXiMgr.IsInPlay2021()
	if not CLuaQiXiMgr.Start2020  then return false end
	if CScene.MainScene == nil then return false end
	local data = QiXi_FireWork2021.GetData() 
	local pid = CScene.MainScene.GamePlayDesignId
	if pid == data.HZGameplayId or pid == data.JSJGameplayId then
		return true
	end
	return false
end

function CLuaQiXiMgr.OnDisconnect()
	MusicFireworkMgr.Inst:ClearData()
end

--[[
    @desc: 主角创建事件，转场景会触发
    author:{author}
    time:2020-07-21 10:33:39
    @return:
]]
function CLuaQiXiMgr.MainPlayerCreated()
	CLuaQiXiMgr.ProcessCamera()
	CLuaQiXiMgr.ProcessSceneBrightness(0)
	CLuaQiXiMgr.Process2021()
	if CLuaQiXiMgr.CacheId and CLuaQiXiMgr.CacheTime then
		CLuaQiXiMgr.ProcessFireworkChapter(CLuaQiXiMgr.CacheId,CLuaQiXiMgr.CacheTime)
	end
end

function CLuaQiXiMgr.Process2021()
	if CScene.MainScene == nil then return end 
	local data = QiXi_FireWork2021.GetData()
	local pid = CScene.MainScene.GamePlayDesignId
	if pid == data.HZGameplayId or pid == data.JSJGameplayId then
		CUIManager.ShowUI(CLuaUIResources.QiXi2021RightBottom)
		CSkillButtonBoardWnd.Hide(true)
	end
end

CLuaQiXiMgr.YanHuaData = {}
function CLuaQiXiMgr.SyncYanHuaPlayInfo(isOwner,visitorNum,effectType,timeStamp,maxVisitorNum,status,chapterIndex,chapterCount)
	CLuaQiXiMgr.YanHuaData = {}
	CLuaQiXiMgr.YanHuaData.isOwner = isOwner
	CLuaQiXiMgr.YanHuaData.visitorNum = visitorNum
	CLuaQiXiMgr.YanHuaData.effectType = effectType
	CLuaQiXiMgr.YanHuaData.timeStamp = timeStamp
	CLuaQiXiMgr.YanHuaData.maxVisitorNum = maxVisitorNum
	CLuaQiXiMgr.YanHuaData.canManualStart = status == 3
	CLuaQiXiMgr.YanHuaData.status = status
	CLuaQiXiMgr.YanHuaData.chapterIndex = chapterIndex
	CLuaQiXiMgr.YanHuaData.chapterCount = chapterCount
	g_ScriptEvent:BroadcastInLua("SyncYanHuaPlayInfo")
end

--[[
    @desc: 2020七夕副本场景变暗,点光源变强,0表示暗，1表示亮
    author:{author}
    time:2020-07-22 17:02:51
    @return:
]]
function CLuaQiXiMgr.ProcessSceneBrightness(value)
	if CScene.MainScene.GamePlayDesignId == 51101665 then
		CLightMgr.Inst:SetEnvBrightness(value)
    end
end

CLuaQiXiMgr.Start2020 = false

function CLuaQiXiMgr.CheckScene(data)
	if CScene.MainScene == nil then return false end 
	local mapid = CScene.MainScene.SceneTemplateId
	if mapid == data.SceneId then return true end
	if data.ExtSceneIds then
		local len = data.ExtSceneIds.Length
		for i=0,len-1 do
			if mapid == data.ExtSceneIds[i] then 
				return true
			end
		end
	end
	return false
end

CLuaQiXiMgr.CacheId = nil
CLuaQiXiMgr.CacheTime = nil

--[[
    @desc: RPC调用，开启烟花大会的章节
    author:{author}
    time:2020-07-21 10:42:59
    --@chapterId: 章节
	--@time: 开始时间
    @return:
]]
function CLuaQiXiMgr.ProcessFireworkChapter(chapterId,time)
	if time > 0 then
		if CLuaQiXiMgr.CacheTime ~= time or CLuaQiXiMgr.CacheId ~= chapterId then
			CLuaQiXiMgr.CacheId = chapterId
			CLuaQiXiMgr.CacheTime = time
			return
		else
			CLuaQiXiMgr.CacheId = nil
			CLuaQiXiMgr.CacheTime = nil
		end
	end
	if CLuaQiXiMgr.Start2020 == false then return end
	local data = QiXi_FireWorkChapter.GetData(chapterId)
	if data == nil then return end

	if not CLuaQiXiMgr.CheckScene(data) then return end	

	local path = "Assets/Res/MusicFirework/"..chapterId..".asset"
	MusicFireworkMgr.Inst:Play(path,data.Music,time,data.Height)
end

--[[
    @desc: 金沙镜或者杭州,控制镜头上仰幅度
    author:{author}
    time:2020-07-21 10:52:34
    @return:
]]
function CLuaQiXiMgr.ProcessCamera()
	local data = QiXi_FireWork2021.GetData()
	local scene = {16000224,16000006,data.HZPublicmapId,data.JSJPublicmapId}
	if CScene.MainScene == nil then return end
	local isvalid = false
	for i=1,#scene do
		if CScene.MainScene.SceneTemplateId == scene[i] then 
			isvalid =true
			break
		end
	end
	if isvalid then 
		CameraFollow.Inst.MaxSpHeight = 5--控制镜头上仰幅度
	else
		CameraFollow.Inst.MaxSpHeight = 0
	end
end

--[[
    @desc:显示怪物的血条 
    author:{author}
    time:2020-07-31 17:21:52
    --@engineId:
	--@hp:
	--@hpFull: 
    @return:
]]
function CLuaQiXiMgr.ShowQiXiTianDengHpBar(engineId, hp, hpFull)
	local obj = CClientObjectMgr.Inst:GetObject(engineId)
	if obj == nil then return end
	obj.EnableHpBar = true
	obj.Hp = hp
	obj.HpFull = hpFull
	CHeadInfoWnd.Instance:OnClientObjExcluded(engineId,false)
end

--[[
    @desc: 七夕2021烟花副本显示邀请界面
    author:Codee
    time:2021-06-01 10:50:04
]]
function CLuaQiXiMgr.ShowInvitWnd()
	local title = LocalString.GetString("我的好友")
	local btndata = {
		Pbtn = {
			Name = LocalString.GetString("邀请"),
			Func = function(playerid)
				Gac2Gas.RequestInvitePlayerToYanHuaPlay(playerid)
			end
		},
		Fbtn = {
			Name = LocalString.GetString("一键邀请"),
			Func = function()--一键邀请
				Gac2Gas.RequestInviteOnlineFriendsToYanHuaPlay()
			end
		},
		Zbtn = {
			Name = LocalString.GetString("一键喊话"),
			Func = function()--一键邀请
				Gac2Gas.RequestInviteSectMembersToYanHuaPlay()
			end
		},
		Bbtn = {
			Name = LocalString.GetString("一键喊话"),
			Func = function()--一键邀请
				Gac2Gas.RequestInviteGuildMembersToYanHuaPlay()
			end
		},
	}
	LuaCommonPlayerListMgr.ShowWnd2(title,btndata)
end
