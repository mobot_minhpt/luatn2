-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCommonItem = import "L10.Game.CCommonItem"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPackageItemCell = import "L10.UI.CPackageItemCell"
local CPlayerShop_Sell = import "L10.UI.CPlayerShop_Sell"
local CPlayerShopData = import "L10.UI.CPlayerShopData"
local CPlayerShopItem = import "L10.UI.CPlayerShopItem"
local CPlayerShopItemData = import "L10.UI.CPlayerShopItemData"
local CPlayerShopLockItem = import "L10.UI.CPlayerShopLockItem"
local CPlayerShopMgr = import "L10.UI.CPlayerShopMgr"
local DelegateFactory = import "DelegateFactory"
local Double = import "System.Double"
local EnumEventType = import "EnumEventType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EventManager = import "EventManager"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local QnButton = import "L10.UI.QnButton"
local String = import "System.String"
local UInt32 = import "System.UInt32"
CPlayerShop_Sell.m_OnDisable_CS2LuaHook = function (this)
    EventManager.RemoveListenerInternal(EnumEventType.ChangePlayerShopNameSuccess, MakeDelegateFromCSFunction(this.OnChangePlayerShopNameSuccess, MakeGenericClass(Action2, Double, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.RequestAddPlayerShopCounterPosFailure, MakeDelegateFromCSFunction(this.OnRequestAddPlayerShopCounterPosFailure, MakeGenericClass(Action3, Double, UInt32, UInt32), this))
    this.m_WatchBillButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_WatchBillButton.OnClick, MakeDelegateFromCSFunction(this.OnWatchBill, MakeGenericClass(Action1, QnButton), this), false)
    this.m_WatchInfoButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_WatchInfoButton.OnClick, MakeDelegateFromCSFunction(this.OnWatchInfo, MakeGenericClass(Action1, QnButton), this), false)
    this.m_InSellGrid.OnSelectAtRow = CommonDefs.CombineListner_Action_int(this.m_InSellGrid.OnSelectAtRow, MakeDelegateFromCSFunction(this.OnSelectShopRow, MakeGenericClass(Action1, Int32), this), false)
    this.m_PackageGrid.OnSelectAtRow = CommonDefs.CombineListner_Action_int(this.m_PackageGrid.OnSelectAtRow, MakeDelegateFromCSFunction(this.OnSelectPackageItem, MakeGenericClass(Action1, Int32), this), false)
    this.m_DeleteShelfPlaceButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_DeleteShelfPlaceButton.OnClick, MakeDelegateFromCSFunction(this.OnDeleteShelfPlaceButton, MakeGenericClass(Action1, QnButton), this), false)
end
CPlayerShop_Sell.m_Init_CS2LuaHook = function (this, data, itemdata)
    this.FirstEmptyPlace = 1
    while CommonDefs.DictContains(itemdata, typeof(UInt32), this.FirstEmptyPlace) do
        this.FirstEmptyPlace = this.FirstEmptyPlace + 1
    end
    this.m_ShopItemList = InitializeListWithCollection(CreateFromClass(MakeGenericClass(List, CPlayerShopItemData)), CPlayerShopItemData, itemdata.Values)
    this.m_ShopNameLabel.text = CPlayerShopData.Main.PlayerShopName
    this.m_SellCountLabel.text = System.String.Format("{0}/{1}", itemdata.Count, CPlayerShopData.Main.MaxShelfCount)

    --删格子按钮暂时屏蔽
    this.m_DeleteShelfPlaceButton.Visible = false

    this:InitPackageItems()
    this.m_InSellGrid:ReloadData(true, false)
    this.m_PackageGrid:ReloadData(true, false)
    this.m_NoPackageLabel:SetActive(this.m_CanSellList.Count == 0)
    this.m_NoShopItemLabel:SetActive(false)
    if data.Reason == CPlayerShopData.RequestReason.OpenNewCounter then
        this.m_InSellGrid:ScrollToEnd()
    end

    local ZSWButton = this.transform:Find('ZSWButton').gameObject
    local onQiugouClick = function(go)
      Gac2Gas.OpenRequestBuyWindow(0)
    end
    CommonDefs.AddOnClickListener(ZSWButton,DelegateFactory.Action_GameObject(onQiugouClick),false)
end
CPlayerShop_Sell.m_InitPackageItems_CS2LuaHook = function (this)
    CommonDefs.ListClear(this.m_CanSellList)
    CommonDefs.ListClear(this.m_PackagePlaces)
    local bagCount = CClientMainPlayer.Inst.ItemProp:GetPlaceSize(EnumItemPlace.Bag)
    do
        local i = 1
        while i <= bagCount do
            local itemId = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
            if itemId ~= nil then
                local item = CItemMgr.Inst:GetById(itemId)
                if CPlayerShopMgr.Inst:isAllowToTrade(item) then
                    CommonDefs.ListAdd(this.m_CanSellList, typeof(CCommonItem), item)
                    CommonDefs.ListAdd(this.m_PackagePlaces, typeof(UInt32), i)
                end
            end
            i = i + 1
        end
    end
end
CPlayerShop_Sell.m_NumberOfRows_CS2LuaHook = function (this, view)
    if view == this.m_InSellGrid then
        local tmp = CPlayerShopData.Main.MaxShelfCount
        return math.floor(math.min(tmp + 1, CPlayerShopMgr.Counter_Num_Max))
    else
        return this.m_CanSellList.Count
    end
end
CPlayerShop_Sell.m_ItemAt_CS2LuaHook = function (this, view, row)
    if view == this.m_InSellGrid then
        if row < this.m_ShopItemList.Count then
            local data = this.m_ShopItemList[row]
            local itemcell = TypeAs(view:GetFromPool(0), typeof(CPlayerShopItem))
            itemcell:UpdateData(data, true, false)
            return itemcell
        elseif row < CPlayerShopData.Main.MaxShelfCount then
            local emptyItem = view:GetFromPool(2)
            return emptyItem
        else
            local itemlock = TypeAs(view:GetFromPool(1), typeof(CPlayerShopLockItem))
            itemlock:UpdateData(CPlayerShopData.Main)
            return itemlock
        end
    else
        local cell = TypeAs(view:GetFromPool(0), typeof(CPackageItemCell))
        local item = this.m_CanSellList[row]
        cell:Init(item.Id, false, false, false, nil)
        cell.Selected = false
        return cell
    end
end
CPlayerShop_Sell.m_OnSelectShopRow_CS2LuaHook = function (this, row)
    if row < this.m_ShopItemList.Count then
        local data = this.m_ShopItemList[row]
        CPlayerShopMgr.ShowUnShelfWnd(data)
    elseif row >= CPlayerShopData.Main.MaxShelfCount then
        this:TryAddShopCounter()
    end
end
CPlayerShop_Sell.m_OnSelectPackageItem_CS2LuaHook = function (this, row)
    if not CPlayerShopData.Main.HasEmptyPlace then
        g_MessageMgr:ShowMessage("BAGGAGE_IS_FULL")
        return
    end
    local item = this.m_CanSellList[row]
    local protectTime = CPlayerShopMgr.Inst:CheckProtectTime(item)
    if protectTime > 0 then
        g_MessageMgr:ShowMessage("PLAYER_SHOP_ON_PROTECT_TIME", math.ceil(1 * protectTime / 60))
        return
    end
    if item.IsItem and item.Item:IsExpire() then
        g_MessageMgr:ShowMessage("ITEM_NOT_ALLOW_TO_TRADE")
        return
    end

    local data = CreateFromClass(CPlayerShopItemData)
    data.ShopId = CPlayerShopData.Main.PlayerShopId
    data.Item = item
    data.OwnerId = CPlayerShopData.Main.OwnerId
    data.OwnerName = CPlayerShopData.Main.OwnerName
    data.Count = item.IsItem and item.Item.Count or 1
    data.ShelfPlace = this.FirstEmptyPlace
    data.Price = 100
    data.FocusCount = 0
    data.IsFocus = false
    data.PackagePlace = this.m_PackagePlaces[row]
    do
        local i = 0
        while i < this.m_CanSellList.Count do
            local cell = TypeAs(this.m_PackageGrid:GetItemAtRow(i), typeof(CPackageItemCell))
            if cell ~= nil then
                cell.Selected = (i == row)
            end
            i = i + 1
        end
    end
    CPlayerShopMgr.ShowShelfWnd(data)
end
CPlayerShop_Sell.m_TryAddShopCounter_CS2LuaHook = function (this)
    if CClientMainPlayer.Inst.Silver < CPlayerShopData.Main.OpenNextShelfPrice then
        g_MessageMgr:ShowMessage("PLAYERSHOP_NOT_ENOUGH_YINLIANG_UNLOCK_BUDGET", CPlayerShopData.Main.OpenNextShelfPrice)
    else
        local format = g_MessageMgr:FormatMessage("PLAYERSHOP_BUY_COUNTER_CONFIRM", CPlayerShopData.Main.OpenNextShelfPrice)
        MessageWndManager.ShowOKCancelMessage(format, DelegateFactory.Action(function ()
            Gac2Gas.RequestAddPlayerShopCounterPos(CPlayerShopData.Main.PlayerShopId, (CPlayerShopData.Main.MaxShelfCount + 1))
        end), nil, nil, nil, false)
    end
end
CPlayerShop_Sell.m_OnRequestAddPlayerShopCounterPosFailure_CS2LuaHook = function (this, shopId, place, silver)
    if shopId == CPlayerShopData.Main.PlayerShopId then
        MessageWndManager.ShowOKMessage(LocalString.GetString("开启新的格子失败"), nil)
    end
end
