-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CommonDefs = import "L10.Game.CommonDefs"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CTickMgr = import "L10.Engine.CTickMgr"
local CValentineMgr = import "L10.UI.CValentineMgr"
local CValentineQAWnd = import "L10.UI.CValentineQAWnd"
local DelegateFactory = import "DelegateFactory"
local EnumGender = import "L10.Game.EnumGender"
local ETickType = import "L10.Engine.ETickType"
local Extensions = import "Extensions"
local Gac2Gas = import "L10.Game.Gac2Gas"
local GameObject = import "UnityEngine.GameObject"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local Valentine_Questions = import "L10.Game.Valentine_Questions"
local VoidDelegate = import "UIEventListener+VoidDelegate"

CValentineQAWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.m_CloseBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
        this:Close()
        Gac2Gas.SelectValentineAnswer(this.m_QuestionId, 0)
    end)
end
CValentineQAWnd.m_Init_CS2LuaHook = function (this) 
    this.m_AnswerTemplate:SetActive(false)
    if CValentineMgr.Inst.m_ValentineQAInfo.m_QuestionId ~= this.m_QuestionId then
        this:InitTime()
        Extensions.RemoveAllChildren(this.m_AnswerTable.transform)
        this.m_QuestionId = CValentineMgr.Inst.m_ValentineQAInfo.m_QuestionId
        this.m_AnswerList = CreateFromClass(MakeGenericClass(List, String))
        local data = Valentine_Questions.GetData(this.m_QuestionId)
        if data == nil then
            return
        end
        CommonDefs.ListAdd(this.m_AnswerList, typeof(String), data.Answer_1)
        CommonDefs.ListAdd(this.m_AnswerList, typeof(String), data.Answer_2)
        CommonDefs.ListAdd(this.m_AnswerList, typeof(String), data.Answer_3)
        CommonDefs.ListAdd(this.m_AnswerList, typeof(String), data.Answer_4)
        if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Gender == EnumGender.Female then
            this.m_QuestionLabel.text = LocalString.GetString("你") .. data.Question
        else
            this.m_QuestionLabel.text = CValentineMgr.Inst.m_ValentineQAInfo.m_GirlName .. data.Question
        end
        this:GetRandomList()
        this.m_AnswerObjList = CreateFromClass(MakeGenericClass(List, GameObject))
        for i = 0, 3 do
            local obj = NGUITools.AddChild(this.m_AnswerTable.gameObject, this.m_AnswerTemplate)
            obj:SetActive(true)
            CommonDefs.ListAdd(this.m_AnswerObjList, typeof(GameObject), obj)
            CommonDefs.GetComponentInChildren_GameObject_Type(obj, typeof(UILabel)).text = this.m_AnswerList[this.m_AnswerIndexList[i]]
            UIEventListener.Get(obj).onClick = MakeDelegateFromCSFunction(this.OnAnswerClick, VoidDelegate, this)
        end
        this.m_AnswerTable:Reposition()
        this.m_CanAnswer = true
    end
end
CValentineQAWnd.m_InitTime_CS2LuaHook = function (this) 
    if this.m_CountDownTick ~= nil then
        invoke(this.m_CountDownTick)
    end
    this.m_TotalTime = math.floor((CValentineMgr.Inst.m_ValentineQAInfo.m_ExpireTime - CServerTimeMgr.Inst.timeStamp))
    this.m_LeftTime = this.m_TotalTime
    this.m_CountDownLabel.text = (LocalString.GetString("还剩") .. this.m_LeftTime) .. LocalString.GetString("秒")
    this.m_CountDownSlider.value = 1
    this.m_CountDownTick = CTickMgr.Register(DelegateFactory.Action(function () 
        this.m_LeftTime = this.m_LeftTime - 1
        this.m_CountDownLabel.text = (LocalString.GetString("还剩") .. this.m_LeftTime) .. LocalString.GetString("秒")
        this.m_CountDownSlider.value = this.m_LeftTime / this.m_TotalTime
        if this.m_LeftTime <= 0 then
            this:Close()
        end
    end), 1000, ETickType.Loop)
end
CValentineQAWnd.m_OnAnswerClick_CS2LuaHook = function (this, go) 
    if CClientMainPlayer.Inst == nil then
        return
    end
    if CClientMainPlayer.Inst.Gender == EnumGender.Male and CValentineMgr.Inst.m_ValentineQAInfo.m_State == EnumValentinePlayState_lua.eWaitForGirlAnswer then
        g_MessageMgr:ShowMessage("CANNOT_ANSWER_BEFORE_GIRL")
        return
    end
    if not this.m_CanAnswer then
        return
    end
    for i = 0, 3 do
        if go == this.m_AnswerObjList[i] then
            go.transform:Find("clickbg").gameObject:SetActive(true)
            Gac2Gas.SelectValentineAnswer(this.m_QuestionId, this.m_AnswerIndexList[i] + 1)
            break
        end
    end
    this.m_CanAnswer = false
end
CValentineQAWnd.m_GetRandomList_CS2LuaHook = function (this) 
    this.m_AnswerIndexList = InitializeList(CreateFromClass(MakeGenericClass(List, Int32)), Int32, 0, 1, 2, 3)
    do
        local i = 0 local key local value
        while i < 3 do
            key = UnityEngine_Random(i + 1, 4)
            value = this.m_AnswerIndexList[i]
            this.m_AnswerIndexList[i] = this.m_AnswerIndexList[key]
            this.m_AnswerIndexList[key] = value
            i = i + 1
        end
    end
end
