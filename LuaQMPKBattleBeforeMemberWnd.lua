local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UITable = import "UITable"
local GameObject = import "UnityEngine.GameObject"
local CButton = import "L10.UI.CButton"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local Animation = import "UnityEngine.Animation"
local UITableTween = import "L10.UI.UITableTween"
LuaQMPKBattleBeforeMemberWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaQMPKBattleBeforeMemberWnd, "Table", "Table", UITable)
RegistChildComponent(LuaQMPKBattleBeforeMemberWnd, "PlayerTemplate", "PlayerTemplate", GameObject)
RegistChildComponent(LuaQMPKBattleBeforeMemberWnd, "DetailBtn", "DetailBtn", CButton)
RegistChildComponent(LuaQMPKBattleBeforeMemberWnd, "GroupLabel", "GroupLabel", UILabel)
RegistChildComponent(LuaQMPKBattleBeforeMemberWnd, "TeamLabel", "TeamLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaQMPKBattleBeforeMemberWnd, "m_IsLeft")
RegistClassMember(LuaQMPKBattleBeforeMemberWnd, "m_CurRound")
RegistClassMember(LuaQMPKBattleBeforeMemberWnd, "m_Force")
RegistClassMember(LuaQMPKBattleBeforeMemberWnd, "m_IsLeader")
RegistClassMember(LuaQMPKBattleBeforeMemberWnd, "m_ColorList")
RegistClassMember(LuaQMPKBattleBeforeMemberWnd, "m_RoundToMaxMember")
RegistClassMember(LuaQMPKBattleBeforeMemberWnd, "m_AllMemberViewList")
RegistClassMember(LuaQMPKBattleBeforeMemberWnd, "m_MemberViewList")
RegistClassMember(LuaQMPKBattleBeforeMemberWnd, "m_HasShowPlayerFx")
function LuaQMPKBattleBeforeMemberWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.DetailBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDetailBtnClick()
	end)


    --@endregion EventBind end
	self.m_IsLeft = false
	self.m_Force = nil
	self.m_CurRound = 0
	self.m_MemberViewList = nil
	self.m_AllMemberViewList = nil
	self.m_IsLeader = false
	self.m_HasShowPlayerFx = {}
	self.PlayerTemplate.gameObject:SetActive(false)
	self:InitBaseData()
end

function LuaQMPKBattleBeforeMemberWnd:InitAllMemberViewList()
	if not self.m_AllMemberViewList then
		self.m_AllMemberViewList = {}
		Extensions.RemoveAllChildren(self.Table.transform)
		for i = 1,5 do
			local member = CUICommonDef.AddChild(self.Table.gameObject,self.PlayerTemplate.gameObject)
			member.gameObject:SetActive(true)
			table.insert(self.m_AllMemberViewList,member)
		end
		--self.Table:Reposition()
		self.Table.enabled = false
	end
end

function LuaQMPKBattleBeforeMemberWnd:InitBaseData()
	self.m_ColorList = {}
	self.m_ColorList[EnumCommonForce_lua.eAttack] = {
		bg = "4674A0",		-- 整体背景颜色
		text = "7CB1E3",	-- 队名颜色
		group = "C7E4FF",	-- 组名颜色
		groupIcon = "ADC6DE",	-- 组别图标颜色
		groupbgPath = "qmpkbattlebeforewnd_lan_zhenyingchendi",	-- 组别背景资源
		playerHeadbgPath = "qmpkbattlebeforewnd_lan_touxiangkuang",	-- 头像背景资源
		playerHeadHighlightPath = "qmpkbattlebeforewnd_niehuan_xuanzhezhong", -- 头像背景高亮时资源
		playerNameBgColor = {"64A1E3",0.33}, -- 玩家名部分背景颜色
		detailBtnPath = "qmpkbattlebeforewnd_lan_xiangqing",	--战队详情按钮资源
		fxHighlightBg = {"4965FF","7795FF",0.78},				--头部特效资源颜色及透明度
		highlightFxBg = {"1C3173",0.66}
	}
	self.m_ColorList[EnumCommonForce_lua.eDefend] = {
		bg = "B74C4F",		-- 整体背景颜色
		text = "F38383",	-- 队名颜色
		group = "FEBCB5",	-- 组名颜色
		groupIcon = "F3C6C6",	-- 组别图标颜色
		groupbgPath = "qmpkbattlebeforewnd_hong_zhenyingchendi",	-- 组别背景资源
		playerHeadbgPath = "qmpkbattlebeforewnd_hong_touxiangkuang",	-- 头像背景资源
		playerHeadHighlightPath = "qmpkbattlebeforewnd_niehuan_xuanzhezhong_hong", -- 头像背景高亮时资源
		playerNameBgColor = {"ED5A63",0.33}, -- 玩家名部分背景颜色
		detailBtnPath = "qmpkbattlebeforewnd_hong_xiangqing",	--战队详情按钮资源
		fxHighlightBg = {"FF9B49","FF8C8F",0.78},				--头部特效资源颜色及透明度
		highlightFxBg = {"731C28",0.66}
	}
	self.m_RoundToMaxMember = {}
	self.m_RoundToMaxMember[1] = 5	--团体战5人上场
	self.m_RoundToMaxMember[2] = 1	--单人战1人上场
	self.m_RoundToMaxMember[3] = 3	--三人战3人上场
	self.m_RoundToMaxMember[4] = 3	--双人战2人上场，包括一名被ban的成员
	self.m_RoundToMaxMember[5] = 5	--团体战5人上场
end

function LuaQMPKBattleBeforeMemberWnd:SetPlayerTemplateForceColor(gameObject,force)
	gameObject.transform:Find("Name"):GetComponent(typeof(UILabel)).color = NGUIText.ParseColor24("ffffff", 0)
	gameObject.transform:Find("Head/Bg"):GetComponent(typeof(CUITexture)):LoadMaterial(CLuaQMPKMgr.GetQmpkTexturePath(self.m_ColorList[force].playerHeadbgPath))
	local highLight = gameObject.transform:Find("Head/HighlightBg"):GetComponent(typeof(CUITexture))
	highLight:LoadMaterial(CLuaQMPKMgr.GetQmpkTexturePath(self.m_ColorList[force].playerHeadHighlightPath))
	--local highLight1 = highLight.transform:Find("HighlightBg (1)"):GetComponent(typeof(UITexture))
	local highLight2 = highLight.transform:Find("HighlightBg (2)"):GetComponent(typeof(UITexture))
	-- highLight1.color = NGUIText.ParseColor24(self.m_ColorList[force].fxHighlightBg[1], 0)
	-- highLight1.alpha = self.m_ColorList[force].fxHighlightBg[3]
	highLight2.color = NGUIText.ParseColor24(self.m_ColorList[force].fxHighlightBg[2], 0)
	local nameBg = highLight.transform:Find("Bg"):GetComponent(typeof(UITexture))
	nameBg.color = NGUIText.ParseColor24(self.m_ColorList[force].playerNameBgColor[1], 0)
	nameBg.alpha = self.m_ColorList[force].playerNameBgColor[2]
end

function LuaQMPKBattleBeforeMemberWnd:InitPlayerForce(force,teamName,myforce,isLeft,isLeader)
	self.m_IsLeft = isLeft
	self.m_IsLeader = isLeader
	if self.m_Force ~= force then
		self.m_Force = force
		local bg = self.transform:Find("BG"):GetComponent(typeof(UITexture))
		bg.color = NGUIText.ParseColor24(self.m_ColorList[self.m_Force].bg, 0)
		self.TeamLabel.color = NGUIText.ParseColor24(self.m_ColorList[self.m_Force].text, 0)
		self.GroupLabel.color = NGUIText.ParseColor24(self.m_ColorList[self.m_Force].group, 0)
		self.DetailBtn.transform:GetComponent(typeof(CUITexture)):LoadMaterial(CLuaQMPKMgr.GetQmpkTexturePath(self.m_ColorList[self.m_Force].detailBtnPath))
		local groupBg = self.GroupLabel.transform:Find("Bg"):GetComponent(typeof(CUITexture))
		groupBg:LoadMaterial(CLuaQMPKMgr.GetQmpkTexturePath(self.m_ColorList[self.m_Force].groupbgPath))
		if (self.m_Force == EnumCommonForce_lua.eDefend and self.m_IsLeft) or (not self.m_IsLeft and self.m_Force == EnumCommonForce_lua.eAttack) then
			groupBg.texture.flip = UIBasicSprite.Flip.Horizontally
		else
			groupBg.texture.flip = UIBasicSprite.Flip.Nothing
		end
		groupBg.transform:Find("Texture"):GetComponent(typeof(UITexture)).color = NGUIText.ParseColor24(self.m_ColorList[self.m_Force].groupIcon, 0)
		local fxHighlightBg = bg.transform:Find("HighlightBg (1)"):GetComponent(typeof(UITexture))
		fxHighlightBg.color = NGUIText.ParseColor24(self.m_ColorList[self.m_Force].highlightFxBg[1], 0)
		fxHighlightBg.alpha = self.m_ColorList[self.m_Force].highlightFxBg[2]
		self:InitAllMemberViewList()
	end
	local btnNameLabel = self.DetailBtn.transform:Find("Label"):GetComponent(typeof(UILabel))
	self.TeamLabel.text = CUICommonDef.cutDown(teamName)
	if force == myforce then
		self.GroupLabel.text = LocalString.GetString("我方")
		btnNameLabel.text = LocalString.GetString("我方战队")
	elseif myforce ~= EnumCommonForce_lua.eDefend and myforce ~= EnumCommonForce_lua.eAttack then
		self.GroupLabel.text = force == EnumCommonForce_lua.eAttack and LocalString.GetString("蓝方") or LocalString.GetString("红方") 
		btnNameLabel.text = force == EnumCommonForce_lua.eAttack and LocalString.GetString("蓝方战队") or LocalString.GetString("红方战队") 
	else
		self.GroupLabel.text = LocalString.GetString("敌方")
		btnNameLabel.text = LocalString.GetString("敌方战队")
	end
	
end
--[[
	bpStatus : bp阶段或finish阶段
	myForce : 玩家所在阵营
	round ： 当前回合
	memberList : 选择玩家列表
	banMember : ban玩家对象
]]
function LuaQMPKBattleBeforeMemberWnd:UpdateMember(bpStatus,myForce,round,memberList,banMember)
	self.DetailBtn.gameObject:SetActive(bpStatus ~= EnumQmpkBanPickStatus.eAllFinish and (myForce == EnumCommonForce_lua.eDefend or myForce == EnumCommonForce_lua.eAttack))
	if round ~= self.m_CurRound or not self.m_MemberViewList then
		self.m_MemberViewList = {}
		self.m_HasShowPlayerFx = {}
		self.m_CurRound = round
		local startIndex = 2 - math.floor(self.m_RoundToMaxMember[round] / 2)
		for i = 1,#self.m_AllMemberViewList do
			local member = self.m_AllMemberViewList[i]
			if i > startIndex and i <= startIndex + self.m_RoundToMaxMember[round] then
				member.gameObject:SetActive(true)
				table.insert(self.m_MemberViewList,member)
				table.insert(self.m_HasShowPlayerFx,false)
			else
				member.gameObject:SetActive(false)
			end
			self:SetPlayerTemplateForceColor(member,self.m_Force)
		end
		self:SetMemberviewListOffset()
	end
	if round == 4 then	-- 第四轮特殊处理
		banObj = self.m_MemberViewList[2]
		local anotherForce = self.m_Force
		if self.m_Force == EnumCommonForce_lua.eAttack then anotherForce = EnumCommonForce_lua.eDefend end
		if self.m_Force == EnumCommonForce_lua.eDefend then anotherForce = EnumCommonForce_lua.eAttack end
		self:SetPlayerTemplateForceColor(banObj,anotherForce)
		self:ShowMemberItem(self.m_MemberViewList[1],memberList[1],myForce,bpStatus,round,1)
		self:ShowMemberItem(self.m_MemberViewList[3],memberList[2],myForce,bpStatus,round,3)
		self:ShowBanMemberItem(banObj,banMember,myForce,bpStatus,2)
	else
		for i = 1,#self.m_MemberViewList do
			local item = self.m_MemberViewList[i]
			local data = memberList[i]
			self:SetPlayerTemplateForceColor(item,self.m_Force)
			self:ShowMemberItem(item,data,myForce,bpStatus,round,i)
		end
	end
end

function LuaQMPKBattleBeforeMemberWnd:ShowMemberItem(item,data,myForce,bpStatus,round,index)
	if not item then return end
	local portrait = item.transform:Find("Head/Portrait"):GetComponent(typeof(CUITexture))
	local highlightBg = item.transform:Find("Head/HighlightBg").gameObject
	local waitFx = item.transform:Find("Head/WaitFx").gameObject
	local lockIcon = item.transform:Find("Head/LockIcon").gameObject
	local banIcon = item.transform:Find("Head/BanIcon").gameObject
	local name = item.transform:Find("Name"):GetComponent(typeof(UILabel))
	--local nameBg = item.transform:Find("Name/Bg").gameObject
	local ani = item.transform:GetComponent(typeof(Animation))

	local isBPNotAllFinish = bpStatus ~= EnumQmpkBanPickStatus.eAllFinish
	local isSelectFinish = bpStatus ~= EnumQmpkBanPickStatus.ePickBegin
	local isOtherMember = myForce ~= EnumCommonForce_lua.eAttack and  myForce ~= EnumCommonForce_lua.eDefend -- 观众
	if data and ((bpStatus == EnumQmpkBanPickStatus.eAllFinish) or (data.Status == 1 or round == 5) or (myForce == self.m_Force)) then
		local id = data.Id
		local Name = data.Name
		local class = data.Class
		local gender = data.Gender
		portrait.gameObject:SetActive(true)
		waitFx.gameObject:SetActive(false)
		local namecolor = self.m_Force == EnumCommonForce_lua.eAttack and "aae5ff" or "ffb6aa"
		if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == id and not isBPNotAllFinish then
			namecolor = "00FF00"
		end
		name.text = SafeStringFormat3("[%s]%s[-]",namecolor,Name)
		--name.alpha = 1
		portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(class, gender, -1), false)
		banIcon.gameObject:SetActive(false)
		-- 第4轮标记强制出战，第五轮全部强制出战，非准备阶段显示
		lockIcon.gameObject:SetActive(isBPNotAllFinish and (data.Status == 1 or round == 5))	
		local needShowHighlight = not isSelectFinish and myForce == self.m_Force and data.Status ~= 1 and round ~= 5
		highlightBg.gameObject:SetActive(needShowHighlight)
		--nameBg.gameObject:SetActive(needShowHighlight)
		if self.m_HasShowPlayerFx and not self.m_HasShowPlayerFx[index] then
			if not needShowHighlight then
				ani:Stop()
				ani:Play("qmpkbattlebeforewnd_xuanren")
				self.m_HasShowPlayerFx[index]  = true
			end
		end
	else
		portrait.gameObject:SetActive(false)
		waitFx.gameObject:SetActive(true)
		highlightBg.gameObject:SetActive(myForce == self.m_Force)
		lockIcon.gameObject:SetActive(false)
		banIcon.gameObject:SetActive(false)
		--nameBg.gameObject:SetActive(myForce == self.m_Force)
		if isOtherMember then	--观众视角
			name.text = self.m_Force == EnumCommonForce_lua.eAttack and LocalString.GetString("[898fa0]蓝方选择中[-]") or LocalString.GetString("[b59b9d]红方选择中[-]") 
		else
			local showColor = "ffffff"
			if self.m_Force == EnumCommonForce_lua.eAttack then 
				showColor = myForce == self.m_Force and "d6cdcd" or "898fa0"
			else
				showColor = myForce == self.m_Force and "d6cdcd" or "b59b9d"
			end
			local showLabel = myForce == self.m_Force and LocalString.GetString("队长选择中") or LocalString.GetString("敌方选择中")
			showLabel = self.m_IsLeader and myForce == self.m_Force and LocalString.GetString("请选择出战角色") or showLabel
			name.text = SafeStringFormat3("[%s]%s[-]",showColor,showLabel)
		end 
		--name.alpha = 0.7
	end
end

function LuaQMPKBattleBeforeMemberWnd:ShowBanMemberItem(item,data,myForce,bpStatus,index)
	if not item then return end
	local portrait = item.transform:Find("Head/Portrait"):GetComponent(typeof(CUITexture))
	local highlightBg = item.transform:Find("Head/HighlightBg").gameObject
	local waitFx = item.transform:Find("Head/WaitFx").gameObject
	local lockIcon = item.transform:Find("Head/LockIcon").gameObject
	local banIcon = item.transform:Find("Head/BanIcon").gameObject
	local name = item.transform:Find("Name"):GetComponent(typeof(UILabel))
	--local nameBg = item.transform:Find("Name/Bg").gameObject
	local ani = item.transform:GetComponent(typeof(Animation))

	local isBPNotAllFinish = bpStatus ~= EnumQmpkBanPickStatus.eAllFinish
	local isSelectFinish = bpStatus ~= EnumQmpkBanPickStatus.eBanBegin
	local isOtherMember = myForce ~= EnumCommonForce_lua.eAttack and  myForce ~= EnumCommonForce_lua.eDefend -- 观众
	local isMyForce = myForce == self.m_Force		-- 我方阵营
	
	if not isBPNotAllFinish then
		item.gameObject:SetActive(false)
		return
	end
	highlightBg.gameObject:SetActive(not isSelectFinish and isMyForce)
	--nameBg.gameObject:SetActive(not isSelectFinish and isMyForce)
	lockIcon.gameObject:SetActive(false)
	banIcon.gameObject:SetActive(true)

	if isOtherMember then
		name.text = self.m_Force == EnumCommonForce_lua.eAttack and LocalString.GetString("[898fa0]蓝方禁选中[-]") or LocalString.GetString("[b59b9d]红方禁选中[-]") 
	elseif myForce == self.m_Force then	-- 我方阵营
		name.text = self.m_Force == EnumCommonForce_lua.eAttack and LocalString.GetString("[d6cdcd]请选择[dc6a66]敌方禁选[-]角色[-]") or LocalString.GetString("[d6cdcd]请选择[65a9ff]敌方禁选[-]角色[-]") 
	else	-- 敌方阵营
		name.text = self.m_Force == EnumCommonForce_lua.eAttack and LocalString.GetString("[898fa0]敌方禁选中[-]") or LocalString.GetString("[b59b9d]敌方禁选中[-]") 
	end 
	--name.alpha = 0.7
	if data and data.Id ~= 0 and (isMyForce or bpStatus == EnumQmpkBanPickStatus.ePickBegin or bpStatus == EnumQmpkBanPickStatus.ePickFinish) then
		local id = data.Id
		local Name = data.Name
		local class = data.Class
		local gender = data.Gender
		portrait.gameObject:SetActive(true)
		waitFx.gameObject:SetActive(false)
		local showNametext = ""
		if self.m_Force == EnumCommonForce_lua.eAttack then
			showNametext = SafeStringFormat3(LocalString.GetString("[dc6a66]禁止红方选择[-] [ffb6aa]%s[-]"),Name)
		else
			showNametext = SafeStringFormat3(LocalString.GetString("[65A9FF]禁止蓝方选择[-] [AAE5FF]%s[-]"),Name)
		end
		name.text = showNametext
		--name.alpha = 1
		portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(class, gender, -1), false)
		if self.m_HasShowPlayerFx and not self.m_HasShowPlayerFx[index] then
			if isSelectFinish then
				ani:Stop()
				ani:Play("qmpkbattlebeforewnd_xuanren")
				self.m_HasShowPlayerFx[index]  = true
			end
		end
	else
		portrait.gameObject:SetActive(false)
		waitFx.gameObject:SetActive(true)
	end
end

function LuaQMPKBattleBeforeMemberWnd:SetMemberviewListOffset()
	local leftx = {142.5,77.5,52.5,77.5,142.5}
	local rightx = {166,231,256,231,166}
	local yoffset = {232, 121, -4, -129, -240}
	local maxCount = #self.m_MemberViewList
	for i = 1,maxCount do
		local index = 2 - math.floor(maxCount / 2) + i
		local newx = self.m_IsLeft and leftx[index] or rightx[index]
		local newy = yoffset[index]
		LuaUtils.SetLocalPositionX(self.m_MemberViewList[i].transform, newx)
		LuaUtils.SetLocalPositionY(self.m_MemberViewList[i].transform, newy)
	end
	self.Table.transform:GetComponent(typeof(UITableTween)):PlayAnim()
end

--@region UIEvent

function LuaQMPKBattleBeforeMemberWnd:OnDetailBtnClick()
	if self.m_Force then 
		Gac2Gas.QueryQmpkBiWuZhanDuiInfo(self.m_Force)
	end
end

--@endregion UIEvent

