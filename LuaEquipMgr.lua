local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CChuanjiabaoWordType=import "L10.Game.CChuanjiabaoWordType"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CItem = import "L10.Game.CItem"
local CItemMgr = import "L10.Game.CItemMgr"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local EnumQualityType = import "L10.Game.EnumQualityType"
local MessageWndManager = import "L10.UI.MessageWndManager"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local UILabelOverflow = import "UILabel+Overflow"
local IdPartition = import "L10.Game.IdPartition"
local Talisman_Setting = import "L10.Game.Talisman_Setting"
require("common/equip/EquipMaxWord")

CLuaEquipMgr={}
CLuaEquipMgr.WordMaxMark = "#293 "
CLuaEquipMgr.GhostDisassembleEquip=nil
CLuaEquipMgr.DisassembleItemId = 0
CLuaEquipMgr.EnableVersion2 = false --开启神兵三阶新材料

function CLuaEquipMgr.CanDisassembleEquip(place, equipId) 
    if place ~= EnumItemPlace.Bag then
        return false
    end

    local commonItem = CItemMgr.Inst:GetById(equipId)
    if commonItem == nil or not commonItem.IsEquip then
        return false
    end
    if commonItem.Equip.IntensifyLevel > 0 then
        g_MessageMgr:ShowMessage("Equipment_Fenjie_Is_Qianghua")
        return false
    end

    if commonItem.Equip.HasGems then
        g_MessageMgr:ShowMessage("Equipment_Fenjie_Is_BaoShi")
        return false
    end
    return true
end

function CLuaEquipMgr.GetDisassembleRetItems(equipId) 
    local commonItem = CItemMgr.Inst:GetById(equipId)
    local ret={}
    local ccm = {}
    local getItems = nil
    local cost = 0
    EquipmentTemplate_Disassemble.ForeachKey(function (id) 
        local data = EquipmentTemplate_Disassemble.GetData(id)
        if data == nil then
            return
        end
        if data.GradeRange[0] <= commonItem.Equip.Grade and data.GradeRange[1] >= commonItem.Equip.Grade and EnumToInt(commonItem.Equip.Color) == data.Color then
            cost = data.Cost
            local dic=data.GetItem
            if dic then
                CommonDefs.DictIterate(dic, DelegateFactory.Action_object_object(function (___key, ___value) 
                    local itemId=tonumber(___key)
                    local count=tonumber(___value)
                    if count>0 then
                        table.insert( ret,{itemId,count} )
                    end
                end))
            end


            if commonItem.Equip.IsIdentifiable then
                cost = cost + data.IdentifiableCost
            end

            if commonItem.Equip.IsIdentifiable and data.GetItemIdentifiable ~= nil then
                CommonDefs.DictIterate(data.GetItemIdentifiable, DelegateFactory.Action_object_object(function (___key, ___value) 
                    local itemId=tonumber(___key)
                    local count=tonumber(___value)
                    if count>0 then
                        table.insert( ret,{itemId,count} )
                    end
                end))
            end

            local ccmlist = data.UseChangChunMuCost
            local wct = commonItem.Equip.WordsCount
            if wct > 8 then wct = 8 end
            for i=0,ccmlist.Length,3 do
                if ccmlist[i] and wct == ccmlist[i] then
                    ccm={ccmlist[i+1],ccmlist[i+2]}
                    break
                end
            end
        end
    end)
    if commonItem.Equip.Color == EnumQualityType.Purple or commonItem.Equip.Color == EnumQualityType.Ghost then
        local yuanshiCount =AllFormulas.Action_Formula[373].Formula(nil,nil,{commonItem.Equip.Grade, commonItem.Equip.Hole})
        if yuanshiCount > 0 then
            local jingangzuanYuanshiItemId = GameSetting_Common.GetData().JinGangZuanYuanShiItemId
            table.insert( ret,{jingangzuanYuanshiItemId,yuanshiCount} )
        end
    end

    return ret,ccm
end
function CLuaEquipMgr.GetCostYinLiang(equipId) 
    local commonItem = CItemMgr.Inst:GetById(equipId)
    if commonItem == nil or not commonItem.IsEquip then
        return 0
    end

    local cost = 0
    local isIdentifiable = commonItem.Equip.IsIdentifiable
    EquipmentTemplate_Disassemble.ForeachKey(function (id) 
        local data = EquipmentTemplate_Disassemble.GetData(id)
        if data == nil then
            return
        end
        if data.GradeRange[0] <= commonItem.Equip.Grade and data.GradeRange[1] >= commonItem.Equip.Grade and EnumToInt(commonItem.Equip.Color) == data.Color then
            cost = data.Cost
            if isIdentifiable then
                cost = cost + data.IdentifiableCost
            end
        end
    end)
    if commonItem.Equip.BaptizeScore > 0 then
        cost = cost + math.floor((commonItem.Equip.BaptizeScore * GameSetting_Common.GetData().DissambleRate))
    end
    return cost
end

function CLuaEquipMgr.ProcessGhostEquipDisassemble(commonItem, place, pos, equipId, costBujingmu, costCCM) 
    local msg = g_MessageMgr:FormatMessage("Double_EquipWords_ChaiGui_JiFen_Config", commonItem.Equip.BaptizeScore)
    MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
        --这个时候强制关掉拆解界面
        if CUIManager.IsLoaded(CUIResources.GhostEquipWordDisassembleWnd) or CUIManager.IsLoading(CUIResources.GhostEquipWordDisassembleWnd) then
            CUIManager.CloseUI(CUIResources.GhostEquipWordDisassembleWnd)
        end

        CUIManager.CloseUI(CLuaUIResources.GhostEquipDisassembleWnd)
        Gac2Gas.RequestDisassembleEquip(EnumToInt(place), pos, equipId, costBujingmu, costCCM)
    end), nil, nil, nil, false)
end

function CLuaEquipMgr.ProcessAdvEquipDisassemble(commonItem, place, pos, equipId, costBujingmu, costCCM)
    local msg = g_MessageMgr:FormatMessage("Equipment_Fenjie_Expensive_Confirm_Purple", commonItem.Equip.BaptizeScore)
    MessageWndManager.ShowOKCancelMessage(
        msg,
        DelegateFactory.Action(
            function()
                CUIManager.CloseUI(CLuaUIResources.EquipAdvDisassembleWnd)
                Gac2Gas.RequestDisassembleEquip(EnumToInt(place), pos, equipId, costBujingmu, costCCM)
            end
        ),
        nil,
        nil,
        nil,
        false
    )
end


--   如果拆解鬼装的话 洗炼积分为0的装备不弹出提示框
function CLuaEquipMgr.RequestDisassembleEquip(place, equipId, costBujingmu,costCCM) 
    local pos = CClientMainPlayer.Inst.ItemProp:GetItemPos(place, equipId)
    local commonItem = CItemMgr.Inst:GetById(equipId)
    if commonItem == nil or not commonItem.IsEquip then
        return
    end
    --鬼装的拆解这么处理
    if CSwitchMgr.EnableGhostDisassemble then
        if commonItem.Equip.IsGhost then
            CLuaEquipMgr.ProcessGhostEquipDisassemble(commonItem, place, pos, equipId, costBujingmu,costCCM)
            return
        end
    end

    if CLuaEquipMgr.EnableVersion2 then
        if commonItem.Equip.WordsCount >= 6 and commonItem.Equip.Color == EnumQualityType.Purple then
            CLuaEquipMgr.ProcessAdvEquipDisassemble(commonItem, place, pos, equipId, costBujingmu, costCCM)
            return
        end
    end

    local design = EquipmentTemplate_Equip.GetData(commonItem.TemplateId)
    if design == nil then
        return
    end

    local getItems = {}
    local getString={}
    local cost = 0
    local isIdentifiable = commonItem.Equip.IsIdentifiable

    EquipmentTemplate_Disassemble.ForeachKey(function (id) 
        local data = EquipmentTemplate_Disassemble.GetData(id)
        if data == nil then
            return
        end
        if data.GradeRange[0] <= commonItem.Equip.Grade and data.GradeRange[1] >= commonItem.Equip.Grade and EnumToInt(commonItem.Equip.Color) == data.Color then
            cost = data.Cost
            -- getItems = data.GetItem
            local dic=data.GetItem
            if dic then
                CommonDefs.DictIterate(dic, DelegateFactory.Action_object_object(function (___key, ___value) 
                    -- getItems[tonumber(___key)]=tonumber(___value)
                    local itemId=tonumber(___key)
                    local count=tonumber(___value)
                    if count>0 then
                        local raw=SafeStringFormat3("[%s]%sx%d[-]", CItem.GetColorString(itemId), CItem.GetName(itemId), count)
                        table.insert( getString,raw )
                    end
                end))
            end

            if isIdentifiable then
                cost = cost + data.IdentifiableCost
            end

            if isIdentifiable and data.GetItemIdentifiable ~= nil then
                CommonDefs.DictIterate(data.GetItemIdentifiable, DelegateFactory.Action_object_object(function (___key, ___value) 
                    local itemId=tonumber(___key)
                    local count=tonumber(___value)
                    if count>0 then
                        local raw=SafeStringFormat3("[%s]%sx%d[-]", CItem.GetColorString(itemId), CItem.GetName(itemId), count)
                        table.insert( getString,raw )
                    end
                end))
            end
        end
    end)

    --返还金刚钻
    if commonItem.Equip.Color == EnumQualityType.Purple or commonItem.Equip.Color == EnumQualityType.Ghost then
        local yuanshiCount =AllFormulas.Action_Formula[373].Formula(nil,nil,{commonItem.Equip.Grade, commonItem.Equip.Hole})
        if yuanshiCount > 0 then
            local jingangzuanYuanshiItemId = GameSetting_Common.GetData().JinGangZuanYuanShiItemId

            local raw=SafeStringFormat3("[%s]%sx%d[-]", CItem.GetColorString(jingangzuanYuanshiItemId), CItem.GetName(jingangzuanYuanshiItemId), yuanshiCount)
            table.insert( getString,raw )
        end
    end

    local itemGetString=table.concat(getString, LocalString.GetString("、"))

    if commonItem.Equip.BaptizeScore > 0 then
        cost = cost + math.floor((commonItem.Equip.BaptizeScore * GameSetting_Common.GetData().DissambleRate))
    end

    local message = nil
    local fixCost = false
    if commonItem.Equip.IsBinded then
        message = "Equipment_Fenjie_Expensive_Confirm"
        if commonItem.Equip.Color == EnumQualityType.Blue and not isIdentifiable then
            message = "Equipment_Fenjie_Expensive_Confirm_Blue_YinPiao"
        end
        if CClientMainPlayer.Inst.FreeSilver < cost and CClientMainPlayer.Inst.FreeSilver > 0 then
            message = "Equipment_Fenjie_Expensive_YinPiaoLess_Confirm"
            if commonItem.Equip.Color == EnumQualityType.Blue and not isIdentifiable then
                message = "Equipment_Fenjie_Expensive_Confirm_Blue_YinPiao_Less"
            end

            fixCost = true
        elseif CClientMainPlayer.Inst.FreeSilver <= 0 and CClientMainPlayer.Inst.Silver > 0 then
            message = "Equipment_Fenjie_Expensive_YinLiang_Confirm"
            if commonItem.Equip.Color == EnumQualityType.Blue and not isIdentifiable then
                message = "Equipment_Fenjie_Expensive_Confirm_Blue"
            end
        end
    else
        message = "Equipment_Fenjie_Expensive_YinLiang_Confirm"
        if commonItem.Equip.Color == EnumQualityType.Blue and not isIdentifiable then
            message = "Equipment_Fenjie_Expensive_Confirm_Blue"
        end
    end

    if message=="" then
        return
    end
    local messagestr = nil
    if fixCost then
        local yinpiao = CClientMainPlayer.Inst.FreeSilver
        local yinliang = cost - yinpiao
        if commonItem.Equip.Color == EnumQualityType.Blue and not isIdentifiable then
            messagestr = g_MessageMgr:FormatMessage(message, commonItem.ColoredName, yinliang, yinpiao, commonItem.Equip.BaptizeScore)
        else
            messagestr = g_MessageMgr:FormatMessage(message, commonItem.ColoredName, yinliang, yinpiao, itemGetString, commonItem.Equip.BaptizeScore)
        end
    else
        if commonItem.Equip.Color == EnumQualityType.Blue and not isIdentifiable then
            messagestr = g_MessageMgr:FormatMessage(message, commonItem.ColoredName, cost, commonItem.Equip.BaptizeScore)
        else
            messagestr = g_MessageMgr:FormatMessage(message, commonItem.ColoredName, cost, itemGetString, commonItem.Equip.BaptizeScore)
        end
    end

    if messagestr=="" then
        return
    end

    MessageWndManager.ShowOKCancelMessage(messagestr, DelegateFactory.Action(function () 
        Gac2Gas.RequestDisassembleEquip(EnumToInt(place), pos, equipId, costBujingmu, costCCM)
    end), nil, nil, nil, false)
end

function CLuaEquipMgr.CanRemoveStoneOnEquipment(equipId, removeStoneItemId, costYuanBao, newItemId, newItemCount, additionalId, additionalCount) 
    if CEquipmentProcessMgr.Inst.SelectEquipment.itemId ~= equipId then
        return
    end
    local stoneItemId = CLuaEquipMgr.SelectedEquipmentHoleInfo.stoneItemId
    CLuaRemoveInlayGemWnd.s_RemoveReferenceStone = false
    if stoneItemId>0 and stoneItemId<100 then
        CLuaRemoveInlayGemWnd.s_RemoveReferenceStone = true
        --引用类型
        local commonItem = CItemMgr.Inst:GetById(CEquipmentProcessMgr.Inst.SelectEquipment.itemId)
        stoneItemId = commonItem.Equip.HoleItems[stoneItemId]
    end
    if stoneItemId ~= removeStoneItemId then
        return
    end
    CLuaEquipMgr.m_GemRemoveCostYuanbao = costYuanBao
    CLuaEquipMgr.m_GemRemoveBaseItemId = newItemId
    CLuaEquipMgr.m_GemRemoveSplitStoneCount = newItemCount

    CLuaEquipMgr.m_GemRemoveAdditionalItemId = additionalId
    CLuaEquipMgr.m_GemRemoveSplitAdditionalItemCount = additionalCount

    CUIManager.ShowUI(CUIResources.RemoveInlayGemWnd)
end

CLuaEquipMgr.showButtons=false
CLuaEquipMgr.UpgradeAction=nil
CLuaEquipMgr.PutoffAction=nil
function CLuaEquipMgr.ShowGemInfoWnd(showButtons,upgradeAct,putoffAct)
    CLuaEquipMgr.showButtons=showButtons
    CLuaEquipMgr.UpgradeAction=upgradeAct
    CLuaEquipMgr.PutoffAction=putoffAct
    CUIManager.ShowUI(CUIResources.GemInfoWnd)
end

--当前选中的升级的装备
CLuaEquipMgr.m_GemUpdateSelectedEquipment = nil

CLuaEquipMgr.m_GemInlaySelectedEquipment = nil
CLuaEquipMgr.m_GemInlaySelectedJewelInfo = nil


--卸载宝石
CLuaEquipMgr.m_GemRemoveCostYuanbao = 0
CLuaEquipMgr.m_GemRemoveBaseItemId = 0
CLuaEquipMgr.m_GemRemoveSplitStoneCount = 0

CLuaEquipMgr.m_GemRemoveAdditionalItemId = 0
CLuaEquipMgr.m_GemRemoveSplitAdditionalItemCount = 0

CLuaEquipMgr.SelectedEquipmentHoleInfo = nil

-- 新装备融炼相关
CLuaEquipMgr.m_MaterialEquipItemId = ""
CLuaEquipMgr.m_MainEquipItemId = ""
CLuaEquipMgr.m_ResultEquipItemId = ""

-- 拆解相关
CLuaEquipMgr.m_DisassembleData = {}

-- 锻造相关
CLuaEquipMgr.m_ForgeItemId = ""
-- 锻造重置
CLuaEquipMgr.m_ForgeResetItemId = ""
CLuaEquipMgr.m_ForgeResetCostData = {}

-- 判断是否要跳过特效
CLuaEquipMgr.m_PassCompositeFx = false

-- 打开材料装备选择界面
function CLuaEquipMgr:SelectMaterialEquip()
	local itemId = CLuaEquipMgr.m_MainEquipItemId

	-- 当前选中的主装备
	local itemData = CItemMgr.Inst:GetById(itemId)
	local equip = itemData.Equip
	local data = EquipmentTemplate_Equip.GetData(equip.TemplateId)

	local titleStr = LocalString.GetString("选择熔炼材料装备")
	local btnStr = LocalString.GetString("确认选择")
	local descStr = nil
	local enableMultiSelect = false
	local initFunc = nil
	local sortFunc = nil

	local itemSelectFunc = function (chooseItemId)
		local chooseItemData = CItemMgr.Inst:GetById(chooseItemId)
        if chooseItemId == itemId then
            return false
        end

		if chooseItemData and chooseItemData.IsEquip then
			local chooseEquip = chooseItemData.Equip
			local chooseData = EquipmentTemplate_Equip.GetData(chooseEquip.TemplateId)

			-- 部位&绑定必须相同
			-- 必须为红紫鬼 且不高于主装备
			-- 等级也要小于等于主装备
            -- 词条要大于等于主装备

            -- 检查类别与绑定
            if chooseData.Type ~= data.Type or chooseEquip.IsBinded ~= equip.IsBinded then
                return false
            end

            -- 检查词条
            if chooseEquip.WordsCount < equip.WordsCount then
                return false
            end

            -- 锻造的不能熔炼
            if chooseEquip.ForgeLevel > 0 then
                return false
            end

            -- 检查颜色
            local mainColor = data.Color
            local matColor = chooseData.Color
            local colorData = ExtraEquip_CompositeColor.GetData(mainColor)
            if colorData == nil then
                return false
            end
            local colorMatch = false

            for i= 0, colorData.SourceColor.Length -1 do
                if matColor == colorData.SourceColor[i] then
                    colorMatch = true
                end
            end 
            if not colorMatch then
                return false
            end

            -- 检查Tc
            local mainTc = data.TC
            local matTc = chooseData.TC

            local tcData = ExtraEquip_CompositeTC.GetData(mainTc)
            if tcData == nil then
                return false
            end
            local tcMatch = false
            
            local lowLv = tcData.SourceTC[0]
            local highLv = lowLv
            if tcData.SourceTC.Length == 2 then
                highLv = tcData.SourceTC[1]
            end

            -- 颜色与tc必须有一个想等
            if mainTc ~= matTc and mainColor ~= matColor then
                return false
            end

            if matTc >= lowLv and matTc <= highLv then
                return true
            end
		end
		return false
	end

	local commitFunc = function (items)
		if #items == 0 then
			g_MessageMgr:ShowMessage("Equip_Composite_Not_Choose_Material_Equip")
			return
		end

		if items[1] ~= nil and not System.String.IsNullOrEmpty(items[1]) then
			local itemId = items[1]
            -- 选择时候 检查一下
            CLuaEquipMgr:CheckUnsetComposite(itemId)
			CLuaEquipMgr:CheckUnsetBaptize(itemId)
			CLuaEquipMgr:CheckUnsetRecreate(itemId)
            CLuaEquipMgr.m_MaterialEquipItemId = itemId
			g_ScriptEvent:BroadcastInLua("Equip_Composite_Select_Material_Equip", itemId)
		end

        CUIManager.CloseUI(CLuaUIResources.CommonItemChooseWnd)
	end

	local clickFunc = nil
	local accessTemplateIds = {}

    local voidTetx = g_MessageMgr:FormatMessage("Equip_Composite_No_Material_Equip")
    local tipMsg = "Equip_Composite_Material_Equip_Choose_Tip"
	
	LuaCommonItemChooseMgr:ShowItemChooseWnd(titleStr, btnStr, descStr, enableMultiSelect, 
		itemSelectFunc, sortFunc, initFunc, commitFunc, clickFunc, voidTetx, accessTemplateIds, tipMsg)

end

CLuaEquipMgr.EnumCompositWordTipState = {
    Normal = 1,
    Locked = 2,
    CanLock = 3,
    CantLock = 4,
    Disable = 5,
}

-- 获得消耗数据
function CLuaEquipMgr:GetCompositeCostData(mainItem, materialItem, lockCount)

    local mainData = EquipmentTemplate_Equip.GetData(mainItem.TemplateId)
    local matData = EquipmentTemplate_Equip.GetData(materialItem.TemplateId)

    local level = mainData.TC
    local color = EnumToInt(mainItem.Equip.Color)
    local isBind = mainItem.Equip.IsBinded
    local returnData = nil
    local returnId = 0

	ExtraEquip_CompositeCost.Foreach(function(id, data)
        local range = data.GradeRange
        if color == data.Color and level>= range[0] and level <= range[1] then
            print(color, level, range[0], range[1], data.ID)
            returnData = data
            returnId = data.ID
            return
        end
    end)

    returnData = ExtraEquip_CompositeCost.GetData(returnId)

    local isColorDiff = color ~= EnumToInt(materialItem.Equip.Color)
    local isTcDiff = mainData.TC ~= matData.TC

    local yinLiangCost = 0
    local data = nil

    print(isTcDiff, isColorDiff, lockCount)
    if returnData then
        if isTcDiff then
            data = returnData.ItemCost_0_0_1
        elseif isColorDiff then
            data = returnData.ItemCost_0_1_0
        else
            if lockCount == 0 then
                data = returnData.ItemCost_0_0_0
            elseif lockCount == 1 then
                data = returnData.ItemCost_1_0_0
            elseif lockCount == 2 then
                data = returnData.ItemCost_2_0_0
            end
        end

        -- 非绑定才会消耗银两
        if not isBind then
            if isTcDiff then
                yinLiangCost = returnData.YinLiangCost_0_0_1
            elseif isColorDiff then
                yinLiangCost = returnData.YinLiangCost_0_1_0
            else
                yinLiangCost = returnData.YinLiangCost_0_0_0
            end
        end
    end
    return data, yinLiangCost
end

-- 获得消耗数据
-- 只负责显示与回调，具体的上锁逻辑等由界面处理
-- wordList {id, state}的table
function CLuaEquipMgr:InitWord(itemId, table, wordItem, wordList, heightList, callBack)
    Extensions.RemoveAllChildren(table.transform)

    -- 处理颜色
    local item = CItemMgr.Inst:GetById(itemId)
    local color = item.DisplayColor

    for i, wordData in ipairs(wordList) do
        local g = NGUITools.AddChild(table.gameObject, wordItem)
        g:SetActive(true)
        
        local nameLabel = g.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
        local descLabel = g.transform:Find("DescLabel"):GetComponent(typeof(UILabel))
        local bgSprite = g.transform:Find("Bg"):GetComponent(typeof(UISprite))
        
        local data = Word_Word.GetData(wordData.id)

        descLabel.overflowMethod = UILabelOverflow.ShrinkContent
        if data then
            nameLabel.text = SafeStringFormat3(LocalString.GetString("[%s]"), data.Name)
            descLabel.text = data.Description
        else
            nameLabel.text = ""
            descLabel.text = ""
        end

        nameLabel.color = color
        descLabel.color = color

        if heightList then
            descLabel.height = heightList[i]
        end
        

        -- 修改按钮形式
        local lock = g.transform:Find("Lock").gameObject
        local lockBg = g.transform:Find("LockBg")
        local forbidden = g.transform:Find("Forbidden ")

        lock.gameObject:SetActive(false)

        if lockBg then
            lockBg.gameObject:SetActive(false)
            forbidden.gameObject:SetActive(false)
        end

        if wordData.state == CLuaEquipMgr.EnumCompositWordTipState.CantLock and forbidden then
            forbidden.gameObject:SetActive(true)
        elseif wordData.state == CLuaEquipMgr.EnumCompositWordTipState.Locked then
            if lockBg then
                lockBg.gameObject:SetActive(true)
            end
            lock.gameObject:SetActive(true)
        elseif wordData.state == CLuaEquipMgr.EnumCompositWordTipState.CanLock then
            if lockBg then
                lockBg.gameObject:SetActive(true)
            end
        elseif wordData.state == CLuaEquipMgr.EnumCompositWordTipState.Disable then
            nameLabel.color = Color(1,1,1,0.5)
            descLabel.color = Color(1,1,1,0.5)
        end

        if callBack and lockBg then
            wordData.itemId = itemId
            UIEventListener.Get(lockBg.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
                callBack(wordData)
            end)
        end
    end

    table:Reposition()
    RegisterTickOnce(function()
        if table then
            table:Reposition()
        end
    end, 1)
end

-- 获取物品的位置
function CLuaEquipMgr:GetItemPlaceAndPos(itemId)
    local item = CItemMgr.Inst:GetById(itemId)

    if item then
        local place = item.OnBody and EnumItemPlace.Body or EnumItemPlace.Bag
        local pos = CItemMgr.Inst:FindItemPosition(place, itemId)

        return EnumToInt(place), pos
    else
        return 0,0
    end
end

-- 检查是否有未保存的熔炼结果
function CLuaEquipMgr:CheckUnsetComposite(itemId, callBack)

    local item = CItemMgr.Inst:GetById(itemId)
    local equip = item.Equip
    -- 材料装备固定词条
    local hasUnset = equip:HasTempCompositeWordData()

    if hasUnset then
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("Equip_Composite_Unset_Tip", item.ColoredName), 
            function()
                if callBack then
                    callBack()
                else
                    local place, pos = CLuaEquipMgr:GetItemPlaceAndPos(itemId)
                    Gac2Gas.CompositeExtraEquipWordResultConfirm(place, pos, itemId, false)
                end
        end, function()
            -- 跳过特效
            CLuaEquipMgr.m_PassCompositeFx = true
            CLuaEquipMgr.m_ResultEquipItemId = itemId
            CUIManager.ShowUI(CLuaUIResources.EquipCompositeResultWnd)
        end, LocalString.GetString("删除词条"), LocalString.GetString("查看词条"), false)
    else
        if callBack then
            callBack()
        end
    end
end

-- 检查是否有未保存的洗练结果
function CLuaEquipMgr:CheckUnsetBaptize(itemId, callBack)
    local item = CItemMgr.Inst:GetById(itemId)
    local equip = item.Equip

    -- 材料装备固定词条
    local hasUnset = equip:HasTempWordData()

    if hasUnset then
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("Equip_Baptize_Unset_Tip", item.ColoredName), 
            function()
                if callBack then
                    callBack()
                end
        end, function()
            -- 打开洗练界面
            CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize = CItemMgr.Inst:GetItemInfo(itemId)
            CUIManager.ShowUI(CUIResources.EquipWordBaptizeWnd)
        end, LocalString.GetString("删除词条"), LocalString.GetString("查看词条"), false)
    else
        if callBack then
            callBack()
        end
    end
end

CLuaEquipMgr.m_OpenUnsetRecreate = false

-- 检查是否有未保存的再造
-- 取消是去查看再造的结果
function CLuaEquipMgr:CheckUnsetRecreate(itemId, callBack)
    local item = CItemMgr.Inst:GetById(itemId)
    local equip = item.Equip

    -- 材料装备的再造数据
    local hasUnset = equip.RecreateData.Data and equip.RecreateData.Data.Length > 0

    if hasUnset then
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("Equip_Recreate_Unset_Tip", item.ColoredName), 
            function()
                if callBack then
                    callBack()
                end
        end, function()
            -- 打开再造界面
            CLuaEquipMgr.m_OpenUnsetRecreate = true
            CLuaEquipMgr.m_RecreateItemId = itemId
            CUIManager.ShowUI(CLuaUIResources.RecreateExtraEquipWnd)

        end, LocalString.GetString("删除词条"), LocalString.GetString("查看词条"), false)
    else
        if callBack then
            callBack()
        end
    end
end

-- 检查并更新物品
function CLuaEquipMgr:CheckItemUpdate()
    -- 新装备融炼相关
    if CLuaEquipMgr.m_MaterialEquipItemId == nil or CLuaEquipMgr.m_MaterialEquipItemId == "" or
        CItemMgr.Inst:GetById(CLuaEquipMgr.m_MaterialEquipItemId) == nil then
        CLuaEquipMgr.m_MaterialEquipItemId = ""
    end
    if CLuaEquipMgr.m_MainEquipItemId == nil or CLuaEquipMgr.m_MainEquipItemId == "" or
        CItemMgr.Inst:GetById(CLuaEquipMgr.m_MainEquipItemId) == nil then
        CLuaEquipMgr.m_MainEquipItemId = ""
    end
    if CLuaEquipMgr.m_ResultEquipItemId == nil or CLuaEquipMgr.m_ResultEquipItemId == "" or
        CItemMgr.Inst:GetById(CLuaEquipMgr.m_ResultEquipItemId) == nil then
        CLuaEquipMgr.m_ResultEquipItemId = ""
    end
    if CLuaEquipMgr.m_ForgeItemId == nil or CLuaEquipMgr.m_ForgeItemId == "" or
        CItemMgr.Inst:GetById(CLuaEquipMgr.m_ForgeItemId) == nil then
        CLuaEquipMgr.m_ForgeItemId = ""
    end
    if CLuaEquipMgr.m_ForgeResetItemId == nil or CLuaEquipMgr.m_ForgeResetItemId == "" or
        CItemMgr.Inst:GetById(CLuaEquipMgr.m_ForgeResetItemId) == nil then
        CLuaEquipMgr.m_ForgeResetItemId = ""
    end
end

-- 尝试选择额外装备
function CLuaEquipMgr:TryGetExtraEquip()
    local bagCount = CClientMainPlayer.Inst.ItemProp:GetPlaceMaxSize(EnumItemPlace.Bag)
    -- 遍历包裹里的物品
    for i=0, bagCount-1 do
        local itemId = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i + 1)
        -- 确定合适的物品
        local itemData = CItemMgr.Inst:GetById(itemId)

        if itemData and itemData.IsEquip and itemData.Equip.IsExtraEquipment then
            return CItemMgr.Inst:GetItemInfo(itemId)
        end
    end

    local bodyCount = CClientMainPlayer.Inst.ItemProp:GetPlaceMaxSize(EnumItemPlace.Body)
    -- 遍历包裹里的物品
    for i=0, bodyCount-1 do
        local itemId = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Body, i + 1)
        -- 确定合适的物品
        local itemData = CItemMgr.Inst:GetById(itemId)
        if itemData and itemData.IsEquip and itemData.Equip.IsExtraEquipment then
            return CItemMgr.Inst:GetItemInfo(itemId)
        end
    end

    return nil
end

-- 是否是锻造相关道具
function CLuaEquipMgr:IsForgeItem(teamplateId)
    local setting = ExtraEquip_Setting.GetData()

    if setting.DuanZaoJingYuanItemId == teamplateId or
    setting.HongBeiItemId == teamplateId or
    setting.ZiBeiItemId == teamplateId or
    setting.DownLongItemId == teamplateId or
    setting.UpLongItemId == teamplateId then
        return true
    end
    
    return false
end

-- 是否是熔炼道具
function CLuaEquipMgr:IsCompositeItem(teamplateId)
    local setting = ExtraEquip_Setting.GetData()
    if teamplateId == setting.TianDiLuItemId then
        return true
    end
    return false
end

function CLuaEquipMgr:ClearExtraEquipInfo()
    -- 新装备融炼相关
    self.m_MaterialEquipItemId = ""
    self.m_MainEquipItemId = ""
    self.m_ResultEquipItemId = ""

    -- 拆解相关
    self.m_DisassembleData = {}

    -- 锻造相关
    self.m_ForgeItemId = ""
    -- 锻造重置
    self.m_ForgeResetItemId = ""
    self.m_ForgeResetCostData = {}
    self.m_PassCompositeFx = false
end

CLuaEquipMgr.m_RecreateItemId = ""
CLuaEquipMgr.m_RecreateTempScore = 0
CLuaEquipMgr.m_IsWuLian = false

-- 获取再造的消耗
function CLuaEquipMgr:GetRecreateCost(itemId, isWuLian)
    local item = CItemMgr.Inst:GetById(itemId)
    local equipData = EquipmentTemplate_Equip.GetData(item.Equip.TemplateId)

    local level = equipData.TC
    local color = EnumToInt(item.Equip.Color)
    local returnId = 0

	ExtraEquip_RecreateCost.Foreach(function(id, data)
        local range = data.GradeRange
        if color == data.Color and level>= range[0] and level <= range[1] then
            returnId = data.ID
        end
    end)

    if returnId == 0 then
        return 0,0
    end

    local returnData = ExtraEquip_RecreateCost.GetData(returnId)

    if isWuLian then
        return returnData.FiveItemCost[0], returnData.FiveItemCost[1], returnData
    else
        return returnData.ItemCost[0], returnData.ItemCost[1], returnData
    end
end

function CLuaEquipMgr:EquipWordIdIsMaxWord(itemId,wordIdList,isShenBingWord)
    local item = CItemMgr.Inst:GetById(itemId)
    if not item then return end

    local equipData = EquipmentTemplate_Equip.GetData(item.Equip.TemplateId)

    local equipTC = equipData.TC
    
    local colorData = ExtraEquip_WordMaxTC.GetData(equipData.Color)
    local colorDeviation = colorData and colorData.TCDEVIATION or 0
    
    local wordMaxTC = 0
    if isShenBingWord then
        wordMaxTC = AllFormulas.Action_Formula[ShenBing_Setting.GetData().MaxWordTcFormulaId].Formula(nil,nil,{equipTC, item.Equip.HolyTrainLevel})
    else
        if IdPartition.IdIsTalisman(item.Equip.TemplateId) then
            wordMaxTC = equipTC * ((100 + Talisman_Setting.GetData().Basic_Word_TC_Range*100) / 100)
        elseif IdPartition.IdIsBackPendant(item.Equip.TemplateId) then
            wordMaxTC = equipTC * ((100 + ExtraEquip_Setting.GetData().WORD_TC_UP_DEVIATION*100) / 100)
        elseif IdPartition.IdIsHiddenWeapon(item.Equip.TemplateId) then
            local maxDeviation = (100 + ExtraEquip_Setting.GetData().WORD_TC_UP_DEVIATION*100) * (100 + colorDeviation) / 100
            wordMaxTC = equipTC * (maxDeviation / 100)
        elseif IdPartition.IdIsBeaded(item.Equip.TemplateId) then
            local maxDeviation = (100 + ExtraEquip_Setting.GetData().WORD_TC_UP_DEVIATION*100) * (100 + colorDeviation) / 100
            wordMaxTC = equipTC * (maxDeviation / 100)
        else
            wordMaxTC = equipTC * ((100 + GameSetting_Common.GetData().WORD_TC_DEVIATION*100) / 100)
        end
        wordMaxTC = math.max(1,math.floor(wordMaxTC))
    end

    local isTopWords = {}
    for _, wordId in ipairs(wordIdList) do
        local wordClass = math.floor(wordId/100)
        local wordIndex = wordId%100
        local maxIndex = wordIndex
        for i=wordIndex+1,1000 do
            local currentWordId = wordClass * 100 + i
            local wordData = Word_Word.GetData(currentWordId)
            if not wordData then break end
            local wordTC = wordData.TC
            if wordTC <= wordMaxTC then
                maxIndex = i
            else
                break
            end
        end

        local maxWordId = wordClass * 100 + maxIndex
        local maxWordData = Word_Word.GetData(maxWordId)
        local oriWordData = Word_Word.GetData(wordId)
        if not (maxWordData and oriWordData) then
            table.insert(isTopWords, false)
        else
            if maxWordData.Description == oriWordData.Description then
                table.insert(isTopWords, true)
            else
                table.insert(isTopWords, false)
            end
        end
    end

    return isTopWords
end

CLuaEquipMgr.m_ZhushabiWordMaxLevel = nil
function CLuaEquipMgr:ChuanjiabaoWordIsMaxWord(itemId, wordIdList)
    local item = CItemMgr.Inst:GetById(itemId)
    if not item then return end

    local cjbTemplate = Item_Item.GetData(item.TemplateId)

    local wordInfo = nil
    if item.Item.Type==EnumItemType_lua.EvaluatedZhushabi then
        if item.Item.ExtraVarData and item.Item.ExtraVarData.Data then
            wordInfo = CreateFromClass(CChuanjiabaoWordType)
            wordInfo:LoadFromString(item.Item.ExtraVarData.Data)
        end
    else--传家宝
        wordInfo = item.Item.ChuanjiabaoItemInfo and item.Item.ChuanjiabaoItemInfo.WordInfo
    end

    local star = wordInfo.Star
    local name = cjbTemplate.Name
    local cjbSetting = House_ChuanjiabaoSetting.GetData()
    local lv = 0
    if string.find(name,LocalString.GetString("1级")) then
        lv = cjbSetting.ChuanjiabaoLowLevel
    elseif string.find(name,LocalString.GetString("2级")) then
        lv = cjbSetting.ChuanjiabaoNormalLevel
    elseif string.find(name,LocalString.GetString("3级")) then
        lv = cjbSetting.ChuanjiabaoHighLevel
    end

    local type = wordInfo.Type

    if not CLuaEquipMgr.m_ZhushabiWordMaxLevel then
        self:LoadZhushabiWordMaxLevelData()
    end

    local isTopWords = {}
    for _, wordId in ipairs(wordIdList) do
        local wordClass = math.floor(wordId/100)
        local wordIndex = wordId%100
        local maxIndex = self.m_ZhushabiWordMaxLevel[type][star][lv]

        local maxWordId = wordClass * 100 + maxIndex
        local maxWordData = Word_Word.GetData(maxWordId)
        while(not maxWordData and maxIndex>0) do
            maxIndex = maxIndex - 1
            maxWordId = wordClass * 100 + maxIndex
            maxWordData = Word_Word.GetData(maxWordId)
        end

        local oriWordData = Word_Word.GetData(wordId)
        if not (maxWordData and oriWordData) then
            table.insert(isTopWords, false)
        else
            if maxWordData.Description == oriWordData.Description then
                table.insert(isTopWords, true)
            else
                table.insert(isTopWords, false)
            end
        end
    end

    return isTopWords
end

function CLuaEquipMgr:LoadZhushabiWordMaxLevelData()
    self.m_ZhushabiWordMaxLevel = {}
    House_ZhushabiWordLevel.Foreach(function(id, data)
        local type = data.Type
        local typeKeys = {}
        if type == "RenGe" then
            table.insert(typeKeys,100)
        elseif type == "Other" then
            table.insert(typeKeys,200)
            table.insert(typeKeys,300)
        end
        local cjbSetting = House_ChuanjiabaoSetting.GetData()
        for _, typeKey in ipairs(typeKeys) do
            self.m_ZhushabiWordMaxLevel[typeKey] = self.m_ZhushabiWordMaxLevel[typeKey] or {}
            local StarLevel = data.StarLevel
            self.m_ZhushabiWordMaxLevel[typeKey][StarLevel] = self.m_ZhushabiWordMaxLevel[typeKey][StarLevel] or {}

            for i= 0, data.High.Length -1 do
                self.m_ZhushabiWordMaxLevel[typeKey][StarLevel][cjbSetting.ChuanjiabaoHighLevel] =  math.max(self.m_ZhushabiWordMaxLevel[typeKey][StarLevel][cjbSetting.ChuanjiabaoHighLevel] or 0,data.High[i])
            end

            for i= 0, data.Normal.Length -1 do
                self.m_ZhushabiWordMaxLevel[typeKey][StarLevel][cjbSetting.ChuanjiabaoNormalLevel] =  math.max(self.m_ZhushabiWordMaxLevel[typeKey][StarLevel][cjbSetting.ChuanjiabaoNormalLevel] or 0,data.Normal[i])
            end

            for i= 0, data.Low.Length -1 do
                self.m_ZhushabiWordMaxLevel[typeKey][StarLevel][cjbSetting.ChuanjiabaoLowLevel] =  math.max(self.m_ZhushabiWordMaxLevel[typeKey][StarLevel][cjbSetting.ChuanjiabaoLowLevel] or 0,data.Low[i])
            end
        end
    end)
end