local UITexture = import "UITexture"

local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaBaoYuDuoYu2023SubmitWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaBaoYuDuoYu2023SubmitWnd, "LingYuLabel", "LingYuLabel", UILabel)
RegistChildComponent(LuaBaoYuDuoYu2023SubmitWnd, "SubmitBtn", "SubmitBtn", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023SubmitWnd, "TeamScore", "TeamScore", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023SubmitWnd, "TeamSubmitInfo", "TeamSubmitInfo", UILabel)
RegistChildComponent(LuaBaoYuDuoYu2023SubmitWnd, "ProgressTex1", "ProgressTex1", UITexture)
RegistChildComponent(LuaBaoYuDuoYu2023SubmitWnd, "ProgressTex2", "ProgressTex2", UITexture)
RegistChildComponent(LuaBaoYuDuoYu2023SubmitWnd, "ProgressTex3", "ProgressTex3", UITexture)
RegistChildComponent(LuaBaoYuDuoYu2023SubmitWnd, "ProgressTex4", "ProgressTex4", UITexture)
RegistChildComponent(LuaBaoYuDuoYu2023SubmitWnd, "ProgressTex5", "ProgressTex5", UITexture)
RegistChildComponent(LuaBaoYuDuoYu2023SubmitWnd, "Player1", "Player1", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023SubmitWnd, "Player2", "Player2", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023SubmitWnd, "Player3", "Player3", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023SubmitWnd, "Player4", "Player4", GameObject)
RegistChildComponent(LuaBaoYuDuoYu2023SubmitWnd, "Player5", "Player5", GameObject)

--@endregion RegistChildComponent end

function LuaBaoYuDuoYu2023SubmitWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self:InitWndData()
    self:RefreshVariableUI()
    self:InitUIEvent()
end

function LuaBaoYuDuoYu2023SubmitWnd:Init()

end

--@region UIEvent

--@endregion UIEvent

function LuaBaoYuDuoYu2023SubmitWnd:InitWndData()
    self.treasureDemandNumber = Double11_BYDLY.GetData().TreasureNeedNum
    self.teamColorList = {NGUIText.ParseColor24("A43C3C",0),
                          NGUIText.ParseColor24("FF854F",0),
                          NGUIText.ParseColor24("F3EC80",0),
                          NGUIText.ParseColor24("007F4D",0),
                          NGUIText.ParseColor24("50AEFF",0)}
    self.progressTexList = {self.ProgressTex1, self.ProgressTex2,  self.ProgressTex3,  self.ProgressTex4,  self.ProgressTex5}
    self.playerList = {self.Player1, self.Player2, self.Player3, self.Player4, self.Player5}
    
    self.buffId = Double11_BYDLY.GetData().TreasureBuffCls
    self.treasureBuffList = {}
    for i = 1, Double11_BYDLY.GetData().TreasureMaxNum do
        local realBuffId = self.buffId * 100 + i
        self.treasureBuffList[realBuffId] = true
    end
end 

function LuaBaoYuDuoYu2023SubmitWnd:RefreshVariableUI()
    if not CClientMainPlayer.Inst then
        return
    end
    
    local treasureData = LuaShuangshiyi2023Mgr.m_BaoYuDuoYuTreasureInfo
    if treasureData == nil or treasureData == {} then
        self.TeamSubmitInfo.text = 0 .. "/" .. self.treasureDemandNumber
        for i = 1, #self.progressTexList do
            self.progressTexList[i].fillAmount = 0
        end
        for i = 1, #self.playerList do
            self.playerList[i]:SetActive(false)
        end
    else
        self.TeamSubmitInfo.text = treasureData.totalTreasure .. "/" .. self.treasureDemandNumber
        local sum = 0
        local mainPlayerDataIndex = 0
        for i = 1, #treasureData.formatTeamTreasureInfo do
            if treasureData.formatTeamTreasureInfo[i].playerId == CClientMainPlayer.Inst.Id then
                mainPlayerDataIndex = i
                sum = sum + treasureData.formatTeamTreasureInfo[i].treasure
                --refresh left progress
                local tex = self.progressTexList[1]
                tex.fillAmount = sum / self.treasureDemandNumber
                tex.color = self.teamColorList[1]

                --refrsh right names
                local playerObj = self.playerList[1]
                playerObj:SetActive(true)
                playerObj.transform:Find("Circle"):GetComponent(typeof(UITexture)).color = self.teamColorList[1]
                playerObj.transform:Find("PlayerName"):GetComponent(typeof(UILabel)).text = treasureData.formatTeamTreasureInfo[i].playerName
                playerObj.transform:Find("PlayerSubmitNumber"):GetComponent(typeof(UILabel)).text = LocalString.GetString("提交") ..
                        treasureData.formatTeamTreasureInfo[i].treasure .. LocalString.GetString("个")
            end
        end
        
        for i = 1, #treasureData.formatTeamTreasureInfo do
            if i ~= mainPlayerDataIndex then
                local newIndex = mainPlayerDataIndex > i and (i+1) or i
                sum = sum + treasureData.formatTeamTreasureInfo[i].treasure
                --refresh left progress
                local tex = self.progressTexList[newIndex]
                tex.fillAmount = sum / self.treasureDemandNumber
                tex.color = self.teamColorList[newIndex]

                --refrsh right names
                local playerObj = self.playerList[newIndex]
                playerObj:SetActive(true)
                playerObj.transform:Find("Circle"):GetComponent(typeof(UITexture)).color = self.teamColorList[newIndex]
                playerObj.transform:Find("PlayerName"):GetComponent(typeof(UILabel)).text = treasureData.formatTeamTreasureInfo[i].playerName
                playerObj.transform:Find("PlayerSubmitNumber"):GetComponent(typeof(UILabel)).text = LocalString.GetString("提交") ..
                        treasureData.formatTeamTreasureInfo[i].treasure .. LocalString.GetString("个") 
            end
        end
        for i = #treasureData.formatTeamTreasureInfo + 1, #self.progressTexList do
            self.progressTexList[i].fillAmount = 0
        end
        for i = #treasureData.formatTeamTreasureInfo + 1, #self.playerList do
            self.playerList[i]:SetActive(false)
        end
    end

    local buffLevel = LuaShuangshiyi2023Mgr.m_PvePickBuffLevel
    self.LingYuLabel.text = LocalString.GetString("当前持有灵玉宝箱") .. buffLevel .. LocalString.GetString("个")
end 

function LuaBaoYuDuoYu2023SubmitWnd:InitUIEvent()
    UIEventListener.Get(self.SubmitBtn).onClick = DelegateFactory.VoidDelegate(function (_)
        Gac2Gas.Double11BYDLYSubmitTreasure()
        CUIManager.CloseUI(CLuaUIResources.BaoYuDuoYu2023SubmitWnd)
    end)
end 

function LuaBaoYuDuoYu2023SubmitWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncDouble11BYDLYFirstStageInfo", self, "RefreshVariableUI")
end

function LuaBaoYuDuoYu2023SubmitWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncDouble11BYDLYFirstStageInfo", self, "RefreshVariableUI")
end 
