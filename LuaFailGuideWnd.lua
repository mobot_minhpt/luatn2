local GameObject = import "UnityEngine.GameObject"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaFailGuideWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaFailGuideWnd, "EnhanceButton", "EnhanceButton", GameObject)
RegistChildComponent(LuaFailGuideWnd, "SkillButton", "SkillButton", GameObject)
RegistChildComponent(LuaFailGuideWnd, "LingShouButton", "LingShouButton", GameObject)
RegistChildComponent(LuaFailGuideWnd, "GuildButton", "GuildButton", GameObject)

--@endregion RegistChildComponent end

function LuaFailGuideWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.EnhanceButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnEnhanceButtonClick()
	end)


	
	UIEventListener.Get(self.SkillButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSkillButtonClick()
	end)


	
	UIEventListener.Get(self.LingShouButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLingShouButtonClick()
	end)


	
	UIEventListener.Get(self.GuildButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGuildButtonClick()
	end)


    --@endregion EventBind end
end

function LuaFailGuideWnd:Init()

end

--@region UIEvent

function LuaFailGuideWnd:OnEnhanceButtonClick()
    CUIManager.ShowUI(CUIResources.EquipmentProcessWnd)
end

function LuaFailGuideWnd:OnSkillButtonClick()
    CUIManager.ShowUI(CUIResources.SkillWnd)
end

function LuaFailGuideWnd:OnLingShouButtonClick()
    CUIManager.ShowUI(CUIResources.LingShouMainWnd)
end

function LuaFailGuideWnd:OnGuildButtonClick()
    if not CClientMainPlayer.Inst then return end
    if not CClientMainPlayer.Inst:IsInGuild() then
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("Guide_NoGuild_Alert"),
        function()
            CUIManager.ShowUI(CUIResources.GuildWnd)
        end,function()

        end,nil,nil,false)
    else
        CUIManager.ShowUI(CUIResources.GuildTrainWnd)
    end
end


--@endregion UIEvent

