local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local SoundManager=import "SoundManager"
local PlayerSettings=import "L10.Game.PlayerSettings"
local CTeamGroupMgr = import "L10.Game.CTeamGroupMgr"
local Gac2Gas2 = import "L10.Game.Gac2Gas"
local CTeamMgr = import "L10.Game.CTeamMgr"
local TweenScale = import "TweenScale"
local CGamePlayMgr = import "L10.Game.CGamePlayMgr"
local CChatMgr = import "L10.Game.CChatMgr"
local CScene = import "L10.Game.CScene"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaMusicPlayHitingWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaMusicPlayHitingWnd, "SongName", "SongName", UILabel)
RegistChildComponent(LuaMusicPlayHitingWnd, "PlayerNumLabel", "PlayerNumLabel", UILabel)
RegistChildComponent(LuaMusicPlayHitingWnd, "ScoreLabel", "ScoreLabel", UILabel)
RegistChildComponent(LuaMusicPlayHitingWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaMusicPlayHitingWnd, "HitEffectLabel", "HitEffectLabel", UILabel)
RegistChildComponent(LuaMusicPlayHitingWnd, "ComboNum", "ComboNum", UILabel)
RegistChildComponent(LuaMusicPlayHitingWnd, "PlayerName1", "PlayerName1", UILabel)
RegistChildComponent(LuaMusicPlayHitingWnd, "Score1", "Score1", UILabel)
RegistChildComponent(LuaMusicPlayHitingWnd, "PlayerName2", "PlayerName2", UILabel)
RegistChildComponent(LuaMusicPlayHitingWnd, "Score2", "Score2", UILabel)
RegistChildComponent(LuaMusicPlayHitingWnd, "TipMsgLabel", "TipMsgLabel", UILabel)
RegistChildComponent(LuaMusicPlayHitingWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaMusicPlayHitingWnd, "HitTableView", "HitTableView", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaMusicPlayHitingWnd,"m_HitNodeTable")
RegistClassMember(LuaMusicPlayHitingWnd,"m_Music")
RegistClassMember(LuaMusicPlayHitingWnd,"m_ComboNum")
RegistClassMember(LuaMusicPlayHitingWnd,"m_HittingEvenet")
RegistClassMember(LuaMusicPlayHitingWnd,"m_LeftTime")
RegistClassMember(LuaMusicPlayHitingWnd,"m_EffectTween")
function LuaMusicPlayHitingWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.CloseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseButtonClick()
	end)


    --@endregion EventBind end
    self.m_HitNodeTable = {}

    self.m_EffectStr = {
        SafeStringFormat3(LocalString.GetString("[c][FFFFFFB2]错失[-][c]")),
        SafeStringFormat3(LocalString.GetString("[c][FFFFFFFF]良好[-][c]")),
        SafeStringFormat3(LocalString.GetString("[c][F0E488FF]优秀[-][c]")),
        SafeStringFormat3(LocalString.GetString("[c][E9B604FF]完美[-][c]")),
        SafeStringFormat3(LocalString.GetString("[c][FFFFFFB2]错失[-][c]")),
    }
    --完美连击
    LuaMeiXiangLouMgr.m_MyPerfectCombo = true
    LuaMeiXiangLouMgr.m_PartnerPerfectCombo = true

    self.m_EffectTween = self.HitEffectLabel.transform:GetComponent(typeof(TweenScale))
    self.m_EffectTween.enabled = false

    if not PlayerSettings.MusicEnabled then
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("MXL_SONGPLAY_OPENMUSIC"), 
        DelegateFactory.Action(function ()            
            LuaMeiXiangLouMgr.m_MusicVolunm = 1
            if LuaMeiXiangLouMgr.m_Music then
                LuaMeiXiangLouMgr.m_Music:setVolume(LuaMeiXiangLouMgr.m_MusicVolunm)
            end
        end), 
        DelegateFactory.Action(function ()            
            LuaMeiXiangLouMgr.m_MusicVolunm = 0
            if LuaMeiXiangLouMgr.m_Music then
                LuaMeiXiangLouMgr.m_Music:setVolume(LuaMeiXiangLouMgr.m_MusicVolunm)
            end
        end), LocalString.GetString("是"), LocalString.GetString("否"), false)
    else
        LuaMeiXiangLouMgr.m_MusicVolunm = 1
    end
end

function LuaMusicPlayHitingWnd:Init()
    for i=1,self.HitTableView.transform.childCount,1 do
        local name = "HitNode"..i
        local node = self.HitTableView.transform:Find("HitNode"..i)
        
        if node then
            node = node:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
            self.m_HitNodeTable[i] = node
        end
    end
    self.m_ComboNum = 0
    LuaMeiXiangLouMgr.m_SingleCombo = 0
    self.m_LeftTime = 0
    self.ScoreLabel.text = nil
    self:InitMusic()
    self:InitTopMusicInfo()
    self.PlayerName1.text = nil
    self.PlayerName2.text = nil
    self.Score1.text = nil
    self.Score2.text = nil
end

function LuaMusicPlayHitingWnd:OnEnable()
    g_ScriptEvent:AddListener("ServerSongBeatArrived", self, "OnServerSongBeatArrived")
    g_ScriptEvent:AddListener("SyncSongPlayPressButton", self, "OnSyncSongPlayPressButton")
    g_ScriptEvent:AddListener("EndSongPlay", self, "OnEndSongPlay")
    g_ScriptEvent:AddListener("OnFmodMarkerTrigger", self, "OnFmodMarkerTrigger")
    g_ScriptEvent:AddListener("StartSongPlay", self, "OnStartSongPlay")
    g_ScriptEvent:AddListener("AppFocusChange", self, "OnApplicationFocus")
    g_ScriptEvent:AddListener("MainPlayerCreated", self, "OnMainPlayCreated")
end

function LuaMusicPlayHitingWnd:OnDestroy()
    g_ScriptEvent:RemoveListener("ServerSongBeatArrived", self, "OnServerSongBeatArrived")
    g_ScriptEvent:RemoveListener("SyncSongPlayPressButton", self, "OnSyncSongPlayPressButton")
    g_ScriptEvent:RemoveListener("EndSongPlay", self, "OnEndSongPlay")
    g_ScriptEvent:RemoveListener("OnFmodMarkerTrigger", self, "OnFmodMarkerTrigger")
    g_ScriptEvent:RemoveListener("StartSongPlay", self, "OnStartSongPlay")
    g_ScriptEvent:RemoveListener("AppFocusChange", self, "OnApplicationFocus")
    g_ScriptEvent:RemoveListener("MainPlayerCreated", self, "OnMainPlayCreated")

    SoundManager.s_BlockBgMusic=false
    if LuaMeiXiangLouMgr.m_Music then
        SoundManager.Inst:StopSound(LuaMeiXiangLouMgr.m_Music)
        LuaMeiXiangLouMgr.m_Music=nil
    end
    if self.m_ClientBeatTick then
        UnRegisterTick(self.m_ClientBeatTick)
        self.m_ClientBeatTick = nil
    end
    if self.m_LeftTimeCountDownTick then
        UnRegisterTick(self.m_LeftTimeCountDownTick)
        self.m_LeftTimeCountDownTick = nil
    end
    SoundManager.Inst:StartBGMusic()

    --恢复侧边栏消息
    if not CUIManager.IsLoaded(CLuaUIResources.CommonSideDialogWnd) then
        CUIManager.ShowUI(CLuaUIResources.CommonSideDialogWnd)
    else
        g_ScriptEvent:BroadcastInLua("OnUpdateCommonSideDialogList")
    end
end

function LuaMusicPlayHitingWnd:OnEndSongPlay()

end

--luadouzizhuwnd.lua
function LuaMusicPlayHitingWnd:RefreshTeamMember()
    local songId = LuaMeiXiangLouMgr.m_CurPlaySongId
    local data = MeiXiangLou_SongInfo.GetData(songId)
    if data.OperatorNumber ~= 2 then
        return
    end

    local memberList = CTeamMgr.Inst.Members
    if not memberList then
        self.PlayerName2.text = LocalString.GetString("队友")
        return
    end

    local player = CClientMainPlayer.Inst

    local findPartner = false
    for i=0,memberList.Count-1,1 do
        local member = memberList[i]
        if member and member.m_MemberId ~= player.Id then
            findPartner = true
            self.PlayerName2.text = member.m_MemberName
            LuaMeiXiangLouMgr.m_PartnerMemberInfo = member
        end
    end
    if not findPartner then
        self.PlayerName2.text = LocalString.GetString("队友")
    end
end

function LuaMusicPlayHitingWnd:InitMusic()
    SoundManager.s_BlockBgMusic=true
    SoundManager.Inst:StopBGMusic()
    local songId = LuaMeiXiangLouMgr.m_CurPlaySongId
    local songInfo = MeiXiangLou_SongInfo.GetData(songId)
    local event = songInfo.SongEvent
    self.m_HittingEvenet = event
    self.m_HittingSongId = songId

    --prepare data
    local startId = songInfo.StartId
    local endId = songInfo.EndId
    
    LuaMeiXiangLouMgr.m_MusicMarkerData = {}
    for idx = startId,endId,1 do
        local data = MeiXiangLou_Song.GetData(idx)
        if data then
            local info = {}
            info.Operator = data.Operator
            local ss = data.Buttons
            info.Buttons = {}
            info.LongPressTime = {}
            info.rhythmIdx = idx
            for cStr in string.gmatch(ss, "([^;]+)") do
                for btnzhStr in string.gmatch(cStr, "([^,]+)") do
                    local sbtn = tonumber(btnzhStr)
                    if sbtn then
                        table.insert(info.Buttons,sbtn)
                        table.insert(info.LongPressTime,0)
                    else
                        for btnIdx, longPreesTime in string.gmatch(btnzhStr, "(%d+)%(([^%s]-)%)") do
                            table.insert(info.Buttons,tonumber(btnIdx))
                            table.insert(info.LongPressTime,tonumber(longPreesTime))  
                        end
                    end
                end   
            end
            LuaMeiXiangLouMgr.m_MusicMarkerData[data.MusicMarker] = info
        end 
    end

end

function LuaMusicPlayHitingWnd:OnStartSongPlay(songId)
    local timePosition = 0
    local songInfo = MeiXiangLou_SongInfo.GetData(songId)
    local event = songInfo.SongEvent
    
    LuaMeiXiangLouMgr.m_Music = SoundManager.Inst:PlaySound(event,Vector3.zero, LuaMeiXiangLouMgr.m_MusicVolunm, nil, timePosition)
    
    SoundManager.Inst:AddMarkerCallback(LuaMeiXiangLouMgr.m_Music)
    self:InitLeftTime(timePosition/1000)
end

function LuaMusicPlayHitingWnd:OnApplicationFocus(args)
    if not CommonDefs.IsPCGameMode() then
        local focusStatus = args[0]
        LuaMeiXiangLouMgr.m_FocusStatus = focusStatus
        if focusStatus == false then
            SoundManager.Inst:StopSound(LuaMeiXiangLouMgr.m_Music)
            if self.m_LeftTimeCountDownTick then
                UnRegisterTick(self.m_LeftTimeCountDownTick)
                self.m_LeftTimeCountDownTick = nil
            end
            self.m_LeftTime = 0
            LuaMeiXiangLouMgr.m_Music=nil
            LuaMeiXiangLouMgr.m_LoseFocusTimeStamp = CServerTimeMgr.Inst.timeStamp
        else
            LuaMeiXiangLouMgr.m_GetFocusTimeStamp = CServerTimeMgr.Inst.timeStamp
            LuaMeiXiangLouMgr.m_LastTimePosition = LuaMeiXiangLouMgr.m_LastTimePosition + (LuaMeiXiangLouMgr.m_GetFocusTimeStamp - LuaMeiXiangLouMgr.m_LoseFocusTimeStamp)
        end
    end
end

function LuaMusicPlayHitingWnd:InitTopMusicInfo()
    local songId = LuaMeiXiangLouMgr.m_CurPlaySongId
    local data = MeiXiangLou_SongInfo.GetData(songId)
    self.SongName.text = data.Name
    local difName = data.Difficulty
    if difName == LocalString.GetString("简单") then
        difName = LocalString.GetString("入门")
    elseif difName == LocalString.GetString("中等") then
        difName = LocalString.GetString("高手")
    elseif difName == LocalString.GetString("困难") then
        difName = LocalString.GetString("挑战")
    end
    local playerNumStr = data.OperatorNumber == 1 and LocalString.GetString("单人") or LocalString.GetString("双人")
    self.PlayerNumLabel.text = difName.."--"..playerNumStr

    self.TitleLabel.text = nil
    self.HitEffectLabel.text = nil
    self.ComboNum.text = 0

    self.TipMsgLabel.transform.parent.gameObject:SetActive(data.OperatorNumber == 2)
    if data.OperatorNumber == 2 then
        local colorName = LuaMeiXiangLouMgr.m_IsLeader and LocalString.GetString("紫色") or LocalString.GetString("蓝色")
        local colorStr = LuaMeiXiangLouMgr.m_IsLeader and "e05dfd" or "1ec9fb"
        self.TipMsgLabel.text = SafeStringFormat3(LocalString.GetString("只需点击[%s]%s[-]的按钮"),colorStr,colorName)
    end

end

function LuaMusicPlayHitingWnd:OnSongBeatArrived(songId, rhythmIdx, rhythmId)
    local ryhData = MeiXiangLou_Song.GetData(rhythmIdx)
    if ryhData then
        local ss = ryhData.Buttons
        local btn = {}
        local pressTime = {}
        local operator = ryhData.Operator
        for cStr in string.gmatch(ss, "([^;]+)") do
            for btnzhStr in string.gmatch(cStr, "([^,]+)") do
                local sbtn = tonumber(btnzhStr)
                if sbtn then
                    local btnnode = self.m_HitNodeTable[sbtn]
                    --print(sbtn,btnnode)
                    if btnnode then
                        btnnode:Show(operator,0,sbtn,rhythmIdx)     
                    end             
                else
                    for btnIdx, longPreesTime in string.gmatch(btnzhStr, "(%d+)%(([^%s]-)%)") do
                        local btnnode = self.m_HitNodeTable[tonumber(btnIdx)]
                        if btnnode then
                            btnnode:Show(operator,tonumber(longPreesTime),tonumber(btnIdx),rhythmIdx)
                        end
                    end
                end
                
            end   
        end
    
    end
end

function LuaMusicPlayHitingWnd:OnServerSongBeatArrived(songId, rhythmIdx, rhythmId,timePosition)
    self:InitLeftTime(timePosition)
    self.m_HittingSongId = songId
end

function LuaMusicPlayHitingWnd:OnSyncSongPlayPressButton(playerId, buttonIdx, perform, score)
    perform = perform + 1

    if playerId ~= CClientMainPlayer.Inst.Id then
        self.Score2.text = score    
    else
        self.Score1.text = score
    end

    if playerId == CClientMainPlayer.Inst.Id then
        if perform == EnumButtonPressPerform.Miss or perform == EnumButtonPressPerform.Ignore then
            LuaMeiXiangLouMgr.m_SingleCombo = 0
            LuaMeiXiangLouMgr.m_MyPerfectCombo = false
        else
            LuaMeiXiangLouMgr.m_SingleCombo = LuaMeiXiangLouMgr.m_SingleCombo + 1
        end
        self.ComboNum.text = SafeStringFormat3(LocalString.GetString("连击 %d"),LuaMeiXiangLouMgr.m_SingleCombo)
    elseif perform == EnumButtonPressPerform.Miss or perform == EnumButtonPressPerform.Ignore then 
        LuaMeiXiangLouMgr.m_PartnerPerfectCombo = false
    end

    local effectStr = self.m_EffectStr[perform]
    local colorstr = LuaMeiXiangLouMgr.m_PerpormColor[perform]
    self.HitEffectLabel.text = SafeStringFormat3("[%s]%s[-]",colorstr,effectStr)--effectStr
    self.m_EffectTween.enabled = true
    self.m_EffectTween:ResetToBeginning()
    self.m_EffectTween:PlayForward()

    if buttonIdx > 100 then
        buttonIdx = buttonIdx - 100
    end
    local hitnode = self.m_HitNodeTable[tonumber(buttonIdx)]
    if hitnode then
        hitnode:ShowPerformEffect(perform)
    end

end

function LuaMusicPlayHitingWnd:OnFmodMarkerTrigger(params)
    local path = string.lower(params[0])
    local name = params[1]
    local position = tonumber(params[2])/1000
    local mark = name
    
    if not self.m_HittingEvenet or string.lower(self.m_HittingEvenet) ~= path then
        return
    end
    local info = LuaMeiXiangLouMgr.m_MusicMarkerData[mark]
    
    if info then
        self:OnSongBeatArrived(self.m_HittingSongId, info.rhythmIdx, 0)
        --判断结束
        local songInfo = MeiXiangLou_SongInfo.GetData(self.m_HittingSongId)
        local data = MeiXiangLou_Song.GetData(info.rhythmIdx)
        LuaMeiXiangLouMgr.m_LastTimePosition = data.Delay

        if (songInfo  or self.m_LeftTime == 0) and (LuaMeiXiangLouMgr.m_TwoPlayerResultData or LuaMeiXiangLouMgr.m_SingleResultData)then
            local endIdx = songInfo.EndId
            if info.rhythmIdx >= endIdx then
                CUIManager.ShowUI(CLuaUIResources.MusicPlayHitResultWnd)
                CUIManager.CloseUI(CLuaUIResources.MusicPlayHitingWnd)
            end 
        end
        
    end

    if self.m_LeftTime == 0 then
        self:InitLeftTime(position)
    end
end

function LuaMusicPlayHitingWnd:InitLeftTime(position)
    self.ScoreLabel.text = nil
    if not LuaMeiXiangLouMgr.m_Music then
        return
    end
    local length = SoundManager.Inst:GetEventLength(LuaMeiXiangLouMgr.m_Music)
    length = length / 1000
    
    local left = length - position
    if left<0 then
        left = 0
    end
    if self.m_LeftTime == 0 and left > 0 and not self.m_LeftTimeCountDownTick then
        self.m_LeftTime = left
        self.ComboNum.text = SafeStringFormat3(LocalString.GetString("连击 %d"),LuaMeiXiangLouMgr.m_SingleCombo)
        self.m_LeftTimeCountDownTick = RegisterTick(function()
            self.m_LeftTime = self.m_LeftTime - 1
            if self.m_LeftTime < 0 then
                self.m_LeftTime = 0
                if (LuaMeiXiangLouMgr.m_TwoPlayerResultData or LuaMeiXiangLouMgr.m_SingleResultData) then
                    CUIManager.ShowUI(CLuaUIResources.MusicPlayHitResultWnd)
                    CUIManager.CloseUI(CLuaUIResources.MusicPlayHitingWnd)
                end
            end
            self.ScoreLabel.text = cs_string.Format(LocalString.GetString("[ACF9FF]{0:00}:{1:00}[-]"), math.floor(self.m_LeftTime / 60), self.m_LeftTime % 60)
        end,1000)
    end
    
    self.ScoreLabel.text = cs_string.Format(LocalString.GetString("[ACF9FF]{0:00}:{1:00}[-]"), math.floor(left / 60), left % 60)

    --team
    if self.m_HittingSongId then
        local data = MeiXiangLou_SongInfo.GetData(self.m_HittingSongId)
        if data.OperatorNumber == 2 then
            local myPlayer = CClientMainPlayer.Inst
            self.PlayerName1.text = myPlayer.RealName
            local color1 = LuaMeiXiangLouMgr.m_IsLeader and NGUIText.ParseColor24("e05dfd", 0) or NGUIText.ParseColor24("1ec9fb", 0)
            local colorName = LuaMeiXiangLouMgr.m_IsLeader and LocalString.GetString("紫色") or LocalString.GetString("蓝色")
            local color2 = not LuaMeiXiangLouMgr.m_IsLeader and NGUIText.ParseColor24("e05dfd", 0) or NGUIText.ParseColor24("1ec9fb", 0)
            self.PlayerName1.color = color1
            self.PlayerName2.color = color2
            self.Score1.color = color1
            self.Score2.color = color2
            local colorStr = LuaMeiXiangLouMgr.m_IsLeader and "e05dfd" or "1ec9fb"
            self.TipMsgLabel.text = SafeStringFormat3(LocalString.GetString("只需点击[%s]%s[-]的按钮"),colorStr,colorName)
        else
            self.PlayerName1.text = nil
            self.PlayerName2.text = nil
            self.Score1.text = nil
            self.Score2.text = nil
            self.Score1.color = NGUIText.ParseColor24("e05dfd", 0)
            self.PlayerName1.color = NGUIText.ParseColor24("e05dfd", 0)
            self.TipMsgLabel.text = nil
        end 
        self:RefreshTeamMember()
    end
end

function LuaMusicPlayHitingWnd:OnMainPlayCreated()
    local gamePlayId=0
    if CScene.MainScene then
        gamePlayId=CScene.MainScene.GamePlayDesignId
    end
    local songPlayId = MeiXiangLou_Setting.GetData().SongPlayId
    
    if gamePlayId ~= songPlayId then
        CUIManager.CloseUI(CLuaUIResources.MusicPlayHitingWnd)
    end
end
--@region UIEvent

function LuaMusicPlayHitingWnd:OnCloseButtonClick()
    local gamePlayId=0
    if CScene.MainScene then
        gamePlayId=CScene.MainScene.GamePlayDesignId
    end
    local msg = g_MessageMgr:FormatMessage("MeiXiangLou_Music_Quite_Comfire")
    if gamePlayId ~= MeiXiangLou_Setting.GetData().SongPlayId then
        msg = g_MessageMgr:FormatMessage("MeiXiangLou_Music_Quite_Comfire_Task")
    end
    MessageWndManager.ShowOKCancelMessage(msg, 
        DelegateFactory.Action(function ()
            CGamePlayMgr.Inst:LeavePlay()
        end),
        nil,
        LocalString.GetString("确定"), LocalString.GetString("取消"), false)
end


--@endregion UIEvent

