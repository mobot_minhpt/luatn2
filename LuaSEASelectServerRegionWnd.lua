local QnTabView = import "L10.UI.QnTabView"

local UILabel = import "UILabel"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CLoginMgr = import "L10.Game.CLoginMgr"

LuaSEASelectServerRegionWnd = class()
LuaSEASelectServerRegionWnd.m_SelectSEARegion = true
--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSEASelectServerRegionWnd, "OpBtn", "OpBtn", GameObject)
RegistChildComponent(LuaSEASelectServerRegionWnd, "SEAReion", "SEAReion", GameObject)
RegistChildComponent(LuaSEASelectServerRegionWnd, "VNRegion", "VNRegion", GameObject)
RegistChildComponent(LuaSEASelectServerRegionWnd, "TipLab", "TipLab", UILabel)
RegistChildComponent(LuaSEASelectServerRegionWnd, "Tab", "Tab", QnTabView)

--@endregion RegistChildComponent end

function LuaSEASelectServerRegionWnd:Awake()
    self.m_SelectSEARegion = not CommonDefs.IS_SEA_SELECT_VN_REGIONS
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    UIEventListener.Get(self.SEAReion).onClick = DelegateFactory.VoidDelegate(function (go)
        self:SelectRegion(true)
    end)

    UIEventListener.Get(self.VNRegion).onClick = DelegateFactory.VoidDelegate(function (go)
        self:SelectRegion(false)
    end)
    
    UIEventListener.Get(self.OpBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        CommonDefs.IS_SEA_SELECT_VN_REGIONS = not self.m_SelectSEARegion
        CUIManager.CloseUI(CLuaUIResources.SEASelectServerRegionWnd)
        CLoginMgr.Inst.m_SelectedGameServer = nil
        EventManager.BroadcastInternalForLua(EnumEventType.ChangeSEARegion,{})
    end)
end

function LuaSEASelectServerRegionWnd:Init()
    if self.m_SelectSEARegion then
        --预选SEA选项
        self.Tab:ChangeTo(0)
        self:SelectRegion(true)
    else    
        --预选VN选项
        self.Tab:ChangeTo(1)
        self:SelectRegion(false)
    end
end

--@region UIEvent

--@endregion UIEvent

function LuaSEASelectServerRegionWnd:SelectRegion(isSEARegion)
    self.m_SelectSEARegion = isSEARegion
    self.TipLab.text = isSEARegion and g_MessageMgr:FormatMessage("SEA_SELECT_SEA_REGION_TIP") or g_MessageMgr:FormatMessage("SEA_SELECT_VN_REGION_TIP")
end

