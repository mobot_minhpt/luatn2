local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CMainCamera = import "L10.Engine.CMainCamera"
local CPostEffect = import "L10.Engine.CPostEffect"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CUIFx = import "L10.UI.CUIFx"

CLuaUITriggerWnd = class()
CLuaUITriggerWnd.s_TriggerId = 0

RegistClassMember(CLuaUITriggerWnd, "m_ClickButton")
RegistClassMember(CLuaUITriggerWnd, "m_UIFx")
RegistClassMember(CLuaUITriggerWnd, "m_TriggerId")
RegistClassMember(CLuaUITriggerWnd, "m_TriggerTick")

function CLuaUITriggerWnd:Awake()
	self.m_ClickButton = self.transform:Find("Click/Button").gameObject
	CommonDefs.AddOnClickListener(self.m_ClickButton, DelegateFactory.Action_GameObject(function(go)
		self:OnButtonClicked(go)
	end), false)
	self.m_UIFx = self.transform:Find("Click/Fx"):GetComponent(typeof(CUIFx))
	self.m_TriggerId = CLuaUITriggerWnd.s_TriggerId

	if CClientMainPlayer.Inst then
		local worldPos = CClientMainPlayer.Inst.RO.Position
		local screenPos = CMainCamera.Main:WorldToScreenPoint(worldPos)
		self.m_ClickButton.transform.position = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)
	end

	local data = ZhuJueJuQing_UITrigger.GetData(self.m_TriggerId)
	if data then
		self.transform:Find("Click/Button/Guide"):GetComponent(typeof(CUITexture)):LoadMaterial(data.Icon)
		self.m_ClickButton.transform:GetComponent(typeof(UISprite)).color  = NGUIText.ParseColor24(data.CircleColor, 0)
	end
end

function CLuaUITriggerWnd:Init()
	local bgTex = self.transform:Find("_BgMask_"):GetComponent(typeof(UITexture))
	bgTex.color = Color(1, 1, 1, 0.5)
end

function CLuaUITriggerWnd:Start()
	self.m_UIFx:LoadFx(CLuaUIFxPaths.QTESingleClickFx)
end

function CLuaUITriggerWnd:OnDestroy()
	self:ClearTick()
end

function CLuaUITriggerWnd:ClearTick()
	if self.m_TriggerTick then
		UnRegisterTick(self.m_TriggerTick)
		self.m_TriggerTick = nil
	end
end

function CLuaUITriggerWnd:OnButtonClicked(go)
	self.m_ClickButton:SetActive(false)
	self.m_UIFx:DestroyFx()

	local data = ZhuJueJuQing_UITrigger.GetData(self.m_TriggerId)
	local finishTime = 3
	local closeTime = 3
	if data then
		self.m_UIFx:LoadFx(data.Fx)
		finishTime = data.FinishTime
		closeTime = data.FxDuration
	else
		CPostEffect.EnableWaterWaveMat(false)
	end

	self:ClearTick()

	local waitCloseFunc = function()
		Gac2Gas.FinishUITrigger(self.m_TriggerId)

		self:ClearTick()
		if closeTime - finishTime > 0 then
			self.m_TriggerTick = RegisterTickOnce(function()
				CUIManager.CloseUI(CLuaUIResources.UITriggerWnd)
			end, (closeTime - finishTime) * 1000)
		else
			CUIManager.CloseUI(CLuaUIResources.UITriggerWnd)
		end
	end

	if finishTime > 0 then
		self.m_TriggerTick = RegisterTickOnce(function()
			waitCloseFunc()
		end, finishTime * 1000)
	else
		waitCloseFunc()
	end
end

