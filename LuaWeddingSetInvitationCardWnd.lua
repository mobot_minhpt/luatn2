local UITexture        = import "UITexture"
local UIInput          = import "UIInput"
local CommonDefs       = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel          = import "UILabel"
local GameObject       = import "UnityEngine.GameObject"
local UIGrid           = import "UIGrid"
local QnCheckBox       = import "L10.UI.QnCheckBox"
local CWordFilterMgr   = import "L10.Game.CWordFilterMgr"
local EnumGender       = import "L10.Game.EnumGender"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local EnumMoneyType    = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"

local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local CPropertyAppearance          = import "L10.Game.CPropertyAppearance"
local CPropertySkill               = import "L10.Game.CPropertySkill"

LuaWeddingSetInvitationCardWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaWeddingSetInvitationCardWnd, "RecieverName", "RecieverName", UILabel)
RegistChildComponent(LuaWeddingSetInvitationCardWnd, "GroomName", "GroomName", UILabel)
RegistChildComponent(LuaWeddingSetInvitationCardWnd, "BrideName", "BrideName", UILabel)
RegistChildComponent(LuaWeddingSetInvitationCardWnd, "SignLabel", "SignLabel", UILabel)
RegistChildComponent(LuaWeddingSetInvitationCardWnd, "FriendButton", "FriendButton", GameObject)
RegistChildComponent(LuaWeddingSetInvitationCardWnd, "WorldButton", "WorldButton", GameObject)
RegistChildComponent(LuaWeddingSetInvitationCardWnd, "ActionTemplate", "ActionTemplate", GameObject)
RegistChildComponent(LuaWeddingSetInvitationCardWnd, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaWeddingSetInvitationCardWnd, "Input", "Input", UIInput)
RegistChildComponent(LuaWeddingSetInvitationCardWnd, "FriendLabel", "FriendLabel", GameObject)
RegistChildComponent(LuaWeddingSetInvitationCardWnd, "WorldLabel", "WorldLabel", UILabel)
RegistChildComponent(LuaWeddingSetInvitationCardWnd, "SendButton", "SendButton", GameObject)
RegistChildComponent(LuaWeddingSetInvitationCardWnd, "GroomModel", "GroomModel", UITexture)
RegistChildComponent(LuaWeddingSetInvitationCardWnd, "BrideModel", "BrideModel", UITexture)

--@endregion RegistChildComponent end

RegistClassMember(LuaWeddingSetInvitationCardWnd, "moneyCtrl")
RegistClassMember(LuaWeddingSetInvitationCardWnd, "closeButton")

RegistClassMember(LuaWeddingSetInvitationCardWnd, "curClass")
RegistClassMember(LuaWeddingSetInvitationCardWnd, "classButtons")
RegistClassMember(LuaWeddingSetInvitationCardWnd, "actionIds")
RegistClassMember(LuaWeddingSetInvitationCardWnd, "curActionId")
RegistClassMember(LuaWeddingSetInvitationCardWnd, "checkBoxs")
RegistClassMember(LuaWeddingSetInvitationCardWnd, "invitationType")
RegistClassMember(LuaWeddingSetInvitationCardWnd, "costs")
RegistClassMember(LuaWeddingSetInvitationCardWnd, "silverCost")

RegistClassMember(LuaWeddingSetInvitationCardWnd, "groomIdentifier")
RegistClassMember(LuaWeddingSetInvitationCardWnd, "brideIdentifier")
RegistClassMember(LuaWeddingSetInvitationCardWnd, "groomRO")
RegistClassMember(LuaWeddingSetInvitationCardWnd, "brideRO")

function LuaWeddingSetInvitationCardWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.FriendButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnFriendButtonClick()
	end)

	UIEventListener.Get(self.WorldButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnWorldButtonClick()
	end)

	UIEventListener.Get(self.SendButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSendButtonClick()
	end)

    --@endregion EventBind end
	self:InitUIComponents()
	self:InitActive()
end

-- 初始化UI组件
function LuaWeddingSetInvitationCardWnd:InitUIComponents()
	self.classButtons = {}
	local friendTrans = self.FriendButton.transform
	local friendNormal = friendTrans:Find("Sprite").gameObject
	local friendSelected = friendTrans:Find("Selected").gameObject
	table.insert(self.classButtons, {normal = friendNormal, selected = friendSelected})

	local worldTrans = self.WorldButton.transform
	local worldNormal = worldTrans:Find("Sprite").gameObject
	local worldSelected = worldTrans:Find("Selected").gameObject
	table.insert(self.classButtons, {normal = worldNormal, selected = worldSelected})

	self.GroomName.text = ""
	self.BrideName.text = ""

	self.moneyCtrl = self.transform:Find("Anchor"):Find("SettingWnd"):Find("QnCostAndOwnMoney"):GetComponent(typeof(CCurentMoneyCtrl))

	self.closeButton = self.transform:Find("Bg"):Find("CloseButton").gameObject

	UIEventListener.Get(self.closeButton).onClick = DelegateFactory.VoidDelegate(function (go)
		LuaWeddingIterationMgr:CloseSetInvitationCardWnd()
	end)
end

function LuaWeddingSetInvitationCardWnd:InitActive()
	self.ActionTemplate:SetActive(false)
end


function LuaWeddingSetInvitationCardWnd:OnEnable()
	g_ScriptEvent:AddListener("SyncNewWeddingPartner", self, "OnSyncNewWeddingPartner")
end

function LuaWeddingSetInvitationCardWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SyncNewWeddingPartner", self, "OnSyncNewWeddingPartner")
end

function LuaWeddingSetInvitationCardWnd:OnDestroy()
	CUIManager.DestroyModelTexture(self.groomIdentifier)
	CUIManager.DestroyModelTexture(self.brideIdentifier)
end

-- 同步伴侣信息
function LuaWeddingSetInvitationCardWnd:OnSyncNewWeddingPartner(partnerName)
	local gender = CClientMainPlayer.Inst.Gender
	local name = CClientMainPlayer.Inst.Name
	if gender == EnumGender.Male then
		self.GroomName.text = name
		self.BrideName.text = partnerName
	else
		self.GroomName.text = partnerName
		self.BrideName.text = name
	end

	self:InitModel()
	self:PlayAction(self.curActionId)
end


function LuaWeddingSetInvitationCardWnd:Init()
	self:SyncPartnerInfo()

	self:InitClassMembers()

	self:UpdateClass()
	self:InitAction()
	self:InitInput()
	self:InitCheckBox()
	self:InitWorldLabel()

	self:UpdateMoneyType()
	self:UpdateCost()
	self:InitBgFx()
end

function LuaWeddingSetInvitationCardWnd:SyncPartnerInfo()
	Gac2Gas.QueryNewWeddingPartner()
end

function LuaWeddingSetInvitationCardWnd:InitClassMembers()
	local cachedInfo = LuaWeddingIterationMgr.cachedInvitationInfo
	if not cachedInfo.class then cachedInfo.class = 1 end
	self.curClass = cachedInfo.class
end

-- 初始化人物模型
function LuaWeddingSetInvitationCardWnd:InitModel()
	local player = CClientMainPlayer.Inst
	local partnerInfo = LuaWeddingIterationMgr.partnerInfo
	local groomClass = player.Gender == EnumGender.Male and player.Class or partnerInfo.class
	local brideClass = player.Gender == EnumGender.Male and partnerInfo.class or player.Class

	self.groomIdentifier = SafeStringFormat3("__%s__", tostring(self.GroomModel.gameObject:GetInstanceID()))
	self.brideIdentifier = SafeStringFormat3("__%s__", tostring(self.BrideModel.gameObject:GetInstanceID()))

	self:CreateModel(self.groomIdentifier, self.GroomModel, EnumGender.Male, groomClass)
	self:CreateModel(self.brideIdentifier, self.BrideModel, EnumGender.Female, brideClass)
end

-- 创建人物模型
function LuaWeddingSetInvitationCardWnd:CreateModel(identifier, tex, gender, class)
    local loader = self:GetModelLoader(class, gender)

    tex.mainTexture = CUIManager.CreateModelTexture(identifier, loader,
        180, 0.05, -1, 4.66, false, true, 1.0, false, false)
end

-- 生成外观
function LuaWeddingSetInvitationCardWnd:GetModelLoader(class, gender)
    return LuaDefaultModelTextureLoader.Create(function (ro)
        local appearanceData = CPropertyAppearance.GetDataFromCustom(class, gender,
            CPropertySkill.GetDefaultYingLingStateByGender(gender), nil, nil, 0, 0, 0, 0,
            WeddingIteration_Setting.GetData().WeddingDressHeadId,
            WeddingIteration_Setting.GetData().WeddingDressBodyId,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, nil, nil, nil, nil, 0, 0, 0, 0, 0, 0, nil, 0)

        CClientMainPlayer.LoadResource(ro, appearanceData, true, 1.0,
            0, false, 0, false, 0, false, nil, false)

        if gender == EnumGender.Male then
            self.groomRO = ro
        else
            self.brideRO = ro
        end
    end)
end

-- 更新类别
function LuaWeddingSetInvitationCardWnd:UpdateClass()
	for i = 1, 2 do
		if i == self.curClass then
			self.classButtons[i].normal:SetActive(false)
			self.classButtons[i].selected:SetActive(true)
		else
			self.classButtons[i].normal:SetActive(true)
			self.classButtons[i].selected:SetActive(false)
		end
	end

	if self.curClass == 1 then
		self.FriendLabel:SetActive(true)
		self.WorldLabel.gameObject:SetActive(false)
	else
		self.FriendLabel:SetActive(false)
		self.WorldLabel.gameObject:SetActive(true)
	end
end

-- 初始化人物动作
function LuaWeddingSetInvitationCardWnd:InitAction()
	self.actionIds = LuaWeddingIterationMgr:GetActionIds()

	for i = 1, #self.actionIds do
		local id = self.actionIds[i]
		local data = Expression_Show.GetData(id)

		local actionGo = NGUITools.AddChild(self.Grid.gameObject, self.ActionTemplate)
		local trans = actionGo.transform
		trans:Find("Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(data.Icon)
		trans:Find("Label"):GetComponent(typeof(UILabel)).text = data.ExpName
		trans:Find("Selected").gameObject:SetActive(false)

		UIEventListener.Get(actionGo).onClick = DelegateFactory.VoidDelegate(function (go)
			self:UpdateAction(i)
		end)

		actionGo:SetActive(true)
	end

	self.Grid:Reposition()

	local cachedInfo = LuaWeddingIterationMgr.cachedInvitationInfo
	if not cachedInfo.actionId then cachedInfo.actionId = 1 end
	self:UpdateAction(cachedInfo.actionId)
end

-- 更新动作
function LuaWeddingSetInvitationCardWnd:UpdateAction(i)
	self:PlayAction(i)
	if self.curActionId == i then return end

	local gridTrans = self.Grid.transform
	gridTrans:GetChild(i - 1):Find("Selected").gameObject:SetActive(true)
	if self.curActionId then
		gridTrans:GetChild(self.curActionId - 1):Find("Selected").gameObject:SetActive(false)
	end

	self.curActionId = i
	LuaWeddingIterationMgr.cachedInvitationInfo.actionId = i
end

-- 播放动作
function LuaWeddingSetInvitationCardWnd:PlayAction(i)
	LuaWeddingIterationMgr:PlayActionInInvitationCard(self.groomRO, self.brideRO, self.groomIdentifier, self.brideIdentifier, i)
end

-- 初始化输入
function LuaWeddingSetInvitationCardWnd:InitInput()
	local cachedInfo = LuaWeddingIterationMgr.cachedInvitationInfo
	self.Input.characterLimit = CommonDefs.IS_VN_CLIENT and 15 or 7
	if cachedInfo.sign then
		self.Input.value = cachedInfo.sign
	else
		self.Input.value = ""
	end

	self.SignLabel.text = SafeStringFormat3(LocalString.GetString("%s 敬邀"), self.Input.value) 
    CommonDefs.AddEventDelegate(self.Input.onChange, DelegateFactory.Action(function ( ... )
        self.SignLabel.text = self.Input.value
		cachedInfo.sign = self.Input.value
    end))
end

-- 初始化check box
function LuaWeddingSetInvitationCardWnd:InitCheckBox()
	self.invitationType = {
		eGuild = 1,
		eSect = 2,
		eAllFriend = 3,
		eWorld = 4,
	}

	self.checkBoxs = {}

	local trans = self.FriendLabel.transform
	local cachedInfo = LuaWeddingIterationMgr.cachedInvitationInfo
	if not cachedInfo.selected then cachedInfo.selected = {false, false, false} end

	self:InitOneCheckBox(self.invitationType.eGuild, trans, "GuildBox")
	self:InitOneCheckBox(self.invitationType.eAllFriend, trans, "AllFriendBox")
	self:InitOneCheckBox(self.invitationType.eSect, trans, "SectBox")

	self.costs = {}
	self.costs[self.invitationType.eGuild] = WeddingIteration_Setting.GetData().WeddingInviteGuildPrice
	self.costs[self.invitationType.eSect] = WeddingIteration_Setting.GetData().WeddingInviteSectPrice
	self.costs[self.invitationType.eAllFriend] = WeddingIteration_Setting.GetData().WeddingInviteFriendPrice
	local worldId = self.invitationType.eWorld
	self.costs[worldId] = WeddingIteration_Setting.GetData().WeddingInviteWorldPrice
end

-- 初始化一个checkbox
function LuaWeddingSetInvitationCardWnd:InitOneCheckBox(id, trans, name)
	local cachedInfo = LuaWeddingIterationMgr.cachedInvitationInfo

	self.checkBoxs[id] = trans:Find(name):GetComponent(typeof(QnCheckBox))
	self.checkBoxs[id].Selected = cachedInfo.selected[id]
	self.checkBoxs[id].OnValueChanged = DelegateFactory.Action_bool(function(select)
		cachedInfo.selected[id] = select
		self:UpdateCost()
    end)
end

-- 更新消耗
function LuaWeddingSetInvitationCardWnd:UpdateCost()
	if self.curClass == 1 then
		local cost = 0
		for id, checkBox in pairs(self.checkBoxs) do
			if checkBox.Selected then
				cost = cost + self.costs[id]
			end
		end
		self.silverCost = cost
		self.moneyCtrl:SetCost(cost)
	else
		self.moneyCtrl:SetCost(self.costs[self.invitationType.eWorld])
	end
end

-- 更新拥有
function LuaWeddingSetInvitationCardWnd:UpdateMoneyType()
	if self.curClass == 1 then
		self.moneyCtrl:SetType(EnumMoneyType.YinLiang, EnumPlayScoreKey.NONE, true)
	else
		self.moneyCtrl:SetType(EnumMoneyType.LingYu, EnumPlayScoreKey.NONE, true)
	end
end

-- 更换熟人或世界
function LuaWeddingSetInvitationCardWnd:ChangeClass(class)
	if self.curClass ~= class then
		self.curClass = class
		LuaWeddingIterationMgr.cachedInvitationInfo.class = class
		self:UpdateClass()
		self:UpdateMoneyType()
		self:UpdateCost()
	end
end

-- 初始化WorldLabel
function LuaWeddingSetInvitationCardWnd:InitWorldLabel()
	self.WorldLabel.text = g_MessageMgr:FormatMessage("Wedding_World_Invitation_Description")
end

-- 加载背景特效
function LuaWeddingSetInvitationCardWnd:InitBgFx()
    local cuiFx = self.transform:Find("Bg"):Find("Fx"):GetComponent(typeof(CUIFx))
    cuiFx:LoadFx(g_UIFxPaths.WeddingQingJianFxPath)
end

--@region UIEvent

function LuaWeddingSetInvitationCardWnd:OnFriendButtonClick()
	self:ChangeClass(1)
end

function LuaWeddingSetInvitationCardWnd:OnWorldButtonClick()
	self:ChangeClass(2)
end

function LuaWeddingSetInvitationCardWnd:OnSendButtonClick()
	local tbl = {}
	if self.curClass == 1 then
		for id, checkBox in pairs(self.checkBoxs) do
			if checkBox.Selected then
				table.insert(tbl, id)
			end
		end

		if #tbl == 0 then
			g_MessageMgr:ShowMessage("WEDDING_INVITATION_CARD_NEED_SELECT_RECEIVER")
			return
		end
	else
		table.insert(tbl, self.invitationType.eWorld)
	end

	if not self.moneyCtrl.moneyEnough then
		g_MessageMgr:ShowMessage("INVITE_NOT_ENOUGH_MONEY")
		return
	end

	-- 滤除关键字
	local ret = CWordFilterMgr.Inst:DoFilterOnSend(self.Input.value, nil, nil, true)
    local msg = ret.msg

	if msg == nil or ret.shouldBeIgnore then
		return
	end

	LuaWeddingIterationMgr:SendWeddingInvitation(tbl, msg, self.curActionId)
end

--@endregion UIEvent

