local Ease = import "DG.Tweening.Ease"


LuaZhuErDanDaDiShuItem = class()
RegistClassMember(LuaZhuErDanDaDiShuItem, "m_LifeTime")

RegistClassMember(LuaZhuErDanDaDiShuItem, "m_ClickRegion")
RegistClassMember(LuaZhuErDanDaDiShuItem, "m_ShowTime")
RegistClassMember(LuaZhuErDanDaDiShuItem, "m_CurrentTime")
RegistClassMember(LuaZhuErDanDaDiShuItem, "m_Score")
RegistClassMember(LuaZhuErDanDaDiShuItem, "m_HitFx")
RegistClassMember(LuaZhuErDanDaDiShuItem, "m_Finished")
RegistClassMember(LuaZhuErDanDaDiShuItem, "m_Tick")
RegistClassMember(LuaZhuErDanDaDiShuItem, "m_CanHit")

--状态：隐藏，出现，被击中，

function LuaZhuErDanDaDiShuItem:Awake()
    self.m_CurrentTime = 0
    self.m_ShowTime = 9999

    self.transform.localPosition = Vector3(0,-480,0)

    self.m_HitFx = self.transform:Find("HitFx"):GetComponent(typeof(CUIFx))
	self.m_HitFx:DestroyFx()
    self.m_Finished = false
    self.m_CanHit = true
end


function LuaZhuErDanDaDiShuItem:Init()
    self.m_Score = 1
    self.m_ClickRegion = self.gameObject
    UIEventListener.Get(self.m_ClickRegion).onClick = DelegateFactory.VoidDelegate(function(go)
        if not self.m_CanHit then return end
        if self.m_Finished then
            return
        end
        LuaTweenUtils.DOKill(self.transform, false)

        g_ScriptEvent:BroadcastInLua("ZhuErDan_DaDiShu_AddScore",self.m_Score)
        self.m_CanHit = false
        self.m_HitFx:DestroyFx()
        self.m_HitFx:LoadFx("Fx/UI/Prefab/UI_jieyuanbao.prefab")

        if self.m_Tick then
            UnRegisterTick(self.m_Tick)
            self.m_Tick = nil
        end
        self.m_Tick = RegisterTickOnce(function()
            self:Hide()
            self.m_Tick = nil
        end,500)
    end)

    self.m_ShowTime = math.random()*10
    self.m_CurrentTime = 0
end

function LuaZhuErDanDaDiShuItem:Show()
    self.m_CanHit = true

    self.gameObject:SetActive(true)

    local showTween = LuaTweenUtils.SetEase(LuaTweenUtils.TweenPositionY(self.transform,-140,0.8),Ease.InOutSine)

    local hideTween = LuaTweenUtils.SetEase(LuaTweenUtils.TweenPositionY(self.transform,-480,0.8),Ease.InOutSine)

    LuaTweenUtils.SetDelay(hideTween,1)
end
function LuaZhuErDanDaDiShuItem:Hide()
    self.transform.localPosition = Vector3(0,-480,0)
    -- self.gameObject:SetActive(false)

    self.m_ShowTime = math.random()*10
    self.m_CurrentTime = 0
end

function LuaZhuErDanDaDiShuItem:Update()
    if self.m_Finished then return end
    self.m_CurrentTime = self.m_CurrentTime+Time.deltaTime

    if self.m_CurrentTime>self.m_ShowTime then
        self:Show()
        self.m_CurrentTime = 0
        self.m_ShowTime = math.random()*8+2
    end
end

function LuaZhuErDanDaDiShuItem:OnEnable()
    g_ScriptEvent:AddListener("ZhuErDan_DaDiShu_Finish", self, "OnGameFinish")
end
function LuaZhuErDanDaDiShuItem:OnDisable()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end

	g_ScriptEvent:RemoveListener("ZhuErDan_DaDiShu_Finish", self, "OnGameFinish")
end
function LuaZhuErDanDaDiShuItem:OnGameFinish()
    self.m_Finished = true
end