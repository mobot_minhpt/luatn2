-- Auto Generated!!
local CGuildLeagueBattleStateWnd = import "L10.UI.CGuildLeagueBattleStateWnd"
local CGuildLeagueMgr = import "L10.Game.CGuildLeagueMgr"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local Component = import "UnityEngine.Component"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local EventDelegate = import "EventDelegate"
local TweenColor = import "TweenColor"
local UIEventListener = import "UIEventListener"
local UITweener = import "UITweener"
local Vector3 = import "UnityEngine.Vector3"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
CGuildLeagueBattleStateWnd.m_Init_CS2LuaHook = function (this) 
    local homeHpInfo = LuaGuildLeagueMgr.m_HomeHpInfo
    if homeHpInfo==nil then
        this.slider1.value = 0
        this.slider2.value = 0
        this.expLabel1.text = ""
        this.expLabel2.text = ""
    --将军威
    elseif homeHpInfo.force1 == 1 then
        this.slider1.value = homeHpInfo.curHp1 / homeHpInfo.fullHp1
        this.slider2.value = homeHpInfo.curHp2 / homeHpInfo.fullHp2
        this.expLabel1.text = tostring(homeHpInfo.curHp1)
        this.expLabel2.text = tostring(homeHpInfo.curHp2)
    else
        this.slider1.value = homeHpInfo.curHp2 / homeHpInfo.fullHp2
        this.slider2.value = homeHpInfo.curHp1 / homeHpInfo.fullHp1
        this.expLabel1.text = tostring(homeHpInfo.curHp2)
        this.expLabel2.text = tostring(homeHpInfo.curHp1)
    end
    if CGuildLeagueMgr.Inst:GetMyForce() == 1 then
        this.mark1:SetActive(true)
        this.mark2:SetActive(false)
    else
        this.mark1:SetActive(false)
        this.mark2:SetActive(true)
    end
end
CGuildLeagueBattleStateWnd.m_Awake_CS2LuaHook = function (this) 
    UIEventListener.Get(this.expandButton).onClick = DelegateFactory.VoidDelegate(function (p) 
        this.expandButton.transform.localEulerAngles = Vector3(0, 0, 0)
        CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
    end)
    CommonDefs.ListAdd(this.slider1.onChange, typeof(EventDelegate), CreateFromClass(EventDelegate, DelegateFactory.Callback(function () 
        this:OnValueChange(this.slider1)
    end)))
    CommonDefs.ListAdd(this.slider2.onChange, typeof(EventDelegate), CreateFromClass(EventDelegate, DelegateFactory.Callback(function () 
        this:OnValueChange(this.slider2)
    end)))
    this.mark1:SetActive(false)
    this.mark2:SetActive(false)
end
CGuildLeagueBattleStateWnd.m_OnValueChange_CS2LuaHook = function (this, slider) 
    if slider.value > 0.5 then
        slider.foregroundWidget.color = Color.green
        this:SetBlink(slider, false)
    elseif slider.value > 0.1 then
        slider.foregroundWidget.color = Color.yellow
        this:SetBlink(slider, false)
    else
        slider.foregroundWidget.color = Color.red
        this:SetBlink(slider, true)
    end
end
CGuildLeagueBattleStateWnd.m_SetBlink_CS2LuaHook = function (this, slider, b) 
    local cmp = CommonDefs.GetComponent_GameObject_Type(slider.foregroundWidget.gameObject, typeof(TweenColor))
    if b then
        if cmp == nil then
            cmp = CommonDefs.AddComponent_GameObject_Type(slider.foregroundWidget.gameObject, typeof(TweenColor))
        end
        if cmp ~= nil then
            cmp.from = Color.red
            cmp.to = Color(1, 0, 0, 0)
            cmp.style = UITweener.Style.PingPong
            cmp.duration = 0.5
        end
    else
        if cmp ~= nil then
            Component.Destroy(cmp)
        end
    end
end

CGuildLeagueBattleStateWnd.m_OnEnable_CS2LuaHook = function (this)
    LuaGuildLeagueBattleStateWnd:OnEnable(this)
    g_ScriptEvent:AddListener("OnUpdateGuildLeagueHomeHp", LuaGuildLeagueBattleStateWnd, "OnUpdateGuildLeagueHomeHp")
    EventManager.AddListener(EnumEventType.HideTopAndRightTipWnd, MakeDelegateFromCSFunction(this.OnHideTopAndRightTipWnd, Action0, this))
end

CGuildLeagueBattleStateWnd.m_OnDisable_CS2LuaHook = function (this)
    LuaGuildLeagueBattleStateWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnUpdateGuildLeagueHomeHp", LuaGuildLeagueBattleStateWnd, "OnUpdateGuildLeagueHomeHp")
    EventManager.RemoveListener(EnumEventType.HideTopAndRightTipWnd, MakeDelegateFromCSFunction(this.OnHideTopAndRightTipWnd, Action0, this))
    --这个界面不显示的话，那么tip界面肯定也不显示
    if CUIManager.IsLoaded(CUIResources.TopAndRightTipWnd) then
        CUIManager.CloseUI(CUIResources.TopAndRightTipWnd)
    end
end

LuaGuildLeagueBattleStateWnd = {}

LuaGuildLeagueBattleStateWnd.m_Wnd = nil

function LuaGuildLeagueBattleStateWnd:OnEnable(this)
    self.m_Wnd = this
end

function LuaGuildLeagueBattleStateWnd:OnDisable()
    self.m_Wnd = nil
end

function LuaGuildLeagueBattleStateWnd:OnUpdateGuildLeagueHomeHp()
    self.m_Wnd:Init()
end


