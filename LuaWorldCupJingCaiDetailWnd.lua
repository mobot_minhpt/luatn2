require("common/common_include")

local UILabel = import "UILabel"
local UIScrollView = import "UIScrollView"
local MsgPackImpl = import "MsgPackImpl"
local LocalString = import "LocalString"
local Extensions = import "Extensions"
local UISprite = import "UISprite"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUITexture = import "L10.UI.CUITexture"

CLuaWorldCupJingCaiDetailWnd=class()
RegistClassMember(CLuaWorldCupJingCaiDetailWnd, "m_ScorllView")
RegistClassMember(CLuaWorldCupJingCaiDetailWnd, "m_Table")
RegistClassMember(CLuaWorldCupJingCaiDetailWnd, "m_ItemTemplate")

RegistClassMember(CLuaWorldCupJingCaiDetailWnd, "m_TitleLabel")

function CLuaWorldCupJingCaiDetailWnd:Init()
    self.m_ScorllView = FindChild(self.transform, "ScrollView"):GetComponent(typeof(UIScrollView))
    self.m_Table = FindChild(self.transform, "Table"):GetComponent(typeof(UITable))

    self.m_ItemTemplate = FindChild(self.transform, "PlayInfoItem").gameObject
    self.m_ItemTemplate:SetActive(false)

    self.m_TitleLabel = FindChild(self.transform, "TitleLabel"):GetComponent(typeof(UILabel))

    local pid = CLuaWorldCupMgr.m_Detail_Pid
    local resultData = CLuaWorldCupMgr.m_Detail_ResultData
    local value = CLuaWorldCupMgr.m_Detail_Value

    self:Refresh(pid, resultData, value)
end

function CLuaWorldCupJingCaiDetailWnd:Refresh(pid, resultData, value)
    -- 清空内容
    Extensions.RemoveAllChildren(self.m_Table.transform)

    self.m_TitleLabel.text = SafeStringFormat(LocalString.GetString("第%d期竞彩详情"), pid)

    local list = MsgPackImpl.unpack(resultData)

    local index = 1
    for i = 0, list.Count-1, 6 do
        local go = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_ItemTemplate)
        go:SetActive(true)

        local round = list[i]
        local tid1 = list[i+1]
        local tid2 = list[i+2]
        local score1 = list[i+3]
        local score2 = list[i+4]
        local dianqiu = list[i+5]

        local PlayInfo = CLuaWorldCupMgr.GetPlayInfoById(round)
        local PlayTime = go.transform:Find("PlayTime"):GetComponent(typeof(UILabel))
        PlayTime.text = LocalString.GetString(PlayInfo.Time)

        local PlayRound = go.transform:Find("PlayRound"):GetComponent(typeof(UILabel))
        PlayRound.text = LocalString.GetString(PlayInfo.RoundInfo)

        local TeamInfo1 = CLuaWorldCupMgr.GetTeamInfoById(tid1)
        local Team1Name = go.transform:Find("Team1Name"):GetComponent(typeof(UILabel))
        Team1Name.text = LocalString.GetString(TeamInfo1.Name)

        local TeamInfo2 = CLuaWorldCupMgr.GetTeamInfoById(tid2)
        local Team2Name = go.transform:Find("Team2Name"):GetComponent(typeof(UILabel))
        Team2Name.text = LocalString.GetString(TeamInfo2.Name)

        local Texture1 = go.transform:Find("Texture1"):GetComponent(typeof(CUITexture))
        Texture1:LoadMaterial(TeamInfo1.Icon)

        local Texture2 = go.transform:Find("Texture2"):GetComponent(typeof(CUITexture))
        Texture2:LoadMaterial(TeamInfo2.Icon)

        local DianQiu = go.transform:Find("DianQiu"):GetComponent(typeof(UISprite))
        if dianqiu == 1 then
            DianQiu.gameObject:SetActive(true)
        else
            DianQiu.gameObject:SetActive(false)
        end

        local vs1 = go.transform:Find("vs1"):GetComponent(typeof(UISprite))
        local vs2 = go.transform:Find("vs2"):GetComponent(typeof(UILabel))
        if score1 == -1 then
            vs1.gameObject:SetActive(true)
            vs2.gameObject:SetActive(false)
        else
            vs1.gameObject:SetActive(false)
            vs2.gameObject:SetActive(true)
            vs2.text = SafeStringFormat(LocalString.GetString("%s:%s"), score1, score2)
        end

        local MyJingCai = go.transform:Find("MyJingCai"):GetComponent(typeof(UILabel))
        local myVote = string.sub(value, index, index)
        myVote = tonumber(myVote)
        MyJingCai.text = CLuaWorldCupMgr.m_SelectDataTbl[myVote] or LocalString.GetString("未知")
        CLuaWorldCupMgr.HandleMyJingCaiTextColor(MyJingCai, score1, score2, myVote)

        index = index + 1
    end

    self.m_Table:Reposition()
    self.m_ScorllView:ResetPosition()
end

function CLuaWorldCupJingCaiDetailWnd:OnEnable()
end

function CLuaWorldCupJingCaiDetailWnd:OnDisable()
end

return CLuaWorldCupJingCaiDetailWnd
