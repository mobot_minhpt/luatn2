local CScene = import "L10.Game.CScene"
local AMPlayer = import "L10.Game.CutScene.AMPlayer"
local ETrackType = import "L10.Game.CutScene.ETrackType"
local CPropertyAppearance = import "L10.Game.CPropertyAppearance"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CClientOtherPlayer = import "L10.Game.CClientOtherPlayer"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local CCommonUIFxWnd = import "L10.UI.CCommonUIFxWnd"
local CSceneDynamicStuff = import "L10.Engine.Scene.CSceneDynamicStuff"

LuaHuanHunShanZhuangMgr = {}

function LuaHuanHunShanZhuangMgr:Init()
    g_ScriptEvent:AddListener("UpdateGamePlayStageInfo", self, "OnUpdateGamePlayStageInfo")
end
LuaHuanHunShanZhuangMgr:Init()

-- 是否在PVE副本
function LuaHuanHunShanZhuangMgr:IsInPlay()
    if CScene.MainScene then
        return CScene.MainScene.GamePlayDesignId == ShuJia2022_Setting.GetData().EasyPlayGameplayID or
            CScene.MainScene.GamePlayDesignId == ShuJia2022_Setting.GetData().HardGameplayID
    end
    return false
end

-- 是否在迷雾岭（除PVE副本）
function LuaHuanHunShanZhuangMgr:IsInMiWuLing()
    if CScene.MainScene then
        local id = CScene.MainScene.SceneTemplateId
        return id == 16000490 or id == 16102595 or id == 16102486 or id == 16102547 or id == 16102552 or id == 16102556 or id == 16102632
    end
    return false
end

function LuaHuanHunShanZhuangMgr:OnMainPlayerCreated()
    if self:IsInPlay() then
        CLuaScreenCaptureWnd.CameraFollowResetToDefaultOnDestroy = false
        --g_MessageMgr:ShowMessage("ShuJia2022_HuanHunShanZhuangNei_Tips")
        LuaCommonTextImageRuleMgr:ShowOnlyImageWnd(3)
    elseif self.m_WaitForShowResult then
        self.m_WaitForShowResult = false
        CUIManager.ShowUI(CLuaUIResources.HuanHunShanZhuangResultWnd)
    end
end

function LuaHuanHunShanZhuangMgr:OnMainPlayerDestroyed()
    CLuaScreenCaptureWnd.CameraFollowResetToDefaultOnDestroy = true

    self.m_TaskStageGamePlayId = nil
    self.m_TaskSategTitle = nil
    self.m_TaskContent = nil

    self.m_TopAndRightWndContent = nil
end

LuaHuanHunShanZhuangMgr.m_TopAndRightWndContent = nil
function LuaHuanHunShanZhuangMgr:ShowTopRightWnd(wnd)
    if self:IsInPlay() then 
        self.m_TopAndRightWndContent = wnd.contentNode
        self:UpdateTopRightWnd()
    else
        CUIManager.CloseUI(CLuaUIResources.HuanHunShanZhuangTopRightWnd)
    end
    --LuaTopAndRightMenuWndMgr:ShowTopRightWnd(wnd, CLuaUIResources.HuanHunShanZhuangTopRightWnd, function() return self:IsInPlay() end)
end

LuaHuanHunShanZhuangMgr.m_TaskStageGamePlayId = nil
LuaHuanHunShanZhuangMgr.m_TaskSategTitle = nil
LuaHuanHunShanZhuangMgr.m_TaskContent = nil
function LuaHuanHunShanZhuangMgr:OnUpdateGamePlayStageInfo(gameplayId, title, content)
    self.m_TaskStageGamePlayId = gameplayId
    self.m_TaskSategTitle = title
    self.m_TaskContent = content
end

function LuaHuanHunShanZhuangMgr:UpdateTopRightWnd()
    if self.m_TopAndRightWndContent then
        if self.m_Stage == 3 and self.m_IsShow or self.m_Stage == 1 then
            self.m_TopAndRightWndContent:SetActive(false)
            CUIManager.ShowUI(CLuaUIResources.HuanHunShanZhuangTopRightWnd)
        else
            self.m_TopAndRightWndContent:SetActive(true)
            CUIManager.CloseUI(CLuaUIResources.HuanHunShanZhuangTopRightWnd)
        end
    end
end

LuaHuanHunShanZhuangMgr.m_IsShow = true
LuaHuanHunShanZhuangMgr.m_Flags = {}
function LuaHuanHunShanZhuangMgr:SendHuanHunShanZhuangShengSiMen(isShow, flagData)
    --print("SendHuanHunShanZhuangShengSiMen", isShow)    
    self.m_IsShow = isShow
    self:UpdateTopRightWnd()

    local list = flagData.Length == 0 and {} or g_MessagePack.unpack(flagData)
    self.m_Flags = {}
    for i = 1, #list, 6 do
        local flagIdx = list[i] -- 1~8对应开、休、生、杜、景、死、惊、伤
        local flagEngineId = list[i+1]
        local posX = list[i+2]
        local posY = list[i+3]
        local isTrigger = list[i+4]
        local isFound = list[i+5]
        self.m_Flags[flagIdx] = {
            engineId = flagEngineId, x = posX, y = posY, isTrigger = isTrigger, isFound = isFound
        }
        --print("SendHuanHunShanZhuangShengSiMen", flagIdx, flagEngineId, posX, posY, isTrigger, isFound)
    end
    g_ScriptEvent:BroadcastInLua("SendHuanHunShanZhuangShengSiMen")
end

function LuaHuanHunShanZhuangMgr.PlayMultiPlayerVideo(videoName, onPlayStart, onPlayFinish)
    local dict = CreateFromClass(MakeGenericClass(Dictionary, ETrackType, CPropertyAppearance))
    if CClientMainPlayer.Inst then
        CommonDefs.DictAdd(dict, typeof(ETrackType), ETrackType.MainPlayer, typeof(CPropertyAppearance), CClientMainPlayer.Inst.AppearanceProp)
    end
    local trackType = {ETrackType.JieBai_1, ETrackType.JieBai_2, ETrackType.JieBai_3, ETrackType.JieBai_4}
    local idx = 1
    CClientObjectMgr.Inst:ForeachObject(DelegateFactory.Action_CClientObject(function(obj)
        if idx <= 4 and TypeIs(obj, typeof(CClientOtherPlayer)) then
            CommonDefs.DictAdd(dict, typeof(ETrackType), trackType[idx], typeof(CPropertyAppearance), obj.AppearanceProp)
            idx = idx + 1
        end
    end))
    AMPlayer.Inst:Play(videoName, onPlayStart and DelegateFactory.Action_string(onPlayStart), onPlayFinish and DelegateFactory.Action_string(onPlayFinish), dict);
end

LuaHuanHunShanZhuangMgr.m_Stage = 0
-- stage: 1第一关开始 2第一关结束 3第二关开始 4第二关结束 5boss战开始
-- stage=2,4,5时播动画，isShowFx=false时不播(比如断线重连时)
function LuaHuanHunShanZhuangMgr:SyncHuanHunShanZhuangPlayStageChange(stage, isShowFx) 
    print("SyncHuanHunShanZhuangPlayStageChange", stage, isShowFx)
    self.m_Stage = stage

    g_ScriptEvent:BroadcastInLua("SyncHuanHunShanZhuangPlayStageChange", stage, isShowFx)

    CLuaSceneMgr:ShowOrHideSceneObject(16102535, "Models/jz_shuqi_qiaoshen", true) -- 桥
    CLuaSceneMgr:ShowOrHideSceneObject(16102535, "Models/Others/jz_shuqi_001_019", true) -- 石柱1
    CLuaSceneMgr:ShowOrHideSceneObject(16102535, "Models/Others/jz_shuqi_001_019 (1)", true) -- 石柱2
    CLuaSceneMgr:ShowOrHideSceneObject(16102535, "OtherObjects/Fx/jz_shuqi_001_012_men", true) -- 门

    if stage == 1 or stage == 2 and isShowFx then
        local obj = CRenderScene.Inst.transform:Find("Models/Others/jz_shuqi_001_019")
        if obj then
            LuaUtils.SetLocalPositionY(obj:GetChild(0), -1.320999)
        end
        obj = CRenderScene.Inst.transform:Find("Models/Others/jz_shuqi_001_019 (1)")
        if obj then
            LuaUtils.SetLocalPositionY(obj:GetChild(0), -1.320999)
        end
        CLuaSceneMgr:ShowOrHideSceneObject(16102535, "PlaneClouds", true)
    else
        local obj = CRenderScene.Inst.transform:Find("Models/Others/jz_shuqi_001_019")
        if obj then
            LuaUtils.SetLocalPositionY(obj:GetChild(0), 0)
        end
        obj = CRenderScene.Inst.transform:Find("Models/Others/jz_shuqi_001_019 (1)")
        if obj then
            LuaUtils.SetLocalPositionY(obj:GetChild(0), 0)
        end
        CLuaSceneMgr:ShowOrHideSceneObject(16102535, "PlaneClouds", false)
    end

    -- 迷雾（PVE副本一定是白天）
    local dayStuffs = CRenderScene.Inst.transform:Find("Models/DayStuffs")
    dayStuffs = dayStuffs and dayStuffs:GetComponent(typeof(CSceneDynamicStuff))
    if dayStuffs then
        dayStuffs:ShowStuff(stage == 1 or stage == 2 and isShowFx, "Assets/Res/Fx/scene/Prefab/miwuling_pingzhang.prefab", "Frog")
    end

    if stage == 1 then
        self:UpdateTopRightWnd()
        CLuaSceneMgr:ShowOrHideSceneObject(16102535, "Models/jz_shuqi_qiaoshen", false)
    elseif stage == 2 then
        if isShowFx then
            CLuaSceneMgr:ShowOrHideSceneObject(16102535, "Models/jz_shuqi_qiaoshen", false)
            RegisterTickOnce(function()
                self.PlayMultiPlayerVideo("miwulin_dcy", 
                function(name)
                    CLuaSceneMgr:ShowOrHideSceneObject(16102535, "Models/jz_shuqi_qiaoshen", true)
                    if dayStuffs then
                        dayStuffs:ShowStuff(false, "Assets/Res/Fx/scene/Prefab/miwuling_pingzhang.prefab", "Frog")
                    end
                end, 
                function(name)
                    self:UpdateTopRightWnd()
                    local obj = CRenderScene.Inst.transform:Find("Models/jz_shuqi_qiaoshen")
                    if obj then
                        LuaUtils.SetLocalPosition(obj:GetChild(0), -12.24539, 7.383379, 0)
                        LuaUtils.SetLocalPosition(obj:GetChild(1), -8.418747, 7.411177, 0)
                        LuaUtils.SetLocalPosition(obj:GetChild(2), -3.263092, 7.448627, 0)
                        LuaUtils.SetLocalPosition(obj:GetChild(3), 2.030365, 7.487082, 0)
                        LuaUtils.SetLocalPosition(obj:GetChild(4), 7.110611, 7.523985, 0)
                        LuaUtils.SetLocalPosition(obj:GetChild(5), 10.99893, 7.552231, 0)
                        LuaUtils.SetLocalRotation(obj:GetChild(0), 0.4161987, -90, 0)
                        LuaUtils.SetLocalRotation(obj:GetChild(1), 0.4161987, -90, 0)
                        LuaUtils.SetLocalRotation(obj:GetChild(2), 0.4161987, -90, 0)
                        LuaUtils.SetLocalRotation(obj:GetChild(3), 0.4161987, -90, 0)
                        LuaUtils.SetLocalRotation(obj:GetChild(4), 0.4161987, -90, 0)
                        LuaUtils.SetLocalRotation(obj:GetChild(5), 0.4161987, -90, 0)
                    end
                    obj = CRenderScene.Inst.transform:Find("Models/Others/jz_shuqi_001_019")
                    if obj then
                        LuaUtils.SetLocalPositionY(obj:GetChild(0), 0)
                    end
                    obj = CRenderScene.Inst.transform:Find("Models/Others/jz_shuqi_001_019 (1)")
                    if obj then
                        LuaUtils.SetLocalPositionY(obj:GetChild(0), 0)
                    end
                end)
            end, 2000)
        else
            self:UpdateTopRightWnd()
        end
    elseif stage == 4 then
        if isShowFx then
            CCommonUIFxWnd.Instance:LoadUIFxAssignedPath("Fx/UI/Prefab/UI_shuqibaguazhen.prefab", 2)
            RegisterTickOnce(function()
                CCommonUIFxWnd.Instance:DestroyUIFxAssignedPath(2)
                self.PlayMultiPlayerVideo("miwuling_ffzdh", 
                function(name)
                    self:UpdateTopRightWnd()
                end, 
                function(name) 
                    CLuaSceneMgr:ShowOrHideSceneObject(16102535, "OtherObjects/Fx/jz_shuqi_001_012_men", false)
                end)
            end, 2000)
        else
            self:UpdateTopRightWnd()
            CLuaSceneMgr:ShowOrHideSceneObject(16102535, "OtherObjects/Fx/jz_shuqi_001_012_men", false)
        end
    elseif stage == 5 then
        self:UpdateTopRightWnd()
        CLuaSceneMgr:ShowOrHideSceneObject(16102535, "OtherObjects/Fx/jz_shuqi_001_012_men", false)
        if isShowFx then
            self.PlayMultiPlayerVideo("hhz_miwuling", nil, function(name)
                Gac2Gas.PlayGameVideoDone("hhz_miwuling")
            end)
        else
            Gac2Gas.PlayGameVideoDone("hhz_miwuling")
        end
    elseif stage > 5 then
        self:UpdateTopRightWnd()
        CLuaSceneMgr:ShowOrHideSceneObject(16102535, "OtherObjects/Fx/jz_shuqi_001_012_men", false)
    else
        self:UpdateTopRightWnd()
    end
end

-- isShowFx: 真凶揭晓时(true，播放迷雾驱散) or 真凶揭晓之后进入迷雾岭时(false)
-- 处理迷雾岭公共场景的场景迷雾和小地图的变化
-- PS: 目前没有迷雾驱散的动画，直接设置场景迷雾和各种机关的显示就行
LuaHuanHunShanZhuangMgr.ShowFog = false
function LuaHuanHunShanZhuangMgr:SyncMiWuLingFogClear(isShowFx, showFog)
    print("SyncMiWuLingFogClear", isShowFx, showFog)
    self.ShowFog = showFog

    if not self:IsInMiWuLing() then return end 
    
    local mapId = CScene.MainScene.SceneTemplateId
    CLuaSceneMgr:ShowOrHideSceneObject(mapId, "Models/jz_shuqi_qiaoshen", not showFog) -- 桥
    CLuaSceneMgr:ShowOrHideSceneObject(mapId, "Models/Others/jz_shuqi_001_019", true) --石柱1
    CLuaSceneMgr:ShowOrHideSceneObject(mapId, "Models/Others/jz_shuqi_001_019 (1)", true) --石柱2
    local obj = CRenderScene.Inst.transform:Find("Models/Others/jz_shuqi_001_019")
    if obj then
        LuaUtils.SetLocalPositionY(obj:GetChild(0), showFog and -1.320999 or 0)
    end
    obj = CRenderScene.Inst.transform:Find("Models/Others/jz_shuqi_001_019 (1)")
    if obj then
        LuaUtils.SetLocalPositionY(obj:GetChild(0), showFog and -1.320999 or 0)
    end
    CLuaSceneMgr:ShowOrHideSceneObject(mapId, "OtherObjects/Fx/jz_shuqi_001_012_men", showFog) -- 门

    -- 迷雾（只可能出现在白天）
    if not CRenderScene.Inst.AtNight then
        local dayStuffs = CRenderScene.Inst.transform:Find("Models/DayStuffs")
        dayStuffs = dayStuffs and dayStuffs:GetComponent(typeof(CSceneDynamicStuff))
        if dayStuffs then
            dayStuffs:ShowStuff(showFog, "Assets/Res/Fx/scene/Prefab/miwuling_pingzhang.prefab", "Frog")
        end
    end

    -- 迷雾面片
    CLuaSceneMgr:ShowOrHideSceneObject(mapId, "PlaneClouds", showFog)

    g_ScriptEvent:BroadcastInLua("SyncMiWuLingFogClear", showFog)
end

function LuaHuanHunShanZhuangMgr:SendHuanHunShanZhuangPlayTimes(tiaozhanTimes, zhuzhanTimes, hardModeTimes)
    print("SendHuanHunShanZhuangPlayTimes", tiaozhanTimes, zhuzhanTimes, hardModeTimes)
    if CUIManager.IsLoaded(CLuaUIResources.HuanHunShanZhuangSignUpWnd) then
        g_ScriptEvent:BroadcastInLua("SendHuanHunShanZhuangPlayTimes", tiaozhanTimes, zhuzhanTimes, hardModeTimes)
    else
        LuaHuanHunShanZhuangSignUpWnd.s_OpenWndWithInfo = {tiaozhanTimes, zhuzhanTimes, hardModeTimes}
        CUIManager.ShowUI(CLuaUIResources.HuanHunShanZhuangSignUpWnd)
    end
end

LuaHuanHunShanZhuangMgr.m_WaitForShowResult = false
function LuaHuanHunShanZhuangMgr:SendHuanHunShanZhuangPlayResult(isWin, isHardMode, duration, memberInfo, rewardType)
    print("SendHuanHunShanZhuangPlayResult", isWin, isHardMode, duration)
    local team = {}
    local list = memberInfo.Length == 0 and {} or g_MessagePack.unpack(memberInfo)
    for i = 1, #list, 7 do
        local memberId = list[i]
        local memberName = list[i+1]
        local level = list[i+2]
        local class = list[i+3]
        local hasFeiSheng = list[i+4]
        local isLeader = list[i+5]
        local hardModeTimes = list[i+6]
        team[math.floor((i - 1) / 7) + 1] = {id=memberId,name=memberName,lv=level,cls=class,hasFeiSheng=hasFeiSheng,isLeader=isLeader,hardTimes=hardModeTimes}
        print("SendHuanHunShanZhuangPlayResult", memberId, memberName, level, class, hasFeiSheng, isLeader, hardModeTimes)
    end

    LuaHuanHunShanZhuangResultWnd.s_IsWin = isWin
    LuaHuanHunShanZhuangResultWnd.s_IsNormal = not isHardMode
    LuaHuanHunShanZhuangResultWnd.s_Duration = duration
    LuaHuanHunShanZhuangResultWnd.s_TeamInfo = team
    LuaHuanHunShanZhuangResultWnd.s_RewardType = rewardType
    self.m_WaitForShowResult = true
end

LuaHuanHunShanZhuangMgr.YinNum = 0
LuaHuanHunShanZhuangMgr.YangNum = 0
function LuaHuanHunShanZhuangMgr:SyncHuanHunShanZHuangYinYang(yinNum, yangNum)
    --print("SyncHuanHunShanZHuangYinYang", yinNum, yangNum)
    LuaHuanHunShanZhuangMgr.YinNum = yinNum
    LuaHuanHunShanZhuangMgr.YangNum = yangNum
    g_ScriptEvent:BroadcastInLua("SyncHuanHunShanZHuangYinYang", yinNum, yangNum)
end
