local LuaGameObject=import "LuaGameObject"
--local Gac2Gas=import "L10.Game.Gac2Gas"
local CommonDefs = import "L10.Game.CommonDefs"
local UIEventListener = import "UIEventListener"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemMgr = import "L10.Game.CItemMgr"
local CButton = import "L10.UI.CButton"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local DateTime = import "System.DateTime"

--时装礼包
LuaCarnival2020View=class()
RegistClassMember(LuaCarnival2020View,"m_OpenBtn")
RegistClassMember(LuaCarnival2020View,"m_TipBtn")
RegistClassMember(LuaCarnival2020View,"m_ItemNode1")
RegistClassMember(LuaCarnival2020View,"m_ItemNode2")
RegistClassMember(LuaCarnival2020View,"m_ItemNode3")
RegistClassMember(LuaCarnival2020View,"m_ItemNode4")

function LuaCarnival2020View:Init()
    self.m_OpenBtn=LuaGameObject.GetChildNoGC(self.transform,"Button").gameObject
    self.m_TipBtn=LuaGameObject.GetChildNoGC(self.transform,"TipBtn").gameObject
    self.m_ItemNode1=self.transform:Find("Info/ItemNode1").gameObject
    self.m_ItemNode2=self.transform:Find("Info/ItemNode2").gameObject
    self.m_ItemNode3=self.transform:Find("Info/ItemNode3").gameObject
    self.m_ItemNode4=self.transform:Find("Info/ItemNode4").gameObject

    local ticketNum = JiaNianHua_Setting.GetData().Carnival2020TicketNum

    self:InitBuyTipText()

    self:InitNodeItem(self.m_ItemNode1,1)
    self:InitNodeItem(self.m_ItemNode2,2)
    self:InitNodeItem(self.m_ItemNode3,3)
    self:InitNodeItem(self.m_ItemNode4,4)

    local buyLabel4 = self.m_ItemNode4.transform:Find('PriceLabel').gameObject
    buyLabel4:SetActive(false)

    local onCheckClick = function(go)
      if CClientMainPlayer.Inst and CClientMainPlayer.Inst.MaxLevel >= 30 then
        CLuaWebBrowserMgr.OpenHouseActivityUrl("Carnival2020")
      else
        g_MessageMgr:ShowMessage('CUSTOM_STRING2',LocalString.GetString('等级不足，请先将角色升至30级'))
      end
    end
    CommonDefs.AddOnClickListener(self.m_OpenBtn,DelegateFactory.Action_GameObject(onCheckClick),false)
    local onTipClick = function(go)
      g_MessageMgr:ShowMessage('Carnival2020ButTip')
    end
    CommonDefs.AddOnClickListener(self.m_TipBtn,DelegateFactory.Action_GameObject(onTipClick),false)

    Gac2Gas.QueryDieKeVipTicketSellInfo()
end

function LuaCarnival2020View:OnEnable()
	g_ScriptEvent:AddListener("UpdateCarnival2020Data", self, "UpdateBuyBtn")
end

function LuaCarnival2020View:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateCarnival2020Data", self, "UpdateBuyBtn")
end

function LuaCarnival2020View:UpdateBuyBtn()
  self.SpeBuyBtn:SetActive(true)
  self:InitSpeBuyBtn(self.SpeBuyBtn, self.SpeBuyGoodsId)
end

function LuaCarnival2020View:InitBuyTipText()
  self.BuyTipTextTable = {}
  local locationString = LocalString.GetString('活动地点：杭州国际博览中心1D会场（萧山区奔竞大道353号）\n[c][FF0000]本商品不支持退款，购票后所有福利须线下现场检票后领取。[-][c]')
  self.BuyTipTextTable[1] = LocalString.GetString('[c][E3F88F]单日豪华票[-][c]购买须知：\n请确认您现在购买的是：\n单日豪华票（仅8月30日可用）\n') .. locationString
  self.BuyTipTextTable[2] = LocalString.GetString('[c][E3F88F]双日豪华票[-][c]购买须知：\n请确认您现在购买的是：\n双日豪华票（8月29、30日可用）\n') .. locationString
  self.BuyTipTextTable[3] = LocalString.GetString('[c][E3F88F]单日畅玩票[-][c]购买须知：\n请确认您现在购买的是：\n单日畅玩票（仅8月30日可用）\n') .. locationString
  self.BuyTipTextTable[4] = LocalString.GetString('[c][E3F88F]蝶客VIP套票[-][c]购买须知：\n请确认您现在购买的是：\n蝶客VIP套票（仅8月30日可用）\n') .. locationString
end

function LuaCarnival2020View:PlayerDataSaveCheck()
  local playerDataSaved = false

  if CClientMainPlayer.Inst ~= nil and CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.TempPlayData, typeof(UInt32), EnumTempPlayDataKey_lua.eJiaNianHua2020PersonalInfo) then
    local info = CommonDefs.DictGetValue(CClientMainPlayer.Inst.PlayProp.TempPlayData, typeof(UInt32), EnumTempPlayDataKey_lua.eJiaNianHua2020PersonalInfo)
    if info then
      playerDataSaved = true
    end
  end

  return playerDataSaved
end

function LuaCarnival2020View:InitSpeBuyBtn(buyBtn,goodsId)
  if not self:PlayerDataSaveCheck() then
    buyBtn:GetComponent(typeof(CButton)).Enabled = true
    buyBtn.transform:Find('Label'):GetComponent(typeof(UILabel)).text = LocalString.GetString('预填购票信息')
    local onBuyClick = function(go)
      CUIManager.ShowUI(CLuaUIResources.Carnival2020RealNameWnd)
    end
    CommonDefs.AddOnClickListener(buyBtn,DelegateFactory.Action_GameObject(onBuyClick),false)
  else
    buyBtn:GetComponent(typeof(CButton)).Enabled = true
    buyBtn.transform:Find('Label'):GetComponent(typeof(UILabel)).text = LocalString.GetString('购买')
    local onBuyClick = function(go)
      CUIManager.ShowUI(CLuaUIResources.Carnival2020VipTicketWnd)
    end
    CommonDefs.AddOnClickListener(buyBtn,DelegateFactory.Action_GameObject(onBuyClick),false)
  end
end

function LuaCarnival2020View:InitNodeItem(node,index)
  local data = JiaNianHua_2020Sale.GetData(index)
  if data then
    local itemsId = data.ItemsId
    local goodsId = data.GoodsId
    local buyBtn = node.transform:Find('BuyButton').gameObject

    if goodsId and goodsId > 0 and index < 4 then
      local now = CServerTimeMgr.Inst:GetZone8Time()
      local endOfTheBuyDay = CreateFromClass(DateTime, 2020, 8, 27, 0, 0, 0)
      local endSeconds = endOfTheBuyDay:Subtract(now).TotalSeconds

      if endSeconds < 0 then
        buyBtn:GetComponent(typeof(CButton)).Enabled = false
        buyBtn.transform:Find('Label'):GetComponent(typeof(UILabel)).text = LocalString.GetString('售票结束')
      else
        buyBtn:GetComponent(typeof(CButton)).Enabled = true
        local onBuyClick = function(go)
          if self.BuyTipTextTable and self.BuyTipTextTable[index] then
            local message = self.BuyTipTextTable[index]
            MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
              Gac2Gas.RequestBuyJiaNianHua2020Ticket(goodsId)
            end), nil, LocalString.GetString("购买"), nil, false)
          end
        end
        CommonDefs.AddOnClickListener(buyBtn,DelegateFactory.Action_GameObject(onBuyClick),false)
      end
    else
      --buyBtn:GetComponent(typeof(CButton)).enabled = false
      self.SpeBuyBtn = buyBtn
      self.SpeBuyGoodsId = goodsId
      self.SpeBuyBtn:SetActive(false)
    end

    for i=1,4 do
      local itemNode = node.transform:Find('Item'..i).gameObject
      itemNode:SetActive(false)
    end

    local itemsTable = g_LuaUtil:StrSplit(itemsId, ",")
    if itemsTable then
      for i,v in pairs(itemsTable) do
        local itemId = tonumber(v)
        local itemData = CItemMgr.Inst:GetItemTemplate(itemId)

        local itemNode = node.transform:Find('Item'..i).gameObject
        itemNode:SetActive(true)
        local bonusIcon = node.transform:Find('Item'..i..'/Normal/IconTexture'):GetComponent(typeof(CUITexture))
        bonusIcon:LoadMaterial(itemData.Icon)
        local iconLabel = node.transform:Find('Item'..i..'/Normal/DescLabel').gameObject
        iconLabel:SetActive(false)

        local clickNode = node.transform:Find('Item'..i).gameObject
        UIEventListener.Get(clickNode).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(clickNode).onClick, DelegateFactory.VoidDelegate(function (go)
          CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
        end), true)

      end
    end
  end
end

return LuaCarnival2020View
