local DelegateFactory = import "DelegateFactory"
local UILabel         = import "UILabel"
local UIInput         = import "UIInput"
local CWordFilterMgr  = import "L10.Game.CWordFilterMgr"
local UIEventListener = import "UIEventListener"
local CUIManager      = import "L10.UI.CUIManager"

LuaWeddingDayPromiseWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaWeddingDayPromiseWnd, "maleName")
RegistClassMember(LuaWeddingDayPromiseWnd, "femaleName")
RegistClassMember(LuaWeddingDayPromiseWnd, "input")
RegistClassMember(LuaWeddingDayPromiseWnd, "desc")
RegistClassMember(LuaWeddingDayPromiseWnd, "okButton")
RegistClassMember(LuaWeddingDayPromiseWnd, "fx")

RegistClassMember(LuaWeddingDayPromiseWnd, "tick")

function LuaWeddingDayPromiseWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
    self:InitEventListener()
end

-- 初始化UI组件
function LuaWeddingDayPromiseWnd:InitUIComponents()
    local anchor = self.transform:Find("Anchor")

    self.maleName = anchor:Find("Couple/MaleName"):GetComponent(typeof(UILabel))
    self.femaleName = anchor:Find("Couple/FemaleName"):GetComponent(typeof(UILabel))
    self.input = anchor:Find("Promise/InputLabel"):GetComponent(typeof(UIInput))
    self.desc = anchor:Find("Promise/Label"):GetComponent(typeof(UILabel))
    self.okButton = anchor:Find("OKButton")
    self.fx = self.transform:Find("BgWnd/Fx"):GetComponent(typeof(CUIFx))
end

-- 初始化点击响应
function LuaWeddingDayPromiseWnd:InitEventListener()
    UIEventListener.Get(self.okButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOKButtonClick()
	end)
end


function LuaWeddingDayPromiseWnd:Init()
    self.fx:LoadFx(g_UIFxPaths.WeddingPromiseLightFx)
    self.desc.text = g_MessageMgr:FormatMessage("MARRIGE_ANNIVERSARY_PROMISE_CONTENT")
    self.maleName.text = LuaWeddingIterationMgr.promiseInfo.groomName
    self.femaleName.text = LuaWeddingIterationMgr.promiseInfo.brideName

    self.tick = RegisterTick(function()
		CUIManager.CloseUI(CLuaUIResources.WeddingDayPromiseWnd)
	end, 1000 * WeddingDay_Setting.GetData().DeclareCountdownSeconds)
end

-- 关闭界面时清除计时器
function LuaWeddingDayPromiseWnd:OnDestroy()
    if self.tick then
		UnRegisterTick(self.tick)
		self.tick = nil
	end
end

--@region UIEvent

function LuaWeddingDayPromiseWnd:OnOKButtonClick()
    -- 滤除关键字
    local ret = CWordFilterMgr.Inst:DoFilterOnSend(self.input.value, nil, LocalString.GetString("千千结宣言"), true)
    local msg = ret.msg

    if msg == nil or ret.shouldBeIgnore then
        return
    end

    if msg == "" then
        g_MessageMgr:ShowMessage("WEDDING_DAY_PROMISE_NEED_INPUT")
        return
    end

    Gac2Gas.BroadcastWeddingDayDeclareMsg(msg)
    CUIManager.CloseUI(CLuaUIResources.WeddingDayPromiseWnd)
end

--@endregion UIEvent
