local CActivityAlertMgr = import "L10.UI.CActivityAlertMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
CActivityAlertMgr.m_CanJoin_CS2LuaHook = function(this, tasktype)
    if CClientMainPlayer.Inst== nil then return false end
    --先判断玩家可不可以参与
    local queryNpc = Task_QueryNpc.GetData(tasktype)
    if queryNpc ~= nil then
        local schedule = Task_Schedule.GetData(queryNpc.ScheduleID)
        if schedule ~= nil then
            if schedule.TaskID ~= nil and schedule.TaskID.Length > 0 then
                local task = Task_Task.GetData(schedule.TaskID[0])
                local level = CClientMainPlayer.Inst.Level
                if CClientMainPlayer.Inst.HasFeiSheng then
                    level = CClientMainPlayer.Inst.XianShenLevel
                    --飞升
                    if task.GradeCheckFS1 ~= nil and task.GradeCheckFS1.Length > 0 then
                        local gradeCheck = task.GradeCheckFS1[0]
                        if gradeCheck > level then
                            return false
                        end
                    end
                else
                    local taskLevel = CLuaScheduleMgr:GetTaskCheckLevel(task)
                    if taskLevel > level then
                        return false
                    end
                end
            end
        else
            return false
        end
    else
        return false
    end
    return true
end

CActivityAlertMgr.m_OnMainPlayerCreated_CS2LuaHook = function(this)
    this.infoList:Clear()
    this.YaYunNpcId = 0
    this.YaYunNpcCount = 0
    Gac2Gas.QueryServerOpenPassedDay(0)
    Gac2Gas.GetDynamicActivitySimpleInfo()
    Gac2Gas.ZhouNianShopAlertStatusQuery()
    Gac2Gas.QueryYanYunResNpcCount()
end

LuaActivityAlertMgr = {}
LuaActivityAlertMgr.AlertScheduleIDs = {}

function LuaActivityAlertMgr:SetAlertActivity(scheduleID,bshow,data)
    if bshow then
        LuaActivityAlertMgr.AlertScheduleIDs[scheduleID] = {data}
    else
        LuaActivityAlertMgr.AlertScheduleIDs[scheduleID] = nil
    end
end
