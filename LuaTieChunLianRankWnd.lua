local CPlayerInfoMgr=import "CPlayerInfoMgr"
local CRankData=import "L10.UI.CRankData"
local EnumPlayerInfoContext=import "L10.Game.EnumPlayerInfoContext"
local EChatPanel=import "L10.Game.EChatPanel"
local AlignType=import "CPlayerInfoMgr+AlignType"

CLuaTieChunLianRankWnd = class()
RegistClassMember(CLuaTieChunLianRankWnd,"m_TableView")
RegistClassMember(CLuaTieChunLianRankWnd,"m_RankList")
RegistClassMember(CLuaTieChunLianRankWnd,"m_MyRankLabel")
RegistClassMember(CLuaTieChunLianRankWnd,"m_MyNameLabel")
RegistClassMember(CLuaTieChunLianRankWnd,"m_MyTimeLabel")

function CLuaTieChunLianRankWnd:Init()
    self.m_MyRankLabel = self.transform:Find("MainPlayerInfo/MyRankLabel"):GetComponent(typeof(UILabel))
    self.m_MyNameLabel = self.transform:Find("MainPlayerInfo/MyNameLabel"):GetComponent(typeof(UILabel))
    self.m_MyTimeLabel = self.transform:Find("MainPlayerInfo/MyTimeLabel"):GetComponent(typeof(UILabel))

    self.m_RankList={}
    if CClientMainPlayer.Inst then
        self.m_MyNameLabel.text= CClientMainPlayer.Inst.Name
    end
    self.m_MyRankLabel.text=LocalString.GetString("未上榜")
    self.m_MyTimeLabel.text="—"
    
    self.m_TableView=self.transform:Find("TableView"):GetComponent(typeof(QnTableView))
    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_RankList
        end,
        function(item,index)
            item:SetBackgroundTexture(index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)

            self:InitItem(item,index,self.m_RankList[index+1])
        end)

    self.m_TableView.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        -- self:OnSelectAtRow(row)    
        local data=self.m_RankList[row+1]
        if data then
            CPlayerInfoMgr.ShowPlayerPopupMenu(data.PlayerIndex, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,AlignType.Default)
        end
    end)
    Gac2Gas.QueryRank(41000189)

end

function CLuaTieChunLianRankWnd:InitItem(item,index,info)
    local tf=item.transform
    local timeLabel=tf:Find("TimeLabel"):GetComponent(typeof(UILabel))
    timeLabel.text=""
    local nameLabel=tf:Find("NameLabel"):GetComponent(typeof(UILabel))
    nameLabel.text=" "
    local rankLabel=tf:Find("RankLabel"):GetComponent(typeof(UILabel))
    rankLabel.text=""
    local rankSprite=tf:Find("RankLabel/RankImage"):GetComponent(typeof(UISprite))
    rankSprite.spriteName=""
    
    -- local info=self.m_RankList[index+1]

    local rank=info.Rank
    if rank==1 then
        rankSprite.spriteName="Rank_No.1"
    elseif rank==2 then
        rankSprite.spriteName="Rank_No.2png"
    elseif rank==3 then
        rankSprite.spriteName="Rank_No.3png"
    else
        rankLabel.text=tostring(rank)
    end

    nameLabel.text = info.Name

    timeLabel.text = info.Value
end

function CLuaTieChunLianRankWnd:OnEnable()
    g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
    
end
function CLuaTieChunLianRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")

end

function CLuaTieChunLianRankWnd:OnRankDataReady()
    local myInfo = CRankData.Inst.MainPlayerRankInfo
    if myInfo.Rank>0 then
        self.m_MyRankLabel.text=myInfo.Rank
    else
        self.m_MyRankLabel.text=LocalString.GetString("未上榜")
    end
    self.m_MyNameLabel.text = myInfo.Name
    if CClientMainPlayer.Inst then
        self.m_MyNameLabel.text = CClientMainPlayer.Inst.Name
    end

    if myInfo.Value>0 then
        self.m_MyTimeLabel.text = myInfo.Value
    else
        self.m_MyTimeLabel.text = "—"
    end

    self.m_RankList={}
    for i=1,CRankData.Inst.RankList.Count do
        table.insert( self.m_RankList, CRankData.Inst.RankList[i-1])
    end
    

    self.m_TableView:ReloadData(true,false)
end