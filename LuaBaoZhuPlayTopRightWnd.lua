local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local CTopAndRightTipWnd=import "L10.UI.CTopAndRightTipWnd"

LuaBaoZhuPlayTopRightWnd = class()
RegistChildComponent(LuaBaoZhuPlayTopRightWnd,"rightBtn", GameObject)
RegistChildComponent(LuaBaoZhuPlayTopRightWnd,"tipNode1", GameObject)
RegistChildComponent(LuaBaoZhuPlayTopRightWnd,"tipNode2", GameObject)
RegistChildComponent(LuaBaoZhuPlayTopRightWnd,"tipNode3", GameObject)
RegistChildComponent(LuaBaoZhuPlayTopRightWnd,"tipNodePlayer", GameObject)

--RegistClassMember(LuaBaoZhuPlayTopRightWnd, "cacheDataTable")

function LuaBaoZhuPlayTopRightWnd:Close()
	--CUIManager.CloseUI(CLuaUIResources.)
end

function LuaBaoZhuPlayTopRightWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateBaoZhuPlayTopRankInfo", self, "UpdateTop")
	g_ScriptEvent:AddListener("UpdateBaoZhuPlaySelfRankInfo", self, "UpdateSelf")
end

function LuaBaoZhuPlayTopRightWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateBaoZhuPlayTopRankInfo", self, "UpdateTop")
	g_ScriptEvent:RemoveListener("UpdateBaoZhuPlaySelfRankInfo", self, "UpdateSelf")
end

function LuaBaoZhuPlayTopRightWnd:UpdateTop(data)
  local rankNode = {self.tipNode1,self.tipNode2,self.tipNode3}
  local node = rankNode[data.showRank]
  if node then
    node.transform:Find('text'):GetComponent(typeof(UILabel)).text = data.name
    node.transform:Find('num'):GetComponent(typeof(UILabel)).text = data.score

    local rankSprite = {'headinfownd_jiashang_01','headinfownd_jiashang_02','headinfownd_jiashang_03'}
		node.transform:Find('node'):GetComponent(typeof(UISprite)).spriteName = rankSprite[data.rank]
  end
end

function LuaBaoZhuPlayTopRightWnd:UpdateSelf(data)
  if data.rank > 3 then
    self.tipNodePlayer.transform:Find('node').gameObject:SetActive(false)
    self.tipNodePlayer.transform:Find('Label').gameObject:SetActive(true)
    self.tipNodePlayer.transform:Find('Label'):GetComponent(typeof(UILabel)).text = data.rank
  else
    self.tipNodePlayer.transform:Find('node').gameObject:SetActive(true)
    self.tipNodePlayer.transform:Find('Label').gameObject:SetActive(false)
    local rankSprite = {'headinfownd_jiashang_01','headinfownd_jiashang_02','headinfownd_jiashang_03'}
    self.tipNodePlayer.transform:Find('node'):GetComponent(typeof(UISprite)).spriteName = rankSprite[data.rank]
  end

  self.tipNodePlayer.transform:Find('text'):GetComponent(typeof(UILabel)).text = data.name
  self.tipNodePlayer.transform:Find('num'):GetComponent(typeof(UILabel)).text = data.score
end

function LuaBaoZhuPlayTopRightWnd:Clear()
  self.tipNodePlayer.transform:Find('node').gameObject:SetActive(false)
  self.tipNodePlayer.transform:Find('Label').gameObject:SetActive(false)
  self.tipNodePlayer.transform:Find('text'):GetComponent(typeof(UILabel)).text = CClientMainPlayer.Inst.Name
  self.tipNodePlayer.transform:Find('num'):GetComponent(typeof(UILabel)).text = '0'

  local rankNode = {self.tipNode1,self.tipNode2,self.tipNode3}
  for i,v in pairs(rankNode) do
    v.transform:Find('text'):GetComponent(typeof(UILabel)).text = ''
    v.transform:Find('num'):GetComponent(typeof(UILabel)).text = ''
  end
end

function LuaBaoZhuPlayTopRightWnd:Init()
	UIEventListener.Get(self.rightBtn).onClick=LuaUtils.VoidDelegate(function(go)
		LuaUtils.SetLocalRotation(self.rightBtn.transform,0,0,0)
		CTopAndRightTipWnd.showPackage = false
		CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
	end)

	self:Clear()
end

return LuaBaoZhuPlayTopRightWnd
