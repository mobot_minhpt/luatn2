require("3rdParty/ScriptEvent")
require("common/common_include")

local UILabel = import "UILabel"
local UIScrollView = import "UIScrollView"
local UITable = import "UITable"
local CommonDefs = import "L10.Game.CommonDefs"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUITexture = import "L10.UI.CUITexture"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CWebBrowserMgr = import "L10.Game.CWebBrowserMgr"

LuaNeteaseMemberView=class()
RegistClassMember(LuaNeteaseMemberView,"m_Banner")
RegistClassMember(LuaNeteaseMemberView,"m_AwardLabel")
RegistClassMember(LuaNeteaseMemberView,"m_AwardHeadItemIcon")
RegistClassMember(LuaNeteaseMemberView,"m_InfoButton")
RegistClassMember(LuaNeteaseMemberView,"m_BindButton")
RegistClassMember(LuaNeteaseMemberView,"m_MemberInfoLabel")

RegistClassMember(LuaNeteaseMemberView,"m_SrollView")
RegistClassMember(LuaNeteaseMemberView,"m_Table")
RegistClassMember(LuaNeteaseMemberView,"m_Template")


RegistClassMember(LuaNeteaseMemberView,"m_IsMember")
RegistClassMember(LuaNeteaseMemberView,"m_IsMemberBinded")
RegistClassMember(LuaNeteaseMemberView,"m_MemberExpireTime")
RegistClassMember(LuaNeteaseMemberView,"m_IsShortCard")

function LuaNeteaseMemberView:InitInfo()

    self.m_IsMember = false
    self.m_IsMemberBinded = false
    self.m_MemberExpireTime = 0
    self.m_IsShortCard = false

    self.m_Banner = self.transform:Find("Header/Banner"):GetComponent(typeof(UITexture))
    self.m_AwardLabel = self.transform:Find("Header/Banner/Sprite/Label"):GetComponent(typeof(UILabel))
    self.m_AwardHeadItemIcon = self.transform:Find("Header/Banner/Sprite/Texture"):GetComponent(typeof(CUITexture))
    self.m_InfoButton = self.transform:Find("Header/Info/InfoButton").gameObject
    self.m_BindButton = self.transform:Find("Header/Info/BindButton").gameObject
    self.m_MemberInfoLabel = self.transform:Find("Header/Info/Label"):GetComponent(typeof(UILabel))

    self.m_SrollView = self.transform:Find("ScrollView"):GetComponent(typeof(UIScrollView))
    self.m_Table = self.transform:Find("ScrollView/Table"):GetComponent(typeof(UITable))
    self.m_Template = self.transform:Find("ScrollView/Template").gameObject
    self.m_Template:SetActive(false)

    CommonDefs.AddOnClickListener(self.m_InfoButton, DelegateFactory.Action_GameObject(function(go) self:OnInfoButtonClick() end), false)
    CommonDefs.AddOnClickListener(self.m_Banner.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnBannerClick() end), false)
    CommonDefs.AddOnClickListener(self.m_BindButton, DelegateFactory.Action_GameObject(function(go) self:OnBindButtonClick() end), false)

    if LuaWelfareMgr.GetNeteaseMemberViewBannerTexture() then
        self:SetBanner()
    else
        LuaWelfareMgr.DownloadNeteaseMemberViewBannerInfo()
    end

    local icon = Item_Item.GetData(NeteaseMember_Setting.GetData().HeadIconItemID).Icon
    self.m_AwardHeadItemIcon:LoadMaterial(icon)

    self:UpdateInfo()
    LuaWelfareMgr.QueryNeteaseMemberWelfareInfo()
end

function LuaNeteaseMemberView:InitItem(go, rightType, data)
    local bgNormal = go.transform:Find("BgNormal").gameObject
    local bgShop = go.transform:Find("BgShop").gameObject
    local titleLabel = go.transform:Find("TitleLabel"):GetComponent(typeof(UILabel))
    local iconTexture = go.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local awardedIcon = go.transform:Find("AwardedIcon").gameObject
    local alertIcon = go.transform:Find("AlertIcon").gameObject

    local isShopMall = (rightType == EnumNeteaseMemberRightType.eShopMall)
    bgNormal:SetActive(not isShopMall)
    bgShop:SetActive(isShopMall)
    awardedIcon:SetActive(data.isRewarded)
    --根据最新的需求，短期会员卡不能领取绑定礼，这里取消红点显示
    alertIcon:SetActive(self.m_IsMemberBinded and data.hasRight and (not self.m_IsShortCard) and (not data.isRewarded))

    if rightType == EnumNeteaseMemberRightType.eShopMall then
        titleLabel.text = LocalString.GetString("会员商城")
        awardedIcon:SetActive(false)
        alertIcon:SetActive(false)
        iconTexture:LoadMaterial("UI/Texture/Transparent/Material/NeteaseMemberWindow_shangcheng_icon.mat")
    elseif rightType == EnumNeteaseMemberRightType.eHolidayGift then
        local holidaygift = NeteaseMember_HolidayGift.GetData(data.holidayId)
        local itemID = holidaygift.ItemID
        local item = Item_Item.GetData(itemID)
        titleLabel.text = LocalString.GetString(item.Name)
        iconTexture:LoadMaterial(holidaygift.Icon)
    else
        local id = LuaWelfareMgr.GetAwardByRightType(rightType)
        local award = NeteaseMember_Award.GetData(id)
        if award then
            titleLabel.text = award.Name
            iconTexture:LoadMaterial(award.Icon)
        else
            titleLabel.text = ""
            iconTexture:Clear()
        end
    end

    CommonDefs.AddOnClickListener(iconTexture.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnGetAward(go, rightType, data) end), false)
end

function LuaNeteaseMemberView:OnGetAward(go, rightType, data)
     if rightType == EnumNeteaseMemberRightType.eShopMall then
        LuaWelfareMgr.OpenNeteaseMemberMall()
    elseif rightType == EnumNeteaseMemberRightType.eHolidayGift then
        if not self.m_IsMemberBinded then
            local holidaygift = NeteaseMember_HolidayGift.GetData(data.holidayId)
            LuaMessageTipMgr:ShowMessage(g_MessageMgr:FormatMessage(holidaygift.TipMessage), LocalString.GetString("前往绑定"), function ( ... )
                if LuaWelfareMgr.m_NeteaseMemberBasicInfo.notExist then
                    g_MessageMgr:ShowMessage("NETEASE_MEMBER_NOT_USED_ON_OPEN_SHOP")
                else
                    LuaWelfareMgr.RequestUseNeteaseMemberRight(EnumNeteaseMemberRightType.eBindGame)
                end
            end)
        elseif not data.isRewarded then
            LuaWelfareMgr.RequestNeteaseMemberHolidayGift(data.holidayId)
        end   
    else
        if not self.m_IsMemberBinded then
            local id = LuaWelfareMgr.GetAwardByRightType(rightType)
            local award = NeteaseMember_Award.GetData(id)
            LuaMessageTipMgr:ShowMessage(g_MessageMgr:FormatMessage(award.TipMessage), LocalString.GetString("前往绑定"), function ( ... )
                if LuaWelfareMgr.m_NeteaseMemberBasicInfo.notExist then
                    g_MessageMgr:ShowMessage("NETEASE_MEMBER_NOT_USED_ON_OPEN_SHOP")
                else
                    LuaWelfareMgr.RequestUseNeteaseMemberRight(EnumNeteaseMemberRightType.eBindGame)
                end
            end)
        elseif not data.isRewarded then
            LuaWelfareMgr.RequestUseNeteaseMemberRight(rightType)
        end
    end
end



function LuaNeteaseMemberView:UpdateInfo()
    self.m_AwardLabel.text = (self.m_IsMember and self.m_IsShortCard and LocalString.GetString("年卡专享")) or LocalString.GetString("绑定礼专享")
    if self.m_IsMember then
        local expireTimeStr = CServerTimeMgr.ConvertTimeStampToZone8Time(self.m_MemberExpireTime):ToString("yyyy/MM/dd")
        if self.m_IsMemberBinded then
            if self.m_IsShortCard then
                self.m_MemberInfoLabel.text = g_MessageMgr:FormatMessage("BIND_SHORT_NETEASE_MEMBER_LABEL_TEXT", expireTimeStr)
            else
                self.m_MemberInfoLabel.text = g_MessageMgr:FormatMessage("BIND_NETEASE_MEMBER_LABEL_TEXT", expireTimeStr)
            end
        else
            self.m_MemberInfoLabel.text = g_MessageMgr:FormatMessage("NOBIND_NETEASE_MEMBER_LABEL_TEXT", expireTimeStr)
        end
    else
        self.m_MemberInfoLabel.text = g_MessageMgr:FormatMessage("NOT_NETEASE_MEMBER_LABEL_TEXT")
    end

    self.m_BindButton:SetActive(self.m_IsMember and not self.m_IsMemberBinded)


    Extensions.RemoveAllChildren(self.m_Table.transform)
    for rightType,data in pairs(LuaWelfareMgr.m_NeteaseMemberDataList) do
        local go = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_Template)
        go:SetActive(true)
        self:InitItem(go, rightType, data)
    end
    self.m_Table:Reposition()
    self.m_SrollView:ResetPosition()
end

function LuaNeteaseMemberView:OnEnable()

    g_ScriptEvent:AddListener("OnQueryNeteaseMemberInfoResult", self, "OnQueryNeteaseMemberInfoResult")
    g_ScriptEvent:AddListener("OnNeteaseMemberBannerReady", self, "OnNeteaseMemberBannerReady")
    g_ScriptEvent:AddListener("OnQueryNeteaseMemberInPurchaseInfoResult", self, "OnQueryNeteaseMemberInPurchaseInfoResult")
    g_ScriptEvent:AddListener("NotifyChargeDone", self, "NotifyChargeDone")
    self:InitInfo()
end

function LuaNeteaseMemberView:OnDisable()

    g_ScriptEvent:RemoveListener("OnQueryNeteaseMemberInfoResult", self, "OnQueryNeteaseMemberInfoResult")
    g_ScriptEvent:RemoveListener("OnNeteaseMemberBannerReady", self, "OnNeteaseMemberBannerReady")
    g_ScriptEvent:RemoveListener("OnQueryNeteaseMemberInPurchaseInfoResult", self, "OnQueryNeteaseMemberInPurchaseInfoResult")
    g_ScriptEvent:RemoveListener("NotifyChargeDone", self, "NotifyChargeDone")
end

function LuaNeteaseMemberView:OnQueryNeteaseMemberInfoResult()
    local basicInfo = LuaWelfareMgr.m_NeteaseMemberBasicInfo
    self.m_IsMember = (not basicInfo.expired) --未过期才认为是会员
    self.m_IsMemberBinded = basicInfo.bUsed
    self.m_MemberExpireTime = basicInfo.expiredTime
    self.m_IsShortCard = basicInfo.isShortCard
    self:UpdateInfo()
end

function LuaNeteaseMemberView:OnNeteaseMemberBannerReady()
   self:SetBanner() 
end

function LuaNeteaseMemberView:OnQueryNeteaseMemberInPurchaseInfoResult(status, url)
    if status == 1 and url ~= nil then
        LuaWelfareMgr.OpenQyClub(url)
    else
        self:OpenBannerURL()
    end
end

function LuaNeteaseMemberView:NotifyChargeDone(args)
    --订单轮询有结果后尝试刷新一下会员界面，不进行详细区分
    LuaWelfareMgr.QueryNeteaseMemberWelfareInfo()
end

function LuaNeteaseMemberView:SetBanner()
    self.m_Banner.mainTexture = LuaWelfareMgr.m_NeteaseMemberBannerTextureData
end


function LuaNeteaseMemberView:OnInfoButtonClick()
    g_MessageMgr:ShowMessage("Netease_Member_Tips")
end

function LuaNeteaseMemberView:OnBannerClick()
    if CommonDefs.IsIOSPlatform() then
        LuaWelfareMgr.QueryNeteaseMemberInPurchaseInfo()
    else
        self:OpenBannerURL()
    end
end

function LuaNeteaseMemberView:OpenBannerURL()
    if LuaWelfareMgr.m_NeteaseMemberBannerClickURL then
        CWebBrowserMgr.Inst:OpenUrl(LuaWelfareMgr.m_NeteaseMemberBannerClickURL)
    end
end

function LuaNeteaseMemberView:OnBindButtonClick()
    LuaWelfareMgr.RequestUseNeteaseMemberRight(EnumNeteaseMemberRightType.eBindGame)
end

