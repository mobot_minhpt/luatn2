local Animation = import "UnityEngine.Animation"

LuaTreasureSeaHintWnd = class()
LuaTreasureSeaHintWnd.s_Type = nil
LuaTreasureSeaHintWnd.s_Info = nil

RegistClassMember(LuaTreasureSeaHintWnd, "LvUp")
RegistClassMember(LuaTreasureSeaHintWnd, "Hunting")
RegistClassMember(LuaTreasureSeaHintWnd, "GangwayBattle")

RegistClassMember(LuaTreasureSeaHintWnd, "m_CloseTick")

function LuaTreasureSeaHintWnd:Awake()
    self.LvUp = self.transform:Find("Anchor/Hint/LvUp"):GetComponent(typeof(Animation))
    self.Hunting = self.transform:Find("Anchor/Hint/Hunting"):GetComponent(typeof(Animation))
    self.GangwayBattle = self.transform:Find("Anchor/Hint/GangwayBattle"):GetComponent(typeof(Animation))
    self.LvUp.gameObject:SetActive(false)
    self.Hunting.gameObject:SetActive(false)
    self.GangwayBattle.gameObject:SetActive(false)
end

function LuaTreasureSeaHintWnd:Init()
    CUIManager.CloseUI(CLuaUIResources.TreasureSeaEventWnd)
    UnRegisterTick(self.m_CloseTick)
    self.m_CloseTick = RegisterTickOnce(function()
        CUIManager.CloseUI(CLuaUIResources.TreasureSeaHintWnd)
    end, 2000)
    self.LvUp.gameObject:SetActive(false)
    self.Hunting.gameObject:SetActive(false)
    self.GangwayBattle.gameObject:SetActive(false)
    self:OnHint(LuaTreasureSeaHintWnd.s_Type, LuaTreasureSeaHintWnd.s_Info)
end

function LuaTreasureSeaHintWnd:OnDestroy()
    UnRegisterTick(self.m_CloseTick)
    self.m_CloseTick = nil
end

function LuaTreasureSeaHintWnd:OnHint(type, info)
    self.LvUp.gameObject:SetActive(false)
    self.Hunting.gameObject:SetActive(false)
    self.GangwayBattle.gameObject:SetActive(false)
    if type == 1 then -- LvUp
        self.LvUp.gameObject:SetActive(true)
        self.LvUp.transform:Find("Node/Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("%d级          %d级"), info.lv1, info.lv2)
        self.LvUp:Play("treasureseahintwnd_levelup")
    elseif type == 2 then -- Hunting
        self.Hunting.gameObject:SetActive(true)
        local label = self.Hunting.transform:Find("Node/Label"):GetComponent(typeof(UILabel))
        label.text = SafeStringFormat3("%s%d", info.success and  "+" or "-", math.abs(info.gold))
        label.color = info.success and NGUIText.ParseColor24("FFFE91", 0) or NGUIText.ParseColor24("FE504F", 0)
        self.Hunting:Play("treasureseahintwnd_"..(info.success and "success" or "fail"))
    elseif type == 3 then -- GangwayBattle
        self.GangwayBattle.gameObject:SetActive(true)
        local label = self.GangwayBattle.transform:Find("Node/Label"):GetComponent(typeof(UILabel))
        label.text = SafeStringFormat3("%s%d", info.success and  "+" or "-", math.abs(info.gold))
        label.color = info.success and NGUIText.ParseColor24("FFFE91", 0) or NGUIText.ParseColor24("FE504F", 0)
        self.GangwayBattle:Play("treasureseahintwnd_"..(info.success and "success" or "fail"))
    end
end
