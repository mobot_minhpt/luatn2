local GameObject = import "UnityEngine.GameObject"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local UILabel = import "UILabel"
local CBaseWnd = import "L10.UI.CBaseWnd"
local Animation = import "UnityEngine.Animation"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CUITexture = import "L10.UI.CUITexture"
local UIInput = import "UIInput"
local CButton = import "L10.UI.CButton"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local Screen = import "UnityEngine.Screen"
local EWeaponVisibleReason = import "L10.Engine.EWeaponVisibleReason"
local EnumYingLingState = import "L10.Game.EnumYingLingState"

LuaAppearanceFashionEvaluationWnd = class()
RegistChildComponent(LuaAppearanceFashionEvaluationWnd, "m_ItemNameLabel", "ItemNameLabel", UILabel)
RegistChildComponent(LuaAppearanceFashionEvaluationWnd, "m_ScoreLabel", "ScoreLabel", UILabel)
RegistChildComponent(LuaAppearanceFashionEvaluationWnd, "m_QualityLabel", "QualityLabel", UILabel)
RegistChildComponent(LuaAppearanceFashionEvaluationWnd, "m_ModelTexture", "ModelTexture", UITexture)
RegistChildComponent(LuaAppearanceFashionEvaluationWnd, "m_EvaluateTable", "EvaluateTable", UITable)
RegistChildComponent(LuaAppearanceFashionEvaluationWnd, "m_EvaluateItemTemplate", "EvaluateItem", GameObject)
RegistChildComponent(LuaAppearanceFashionEvaluationWnd, "m_OtherEvaluateItemTemplate", "OtherEvaluateItem", GameObject)
RegistChildComponent(LuaAppearanceFashionEvaluationWnd, "m_ButtonTable", "ButtonTable", UITable)
RegistChildComponent(LuaAppearanceFashionEvaluationWnd, "m_ExpressionButton", "ExpressionButton", CButton)
RegistChildComponent(LuaAppearanceFashionEvaluationWnd, "m_TransformButton", "TransformButton", CButton)
RegistChildComponent(LuaAppearanceFashionEvaluationWnd, "m_ResetButton", "ResetButton", CButton)
RegistChildComponent(LuaAppearanceFashionEvaluationWnd, "m_SubmitButton", "SubmitButton", CButton)

RegistClassMember(LuaAppearanceFashionEvaluationWnd, "m_OtherInput")

RegistClassMember(LuaAppearanceFashionEvaluationWnd, "m_Identifier")
RegistClassMember(LuaAppearanceFashionEvaluationWnd, "m_CurRotation")
RegistClassMember(LuaAppearanceFashionEvaluationWnd, "m_RequestEvaluateFasionSuitId")
RegistClassMember(LuaAppearanceFashionEvaluationWnd, "m_HeadId")
RegistClassMember(LuaAppearanceFashionEvaluationWnd, "m_BodyId")
RegistClassMember(LuaAppearanceFashionEvaluationWnd, "m_TransformMonsterId")
RegistClassMember(LuaAppearanceFashionEvaluationWnd, "m_TransformExpressionId")
RegistClassMember(LuaAppearanceFashionEvaluationWnd, "m_SpecialExpressionId")
RegistClassMember(LuaAppearanceFashionEvaluationWnd, "m_IsTransformOn")
RegistClassMember(LuaAppearanceFashionEvaluationWnd, "m_PreviewRO")
RegistClassMember(LuaAppearanceFashionEvaluationWnd, "m_EvaluateInfoTbl")
RegistClassMember(LuaAppearanceFashionEvaluationWnd, "m_EvaluateIndexTbl")

function LuaAppearanceFashionEvaluationWnd:Awake()
    self.m_EvaluateItemTemplate:SetActive(false)
    self.m_OtherEvaluateItemTemplate:SetActive(false)
    UIEventListener.Get(self.m_ModelTexture.gameObject).onDrag = DelegateFactory.VectorDelegate(function(go,delta) self:OnDragModel(go,delta) end)
    UIEventListener.Get(self.m_ExpressionButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnExpressionButtonClick() end)
    UIEventListener.Get(self.m_TransformButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnTransformButtonClick() end)
    UIEventListener.Get(self.m_ResetButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnResetButtonClick() end)
    UIEventListener.Get(self.m_SubmitButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnSubmitButtonClick() end)

    self.m_EvaluateInfoTbl = {LocalString.GetString("整体满意度"),LocalString.GetString("版型"),LocalString.GetString("配色"),
                                LocalString.GetString("性价比"),LocalString.GetString("变身形象"),LocalString.GetString("特殊动作")}
    self.m_Identifier = "__AppearanceFashionEvaluationkWnd__"
    self.m_CurRotation = 180
end

function LuaAppearanceFashionEvaluationWnd:Init()
    self.m_RequestEvaluateFasionSuitId = LuaAppearancePreviewMgr.m_RequestEvaluateFasionSuitId
    local suitData = Wardrobe_FashionSuit.GetData(self.m_RequestEvaluateFasionSuitId)
    local headData = Fashion_Fashion.GetData(suitData.HeadId)
    local bodyData = Fashion_Fashion.GetData(suitData.BodyId)
    local fashionName = suitData.Name
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer==nil then
        self:Close()
        return
    end
    self.m_HeadId = suitData.HeadId
    self.m_BodyId = suitData.BodyId
    self.m_TransformMonsterId, self.m_TransformExpressionId, self.m_SpecialExpressionId = LuaAppearancePreviewMgr:GetFashionSuitTransformAndExpressionInfo(headData, bodyData)
    self.m_IsTransformOn = false
    self.m_ItemNameLabel.text = fashionName
    self.m_ScoreLabel.text = tostring(headData.Fashionability + bodyData.Fashionability)
    self.m_QualityLabel.text = bodyData.Quality>0 and LuaAppearancePreviewMgr:GetFashionQualityName(bodyData.Quality) or LocalString.GetString("无")
    self.m_QualityLabel.color = LuaAppearancePreviewMgr:GetFashionQualityColor(bodyData.Quality)

    self.m_TransformButton.gameObject:SetActive(self.m_TransformMonsterId>0)
    self.m_TransformButton.Selected = self.m_IsTransformOn
    self.m_ExpressionButton.gameObject:SetActive((self.m_IsTransformOn and self.m_TransformExpressionId>0) or self.m_SpecialExpressionId>0)
    self.m_ButtonTable:Reposition()
    self:UpdateSubmitButtonDisplay()
    self:InitEvaluationContent(self.m_TransformMonsterId>0, self.m_TransformExpressionId>0 or self.m_SpecialExpressionId>0)
    self:LoadModel()
end

function LuaAppearanceFashionEvaluationWnd:OnDragModel(go,delta)
    local deltaVal = -delta.x / Screen.width * 360
    self.m_CurRotation = self.m_CurRotation + deltaVal
    CUIManager.SetModelRotation(self.m_Identifier, self.m_CurRotation)
end

function LuaAppearanceFashionEvaluationWnd:UpdateSubmitButtonDisplay()
    local isEvaluated = LuaAppearancePreviewMgr:IsFashionSuitEvaluated(self.m_RequestEvaluateFasionSuitId)
    self.m_SubmitButton.Enabled = not isEvaluated
    self.m_SubmitButton.Text = isEvaluated and LocalString.GetString("已评价") or LocalString.GetString("提  交")
end

function LuaAppearanceFashionEvaluationWnd:OnExpressionButtonClick()
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer==nil then return end
    self:LoadResource()
    CClientMainPlayer.ShowExpressionAction(self.m_PreviewRO,mainplayer.Gender, self.m_IsTransformOn and self.m_TransformExpressionId or self.m_SpecialExpressionId)
end

function LuaAppearanceFashionEvaluationWnd:OnTransformButtonClick()
    self.m_IsTransformOn = not self.m_IsTransformOn
    self.m_TransformButton.Selected = self.m_IsTransformOn
    self.m_ExpressionButton.gameObject:SetActive((self.m_IsTransformOn and self.m_TransformExpressionId>0) or self.m_SpecialExpressionId>0)
    self.m_ButtonTable:Reposition()
    self:LoadResource()
end

function LuaAppearanceFashionEvaluationWnd:OnResetButtonClick()
    self.m_CurRotation = 180
    CUIManager.SetModelRotation(self.m_Identifier, self.m_CurRotation)
end

function LuaAppearanceFashionEvaluationWnd:OnSubmitButtonClick()
    local manyidu, banxing, peise, xingjiabi, bianshen, special, others = 0,0,0,0,0,0,""
    for i = 1, #self.m_EvaluateIndexTbl do
        local root = self.m_EvaluateTable.transform:GetChild(i-1):Find("Table")
        local index = self.m_EvaluateIndexTbl[i]
        local score = self:GetEvaluateScore(root)
        if score==0 then
            --评分项如果没有评分，提示消息并返回
            g_MessageMgr:ShowMessage("Appearance_Fashion_Evaluation_Need_Evaluate_Option", self.m_EvaluateInfoTbl[index])
            return 
        end
        if index==1 then manyidu = score
            elseif index==2 then banxing = score
            elseif index==3 then peise = score
            elseif index==4 then xingjiabi = score
            elseif index==5 then bianshen = score
            elseif index==6 then special = score end
    end
    others = self.m_OtherInput.value
    local ret = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(others, true)
    if (not ret.msg) or ret.shouldBeIgnore then return end
    LuaAppearancePreviewMgr:RequestEvaluateFashionSuit_Permanent(self.m_RequestEvaluateFasionSuitId, 
        manyidu, banxing, peise, xingjiabi, bianshen, special, ret.msg)
end

function LuaAppearanceFashionEvaluationWnd:InitEvaluationContent(hasTrasnform, hasExpression)
    self.m_EvaluateIndexTbl = {1,2,3,4} --对应self.m_EvaluateInfoTbl的序号
    if hasTrasnform then table.insert(self.m_EvaluateIndexTbl, 5) end
    if hasExpression then table.insert(self.m_EvaluateIndexTbl, 6) end

    Extensions.RemoveAllChildren(self.m_EvaluateTable.transform)

    for i = 1, #self.m_EvaluateIndexTbl do
        local child = CUICommonDef.AddChild(self.m_EvaluateTable.gameObject, self.m_EvaluateItemTemplate)
        child:SetActive(true)
        self:InitItem(child, self.m_EvaluateIndexTbl[i])
    end
    local child = CUICommonDef.AddChild(self.m_EvaluateTable.gameObject, self.m_OtherEvaluateItemTemplate)
    child:SetActive(true)
    self.m_OtherInput = child.transform:Find("Input"):GetComponent(typeof(UIInput))
    self:InitInput(self.m_OtherInput)

    self.m_EvaluateTable:Reposition()
end

function LuaAppearanceFashionEvaluationWnd:InitItem(itemGo, index) 
    local titleLabel = itemGo.transform:Find("TitleLabel"):GetComponent(typeof(UILabel))
    titleLabel.text = self.m_EvaluateInfoTbl[index]
    local tblRoot = itemGo.transform:Find("Table")
    local n = tblRoot.childCount
    for i=0,n-1 do
        local childGo = tblRoot:GetChild(i).gameObject
        --onclick点击显示有延迟，根据策划qa的建议换成按下时执行选中
        UIEventListener.Get(childGo).onPress = DelegateFactory.BoolDelegate(function (go, isPressed) self:OnEvaluateButtonClick(tblRoot, go) end)
    end
end

--仿照CharacterCreationWnd Name输入的处理，注意只能执行一次
function LuaAppearanceFashionEvaluationWnd:InitInput(input)
    input.characterLimit = 200
	if CommonDefs.IsInMobileDevice() then
        EventDelegate.Add(input.onSubmit, DelegateFactory.Callback(function ()
            self:OnInputChange(input)
        end))
    else
        EventDelegate.Add(input.onChange, DelegateFactory.Callback(function ()
            self:OnInputChange(input)
        end))
    end
end

function LuaAppearanceFashionEvaluationWnd:OnInputChange(input)
    local str = input.value
    local maxCharLimit = 100 --限定100个中文字
    if CUICommonDef.GetStrByteLength(str) > maxCharLimit then
        repeat
            str = CommonDefs.StringSubstring2(str, 0, CommonDefs.StringLength(str) - 1)
        until not (CUICommonDef.GetStrByteLength(str) > maxCharLimit)
        input.value = str
    end
end

function LuaAppearanceFashionEvaluationWnd:GetEvaluateScore(root)
    local n = root.childCount
    for i=n-1,0,-1 do
        if root:GetChild(i):GetComponent(typeof(CButton)).Selected then
            return i+1
        end
    end
    return 0
end

function LuaAppearanceFashionEvaluationWnd:OnEvaluateButtonClick(root, go)
    local childCount =root.childCount
    local selectedIndex = -1
    for i=0, childCount-1 do
        local childGo = root:GetChild(i).gameObject
        if childGo == go then
            selectedIndex = i
            break
        end
    end
    for i=0, childCount-1 do
        local childGo = root:GetChild(i).gameObject
        if i<=selectedIndex then
            childGo:GetComponent(typeof(CButton)).Selected = true
        else
            childGo:GetComponent(typeof(CButton)).Selected = false
        end
    end
end

function LuaAppearanceFashionEvaluationWnd:LoadModel()
    self.m_ModelTexture.mainTexture = CUIManager.CreateModelTexture(self.m_Identifier, self:GetModelLoader(),
        self.m_CurRotation, 0.05, -1, 4.66, false, true, 1.0, true, false)
end

function LuaAppearanceFashionEvaluationWnd:GetModelLoader()
    return LuaDefaultModelTextureLoader.Create(function (ro)
        self.m_PreviewRO = ro
       self:LoadResource()
    end)
end

function LuaAppearanceFashionEvaluationWnd:LoadResource()
    --清除一些表情动作的状态信息，类似CFullScreenModelTextureLoader中的处理
    self.m_PreviewRO:DestroyAllFX()
    self.m_PreviewRO:RemoveChild("weapon03")
    self.m_PreviewRO:SetSepSkeleton(nil)
    self.m_PreviewRO.SepAniEndCallback = nil
    self.m_PreviewRO:SetWeaponVisible(EWeaponVisibleReason.Expression, true)
    local fakeAppear = LuaAppearancePreviewMgr:GetPreviewFashionInfo(self.m_HeadId, self.m_BodyId, 0, 0, self.m_IsTransformOn)
    fakeAppear.YingLingState = EnumToInt(EnumYingLingState.eDefault) --#206127 【外观评价】阿乌显示原始性别 【2023.9.21投放】9.7Build版本
    --注意monsterid，如果不开启变身的情况下，不显示变身效果，原角色已经变身时，进入该界面默认也不显示变身
    CClientMainPlayer.LoadResource(self.m_PreviewRO, fakeAppear, true, 1.0, 0, false, self.m_IsTransformOn and fakeAppear.ResourceId or 0, false, 0, false, nil, false, false)
end

function LuaAppearanceFashionEvaluationWnd:OnDestroy()
    CUIManager.DestroyModelTexture(self.m_Identifier)
end

function LuaAppearanceFashionEvaluationWnd:OnEnable()
    g_ScriptEvent:AddListener("OnEvaluateFashionSuitSuccess_Permanent",self,"OnEvaluateFashionSuitSuccess_Permanent")
end

function LuaAppearanceFashionEvaluationWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnEvaluateFashionSuitSuccess_Permanent",self,"OnEvaluateFashionSuitSuccess_Permanent")
end

function LuaAppearanceFashionEvaluationWnd:OnEvaluateFashionSuitSuccess_Permanent(suitId)
    if suitId == self.m_RequestEvaluateFasionSuitId then
        g_MessageMgr:ShowMessage("Appearance_Fashion_Evaluation_Complete")
        self:Close() -- 评价成功直接关闭
    end
end

function LuaAppearanceFashionEvaluationWnd:Close()
    self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end
