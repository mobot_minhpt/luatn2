require("common/common_include")

local QnCheckBox = import "L10.UI.QnCheckBox"
local UISprite = import "UISprite"
local CUITexture = import "L10.UI.CUITexture"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local Item_Item = import "L10.Game.Item_Item"
local CItemMgr = import "L10.Game.CItemMgr"
local Mall_LingYuMall = import "L10.Game.Mall_LingYuMall"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"
local CItemAccessListMgr=import "L10.UI.CItemAccessListMgr"
local MessageMgr = import "L10.Game.MessageMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local Baby_Setting = import "L10.Game.Baby_Setting"
local CButton = import "L10.UI.CButton"

LuaBabyTiLiGetTip=class()

RegistChildComponent(LuaBabyTiLiGetTip, "Anchor", GameObject)
RegistChildComponent(LuaBabyTiLiGetTip, "ApplyButton", CButton)
-- 健体丸物品
RegistChildComponent(LuaBabyTiLiGetTip, "GetHint", GameObject)
RegistChildComponent(LuaBabyTiLiGetTip, "DisableSprite", GameObject)
RegistChildComponent(LuaBabyTiLiGetTip, "QualitySprite", UISprite)
RegistChildComponent(LuaBabyTiLiGetTip, "IconTexture", CUITexture)
RegistChildComponent(LuaBabyTiLiGetTip, "CountLabel", UILabel)
RegistChildComponent(LuaBabyTiLiGetTip, "NameLabel", UILabel)
RegistChildComponent(LuaBabyTiLiGetTip, "ItemCell", GameObject)

RegistChildComponent(LuaBabyTiLiGetTip, "QnCheckBox", QnCheckBox)
RegistChildComponent(LuaBabyTiLiGetTip, "QnCostAndOwnMoney", CCurentMoneyCtrl)


RegistClassMember(LuaBabyTiLiGetTip, "Key_BabyTiLiAutoUseLingYu")

function LuaBabyTiLiGetTip:Init()


    self.Key_BabyTiLiAutoUseLingYu = "Key_BabyTiLiAutoUseLingYu"
    -- 初始化健体丸物品
    local setting = Baby_Setting.GetData()
    local itemId = setting.JianTiWanId
    local item = Item_Item.GetData(itemId)

    self.NameLabel.text = item.Name
    self.IconTexture:LoadMaterial(item.Icon)
    self.ApplyButton.Text = SafeStringFormat3(LocalString.GetString("增加%s"), tostring(setting.JianTiWanAddTili))

    -- 健体丸灵玉价格
    local mall = Mall_LingYuMall.GetData(itemId)
    self.QnCostAndOwnMoney:SetType(13, 0, true)
    self.QnCostAndOwnMoney:SetCost(mall.Jade)

    -- 是否使用灵玉
    local autoUseMoney = self:AutoUseMoney()
    self.QnCheckBox:SetSelected(autoUseMoney, true)

    local onDisableSpriteClicked = function (go)
        self:OnDisableSpriteClicked(go)
    end
    CommonDefs.AddOnClickListener(self.DisableSprite, DelegateFactory.Action_GameObject(onDisableSpriteClicked), false)

    local onApplyButtonClicked = function (go)
        self:OnApplyButtonClicked(go)
    end
    CommonDefs.AddOnClickListener(self.ApplyButton.gameObject, DelegateFactory.Action_GameObject(onApplyButtonClicked), false)

    self.QnCheckBox.OnValueChanged = DelegateFactory.Action_bool(function (value)
        self:OnQnCheckBoxChanged(value)
    end)

    self:UpdateShowArea()
end

function LuaBabyTiLiGetTip:UpdateShowArea()
    local autoUseMoney = self:AutoUseMoney()
    local itemCount = self:GetNeedItemCount()
    self:UpdateNeedItem()

    self.QnCheckBox.gameObject:SetActive(true)

    if itemCount > 0 then
        self.ItemCell:SetActive(true)
        self.NameLabel.gameObject:SetActive(true)
        self.QnCostAndOwnMoney.gameObject:SetActive(false)
    else
        if autoUseMoney then
            self.ItemCell:SetActive(false)
            self.NameLabel.gameObject:SetActive(false)
            self.QnCostAndOwnMoney.gameObject:SetActive(true)
        else
            self.ItemCell:SetActive(true)
            self.NameLabel.gameObject:SetActive(true)
            self.QnCostAndOwnMoney.gameObject:SetActive(false)
        end
    end
end

function LuaBabyTiLiGetTip:UpdateNeedItem()
    local needItemCount = self:GetNeedItemCount()

    if needItemCount > 0 then
        self.DisableSprite:SetActive(false)
        self.GetHint:SetActive(false)
        self.CountLabel.text = SafeStringFormat3("%d/%d", needItemCount, 1)
    else
        self.DisableSprite:SetActive(true)
        self.GetHint:SetActive(true)
        self.CountLabel.text = SafeStringFormat3("[FF0000]%d[-]/%d", needItemCount, 1)
    end
    
end

-- 获得健体丸的数量
function LuaBabyTiLiGetTip:GetNeedItemCount()
    local setting = Baby_Setting.GetData()
    local itemId = setting.JianTiWanId

    local bindItemCountInBag, notBindItemCountInBag = CItemMgr.Inst:CalcItemCountInBagByTemplateId(itemId)
    local totalCount = bindItemCountInBag + notBindItemCountInBag
    return totalCount
end


function LuaBabyTiLiGetTip:AutoUseMoney()
    return PlayerPrefs.GetInt(self.Key_BabyTiLiAutoUseLingYu, 1) > 0
end

function LuaBabyTiLiGetTip:OnDisableSpriteClicked(go)
    local setting = Baby_Setting.GetData()
    local itemId = setting.JianTiWanId
    CItemAccessListMgr.Inst:ShowItemAccessInfo(itemId, false, go.transform, CTooltipAlignType.Right)
end

-- 请求使用健体丸 
function LuaBabyTiLiGetTip:OnApplyButtonClicked(go)
    local autoUseMoney = self:AutoUseMoney()
    local itemCount = self:GetNeedItemCount()

    if itemCount > 0 then
        -- 请求 babyId, useJade
        Gac2Gas.RequestQuickAddBabyTili(LuaBabyMgr.m_AddTiLiBabyId, false)
    else
        -- 道具不足
        if not autoUseMoney then
            MessageMgr.Inst:ShowMessage("BABY_JIANTIWAN_NOT_ENOUGH", {})
            self:OnDisableSpriteClicked(go)
        else
            if not self.QnCostAndOwnMoney.moneyEnough then
                MessageWndManager.ShowOKCancelMessage(LocalString.GetString("灵玉不足，是否前往充值？"), DelegateFactory.Action(function () 
                    CShopMallMgr.ShowChargeWnd()
                end), nil, LocalString.GetString("充值"), nil, false)
            else
                -- 请求
                Gac2Gas.RequestQuickAddBabyTili(LuaBabyMgr.m_AddTiLiBabyId, true)
            end
        end
    end
end


function LuaBabyTiLiGetTip:OnQnCheckBoxChanged(value)
    if value then
        PlayerPrefs.SetInt(self.Key_BabyTiLiAutoUseLingYu, 1)
    else
        PlayerPrefs.SetInt(self.Key_BabyTiLiAutoUseLingYu, 0)
    end
    self:UpdateShowArea()
end

function LuaBabyTiLiGetTip:Update()
    self:ClickThroughToClose()
end

function LuaBabyTiLiGetTip:ClickThroughToClose()
    if Input.GetMouseButtonDown(0) then       
        if (not CUICommonDef.IsOverUI or (UICamera.lastHit.collider and
           not CUICommonDef.IsChild(self.transform, UICamera.lastHit.collider.transform) and
            CUICommonDef.GetNearestPanelDepth(self.transform) > CUICommonDef.GetNearestPanelDepth(UICamera.lastHit.collider.transform))) then
            CUIManager.CloseUI(CLuaUIResources.BabyTiLiGetTip)
        end
    end
end

function LuaBabyTiLiGetTip:QuickAddBabyTiliSuccess(babyId)
    if babyId == LuaBabyMgr.m_AddTiLiBabyId then
        self:UpdateShowArea()
    end
end

function LuaBabyTiLiGetTip:OnEnable()
    g_ScriptEvent:AddListener("SendItem", self, "UpdateShowArea")
    g_ScriptEvent:AddListener("SetItemAt", self, "UpdateShowArea")
    g_ScriptEvent:AddListener("QuickAddBabyTiliSuccess", self, "QuickAddBabyTiliSuccess")
end

function LuaBabyTiLiGetTip:OnDisable()
    g_ScriptEvent:RemoveListener("SendItem", self, "UpdateShowArea")
    g_ScriptEvent:RemoveListener("SetItemAt", self, "UpdateShowArea")
    g_ScriptEvent:RemoveListener("QuickAddBabyTiliSuccess", self, "QuickAddBabyTiliSuccess")
end


return LuaBabyTiLiGetTip