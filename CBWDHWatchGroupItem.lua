-- Auto Generated!!
local CBWDHWatchGroupItem = import "L10.UI.CBWDHWatchGroupItem"
local LocalString = import "LocalString"
CBWDHWatchGroupItem.m_Init_CS2LuaHook = function (this, group) 
    this.group = group
    repeat
        local default = group
        if default == 1 then
            this.nameLabel.text = LocalString.GetString("新锐组")
            break
        elseif default == 2 then
            this.nameLabel.text = LocalString.GetString("英武组")
            break
        elseif default == 3 then
            this.nameLabel.text = LocalString.GetString("神勇组")
            break
        elseif default == 4 then
            this.nameLabel.text = LocalString.GetString("天罡组")
            break
        elseif default == 5 then
            this.nameLabel.text = LocalString.GetString("天元组")
            break
        end
    until 1
end
