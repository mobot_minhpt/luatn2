local CButton = import "L10.UI.CButton"
local GameObject = import "UnityEngine.GameObject"
local UIProgressBar = import "UIProgressBar"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"

LuaGuildTerritorialWarsBeiZhanView = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaGuildTerritorialWarsBeiZhanView, "TopLabel", "TopLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsBeiZhanView, "TopReadMeLabel", "TopReadMeLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsBeiZhanView, "ExchangeTimesLabel", "ExchangeTimesLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsBeiZhanView, "ExchangeCostLabel", "ExchangeCostLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsBeiZhanView, "ExchangeButton", "ExchangeButton", CButton)
RegistChildComponent(LuaGuildTerritorialWarsBeiZhanView, "ScoreProgressLabel", "ScoreProgressLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsBeiZhanView, "ScoreLabel", "ScoreLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsBeiZhanView, "ExchangeButtonReddot", "ExchangeButtonReddot", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsBeiZhanView, "ExchangeItemIcon", "ExchangeItemIcon", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsBeiZhanView, "ScoreProgress", "ScoreProgress", UIProgressBar)
RegistChildComponent(LuaGuildTerritorialWarsBeiZhanView, "ScoreSubmitBtn", "ScoreSubmitBtn", CButton)
RegistChildComponent(LuaGuildTerritorialWarsBeiZhanView, "HpLabel", "HpLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsBeiZhanView, "HpProgressLabel", "HpProgressLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsBeiZhanView, "HpSubmitBtn", "HpSubmitBtn", CButton)
RegistChildComponent(LuaGuildTerritorialWarsBeiZhanView, "HpProgress", "HpProgress", UIProgressBar)
RegistChildComponent(LuaGuildTerritorialWarsBeiZhanView, "BuffLabel", "BuffLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsBeiZhanView, "BuffProgressLabel", "BuffProgressLabel", UILabel)
RegistChildComponent(LuaGuildTerritorialWarsBeiZhanView, "BuffSubmitBtn", "BuffSubmitBtn", CButton)
RegistChildComponent(LuaGuildTerritorialWarsBeiZhanView, "BuffProgress", "BuffProgress", UIProgressBar)
RegistChildComponent(LuaGuildTerritorialWarsBeiZhanView, "ScoreItemIcon", "ScoreItemIcon", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsBeiZhanView, "HpItemIcon", "HpItemIcon", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsBeiZhanView, "BuffItemIcon", "BuffItemIcon", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsBeiZhanView, "lvse", "lvse", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsBeiZhanView, "hongse", "hongse", GameObject)
RegistChildComponent(LuaGuildTerritorialWarsBeiZhanView, "huangse", "huangse", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaGuildTerritorialWarsBeiZhanView, "m_LinqiNum")
RegistClassMember(LuaGuildTerritorialWarsBeiZhanView, "m_FxList")
RegistClassMember(LuaGuildTerritorialWarsBeiZhanView, "m_ExpList")
RegistClassMember(LuaGuildTerritorialWarsBeiZhanView, "m_LvList")
function LuaGuildTerritorialWarsBeiZhanView:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ExchangeButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnExchangeButtonClick()
	end)


	UIEventListener.Get(self.ExchangeItemIcon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnExchangeItemIconClick()
	end)

	
	UIEventListener.Get(self.ScoreSubmitBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnScoreSubmitBtnClick()
	end)


	
	UIEventListener.Get(self.HpSubmitBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnHpSubmitBtnClick()
	end)


	
	UIEventListener.Get(self.BuffSubmitBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBuffSubmitBtnClick()
	end)


	UIEventListener.Get(self.ScoreItemIcon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnScoreItemIconClick()
	end)

	UIEventListener.Get(self.HpItemIcon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnHpItemIconClick()
	end)

	UIEventListener.Get(self.BuffItemIcon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBuffItemIconClick()
	end)

    --@endregion EventBind end
end

function LuaGuildTerritorialWarsBeiZhanView:Start()
    self.m_LinqiNum = 0
    self.TopReadMeLabel.text = g_MessageMgr:FormatMessage("GuildTerritorialWarsBeiZhanView_TopReadMeLabel")
    self.m_FxList = {self.huangse, self.hongse, self.lvse}
    for i = 1, #self.m_FxList do
        self.m_FxList[i].transform:Find("xishou").gameObject:SetActive(false)
        self.m_FxList[i].transform:Find("andi").gameObject:SetActive(false)
    end
    self.m_ExpList = {}
    self.m_LvList = {}
    self:InitExchangeView()
    self:InitScoreView()
    self:InitHpView()
    self:InitBuffView()
end

function LuaGuildTerritorialWarsBeiZhanView:InitExchangeView()
    local remainExchangeTimes, allExchangeTimes = 0, GuildTerritoryWar_RelatedPlaySetting.GetData().MaxNumFengXue 
    if LuaGuildTerritorialWarsMgr.m_SubmitLingQiInfo and LuaGuildTerritorialWarsMgr.m_SubmitLingQiInfo.levelInfo  then
        remainExchangeTimes = allExchangeTimes - LuaGuildTerritorialWarsMgr.m_SubmitLingQiInfo.todayExchangeFXYCount
    end
    local linqiCost = GuildTerritoryWar_RelatedPlaySetting.GetData().NumExchangeFengXueYu
    self.ExchangeTimesLabel.text = SafeStringFormat3(LocalString.GetString("今日剩余兑换次数 %s%d/%d"),remainExchangeTimes == 0 and "[FF0000]" or "[FFFFFF]", remainExchangeTimes, allExchangeTimes) 
    self.ExchangeCostLabel.text = SafeStringFormat3(LocalString.GetString("%d灵气兑换1个"),linqiCost) 
    self.ExchangeButton.Enabled = remainExchangeTimes > 0
    self.ExchangeButtonReddot.gameObject:SetActive(remainExchangeTimes > 0 and self.m_LinqiNum >= linqiCost)
end

function LuaGuildTerritorialWarsBeiZhanView:InitScoreView()
    local scoreLv = 0 -- 当前等级
    local scoreExp = 0 -- 当前经验
    local totalExp = 0 -- 总经验
    local scoreTotal = 0 -- 可获得总数
    local scoreCurrent = 0 -- 当前已获得数
    if LuaGuildTerritorialWarsMgr.m_SubmitLingQiInfo and LuaGuildTerritorialWarsMgr.m_SubmitLingQiInfo.levelInfo then
        local levelInfo = LuaGuildTerritorialWarsMgr.m_SubmitLingQiInfo.levelInfo
        local scoreInfo = levelInfo.scoreInfo
        scoreLv = scoreInfo.scoreLv 
        scoreExp = scoreInfo.scoreExp 
        scoreTotal = scoreInfo.total 
        scoreCurrent = scoreInfo.scoreCurrent 
        local data = GuildTerritoryWar_LingQi_Score.GetData(scoreLv)
        if data then
            totalExp = data.LevelUpLingQi
            if self.m_FxList[1] and ((self.m_ExpList[1] and scoreExp > self.m_ExpList[1]) or (self.m_LvList[1] and scoreLv > self.m_LvList[1])) then
                self.m_FxList[1].transform:Find("xishou").gameObject:SetActive(true)
                self.m_FxList[1].transform:Find("andi").gameObject:SetActive(scoreLv > self.m_LvList[1])
            end
            self.m_ExpList[1] = scoreExp
            self.m_LvList[1] = scoreLv
        end
    end
    
    local progress = totalExp == 0 and 0 or (scoreExp / totalExp)
    self.ScoreProgressLabel.text = SafeStringFormat3("%d%%", progress * 100)
    self.ScoreLabel.text = SafeStringFormat3("%d/%d",scoreCurrent, scoreTotal)
    self.ScoreProgress.value = progress
    self.ScoreSubmitBtn.Text = scoreCurrent == scoreTotal and LocalString.GetString("已达上限") or LocalString.GetString("注入灵气")
    self.ScoreSubmitBtn.Enabled = scoreCurrent < scoreTotal
end

function LuaGuildTerritorialWarsBeiZhanView:InitHpView()
    local hpLv = 0
    local hpExp = 0
    local totalExp = 0
    local hpTotal = 0
    local hpCurrent = 0
    if LuaGuildTerritorialWarsMgr.m_SubmitLingQiInfo and LuaGuildTerritorialWarsMgr.m_SubmitLingQiInfo.levelInfo then
        local levelInfo = LuaGuildTerritorialWarsMgr.m_SubmitLingQiInfo.levelInfo
        local hpInfo = levelInfo.hpInfo
        hpLv = hpInfo.hpLv
        hpExp = hpInfo.hpExp
        hpTotal = hpInfo.total
        hpCurrent = hpInfo.hpCurrent
        local data = GuildTerritoryWar_LingQi_HP.GetData(hpLv)
        if data then
            totalExp = data.LevelUpLingQi
            if self.m_FxList[2] and ((self.m_ExpList[2] and hpExp > self.m_ExpList[2]) or (self.m_LvList[2] and hpLv > self.m_LvList[2])) then
                self.m_FxList[2].transform:Find("xishou").gameObject:SetActive(true)
                self.m_FxList[2].transform:Find("andi").gameObject:SetActive(hpLv > self.m_LvList[2])
            end
            self.m_ExpList[2] = hpExp
            self.m_LvList[2] = hpLv
        end
    end
    local progress = totalExp == 0 and 0 or (hpExp / totalExp)
    self.HpProgressLabel.text = SafeStringFormat3("%d%%", progress * 100)
    self.HpLabel.text = SafeStringFormat3("%d/%s",hpCurrent, LocalString.GetString("1亿"))
    self.HpProgress.value = progress
    self.HpSubmitBtn.Text = hpTotal == hpCurrent and LocalString.GetString("已达上限") or LocalString.GetString("注入灵气")
    self.HpSubmitBtn.Enabled = hpCurrent < hpTotal
end

function LuaGuildTerritorialWarsBeiZhanView:InitBuffView()
    local buffLv = 0
    local buffExp = 0
    local totalExp = 0
    local buffTotal = 0
    local buffCurrent = 0
    if LuaGuildTerritorialWarsMgr.m_SubmitLingQiInfo and LuaGuildTerritorialWarsMgr.m_SubmitLingQiInfo.levelInfo then
        local levelInfo = LuaGuildTerritorialWarsMgr.m_SubmitLingQiInfo.levelInfo
        local buffInfo = levelInfo.buffInfo
        buffLv = buffInfo.buffLv
        buffExp = buffInfo.buffExp
        buffTotal = buffInfo.total
        buffCurrent = buffInfo.buffCurrent
        local data = GuildTerritoryWar_LingQi_Buff.GetData(buffLv)
        if data then
            totalExp = data.LevelUpLingQi
            if self.m_FxList[3] and ((self.m_ExpList[3] and buffExp > self.m_ExpList[3]) or (self.m_LvList[3] and buffLv > self.m_LvList[3])) then
                self.m_FxList[3].transform:Find("xishou").gameObject:SetActive(true)
                self.m_FxList[3].transform:Find("andi").gameObject:SetActive(buffLv > self.m_LvList[3])
            end
            self.m_ExpList[3] = buffExp
            self.m_LvList[3] = buffLv
        end
    end
    local progress = totalExp == 0 and 0 or (buffExp / totalExp)
    self.BuffProgressLabel.text = SafeStringFormat3("%d%%", progress * 100)
    self.BuffLabel.text = SafeStringFormat3("Lv.%d/Lv.%d",buffCurrent, buffTotal)
    self.BuffProgress.value = progress
    self.BuffSubmitBtn.Text = buffCurrent == buffTotal and LocalString.GetString("已达上限") or LocalString.GetString("注入灵气")
    self.BuffSubmitBtn.Enabled = buffCurrent < buffTotal
    Extensions.SetLocalPositionZ(self.BuffItemIcon.transform, buffCurrent == 0 and -1 or 0)
end

function LuaGuildTerritorialWarsBeiZhanView:OnEnable()
    g_ScriptEvent:AddListener("SendGTWSubmitLingQiInfo", self, "OnSendGTWSubmitLingQiInfo")
    Gac2Gas.RequestGTWSubmitLingQiInfo()
end

function LuaGuildTerritorialWarsBeiZhanView:OnDisable()
    g_ScriptEvent:RemoveListener("SendGTWSubmitLingQiInfo", self, "OnSendGTWSubmitLingQiInfo")
end

function LuaGuildTerritorialWarsBeiZhanView:OnSendGTWSubmitLingQiInfo(currentHaveLingQi, todayExchangeFXYCount, levelInfo)
    self.m_LinqiNum = LuaGuildTerritorialWarsMgr.m_SubmitLingQiInfo.currentHaveLingQi
    self.TopLabel.text = SafeStringFormat3(LocalString.GetString("当前拥有棋局灵气 %d"),self.m_LinqiNum) 
    for i = 1, #self.m_FxList do
        self.m_FxList[i].transform:Find("xishou").gameObject:SetActive(false)
        self.m_FxList[i].transform:Find("andi").gameObject:SetActive(false)
    end
    self:InitExchangeView()
    self:InitScoreView()
    self:InitHpView()
    self:InitBuffView()
end
--@region UIEvent

function LuaGuildTerritorialWarsBeiZhanView:OnExchangeButtonClick()
    local linqiCost = GuildTerritoryWar_RelatedPlaySetting.GetData().NumExchangeFengXueYu
    if self.m_LinqiNum < linqiCost then
        g_MessageMgr:ShowMessage("GuildTerritorialWars_NoneLinqi")
        return
    end
    Gac2Gas.ExchangeGTWRelatedPlayFXY()
end

function LuaGuildTerritorialWarsBeiZhanView:OnExchangeItemIconClick()
    CItemInfoMgr.ShowLinkItemTemplateInfo(GuildTerritoryWar_RelatedPlaySetting.GetData().FengXueYuItemId, false, nil, AlignType.Default, 0, 0, 0, 0) 
end

function LuaGuildTerritorialWarsBeiZhanView:OnScoreSubmitBtnClick()
    if self.m_LinqiNum == 0 then
        g_MessageMgr:ShowMessage("GuildTerritorialWars_NoneLinqi")
        return
    end
    CLuaNumberInputMgr.ShowNumInputBox(1, math.min(self.m_LinqiNum,999) , self.m_LinqiNum , function (val)
        Gac2Gas.ExchangeGTWRelatedPlayScore(val)
    end, LocalString.GetString("请输入要注入的棋局灵气数量"), -1)
end

function LuaGuildTerritorialWarsBeiZhanView:OnHpSubmitBtnClick()
    if self.m_LinqiNum == 0 then
        g_MessageMgr:ShowMessage("GuildTerritorialWars_NoneLinqi")
        return
    end
    CLuaNumberInputMgr.ShowNumInputBox(1, math.min(self.m_LinqiNum,999) , self.m_LinqiNum , function (val)
        Gac2Gas.ExchangeGTWRelatedPlayCityMonsterHp(val)
    end, LocalString.GetString("请输入要注入的棋局灵气数量"), -1)
end

function LuaGuildTerritorialWarsBeiZhanView:OnBuffSubmitBtnClick()
    if self.m_LinqiNum == 0 then
        g_MessageMgr:ShowMessage("GuildTerritorialWars_NoneLinqi")
        return
    end
    CLuaNumberInputMgr.ShowNumInputBox(1, math.min(self.m_LinqiNum,999) , self.m_LinqiNum , function (val)
        Gac2Gas.ExchangeGTWRelatedPlayBuff(val)
    end, LocalString.GetString("请输入要注入的棋局灵气数量"), -1)
end

function LuaGuildTerritorialWarsBeiZhanView:OnScoreItemIconClick()
    g_MessageMgr:ShowMessage("GuildTerritorialWarsBeiZhanView_ScoreTip")
end

function LuaGuildTerritorialWarsBeiZhanView:OnHpItemIconClick()
    g_MessageMgr:ShowMessage("GuildTerritorialWarsBeiZhanView_HpTip")
end

function LuaGuildTerritorialWarsBeiZhanView:OnBuffItemIconClick()
    g_MessageMgr:ShowMessage("GuildTerritorialWarsBeiZhanView_BuffTip")
end


--@endregion UIEvent

