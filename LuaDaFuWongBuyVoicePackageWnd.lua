local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CButton = import "L10.UI.CButton"
local UILabel = import "UILabel"

LuaDaFuWongBuyVoicePackageWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDaFuWongBuyVoicePackageWnd, "OKBtn", "OKBtn", CButton)
RegistChildComponent(LuaDaFuWongBuyVoicePackageWnd, "CancelBtn", "CancelBtn", CButton)
RegistChildComponent(LuaDaFuWongBuyVoicePackageWnd, "Label", "Label", UILabel)
RegistChildComponent(LuaDaFuWongBuyVoicePackageWnd, "QnCostAndOwnMoney", "QnCostAndOwnMoney", CCurentMoneyCtrl)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongBuyVoicePackageWnd, "m_Id")
function LuaDaFuWongBuyVoicePackageWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.OKBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOKBtnClick()
	end)


	
	UIEventListener.Get(self.CancelBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCancelBtnClick()
	end)


    --@endregion EventBind end
end

function LuaDaFuWongBuyVoicePackageWnd:Init()
	if not LuaDaFuWongMgr.CurBuyVoicePackageId then return end
	self.m_Id = LuaDaFuWongMgr.CurBuyVoicePackageId
	local data = DaFuWeng_VoicePackage.GetData(self.m_Id)
	if not data then return end
	self.Label.text = g_MessageMgr:FormatMessage("DaFuWeng_BuyVoicePackage_Confirm",data.Title,data.Duration)
	self.QnCostAndOwnMoney:SetCost(data.JadeCost)
end

--@region UIEvent

function LuaDaFuWongBuyVoicePackageWnd:OnOKBtnClick()
	-- if CClientMainPlayer.Inst then
	-- 	if CClientMainPlayer.Inst.Jade + CClientMainPlayer.Inst.BindJade < cost then
	-- 		MessageWndManager.ShowOKCancelMessage(LocalString.GetString("灵玉不足，是否前往充值？"), DelegateFactory.Action(function ()
	-- 			CShopMallMgr.ShowChargeWnd()
	-- 		end), nil, nil, nil, false)
	-- 	else
	-- 		Gac2Gas.RequestBuyDaFuWengLevel(level)
	-- 	end
	-- end
	Gac2Gas.RequestBuyDaFuWengVoicePackage(self.m_Id)
end

function LuaDaFuWongBuyVoicePackageWnd:OnCancelBtnClick()
	CUIManager.CloseUI(CLuaUIResources.DaFuWongBuyVoicePackageWnd)
end

--@endregion UIEvent

function LuaDaFuWongBuyVoicePackageWnd:OnEnable()
	g_ScriptEvent:AddListener("OnTongXingZhengDataUpdate", self, "OnCancelBtnClick")
end

function LuaDaFuWongBuyVoicePackageWnd:OnDisable()
	g_ScriptEvent:AddListener("OnTongXingZhengDataUpdate", self, "OnCancelBtnClick")
end