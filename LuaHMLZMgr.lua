local CScene=import "L10.Game.CScene"
local CActivityMgr = import "L10.Game.CActivityMgr"
LuaHMLZMgr = {}

LuaHMLZMgr.m_State = 0
LuaHMLZMgr.m_Count = 0

function LuaHMLZMgr:SyncPlayState(playState, progressTbl_U)
    local count = 0
    CommonDefs.DictIterate(MsgPackImpl.unpack(progressTbl_U), DelegateFactory.Action_object_object(function (___key, ___value) 
        count = count + ___value
    end))
    self.m_Count = count

    self:UpdateActivity(playState)
    self.m_State = playState
    g_ScriptEvent:BroadcastInLua("ZYJ2021HMLZ_SyncPlayState", playState, self.m_Count)
end

function LuaHMLZMgr:IsInPlay2021()
	if CScene.MainScene then
        local GameplayId = 51102185
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        if gamePlayId == GameplayId then
           return true
        end
    end
    return false
end

function LuaHMLZMgr:UpdateActivity(playState)
    local Idle = 1000
    local Stage_1 = 1001
	local Stage_2 = 1002
    if self.m_State == Stage_1 then
        CActivityMgr.Inst:DeleteActivity(self.m_TaskTable[1].Title)
    end
    if self.m_State == Stage_2 then
        CActivityMgr.Inst:DeleteActivity(self.m_TaskTable[2].Title)
    end
    if playState == Stage_1 or playState == Stage_2 then
        LuaHMLZMgr.m_TaskTable = {
            {Title = LocalString.GetString("魂梦流转")  .. ' '.. g_MessageMgr:FormatMessage("2021ZYJ_HMLZ_Title1"), Description = g_MessageMgr:FormatMessage("2021ZYJ_HMLZ_Description1")},
            {Title = LocalString.GetString("魂梦流转") .. ' ' .. g_MessageMgr:FormatMessage("2021ZYJ_HMLZ_Title2"), Description = g_MessageMgr:FormatMessage("2021ZYJ_HMLZ_Description2")}
        }
        local stage = (playState == Stage_2) and 2 or 1
        local onclick = DelegateFactory.Action(function()
            g_MessageMgr:ShowMessage("ZYJ2021HMLZ_RULE")
        end)
        CActivityMgr.Inst:UpdateAcitivity(self.m_TaskTable[stage].Title, self.m_TaskTable[stage].Description, onclick, 0);
    end
end

function LuaHMLZMgr:Leave()
    LuaHMLZMgr:UpdateActivity(0)
end



