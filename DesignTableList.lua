local CommonDefs = import "L10.Game.CommonDefs"
local CDesignData = import "CDesignData"
local s_BinaryDesignDataList = {
	"DesignBinary/Character/Initialization",
    "DesignBinary/NPC/NPC_1",
    "DesignBinary/NPC/NPC_2",
    "DesignBinary/NPC/NPC_3",
    "DesignBinary/NPC/Monster_1",
    "DesignBinary/NPC/Monster_2",
    "DesignBinary/NPC/Monster_3",
    "DesignBinary/NPC/Pet",
    "DesignBinary/NPC/Pick",
    "DesignBinary/NPC/Temple",
	"DesignBinary/NPC/Platform",
    "DesignBinary/Equipment/GemGroup",
    "DesignBinary/Equipment/Jewel",
    "DesignBinary/Status/Buff_1",
    "DesignBinary/Status/Buff_2",
    "DesignBinary/Status/Buff_3",
    "DesignBinary/Status/Buff_4",
    "DesignBinary/Status/Buff_5",
    "DesignBinary/Status/Skill_1",
    "DesignBinary/Status/Skill_2",
    "DesignBinary/Status/Skill_3",
    "DesignBinary/Status/Skill_4",
    "DesignBinary/Status/Skill_5",
    "DesignBinary/Status/Skill_6",
    "DesignBinary/Status/Skill_7",
    "DesignBinary/Status/Fx",
    "DesignBinary/Status/Relive",
    "DesignBinary/Status/Flag",
    "DesignBinary/Status/Bullet",
    "DesignBinary/Status/Expression",
    "DesignBinary/Map/PublicMap",
    "DesignBinary/Character/Animation",
    "DesignBinary/Character/RandomName",
    "DesignBinary/Character/Experience",
    "DesignBinary/GamePlay/GuanNing",
    -- "DesignBinary/Fighting/Action",
    "DesignBinary/GamePlay/PersonalSpace",
    "DesignBinary/Message/Dialog_1",
    "DesignBinary/Message/Dialog_2",
    "DesignBinary/Message/Dialog_3",
    "DesignBinary/Message/Dialog_4",
    "DesignBinary/Message/Dialog_5",
    "DesignBinary/Equipment/EquipmentTemplate",
    "DesignBinary/Equipment/PreciousEquipment",
    "DesignBinary/Item/Item_1",
    "DesignBinary/Item/Item_2",
    "DesignBinary/Item/Item_3",
    "DesignBinary/Item/Item_4",
    "DesignBinary/Item/Item_5",
    "DesignBinary/Item/ItemGet",
    "DesignBinary/Equipment/EquipBaptize",
    "DesignBinary/Equipment/Word",
    "DesignBinary/Message/Message_1",
    "DesignBinary/Message/Message_2",
    "DesignBinary/Message/Message_3",
    "DesignBinary/Message/Message_4",
    "DesignBinary/Message/Message_5",
    "DesignBinary/System/Rank",
    "DesignBinary/System/Title",
    "DesignBinary/GamePlay/Story",
    "DesignBinary/GamePlay/MenPaiTiaoZhan",
    "DesignBinary/GamePlay/QianDao",
    "DesignBinary/GamePlay/KaiFuActivity",
    "DesignBinary/GamePlay/HolidayOnlineTime",
    "DesignBinary/GamePlay/ZhouMoActivity",
    "DesignBinary/GamePlay/GuildFreight",
    "DesignBinary/GamePlay/WaBao",
    "DesignBinary/Task/Task_1",
    "DesignBinary/Task/Task_2",
    "DesignBinary/Task/Task_3",
    "DesignBinary/Task/Task_4",
    "DesignBinary/Task/Task_5",
    "DesignBinary/GamePlay/GamePlay",
    "DesignBinary/GamePlay/Question",
    "DesignBinary/Item/Mall",
    "DesignBinary/NPC/LingShou",
    "DesignBinary/Character/GuaJi",
    "DesignBinary/Status/LifeSkill",
    "DesignBinary/Item/Charge",
    "DesignBinary/Status/Aureola",
    "DesignBinary/Equipment/EquipIntensify",
    "DesignBinary/Item/Shop",
    "DesignBinary/System/PlayerCapacity",
    "DesignBinary/System/Fashion",
    "DesignBinary/GamePlay/TeamMatch",
    "DesignBinary/Guide/GuideConfig",
    "DesignBinary/Status/Practice",
    "DesignBinary/GamePlay/KeJu",
    "DesignBinary/System/PlayerShop",
    "DesignBinary/Message/NameReserve",
    "DesignBinary/Status/GameVideo",
    "DesignBinary/Guide/Guide",
    "DesignBinary/GamePlay/Achievement",
    "DesignBinary/Message/WordFilter",
    "DesignBinary/System/Wing",
    "DesignBinary/GamePlay/GuildLeague",
    "DesignBinary/Equipment/Talisman",
    "DesignBinary/Equipment/ExtraEquip",
    "DesignBinary/Item/GameplayItem",
    "DesignBinary/Message/Mail",
    "DesignBinary/System/Minimap",
    "DesignBinary/System/Quality",
    "DesignBinary/System/Promotion",
    "DesignBinary/Message/ChatBot",
    "DesignBinary/Character/Grow",
    "DesignBinary/System/Guild",
    "DesignBinary/System/BaZi",
    "DesignBinary/System/GameSetting",
    "DesignBinary/Status/Conflict",
    "DesignBinary/Map/CrossMapPath",
    "DesignBinary/Map/Location",
    "DesignBinary/GamePlay/YinLiangDuoBao",
    "DesignBinary/GamePlay/BiWu",
    "DesignBinary/GamePlay/BiWuCross",
    "DesignBinary/GamePlay/Wedding",
    "DesignBinary/GamePlay/HongBao",
    "DesignBinary/GamePlay/ShiTu",
    "DesignBinary/GamePlay/ShiTuTask",
    "DesignBinary/System/ZuoQi",
    "DesignBinary/System/ZengSong",
    "DesignBinary/System/MultipleExp",
    "DesignBinary/System/AccountTransfer",
    "DesignBinary/GamePlay/ZhongYuanJie",
    "DesignBinary/GamePlay/QiXi",
    "DesignBinary/GamePlay/Huiliu",
    "DesignBinary/GamePlay/PlayConfig",
    "DesignBinary/GamePlay/ChuangGuan",
    "DesignBinary/Status/AntiExchange",
    "DesignBinary/GamePlay/BangHua",
    "DesignBinary/GamePlay/GuildChallenge",
    "DesignBinary/GamePlay/GuildOccupationWar",
    "DesignBinary/System/WinShortCutKey",
    "DesignBinary/GamePlay/JieBai",
    "DesignBinary/System/Transform",
    "DesignBinary/GamePlay/DuoHun",
    "DesignBinary/GamePlay/CuJu",
    "DesignBinary/System/Paimai",
    "DesignBinary/Item/ItemBag",
    "DesignBinary/GamePlay/ExpressionHead",
    "DesignBinary/System/Zhuangshiwu",
    "DesignBinary/System/ProfessionTransfer",
    "DesignBinary/GamePlay/FamilyTree",
    "DesignBinary/GamePlay/ZhaoQin",
    "DesignBinary/GamePlay/YiTiaoLong",
    "DesignBinary/System/Seal",
    "DesignBinary/System/Exchange",
    "DesignBinary/System/House",
    "DesignBinary/System/QianKunDai",
    "DesignBinary/GamePlay/DouHunTan",
    "DesignBinary/GamePlay/YuanXiao",
    "DesignBinary/GamePlay/ChunJie",
    "DesignBinary/GamePlay/DaoJuLiBao",
    "DesignBinary/GamePlay/YuanDan",
    "DesignBinary/GamePlay/HouseOfYangyang",
    "DesignBinary/GamePlay/DouHunCross",
    "DesignBinary/GamePlay/ShengDan",
    "DesignBinary/GamePlay/Hanjiahuodong",
    "DesignBinary/GamePlay/Gamble",
    "DesignBinary/System/ChuanjiabaoModel",
    "DesignBinary/GamePlay/Valentine",
    "DesignBinary/GamePlay/ChuJia",
    "DesignBinary/System/SecondaryPassword",
    "DesignBinary/GamePlay/QingMingJiJiu",
    "DesignBinary/GamePlay/GaoChang",
    "DesignBinary/System/Shimin",
    "DesignBinary/GamePlay/Subtitle",
    "DesignBinary/GamePlay/QingQiuPlay",
    "DesignBinary/GamePlay/Divination",
    "DesignBinary/GamePlay/TaoHuaBaoXia",
    "DesignBinary/GamePlay/DuoMaoMao",
    "DesignBinary/GamePlay/ZhuJueJuQing",
    "DesignBinary/GamePlay/ZhouNianQing",
    "DesignBinary/GamePlay/LiuYi",
    "DesignBinary/GamePlay/TowerDefense",
    "DesignBinary/GamePlay/TianGongGeShip",
    "DesignBinary/System/Kefuzhuanqu",
    "DesignBinary/System/VoiceAssistant",
    "DesignBinary/NPC/LingShouBaby",
    "DesignBinary/GamePlay/JiangXueYuanShuang",
    "DesignBinary/GamePlay/ShuJia",
    "DesignBinary/GamePlay/JueDou",
    "DesignBinary/GamePlay/QuanMinPK",
    "DesignBinary/GamePlay/XiaLvPK",
    "DesignBinary/GamePlay/WeddingDay",
    "DesignBinary/GamePlay/ChaoDu",
    "DesignBinary/GamePlay/MaPiTengYunSai",
    "DesignBinary/GamePlay/HuaKui",
    "DesignBinary/System/GuildStatue",
    "DesignBinary/GamePlay/HousePuppet",
    "DesignBinary/GamePlay/JiaNianHua",
    "DesignBinary/GamePlay/Concert",
    "DesignBinary/GamePlay/ZhongQiu",
    "DesignBinary/GamePlay/HuluBrothers",
    "DesignBinary/GamePlay/GuGuMiGong",
    "DesignBinary/GamePlay/LianLianKan",
    "DesignBinary/GamePlay/MapiMysticalShop",
    "DesignBinary/GamePlay/PaiZhao",
    "DesignBinary/GamePlay/JiayuanJiyu",
    "DesignBinary/GamePlay/TeamRecruit",
    "DesignBinary/System/Sanjiefengyun",
    "DesignBinary/GamePlay/LiaoLuoWanTraining",
    "DesignBinary/GamePlay/XueQiuDaZhan",
    "DesignBinary/System/Xingguan",
    "DesignBinary/GamePlay/FeiSheng",
    "DesignBinary/GamePlay/HuYiTianHouse",
    "DesignBinary/GamePlay/Huabi",
    "DesignBinary/Equipment/EquipHole",
    "DesignBinary/GamePlay/HouseCompetition",
    "DesignBinary/GamePlay/TianQiMysticalShop",
    "DesignBinary/GamePlay/XianZongShan",
    "DesignBinary/GamePlay/ZhiYuan",
    "DesignBinary/GamePlay/DouDiZhu",
    "DesignBinary/GamePlay/ShiJieShiJian",
    "DesignBinary/GamePlay/BingQiPu",
    "DesignBinary/GamePlay/ShenBingChuShi",
    "DesignBinary/Status/IdentifySkill",
    "DesignBinary/Equipment/ShenBing",
    "DesignBinary/GamePlay/QingLiangYiXia",
    "DesignBinary/GamePlay/GongZhongHao",
    "DesignBinary/System/Cangbaoge",
    "DesignBinary/GamePlay/ServerSubmit",
    "DesignBinary/GamePlay/CityWar",
    "DesignBinary/GamePlay/GuoQingJiaoChang",
    "DesignBinary/NPC/Baby",
    "DesignBinary/Status/LianShen",
    "DesignBinary/GamePlay/YaYun",
    "DesignBinary/GamePlay/MonsterSiege",
    "DesignBinary/NPC/LingShouPartner",
    "DesignBinary/GamePlay/YuanDan2019",
    "DesignBinary/GamePlay/YuanDan2020",
    "DesignBinary/GamePlay/TianMenShanBattle",
    "DesignBinary/GamePlay/WinterHouse",
    "DesignBinary/GamePlay/YangYuShaoNianQi",
    "DesignBinary/GamePlay/MengHuaLu",
    "DesignBinary/GamePlay/NewYear2019",
    "DesignBinary/GamePlay/Valentine2019",
    "DesignBinary/GamePlay/FriendCircle",
    "DesignBinary/GamePlay/StarBiWuShow",
    "DesignBinary/GamePlay/DaShen",
    "DesignBinary/GamePlay/QingMing2019",
    "DesignBinary/GamePlay/QTE",
    "DesignBinary/GamePlay/ZhuSiDong",
    "DesignBinary/GamePlay/XianZhi",
    "DesignBinary/GamePlay/WuYi",
    "DesignBinary/GamePlay/TeamReverse",
    "DesignBinary/GamePlay/NpcHaoGanDu",
    "DesignBinary/GamePlay/LiangHao",
    "DesignBinary/GamePlay/NeteaseMember",
    "DesignBinary/GamePlay/LiuYi2019",
    "DesignBinary/System/AttrGroup",
    "DesignBinary/GamePlay/DuanWu",
    "DesignBinary/GamePlay/FestivalGift",
    "DesignBinary/GamePlay/ZhongCao",
    "DesignBinary/Map/CityReplaceHelper",
    "DesignBinary/Map/CityReplaceHelperGen",
    "DesignBinary/Task/CityReplaceHelperGenTask_1",
    "DesignBinary/Task/CityReplaceHelperGenTask_2",
    "DesignBinary/Task/CityReplaceHelperGenTask_3",
		"DesignBinary/Task/CityReplaceHelperGenTask_4",
		"DesignBinary/Task/CityReplaceHelperGenTask_5",
		"DesignBinary/GamePlay/RoleDrawingCard",
		"DesignBinary/GamePlay/LiaoLuoWan",
		"DesignBinary/GamePlay/WorldCup",
		"DesignBinary/GamePlay/ShouXiDaDiZi",
		"DesignBinary/System/MedicineBox",
		"DesignBinary/Map/ScenesItem",
		"DesignBinary/GamePlay/MergeBattle",
		"DesignBinary/GamePlay/UnintermittentHell",
		"DesignBinary/GamePlay/ZhongQiu2019",
		"DesignBinary/GamePlay/GuildDining",
		"DesignBinary/GamePlay/GuoQingXianLi",
		"DesignBinary/System/BindRepo",
		"DesignBinary/GamePlay/Halloween",
		"DesignBinary/GamePlay/SinglesDay2019",
		"DesignBinary/GamePlay/Authentication",
		"DesignBinary/GamePlay/QiLinDong",
		"DesignBinary/GamePlay/Welfare",
		"DesignBinary/GamePlay/SanXingBattle",
    "DesignBinary/GamePlay/HanJia2020",
    "DesignBinary/GamePlay/SchoolContribution",
    "DesignBinary/GamePlay/NewbieSchoolTask",
    "DesignBinary/GamePlay/ChunJie2020",
    "DesignBinary/GamePlay/HuiGuiJieBan",
		"DesignBinary/GamePlay/Valentine2020",
		"DesignBinary/System/RanFaJi",
		"DesignBinary/GamePlay/JiuGeShiHun",
		"DesignBinary/GamePlay/LiuYi2020",
		"DesignBinary/GamePlay/ShiTuCultivate",
		"DesignBinary/GamePlay/ChiJi",
    "DesignBinary/GamePlay/DuanWu2020",
    "DesignBinary/GamePlay/Duanwu2021",
    "DesignBinary/GamePlay/WuYi2020",
    "DesignBinary/GamePlay/ButterflyCrisis",
    "DesignBinary/GamePlay/WorldButterfly2020",
    "DesignBinary/GamePlay/ZhanLong",
    "DesignBinary/GamePlay/Spokesman",
    "DesignBinary/GamePlay/SpokesmanTCG",
    "DesignBinary/GamePlay/Double11",
    "DesignBinary/GamePlay/Halloween2020",
    "DesignBinary/GamePlay/HuiLiuNew",
    "DesignBinary/GamePlay/PlayReport",
    "DesignBinary/GamePlay/AnQiIsland",
    "DesignBinary/GamePlay/YanHua",
    "DesignBinary/GamePlay/HanJia2021",
    "DesignBinary/GamePlay/Yuanxiao2021",
    "DesignBinary/GamePlay/Chunjie2021",
    "DesignBinary/System/Menpai",
    "DesignBinary/GamePlay/ZhuoYao",
    "DesignBinary/Status/SoulCore",
    "DesignBinary/GamePlay/OffWorldPass",
    "DesignBinary/GamePlay/WorldEvent",
    "DesignBinary/System/LianHua",
    "DesignBinary/GamePlay/XinFu",
    "DesignBinary/GamePlay/ShiMenShouWei",
	  "DesignBinary/System/SectInviteNpc",
    "DesignBinary/GamePlay/QingMing2021",
    "DesignBinary/System/FuXiLog",
    "DesignBinary/System/FuXiDance",
    "DesignBinary/System/Version",
    "DesignBinary/GamePlay/WuYi2021",
    "DesignBinary/GamePlay/ShengChenGang",
    "DesignBinary/GamePlay/LiuYi2021",	
    "DesignBinary/GamePlay/HouseFish",
    "DesignBinary/GamePlay/ShuiGuoPaiDui",
    "DesignBinary/GamePlay/HaiDiaoFuBen",
    "DesignBinary/GamePlay/ShowPicture",
    "DesignBinary/GamePlay/ShuJia2021",
    "DesignBinary/GamePlay/LaBa",
    "DesignBinary/GamePlay/OlympicGameActivity",
    "DesignBinary/GamePlay/Halloween2021",
    "DesignBinary/GamePlay/Halloween2022",
    "DesignBinary/GamePlay/Arena",
    "DesignBinary/GamePlay/LuoChaHaiShi",
    "DesignBinary/GamePlay/HouseFishShop",
    "DesignBinary/NPC/NPCChat",
    "DesignBinary/GamePlay/SectItemRedPack",
	  "DesignBinary/GamePlay/ZhaiXing",
    "DesignBinary/GamePlay/WuMenHuaShi",
    "DesignBinary/GamePlay/GuildLeagueCross",
    "DesignBinary/GamePlay/ScrollTask",
    "DesignBinary/GamePlay/WeddingIteration",
    "DesignBinary/GamePlay/YuanDan2022",
    "DesignBinary/GamePlay/Christmas2021",
    "DesignBinary/GamePlay/ShangShi",
    "DesignBinary/GamePlay/SchoolRebuilding",
    "DesignBinary/GamePlay/HanJia2022",
    "DesignBinary/GamePlay/WinterOlympic",
    "DesignBinary/GamePlay/GuildTerritoryWar",
    "DesignBinary/GamePlay/ShengXiaoCard",
    "DesignBinary/System/Report",
    "DesignBinary/Status/SkillAppearance",
    "DesignBinary/GamePlay/QingMing2022",
    "DesignBinary/GamePlay/MeiXiangLou",
    "DesignBinary/GamePlay/Business",
    "DesignBinary/GamePlay/GaoChangCross",
    "DesignBinary/GamePlay/XinBaiLianDong",
    "DesignBinary/GamePlay/ZhanLing",
    "DesignBinary/System/BeginnerGuide",
    "DesignBinary/GamePlay/NavalWar",
    "DesignBinary/GamePlay/QiXi2022",
    "DesignBinary/GamePlay/ShuJia2022",
    "DesignBinary/System/Instrument",
    "DesignBinary/GamePlay/VoiceAchievement",
    "DesignBinary/GamePlay/ZhongQiu2022",
    "DesignBinary/GamePlay/WorldCup2022",
    "DesignBinary/GamePlay/GuoQing2022",
    "DesignBinary/System/Physics",
    "DesignBinary/GamePlay/GuildCustomBuildPreTask",
    "DesignBinary/GamePlay/YuanDan2023",
    "DesignBinary/GamePlay/HanJia2023",
    "DesignBinary/GamePlay/HanJia2024",
    "DesignBinary/GamePlay/PengDaoFuYao",
	  "DesignBinary/GamePlay/Christmas2022",
    "DesignBinary/GamePlay/YuanXiao2023",
    "DesignBinary/Status/CameraDef",
    "DesignBinary/GamePlay/Valentine2023",
    "DesignBinary/Character/PropGuide",
    "DesignBinary/GamePlay/GaoChangJiDou",
    "DesignBinary/GamePlay/DaFuWeng",
    "DesignBinary/GamePlay/NanDuFanHua",
    "DesignBinary/GamePlay/ZhouNianQing2023",
    "DesignBinary/GamePlay/QingMing2023",
    "DesignBinary/GamePlay/Duanwu2023",
    "DesignBinary/GamePlay/WuYi2023",
    "DesignBinary/GamePlay/LiYuZhangDaiFood",
    "DesignBinary/System/CompetitionHonor",
    "DesignBinary/System/StoreRoom",
    "DesignBinary/System/Wardrobe",
    "DesignBinary/Character/PinchFace",
	  "DesignBinary/GamePlay/LiuYi2023",
	  "DesignBinary/GamePlay/ShuJia2023",
	  "DesignBinary/GamePlay/Pass",
	  "DesignBinary/GamePlay/GuildPinTu",
	  "DesignBinary/GamePlay/FashionLottery",
	  "DesignBinary/GamePlay/GuoQing2023",
    "DesignBinary/GamePlay/AsianGames2023",
    "DesignBinary/GamePlay/Carnival2023",
    "DesignBinary/GamePlay/YaoYeManJuan",
    "DesignBinary/GamePlay/FestivalHelper",
    "DesignBinary/GamePlay/HengYuDangKou",
    "DesignBinary/GamePlay/LuoCha2023",
    "DesignBinary/System/CreditScore",
    "DesignBinary/GamePlay/CaiweiJixing",
    "DesignBinary/GamePlay/QiFuShouCai",
	  "DesignBinary/GamePlay/Halloween2023",
    "DesignBinary/GamePlay/Christmas2023",
    "DesignBinary/GamePlay/YuanDan2024",
	"DesignBinary/GamePlay/ChunJie2024",
    "DesignBinary/GamePlay/Valentine2024",
}
if CommonDefs.IS_HMT_CLIENT then
	table.insert(s_BinaryDesignDataList, "DesignBinary/GamePlay/FacebookShare")
end

CDesignData.s_BinaryDesignDataList = Table2Array(s_BinaryDesignDataList, MakeArrayClass(cs_string))
s_BinaryDesignDataList = nil
