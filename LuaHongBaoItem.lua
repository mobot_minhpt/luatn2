local EnumRequestSource = import "L10.UI.EnumRequestSource"
local EnumHongbaoType = import "L10.UI.EnumHongbaoType"
local CHongBaoMgr = import "L10.UI.CHongBaoMgr"
local QnLabel=import "L10.UI.QnLabel"
local EnumPlayerInfoContext=import "L10.Game.EnumPlayerInfoContext"
local CPlayerInfoMgr=import "CPlayerInfoMgr"
local AlignType = import "CPlayerInfoMgr+AlignType"
local EChatPanel=import "L10.Game.EChatPanel"

CLuaHongBaoItem = class()
RegistChildComponent(CLuaHongBaoItem, "m_OwnerNameLabel", QnLabel)
RegistChildComponent(CLuaHongBaoItem, "m_ContentLabel", UILabel)
RegistChildComponent(CLuaHongBaoItem, "m_SnatchedLabel", QnLabel)
RegistChildComponent(CLuaHongBaoItem, "m_SnatchingTexture", UITexture)
RegistChildComponent(CLuaHongBaoItem, "m_DetailLabel", QnLabel)
RegistChildComponent(CLuaHongBaoItem, "m_BgTex", CUITexture)
RegistChildComponent(CLuaHongBaoItem, "m_MoreContentObj", GameObject)
RegistChildComponent(CLuaHongBaoItem, "m_VoiceFlagObj", GameObject)

RegistClassMember(CLuaHongBaoItem,"m_HongbaoId")
RegistClassMember(CLuaHongBaoItem,"m_OwnerId")
RegistClassMember(CLuaHongBaoItem,"m_GiftChannelType")
RegistClassMember(CLuaHongBaoItem,"m_Event")
RegistClassMember(CLuaHongBaoItem,"m_HongbaoInfo")
RegistClassMember(CLuaHongBaoItem,"m_HongBaoCoverType")

function CLuaHongBaoItem:Awake()
--self.m_HongbaoId = ?
--self.m_OwnerId = ?
--self.m_GiftChannelType = ?
--self.m_Event = ?
--self.m_HongbaoInfo = ?

    if self.m_MoreContentObj then
        self.m_MoreContentObj:SetActive(false)
    end
    if self.m_VoiceFlagObj then
        self.m_VoiceFlagObj:SetActive(false)
    end
    self.m_HongBaoCoverType = 0
end

function CLuaHongBaoItem:OnEnable( )
    if self.m_DetailLabel ~= nil then
        UIEventListener.Get(self.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            CLuaHongBaoMgr.ShowHongBaoDetailWnd(self.m_HongbaoId,self.m_Event,self.m_HongBaoCoverType)
        end)
        UIEventListener.Get(self.m_OwnerNameLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            CPlayerInfoMgr.ShowPlayerPopupMenu(
                self.m_OwnerId,
                EnumPlayerInfoContext.ChatLink,
                EChatPanel.Undefined,
                nil,
                nil,
                self.m_OwnerNameLabel.transform:TransformPoint(Vector3(400,45,0)),
                AlignType.Bottom)
        end)
    else
        UIEventListener.Get(self.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            self:SnatchHongbao(go)
        end)
    end
end
function CLuaHongBaoItem:SnatchHongbao( go)
    CHongBaoMgr.m_RequestSource = EnumRequestSource.AutoRequest
    if CHongBaoMgr.m_HongbaoType == EnumHongbaoType.Gift then
        Gac2Gas.RequestSnatchDaoJuLiBao(self.m_HongbaoId, self.m_GiftChannelType)
        Gac2Gas.RequestRecentDaoJuLiBaoInfo()
    elseif CHongBaoMgr.m_HongbaoType == EnumHongbaoType.SectHongbao then
        LuaZongMenMgr.m_SectHongBaoId = self.m_HongbaoId
        print("RequestSectItemRedPackDetail")
        Gac2Gas.RequestSectItemRedPackDetail(self.m_HongbaoId)
        return
    else
        if self.m_HongbaoInfo.m_IsVoiceType then
            CLuaHongBaoMgr.SnatchVoiceHongbao(self.m_HongbaoInfo)
            return
        end
        Gac2Gas.RequestSnatchHongBao(self.m_HongbaoId, EnumToInt(CHongBaoMgr.m_HongbaoType), "")
        Gac2Gas.RequestRecentHongBaoInfoV2(EnumToInt(CHongBaoMgr.m_HongbaoType))
    end
    self:ShowHongbaoDetail(nil)
end
function CLuaHongBaoItem:ShowHongbaoDetail(go)
    CLuaHongBaoMgr.ShowHongBaoDetailWnd(self.m_HongbaoId,self.m_Event,self.m_HongBaoCoverType)
end
function CLuaHongBaoItem:UpdateData( hongbao)
    self.m_HongBaoCoverType = hongbao.m_HongBaoCoverType or 0
    if CHongBaoMgr.m_HongbaoType ~= EnumHongbaoType.Gift then
        local texPath = CHongBaoMgr.Inst:GetHongBaoIconByAmount(hongbao.m_HongbaoAmount, true,self.m_HongBaoCoverType)
        if CLuaHongBaoMgr.IsShengDan() then
            --圣诞期间显示另一种样式
			local part1, part2 = string.match(texPath, "([^%.]+)%.([^%.]+)")
            texPath = part1 .. "_ShengDan." .. part2
        end

        if hongbao.m_Status == 1 then--EnumHongBaoStatus_lua.Snatching then
            self.m_BgTex:LoadMaterial(texPath, false, false)
        else
			-- local part1, part2 = string.match(texPath, "([^%.]+)%.([^%.]+)")
            -- if part1 and part2 then
            --     self.m_BgTex:LoadMaterial(part1 .. "_01." .. part2)
            -- end
            self.m_BgTex:LoadMaterial(CLuaHongBaoMgr.GetOpenHongBaoIconByAmount(hongbao.m_HongbaoAmount,self.m_HongBaoCoverType), false, false)
        end
    end
    self.m_HongbaoInfo = hongbao
    self.m_OwnerNameLabel.Text = hongbao.m_OwnerName
    self.m_ContentLabel.text = hongbao.m_Content

    if self.m_VoiceFlagObj ~= nil then
        self.m_VoiceFlagObj:SetActive(hongbao.m_IsVoiceType)
    end

    if self.m_MoreContentObj ~= nil then
        local str
        local default
        default, str = self.m_ContentLabel:Wrap(hongbao.m_Content)

		local part1, part2, part3, part4 = string.match(str, "([^\n]+)\n([^\n]+)\n([^\n]+)\n([^\n]+)")
		if part1 and part2 and part3 and part4 then
			self.m_MoreContentObj:SetActive(true)
		end
    end

    self.m_HongbaoId = hongbao.m_HongbaoId
    self.m_OwnerId = hongbao.m_OwnerId
    self.m_GiftChannelType = hongbao.m_ChannelType
    self.m_Event = hongbao.m_EventType
    repeat
        local extern = hongbao.m_Status
        if extern == 1 then--EnumHongBaoStatus_lua.Snatching then
            break
        elseif extern == 2 then--EnumHongBaoStatus_lua.Snatched then
            self.m_SnatchedLabel.Text = LocalString.GetString("已抢过")
            break
        elseif extern == 3 then--EnumHongBaoStatus_lua.SnatchEnd then
            self.m_SnatchedLabel.Text = LocalString.GetString("已抢光")
            break
        elseif extern == 4 then--EnumHongBaoStatus_lua.Expired then
            self.m_SnatchedLabel.Text = LocalString.GetString("已过期")
            break
        else
            break
        end
    until 1
end
