-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CItemComposeMgr = import "L10.UI.CItemComposeMgr"
local CItemComposeWnd = import "L10.UI.CItemComposeWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local CPackageView = import "L10.UI.CPackageView"
local EnumComposeItemType = import "L10.UI.EnumComposeItemType"
local EnumEventType = import "EnumEventType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EventManager = import "EventManager"
local Int32 = import "System.Int32"
local Item_Item = import "L10.Game.Item_Item"
local QnButton = import "L10.UI.QnButton"
local String = import "System.String"
local UInt32 = import "System.UInt32"
CItemComposeWnd.m_InitStaticParameter_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst == nil then
        return
    end
    CPackageView.OpenGemCompose = CommonDefs.IsPlayEnabled("JewelHeCheng")
    this.gemComposeButton:SetActive(CPackageView.OpenGemCompose)
    if CPackageView.OpenGemCompose then
        CItemComposeWnd.defaultIndex = 0
        CItemComposeWnd.defaultType = 1
    else
        CItemComposeWnd.defaultIndex = 1
        CItemComposeWnd.defaultType = 2
    end
end
CItemComposeWnd.m_Init_CS2LuaHook = function (this) 

    this:InitStaticParameter()

    this.itemList.SelectTemplateId = 0
    this.wushanComposeDetail.gameObject:SetActive(true)
    this.itemList.OnWushanSelect = MakeDelegateFromCSFunction(this.wushanComposeDetail.FillHoleWithStone, MakeGenericClass(Action1, Item_Item), this.wushanComposeDetail)
    if CItemComposeMgr.ComposeItemType == EnumComposeItemType.jewery then
        this.radioBox:ChangeTo(0, true)
        this.itemList:Init(EnumComposeItemType.jewery, true)
        this.curType = EnumComposeItemType.jewery
        this.wushanComposeDetail:Init(EnumComposeItemType.jewery)
    elseif CItemComposeMgr.ComposeItemType == EnumComposeItemType.diamond then
        this.radioBox:ChangeTo(2, true)
        this.itemList:Init(EnumComposeItemType.diamond, true)
        this.curType = EnumComposeItemType.diamond
        this.wushanComposeDetail:Init(EnumComposeItemType.diamond)
    elseif CItemComposeMgr.ComposeItemType == EnumComposeItemType.wushan then
        this.radioBox:ChangeTo(1, true)
        this.itemList:Init(EnumComposeItemType.wushan, true)
        this.curType = EnumComposeItemType.wushan
        this.wushanComposeDetail:Init(EnumComposeItemType.wushan)
    elseif CItemComposeMgr.ComposeItemType == EnumComposeItemType.none then
        this.radioBox:ChangeTo(CItemComposeWnd.defaultIndex, true)
        this.itemList:Init(CommonDefs.ConvertIntToEnum(typeof(EnumComposeItemType), CItemComposeWnd.defaultType), true)
        this.curType = CommonDefs.ConvertIntToEnum(typeof(EnumComposeItemType), CItemComposeWnd.defaultType)
        this.wushanComposeDetail:Init(CommonDefs.ConvertIntToEnum(typeof(EnumComposeItemType), CItemComposeWnd.defaultType))
    end
    this:OnRadioBoxSelect(nil, this.radioBox.CurrentSelectIndex)
    this.radioBox.OnSelect = MakeDelegateFromCSFunction(this.OnRadioBoxSelect, MakeGenericClass(Action2, QnButton, Int32), this)
end
CItemComposeWnd.m_OnRadioBoxSelect_CS2LuaHook = function (this, button, index) 

    this.itemList.SelectTemplateId = 0
    this.itemList:Init(CommonDefs.ConvertIntToEnum(typeof(EnumComposeItemType), (index + 1)), true)
    if index == 1 then
        this.wushanComposeDetail:Init(EnumComposeItemType.wushan)
        this.curType = EnumComposeItemType.wushan
    elseif index == 2 then
        this.wushanComposeDetail:Init(EnumComposeItemType.diamond)
        this.curType = EnumComposeItemType.diamond
    elseif index == 0 then
        this.wushanComposeDetail:Init(EnumComposeItemType.jewery)
        this.curType = EnumComposeItemType.jewery
    end
end
CItemComposeWnd.m_OnSetItemAt_CS2LuaHook = function (this, place, pos, oldId, newId) 
    if System.String.IsNullOrEmpty(oldId) and not System.String.IsNullOrEmpty(newId) then
        this.itemList:Init(this.curType, false)
        this.wushanComposeDetail:OnItemSetorSend()
    end
end
CItemComposeWnd.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
    CItemComposeMgr.ComposeItem = nil
    CItemComposeMgr.ComposeItemType = EnumComposeItemType.none
end
