local CChatMgr=import "L10.Game.CChatMgr"
local UIInput=import "UIInput"
local UITexture=import "UITexture"
local CJingLingWebImageMgr=import "L10.Game.CJingLingWebImageMgr"

CLuaCommonShare2PersonalSpaceWnd=class()
RegistClassMember(CLuaCommonShare2PersonalSpaceWnd,"m_ShareButton")
RegistClassMember(CLuaCommonShare2PersonalSpaceWnd,"m_Texture")
RegistClassMember(CLuaCommonShare2PersonalSpaceWnd,"m_Input")

function CLuaCommonShare2PersonalSpaceWnd:Init()
    self.m_ShareButton=FindChild(self.transform,"ShareButton").gameObject
    self.m_Texture=FindChild(self.transform,"Texture"):GetComponent(typeof(UITexture))
    self.m_Input=FindChild(self.transform,"Input"):GetComponent(typeof(UIInput))

    if CLuaShareMgr.m_Url then
        --有链接下载
        CJingLingWebImageMgr.Inst:GetImageByUrl(CLuaShareMgr.m_Url,DelegateFactory.Action_Texture2D(function(tex)
            --1200*768
            local width=tex.width
            local height=tex.height
            
            local ratio=width/height
            local baseRatio=1200/768
            if ratio>baseRatio then
                self.m_Texture.width=1200
                self.m_Texture.height=math.floor(1200/ratio)
            else
                self.m_Texture.height=768
                self.m_Texture.width=math.floor(768*ratio)
            end
            self.m_Texture.mainTexture=tex
        end))
    end
    local function OnClick(go)
        self:Share()
    end
    CommonDefs.AddOnClickListener(self.m_ShareButton,DelegateFactory.Action_GameObject(OnClick),false)
	
	if CommonDefs.IS_VN_CLIENT then
		self.m_ShareButton:SetActive(false)
		self.m_Input.gameObject:SetActive(false)
	end
end

function CLuaCommonShare2PersonalSpaceWnd:Share()
    local shareText=self.m_Input.value
    shareText=CChatMgr.Inst:FilterYangYangEmotion(shareText)

    if CLuaShareMgr.m_Url then
        if CLuaShareMgr.m_ShareCallback then
            CLuaShareMgr.m_ShareCallback(shareText)
        end
    end
end

return CLuaCommonShare2PersonalSpaceWnd
