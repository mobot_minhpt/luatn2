local UIScrollView = import "UIScrollView"
local UIGrid = import "UIGrid"
local CButton = import "L10.UI.CButton"
local ShareMgr = import "ShareMgr"
local QnCheckBoxGroup = import "L10.UI.QnCheckBoxGroup"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local UITabBar = import "L10.UI.UITabBar"
local DelegateFactory  = import "DelegateFactory"
local UITexture = import "UITexture"
local CLoginMgr = import "L10.Game.CLoginMgr"
local File = import "System.IO.File"
local NativeTools = import "L10.Engine.NativeTools"
local EnumGender = import "L10.Game.EnumGender"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local LoadPicMgr = import "L10.Game.LoadPicMgr"
local CSocialWndMgr = import "L10.UI.CSocialWndMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local CCPlayerCtrl = import "L10.Game.CCPlayerCtrl"
local CWinCGMgr = import "L10.Game.CWinCGMgr"
local QnNewSlider = import "L10.UI.QnNewSlider"
local CCPlayerMgr = import "L10.Game.CCPlayerMgr"
local CCPlayer = import "CCPlayer"
local Vector2 = import "UnityEngine.Vector2"
local Screen = import "UnityEngine.Screen"
local CFacialMgr = import "L10.Game.CFacialMgr"
local CPinchFaceCinemachineCtrlMgr = import "L10.Game.CPinchFaceCinemachineCtrlMgr"
local RenderTexture = import "UnityEngine.RenderTexture"
local RenderTextureFormat = import "UnityEngine.RenderTextureFormat"
local RenderTextureReadWrite = import "UnityEngine.RenderTextureReadWrite"
local Texture2D = import "UnityEngine.Texture2D"
local TextureFormat = import "UnityEngine.TextureFormat"
local Rect = import "UnityEngine.Rect"
local CPinchFaceHubMgr = import "L10.Game.CPinchFaceHubMgr"

LuaPinchFaceShareWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaPinchFaceShareWnd, "TabBar", "TabBar", UITabBar)
RegistChildComponent(LuaPinchFaceShareWnd, "QRCodeTexture", "QRCodeTexture", UITexture)
RegistChildComponent(LuaPinchFaceShareWnd, "ScreenshotTexture", "ScreenshotTexture", UITexture)
RegistChildComponent(LuaPinchFaceShareWnd, "PictureView", "PictureView", GameObject)
RegistChildComponent(LuaPinchFaceShareWnd, "VideoView", "VideoView", GameObject)
RegistChildComponent(LuaPinchFaceShareWnd, "QnCheckBoxGroup", "QnCheckBoxGroup", QnCheckBoxGroup)
RegistChildComponent(LuaPinchFaceShareWnd, "BtnScroll View", "BtnScroll View", UIScrollView)
RegistChildComponent(LuaPinchFaceShareWnd, "BtnGrid", "BtnGrid", UIGrid)
RegistChildComponent(LuaPinchFaceShareWnd, "DownloadBtn", "DownloadBtn", CButton)
RegistChildComponent(LuaPinchFaceShareWnd, "WexinBtn", "WexinBtn", CButton)
RegistChildComponent(LuaPinchFaceShareWnd, "PersonalSpaceButton", "PersonalSpaceButton", CButton)
RegistChildComponent(LuaPinchFaceShareWnd, "InGameShareBtn", "InGameShareBtn", CButton)
RegistChildComponent(LuaPinchFaceShareWnd, "WeixinPengyouQuanBtn", "WeixinPengyouQuanBtn", CButton)
RegistChildComponent(LuaPinchFaceShareWnd, "WeiBoBtn", "WeiBoBtn", CButton)
RegistChildComponent(LuaPinchFaceShareWnd, "GodLikePengYouQUanBtn", "GodLikePengYouQUanBtn", CButton)
RegistChildComponent(LuaPinchFaceShareWnd, "PlayerInfo", "PlayerInfo", GameObject)
RegistChildComponent(LuaPinchFaceShareWnd, "Portait", "Portait", CUITexture)
RegistChildComponent(LuaPinchFaceShareWnd, "AccountLabel", "AccountLabel", UILabel)
RegistChildComponent(LuaPinchFaceShareWnd, "ServerNameLabel", "ServerNameLabel", UILabel)
RegistChildComponent(LuaPinchFaceShareWnd, "IDLabel", "IDLabel", UILabel)
RegistChildComponent(LuaPinchFaceShareWnd, "Logo", "Logo", GameObject)
RegistChildComponent(LuaPinchFaceShareWnd, "Evaluation", "Evaluation", GameObject)
RegistChildComponent(LuaPinchFaceShareWnd, "VideoCtrl", "VideoCtrl", CCPlayerCtrl)
RegistChildComponent(LuaPinchFaceShareWnd, "VideoSlider", "VideoSlider", QnNewSlider)
RegistChildComponent(LuaPinchFaceShareWnd, "PauseButton", "PauseButton", CButton)
RegistChildComponent(LuaPinchFaceShareWnd, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaPinchFaceShareWnd, "PreviewAniTexture", "PreviewAniTexture", UITexture)
RegistChildComponent(LuaPinchFaceShareWnd, "TopLeftAnchor", "TopLeftAnchor", GameObject)
RegistChildComponent(LuaPinchFaceShareWnd, "CaptureWidget", "CaptureWidget", UIWidget)
RegistChildComponent(LuaPinchFaceShareWnd, "ScreenWidget", "ScreenWidget", UIWidget)
--@endregion RegistChildComponent end
RegistClassMember(LuaPinchFaceShareWnd, "m_TabIndex")
RegistClassMember(LuaPinchFaceShareWnd, "m_Paused")
RegistClassMember(LuaPinchFaceShareWnd, "m_MovieTotalTime")
RegistClassMember(LuaPinchFaceShareWnd, "m_DeltaTime")
RegistClassMember(LuaPinchFaceShareWnd, "m_IsStartMovie")
RegistClassMember(LuaPinchFaceShareWnd, "m_HasCaptureMovie")
RegistClassMember(LuaPinchFaceShareWnd, "m_UploadedCaptrueMovieUrl")
RegistClassMember(LuaPinchFaceShareWnd, "m_UploadedCaptrueMovieShareId")
RegistClassMember(LuaPinchFaceShareWnd, "m_GetFacialDataQRCodeTick")

function LuaPinchFaceShareWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.DownloadBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnDownloadBtnClick()
	end)


	
	UIEventListener.Get(self.WexinBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnWexinBtnClick()
	end)


	
	UIEventListener.Get(self.WeixinPengyouQuanBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnWeixinPengyouQuanBtnClick()
	end)


	
	UIEventListener.Get(self.WeiBoBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnWeiBoBtnClick()
	end)


	
	UIEventListener.Get(self.GodLikePengYouQUanBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGodLikePengYouQUanBtnClick()
	end)


    --@endregion EventBind end
    self.TabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function(go,index)
        self:OnTabBarChanged(go,index)
    end)

    UIEventListener.Get(self.PersonalSpaceButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnPersonalSpaceButtonClick()
	end)

    UIEventListener.Get(self.InGameShareBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnInGameShareBtnClick()
	end)

    UIEventListener.Get(self.PauseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnPauseButtonClick()
	end)

    self.VideoSlider.OnValueChanged = DelegateFactory.Action_float(function(value)
        self:OnSliderValueChanged(value)
    end)

    self.VideoSlider.m_UISlider.onDragFinished = LuaUtils.OnDragFinished(function() 
        self:OnDragVideoSliderFinished()
    end)

    self.VideoCtrl.OnBeginPlay = DelegateFactory.Action(function()
        self:OnCCPlayerBeginPlay()
    end)

    self.VideoCtrl.OnEndPlay = DelegateFactory.Action(function()
        self:OnCCPlayerEndPlay()
    end)

    UIEventListener.Get(self.VideoSlider.m_UISlider.thumb.gameObject).onPress = CommonDefs.CombineListner_BoolDelegate(
        UIEventListener.Get(self.VideoSlider.m_UISlider.thumb.gameObject).onPress, DelegateFactory.BoolDelegate(function (go,state)
            self:OnPress(go,state) 
        end), true)

    if CommonDefs.IS_VN_CLIENT then
        self.TabBar.transform:GetChild(1).gameObject:SetActive(false)
    end
end

function LuaPinchFaceShareWnd:Init()
    self.VideoCtrl.transform.localEulerAngles = CommonDefs.IsAndroidPlatform() and Vector3(0,0,0) or Vector3(0,0,180)
    self.VideoCtrl.transform.localPosition = CommonDefs.IsAndroidPlatform() and Vector3(0,-400,0) or Vector3(0,400,0)
    self.VideoCtrl.transform.localScale = CommonDefs.IsAndroidPlatform() and Vector3(1,1,1) or Vector3(-1,1,1)
    self.QRCodeTexture.width = LuaPinchFaceMgr.m_EnableHubQRCode and 256 or 512
    self.PlayerInfo.gameObject:SetActive(LuaPinchFaceMgr.m_IsInModifyMode)
    self.PersonalSpaceButton.gameObject:SetActive(LuaPinchFaceMgr.m_IsInModifyMode and CLoginMgr.Inst:IsNetEaseOfficialLogin())
    self.InGameShareBtn.gameObject:SetActive(LuaPinchFaceMgr.m_IsInModifyMode and CLoginMgr.Inst:IsNetEaseOfficialLogin())
    self.VideoCtrl.gameObject:SetActive(false)
    self.VideoSlider.gameObject:SetActive(false)
    self.PauseButton.gameObject:SetActive(false)
    self.PreviewAniTexture.gameObject:SetActive(true)
    self.VideoSlider.Enabled = CCPlayerMgr.Inst:CanUseSeek()
    self.VideoSlider.m_Value = 0
    self.VideoSlider:SetPercentage()
    self.WexinBtn.gameObject:SetActive(CLoginMgr.Inst:IsNetEaseOfficialLogin())
    self.WeixinPengyouQuanBtn.gameObject:SetActive(CLoginMgr.Inst:IsNetEaseOfficialLogin())
    self.WeiBoBtn.gameObject:SetActive(CLoginMgr.Inst:IsNetEaseOfficialLogin())
    self.GodLikePengYouQUanBtn.gameObject:SetActive(CLoginMgr.Inst:IsNetEaseOfficialLogin())
    if (Application.platform == RuntimePlatform.WindowsPlayer or Application.platform == RuntimePlatform.WindowsEditor or NativeTools.IsAllChannelApk() or LuaCloudGameFaceMgr:IsCloudGameFace())
        or not CommonDefs.IS_CN_CLIENT
    then
        self.WexinBtn.gameObject:SetActive(false)
        self.WeixinPengyouQuanBtn.gameObject:SetActive(false)
        self.WeiBoBtn.gameObject:SetActive(false)
        self.GodLikePengYouQUanBtn.gameObject:SetActive(false)
    end
    self.BtnGrid:Reposition()
    self:InitEvaluation()
    if LuaPinchFaceMgr.m_IsInModifyMode and CClientMainPlayer.Inst then
        self.AccountLabel.text = CClientMainPlayer.Inst.Name
        local gameServer = CLoginMgr.Inst:GetSelectedGameServer()
        self.ServerNameLabel.text = gameServer and gameServer.name or nil
        self.Portait:LoadNPCPortrait(CClientMainPlayer.Inst.PortraitName,false)
        self.IDLabel.text = CClientMainPlayer.Inst.Id
    end
    self:InitQnCheckBoxGroup()
    self:InitTextures()
    self:PauseOrPlay(true)
    self.TabBar:ChangeTab(0, false)
end

function LuaPinchFaceShareWnd:InitTextures()
    --if not VideoRecordMgr.IsPlatformSupport() or not CPinchFaceCinemachineCtrlMgr.Inst then return end
    if not CPinchFaceCinemachineCtrlMgr.Inst then return end
    local camera = CPinchFaceCinemachineCtrlMgr.Inst.m_Cam

   -- 截图
    local tex = CUICommonDef.DoCaptureWithClip(camera, 16/9)
    self.ScreenshotTexture.mainTexture = tex

    -- 视频
    local texWidth, texHeight = self.ScreenshotTexture.width, self.ScreenshotTexture.height
    local texRatio = texWidth / texHeight
    local rtRatio = self.ScreenWidget.width / self.ScreenWidget.height
    local newWidth, newHeight = texWidth, texHeight
    if rtRatio > texRatio then
        newWidth = texHeight * rtRatio
    elseif rtRatio ~= 0 then
        newHeight = texWidth / rtRatio
    end
    
    self.ScreenshotTexture.width = newWidth
    self.ScreenshotTexture.height = newHeight
    self.PreviewAniTexture.width = newWidth
    self.PreviewAniTexture.height = newHeight
    self.VideoCtrl.m_Texture.width = newWidth
    self.VideoCtrl.m_Texture.height = newHeight

    local rt
    if CommonDefs.IsAndroidPlatform() then
        local screenScale = Screen.height / self.ScreenWidget.height
        local ratio = self.ScreenshotTexture.width / self.ScreenWidget.width
        local width = Screen.width / screenScale * ratio
        local height = Screen.height / screenScale * ratio
        rt = RenderTexture.GetTemporary(width, height, 16, RenderTextureFormat.ARGB32, RenderTextureReadWrite.sRGB)
        
        ratio = (width - self.ScreenshotTexture.width) / width
        self.PreviewAniTexture.uvRect = Rect(ratio * 0.5, 0, 1 - ratio, 1)
    else
        rt = RenderTexture.GetTemporary(self.ScreenshotTexture.width, self.ScreenshotTexture.height, 16, RenderTextureFormat.ARGB32, RenderTextureReadWrite.sRGB)
    end
    camera.targetTexture = rt
    self.PreviewAniTexture.mainTexture = rt

    if CPinchFaceCinemachineCtrlMgr.Inst then
        local camera = CPinchFaceCinemachineCtrlMgr.Inst.m_Cam
        camera.rect = Rect(0, 0, 1, 1)
    end
end

function LuaPinchFaceShareWnd:InitEvaluation()
    local evaluationCommentData = LuaPinchFaceMgr:AnalyzeCurEvaluation()
    if evaluationCommentData == nil then
        self.Evaluation.gameObject:SetActive(false)
        return
    end
    local male = self.Evaluation.transform:Find("Male")
    local female = self.Evaluation.transform:Find("Famle")
    male.gameObject:SetActive(LuaPinchFaceMgr.m_CurEnumGender == EnumGender.Male)
    female.gameObject:SetActive(LuaPinchFaceMgr.m_CurEnumGender ~= EnumGender.Male)
    local root = LuaPinchFaceMgr.m_CurEnumGender == EnumGender.Male and male or female
    local titleLable = root:Find("TitleLable")
    local label01 = titleLable.transform:Find("Label01"):GetComponent(typeof(UILabel))
    local label02 = titleLable.transform:Find("Label02"):GetComponent(typeof(UILabel))
    local label03 = titleLable.transform:Find("Label03"):GetComponent(typeof(UILabel))
    local label04 = titleLable.transform:Find("Label04"):GetComponent(typeof(UILabel))
    label01.text = evaluationCommentData.Evaluation[0]
    label02.text = evaluationCommentData.Evaluation[1]
    label03.text = evaluationCommentData.Evaluation[2]
    label04.text = evaluationCommentData.Evaluation[3]
    local poemLable = root:Find("PoemLable")
    local des1 = poemLable.transform:Find("Label01"):GetComponent(typeof(UILabel))
    local des2 = poemLable.transform:Find("Label02"):GetComponent(typeof(UILabel))
    des1.text = evaluationCommentData.Description[0]
    des2.text = evaluationCommentData.Description[1]
end

function LuaPinchFaceShareWnd:InitQnCheckBoxGroup()
    local options = {
        LocalString.GetString("显示游戏图标"),LocalString.GetString("显示面容数据"),
        LocalString.GetString("显示评语")
    }
    local defaultSelected = {
        true,false,true
    }
    if LuaPinchFaceMgr.m_IsInModifyMode then
        table.insert(options,LocalString.GetString("显示玩家名称"))
        table.insert(defaultSelected,true)
    end
    self.QnCheckBoxGroup.AllowMultiSelected = true
    self.QnCheckBoxGroup:InitWithOptions(Table2Array(options, MakeArrayClass(String)), true, true)
    self.QnCheckBoxGroup.OnSelect = DelegateFactory.Action_QnCheckBox_int(function(checkbox,level)
        self:OnSelectQnCheckBoxGroup(checkbox,level)
    end)
    for i = 1, #options do
        self.QnCheckBoxGroup:SetSelect(i - 1,  defaultSelected[i], false)
    end
end

function LuaPinchFaceShareWnd:CombineVideoWebShareUrl(videoShareId)
    if CommonDefs.IS_PUB_RELEASE then
        return string.format("https://qnm.163.com/videoshare/?shareId=%s&share=copy&spreadtimes=2#/",videoShareId)
    else
        return string.format("https://test.nie.163.com/test_html/qnm/videoshare/?shareId=%s&share=copy&spreadtimes=2#/",videoShareId)
    end
end


function LuaPinchFaceShareWnd:DoShare(channel)
    if self.m_TabIndex == 0 then 
        self:CapturePic(function(fullPath, jpgBytes)
            ShareMgr.ShareImage(channel, fullPath)
        end)
    elseif self.m_TabIndex == 1 then
        local videoPath = LuaPinchFaceMgr.m_CaptureMovieFilePath
        if File.Exists(videoPath) then
            local onFail = DelegateFactory.Action(function()
                g_MessageMgr:ShowMessage("PinchFaceHub_UploadVideoFailed")
            end)

            local onSuccess = function(shareId,videoUrl)
                self.m_UploadedCaptrueMovieUrl = videoUrl
                self.m_UploadedCaptrueMovieShareId = shareId
                
                g_MessageMgr:ShowMessage("PinchFaceHub_UploadVideoSuccess")
                CLogMgr.Log(string.format("%s,%s",shareId,videoUrl))

                local designData = GameSetting_Share.GetData()
                local title = designData.Share_PinchVideoShare_Title
                local text = designData.Share_PinchVideoShare_Description
                local thumbUrl = designData.Share_PinchVideoShare_Thumb
                if channel == ShareMgr.NT_SHARE_TYPE_GODLIKE_FRIEND then
                    ShareMgr.ShareVideo(channel,videoUrl,title,"",text)
                else
                    local webUrl = self:CombineVideoWebShareUrl(shareId)
                    ShareMgr.ShareCommonUrl(channel,webUrl,title,text,thumbUrl)
                end
            end
            if not self.m_UploadedCaptrueMovieUrl or not self.m_UploadedCaptrueMovieShareId then
                g_MessageMgr:ShowMessage("PinchFaceHub_VideoUploading")
                local data = File.ReadAllBytes(videoPath)
                CPinchFaceHubMgr.Inst:UploadVideo(data,onFail,DelegateFactory.Action_string_string(onSuccess))
            else
                onSuccess(self.m_UploadedCaptrueMovieShareId,self.m_UploadedCaptrueMovieUrl)
            end
        end
    end
end

function LuaPinchFaceShareWnd:CapturePic(onFinishAction)
    local scale = CUICommonDef.m_CaptureWidth / Screen.width
    local bIsWinSocialWndOpened = false
    if CommonDefs.IsPCGameMode() then
        local CWinSocialWndMgr = import "L10.UI.CWinSocialWndMgr"
        bIsWinSocialWndOpened = CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened
    end
    if bIsWinSocialWndOpened then
        scale = scale / (1 - Constants.WinSocialWndRatio)
    end
    local panel = self.PictureView.transform:Find("PictureScrollView"):GetComponent(typeof(UIPanel))
	local clipRegion = panel.finalClipRegion
	local panelWidth = clipRegion.z
	local panelHeight = clipRegion.w
    local leftBottomPos = self:Local2Screen(Vector3(-panelWidth / 2, -panelHeight / 2 , 0), scale)
    local rightTopPos = self:Local2Screen(Vector3(panelWidth / 2, panelHeight / 2 , 0), scale)
    local partWidth = math.abs(math.floor(rightTopPos.x - leftBottomPos.x))
    local partHeight = math.abs(math.floor(rightTopPos.y - leftBottomPos.y))
    CUICommonDef.m_CapturePartOfScreenPrecision = 1418 / partWidth
    CUICommonDef.CapturePartOfScreen("PinchFaceShareShoot", true, false, nil, DelegateFactory.Action_string_bytes(
        function(fullPath, jpgBytes)
            if onFinishAction then
                onFinishAction(fullPath, jpgBytes)
            end
        end
    ), leftBottomPos, partWidth, partHeight, false) 
end

function LuaPinchFaceShareWnd:Local2Screen(localPos, scale)
	local worldPos = self.transform:TransformPoint(localPos)
	local screenPos = UICamera.currentCamera:WorldToScreenPoint(worldPos)
	return Vector2(screenPos.x * scale, screenPos.y * scale)
end

function LuaPinchFaceShareWnd:OnDestroy()
    if self.QRCodeTexture.mainTexture then
        GameObject.Destroy(self.QRCodeTexture.mainTexture)
        self.QRCodeTexture.mainTexture = nil
    end

    if self.ScreenshotTexture.mainTexture then
        GameObject.Destroy(self.ScreenshotTexture.mainTexture)
        self.ScreenshotTexture.mainTexture = nil
    end

    local rt = self.PreviewAniTexture.mainTexture
	if rt then
		RenderTexture.ReleaseTemporary(rt)
        self.PreviewAniTexture.mainTexture = nil
	end

    if CPinchFaceCinemachineCtrlMgr.Inst then
        local camera = CPinchFaceCinemachineCtrlMgr.Inst.m_Cam
        camera.targetTexture = nil

        local scale = 1
        if CommonDefs.IsPCGameMode() then
            local CWinSocialWndMgr = import "L10.UI.CWinSocialWndMgr"
            scale = CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened and 1 - Constants.WinSocialWndRatio or 1
        end
        camera.rect = Rect(0, 0, scale, 1)
    end
end

--@region UIEvent
function LuaPinchFaceShareWnd:OnTabBarChanged(go,index)
    self.m_TabIndex = index
    local canUseMovieCapture = LuaPinchFaceMgr:CanUseMovieCapture()
    local isShowPictureView = index == 0 or not canUseMovieCapture
    local isShowVideoView = index == 1 and canUseMovieCapture
    self.PictureView.gameObject:SetActive(isShowPictureView)
    self.VideoView.gameObject:SetActive(isShowVideoView)
    self.TabBar.tabButtons[0].transform:Find("Normal").gameObject:SetActive(isShowVideoView)
    self.TabBar.tabButtons[0].transform:Find("Highlight").gameObject:SetActive(isShowPictureView)
    self.TabBar.tabButtons[1].transform:Find("Normal").gameObject:SetActive(isShowPictureView)
    self.TabBar.tabButtons[1].transform:Find("Highlight").gameObject:SetActive(isShowVideoView)
    if index == 1 then
        local Main = import "L10.Engine.Main"
        if CommonDefs.IsAndroidPlatform() and Main.Inst.EngineVersion < 533184 then 
            if not LuaCloudGameFaceMgr:IsCloudGameFace() then
                g_MessageMgr:ShowMessage("Pinchface_Video_PleaseUpgrade")
            end
            self.TabBar:ChangeTab(0, false)
            return
        elseif not canUseMovieCapture then
            g_MessageMgr:ShowMessage("MovieCaptureForbidden")
            self.TabBar:ChangeTab(0, false)
            return
        end
    end
    if isShowVideoView then
        if not self.m_HasCaptureMovie then
            self:StartCapture(self.PreviewAniTexture.mainTexture)
        end
    elseif index == 0 then
        LuaPinchFaceMgr:StopPlayMovie()
        self:PauseOrPlay(true)    
    end
end

function LuaPinchFaceShareWnd:OnSelectQnCheckBoxGroup(checkbox,level)
    if level == 0 then
        self.Logo.gameObject:SetActive(checkbox.Selected)
    elseif level == 1 then
        self:InitQrCode(checkbox.Selected)
    elseif level == 2 then
        self.Evaluation.gameObject:SetActive(checkbox.Selected)
    elseif level == 3 then
        self.PlayerInfo.gameObject:SetActive(LuaPinchFaceMgr.m_IsInModifyMode and checkbox.Selected)
    end
end

function LuaPinchFaceShareWnd:InitQrCode(isShow)
    self.QRCodeTexture.gameObject:SetActive(isShow)
    if self.QRCodeTexture.mainTexture then return end
    self.QnCheckBoxGroup:SetSelect(1,  false, true)
    self.QRCodeTexture.gameObject:SetActive(false)

    local resizeTex = self.ScreenshotTexture.mainTexture
    local data = CommonDefs.EncodeToJPG((TypeAs(resizeTex, typeof(Texture2D))))

    if self.m_GetFacialDataQRCodeTick then
        g_MessageMgr:ShowMessage("PinchFace_GenFacialDataQRCode_Processing")
        return
    end
    self:CancelGetFacialDataQRCodeTick()
    self.m_GetFacialDataQRCodeTick = RegisterTickOnce(function()
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("与服务器连接超时，请稍后重试！"))
        self:CancelGetFacialDataQRCodeTick()
    end, 5000)

    
    CFacialMgr.GetFacialDataQRCodeInHub(LuaPinchFaceMgr.m_RO, EnumToInt(LuaPinchFaceMgr.m_CurEnumClass), EnumToInt(LuaPinchFaceMgr.m_CurEnumGender), LuaPinchFaceMgr.m_HairId, LuaPinchFaceMgr.m_HeadId, data, DelegateFactory.Action_Texture(function (texture)
        if self.QRCodeTexture then
            self.QRCodeTexture.mainTexture = texture
            self.QRCodeTexture.gameObject:SetActive(texture ~= nil)
        end
        if texture == nil then 
            g_MessageMgr:ShowMessage("Pinchface_ShareData_Failed")
        end
        if self.QnCheckBoxGroup then
            self.QnCheckBoxGroup:SetSelect(1,  texture ~= nil, true)
        end
        self:CancelGetFacialDataQRCodeTick()
    end))
end

function LuaPinchFaceShareWnd:CancelGetFacialDataQRCodeTick()
    if self.m_GetFacialDataQRCodeTick then
        UnRegisterTick(self.m_GetFacialDataQRCodeTick)
        self.m_GetFacialDataQRCodeTick = nil
    end
end

function LuaPinchFaceShareWnd:OnDownloadBtnClick()
    if self.m_TabIndex == 0 then
        self:CapturePic(function(fullPath, jpgBytes)
            pcall(function() 
                NativeTools.UniSaveImage(jpgBytes,DelegateFactory.Action_bool(function(success)
                    if not LuaCloudGameFaceMgr:IsCloudGameFace()  then
                        if success then
                            g_MessageMgr:ShowMessage("PinchFace_Share_DownloadToAlbum_Success")
                        end
                    end
                end)) 
            end) 
        end)   
    elseif self.m_TabIndex == 1 then
        NativeTools.UniSaveVideoAtPath(LuaPinchFaceMgr.m_CaptureMovieFilePath,DelegateFactory.Action_bool(function(success)
            if success then
                g_MessageMgr:ShowMessage("PinchFaceHub_SaveVideoSuccess")
            else
                g_MessageMgr:ShowMessage("PinchFaceHub_SaveVideoFailed")
            end
        end))
    end
end

function LuaPinchFaceShareWnd:OnWexinBtnClick()
    self:DoShare(ShareMgr.NT_SHARE_TYPE_WEIXIN_FRIEND)
end

function LuaPinchFaceShareWnd:OnWeixinPengyouQuanBtnClick()
    self:DoShare(ShareMgr.NT_SHARE_TYPE_WEIXIN_TIMELINE)
end

function LuaPinchFaceShareWnd:OnWeiBoBtnClick()
    self:DoShare(ShareMgr.NT_SHARE_TYPE_WEIBO)
end

function LuaPinchFaceShareWnd:OnGodLikePengYouQUanBtnClick()
    self:DoShare(ShareMgr.NT_SHARE_TYPE_GODLIKE_TIMELINE)
end

function LuaPinchFaceShareWnd:OnPersonalSpaceButtonClick()
    if not CPersonalSpaceMgr.Inst:OpenPersonalSpaceCheck(false) then
        g_MessageMgr:ShowMessage("PERSONAL_SPACE_NOT_OPEN")
        return
    end

    self:CapturePic(function(fullPath, jpgBytes)
        ShareMgr.ShareLocalImage2PersonalSpace(fullPath, DelegateFactory.Action(function ()
            g_ScriptEvent:BroadcastInLua("PersonalSpaceShareFinished")
            CUIManager.CloseUI(CUIResources.Share2PersonalSpaceWnd)
            g_MessageMgr:ShowMessage("SHARE_SUCCESS")
            if CUIManager.IsLoaded(CUIResources.ScreenCaptureWnd) then
                Gac2Gas.ScreenCapturePersonalSpaceShareFinished()
            end
        end))
    end) 
end

function LuaPinchFaceShareWnd:OnInGameShareBtnClick()
    CUIManager.CloseUI(CUIResources.SocialWnd)
    self.QnCheckBoxGroup:SetSelect(1, false, true)
    self.QRCodeTexture.gameObject:SetActive(false)
    self:CapturePic(function(fullPath, jpgBytes)
        local data = File.ReadAllBytes(fullPath)
        LoadPicMgr.UploadPic(CPersonalSpaceMgr.Upload_PicType, data, DelegateFactory.Action_string(function (url) 
            LoadPicMgr.SaveUploadPic(CPersonalSpaceMgr.Upload_PicType, url, LoadPicMgr.NORSuffix, DelegateFactory.Action_CPersonalSpace_BaseRet(function (ret) 
                if ret.code == 0 then
                    LuaInGameShareMgr.DoShare(bit.bor(EnumInGameShareChannelDefine.eWorld, EnumInGameShareChannelDefine.eGuild, EnumInGameShareChannelDefine.eTeam), function()
                        return g_MessageMgr:FormatMessage("PinchFace_InGameShare_Msg", ret.msg, self.ScreenshotTexture.width, self.ScreenshotTexture.height)
                    end, self.InGameShareBtn.transform, CPopupMenuInfoMgr.AlignType.Top, function (shareText, channel)
                        local channel = channel == EnumInGameShareChannelDefine.eWorld and EChatPanel.World or (channel == EnumInGameShareChannelDefine.eGuild and EChatPanel.Guild or EChatPanel.Team)
                        CSocialWndMgr.ShowChatWnd(channel)
                    end)
                end
            end), nil)
        end))
    end)   
end

function LuaPinchFaceShareWnd:OnSliderValueChanged(value)
    if not self.m_MovieTotalTime then return end
    self.m_DeltaTime = value * self.m_MovieTotalTime
    self.TimeLabel.text = SafeStringFormat3("%02d:%02d/%02d:%02d",self.m_DeltaTime / 60, self.m_DeltaTime % 60, self.m_MovieTotalTime / 60, self.m_MovieTotalTime % 60)
    if value >= 1 then
        self.m_IsStartMovie = false
        self.VideoSlider.Enabled = false
        self:PauseOrPlay(true)
    end
end

function LuaPinchFaceShareWnd:OnDragVideoSliderFinished()
    if self.VideoSlider.m_Value < 1 then
        self.VideoCtrl:Seek(self.VideoSlider.m_Value)
        self:Resume()
        self:PauseOrPlay(false)
    else
        self:PlayMovie()
    end
end

function LuaPinchFaceShareWnd:OnPauseButtonClick()
    self:PauseOrPlay(not self.m_Paused)
end

function LuaPinchFaceShareWnd:OnPress(go, pressed) 
    if pressed then
        self:PauseOrPlay(true)
    end
end
--@endregion UIEvent

function LuaPinchFaceShareWnd:PauseOrPlay(pause)
    self.m_Paused = pause
    self.PauseButton.Selected = not self.m_Paused
    if self.m_Paused then
        if self.VideoCtrl:IsPlaying() then
            self:Pause()
        end
    else
        if self.VideoSlider.m_Value > 0 and self.VideoSlider.m_Value < 1 then
            self:Resume()
        elseif not self.m_IsStartMovie then
            self:PlayMovie()
        end
    end
end

function LuaPinchFaceShareWnd:Pause()
    if CommonDefs.IsAndroidPlatform() then
        CCPlayerMgr.Inst.m_IsPauseSign = true
        CCPlayer.CCPlayerPluginPauseVideo()
    else
        self.VideoCtrl:Pause()
    end
end

function LuaPinchFaceShareWnd:Resume()
    if CCPlayerMgr.Inst.m_IsPauseSign then
        if CommonDefs.IsAndroidPlatform() then
            CCPlayerMgr.Inst.m_IsPauseSign = false
            CCPlayer.CCPlayerPluginResumeVideo()
        else
            self.VideoCtrl:Resume()
        end
    end
end

function LuaPinchFaceShareWnd:PlayMovie()
    if not self.m_HasCaptureMovie then return end
    self.m_MovieTotalTime = LuaPinchFaceMgr.m_CaptureMovieTotalTime
    self.m_DeltaTime = 0
    self.VideoSlider.m_Value = 0
    self.VideoSlider:SetPercentage()
    if CommonDefs.IsAndroidPlatform() then
        self.VideoCtrl:Stop()
    end
    self.VideoCtrl:PlayPureUrl(LuaPinchFaceMgr.m_CaptureMovieFilePath, CWinCGMgr.Inst.videoVolume)
end

function LuaPinchFaceShareWnd:StopMovie()
    self.m_IsStartMovie = false
    self.VideoSlider.Enabled = false
    self:PauseOrPlay(true)
    self.VideoSlider.m_Value = 1
    self.VideoSlider:SetPercentage()
end

function LuaPinchFaceShareWnd:OnCCPlayerBeginPlay()
    self.m_IsStartMovie = true
    self.VideoSlider.Enabled = CCPlayerMgr.Inst:CanUseSeek()
    self:PauseOrPlay(false)
end

function LuaPinchFaceShareWnd:OnCCPlayerEndPlay()
    print("OnCCPlayerEndPlay")
    self:StopMovie()
end

function LuaPinchFaceShareWnd:Update()
    if self.m_Paused or not self.m_DeltaTime or not self.m_MovieTotalTime then return end
    if self.m_DeltaTime > self.m_MovieTotalTime then
        self.m_DeltaTime = self.m_MovieTotalTime
    end
    if self.VideoSlider then
        self.VideoSlider.m_Value = self.m_DeltaTime / self.m_MovieTotalTime
        self.VideoSlider:SetPercentage()
    end

    if self.VideoCtrl:IsPlaying() then
        local playDuration = self.VideoCtrl:GetPlayDuration()
        self.m_MovieTotalTime = playDuration / 1000
    end
    self.m_DeltaTime = self.m_DeltaTime + Time.deltaTime
end

function LuaPinchFaceShareWnd:OnEnable()
    g_ScriptEvent:AddListener("OnCompletedPinchFaceCaptureMovieFileWriting", self, "OnCompletedPinchFaceCaptureMovieFileWriting")
    g_ScriptEvent:AddListener("OnScreenChange", self, "OnScreenChange")
end

function LuaPinchFaceShareWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnCompletedPinchFaceCaptureMovieFileWriting", self, "OnCompletedPinchFaceCaptureMovieFileWriting")
    g_ScriptEvent:RemoveListener("OnScreenChange", self, "OnScreenChange")
    self:CancelGetFacialDataQRCodeTick()
    LuaPinchFaceMgr:StopPlayMovie()
    CUICommonDef.m_CapturePartOfScreenPrecision = 1
end

function LuaPinchFaceShareWnd:OnCompletedPinchFaceCaptureMovieFileWriting()
    self.m_HasCaptureMovie = true
    self.VideoCtrl.gameObject:SetActive(true)
    self.VideoSlider.gameObject:SetActive(CCPlayerMgr.Inst:CanUseSeek())
    self.PauseButton.gameObject:SetActive(true)

    self.DownloadBtn.gameObject:SetActive(true)
    if (CommonDefs.IsAndroidPlatform() or CommonDefs.IsIOSPlatform())
        and CommonDefs.IS_CN_CLIENT and not LuaCloudGameFaceMgr:IsCloudGameFace() and CLoginMgr.Inst:IsNetEaseOfficialLogin()
    then
        self.WexinBtn.gameObject:SetActive(true)
        self.WeixinPengyouQuanBtn.gameObject:SetActive(true)
        self.WeiBoBtn.gameObject:SetActive(true)
        self.GodLikePengYouQUanBtn.gameObject:SetActive(true)
    end
end

function LuaPinchFaceShareWnd:OnScreenChange()
    if LuaPinchFaceMgr.m_IsInPlayMovie then
        LuaPinchFaceMgr:StopPlayMovie()
        MessageWndManager.ShowOKMessage(g_MessageMgr:FormatMessage("PinchFace_Video_Failed"), DelegateFactory.Action(function()
            self:StartCapture(self.PreviewAniTexture.mainTexture)
        end))
    end
end

function LuaPinchFaceShareWnd:StartCapture(rt)
    LuaPinchFaceMgr:StopPlayMovie()

    LuaPinchFaceMgr:PlayMovie(true, nil, nil, false,rt)

    self.VideoSlider.gameObject:SetActive(false)
    self.PauseButton.gameObject:SetActive(false)
    self.DownloadBtn.gameObject:SetActive(false)
    if (CommonDefs.IsAndroidPlatform() or CommonDefs.IsIOSPlatform())
        and CommonDefs.IS_CN_CLIENT and not LuaCloudGameFaceMgr:IsCloudGameFace()
    then
        self.WexinBtn.gameObject:SetActive(false)
        self.WeixinPengyouQuanBtn.gameObject:SetActive(false)
        self.WeiBoBtn.gameObject:SetActive(false)
        self.GodLikePengYouQUanBtn.gameObject:SetActive(false)
    end
end