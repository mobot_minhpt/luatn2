local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local UITabBar = import "L10.UI.UITabBar"
local DelegateFactory  = import "DelegateFactory"
local AlignType = import "L10.UI.CTooltip+AlignType"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Animation = import "UnityEngine.Animation"

LuaWuYi2023XXSCMainWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaWuYi2023XXSCMainWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaWuYi2023XXSCMainWnd, "TopTimeLabel", "TopTimeLabel", UILabel)
RegistChildComponent(LuaWuYi2023XXSCMainWnd, "TopReadMeLabel", "TopReadMeLabel", UILabel)
RegistChildComponent(LuaWuYi2023XXSCMainWnd, "OwnGoldLabel", "OwnGoldLabel", UILabel)
RegistChildComponent(LuaWuYi2023XXSCMainWnd, "AddGoldBtn", "AddGoldBtn", GameObject)
RegistChildComponent(LuaWuYi2023XXSCMainWnd, "OwnSilverLabel", "OwnSilverLabel", UILabel)
RegistChildComponent(LuaWuYi2023XXSCMainWnd, "AddSilverBtn", "AddSilverBtn", GameObject)
RegistChildComponent(LuaWuYi2023XXSCMainWnd, "UITabBar", "UITabBar", UITabBar)
RegistChildComponent(LuaWuYi2023XXSCMainWnd, "WuYi2023XXSCMainWndCommonView", "WuYi2023XXSCMainWndCommonView", GameObject)
RegistChildComponent(LuaWuYi2023XXSCMainWnd, "WuYi2023XXSCMainWndSpecialView", "WuYi2023XXSCMainWndSpecialView", GameObject)
RegistChildComponent(LuaWuYi2023XXSCMainWnd, "OwnCopperLabel", "OwnCopperLabel", UILabel)
RegistChildComponent(LuaWuYi2023XXSCMainWnd, "AddCopperBtn", "AddCopperBtn", GameObject)
--@endregion RegistChildComponent end
RegistClassMember(LuaWuYi2023XXSCMainWnd, "m_DefaultIndex")
RegistClassMember(LuaWuYi2023XXSCMainWnd, "m_CurIndex")
RegistClassMember(LuaWuYi2023XXSCMainWnd, "m_UpdateTopTimeLabelTick")
RegistClassMember(LuaWuYi2023XXSCMainWnd, "m_RemainingTime")
RegistClassMember(LuaWuYi2023XXSCMainWnd, "m_Ani")

function LuaWuYi2023XXSCMainWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)


	
	UIEventListener.Get(self.AddGoldBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAddGoldBtnClick()
	end)


	
	UIEventListener.Get(self.AddSilverBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAddSilverBtnClick()
	end)


	UIEventListener.Get(self.AddCopperBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnAddCopperBtnClick()
	end)
    --@endregion EventBind end
end

function LuaWuYi2023XXSCMainWnd:Init()
	self.TopReadMeLabel.text = g_MessageMgr:FormatMessage("WuYi2023XXSCMainWnd_TopReadMeLabel")
	self.m_Ani = self.transform:GetComponent(typeof(Animation))
	self.m_RemainingTime = CServerTimeMgr.Inst:GetTimeStampByStr(WuYi2023_XinXiangShiCheng.GetData().RemainingTime)
	self:CancelUpdateTopTimeLabelTick()
	self.m_UpdateTopTimeLabelTick = RegisterTick(function ()
		self:UpdateTopTimeLabel()
	end, 500)
	self.UITabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
        self:OnTabChange(go, index)
    end)
	self.m_DefaultIndex = LuaWuYi2023Mgr.m_XXSCDefaultIndex
	self.UITabBar:ChangeTab(self.m_DefaultIndex or 0, false)
	self.OwnGoldLabel.text = ""
	self.OwnSilverLabel.text = ""
	self.OwnCopperLabel.text  = ""
	Gac2Gas.XinXiangShiChengQueryLotteryInfo()
end

function LuaWuYi2023XXSCMainWnd:UpdateTopTimeLabel()
	local leftTime = self.m_RemainingTime - CServerTimeMgr.Inst.timeStamp
	local days = math.floor(leftTime / (24 * 3600))
	local hour = math.floor((leftTime % (24 * 3600)) / 3600)
	local minute = math.floor(leftTime % 3600 / 60)
	local sec = leftTime % 60
	self.TopTimeLabel.text = SafeStringFormat3(LocalString.GetString("%d天%02d时%02d分%02d秒"),days,hour,minute,sec) 
	self.TopTimeLabel.gameObject:SetActive(leftTime >= 0)
	if leftTime < 0 then
		self:CancelUpdateTopTimeLabelTick()
	end
end

function LuaWuYi2023XXSCMainWnd:CancelUpdateTopTimeLabelTick()
	if self.m_UpdateTopTimeLabelTick then
		UnRegisterTick(self.m_UpdateTopTimeLabelTick)
		self.m_UpdateTopTimeLabelTick = nil
	end
end

function LuaWuYi2023XXSCMainWnd:OnEnable()
	g_ScriptEvent:AddListener("OnXinXiangShiChengSendLotteryInfo", self, "OnXinXiangShiChengSendLotteryInfo")
end

function LuaWuYi2023XXSCMainWnd:OnDisable()
	LuaWuYi2023Mgr.m_XXSCDefaultIndex = nil -- 关闭界面时清空默认界面编号
	g_ScriptEvent:RemoveListener("OnXinXiangShiChengSendLotteryInfo", self, "OnXinXiangShiChengSendLotteryInfo")
	self:CancelUpdateTopTimeLabelTick()
end

function LuaWuYi2023XXSCMainWnd:OnXinXiangShiChengSendLotteryInfo()
	local info = LuaWuYi2023Mgr.m_LotteryInfo
	self.OwnGoldLabel.text = info.haveGold
	self.OwnSilverLabel.text = info.haveSilver
	self.OwnCopperLabel.text = info.hasBronze

	if not self.m_DefaultIndex then
		self.m_DefaultIndex = (#info.itemInfo > 0 and info.numberOfDraws == #info.itemInfo) and 1 or 0
		if self.m_DefaultIndex ~= self.m_CurIndex then
			self.UITabBar:ChangeTab(self.m_DefaultIndex, false)
		end
	end
end

--@region UIEvent

function LuaWuYi2023XXSCMainWnd:OnTipButtonClick()
	g_MessageMgr:ShowMessage("WuYi2023XXSCMainWnd_ReadMe")
end

function LuaWuYi2023XXSCMainWnd:OnAddGoldBtnClick()
	local lotteryItemId = WuYi2023_XinXiangShiCheng.GetData().LotteryItemId
	CItemAccessListMgr.Inst:ShowItemAccessInfo(lotteryItemId[0], true, self.AddGoldBtn.transform, AlignType.Right)
end

function LuaWuYi2023XXSCMainWnd:OnAddSilverBtnClick()
	local lotteryItemId = WuYi2023_XinXiangShiCheng.GetData().LotteryItemId
	CItemAccessListMgr.Inst:ShowItemAccessInfo(lotteryItemId[1], true, self.AddSilverBtn.transform, AlignType.Right)
end

function LuaWuYi2023XXSCMainWnd:OnAddCopperBtnClick()
	local lotteryItemId = WuYi2023_XinXiangShiCheng.GetData().LotteryItemId
	CItemAccessListMgr.Inst:ShowItemAccessInfo(lotteryItemId[2], true, self.AddSilverBtn.transform, AlignType.Left)
end

function LuaWuYi2023XXSCMainWnd:OnTabChange(go, index)
	self.m_CurIndex = index
	self.WuYi2023XXSCMainWndSpecialView.gameObject:SetActive(index == 0)
	self.WuYi2023XXSCMainWndCommonView.gameObject:SetActive(index == 1)
	for i = 0, 1 do
		local btn = self.UITabBar:GetTabGoByIndex(i)
		local normal = btn.transform:Find("Normal")
		local select = btn.transform:Find("Select")
		normal.gameObject:SetActive(index ~= i)
		select.gameObject:SetActive(index == i)
	end
	self.m_Ani:Play(index == 0 and "wuyi2023xxscmainwnd_special_show" or "wuyi2023xxscmainwnd_common_show")
end

--@endregion UIEvent

