-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CSecondPwdWnd = import "L10.UI.CSecondPwdWnd"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CSecondPwdWnd.m_Awake_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)
    UIEventListener.Get(this.openPwdBtn).onClick = MakeDelegateFromCSFunction(this.OnClickOpenPwdButton, VoidDelegate, this)
    UIEventListener.Get(this.contentBtn).onClick = MakeDelegateFromCSFunction(this.OnClickContentButton, VoidDelegate, this)
    UIEventListener.Get(this.changePwdBtn).onClick = MakeDelegateFromCSFunction(this.OnClickChangePwdButton, VoidDelegate, this)
    UIEventListener.Get(this.closePwdBtn).onClick = MakeDelegateFromCSFunction(this.OnClickClosePwdButton, VoidDelegate, this)
end
CSecondPwdWnd.m_UpdateUI_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst:IsSecondPwdOpened() then
        this.notOpenGo:SetActive(false)
        this.openGo:SetActive(true)
    else
        this.notOpenGo:SetActive(true)
        this.openGo:SetActive(false)
    end
end
