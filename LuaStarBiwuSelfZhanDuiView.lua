local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CButton = import "L10.UI.CButton"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CCommonPlayerListMgr = import "L10.Game.CCommonPlayerListMgr"
local CIMMgr = import "L10.Game.CIMMgr"
local Profession = import "L10.Game.Profession"
local EnumCommonPlayerListUpdateType = import "L10.Game.EnumCommonPlayerListUpdateType"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local CCommonPlayerDisplayData = import "L10.Game.CCommonPlayerDisplayData"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
LuaStarBiwuSelfZhanDuiView = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaStarBiwuSelfZhanDuiView, "MemberInfoRoot", "MemberInfoRoot", GameObject)
RegistChildComponent(LuaStarBiwuSelfZhanDuiView, "Empty", "Empty", UILabel)
RegistChildComponent(LuaStarBiwuSelfZhanDuiView, "OutofDate", "OutofDate", UILabel)
RegistChildComponent(LuaStarBiwuSelfZhanDuiView, "QuitButton", "QuitButton", CButton)
RegistChildComponent(LuaStarBiwuSelfZhanDuiView, "SettingButton", "SettingButton", CButton)
RegistChildComponent(LuaStarBiwuSelfZhanDuiView, "MemberRoot", "MemberRoot", GameObject)
RegistChildComponent(LuaStarBiwuSelfZhanDuiView, "MemberInfoItem", "MemberInfoItem", GameObject)
RegistChildComponent(LuaStarBiwuSelfZhanDuiView, "SloganLabel", "SloganLabel", UILabel)
RegistChildComponent(LuaStarBiwuSelfZhanDuiView, "ModifySloganButton", "ModifySloganButton", CButton)
RegistChildComponent(LuaStarBiwuSelfZhanDuiView, "Name", "Name", UILabel)
RegistChildComponent(LuaStarBiwuSelfZhanDuiView, "TipLabel", "TipLabel", UILabel)
RegistChildComponent(LuaStarBiwuSelfZhanDuiView, "RequestButton", "RequestButton", CButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaStarBiwuSelfZhanDuiView,"m_CurrentRequestPlayerIndex")
RegistClassMember(LuaStarBiwuSelfZhanDuiView,"m_ZhanduiMemberIdTable")
RegistClassMember(LuaStarBiwuSelfZhanDuiView,"memberList")
RegistClassMember(LuaStarBiwuSelfZhanDuiView,"m_RequestRedDot")
RegistClassMember(LuaStarBiwuSelfZhanDuiView,"m_InvitedRedDot")
RegistClassMember(LuaStarBiwuSelfZhanDuiView,"m_ApplicationFirstFlag")
RegistClassMember(LuaStarBiwuSelfZhanDuiView,"m_EndStamp")
function LuaStarBiwuSelfZhanDuiView:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.QuitButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnQuitButtonClick()
	end)


	
	UIEventListener.Get(self.SettingButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSettingButtonClick()
	end)


	
	UIEventListener.Get(self.ModifySloganButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnModifySloganButtonClick()
	end)


	
	UIEventListener.Get(self.RequestButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRequestButtonClick()
	end)

	local InvitedButton = self.Empty.transform:Find("InvitedButton").gameObject
	local platformBtn = self.Empty.transform:Find("PlatformButton").gameObject
	UIEventListener.Get(InvitedButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnInvitedBtnClick()
	end)

	UIEventListener.Get(platformBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    g_ScriptEvent:BroadcastInLua("OnStarBiWuZhanDuiPlatformChangeTab", 0)
	end)


    --@endregion EventBind end
	self.m_RequestRedDot = self.RequestButton.transform:Find("Alert").gameObject
	self.m_RequestRedDot.gameObject:SetActive(false)
	self.m_InvitedRedDot = self.Empty.transform:Find("InvitedButton/Alert").gameObject
	self.m_InvitedRedDot.gameObject:SetActive(false)
	self.m_ApplicationFirstFlag = true
	self.memberList = nil
	self.m_ZhanduiMemberIdTable = nil
	self.m_CurrentRequestPlayerIndex = -1
    self.m_EndStamp = 0
	self:InitMembers()
    self:CheckEndTime()
end

function LuaStarBiwuSelfZhanDuiView:CheckEndTime()
    local time = StarBiWuShow_Setting.GetData().Create_ZhanDui_End_Time
    self.m_EndStamp = CServerTimeMgr.Inst:GetTimeStampByStr(time)
    if CServerTimeMgr.Inst.timeStamp >= self.m_EndStamp then
        self.MemberInfoRoot.gameObject:SetActive(false)
        self.Empty.gameObject:SetActive(false)
        self.OutofDate.gameObject:SetActive(true)
    else
        self:InitNoZhanDui()
    end
end

function LuaStarBiwuSelfZhanDuiView:InitMembers()
	self.memberList ={}
    self.MemberInfoItem:SetActive(false)
    do
        local i = 0
        while i < StarBiWuShow_Setting.GetData().Max_ZhanDui_MemberNum do
            local go = NGUITools.AddChild(self.MemberRoot.gameObject,self.MemberInfoItem)
            go:SetActive(false)
            local trans = go.transform
            self:SetItemSelected(trans,false)
            table.insert( self.memberList,trans )
            i = i + 1
        end
        self.MemberRoot:GetComponent(typeof(UIGrid)):Reposition()
    end
end

function LuaStarBiwuSelfZhanDuiView:SetItemSelected(transform,state)
    local m_SelectedObj = transform:Find("InfoRoot/Sprite").gameObject
    m_SelectedObj:SetActive(state)
end

function LuaStarBiwuSelfZhanDuiView:UpdateZhanDuiNameAndSlogan( )
    self.Name.text = CLuaStarBiwuMgr.m_CurrentZhanduiName
    self.SloganLabel.text = CLuaStarBiwuMgr.m_CurrentZhanduiSlogan
end

function LuaStarBiwuSelfZhanDuiView:ReplyStarBiwuZhanDuiInfo( )
    if CLuaStarBiwuMgr.m_MySelfZhanduiId <= 0 then
        if CServerTimeMgr.Inst.timeStamp >= self.m_EndStamp then
            self.MemberInfoRoot.gameObject:SetActive(false)
            self.Empty.gameObject:SetActive(false)
            self.OutofDate.gameObject:SetActive(true)
        else
            self:InitNoZhanDui()
        end
        return
    end 

    if CLuaStarBiwuMgr.m_MySelfZhanduiId ~= CLuaStarBiwuMgr.m_CurrentZhanduiId then
        return
    end
	self.MemberInfoRoot.gameObject:SetActive(true)
	self.Empty.gameObject:SetActive(false)
    self.OutofDate.gameObject:SetActive(false)
    self.m_ZhanduiMemberIdTable = {}
    do
        local i = 0
        local maxCnt = StarBiWuShow_Setting.GetData().Max_ZhanDui_MemberNum
        while i < maxCnt do
            local go=self.memberList[i+1].gameObject
            local tf=go.transform
            go:SetActive(true)
            if i < #CLuaStarBiwuMgr.m_MemberInfoTable then
                local info=CLuaStarBiwuMgr.m_MemberInfoTable[i + 1]
                self:InitMemberItem(tf,CLuaStarBiwuMgr.m_MemberInfoTable[i + 1], i, CLuaStarBiwuMgr.m_ChuZhanInfoList)
                UIEventListener.Get(go).onClick=DelegateFactory.VoidDelegate(function(p)
                    self:OnItemClicked(info.m_Id,p)
                end)
                self.m_ZhanduiMemberIdTable[info.m_Id] = 1
            else
                self:InitMemberItem(tf,nil,i,nil)
                UIEventListener.Get(go).onClick=DelegateFactory.VoidDelegate(function(p)
                    self:OnInviteBtnClick()
                end)
            end
            i = i + 1
        end
    end
    self:UpdateZhanDuiNameAndSlogan( )
    self.TipLabel.text = CLuaStarBiwuMgr.m_MyZhanduiTip
	if CLuaStarBiwuMgr.m_RequestZhanDuiMemberList then
		self:OnReplyRequestPlayers(CLuaStarBiwuMgr.m_RequestZhanDuiMemberList)
	end
end

function LuaStarBiwuSelfZhanDuiView:OnItemClicked( playerId, go)
    if playerId <= 0 then
        CUIManager.ShowUI(CLuaUIResources.QMPKRecruitWnd)
        return
    end
    CPlayerInfoMgr.ShowPlayerPopupMenu(math.floor(playerId), EnumPlayerInfoContext.StarBiwu, EChatPanel.Undefined, nil, nil, go.transform.position, CPlayerInfoMgr.AlignType.Right)

    do
        local i = 0 local cnt = #self.memberList
        while i < cnt do
            self:SetItemSelected(self.memberList[i+1],self.memberList[i+1].gameObject == go)
            i = i + 1
        end
    end
end

function LuaStarBiwuSelfZhanDuiView:OnReplyRequestPlayers( players )
	local needShow = false
    if #players > 0 and self.m_ApplicationFirstFlag and CLuaStarBiwuMgr.m_IsTeamLeader then
        needShow = true
    end
	self.m_RequestRedDot.gameObject:SetActive(needShow)
end

function LuaStarBiwuSelfZhanDuiView:OnReplyInvitedTeams( )
    local teams = CLuaStarBiwuMgr.m_InvitedZhanDuiMemberList
	local needShow = false
    if #teams > 0 and self.m_ApplicationFirstFlag and CLuaStarBiwuMgr.m_MySelfZhanduiId == 0 then
        needShow = true
    end
	self.m_InvitedRedDot.gameObject:SetActive(needShow)
end

function LuaStarBiwuSelfZhanDuiView:InitMemberItem(transform,member, index, chuZhanInfo)
    local IconTexture = transform:Find("InfoRoot/Icon"):GetComponent(typeof(CUITexture))
    local LevelLabel = transform:Find("InfoRoot/LevelBG/LevelLabel"):GetComponent(typeof(UILabel))
    local NameLabel = transform:Find("InfoRoot/NameLabel"):GetComponent(typeof(UILabel))
    local LeaderMark = transform:Find("InfoRoot/LeaderMark").gameObject
    local ClazzMarkSprite = transform:Find("InfoRoot/ClazzSprite"):GetComponent(typeof(UISprite))
    local m_GroupObj = transform:Find("InfoRoot/ChuZhanSprite1").gameObject
    local m_ThreeVThreeObj = transform:Find("InfoRoot/ChuZhanSprite2").gameObject
    local m_SigleObj = transform:Find("InfoRoot/ChuZhanSprite3").gameObject
    local m_TwoVTwoObj = transform:Find("InfoRoot/ChuZhanSprite4").gameObject
    local m_SigleIndexLabel = transform:Find("InfoRoot/ChuZhanSprite3/BG/Label"):GetComponent(typeof(UILabel))
    local m_SelectedObj = transform:Find("InfoRoot/Sprite").gameObject
    local m_NoMemberObj = transform:Find("Label").gameObject
    local m_InfoRoot = transform:Find("InfoRoot").gameObject

    local m_PlayerId=0

    if nil == member then
        m_NoMemberObj:SetActive(true)
        m_InfoRoot:SetActive(false)
        m_PlayerId = 0
        return
    end
    m_NoMemberObj:SetActive(false)
    m_InfoRoot:SetActive(true)
    m_SelectedObj:SetActive(false)
    m_PlayerId = member.m_Id
    IconTexture:LoadNPCPortrait(CUICommonDef.GetPortraitName(member.m_Class, member.m_Gender, -1), false)
    LevelLabel.text = System.String.Format("lv.{0}", member.m_Grade)
    NameLabel.text = member.m_Name
    LeaderMark:SetActive(index == 0)
    ClazzMarkSprite.spriteName = Profession.GetIcon(member.m_Class)
    if CClientMainPlayer.Inst ~= nil and m_PlayerId == CClientMainPlayer.Inst.Id then
        NameLabel.color = Color(16 / 255, 140/ 255, 0)
    else
        NameLabel.color = Color.white
    end
    if chuZhanInfo == nil or chuZhanInfo.Length < 6 then
        return
    end
    if (bit.band(chuZhanInfo[0], (bit.lshift(1, index)))) ~= 0 or (bit.band(chuZhanInfo[4], (bit.lshift(1, index)))) ~= 0 then
        m_GroupObj:SetActive(true)
    else
        m_GroupObj:SetActive(false)
    end
    if (bit.band(chuZhanInfo[1], (bit.lshift(1, index)))) ~= 0 or (bit.band(chuZhanInfo[5], (bit.lshift(1, index)))) ~= 0 then
        m_SigleObj:SetActive(true)
        if (bit.band(chuZhanInfo[1], (bit.lshift(1, index)))) ~= 0 then
            m_SigleIndexLabel.text = LocalString.GetString("一")
        else
            m_SigleIndexLabel.text = LocalString.GetString("二")
        end
    else
        m_SigleObj:SetActive(false)
    end
    if (bit.band(chuZhanInfo[3], (bit.lshift(1, index)))) ~= 0 then
        m_ThreeVThreeObj:SetActive(true)
    else
        m_ThreeVThreeObj:SetActive(false)
    end

    m_TwoVTwoObj:SetActive((bit.band(chuZhanInfo[2], (bit.lshift(1, index)))) ~= 0)
end

function LuaStarBiwuSelfZhanDuiView:InitNoZhanDui()
	self.MemberInfoRoot.gameObject:SetActive(false)
	self.Empty.gameObject:SetActive(true)
    self.OutofDate.gameObject:SetActive(false)
	if CLuaStarBiwuMgr.m_InvitedZhanDuiMemberList then
		self:OnReplyInvitedTeams()
	end
end


--@region UIEvent
function LuaStarBiwuSelfZhanDuiView:OnInviteBtnClick()
	if not CClientMainPlayer.Inst then return end
  
	  CCommonPlayerListMgr.Inst.WndTitle = LocalString.GetString("邀请战队成员")
	  CCommonPlayerListMgr.Inst.ButtonText = LocalString.GetString("邀请")
	  CCommonPlayerListMgr.Inst.UpdateType = EnumCommonPlayerListUpdateType.Default
	  CommonDefs.ListClear(CCommonPlayerListMgr.Inst.allData)
  
	CommonDefs.DictIterate(CClientMainPlayer.Inst.RelationshipProp.Friends, DelegateFactory.Action_object_object(function(k, v)
	  local basicInfo = CIMMgr.Inst:GetBasicInfo(k)
	  if basicInfo and not self.m_ZhanduiMemberIdTable[basicInfo.ID] then
		local data = CreateFromClass(CCommonPlayerDisplayData, basicInfo.ID, basicInfo.Name, basicInfo.Level, basicInfo.Class, basicInfo.Gender, basicInfo.Expression, true)
		CommonDefs.ListAdd(CCommonPlayerListMgr.Inst.allData, typeof(CCommonPlayerDisplayData), data)
	  end
	end))
  
	local callback = function (playerId)
	  	Gac2Gas.IntiveOtherJoinStarBiwuZhanDui(playerId)
	end
  
	CCommonPlayerListMgr.Inst.OnPlayerSelected = DelegateFactory.Action_ulong(callback)
	CUIManager.ShowUI(CIndirectUIResources.CommonPlayerListWnd)
end

function LuaStarBiwuSelfZhanDuiView:OnQuitButtonClick()
	MessageWndManager.ShowOKCancelMessage(LocalString.GetString("确定要退出战队吗？"), DelegateFactory.Action(function()
        self:confirmQuit()
    end), nil, nil, nil, false)
end

function LuaStarBiwuSelfZhanDuiView:confirmQuit()
	Gac2Gas.RequestLeaveStarBiwuZhanDui()
    CUIManager.CloseUI(CLuaUIResources.StarBiwuZhanDuiPlatformWnd)
end

function LuaStarBiwuSelfZhanDuiView:OnSettingButtonClick()
	CUIManager.ShowUI(CLuaUIResources.StarBiwuZhanDuiSettingWnd)
end

function LuaStarBiwuSelfZhanDuiView:OnModifySloganButtonClick()
	CUIManager.ShowUI(CLuaUIResources.StarBiwuModifySloganWnd)
end

function LuaStarBiwuSelfZhanDuiView:OnRequestButtonClick()
	self.m_ApplicationFirstFlag = false
	self.m_RequestRedDot:SetActive(false)
	g_ScriptEvent:BroadcastInLua("OnStarBiWuZhanDuiPlatformClickRedDot")
	CUIManager.ShowUI(CLuaUIResources.StarBiWuZhanDuiRequestWnd)
end

function LuaStarBiwuSelfZhanDuiView:OnInvitedBtnClick()
	self.m_ApplicationFirstFlag = false
	self.m_InvitedRedDot:SetActive(false)
	g_ScriptEvent:BroadcastInLua("OnStarBiWuZhanDuiPlatformClickRedDot")
	CUIManager.ShowUI(CLuaUIResources.StarBiWuZhanDuiInvitedWnd)
end

--@endregion UIEvent
function LuaStarBiwuSelfZhanDuiView:OnEnable()
	g_ScriptEvent:BroadcastInLua("OnStarBiWuZhanDuiPlatformWndChange", LocalString.GetString("我的战队"))
	g_ScriptEvent:AddListener("ReplyStarBiwuZhanDuiInfo", self, "ReplyStarBiwuZhanDuiInfo")
	g_ScriptEvent:AddListener("UpdateStarBiwuZhanDuiTitleAndKouHaoSuccess", self, "UpdateZhanDuiNameAndSlogan")
	g_ScriptEvent:AddListener("ReplyStarBiwuZhanDuiApplyList", self, "OnReplyRequestPlayers")
	g_ScriptEvent:AddListener("ReplyStarBiwuZhanDuiInvitedList", self, "OnReplyInvitedTeams")
	Gac2Gas.RequestSelfStarBiwuZhanDuiInfo()
end

function LuaStarBiwuSelfZhanDuiView:OnDisable()
	g_ScriptEvent:RemoveListener("ReplyStarBiwuZhanDuiInfo", self, "ReplyStarBiwuZhanDuiInfo")
	g_ScriptEvent:RemoveListener("UpdateStarBiwuZhanDuiTitleAndKouHaoSuccess", self, "UpdateZhanDuiNameAndSlogan")
	g_ScriptEvent:RemoveListener("ReplyStarBiwuZhanDuiApplyList", self, "OnReplyRequestPlayers")
	g_ScriptEvent:RemoveListener("ReplyStarBiwuZhanDuiInvitedList", self, "OnReplyInvitedTeams")
end
