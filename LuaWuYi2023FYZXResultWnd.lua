local UITable = import "UITable"
local UIGrid = import "UIGrid"
local UILabel = import "UILabel"
local UISlider = import "UISlider"
local GameObject = import "UnityEngine.GameObject"
local DelegateFactory  = import "DelegateFactory"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"
local CLoginMgr = import "L10.Game.CLoginMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local Animation = import "UnityEngine.Animation"

LuaWuYi2023FYZXResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaWuYi2023FYZXResultWnd, "ReStartButton", "ReStartButton", GameObject)
RegistChildComponent(LuaWuYi2023FYZXResultWnd, "ShareButton", "ShareButton", GameObject)
RegistChildComponent(LuaWuYi2023FYZXResultWnd, "Win", "Win", GameObject)
RegistChildComponent(LuaWuYi2023FYZXResultWnd, "Lose", "Lose", GameObject)
RegistChildComponent(LuaWuYi2023FYZXResultWnd, "Tie", "Tie", GameObject)
RegistChildComponent(LuaWuYi2023FYZXResultWnd, "Logo", "Logo", GameObject)
RegistChildComponent(LuaWuYi2023FYZXResultWnd, "PlayerInfoNode", "PlayerInfoNode", GameObject)
RegistChildComponent(LuaWuYi2023FYZXResultWnd, "InfoLabel1", "InfoLabel1", UILabel)
RegistChildComponent(LuaWuYi2023FYZXResultWnd, "InfoLabel2", "InfoLabel2", UILabel)
RegistChildComponent(LuaWuYi2023FYZXResultWnd, "CoinNumLabel1", "CoinNumLabel1", UILabel)
RegistChildComponent(LuaWuYi2023FYZXResultWnd, "CoinNumLabel2", "CoinNumLabel2", UILabel)
RegistChildComponent(LuaWuYi2023FYZXResultWnd, "Slider1", "Slider1", UISlider)
RegistChildComponent(LuaWuYi2023FYZXResultWnd, "Slider2", "Slider2", UISlider)
RegistChildComponent(LuaWuYi2023FYZXResultWnd, "Template1", "Template1", GameObject)
RegistChildComponent(LuaWuYi2023FYZXResultWnd, "Template2", "Template2", GameObject)
RegistChildComponent(LuaWuYi2023FYZXResultWnd, "Grid1", "Grid1", UIGrid)
RegistChildComponent(LuaWuYi2023FYZXResultWnd, "Grid2", "Grid2", UIGrid)
RegistChildComponent(LuaWuYi2023FYZXResultWnd, "RewardTable", "RewardTable", UITable)
RegistChildComponent(LuaWuYi2023FYZXResultWnd, "RewardItem1", "RewardItem1", GameObject)
RegistChildComponent(LuaWuYi2023FYZXResultWnd, "RewardItem2", "RewardItem2", GameObject)
RegistChildComponent(LuaWuYi2023FYZXResultWnd, "LianZhan", "LianZhan", GameObject)
RegistChildComponent(LuaWuYi2023FYZXResultWnd, "LianZhanNumLabel", "LianZhanNumLabel", UILabel)
RegistChildComponent(LuaWuYi2023FYZXResultWnd, "RewardView", "RewardView", GameObject)
RegistChildComponent(LuaWuYi2023FYZXResultWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaWuYi2023FYZXResultWnd, "RewardItem3", "RewardItem3", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaWuYi2023FYZXResultWnd,"m_Ani")

function LuaWuYi2023FYZXResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ReStartButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnReStartButtonClick()
	end)


	
	UIEventListener.Get(self.ShareButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareButtonClick()
	end)


    --@endregion EventBind end
end

function LuaWuYi2023FYZXResultWnd:Init()
	self.Logo.gameObject:SetActive(false)
	self.PlayerInfoNode.gameObject:SetActive(false)
	self.Template1.gameObject:SetActive(false)
	self.Template2.gameObject:SetActive(false)
	self.LianZhan.gameObject:SetActive(false)
	self:InitBottomPlayerInfoNode()
	self:InitReward()
	self:InitTeamInfo()
	self:InitPlayersInfo()
	local info = LuaWuYi2023Mgr.m_PlayResultInfo
	self.Win.gameObject:SetActive(false)
	self.Lose.gameObject:SetActive(false)
	self.Tie.gameObject:SetActive(false)
	local panel = self.transform:GetComponent(typeof(UIPanel))
	panel.alpha = 1
	self.m_Ani = self.transform:GetComponent(typeof(Animation))
	local aniArr = {[1] = "common_shengli_1",[0] = "common_shibai_1", [2] = "common_pingju_1"}
	self.m_Ani:Play(aniArr[info.win])
end

function LuaWuYi2023FYZXResultWnd:InitReward()
	local rewardItemIds = WuYi2023_FenYongZhenXian.GetData().RewardItemIds 
	if rewardItemIds.Length < 3 then return end
	self:InitRewardItem(self.RewardItem1, rewardItemIds[0])
    self:InitRewardItem(self.RewardItem2, rewardItemIds[1])
    self:InitRewardItem(self.RewardItem3, rewardItemIds[2])
	local info = LuaWuYi2023Mgr.m_PlayResultInfo
	self.RewardItem1.gameObject:SetActive(info.winReward > 0)
	self.RewardItem2.gameObject:SetActive(info.extraReward > 0)
	self.RewardItem3.gameObject:SetActive(info.loseReward > 0)
	self.RewardTable:Reposition()
	self.RewardView.gameObject:SetActive(info.winReward > 0 or info.extraReward > 0 or info.loseReward > 0)
end

function LuaWuYi2023FYZXResultWnd:InitRewardItem(item, itemId)
    local icon = item.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local qualitySprite = item.transform:Find("QualitySprite"):GetComponent(typeof(UISprite))
    
    local itemData = Item_Item.GetData(itemId)
    icon:LoadMaterial(itemData.Icon)
    qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(itemData.NameColor)
    UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType.Default, 0, 0, 0, 0)
	end)
end

function LuaWuYi2023FYZXResultWnd:InitTeamInfo()
	local info = LuaWuYi2023Mgr.m_PlayResultInfo
	if info.teamInfo then
		local force1, force2 = 0, 1
		local info1 = info.teamInfo[force1]
		local info2 = info.teamInfo[force2]
		self.CoinNumLabel1.text = info1.score
		self.CoinNumLabel2.text = info2.score
		if info1.remain > 0 then
			self.InfoLabel1.text = SafeStringFormat3(LocalString.GetString("[99e3ff]蓝方剩余[ffffff]%d米"), info1.remain)
			self.Slider1.value = (info1.total - info1.remain) / info1.total
		else
			local totalSeconds = info1.usetime
			self.InfoLabel1.text = SafeStringFormat3(LocalString.GetString("[99e3ff]蓝方用时[ffffff]%02d:%02d"), math.floor(totalSeconds / 60), totalSeconds % 60)
			self.Slider1.value = 1
		end
		if info2.remain > 0 then
			self.InfoLabel2.text = SafeStringFormat3(LocalString.GetString("[ffc8c8]红方剩余[ffffff]%d米"), info2.remain)
			self.Slider2.value = (info2.total - info2.remain) / info2.total
		else
			local totalSeconds = info2.usetime
			self.InfoLabel2.text = SafeStringFormat3(LocalString.GetString("[ffc8c8]红方用时[ffffff]%02d:%02d"), math.floor(totalSeconds / 60), totalSeconds % 60)
			self.Slider2.value = 1
		end
	end
	self.InfoLabel1.color = Color.white
	self.InfoLabel2.color = Color.white
end

function LuaWuYi2023FYZXResultWnd:InitPlayersInfo()
	Extensions.RemoveAllChildren(self.Grid1.transform)
	Extensions.RemoveAllChildren(self.Grid2.transform)
	local info = LuaWuYi2023Mgr.m_PlayResultInfo
	if info.playerInfo then
		for id, playerData in pairs(info.playerInfo) do
			print(id, playerData,playerData.force)
			local obj = NGUITools.AddChild(playerData.force == 0 and self.Grid1.gameObject or self.Grid2.gameObject, playerData.force == 0 and self.Template1.gameObject or self.Template2.gameObject)
			obj:SetActive(true)
			self:InitTemplate(obj, playerData, id)
		end
	end
	self.Grid1:Reposition()
	self.Grid2:Reposition()
end

function LuaWuYi2023FYZXResultWnd:InitTemplate(item, data, playerId)
	local portrait = item.transform:Find("Portrait"):GetComponent(typeof(CUITexture))
	local levelLabel = item.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
	local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
	local label2 = item.transform:Find("Label2"):GetComponent(typeof(UILabel))
	local label3 = item.transform:Find("Label3"):GetComponent(typeof(UILabel))
	local label4 = item.transform:Find("Label4"):GetComponent(typeof(UILabel))
	local label5 = item.transform:Find("Label5"):GetComponent(typeof(UILabel))
	local skillIcon = item.transform:Find("Panel/SkillIcon"):GetComponent(typeof(CUITexture))

	portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(data.class, data.gender,-1), false)
	levelLabel.text = data.level
	levelLabel.color = data.isFeiSheng and NGUIText.ParseColor24("fe7900", 0) or Color.white
	nameLabel.text = data.playerName
	label2.text = data.killEnemy
	label3.text = data.killHorce
	label4.text = data.control
	label5.text = data.protect
	local color = NGUIText.ParseColor24(data.force == 0 and "99E3FF" or "FFC8C8", 0)
	if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == playerId then
		color = Color.green
	end
	nameLabel.color = color
	label2.color = color
	label3.color = color
	label4.color = color
	label5.color = color
	local skillData = Skill_AllSkills.GetData(data.chooseSkillId)
	if skillData then
		skillIcon:LoadSkillIcon(skillData.SkillIcon)
	end
	skillIcon.gameObject:SetActive(skillData)
	UIEventListener.Get(skillIcon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    CSkillInfoMgr.ShowSkillInfoWnd(data.chooseSkillId, true, 0, 0, nil)
	end)
end

function LuaWuYi2023FYZXResultWnd:InitBottomPlayerInfoNode()
	if not CClientMainPlayer.Inst then
		return
	end
	local myGameServer = CLoginMgr.Inst:GetSelectedGameServer()
	self.PlayerInfoNode.transform:Find("Name"):GetComponent(typeof(UILabel)).text = CClientMainPlayer.Inst.Name
	self.PlayerInfoNode.transform:Find("ID"):GetComponent(typeof(UILabel)).text = CClientMainPlayer.Inst.Id
	self.PlayerInfoNode.transform:Find("Server"):GetComponent(typeof(UILabel)).text = myGameServer.name
	self.PlayerInfoNode.transform:Find("Icon"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(CClientMainPlayer.Inst.Class, CClientMainPlayer.Inst.Gender,-1),false)
	local levelLabel = self.PlayerInfoNode.transform:Find("Icon/Lv"):GetComponent(typeof(UILabel))
	levelLabel.text = CClientMainPlayer.Inst.Level
	levelLabel.color = CClientMainPlayer.Inst.HasFeiSheng and NGUIText.ParseColor24("fe7900", 0) or Color.white
	local info = LuaWuYi2023Mgr.m_PlayResultInfo
	self.LianZhanNumLabel.text = info.combo and info.combo or ""
end

--@region UIEvent

function LuaWuYi2023FYZXResultWnd:OnReStartButtonClick()
	CUIManager.CloseUI(CLuaUIResources.WuYi2023FYZXResultWnd)
	CUIManager.ShowUI(CLuaUIResources.WuYi2023FYZXGamePlayWnd)
end


function LuaWuYi2023FYZXResultWnd:OnShareButtonClick()
	self.ShareButton.gameObject:SetActive(false)
	self.RewardView.gameObject:SetActive(false)
	self.CloseButton.gameObject:SetActive(false)
	self.ReStartButton.gameObject:SetActive(false)
	self.Logo.gameObject:SetActive(true)
	self.PlayerInfoNode.gameObject:SetActive(true)
	local info = LuaWuYi2023Mgr.m_PlayResultInfo
	self.LianZhan.gameObject:SetActive(info.combo and info.combo > 1)
	CUICommonDef.CaptureScreen(
        "screenshot",
        true,
        false,
        nil,
        DelegateFactory.Action_string_bytes(
            function(fullPath, jpgBytes)
				self.ShareButton.gameObject:SetActive(true)
				self.RewardView.gameObject:SetActive(info.winReward > 0 or info.extraReward > 0 or info.loseReward > 0)
				self.CloseButton.gameObject:SetActive(true)
				self.ReStartButton.gameObject:SetActive(true)
				self.Logo.gameObject:SetActive(false)
				self.PlayerInfoNode.gameObject:SetActive(false)
				self.LianZhan.gameObject:SetActive(false)
                ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
            end
        ),
        false
    )
end


--@endregion UIEvent

