-- Auto Generated!!
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPropertyDifMgr = import "L10.UI.CPropertyDifMgr"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local CTalismanWordBaptizeMgr = import "L10.UI.CTalismanWordBaptizeMgr"
local CTalismanWordBaptizeWnd = import "L10.UI.CTalismanWordBaptizeWnd"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local EventManager = import "EventManager"
local Gac2Gas = import "L10.Game.Gac2Gas"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Object = import "System.Object"
local PlayerShowDifProType = import "L10.Game.PlayerShowDifProType"
local String = import "System.String"
local Talisman_WordOption = import "L10.Game.Talisman_WordOption"
local Talisman_XianJia = import "L10.Game.Talisman_XianJia"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CTalismanWordBaptizeWnd.m_Init_CS2LuaHook = function (this) 
    CTalismanWordBaptizeMgr.InitConditionWord()
    this.wordList.needConfirm = false
    local default
    if CTalismanWordBaptizeMgr.baseWordBaptize then
        default = LocalString.GetString("法宝重铸")
    else
        default = LocalString.GetString("法宝炼化")
    end
    this.titleLabel.text = default
    this.baptizeCountLabel.text = ""
    local extern
    if CTalismanWordBaptizeMgr.baseWordBaptize then
        extern = LocalString.GetString("重铸")
    else
        extern = LocalString.GetString("炼化")
    end
    this.recoinButtonLabel.text = extern
    this.wordList:Init()
    this.itemInfo:Init()

    UIEventListener.Get(this.lookupDifBtn).onClick = MakeDelegateFromCSFunction(this.OnClickLookupDifButton, VoidDelegate, this)
end
CTalismanWordBaptizeWnd.m_OnClickLookupDifButton_CS2LuaHook = function (this, go) 
    local talisman = CTalismanWordBaptizeMgr.selectTalisman
    local equipListOnBody = CItemMgr.Inst:GetPlayerEquipmentListAtPlace(EnumItemPlace.Body)

    local find = false
    do
        local i = 0
        while i < equipListOnBody.Count do
            if equipListOnBody[i].itemId == talisman.itemId then
                find = true
                break
            end
            i = i + 1
        end
    end
    if find then
        --是否重铸
        if CTalismanWordBaptizeMgr.baseWordBaptize then
            Gac2Gas.TryConfirmResetTalismanBasicWord(EnumToInt(talisman.place), talisman.pos, talisman.itemId)
        else
            --炼化
            Gac2Gas.TryConfirmResetTalismanExtraWord(EnumToInt(talisman.place), talisman.pos, talisman.itemId)
        end
    else
        g_MessageMgr:ShowMessage("Equip_Compare_NotWear_Tips")
    end
end
CTalismanWordBaptizeWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.recoinButton.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.recoinButton.gameObject).onClick, MakeDelegateFromCSFunction(this.OnRecoinButtonClick, VoidDelegate, this), true)

    EventManager.AddListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
    EventManager.AddListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
    EventManager.AddListener(EnumEventType.ResetTalismanWordFailed, MakeDelegateFromCSFunction(this.OnResetWordFail, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.TryConfirmBaptizeWordResult, MakeDelegateFromCSFunction(this.OnTryConfirmBaptizeWordResult, MakeGenericClass(Action1, Object), this))
end
CTalismanWordBaptizeWnd.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.recoinButton.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.recoinButton.gameObject).onClick, MakeDelegateFromCSFunction(this.OnRecoinButtonClick, VoidDelegate, this), false)

    EventManager.RemoveListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
    EventManager.RemoveListener(EnumEventType.ResetTalismanWordFailed, MakeDelegateFromCSFunction(this.OnResetWordFail, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.TryConfirmBaptizeWordResult, MakeDelegateFromCSFunction(this.OnTryConfirmBaptizeWordResult, MakeGenericClass(Action1, Object), this))
end
CTalismanWordBaptizeWnd.m_OnTryConfirmBaptizeWordResult_CS2LuaHook = function (this, obj) 
    local list = TypeAs(obj, typeof(MakeGenericClass(List, PlayerShowDifProType)))
    if list == nil then
        return
    end
    if list.Count == 0 then
        g_MessageMgr:ShowMessage("BaptizeWordResultNoPropDif")
        return
    end
    CPropertyDifMgr.ShowPlayerProDifWithEquip(list)
end
CTalismanWordBaptizeWnd.m_OnCloseButtonClick_CS2LuaHook = function (this, go) 
    if this.wordList.needConfirm then
        --string tip = MessageMgr.Inst.FormatMessage("Close_Crafting_Equipment");
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Talisman_Close_Confirm"), MakeDelegateFromCSFunction(this.Close, Action0, this), nil, nil, nil, false)
    else
        this:Close()
    end
end
CTalismanWordBaptizeWnd.m_OnRecoinButtonClick_CS2LuaHook = function (this, go) 
    if not CTalismanWordBaptizeMgr.baseWordBaptize and CTalismanWordBaptizeMgr.CheckBaptizeResult() then
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("TALISMAN__LIANHUA_COMFIRM"), DelegateFactory.Action(function () 
            this:Baptize()
        end), nil, nil, nil, false)
    else
        this:Baptize()
    end
end
CTalismanWordBaptizeWnd.m_Baptize_CS2LuaHook = function (this) 
    if this.itemInfo.countAvailable and this.itemInfo.yinliangAvailable then
        if CTalismanWordBaptizeMgr.selectTalisman ~= nil then
            this.wordList.needConfirm = true
            if CTalismanWordBaptizeMgr.baseWordBaptize then
                Gac2Gas.RequestResetTalismanBasicWord(EnumToInt(CTalismanWordBaptizeMgr.selectTalisman.place), CTalismanWordBaptizeMgr.selectTalisman.pos, CTalismanWordBaptizeMgr.selectTalisman.itemId, false)
            else
                Gac2Gas.RequestResetTalismanExtraWord(EnumToInt(CTalismanWordBaptizeMgr.selectTalisman.place), CTalismanWordBaptizeMgr.selectTalisman.pos, CTalismanWordBaptizeMgr.selectTalisman.itemId, false)
            end
            this.recoinButton.Enabled = false
        end
    elseif not this.itemInfo.yinliangAvailable then
        g_MessageMgr:ShowMessage("NotEnough_Silver")
    elseif not this.itemInfo.countAvailable then
        if not this.itemInfo.useLingyu then
            g_MessageMgr:ShowMessage("Talisman_Refinery_No_Item")
        elseif this.itemInfo.lingyuAvailable then
            this.wordList.needConfirm = true
            if CTalismanWordBaptizeMgr.baseWordBaptize then
                Gac2Gas.RequestResetTalismanBasicWord(EnumToInt(CTalismanWordBaptizeMgr.selectTalisman.place), CTalismanWordBaptizeMgr.selectTalisman.pos, CTalismanWordBaptizeMgr.selectTalisman.itemId, true)
            else
                Gac2Gas.RequestResetTalismanExtraWord(EnumToInt(CTalismanWordBaptizeMgr.selectTalisman.place), CTalismanWordBaptizeMgr.selectTalisman.pos, CTalismanWordBaptizeMgr.selectTalisman.itemId, true)
            end
            this.recoinButton.Enabled = false
        else
            CShopMallMgr.TryCheckEnoughJade(this.itemInfo.lingyuCost, this.itemInfo.TemplateId)
        end
    end
end
CTalismanWordBaptizeMgr.m_InitConditionWord_CS2LuaHook = function () 
    local equipInfo = CTalismanWordBaptizeMgr.selectTalisman
    if equipInfo == nil then
        return
    end
    if CTalismanWordBaptizeMgr.autoBaptizeWordList == nil then
        CTalismanWordBaptizeMgr.autoBaptizeWordList = CreateFromClass(MakeGenericClass(Dictionary, UInt32, UInt32))
    else
        CommonDefs.DictClear(CTalismanWordBaptizeMgr.autoBaptizeWordList)
    end
    local talisman = EquipmentTemplate_Equip.GetData(equipInfo.templateId)
    if talisman ~= nil then
        local wordOption = Talisman_WordOption.GetData(talisman.Type)
        if wordOption ~= nil then
            local xianjia = Talisman_XianJia.GetData(equipInfo.templateId)
            local wordStr = CommonDefs.StringSplit_ArrayChar(wordOption.Word, ";")
            if xianjia == nil then
                wordStr = CommonDefs.StringSplit_ArrayChar(wordOption.NormalWord, ";")
            end
            if wordStr ~= nil then
                do
                    local i = 0
                    while i < wordStr.Length do
                        local condition = CommonDefs.StringSplit_ArrayChar(wordStr[i], ",")
                        if condition ~= nil and condition.Length >= 2 then
                            local wordId, amount
                            local default
                            default, wordId = System.UInt32.TryParse(condition[0])
                            local extern
                            extern, amount = System.UInt32.TryParse(condition[1])
                            if default and extern then
                                if not CommonDefs.DictContains(CTalismanWordBaptizeMgr.autoBaptizeWordList, typeof(UInt32), wordId) then
                                    CommonDefs.DictAdd(CTalismanWordBaptizeMgr.autoBaptizeWordList, typeof(UInt32), wordId, typeof(UInt32), amount)
                                end
                            end
                        end
                        i = i + 1
                    end
                end
            end
        end
    end
    CTalismanWordBaptizeMgr.checkScore = false
end
CTalismanWordBaptizeMgr.m_CheckBaptizeResult_CS2LuaHook = function () 
    if CTalismanWordBaptizeMgr.autoBaptizeWordList == nil then
        return true
    end
    --检查评分
    if CTalismanWordBaptizeMgr.checkScore and CTalismanWordBaptizeMgr.postScore > CTalismanWordBaptizeMgr.preScore then
        return true
    end

    local talisman = CItemMgr.Inst:GetById(CTalismanWordBaptizeMgr.selectTalisman.itemId)
    local wordContents = talisman.Equip:GetTempExtWordArray()
    local wordDic = CreateFromClass(MakeGenericClass(Dictionary, UInt32, UInt32))
    do
        local i = 0
        while i < wordContents.Length do
            local wordId = math.floor(wordContents[i] / 100)
            if CommonDefs.DictContains(wordDic, typeof(UInt32), wordId) then
                DictElePosIncDec(wordDic, typeof(UInt32), wordId, typeof(UInt32), 1)
            else
                CommonDefs.DictAdd(wordDic, typeof(UInt32), wordId, typeof(UInt32), 1)
            end
            i = i + 1
        end
    end
    do
        local __itr_is_triger_return = nil
        local __itr_result = nil
        CommonDefs.DictIterateWithRet(CTalismanWordBaptizeMgr.autoBaptizeWordList, DelegateFactory.DictIterFunc(function (___key, ___value) 
            local word = {}
            word.Key = ___key
            word.Value = ___value
            local continue
            repeat
                if CommonDefs.DictContains(wordDic, typeof(UInt32), word.Key) and CommonDefs.DictGetValue(wordDic, typeof(UInt32), word.Key) >= word.Value then
                    __itr_is_triger_return = true
                    __itr_result = true
                    return true
                else
                    continue = true
                    break
                end
                continue = true
            until 1
            if not continue then
                return
            end
        end))
        if __itr_is_triger_return == true then
            return __itr_result
        end
    end

    return false
end
