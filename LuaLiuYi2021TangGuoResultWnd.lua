
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CLoginMgr = import "L10.Game.CLoginMgr"

LuaLiuYi2021TangGuoResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaLiuYi2021TangGuoResultWnd, "RestartBtn", "RestartBtn", GameObject)
RegistChildComponent(LuaLiuYi2021TangGuoResultWnd, "ShareBtn", "ShareBtn", GameObject)
RegistChildComponent(LuaLiuYi2021TangGuoResultWnd, "CloseBtn", "CloseBtn", GameObject)
RegistChildComponent(LuaLiuYi2021TangGuoResultWnd, "ScoreLabel", "ScoreLabel", UILabel)
RegistChildComponent(LuaLiuYi2021TangGuoResultWnd, "RewardLabel", "RewardLabel", UILabel)
RegistChildComponent(LuaLiuYi2021TangGuoResultWnd, "PlayerInfoNode", "PlayerInfoNode", GameObject)
RegistChildComponent(LuaLiuYi2021TangGuoResultWnd, "HappyTexture", "HappyTexture", GameObject)
RegistChildComponent(LuaLiuYi2021TangGuoResultWnd, "SadTexture", "SadTexture", GameObject)

--@endregion RegistChildComponent end

function LuaLiuYi2021TangGuoResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.RestartBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRestartBtnClick()
	end)

	UIEventListener.Get(self.ShareBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareBtnClick()
	end)

    --@endregion EventBind end
end

function LuaLiuYi2021TangGuoResultWnd:Init()
	self.ScoreLabel.text = LuaLiuYi2021TangGuoMgr.m_Rank
	self:InitPlayerInfo()

	self.HappyTexture:SetActive(LuaLiuYi2021TangGuoMgr.m_Rank<=3)
	self.SadTexture:SetActive(LuaLiuYi2021TangGuoMgr.m_Rank>3)

	local data = LiuYi2021_Setting.GetData().TangGuoReward
	local rewardId = nil
	local minRank = 0
	CommonDefs.DictIterate(data, DelegateFactory.Action_object_object(function(key,val)
		-- 比它小里面最大的
		if key <= LuaLiuYi2021TangGuoMgr.m_Rank and key > minRank then
			minRank = key
			rewardId = val
		end
	end))
	local item = Item_Item.GetData(rewardId)
	if item then
		self.RewardLabel.text = SafeStringFormat3(LocalString.GetString("[acf8ff]获得[ff5050]%s"), item.Name)
	end

	if CommonDefs.IS_VN_CLIENT then
		self.ShareBtn.gameObject:SetActive(false)
	end
end

function LuaLiuYi2021TangGuoResultWnd:InitPlayerInfo()
	if CLoginMgr.Inst then
		local myGameServer = CLoginMgr.Inst:GetSelectedGameServer()
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Server"), typeof(UILabel)).text = myGameServer.name
	end

	if CClientMainPlayer.Inst then
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Name"), typeof(UILabel)).text = CClientMainPlayer.Inst.Name
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Icon/Lv"), typeof(UILabel)).text = tostring(CClientMainPlayer.Inst.Level)
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Icon"), typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(CClientMainPlayer.Inst.Class, CClientMainPlayer.Inst.Gender, -1), false)
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("ID"), typeof(UILabel)).text = tostring(CClientMainPlayer.Inst.Id)
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Server"), typeof(UILabel)).text = CClientMainPlayer.Inst:GetMyServerName()
	elseif LuaLiuYi2021TangGuoMgr.m_Name then
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Name"), typeof(UILabel)).text = LuaLiuYi2021TangGuoMgr.m_Name
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Icon/Lv"), typeof(UILabel)).text = tostring(LuaLiuYi2021TangGuoMgr.m_Level)
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Icon"), typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(LuaLiuYi2021TangGuoMgr.m_Class, LuaLiuYi2021TangGuoMgr.m_Gender, -1), false)
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("ID"), typeof(UILabel)).text = tostring(LuaLiuYi2021TangGuoMgr.m_Id)
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Server"), typeof(UILabel)).text = LuaLiuYi2021TangGuoMgr.m_ServerName
	end
end

--@region UIEvent

function LuaLiuYi2021TangGuoResultWnd:OnRestartBtnClick()
	CUIManager.ShowUI(CLuaUIResources.LiuYi2021TangGuoEnterWnd)
	CUIManager.CloseUI(CLuaUIResources.LiuYi2021TangGuoResultWnd)
end

function LuaLiuYi2021TangGuoResultWnd:OnShareBtnClick()
	CUICommonDef.CaptureScreenAndShare()
end

--@endregion UIEvent

