local UIProgressBar = import "UIProgressBar"
local CTrackMgr = import "L10.Game.CTrackMgr"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local AlignType1 = import "CPlayerInfoMgr+AlignType"
local CIMMgr = import "L10.Game.CIMMgr"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"

LuaZongMenInfoView = class()

RegistChildComponent(LuaZongMenInfoView,"m_RuMenBiaoZhunScrollView","RuMenBiaoZhunScrollView", UIScrollView)
RegistChildComponent(LuaZongMenInfoView,"m_RuMenBiaoZhunGrid","RuMenBiaoZhunGrid", UITable)
RegistChildComponent(LuaZongMenInfoView,"m_ZongMenRuleScrollView","ZongMenRuleScrollView", UIScrollView)
RegistChildComponent(LuaZongMenInfoView,"m_ZongMenRuleGrid","ZongMenRuleGrid", UITable)
RegistChildComponent(LuaZongMenInfoView,"m_RuleLabelTemplate","RuleLabelTemplate", GameObject)
RegistChildComponent(LuaZongMenInfoView,"m_ZongMenNameLabel","ZongMenNameLabel", UILabel)
RegistChildComponent(LuaZongMenInfoView,"m_TipButton","TipButton", GameObject)
RegistChildComponent(LuaZongMenInfoView,"m_WuXingTex","WuXingTex", CUITexture)
RegistChildComponent(LuaZongMenInfoView,"m_GuiBiWuXingTex","GuiBiWuXingTex", CUITexture)
RegistChildComponent(LuaZongMenInfoView,"m_MonsterPortrait","MonsterPortrait", CUITexture)
RegistChildComponent(LuaZongMenInfoView,"m_MonsterNameLabel","MonsterNameLabel", UILabel)
RegistChildComponent(LuaZongMenInfoView,"m_AddMonsterSprite","AddMonsterSprite", GameObject)
RegistChildComponent(LuaZongMenInfoView,"m_Player","Player", GameObject)
RegistChildComponent(LuaZongMenInfoView,"m_PlayerPortrait","PlayerPortrait", CUITexture)
RegistChildComponent(LuaZongMenInfoView,"m_PlayerNameLabel","PlayerNameLabel", UILabel)
RegistChildComponent(LuaZongMenInfoView,"m_Player2","Player2", GameObject)
RegistChildComponent(LuaZongMenInfoView,"m_Player2Portrait","Player2Portrait", CUITexture)
RegistChildComponent(LuaZongMenInfoView,"m_Player2NameLabel","Player2NameLabel", UILabel)
RegistChildComponent(LuaZongMenInfoView,"m_Progress","Progress", UIProgressBar)
RegistChildComponent(LuaZongMenInfoView,"m_ProgressLabel","ProgressLabel", UILabel)
RegistChildComponent(LuaZongMenInfoView,"m_ProgressSprite","ProgressSprite", UISprite)
RegistChildComponent(LuaZongMenInfoView,"m_InfoLabelTemplate","InfoLabelTemplate", GameObject)
RegistChildComponent(LuaZongMenInfoView,"m_InfoGrid","InfoGrid", UIGrid)
RegistChildComponent(LuaZongMenInfoView,"m_ChengWeiButton","ChengWeiButton", GameObject)
RegistChildComponent(LuaZongMenInfoView,"m_WeiJieButton","WeiJieButton", GameObject)
RegistChildComponent(LuaZongMenInfoView,"m_DongTaiButton","DongTaiButton", GameObject)
RegistChildComponent(LuaZongMenInfoView,"m_MaxLevelSprite","MaxLevelSprite", GameObject)
RegistChildComponent(LuaZongMenInfoView,"m_NoneGuiBiWuXingTexLabel","NoneGuiBiWuXingTexLabel", GameObject)
RegistChildComponent(LuaZongMenInfoView,"m_ChangeNameButton","ChangeNameButton", GameObject)
RegistChildComponent(LuaZongMenInfoView,"m_GuiBiWuXingArea","GuiBiWuXingArea", GameObject)
RegistChildComponent(LuaZongMenInfoView,"m_ChangeRuMenBiaoZhunButton","ChangeRuMenBiaoZhunButton", GameObject)
RegistChildComponent(LuaZongMenInfoView,"m_NiuZhuanWuXingTag","NiuZhuanWuXingTag", GameObject)
RegistChildComponent(LuaZongMenInfoView,"m_ChangeRuleButton","ChangeRuleButton", GameObject)

RegistClassMember(LuaZongMenInfoView, "m_InfoLabels")
RegistClassMember(LuaZongMenInfoView, "m_JoinStandard")

function LuaZongMenInfoView:Start()
    self.m_RuleLabelTemplate:SetActive(false)
    self.m_InfoLabelTemplate:SetActive(false)
    self.m_NiuZhuanWuXingTag:SetActive(LuaZongMenMgr.m_NiuZhuanWuXingResult ~= 0)
    self.m_ChangeRuleButton:SetActive(false)
    UIEventListener.Get(self.m_TipButton).onClick = DelegateFactory.VoidDelegate(function (p)
        g_MessageMgr:ShowMessage("ZongMen_Info")
    end)
    UIEventListener.Get(self.m_ChengWeiButton).onClick = DelegateFactory.VoidDelegate(function (p)
        CUIManager.ShowUI(CLuaUIResources.ZongMenChengWeiWnd)
    end)
    UIEventListener.Get(self.m_WeiJieButton).onClick = DelegateFactory.VoidDelegate(function (p)
        CUIManager.ShowUI(CLuaUIResources.ZongMenWeiJieWnd)
    end)
    UIEventListener.Get(self.m_DongTaiButton).onClick = DelegateFactory.VoidDelegate(function (p)
        CUIManager.ShowUI(CLuaUIResources.ZongMenHistoryWnd)
    end)
    UIEventListener.Get(self.m_AddMonsterSprite).onClick = DelegateFactory.VoidDelegate(function (p)
        self:FindLianYaoFaZheng()
    end)
    UIEventListener.Get(self.m_ChangeNameButton).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnChangeNameButtonClick()
    end)
    UIEventListener.Get(self.m_GuiBiWuXingArea).onClick = DelegateFactory.VoidDelegate(function (p)
        g_MessageMgr:ShowMessage("AvoidWuXingReminder")
    end)
    UIEventListener.Get(self.m_ChangeRuMenBiaoZhunButton).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnChangeRuMenBiaoZhunButtonClick()
    end)
    UIEventListener.Get(self.m_ChangeRuleButton).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnChangeRuleButtonClick()
    end)
end

function LuaZongMenInfoView:OnChangeRuleButtonClick()
    CLuaCommonAnnounceMgr.ShowWnd(LocalString.GetString("宗派宗旨修改"), LocalString.GetString("请输入宗派宗旨"), function (text)    
        local ret = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(text, false)
        if ret.msg == nil or ret.shouldBeIgnore then
            g_MessageMgr:ShowMessage("Speech_Violation")
            return
        end
        if CClientMainPlayer.Inst then
            CLuaCommonAnnounceMgr.CloseWnd()
            Gac2Gas.RequestSetSectMission_Ite(CClientMainPlayer.Inst.BasicProp.SectId,text)
        end
    end, 160, LocalString.GetString("1~80个汉字"), "")
end

function LuaZongMenInfoView:OnChangeRuMenBiaoZhunButtonClick()
    LuaZongMenMgr.m_GuiZe = self.m_JoinStandard
    LuaZongMenMgr.m_GuiZeIsRule = false
    CUIManager.ShowUI(CLuaUIResources.ChangeRuMenBiaoZhunWnd)
end

function LuaZongMenInfoView:OnChangeNameButtonClick()
    CUIManager.ShowUI(CLuaUIResources.ZongMenChangeNameWnd)
end

function LuaZongMenInfoView:FindLianYaoFaZheng()
    local lianrenData = LianHua_Setting.GetData().FaZhenFakeNpcInfo
    local lianHuaGuaiPosAndArea = LianHua_Setting.GetData().LianHuaGuaiPosAndArea
    CTrackMgr.Inst:FindNPC(lianrenData[0],tonumber(Menpai_Setting.GetData("RaidMapId").Value),lianHuaGuaiPosAndArea[0]/64, lianHuaGuaiPosAndArea[1]/64, nil, nil)
    CUIManager.CloseUI(CLuaUIResources.ZongMenMainWnd)
end

function LuaZongMenInfoView:FindLianRenFaZheng(isLeft)
    local lianrenData = LianHua_Setting.GetData().FaZhenFakeNpcInfo
    CTrackMgr.Inst:FindNPC(lianrenData[0],tonumber(Menpai_Setting.GetData("RaidMapId").Value),lianrenData[isLeft and 1 or 3]/64, lianrenData[isLeft and 2 or 4]/64, nil, nil)
    CUIManager.CloseUI(CLuaUIResources.ZongMenMainWnd)
end

function LuaZongMenInfoView:OnEnable()
    if CommonDefs.IS_VN_CLIENT then
        self.transform.localScale = Vector3.zero
    end
    g_ScriptEvent:AddListener("OnZongMenInfoResult", self, "OnZongMenInfoResult")
    g_ScriptEvent:AddListener("OnReplyBatchChangeSectOffice", self, "OnReplyBatchChangeSectOffice")
    g_ScriptEvent:AddListener("OnSyncSectChangeName", self, "OnSyncSectChangeName")
    g_ScriptEvent:AddListener("OnNiuZhuanWuXingResult", self, "OnNiuZhuanWuXingResult")
    g_ScriptEvent:AddListener("OnSyncSectMission_Ite", self, "OnSyncSectMission_Ite")
    Gac2Gas.QuerySectInfo(CClientMainPlayer.Inst.BasicProp.SectId,"overview",0)
end

function LuaZongMenInfoView:OnDisable()
    g_ScriptEvent:RemoveListener("OnZongMenInfoResult", self, "OnZongMenInfoResult")
    g_ScriptEvent:RemoveListener("OnReplyBatchChangeSectOffice", self, "OnReplyBatchChangeSectOffice")
    g_ScriptEvent:RemoveListener("OnSyncSectChangeName", self, "OnSyncSectChangeName")
    g_ScriptEvent:RemoveListener("OnNiuZhuanWuXingResult", self, "OnNiuZhuanWuXingResult")
    g_ScriptEvent:RemoveListener("OnSyncSectMission_Ite", self, "OnSyncSectMission_Ite")
end

function LuaZongMenInfoView:OnSyncSectMission_Ite(sectId, missionStr)
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.SectId == sectId then
        Extensions.RemoveAllChildren(self.m_ZongMenRuleGrid.transform)
        local go = CUICommonDef.AddChild(self.m_ZongMenRuleGrid.gameObject, self.m_RuleLabelTemplate)
        go:SetActive(true)
        go:GetComponent(typeof(UILabel)).text = missionStr
        self.m_ZongMenRuleGrid:Reposition()
        self.m_ZongMenRuleScrollView:ResetPosition()
    end
end

function LuaZongMenInfoView:OnNiuZhuanWuXingResult()
    self.m_NiuZhuanWuXingTag:SetActive(LuaZongMenMgr.m_NiuZhuanWuXingResult ~= 0)
    if LuaZongMenMgr.m_NiuZhuanWuXingResult ~= 0 then
        self.m_WuXingTex:LoadMaterial(LuaZongMenMgr:GetMingGeMatPath(LuaZongMenMgr.m_NiuZhuanWuXingResult))
    end
end

function LuaZongMenInfoView:OnSyncSectChangeName(newName)
    self.m_ZongMenNameLabel.text = newName
    LuaZongMenMgr.m_ZongMenName = newName
end

function LuaZongMenInfoView:OnReplyBatchChangeSectOffice()
    Gac2Gas.QuerySectInfo(CClientMainPlayer.Inst.BasicProp.SectId,"overview",0)
end

function LuaZongMenInfoView:OnZongMenInfoResult(data,canSetMission)
    if CommonDefs.IS_VN_CLIENT then
        self.transform.localScale = Vector3.one
    end
    
    self.m_ChangeRuleButton.gameObject:SetActive(canSetMission and canSetMission > 0)
    if not data then
        return
    end
    self.m_ZongMenNameLabel.text = data.Name.StringData
    LuaZongMenMgr.m_ZongMenName = self.m_ZongMenNameLabel.text
    self:InitRulesView(data)
    self:InitLianHuaView(data)
    self:InitInfoView(data)
    if data.HiddenFaZhenWuXing ~= 0 then
        LuaZongMenMgr:ChangeNiuZhuanWuXingResult(data.HiddenFaZhenWuXing, data.FaZhenWuXing, data.ChangeWuxingLeftTime)
    end
end

function LuaZongMenInfoView:InitRulesView(data)
    local joinStandard = data.JoinStandard
    
    self.m_JoinStandard = joinStandard
    Extensions.RemoveAllChildren(self.m_RuMenBiaoZhunGrid.transform)
    Extensions.RemoveAllChildren(self.m_ZongMenRuleGrid.transform)
    local joinStandardList = {}
    for i = 0, joinStandard.Length - 1 do     
        local id = tonumber(joinStandard[i])
        local data = Menpai_JoinStandard.GetData(id) 
        if data then
            table.insert(joinStandardList, data)
        end
    end
    if not LuaZongMenMgr.m_OpenNewSystem then
        local instructionList = {}
        local instruction = data.Instruction
        for i = 0, instruction.Length - 1 do
            local id = tonumber(instruction[i])
            local data = Menpai_Rules.GetData(id)
            if data then
                table.insert(instructionList, data)
            end
        end
        for i, data in pairs(instructionList) do
            local go = CUICommonDef.AddChild(self.m_ZongMenRuleGrid.gameObject, self.m_RuleLabelTemplate)
            go:SetActive(true)
            go:GetComponent(typeof(UILabel)).text = SafeStringFormat3("%d.%s", i, data.Description) 
        end
    else
        if CClientMainPlayer.Inst then
            Gac2Gas.QuerySectMission_Ite(CClientMainPlayer.Inst.BasicProp.SectId)
        end
    end
    for i, data in pairs(joinStandardList) do
        local go = CUICommonDef.AddChild(self.m_RuMenBiaoZhunGrid.gameObject, self.m_RuleLabelTemplate)
        go:SetActive(true)
        go:GetComponent(typeof(UILabel)).text = SafeStringFormat3("%d.%s", i, data.Description) 
    end
    self.transform:Find("LeftView/BottomView/Label"):GetComponent(typeof(UILabel)).text = LuaZongMenMgr.m_OpenNewSystem and LocalString.GetString("宗派宗旨") or LocalString.GetString("宗派规则")
    self.m_RuMenBiaoZhunGrid:Reposition()
    self.m_ZongMenRuleGrid:Reposition()
    self.m_RuMenBiaoZhunScrollView:ResetPosition()
    self.m_ZongMenRuleScrollView:ResetPosition()
end

function LuaZongMenInfoView:InitLianHuaView(data)
    self.m_WuXingTex:LoadMaterial(LuaZongMenMgr:GetMingGeMatPath(LuaZongMenMgr.m_NiuZhuanWuXingResult ~= 0 and LuaZongMenMgr.m_NiuZhuanWuXingResult or
        (data.FaZhenWuXing and data.FaZhenWuXing or 0)))
    self.m_GuiBiWuXingTex:LoadMaterial(LuaZongMenMgr:GetMingGeMatPath(data.AvoidWuxing and data.AvoidWuxing or 0))
    self.m_NoneGuiBiWuXingTexLabel.gameObject:SetActive((data.AvoidWuxing and data.AvoidWuxing)== nil)
    self.m_GuiBiWuXingTex.gameObject:SetActive((data.AvoidWuxing and data.AvoidWuxing)~= nil)
    local lianHuaMonsterId = data.LianHuaMonsterId
    local monsterData = ZhuoYao_TuJian.GetData(lianHuaMonsterId)
    self.m_MonsterPortrait:LoadNPCPortrait(monsterData and monsterData.Icon or nil)
    self.m_MonsterPortrait.gameObject:SetActive(monsterData ~= nil)
    if monsterData then
        UIEventListener.Get(self.m_MonsterPortrait.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
            self:FindLianYaoFaZheng()
        end)
    end
    self.m_MonsterNameLabel.text = monsterData and monsterData.Name or LocalString.GetString("待祭妖兽")
    self.m_AddMonsterSprite:SetActive(monsterData == nil)
    local info = data.LianHuaPeopleInfo
    local list = {}
    CommonDefs.DictIterate(info, DelegateFactory.Action_object_object(function (___key, ___value)
        table.insert(list , ___value)
    end))
    if #list >= 1 then
        local t = list[1]
        self.m_PlayerPortrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(t.Class,t.Gender, -1))
        self.m_PlayerNameLabel.text = t.Name
        UIEventListener.Get(self.m_PlayerPortrait.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
            self:FindLianRenFaZheng(true)
        end)
    end
    if #list >= 2 then
        local t = list[2]
        self.m_Player2Portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(t.Class,t.Gender, -1))
        self.m_Player2NameLabel.text = t.Name
        UIEventListener.Get(self.m_Player2Portrait.gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
            self:FindLianRenFaZheng()
        end)
    end
    self.m_Player:SetActive(#list >= 1)  
    self.m_Player2:SetActive(#list >= 2)
end

function LuaZongMenInfoView:InitInfoView(data)
    local levelupdata =  Menpai_LevelUp.GetData(data.Level)
    local curMaxExp = levelupdata and levelupdata.ShengWang or data.ShengWang
    local value = data.ShengWang / curMaxExp
    self.m_Progress.value = data.Level == 5 and 0 or value
    self.m_ProgressLabel.text = SafeStringFormat3(LocalString.GetString("声望值 %d/%d"),data.ShengWang , curMaxExp)
    self.m_MaxLevelSprite:SetActive(data.Level == 5)
    self.m_ProgressSprite.gameObject:SetActive(data.Level ~= 5)
    self.m_ProgressLabel.gameObject:SetActive(data.Level ~= 5)
    self.m_ProgressSprite.spriteName = SafeStringFormat3("guildwnd_grade_0%d", data.Level) 
    local list = {LocalString.GetString("门主"),LocalString.GetString("宗派ID"),LocalString.GetString("宗派入口"),LocalString.GetString("宗派人数"),
    LocalString.GetString("当前流派"),LocalString.GetString("秘术等级"),LocalString.GetString("邪派值"),LocalString.GetString("活跃度")}
    Extensions.RemoveAllChildren(self.m_InfoGrid.transform)
    local sectEntranceIndex = 3
    self.m_InfoLabels = {}
    for i,text in pairs(list) do
        local go = NGUITools.AddChild(self.m_InfoGrid.gameObject, self.m_InfoLabelTemplate)
        go:SetActive(true)
        go.transform:Find("InfoLabel"):GetComponent(typeof(UILabel)).text = text
        local levelspite =  go.transform:Find("ValueLabel/LevelSprite"):GetComponent(typeof(UISprite))
        levelspite.gameObject:SetActive(false)
        table.insert(self.m_InfoLabels, go.transform:Find("ValueLabel"):GetComponent(typeof(UILabel)))
    end
    self.m_InfoGrid:Reposition()
    self.m_RuMenBiaoZhunGrid:Reposition()
    UIEventListener.Get(self.m_InfoLabels[1].gameObject).onClick = DelegateFactory.VoidDelegate(function (p)
        local pos = Vector3(540, 0)
        pos = self.m_InfoLabels[1].transform:TransformPoint(pos)
        self:ShowMenZhuPlayerPopupMenu(data.LeaderId, pos)
    end)
    self.m_InfoLabels[1].text = data.LeaderName
    self.m_InfoLabels[2].text = data.Id
    self.m_InfoLabels[sectEntranceIndex].text = CUICommonDef.TranslateToNGUIText(SafeStringFormat3("[04fa21][%s,%d,%d]",PublicMap_PublicMap.GetData(data.EntranceMapId).Name,data.EntrancePosX,data.EntrancePosY))
    self.m_InfoLabels[4].text = SafeStringFormat3("%d/30", data.DiziCount)
    self.m_InfoLabels[5].text = data.School == 1 and LocalString.GetString("邪派") or LocalString.GetString("正派")
    self.m_InfoLabels[6].text = SafeStringFormat3(LocalString.GetString("%d级"),data.MiShuLevel)
    self.m_InfoLabels[7].text = data.Evil
    self.m_InfoLabels[8].text = data.Activity
end

function LuaZongMenInfoView:OnEditSectEntranceButtonClicked()
    CUIManager.ShowUI(CLuaUIResources.ChangeZongMenEntranceWnd)
end

function LuaZongMenInfoView:ShowMenZhuPlayerPopupMenu(leaderId, pos)
    CPlayerInfoMgr.ShowPlayerPopupMenu(leaderId, CIMMgr.Inst:IsMyFriend(leaderId) and EnumPlayerInfoContext.FriendInFriendWnd or EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, pos, AlignType1.Default)
end