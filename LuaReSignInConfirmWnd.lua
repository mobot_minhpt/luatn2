local Item_Item = import "L10.Game.Item_Item"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"

LuaReSignInConfirmWnd = class()
LuaReSignInConfirmWnd.s_Desc = ""
LuaReSignInConfirmWnd.s_Items = {}

function LuaReSignInConfirmWnd:Awake()
    self.transform:Find("Content/DescLabel"):GetComponent(typeof(UILabel)).text = LuaReSignInConfirmWnd.s_Desc
    local rewardGrid = self.transform:Find("Content/Grid"):GetComponent(typeof(UIGrid))
    local rewardTemplate = self.transform:Find("Content/ItemTemplate").gameObject
    rewardTemplate:SetActive(false)
    Extensions.RemoveAllChildren(rewardGrid.transform)
    for _, item in ipairs(LuaReSignInConfirmWnd.s_Items) do
        local go = NGUITools.AddChild(rewardGrid.gameObject, rewardTemplate)
        go:SetActive(true)
        self:InitRewardItem(go.transform, item.id, item.cnt)
    end
    rewardGrid:Reposition()
    UIEventListener.Get(self.transform:Find("Content/OkBtn").gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        Gac2Gas.RequestBuQianDao_New()
        CUIManager.CloseUI(CLuaUIResources.ReSignInConfirmWnd)
    end)
    UIEventListener.Get(self.transform:Find("Content/CancelBtn").gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.CloseUI(CLuaUIResources.ReSignInConfirmWnd)
    end)
end

function LuaReSignInConfirmWnd:Init()

end

function LuaReSignInConfirmWnd:InitRewardItem(trans, itemId, count)
    local iconTexture = trans:Find("IconTexture"):GetComponent(typeof(CUITexture))
    iconTexture:Clear()
    local qualitySprite = trans:Find("QualitySprite"):GetComponent(typeof(UISprite))
    qualitySprite.spriteName = nil
    local amountLabel = trans:Find("AmountLabel"):GetComponent(typeof(UILabel))
    amountLabel.text = nil

    local itemTemplate = Item_Item.GetData(itemId)
    if not itemTemplate then return end

    iconTexture:LoadMaterial(itemTemplate.Icon)
    if count > 1 then
        amountLabel.text = tostring(count)
    end
    qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(itemTemplate, nil, false)

    CommonDefs.AddOnClickListener(trans.gameObject, DelegateFactory.Action_GameObject(function (go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
    end), false)
end
