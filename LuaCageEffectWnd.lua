require("common/common_include")

local CBaseWnd = import "L10.UI.CBaseWnd"
local UIPanel = import "UIPanel"
local CommonDefs = import "L10.Game.CommonDefs"
local UITexture = import "UITexture"
local Screen = import "UnityEngine.Screen"
local DelegateFactory = import "DelegateFactory"
local Time = import "UnityEngine.Time"

LuaCageEffectWnd = class()
RegistClassMember(LuaCageEffectWnd,"m_CloseButton")
RegistClassMember(LuaCageEffectWnd,"m_Background")
RegistClassMember(LuaCageEffectWnd,"m_LastWidth")
RegistClassMember(LuaCageEffectWnd,"m_LastHeight")
RegistClassMember(LuaCageEffectWnd,"m_LastUpdateTime")

function LuaCageEffectWnd:Awake()
    self.gameObject:GetComponent(typeof(UIPanel)).IgnoreIphoneXMargin = true
end

function LuaCageEffectWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaCageEffectWnd:AdjustBackground()
    if self.m_LastWidth and self.m_LastWidth == Screen.width and self.m_LastHeight and self.m_LastHeight == Screen.height then
        return
    end
    self.m_LastWidth = Screen.width
    self.m_LastHeight = Screen.height
    local screenRatio = Screen.width / Screen.height
    if screenRatio > 2 then
        self.m_Background.aspectRatio = screenRatio
        self.m_Background:ResetAnchors()
    else
        self.m_Background.aspectRatio = 2
        self.m_Background:ResetAnchors()
    end
end

function LuaCageEffectWnd:Init()
	self.m_CloseButton = self.transform:Find("CloseButton").gameObject
	self.m_Background = self.transform:Find("Background"):GetComponent(typeof(UITexture))
    CommonDefs.AddOnClickListener(self.m_CloseButton, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)
    self.m_LastUpdateTime = Time.realtimeSinceStartup

	self:AdjustBackground()
end

function LuaCageEffectWnd:OnEnable()
	if not self.m_Background then return end
	self:AdjustBackground()
end

function LuaCageEffectWnd:Update()
    if Time.realtimeSinceStartup - self.m_LastUpdateTime > 0.1 then
        self.m_LastUpdateTime = Time.realtimeSinceStartup
        self:AdjustBackground()
    end
end
