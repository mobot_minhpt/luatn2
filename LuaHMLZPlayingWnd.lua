local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CUIFx = import "L10.UI.CUIFx"

LuaHMLZPlayingWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaHMLZPlayingWnd, "ExpandButton", "ExpandButton", GameObject)
RegistChildComponent(LuaHMLZPlayingWnd, "Grid", "Grid", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaHMLZPlayingWnd, "m_Count")
RegistClassMember(LuaHMLZPlayingWnd, "m_Stage_1")

function LuaHMLZPlayingWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaHMLZPlayingWnd:Init()
    UIEventListener.Get(self.ExpandButton).onClick = DelegateFactory.VoidDelegate(function(p)
        self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
        self:SetUIActive(false)
        CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
    end)
    self.m_bg = self.transform:Find("Anchor/bg").gameObject
    self.m_Count = 0
    self:OnSyncProcessBar(LuaHMLZMgr.m_State, LuaHMLZMgr.m_Count)
end

function LuaHMLZPlayingWnd:OnEnable()
    g_ScriptEvent:AddListener("ZYJ2021HMLZ_SyncPlayState", self, "OnSyncProcessBar")
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
end

function LuaHMLZPlayingWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ZYJ2021HMLZ_SyncPlayState", self, "OnSyncProcessBar")
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
end

function LuaHMLZPlayingWnd:OnHideTopAndRightTipWnd( )
    self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 180)
    self:SetUIActive(true)
end

function LuaHMLZPlayingWnd:OnSyncProcessBar(playState, count)
    local Stage_1 = 1001
    local Idle = 1000
    -- print("rrrrrr")
    -- print(playState, count)
    self:SetUIActive(playState == Stage_1 or playState == Idle)
    self:SetUIActive(playState == Stage_1 or playState == Idle)
    if playState == Stage_1 then
        while count > self.m_Count do
            local go = self.Grid.transform:GetChild(self.m_Count)
            go:Find("Shine"):GetComponent(typeof(CUIFx)):LoadFx("fx/ui/prefab/UI_linghundianliang.prefab")
            go:Find("Yellow").gameObject:SetActive(true)
            self.m_Count = self.m_Count + 1
        end
    end

end

function LuaHMLZPlayingWnd:SetUIActive(state)
    self.Grid:SetActive(state)
    self.m_bg:SetActive(state)
end

--@region UIEvent

--@endregion UIEvent

