-- Auto Generated!!
local CPlayerShopData = import "L10.UI.CPlayerShopData"
local CPlayerShopMgr = import "L10.UI.CPlayerShopMgr"
local CPlayerShopWnd = import "L10.UI.CPlayerShopWnd"
CPlayerShopWnd.m_OnSelectRadioBox_CS2LuaHook = function (this, button, index)
    if index == 0 then
        this.m_PlayerShopBuy.gameObject:SetActive(true)
        this.m_PlayerShopOperate.gameObject:SetActive(false)
        this.m_PlayerShopPublicity.gameObject:SetActive(false)
        this.m_PlayerShopBuy:Init()
    elseif index == 1 then
        this.m_PlayerShopBuy.gameObject:SetActive(false)
        this.m_PlayerShopOperate.gameObject:SetActive(true)
        this.m_PlayerShopPublicity.gameObject:SetActive(false)
        this.m_PlayerShopOperate:Init()
    elseif index == 2 then
        this.m_PlayerShopBuy.gameObject:SetActive(false)
        this.m_PlayerShopOperate.gameObject:SetActive(false)
        this.m_PlayerShopPublicity.gameObject:SetActive(true)
        this.m_PlayerShopPublicity:Init()
    end
end
CPlayerShopWnd.m_OnOpenPlayerShopWnd_CS2LuaHook = function (this, playerShopId, bankruptTime)
    CPlayerShopData.Main.PlayerShopId = math.floor(tonumber(playerShopId or 0))
    if CPlayerShopData.Main.PlayerShopId ~= 0 then
        CPlayerShopData.Main.BankruptTime = math.floor(tonumber(bankruptTime or 0))
    else
        CPlayerShopData.s_OpenMoney = math.floor(tonumber(bankruptTime or 0)) + CPlayerShopMgr.Open_Shop_Money
    end
    --m_RadioBox.SetTitle(1, CPlayerShopData.Main.HasPlayerShop ? "店铺管理" : "开店");
    this.m_RadioBox:ChangeTo(this.m_RadioBox.CurrentSelectIndex, true)
end
