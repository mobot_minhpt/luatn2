local UITabBar = import "L10.UI.UITabBar"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnExpandListBox = import "L10.UI.QnExpandListBox"
local UITable = import "UITable"
local GameObject = import "UnityEngine.GameObject"
local Extensions = import "Extensions"
local CUICommonDef = import "L10.UI.CUICommonDef"
local Profession = import "L10.Game.Profession"

LuaWuLiangPassedTeamInfoWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaWuLiangPassedTeamInfoWnd, "DifficultySelectBox", "DifficultySelectBox", QnExpandListBox)
RegistChildComponent(LuaWuLiangPassedTeamInfoWnd, "LevelSelectBox", "LevelSelectBox", QnExpandListBox)
RegistChildComponent(LuaWuLiangPassedTeamInfoWnd, "PlayerInfo", "PlayerInfo", GameObject)
RegistChildComponent(LuaWuLiangPassedTeamInfoWnd, "CompanionInfo", "CompanionInfo", GameObject)
RegistChildComponent(LuaWuLiangPassedTeamInfoWnd, "PlayerTableBar", "PlayerTableBar", UITabBar)
RegistChildComponent(LuaWuLiangPassedTeamInfoWnd, "Detail", "Detail", GameObject)
RegistChildComponent(LuaWuLiangPassedTeamInfoWnd, "HideScrollBg", "HideScrollBg", GameObject)

--@endregion RegistChildComponent end

function LuaWuLiangPassedTeamInfoWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.HideScrollBg.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnHideScrollBgClick()
	end)


    --@endregion EventBind end
end

function LuaWuLiangPassedTeamInfoWnd:Init()
    -- 设置层数的下拉菜单
    local levelNames = CreateFromClass(MakeGenericClass(List, String))
    for i = 6, 25 do
        CommonDefs.ListAdd(levelNames, typeof(String), SafeStringFormat3(LocalString.GetString("第%s层"), Extensions.ConvertToChineseString(i)))
    end
    -- 设置下拉菜单
    self.LevelSelectBox:SetItems(levelNames)

    -- 难度下拉
    local names = CreateFromClass(MakeGenericClass(List, String))
    CommonDefs.ListAdd(names, typeof(String), LocalString.GetString("苦境"))
    CommonDefs.ListAdd(names, typeof(String), LocalString.GetString("集境"))
    CommonDefs.ListAdd(names, typeof(String), LocalString.GetString("灭境"))
    self.DifficultySelectBox:SetItems(names)

    self.LevelSelectBox.ClickCallback = DelegateFactory.Action_int(function(index)
        self:InitNull()
        self:SendRpc()
    end)

    self.DifficultySelectBox.ClickCallback = DelegateFactory.Action_int(function(index)
        self:InitNull()
        self:SendRpc()
    end)

    -- 设置初始化的下拉
    self:InitNull()
    self:InitSelectBox()
end

function LuaWuLiangPassedTeamInfoWnd:InitSelectBox()
    local levelId = LuaWuLiangMgr.passedTeamInfoLevelId

    local level = math.floor(levelId/100)
    local difficult = levelId - level*100

    -- 设置初始值
    -- 需要减去6 因为前五层没有
    if level-6 < 0 then
        self.LevelSelectBox:SetIndex(0, false)
    else
        self.LevelSelectBox:SetIndex(level-6, false)
    end
    
    if difficult-1<0 then
        self.DifficultySelectBox:SetIndex(0, false)
    else
        self.DifficultySelectBox:SetIndex(difficult-1, false)
    end

    -- 发送请求
    self:SendRpc()
end

function LuaWuLiangPassedTeamInfoWnd:InitNull()
    for i = 1, 5 do
        local item = self.PlayerTableBar.transform:Find(tostring(i))
        item.gameObject:SetActive(false)
    end
    self.Detail:SetActive(false)
end

function LuaWuLiangPassedTeamInfoWnd:SendRpc()
    
    -- 用level和difficult查playid
    local level = self.LevelSelectBox:GetCurrentIndex() + 6
    local difficult = self.DifficultySelectBox:GetCurrentIndex() + 1

    self.m_CurrentPlayId = XinBaiLianDong_WuLiangPlay.GetData(level*100 + difficult).GameplayId
    Gac2Gas.WuLiangShenJing_QueryRecentPass(self.m_CurrentPlayId)
end

-- [2] = {
--     ['PlayerInfo'] = {
--             ['Class'] = 2,
--             ['Heal'] = 0,
--             ['Name'] = '我的名字七个字',
--             ['DPS'] = 9621.7125,
--             ['Id'] = 725400001,
--             ['Gender'] = 0,
--             ['UnderDamage'] = 0,
--             ['Level'] = 151
--     },
--     ['HuoBanInfo'] = {
--             [18006048] = {
--                     ['Attr'] = {
--                             ['AdjmAttMax'] = 17152.673323862,

--                     ['SkillLevelTbl'] = {

--                     },
--                     ['Hp'] = 362301.62222749,
--                     ['InheritLevel'] = 0,
--                     ['EngineId'] = 5004,
--                     ['DPS'] = 41548.9297,
--                     ['UnderDamage'] = 0,
--                     ['Level'] = 151,
--                     ['Tid'] = 18006048,
--                     ['Heal'] = 0
--             }
--     },
--     ['UpdateTime'] = 1655110773
-- }

function LuaWuLiangPassedTeamInfoWnd:OnData(playId, data)
    if self.m_CurrentPlayId == playId then
        self.m_Data = data
        self:ShowPlayer()
    end
end

-- 每次刷新时调用
function LuaWuLiangPassedTeamInfoWnd:ShowPlayer()
    -- 初始化playerInfo
    for i = 1, 5 do
        local item = self.PlayerTableBar.transform:Find(tostring(i))
        local nameLabel = item:Find("PlayerNameLabel"):GetComponent(typeof(UILabel))
        local levelLabel = item:Find("Icon/LevelLabel"):GetComponent(typeof(UILabel))
        local powerLabel = item:Find("Power/PowerLabel"):GetComponent(typeof(UILabel))
        local icon = item:Find("Icon"):GetComponent(typeof(CUITexture))

        local data = self.m_Data[i]

        if data and data["PlayerInfo"] then
            local playerInfo = data["PlayerInfo"]
            item.gameObject:SetActive(true)

            icon:LoadNPCPortrait(CUICommonDef.GetPortraitName(playerInfo.Class, playerInfo.Gender, -1), false)
            levelLabel.color = Color(1,1,1,1)
            levelLabel.text = playerInfo.Level
            powerLabel.text = playerInfo.Capacity
            nameLabel.text = playerInfo.Name
        else
            item.gameObject:SetActive(false)
        end
    end

    self.PlayerTableBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index) 
        self:ShowDetail(index+1)
    end)
    -- 默认显示第一个

    local data = self.m_Data[1]
    if data and data["PlayerInfo"] then
        self.PlayerTableBar:ChangeTab(0, false)
    end
    
end

-- 显示具体信息
function LuaWuLiangPassedTeamInfoWnd:ShowDetail(index)
    self.Detail:SetActive(true)

    local data = self.m_Data[index]
    local playerInfo = data["PlayerInfo"]
    local huoBanInfo = data["HuoBanInfo"]

    -- 玩家信息
    local icon = self.PlayerInfo.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local nameLabel = self.PlayerInfo.transform:Find("PlayerNameLabel"):GetComponent(typeof(UILabel))
    local levelLabel = self.PlayerInfo.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
    local xiuWeiLabel = self.PlayerInfo.transform:Find("XiuWeiLabel"):GetComponent(typeof(UILabel))
    local xiuLianLabel = self.PlayerInfo.transform:Find("XiuLianLabel"):GetComponent(typeof(UILabel))
    local zhanLiLabel = self.PlayerInfo.transform:Find("ZhanLiLabel"):GetComponent(typeof(UILabel))

    nameLabel.text = tostring(playerInfo.Name)
    levelLabel.text = tostring(playerInfo.Level)
    xiuWeiLabel.text = tostring(playerInfo.XiuWei)
    xiuLianLabel.text = tostring(playerInfo.XiuLian)
    zhanLiLabel.text = tostring(playerInfo.Capacity)

    icon:LoadNPCPortrait(CUICommonDef.GetPortraitName(playerInfo.Class, playerInfo.Gender, -1), false)

    -- 伙伴信息
    local huobanIndex = 1
    for id, huobanInfo in pairs(huoBanInfo) do
        -- 塞一个id，后面显示信息用
        huobanInfo.MonsterId = id

        local item = self.CompanionInfo.transform:Find(tostring(huobanIndex))
        huobanIndex = huobanIndex + 1
        item.gameObject:SetActive(true)

        local classSprite = item:Find("ZhiYeSprite"):GetComponent(typeof(UISprite))
        local icon = item:Find("Icon"):GetComponent(typeof(CUITexture))
        local nameLabel = item:Find("NameLabel"):GetComponent(typeof(UILabel))
        local skillLabel = item:Find("SkillLabel"):GetComponent(typeof(UILabel))
        local attributeLabel = item:Find("AttributeLabel"):GetComponent(typeof(UILabel))

        local monsterData = Monster_Monster.GetData(id)
        local huoBanData = XinBaiLianDong_WuLiangHuoBan.GetData(id)

        icon:LoadNPCPortrait(monsterData.HeadIcon, false)

        classSprite.spriteName = Profession.GetIconByNumber(huoBanData.Class)
        nameLabel.text = huoBanData.Name

        local huoBanData = XinBaiLianDong_WuLiangHuoBan.GetData(id)
        local levelData = XinBaiLianDong_WuLiangInherit.GetData(huobanInfo.InheritLevel or 0)

        attributeLabel.text = tostring(math.floor(levelData.PropertyCoefficient*100+huoBanData.PropertyCoefficient*100 + 0.01).."%")

        -- 技能培养等级
        local skillLevelSum = 0
        for k,v in pairs(huobanInfo.SkillLevelTbl) do
            skillLevelSum = v + skillLevelSum
        end
        skillLabel.text = tostring(skillLevelSum)

        UIEventListener.Get(icon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            -- 显示伙伴信息
            LuaWuLiangMgr:OpenHuoBanTipWnd(huobanInfo, true)
        end)
    end

    -- 隐藏不用节点
    for i = huobanIndex, 2 do
        self.CompanionInfo.transform:Find(tostring(huobanIndex)).gameObject:SetActive(false)
    end
end

function LuaWuLiangPassedTeamInfoWnd:OnEnable()
	g_ScriptEvent:AddListener("WuLiangShenJing_QueryRecentPassResult", self, "OnData")
end

function LuaWuLiangPassedTeamInfoWnd:OnDisable()
	g_ScriptEvent:RemoveListener("WuLiangShenJing_QueryRecentPassResult", self, "OnData")
end


--@region UIEvent

function LuaWuLiangPassedTeamInfoWnd:OnHideScrollBgClick()
    self.DifficultySelectBox:HideScrollView()
    self.LevelSelectBox:HideScrollView()
end


--@endregion UIEvent

