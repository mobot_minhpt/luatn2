local EnumItemPlace = import "L10.Game.EnumItemPlace"
local AlignType = import "L10.UI.CTooltip+AlignType"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
local LingShou_Setting=import "L10.Game.LingShou_Setting"
local CItemMgr=import "L10.Game.CItemMgr"
local CUITexture=import "L10.UI.CUITexture"
local CItemCountUpdate=import "L10.UI.CItemCountUpdate"
local EPlayerFightProp=import "L10.Game.EPlayerFightProp"
local CItemAccessListMgr=import "L10.UI.CItemAccessListMgr"
local LingShou_LingShou=import "L10.Game.LingShou_LingShou"
local CLingShouModelTextureLoader=import "L10.UI.CLingShouModelTextureLoader"
local CLingShouMgr = import "L10.Game.CLingShouMgr"
local QnTabView=import "L10.UI.QnTabView"


CLuaLingShouXiuWuDetailView = class()
RegistClassMember(CLuaLingShouXiuWuDetailView,"textureLoader")
RegistClassMember(CLuaLingShouXiuWuDetailView,"tableNode")
RegistClassMember(CLuaLingShouXiuWuDetailView,"m_WuxingLabel")
RegistClassMember(CLuaLingShouXiuWuDetailView,"m_XiuweiLabel")
RegistClassMember(CLuaLingShouXiuWuDetailView,"m_QinmiLabel")
RegistClassMember(CLuaLingShouXiuWuDetailView,"m_WndTabView")
RegistClassMember(CLuaLingShouXiuWuDetailView,"m_LastZhandouli")
RegistClassMember(CLuaLingShouXiuWuDetailView,"m_LastZhandouli2Id")
RegistClassMember(CLuaLingShouXiuWuDetailView,"m_FxNode")

function CLuaLingShouXiuWuDetailView:Awake()
    self.m_WndTabView=self.transform.parent:GetComponent(typeof(QnTabView))
    self.textureLoader = self.transform:Find("Normal/Prop1/Texture"):GetComponent(typeof(CLingShouModelTextureLoader))
    self.tableNode = self.transform.parent:Find("LingShouTable")

    self.m_WuxingLabel=FindChild(self.transform,"WuxingLabel"):GetComponent(typeof(UILabel))
    self.m_XiuweiLabel=FindChild(self.transform,"XiuweiLabel"):GetComponent(typeof(UILabel))
    self.m_QinmiLabel=FindChild(self.transform,"QinmiLabel"):GetComponent(typeof(UILabel))
    self.m_FxNode = self.transform:Find("Normal/Prop1/ZhandouliLabel/FxNode"):GetComponent(typeof(CUIFx))
    self:InitTiWu()
    self:InitTiXiu()
    self:InitTiQin()
end

function CLuaLingShouXiuWuDetailView:OnEnable( )
    self.tableNode.gameObject:SetActive(true)
    self.m_FxNode:DestroyFx()
    self.m_LastZhandouli = nil

    g_ScriptEvent:AddListener("SelectLingShouAtRow", self, "SelectLingShouAtRow")
    g_ScriptEvent:AddListener("GetLingShouDetails", self, "GetLingShouDetails")
    g_ScriptEvent:AddListener("SyncLingShouPartnerInfo", self, "UpdateQinmi")
    self.transform:Find("Normal").gameObject:SetActive(false)
    local selectedLingShou = CLingShouMgr.Inst.selectedLingShou
    if selectedLingShou and selectedLingShou~="" then
        CLingShouMgr.Inst:RequestLingShouDetails(selectedLingShou)
    end
end

function CLuaLingShouXiuWuDetailView:OnDisable()
    g_ScriptEvent:RemoveListener("SelectLingShouAtRow", self, "SelectLingShouAtRow")
    g_ScriptEvent:RemoveListener("GetLingShouDetails", self, "GetLingShouDetails")
    g_ScriptEvent:RemoveListener("SyncLingShouPartnerInfo", self, "UpdateQinmi")
end

function CLuaLingShouXiuWuDetailView:SelectLingShouAtRow( row)
    --CLingShouMgr.Inst.selectedSkillId = 0;
    --请求详细信息
    --如果管理器有他的详情的话，就不用再请求了
    local selectedLingShou = CLingShouMgr.Inst.selectedLingShou
    if selectedLingShou and selectedLingShou~="" then
        CLingShouMgr.Inst:RequestLingShouDetails(selectedLingShou)
    else
        self:InitNull()
    end
end

function CLuaLingShouXiuWuDetailView:UpdateQinmi(lingShouId,partnerInfo)
    local details = CLingShouMgr.Inst:GetLingShouDetails(lingShouId)
    if details then
        local bindPartenerLingShouId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.BindedPartenerLingShouId or nil
        if details.data then--and bindPartenerLingShouId and details.data.Id == bindPartenerLingShouId then
            self.m_QinmiLabel.text = tostring(details.data.Props.PartnerInfo.Affinity / 10)
        else
        self.m_QinmiLabel.text = '0'
        end

        local zhandouliLabel = self.transform:Find("Normal/Prop1/ZhandouliLabel"):GetComponent(typeof(UILabel))
        local qichangId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.JieBanQiChangId or 0
        local curZhandouli = CLuaLingShouMgr.GetLingShouPartnerZhandouli(details.data,details.fightProperty,qichangId)
        zhandouliLabel.text = curZhandouli
        self.m_FxNode:DestroyFx()
        if self.m_LastZhandouli then
            if tonumber(curZhandouli) > tonumber(self.m_LastZhandouli) and details.id == self.m_LastZhandouli2Id then
                self.m_FxNode:LoadFx("Fx/UI/Prefab/UI_dagongshouce_kekaiqicishu.prefab")
            end
        end
        self.m_LastZhandouli = curZhandouli
        self.m_LastZhandouli2Id = details.id
    end
end

function CLuaLingShouXiuWuDetailView:GetLingShouDetails( args)
    if self.m_WndTabView.CurrentSelectTab ~= 3 then return end
    local lingShouId, details=args[0],args[1]
    if lingShouId == CLingShouMgr.Inst.selectedLingShou then
        self.transform:Find("Normal").gameObject:SetActive(false)

        local tiqintf = self.transform:Find("Normal/TiQin")
        local tixiuBtn = tiqintf:Find("TixiuBtn").gameObject
        local bindPartenerLingShouId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.BindedPartenerLingShouId or nil

        --有没有孩子
        local ownBabyId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.OwnBabyId or nil
        local haveOwnBaby = ownBabyId~=nil and ownBabyId~=""

        local haveBaby = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.Babys.Count>0 or false
        -- if lingShouId == bindPartenerLingShouId then
        if haveBaby and haveOwnBaby then
            CUICommonDef.SetActive(tixiuBtn,true,true)
        else
            CUICommonDef.SetActive(tixiuBtn,false,true)
        end
        -- else
            -- CUICommonDef.SetActive(tixiuBtn,false,true)
        -- end

        if details ~= nil then
            self:InitView(details)

            self.textureLoader:Init(details)

            self.m_XiuweiLabel.text= tostring(math.floor(details.data.Props.Xiuwei))
            self.m_WuxingLabel.text= details.strWuxing

            local bindPartenerLingShouId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.BindedPartenerLingShouId or nil
            if details.data then --and bindPartenerLingShouId and details.data.Id == bindPartenerLingShouId then
              self.m_QinmiLabel.text = tostring(details.data.Props.PartnerInfo.Affinity / 10)
            else
              self.m_QinmiLabel.text = '0'
            end
            local  marryInfo = details.data.Props.MarryInfo
            local isDongFnag = marryInfo.IsInDongfang>0
            local isLeave = (marryInfo.LeaveStartTime + marryInfo.LeaveDuration) > CServerTimeMgr.Inst.timeStamp

            if isDongFnag or isLeave then
                self:SetDongFangOrLeaveState(true, isDongFnag, isLeave)
            else
                self:SetDongFangOrLeaveState(false, false, false)
            end
        else
            self:InitNull()
        end
    end
end
function CLuaLingShouXiuWuDetailView:InitNull( )
    -- self.pinzhi:InitNull()
    self.textureLoader:InitNull()
    -- self.detailsDisplay:Fill(nil)
    self:InitView(nil)

    self:SetDongFangOrLeaveState(false, false, false)
end
function CLuaLingShouXiuWuDetailView:SetDongFangOrLeaveState( active, isDongFang, isLeave)
    self.transform:Find("Normal").gameObject:SetActive(not active)
    self.transform.parent:Find("DongFangOrLeave").gameObject:SetActive(active)

    if active then
        local tf2 = self.transform.parent:Find("DongFangOrLeave")
        if isDongFang then
            tf2:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("灵兽生宝宝中，无法进行修悟相关的操作")
        elseif isLeave then
            tf2:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("灵兽离家出走中，无法进行修悟相关的操作")
        end
    end
end

-- return CLuaLingShouXiuWuDetailView

function CLuaLingShouXiuWuDetailView:InitView(details)
    local notNull = details and true or false

    local transform = self.transform:Find("Normal/Prop1")

    local zhandouliLabel = transform:Find("ZhandouliLabel"):GetComponent(typeof(UILabel))
    local nameLabel = transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local levelLabel = transform:Find("LevelLabel"):GetComponent(typeof(UILabel))

    local pinzhi = transform:Find("PinZhi"):GetComponent(typeof(UISprite))
    pinzhi.spriteName = notNull and CLuaLingShouMgr.GetLingShouQualitySprite(CLuaLingShouMgr.GetLingShouQuality(details.data.Props.Quality)) or nil


    local corZiZhiLabel = transform:Find("Cor/CorZiZhiLabel"):GetComponent(typeof(UILabel))
    local staZiZhiLabel = transform:Find("Sta/StaZiZhiLabel"):GetComponent(typeof(UILabel))
    local strZiZhiLabel = transform:Find("Str/StrZiZhiLabel"):GetComponent(typeof(UILabel))
    local intZiZhiLabel = transform:Find("Int/IntZiZhiLabel"):GetComponent(typeof(UILabel))
    local agiZiZhiLabel = transform:Find("Agi/AgiZiZhiLabel"):GetComponent(typeof(UILabel))

    --星级
    local starLevel = notNull and details.data.Props.ZizhiLevel or 0
    local zizhiStar = transform:Find("ZizhiStar")
    local sprites={}
    for i=1,5 do
        local sprite = zizhiStar:GetChild(i-1):GetComponent(typeof(UISprite))
        table.insert( sprites,sprite )
        sprite.spriteName = "common_star_70%"
    end
    local c1 = math.floor(starLevel/2)
    local c2 = starLevel%2
    for i=1,c1 do
        local sprite = zizhiStar:GetChild(i-1)
        sprites[i].spriteName = "common_star_3_1"
    end
    if c2==1 then
        sprites[c1+1].spriteName = "common_star_3_2"
    end

    local shenShouSprite = transform:Find("ShenShou"):GetComponent(typeof(UISprite))


    local bindPartenerLingShouId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.BindedPartenerLingShouId or nil
    local qichangId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.JieBanQiChangId or 0
    local isJieBan = bindPartenerLingShouId == details.id
    zhandouliLabel.text = notNull and tostring(isJieBan and CLuaLingShouMgr.GetLingShouPartnerZhandouli(details.data,details.fightProperty,qichangId) or CLuaLingShouMgr.GetLingShouZhandouli(details.data)) or ""
    zhandouliLabel.transform:Find("Label"):GetComponent(typeof(UILabel)).text =isJieBan and LocalString.GetString("结伴战斗力") or LocalString.GetString("出战战斗力")

    local curZhandouli = zhandouliLabel.text
    if curZhandouli == "" then
        curZhandouli = 0
    end

    self.m_FxNode:DestroyFx()
    if self.m_LastZhandouli then
        if tonumber(curZhandouli) > tonumber(self.m_LastZhandouli) and details.id == self.m_LastZhandouli2Id then
            -- self.m_FxNode:DestroyFx()
            self.m_FxNode:LoadFx("Fx/UI/Prefab/UI_dagongshouce_kekaiqicishu.prefab")
        end
    end
    self.m_LastZhandouli = curZhandouli
    self.m_LastZhandouli2Id = details.id

    nameLabel.text = notNull and details.data.Name or nil
    levelLabel.text = notNull and "Lv." .. tostring(details.data.Level) or nil
    shenShouSprite.enabled = false
    local data = LingShou_LingShou.GetData(details.data.TemplateId)
    if data ~= nil then
        if data.ShenShou > 0 then
            shenShouSprite.enabled = true
        end
    end

    local improve = ""
    if notNull then
        local add = NumberTruncate((details.fightProperty:GetParam(EPlayerFightProp.WuxingImprove) * 100), 2)
        if add > 0 then improve = System.String.Format("[ffed5f](+{0:f1}%)[-]", add) end
    end

    corZiZhiLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.CorZizhi)))) .. improve or ""

    staZiZhiLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.StaZizhi)))) .. improve or ""

    strZiZhiLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.StrZizhi)))) .. improve or ""

    intZiZhiLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.IntZizhi)))) .. improve or ""

    agiZiZhiLabel.text = notNull and tostring((math.floor(details.fightProperty:GetParam(EPlayerFightProp.AgiZizhi)))) .. improve or ""
end


function CLuaLingShouXiuWuDetailView:InitTiWu()
    local transform = self.transform:Find("Normal/TiWu")


    local countUpdate = transform:GetComponent(typeof(CItemCountUpdate))
    local nameLabel = transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local wuxingLabel = transform:Find("WuxingLabel"):GetComponent(typeof(UILabel))
    local btnLabel = transform:Find("TiwuBtn/BtnLabel"):GetComponent(typeof(UILabel))
    local icon = transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local tiwuBtn = transform:Find("TiwuBtn").gameObject

    countUpdate.templateId = CLingShouMgr.Inst.tiwuTempalteId
    countUpdate.format = "{0}/1"
    countUpdate.onChange = DelegateFactory.Action_int(function (val)
        if val < 1 then
            countUpdate.format = "[ff0000]{0}[-]/1"
            btnLabel.text = LocalString.GetString("获得途径")
        else
            countUpdate.format = "{0}/1"
            btnLabel.text = LocalString.GetString("提悟性")
        end
    end)
    countUpdate:UpdateCount()

    local data = CItemMgr.Inst:GetItemTemplate(countUpdate.templateId)
    nameLabel.text = data.Name
    icon:LoadMaterial(data.Icon)

    UIEventListener.Get(tiwuBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:TiWu(go) end)
end

function CLuaLingShouXiuWuDetailView:TiWu( go)
    local tiwuTempalteId = LingShou_Setting.GetData().WuxingItemId
    local btnLabel = go.transform:GetChild(0):GetComponent(typeof(UILabel))

    if btnLabel.text == LocalString.GetString("获得途径") then
        CItemAccessListMgr.Inst:ShowItemAccessInfo(tiwuTempalteId, false, go.transform, AlignType.Right)
    elseif btnLabel.text == LocalString.GetString("提悟性") then
        --需要选择背包里的一个物品
        local default, pos, itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, tiwuTempalteId)
        local find = default
        --if (!autoBuy)
        --{
        if not find then
            g_MessageMgr:ShowMessage("LINGSHOU_NOT_HAVE_WUXING_ITEM")
            return
        end
        Gac2Gas.RequestImproveLingShouWuxing(CLingShouMgr.Inst.selectedLingShou, EnumItemPlace_lua.Bag, pos, itemId or "", false)
    end
end


function CLuaLingShouXiuWuDetailView:InitTiXiu()
    local transform = self.transform:Find("Normal/TiXiu")
    local countUpdate = transform:GetComponent(typeof(CItemCountUpdate))
    local nameLabel = transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local xiuweiLabel = transform:Find("XiuweiLabel"):GetComponent(typeof(UILabel))
    local btnLabel = transform:Find("TixiuBtn/BtnLabel"):GetComponent(typeof(UILabel))
    local icon = transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local tixiuBtn = transform:Find("TixiuBtn").gameObject

    local tixiuTemaplteId = LingShou_Setting.GetData().XiuWeiItemIds[0]

    countUpdate.templateId = tixiuTemaplteId
    countUpdate.format = "{0}/1"
    countUpdate.onChange = DelegateFactory.Action_int(function (val)
        if val < 1 then
            countUpdate.format = "[ff0000]{0}[-]/1"
            btnLabel.text = LocalString.GetString("获得途径")
        else
            countUpdate.format = "{0}/1"
            btnLabel.text = LocalString.GetString("提修为")
        end
    end)
    countUpdate:UpdateCount()

    local data = CItemMgr.Inst:GetItemTemplate(countUpdate.templateId)
    nameLabel.text = data.Name
    icon:LoadMaterial(data.Icon)

    UIEventListener.Get(tixiuBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:TiXiu(go) end)
end

function CLuaLingShouXiuWuDetailView:TiXiu( go)
    local tixiuTemaplteId = LingShou_Setting.GetData().XiuWeiItemIds[0]

    local btnLabel = go.transform:GetChild(0):GetComponent(typeof(UILabel))
    if btnLabel.text == LocalString.GetString("获得途径") then
        CItemAccessListMgr.Inst:ShowItemAccessInfo(tixiuTemaplteId, false, go.transform, AlignType.Right)
    elseif btnLabel.text == LocalString.GetString("提修为") then
        local default, pos, itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, tixiuTemaplteId)
        if default then
            if not System.String.IsNullOrEmpty(CLingShouMgr.Inst.selectedLingShou) then
                Gac2Gas.RequestFeedLingShou(CLingShouMgr.Inst.selectedLingShou, EnumItemPlace_lua.Bag, pos, itemId)
            end
        end
    end
end

function CLuaLingShouXiuWuDetailView:InitTiQin()
  if CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.SealId < 3 then
    local transform = self.transform:Find("Normal/TiQin")
    local transformLock = self.transform:Find("Normal/TiQinLock")
    transform.gameObject:SetActive(false)
    transformLock.gameObject:SetActive(true)
  else
    local transform = self.transform:Find("Normal/TiQin")
    local transformLock = self.transform:Find("Normal/TiQinLock")
    transform.gameObject:SetActive(true)
    transformLock.gameObject:SetActive(false)
    local countUpdate = transform:GetComponent(typeof(CItemCountUpdate))
    local nameLabel = transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local btnLabel = transform:Find("TixiuBtn/BtnLabel"):GetComponent(typeof(UILabel))
    local icon = transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local tixiuBtn = transform:Find("TixiuBtn").gameObject

    local tiqinTemaplteId = LingShouPartner_Setting.GetData().AffinityImproveItemId

    countUpdate.templateId = tiqinTemaplteId
    countUpdate.format = "{0}/1"
    countUpdate.onChange = DelegateFactory.Action_int(function (val)
        if val < 1 then
            countUpdate.format = "[ff0000]{0}[-]/1"
            btnLabel.text = LocalString.GetString("获得途径")
        else
            countUpdate.format = "{0}/1"
            btnLabel.text = LocalString.GetString("提亲密度")
        end
    end)
    countUpdate:UpdateCount()

    local data = CItemMgr.Inst:GetItemTemplate(countUpdate.templateId)
    nameLabel.text = data.Name
    icon:LoadMaterial(data.Icon)

    UIEventListener.Get(tixiuBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:TiQin(go) end)
  end
end

function CLuaLingShouXiuWuDetailView:TiQin( go)
    local tiqinTemaplteId = LingShouPartner_Setting.GetData().AffinityImproveItemId

    local btnLabel = go.transform:GetChild(0):GetComponent(typeof(UILabel))
    if btnLabel.text == LocalString.GetString("获得途径") then
        CItemAccessListMgr.Inst:ShowItemAccessInfo(tiqinTemaplteId, false, go.transform, AlignType.Right)
    elseif btnLabel.text == LocalString.GetString("提亲密度") then
        local lingshouId = CLingShouMgr.Inst.selectedLingShou

        local bindPartenerLingShouId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.BindedPartenerLingShouId or nil
        if bindPartenerLingShouId~=lingshouId then
            g_MessageMgr:ShowMessage("JieBan_UpdateQinMiFail_NotJieBan")
            return
        end

        local default, pos, itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, tiqinTemaplteId)
        if default then
            if not System.String.IsNullOrEmpty(CLingShouMgr.Inst.selectedLingShou) then
                Gac2Gas.RequestImporveLingShouBabyPartnerAffinity(CLingShouMgr.Inst.selectedLingShou, pos, itemId)
            end
        end
    end
end
