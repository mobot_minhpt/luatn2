local DelegateFactory = import "DelegateFactory"
local CUIFxPaths      = import "L10.UI.CUIFxPaths"
local Vector4         = import "UnityEngine.Vector4"
local CUICommonDef    = import "L10.UI.CUICommonDef"
local TweenRotation   = import "TweenRotation"

LuaGuanYaoJianWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaGuanYaoJianWnd, "found")
RegistClassMember(LuaGuanYaoJianWnd, "template")
RegistClassMember(LuaGuanYaoJianWnd, "grid")
RegistClassMember(LuaGuanYaoJianWnd, "awardButton")
RegistClassMember(LuaGuanYaoJianWnd, "awardButtonTween")
RegistClassMember(LuaGuanYaoJianWnd, "awardMark")
RegistClassMember(LuaGuanYaoJianWnd, "awardFx")

function LuaGuanYaoJianWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    self:InitUIComponents()
    self:InitActive()

    UIEventListener.Get(self.awardButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnAwardButtonClick()
    end)
end

-- 初始化UI组件
function LuaGuanYaoJianWnd:InitUIComponents()
    local anchor = self.transform:Find("Anchor")

    self.found = anchor:Find("Found"):GetComponent(typeof(UILabel))
    self.template = anchor:Find("Template").gameObject
    self.grid = anchor:Find("ScrollView/Grid"):GetComponent(typeof(UIGrid))
    self.awardButton = anchor:Find("Award/Button")
    self.awardButtonTween = self.awardButton:GetComponent(typeof(TweenRotation))
    self.awardMark = self.awardButton:Find("Mark").gameObject
    self.awardFx = anchor:Find("Award/Fx"):GetComponent(typeof(CUIFx))
end

function LuaGuanYaoJianWnd:InitActive()
    self.template:SetActive(false)
end

function LuaGuanYaoJianWnd:OnEnable()
    g_ScriptEvent:AddListener("GuanYaoJianUnlock", self, "OnGuanYaoJianUnlock")
    g_ScriptEvent:AddListener("GuanYaoJianRewardSuccess", self, "OnGuanYaoJianRewardSuccess")
end

function LuaGuanYaoJianWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GuanYaoJianUnlock", self, "OnGuanYaoJianUnlock")
    g_ScriptEvent:RemoveListener("GuanYaoJianRewardSuccess", self, "OnGuanYaoJianRewardSuccess")
end

function LuaGuanYaoJianWnd:OnGuanYaoJianUnlock(id)
    local child = self.grid.transform:GetChild(id - 1001)
    if not child then return end

    local dianLiangFx = child:Find("Portrait/DianLiangFx"):GetComponent(typeof(CUIFx))
    dianLiangFx:LoadFx(g_UIFxPaths.GuanYaoJianDianLiangFxPath)
    local liuGuangFx = child:Find("Portrait/LiuGuangFx"):GetComponent(typeof(CUIFx))
    liuGuangFx:DestroyFx()

    self:SetPortrait(child, id)
    self:SetName(child, id)
    self:SetDiscovery(child, id)
    self:UpdateAward()
end

function LuaGuanYaoJianWnd:OnGuanYaoJianRewardSuccess()
    self:UpdateAward()
    self.awardFx:LoadFx(g_UIFxPaths.XinShengHuaJiUnlockFxPath)
end


function LuaGuanYaoJianWnd:Init()
    self:InitGrid()
    self:InitFound()
    self:UpdateAward()
end

-- 初始化已发现数量
function LuaGuanYaoJianWnd:InitFound()
    local totalCount = XinBaiLianDong_GuanYaoJian.GetDataCount()

    local count = 0
    for i = 1, totalCount do
        if self:IsTaskFinished(i + 1000) then
            count = count + 1
        end
    end

    self.found.text = SafeStringFormat3("%d/%d", count, totalCount)
end

function LuaGuanYaoJianWnd:InitGrid()
    local count = XinBaiLianDong_GuanYaoJian.GetDataCount()

    Extensions.RemoveAllChildren(self.grid.transform)
    local firstUnlockableItem = nil
    local scrollView = self.grid.transform.parent:GetComponent(typeof(UIScrollView))
    for i = 1, count do
        local id = 1000 + i
        local data = XinBaiLianDong_GuanYaoJian.GetData(id)

        local child = CUICommonDef.AddChild(self.grid.gameObject, self.template)
        child:SetActive(true)

        local portrait = child.transform:Find("Portrait"):GetComponent(typeof(CUITexture))
        portrait:LoadNPCPortrait(data.Portrait)
        UIEventListener.Get(portrait.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnPortraitClick(id)
        end)

        if not LuaXinBaiLianDongMgr.GYJUnlock[id] and self:IsTaskFinished(id) then
            local cuiFx = portrait.transform:Find("LiuGuangFx"):GetComponent(typeof(CUIFx))
            cuiFx:LoadFx(CUIFxPaths.CommonYellowLineFxPath)
            cuiFx.ScrollView = scrollView

            local gap = 5
            CUIFx.DoAni(Vector4(- portrait.texture.width * 0.5 + gap, portrait.texture.height * 0.5 - gap,
                portrait.texture.width - gap * 2, portrait.texture.height - gap * 2), true, cuiFx)

            if not firstUnlockableItem then
                firstUnlockableItem = child
            end
        end

        self:SetPortrait(child.transform, id)
        self:SetName(child.transform, id)
        self:SetDiscovery(child.transform, id)

        local thread = child.transform:Find("Thread"):GetComponent(typeof(UILabel))
        thread.text = data.ThreadDescription
    end
    self.grid:Reposition()

    CUICommonDef.SetFullyVisible(firstUnlockableItem, scrollView)
end

-- 设置头像显示
function LuaGuanYaoJianWnd:SetPortrait(child, id)
    local portrait = child:Find("Portrait"):GetComponent(typeof(CUITexture))
    if LuaXinBaiLianDongMgr.GYJUnlock[id] then
        portrait.color = NGUIText.ParseColor24("FFFFFF", 0)
        portrait.alpha = 1
    else
        portrait.color = NGUIText.ParseColor24("000000", 0)
        portrait.alpha = 0.5
    end
end

-- 设置姓名
function LuaGuanYaoJianWnd:SetName(child, id)
    local name = child:Find("Name"):GetComponent(typeof(UILabel))

    if LuaXinBaiLianDongMgr.GYJUnlock[id] then
        name.text = XinBaiLianDong_GuanYaoJian.GetData(id).ThreadName
    else
        name.text = LocalString.GetString("尚未解锁")
    end
end

-- 设置发现
function LuaGuanYaoJianWnd:SetDiscovery(child, id)
    local discovery = child:Find("Discovery"):GetComponent(typeof(UILabel))

    if LuaXinBaiLianDongMgr.GYJUnlock[id] then
        discovery.text = XinBaiLianDong_GuanYaoJian.GetData(id).Discovery
    else
        discovery.text = LocalString.GetString("尚未解锁")
    end
end

-- 判断任务是否完成
function LuaGuanYaoJianWnd:IsTaskFinished(id)
    return CClientMainPlayer.Inst.TaskProp:IsMainPlayerTaskFinished(XinBaiLianDong_GuanYaoJian.GetData(id).ThreadTaskId)
end

-- 更新领取奖励图标
function LuaGuanYaoJianWnd:UpdateAward()
    local isReward = LuaXinBaiLianDongMgr.GYJIsReward and true or false
    local isAllUnlock = LuaXinBaiLianDongMgr:IsGYJAllUnlock()
    local needClick = (isAllUnlock and not isReward) and true or false

    self.awardMark:SetActive(isReward)
    LuaUtils.SetLocalRotationZ(self.awardButton, 0)
    self.awardButtonTween.enabled = needClick

    self.awardFx:DestroyFx()
    if needClick then
        self.awardFx:LoadFx(g_UIFxPaths.XinBaiProgressFxPath)
    end
end

--@region UIEvent

function LuaGuanYaoJianWnd:OnPortraitClick(id)
    if not LuaXinBaiLianDongMgr.GYJUnlock[id] and self:IsTaskFinished(id) then
        Gac2Gas.GuanYaoJian_Unlock(id)
    end
end

function LuaGuanYaoJianWnd:OnAwardButtonClick()
    Gac2Gas.GuanYaoJian_Reward()
end

--@endregion UIEvent

