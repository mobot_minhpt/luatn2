local CSkillMgr = import "L10.Game.CSkillMgr"
local CClientFurnitureMgr=import "L10.Game.CClientFurnitureMgr"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
local NGUIMath = import "NGUIMath"
local CClientObjectMgr=import "L10.Game.CClientObjectMgr"
local CMainCamera=import "L10.Engine.CMainCamera"
CLuaHouseWoodPileStatusWnd = class()

RegistClassMember(CLuaHouseWoodPileStatusWnd, "m_DamageLabel")
RegistClassMember(CLuaHouseWoodPileStatusWnd, "m_TimeLabel")
RegistClassMember(CLuaHouseWoodPileStatusWnd, "m_HurtLabel")
RegistClassMember(CLuaHouseWoodPileStatusWnd, "m_SkillItem")
RegistClassMember(CLuaHouseWoodPileStatusWnd, "m_Node")
RegistClassMember(CLuaHouseWoodPileStatusWnd, "m_DurationTick")
RegistClassMember(CLuaHouseWoodPileStatusWnd, "m_Extension")
RegistClassMember(CLuaHouseWoodPileStatusWnd, "m_Background")
RegistClassMember(CLuaHouseWoodPileStatusWnd, "m_StartTime")
RegistClassMember(CLuaHouseWoodPileStatusWnd, "m_PileId")
RegistClassMember(CLuaHouseWoodPileStatusWnd, "m_YOffset")

function CLuaHouseWoodPileStatusWnd:Awake()
    self.m_YOffset = 0
    self.m_Node = self.transform:Find("Anchor")
    self.m_Extension=self.transform:Find("Anchor/Extension").gameObject
    self.m_Background = self.transform:Find("Anchor/Background"):GetComponent(typeof(UISprite))

    self.m_DamageLabel = self.transform:Find("Anchor/DamageLabel"):GetComponent(typeof(UILabel))
    self.m_HurtLabel = self.transform:Find("Anchor/HurtLabel"):GetComponent(typeof(UILabel))

    local label1 = self.transform:Find("Anchor/Label1"):GetComponent(typeof(UILabel))
    local label3 = self.transform:Find("Anchor/Label3"):GetComponent(typeof(UILabel))
    local label4 = self.transform:Find("Anchor/Label4"):GetComponent(typeof(UILabel))
    if CLuaHouseWoodPileMgr.m_IsHeal then
        label1.text = LocalString.GetString("总治疗")
        label3.text = LocalString.GetString("秒治")
        label4.text = LocalString.GetString("治疗技能（前三）")
        self.m_DamageLabel.color = Color.green
        self.m_HurtLabel.color = Color.green
    else
        label1.text = LocalString.GetString("总伤害")
        label3.text = LocalString.GetString("秒伤")
        label4.text = LocalString.GetString("输出技能（前三）")
        self.m_DamageLabel.color = Color.red
        self.m_HurtLabel.color = Color.red
    end


    local closeButton = self.transform:Find("Anchor/CloseButton").gameObject
    UIEventListener.Get(closeButton).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.CloseUI(CLuaUIResources.HouseWoodPileStatusWnd)
        Gac2Gas.RequestStopFightWithPile(0)
        --清空
        CLuaHouseWoodPileMgr.m_Damages = nil
    end)

    local refreshButton =self.transform:Find("Anchor/RefreshButton").gameObject
    UIEventListener.Get(refreshButton).onClick = DelegateFactory.VoidDelegate(function(go)
        CLuaHouseWoodPileMgr.m_Damages = {}
        Gac2Gas.RequestEmptyPileFightStatics(0)
        self:ClearInfo()
    end)

    self.m_SkillItem = self.transform:Find("Anchor/SkillItem").gameObject
    self.m_SkillItem:SetActive(false)

    self.m_TimeLabel = self.transform:Find("Anchor/TimeLabel"):GetComponent(typeof(UILabel))

    self:ClearInfo()

    self:Update()
end

function CLuaHouseWoodPileStatusWnd:ClearInfo()
    self:InitInfo()
end

function CLuaHouseWoodPileStatusWnd:Init()
    self:RegisterAutoCloseTick()
end

function CLuaHouseWoodPileStatusWnd:FormatTime(time)
    return SafeStringFormat3(LocalString.GetString("%ds"),time)
end

function CLuaHouseWoodPileStatusWnd:InitInfo()
    local damageInfo = {
        startTime = CServerTimeMgr.Inst.timeStamp,
		petDamage = 0,
		lingshouDamage = 0,
		totalDamage = 0,
        otherPetDamage = 0,
        skillDamageInfo = {},
        specialPetDamageInfo = {},
    }
    if CLuaHouseWoodPileMgr.m_Damages then
        for k,v in pairs(CLuaHouseWoodPileMgr.m_Damages) do
            damageInfo.startTime = math.min(damageInfo.startTime,v.startTime)
            damageInfo.petDamage = damageInfo.petDamage+math.abs(v.petDamage)
            damageInfo.lingshouDamage = damageInfo.lingshouDamage+math.abs(v.lingshouDamage)
            damageInfo.totalDamage = damageInfo.totalDamage+math.abs(v.totalDamage)
            damageInfo.otherPetDamage = damageInfo.otherPetDamage+math.abs(v.otherPetDamage)
            for skillCls,skillDamage in pairs(v.skillDamageInfo) do
                if damageInfo.skillDamageInfo[skillCls] then
                    damageInfo.skillDamageInfo[skillCls] = damageInfo.skillDamageInfo[skillCls]+math.abs(skillDamage)
                else
                    damageInfo.skillDamageInfo[skillCls] = math.abs(skillDamage)
                end
            end
            for petCls,petDamage in pairs(v.specialPetDamageInfo) do
                if damageInfo.specialPetDamageInfo[petCls] then
                    damageInfo.specialPetDamageInfo[petCls] = damageInfo.specialPetDamageInfo[petCls]+math.abs(petDamage)
                else
                    damageInfo.specialPetDamageInfo[petCls] = math.abs(petDamage)
                end
            end
        end
    end
    -- local max3
    local tmp = {}
    for k,v in pairs(damageInfo.skillDamageInfo) do
        table.insert( tmp,{k,v} )
    end
    table.sort( tmp,function(a,b)
        if a[2]>b[2] then
            return true
        elseif a[2]<b[2] then
            return false
        else
            return a[1]>b[1]
        end
    end )
    damageInfo.skillDamageInfo = tmp

    self.m_DamageLabel.text = tostring(math.floor(damageInfo.totalDamage))

    local function updateDuration()
        local second = math.floor(CServerTimeMgr.Inst.timeStamp - damageInfo.startTime)
        self.m_TimeLabel.text = tostring(self:FormatTime(second))
        local hurtStr = "0"
        if second~=0 then
            hurtStr = tostring(math.floor(damageInfo.totalDamage/second))
        end
        self.m_HurtLabel.text = hurtStr
    end

    if self.m_DurationTick then
        UnRegisterTick(self.m_DurationTick)
    end
    self.m_DurationTick = RegisterTick(function()
        updateDuration()
    end,1000)
    updateDuration()

    local grid = self.transform:Find("Anchor/Grid").gameObject
    Extensions.RemoveAllChildren(grid.transform)

    local playerLevel = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Level or 1

    local max = math.min(3,#damageInfo.skillDamageInfo)
    for i=1,max do
        local skillCls = damageInfo.skillDamageInfo[i][1]
        local damage = damageInfo.skillDamageInfo[i][2]
        local go = NGUITools.AddChild(grid,self.m_SkillItem)
        go:SetActive(true)
        local icon = go.transform:Find("Mask/Icon"):GetComponent(typeof(CUITexture))
        icon:LoadSkillIcon(Skill_AllSkills.GetData(skillCls*100+1).SkillIcon)

        local colorIcon = go.transform:Find("Panel/ColorSystemIcon"):GetComponent(typeof(UISprite))
        colorIcon.gameObject:SetActive(false)
        if Skill_ColorSkill.Exists(skillCls) then--是否是色系技能
            colorIcon.gameObject:SetActive(true)
            colorIcon.spriteName = CSkillMgr.Inst:GetColorSystemSkillIcon(skillCls)
        end

        local label = go.transform:Find("Label"):GetComponent(typeof(UILabel))
        local hurtStr = "0"
        if damageInfo.totalDamage~=0 then
            hurtStr = tostring(math.floor(damage*100/damageInfo.totalDamage)).."%"
        end
        label.text = hurtStr
    end
    grid:GetComponent(typeof(UIGrid)):Reposition()

    if not CLuaHouseWoodPileMgr.m_IsHeal then
        self.m_Extension:SetActive(true)
        local item = self.m_Extension.transform:Find("Item").gameObject
        item:SetActive(false)
        local grid2 = self.m_Extension.transform:Find("Grid").gameObject
        Extensions.RemoveAllChildren(grid2.transform)

        local function addChild(name,hurt)
            local go = NGUITools.AddChild(grid2,item)
            go:SetActive(true)
            local label1 = go:GetComponent(typeof(UILabel))
            label1.text = name
            local label2 = go.transform:Find("Label"):GetComponent(typeof(UILabel))
            local hurtStr = "0%"
            if damageInfo.totalDamage~=0 then
                hurtStr =  tostring(math.floor(hurt*100/damageInfo.totalDamage)).."%"
            end
            label2.text = hurtStr
        end

        local tmp = {}

        for k,v in pairs(damageInfo.specialPetDamageInfo) do
            local id = k
            local name = Pet_Pet.GetData(id*100+1).Name
            --这个先留着
            name = string.gsub(name,"(%d+)"..LocalString.GetString("级"),"")
            table.insert( tmp,{name,v,#tmp} )
        end
        -- if damageInfo.otherPetDamage>0 then
        if damageInfo.lingshouDamage>0 then
            table.insert( tmp,{LocalString.GetString("灵兽"),damageInfo.lingshouDamage,#tmp} )
        end
        if damageInfo.otherPetDamage>0 then
            table.insert( tmp,{LocalString.GetString("其他召唤物"),damageInfo.otherPetDamage,#tmp} )
        end
        table.sort( tmp,function(a,b)
            if a[2]>b[2] then
                return true
            elseif a[2]<b[2] then
                return false
            else
                return a[3]>b[3]
            end
        end )
        for i,v in ipairs(tmp) do
            addChild(v[1],v[2])
        end
        
            -- end
        grid2:GetComponent(typeof(UIGrid)):Reposition()
        local b = NGUIMath.CalculateRelativeWidgetBounds(self.m_Extension.transform)
        local height = math.floor(math.abs(b.extents.y))*2 + 311+20
        self.m_Background.height = height

        self.m_YOffset = math.floor((height-311)/2)
    else
        self.m_Extension:SetActive(false)
        self.m_YOffset = 0
    end
end

function CLuaHouseWoodPileStatusWnd:OnEnable()
	g_ScriptEvent:AddListener("SyncPileFightProgressInfo", self, "OnSyncPileFightProgressInfo")

end

function CLuaHouseWoodPileStatusWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SyncPileFightProgressInfo", self, "OnSyncPileFightProgressInfo")

end


function CLuaHouseWoodPileStatusWnd:OnSyncPileFightProgressInfo(pileId,damageInfo)
    self:RegisterAutoCloseTick()

    self:InitInfo()
end
function CLuaHouseWoodPileStatusWnd:RegisterAutoCloseTick()
end

function CLuaHouseWoodPileStatusWnd:OnDestroy()
    if self.m_DurationTick then
        UnRegisterTick(self.m_DurationTick)
        self.m_DurationTick = nil
    end
end

function CLuaHouseWoodPileStatusWnd:Update()
    if self.m_Node then
        local startTime = 0
        local lastPileId = 0
        if CLuaHouseWoodPileMgr.m_Damages then
            for pileId,v in pairs(CLuaHouseWoodPileMgr.m_Damages) do
                if v.startTime>startTime then
                    lastPileId=pileId
                end
            end
        end
        local engineId = 0
        if lastPileId>0 then
            local fur = CommonDefs.DictGetValue_LuaCall(CClientFurnitureMgr.Inst.WoodPileFurnitureList,lastPileId)
            engineId = fur and fur.EngineId or 0
        end
        if engineId>0 then
            local obj = CClientObjectMgr.Inst:GetObject(engineId)
            if obj then
                local cam = CMainCamera.Main
                local viewPos = cam:WorldToViewportPoint(obj.RO.TopAnchorPos)
                if viewPos.x>0.5 then
                    self.m_Node.localPosition = Vector3(-350,self.m_YOffset,0)
                elseif viewPos.x<0.5 then
                    self.m_Node.localPosition = Vector3(350,self.m_YOffset,0)
                end
            else--可能死了
                CUIManager.CloseUI(CLuaUIResources.HouseWoodPileStatusWnd)
            end
        end
    end
end