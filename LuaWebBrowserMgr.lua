local CWebBrowserMgr = import "L10.Game.CWebBrowserMgr"
local CPersonalSpaceMgr=import "L10.Game.CPersonalSpaceMgr"
local WWW=import "UnityEngine.WWW"
local CJingLingMgr = import "L10.Game.CJingLingMgr"
local HTTPHelper = import "L10.Game.HTTPHelper"
local ShareMgr = import "ShareMgr"
local CWebLinkShareMgr = import "L10.Game.CWebLinkShareMgr"
local CClientHouseMgr=import "L10.Game.CClientHouseMgr"

-- 内置浏览器的白名单维护
local sharelinkPrefixTable = {
    "https://game.ds.163.com/l10",
    "https://game.dev.ds.163.com/l10",
    "https://ds.163.com",
    "https://m.ds.163.com",
    "https://act.ds.163.com/",
    "https://h5.ds.163.com/v1/",
    "https://app.16163.com/ds/ulinks/",
    "https://qnm.cbg.163.com/",
    "https://qnm.dev3.cbg.163.com/",
    "https://h5.cc.163.com",
    "https://cc.163.com",
    "https://ux-kube.leihuo.netease.com",
    "https://research-game.163.com",
    "https://unisdk.update.netease.com",
    "https://protocol.unisdk.netease.com",
}
for _, v in pairs(sharelinkPrefixTable) do
    CommonDefs.ListAdd_LuaCall(CWebLinkShareMgr.s_PrefixUrlList, v)
end

g_WebBrowserOnReceivedMessage={}

g_WebBrowserOnReceivedMessage["openurl"] = function(isUniWebView, webview, message)
    if message.args and CommonDefs.DictContains_LuaCall(message.args, "url") then
        local url = CommonDefs.DictGetValue_LuaCall(message.args, "url")
        CWebBrowserMgr.Inst:OpenExtenalUrl(url)
        if CommonDefs.DictContains_LuaCall(message.args, "autoclose") then
            local autoclose = tonumber(CommonDefs.DictGetValue_LuaCall(message.args, "autoclose"))
            if autoclose~=nil and autoclose==1 then
                if isUniWebView then
                    if webview then
                        webview:WebViewDone("")
                    end
                else
                    CWebBrowserMgr.Inst:CloseNgWebView()
                end
            end
        end
    end
end

CWebBrowserMgr.m_OnReceivedMessage = function(isUniWebView, webview, message)
    if message.path == "extend/gmcmd" then
        if message.args then
            if CommonDefs.DictContains(message.args, typeof(String), "act") then
                local actName = CommonDefs.DictGetValue(message.args, typeof(String), "act")
                if actName == "jinglingwnd" then
                    local searchString = ""
                    if CommonDefs.DictContains(message.args, typeof(String), "searchstring") then
                        searchString = CommonDefs.DictGetValue(message.args, typeof(String), "searchstring")
                    end
                    CJingLingMgr.Inst:ShowJingLingWnd(searchString, "o_gamepage", true, false, nil)
                end
                if isUniWebView then
                    if webview then
                        --CWebBrowserMgr.Inst:CloseWebView(webview)
                        webview:WebViewDone("")
                    end
                else
                    CWebBrowserMgr.Inst:CloseNgWebView()
                end
            end
        end
    elseif message.path == "shareimg" then
        if
            message.args and CommonDefs.DictContains(message.args, typeof(String), "url") and
                CommonDefs.DictContains(message.args, typeof(String), "channel")
         then
            local url = CommonDefs.DictGetValue(message.args, typeof(String), "url")
            local channel = CommonDefs.DictGetValue(message.args, typeof(String), "channel")
            HTTPHelper.DownloadImageWithCallback(
                url,
                DelegateFactory.Action_bool_string(
                    function(success, filePath)
                        if success then
                            ShareMgr.ShareImage(channel, filePath)
                        end
                    end
                )
            )
        end
    elseif message.path == "shareurlingame" then
        if
            message.args and CommonDefs.DictContains(message.args, typeof(String), "url") and
                CommonDefs.DictContains(message.args, typeof(String), "title")
         then
            local url = CommonDefs.DictGetValue(message.args, typeof(String), "url")
            local title = CommonDefs.DictGetValue(message.args, typeof(String), "title")
            CWebLinkShareMgr.Inst:ShareUrl(url, title)
            if isUniWebView then
                if webview then
                    webview:WebViewDone("")
                end
            else
                CWebBrowserMgr.Inst:CloseNgWebView()
            end
        end
    else
        if g_WebBrowserOnReceivedMessage[message.path] then
            g_WebBrowserOnReceivedMessage[message.path](isUniWebView, webview, message)
        end
    end
end

CLuaWebBrowserMgr = {}

CLuaWebBrowserMgr.m_CBGBaseURLs = {
    "https://qnm.cbg.163.com",
    "https://qnm.dev3.cbg.163.com",
    "https://activity.cbg.163.com",
}

--家园活动
function CLuaWebBrowserMgr.OpenHouseActivityUrl(activityName)
    if not CPersonalSpaceMgr.Inst.Token then
        return
    end
    local targetPlayerId = nil
    if string.find(activityName,"JiaYuan2022Index@") then
        targetPlayerId = string.match(activityName,"JiaYuan2022Index@(%w+)")
        activityName = "JiaYuan2022Index"
    end
    if CClientMainPlayer.Inst ~= nil then
        local houserole = 0
        if CClientHouseMgr.Inst.mBasicProp ~= nil then
            if CClientHouseMgr.Inst.mBasicProp.OwnerId == CClientMainPlayer.Inst.Id then
                houserole = 1
            elseif
                CClientHouseMgr.Inst.mBasicProp.OwnerId2 ~= 0 and
                    CClientMainPlayer.Inst.Id == CClientHouseMgr.Inst.mBasicProp.OwnerId2
             then
                houserole = 2
            elseif CClientHouseMgr.Inst:IsCurHouseRoomer() then
                houserole = 3
            end
        end
        local url = CWebBrowserMgr.Inst:GetApiUrlForInternalBrowser() .. "qnm/activity/login" .. "?" .. CWebBrowserMgr.s_SkeyPara .. "="
            .. CPersonalSpaceMgr.Inst.Token .. "&" .. CWebBrowserMgr.s_RoleIdPara .. "=" .. CClientMainPlayer.Inst.Id
            .. "&" .. CWebBrowserMgr.s_ActivityPara .. "=" .. activityName .. "&address=" .. WWW.EscapeURL(CClientHouseMgr.Inst:GetHouseName())
            .. "&houserole=" .. houserole
        
        if targetPlayerId then
            url = url.."&targetroleid=" .. tostring(targetPlayerId)
        end
        CWebBrowserMgr.Inst:OpenUrl(url)
    end
end

function CLuaWebBrowserMgr:CheckCBGAutoLogin(url)
    if url == nil or url == "" then return false end
    for __, baseUrl in pairs(self.m_CBGBaseURLs) do
        if StringStartWith(url,baseUrl) then
            return true
        end
    end
    return false
end
