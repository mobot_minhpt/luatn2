local QnModelPreviewer = import "L10.UI.QnModelPreviewer"
local UIGrid = import "UIGrid"
local CButton = import "L10.UI.CButton"
local Constants = import "L10.Game.Constants"
local MessageMgr = import "L10.Game.MessageMgr"
local EquipIntensify_Suit = import "L10.Game.EquipIntensify_Suit"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CFeiShengMgr = import "L10.Game.CFeiShengMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaTreasureHouseMyRoleView=class()

RegistClassMember(LuaTreasureHouseMyRoleView, "HintLabel")
RegistClassMember(LuaTreasureHouseMyRoleView, "MoreInfo")

RegistClassMember(LuaTreasureHouseMyRoleView, "PlayerNameLabel")
RegistClassMember(LuaTreasureHouseMyRoleView, "PlayerGradeLabel")
RegistClassMember(LuaTreasureHouseMyRoleView, "ModelPreviewer")

-------  玩家详情  -------
RegistClassMember(LuaTreasureHouseMyRoleView, "InfoGrid")
RegistClassMember(LuaTreasureHouseMyRoleView, "InfoTemplate")

-------  玩家财富信息  -------
RegistClassMember(LuaTreasureHouseMyRoleView, "YinPiaoLabel")
RegistClassMember(LuaTreasureHouseMyRoleView, "YinLiangLabel")
RegistClassMember(LuaTreasureHouseMyRoleView, "YuanBaoLabel")
RegistClassMember(LuaTreasureHouseMyRoleView, "LingYuLabel")

-------  按钮信息  -------
RegistClassMember(LuaTreasureHouseMyRoleView, "RegisterBtn") -- 未登记状态
RegistClassMember(LuaTreasureHouseMyRoleView, "CheckLabel")	-- 审核状态

RegistClassMember(LuaTreasureHouseMyRoleView, "Opetations")	-- 可操作状态
RegistClassMember(LuaTreasureHouseMyRoleView, "ShowBtn")	-- 待售区展示(可操作状态)
RegistClassMember(LuaTreasureHouseMyRoleView, "SellBtn_Operation")	--藏宝阁出售(可操作状态)

RegistClassMember(LuaTreasureHouseMyRoleView, "ToShowRoot")	-- 待售区展示状态
RegistClassMember(LuaTreasureHouseMyRoleView, "ToShowLabel")	-- 待售区展示中时间Label
RegistClassMember(LuaTreasureHouseMyRoleView, "UnShelfBtn")		-- 下架(待售区展示中状态)
RegistClassMember(LuaTreasureHouseMyRoleView, "SellBtn_ToShow")	-- 藏宝阁出售(待售区展示中状态)

RegistClassMember(LuaTreasureHouseMyRoleView, "CheckInterval") -- 审核间隔（7天）

function LuaTreasureHouseMyRoleView:Init()
	self.CheckInterval = 7
	self:InitClassMembers()
	self:UpdateShowInfo()
end

function LuaTreasureHouseMyRoleView:OnEnable()
	g_ScriptEvent:AddListener("MainPlayerUpdateMoney", self, "UpdateWealthInfo")
	g_ScriptEvent:AddListener("MainPlayerPlayPropUpdate", self, "UpdateButtons")
end

function LuaTreasureHouseMyRoleView:OnDisable()
	g_ScriptEvent:RemoveListener("MainPlayerUpdateMoney", self, "UpdateWealthInfo")
	g_ScriptEvent:RemoveListener("MainPlayerPlayPropUpdate", self, "UpdateButtons")
end

function LuaTreasureHouseMyRoleView:InitClassMembers()
	self.HintLabel = self.transform:Find("Header/HintLabel"):GetComponent(typeof(UILabel))
	self.MoreInfo = self.transform:Find("Header/MoreInfo").gameObject

	self.PlayerNameLabel = self.transform:Find("Middle/Left/PlayerInfo/PlayerNameLabel"):GetComponent(typeof(UILabel))
	self.PlayerGradeLabel = self.transform:Find("Middle/Left/PlayerInfo/PlayerGradeLabel"):GetComponent(typeof(UILabel))
	self.ModelPreviewer = self.transform:Find("Middle/Left/ModelPreviewer"):GetComponent(typeof(QnModelPreviewer))

	self.InfoGrid = self.transform:Find("Middle/Right/Infos/InfoGrid"):GetComponent(typeof(UIGrid))
	self.InfoTemplate = self.transform:Find("Middle/Right/Infos/InfoTemplate").gameObject

	self.YinPiaoLabel = self.transform:Find("Middle/Right/Wealth/WealthGrid/YinPiao/ValueLabel"):GetComponent(typeof(UILabel))
	self.YinLiangLabel = self.transform:Find("Middle/Right/Wealth/WealthGrid/YinLiang/ValueLabel"):GetComponent(typeof(UILabel))
	self.YuanBaoLabel = self.transform:Find("Middle/Right/Wealth/WealthGrid/YuanBao/ValueLabel"):GetComponent(typeof(UILabel))
	self.LingYuLabel = self.transform:Find("Middle/Right/Wealth/WealthGrid/LingYu/ValueLabel"):GetComponent(typeof(UILabel))

	
	-- 按钮初始化
	self.RegisterBtn = self.transform:Find("Bottom/Buttons/RegisterBtn"):GetComponent(typeof(CButton))

	self.CheckLabel = self.transform:Find("Bottom/Buttons/CheckLabel"):GetComponent(typeof(UILabel))

	self.Opetations = self.transform:Find("Bottom/Buttons/Opetations").gameObject
	self.ShowBtn = self.transform:Find("Bottom/Buttons/Opetations/ShowBtn"):GetComponent(typeof(CButton))
	self.SellBtn_Operation = self.transform:Find("Bottom/Buttons/Opetations/SellBtn_Operation"):GetComponent(typeof(CButton))

	self.ToShowRoot = self.transform:Find("Bottom/Buttons/ToShowRoot").gameObject
	self.ToShowLabel = self.transform:Find("Bottom/Buttons/ToShowRoot/ToShowLabel"):GetComponent(typeof(UILabel))
	self.UnShelfBtn = self.transform:Find("Bottom/Buttons/ToShowRoot/UnShelfBtn"):GetComponent(typeof(CButton))
	self.SellBtn_ToShow = self.transform:Find("Bottom/Buttons/ToShowRoot/SellBtn_ToShow"):GetComponent(typeof(CButton))

end

-- 更新需要显示的信息
function LuaTreasureHouseMyRoleView:UpdateShowInfo()

	local onMoreInfo = function (go)
		self:OnMoreInfoClick(go)
	end
	CommonDefs.AddOnClickListener(self.MoreInfo,DelegateFactory.Action_GameObject(onMoreInfo),false)

	if not CClientMainPlayer.Inst then return end

	self.PlayerNameLabel.text = CClientMainPlayer.Inst.Name
	-- 等级
	local color = "FFFFFF"
	if CClientMainPlayer.Inst.IsInXianShenStatus then
		color = Constants.ColorOfFeiSheng
	end
	local levelStr = SafeStringFormat3("[c][%s]lv. %s[-][/c]", color, tostring(CClientMainPlayer.Inst.Level))
	self.PlayerGradeLabel.text = levelStr

	-- 玩家模型
	self.ModelPreviewer:PreviewMainPlayer(System.UInt32.MaxValue, 0, 0)
	-- 个人详情
	self:UpdatePlayerInfo()
	-- 财富信息
	self:UpdateWealthInfo()

	-- 登记按钮
	local onRegisterBtnClicked = function (go)
		self:OnRegisterBtnClicked()
	end
	CommonDefs.AddOnClickListener(self.RegisterBtn.gameObject,DelegateFactory.Action_GameObject(onRegisterBtnClicked),false)

	-- 待售区展示按钮
	local onShowBtnClicked = function (go)
		self:OnShowBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.ShowBtn.gameObject,DelegateFactory.Action_GameObject(onShowBtnClicked),false)

	-- 藏宝阁出售(操作)按钮
	local onSellBtnClicked = function (go)
		self:OnSellBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.SellBtn_Operation.gameObject,DelegateFactory.Action_GameObject(onSellBtnClicked),false)
	CommonDefs.AddOnClickListener(self.SellBtn_ToShow.gameObject,DelegateFactory.Action_GameObject(onSellBtnClicked),false)

	-- 待售区下架按钮
	local onUnshelfBtnClicked = function (go)
		self:OnUnshelfBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.UnShelfBtn.gameObject,DelegateFactory.Action_GameObject(onUnshelfBtnClicked),false)

	-- 按钮显示的状况
	self:UpdateButtons()

end

-- 更新玩家综合信息
function LuaTreasureHouseMyRoleView:UpdatePlayerInfo()
	CUICommonDef.ClearTransform(self.InfoGrid.transform)

	for i = 1, 8 do
		local go = NGUITools.AddChild(self.InfoGrid.gameObject, self.InfoTemplate)
		go:SetActive(true)
		self:InitPlayerProperty(go, i)
	end
	self.InfoGrid:Reposition()
	
end

-- Init玩家综合信息
function LuaTreasureHouseMyRoleView:InitPlayerProperty(go, index)
	local PropertyNameLabel = go.transform:Find("PropertyNameLabel"):GetComponent(typeof(UILabel))
	local ValueLabel = go.transform:Find("ValueLabel"):GetComponent(typeof(UILabel))
	if index == 1 then -- 战斗力
		PropertyNameLabel.text = LocalString.GetString("[acf8ff]战斗力[-]")
		ValueLabel.text = tostring(CClientMainPlayer.Inst.PlayProp.PlayerCapacity)
	elseif index == 2 then -- 装评
		PropertyNameLabel.text = LocalString.GetString("[acf8ff]装评[-]")
		ValueLabel.text = tostring(CClientMainPlayer.Inst.EquipScore)
	elseif index == 3 then -- 修为  
		PropertyNameLabel.text = LocalString.GetString("[acf8ff]修为[-]")
		ValueLabel.text = SafeStringFormat3("%0.1f", CClientMainPlayer.Inst.BasicProp.XiuWeiGrade)
	elseif index == 4 then -- 修炼
		PropertyNameLabel.text = LocalString.GetString("[acf8ff]修炼[-]")
		ValueLabel.text = SafeStringFormat3("%0.1f", CClientMainPlayer.Inst.BasicProp.XiuLian)
	elseif index == 5 then -- 强化套装
		local intesifySuitId = CClientMainPlayer.Inst.ItemProp.IntesifySuitId
		local find = EquipIntensify_Suit.GetData(intesifySuitId)
		PropertyNameLabel.text = LocalString.GetString("[acf8ff]强化套装[-]")
		if not find then
			ValueLabel.text = "0"
		else
			ValueLabel.text = tostring(find.IntensifyLevel)
		end
		
	elseif index == 6 then -- 灵兽评分
		PropertyNameLabel.text = LocalString.GetString("[acf8ff]灵兽评分[-]")
		ValueLabel.text = tostring(CLuaPlayerCapacityMgr.GetValue("LingShou"))
	elseif index == 7 then -- 家园等级
		PropertyNameLabel.text = LocalString.GetString("[acf8ff]家园等级[-]")
		if not CClientHouseMgr.inst.mConstructProp then
			ValueLabel.text = "0"
		else
			ValueLabel.text = tostring(CClientHouseMgr.inst.mConstructProp.ZhengwuGrade)
		end
	elseif index == 8 then -- 星官点数
		PropertyNameLabel.text = LocalString.GetString("[acf8ff]星官点数[-]")
		local xingmangCount = 0
		if CClientMainPlayer.Inst.HasFeiSheng then
			xingmangCount = CFeiShengMgr.Inst.m_TotalXingmangCount
		end
		ValueLabel.text = tostring(xingmangCount)
	end
end

-- 更新玩家财富信息
function LuaTreasureHouseMyRoleView:UpdateWealthInfo()
	if not CClientMainPlayer.Inst then return end
	self.YinPiaoLabel.text = tostring(CClientMainPlayer.Inst.FreeSilver)
	self.YinLiangLabel.text = tostring(CClientMainPlayer.Inst.Silver)
	self.YuanBaoLabel.text = tostring(CClientMainPlayer.Inst.ItemProp.YuanBao)
	self.LingYuLabel.text = tostring(CClientMainPlayer.Inst.Jade)
end

-- 更新按钮信息
function LuaTreasureHouseMyRoleView:UpdateButtons()
	self.RegisterBtn.gameObject:SetActive(false)
	self.CheckLabel.gameObject:SetActive(false)
	self.Opetations:SetActive(false)
	self.ToShowRoot:SetActive(false)

	local CBGStatus = CClientMainPlayer.Inst.BasicProp.CangbaogeInfo.CBGStatus

	if CBGStatus == EnumCangbaogeStatus.eInit then
		-- 未登记状态
		self.RegisterBtn.gameObject:SetActive(true)
		self.HintLabel.text = LocalString.GetString("角色交易前需进行登记，登记审核后才能对角色进行待售展示和上架出售操作")
	elseif CBGStatus == EnumCangbaogeStatus.eRegistered then
		self.HintLabel.text = LocalString.GetString("角色登记后连续14天未进行待售展示和上架操作，将取消登记")
		-- 审核状态
		local lastRegisterTime = CClientMainPlayer.Inst.PlayProp.CangbaogePlayInfo.LastRegisterTime
		local nowTime = CServerTimeMgr.Inst.timeStamp
		-- 如果注册时间在7天内, 则显示还在审核中
		if nowTime - lastRegisterTime < (self.CheckInterval * 24 * 60 * 60) then
			self.CheckLabel.gameObject:SetActive(true)
			local registerDate = CServerTimeMgr.ConvertTimeStampToZone8Time(lastRegisterTime)
			local checkDate = registerDate:AddDays(self.CheckInterval)
			self.CheckLabel.text = SafeStringFormat3(LocalString.GetString("角色审核中 (将于%s完成审核)"), checkDate:ToString("yyyy-MM-dd, HH:mm"))
		else
			-- GameOnShelfTime == 0 表示还未上架
			if CClientMainPlayer.Inst.PlayProp.CangbaogePlayInfo.GameOnShelfTime == 0 then
				-- 可操作状态
				self.Opetations:SetActive(true)
			else
				-- 待售区展示状态
				self.ToShowRoot:SetActive(true)
				-- 显示自动下架时间
				local offShelfTime = CClientMainPlayer.Inst.PlayProp.CangbaogePlayInfo.GameNextOffShelfTime
				local offShelfDate =  CServerTimeMgr.ConvertTimeStampToZone8Time(offShelfTime)
				self.ToShowLabel.text = SafeStringFormat3(LocalString.GetString("待售区展示中 (%s将自动下架)"), offShelfDate:ToString("yyyy-MM-dd, HH:mm"))
			end
		end
	end
end

-- 藏宝阁-我的角色-更多信息
function LuaTreasureHouseMyRoleView:OnMoreInfoClick(go)
	MessageMgr.Inst:ShowMessage("CBG_MYROLE_TIPS", {})
end

-- 点击注册按钮
function LuaTreasureHouseMyRoleView:OnRegisterBtnClicked(go)
	Gac2Gas.CBG_RequestGetConditionsInfo(CClientMainPlayer.Inst.Id, "register", "")
end

-- 待售区展示按钮
function LuaTreasureHouseMyRoleView:OnShowBtnClicked(go)
	CUIManager.ShowUI(CLuaUIResources.TreasureHousePutToShowWnd)
end

-- 藏宝阁出售按钮
function LuaTreasureHouseMyRoleView:OnSellBtnClicked(go)
	Gac2Gas.CBG_RequestGetConditionsInfo(CClientMainPlayer.Inst.Id, "onshelf", "")
end

-- 待售区下架按钮
function LuaTreasureHouseMyRoleView:OnUnshelfBtnClicked(go)
	Gac2Gas.CBG_RequestGameOffShelf(CClientMainPlayer.Inst.Id, "")
end

return LuaTreasureHouseMyRoleView
