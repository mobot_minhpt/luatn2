local DelegateFactory = import "DelegateFactory"
local UISlider        = import "UISlider"
local UILabel         = import "UILabel"
local CUITexture      = import "L10.UI.CUITexture"
local CUIWidgetHSV    = import "L10.UI.CUIWidgetHSV"

LuaHSVTestWnd = class()

RegistClassMember(LuaHSVTestWnd, "m_Texture")
RegistClassMember(LuaHSVTestWnd, "m_Sprite")
RegistClassMember(LuaHSVTestWnd, "m_CUIWidgetHSV")

RegistClassMember(LuaHSVTestWnd, "m_HSlider")
RegistClassMember(LuaHSVTestWnd, "m_SSlider")
RegistClassMember(LuaHSVTestWnd, "m_VSlider")
RegistClassMember(LuaHSVTestWnd, "m_Hue")
RegistClassMember(LuaHSVTestWnd, "m_Saturation")
RegistClassMember(LuaHSVTestWnd, "m_Brightness")


function LuaHSVTestWnd:Awake()
    self.m_Texture = self.transform:Find("Texture"):GetComponent(typeof(CUITexture))
    self.m_Sprite = self.transform:Find("Sprite"):GetComponent(typeof(UISprite))
    self.m_HSlider = self.transform:Find("Table/HSlider"):GetComponent(typeof(UISlider))
    self.m_SSlider = self.transform:Find("Table/SSlider"):GetComponent(typeof(UISlider))
    self.m_VSlider = self.transform:Find("Table/VSlider"):GetComponent(typeof(UISlider))

    if LuaHSVTestMgr.m_MatInfo.isSprite then
        self.m_CUIWidgetHSV = self.transform:Find("Sprite"):GetComponent(typeof(CUIWidgetHSV))
    else
        self.m_CUIWidgetHSV = self.transform:Find("Texture"):GetComponent(typeof(CUIWidgetHSV))
    end

    self.m_Hue, self.m_Saturation, self.m_Brightness = 0, 0, 0

    self.m_HSlider.OnChangeValue = DelegateFactory.Action_float(function(value)
        self.m_Hue = math.floor(value *(180-(-180)) + (-180))
        self.m_HSlider.transform:Find("Percentage"):GetComponent(typeof(UILabel)).text = tostring(self.m_Hue)
        self:UpdateHSV()
    end)
    self.m_SSlider.OnChangeValue = DelegateFactory.Action_float(function(value)
        self.m_Saturation = math.floor(value *(100-(-100)) + (-100))
        self.m_SSlider.transform:Find("Percentage"):GetComponent(typeof(UILabel)).text = tostring(self.m_Saturation)
        self:UpdateHSV()
    end)
    self.m_VSlider.OnChangeValue = DelegateFactory.Action_float(function(value)
        self.m_Brightness = math.floor(value *(100-(-100)) + (-100))
        self.m_VSlider.transform:Find("Percentage"):GetComponent(typeof(UILabel)).text = tostring(self.m_Brightness)
        self:UpdateHSV()
    end)

    self.m_HSlider.value = 0.5
    self.m_SSlider.value = 0.5
    self.m_VSlider.value = 0.5
end

function LuaHSVTestWnd:Init()
    local isSprite = LuaHSVTestMgr.m_MatInfo.isSprite
    local widget = nil
    if isSprite then
        self.m_Texture.gameObject:SetActive(false)
        self.m_Sprite.spriteName = LuaHSVTestMgr.m_MatInfo.path
        widget = self.m_Sprite
    else
        self.m_Sprite.gameObject:SetActive(false)
        self.m_Texture:LoadMaterial(LuaHSVTestMgr.m_MatInfo.path)
        widget = self.m_Texture.texture
    end

    local oriWidth = LuaHSVTestMgr.m_MatInfo.width
    local oriHeight = LuaHSVTestMgr.m_MatInfo.height
    if oriWidth>oriHeight then
        if oriWidth>512 then
            widget.width = 512
            widget.height = oriHeight * (512/oriWidth)
        else
            widget.width = oriWidth
            widget.height = oriHeight
        end
    else
        if oriHeight>512 then
            widget.width = oriWidth * (512/oriHeight)
            widget.height = oriHeight
        else
            widget.width = oriWidth
            widget.height = oriHeight
        end
    end
end

function LuaHSVTestWnd:UpdateHSV()
    self.m_CUIWidgetHSV:SetHSVOffset(self.m_Hue, self.m_Saturation, self.m_Brightness)
end

LuaHSVTestMgr = {}
LuaHSVTestMgr.m_MatInfo = nil
function LuaHSVTestMgr:Show(matPath, width, height, isSprite)
    self.m_MatInfo = {}
    self.m_MatInfo.path = matPath
    self.m_MatInfo.width = width
    self.m_MatInfo.height = height
    self.m_MatInfo.isSprite = isSprite and true or false
    CUIManager.ShowUI("HSVTestWnd")
end

