local CUITexture = import "L10.UI.CUITexture"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local UISprite = import "UISprite"

LuaAppearanceCommonButtonDisplayView = class()
RegistChildComponent(LuaAppearanceCommonButtonDisplayView,"m_RegularButtonRoot", "RegularButtonRoot", Transform)
RegistChildComponent(LuaAppearanceCommonButtonDisplayView,"m_CollectButton", "CollectButton", CButton) --收藏按钮
RegistChildComponent(LuaAppearanceCommonButtonDisplayView,"m_BigButton", "BigButton", CButton) --显示一个按钮时，使用该按钮
RegistChildComponent(LuaAppearanceCommonButtonDisplayView,"m_LeftButton", "LeftButton", CButton) --显示两个按钮时，左边使用该按钮
RegistChildComponent(LuaAppearanceCommonButtonDisplayView,"m_RightButton", "RightButton", CButton) --显示两个按钮时，右边使用该按钮

function LuaAppearanceCommonButtonDisplayView:Awake()
end

-- buttonTbl每个元素的格式 {text=xxx, isYellow=xxx, action=xxx}
function LuaAppearanceCommonButtonDisplayView:Init(buttonTbl)
    local bShowCollect = false --根据最新的需求去掉了收藏功能
    if bShowCollect then
        -- self.m_CollectButton.gameObject:SetActive(true)
        -- self.m_CollectButton.Selected = isCollected
        -- Extensions.SetLocalPositionX(self.m_RegularButtonRoot.transform, 0)
        -- UIEventListener.Get(self.m_CollectButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        --      if collectFunc then collectFunc(self.m_CollectButton.gameObject) end
        -- end)
    else
        self.m_CollectButton.gameObject:SetActive(false)
        Extensions.SetLocalPositionX(self.m_RegularButtonRoot.transform, -32)
    end
    self.m_BigButton.gameObject:SetActive(false)
    self.m_LeftButton.gameObject:SetActive(false)
    self.m_RightButton.gameObject:SetActive(false)
    local tbl = {}
    --最少显示0个按钮，最多显示2个按钮
    if buttonTbl and #buttonTbl>0 and #buttonTbl<3 then
        if #buttonTbl==1 then
            table.insert(tbl, self.m_BigButton)
        else
            table.insert(tbl, self.m_LeftButton)
            table.insert(tbl, self.m_RightButton)
        end
    end

    for i=1,#buttonTbl do
        local btn = tbl[i]
        local info = buttonTbl[i]
        btn.gameObject:SetActive(true)
        btn.Text = info.text
        btn:SetBackgroundSprite(info.isYellow and g_sprites.AppearanceCommonOrangeButtonBg or g_sprites.AppearanceCommonBlueButtonBg)
        btn:SetFontTextColor(info.isYellow and NGUIText.ParseColor24("754923", 0) or NGUIText.ParseColor24("1C4165", 0))
        UIEventListener.Get(btn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            if info.action then
                info.action(go)
            end
        end)
    end
end



