require("3rdParty/ScriptEvent")
require("common/common_include")
require("ui/teamgroup/LuaTeamGroupPlayerInfoItem")
local LuaGameObject = import "LuaGameObject"
local CTeamGroupMgr = import "L10.Game.CTeamGroupMgr"
local CUICommonDef = import "L10.UI.CUICommonDef" 
local Vector3 = import "UnityEngine.Vector3"

CLuaTeamGroupTeamInfo= class()
RegistClassMember(CLuaTeamGroupTeamInfo,"m_IndexLabel")
RegistClassMember(CLuaTeamGroupTeamInfo,"m_CrossServerLabel")
RegistClassMember(CLuaTeamGroupTeamInfo,"m_Container")
RegistClassMember(CLuaTeamGroupTeamInfo,"m_MemberPrefab")
RegistClassMember(CLuaTeamGroupTeamInfo,"m_MemberTable")
RegistClassMember(CLuaTeamGroupTeamInfo,"m_Member2IDTable")
RegistClassMember(CLuaTeamGroupTeamInfo,"m_OnItemPress")
RegistClassMember(CLuaTeamGroupTeamInfo,"m_OnItemDrag")
RegistClassMember(CLuaTeamGroupTeamInfo,"m_OnTitlePress")
RegistClassMember(CLuaTeamGroupTeamInfo,"m_OnTitleDrag")
RegistClassMember(CLuaTeamGroupTeamInfo,"m_Parent")
RegistClassMember(CLuaTeamGroupTeamInfo,"m_Title")
RegistClassMember(CLuaTeamGroupTeamInfo,"m_Index")
RegistClassMember(CLuaTeamGroupTeamInfo,"m_gameObject")
function CLuaTeamGroupTeamInfo:ctor()
 
end
function CLuaTeamGroupTeamInfo:AttachToGo(go)
	self.m_gameObject = go
	local trans = go.transform
	self.m_IndexLabel = LuaGameObject.GetChildNoGC(trans,"IndexLabel").label
	self.m_CrossServerLabel = LuaGameObject.GetChildNoGC(trans,"CrossServerLabel").label
	self.m_Container = LuaGameObject.GetChildNoGC(trans,"MemberContainer").widget
	self.m_MemberPrefab = LuaGameObject.GetChildNoGC(trans,"PlayerInfoPrefab").gameObject
	self.m_Title = LuaGameObject.GetChildNoGC(trans,"TitleSprite").gameObject
	self:InitMembers()
	local OnTitlePress = function(go,flag)
		if self.m_OnTitlePress ~= nil then
			self.m_OnTitlePress(self.m_Parent,go,flag)
		end
	end
	local OnTitleDrag = function(go,delta)
		if self.m_OnTitleDrag ~= nil then
			self.m_OnTitleDrag(self.m_Parent,go,delta)
		end
	end
	CommonDefs.AddOnDragListener(self.m_Title,DelegateFactory.Action_GameObject_Vector2(OnTitleDrag),false)
	CommonDefs.AddOnPressListener(self.m_Title,DelegateFactory.Action_GameObject_bool(OnTitlePress),false)
end
function CLuaTeamGroupTeamInfo:RefreshInfo(teamIndex)
	self.m_IndexLabel.text = SafeStringFormat3(LocalString.GetString("%d队"),(teamIndex-1)%6+1)
	self.m_Index = teamIndex
	local teamUnit = CTeamGroupMgr.Instance:GetTeamGroupUnit(teamIndex)
	if teamUnit == nil then 
		for i = 1,5 do 
			local InfoItem = self.m_MemberTable[i]
			InfoItem:RefreshInfo(-1)
			self.m_Member2IDTable[InfoItem.m_gameObject] = -1
		end
		self.m_CrossServerLabel.text = ""
	else
		for i = 1,5 do 
			local InfoItem = self.m_MemberTable[i]
			if i <= teamUnit.MemberList.Count then 
				local member = teamUnit.MemberList[i-1]
				InfoItem:RefreshInfo(member.PlayerID)
				self.m_Member2IDTable[InfoItem.m_gameObject] = member.PlayerID 
			else
				InfoItem:RefreshInfo(-1)
				self.m_Member2IDTable[InfoItem.m_gameObject] = -1
			end
		end
		if IsCrossServerTeamInfoDisplayOpen() then
			self.m_CrossServerLabel.text = GetCrossServerTeamInfoDisplayText(teamUnit.TeamId)
		else
			self.m_CrossServerLabel.text = ""
		end
	end
end
function CLuaTeamGroupTeamInfo:InitMembers()
	self.m_MemberTable = {}
	self.m_Member2IDTable = {}
	local height = self.m_Container.height
	local containerGO = self.m_Container.gameObject
	for i = 1,5 do
		local member = CUICommonDef.AddChild(containerGO,self.m_MemberPrefab)
		local InfoItem = CLuaTeamGroupPlayerInfoItem:new()
		InfoItem.m_OnPress = self.OnItemPress
		InfoItem.m_OnDrag = self.OnItemDrag
		InfoItem.m_Parent = self
		InfoItem:AttachToGo(member)
		self.m_MemberTable[i] = InfoItem
		local pos = Vector3.zero
		pos.y = height/2 - height/5*(0.5 + i - 1)
		member.transform.localPosition = pos
		self.m_Member2IDTable[member] = -1
	end
	self.m_MemberPrefab:SetActive(false)
end
function CLuaTeamGroupTeamInfo:GameObjToPlayerID(go)
	return self.m_Member2IDTable[go]
end
function CLuaTeamGroupTeamInfo:OnItemPress(go,flag)
	if self.m_OnItemPress ~= nil then
		self.m_OnItemPress(self.m_Parent,go,flag)
	end
end
function CLuaTeamGroupTeamInfo:OnItemDrag(go,delta)
	if self.m_OnItemDrag ~= nil then
		self.m_OnItemDrag(self.m_Parent,go,delta)
	end
end

return CLuaTeamGroupTeamInfo
