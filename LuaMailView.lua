local QnTableView = import "L10.UI.QnTableView" 
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CMailMgr = import "L10.Game.CMailMgr"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local UILabel = import "UILabel"
local QnCheckBox = import "L10.UI.QnCheckBox"
local IdPartition=import "L10.Game.IdPartition"

CLuaMailView = class()
RegistClassMember(CLuaMailView,"m_MailTableView")
RegistClassMember(CLuaMailView,"m_MailContentView")
RegistClassMember(CLuaMailView,"m_EmptyLabel")
RegistClassMember(CLuaMailView,"m_MailNumLabel")
RegistClassMember(CLuaMailView,"m_AlmostFullIcon")
RegistClassMember(CLuaMailView,"m_SelectedMailId")
RegistClassMember(CLuaMailView,"m_SelectedMailRow")
RegistClassMember(CLuaMailView,"m_SelectedMail")
RegistClassMember(CLuaMailView,"m_UpdateTick")
RegistClassMember(CLuaMailView,"m_NeedUpdate")

RegistClassMember(CLuaMailView,"m_AllMails") --邮件数据
RegistClassMember(CLuaMailView,"m_IsInBatchDeleteMode") --是否处于批量删除模式下
RegistClassMember(CLuaMailView,"m_CandidateDeleteMailTbl") --待删除邮件ID集合
RegistClassMember(CLuaMailView,"m_BatchDeleteButton") -- 批量删除按钮
RegistClassMember(CLuaMailView,"m_ConfirmDeleteButton") --确认批量删除按钮
RegistClassMember(CLuaMailView,"m_CancelDeleteButton") --取消批量删除按钮
RegistClassMember(CLuaMailView,"m_SelectAllCheckbox") --选取所有可以一键删除的邮件

function CLuaMailView:Start()

end

function CLuaMailView:OnEnable()
    --Copy From Old C# CMailView.cs : 引导过程中可能把窗口隐藏，需要把初始化移动至OnEnable

    self.m_SelectedMailId = nil
    self.m_SelectedMailRow = -1
    --左侧邮件列表
    self.m_MailTableView = self.transform:Find("MailListView"):GetComponent(typeof(QnTableView))
    --邮件内容页
    self.m_MailContentView = self.transform:Find("MailContentView"):GetComponent(typeof(CCommonLuaScript))
    --无邮件提示文字
    self.m_EmptyLabel = self.transform:Find("MailListView/EmptyLabel").gameObject
    --邮件数量文字
    self.m_MailNumLabel = self.transform:Find("MailListView/Panel/MailNumDisplay"):GetComponent(typeof(UILabel))
    --邮件数将满警告
    self.m_AlmostFullIcon = self.transform:Find("MailListView/Panel/MailNumDisplay/Icon").gameObject

    self.m_BatchDeleteButton = self.transform:Find("MailListView/Panel/BatchDeleteButton").gameObject
    self.m_ConfirmDeleteButton = self.transform:Find("MailListView/Panel/ConfirmDeleteButton").gameObject
    self.m_CancelDeleteButton = self.transform:Find("MailListView/Panel/CancelDeleteButton").gameObject

    self.m_BatchConfirmBtn = self.transform:Find("MailListView/Panel/BatchConfirmBtn").gameObject

    self.m_SelectAllCheckbox = self.transform:Find("MailListView/Panel/Checkbox"):GetComponent(typeof(QnCheckBox))
    self.m_IsInBatchDeleteMode = false
    self.m_AllMails = {}
    self:InitBatchDeleteMode()

    self.m_MailTableView.m_DataSource = DefaultTableViewDataSource.Create(
        function() return #self.m_AllMails end,
        function(item,row) self:InitMailItem(item,row) end
    )

    self.m_MailTableView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        local mails = self.m_AllMails
        local mail = mails[row+1]
        self.m_SelectedMailId = mail.MailId

        self.m_MailContentView.gameObject:SetActive(true)
        self.m_MailContentView:Init({mail.MailId})
 
        --取消选择旧的邮件
        if(self.m_SelectedMailRow >= 0)then
            local item = self.m_MailTableView:GetItemAtRow(self.m_SelectedMailRow).gameObject:GetComponent(typeof(CCommonLuaScript))
            item.m_LuaSelf:SetSelected(false)
        end
        --选择新的邮件
        do
            self.m_SelectedMailRow = row
            local item = self.m_MailTableView:GetItemAtRow(row).gameObject:GetComponent(typeof(CCommonLuaScript))
            item.m_LuaSelf:SetSelected(true)
        end
    end)

    self.m_NeedUpdate = true
    self:RefreshMailTableView(true)
    self:SelectMail(self.m_SelectedMailRow)

    self.m_UpdateTick = RegisterTick(function()
        if self.m_IsInBatchDeleteMode then
            return
        end
        if(CMailMgr.Inst.MailUpdated)then
            CMailMgr.Inst.MailUpdated = false
            self:RefreshMailTableView(true)
        end  
        if(self.m_NeedUpdate)then
            self.m_NeedUpdate = false
            self.m_MailTableView:ScrollToRow(self.m_SelectedMailRow)
        end
    end, 200)

    self.m_SelectAllCheckbox.OnValueChanged = DelegateFactory.Action_bool(function (checked) self:OnSelectAllChecked(checked) end)
    CommonDefs.AddOnClickListener(self.m_BatchDeleteButton, DelegateFactory.Action_GameObject(function(go) self:OnBatchDeleteButtonClick(go) end), false)
    CommonDefs.AddOnClickListener(self.m_ConfirmDeleteButton, DelegateFactory.Action_GameObject(function(go) self:OnConfirmDeleteButtonClick(go) end), false)
    CommonDefs.AddOnClickListener(self.m_CancelDeleteButton, DelegateFactory.Action_GameObject(function(go) self:OnCancelDeleteButtonClick(go) end), false)
    CommonDefs.AddOnClickListener(self.m_BatchConfirmBtn, DelegateFactory.Action_GameObject(function(go) self:OnBatchConfirmBtn(go) end), false)

    --监听领取奖励是否成功
    g_ScriptEvent:AddListener("MailAcceptAwardsDone", self, "OnAcceptAwardsDone")
end

function CLuaMailView:OnDisable()
    --监听领取奖励是否成功
    g_ScriptEvent:RemoveListener("MailAcceptAwardsDone", self, "OnAcceptAwardsDone")
    
    if(self.m_UpdateTick)then
        UnRegisterTick(self.m_UpdateTick)
        self.m_UpdateTick = nil
    end
end

function CLuaMailView:OnAcceptAwardsDone(args)
    local success = args[0]
    local mailId = args[1]
    local mail = CMailMgr.Inst:GetMail(mailId)

    if mail ~= nil and mail.MailId == mailId then
        for i, v in ipairs(self.m_AllMails) do
            if v.MailId == mailId then
                self:SelectMail(i-1)
                break
            end
        end
    end

    -- 处理一键领取的显示
    if self.m_BatchConfirmList then
        for pos, id in ipairs(self.m_BatchConfirmList) do
            if id == mailId then
                local itemList = mail.ItemList
                if success and itemList then
                    for i = 0, itemList.Count -1 do
                        local item = itemList[i]

                        if IdPartition.IdIsEquip(item.TemplateId) then
                            table.insert( self.m_BatchRewardItemList, {ItemID = item.TemplateId, Count = item.Num, RealItemId = item.ItemId})
                        else
                            table.insert( self.m_BatchRewardItemList, {ItemID = item.TemplateId, Count = item.Num})
                        end
                    end
                end

                table.remove(self.m_BatchConfirmList, pos)
                if #self.m_BatchConfirmList == 0 and #self.m_BatchRewardItemList > 0 then
                    local buttonList = {
                        {spriteName = "blue", buttonLabel = LocalString.GetString("确定"), clickCB = function() CUIManager.CloseUI(CLuaUIResources.CommonGetRewardWnd) end},
                    }
                    LuaCommonGetRewardWnd.m_materialName = "gongxihuode"
                    LuaCommonGetRewardWnd.m_Reward1List = self.m_BatchRewardItemList
                    LuaCommonGetRewardWnd.m_Reward2Label = nil
                    LuaCommonGetRewardWnd.m_Reward2List = {}
                    LuaCommonGetRewardWnd.m_hint = ""
                    LuaCommonGetRewardWnd.m_button = buttonList
                    CUIManager.ShowUI(CLuaUIResources.CommonGetRewardWnd)
                end
            end
        end
    end
end

function CLuaMailView:InitBatchDeleteMode( )
    self.m_CandidateDeleteMailTbl = {}
    self.m_SelectAllCheckbox:SetSelected(false, true)
    self.m_BatchDeleteButton:SetActive(not self.m_IsInBatchDeleteMode)
    self.m_ConfirmDeleteButton:SetActive(self.m_IsInBatchDeleteMode)
    self.m_CancelDeleteButton:SetActive(self.m_IsInBatchDeleteMode)
    self.m_MailNumLabel.gameObject:SetActive(not self.m_IsInBatchDeleteMode)
    self.m_SelectAllCheckbox.gameObject:SetActive(self.m_IsInBatchDeleteMode)
    self.m_BatchConfirmBtn.gameObject:SetActive(not self.m_IsInBatchDeleteMode)
    self:RefreshBatchConfirmBtn()
    
    LuaMailMgr:SetBatchDeleteMode(self.m_IsInBatchDeleteMode)
end

function CLuaMailView:RefreshBatchConfirmBtn( )
    local needActive = false
    if (not self.m_IsInBatchDeleteMode) and self.m_AllMails then
        for __, mail in pairs(self.m_AllMails) do
            if mail and mail.CanAcceptAwards and mail.IsHandled == 0 then
                if mail.Template.SelectableNum <= 0 then
                    needActive = true
                end
            end
        end
    end
    CUICommonDef.SetActive(self.m_BatchConfirmBtn.gameObject, needActive, true)
end

function CLuaMailView:OnSelectAllChecked(checked)
    self.m_CandidateDeleteMailTbl = {}
    if checked then
        for __,mail in pairs(self.m_AllMails) do
            local data = mail.Template
            if data.HandleNotDel == 0 and self:CanDelete(mail) then
                self.m_CandidateDeleteMailTbl[mail.MailId] = true
            end
        end
    end
    self:RefreshMailTableView(true)
end

function CLuaMailView:OnBatchDeleteButtonClick(go)
    self.m_IsInBatchDeleteMode = true
    self:InitBatchDeleteMode()
    self:RefreshMailTableView(true)
end

function CLuaMailView:OnBatchConfirmBtn(go)
    local needSelect = false

    -- 一键领取
    if self.m_BatchConfirmList == nil or #self.m_BatchConfirmList == 0 then
        self.m_BatchConfirmList = {}
        self.m_BatchRewardItemList = {}
        for __, mail in pairs(self.m_AllMails) do
            if mail and mail.CanAcceptAwards and mail.IsHandled == 0 then
                if mail.Template.SelectableNum > 0 then
                    -- 需要选择奖励的邮件
                    needSelect = true
                else
                    --请求领取奖励
                    table.insert(self.m_BatchConfirmList, mail.MailId)
                    CMailMgr.Inst:HandleMail(mail.MailId, "")
                end
            end
        end
    end
    
    if needSelect then
        g_MessageMgr:ShowMessage("Mail_Need_Choose_Award_Item")
    end
end

function CLuaMailView:OnConfirmDeleteButtonClick(go)
    --逐一删除，结束后刷新
    local total = 0
    for __,mail in pairs(self.m_CandidateDeleteMailTbl) do
        total = total + 1
    end
    if total>0 then
        Gac2Gas.RequestBatchDelMailBegin()
        for mailId,__ in pairs(self.m_CandidateDeleteMailTbl) do
            Gac2Gas.RequestBatchDelMail(mailId)
        end
        Gac2Gas.RequestBatchDelMailEnd()
    end

    self.m_IsInBatchDeleteMode = false
    self:InitBatchDeleteMode()
    self:RefreshMailTableView(true)
end

function CLuaMailView:OnCancelDeleteButtonClick(go)
    self.m_IsInBatchDeleteMode = false
    self:InitBatchDeleteMode()
    self:RefreshMailTableView(true)
end

function CLuaMailView:OnDestroy()

    if(self.m_UpdateTick)then
        UnRegisterTick(self.m_UpdateTick)
        self.m_UpdateTick = nil
    end
end

function CLuaMailView:SortAllMailsWhenDeleteMode()
    --根据最新的需求，保持原来的顺序不改变
    if true then return end
    table.sort(self.m_AllMails, function (mail1, mail2)
        local canDelMail1 = self:CanDelete(mail1)
        local canDelMail2 = self:CanDelete(mail2)
        if canDelMail1 == canDelMail2 then
            return mail2.SendTime < mail1.SendTime
        elseif canDelMail1 then
            return false
        else
            return true
        end

    end)
end

function CLuaMailView:RefreshMailTableView(_reset)
    self.m_SelectedMailRow = -1
    local mails = CMailMgr.Inst:GetMails()
    self.m_AllMails = {}
    local existMail = false
    for i=0,mails.Count-1 do
        table.insert(self.m_AllMails, mails[i])
        if mails[i].MailId == self.m_SelectedMailId then
            existMail = true
        end
    end
    self:RefreshBatchConfirmBtn()
    
    --批量删除排序
    if self.m_IsInBatchDeleteMode then
        self.m_SelectedMailId = nil
        --根据最新的需求，保持原来的顺序不改变
        --self:SortAllMailsWhenDeleteMode()
    end
    self.m_MailTableView:ReloadData(not existMail, _reset) --reloaddata的第一个参数为true会导致重新选择item，从而可能会调用到选中条目的回调
    local selection_retargeted = false
    if(not existMail and #self.m_AllMails > 0)then
        selection_retargeted = true
        self.m_SelectedMailRow = 0

        for idx,mail in pairs(self.m_AllMails) do
            if not mail.bIsRead then
                self.m_SelectedMailId = mail.MailId
                self.m_SelectedMailRow = idx-1
                break
            end
        end
    end
    self.m_EmptyLabel:SetActive(not (mails.Count > 0))

    self.m_MailNumLabel.text = System.String.Format("{0}/{1}", mails.Count, CMailMgr.Inst.MaxMailCapacity)

    self.m_AlmostFullIcon:SetActive(mails.Count >= CMailMgr.Inst.MaxNumToShowAlmostFullIcon)

    --重新定位选中邮件
    if(selection_retargeted)then

        self:SelectMail(self.m_SelectedMailRow)
    elseif(self.m_SelectedMailRow == -1)then
        self.m_MailContentView:Init({nil})
    end
end

function CLuaMailView:SelectMail(_row)
    if(_row ~= nil and _row >= 0)then
        self.m_MailTableView:SetSelectRow(_row, true)
    else
        self.m_SelectedMailId = nil
        self.m_SelectedMailRow = -1
        self.m_MailContentView:Init({nil})
    end
end

function CLuaMailView:GetMailNum()
    local mails = #self.m_AllMails
    return mails.Count
end

function CLuaMailView:InitMailItem(_item, _row)
    local mails = self.m_AllMails
    local mail = mails[_row + 1]
    local mail_item = _item.gameObject:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
    local selected = false
    if(mail.MailId == self.m_SelectedMailId)then
        self.m_SelectedMailRow = _row
        selected = true
    end
    mail_item:InitContent(mail)
    mail_item:SetSelected(selected)

    --初始化批量删除状态
    if self.m_IsInBatchDeleteMode then

        if not self:CanDelete(mail) then
            mail_item:HideCheckbox()
        else
            mail_item:ShowCheckbox(self.m_CandidateDeleteMailTbl and self.m_CandidateDeleteMailTbl[mail.MailId] or false, function (checked, mailId)
                self:OnMailItemCheckBoxSelected(checked, mailId)
            end)
        end
    else
        mail_item:HideCheckbox()
    end
end

function CLuaMailView:CanDelete(mail)
    local hasAwards = mail.CanAcceptAwards
    local notAwarded = mail.IsHandled == 0
    --未读邮件、假邮件、未领奖邮件、不能从客户端删除的邮件，均不能被选取进行批量删除
    if not mail.bIsRead or mail.IsFake or (hasAwards and notAwarded) or mail.CannotDelFromClient>0 then
        return false
    else
        return true
    end
end

function CLuaMailView:OnMailItemCheckBoxSelected(checked, mailId)
    if checked then
        self.m_CandidateDeleteMailTbl[mailId] = true
    elseif self.m_CandidateDeleteMailTbl[mailId] then
        self.m_CandidateDeleteMailTbl[mailId] = nil
    end
end
