-- Auto Generated!!
local CAuctionItemInfo = import "L10.Game.CAuctionItemInfo"
local CAuctionMgr = import "L10.Game.CAuctionMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPackageItemCell = import "L10.UI.CPackageItemCell"
local CSelfAuctionShopItem = import "L10.UI.CSelfAuctionShopItem"
local CSelfAuctionShopWnd = import "L10.UI.CSelfAuctionShopWnd"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local EnumEventType = import "EnumEventType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local EventManager = import "EventManager"
local Gac2Gas = import "L10.Game.Gac2Gas"
local Item_Item = import "L10.Game.Item_Item"
local LocalString = import "LocalString"
local Paimai_SortType = import "L10.Game.Paimai_SortType"
local String = import "System.String"
local UIGrid = import "UIGrid"
local UIScrollView = import "UIScrollView"
local Vector3 = import "UnityEngine.Vector3"
CSelfAuctionShopWnd.m_OnEnable_CS2LuaHook = function (this) 
    this:showPackage()
    this:showShelf()
    Gac2Gas.GetMyPaimaiOnShelfGoods()
    EventManager.AddListener(EnumEventType.OnMyAuctionShelfListRefresh, MakeDelegateFromCSFunction(this.showShelfItems, Action0, this))
    EventManager.AddListener(EnumEventType.OnAuctionItemOffShelf, MakeDelegateFromCSFunction(this.onShelfChange, Action0, this))
    EventManager.AddListener(EnumEventType.OnAuctionItemOnShelf, MakeDelegateFromCSFunction(this.onShelfChange, Action0, this))
end
CSelfAuctionShopWnd.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListener(EnumEventType.OnMyAuctionShelfListRefresh, MakeDelegateFromCSFunction(this.showShelfItems, Action0, this))
    EventManager.RemoveListener(EnumEventType.OnAuctionItemOffShelf, MakeDelegateFromCSFunction(this.onShelfChange, Action0, this))
    EventManager.RemoveListener(EnumEventType.OnAuctionItemOnShelf, MakeDelegateFromCSFunction(this.onShelfChange, Action0, this))
end
CSelfAuctionShopWnd.m_showPackage_CS2LuaHook = function (this) 
    CommonDefs.ListClear(this.itemCellList)
    local equip = Paimai_SortType.GetData(1)
    local item1 = Paimai_SortType.GetData(2)
    local item2 = Paimai_SortType.GetData(3)
    local item3 = Paimai_SortType.GetData(4)

    CUICommonDef.ClearTransform(this.leftGrid.transform)
    local ids = CreateFromClass(MakeGenericClass(List, String))
    local n = CClientMainPlayer.Inst.ItemProp:GetPlaceMaxSize(EnumItemPlace.Bag)
    do
        local i = 0
        while i < n do
            local itemId = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i + 1)
            if itemId ~= nil then
                local item = CItemMgr.Inst:GetById(itemId)
                if item.IsPrecious and (not item.IsBinded) then
                    if item.IsEquip then
                        local et = EquipmentTemplate_Equip.GetData(item.TemplateId)
                        if et ~= nil then
                            if this:containInArray(equip.Value, et.Type) then
                                CommonDefs.ListAdd(ids, typeof(String), itemId)
                            end
                        end
                    elseif item.IsItem then
                        local itemt = Item_Item.GetData(item.TemplateId)
                        if this:containInArray(item1.Value, itemt.Type) or this:containInArray(item2.Value, itemt.Type) or this:containInArray(item3.Value, itemt.Type) then
                            CommonDefs.ListAdd(ids, typeof(String), itemId)
                        end
                    end
                end
            end
            i = i + 1
        end
    end

    do
        local i = 0
        while i < ids.Count do
            local instance = CommonDefs.Object_Instantiate(this.itemCellPrefab)
            this.leftGrid:AddChild(instance.transform)
            instance.transform.localScale = Vector3.one
            instance:SetActive(true)
            local cell = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CPackageItemCell))
            cell.OnItemClickDelegate = MakeDelegateFromCSFunction(this.onCellClick, MakeGenericClass(Action1, CPackageItemCell), this)

            CommonDefs.ListAdd(this.itemCellList, typeof(CPackageItemCell), cell)
            cell:Init(ids[i], false, false, false, this.leftBagScrollView)
            this.leftGrid:Reposition()
            cell.Selected = false
            i = i + 1
        end
    end
    if ids.Count == 0 then
        this.bagEmptyLabel.text = LocalString.GetString("无未绑定珍品")
    else
        this.bagEmptyLabel.text = ""
    end
    this.leftBagScrollView.movement = UIScrollView.Movement.Unrestricted
    this.leftBagScrollView:InvalidateBounds()
    this.leftBagScrollView.movement = UIScrollView.Movement.Vertical
    this.leftBagScrollView:ResetPosition()
end
CSelfAuctionShopWnd.m_showShelf_CS2LuaHook = function (this) 
    CommonDefs.ListClear(this.shelfList)
    this.rightGrid.arrangement = UIGrid.Arrangement.Vertical
    CUICommonDef.ClearTransform(this.rightGrid.transform)
    for i = 0, 4 do
        local instance = CommonDefs.Object_Instantiate(this.itemPrefab)

        this.rightGrid:AddChild(instance.transform)
        instance.transform.localScale = Vector3.one
        instance:SetActive(true)
        local cell = CommonDefs.GetComponent_GameObject_Type(instance, typeof(CSelfAuctionShopItem))
        cell:SetData(nil)
        cell.clickCallback = MakeDelegateFromCSFunction(this.onShelfItemClick, MakeGenericClass(Action1, CAuctionItemInfo), this)
        CommonDefs.ListAdd(this.shelfList, typeof(CSelfAuctionShopItem), cell)
        this.rightGrid:Reposition()
    end

    this.rightShopScrollView.movement = UIScrollView.Movement.Unrestricted
    this.rightShopScrollView:InvalidateBounds()
    this.rightShopScrollView:SetDragAmount(0, 0, false)
    this.rightShopScrollView.movement = UIScrollView.Movement.Vertical
end
CSelfAuctionShopWnd.m_containInArray_CS2LuaHook = function (this, array, k) 

    do
        local i = 0
        while i < array.Length do
            if array[i] == k then
                return true
            end
            i = i + 1
        end
    end
    return false
end
CSelfAuctionShopWnd.m_showShelfItems_CS2LuaHook = function (this) 
    if this.shelfList.Count == 0 then
        this:showShelf()
    end

    local dataList = CAuctionMgr.Inst.CurrentItemInfoList
    for i = 0, 4 do
        local shelf = this.shelfList[i]
        if i >= dataList.Count then
            shelf:SetData(nil)
        else
            shelf:SetData(dataList[i])
        end
    end
    this.countLabel.text = System.String.Format("{0}/{1}", dataList.Count, CAuctionMgr.MaxShelfCount)
    CAuctionMgr.Inst.SelfShelfItemCount = dataList.Count
end
CSelfAuctionShopWnd.m_onShelfChange_CS2LuaHook = function (this) 
    Gac2Gas.GetMyPaimaiOnShelfGoods()
    this:showPackage()
end
CSelfAuctionShopWnd.m_onCellClick_CS2LuaHook = function (this, cell) 
    do
        local i = 0
        while i < this.itemCellList.Count do
            this.itemCellList[i].Selected = false
            i = i + 1
        end
    end
    cell.Selected = true
    CAuctionMgr.Inst.itemToShelf = CItemMgr.Inst:GetById(cell.ItemId)
    CUIManager.ShowUI(CUIResources.AuctionOnShelfWnd)
end
CSelfAuctionShopWnd.m_onShelfItemClick_CS2LuaHook = function (this, item) 
    if item == nil then
        return
    end
    CAuctionMgr.Inst.FocusInstanceID = item.AuctionID
    CUIManager.ShowUI(CUIResources.AuctionShelfDetailWnd)
end
