local CUIFx=import "L10.UI.CUIFx"
local CEquipBaptizeDescTable=import "L10.UI.CEquipBaptizeDescTable"
local CBaseEquipmentItem=import "L10.UI.CBaseEquipmentItem"
local PathType = import "DG.Tweening.PathType"
local PathMode = import "DG.Tweening.PathMode"
local MessageWndManager = import "L10.UI.MessageWndManager"
local LuaTweenUtils = import "LuaTweenUtils"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local Ease = import "DG.Tweening.Ease"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local CPropertyDifMgr = import "L10.UI.CPropertyDifMgr"
local CPreciousItemShareMgr = import "L10.Game.CPreciousItemShareMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CGetMoneyMgr = import "L10.UI.CGetMoneyMgr"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CEquipmentBaptizeMgr = import "L10.Game.CEquipmentBaptizeMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CAppStoreReviewBox = import "L10.UI.CAppStoreReviewBox"
local Vector4=import "UnityEngine.Vector4"


CLuaEquipWordBaptizeWnd = class()
RegistClassMember(CLuaEquipWordBaptizeWnd,"m_ItemId")
RegistClassMember(CLuaEquipWordBaptizeWnd,"equipBaptizeCost")
RegistClassMember(CLuaEquipWordBaptizeWnd,"closeButton")
RegistClassMember(CLuaEquipWordBaptizeWnd,"replaceBtn")
RegistClassMember(CLuaEquipWordBaptizeWnd,"baptizeBtn")
RegistClassMember(CLuaEquipWordBaptizeWnd,"newWordCount")
RegistClassMember(CLuaEquipWordBaptizeWnd,"equipItem")
RegistClassMember(CLuaEquipWordBaptizeWnd,"baptizeCount")
RegistClassMember(CLuaEquipWordBaptizeWnd,"countLabel")
RegistClassMember(CLuaEquipWordBaptizeWnd,"preDescTable")
RegistClassMember(CLuaEquipWordBaptizeWnd,"postDescTable")
RegistClassMember(CLuaEquipWordBaptizeWnd,"preScoreLabel")
RegistClassMember(CLuaEquipWordBaptizeWnd,"postScoreLabel")
RegistClassMember(CLuaEquipWordBaptizeWnd,"preScore")
RegistClassMember(CLuaEquipWordBaptizeWnd,"postScore")
RegistClassMember(CLuaEquipWordBaptizeWnd,"lookupDifBtn")
RegistClassMember(CLuaEquipWordBaptizeWnd,"shareBtn")
RegistClassMember(CLuaEquipWordBaptizeWnd,"shareFx")
RegistClassMember(CLuaEquipWordBaptizeWnd,"needConfirm")

RegistClassMember(CLuaEquipWordBaptizeWnd,"m_XiLianRadioBox")

function CLuaEquipWordBaptizeWnd:SetPreScore(score)
    self.preScoreLabel.text = CEquipmentBaptizeMgr.FormatScore(score)
    self.preScore = score
end
function CLuaEquipWordBaptizeWnd:SetPostScore(score)
    self.postScoreLabel.text = CEquipmentBaptizeMgr.FormatScore(score)
    self.postScore = score
end
function CLuaEquipWordBaptizeWnd:UpdateCount()
    self.countLabel.text = SafeStringFormat3(LocalString.GetString("已洗炼%d次"), self.baptizeCount)
end

function CLuaEquipWordBaptizeWnd:Init( )
    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    if equip ~= nil then
        self.m_ItemId = equip.itemId
    end

    self:SetPreScore(0)
    self:SetPostScore(0)

    --默认装备的洗炼目标是全选
    CEquipmentBaptizeMgr.Inst:InitCondition()

    self:InitStatus()
    self:TryShowLastResult()
end
function CLuaEquipWordBaptizeWnd:RefreshCurrentEquipWordDescription( )
    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    if equip ~= nil then
        local item = CItemMgr.Inst:GetById(equip.itemId)
        
        local fixedWordList,extWordList = item.Equip:GetFixedWordList(false), item.Equip:GetExtWordList(false)

        local fixedWordMaxTable = CLuaEquipMgr:EquipWordIdIsMaxWord(self.m_ItemId,List2Table(fixedWordList))
        local fixedWordMaxList = Table2List(fixedWordMaxTable, MakeGenericClass(List, Boolean))

        local extWordMaxTable = CLuaEquipMgr:EquipWordIdIsMaxWord(self.m_ItemId,List2Table(extWordList))
        local extWordMaxList = Table2List(extWordMaxTable, MakeGenericClass(List, Boolean))

        self.preDescTable:Init(fixedWordList,extWordList, nil, nil,fixedWordMaxList,extWordMaxList)
        --preScoreLabel.text = CEquipmentBaptizeMgr.FormatScore(item.Equip.Score);
        self:SetPreScore(item.Equip.Score)
    end
end
function CLuaEquipWordBaptizeWnd:TryClose( go) 
    if self.needConfirm then
        local tip = g_MessageMgr:FormatMessage("Close_Crafting_Equipment")
        MessageWndManager.ShowOKCancelMessage(tip, DelegateFactory.Action(function () 
            -- self:Close()
            CUIManager.CloseUI(CUIResources.EquipWordBaptizeWnd)
        end), nil, nil, nil, false)
    else
        -- self:Close()
        CUIManager.CloseUI(CUIResources.EquipWordBaptizeWnd)
    end
end
function CLuaEquipWordBaptizeWnd:InitStatus( )
    self.equipBaptizeCost:SetBaseInfo()

    self:UpdateCount()
    self:RefreshCurrentEquipWordDescription()

    --TryShowLastResult();
    self:UpdateReplaceBtnState()
end

function CLuaEquipWordBaptizeWnd:UpdateReplaceBtnState()
    if self:HasTempWordData() then
        CUICommonDef.SetActive(self.replaceBtn, true)
    else
        CUICommonDef.SetActive(self.replaceBtn, false)
    end
end
function CLuaEquipWordBaptizeWnd:TryShowLastResult( )
    self:SetPostScore(0)
    self.postDescTable:Clear()
    self.newWordCount = 0

    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    if equip ~= nil then
        self.equipItem:UpdateData(equip.itemId)
        local item = CItemMgr.Inst:GetById(equip.itemId)
        if item.Equip:HasTempWordData() then
            self.needConfirm = true
            --请求数据
            Gac2Gas.RequestEquipPropertyChangedWithTempWords(EnumToInt(equip.place), equip.pos, equip.itemId)
        else
            self.needConfirm = false
        end
    end
end
function CLuaEquipWordBaptizeWnd:HasTempWordData( )
    local item = CItemMgr.Inst:GetById(self.m_ItemId)
    if item ~= nil then
        if item.Equip:HasTempWordData() then
            return true
        end
    end
    return false
end
function CLuaEquipWordBaptizeWnd:Awake( )
    CLuaEquipXiLianMgr.ClearOption()
    self.m_ItemId = nil

    local script = self.transform:Find("Anchor/Cost"):GetComponent(typeof(CCommonLuaScript))
    script:Init({})
    self.equipBaptizeCost = script.m_LuaSelf

    self.closeButton = self.transform:Find("Wnd_Bg_Primary_Normal/CloseButton").gameObject
    self.replaceBtn = self.transform:Find("Anchor/ReplaceBtn").gameObject
    self.baptizeBtn = self.transform:Find("Anchor/BaptizeBtn").gameObject
self.newWordCount = 0
    self.equipItem = self.transform:Find("EquipItem"):GetComponent(typeof(CBaseEquipmentItem))
self.baptizeCount = 0
    self.countLabel = self.transform:Find("Anchor/EndLoglabel (1)"):GetComponent(typeof(UILabel))
    self.preDescTable = self.transform:Find("Anchor/Desc (1)/Desc"):GetComponent(typeof(CEquipBaptizeDescTable))
    self.postDescTable = self.transform:Find("Anchor/Desc (1)/Desc (2)"):GetComponent(typeof(CEquipBaptizeDescTable))
    self.preScoreLabel = self.transform:Find("Anchor/Desc (1)/PreScoreLabel"):GetComponent(typeof(UILabel))
    self.postScoreLabel = self.transform:Find("Anchor/Desc (1)/PostScoreLabel"):GetComponent(typeof(UILabel))
self.preScore = 0
self.postScore = 0
    self.lookupDifBtn = self.transform:Find("Anchor/LookupButton").gameObject
    self.shareBtn = self.transform:Find("Anchor/ShareButton").gameObject
    self.shareFx = self.transform:Find("Anchor/ShareButton/Fx"):GetComponent(typeof(CUIFx))
self.needConfirm = false
-- self.achieveTheGoal = false
-- self.m_AchieveAdvanceGoal =false


--高级洗炼目标
    local targetTf = self.transform:Find("Target")
    if CClientMainPlayer.Inst then
        local vipLevel = CClientMainPlayer.Inst.ItemProp.Vip.Level
        local equipData = CEquipmentProcessMgr.Inst.BaptizingEquipment
        local equipType = EquipmentTemplate_Equip.GetData(equipData.TemplateId).Type
        if vipLevel >= GameSetting_Common.GetData().AutoXiLianWordVIP and 
        (equipData.IsGhostEquipment or equipData.IsPurpleEquipment or equipData.IsRedEquipment) then
            targetTf.gameObject:SetActive(true)
        else
            targetTf.gameObject:SetActive(false)
        end
    else
        targetTf.gameObject:SetActive(false)
    end

    self.m_XiLianRadioBox = targetTf:GetComponent(typeof(QnRadioBox))
    local baseXiLianButton = targetTf:Find("Item1/DescLabel").gameObject
    UIEventListener.Get(baseXiLianButton).onClick = DelegateFactory.VoidDelegate(function(go)
        g_MessageMgr:ShowMessage("Default_XiLian_Mode")
    end)

    local targetButton = targetTf:Find("Item2/DescLabel").gameObject
    UIEventListener.Get(targetButton).onClick = DelegateFactory.VoidDelegate(function(go)
        local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
        local place = equip.place
        local pos = equip.pos
        local equipId = equip.itemId
        Gac2Gas.QueryAutoBaptizeTargetWordCondList(place, pos, equipId)
    end)

    self.shareBtn:SetActive(CPreciousItemShareMgr.Inst:EnableForumShareForEquipBaptize())
    self:HideShareFx()
    UIEventListener.Get(self.shareBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnShareButtonClick(go) end)
    UIEventListener.Get(self.closeButton).onClick = DelegateFactory.VoidDelegate(function(go) self:TryClose(go) end)
    --采用新的修饰词
    UIEventListener.Get(self.replaceBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        if self.newWordCount > 0 then
            local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
            local commonItem = CItemMgr.Inst:GetById(equip.itemId)
            if commonItem == nil then
                return
            end
            if equip.place == EnumItemPlace.Body and commonItem.Equip.TempGrade > CClientMainPlayer.Inst.FinalMaxLevelForEquip then
                local message = g_MessageMgr:FormatMessage("BAPTIZE_CONFIRM_LEVEL_OVER", commonItem.Equip.ColoredDisplayName, commonItem.Equip.TempGrade)
                MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function () self:ConfirmWord() end), nil, nil, nil, false)
                return
            end
            if commonItem.Equip.TempGrade > commonItem.Equip.Grade then
                local message = g_MessageMgr:FormatMessage("BAPTIZE_CONFIRM", commonItem.Equip.ColoredDisplayName, commonItem.Equip.TempGrade)
                MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function () self:ConfirmWord() end), nil, nil, nil, false)
                return
            else
                self:ConfirmWord()
            end
        else
            self.needConfirm = false
            g_MessageMgr:ShowMessage("Baptize_WordCount_TooLow")
        end
        self:UpdateReplaceBtnState()
    end)
    UIEventListener.Get(self.baptizeBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        self:OnClickXiLianButton(p)
    end)
    UIEventListener.Get(self.lookupDifBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnClickLookupDifButton(go) end)
end

function CLuaEquipWordBaptizeWnd:OnClickXiLianButton(go)
    -- 检查是否有未保存的熔炼结果
    CLuaEquipMgr:CheckUnsetComposite(self.m_ItemId, function()
        CLuaEquipMgr:CheckUnsetRecreate(self.m_ItemId, function()
            self:DoXiLian(go)
        end
        )
    end) 
end

function CLuaEquipWordBaptizeWnd:DoXiLian(go)
    if self.m_XiLianRadioBox.CurrentSelectIndex==1 then
        if not CLuaEquipXiLianMgr.IsAdvanceXiLianHaveChoice() then
            g_MessageMgr:ShowMessage("Advance_XiLian_NoTarget")
            return
        end
    end
    
    local fixedWordList = nil
    local extWordList = nil

    local equipInfo = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    if equip then
        local item = CItemMgr.Inst:GetById(equip.itemId)
        if item then
            if item.Equip:HasTempWordData() then
                fixedWordList = InitializeListWithArray(CreateFromClass(MakeGenericClass(List, UInt32)), UInt32, item.Equip:GetTempFixedWordArray())
                extWordList = InitializeListWithArray(CreateFromClass(MakeGenericClass(List, UInt32)), UInt32, item.Equip:GetTempExtWordArray())
            end
        end
    end
    local hasTemp = fixedWordList and extWordList
    local achieveTheGoal = false
    if hasTemp then
        if self.m_XiLianRadioBox.CurrentSelectIndex==1 then
            achieveTheGoal = CLuaEquipXiLianMgr.CheckBaptizeResult(fixedWordList, extWordList,true)
            --高级洗炼
            if achieveTheGoal then
                local tip = g_MessageMgr:FormatMessage("Better_AdvanceXiLian_Equip")
                MessageWndManager.ShowOKCancelMessage(tip, DelegateFactory.Action(function () 
                    self:TryBaptize()
                end), nil, nil, nil, false)
            else
                self:TryBaptize()
            end
        else
            achieveTheGoal = CLuaEquipXiLianMgr.CheckBaptizeResult(fixedWordList, extWordList)
            if achieveTheGoal then
                local tip = g_MessageMgr:FormatMessage("Better_Crafting_Equipment")
                MessageWndManager.ShowOKCancelMessage(tip, DelegateFactory.Action(function () 
                    self:TryBaptize()
                end), nil, nil, nil, false)
            else
                self:TryBaptize()
            end
        end
    else
        self:TryBaptize()
    end
end

function CLuaEquipWordBaptizeWnd:OnShareButtonClick(go)
    self:HideShareFx()
    CPreciousItemShareMgr.Inst:ShowEquipBaptizeShareWnd()
end

function CLuaEquipWordBaptizeWnd:OnClickLookupDifButton( go) 
    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    local equipListOnBody = CItemMgr.Inst:GetPlayerEquipmentListAtPlace(EnumItemPlace.Body)
    local find = false
    do
        local i = 0
        while i < equipListOnBody.Count do
            if equipListOnBody[i].itemId == equip.itemId then
                find = true
                break
            end
            i = i + 1
        end
    end
    if find then
        --是否装备
        Gac2Gas.TryConfirmBaptizeWord(EnumToInt(equip.place), equip.pos, equip.itemId, true)
    else
        g_MessageMgr:ShowMessage("Equip_Compare_NotWear_Tips")
    end
end
function CLuaEquipWordBaptizeWnd:ConfirmWord( )
    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    Gac2Gas.BaptizeWordResultConfirm(EnumToInt(equip.place), equip.pos, equip.itemId, true)
    self.needConfirm = false
    self.postDescTable:Clear()
    self:SetPostScore(0)
end
function CLuaEquipWordBaptizeWnd:TryBaptize( )
    if not self.equipBaptizeCost:yinliangEnough() then
        g_MessageMgr:ShowMessage("SILVER_NOT_ENOUGH")
        return
    end

    local canBaptize = true

    canBaptize = CEquipmentProcessMgr.Inst:CheckBaptizeCostNum()
    --净瓶不够
    if not canBaptize then
        if not self.equipBaptizeCost:isAutoCostJade() then
            canBaptize = false
            g_MessageMgr:ShowMessage("BAPTIZE_EQUIP_WORD_ITEM_NOT_ENOUGH", LocalString.GetString("净瓶"))
        else
            --自动消耗灵玉
            if not self.equipBaptizeCost:lingyuEnough() then
                canBaptize = false
                CGetMoneyMgr.Inst:GetLingYu(self.equipBaptizeCost:LingYuCost(), self.equipBaptizeCost.TemplateId)
            else
                canBaptize = true
            end
        end
    end

    if canBaptize then
        local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
        self:HideShareFx()
        --needConfirm = false;
        --判断有没有净瓶
        Gac2Gas.RequestBaptizeEquipmentWord(EnumToInt(equip.place), equip.pos, equip.itemId, false, self.equipBaptizeCost:isAutoCostJade())
        CUICommonDef.SetActive(self.baptizeBtn, false, true)
        CUICommonDef.SetActive(self.replaceBtn, false, true)
    end
end
function CLuaEquipWordBaptizeWnd:OnEnable( )
    g_ScriptEvent:AddListener("BaptizeWordResult", self, "OnBaptizeWordResult")
    g_ScriptEvent:AddListener("BaptizeFail", self, "BaptizeFail")
    g_ScriptEvent:AddListener("SendItem", self, "OnSendItem")
    g_ScriptEvent:AddListener("RequestEquipPropertyChangedWithTempWordsReturn", self, "OnRequestEquipPropertyChangedWithTempWordsReturn")
    g_ScriptEvent:AddListener("TryConfirmBaptizeWordResult", self, "OnTryConfirmBaptizeWordResult")
    g_ScriptEvent:AddListener("ConfirmAdvanceXiLian", self, "OnConfirmAdvanceXiLian")
end
function CLuaEquipWordBaptizeWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("BaptizeWordResult", self, "OnBaptizeWordResult")
    g_ScriptEvent:RemoveListener("BaptizeFail", self, "BaptizeFail")
    g_ScriptEvent:RemoveListener("SendItem", self, "OnSendItem")
    g_ScriptEvent:RemoveListener("RequestEquipPropertyChangedWithTempWordsReturn", self, "OnRequestEquipPropertyChangedWithTempWordsReturn")
    g_ScriptEvent:RemoveListener("TryConfirmBaptizeWordResult", self, "OnTryConfirmBaptizeWordResult")
    g_ScriptEvent:RemoveListener("ConfirmAdvanceXiLian", self, "OnConfirmAdvanceXiLian")
end

function CLuaEquipWordBaptizeWnd:OnConfirmAdvanceXiLian()
    self.m_XiLianRadioBox:ChangeTo(1,true)

end

function CLuaEquipWordBaptizeWnd:OnBaptizeWordResult(args)
    local fixedWordList, extWordList, score, changeProperties = args[0], args[1], args[2], args[3]
    self.baptizeCount=self.baptizeCount+1
    CUICommonDef.SetActive(self.baptizeBtn, true,true)
    self:BaptizeWordResult(fixedWordList, extWordList, score, false, changeProperties)
    self:UpdateReplaceBtnState()
end
function CLuaEquipWordBaptizeWnd:BaptizeFail(args)
    CUICommonDef.SetActive(self.baptizeBtn, true,true)
    self:UpdateReplaceBtnState()
end

function CLuaEquipWordBaptizeWnd:OnRequestEquipPropertyChangedWithTempWordsReturn(args)
    local dic =args[0]
    local equipInfo = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    if equip then
        local item = CItemMgr.Inst:GetById(equip.itemId)
        if item then
            if item.Equip:HasTempWordData() then
                local list1 = InitializeListWithArray(CreateFromClass(MakeGenericClass(List, UInt32)), UInt32, item.Equip:GetTempFixedWordArray())
                local list2 = InitializeListWithArray(CreateFromClass(MakeGenericClass(List, UInt32)), UInt32, item.Equip:GetTempExtWordArray())
                self:BaptizeWordResult(list1, list2, item.Equip.TempScore, true, dic)
            end
        end
    end
end

function CLuaEquipWordBaptizeWnd:OnSendItem( args) 
    local itemId = args[0]
    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    if equip ~= nil and equip.itemId == itemId then
        --RefreshCurrentEquipWordDescription();
        --equipBaptizeCost.SetBaseInfo();
        self:InitStatus()
    end
end
function CLuaEquipWordBaptizeWnd:BaptizeWordResult( fixedWordList, extWordList, score, lastNotConfirm, changeProperties) 
    self:UpdateCount()

    self.needConfirm = true

    self.newWordCount = 0
    do
        local i = 0
        while i < fixedWordList.Count do
            if fixedWordList[i] > 0 then
                self.newWordCount = self.newWordCount + 1
            end
            i = i + 1
        end
    end
    do
        local i = 0
        while i < extWordList.Count do
            if extWordList[i] > 0 then
                self.newWordCount = self.newWordCount + 1
            end
            i = i + 1
        end
    end
    --add zzx
    if self.newWordCount >= 7 then
        CAppStoreReviewBox.Show()
    end
    
    if self.m_XiLianRadioBox.CurrentSelectIndex==1 then
        --高级洗炼
        if CLuaEquipXiLianMgr.CheckBaptizeResult(fixedWordList, extWordList,true) then
            CPreciousItemShareMgr.Inst:RequestTriggerBaptizeShare(self.equipItem.itemId, false)
            self:ShowShareFx()
        end
    else
        if CLuaEquipXiLianMgr.CheckBaptizeResult(fixedWordList, extWordList) then
            CPreciousItemShareMgr.Inst:RequestTriggerBaptizeShare(self.equipItem.itemId, false)
            self:ShowShareFx()
        end
    end
    local fixedWordMaxTable = CLuaEquipMgr:EquipWordIdIsMaxWord(self.m_ItemId,List2Table(fixedWordList))
    local fixedWordMaxList = Table2List(fixedWordMaxTable, MakeGenericClass(List, Boolean))

    local extWordMaxTable = CLuaEquipMgr:EquipWordIdIsMaxWord(self.m_ItemId,List2Table(extWordList))
    local extWordMaxList = Table2List(extWordMaxTable, MakeGenericClass(List, Boolean))
    self.postDescTable:Init(fixedWordList, extWordList, nil, changeProperties,fixedWordMaxList,extWordMaxList)
    self:SetPostScore(score)
end
function CLuaEquipWordBaptizeWnd:OnTryConfirmBaptizeWordResult( args) 
    local list = args[0]
    -- local list = TypeAs(obj, typeof(MakeGenericClass(List, PlayerShowDifProType)))
    if list == nil then
        return
    end
    if list.Count == 0 then
        g_MessageMgr:ShowMessage("BaptizeWordResultNoPropDif")
        return
    end
    CPropertyDifMgr.ShowPlayerProDifWithEquip(list)
end
function CLuaEquipWordBaptizeWnd:ShowShareFx( )
    if self.shareBtn.activeSelf then
        self.shareFx:LoadFx(CUIFxPaths.CommonBlueLineFxPath)
        LuaTweenUtils.DOKill(self.shareFx.FxRoot, false)
        local background = CommonDefs.GetComponent_GameObject_Type(self.shareBtn, typeof(UISprite))
        local gap = 5
        local bounds = Vector4(- background.width * 0.5 + gap, background.height * 0.5 - gap, background.width - gap * 2, background.height - gap * 2)
        local waypoints = CUICommonDef.GetWayPoints(bounds, 1, true)
        self.shareFx.FxRoot.localPosition = waypoints[0]
        LuaTweenUtils.DOLuaLocalPath(self.shareFx.FxRoot, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, - 1, Ease.Linear)
    end
end
function CLuaEquipWordBaptizeWnd:HideShareFx( )
    if self.shareBtn.activeSelf then
        LuaTweenUtils.DOKill(self.shareFx.FxRoot, false)
        self.shareFx:DestroyFx()
    end
end

