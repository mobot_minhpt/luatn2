-- Auto Generated!!
local CExpertTeamAnswerWnd = import "L10.UI.CExpertTeamAnswerWnd"
local CExpertTeamMgr = import "L10.Game.CExpertTeamMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local String = import "System.String"
local System = import "System"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CExpertTeamAnswerWnd.m_Init_CS2LuaHook = function (this) 
    UIEventListener.Get(this.backBtn).onClick = DelegateFactory.VoidDelegate(function (go) 
        this:Close()
    end)

    if CExpertTeamMgr.Inst.SaveQuestionData ~= nil then
        local _data = CExpertTeamMgr.Inst.SaveQuestionData
        if CommonDefs.DictContains(_data, typeof(String), "id") then
            this.qid = System.UInt64.Parse(ToStringWrap(CommonDefs.DictGetValue(_data, typeof(String), "id")))
        elseif CommonDefs.DictContains(_data, typeof(String), "qid") then
            this.qid = System.UInt64.Parse(ToStringWrap(CommonDefs.DictGetValue(_data, typeof(String), "qid")))
        end


        this.questionLabel.text = ToStringWrap(CommonDefs.DictGetValue(_data, typeof(String), "content"))

        CExpertTeamMgr.Inst:SetPlayerIcon(System.UInt64.Parse(ToStringWrap(CommonDefs.DictGetValue(_data, typeof(String), "role_id"))), ToStringWrap(CommonDefs.DictGetValue(_data, typeof(String), "avatar")), this.iconNode, nil, this.questionLabel.text, this.qid, 0)

        UIEventListener.Get(this.sendBtn).onClick = MakeDelegateFromCSFunction(this.SendAnswer, VoidDelegate, this)
    else
        this:Close()
    end
end
CExpertTeamAnswerWnd.m_SplitStringToS_CS2LuaHook = function (this, info) 
    local MaxByteNum = 255
    local result = CreateFromClass(MakeGenericClass(List, String))

    if CommonDefs.UTF8Encoding_GetByteCount(info) <= MaxByteNum then
        CommonDefs.ListAdd(result, typeof(String), info)
    else
        local index = 0
        local count = 1
        do
            local i = 0
            while i < CommonDefs.StringLength(info) - 1 do
                local str1 = CommonDefs.StringSubstring2(info, index, count)
                local str2 = CommonDefs.StringSubstring2(info, index, count + 1)
                count = count + 1
                if CommonDefs.UTF8Encoding_GetByteCount(str1) <= MaxByteNum and CommonDefs.UTF8Encoding_GetByteCount(str2) > MaxByteNum then
                    CommonDefs.ListAdd(result, typeof(String), str1)
                    index = i + 1
                    count = 1
                elseif i == CommonDefs.StringLength(info) - 2 then
                    CommonDefs.ListAdd(result, typeof(String), str2)
                end
                i = i + 1
            end
        end
    end

    return result
end
CExpertTeamAnswerWnd.m_SendAnswer_CS2LuaHook = function (this, go) 
    local answer = this.inputNode.value
    if System.String.IsNullOrEmpty(answer) then
        g_MessageMgr:ShowMessage("Content_Cannot_Be_Empty")
        return
    end

    if CommonDefs.StringLength(answer) > CExpertTeamAnswerWnd.MaxNum then
        g_MessageMgr:ShowMessage("Hongbao_Input_Limit", CExpertTeamAnswerWnd.MaxNum)
        return
    end

    local sendContent = CExpertTeamMgr.GetSendText(answer)

    if System.String.IsNullOrEmpty(sendContent) then
        g_MessageMgr:ShowMessage("Speech_Violation")
        return
    end

    Gac2Gas.PushExpertAnswerBegin()

    local desList = this:SplitStringToS(sendContent)
    do
        local i = 0
        while i < desList.Count do
            Gac2Gas.PushExpertAnswer(desList[i])
            i = i + 1
        end
    end
    Gac2Gas.PushExpertAnswerEnd(this.qid)
end
