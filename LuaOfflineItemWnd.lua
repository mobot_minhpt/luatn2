local MsgPackImpl = import "MsgPackImpl"
local MessageWndManager = import "L10.UI.MessageWndManager"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemMgr = import "L10.Game.CItemMgr"
local BoxCollider = import "UnityEngine.BoxCollider"
local CCommonSelector = import "L10.UI.CCommonSelector"
local GameplayItem_Setting = import "L10.Game.GameplayItem_Setting"


CLuaOfflineItemWnd = class()
RegistClassMember(CLuaOfflineItemWnd,"TitleLabel")
RegistClassMember(CLuaOfflineItemWnd,"DesLabel")
RegistClassMember(CLuaOfflineItemWnd,"SmallIconTexture")
RegistClassMember(CLuaOfflineItemWnd,"CloseButton")
RegistClassMember(CLuaOfflineItemWnd,"ConfirmButton")
RegistClassMember(CLuaOfflineItemWnd,"NameInput")
RegistClassMember(CLuaOfflineItemWnd,"PhoneInput")
RegistClassMember(CLuaOfflineItemWnd,"AddrInput")
RegistClassMember(CLuaOfflineItemWnd,"ShowArea")
RegistClassMember(CLuaOfflineItemWnd,"alreadyOpened")
RegistClassMember(CLuaOfflineItemWnd,"item")
RegistClassMember(CLuaOfflineItemWnd,"offlineItem")

RegistClassMember(CLuaOfflineItemWnd,"m_AddressRoot")
RegistClassMember(CLuaOfflineItemWnd,"m_ProvinceSelector")
RegistClassMember(CLuaOfflineItemWnd,"m_CitySelecotr")

RegistClassMember(CLuaOfflineItemWnd,"m_ProvinceName")
RegistClassMember(CLuaOfflineItemWnd,"m_CityName")

CLuaOfflineItemWnd.m_ItemId=nil

function CLuaOfflineItemWnd:Awake()
    self.TitleLabel = self.transform:Find("ShowArea/ScrollPanel/BG/TitleLabel"):GetComponent(typeof(UILabel))
    self.DesLabel = self.transform:Find("ShowArea/ScrollPanel/BG/DesLabel"):GetComponent(typeof(UILabel))
    self.SmallIconTexture = self.transform:Find("ShowArea/ScrollPanel/BG/IconRoot/IconBG/Texture"):GetComponent(typeof(CUITexture))
    self.CloseButton = self.transform:Find("ShowArea/ScrollPanel/BG/CloseButton").gameObject
    self.ConfirmButton = self.transform:Find("ShowArea/ScrollPanel/BG/ConfirmButton").gameObject
    self.NameInput = self.transform:Find("ShowArea/ScrollPanel/BG/NameInput"):GetComponent(typeof(UIInput))
    self.PhoneInput = self.transform:Find("ShowArea/ScrollPanel/BG/PhoneInput"):GetComponent(typeof(UIInput))
    self.AddrInput = self.transform:Find("ShowArea/ScrollPanel/BG/Address/AddrInput"):GetComponent(typeof(UIInput))
    self.ShowArea = self.transform:Find("ShowArea"):GetComponent(typeof(UIWidget))
    self.alreadyOpened = false
--self.item = ?
--self.offlineItem = ?
--self.jingnangInitPos = ?
    UIEventListener.Get(self.ConfirmButton).onClick=DelegateFactory.VoidDelegate(function(go)
        self:onConfirmClick(go)
    end)

    self.m_AddressRoot =  self.transform:Find("ShowArea/ScrollPanel/BG/Address").gameObject
    self.m_ProvinceSelector = self.transform:Find("ShowArea/ScrollPanel/BG/Address/Province"):GetComponent(typeof(CCommonSelector))
    self.m_CitySelecotr = self.transform:Find("ShowArea/ScrollPanel/BG/Address/City"):GetComponent(typeof(CCommonSelector))

end
function CLuaOfflineItemWnd:Init()
    self:InitProvinceSelector()
    self:showGift()
end

function CLuaOfflineItemWnd:InitProvinceSelector()
    local provinceList = CommonDefs.ListCreate(typeof(cs_string))
    local provinceTbl = {}
    local excludeProvinces = {}
    excludeProvinces[LocalString.GetString("台湾省")] = 1
    excludeProvinces[LocalString.GetString("香港特别行政区")] = 1
    excludeProvinces[LocalString.GetString("澳门特别行政区")] = 1
    Shimin_Province.ForeachKey(function (key) 
        local data = Shimin_Province.GetData(key)
        local name = data.Province
        if excludeProvinces[name] then
            return
        end
        CommonDefs.ListAdd_LuaCall(provinceList, name)
        provinceTbl[name] =  data.City
    end)
    self.m_ProvinceSelector:Init(provinceList,  DelegateFactory.Action_int(function ( index )
        local name = provinceList[index]
        self.m_ProvinceSelector:SetName(name)
        self.m_ProvinceName = name
        self:InitCitySelector( provinceTbl[name])
    end))
    self.m_ProvinceSelector:SetName(LocalString.GetString("省份"))
    self.m_ProvinceName = nil
    self:InitCitySelector(nil)
end

function CLuaOfflineItemWnd:InitCitySelector(cityArr)
    local cityList = CommonDefs.ListCreate(typeof(cs_string))
    if cityArr then
        for i=0,cityArr.Length-1 do
            local name = Shimin_City.GetData(cityArr[i]).City
            CommonDefs.ListAdd_LuaCall(cityList, name)
        end
    end
     self.m_CitySelecotr:Init(cityList,  DelegateFactory.Action_int(function ( index )
        local selectedName = cityList[index]
        self.m_CitySelecotr:SetName(selectedName)
        self.m_CityName = selectedName
        local addr = self.m_ProvinceName..self.m_CityName
        self.AddrInput.characterLimit = GameplayItem_Setting.GetData().MaxAddressLength - CommonDefs.StringLength(addr)
    end))
    self.m_CitySelecotr:SetName(LocalString.GetString("城市"))
    self.m_CityName = nil
end

-- Auto Generated!!
function CLuaOfflineItemWnd:showGift( )
    self.item = CItemMgr.Inst:GetById(CLuaOfflineItemWnd.m_ItemId)
    self.offlineItem = GameplayItem_OfflineItem.GetData(self.item.TemplateId)

    if self.offlineItem == nil then
        return
    end
    if (self.offlineItem.NeedAddr > 0 and self.offlineItem.NeedName > 0) or (self.offlineItem.NeedAddr > 0 and self.offlineItem.NeedPhone > 0) then
        self.ShowArea.height = 827-- COfflineItemWnd.height1
    else
        self.ShowArea.height = 717-- COfflineItemWnd.height2
    end

    self.SmallIconTexture:LoadMaterial(self.offlineItem.Icon)

    self.TitleLabel.text = self.offlineItem.Title
    self.DesLabel.text = self.offlineItem.Description

    local setting = GameplayItem_Setting.GetData()
    self.PhoneInput.gameObject:SetActive(self.offlineItem.NeedPhone > 0)
    self.PhoneInput.characterLimit = setting.MaxPhoneNumLength
    self.m_AddressRoot:SetActive(self.offlineItem.NeedAddr > 0)
    self.AddrInput.characterLimit = setting.MaxAddressLength
    self.NameInput.gameObject:SetActive(self.offlineItem.NeedPhone > 0)
    self.NameInput.characterLimit = setting.MaxNameLength

    self:ifNeedInput()--有问题，待确认
end

function CLuaOfflineItemWnd:ifNeedInput( )
    if self.item.Item.ExtraVarData.Data ~= nil then
        local itemInfo = MsgPackImpl.unpack(self.item.Item.ExtraVarData.Data)
        if itemInfo ~= nil then
            local addr = CommonDefs.DictGetValue_LuaCall(itemInfo,"addr")
            if self.offlineItem.NeedAddr > 0 then
                self.AddrInput.value = addr
                self.alreadyOpened = true
            end
            local name = CommonDefs.DictGetValue_LuaCall(itemInfo,"name")
            if self.offlineItem.NeedName > 0 then
                self.NameInput.value = name
                self.alreadyOpened = true
            end
            local phone = CommonDefs.DictGetValue_LuaCall(itemInfo,"phone")
            if self.offlineItem.NeedPhone > 0 then
                self.PhoneInput.value = phone
                self.alreadyOpened = true
            end
        end
    end
    self.PhoneInput:GetComponent(typeof(BoxCollider)).enabled = not self.alreadyOpened
    self.AddrInput:GetComponent(typeof(BoxCollider)).enabled = not self.alreadyOpened
    self.NameInput:GetComponent(typeof(BoxCollider)).enabled = not self.alreadyOpened
end
function CLuaOfflineItemWnd:onConfirmClick( go) 
    if self.alreadyOpened then
        CUIManager.CloseUI(CLuaUIResources.OfflineItemWnd)
    else
        local phone = ""
        local name = ""
        local addr = ""

        if self.offlineItem.NeedAddr > 0 then
            if self.m_ProvinceName == nil or self.m_CityName == nil then
                g_MessageMgr:ShowMessage("CUSTOM_STRING2",LocalString.GetString("需要选择省份和城市信息"))
                return
            end

            addr = self.AddrInput.value
            addr = self.m_ProvinceName..self.m_CityName..StringTrim(addr)

            local minAddressLen = GameplayItem_Setting.GetData().MinAddressLength
            local maxAddressLen = GameplayItem_Setting.GetData().MaxAddressLength
            if CommonDefs.StringLength(addr) <= minAddressLen then
                g_MessageMgr:ShowMessage("CUSTOM_STRING2",SafeStringFormat3(LocalString.GetString("输入地址总信息需大于%d字"), minAddressLen))
                return
            elseif CommonDefs.StringLength(addr) > maxAddressLen then
                g_MessageMgr:ShowMessage("OfflineItem_Address_Too_Long")
                return
            end
            if not addr or addr == "" then
                g_MessageMgr:ShowMessage("OfflineItem_Need_Addr")
                return 
            end
            
        end
        
        if self.offlineItem.NeedPhone > 0 then
            phone = self.PhoneInput.value
            phone = StringTrim(phone)
            if string.match(phone,"[1]%d%d%d%d%d%d%d%d%d%d") ~= phone then
                g_MessageMgr:ShowMessage("OfflineItem_Invalid_Phone_Number")
                return
            end
        end
        if self.offlineItem.NeedName > 0 then
            name = self.NameInput.value
            name = StringTrim(name)
            if not name or name == "" then
                g_MessageMgr:ShowMessage("NAME_CANNOT_BE_EMPTY")
                return
            end
        end
        local Pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, self.item.Id)

        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Record_Address_Confirm"), DelegateFactory.Action(function () 
            Gac2Gas.PlayerInputContactInfoByUseItem(EnumItemPlace_lua.Bag, Pos, phone, name, addr)
        end), nil, nil, nil, false)
    end
end

function CLuaOfflineItemWnd:OnEnable()
	g_ScriptEvent:AddListener("PlayerInputContactInfoByUseItemSuccess", self, "OnPlayerInputContactInfoByUseItemSuccess")
end

function CLuaOfflineItemWnd:OnDisable()
	g_ScriptEvent:AddListener("PlayerInputContactInfoByUseItemSuccess", self, "OnPlayerInputContactInfoByUseItemSuccess")
end

function CLuaOfflineItemWnd:OnPlayerInputContactInfoByUseItemSuccess()
    CUIManager.CloseUI(CLuaUIResources.OfflineItemWnd)
end
