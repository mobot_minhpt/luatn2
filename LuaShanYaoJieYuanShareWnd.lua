local DelegateFactory              = import "DelegateFactory"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local EnumWarnFXType               = import "L10.Game.EnumWarnFXType"
local CEffectMgr                   = import "L10.Game.CEffectMgr"
local Screen                       = import "UnityEngine.Screen"
local TweenAlpha                   = import "TweenAlpha"

LuaShanYaoJieYuanShareWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end
RegistClassMember(LuaShanYaoJieYuanShareWnd, "buddyName")
RegistClassMember(LuaShanYaoJieYuanShareWnd, "model")
RegistClassMember(LuaShanYaoJieYuanShareWnd, "word")
RegistClassMember(LuaShanYaoJieYuanShareWnd, "bubble")
RegistClassMember(LuaShanYaoJieYuanShareWnd, "storyButton")

RegistClassMember(LuaShanYaoJieYuanShareWnd, "modelIdentifier")
RegistClassMember(LuaShanYaoJieYuanShareWnd, "modelLoader")
RegistClassMember(LuaShanYaoJieYuanShareWnd, "bubbleTick")
RegistClassMember(LuaShanYaoJieYuanShareWnd, "ro")
RegistClassMember(LuaShanYaoJieYuanShareWnd, "rotation")

function LuaShanYaoJieYuanShareWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
    self:InitEventListener()
    self:InitModelLoader()
end

function LuaShanYaoJieYuanShareWnd:InitUIComponents()
    local anchor = self.transform:Find("Anchor")
    self.buddyName = anchor:Find("Model/Name"):GetComponent(typeof(UILabel))
    self.model = anchor:Find("Model/Texture"):GetComponent(typeof(CUITexture))
    self.word = anchor:Find("Model/Word"):GetComponent(typeof(UILabel))
    self.bubble = anchor:Find("Model/Word/Bubble"):GetComponent(typeof(UIWidget))
    self.storyButton = anchor:Find("StoryButton").gameObject
end

function LuaShanYaoJieYuanShareWnd:InitEventListener()
    UIEventListener.Get(self.storyButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnStoryButtonClick()
	end)

    UIEventListener.Get(self.model.gameObject).onDrag = DelegateFactory.VectorDelegate(function (go, delta)
        self:OnModelDrag(go, delta)
    end)
end

function LuaShanYaoJieYuanShareWnd:Init()
    self:UpdateModel()
    self:UpdateBubble()
    self.buddyName.text = XinBaiLianDong_YaoBan.GetData(LuaXinBaiLianDongMgr.SYJYInfo.shareBuddyId).UnlockName
end

function LuaShanYaoJieYuanShareWnd:InitModelLoader()
    self.modelLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        ro.Scale = 1
        self.ro = ro
        self:LoadModel(ro)
    end)
    self.modelIdentifier = SafeStringFormat3("__%s__", tostring(self.model.gameObject:GetInstanceID()))
end

-- 载入模型
function LuaShanYaoJieYuanShareWnd:LoadModel(ro)
    local modelPath = XinBaiLianDong_YaoBan.GetData(LuaXinBaiLianDongMgr.SYJYInfo.shareBuddyId).Model
    ro:LoadMain(modelPath, nil, false, false, false)
end

function LuaShanYaoJieYuanShareWnd:UpdateModel()
    self.model.mainTexture = CUIManager.CreateModelTexture(self.modelIdentifier, self.modelLoader, 180, 0.05, -1, 4.66, false, true, 2, true, false)
    self:SetRotation(180)

    -- 添加特效
    local fxId = XinBaiLianDong_YaoBan.GetData(LuaXinBaiLianDongMgr.SYJYInfo.shareBuddyId).Fx
    local fx = CEffectMgr.Inst:AddObjectFX(fxId, self.ro, 0, 2, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero)
    self.ro:AddFX("SelfFx", fx)
end

-- 更新气泡显示
function LuaShanYaoJieYuanShareWnd:UpdateBubble()
    self.word.text = XinBaiLianDong_YaoBan.GetData(LuaXinBaiLianDongMgr.SYJYInfo.shareBuddyId).UnlockDialog
    self.word.gameObject:SetActive(true)

    self.word:UpdateNGUIText()
    NGUIText.regionWidth = 1000000
    local size = NGUIText.CalculatePrintedSize(self.word.text)
    self.word.width = size.x > 453 and 453 or math.ceil(size.x)
    self.bubble:ResetAndUpdateAnchors()

    local tweenAlpha = self.word.transform:GetComponent(typeof(TweenAlpha))
    tweenAlpha.tweenFactor = 0
    tweenAlpha:PlayForward()

    self:ClearTick()
    self.bubbleTick = RegisterTickOnce(function()
        tweenAlpha.tweenFactor = 1
        tweenAlpha:PlayReverse()
    end, 10000)
end

-- 设置模型旋转
function LuaShanYaoJieYuanShareWnd:SetRotation(val)
    self.rotation = val
    CUIManager.SetModelRotation(self.modelIdentifier, val)
end

function LuaShanYaoJieYuanShareWnd:ClearTick()
    if self.bubbleTick then
        UnRegisterTick(self.bubbleTick)
        self.bubbleTick = nil
    end
end

function LuaShanYaoJieYuanShareWnd:OnDestroy()
    self:ClearTick()
    CUIManager.DestroyModelTexture(self.modelIdentifier)
end


--@region UIEvent

function LuaShanYaoJieYuanShareWnd:OnStoryButtonClick()
    local msg = XinBaiLianDong_YaoBan.GetData(LuaXinBaiLianDongMgr.SYJYInfo.shareBuddyId).StoryMsg
    LuaXinBaiLianDongMgr:ShowShanYaoJieYuanStoryWnd(msg)
end

function LuaShanYaoJieYuanShareWnd:OnModelDrag(go, delta)
    local rot = -delta.x / Screen.width * 360
    self:SetRotation(self.rotation + rot)
end

--@endregion UIEvent
