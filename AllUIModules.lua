-- print("collectgarbage1",collectgarbage("count"))
function IsRunningServerCode()
    return false
end

require "game/instrument/InstrumentMgr"

require "ui/kiteplay/LuaKitePlayMgr"
require "ui/zhounianqing/LuaZhouNianQingMgr"
require "ui/worldevent/LuaWorldEventMgr"
require "ui/bingqipu/LuaBingQiPuMgr"
require "ui/shenbing/LuaShenbingMgr"
require "ui/equip/identifyskill/LuaEquipIdentifySkillMgr"
require "ui/common/LuaItemSingleColumnListMgr"
require "ui/im/LuaIMMgr"
require "ui/quanminpk/LuaQMPKMgr"
require "ui/schedule/LuaScheduleMgr"
require "ui/guild/LuaGuildMgr"
require "ui/carnivalcollect/LuaCarnivalCollectMgr"
require "ui/im/LuaIMMgr"
require "ui/guimenguan/LuaGuiMenGuanMgr"
require "ui/lingshou/LuaLingShouOtherMgr"
require "ui/common/LuaCommonCharacterDescMgr"
require "ui/zhujuejuqing/LuaZhuJueJuQingMgr"
require "ui/common/LuaCommonAnnounceMgr"
require "ui/gqjc/LuaGuoQingJiaoChangMgr"
require "ui/skill/LuaLifeSkillMgr"
require "ui/baby/LuaBabyMgr"
require "ui/guildleague/LuaGuildLeagueMgr"
require "ui/guildleague/LuaGuildLeagueCrossMgr"
require "ui/guide/LuaGuideMgr"
require "ui/main/minimap/LuaMiniMapMgr"
require "ui/bagualu/LuaBaGuaLuMgr"
require "ui/christmas/LuaChristmasMgr"
require "ui/christmas/LuaChristmasTreeAddItemMgr"
require "ui/debatewithnpc/LuaDebateWithNpcMgr"
require "ui/yuanxiao/LuaYuanxiaoMgr"
require "ui/house/LuaHouseMgr"
require "ui/house/LuaHouseWoodPileMgr"
require "ui/wedding/LuaWeddingMgr"
require "ui/fabao/LuaTalismanMgr"
require "ui/pvp/LuaPVPMgr"
require "ui/zhusidong/LuaQingHongFlyMgr"
require "ui/cakecutting/LuaCakeCuttingMgr"
require "ui/xianzhi/LuaXianzhiMgr"
require "ui/wuyi/LuaWuYiMgr"
require "ui/screencapture/LuaScreenCaptureMgr"
require "ui/sceneinteractive/LuaSceneInteractiveMgr"
require "ui/CLuaItemMgr"
require "ui/sanxinggameplay/LuaSanxingGamePlayMgr"
require "ui/house/LuaClientFurnitureMgr"
require "ui/horserace/LuaNewHorseRaceMgr"
require "ui/fruitparty/LuaFruitPartyMgr"


--rpc
require "ui/wuyi2020/LuaDiYuShiKongMgr"
require "ui/carnivalcaipiao2019/LuaCarnivalCaiPiao2019Mgr"
require "ui/huiliu/LuaHuiLiuMgr"
require "ui/charactercard/LuaCharacterCardMgr"
require "ui/snowball2019/LuaSnowBallMgr"
require "ui/house/spokesman/LuaSpokesmanMgr"
require "ui/halloween2020/LuaHalloween2020Mgr"
require "ui/zongmen/LuaZongMenMgr"
require "ui/zongmen/LuaZhuoYaoMgr"
require "ui/haoyixing/LuaHaoYiXingMgr"

--addlistener
require "ui/LuaEntryUIMgr"
require "ui/main/activity/LuaActivityZhiBoMgr"
require "ui/personalspace/LuaPersonalSpaceMgr"
require "ui/liuyi/LuaLiuYiMgr"
require "ui/appearance/LuaAppearancePreviewMgr"
require "ui/musicbox/LuaMusicBoxMgr"
require "ui/starbiwu/LuaStarBiwuMgr"
require "ui/LuaSceneLvJingMgr"
require "ui/zhaixing/LuaZhaiXingMgr"
require "ui/shuimanjinshan/LuaShuiManJinShanMgr"


--hook
require "ui/cjb/LuaCJBMakeWnd"
require "ui/input/LuaWindowsShortCutKey"
require "ui/house/LuaPlantWnd"
require "ui/fashion/CClothespressEmptyTemplate"
require "ui/shopmall/CFashionPreviewTextureLoader"
require "ui/house/LuaHouseCommunityMasterView"
require "ui/main/LuaTopAndRightMenuWnd"
require "ui/main/LuaHeadInfoItem"
require "ui/main/LuaCountdownView"
require "ui/webbrowser/LuaWebBrowserMgr"
require "ui/common/LuaItemAccessListMgr"
require "ui/equip/LuaEquipmentSuitInfoMgr"
require "ui/welfare/LuaWelfareMgr"
require "ui/guild/LuaBattleSituationMgr"
require "ui/npc/CLuaNPCHeadInfoItem"
require "ui/task/LuaTaskListView"
require "ui/common/LuaColorUtils"
require 'ui/house/CProductCompositeWnd'
require 'ui/fightingspirit/LuaFightingSpiritMgr'
require 'ui/task/LuaTeamListBoard'
require 'ui/im/LuaGroupIMMainView'
require 'ui/im/LuaGroupIMGroupItem'
require 'ui/im/LuaGroupIMContentView'

--def
--def
require "ui/personalspace/LuaPersonalSpaceDefs"
--local var
require "ui/huabi/LuaHuaBiConfirmWnd"


--fields
require "ui/mapi/LuaTianQiMysticlShopWnd"--local var

--继承
require "ui/lingshou/wash/LuaLingShouBabyBaseWashWnd"
require "ui/lingshou/wash/LuaLingShouBabyAutoWashWnd"
require "ui/lingshou/wash/LuaLingShouBabyHandWashWnd"
require "ui/zhushabi/LuaZhuShaBiXiLianWnd"
require "ui/zhushabi/LuaZhuShaBiAutoXiLianWnd"

require "ui/house/spokesman/LuaSpokesmanHomeMgr"
require 'ui/qiugou/LuaQiugouMgr'
require "ui/personalspace/LuaWishMgr"
require 'ui/baozhuplay/LuaBaoZhuPlayMgr'
require "ui/offworldpass/LuaOffWorldPassMgr"
require "ui/qingming2021/LuaQingMing2021Mgr"
require "ui/appearance/LuaFashionMgr"

require "ui/LuaHSVTestWnd"
require "ui/common/LuaSceneRemainTimeLabel"

-- 2021周年庆
require "game/zhounianqing2021/LuaZhouNianQing2021Mgr"

require "game/fuxiani/LuaFuxiAniMgr"

-- 2021暑假
require "game/shujia2021/LuaShuJia2021Mgr"

require "ui/common/LuaCommonPlayerListMgr"

-- 2021七夕
require "game/qixi2021/LuaQiXi2021Mgr"

-- 2021中元节
require "ui/Zhongyuanjie2021/LuaHMLZMgr"
require "ui/Zhongyuanjie2021/LuaZuiMengLuMgr"

--证物录
require "ui/zhengwulu/LuaZhengWuLuMgr"
require "ui/house/competition/LuaHouseCompetitionMgr"

-- 天降宝箱
require "game/item/LuaTianJiangBaoXiangMgr"

require "game/im/LuaNPCChatMgr"
require "ui/house/pool/LuaPoolMgr"
require "ui/house/LuaHouseTerrainMgr"
require "ui/common/LuaCommonItemSelectMgr"
--中元节
require "game/festival/ZhongYuanJie2022Mgr"

--跨服比武
require "ui/biwucross/LuaBiWuCrossMgr"

-- 2022万圣节
require "ui/halloween2022/LuaHalloween2022Mgr"

-- 新节日+日程红点机制
require "ui/schedule/LuaActivityRedDotMgr"

require "ui/hongbao/LuaHongBaoMgr"

require "ui/main/LuaCommonGamePlayTaskView"

require "engine/physics/LuaPhysicsSyncMgr"

require "ui/welfare/LuaExchangeCodeWnd"

require "ui/quiz/CTaskQuizWnd"
require "ui/quiz/CBaoLingDanQAWnd"
require "ui/main/LuaTeamMgr"
require "ui/main/activity/LuaActivityAlertMgr"
require "ui/main/CYaoQianShuWnd"

--VN SDK 获得国家代码/ 谷歌翻译/ 币种价格的lua中转
require "ui/LuaSEASdkMgr"


__ui_cache = {
	g_sprites = "ui/sprites",
	g_UIFxPaths = "ui/UIFxPaths",
	CLuaEquipMgr = "ui/equip/LuaEquipMgr",
	CLuaPlayerCapacityMgr = "ui/main/mainplayer/LuaPlayerCapacityMgr",
	EnumDouDiZhuType = "ui/doudizhu/LuaDouDiZhuMgr",
	EnumGuildDouDiZhuFightStage = "ui/doudizhu/LuaDouDiZhuMgr",
	CLuaDouDiZhuMgr = "ui/doudizhu/LuaDouDiZhuMgr",
	CLuaDouDiZhuCardMgr = "ui/doudizhu/LuaDouDiZhuMgr",
	LuaPaintPicMgr = "ui/paintpic/LuaPaintPicMgr",
	CLuaNPCShopInfoMgr = "ui/npcshop/LuaNPCShopMgr",

	LuaShengXiaoCardMgr = "ui/chunjie2022/LuaShengXiaoCardMgr",
	LuaShengXiaoCardPlayerView = "ui/chunjie2022/LuaShengXiaoCardPlayerView",
	LuaShengXiaoCard = "ui/chunjie2022/LuaShengXiaoCard",

	CLuaShareMgr = "ui/share/LuaShareMgr",
	CLuaLingShouMgr = "ui/lingshou/LuaLingShouMgr",
	CLuaBWDHGameVideoMgr = "ui/biwudahui/LuaBWDHGameVideoMgr",
	CLuaLianLianKanMgr = "ui/lianliankan2018/LuaLianLianKanMgr",
	EnumQYSJTaskStatus = "ui/valentine2019/LuaValentine2019Mgr",
	LuaValentine2019Mgr = "ui/valentine2019/LuaValentine2019Mgr",
	EnumCangbaogeStatus = "ui/treasurehouse/LuaTreasureHouseMgr",
	LuaTreasureHouseMgr = "ui/treasurehouse/LuaTreasureHouseMgr",
	LuaQingming2019Mgr = "ui/qingming2019/LuaQingming2019Mgr",
	EnumMengHuaLuGridColor = "ui/menghualu/LuaMengHuaLuMgr",
	LuaMengHuaLuMgr = "ui/menghualu/LuaMengHuaLuMgr",
	LuaCopySceneChosenMgr = "ui/copyScene/LuaCopySceneChosenMgr",
	LuaAutoBaptizeNecessaryConditionMgr = "ui/equip/xilian/LuaAutoBaptizeNecessaryConditionMgr",
	LuaBaoWeiNanGuaMgr = "ui/baoweinangua/LuaBaoWeiNanGuaMgr",
	LuaTaoQuanMgr = "ui/youleyuan/LuaTaoQuanMgr",
	CLuaDuanWuDaZuoZhanMgr = "ui/duanwudazuozhan/LuaDuanWuDaZuoZhanMgr",
	CLuaHouseFishMgr = "ui/house/LuaHouseFishMgr",

	--game module
	DuanWu2020Mgr = "game/festival/DuanWu2020Mgr",
	LuaYuanXiao2020Mgr = "game/festival/YuanXiao2020Mgr",
	LuaPlayerReportMgr = "game/report/PlayerReportMgr",
	g_PopupMenuMgr = "game/menu/PopupMenuMgr",
	LuaShiTuTrainingHandbookMgr = "game/shitu/LuaShiTuTrainingHandbookMgr",
	luaGuildVoteMgr = "game/luaGuildVoteMgr",
	HanJia2020Mgr = "game/festival/HanJia2020Mgr",
	LuaSchoolTaskMgr = "game/schooltask/SchoolTaskMgr",
	EnumNewbieSchoolShiLianType = "game/schooltask/SchoolTaskMgr",
	EnumNewbieSchoolCourseType = "game/schooltask/SchoolTaskMgr",
	LuaSimpleFlagMgr = "game/simpleflag/LuaSimpleFlagMgr",
	LuaKeJuMgr = "game/keju/LuaKeJuMgr",
	LuaAccountTransferMgr = "game/account/LuaAccountTransferMgr",
	LuaNationalDayMgr = "game/festival/NationalDayMgr",
	LuaAchievementMgr = "game/achievement/AchievementMgr",
	LuaLiangHaoMgr = "game/lianghao/LiangHaoMgr",
	LuaJingLingMgr = "game/jingling/JingLingMgr",
	LuaQTEMgr = "game/qte/LuaQTEMgr",
	ZhongYuanJie2020Mgr = "game/festival/ZhongYuanJie2020Mgr",
	
	--命令模式 撤销重做
	LuaCommandMgr = "game/common/LuaCommandMgr",
	--end

	--havefields
	CLuaMultiQuizWnd = "ui/quiz/LuaMultiQuizWnd",
	CLuaQiXiDaTiWnd = "ui/qixi/LuaQiXiDaTiWnd",
	LuaBingQiPuWnd = "ui/bingqipu/LuaBingQiPuWnd",
	LuaBQPCommitEquipWnd = "ui/bingqipu/LuaBQPCommitEquipWnd",
	LuaBQPResultWnd = "ui/bingqipu/LuaBQPResultWnd",
	LuaBQPSponsorListWnd = "ui/bingqipu/LuaBQPSponsorListWnd",
	CLuaDouDiZhuGuildChooseWnd = "ui/doudizhu/LuaDouDiZhuGuildChooseWnd",
	CLuaDouDiZhuGuildRankWnd = "ui/doudizhu/LuaDouDiZhuGuildRankWnd",
	CLuaFightingSpiritFavorRankWnd = "ui/fightingspirit/LuaFightingSpiritFavorRankWnd",
	CLuaFightingSpiritFavorWnd = "ui/fightingspirit/LuaFightingSpiritFavorWnd",
	CLuaFightingSpiritFutiInfoTip = "ui/fightingspirit/LuaFightingSpiritFutiInfoTip",
	LuaHandkerchiefFlyWnd = "ui/HandkerchiefFly/LuaHandkerchiefFlyWnd",
	CLuaHuluwaZhaoHuanWnd = "ui/huluwa/LuaHuluwaZhaoHuanWnd",
	CLuaHuluwaLyricCtrl = "ui/huluwa/LuaHuluwaLyricCtrl",
	CLuaHuluwaDropCtrl = "ui/huluwa/LuaHuluwaDropCtrl",
	CLuaInjusticeWnd = "ui/kiteplay/LuaInjusticeWnd",
	CLuaKitePlayOpenWnd = "ui/kiteplay/LuaKitePlayOpenWnd",
	CLuaKiteRankWnd = "ui/kiteplay/LuaKitePlayRankWnd",
	CLuaKitePlayResultWnd = "ui/kiteplay/LuaKitePlayResultWnd",
	CLuaKitePlayWnd = "ui/kiteplay/LuaKitePlayWnd",
	LuaShenbingDuliniangWnd = "ui/shenbing/LuaShenbingDuliniangWnd",
	LuaShenbingEnterWnd = "ui/shenbing/LuaShenbingEnterWnd",
	LuaShenBingExchangeQyWnd = "ui/shenbing/LuaShenBingExchangeQyWnd",
	CLuaSpriteFestivalPintuWnd = "ui/springfestival/LuaSpringFestivalPintuWnd",
	LuaTreasureHouseWnd = "ui/treasurehouse/LuaTreasureHouseWnd",
	CLuaValentineVoiceLoveWnd = "ui/valentine/LuaValentineVoiceLoveWnd",
	LuaWeekendFightInfoWnd = "ui/weekendfight/LuaWeekendFightInfoWnd",
	CLuaWeekendFightOpenWnd = "ui/weekendfight/LuaWeekendFightOpenWnd",
	LuaWorldEventDrawLineWnd = "ui/worldevent/LuaWorldEventDrawLineWnd",
	LuaDrawLineWnd = "ui/worldevent/LuaDrawLineWnd",
	LuaWorldEventHisWnd = "ui/worldevent/LuaWorldEventHisWnd",
	LuaWorldEventHorShowWnd = "ui/worldevent/LuaWorldEventHorShowWnd",
	LuaWorldEventPintuWnd = "ui/worldevent/LuaWorldEventPintuWnd",
	LuaWorldEventShareWnd = "ui/worldevent/LuaWorldEventShareWnd",
	LuaWorldEventVerShowWnd = "ui/worldevent/LuaWorldEventVerShowWnd",
	CLuaZhouNianQingPintuWnd = "ui/zhounianqing/LuaZhouNianQingPintuWnd",
	CLuaCommonPintuWnd = "ui/mihan/LuaCommonPintuWnd",
	CLuaFenshuiWnd = "ui/fenshui/LuaFenshuiWnd",
	CLuaDramaEntranceWnd = "ui/drama/LuaDramaEntranceWnd",
	CLuaJuqingPicWnd = "ui/juqing/LuaJuqingPicWnd",
	CLuaMiHanWnd = "ui/mihan/LuaMiHanWnd",
	CLuaMapiMysticlShopWnd = "ui/mapi/LuaMapiMysticlShopWnd",
	CLuaMapiMysticlShop_Entry = "ui/mapi/LuaMapiMysticlShop_Entry",
	--end


	CLuaActivityZhiBoView = "ui/main/activity/LuaActivityZhiBoView",
	LuaCurrentTarget = "ui/main/base/LuaCurrentTarget",
	CLuaBWDHGameVideoMemberWnd = "ui/biwudahui/LuaBWDHGameVideoMemberWnd",
	CLuaBWDHGameVideoWnd = "ui/biwudahui/LuaBWDHGameVideoWnd",
	CLuaCarnivalLotteryWnd = "ui/carnivallottery/LuaCarnivalLotteryWnd",
	CLuaTowerDefenseEditWnd = "ui/duanwuwudu/LuaTowerDefenseEditWnd",
	CLuaTowerDefenseFurniturePlaceHint = "ui/duanwuwudu/LuaTowerDefenseFurniturePlaceHint",
	CLuaTowerDefensePopupMenu = "ui/duanwuwudu/LuaTowerDefensePopupMenu",
	CLuaTowerDefenseResultWnd = "ui/duanwuwudu/LuaTowerDefenseResultWnd",
	CLuaTowerDefenseStateWnd = "ui/duanwuwudu/LuaTowerDefenseStateWnd",
	CLuaTowerDefenseStatusWnd = "ui/duanwuwudu/LuaTowerDefenseStatusWnd",
	CLuaGuanNingBattleDataWnd = "ui/guanning/LuaGuanNingBattleDataWnd",
	CLuaGuanNingFriendRankView = "ui/guanning/LuaGuanNingFriendRankView",
	CLuaGuanNingHistoryBattleDataView = "ui/guanning/LuaGuanNingHistoryBattleDataView",
	CLuaGuanNingLastBattleDataView = "ui/guanning/LuaGuanNingLastBattleDataView",
	CLuaGuanNingPlayerAchievementWnd = "ui/guanning/LuaGuanNingPlayerAchievementWnd",
	CLuaGuanNingGroupWnd = "ui/guanning/LuaGuanNingGroupWnd",
	CLuaGuanNingGroupInfoTip = "ui/guanning/LuaGuanNingGroupInfoTip",
	LuaGuanNingCommandWnd = "ui/guanning/LuaGuanNingCommandWnd",

	CLuaHuoGuoInfoWnd = "ui/huoguo/LuaHuoGuoInfoWnd",
	CLuaLingShouBabyInjectExpWnd = "ui/lingshou/baby/LuaLingShouBabyInjectExpWnd",
	CLuaLingShouBabyPropertyDetailView = "ui/lingshou/baby/LuaLingShouBabyPropertyDetailView",
	CLuaLingShouBabySkillDetailView = "ui/lingshou/baby/LuaLingShouBabySkillDetailView",
	CLuaLingShouBabyUseItem = "ui/lingshou/baby/LuaLingShouBabyUseItem",
	CLuaLingShouBabyUseWnd = "ui/lingshou/baby/LuaLingShouBabyUseWnd",
	CLuaLingShouBabyWnd = "ui/lingshou/baby/LuaLingShouBabyWnd",
	CLuaLingShouOtherBabyWnd = "ui/lingshou/other/LuaLingShouOtherBabyWnd",
	CLuaLingShouBabyWashResultView = "ui/lingshou/wash/LuaLingShouBabyWashResultView",

	CLuaLingShouBabySkillSlot = "ui/lingshou/LuaLingShouBabySkillSlot",
	CLuaLingShouMarryInfoWnd = "ui/lingshou/LuaLingShouMarryInfoWnd",
	CLuaLingShouZhunHunZhengWnd = "ui/lingshou/LuaLingShouZhunHunZhengWnd",
	CLuaLingShouZhandouliWnd = "ui/lingshou/LuaLingShouZhandouliWnd",
	CLuaExpWnd_PopupMenu = "ui/main/base/LuaExpWnd_PopupMenu",
	CLuaOtherScoreWnd = "ui/main/mainplayer/LuaOtherScoreWnd",
	CLuaPlayerCapacityView = "ui/main/mainplayer/LuaPlayerCapacityView",
	CLuaPlayerCapacityRecommendWnd = "ui/main/mainplayer/LuaPlayerCapacityRecommendWnd",
	CLuaPlayerScoreView = "ui/main/mainplayer/LuaPlayerScoreView",
	CLuaPlayerAchievementView = "ui/main/mainplayer/LuaPlayerAchievementView",
	CLuaZhuXianAchievementWnd = "ui/main/mainplayer/LuaZhuXianAchievementWnd",
	CLuaBottomRightActionBoard = "ui/main/LuaBottomRightActionBoard",
	LuaJuQingPinTuWnd = "ui/mihan/LuaJuQingPinTuWnd",
	CLuaPlayerShopBatchBuyWnd = "ui/playershop/LuaPlayerShopBatchBuyWnd",
	CLuaQMPKGameVideoWnd = "ui/quanminpk/LuaQMPKGameVideoWnd",
	CLuaShakeDicesWnd = "ui/shakedices/LuaShakeDicesWnd",
	CLuaShare2PersonalSpaceWnd = "ui/share/LuaShare2PersonalSpaceWnd",
	CLuaCommonShare2PersonalSpaceWnd = "ui/share/LuaCommonShare2PersonalSpaceWnd",
	CLuaShareMessageWnd = "ui/share/CLuaShareMessageWnd",
	CLuaShareBox = "ui/share/LuaShareBox",
	CLuaTeamGroupDetailWnd = "ui/teamgroup/LuaTeamGroupDetailWnd",
	CLuaTeamGroupPlayerInfoItem = "ui/teamgroup/LuaTeamGroupPlayerInfoItem",
	CLuaTeamGroupTeamInfo = "ui/teamgroup/LuaTeamGroupTeamInfo",
	CLuaTeamGroupWnd = "ui/teamgroup/LuaTeamGroupWnd",
	CLuaTeamManagerWnd = "ui/teamgroup/LuaTeamManagerWnd",
	CLuaJieZhiKeZiWnd = "ui/wedding/LuaJieZhiKeZiWnd",
	CLuaFashionGiftDetailWnd = "ui/welfare/LuaFashionGiftDetailWnd",
	LuaSignInWnd = "ui/welfare/LuaSignInWnd",
	LuaMainWelfareWnd = "ui/welfare/LuaMainWelfareWnd",
	LuaWelfareGiftWnd = "ui/welfare/LuaWelfareGiftWnd",
	LuaFirstChargeGiftWindow = "ui/welfare/LuaFirstChargeGiftWindow",
	LuaWelfareBonusWindow = "ui/welfare/LuaWelfareBonusWindow",
	CLuaLingShouMarriageMatchInputWnd = "ui/lingshou/match/LuaLingShouMarriageMatchInputWnd",
	CLuaLingShouMarriageMatchWnd = "ui/lingshou/match/LuaLingShouMarriageMatchWnd",
	CLuaLingShouMarriageMatchSelectWnd = "ui/lingshou/match/LuaLingShouMarriageMatchSelectWnd",
	CLuaSelfieAdditiveItem = "ui/selfie/LuaSelfieAdditiveItem",
	CLuaHuluwaDropEffect = "ui/huluwa/LuaHuluwaDropEffect",
	CLuaMapiFanyuInfoWnd = "ui/zuoqi/LuaMapiFanyuInfoWnd",
	CLuaFBDHeChengWnd = "ui/qiankundai/LuaFBDHeChengWnd",
	CLuaSelectFBDHeChengEquip = "ui/qiankundai/LuaSelectFBDHeChengEquip",
	CLuaSelectSYZYEquip = "ui/qiankundai/LuaSelectSYZYEquip",
	CLuaShouYuanZhenYingWnd = "ui/qiankundai/LuaShouYuanZhenYingWnd",
	CLuaXingGuanCostItemWnd = "ui/feisheng/LuaXingGuanCostItemWnd",
	CLuaLLWChooseWnd = "ui/liaoluowan/LuaLLWChooseWnd",
	CLuaGhostEquipWordDisassembleWnd = "ui/equip/ghost/LuaGhostEquipWordDisassembleWnd",
	CLuaGhostEquipWordComposeWnd = "ui/equip/ghost/LuaGhostEquipWordComposeWnd",
	CLuaEquipWordBaptizeWnd = "ui/equip/xilian/LuaEquipWordBaptizeWnd",
	LuaEquipChouBingWnd = "ui/equip/xilian/LuaEquipChouBingWnd",
	CLuaEquipBaptizeCost = "ui/equip/xilian/LuaEquipBaptizeCost",
	CLuaLingShouConsumeBabyExpItemWnd = "ui/lingshou/LuaLingShouConsumeBabyExpItemWnd",
	CLuaFeiShengRuleConfirmWnd = "ui/feisheng/LuaFeiShengRuleConfirmWnd",
	CLuaFurnitureExchangeWnd = "ui/house/LuaFurnitureExchangeWnd",
	CLuaHouseIntroWnd = "ui/house/LuaHouseIntroWnd",
	CLuaFurnitureHeadInfoWnd = "ui/house/LuaFurnitureHeadInfoWnd",
	CLuaHouseStuffQuickMakeWnd = "ui/skill/lifeskill/LuaHouseStuffQuickMakeWnd",
	CHouseCompetitionVictoryWnd = "ui/house/LuaHouseCompetitionVictoryWnd",
	CLuaGuanNingTaskView = "ui/guanning/LuaGuanNingTaskView",
	CLuaGuanNingWuShenTaskWnd = "ui/guanning/wushen/LuaGuanNingWuShenTaskWnd",
	LuaPresentGivenWnd = "ui/present/LuaPresentGivenWnd",
	LuaPresentReceivedWnd = "ui/present/LuaPresentReceivedWnd",
	LuaProfessionTransferRequirementWnd = "ui/professiontransfer/LuaProfessionTransferRequirementWnd",
	LuaProfessionTransferRuleWnd = "ui/professiontransfer/LuaProfessionTransferRuleWnd",
	LuaProfessionTransferWnd = "ui/professiontransfer/LuaProfessionTransferWnd",
	LuaJueJiExchangeWnd = "ui/professiontransfer/LuaJueJiExchangeWnd",
	LuaPreciousItemShareWnd = "ui/share/LuaPreciousItemShareWnd",
	LuaEquipBaptizeShareWnd = "ui/share/LuaEquipBaptizeShareWnd",
	CLuaDouDiZhuCard = "ui/doudizhu/LuaDouDiZhuCard",
	CLuaDouDiZhuPlayerView = "ui/doudizhu/LuaDouDiZhuPlayerView",
	CLuaDouDiZhuWnd = "ui/doudizhu/LuaDouDiZhuWnd",
	CLuaDouDiZhuRankWnd = "ui/doudizhu/LuaDouDiZhuRankWnd",
	CLuaDouDiZhuHouseResultWnd = "ui/doudizhu/LuaDouDiZhuHouseResultWnd",
	CLuaDouDiZhuGuildResultWnd = "ui/doudizhu/LuaDouDiZhuGuildResultWnd",
	CLuaDouDiZhuQuickChatWnd = "ui/doudizhu/LuaDouDiZhuQuickChatWnd",
	CLuaDouDiZhuPickPlayerToWatchWnd = "ui/doudizhu/LuaDouDiZhuPickPlayerToWatchWnd",
	CLuaMiniDouDiZhuWnd = "ui/doudizhu/remote/LuaMiniDouDiZhuWnd",
	CLuaRemoteDouDiZhuApplyListWnd = "ui/doudizhu/remote/LuaRemoteDouDiZhuApplyListWnd",
	CLuaRemoteDouDiZhuInviteWnd = "ui/doudizhu/remote/LuaRemoteDouDiZhuInviteWnd",
	CLuaRemoteDouDiZhuInviteDlg = "ui/doudizhu/remote/LuaRemoteDouDiZhuInviteDlg",
	CLuaRankNumWnd = "ui/common/LuaRankNumWnd",
	LuaEnvelopeDisplayWnd = "ui/common/LuaEnvelopeDisplayWnd",
	CLuaEquipChuiLianWnd = "ui/equip/zhounianqing/LuaEquipChuiLianWnd",
	CLuaEquipZhiJiShiUseWnd = "ui/equip/zhounianqing/LuaEquipZhiJiShiUseWnd",
	CLuaZNQDiscountView = "ui/welfare/LuaZNQDiscountView",
	LuaEquipScoreImproveWnd = "ui/equip/LuaEquipScoreImproveWnd",
	CLuaPVPWnd = "ui/mptz/LuaPVPWnd",
	LuaPVPRuleWnd = "ui/mptz/LuaPVPRuleWnd",
	LuaPVPSkillWnd = "ui/mptz/LuaPVPSkillWnd",
	LuaWipeLightEffectWnd = "ui/task/LuaWipeLightEffectWnd",
	LuaFlowerWithBloodEffectWnd = "ui/juqing/LuaFlowerWithBloodEffectWnd",
	LuaDriveAwayGhostWnd = "ui/juqing/LuaDriveAwayGhostWnd",
	LuaFlowerWitheredEffectWnd = "ui/juqing/LuaFlowerWitheredEffectWnd",
	LuaCageEffectWnd = "ui/juqing/LuaCageEffectWnd",
	LuaLeafWithMemoryWnd = "ui/juqing/LuaLeafWithMemoryWnd",
	LuaPanZhuangWnd = "ui/juqing/LuaPanZhuangWnd",
	LuaPourWineWnd = "ui/juqing/LuaPourWineWnd",
	LuaCouncilWnd = "ui/juqing/LuaCouncilWnd",
	CLuaTaskWnd = "ui/task/LuaTaskWnd",
	CLuaTaskDetailView = "ui/task/LuaTaskDetailView",
	LuaTaskMasterListItem = "ui/task/LuaTaskMasterListItem",
	CLuaWorldQiyishuWnd = "ui/worldevent/LuaWorldQiyishuWnd",
	CLuaTianQiMysticlShop_Entry = "ui/mapi/LuaTianQiMysticlShop_Entry",
	LuaJingLingWnd = "ui/jingling/LuaJingLingWnd",
	LuaJingLingHotspotView = "ui/jingling/LuaJingLingHotspotView",
	CLuaJingLingRecommendationView = "ui/jingling/LuaJingLingRecommendationView",
	LuaJingLingQuickPurchaseWnd = "ui/jingling/LuaJingLingQuickPurchaseWnd",
	LuaJingLingCommonInputWnd = "ui/jingling/LuaJingLingCommonInputWnd",
	LuaJingLingHotWordsMenu = "ui/jingling/LuaJingLingHotWordsMenu",
	CLuaWorldCupWnd = "ui/worldcup/LuaWorldCupWnd",
	CLuaWorldCupAgendaWnd = "ui/worldcup/LuaWorldCupAgendaWnd",
	LuaBQPConfirmCommitWnd = "ui/bingqipu/LuaBQPConfirmCommitWnd",
	LuaBQPMySponsorWnd = "ui/bingqipu/LuaBQPMySponsorWnd",
	LuaBQPPromotionWnd = "ui/bingqipu/LuaBQPPromotionWnd",
	LuaBQPEquipInfoWnd = "ui/bingqipu/LuaBQPEquipInfoWnd",
	CLuaDuanWuDaZuoZhanTopRightWnd = "ui/duanwudazuozhan/LuaDuanWuDaZuoZhanTopRightWnd",
	CLuaDuanWuDaZuoZhanRankWnd = "ui/duanwudazuozhan/LuaDuanWuDaZuoZhanRankWnd",
	CLuaDuanWuDaZuoZhanResultWnd = "ui/duanwudazuozhan/LuaDuanWuDaZuoZhanResultWnd",
	LuaSettingWnd = "ui/setting/LuaSettingWnd",
	LuaSettingWnd_BasicSetting = "ui/setting/LuaSettingWnd_BasicSetting",
	LuaSettingWnd_RenderSetting = "ui/setting/LuaSettingWnd_RenderSetting",
	LuaSettingWnd_VoiceSetting = "ui/setting/LuaSettingWnd_VoiceSetting",
	LuaSettingWnd_OtherSetting = "ui/setting/LuaSettingWnd_OtherSetting",
	LuaSettingWnd_SecondPasswordSetting = "ui/setting/LuaSettingWnd_SecondPasswordSetting",
	LuaSkillWnd = "ui/skill/LuaSkillWnd",
	LuaSkillUpgradeView = "ui/skill/LuaSkillUpgradeView",
	LuaSkillPreferenceView = "ui/skill/LuaSkillPreferenceView",
	LuaOtherSkillView = "ui/skill/LuaOtherSkillView",
	LuaLivingSkillWnd = "ui/skill/LuaLivingSkillWnd",
	CLuaLivingSkillMakeWnd = "ui/skill/lifeskill/LuaLivingSkillMakeWnd",
	LuaTianFuSkillWnd = "ui/skill/LuaTianFuSkillWnd",
	LuaYingLingSwitchingTable = "ui/skill/preferenceview/LuaYingLingSwitchingTable",
	LuaSkillPreferenceTopView = "ui/skill/preferenceview/LuaSkillPreferenceTopView",
	LuaSkillPreferenceBottomView = "ui/skill/preferenceview/LuaSkillPreferenceBottomView",
	LuaSkillPreferenceViewYingLingSwitchRoot = "ui/skill/preferenceview/LuaSkillPreferenceViewYingLingSwitchRoot",
	LuaSkillPreferenceViewSkillButtonBoard = "ui/skill/preferenceview/LuaSkillPreferenceViewSkillButtonBoard",
	LuaMPTZYingLingSwitchRoot = "ui/skill/preferenceview/LuaMPTZYingLingSwitchRoot",
	LuaExchange125SkillBookWnd = "ui/skill/LuaExchange125SkillBookWnd",
	CLuaZhuojiResultInfoWnd = "ui/liuyi/LuaZhuojiResultInfoWnd",
	CLuaZhuojiSingUpWnd = "ui/liuyi/LuaZhuojiSingUpWnd",
	CLuaZhuojiInfoWnd = "ui/liuyi/LuaZhuojiInfoWnd",
	CLuaZhuojiTopRightWnd = "ui/liuyi/LuaZhuojiTopRightWnd",
	CLuaShenbingZhuzaoWnd = "ui/shenbing/LuaShenbingZhuzaoWnd",
	CLuaShenbingPeiyangWnd = "ui/shenbing/LuaShenbingPeiyangWnd",
	CLuaShenbingSubmitWnd = "ui/shenbing/LuaShenbingSubmitWnd",
	LuaShenBingExchangeQyBuyWnd = "ui/shenbing/LuaShenBingExchangeQyBuyWnd",
	CLuaShenbingColorationWnd = "ui/shenbing/LuaShenbingColorationWnd",
	CLuaShenbingColorComposeWnd = "ui/shenbing/LuaShenbingColorComposeWnd",
	CLuaShenbingColorChooseWnd = "ui/shenbing/LuaShenbingColorChooseWnd",
	CLuaShenbingRestoreWnd = "ui/shenbing/LuaShenbingRestoreWnd",
	LuaEquipIdentifySkillWnd = "ui/equip/identifyskill/LuaEquipIdentifySkillWnd",
	LuaEquipIdentifySkillItemChooseWnd = "ui/equip/identifyskill/LuaEquipIdentifySkillItemChooseWnd",
	LuaItemSingleColumnListWnd = "ui/common/LuaItemSingleColumnListWnd",
	CLuaQMPKConfigMainWnd = "ui/quanminpk/config/LuaQMPKConfigMainWnd",
	CLuaQMPKEquipmentConfig = "ui/quanminpk/config/LuaQMPKEquipmentConfig",
	CLuaQMPKEquipmentFrame = "ui/quanminpk/config/LuaQMPKEquipmentFrame",
	CLuaQMPKTalismanConfig = "ui/quanminpk/config/LuaQMPKTalismanConfig",
	CLuaQMPKQiYuanConfig = "ui/quanminpk/config/LuaQMPKQiYuanConfig",
	CLuaQMPKAgendaWnd = "ui/quanminpk/LuaQMPKAgendaWnd",
	CLuaQMPKBattleWnd = "ui/quanminpk/LuaQMPKBattleWnd",
	CLuaQMPKInfoWnd = "ui/quanminpk/LuaQMPKInfoWnd",
	CLuaQMPKTopRightMenu = "ui/quanminpk/LuaQMPKTopRightMenu",
	CLuaQMPKLingShouConfig = "ui/quanminpk/config/LuaQMPKLingShouConfig",
	CLuaQMPKTalismanEquipView = "ui/quanminpk/config/LuaQMPKTalismanEquipView",
	CLuaQMPKMeiRiYiZhanWnd = "ui/quanminpk/LuaQMPKMeiRiYiZhanWnd",
	CLuaQMPKCreateZhanDuiWnd = "ui/quanminpk/LuaQMPKCreateZhanDuiWnd",
	LuaQMPKStatueBuildWnd = "ui/quanminpk/LuaQMPKStatueBuildWnd",
	LuaQMPKStatuePopupMenu = "ui/quanminpk/LuaQMPKStatuePopupMenu",
	CLuaQMPKBattleDataWnd = "ui/quanminpk/LuaQMPKBattleDataWnd",
	CLuaQMPKCurrentBattleStatusWnd = "ui/quanminpk/LuaQMPKCurrentBattleStatusWnd",
	CLuaQMPKGetItemWnd = "ui/quanminpk/LuaQMPKGetItemWnd",
	CLuaQMPKMatchingWnd = "ui/quanminpk/LuaQMPKMatchingWnd",
	CLuaQMPKModifySloganWnd = "ui/quanminpk/LuaQMPKModifySloganWnd",
	CLuaQMPKPlayerInfoWnd = "ui/quanminpk/LuaQMPKPlayerInfoWnd",
	CLuaQMPKSelfZhanDuiWnd = "ui/quanminpk/LuaQMPKSelfZhanDuiWnd",
	CLuaQMPKServerChosenWnd = "ui/quanminpk/LuaQMPKServerChosenWnd",
	CLuaQMPKShareWnd = "ui/quanminpk/LuaQMPKShareWnd",
	CLuaQMPKZhanDuiSettingWnd = "ui/quanminpk/LuaQMPKZhanDuiSettingWnd",
	CLuaQMPKMatchRecordWnd = "ui/quanminpk/guanzhan/LuaQMPKMatchRecordWnd",
	CLuaQMPKTaoTaiSaiMatchRecordWnd = "ui/quanminpk/guanzhan/LuaQMPKTaoTaiSaiMatchRecordWnd",
	CLuaQMPKJingCaiConfirmWnd = "ui/quanminpk/jingcai/LuaQMPKJingCaiConfirmWnd",
	CLuaQMPKJingCaiContentWnd = "ui/quanminpk/jingcai/LuaQMPKJingCaiContentWnd",
	CLuaQMPKJingCaiWnd = "ui/quanminpk/jingcai/LuaQMPKJingCaiWnd",
	CLuaQMPKJiXiangWuFeedWnd = "ui/quanminpk/jixiangwu/LuaQMPKJiXiangWuFeedWnd",
	CLuaQMPKRecruitWnd = "ui/quanminpk/zhandui/LuaQMPKRecruitWnd",
	CLuaQMPKSearchRecruitWnd = "ui/quanminpk/zhandui/LuaQMPKSearchRecruitWnd",
	CLuaQMPKZhanDuiMemberListWnd = "ui/quanminpk/zhandui/LuaQMPKZhanDuiMemberListWnd",
	CLuaQMPKZhanDuiSearchWnd = "ui/quanminpk/zhandui/LuaQMPKZhanDuiSearchWnd",
	CLuaQMPKStarWnd = "ui/quanminpk/LuaQMPKStarWnd",
	CLuaQMPKDamageWnd = "ui/quanminpk/LuaQMPKDamageWnd",
	CLuaQMPKAntiProfessionConfig = "ui/quanminpk/config/LuaQMPKAntiProfessionConfig",
	CLuaCommonItemListWnd = "ui/common/LuaCommonItemListWnd",
	CLuaScheduleHuoliView = "ui/schedule/LuaScheduleHuoliView",
	CLuaScheduleWnd = "ui/schedule/LuaScheduleWnd",
	CLuaScheduleCalendarWnd = "ui/schedule/LuaScheduleCalendarWnd",
	CLuaScheduleInfoWnd = "ui/schedule/LuaScheduleInfoWnd",
	CLuaSchedulePicWnd = "ui/schedule/LuaSchedulePicWnd",
	CLuaScheduleHuoliTip = "ui/schedule/LuaScheduleHuoliTip",
	CLuaScheduleStarBiwuPage = "ui/schedule/LuaScheduleStarBiwuPage",
	CLuaSkillCastHistoryWnd = "ui/douhun/LuaSkillCastHistoryWnd",
	CLuaSkillCastInfoItem = "ui/douhun/LuaSkillCastInfoItem",
	CLuaFightingSpiritFavorTeamWnd = "ui/fightingspirit/LuaFightingSpiritFavorTeamWnd",
	LuaFightingSpiritOtherTeamWnd = "ui/fightingspirit/LuaFightingSpiritOtherTeamWnd",
	CLuaMergeBattleWnd = "ui/mergebattle/LuaMergeBattleWnd",
	CLuaOverviewDetailWnd = "ui/mergebattle/LuaOverviewDetailWnd",
	CLuaPointsDetailWnd = "ui/mergebattle/LuaPointsDetailWnd",
	CLuaRecallDetailWnd = "ui/mergebattle/LuaRecallDetailWnd",
	CLuaDataDetailWnd = "ui/mergebattle/LuaDataDetailWnd",
	CLuaBindDetailWnd = "ui/mergebattle/LuaBindDetailWnd",
	CLuaMergeBattlePopupWnd = "ui/mergebattle/LuaMergeBattlePopupWnd",
	CLuaPointsComparedWnd = "ui/mergebattle/LuaPointsComparedWnd",
	CLuaPopupRecallWnd = "ui/mergebattle/LuaPopupRecallWnd",
	CLuaCrossSXDDZWnd = "ui/sxddz/LuaCrossSXDDZWnd",
	CLuaCrossSXDDZChallengerListWnd = "ui/sxddz/LuaCrossSXDDZChallengerListWnd",
	CLuaGuildMainWndMemberWnd = "ui/guild/LuaGuildMainWndMemberWnd",
	CLuaGuildMainWnd = "ui/guild/LuaGuildMainWnd",
	CLuaGuildMainWndBenifitWnd = "ui/guild/LuaGuildMainWndBenifitWnd",
	CLuaGuildMainWndActivityWnd = "ui/guild/LuaGuildMainWndActivityWnd",
	CLuaGuildHistoryWnd = "ui/guild/LuaGuildHistoryWnd",
	CLuaGuildMainWndTabContent_Info = "ui/guild/LuaGuildMainWndTabContent_Info",
	CLuaGuildJobChangeWnd = "ui/guild/zhiwei/LuaGuildJobChangeWnd",
	CLuaGuildPowerSettingWnd = "ui/guild/zhiwei/LuaGuildPowerSettingWnd",
	CLuaJoinGuildWnd = "ui/guild/noguild/LuaJoinGuildWnd",
	CLuaGuildWnd = "ui/guild/noguild/LuaGuildWnd",
	LuaBattleSituationWnd = "ui/guild/LuaBattleSituationWnd",
	LuaCarnivalCollectWnd = "ui/carnivalcollect/LuaCarnivalCollectWnd",
	LuaCarnivalCollectSubWnd = "ui/carnivalcollect/LuaCarnivalCollectSubWnd",
	LuaCarnivalDrawWnd = "ui/carnivalcollect/LuaCarnivalDrawWnd",
	LuaFriendStatusWnd = "ui/friend/LuaFriendStatusWnd",
	CLuaMailContentView = "ui/friend/LuaMailContentView",
	LuaMailListItem = "ui/friend/LuaMailListItem",
	LuaCCLiveWnd = "ui/cclive/LuaCCLiveWnd",
	LuaCCLiveListWnd = "ui/cclive/LuaCCLiveListWnd",
	LuaCCLiveFollowingListWnd = "ui/cclive/LuaCCLiveFollowingListWnd",
	LuaCCLiveFollowingListItem = "ui/cclive/LuaCCLiveFollowingListItem",
	LuaTeamCCSettingWnd = "ui/ccmini/LuaTeamCCSettingWnd",
	LuaTreasureHouseMyRoleView = "ui/treasurehouse/LuaTreasureHouseMyRoleView",
	LuaTreasureHouseRegisterWnd = "ui/treasurehouse/LuaTreasureHouseRegisterWnd",
	LuaTreasureHousePutToShowWnd = "ui/treasurehouse/LuaTreasureHousePutToShowWnd",
	LuaTreasureHouseShelfWnd = "ui/treasurehouse/LuaTreasureHouseShelfWnd",
	LuaTreasureHousePutToSellWnd = "ui/treasurehouse/LuaTreasureHousePutToSellWnd",
	CLuaFightingSpiritLiveWatchWnd = "ui/common/guanzhan/LuaFightingSpiritLiveWatchWnd",
	CLuaFightingSpiritLiveWatch_Top = "ui/common/guanzhan/LuaFightingSpiritLiveWatch_Top",
	CLuaGameVideoPlayWnd = "ui/common/guanzhan/LuaGameVideoPlayWnd",
	CLuaGuildLeagueWeeklyInfoWnd = "ui/guild/koudao/LuaGuildLeagueWeeklyInfoWnd",
	CLuaKouDaoJingYingSettingWnd = "ui/guild/koudao/LuaKouDaoJingYingSettingWnd",
	CLuaBangHuiJingYingSettingWnd = "ui/LuaBangHuiJingYingSettingWnd",
	LuaTianMenShanZhuLiSettingWnd = "ui/guild/tianmenshan/LuaTianMenShanZhuLiSettingWnd",
	LuaExchangeYuanBaoWithBaoShiWnd = "ui/exchange/LuaExchangeYuanBaoWithBaoShiWnd",
	CLuaGuiMenGuanStatusView = "ui/guimenguan/LuaGuiMenGuanStatusView",
	CLuaGuiMenGuanPlayInfoWnd = "ui/guimenguan/LuaGuiMenGuanPlayInfoWnd",
	CLuaBaiGuiTuJianWnd = "ui/zhongyuanjie/LuaBaiGuiTuJianWnd",
	CLuaLingShouChosenWnd = "ui/lingshou/guide/LuaLingShouChosenWnd",
	CLuaLingShouGetSkinWnd = "ui/lingshou/LuaLingShouGetSkinWnd",
	CLuaLingShouLearnSkillWnd = "ui/lingshou/skill/LuaLingShouLearnSkillWnd",
	CLuaLingShouSkillDetailView = "ui/lingshou/skill/LuaLingShouSkillDetailView",
	CLuaExchangeJieBanSkillItemWnd = "ui/lingshou/skill/LuaExchangeJieBanSkillItemWnd",
	CLuaExchangeJieBanSkillItemWnd2 = "ui/lingshou/skill/LuaExchangeJieBanSkillItemWnd2",
	CLuaSelectJieBanLingShouWnd = "ui/lingshou/skill/LuaSelectJieBanLingShouWnd",
	CLuaLingShouWashDetailView = "ui/lingshou/wash/LuaLingShouWashDetailView",
	CLuaLingShouXiuWuDetailView = "ui/lingshou/main/LuaLingShouXiuWuDetailView",
	CLuaLingShouPropertyDetailView = "ui/lingshou/main/LuaLingShouPropertyDetailView",
	CLuaLingShouMasterView = "ui/lingshou/main/LuaLingShouMasterView",
	CLuaLingShouMainWnd = "ui/lingshou/main/LuaLingShouMainWnd",
	CLuaLingShouDetailsDisplay = "ui/lingshou/main/LuaLingShouDetailsDisplay",
	CLuaLingShouUseWnd = "ui/lingshou/LuaLingShouUseWnd",
	CLuaLingShouLookUpWnd = "ui/lingshou/LuaLingShouLookUpWnd",
	CLuaCommonCharacterDescWnd = "ui/common/LuaCommonCharacterDescWnd",
	LuaScreenBrokenWnd = "ui/zhujuejuqing/LuaScreenBrokenWnd",
	LuaFallingDownWnd = "ui/zhujuejuqing/LuaFallingDownWnd",
	LuaFallingDownEffect = "ui/zhujuejuqing/LuaFallingDownEffect",
	LuaDrawHanZiWnd = "ui/zhujuejuqing/LuaDrawHanZiWnd",
	LuaFallingDownTreasureWnd = "ui/zhujuejuqing/LuaFallingDownTreasureWnd",
	LuaFallingDownTreasureEffect = "ui/zhujuejuqing/LuaFallingDownTreasureEffect",
	LuaLumberingWnd = "ui/zhujuejuqing/LuaLumberingWnd",
	LuaLumberingBranchEffect = "ui/zhujuejuqing/LuaLumberingBranchEffect",
	LuaBoilMedicineWnd = "ui/zhujuejuqing/LuaBoilMedicineWnd",
	LuaPuSongLingHandWriteEffectWnd = "ui/zhujuejuqing/LuaPuSongLingHandWriteEffectWnd",
	CLuaNewActivityWnd = "ui/schedule/LuaNewActivityWnd",
	CLuaChangYongSheBeiWnd = "ui/changyongshebei/LuaChangYongSheBeiWnd",
	CLuaXianFanComparePropWnd = "ui/feisheng/LuaXianFanComparePropWnd",
	CLuaGuanNingApplyWnd = "ui/guanning/LuaGuanNingApplyWnd",
	CLuaGuanNingChoiceWnd = "ui/guanning/LuaGuanNingChoiceWnd",
	CLuaGuanNingResultWnd = "ui/guanning/LuaGuanNingResultWnd",
	CLuaGuanNingSignalWnd = "ui/guanning/LuaGuanNingSignalWnd",
	CLuaGuanNingStateWnd = "ui/guanning/LuaGuanNingStateWnd",
	CLuaMajiuWnd = "ui/house/LuaMajiuWnd",
	CLuaQMPKJiXiangWuShareWnd = "ui/quanminpk/LuaQMPKJiXiangWuShareWnd",
	CLuaQMPKQieCuoWnd = "ui/quanminpk/LuaQMPKQieCuoWnd",
	CLuaQMPKSearchPlayerWnd = "ui/quanminpk/LuaQMPKSearchPlayerWnd",
	CLuaRealNamePreWnd = "ui/LuaRealNamePreWnd",
	CLuaSelfieAdditiveItemsWnd = "ui/selfie/LuaSelfieAdditiveitemsWnd",
	CLuaCarnivalGiftView = "ui/welfare/LuaCarnivalGiftView",
	CLuaWuCaiShaBingBattleInfoWnd = "ui/wucaishabing/LuaWuCaiShaBingBattleInfoWnd",
	CLuaWuCaiShaBingDetailWnd = "ui/wucaishabing/LuaWuCaiShaBingDetailWnd",
	CLuaWuCaiShaBingEnterWnd = "ui/wucaishabing/LuaWuCaiShaBingEnterWnd",
	CLuaWuCaiShaBingResultWnd = "ui/wucaishabing/LuaWuCaiShaBingResultWnd",
	CLuaWuCaiShaBingStateWnd = "ui/wucaishabing/LuaWuCaiShaBingStateWnd",
	CLuaBindDashenWnd = "ui/welfare/LuaBindDashenWnd",
	CLuaPlaceCityUnitWnd = "ui/citywar/LuaPlaceCityUnitWnd",
	CLuaCityConstructionRankWnd = "ui/citywar/LuaCityConstructionRankWnd",
	CLuaCityWarSecondaryMapWnd = "ui/citywar/LuaCityWarSecondaryMapWnd",
	CLuaCityWarPrimaryMapWnd = "ui/citywar/LuaCityWarPrimaryMapWnd",
	CLuaCityWarOccupyConfirmWnd = "ui/citywar/LuaCityWarOccupyConfirmWnd",
	CLuaCityWarSoldierWnd = "ui/citywar/LuaCityWarSoldierWnd",
	CLuaCityMapInfoTip = "ui/citywar/LuaCityMapInfoTip",
	CLuaCityWarMiniMap = "ui/citywar/LuaCityWarMiniMap",
	CLuaCityMainWnd = "ui/citywar/LuaCityMainWnd",
	LuaCityWarCityInfoWnd = "ui/citywar/LuaCityWarCityInfoWnd",
	CLuaHufuTemplate = "ui/citywar/LuaHufuTemplate",
	CLuaCityWarRuleTip = "ui/citywar/LuaCityWarRuleTip",
	CLuaCityWarScheduleWnd = "ui/citywar/LuaCityWarScheduleWnd",
	LuaCityWarAwardWnd = "ui/citywar/LuaCityWarAwardWnd",
	CLuaMajorCityMapInfoTip = "ui/citywar/LuaMajorCityMapInfoTip",
	CLuaCityWarRobAssetWnd = "ui/citywar/LuaCityWarRobAssetWnd",
	CLuaCityWarChallengeWnd = "ui/citywar/LuaCityWarChallengeWnd",
	CLuaCityWarStateWnd = "ui/citywar/LuaCityWarStateWnd",
	CLuaCityWarResultWnd = "ui/citywar/LuaCityWarResultWnd",
	CLuaCityWarGuildEnemyWnd = "ui/citywar/LuaCityWarGuildEnemyWnd",
	LuaCityWarBiaoCheMenu = "ui/citywar/LuaCityWarBiaoCheMenu",
	LuaCityWarPlaceBiaoCheConfirmWnd = "ui/citywar/LuaCityWarPlaceBiaoCheConfirmWnd",
	LuaCityWarBiaoCheInfoItem = "ui/citywar/LuaCityWarBiaoCheInfoItem",
	LuaCityWarContributionWnd = "ui/citywar/LuaCityWarContributionWnd",
	LuaCityWarBiaoCheResSubmitWnd = "ui/citywar/LuaCityWarBiaoCheResSubmitWnd",
	LuaCityWarBiaoCheConfirmWnd = "ui/citywar/LuaCityWarBiaoCheConfirmWnd",
	LuaCityWarGongFengView = "ui/citywar/LuaCityWarGongFengView",
	CLuaCityWarCopySecondaryMapWnd = "ui/citywar/LuaCityWarCopySecondaryMapWnd",
	CLuaCommonAnnounceWnd = "ui/common/LuaCommonAnnounceWnd",
	CLuaCityWarHistoryWnd = "ui/citywar/LuaCityWarHistoryWnd",
	CLuaCityWarGuildMaterialSubmitWnd = "ui/citywar/LuaCityWarGuildMaterialSubmitWnd",
	LuaCityWarMonsterSiegeWnd = "ui/citywar/LuaCityWarMonsterSiegeWnd",
	CLuaCityWarMonsterSiegeSettingWnd = "ui/citywar/LuaCityWarMonsterSiegeSettingWnd",
	CLuaCityWarMonsterSiegeSituationWnd = "ui/citywar/LuaCityWarMonsterSiegeSituationWnd",
	CLuaTianMenShanBattleStateWnd = "ui/citywar/LuaTianMenShanBattleStateWnd",
	CLuaCityWarDeclareRankWnd = "ui/citywar/LuaCityWarDeclareRankWnd",
	CLuaCityWarBatchUpgradeWnd = "ui/citywar/LuaCityWarBatchUpgradeWnd",
	CLuaCityWarSingleUpgradeWnd = "ui/citywar/LuaCityWarSingleUpgradeWnd",
	CLuaCityWarWatchWnd = "ui/citywar/LuaCityWarWatchWnd",
	CLuaCityWarExchangeWnd = "ui/citywar/LuaCityWarExchangeWnd",
	CLuaCityWarGuildSearchWnd = "ui/citywar/LuaCityWarGuildSearchWnd",
	CLuaCityGuildRankWnd = "ui/citywar/LuaCityGuildRankWnd",
	CLuaCityOccupyRankWnd = "ui/citywar/LuaCityOccupyRankWnd",
	LuaGQJCStateCell = "ui/gqjc/LuaGQJCStateCell",
	LuaGQJCChooseRewardWnd = "ui/gqjc/LuaGQJCChooseRewardWnd",
	LuaGQJCSituationWnd = "ui/gqjc/LuaGQJCSituationWnd",
	LuaGQJCRankWnd = "ui/gqjc/LuaGQJCRankWnd",
	CLuaLingShouBattleConfigWnd = "ui/lingshou/LuaLingShouBattleConfigWnd",
	CLuaHouseStuffMakeUseWnd = "ui/skill/lifeskill/LuaHouseStuffMakeUseWnd",
	CLuaHouseStuffMakeWnd = "ui/skill/lifeskill/LuaHouseStuffMakeWnd",
	CLuaSkillInfoWnd = "ui/skill/LuaSkillInfoWnd",
	CLuaSkillTip = "ui/skill/LuaSkillTip",
	LuaBirthPermissionWnd = "ui/baby/LuaBirthPermissionWnd",
	LuaProductionInspectionWnd = "ui/baby/LuaProductionInspectionWnd",
	LuaPrenatalMessageWnd = "ui/baby/LuaPrenatalMessageWnd",
	LuaBabyNamingWnd = "ui/baby/LuaBabyNamingWnd",
	LuaParentExamWnd = "ui/baby/LuaParentExamWnd",
	LuaParentExamResultWnd = "ui/baby/LuaParentExamResultWnd",
	LuaBabyGrowDiaryWnd = "ui/baby/LuaBabyGrowDiaryWnd",
	LuaBabyGrowDiaryQiYuView = "ui/baby/LuaBabyGrowDiaryQiYuView",
	LuaBabyGrowDiaryView = "ui/baby/LuaBabyGrowDiaryView",
	LuaBabyQiYuShareWnd = "ui/baby/LuaBabyQiYuShareWnd",
	LuaAutoTakingPhotoWnd = "ui/baby/LuaAutoTakingPhotoWnd",
	LuaBabySchedulePlanView = "ui/baby/LuaBabySchedulePlanView",
	LuaBabyInfoView = "ui/baby/LuaBabyInfoView",
	LuaBabySchedulePlanList = "ui/baby/LuaBabySchedulePlanList",
	LuaBabyWnd = "ui/baby/LuaBabyWnd",
	LuaBabyScheduleGainWnd = "ui/baby/LuaBabyScheduleGainWnd",
	LuaBabyProDifShowWnd = "ui/baby/LuaBabyProDifShowWnd",
	LuaBabyTaiMengWnd = "ui/baby/LuaBabyTaiMengWnd",
	CLuaManYueJiuChooseBabyWnd = "ui/baby/LuaManYueJiuChooseBabyWnd",
	LuaBabySchedulePlanDetailWnd = "ui/baby/LuaBabySchedulePlanDetailWnd",
	LuaBabyFeedWnd = "ui/baby/LuaBabyFeedWnd",
	LuaBabyChatWnd = "ui/baby/LuaBabyChatWnd",
	LuaBabyChatHistoryWnd = "ui/baby/LuaBabyChatHistoryWnd",
	LuaUnlockByBabyLevel = "ui/baby/LuaUnlockByBabyLevel",
	LuaBabyExpressionActionWnd = "ui/baby/LuaBabyExpressionActionWnd",
	LuaBabyExpressionUnlockWnd = "ui/baby/LuaBabyExpressionUnlockWnd",
	LuaBabyGetBackWnd = "ui/baby/LuaBabyGetBackWnd",
	LuaBabyClothExpressionActionWnd = "ui/baby/LuaBabyClothExpressionActionWnd",
	LuaBabyQiChangJianDingWnd = "ui/baby/LuaBabyQiChangJianDingWnd",
	LuaBabyQiChangShuWnd = "ui/baby/LuaBabyQiChangShuWnd",
	LuaBabyChooseWnd = "ui/baby/LuaBabyChooseWnd",
	LuaBabyQiChangRequestTip = "ui/baby/LuaBabyQiChangRequestTip",
	LuaBabyPropXiLianWnd = "ui/baby/LuaBabyPropXiLianWnd",
	LuaBabyPoemWnd = "ui/baby/LuaBabyPoemWnd",
	LuaBabyQiChangFxWnd = "ui/baby/LuaBabyQiChangFxWnd",
	LuaBabySkillTip = "ui/baby/LuaBabySkillTip",
	CLuaBabySendHongBaoWnd = "ui/baby/LuaBabySendHongBaoWnd",
	CLuaBabyHongBaoHistoryWnd = "ui/baby/LuaBabyHongBaoHistoryWnd",
	LuaBabyTiLiGetTip = "ui/baby/LuaBabyTiLiGetTip",
	CLuaBabyDivinationWnd = "ui/baby/LuaBabyDivinationWnd",
	LuaBabyFashionPreviewWnd = "ui/baby/LuaBabyFashionPreviewWnd",
	LuaBabyFashionChooseWnd = "ui/baby/LuaBabyFashionChooseWnd",
	LuaShopMallBabyFashionPreviewWnd = "ui/baby/LuaShopMallBabyFashionPreviewWnd",
	LuaCommonRuleTipWnd = "ui/common/LuaCommonRuleTipWnd",
	LuaGuildLeagueInviteWnd = "ui/guildleague/LuaGuildLeagueInviteWnd",
	LuaLianShenWnd = "ui/guild/lianshen/LuaLianShenWnd",
	CLuaLianLianKanPlay = "ui/lianliankan2018/LuaLianLianKanPlay",
	CLuaLianLianKanApplyWnd = "ui/lianliankan2018/LuaLianLianKanApplyWnd",
	CLuaLianLianKanRankWnd = "ui/lianliankan2018/LuaLianLianKanRankWnd",
	CLuaLianLianKanResultWnd = "ui/lianliankan2018/LuaLianLianKanResultWnd",
	CLuaLianLianKanTopRightWnd = "ui/lianliankan2018/LuaLianLianKanTopRightWnd",
	CLuaLianLianKanQuestionWnd = "ui/lianliankan2018/LuaLianLianKanQuestionWnd",
	CLuaZhongCaoFashionWnd = "ui/lianliankan2018/LuaZhongCaoFashionWnd",
	CLuaNPCShopWnd = "ui/npcshop/LuaNPCShopWnd",
	CLuaNPCShopWnd2 = "ui/npcshop/LuaNPCShopWnd2",
	CLuaBaoWeiNanGuaResultWnd = "ui/baoweinangua/LuaBaoWeiNanGuaResultWnd",
	CLuaBaoWeiNanGuaTopRightWnd = "ui/baoweinangua/LuaBaoWeiNanGuaTopRightWnd",
	CLuaTaoQuanWnd = "ui/youleyuan/CLuaTaoQuanWnd",
	CLuaPlayerEquipInfoView = "ui/main/LuaPlayerEquipInfoView",
	CLuaPlayerInfoWnd = "ui/main/LuaPlayerInfoWnd",
	LuaCrossServerFriendPopupMenu = "ui/main/LuaCrossServerFriendPopupMenu",
	CLuaPKProtectSetWnd = "ui/pk/LuaPKProtectSetWnd",
	LuaBaGuaLuLingQiGetWnd = "ui/bagualu/LuaBaGuaLuLingQiGetWnd",
	LuaBaGuaLuWnd = "ui/bagualu/LuaBaGuaLuWnd",
	LuaBaGuaLuLingQiZhuRuWnd = "ui/bagualu/LuaBaGuaLuLingQiZhuRuWnd",
	LuaBaGuaLuJieGuaWnd = "ui/bagualu/LuaBaGuaLuJieGuaWnd",
	LuaChristmasSendGiftWnd = "ui/christmas/LuaChristmasSendGiftWnd",
	LuaNeteaseMemberView = "ui/welfare/LuaNeteaseMemberView",
	LuaBabyFundWindow = "ui/baby/LuaBabyFundWindow",
	LuaChristmasTreeBreedWnd = "ui/christmas/LuaChristmasTreeBreedWnd",
	LuaChristmasTreeAddItemWnd = "ui/christmas/LuaChristmasTreeAddItemWnd",
	LuaNewYear2019DrawWnd = "ui/newyear2019/LuaNewYear2019DrawWnd",
	LuaNewYear2019TaskBookWnd = "ui/newyear2019/LuaNewYear2019TaskBookWnd",
	LuaNewYear2019TaskTipWnd = "ui/newyear2019/LuaNewYear2019TaskTipWnd",
	CLuaNewYear2019NianRewardExchangeWnd = "ui/newyear2019/LuaNewYear2019NianRewardExchangeWnd",
	CLuaNewYear2019NianFireworkConfirmWnd = "ui/newyear2019/LuaNewYear2019NianFireworkConfirmWnd",
	LuaDebateWithNpcWnd = "ui/debatewithnpc/LuaDebateWithNpcWnd",
	LuaQiqiaobanWnd = "ui/qiqiaoban/LuaQiqiaobanWnd",
	LuaSnowBallHisWnd = "ui/snowball2019/LuaSnowBallHisWnd",
	LuaSnowBallRankWnd = "ui/snowball2019/LuaSnowBallRankWnd",
	LuaSnowBallShowWnd = "ui/snowball2019/LuaSnowBallShowWnd",
	LuaYanChiXiaWorkWnd = "ui/hanjia2020/LuaYanChiXiaWorkWnd",
	LuaFlagonOpenWnd = "ui/hanjia2020/LuaFlagonOpenWnd",
	LuaSubmitSnowballsForMakingSnowManWnd = "ui/hanjia2024/LuaSubmitSnowballsForMakingSnowManWnd",
	LuaDecorateSnowmanWnd = "ui/hanjia2024/LuaDecorateSnowmanWnd",
	LuaDuiDuiLeStartWnd = "ui/yuanxiao2020/LuaDuiDuiLeStartWnd",
	LuaDuiDuiLeSettlementWnd = "ui/yuanxiao2020/LuaDuiDuiLeSettlementWnd",
	LuaDuiDuiLePlayInfos = "ui/yuanxiao2020/LuaDuiDuiLePlayInfos",
	LuaDuiDuiLeGamePlayWnd = "ui/yuanxiao2020/LuaDuiDuiLeGamePlayWnd",
	LuaYuanXiao2020QiYuanWnd = "ui/yuanxiao2020/LuaYuanXiao2020QiYuanWnd",
	LuaYuanxiaoTopRightWnd = "ui/yuanxiao/LuaYuanxiaoTopRightWnd",
	LuaYuanxiaoChoosePuzzleWnd = "ui/yuanxiao/LuaYuanxiaoChoosePuzzleWnd",
	CLuaYuanxiaoResultWnd = "ui/yuanxiao/LuaYuanxiaoResultWnd",
	CLuaWinterHouseChooseWnd = "ui/house/LuaWinterHouseChooseWnd",
	CLuaWinterHouseUnlockListWnd = "ui/house/LuaWinterHouseUnlockListWnd",
	CLuaWinterHouseUnlockWnd = "ui/house/LuaWinterHouseUnlockWnd",
	CLuaWinterHouseZswLookupWnd = "ui/house/LuaWinterHouseZswLookupWnd",

	CLuaFurnitureSlider = "ui/house/LuaFurnitureSlider",
	CLuaHouseWoodPileStatusWnd = "ui/house/LuaHouseWoodPileStatusWnd",
	CLuaHouseWoodPileSwitchLevelWnd = "ui/house/LuaHouseWoodPileSwitchLevelWnd",
	CLuaAwardHongbaoWnd = "ui/hongbao/LuaAwardHongbaoWnd",
	CLuaSnatchVoiceHongBaoWnd = "ui/hongbao/LuaSnatchVoiceHongBaoWnd",
	CLuaDoubleExpWindow = "ui/welfare/LuaDoubleExpWindow",
	LuaDaShenWelfareWnd = "ui/welfare/LuaDaShenWelfareWnd",
	CYuanbaoExchangeWnd = "ui/shopmall/CYuanbaoExchangeWnd",
	LuaCopySceneChosenWnd = "ui/copyScene/LuaCopySceneChosenWnd",
	LuaMengHuaLuWnd = "ui/menghualu/LuaMengHuaLuWnd",
	LuaMengHuaLuResultWnd = "ui/menghualu/LuaMengHuaLuResultWnd",
	LuaMengHuaLuMazeItem = "ui/menghualu/LuaMengHuaLuMazeItem",
	LuaMengHuaLuQiChangWnd = "ui/menghualu/LuaMengHuaLuQiChangWnd",
	LuaMengHuaLuBuffListWnd = "ui/menghualu/LuaMengHuaLuBuffListWnd",
	LuaMengHuaLuMonsterWnd = "ui/menghualu/LuaMengHuaLuMonsterWnd",
	LuaMengHuaLuStartWnd = "ui/menghualu/LuaMengHuaLuStartWnd",
	LuaMengHualuRankWnd = "ui/menghualu/LuaMengHualuRankWnd",
	LuaMengHuaLuShopWnd = "ui/menghualu/LuaMengHuaLuShopWnd",
	LuaMengHuaLuSkillTip = "ui/menghualu/LuaMengHuaLuSkillTip",
	LuaMengHuaLuBabyDamageNotice = "ui/menghualu/LuaMengHuaLuBabyDamageNotice",
	CLuaFamilyTreeWnd = "ui/familytree/LuaFamilyTreeWnd",
	LuaAddTagWnd = "ui/familytree/LuaAddTagWnd",
	LuaDelTagWnd = "ui/familytree/LuaDelTagWnd",
	LuaDelOtherTagWnd = "ui/familytree/LuaDelOtherTagWnd",
	LuaFamilyTagDetailWnd = "ui/familytree/LuaFamilyTagDetailWnd",
	CLuaPlayerCapacityOtherWnd = "ui/main/LuaPlayerCapacityOtherWnd",
	CLuaPreciousItemAlertWnd = "ui/common/LuaPreciousItemAlertWnd",
	CLuaQingYuanShouJiListWnd = "ui/valentine2019/LuaQingYuanShouJiListWnd",
	CLuaQingYuanShouJiContentWnd = "ui/valentine2019/LuaQingYuanShouJiContentWnd",
	CLuaQingYuanShouJiMemoirsWnd = "ui/valentine2019/LuaQingYuanShouJiMemoirsWnd",
	CLuaQingYuanShouJiPromiseWnd = "ui/valentine2019/LuaQingYuanShouJiPromiseWnd",
	LuaPreExistenceMarridgeAlbumWnd = "ui/valentine2019/LuaPreExistenceMarridgeAlbumWnd",
	LuaPreExistenceMarridgeDetailWnd = "ui/valentine2019/LuaPreExistenceMarridgeDetailWnd",
	LuaInTheNameOfLoveRankWnd = "ui/valentine2019/LuaInTheNameOfLoveRankWnd",
	LuaNationalDayTXZREnterWnd = "ui/nationalday/LuaNationalDayTXZREnterWnd",
	LuaNationalDayTXZRResultWnd = "ui/nationalday/LuaNationalDayTXZRResultWnd",
	LuaNationalDayJCYWResultWnd = "ui/nationalday/LuaNationalDayJCYWResultWnd",
	CLuaPersonalSpacePlayerCapacityWnd = "ui/personalspace/LuaPersonalSpacePlayerCapacityWnd",
	LuaPersonalSpaceMgrReal = "ui/personalspace/LuaPersonalSpaceMgrReal",
	LuaPersonalSpaceHotTalkWnd = "ui/personalspace/LuaPersonalSpaceHotTalkWnd",
	LuaPersonalSpaceDetailView = "ui/personalspace/LuaPersonalSpaceDetailView",
	LuaPersonalSpaceDetailPicWnd = "ui/personalspace/LuaPersonalSpaceDetailPicWnd",
	LuaPersonalSpaceForwardWnd = "ui/personalspace/LuaPersonalSpaceForwardWnd",
	LuaDetailMovieShowWnd = "ui/personalspace/LuaDetailMovieShowWnd",
	CLuaJuQingDialogWnd = "ui/main/dialog/LuaJuQingDialogWnd",
	CLuaBatDialogWnd = "ui/main/dialog/LuaBatDialogWnd",
	CLuaEquipmentProcessWnd = "ui/equip/LuaEquipmentProcessWnd",
	CLuaLingShouSwitchJieBanSkillWnd = "ui/lingshou/LuaLingShouSwitchJieBanSkillWnd",
	CLuaStarBiwuTopFourWnd = "ui/starbiwu/LuaStarBiwuTopFourWnd",
	CLuaStarBiwuFinalMatchRecordWnd = "ui/starbiwu/LuaStarBiwuFinalMatchRecordWnd",
	CLuaStarBiwuScoreWnd = "ui/starbiwu/LuaStarBiwuScoreWnd",
	CLuaStarBiwuMatchRecordWnd = "ui/starbiwu/LuaStarBiwuMatchRecordWnd",
	CLuaStarBiwuZhanDuiMemberListWnd = "ui/starbiwu/LuaStarBiwuZhanDuiMemberListWnd",
	CLuaStarBiwuBattleDataWnd = "ui/starbiwu/LuaStarBiwuBattleDataWnd",
	CLuaStarBiwuCurrentBattleStatusWnd = "ui/starbiwu/LuaStarBiwuCurrentBattleStatusWnd",
	CLuaStarBiwuModifySloganWnd = "ui/starbiwu/LuaStarBiwuModifySloganWnd",
	CLuaStarBiwuZhanDuiSettingWnd = "ui/starbiwu/LuaStarBiwuZhanDuiSettingWnd",
	CLuaStarBiwuSelfZhanDuiWnd = "ui/starbiwu/LuaStarBiwuSelfZhanDuiWnd",
	CLuaStarBiwuCreateZhanDuiWnd = "ui/starbiwu/LuaStarBiwuCreateZhanDuiWnd",
	CLuaStarBiwuZhanDuiSearchWnd = "ui/starbiwu/LuaStarBiwuZhanDuiSearchWnd",
	CLuaStarBiwuQieCuoWnd = "ui/starbiwu/LuaStarBiwuQieCuoWnd",
	CLuaStarBiwuAgendaWnd = "ui/starbiwu/LuaStarBiwuAgendaWnd",
	CLuaStarBiwuStatueBuildWnd = "ui/starbiwu/LuaStarBiwuStatueBuildWnd",
	CLuaStarBiwuChampionHistoryWnd = "ui/starbiwu/LuaStarBiwuChampionHistoryWnd",
	CLuaStarBiwuChampionTeamWnd = "ui/starbiwu/LuaStarBiwuChampionTeamWnd",
	CLuaStarBiwuDeleteFriendWnd = "ui/starbiwu/LuaStarBiwuDeleteFriendWnd",
	LuaStarBiwuJingCaiDetailWnd = "ui/starbiwu/LuaStarBiwuJingCaiDetailWnd",
	CLuaStarBiwuRewardWnd = "ui/starbiwu/LuaStarBiwuRewardWnd",
	CLuaStarBiwuFinalJingCaiWnd = "ui/starbiwu/LuaStarBiwuFinalJingCaiWnd",
	CLuaStarBiwuStopVoteWnd = "ui/starbiwu/LuaStarBiwuStopVoteWnd",
	LuaStarBiwuEventReviewWnd = "ui/starbiwu/LuaStarBiwuEventReviewWnd",
	LuaStarBiwuReShenBattleStatusWnd = "ui/starbiwu/LuaStarBiwuReShenBattleStatusWnd",
	LuaStarBiwuQuDaoBattleStatusWnd = "ui/starbiwu/LuaStarBiwuQuDaoBattleStatusWnd",

	CLuaWeddingRingTabenWnd = "ui/wedding/LuaWeddingRingTabenWnd",
	LuaStarInviteWatchWnd = "ui/starinvitegame/LuaStarInviteWatchWnd",
	LuaStarInviteSettingWnd = "ui/starinvitegame/LuaStarInviteSettingWnd",
	CLuaCheckGemGroupWnd = "ui/equip/shizhiling/LuaCheckGemGroupWnd",
	CLuaCheckGemGroupTemplate = "ui/equip/shizhiling/LuaCheckGemGroupTemplate",
	CLuaInlayGemListWnd = "ui/equip/shizhiling/LuaInlayGemListWnd",
	CLuaMakeEquipHoleWnd = "ui/equip/shizhiling/LuaMakeEquipHoleWnd",
	CLuaRemoveInlayGemWnd = "ui/equip/shizhiling/LuaRemoveInlayGemWnd",
	CLuaEquipInlayStoneDetailView = "ui/equip/shizhiling/LuaEquipInlayStoneDetailView",
	CLuaGemUpgradeWnd = "ui/equip/shizhiling/LuaGemUpgradeWnd",
	CLuaGemUpdateScrollView = "ui/equip/shizhiling/LuaGemUpdateScrollView",
	CLuaGemUpdateDetailView = "ui/equip/shizhiling/LuaGemUpdateDetailView",
	CLuaGemGroupInfoWnd = "ui/equip/shizhiling/LuaGemGroupInfoWnd",
	CLuaGemGroupInfoDetailView = "ui/equip/shizhiling/LuaGemGroupInfoDetailView",
	CLuaGemInfoWnd = "ui/equip/shizhiling/LuaGemInfoWnd",
	CLuaEquipInfoWnd = "ui/equip/LuaEquipInfoWnd",
	CLuaEquipmentSuitWnd = "ui/equip/LuaEquipmentSuitWnd",
	CLuaTalismanWnd = "ui/fabao/LuaTalismanWnd",
	CLuaTalismanBaptizeWindow = "ui/fabao/LuaTalismanBaptizeWindow",
	CLuaTalismanBaptizeItemList = "ui/fabao/LuaTalismanBaptizeItemList",
	CLuaXianjiaUpgradeWindow = "ui/fabao/LuaXianjiaUpgradeWindow",
	CLuaTalismanInlayStoneDetailView = "ui/fabao/LuaTalismanInlayStoneDetailView",
	CLuaTalismanFxExchangeWnd = "ui/fabao/LuaTalismanFxExchangeWnd",
	CLuaTalismanFxReturnWnd = "ui/fabao/LuaTalismanFxReturnWnd",
	LuaQingmingMakeWineWnd = "ui/qingming2019/LuaQingmingMakeWineWnd",
	LuaPaintPicWnd = "ui/paintpic/LuaPaintPicWnd",
	LuaPaintPicWndType2 = "ui/paintpic/LuaPaintPicWndType2",
	LuaPaintPicWndType3 = "ui/paintpic/LuaPaintPicWndType3",
	CLuaItemInfoWnd = "ui/LuaItemInfoWnd",
	CLuaMusicBoxRecordWnd = "ui/musicbox/LuaMusicBoxRecordWnd",
	CLuaMusicBoxMainWnd = "ui/musicbox/LuaMusicBoxMainWnd",
	CLuaExpProgressWnd = "ui/main/base/LuaExpProgressWnd",
	CExpWnd_PopupMenu = "ui/main/base/LuaExpProgressWnd",
	LuaQingHongFlyWnd = "ui/zhusidong/LuaQingHongFlyWnd",
	LuaCakeCuttingWnd = "ui/cakecutting/LuaCakeCuttingWnd",
	LuaCakeCuttingFightInfoWnd = "ui/cakecutting/LuaCakeCuttingFightInfoWnd",
	LuaCakeCuttingRankWnd = "ui/cakecutting/LuaCakeCuttingRankWnd",
	LuaCakeCuttingDieWnd = "ui/cakecutting/LuaCakeCuttingDieWnd",
	LuaCakeCuttingComboWnd = "ui/cakecutting/LuaCakeCuttingComboWnd",
	LuaCakeCuttingShowWnd = "ui/cakecutting/LuaCakeCuttingShowWnd",
	LuaCaiPiao2019Wnd = "ui/cakecutting/LuaCaiPiao2019Wnd",
	LuaNewSeverInviteWnd = "ui/cakecutting/LuaNewSeverInviteWnd",
	LuaQTEWnd = "ui/qte/LuaQTEWnd",
	LuaQTESingleClickWnd = "ui/qte/LuaQTESingleClickWnd",
	LuaQTEConsecutiveClickWnd = "ui/qte/LuaQTEConsecutiveClickWnd",
	LuaQTESlideWnd = "ui/qte/LuaQTESlideWnd",
	LuaQTESequentialClickWnd = "ui/qte/LuaQTESequentialClickWnd",
	LuaQTEShakeWnd = "ui/qte/LuaQTEShakeWnd",
	LuaQTEFinishFxHelperWnd = "ui/qte/LuaQTEFinishFxHelperWnd",
	CLuaXianzhiChooseImageTaskWnd = "ui/xianzhi/LuaXianzhiChooseImageTaskWnd",
	CLuaXianzhiDescribeWnd = "ui/xianzhi/LuaXianzhiDescribeWnd",
	CLuaXianzhiTaskCompletedWnd = "ui/xianzhi/LuaXianzhiTaskCompletedWnd",
	LuaXianZhiPuWnd = "ui/xianzhi/LuaXianZhiPuWnd",
	LuaXianZhiManageWnd = "ui/xianzhi/LuaXianZhiManageWnd",
	LuaXianZhiShenZhiWnd = "ui/xianzhi/LuaXianZhiShenZhiWnd",
	LuaXianZhiWnd = "ui/xianzhi/LuaXianZhiWnd",
	LuaXianZhiSkillUpgradeWnd = "ui/xianzhi/LuaXianZhiSkillUpgradeWnd",
	LuaXianZhiDetailDescWnd = "ui/xianzhi/LuaXianZhiDetailDescWnd",
	LuaXianZhiTypeDescWnd = "ui/xianzhi/LuaXianZhiTypeDescWnd",
	LuaXianZhiPromotionWnd = "ui/xianzhi/LuaXianZhiPromotionWnd",
	LuaFengXianSignUpWnd = "ui/xianzhi/LuaFengXianSignUpWnd",
	LuaFengXianInviteWnd = "ui/xianzhi/LuaFengXianInviteWnd",
	LuaFengXianInvitationCardWnd = "ui/xianzhi/LuaFengXianInvitationCardWnd",
	LuaFengXianTingFengWnd = "ui/xianzhi/LuaFengXianTingFengWnd",
	LuaFengXianView = "ui/xianzhi/LuaFengXianView",
	LuaXianZhiSkillInfo = "ui/xianzhi/LuaXianZhiSkillInfo",
	LuaWaKuangStartWnd = "ui/wuyi/LuaWaKuangStartWnd",
	LuaWaKuangStateWnd = "ui/wuyi/LuaWaKuangStateWnd",
	LuaWaKuangUpgradeWnd = "ui/wuyi/LuaWaKuangUpgradeWnd",
	LuaWaKuangResultWnd = "ui/wuyi/LuaWaKuangResultWnd",
	LuaWaKuangTaskView = "ui/wuyi/LuaWaKuangTaskView",
	LuaWaKuangRankWnd = "ui/wuyi/LuaWaKuangRankWnd",
	CLuaPropertySwitchWnd = "ui/main/LuaPropertySwitchWnd",
	CLuaPropertySwitchPopupMenu = "ui/main/LuaPropertySwitchPopupMenu",
	CLuaNpcHaoGanDuWnd = "ui/npchaogandu/LuaNpcHaoGanDuWnd",
	CLuaNpcHaoGanDuBuyGiftCountWnd = "ui/npchaogandu/LuaNpcHaoGanDuBuyGiftCountWnd",
	CLuaScreenCaptureWnd = "ui/screencapture/LuaScreenCaptureWnd",
	CLuaScreenCaptureExpressionView = "ui/screencapture/LuaScreenCaptureExpressionView",
	LuaChildrenDay2019TaskBookWnd = "ui/childrenday2019/LuaChildrenDay2019TaskBookWnd",
	LuaChildrenDayPlayOperateWnd = "ui/childrenday2019/LuaChildrenDayPlayOperateWnd",
	LuaChildrenDayPlayShowWnd = "ui/childrenday2019/LuaChildrenDayPlayShowWnd",
	LuaChildrenDayPlayFightInfoWnd = "ui/childrenday2019/LuaChildrenDayPlayFightInfoWnd",
	LuaChildrenDayPlayResultWnd = "ui/childrenday2019/LuaChildrenDayPlayResultWnd",
	LuaChildrenDayPlayTopRightWnd = "ui/childrenday2019/LuaChildrenDayPlayTopRightWnd",
	CLuaDwWusesiHeChengWnd = "ui/duanwu2019/LuaDwWusesiHeChengWnd",
	CLuaDwWusesiTabenWnd = "ui/duanwu2019/LuaDwWusesiTabenWnd",
	CLuaDwZongziRuleWnd = "ui/duanwu2019/LuaDwZongziRuleWnd",
	CLuaDwZongziRuleTipWnd = "ui/duanwu2019/LuaDwZongziRuleTipWnd",
	CLuaDwZongziResultWnd = "ui/duanwu2019/LuaDwZongziResultWnd",
	CLuaDwZongziTaskView = "ui/duanwu2019/LuaDwZongziTaskView",
	CLuaDwZongziTopRightMenu = "ui/duanwu2019/LuaDwZongziTopRightMenu",
	LuaPlayerAppearanceSettingWnd = "ui/lianghao/LuaPlayerAppearanceSettingWnd",
	LuaLiangHaoSettingWnd = "ui/lianghao/LuaLiangHaoSettingWnd",
	LuaLiangHaoRenewWnd = "ui/lianghao/LuaLiangHaoRenewWnd",
	LuaLiangHaoFriendListWnd = "ui/lianghao/LuaLiangHaoFriendListWnd",
	LuaLiangHaoPurchaseWnd = "ui/lianghao/LuaLiangHaoPurchaseWnd",
	LuaLiangHaoAuctionWnd = "ui/lianghao/LuaLiangHaoAuctionWnd",
	LuaLiangHaoMyAuctionWnd = "ui/lianghao/LuaLiangHaoMyAuctionWnd",
	LuaLiangHaoCustomizeWnd = "ui/lianghao/LuaLiangHaoCustomizeWnd",
	LuaLiangHaoMyReqCustomizeCardWnd = "ui/lianghao/LuaLiangHaoMyReqCustomizeCardWnd",
	LuaLiangHaoReqCustomizeCardWnd = "ui/lianghao/LuaLiangHaoReqCustomizeCardWnd",
	LuaAchievementDetailWnd = "ui/achievement/LuaAchievementDetailWnd",
	LuaAchievementAwardsTipWnd = "ui/achievement/LuaAchievementAwardsTipWnd",
	LuaGuildReportWnd = "ui/report/LuaGuildReportWnd",
	CLuaCookingPlayWnd = "ui/cooking/LuaCookingPlayWnd",
	CLuaCharacterCreationCameraCtrl = "ui/login/LuaCharacterCreationCameraCtrl",
	LuaCharacterCreationWnd = "ui/login/LuaCharacterCreationWnd",

	--捏脸相关正式代码
	LuaPinchFaceMgr = "ui/pinchface/LuaPinchFaceMgr",
	LuaPinchFaceWnd = "ui/pinchface/LuaPinchFaceWnd",
	LuaPinchFacePreselectionView = "ui/pinchface/LuaPinchFacePreselectionView",
	LuaPinchFaceMainView = "ui/pinchface/LuaPinchFaceMainView",
	LuaPinchFaceSliderControlView = "ui/pinchface/LuaPinchFaceSliderControlView",
	LuaPinchFacePreviewView = "ui/pinchface/LuaPinchFacePreviewView",
	LuaPinchFaceDisplayView = "ui/pinchface/LuaPinchFaceDisplayView",
	LuaOneDimenSlider = "ui/pinchface/LuaOneDimenSlider",
	LuaTwoDimenSlider = "ui/pinchface/LuaTwoDimenSlider",
	LuaCustomColorSlider = "ui/pinchface/LuaCustomColorSlider",
	LuaPinchFaceModifyWnd = "ui/pinchface/LuaPinchFaceModifyWnd",
	LuaPinchFaceShareWnd = "ui/pinchface/LuaPinchFaceShareWnd",
	LuaPinchFaceTimeCtrlMgr = "ui/pinchface/LuaPinchFaceTimeCtrlMgr",
	LuaPinchFaceBtn = "ui/pinchface/LuaPinchFaceBtn",
	
	-- 云游戏捏脸
	LuaCloudGameFaceMgr = "ui/pinchface/LuaCloudGameFaceMgr",

	LuaTopNoticeCenter = "ui/common/LuaTopNoticeCenter",

	LuaCGMask = "ui/login/LuaCGMask",
	CLuaBoLangGuWnd = "ui/digong/LuaBoLangGuWnd",
	CLuaJianBiHuaWnd = "ui/digong/LuaJianBiHuaWnd",
	CLuaShowPictureWnd = "ui/common/LuaShowPictureWnd",
	LuaCharacterCardWnd = "ui/charactercard/LuaCharacterCardWnd",
	LuaCharacterCardDetailWnd = "ui/charactercard/LuaCharacterCardDetailWnd",
	LuaCharacterCardHeWnd = "ui/charactercard/LuaCharacterCardHeWnd",
	CLuaItemUsageWnd = "ui/main/LuaItemUsageWnd",
	CLuaHouseCompetitionWnd = "ui/house/competition/LuaHouseCompetitionWnd",
	CLuaHouseCompetitionFanPaiZiWnd = "ui/house/competition/LuaHouseCompetitionFanPaiZiWnd",
	CLuaHouseCompetitionFanPaiZiRewardWnd = "ui/house/competition/LuaHouseCompetitionFanPaiZiRewardWnd",
	CLuaHouseCompetitionFanPaiZiAddChancesWnd = "ui/house/competition/LuaHouseCompetitionFanPaiZiAddChancesWnd",
	CLuaShoppingMallWnd = "ui/shopmall/LuaShoppingMallWnd",
	LuaShopMallFashionPreviewWnd = "ui/shopmall/LuaShopMallFashionPreviewWnd",
	LuaShopMallItemSelectWnd = "ui/shopmall/LuaShopMallItemSelectWnd",
	LuaFashionPreviewTryToBuyWnd = "ui/shopmall/LuaFashionPreviewTryToBuyWnd",
	LuaFashionPreviewItemDescriptionRoot = "ui/shopmall/LuaFashionPreviewItemDescriptionRoot",
	CLuaWuJianDiYuTopRightWnd = "ui/wujiandiyu/LuaWuJianDiYuTopRightWnd",
	CLuaWuJianDiYuEnterWnd = "ui/wujiandiyu/LuaWuJianDiYuEnterWnd",
	CLuaWuJianDiYuHeChengWnd = "ui/wujiandiyu/LuaWuJianDiYuHeChengWnd",
	LuaSceneJewelSubmitWnd = "ui/sceneinteractive/LuaSceneJewelSubmitWnd",
	CLuaNewAchievementGetWnd = "ui/main/mainplayer/LuaNewAchievementGetWnd",
	CLuaQMPKHandBookWnd = "ui/quanminpk/LuaQMPKHandBookWnd",
	CLuaNpcHaoGanDuZhanBuWnd = "ui/npchaogandu/LuaNpcHaoGanDuZhanBuWnd",
	CLuaNpcHaoGanDuGiftWnd = "ui/npchaogandu/LuaNpcHaoGanDuGiftWnd",
	LuaCarnivalCaiPiao2019Wnd = "ui/carnivalcaipiao2019/LuaCarnivalCaiPiao2019Wnd",
	CLuaDanMuViewWnd = "ui/common/LuaDanMuViewWnd",
	CLuaQiXiLiuXingQiYuanWnd = "ui/qixi/LuaQiXiLiuXingQiYuanWnd",
	CLuaMailView = "ui/friend/LuaMailView",
	CLuaZQYueShenDianDescWnd = "ui/zhongqiu2019/LuaZQYueShenDianDescWnd",
	CLuaZQYaoBanWnd = "ui/zhongqiu2019/LuaZQYaoBanShangYueWnd",
	CLuaZQSelectionToolTip = "ui/zhongqiu2019/LuaZQSelectionToolTip",
	CLuaZQYueLingShiHeChengWnd = "ui/zhongqiu2019/LuaZQYueLingShiHeChengWnd",
	CLuaZQYueShenDianTopRightWnd = "ui/zhongqiu2019/LuaZQYueShenDianTopRightWnd",
	CLuaZhuShaBiProcessWnd = "ui/zhushabi/LuaZhuShaBiProcessWnd",
	CLuaZhuShaBiStarWnd = "ui/zhushabi/LuaZhuShaBiStarWnd",
	CLuaZhuShaBiXiLianConditionWnd = "ui/zhushabi/LuaZhuShaBiXiLianConditionWnd",
	LuaYunyingfuliWindow = "ui/welfare/LuaYunyingfuliWindow",
	CLuaBookshelfSearchWnd = "ui/shangui/LuaBookshelfSearchWnd",
	CLuaGuqinWnd = "ui/shangui/LuaGuqinWnd",
	CLuaWuZiQiWnd = "ui/diyu/LuaWuZiQiWnd",
	CLuaKaoChengHuangWnd = "ui/diyu/LuaKaoChengHuangWnd",
	LuaGuildBuffDeskWnd = "ui/guild/LuaGuildBuffDeskWnd",
	CLuaCJBPreviewWnd = "ui/house/LuaCJBPreviewWnd",
	CLuaWarehouseView = "ui/main/LuaWarehouseView",
	CLuaWarehouseWnd = "ui/main/LuaWarehouseWnd",
	CLuaWarehousePage = "ui/main/LuaWarehousePage",
	CLuaPackageWnd = "ui/main/LuaPackageWnd",
	CLuaWarehousePackageView = "ui/main/LuaWarehousePackageView",
	CLuaChuanJiaBaoWnd = "ui/house/LuaChuanJiaBaoWnd",
	CLuaChuanjiabaoShelf = "ui/house/LuaChuanjiabaoShelf",
	CLuaChuanjiabaoInfoWindow = "ui/house/LuaChuanjiabaoInfoWindow",
	CLuaHouseFurnitureRotationWnd = "ui/house/LuaHouseFurnitureRotationWnd",
	LuaQYCodeInputBoxWnd = "ui/login/LuaQYCodeInputBoxWnd",
	CLuaItemAccessListWnd = "ui/common/LuaItemAccessListWnd",
	CLuaHalloweenLetterWnd = "ui/halloween2019/LuaHalloweenLetterWnd",
	LuaCleanLingYuWnd = "ui/halloween2019/LuaCleanLingYuWnd",
	CLuaClickThroughCtrl = "ui/common/LuaClickThroughCtrl",
	CLuaCJBMakeEnsureWnd = "ui/house/LuaCJBMakeEnsureWnd",
	LuaDoubleOneBonusTimeLimitWnd = "ui/doubleone2019/LuaDoubleOneBonusTimeLimitWnd",
	LuaDoubleOneBonusLotteryWnd = "ui/doubleone2019/LuaDoubleOneBonusLotteryWnd",
	LuaDoubleOneBonusLotteryRankWnd = "ui/doubleone2019/LuaDoubleOneBonusLotteryRankWnd",
	LuaDoubleOneExamWnd = "ui/doubleone2019/LuaDoubleOneExamWnd",
	LuaDoubleOneExamResultWnd = "ui/doubleone2019/LuaDoubleOneExamResultWnd",
	LuaDoubleOneBaotuanBeginWnd = "ui/doubleone2019/LuaDoubleOneBaotuanBeginWnd",
	LuaDoubleOneBaotuanResultWnd = "ui/doubleone2019/LuaDoubleOneBaotuanResultWnd",
	LuaDoubleOneBaotuanTipWnd = "ui/doubleone2019/LuaDoubleOneBaotuanTipWnd",
	LuaDoubleOneQLDShowWnd = "ui/doubleone2019/LuaDoubleOneQLDShowWnd",
	LuaDoubleOneQLDTopRightWnd = "ui/doubleone2019/LuaDoubleOneQLDTopRightWnd",
	LuaDoubleOneQLDResultWnd = "ui/doubleone2019/LuaDoubleOneQLDResultWnd",
	LuaDoubleOneQLDChangeSkillWnd = "ui/doubleone2019/LuaDoubleOneQLDChangeSkillWnd",
	LuaDoubleOneBaotuanTopRightWnd = "ui/doubleone2019/LuaDoubleOneBaotuanTopRightWnd",
	CLuaEquipWordAutoBaptizeTargetWnd = "ui/equip/xilian/LuaEquipWordAutoBaptizeTargetWnd",
	CLuaEquipWordAutoBaptizeConditionWnd = "ui/equip/xilian/LuaEquipWordAutoBaptizeConditionWnd",
	CLuaEquipWordBaptizeChooseWordWnd = "ui/equip/xilian/LuaEquipWordBaptizeChooseWordWnd",
	CLuaEquipXiLianMgr = "ui/equip/xilian/LuaEquipXiLianMgr",
	CLuaEquipWordAutoBaptizeWnd = "ui/equip/xilian/LuaEquipWordAutoBaptizeWnd",

	LuaCleanScreenWnd = "ui/LuaCleanScreenWnd",
	LuaDiZiWnd = "ui/LuaDiZiWnd",
	LuaPlantTreeWnd = "ui/LuaPlantTreeWnd",
	LuaDayToNightWnd = "ui/LuaDayToNightWnd",
	LuaFireCageWnd = "ui/LuaFireCageWnd",
	CLuaHongBaoRain = "ui/hongbao/LuaHongBaoRain",
	CLuaHongBaoWnd = "ui/hongbao/LuaHongBaoWnd",
	CLuaHongBaoItem = "ui/hongbao/LuaHongBaoItem",
	CLuaChristmasDisplayWnd = "ui/christmas2019/LuaChristmasDisplayWnd",
	CLuaGrabChristmasGiftWnd = "ui/christmas2019/LuaGrabChristmasGiftWnd",
	CLuaGrabChristmasGiftRankWnd = "ui/christmas2019/LuaGrabChristmasGiftRankWnd",
	CLuaGrabGiftTopRightWnd = "ui/christmas2019/LuaGrabGiftTopRightWnd",
	CLuaEditImageHandler = "ui/common/LuaEditImageHandler",
	LuaCustomizeImageEditor = "ui/common/LuaCustomizeImageEditor",
	LuaSanxingPipeiWnd = "ui/sanxinggameplay/LuaSanxingPipeiWnd",
	LuaSanxingResultWnd = "ui/sanxinggameplay/LuaSanxingResultWnd",
	LuaSanxingShowWnd = "ui/sanxinggameplay/LuaSanxingShowWnd",
	LuaSanxingTipWnd = "ui/sanxinggameplay/LuaSanxingTipWnd",
	LuaSanxingTopRightWnd = "ui/sanxinggameplay/LuaSanxingTopRightWnd",
	LuaSanxingTaskView = "ui/sanxinggameplay/LuaSanxingTaskView",
	LuaSanxingGuideWnd = "ui/sanxinggameplay/LuaSanxingGuideWnd",
	LuaSanxingRebornWnd = "ui/sanxinggameplay/LuaSanxingRebornWnd",
	LuaSanxingEndWnd = "ui/sanxinggameplay/LuaSanxingEndWnd",
	LuaSanxingChangeSkillWnd = "ui/sanxinggameplay/LuaSanxingChangeSkillWnd",
	LuaSchoolTaskWishingWnd = "ui/schooltask/LuaSchoolTaskWishingWnd",
	LuaSchoolTaskOptionWnd = "ui/schooltask/LuaSchoolTaskOptionWnd",
	LuaSchoolTaskTranscriptWnd = "ui/schooltask/LuaSchoolTaskTranscriptWnd",
	CLuaHouseBuffShowWnd = "ui/house/LuaHouseBuffShowWnd",
	CLuaHouseBuffListItem = "ui/house/LuaHouseBuffListItem",
	CLuaBuffListWnd = "ui/main/LuaBuffListWnd",
	CLuaBuffListItem = "ui/main/LuaBuffListItem",
	CLuaFurnitureSmallTypeWnd = "ui/house/edit/LuaFurnitureSmallTypeWnd",
	CLuaFurnitureSmallTypeItem = "ui/house/edit/LuaFurnitureSmallTypeItem",
	CLuaFurnitureBigTypeWnd = "ui/house/edit/LuaFurnitureBigTypeWnd",
	CLuaFurnitureColorWnd = "ui/house/edit/LuaFurnitureColorWnd",
	LuaGuildVoteWnd = "ui/LuaGuildVoteWnd",
	LuaGuildVoteEditWnd = "ui/LuaGuildVoteEditWnd",
	LuaGuildVoteDetailWnd = "ui/LuaGuildVoteDetailWnd",
	LuaGuildVoteSelectWnd = "ui/LuaGuildVoteSelectWnd",
	LuaGuildVoteThanksWnd = "ui/LuaGuildVoteThanksWnd",
	LuaLeafPicWnd = "ui/LuaLeafPicWnd",
	LuaSchoolContributionWnd = "ui/schoolcontribution/LuaSchoolContributionWnd",
	CLuaHouseChristmasGiftWnd = "ui/house/LuaHouseChristmasGiftWnd",
	LuaPlayerFashionAttributeWnd = "ui/appearance/LuaPlayerFashionAttributeWnd",
	CLuaChunJieSeriesTaskWnd = "ui/chunjie2020/LuaChunJieSeriesTaskWnd",
	CLuaChunJieSeriesTaskDescWnd = "ui/chunjie2020/LuaChunJieSeriesTaskDescWnd",
	CLuaChunJieSeriesTaskRewardWnd = "ui/chunjie2020/LuaChunJieSeriesTaskRewardWnd",
	CLuaTieChunLianResultWnd = "ui/chunjie2020/LuaTieChunLianResultWnd",
	CLuaTieChunLianRankWnd = "ui/chunjie2020/LuaTieChunLianRankWnd",
	CLuaTieChunLianTopRightWnd = "ui/chunjie2020/LuaTieChunLianTopRightWnd",
	CLuaCrossDouDiZhuRankWnd = "ui/doudizhu/cross/LuaCrossDouDiZhuRankWnd",
	CLuaCrossDouDiZhuApplyWnd = "ui/doudizhu/cross/LuaCrossDouDiZhuApplyWnd",
	CLuaCrossDouDiZhuVoteWnd = "ui/doudizhu/cross/LuaCrossDouDiZhuVoteWnd",
	CLuaCrossDouDiZhuShareWnd = "ui/doudizhu/cross/LuaCrossDouDiZhuShareWnd",
	CLuaCrossDouDiZhuResultWnd = "ui/doudizhu/cross/LuaCrossDouDiZhuResultWnd",
	CLuaCrossDouDiZhuRankResultWnd = "ui/doudizhu/cross/LuaCrossDouDiZhuRankResultWnd",
	LuaHuiLiuFriendWnd = "ui/huiliu/LuaHuiLiuFriendWnd",
	LuaHuiLiuMainWnd = "ui/huiliu/LuaHuiLiuMainWnd",
	LuaHuiLiuNewMainWnd = "ui/huiliu/LuaHuiLiuNewMainWnd",
	LuaHuiLiuTaskWnd = "ui/huiliu/LuaHuiLiuTaskWnd",
	LuaHuiLiuGiftWnd = "ui/huiliu/LuaHuiLiuGiftWnd",
	LuaHuiLiuFundWindow = "ui/huiliu/LuaHuiLiuFundWindow",
	LuaWishShopWnd = "ui/personalspace/LuaWishShopWnd",
	LuaWishPanel = "ui/personalspace/LuaWishPanel",
	LuaDetailWish = "ui/personalspace/LuaDetailWish",
	LuaWishAddWnd = "ui/personalspace/LuaWishAddWnd",
	LuaWishItemWnd = "ui/personalspace/LuaWishItemWnd",
	LuaWishWordWnd = "ui/personalspace/LuaWishWordWnd",
	LuaWishSelfFillWnd = "ui/personalspace/LuaWishSelfFillWnd",
	CLuaQYXTResultWnd = "ui/valentine2020/LuaQYXTResultWnd",
	CLuaQYXTSignUpWnd = "ui/valentine2020/LuaQYXTSignUpWnd",
	CLuaQYXTTopRightWnd = "ui/valentine2020/LuaQYXTTopRightWnd",
	CLuaQYXTTaskView = "ui/valentine2020/LuaQYXTTaskView",
	CLuaQYXTReliveWnd = "ui/valentine2020/LuaQYXTReliveWnd",
	LuaChooseProofWnd = "ui/zhujuejuqing/LuaChooseProofWnd",
	LuaRabbitJumpWnd = "ui/zhujuejuqing/LuaRabbitJumpWnd",
	LuaRabbitMakeDrugWnd = "ui/zhujuejuqing/LuaRabbitMakeDrugWnd",
	CLuaStarBiwuFinalWatchWnd = "ui/starbiwu/LuaStarBiwuFinalWatchWnd",
	LuaMessageListBoxWnd = "ui/common/LuaMessageListBoxWnd",
	CLuaWinterHouseUnlockPreviewWnd = "ui/house/LuaWinterHouseUnlockPreviewWnd",
	LuaMyDuoBaoPreviewer = "ui/yinliangduobao/LuaMyDuoBaoPreviewer",
	CLuaQingMingSignRankWnd = "ui/qingming2020/LuaQingMingSignRankWnd",
	CLuaQingMingRankWnd = "ui/qingming2020/LuaQingMingRankWnd",
	CLuaQingMingBattleWnd = "ui/qingming2020/LuaQingMingBattleWnd",
	CLuaQingMingJieZiXiaoChouWnd = "ui/qingming2020/LuaQingMingJieZiXiaoChouWnd",
	CLuaQingMingJieZiSucceedWnd = "ui/qingming2020/LuaQingMingJieZiSucceedWnd",
	CLuaQingMingFengWuZhiWnd = "ui/qingming2020/LuaQingMingFengWuZhiWnd",
	CLuaQingMingJieZiTipWnd = "ui/qingming2020/LuaQingMingJieZiTipWnd",
	CLuaRanFaRecipeGotWnd = "ui/ranfa/LuaRanFaRecipeGotWnd",
	CLuaRanFaJiBookWnd = "ui/ranfa/LuaRanFaJiBookWnd",
	CLuaRanFaJiMakeWnd = "ui/ranfa/LuaRanFaJiMakeWnd",
	CLuaRanFaRecipeChooseWnd = "ui/ranfa/LuaRanFaRecipeChooseWnd",
	CLuaRanFaMainWnd = "ui/ranfa/LuaRanFaMainWnd",
	LuaKeJuOptionTemplate = "ui/keju/LuaKeJuOptionTemplate",
	LuaDebitNoteWnd = "ui/juqing/LuaDebitNoteWnd",
	LuaScreenBloodFogWnd = "ui/juqing/LuaScreenBloodFogWnd",
	LuaPetCatWnd = "ui/juqing/LuaPetCatWnd",
	LuaObservingSoulDetailTipWnd = "ui/juqing/LuaObservingSoulDetailTipWnd",
	LuaCocoonBreakWnd = "ui/juqing/LuaCocoonBreakWnd",
	LuaMakeClothesPatternWnd = "ui/juqing/makeclothes/LuaMakeClothesPatternWnd",
	LuaClothesPatternView = "ui/juqing/makeclothes/LuaClothesPatternView",
	LuaMakeClothesResultView = "ui/juqing/makeclothes/LuaMakeClothesResultView",
	LuaChildrenDayPlayChoose2020Wnd = "ui/childrenday2020/LuaChildrenDayPlayChoose2020Wnd",
	LuaChildrenDayPlayChooseBall2020Wnd = "ui/childrenday2020/LuaChildrenDayPlayChooseBall2020Wnd",
	LuaChildrenDayPlayFightInfo2020Wnd = "ui/childrenday2020/LuaChildrenDayPlayFightInfo2020Wnd",
	LuaChildrenDayPlayResult2020Wnd = "ui/childrenday2020/LuaChildrenDayPlayResult2020Wnd",
	LuaChildrenDayPlayShow2020Wnd = "ui/childrenday2020/LuaChildrenDayPlayShow2020Wnd",
	LuaChildrenDayPlayTopRight2020Wnd = "ui/childrenday2020/LuaChildrenDayPlayTopRight2020Wnd",
	LuaPUBGStartWnd = "ui/pubg/LuaPUBGStartWnd",
	LuaPUBGSelfPackageWnd = "ui/pubg/LuaPUBGSelfPackageWnd",
	LuaPUBGSelfPackageView = "ui/pubg/LuaPUBGSelfPackageView",
	LuaPUBGLickingBagWnd = "ui/pubg/LuaPUBGLickingBagWnd",
	LuaPUBGSkillSetWnd = "ui/pubg/LuaPUBGSkillSetWnd",
	LuaPUBGResultWnd = "ui/pubg/LuaPUBGResultWnd",
	LuaMiniMapPopRegionsView = "ui/pubg/LuaMiniMapPopRegionsView",
	LuaChiJiTaskView = "ui/pubg/LuaChiJiTaskView",
	LuaShiTuNewTrainingHandbookWnd = "ui/shitu/LuaShiTuNewTrainingHandbookWnd",
	LuaShiTuWenDaoView = "ui/shitu/LuaShiTuWenDaoView",
	LuaShiTuWenDaoRewardWnd = "ui/shitu/LuaShiTuWenDaoRewardWnd",
	LuaShiTuTrainingHandbookWnd = "ui/shitu/LuaShiTuTrainingHandbookWnd",
	LuaShiTuTrainingPlanView = "ui/shitu/LuaShiTuTrainingPlanView",
	LuaShiTuWeeklyHomeworkView = "ui/shitu/LuaShiTuWeeklyHomeworkView",
	LuaShiTuZunShiZhongDaoView = "ui/shitu/LuaShiTuZunShiZhongDaoView",
	LuaTuDiPlayerListWnd = "ui/shitu/LuaTuDiPlayerListWnd",
	CLuaDiYuShiKongWnd = "ui/wuyi2020/LuaDiYuShiKongWnd",
	CLuaDiYuShiKongStateWnd = "ui/wuyi2020/LuaDiYuShiKongStateWnd",
	CLuaQMHuoYunSubWnd = "ui/wuyi2020/LuaQMHuoYunSubWnd",
	CLuaQMHuoYunFHLStateWnd = "ui/wuyi2020/LuaQMHuoYunFHLStateWnd",
	CLuaQMHuoYunFHLResultWnd = "ui/wuyi2020/LuaQMHuoYunFHLResultWnd",
	CLuaDwWusesiHeCheng2020Wnd = "ui/duanwu2020/LuaDwWusesiHeCheng2020Wnd",
	CLuaDwWusesiTaben2020Wnd = "ui/duanwu2020/LuaDwWusesiTaben2020Wnd",
	LuaJiuGeShiHunResultWnd = "ui/duanwu2020/LuaJiuGeShiHunResultWnd",
	LuaJiuGeShiHunTopAndRightWnd = "ui/duanwu2020/LuaJiuGeShiHunTopAndRightWnd",
	LuaDuanWu2020TaskBoardView = "ui/duanwu2020/LuaDuanWu2020TaskBoardView",
	CLuaChargeGuideWnd = "ui/charge/LuaChargeGuideWnd",
	LuaServerMaintenanceWnd = "ui/login/LuaServerMaintenanceWnd",
	LuaButterflyCrisisNoticeWnd = "ui/butterflycrisis/LuaButterflyCrisisNoticeWnd",
	LuaButterflyCrisisMainWnd = "ui/butterflycrisis/LuaButterflyCrisisMainWnd",
	LuaButterflyCrisisStoryWnd = "ui/butterflycrisis/LuaButterflyCrisisStoryWnd",
	LuaButterflyCrisisRMBWnd = "ui/butterflycrisis/LuaButterflyCrisisRMBWnd",
	LuaButterflyCrisisRewardWnd = "ui/butterflycrisis/LuaButterflyCrisisRewardWnd",
	CLuaTiaoXiangPlayWnd = "ui/juqing/LuaTiaoXiangPlayWnd",
	LuaTiaoXiangPlayMgr = "ui/juqing/LuaTiaoXiangPlayMgr",
	LuaHouseUnlockSkyboxWnd = "ui/house/LuaHouseUnlockSkyboxWnd",
	LuaNewYearGameView = "ui/newyear2019/LuaNewYearGameView",
	CLuaLaternEffect = "ui/hongbao/LuaLaternEffect",
	LuaAccountTransferWnd = "ui/accounttransfer/LuaAccountTransferWnd",
    CLuaGuildRenameWnd = "ui/guild/LuaGuildRenameWnd",
	CLuaHouseScreenshotReportWnd = "ui/house/LuaHouseScreenshotReportWnd",
	CLuaHouseScreenshotReportMgr = "ui/house/LuaHouseScreenshotReportWnd",

	CLuaHouseMinimap = "ui/house/LuaHouseMinimap",
	CLuaDialogWnd = "ui/main/dialog/LuaDialogWnd",
	CLuaOtherBabyInfoWnd = "ui/baby/LuaOtherBabyInfoWnd",
	LuaNewWelfareWnd = "ui/welfare/LuaNewWelfareWnd",
	CLuaHuaBiResultWnd = "ui/huabi/LuaHuaBiResultWnd",
	CLuaTaskDialogWnd = "ui/main/dialog/LuaTaskDialogWnd",
	LuaDialogCameraMgr = "ui/main/dialog/LuaDialogCameraMgr",
	CLuaFriendPopupMenu = "ui/main/LuaFriendPopupMenu",
	LuaPetInfoWnd = "ui/lingshou/LuaPetInfoWnd",

	CLuaBWDHMatchWnd = "ui/biwudahui/LuaBWDHMatchWnd",
	CLuaFurnitureRightTopWnd = "ui/house/LuaFurnitureRightTopWnd",
	CLuaDanmuWnd = "ui/common/LuaDanmuWnd",
	CLuaFightingSpiritDamageWnd = "ui/common/guanzhan/LuaFightingSpiritDamageWnd",
	CLuaExpressionActionSettingWnd = "ui/expression/LuaExpressionActionSettingWnd",
	LuaCommonKillInfoTopWnd = "ui/common/zhankuang/LuaCommonKillInfoTopWnd",
	
	LuaLeiGuView = "ui/common/guanzhan/LuaLeiGuView",
	LuaStarBiwuBuyLeiGuTimesWnd = "ui/starbiwu/LuaStarBiwuBuyLeiGuTimesWnd",
	LuaStarBiWuGiftRankWnd = "ui/starbiwu/LuaStarBiWuGiftRankWnd",
	LuaStarBiWuGiftContributionRankWnd = "ui/starbiwu/LuaStarBiWuGiftContributionRankWnd",

	LuaQMPKTopFourWnd = "ui/quanminpk/LuaQMPKTopFourWnd",

	CLuaHandWriteEffectWnd = "ui/zhujuejuqing/LuaHandWriteEffectWnd",
	CLuaHandWriteEffectMgr = "ui/zhujuejuqing/LuaHandWriteEffectWnd",
	CLuaOfflineItemWnd = "ui/offlineitem/LuaOfflineItemWnd",
	CLuaActivityAlert = "ui/main/activity/LuaActivityAlert",
	CLuaActivityLeftTimeWnd = "ui/main/activity/LuaActivityLeftTimeWnd",

	CLuaWorldCupJingCaiDetailWnd = "ui/worldcup/LuaWorldCupJingCaiDetailWnd",
	CLuaServerSubmitWnd = "ui/LuaServerSubmitWnd",

	CLuaHuaZhuWnd = "ui/baby/LuaHuaZhuWnd",
	CLuaCityWarZiCaiShopWnd = "ui/citywar/LuaCityWarZiCaiShopWnd",
	CLuaEquipKeZiWnd = "ui/equip/LuaEquipKeZiWnd",
	CLuaNpcFightingWnd = "ui/LuaNpcFightingWnd",
	CLuaBuffItemWnd = "ui/yaoxia/LuaBuffItemWnd",
	CLuaUITriggerWnd = "ui/juqing/LuaUITriggerWnd",
	LuaCrazyDyeTopRightWnd = "ui/halloween2019/LuaCrazyDyeTopRightWnd",
	CLuaBugWeiChangTopRightWnd = "ui/halloween2019/LuaBugWeiChangTopRightWnd",
	CLuaHalloweenRescueWnd = "ui/halloween2019/LuaHalloweenRescueWnd",
	LuaCrazyDyeBossStateWnd = "ui/halloween2019/LuaCrazyDyeBossStateWnd",
	LuaCrazyDyeResultWnd = "ui/halloween2019/LuaCrazyDyeResultWnd",
	LuaJigsawWnd = "ui/halloween2019/LuaJigsawWnd",
	LuaYingLingShapeShiftingView = "ui/skill/LuaYingLingShapeShiftingView",
	CLuaRescueChristmasTreeStateWnd = "ui/christmas2019/LuaRescueChristmasTreeStateWnd",
	CLuaGuideRegionWnd = "ui/guide/LuaGuideRegionWnd",
	CLuaNPCXuanMeiMgr = "ui/npcxuanmei/LuaNPCXuanMeiWnd",
	CLuaNPCXuanMeiWnd = "ui/npcxuanmei/LuaNPCXuanMeiWnd",
	CLuaQMPKRegisterWnd = "ui/quanminpk/LuaQMPKRegisterWnd",
	LuaItemWithDetailMessageChooseWnd = "ui/common/LuaItemWithDetailMessageChooseWnd",
	LuaItemChooseWndMgr = "ui/common/LuaItemWithDetailMessageChooseWnd",
	LuaObservingSoulWnd = "ui/juqing/LuaObservingSoulWnd",
	LuaObservingSoulMgr = "ui/juqing/LuaObservingSoulWnd",
	LuaMessageBoxTimeLimitWnd = "ui/common/LuaMessageBoxTimeLimitWnd",
	LuaMessageBoxTimeLimitWndMgr = "ui/common/LuaMessageBoxTimeLimitWnd",

	CLuaCommonConfirmWithInfoWnd = "ui/common/LuaCommonConfirmWithInfoWnd",
	CLuaCommonConfirmWithInfoWndMgr = "ui/common/LuaCommonConfirmWithInfoWnd",
	CLuaWarehouseMenu = "ui/main/LuaWarehouseMenu",
	CLuaWarehouseMenuMgr = "ui/main/LuaWarehouseMenu",


	CLuaPlayerReportMgr = "ui/common/LuaPlayerReportWnd",
	CLuaPlayerReportWnd = "ui/common/LuaPlayerReportWnd",

	LuaBackBonusMgr = "ui/backbonus/LuaBackBonusWnd",
	LuaBackBonusWnd = "ui/backbonus/LuaBackBonusWnd",
	LuaMessageTipMgr = "ui/common/LuaMessageTipWithButtonWnd",
	LuaMessageTipWithButtonWnd = "ui/common/LuaMessageTipWithButtonWnd",
	LuaStarBiwuJingCaiItem = "ui/starbiwu/LuaStarBiwuJingCaiItem",
	LuaStarBiwuJingCaiWnd = "ui/starbiwu/LuaStarBiwuJingCaiWnd",
	CLuaStarBiwuGroupMatchWnd = "ui/starbiwu/LuaStarBiwuGroupMatchWnd",
	CLuaOptionMgr = "ui/common/LuaOptionDlg",
	CLuaOptionDlg = "ui/common/LuaOptionDlg",
	LuaEyeCatchTheDogWnd = "ui/juqing/LuaEyeCatchTheDogWnd",
	LuaEyeCatchTheDogMgr = "ui/juqing/LuaEyeCatchTheDogWnd",
	LuaHeShengZiJinWnd = "ui/juqing/LuaHeShengZiJinWnd",
	LuaMakeBeautificationWnd = "ui/juqing/LuaMakeBeautificationWnd",
	LuaMakeBeautificationWndMgr = "ui/juqing/LuaMakeBeautificationWnd",

	LuaMakeClothesWnd = "ui/juqing/makeclothes/LuaMakeClothesWnd",
	LuaMakeClothesMgr = "ui/juqing/makeclothes/LuaMakeClothesWnd",
	LuaChildrenDay2020Mgr = 'ui/childrenday2020/LuaChildrenDay2020Mgr',
	CLuaNewYear2019CunQianGuanWnd = "ui/newyear2019/LuaNewYear2019CunQianGuanWnd",

	CLuaCrossSXDDZChampionConfirmWnd = "ui/sxddz/LuaCrossSXDDZChampionConfirmWnd",

	CLuaWorldCupRenRenZhongCaiPiaoWnd = "ui/worldcup/LuaWorldCupRenRenZhongCaiPiaoWnd",
	CLuaEquipTip = "ui/equip/LuaEquipTip",
	LuaGQJCStateWnd = "ui/gqjc/LuaGQJCStateWnd",
	CLuaCommonItemListMgr = "ui/common/LuaCommonItemListMgr",
	CLuaQMPKJiXiangWuWnd = "ui/quanminpk/jixiangwu/LuaQMPKJiXiangWuWnd",
	CBlowBubblesWnd = "ui/liuyi/LuaBlowBubblesWnd",
	CLuaLetterDisplayMgr = "ui/common/LuaLetterContentDisplayWnd",
	LuaLetterContentDisplayWnd = "ui/common/LuaLetterContentDisplayWnd",
	CLuaFashionGiftMgr = "ui/welfare/LuaFashionGiftView",
	CLuaFashionGiftView = "ui/welfare/LuaFashionGiftView",
	LuaCarnival2020View = "ui/welfare/LuaCarnival2020View",
	LuaHuiLiuFuliWindow = "ui/welfare/LuaHuiLiuFuliWindow",

	CLuaDwMgr2019 = "ui/duanwu2019/LuaDwMgr2019",
	LuaStarInviteMgr = 'ui/starinvitegame/LuaStarInviteMgr',
	LuaButterflyCrisisMgr = "ui/butterflycrisis/LuaButterflyCrisisMgr",
	CLuaQMHuoYunMgr = "ui/wuyi2020/LuaQMHuoYunMgr",
	LuaPUBGMgr = "ui/pubg/LuaPUBGMgr",
	CLuaRanFaMgr = "ui/ranfa/LuaRanFaMgr",
	CLuaQingMing2020Mgr = "ui/qingming2020/LuaQingMing2020Mgr",
	CLuaWinterHouseUnlockPreviewMgr = "ui/house/LuaWinterHouseUnlockPreviewMgr",
	CLuaQYXTMgr = "ui/valentine2020/LuaQYXTMgr",
	CLuaBindRepoMgr = "ui/main/LuaBindRepoMgr",
	CLuaWarehouseMgr = "ui/main/LuaWarehouseMgr",
	LuaYunyingfuliMgr = "ui/welfare/LuaYunyingfuliMgr",
	CLuaZhuShaBiXiLianMgr = "ui/zhushabi/LuaZhuShaBiXiLianMgr",
	CLuaQiXiMgr = "ui/qixi/LuaQiXiMgr",
	CLuaChengZhongMgr = "ui/bodyweight/LuaChengZhongMgr",
	CLuaGuanNingWuShenMgr = "ui/guanning/LuaGuanNingWuShenMgr",
	CLuaEquipZhouNianQingMgr = "ui/equip/zhounianqing/LuaEquipZhouNianQingMgr",
	CLuaRescueChristmasTreeMgr = "ui/christmas2019/LuaRescueChristmasTreeMgr",
	CLuaChristmasDisplayWndMgr = "ui/christmas2019/LuaChristmasDisplayWndMgr",
	LuaGrabChristmasGiftMgr = "ui/christmas2019/LuaGrabChristmasGiftMgr",
	CLuaSchoolContributionMgr = "ui/schoolcontribution/LuaSchoolContributionMgr",
	g_LuaSchoolContributionMgr = "ui/schoolcontribution/LuaSchoolContributionMgr",
	CLuaShopMallMgr = "ui/shopmall/LuaShopMallMgr",
	LuaCookingPlayMgr = "ui/cooking/LuaCookingPlayMgr",
	EnumCookingPlayType = "ui/cooking/LuaCookingPlayMgr",
	CLuaJuQingDialogMgr = "ui/main/dialog/LuaJuQingDialogMgr",
	LuaNewYear2019Mgr = "ui/newyear2019/LuaNewYear2019Mgr",
	LuaMPTZMgr = "ui/mptz/LuaMPTZMgr",
	CLuaWorldCupMgr = "ui/worldcup/LuaWorldCupMgr",
	LuaWuCaiShaBingMgr = "ui/wucaishabing/LuaWuCaiShaBingMgr",
	CLuaXuanZhuanMuMaMgr = "ui/baby/LuaXuanZhuanMuMaMgr",

	LuaShiTuMgr = "ui/shitu/LuaShiTuMgr",
	LuaDoubleOne2019Mgr = "ui/doubleone2019/LuaDoubleOne2019Mgr",
	LuaDanMuMgr = "ui/common/LuaDanMuMgr",
	LuaWuJianDiYuMgr = "ui/wujiandiyu/LuaWuJianDiYuMgr",
	LuaChildrenDay2019Mgr = "ui/childrenday2019/LuaChildrenDay2019Mgr",
	CLuaNpcHaoGanDuMgr="ui/npchaogandu/LuaNpcHaoGanDuMgr",

	LuaXYLPWnd = "ui/zhongyuanjie2020/LuaXYLPWnd",
	LuaXYLPTipWnd = "ui/zhongyuanjie2020/LuaXYLPTipWnd",
	LuaDesireMirrorWnd  = "ui/zhongyuanjie2020/LuaDesireMirrorWnd",

	LuaCommonRankWnd  = "ui/common/LuaCommonRankWnd",
	LuaCommonRankWndMgr = "ui/common/LuaCommonRankWnd",

	CLuaCrossDouDiZhuMgr = "ui/doudizhu/LuaCrossDouDiZhuMgr",
	CLuaFightingSpiritMgr = "ui/fightingspirit/LuaFightingSpiritMgr",
	CLuaMergeBattleMgr = "ui/mergebattle/LuaMergeBattleMgr",
	CLuaGuanNingMgr = "ui/guanning/LuaGuanNingMgr",

    LuaItemExchange4To1Wnd = "ui/common/LuaItemExchange4To1Wnd",

	LuaSpokesmanHireContractWnd = "ui/spokesman/LuaSpokesmanHireContractWnd",
	LuaSpokesmanCardsWnd = "ui/spokesman/LuaSpokesmanCardsWnd",
	LuaSpokesmanDialogWnd = "ui/spokesman/LuaSpokesmanDialogWnd",
	LuaSpokesmanPreviewWnd = "ui/spokesman/LuaSpokesmanPreviewWnd",
	LuaSpokesmanCardsComposeWnd = "ui/spokesman/LuaSpokesmanCardsComposeWnd",
	LuaSpokesmanCardsComposeResultWnd = "ui/spokesman/LuaSpokesmanCardsComposeResultWnd",
	LuaSpokesmanCardTemplate = "ui/spokesman/LuaSpokesmanCardTemplate",
	CLuaSpokesmanExpressionWnd = "ui/house/spokesman/LuaSpokesmanExpressionWnd",
	CLuaSpokesmanPawnshopWnd = "ui/house/spokesman/LuaSpokesmanPawnShopWnd",
	CLuaSpokesmanPawnCertificateWnd = "ui/house/spokesman/LuaSpokesmanPawnCertificateWnd",

	CLuaHouseWoodPileDataHistoryWnd = "ui/house/LuaHouseWoodPileDataHistoryWnd",
	CLuaSpokesmanHouseZhongChouWnd = "ui/house/LuaSpokesmanHouseZhongChouWnd",

	CLuaDiekePossessWnd = "ui/LuaDiekePossessWnd",

	--家园装饰物模板
    CLuaZswTemplateEditWnd =  "ui/jyzhuangshiwutemplate/LuaZswTemplateEditWnd",
    CLuaZswTemplateFillingComfirmWnd = "ui/jyzhuangshiwutemplate/LuaZswTemplateFillingComfirmWnd",
    CLuaZswTemplateMgr = "ui/jyzhuangshiwutemplate/LuaZswTemplateMgr",
	CLuaZswTemplateTopRightWnd = "ui/jyzhuangshiwutemplate/LuaZswTemplateTopRightWnd",
	CLuaClientFurniture = "ui/house/LuaClientFurniture",
	CLuaZswTemplatePreviewWnd = "ui/jyzhuangshiwutemplate/LuaZswTemplatePreviewWnd",
	CLuaZswTemplateComponentListWnd = "ui/jyzhuangshiwutemplate/LuaZswTemplateComponentListWnd",

	LuaSpokesmanZhouBianWindow = "ui/spokesman/LuaSpokesmanZhouBianWindow",
	LuaZhuanZhiFundWindow = "ui/welfare/LuaZhuanZhiFundWindow",

	CLuaPickerWnd = "ui/common/LuaPickerWnd",

	CLuaPlayerPuppetInfoWnd = "ui/house/puppet/LuaPlayerPuppetInfoWnd",
	CLuaPuppetDetailPropertySection = "ui/house/puppet/LuaPuppetDetailPropertySection",
	CLuaCPPuppetInfoWnd = "ui/house/puppet/LuaCPPuppetInfoWnd",

	CLuaFreightCargoList = "ui/guild/huoyun/LuaFreightCargoList",
	CLuaFreightWnd = "ui/guild/huoyun/LuaFreightWnd",
	CLuaFreightTransport = "ui/guild/huoyun/LuaFreightTransport",
	CLuaFreightCargoInfo = "ui/guild/huoyun/LuaFreightCargoInfo",

	CLuaAutoPickupWnd = "ui/LuaAutoPickupWnd",

	LuaCarnival2020VipTickWnd = 'ui/welfare/LuaCarnival2020VipTickWnd',
	LuaCarnival2020RealNameWnd = 'ui/welfare/LuaCarnival2020RealNameWnd',
	LuaQiugouWnd = 'ui/qiugou/LuaQiugouWnd',
	LuaQiugouBuyWnd = 'ui/qiugou/LuaQiugouBuyWnd',
	LuaQiugouSellWnd = 'ui/qiugou/LuaQiugouSellWnd',
	CLuaDouDiZhu2020ApplyWnd = "ui/doudizhu/2020/LuaDouDiZhu2020ApplyWnd",
	CLuaDouDiZhu2020RankWnd = "ui/doudizhu/2020/LuaDouDiZhu2020RankWnd",
	CLuaDouDiZhu2020ResultWnd = "ui/doudizhu/2020/LuaDouDiZhu2020ResultWnd",

	-- 展示家园装饰物预览
	CLuaZhuangshiwuPreviewWnd = "ui/house/LuaZhuangshiwuPreviewWnd",
	CLuaZhuangshiwuPreviewMgr = "ui/house/LuaZhuangshiwuPreviewMgr",

	-- 双十一活动
	CLuaShuangshiyi2020ZhongCaoWnd = "ui/shuangshiyi2020/LuaShuangshiyi2020ZhongCaoWnd",
	CLuaShuangshiyi2020FashionPreviewWnd = "ui/shuangshiyi2020/LuaShuangshiyi2020FashionPreviewWnd",
	CLuaShuangshiyi2020VoucherWnd = "ui/shuangshiyi2020/LuaShuangshiyi2020VoucherWnd",
	CLuaShuangshiyi2020ClearTrolleyResultWnd = "ui/shuangshiyi2020/LuaShuangshiyi2020ClearTrolleyResultWnd",
	CLuaShuangshiyi2020ClearTrolleyEnterWnd = "ui/shuangshiyi2020/LuaShuangshiyi2020ClearTrolleyEnterWnd",
	CLuaShuangshiyi2020ClearTrolleyPlayingWnd = "ui/shuangshiyi2020/LuaShuangshiyi2020ClearTrolleyPlayingWnd",
	CLuaShuangshiyi2020LotteryWnd = "ui/shuangshiyi2020/LuaShuangshiyi2020LotteryWnd",
	CLuaShuangshiyi2020LotteryRankWnd = "ui/shuangshiyi2020/LuaShuangshiyi2020LotteryRankWnd",
	CLuaShuangshiyi2020Mgr = "ui/shuangshiyi2020/LuaShuangshiyi2020Mgr",
	CLuaShuangshiyi2020LotteryRewardWnd = "ui/shuangshiyi2020/LuaShuangshiyi2020LotteryRewardWnd",
	CLuaShuangshiyi2020FashionDescription = "ui/shuangshiyi2020/LuaShuangshiyi2020FashionDescription",

	CLuaEquipRemindWnd = "ui/main/LuaEquipRemindWnd",

	LuaCustomGridHPBar = "ui/common/LuaCustomGridHPBar",

	LuaPlayerHeadInfoItemDieKeProgress = "ui/background/LuaPlayerHeadInfoItemDieKeProgress",
	LuaPlayerHeadInfoItem = "ui/background/LuaPlayerHeadInfoItem",

	--2020万圣节
	CLuaMengGuiJieSignWnd = "ui/halloween2020/LuaMengGuiJieSignWnd",
	CLuaMengGuiJiePiPeiWnd = "ui/halloween2020/LuaMengGuiJiePiPeiWnd",
	CLuaMengGuiJieTipWnd = "ui/halloween2020/LuaMengGuiJieTipWnd",
	CLuaMengGuiJieResultWnd = "ui/halloween2020/LuaMengGuiJieResultWnd",
	CLuaDaoDanTipEnterWnd = "ui/halloween2020/LuaDaoDanTipEnterWnd",
	CLuaMengGuiJieTaskView = "ui/halloween2020/LuaMengGuiJieTaskView",
	CLuaTerrifyItemUseWnd = "ui/halloween2020/LuaTerrifyItemUseWnd",
	CLuaMengGuiJieSkillTipWnd = "ui/halloween2020/LuaMengGuiJieSkillTipWnd",
	CLuaTerrifyItemUseEntryWnd = "ui/halloween2020/LuaTerrifyItemUseEntryWnd",

	CLuaNumberInputMgr = "ui/LuaNumberInputBox",
	CLuaNumberInputBox = "ui/LuaNumberInputBox",
	CLuaFrozenSilverWnd = "ui/LuaFrozenSilverWnd",
	CLuaRedDotForGuild = "ui/main/LuaRedDotForGuild",
	CLuaRedDotForSkill = "ui/main/LuaRedDotForSkill",

	--安期岛之战
	CLuaAnQiDaoStateWnd = "ui/anqidao/LuaAnQiDaoStateWnd",
	CLuaAnQiDaoTipWnd = "ui/anqidao/LuaAnQiDaoTipWnd",
	CLuaAnQiDaoResultWnd = "ui/anqidao/LuaAnQiDaoResultWnd",
	CLuaAnQiDaoSignalWnd = "ui/anqidao/LuaAnQiDaoSignalWnd",
	CLuaAnQiDaoMgr = "ui/anqidao/LuaAnQiDaoMgr",

	-- 仙职窗口迭代
	CLuaXianZhiExtendWnd = "ui/xianzhi/LuaXianZhiExtendWnd",

	-- 2021元宵节
	LuaYuanXiao2021Mgr = "ui/yuanxiao2021/LuaYuanXiao2021Mgr",
	LuaYuanXiao2021QiYuanWnd = "ui/yuanxiao2021/LuaYuanXiao2021QiYuanWnd",
	LuaYuanXiao2021TangYuanSettlementWnd = "ui/yuanxiao2021/LuaYuanXiao2021TangYuanSettlementWnd",
	LuaYuanXiao2021TangYuanEnterWnd = "ui/yuanxiao2021/LuaYuanXiao2021TangYuanEnterWnd",
	LuaYuanXiao2021TangYuanBoardView = "ui/yuanxiao2021/LuaYuanXiao2021TangYuanBoardView",

	CLuaYanHuaDesignWnd = "ui/yanhuaeditor/LuaYanHuaDesignWnd",
	CLuaYanHuaEditorMgr = "ui/yanhuaeditor/LuaYanHuaEditorMgr",
	CLuaYanHuaLiBaoEditorWnd = "ui/yanhuaeditor/LuaYanHuaLiBaoEditorWnd",
	CLuaCameraResetWnd = "ui/yanhuaeditor/LuaCameraResetWnd",
	LuaYanHuaViewWnd = "ui/yanhuaeditor/LuaYanHuaViewWnd",
	CLuaYanHuaReportWnd = "ui/yanhuaeditor/LuaYanHuaReportWnd",
	CLuaYanHuaSinglePreviewWnd = "ui/yanhuaeditor/LuaYanHuaSinglePreviewWnd",

	-- 2021寒假 天成酒壶
	CLuaTcjhMainWnd 		= "ui/hanjia2021/LuaTcjhMainWnd",
	CLuaTcjhMainRewardTab 	= "ui/hanjia2021/LuaTcjhMainRewardTab",
	CLuaTcjhMainTaskTab 	= "ui/hanjia2021/LuaTcjhMainTaskTab",
	CLuaTcjhRewardWnd 		= "ui/hanjia2021/LuaTcjhRewardWnd",
	CLuaTcjhBuyAdvPassWnd 	= "ui/hanjia2021/LuaTcjhBuyAdvPassWnd",
	CLuaTcjhBuyLevelWnd 	= "ui/hanjia2021/LuaTcjhBuyLevelWnd",
	CLuaTcjhRideSelectWnd 	= "ui/hanjia2021/LuaTcjhRideSelectWnd",

	LuaYuanDanShareWnd = 'ui/yuandan/LuaYuanDanShareWnd',

	LuaPetAdventureStartWnd = "ui/hanjia2021/LuaPetAdventureStartWnd",
	LuaPetAdventureStateWnd = "ui/hanjia2021/LuaPetAdventureStateWnd",
	LuaPetAdventureResultWnd = "ui/hanjia2021/LuaPetAdventureResultWnd",
	CLuaChristmasCreateSnowmanStateWnd = "ui/Christmas/LuaChristmasCreateSnowmanStateWnd",

	CLuaQingRenJie2021RankWnd = "ui/qingrenjie2021/LuaQingRenJie2021RankWnd",
	CLuaQingRenJie2021EntryWnd = "ui/qingrenjie2021/LuaQingRenJie2021EntryWnd",
	CLuaQingRenJie2021PlayStatusWnd = "ui/qingrenjie2021/LuaQingRenJie2021PlayStatusWnd",
	CLuaQingRenJie2021Mgr =  "ui/qingrenjie2021/LuaQingRenJie2021Mgr",
	-- 125技能物品合成
	LuaCompound125SkillMgr = "ui/skill/LuaCompound125SkillMgr",
	LuaCompound125SkillItemWnd = "ui/skill/LuaCompound125SkillItemWnd",

	LuaVehicleRenewalMgr = "ui/zuoqi/LuaVehicleRenewalWnd",
	LuaVehicleRenewalWnd = "ui/zuoqi/LuaVehicleRenewalWnd",
	LuaVehicleBatchRenewalFeesWnd = "ui/zuoqi/LuaVehicleBatchRenewalFeesWnd",
	
	--自立师门相关
	CLuaSoulCoreWnd = "ui/zongmen/LuaSoulCoreWnd",
	CLuaSoulCoreInfoView = "ui/zongmen/LuaSoulCoreInfoView",
	LuaAntiProfessionView = "ui/zongmen/LuaAntiProfessionView",
	CLuaOtherSoulCoreWnd = "ui/zongmen/LuaOtherSoulCoreWnd",
	CLuaSoulCoreCondenseWnd = "ui/zongmen/LuaSoulCoreCondenseWnd",
	LuaZongMenCreateIntroductionWnd  = "ui/zongmen/LuaZongMenCreateIntroductionWnd",
	LuaZongMenCreationWnd = "ui/zongmen/LuaZongMenCreationWnd",
	LuaZongMenTuDiJoiningWnd = "ui/zongmen/LuaZongMenTuDiJoiningWnd",
	LuaMingGeJianDingWnd = "ui/zongmen/LuaMingGeJianDingWnd",
	LuaWuXingMingGeWnd = "ui/zongmen/LuaWuXingMingGeWnd",
	LuaZongMenSearchWnd = "ui/zongmen/LuaZongMenSearchWnd",
	LuaZongMenGuiZeWnd = "ui/zongmen/LuaZongMenGuiZeWnd",
	LuaZongMenJoiningWnd = "ui/zongmen/LuaZongMenJoiningWnd",
	LuaInviteToJoinZongMenWnd = "ui/zongmen/LuaInviteToJoinZongMenWnd",
	LuaZongMenTopRightWnd = "ui/zongmen/LuaZongMenTopRightWnd",
	LuaZongMenMainWnd = "ui/zongmen/LuaZongMenMainWnd",
	LuaZongMenInfoView = "ui/zongmen/LuaZongMenInfoView",
	LuaZongMenSkillView = "ui/zongmen/LuaZongMenSkillView",
	LuaZongMenChengWeiWnd = "ui/zongmen/LuaZongMenChengWeiWnd",
	LuaZongMenWeiJieWnd = "ui/zongmen/LuaZongMenWeiJieWnd",
	LuaZongMenRenMianMessageBoxWnd = "ui/zongmen/LuaZongMenRenMianMessageBoxWnd",
	LuaZongMenJurisdictionSettingWnd = "ui/zongmen/LuaZongMenJurisdictionSettingWnd",
	LuaZongMenHistoryWnd = "ui/zongmen/LuaZongMenHistoryWnd",
	LuaZongMenMemberView = "ui/zongmen/LuaZongMenMemberView",
	LuaZongMenChangeWeiJieWnd = "ui/zongmen/LuaZongMenChangeWeiJieWnd",
	LuaZongMenJusticeContributeWnd = "ui/zongmen/LuaZongMenJusticeContributeWnd",
	LuaZongMenJusticeContributeChooseWnd = "ui/zongmen/LuaZongMenJusticeContributeChooseWnd",
	LuaZhuoYaoMainWnd = "ui/zongmen/LuaZhuoYaoMainWnd",
	LuaYaoGuaiInfoWnd = "ui/zongmen/LuaYaoGuaiInfoWnd",
	LuaZhuoYaoTuJianView = "ui/zongmen/LuaZhuoYaoTuJianView",
	LuaZhuoYaoWarehouseView = "ui/zongmen/LuaZhuoYaoWarehouseView",
	LuaWuXingAvoidSettingWnd = "ui/zongmen/LuaWuXingAvoidSettingWnd",
	LuaGuardZongMenWnd = "ui/zongmen/LuaGuardZongMenWnd",
	LuaShiMenRuinedWnd = "ui/zongmen/LuaShiMenRuinedWnd",
	LuaShiMenRuQinTaskView = "ui/zongmen/LuaShiMenRuQinTaskView",
	LuaGuardShiMenRankWnd = "ui/zongmen/LuaGuardShiMenRankWnd",
	LuaSectEntranceButtonWnd = "ui/zongmen/LuaSectEntranceButtonWnd",
	LuaZongMenChangeNameWnd = "ui/zongmen/LuaZongMenChangeNameWnd",
	LuaZongMenChangeSkyboxWnd = "ui/zongmen/LuaZongMenChangeSkyboxWnd",
	LuaChangeRuMenBiaoZhunWnd = "ui/zongmen/LuaChangeRuMenBiaoZhunWnd",
	-- 炼化相关
	LuaRefineMonsterButtonWnd = "ui/zongmen/LuaRefineMonsterButtonWnd",
	LuaRefinePeopleButtonWnd = "ui/zongmen/LuaRefinePeopleButtonWnd",
	LuaRefineMonsterCircleWnd = "ui/zongmen/LuaRefineMonsterCircleWnd",
	LuaRefineMonsterEvilBuffConfirmWnd = "ui/zongmen/LuaRefineMonsterEvilBuffConfirmWnd",
	LuaRefineMonsterListWnd = "ui/zongmen/LuaRefineMonsterListWnd",
	LuaRefineMonsterItemWnd = "ui/zongmen/LuaRefineMonsterItemWnd",
	LuaRefineMonsterRecordWnd = "ui/zongmen/LuaRefineMonsterRecordWnd",
	LuaRefineMonsterLingliDescWnd = "ui/zongmen/LuaRefineMonsterLingliDescWnd",
	LuaRefineMonsterChooseWnd = "ui/zongmen/LuaRefineMonsterChooseWnd",
	-- 克符相关
	LuaAntiProfessionUnlockWnd = "ui/zongmen/LuaAntiProfessionUnlockWnd",
	LuaAntiProfessionLevelUpWnd = "ui/zongmen/LuaAntiProfessionLevelUpWnd",
	LuaAntiProfessionMakeWnd = "ui/zongmen/LuaAntiProfessionMakeWnd",
	LuaAntiProfessionSwapWnd = "ui/zongmen/LuaAntiProfessionSwapWnd",
	LuaAntiProfessionConvertWnd = "ui/zongmen/LuaAntiProfessionConvertWnd",
	LuaAntiProfessionConvertChooseWnd = "ui/zongmen/LuaAntiProfessionConvertChooseWnd",
	LuaAntiProfessionConvertResultWnd = "ui/zongmen/LuaAntiProfessionConvertResultWnd",
	--宗门迭代相关
	LuaZongMenNewSearchWnd = "ui/zongmen/LuaZongMenNewSearchWnd",
	LuaZhuoYaoCaptureView = "ui/zongmen/LuaZhuoYaoCaptureView",
	LuaZhuoYaoHuaFuWnd = "ui/zongmen/LuaZhuoYaoHuaFuWnd",
	LuaZhuoYaoHuaFuResultWnd = "ui/zongmen/LuaZhuoYaoHuaFuResultWnd",
	LuaZhuoYaoHuaFuWndProgress = "ui/zongmen/LuaZhuoYaoHuaFuWndProgress",
	LuaZongMenJoinStandardSearchResultWnd = "ui/zongmen/LuaZongMenJoinStandardSearchResultWnd",

	LuaEnemyZongPaiWnd = "ui/zongmen/LuaEnemyZongPaiWnd",
	LuaEnemyZongPaiArrestListWnd = "ui/zongmen/LuaEnemyZongPaiArrestListWnd",
	LuaZhuoRenTipWnd = "ui/zongmen/LuaZhuoRenTipWnd",
	LuaZongMenEnterChooseWnd = "ui/zongmen/LuaZongMenEnterChooseWnd",

	-- 物品选择窗口
	LuaCommonItemChooseWnd = "ui/common/LuaCommonItemChooseWnd",
	LuaCommonItemChooseMgr = "ui/common/LuaCommonItemChooseMgr",

	LuaBaoZhuPlayShowWnd = 'ui/baozhuplay/LuaBaoZhuPlayShowWnd',
	LuaBaoZhuPlayEndWnd = 'ui/baozhuplay/LuaBaoZhuPlayEndWnd',
	LuaBaoZhuPlayTopRightWnd = 'ui/baozhuplay/LuaBaoZhuPlayTopRightWnd',
	LuaBaoZhuView = 'ui/baozhuplay/LuaBaoZhuView',

	CLuaErHaOpenJinNangWnd = 'ui/LuaErHaOpenJinNangWnd',

	LuaOffWorldPassMainWnd = "ui/offworldpass/LuaOffWorldPassMainWnd",
	CLuaCurentMoneyCtrl = "ui/common/LuaCurentMoneyCtrl",

	LuaMiniMapWeatherTipWnd = "ui/LuaMiniMapWeatherTipWnd",
	LuaHouseUnlockWallpaperWnd = "ui/house/LuaHouseUnlockWallpaperWnd",

	LuaInviteNpcJoinZongMenWnd = "ui/npcjoinzongmen/LuaInviteNpcJoinZongMenWnd",
	LuaNpcInvitedInfoWnd = "ui/npcjoinzongmen/LuaNpcInvitedInfoWnd",
	LuaTrainNpcFavorWnd = "ui/npcjoinzongmen/LuaTrainNpcFavorWnd",
	LuaInviteNpcMgr = "ui/npcjoinzongmen/LuaInviteNpcMgr",
	LuaInsectTaskFindLingShouWnd = "ui/npcjoinzongmen/LuaInsectTaskFindLingShouWnd",

	LuaSystemRecommendGiftWnd = "ui/LuaSystemRecommendGiftWnd",
	LuaRecommendGiftMgr = "ui/LuaRecommendGiftMgr",
	LuaPoolEditWnd = "ui/house/pool/LuaPoolEditWnd",
	LuaAddPoolMaxCountWnd = "ui/house/pool/LuaAddPoolMaxCountWnd",
	LuaBuyPoolComfirmWnd = "ui/house/pool/LuaBuyPoolComfirmWnd",


	LuaWuYi2021Mgr = "ui/wuyi2021/LuaWuYi2021Mgr",

	LuaHaoYiXingCardsWnd = "ui/HaoYiXing/LuaHaoYiXingCardsWnd",
	LuaHaoYiXingCardPreviewWnd = "ui/HaoYiXing/LuaHaoYiXingCardPreviewWnd",
	LuaHaoYiXingCardsComposeWnd = "ui/HaoYiXing/LuaHaoYiXingCardsComposeWnd",
	LuaHaoYiXingCardsComposeResultWnd = "ui/HaoYiXing/LuaHaoYiXingCardsComposeResultWnd",
	LuaHaoYiXingCardTemplate = "ui/HaoYiXing/LuaHaoYiXingCardTemplate",
	LuaHaoYiXingCardStoryWnd = "ui/HaoYiXing/LuaHaoYiXingCardStoryWnd",
	LuaGetNewPersonalSpaceBgWnd = "ui/haoyixing/LuaGetNewPersonalSpaceBgWnd",
	LuaChoosePersonalSpaceNewBgWnd = "ui/personalspace/LuaChoosePersonalSpaceNewBgWnd",
	LuaHaoYiXingExperienceTasksWnd = "ui/haoyixing/LuaHaoYiXingExperienceTasksWnd",
	LuaGetHaoYiXingHaiTangWnd = "ui/HaoYiXing/LuaGetHaoYiXingHaiTangWnd",

	CLuaSanJieFengHuaLuEntryWnd = "ui/worldevent2021/LuaSanJieFengHuaLuEntryWnd",

	LuaNewHorseRaceBaseWnd = "ui/horserace/LuaNewHorseRaceBaseWnd",
	LuaRepairTongTianTaSubmitSilverWnd = "ui/zongmen/LuaRepairTongTianTaSubmitSilverWnd",

	LuaLiuYi2021TangGuoMgr = "ui/liuyi2021/LuaLiuYi2021TangGuoMgr",
	--海钓
	LuaSeaFishingMgr = "ui/seafishing/LuaSeaFishingMgr",
	LuaSeaFishingPlayWnd = "ui/seafishing/LuaSeaFishingPlayWnd",
	LuaSeaFishingSucceedWnd = "ui/seafishing/LuaSeaFishingSucceedWnd",
	LuaSeaFishingTaskView = "ui/seafishing/LuaSeaFishingTaskView",
	LuaHouseFishingWnd = "ui/seafishing/LuaHouseFishingWnd",
	LuaFishBaitWnd = "ui/seafishing/LuaFishBaitWnd",
	LuaFishGetAndRecordWnd = "ui/seafishing/LuaFishGetAndRecordWnd",
	LuaFishingSkillView = "ui/seafishing/LuaFishingSkillView",
	LuaFishingRodAppearanceWnd = "ui/seafishing/LuaFishingRodAppearanceWnd",
	LuaComicPPTWnd = "ui/seafishing/LuaComicPPTWnd",
	LuaSeaFishingPackageWnd = "ui/seafishing/LuaSeaFishingPackageWnd",
	LuaSeaFishingTuJianWnd = "ui/seafishing/LuaSeaFishingTuJianWnd",
	LuaSpecialHaiDiaoPlayPickUpWnd = "ui/seafishing/LuaSpecialHaiDiaoPlayPickUpWnd",
	LuaSeaFishingShopWnd = "ui/seafishing/LuaSeaFishingShopWnd",
	LuaSeaFishingServerRecordWnd = "ui/seafishing/LuaSeaFishingServerRecordWnd",
	--镇宅鱼
	LuaZhenZhaiYuWnd = "ui/house/pool/LuaZhenZhaiYuWnd",
	LuaZhenZhaiYuDetailWnd = "ui/house/pool/LuaZhenZhaiYuDetailWnd",
	LuaFishCreelWnd = "ui/house/pool/LuaFishCreelWnd",
	LuaZhenZhaiYuMgr = "ui/house/pool/LuaZhenZhaiYuMgr",
	LuaFeedZhenZhaiYuWnd = "ui/house/pool/LuaFeedZhenZhaiYuWnd",
	LuaChooseFabao2GongFengWnd = "ui/house/pool/LuaChooseFabao2GongFengWnd",
	LuaBringUpZhenZhaiYuWnd = "ui/house/pool/LuaBringUpZhenZhaiYuWnd",
	LuaFxAndModelPreview = "ui/seafishing/LuaFxAndModelPreview",
	LuaClearZhenZhaiYuWordWnd = "ui/house/pool/LuaClearZhenZhaiYuWordWnd",
	LuaInheritZhenZhaiYuWordWnd = "ui/house/pool/LuaInheritZhenZhaiYuWordWnd",

	-- 插旗决斗
	LuaFlagDuelMgr = "ui/pk/LuaFlagDuelMgr",
	
	--奥运
	LuaOlympicGamesMgr = "ui/olympicgames/LuaOlympicGamesMgr",
	LuaOlympicGamesView = "ui/olympicgames/LuaOlympicGamesView",
	LuaOlympicGamePlayWnd = "ui/olympicgames/LuaOlympicGamePlayWnd",

	--跨服竞技场
	LuaCrossServerColiseumMainWnd = "ui/coliseum/LuaCrossServerColiseumMainWnd",
	LuaColiseumRecordsTipWnd = "ui/coliseum/LuaColiseumRecordsTipWnd",
	LuaColiseumMgr = "ui/coliseum/LuaColiseumMgr",
	LuaColiseumRankWnd = "ui/coliseum/LuaColiseumRankWnd",
	LuaColiseumWeeklyRewardInfoWnd = "ui/coliseum/LuaColiseumWeeklyRewardInfoWnd",
	LuaColiseumSeasonRewardWnd = "ui/coliseum/LuaColiseumSeasonRewardWnd",
	LuaColiseumShopWnd = "ui/coliseum/LuaColiseumShopWnd",
	LuaColiseumEndGameWnd = "ui/coliseum/LuaColiseumEndGameWnd",
	LuaColiseumShareWnd = "ui/coliseum/LuaColiseumShareWnd",
	LuaColiseumShowTeamWnd = "ui/coliseum/LuaColiseumShowTeamWnd",

	--万圣节2021
	LuaHalloween2021GamePlayWnd = "ui/halloween2021/LuaHalloween2021GamePlayWnd",
	LuaHalloween2021SpyTipWnd = "ui/halloween2021/LuaHalloween2021SpyTipWnd",
	LuaHalloween2021View = "ui/halloween2021/LuaHalloween2021View",
	LuaHalloween2021Mgr = "ui/halloween2021/LuaHalloween2021Mgr",
	LuaHalloween2021JieYuanVoteWnd = "ui/halloween2021/LuaHalloween2021JieYuanVoteWnd",

	--水果派对
	LuaFruitPartySignWnd = "ui/fruitparty/LuaFruitPartySignWnd",
	LuaFruitPartyRankWnd = "ui/fruitparty/LuaFruitPartyRankWnd",
	LuaFruitPartyStateWnd = "ui/fruitparty/LuaFruitPartyStateWnd",
	LuaFruitPartyResultWnd = "ui/fruitparty/LuaFruitPartyResultWnd",
	LuaFruitPartyView = "ui/fruitparty/LuaFruitPartyView",
	LuaShiTuFruitPartySignWnd = "ui/fruitparty/LuaShiTuFruitPartySignWnd",
	LuaShiTuFruitPartyNormalStateWnd = "ui/fruitparty/LuaShiTuFruitPartyNormalStateWnd",
	LuaShiTuFruitPartyTaskStateWnd = "ui/fruitparty/LuaShiTuFruitPartyTaskStateWnd",
	LuaShiTuFruitPartyResultWnd = "ui/fruitparty/LuaShiTuFruitPartyResultWnd",
	-- 趣味喇叭
	LuaQuweiLabaMgr = "ui/friend/LuaQuweiLabaMgr",
	LuaLuoCha2021Mgr = "ui/luocha2021/LuaLuoCha2021Mgr",

	-- 摘星
	LuaZhaiXingEnterWnd = "ui/zhaixing/LuaZhaiXingEnterWnd",
	LuaZhaiXingStateWnd = "ui/zhaixing/LuaZhaiXingStateWnd",
	LuaZhaiXingResultWnd = "ui/zhaixing/LuaZhaiXingResultWnd",

	--宗派福利
	LuaZongMenHongBaoSetWnd = "ui/zongmen/LuaZongMenHongBaoSetWnd",
	LuaSectHongBaoItemSelectWnd = "ui/zongmen/LuaSectHongBaoItemSelectWnd",
	LuaChangeZongMenHongBaoItemWnd = "ui/zongmen/LuaChangeZongMenHongBaoItemWnd",
	LuaSectHongbaoDetailNewWnd = "ui/zongmen/LuaSectHongbaoDetailNewWnd",
	LuaSectHongBaoLotteryWnd = "ui/zongmen/LuaSectHongBaoLotteryWnd",
	LuaGetSectHongBaoWnd = "ui/zongmen/LuaGetSectHongBaoWnd",
	LuaHongbaohistoryWndSectHongBaoRoot = "ui/hongbao/LuaHongbaohistoryWndSectHongBaoRoot",
	-- 双十一
	LuaShuangshiyi2021Mgr = "ui/shuangshiyi2021/LuaShuangshiyi2021Mgr",
	LuaShuangshiyi2022Mgr = "ui/doubleone2022/LuaShuangshiyi2022Mgr",
	LuaShuangshiyi2023Mgr = "ui/doubleone2023/LuaShuangshiyi2023Mgr",

	-- 吴门画士
	LuaWuMenHuaShiMgr = "ui/wumenhuashi/LuaWuMenHuaShiMgr",
	
	-- 玩法开放
	LuaGlobalMatchMgr = "ui/GlobalMatch/LuaGlobalMatchMgr",

	LuaScrollTaskWnd = "ui/common/LuaScrollTaskWnd",
	LuaScrollTaskMgr = "ui/common/LuaScrollTaskMgr",

	LuaBiAnYingXueChooseWnd = "ui/juqing/LuaBiAnYingXueChooseWnd",

	-- 婚礼
	LuaWeddingIterationMgr = "ui/weddingiteration/LuaWeddingIterationMgr",

	--食谱
	LuaCookBookWnd = "ui/cookbook/LuaCookBookWnd",
	LuaCookBookMgr = "ui/cookbook/LuaCookBookMgr",
	LuaCookBookItemSelectWnd = "ui/cookbook/LuaCookBookItemSelectWnd",
	LuaOpenIngredientPackResultWnd = "ui/cookbook/LuaOpenIngredientPackResultWnd",
	LuaCookingResultWnd = "ui/cookbook/LuaCookingResultWnd",
	LuaMyShangShiIngredientWnd = "ui/cookbook/LuaMyShangShiIngredientWnd",

	--尺素传吾情
	LuaValentine2022DriftBottleWnd = "ui/valentine2022/LuaValentine2022DriftBottleWnd",
	LuaValentine2022Mgr = "ui/valentine2022/LuaValentine2022Mgr",

	--灯谜集市+元宵2022赏灯玩法
	LuaYuanXiao2022DengMiWnd = "ui/yuanxiao2022/LuaYuanXiao2022DengMiWnd",
	LuaYuanXiao2022Mgr = "ui/yuanxiao2022/LuaYuanXiao2022Mgr",

	--帮会领土战
	LuaGuildTerritorialWarsMgr = "ui/guild/territorialwars/LuaGuildTerritorialWarsMgr",
	LuaGuildTerritorialWarsView = "ui/guild/territorialwars/LuaGuildTerritorialWarsView",
	LuaGuildTerritorialWarsEnterWnd = "ui/guild/territorialwars/LuaGuildTerritorialWarsEnterWnd",
	LuaGuildTerritorialWarsOccupyWnd = "ui/guild/territorialwars/LuaGuildTerritorialWarsOccupyWnd",
	LuaGuildTerritorialWarsMapWnd = "ui/guild/territorialwars/LuaGuildTerritorialWarsMapWnd",
	LuaGuildTerritorialWarsSituationWnd = "ui/guild/territorialwars/LuaGuildTerritorialWarsSituationWnd",
	LuaGuildTerritorialWarsMapTipWnd = "ui/guild/territorialwars/LuaGuildTerritorialWarsMapTipWnd",
	LuaGuildTerritorialWarsMapIconWnd = "ui/guild/territorialwars/LuaGuildTerritorialWarsMapIconWnd",
	LuaGuildTerritorialWarsMapInfoView = "ui/guild/territorialwars/LuaGuildTerritorialWarsMapInfoView",
	LuaGuildTerritorialWarsPlayView = "ui/guild/territorialwars/LuaGuildTerritorialWarsPlayView",
	LuaGuildTerritorialWarsTopRightWnd = "ui/guild/territorialwars/LuaGuildTerritorialWarsTopRightWnd",
	LuaGuildTerritorialWarsJieMengWnd = "ui/guild/territorialwars/LuaGuildTerritorialWarsJieMengWnd",
	LuaGuildTerritorialWarsRankWnd = "ui/guild/territorialwars/LuaGuildTerritorialWarsRankWnd",
	LuaGuildTerritorialWarsSceneChosenWnd = "ui/guild/territorialwars/LuaGuildTerritorialWarsSceneChosenWnd",
	LuaGuildTerritorialWarsScoreSpeedDetailWnd = "ui/guild/territorialwars/LuaGuildTerritorialWarsScoreSpeedDetailWnd",
	LuaGuildTerritorialWarsMapRankView = "ui/guild/territorialwars/LuaGuildTerritorialWarsMapRankView",
	LuaGuildTerritorialWarsPeripheryAlertWnd = "ui/guild/territorialwars/LuaGuildTerritorialWarsPeripheryAlertWnd",
	LuaGuildTerritorialWarsPeripheryMonsterWnd = "ui/guild/territorialwars/LuaGuildTerritorialWarsPeripheryMonsterWnd",
	LuaGuildTerritorialWarsContributionWnd = "ui/guild/territorialwars/LuaGuildTerritorialWarsContributionWnd",
	LuaGuildTerritorialWarsDrawMapPanel = "ui/guild/territorialwars/LuaGuildTerritorialWarsDrawMapPanel",
	LuaGuildTerritorialWarsDrawColorWnd = "ui/guild/territorialwars/LuaGuildTerritorialWarsDrawColorWnd",
	LuaGuildTerritorialWarsResultWnd = "ui/guild/territorialwars/LuaGuildTerritorialWarsResultWnd",
	LuaGuildTerritorialWarsBeiZhanWnd = "ui/guild/territorialwars/LuaGuildTerritorialWarsBeiZhanWnd",
	LuaGuildTerritorialWarsBeiZhanView = "ui/guild/territorialwars/LuaGuildTerritorialWarsBeiZhanView",
	LuaGuildTerritorialWarsGongFengView = "ui/guild/territorialwars/LuaGuildTerritorialWarsGongFengView",
	LuaGuildTerritorialWarsZhanLongWnd = "ui/guild/territorialwars/LuaGuildTerritorialWarsZhanLongWnd",
	LuaGuildTerritorialWarsServerGroupWnd = "ui/guild/territorialwars/LuaGuildTerritorialWarsServerGroupWnd",
	LuaGuildTerritorialWarsZhanLongShopWnd = "ui/guild/territorialwars/LuaGuildTerritorialWarsZhanLongShopWnd",
	LuaGuildTerritorialWarsJingYingSettingWnd = "ui/guild/territorialwars/LuaGuildTerritorialWarsJingYingSettingWnd",
	LuaGuildTerritorialWarsMapChallengeClearTimeView = "ui/guild/territorialwars/LuaGuildTerritorialWarsMapChallengeClearTimeView",
	LuaGuildTerritorialWarsChallengeTopRightWnd = "ui/guild/territorialwars/LuaGuildTerritorialWarsChallengeTopRightWnd",
	--LuaGuildTerritorialWarsChallengeSituatuionWnd = "ui/guild/territorialwars/LuaGuildTerritorialWarsChallengeSituatuionWnd",
	
	LuaGuildExternalAidWnd = "ui/guild/externalaid/LuaGuildExternalAidWnd",
	LuaGuildExternalAidCardWnd = "ui/guild/externalaid/LuaGuildExternalAidCardWnd",
	LuaGuildExternalAidInvitationWnd = "ui/guild/externalaid/LuaGuildExternalAidInvitationWnd",
	LuaGuildExternalAidMgr = "ui/guild/externalaid/LuaGuildExternalAidMgr",
	LuaMiniMapPopWndInfoButton = "ui/common/LuaMiniMapPopWndInfoButton",

	--圣诞2021
	LuaWNXYGiveWishWnd = "ui/christmas2021/LuaWNXYGiveWishWnd",
	LuaWNXYAddGiftWnd = "ui/christmas2021/LuaWNXYAddGiftWnd",
	LuaWNXYGuildWishesWnd = "ui/christmas2021/LuaWNXYGuildWishesWnd",
	LuaChristmas2021Mgr = "ui/christmas2021/LuaChristmas2021Mgr",
	LuaSDXYSignAndRankWnd = "ui/christmas2021/LuaSDXYSignAndRankWnd",
	LuaSDXYGuanXingWnd = "ui/christmas2021/LuaSDXYGuanXingWnd",
	LuaSDXYSucceedWnd = "ui/christmas2021/LuaSDXYSucceedWnd",
	LuaSDXYRankView = "ui/christmas2021/LuaSDXYRankView",
	LuaMJSYPlayWnd = "ui/christmas2021/LuaMJSYPlayWnd",
	LuaSDXYTopRightWnd = "ui/christmas2021/LuaSDXYTopRightWnd",
	LuaChristmas2021TaskView = "ui/christmas2021/LuaChristmas2021TaskView",
	LuaQuestionAndAnswerWnd = "ui/christmas2021/LuaQuestionAndAnswerWnd",
	LuaMJSYInfoWnd = "ui/christmas2021/LuaMJSYInfoWnd",

	--烟花大会
	LuaFireWorkPartyZhongChouWnd = "ui/fireworkparty/LuaFireWorkPartyZhongChouWnd",
	LuaSendShowLoveFireWorkWnd = "ui/fireworkparty/LuaSendShowLoveFireWorkWnd",
	LuaFireWorkPartyMgr = "ui/fireworkparty/LuaFireWorkPartyMgr",
	LuaZhongQiuSelectFriendWnd = "ui/fireworkparty/LuaZhongQiuSelectFriendWnd",
	--狂奔的汤圆
	LuaRunningWildTangYuanRankWnd = "ui/yuanxiao2022/LuaRunningWildTangYuanRankWnd",
	LuaRunningWildTangYuanResultWnd = "ui/yuanxiao2022/LuaRunningWildTangYuanResultWnd",
	LuaRunningWildTangYuanSignWnd = "ui/yuanxiao2022/LuaRunningWildTangYuanSignWnd",
	LuaRunningWildTangYuanTopRightWnd = "ui/yuanxiao2022/LuaRunningWildTangYuanTopRightWnd",
	LuaTangYuan2022Mgr = "ui/yuanxiao2022/LuaTangYuan2022Mgr",

	LuaChunJie2022Mgr = "ui/chunjie2022/LuaChunJie2022Mgr",
	--魅香楼
	LuaQingJiuLaBaWnd = "ui/meixianglou/LuaQingJiuLaBaWnd",
	LuaMeiXiangLouMgr = "ui/meixianglou/LuaMeiXiangLouMgr",
	LuaMusicPlaySignWnd = "ui/meixianglou/LuaMusicPlaySignWnd",
	LuaChooseMusicWnd = "ui/meixianglou/LuaChooseMusicWnd",
	LuaMusicPlayRankWnd = "ui/meixianglou/LuaMusicPlayRankWnd",
	LuaMusicPlayHitingWnd = "ui/meixianglou/LuaMusicPlayHitingWnd",
	LuaMusicPlayHitResultWnd = "ui/meixianglou/LuaMusicPlayHitResultWnd",
	LuaMusicHitNode = "ui/meixianglou/LuaMusicHitNode",

	-- 清明2022
	LuaQingMing2022Mgr = "ui/qingming2022/LuaQingMing2022Mgr",

	-- 跨服高昌
	LuaGaoChangCrossMgr = "ui/gaochang/LuaGaoChangCrossMgr",

	LuaOverheadButtonMgr = "ui/LuaOverheadButtonWnd",

	-- 进度排行榜界面
	LuaCommonProgressRankWndMgr = "ui/common/LuaCommonProgressRankWndMgr",
	LuaBusinessMgr = "ui/business/LuaBusinessMgr",

	-- 据点战
	LuaJuDianBattleMgr = "ui/judianbattle/LuaJuDianBattleMgr",

	--周年庆2022 葫芦娃
	LuaHuLuWaTravelWorldWnd = "ui/huluwa2022/LuaHuLuWaTravelWorldWnd",
	LuaHuLuWaTravelEventPopWnd = "ui/huluwa2022/LuaHuLuWaTravelEventPopWnd",
	LuaHuLuWa2022Mgr = "ui/huluwa2022/LuaHuLuWa2022Mgr",
	LuaRuYiDongSignWnd = "ui/huluwa2022/LuaRuYiDongSignWnd",
	LuaRuYiDongResultWnd = "ui/huluwa2022/LuaRuYiDongResultWnd",
	LuaRuYiDongRankView = "ui/huluwa2022/LuaRuYiDongRankView",
	LuaHuLuWaBianShenPreviewWnd = "ui/huluwa2022/LuaHuLuWaBianShenPreviewWnd",
	LuaRuYiDongTopRightWnd = "ui/huluwa2022/LuaRuYiDongTopRightWnd",
	LuaQiaoDuoRuYiSignWnd = "ui/huluwa2022/LuaQiaoDuoRuYiSignWnd",
	LuaHuLuWaUnLockWnd = "ui/huluwa2022/LuaHuLuWaUnLockWnd",
	LuaQiaoDuoRuYiRankWnd = "ui/huluwa2022/LuaQiaoDuoRuYiRankWnd",
	LuaHuLuWaZhanLingWnd = "ui/huluwa2022/LuaHuLuWaZhanLingWnd",
	LuaHuLuWaUnLockDanHuWnd = "ui/huluwa2022/LuaHuLuWaUnLockDanHuWnd",
	LuaHuLuWaLianDanShouCeWnd = "ui/huluwa2022/LuaHuLuWaLianDanShouCeWnd",
	LuaZhanLingLevelRewardItem = "ui/huluwa2022/LuaZhanLingLevelRewardItem",
	LuaRuYiAdditionSkillWnd = "ui/huluwa2022/LuaRuYiAdditionSkillWnd",

	-- 玩家排行榜
	LuaPlayerRankMgr = "ui/LuaPlayerRankWnd",

	-- 五一斗地主
	LuaDouDiZhuBaoXiangMgr = "ui/doudizhu/baoxiang/LuaDouDiZhuBaoXiangMgr",

	-- 通用图文规则界面
	LuaCommonTextImageRuleMgr = "ui/common/LuaCommonTextImageRuleMgr",
	-- Buff信息
	LuaBuffInfoWndMgr = "ui/common/LuaBuffInfoWndMgr",

	LuaGuildLeagueBattleResultWnd = "ui/guildleague/LuaGuildLeagueBattleResultWnd",
	LuaHaiZhanMgr = "ui/haizhan/LuaHaiZhanMgr",

	-- 倩影初闻录
	LuaQianYingChuWenMgr = "ui/qianyingchuwen/LuaQianYingChuWenMgr",

	-- 新白联动
	LuaXinBaiLianDongMgr = "ui/xinbailiandong/LuaXinBaiLianDongMgr",

	-- shujia2022
	LuaShanYeMiZongMgr = "ui/shujia2022/LuaShanYeMiZongMgr",
	LuaHuanHunShanZhuangMgr = "ui/shujia2022/LuaHuanHunShanZhuangMgr",

	--师徒系統优化
	LuaShiTuMainWnd = "ui/shitu/LuaShiTuMainWnd",
	LuaShiTuMainWndEmptyView = "ui/shitu/LuaShiTuMainWndEmptyView",
	LuaShiTuMainWndShiTuView = "ui/shitu/LuaShiTuMainWndShiTuView",
	LuaShiTuChooseWnd = "ui/shitu/LuaShiTuChooseWnd",
	LuaShiTuResultWnd = "ui/shitu/LuaShiTuResultWnd",
	LuaShiTuRecommendWnd = "ui/shitu/LuaShiTuRecommendWnd",
	LuaWaiMenDiZiWnd = "ui/shitu/LuaWaiMenDiZiWnd",
	LuaShiTuHistoryWnd = "ui/shitu/LuaShiTuHistoryWnd",
	LuaShiTuDirectoryWnd = "ui/shitu/LuaShiTuDirectoryWnd",
	LuaShiTuChooseJiYuWnd = "ui/shitu/LuaShiTuChooseJiYuWnd",
	LuaChuShiGiftWnd = "ui/shitu/LuaChuShiGiftWnd",
	LuaShiTuRatingTipWnd = "ui/shitu/LuaShiTuRatingTipWnd",
	LuaShiTuGiftGivingWnd = "ui/shitu/LuaShiTuGiftGivingWnd",
	LuaShiTuWishGiftWnd = "ui/shitu/LuaShiTuWishGiftWnd",
	LuaShifuPingJiaWnd = "ui/shitu/LuaShifuPingJiaWnd",
	LuaBaiShiPosterWnd = "ui/shitu/LuaBaiShiPosterWnd",
	LuaChuShiPosterWnd = "ui/shitu/LuaChuShiPosterWnd",

	--七夕2022
	LuaQiXi2022Mgr = "ui/qixi2022/LuaQiXi2022Mgr",
	LuaQiXi2022PlotWnd = "ui/qixi2022/LuaQiXi2022PlotWnd",
	LuaQiXi2022EnterWnd = "ui/qixi2022/LuaQiXi2022EnterWnd",
	LuaQiXi2022ChoosePlotWnd = "ui/qixi2022/LuaQiXi2022ChoosePlotWnd",
	LuaQiXi2022ReturnGoodsWnd = "ui/qixi2022/LuaQiXi2022ReturnGoodsWnd",
	LuaQiXi2022FindItemSkillWnd = "ui/qixi2022/LuaQiXi2022FindItemSkillWnd",
	LuaQiXi2022PlotResultWnd = "ui/qixi2022/LuaQiXi2022PlotResultWnd",
	LuaQiXi2022PlantTreesWnd = "ui/qixi2022/LuaQiXi2022PlantTreesWnd",
	LuaQiXi2022ScheduleWnd = "ui/qixi2022/LuaQiXi2022ScheduleWnd",

	-- 端午六一2022
	LuaDuanWu2022Mgr = "ui/duanwu2022/LuaDuanWu2022Mgr",
	LuaLiuYi2022Mgr = "ui/liuyi2022/LuaLiuYi2022Mgr",

	LuaHouseRollerCoasterMgr = "ui/house/LuaHouseRollerCoasterMgr",

	LuaGuildAIDCCInfo = "ui/chat/LuaGuildAIDCCInfo",

	-- 水漫金山
	LuaShuiManJinShanTaskView = "ui/shuimanjinshan/LuaShuiManJinShanTaskView",
	--主角剧情歌词
	LuaZhuJueJuQingLyricWnd = "ui/zhujuejuqing/LuaZhuJueJuQingLyricWnd",
	LuaJuQingLyricMgr = "ui/zhujuejuqing/LuaJuQingLyricMgr",
	----新白 语音成就
	LuaXinBaiVoiceAchivementWnd ="ui/achievement/LuaXinBaiVoiceAchivementWnd",
	LuaVoiceAchievementMgr = "ui/achievement/LuaVoiceAchievementMgr",
	LuaVoiceAchievementItem = "ui/achievement/LuaVoiceAchievementItem",
	LuaUnLockVoiceAchievementWnd = "ui/achievement/LuaUnLockVoiceAchievementWnd",
	LuaVoiceAchievemenAwardPreviewWnd = "ui/achievement/LuaVoiceAchievemenAwardPreviewWnd",

	-- 无量
	LuaWuLiangMgr = "ui/wuliang/LuaWuLiangMgr",

	-- 中秋2022
	LuaZhongQiu2022Mgr = "ui/zhongqiu2022/LuaZhongQiu2022Mgr",

	-- 灵玉礼包推荐
	LuaLingyuGiftRecommendMgr = "ui/welfare/LuaLingyuGiftRecommendMgr",

	-- 世界杯2022
	LuaWorldCup2022Mgr = "ui/worldcup/LuaWorldCup2022Mgr",

	--膜拜榜
	LuaShareMoBaiBangMgr = "ui/share/LuaShareMoBaiBangMgr",

	--国庆2022
	LuaGuoQingPvPMgr = "ui/guoqing2022/LuaGuoQingPvPMgr",
	LuaGuoQingPvEMgr = "ui/guoqing2022/LuaGuoQingPvEMgr",

	--帮会自定义建设 前置任务
	LuaGuildCustomBuildMgr = "ui/guildcustombuild/LuaGuildCustomBuildMgr",
	LuaGuildCustomBuildPreTaskWnd = "ui/guildcustombuild/LuaGuildCustomBuildPreTaskWnd",
	LuaGuildCustomBuildPreTaskView = "ui/guildcustombuild/LuaGuildCustomBuildPreTaskView",
	LuaGuildPreTaskBreakSchemeTopRightWnd = "ui/guildcustombuild/LuaGuildPreTaskBreakSchemeTopRightWnd",
	LuaPreTaskCleanProgress = "ui/guildcustombuild/LuaPreTaskCleanProgress",

	EnumPhysicsController = "common/haizhan/PhysicsShipInc",
	EnumPhysicsCmdType = "common/haizhan/PhysicsShipInc",
	LuaClientPhysicsBullet = "engine/physics/LuaClientPhysicsBullet",
	LuaClientPhysicsShip = "engine/physics/LuaClientPhysicsShip",
	LuaClientPhysicsObject = "engine/physics/LuaClientPhysicsObject",
	LuaClientPhysicsMgr = "engine/physics/LuaClientPhysicsMgr",

	-- 元宵2023
	LuaYuanXiao2023Mgr = "ui/yuanxiao2023/LuaYuanXiao2023Mgr",
	LuaYuanXiaoDefenseMgr = "ui/yuanxiao2023/LuaYuanXiaoDefenseMgr",
	LuaGaoChangJiDouMgr = "ui/gaochang/LuaGaoChangJiDouMgr",

	-- 寒假2023
	LuaHanJia2023Mgr = "ui/hanjia2023/LuaHanJia2023Mgr",
	LuaSnowAdventureStageTemplate = "ui/hanjia2023/LuaSnowAdventureStageTemplate",
	-- 圣诞2022
	LuaChristmas2022Mgr = "ui/christmas2022/LuaChristmas2022Mgr",

	-- 蓬岛伏妖
	LuaPengDaoDialogWnd = "ui/pengdao/LuaPengDaoDialogWnd",
	LuaPengDaoRewardsListWnd = "ui/pengdao/LuaPengDaoRewardsListWnd",
	LuaPengDaoDevelopWnd = "ui/pengdao/LuaPengDaoDevelopWnd",
	LuaPengDaoRankWnd = "ui/pengdao/LuaPengDaoRankWnd",
	LuaPengDaoTuJianWnd = "ui/pengdao/LuaPengDaoTuJianWnd",
	LuaPengDaoSelectDifficultyWnd = "ui/pengdao/LuaPengDaoSelectDifficultyWnd",
	LuaPengDaoPlayWnd = "ui/pengdao/LuaPengDaoPlayWnd",
	LuaPengDaoSelectBlessingWnd = "ui/pengdao/LuaPengDaoSelectBlessingWnd",
	LuaPengDaoResultWnd = "ui/pengdao/LuaPengDaoResultWnd",
	LuaPengDaoReplaceSkillWnd = "ui/pengdao/LuaPengDaoReplaceSkillWnd",
	LuaPengDaoMgr = "ui/pengdao/LuaPengDaoMgr",
	
	-- 春节2023
	LuaChunJie2023Mgr = "ui/chunjie2023/LuaChunJie2023Mgr",

	LuaYuanDan2023Mgr = "ui/yuandan2023/LuaYuanDan2023Mgr",

	-- 情人节2023
	LuaValentine2023Mgr = "ui/valentine2023/LuaValentine2023Mgr",
	
	-- 情人节2024
	LuaValentine2024Mgr = "ui/valentine2024/LuaValentine2024Mgr",


	--家园布置迭代2022
	LuaMultipleFurnitureEditWnd = "ui/house/advanve/LuaMultipleFurnitureEditWnd",
	LuaMultipleChooseListWnd = "ui/house/advanve/LuaMultipleChooseListWnd",
	LuaMultipleFurnishPopupMenu = "ui/house/advanve/LuaMultipleFurnishPopupMenu",
	LuaFurnitureLianPuWnd = "ui/house/advanve/LuaFurnitureLianPuWnd",

	LuaCommonTongXingZhengBuyLevelMgr = "ui/common/LuaCommonTongXingZhengBuyLevelWnd",
	-- 大富翁
	LuaDaFuWongMgr = "ui/dafuwong/LuaDaFuWongMgr",
	-- 属性引导功能
	LuaPlayerPropertyMgr = "ui/main/mainplayer/LuaPlayerPropertyMgr",
	LuaToolBtnMgr = "ui/LuaToolBtnWnd",

	--通用侧边栏消息
	LuaCommonSideDialogWnd = "ui/common/LuaCommonSideDialogWnd",
	LuaCommonSideDialogMgr = "ui/common/LuaCommonSideDialogMgr",

	-- 通用节日
	LuaCommonJieRiMgr = "ui/common/LuaCommonJieRiMainWnd",
	-- 队伍招募
	LuaTeamRecruitMgr = "ui/teamrecruit/LuaTeamRecruitMgr",
	
	-- 清明2023
	LuaQingMing2023Mgr = "ui/qingming2023/LuaQingMing2023Mgr",

	-- 伊人思
	LuaYiRenSiMgr = "ui/yirensi/LuaYiRenSiMgr",

	LuaFourSeasonCardMgr = "ui/zhounianqing2023/fsc/LuaFourSeasonCardMgr",

	-- 竞技场
	LuaArenaMgr = "ui/arena/LuaArenaMgr",

	-- 竞技场-晶元战
	LuaArenaJingYuanMgr = "ui/arena/arenajingyuan/LuaArenaJingYuanMgr",

	-- 端午2023
	LuaDuanWu2023Mgr = "ui/duanwu2023/LuaDuanWu2023Mgr",

	-- 柳如是剧情
	LuaLiuRuShiMgr = "ui/liurushi/LuaLiuRuShiMgr",

	--养育迭代2023
	LuaBabyQiChangShu2023Wnd = "ui/baby2023/LuaBabyQiChangShu2023Wnd",
	LuaBabyQiChangRequest2023Tip = "ui/baby2023/LuaBabyQiChangRequest2023Tip",
	LuaSetTargetQiChangWnd = "ui/baby2023/LuaSetTargetQiChangWnd",
	LuaSchedulePlanView2023 = "ui/baby2023/LuaSchedulePlanView2023",

	--周年庆2023
	LuaZhouNianQing2023Mgr = "ui/zhounianqing2023/LuaZhouNianQing2023Mgr",

	--府库
	LuaStoreRoomWnd = "ui/storeroom2023/LuaStoreRoomWnd",
	LuaStoreRoomMgr = "ui/storeroom2023/LuaStoreRoomMgr",
	LuaStoreRoomPosUnlockWnd = "ui/storeroom2023/LuaStoreRoomPosUnlockWnd",
	LuaStoreRoomConstructWnd = "ui/storeroom2023/LuaStoreRoomConstructWnd",

	-- 六一2023
	LuaLiuYi2023Mgr = "ui/liuyi2023/LuaLiuYi2023Mgr",

	-- 显示拥有的不同类型的钱
	LuaOwnMoneyListMgr = "ui/main/LuaOwnMoneyListWnd",

	-- 通用通行证
	LuaCommonPassportMgr = "ui/passport/LuaCommonPassportMgr",
	LuaCommonHorizontalPopupMenuMgr =  "ui/common/LuaCommonHorizontalPopupMenu",
	
	-- 赛事荣誉
	LuaCompetitionHonorMgr = "ui/competitionhonor/LuaCompetitionHonorMgr",

	-- 永久时装抽卡
	LuaFashionLotteryMgr = "ui/fashionlottery/LuaFashionLotteryMgr",
	
	-- 国庆2023
	LuaGuoQing2023Mgr = "ui/guoqing2023/LuaGuoQing2023Mgr",

	-- 暑假2023
	LuaShuJia2023Mgr = "ui/shujia2023/LuaShuJia2023Mgr",

	-- 嘉年华2023
	LuaCarnival2023Mgr = "ui/carnival2023/LuaCarnival2023Mgr",
	LuaHouseMessageView = "ui/house/LuaHouseMessageView",
	LuaQiShuAlert = "ui/house/LuaQiShuAlert",
	--创角2023
	LuaCharacterCreation2023Wnd = "ui/chuangjue2023/LuaCharacterCreation2023Wnd",
	LuaCharacterCreation2023Mgr = "ui/chuangjue2023/LuaCharacterCreation2023Mgr",
	
	-- 中元节2023
	LuaZhongYuanJie2023Mgr = "ui/zhongyuanjie2023/LuaZhongYuanJie2023Mgr",
	LuaZYPDSignUpWnd = "ui/zhongyuanjie2023/LuaZYPDSignUpWnd",
	LuaZYPDHuaFuWnd = "ui/zhongyuanjie2023/LuaZYPDHuaFuWnd",
	LuaZYPDResultWnd = "ui/zhongyuanjie2023/LuaZYPDResultWnd",
	LuaZYPDStateWnd = "ui/zhongyuanjie2023/LuaZYPDStateWnd",

	--自定义装饰物模板
	LuaCustomZswTemplateTopRightWnd = "ui/jyzhuangshiwutemplate/LuaCustomZswTemplateTopRightWnd",
	LuaUnlockCustomZswTemplateWnd = "ui/jyzhuangshiwutemplate/LuaUnlockCustomZswTemplateWnd",
	LuaCustomZswTemplateEditWnd = "ui/jyzhuangshiwutemplate/LuaCustomZswTemplateEditWnd",
	LuaCustomZswTemplateUpgradeWnd = "ui/jyzhuangshiwutemplate/LuaCustomZswTemplateUpgradeWnd",
	LuaCustomZswTemplateFillingWnd = "ui/jyzhuangshiwutemplate/LuaCustomZswTemplateFillingWnd",

	LuaYaoYeManJuanMgr = "ui/yaoyemanjuan/LuaYaoYeManJuanMgr",
	LuaBlackScreenMgr = "ui/story/LuaBlackScreenMgr",

	-- 横屿荡寇
	LuaHengYuDangKouMgr = "ui/HengYuDangKou/LuaHengYuDangKouMgr",

	LuaCameraMgr = "ui/common/LuaCameraMgr",
	LuaUIModelPreviewMgr = "ui/LuaUIModelPreviewMgr",

	LuaCreditScoreMgr = "ui/creditscore/LuaCreditScoreMgr",

	LuaHalloween2023Mgr = "ui/halloween2023/LuaHalloween2023Mgr",

	-- 圣诞节2023
	LuaChristmas2023Mgr = "ui/christmas2023/LuaChristmas2023Mgr",
	
	-- 元旦节2024
	LuaYuanDan2024BreakEggWnd = "ui/yuandan2024/LuaYuanDan2024BreakEggWnd",
	LuaYuanDan2024PVEEnterWnd = "ui/yuandan2024/LuaYuanDan2024PVEEnterWnd",
	LuaYuanDan2024PVEGridWnd = "ui/yuandan2024/LuaYuanDan2024PVEGridWnd",

	LuaHanJia2024Mgr = "ui/hanjia2024/LuaHanJia2024Mgr",
	
	-- 春节2024
	LuaChunJie2024Mgr = "ui/chunjie2024/LuaChunJie2024Mgr",
	--元宵2024
	LuaYuanXiao2024AdditionView = "ui/yuanxiao2024/LuaYuanXiao2024AdditionView",
	LuaYuanXiaoYanHua2024BottomWnd = "ui/yuanxiao2024/LuaYuanXiaoYanHua2024BottomWnd",
	LuaYuanXiaoYanHua2024TopRightWnd = "ui/yuanxiao2024/LuaYuanXiaoYanHua2024TopRightWnd",
	LuaYuanXiaoDaLuanDouResultWnd = "ui/yuanxiao2024/LuaYuanXiaoDaLuanDouResultWnd",
	LuaYuanXiaoDaLuanDouSkillWnd = "ui/yuanxiao2024/LuaYuanXiaoDaLuanDouSkillWnd",
	LuaYuanXiaoDaLuanDouTopRightWnd = "ui/yuanxiao2024/LuaYuanXiaoDaLuanDouTopRightWnd",
	LuaYuanXiao2024Mgr = "ui/yuanxiao2024/LuaYuanXiao2024Mgr",


	LuaZNQ2024NLDBEnterWnd = "ui/zhounianqing2024/LuaZNQ2024NLDBEnterWnd",
	LuaZNQ2024NLDBEscapeWnd= "ui/zhounianqing2024/LuaZNQ2024NLDBEscapeWnd",
	LuaZNQ2024NLDBPayWnd = "ui/zhounianqing2024/LuaZNQ2024NLDBPayWnd",
	LuaZNQ2024NLDBResultWnd = "ui/zhounianqing2024/LuaZNQ2024NLDBResultWnd",
	LuaZNQ2024NLDBStartWnd = "ui/zhounianqing2024/LuaZNQ2024NLDBStartWnd",
	LuaZNQ2024NLDBTopRightWnd = "ui/zhounianqing2024/LuaZNQ2024NLDBTopRightWnd",
	LuaZQN2024Mgr = "ui/zhounianqing2024/LuaZNQ2024Mgr",
	LuaZNQ2024NLDBWarDataWnd = "ui/zhounianqing2024/LuaZNQ2024NLDBWarDataWnd",
	
	LuaFashionInfoMgr = "ui/LuaFashionInfoMgr",
	
}
--print("collectgarbage2",collectgarbage("count"))

require "ui/AllUIModulesExt"

local dynamic_load = true
if UNITY_EDITOR then
	dynamic_load = false
end
require "DesignBinaryExt"
local __design_data = require "DesignBinaryInc"
-- editor&2018 using dynamic_load
if dynamic_load then
	setmetatable(_G, { __index = function(t,key)
		local v = rawget(_G,key)
		if v then return v end
		
		if __ui_cache[key] then
			local ok = xpcall(
				function() 
					BeginSample("dynamic_load "..key)	
					require(__ui_cache[key]) 
					EndSample()
				end, 
				function(err)
					print(SafeStringFormat3("<color=red>error:%s, file:%s</color>", err, __ui_cache[key]))
				end)
			if ok then
				__ui_cache[key]=nil
				return rawget(_G,key)
			else
				return nil
			end
		end
		if __design_data[key] then
			local v = __design_data[key]
			if v==1 then
				rawset(_G,key,create_sheet_class(key))
			elseif v==2 then
				rawset(_G,key,create_setting_class(key))
			elseif v==3 then
				rawset(_G,key,ext_create_sheet_class(key))
			elseif v==4 then
				rawset(_G,key,create_sheet_class_with_lazyfields(key))
			elseif v==5 then
				rawset(_G,key,create_setting_class_with_lazyfields(key))
			elseif v==6 then
				rawset(_G,key,ext_create_sheet_class_with_lazyfields(key))
			end
			__design_data[key]=nil
			return rawget(_G,key)
		end
		return nil
	end })
else
	--5.2.4全部require
	for k,v in pairs(__ui_cache) do
		local ok = xpcall(function() require(v) end, function(err)
			print(SafeStringFormat3("<color=red>error:%s, file:%s</color>", err, v))
		end)
	end
	__ui_cache = nil

	setmetatable(_G, { __index = function(t,key)
		local v = rawget(_G,key)
		if v then return v end

		if __design_data[key] then
			local v = __design_data[key]
			if v==1 then
				rawset(_G,key,create_sheet_class(key))
			elseif v==2 then
				rawset(_G,key,create_setting_class(key))
			elseif v==3 then
				rawset(_G,key,ext_create_sheet_class(key))
			elseif v==4 then
				rawset(_G,key,create_sheet_class_with_lazyfields(key))
			elseif v==5 then
				rawset(_G,key,create_setting_class_with_lazyfields(key))
			elseif v==6 then
				rawset(_G,key,ext_create_sheet_class_with_lazyfields(key))
			end
			__design_data[key]=nil
			return rawget(_G,key)
		end
		return nil
	end })
end

require "ui/gas2gac"
