require("common/common_include")
local LuaGameObject=import "LuaGameObject"
local Gac2Gas=import "L10.Game.Gac2Gas"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CLianLianKanMgr=import "L10.Game.CLianLianKanMgr"
local LuaUtils=import "LuaUtils"
local CPlayerInfoMgr=import "CPlayerInfoMgr"
local EnumPlayerInfoContext=import "L10.Game.EnumPlayerInfoContext"
local EChatPanel=import "L10.Game.EChatPanel"
local AlignType=import "CPlayerInfoMgr+AlignType"

CLuaLianLianKanBattleMatchWnd=class()
RegistClassMember(CLuaLianLianKanBattleMatchWnd,"m_IndexLookup1")
RegistClassMember(CLuaLianLianKanBattleMatchWnd,"m_IndexLookup2")
RegistClassMember(CLuaLianLianKanBattleMatchWnd,"m_IndexLookup3")

RegistClassMember(CLuaLianLianKanBattleMatchWnd,"m_Stage1NodeTbl")
RegistClassMember(CLuaLianLianKanBattleMatchWnd,"m_Stage2NodeTbl")
RegistClassMember(CLuaLianLianKanBattleMatchWnd,"m_Stage3NodeTbl")
RegistClassMember(CLuaLianLianKanBattleMatchWnd,"m_Stage4Node")

RegistClassMember(CLuaLianLianKanBattleMatchWnd,"m_StageDescLabel")

RegistClassMember(CLuaLianLianKanBattleMatchWnd,"m_NameLabelTbl")
RegistClassMember(CLuaLianLianKanBattleMatchWnd,"m_MoneyLabelTbl")
RegistClassMember(CLuaLianLianKanBattleMatchWnd,"m_AllMoneyLabel")

RegistClassMember(CLuaLianLianKanBattleMatchWnd,"m_Stage")

function CLuaLianLianKanBattleMatchWnd:Init()
    self.m_StageDescLabel=LuaGameObject.GetChildNoGC(self.transform,"StageDescLabel").label

    self.m_IndexLookup1={{1,16},{2,15},{3,14},{4,13},{5,12},{6,11},{7,10},{8,9}}
    self.m_IndexLookup2={{1,8},{2,7},{3,6},{4,5}}
    self.m_IndexLookup3={{1,4},{2,3}}
    
    self.m_Stage1NodeTbl={}
    self.m_Stage1NodeTbl[1]=LuaGameObject.GetChildNoGC(self.transform,"Stage1-1").transform
    self.m_Stage1NodeTbl[2]=LuaGameObject.GetChildNoGC(self.transform,"Stage1-2").transform
    self.m_Stage1NodeTbl[3]=LuaGameObject.GetChildNoGC(self.transform,"Stage1-3").transform
    self.m_Stage1NodeTbl[4]=LuaGameObject.GetChildNoGC(self.transform,"Stage1-4").transform
    self.m_Stage1NodeTbl[5]=LuaGameObject.GetChildNoGC(self.transform,"Stage1-5").transform
    self.m_Stage1NodeTbl[6]=LuaGameObject.GetChildNoGC(self.transform,"Stage1-6").transform
    self.m_Stage1NodeTbl[7]=LuaGameObject.GetChildNoGC(self.transform,"Stage1-7").transform
    self.m_Stage1NodeTbl[8]=LuaGameObject.GetChildNoGC(self.transform,"Stage1-8").transform

    self.m_Stage2NodeTbl={}
    self.m_Stage2NodeTbl[1]=LuaGameObject.GetChildNoGC(self.transform,"Stage2-1").transform
    self.m_Stage2NodeTbl[2]=LuaGameObject.GetChildNoGC(self.transform,"Stage2-2").transform
    self.m_Stage2NodeTbl[3]=LuaGameObject.GetChildNoGC(self.transform,"Stage2-3").transform
    self.m_Stage2NodeTbl[4]=LuaGameObject.GetChildNoGC(self.transform,"Stage2-4").transform

    self.m_Stage3NodeTbl={}
    self.m_Stage3NodeTbl[1]=LuaGameObject.GetChildNoGC(self.transform,"Stage3-1").transform
    self.m_Stage3NodeTbl[2]=LuaGameObject.GetChildNoGC(self.transform,"Stage3-2").transform

    self.m_Stage4Node=LuaGameObject.GetChildNoGC(self.transform,"Stage4").transform

    self.m_NameLabelTbl={}
    self.m_NameLabelTbl[1]=LuaGameObject.GetChildNoGC(self.transform,"A1").label
    self.m_NameLabelTbl[2]=LuaGameObject.GetChildNoGC(self.transform,"B1").label
    self.m_NameLabelTbl[3]=LuaGameObject.GetChildNoGC(self.transform,"F1").label
    self.m_NameLabelTbl[4]=LuaGameObject.GetChildNoGC(self.transform,"E1").label
    self.m_NameLabelTbl[5]=LuaGameObject.GetChildNoGC(self.transform,"G1").label
    self.m_NameLabelTbl[6]=LuaGameObject.GetChildNoGC(self.transform,"H1").label
    self.m_NameLabelTbl[7]=LuaGameObject.GetChildNoGC(self.transform,"D1").label
    self.m_NameLabelTbl[8]=LuaGameObject.GetChildNoGC(self.transform,"C1").label

    self.m_NameLabelTbl[9]=LuaGameObject.GetChildNoGC(self.transform,"D2").label
    self.m_NameLabelTbl[10]=LuaGameObject.GetChildNoGC(self.transform,"C2").label
    self.m_NameLabelTbl[11]=LuaGameObject.GetChildNoGC(self.transform,"G2").label
    self.m_NameLabelTbl[12]=LuaGameObject.GetChildNoGC(self.transform,"H2").label
    self.m_NameLabelTbl[13]=LuaGameObject.GetChildNoGC(self.transform,"F2").label
    self.m_NameLabelTbl[14]=LuaGameObject.GetChildNoGC(self.transform,"E2").label
    self.m_NameLabelTbl[15]=LuaGameObject.GetChildNoGC(self.transform,"A2").label
    self.m_NameLabelTbl[16]=LuaGameObject.GetChildNoGC(self.transform,"B2").label

    for i,v in ipairs(self.m_NameLabelTbl) do
        CommonDefs.AddOnClickListener(v.gameObject,DelegateFactory.Action_GameObject(function(go)
            self:OnClickPlayerName(go)
        end),false)
    end

    self.m_MoneyLabelTbl={}
    self.m_MoneyLabelTbl[1]=LuaGameObject.GetChildNoGC(self.transform,"MoneyLabel1").label
    self.m_MoneyLabelTbl[2]=LuaGameObject.GetChildNoGC(self.transform,"MoneyLabel2").label
    self.m_MoneyLabelTbl[3]=LuaGameObject.GetChildNoGC(self.transform,"MoneyLabel3").label
    self.m_MoneyLabelTbl[4]=LuaGameObject.GetChildNoGC(self.transform,"MoneyLabel4").label
    self.m_MoneyLabelTbl[5]=LuaGameObject.GetChildNoGC(self.transform,"MoneyLabel5").label

    self.m_AllMoneyLabel=LuaGameObject.GetChildNoGC(self.transform,"AllMoneyLabel").label

    self:ResetUI()
    Gac2Gas.QueryLianLianKanFinalOverView()

    local g = LuaGameObject.GetChildNoGC(self.transform,"RefreshBtn").gameObject
    UIEventListener.Get(g).onClick = DelegateFactory.VoidDelegate(function(go)
        self:ResetUI()
        Gac2Gas.QueryLianLianKanFinalOverView()
    end)
end
function  CLuaLianLianKanBattleMatchWnd:ResetUI(  )
    for i,v in ipairs(self.m_NameLabelTbl) do
        v.text=""
    end
        for i,v in ipairs(self.m_MoneyLabelTbl) do
        v.text="0"
    end
    self.m_AllMoneyLabel.text="0"

    self:SetStageDesc(nil)

    for i=1,8 do
        self:InitStageNode(self.m_Stage1NodeTbl[i],0,0,-1,1)
    end
    for i=1,4 do
        self:InitStageNode(self.m_Stage2NodeTbl[i],0,0,-1,2)
    end
    for i=1,2 do
        self:InitStageNode(self.m_Stage3NodeTbl[i],0,0,-1,3)
    end
    self:InitStageNode(self.m_Stage4Node,0,0,-1,4)
end


function CLuaLianLianKanBattleMatchWnd:GetStageDesc(stage)
    if stage==1 then
        return LocalString.GetString("8强赛正在进行")
    elseif stage==2 then
        return LocalString.GetString("4强赛正在进行")
    elseif stage==3 then
        return LocalString.GetString("半决赛正在进行")
    elseif stage==4 then
        return LocalString.GetString("决赛正在进行")
    elseif stage==5 then
        return LocalString.GetString("比赛已结束")
    end
end

function  CLuaLianLianKanBattleMatchWnd:SetStageDesc( desc )
    if desc then
        self.m_StageDescLabel.text=desc
    else
        self.m_StageDescLabel.text=""
    end
end

function CLuaLianLianKanBattleMatchWnd:OnEnable()
    g_ScriptEvent:AddListener("ReplyLianLianKanFinalOverView", self, "OnReplyLianLianKanFinalOverView")
    
end
function CLuaLianLianKanBattleMatchWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ReplyLianLianKanFinalOverView", self, "OnReplyLianLianKanFinalOverView")

end
function CLuaLianLianKanBattleMatchWnd:SetNormalColor(line)
    if line then
    LuaUtils.SetUIWidgetColor(line,41, 67, 105, 255)
    end
end
function CLuaLianLianKanBattleMatchWnd:SetActiveColor( line )
    if line then
    LuaUtils.SetUIWidgetColor(line,0, 255, 0, 255)
    end
end
function CLuaLianLianKanBattleMatchWnd:InitStageNode(node,playerId1,playerId2,winId,nodeStage)
    local line1=LuaGameObject.GetChildNoGC(node,"Line1").sprite
    local line2=LuaGameObject.GetChildNoGC(node,"Line2").sprite
    local line3=LuaGameObject.GetChildNoGC(node,"Line3").sprite
    local line4=LuaGameObject.GetChildNoGC(node,"Line4").sprite

    local winLabel=LuaGameObject.GetChildNoGC(node,"WinLabel").label
    if winLabel then
        winLabel.text=""
    end
    local stage1Data= CLianLianKanMgr.Instance.stage1Data
    local battleSprite=LuaGameObject.GetChildNoGC(node,"BattleSprite").sprite
    if winId==-1 then--//正在进行 或者没开始
        self:SetNormalColor(line1)
        self:SetNormalColor(line2)
        self:SetNormalColor(line3)
        self:SetNormalColor(line4)
        if battleSprite then
        battleSprite.enabled=nodeStage==self.m_Stage
        end
    elseif winId==0 then--//双方弃权
        self:SetNormalColor(line1)
        self:SetNormalColor(line2)
        self:SetNormalColor(line3)
        self:SetNormalColor(line4)
        if battleSprite then
        battleSprite.enabled=nodeStage==self.m_Stage
        end
    elseif winId==playerId1 then
        self:SetActiveColor(line1)
        self:SetActiveColor(line2)
        self:SetNormalColor(line3)
        self:SetNormalColor(line4)
        if battleSprite then
            battleSprite.enabled=nodeStage==self.m_Stage
        end

        if winLabel then
            for i=1,stage1Data.Count do
                if stage1Data[i-1]==playerId1 then
                    winLabel.text = i
                    break
                end
            end
        end
    elseif winId==playerId2 then
        self:SetNormalColor(line1)
        self:SetNormalColor(line2)
        self:SetActiveColor(line3)
        self:SetActiveColor(line4)
        if battleSprite then
            battleSprite.enabled=nodeStage==self.m_Stage
        end
        if winLabel then
            for i=1,stage1Data.Count do
                if stage1Data[i-1]==playerId2 then
                    winLabel.text = i
                    break
                end
            end
        end
    end
    
    if nodeStage==4 then
        winLabel=LuaGameObject.GetChildNoGC(node,"WinnerLabel").label
        if winLabel then
            winLabel.text=""
        end
        if winId==playerId1 or winId==playerId2 then
            if winLabel then
                for i=1,stage1Data.Count do
                if stage1Data[i-1]==winId then
                    winLabel.text = CLianLianKanMgr.Instance.nameList[i-1]
                    break
                end
            end
            end
        end
    end
end

function CLuaLianLianKanBattleMatchWnd:OnClickPlayerName(go)
    local label=LuaGameObject.GetLuaGameObjectNoGC(go.transform).label
    for i,v in ipairs(self.m_NameLabelTbl) do
        if label==v then
            local stageData=CLianLianKanMgr.Instance.stage1Data
            if stageData and stageData.Count>i then
                local playerId=CLianLianKanMgr.Instance.stage1Data[i-1]
                CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,AlignType.Default)
            end
            break
        end
    end

end

function  CLuaLianLianKanBattleMatchWnd:OnReplyLianLianKanFinalOverView( args )
    local stage=args[0]
    self.m_Stage=stage
    self:SetStageDesc(self:GetStageDesc(stage))
    local myName =""
    local myId=0
    if CClientMainPlayer.Inst then
        myName=CClientMainPlayer.Inst.Name
        myId=CClientMainPlayer.Inst.Id
    end

    local moneyData=CLianLianKanMgr.Instance.moneyData
    for i=1,5 do
        self.m_MoneyLabelTbl[i].text=moneyData[i-1]
    end
    self.m_AllMoneyLabel.text=CLianLianKanMgr.Instance.allMoney

    local stage1Data=CLianLianKanMgr.Instance.stage1Data
    local nameList= CLianLianKanMgr.Instance.nameList
    for i,v in ipairs(self.m_NameLabelTbl) do
        local id=stage1Data[i-1]
        if id==myId then
            v.text="[c][00ff00]"..nameList[i-1].."[-]"
        else
            v.text=nameList[i-1]
        end
    end
    local stage2Data=CLianLianKanMgr.Instance.stage2Data
    local stage3Data=CLianLianKanMgr.Instance.stage3Data
    local stage4Data=CLianLianKanMgr.Instance.stage4Data
    local stage5Data=CLianLianKanMgr.Instance.stage5Data
    for i=1,8 do
        local index1=self.m_IndexLookup1[i][1]-1
        local index2=self.m_IndexLookup1[i][2]-1
        local playerId1=stage1Data[index1]
        local playerId2=stage1Data[index2]
        self:InitStageNode(self.m_Stage1NodeTbl[i],playerId1,playerId2,stage2Data[i-1],1)
    end
    for i=1,4 do
        local index1=self.m_IndexLookup2[i][1]-1
        local index2=self.m_IndexLookup2[i][2]-1
        local playerId1=stage2Data[index1]
        local playerId2=stage2Data[index2]
        self:InitStageNode(self.m_Stage2NodeTbl[i],playerId1,playerId2,stage3Data[i-1],2)
    end
    for i=1,2 do
        local index1=self.m_IndexLookup3[i][1]-1
        local index2=self.m_IndexLookup3[i][2]-1
        local playerId1=stage3Data[index1]
        local playerId2=stage3Data[index2]
        self:InitStageNode(self.m_Stage3NodeTbl[i],playerId1,playerId2,stage4Data[i-1],3)
    end
    local playerId1=stage4Data[0]
    local playerId2=stage4Data[1]
    self:InitStageNode(self.m_Stage4Node,playerId1,playerId2,stage5Data[0],4)
end

return CLuaLianLianKanBattleMatchWnd