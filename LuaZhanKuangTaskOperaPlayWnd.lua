local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local Transform = import "UnityEngine.Transform"
local UISlider = import "UISlider"
local CButton = import "L10.UI.CButton"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local SoundManager = import "SoundManager"
local PlayerSettings = import "L10.Game.PlayerSettings"
local Extensions = import "Extensions"
local Time = import "UnityEngine.Time"
local QnButton = import "L10.UI.QnButton"
local CUIFx = import "L10.UI.CUIFx"
local Color = import "UnityEngine.Color"
local Animation = import "UnityEngine.Animation"
local LuaTweenUtils = import "LuaTweenUtils"
local CGamePlayMgr = import "L10.Game.CGamePlayMgr"

LuaZhanKuangTaskOperaPlayWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZhanKuangTaskOperaPlayWnd, "Tip1", "Tip1", GameObject)
RegistChildComponent(LuaZhanKuangTaskOperaPlayWnd, "Tip2", "Tip2", GameObject)
RegistChildComponent(LuaZhanKuangTaskOperaPlayWnd, "Tip1Lab", "Tip1Lab", UILabel)
RegistChildComponent(LuaZhanKuangTaskOperaPlayWnd, "Tip2Lab", "Tip2Lab", UILabel)
RegistChildComponent(LuaZhanKuangTaskOperaPlayWnd, "Cross", "Cross", Transform)
RegistChildComponent(LuaZhanKuangTaskOperaPlayWnd, "Librettos", "Librettos", GameObject)
RegistChildComponent(LuaZhanKuangTaskOperaPlayWnd, "TemplateLab", "TemplateLab", UILabel)
RegistChildComponent(LuaZhanKuangTaskOperaPlayWnd, "Sliders", "Sliders", GameObject)
RegistChildComponent(LuaZhanKuangTaskOperaPlayWnd, "TemplateSlider", "TemplateSlider", UISlider)
RegistChildComponent(LuaZhanKuangTaskOperaPlayWnd, "OpBtns", "OpBtns", Transform)
RegistChildComponent(LuaZhanKuangTaskOperaPlayWnd, "LinesTip", "LinesTip", Transform)
RegistChildComponent(LuaZhanKuangTaskOperaPlayWnd, "CloseButton", "CloseButton", CButton)

--@endregion RegistChildComponent end
-- 音乐
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_MusicName")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_Sound")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_SoundLen")
-- 错误提示
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_Crosses")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_CrossGrid")
-- 进度条
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_Sliders")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_SlidersPercent")
-- 歌词
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_Lyric")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_LyricChar")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_LyricBg")
-- 按钮
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_OpBtns")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_CorrectFx")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_WrongFx")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_ClickFxTishi")
-- 数据
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_TrueButton")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_MarkerPercent")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_Type")
-- 其它
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_Tick")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_IsPlaying")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_TotalLength")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_OffsetY")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_CanWrong")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_StartTime")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_ClickResults")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_CurLyricIndex")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_CorrectPrecent")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_CorrectTimeRange")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_WrongNum")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_ChangeLimit")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_OperaPlayBg")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_AlertBg")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_Key")

RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_MarkerTimes")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_Dot")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_Animation")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_Animation2")

RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_CorrectBgCol")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_WrongBgCol")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_InactiveBgCol")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_InactiveCol")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_TishiTempFx")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_ResultTempFx")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_AllFx")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_TishiFx")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_ResultFx")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_PauseTime")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_Clicked")
RegistClassMember(LuaZhanKuangTaskOperaPlayWnd, "m_VolumeBtn")
function LuaZhanKuangTaskOperaPlayWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

    self.m_OperaPlayBg  = self.transform:Find("Anchor/OperaPlay/Bg/OperaPlayBg"):GetComponent(typeof(UITexture))
    self.m_AlertBg      = self.transform:Find("Anchor/OperaPlay/Bg/OperaPlayBg_Alert"):GetComponent(typeof(UITexture))
    self.m_Dot          = self.transform:Find("Anchor/OperaPlay/Progress/Dot")
    self.m_Animation    = self.transform:GetComponent(typeof(Animation))
    self.m_Animation2   = self.transform:Find("Anchor"):GetComponent(typeof(Animation))
    self.m_TishiTempFx  = self.transform:Find("Anchor/OperaPlay/TipLineTemplateFX").gameObject
    self.m_ResultTempFx = self.transform:Find("Anchor/OperaPlay/ResultTemplateFX").gameObject
    self.m_AllFx        = self.transform:Find("Anchor/OperaPlay/AllFx").gameObject
    self.m_VolumeBtn    = self.transform:Find("Anchor/VolumeBtn"):GetComponent(typeof(CButton))


    self.m_CorrectBgCol = NGUIText.ParseColor24("4469A0", 0)
    self.m_WrongBgCol   = NGUIText.ParseColor24("7A3838", 0)
    self.m_InactiveCol  = NGUIText.ParseColor24("7B8898", 0)
    self.m_InactiveBgCol= NGUIText.ParseColor24("3B4652", 0)


    -- 错误提示
    self.m_CrossGrid = self.Cross:GetComponent(typeof(UIGrid))
    self.m_Crosses = {}

    -- 进度条
    self.m_Sliders = {}
    self.m_SlidersPercent = {}

    -- 歌词
    self.m_Lyric = {}
    self.m_LyricChar = {}

    -- 按钮
    self.m_OpBtns = {}
    for i = 3, 1, -1 do
        local opBtn = self.OpBtns:Find("Btn" .. i):GetComponent(typeof(QnButton))
        opBtn.OnClick = DelegateFactory.Action_QnButton(function (btn)
            self:OnBtnClick(4-i, btn)
        end)
        table.insert(self.m_OpBtns, opBtn)
    end

    UIEventListener.Get(self.CloseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnCloseClick(go)
    end)

    UIEventListener.Get(self.m_VolumeBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnSoundSettingButtonClick(go, true)
    end)

    self.m_ClickResults = {}

    self.m_TotalLength = 1354
    self.m_OffsetY = {-90, 0, 90}

    self.m_IsPlaying = false
    self.m_WrongNum = 0
    self.m_CurLyricIndex = 0

    self.m_MarkerTimes = {}
    self.m_TishiFx = {}
    self.m_ResultFx = {}
    self.m_Clicked = {}
end

function LuaZhanKuangTaskOperaPlayWnd:Init()
    self.m_Key = LuaZhuJueJuQingMgr.OperaPlayKey or 1
    self:InitData()

    self:InitCross(self.m_CanWrong)
    self:UpdateCross(0)

    self:InitLyricAndProgress()

    self:LoadMusic(0)
end

function LuaZhanKuangTaskOperaPlayWnd:Update()
    local totalPercent = 0
    if self.m_SoundLen then
        if self.m_IsPlaying then
            totalPercent = (Time.realtimeSinceStartup - self.m_StartTime) / self.m_SoundLen
        else
            totalPercent = self.m_MarkerPercent[self.m_CurLyricIndex] or 0
        end
    end
    self:UpdateProgress(totalPercent)
    self:UpdateTipLineAndTipFx(totalPercent)
end

--@region UIEvent
function LuaZhanKuangTaskOperaPlayWnd:OnBtnClick(i, btn)
    if self.m_CurLyricIndex == 0 or self.m_Clicked[self.m_CurLyricIndex] then
        return
    end
    if self.m_Type == 0 and self.m_IsPlaying then
        return 
    end

    local trueBtn = self.m_TrueButton[self.m_CurLyricIndex]
    if trueBtn == i then
        self.m_Clicked[self.m_CurLyricIndex] = true
        self:OnClickSuccess(self.m_CurLyricIndex)
        self.m_ClickResults[self.m_CurLyricIndex] = i
        if self.m_Type == 0 then
            self:Continue()
        end
    else
        if self.m_Type == 1 then
            self.m_Clicked[self.m_CurLyricIndex] = true
            self.m_ClickResults[self.m_CurLyricIndex] = i
        end
        self:OnClickFail(self.m_CurLyricIndex, i)
    end
end

function LuaZhanKuangTaskOperaPlayWnd:OnCloseClick(go)
    CGamePlayMgr.Inst:LeavePlay()
end

function LuaZhanKuangTaskOperaPlayWnd:OnSoundSettingButtonClick(go, isLeft)
    if not CUIManager.IsLoaded(CLuaUIResources.MiniVolumeSettingsWnd) then
        LuaMiniVolumeSettingsMgr:OpenWnd(go, go.transform.position, UIWidget.Pivot.Left)
    else
        CUIManager.CloseUI(CLuaUIResources.MiniVolumeSettingsWnd)
    end
end
--@endregion UIEvent

function LuaZhanKuangTaskOperaPlayWnd:OnClickSuccess(lyricIndex)
    local lyric = self.m_Lyric[lyricIndex]
    local bg = self.m_LyricBg[lyricIndex]
    lyric.color = Color.white
    bg.color = self.m_CorrectBgCol

    self:PlayResultFx(lyricIndex, lyricIndex, "dui")
end

function LuaZhanKuangTaskOperaPlayWnd:OnClickFail(lyricIndex, realBtnIdx)
    local result = realBtnIdx
    local go = NGUITools.AddChild(self.Librettos.gameObject, self.TemplateLab.gameObject)
    go:SetActive(true)
    local wrongLyric = go:GetComponent(typeof(UILabel))
    wrongLyric.text = self.m_LyricChar[lyricIndex]
    local bg = go.transform:Find("Bg"):GetComponent(typeof(UITexture))
    bg.color = self.m_WrongBgCol

    wrongLyric.alpha = 0
    if self.m_Type == 1 then
        local tweener = LuaTweenUtils.TweenAlpha(wrongLyric, 0, 1, 0.05)
        LuaTweenUtils.SetDelay(tweener, 0.18)
    end

    local markerPercent = self.m_MarkerPercent[lyricIndex] or 0
    local x = markerPercent * self.m_TotalLength
    local y = self.m_OffsetY[result]
    LuaUtils.SetLocalPosition(wrongLyric.transform, x, y, 0)

    local wrongButton = self.m_OpBtns[realBtnIdx]
    self:PlayResultFx(lyricIndex .. realBtnIdx, lyricIndex, "cuo", wrongLyric, wrongButton)
    self:AddOneCross()
end

function LuaZhanKuangTaskOperaPlayWnd:OnClickMiss(lyricIndex)
    self:PlayResultFx(lyricIndex, lyricIndex, "que")
    self:AddOneCross()
end

function LuaZhanKuangTaskOperaPlayWnd:RefreshTipLineWidth(lyric, button, line)
    line.transform.position = button.transform.position
    local startPos = lyric.transform.localPosition
    local endPos = lyric.transform.parent:InverseTransformPoint(button.transform.position)
    local offset = CommonDefs.op_Subtraction_Vector3_Vector3(endPos, startPos)
    local width = offset.magnitude
    line.width = width
end

function LuaZhanKuangTaskOperaPlayWnd:AddOneCross()
    if self.m_Type == 0 then
        return
    end

    self.m_WrongNum = self.m_WrongNum + 1
    self:UpdateCross(self.m_WrongNum)
    if self.m_WrongNum >= self.m_CanWrong then
        self:OnFail()
    end

    if self.m_CanWrong - self.m_WrongNum <= self.m_ChangeLimit then
        self.m_OperaPlayBg.gameObject:SetActive(false)
        self.m_AlertBg.gameObject:SetActive(true)
    end
end

function LuaZhanKuangTaskOperaPlayWnd:OnSuccess()
    Gac2Gas.OnFinishJuQingOperaPlay(self.m_Key, true)
    CUIManager.CloseUI(CLuaUIResources.ZhanKuangTaskOperaPlayWnd)
end

function LuaZhanKuangTaskOperaPlayWnd:OnFail()
    self:EndMusic()
    Gac2Gas.OnFinishJuQingOperaPlay(self.m_Key, false)
    CUIManager.CloseUI(CLuaUIResources.ZhanKuangTaskOperaPlayWnd)
end

function LuaZhanKuangTaskOperaPlayWnd:OnEnable()
    g_ScriptEvent:AddListener("OnFmodMarkerTrigger", self, "OnFmodMarkerTrigger")
    g_ScriptEvent:AddListener("OnMiniVolumeSettingsChanged", self, "OnMiniVolumeSettingsChanged")
    CUIManager.HideUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EGameUI, nil, false, true)
	CUIManager.HideUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EBgGameUI, nil, false, true)
end

function LuaZhanKuangTaskOperaPlayWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnFmodMarkerTrigger", self, "OnFmodMarkerTrigger")
    g_ScriptEvent:RemoveListener("OnMiniVolumeSettingsChanged", self, "OnMiniVolumeSettingsChanged")
    CUIManager.ShowUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EGameUI, nil, false, true)
	CUIManager.ShowUIGroupByChangeLayer(CUIManager.EUIModuleGroup.EBgGameUI, nil, false, true)
end

function LuaZhanKuangTaskOperaPlayWnd:LoadMusic(time)
    if self.m_MusicName and self.m_IsPlaying == false then
        SoundManager.Inst:ChangeBgMusicVolume("ZhanKuangTaskOperaPlay", 0, false)
        local volum = PlayerSettings.MusicEnabled and PlayerSettings.VolumeSetting or 0
        self.m_Sound = SoundManager.Inst:PlaySound(self.m_MusicName, Vector3.zero, volum, nil, time)
        local slen = SoundManager.Inst:GetEventLength(self.m_Sound)
        self.m_SoundLen = slen / 1000
        self.m_CorrectPrecent = self.m_CorrectTimeRange / self.m_SoundLen
        self.m_StartTime = Time.realtimeSinceStartup
        SoundManager.Inst:AddMarkerCallback(self.m_Sound)
        UnRegisterTick(self.m_Tick)
        self.m_Tick = RegisterTickOnce(function()
            self:OnMusicFinish()
        end, slen)
        self.m_IsPlaying = true
    end
end

function LuaZhanKuangTaskOperaPlayWnd:Pause(id)
    if not self.m_IsPlaying then
        return
    end
    self.m_IsPlaying = false

    self.m_PauseTime = Time.realtimeSinceStartup
    UnRegisterTick(self.m_Tick)
    SoundManager.Inst:SetSoundPaused(self.m_Sound, true)

    self:PlayTishiFx(self.m_CurLyricIndex, true)
end

function LuaZhanKuangTaskOperaPlayWnd:Continue()
    if not self.m_PauseTime or self.m_IsPlaying then
        return
    end
    self.m_IsPlaying = true
    local offset = self.m_PauseTime - self.m_StartTime
    self.m_StartTime = Time.realtimeSinceStartup - offset
    local slen = (self.m_SoundLen - offset) * 1000
    UnRegisterTick(self.m_Tick)
    self.m_Tick = RegisterTickOnce(function()
        self:OnMusicFinish()
    end, slen)

    SoundManager.Inst:SetSoundPaused(self.m_Sound, false)

    self:PlayTishiFx(self.m_CurLyricIndex, false)
end

function LuaZhanKuangTaskOperaPlayWnd:OnMusicFinish()
    --print(table.concat(self.m_MarkerTimes, ","))
    self:EndMusic()
    self:OnSuccess()
end

function LuaZhanKuangTaskOperaPlayWnd:EndMusic()
    SoundManager.Inst:StopSound(self.m_Sound)
    SoundManager.Inst:ChangeBgMusicVolume("ZhanKuangTaskOperaPlay", 1, true)
    self.m_IsPlaying = false
end

function LuaZhanKuangTaskOperaPlayWnd:OnFmodMarkerTrigger(params)
    local path = params[0]
    local name = params[1]
    local position = params[2]
    local mark = tonumber(name)
    if name == "Marker C" then
        UnRegisterTick(self.m_Tick)
        self:OnMusicFinish()
    end
    if self.m_Type == 0 and tonumber(name) then
        self:Pause()
        --table.insert(self.m_MarkerTimes, SafeStringFormat3("%.3f", (Time.realtimeSinceStartup - self.m_StartTime) / self.m_SoundLen))
    end
end

function LuaZhanKuangTaskOperaPlayWnd:OnMiniVolumeSettingsChanged()
    if self.m_Sound then
        SoundManager.Inst:UpdateVolume(self.m_Sound, PlayerSettings.MusicEnabled and PlayerSettings.VolumeSetting or 0)
    end
end

-- 初始化数据
function LuaZhanKuangTaskOperaPlayWnd:InitData()
    local data = ZhuJueJuQing_Kunqu.GetData(self.m_Key)
    if data == nil then
        return 
    end
    local lyric = data.Lyric
    local message = CUICommonDef.TranslateToNGUIText(data.Message)

    self.m_LyricChar = {}
    for i = 1, CommonDefs.StringLength(lyric) do
        table.insert(self.m_LyricChar, CommonDefs.StringSubstring2(lyric, i - 1, 1))
    end

    self.m_MusicName = data.Audio
    self.m_TrueButton = {}
    for i = 0, data.TrueButton.Length - 1 do
        table.insert(self.m_TrueButton, data.TrueButton[i])
    end
    self.m_MarkerPercent = {}
    for i = 0, data.MakerPercent.Length - 1 do
        table.insert(self.m_MarkerPercent, data.MakerPercent[i])
    end
    self.m_Type = data.Type
    self.m_CanWrong = data.CanWrong
    self.m_ChangeLimit = data.ChangeLimit

    if self.m_Type == 0 then
        self.Tip1Lab.text = message
    else
        self.Tip2Lab.text = message
    end

    self.Tip1:SetActive(self.m_Type == 0)
    self.Tip2:SetActive(self.m_Type == 1)
    self.m_CorrectTimeRange = data.CorrectTimeRange
    self.m_CorrectPrecent = 0.02
end

-- 初始化组件
function LuaZhanKuangTaskOperaPlayWnd:InitCross(num)
    for i = 1, num do
        local go = NGUITools.AddChild(self.Cross.gameObject, self.Cross:GetChild(0).gameObject)
        go:SetActive(true)
        table.insert(self.m_Crosses, go:GetComponent(typeof(UITexture)))
    end
    self.Cross:GetChild(0).gameObject:SetActive(false)
    self.m_CrossGrid:Reposition()
end

function LuaZhanKuangTaskOperaPlayWnd:InitLyricAndProgress()
    self.m_Lyric = {}
    self.m_LyricBg = {}
    Extensions.RemoveAllChildren(self.Librettos.transform)
    for i = 1, #self.m_LyricChar do
        local go = NGUITools.AddChild(self.Librettos.gameObject, self.TemplateLab.gameObject)
        go:SetActive(true)
        table.insert(self.m_Lyric, go:GetComponent(typeof(UILabel)))
        self.m_Lyric[i].text = self.m_LyricChar[i]
        self.m_LyricBg[i] = go.transform:Find("Bg"):GetComponent(typeof(UITexture))
        self.m_Lyric[i].color = self.m_InactiveCol
        self.m_LyricBg[i].color = self.m_InactiveBgCol
        local markerPercent = self.m_MarkerPercent[i] or 0
        local x = markerPercent * self.m_TotalLength
        local indexY = self.m_TrueButton[i]
        local y = self.m_OffsetY[indexY]
        LuaUtils.SetLocalPosition(self.m_Lyric[i].transform, x, y, 0)
    end
    self.TemplateLab.gameObject:SetActive(false)

    self.m_Sliders = {}
    self.m_SlidersPercent = {}
    Extensions.RemoveAllChildren(self.Sliders.transform)
    for i = 1, (#self.m_Lyric + 1) do
        local go = NGUITools.AddChild(self.Sliders, self.TemplateSlider.gameObject)
        go:SetActive(true)
        table.insert(self.m_Sliders, go:GetComponent(typeof(UISlider)))
        local percent = i == 1 and self.m_MarkerPercent[i] or (i == (#self.m_Lyric + 1) and 1 or self.m_MarkerPercent[i]) - self.m_MarkerPercent[i - 1]
        table.insert(self.m_SlidersPercent, percent)
        local startPos = i == 1 and Vector3(0, self.m_OffsetY[self.m_TrueButton[i]], 0) or self.m_Lyric[i - 1].transform.localPosition
        local endPos = i == (#self.m_Lyric + 1) and Vector3(self.m_TotalLength, self.m_OffsetY[self.m_TrueButton[i - 1]], 0)  or self.m_Lyric[i].transform.localPosition
        self:InitSliderPosAndRot(self.m_Sliders[i], startPos, endPos)
        self.m_Sliders[i].value = 0
    end
    self.TemplateSlider.gameObject:SetActive(false)
end

function LuaZhanKuangTaskOperaPlayWnd:InitSliderPosAndRot(slider, startPos, endPos)
    slider.transform.localPosition = startPos
    local dir = CommonDefs.op_Subtraction_Vector3_Vector3(endPos, startPos)
    local angle = math.atan2(dir.y, dir.x) * 180 / math.pi
    Extensions.SetLocalRotationZ(slider.transform, angle)
    slider.foregroundWidget.width = dir.magnitude
end

-- 更新
function LuaZhanKuangTaskOperaPlayWnd:UpdateCross(num)
    for i = 1, #self.m_Crosses do
        self.m_Crosses[i].enabled = i <= num
    end
end

function LuaZhanKuangTaskOperaPlayWnd:UpdateProgress(value)
    local curPercent = 0
    for i = 1, #self.m_Sliders do
        local slider    = self.m_Sliders[i]
        local percent   = self.m_SlidersPercent[i]
        local trueValue = Mathf.Clamp((value - curPercent) / percent, 0, 1)

        if trueValue ~= 1 then
            slider.thumb = self.m_Dot
            slider.value = trueValue
            slider:ForceUpdate()
            break
        end

        slider.thumb    = nil
        slider.value    = trueValue
        curPercent      = curPercent + percent
    end
end

function LuaZhanKuangTaskOperaPlayWnd:UpdateTipLineAndTipFx(value)
    local i = 0
    for k = 1, #self.m_MarkerPercent do
        if math.abs(value - self.m_MarkerPercent[k]) < self.m_CorrectPrecent and (not self.m_ClickResults[k]) then
            i = k
            break
        end
    end
    if self.m_CurLyricIndex == i then
        return 
    end
    if self.m_CurLyricIndex ~= 0 and (not self.m_ClickResults[self.m_CurLyricIndex]) then
        self:OnClickMiss(self.m_CurLyricIndex)
    end

    self.m_LastLyricIndex = self.m_CurLyricIndex
    self.m_CurLyricIndex = i

    if self.m_Type == 0 then
        return
    end

    if self.m_CurLyricIndex ~= 0 then
        self:PlayTishiFx(self.m_CurLyricIndex, true)
    end
    
    if self.m_LastLyricIndex and self.m_LastLyricIndex ~= 0 then
        self:PlayTishiFx(self.m_LastLyricIndex, false)
    end
end

function LuaZhanKuangTaskOperaPlayWnd:PlayResultFx(name, key, result, wrongLyric, wrongButton)
    local go = self.m_ResultFx[name]
    if go == nil then
        go = NGUITools.AddChild(self.m_AllFx, self.m_ResultTempFx)
        self.m_ResultFx[name] = go
    end
    local animation = go:GetComponent(typeof(Animation))
    local clickFx = go.transform:Find("ClickFx")
    local resultFx = go.transform:Find("ResultFx")
    local line = nil
    if result ~= "que" then
        line = go.transform:Find(SafeStringFormat3("%s/Line1", result)):GetComponent(typeof(UITexture))
    end
    local label = go.transform:Find(SafeStringFormat3("ResultFx/Fx/%s_b/Label (1)", result)):GetComponent(typeof(UILabel))
    local label2 = nil
    if result == "dui" then
        label2 = go.transform:Find("ResultFx/Bg_Correct/Label"):GetComponent(typeof(UILabel))
    elseif result == "cuo" then
        label2 = go.transform:Find("ResultFx/Bg_Wrong/Label"):GetComponent(typeof(UILabel))
    elseif result == "que" then
        label2 = go.transform:Find("ResultFx/Bg_Inactive/Label"):GetComponent(typeof(UILabel))
    end

    local lyric = wrongLyric or self.m_Lyric[key]
    local index = self.m_TrueButton[key]
    local button = wrongButton or self.m_OpBtns[index]
    local lyricChar = result ~= "que" and self.m_LyricChar[key] or LocalString.GetString("缺")
    local aniName = "zhankuangtaskoperaplaywnd_" .. result

    clickFx.position = button.transform.position
    resultFx.position = lyric.transform.position
    label.text = lyricChar
    label2.text = lyricChar
    animation:Play(aniName)
    if line then
        self:RefreshTipLineWidth(lyric, button, line)
    end
end

function LuaZhanKuangTaskOperaPlayWnd:PlayTishiFx(key, isShow)
    if not self.m_Lyric[key] then
        return 
    end
    local go = self.m_TishiFx[key]
    if go == nil then 
        go = NGUITools.AddChild(self.m_AllFx, self.m_TishiTempFx)
        self.m_TishiFx[key] = go
    end
    local animation = go:GetComponent(typeof(Animation))
    local tishiFx = go.transform:Find("Tishifx")
    local clickFx = go.transform:Find("ClickFxTishi")
    local line = go.transform:Find("tishi/Line1"):GetComponent(typeof(UITexture))
    local lab = go.transform:Find("BtnLab"):GetComponent(typeof(UILabel))

    local lyric = self.m_Lyric[key]
    local index = self.m_TrueButton[key]
    local button = self.m_OpBtns[index]
    local btnLab = button.transform:Find("BtnLab"):GetComponent(typeof(UILabel))

    lab.text = btnLab.text
    tishiFx.position = lyric.transform.position
    clickFx.position = button.transform.position
    lab.transform.position = button.transform.position
    if isShow then
        animation:Play("zhankuangtaskoperaplaywnd_tishi01")
        animation:PlayQueued("zhankuangtaskoperaplaywnd_tishi02")
    else
        animation:Play("zhankuangtaskoperaplaywnd_tishi03")
    end
    self:RefreshTipLineWidth(lyric, button, line)
end

function LuaZhanKuangTaskOperaPlayWnd:OnDestroy()
    UnRegisterTick(self.m_Tick)
    self:EndMusic()
    CUIManager.CloseUI(CLuaUIResources.MiniVolumeSettingsWnd)
end
