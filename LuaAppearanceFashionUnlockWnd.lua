local GameObject = import "UnityEngine.GameObject"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local UILabel = import "UILabel"
local CBaseWnd = import "L10.UI.CBaseWnd"
local Animation = import "UnityEngine.Animation"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CUITexture = import "L10.UI.CUITexture"


LuaAppearanceFashionUnlockWnd = class()
RegistChildComponent(LuaAppearanceFashionUnlockWnd, "m_ModelTexture", "ModelTexture", UITexture)
RegistChildComponent(LuaAppearanceFashionUnlockWnd, "m_ConfirmRoot", "ConfirmRoot", GameObject)
RegistChildComponent(LuaAppearanceFashionUnlockWnd, "m_BigItemIcon", "BigItem", CUITexture)
RegistChildComponent(LuaAppearanceFashionUnlockWnd, "m_BigItemBorder", "BigBorder", UISprite)
RegistChildComponent(LuaAppearanceFashionUnlockWnd, "m_SmallItemIcon", "SmallItem", CUITexture)
RegistChildComponent(LuaAppearanceFashionUnlockWnd, "m_SmallItemBorder", "SmallBorder", UISprite)
RegistChildComponent(LuaAppearanceFashionUnlockWnd, "m_TextLabel", "TextLabel", UILabel)
RegistChildComponent(LuaAppearanceFashionUnlockWnd, "m_OkButton", "OkButton", GameObject)
RegistChildComponent(LuaAppearanceFashionUnlockWnd, "m_CancelButton", "CancelButton", GameObject)
RegistChildComponent(LuaAppearanceFashionUnlockWnd, "m_UnlockSuccessRoot", "UnlockSuccessRoot", GameObject)
RegistChildComponent(LuaAppearanceFashionUnlockWnd, "m_ItemIcon", "Item", CUITexture)
RegistChildComponent(LuaAppearanceFashionUnlockWnd, "m_ItemBorder", "Border", UISprite)
RegistChildComponent(LuaAppearanceFashionUnlockWnd, "m_ItemNameLabel", "ItemNameLabel", UILabel)
RegistChildComponent(LuaAppearanceFashionUnlockWnd, "m_PlayerInfo", "PlayerInfo", GameObject)
RegistChildComponent(LuaAppearanceFashionUnlockWnd, "m_ShareHideRoot", "BottomRight", GameObject)
RegistChildComponent(LuaAppearanceFashionUnlockWnd, "m_ShareButton", "ShareButton", GameObject)
RegistChildComponent(LuaAppearanceFashionUnlockWnd, "m_PresentButton", "PresentButton", GameObject)

RegistClassMember(LuaAppearanceFashionUnlockWnd, "m_Ani")
RegistClassMember(LuaAppearanceFashionUnlockWnd, "m_RequestUnlockFasionId")
RegistClassMember(LuaAppearanceFashionUnlockWnd, "m_RequestUnlockFasionPos")

function LuaAppearanceFashionUnlockWnd:Awake()
    self.m_Ani = self.transform:GetComponent(typeof(Animation))
    UIEventListener.Get(self.m_CancelButton).onClick = DelegateFactory.VoidDelegate(function(go) self:OnCancelButtonClick() end)
    UIEventListener.Get(self.m_OkButton).onClick = DelegateFactory.VoidDelegate(function(go) self:OnOkButtonClick() end)
    UIEventListener.Get(self.m_PresentButton).onClick = DelegateFactory.VoidDelegate(function(go) self:OnPresentButtonClick() end)
    UIEventListener.Get(self.m_ShareButton).onClick = DelegateFactory.VoidDelegate(function(go) self:OnShareButtonClick() end)
    self.m_ShareButton:SetActive(not CommonDefs.IS_VN_CLIENT)
end

function LuaAppearanceFashionUnlockWnd:Init()
    self.m_RequestUnlockFasionId = LuaAppearancePreviewMgr.m_RequestUnlockFasionId
    local fashionData = Fashion_Fashion.GetData(self.m_RequestUnlockFasionId)
    local fashionName = LuaAppearancePreviewMgr:FilterFashionName(fashionData.Name)
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer==nil then
        self:Close()
        return
    end
    self.m_RequestUnlockFasionPos = fashionData.Position
    local extInfo = LuaAppearancePreviewMgr.m_RequestUnlockFasionExtInfo
    local progress = extInfo.type==EnumAppearanceFashionUnlockType.eItem and extInfo.progress or
                        LuaAppearancePreviewMgr:GetFashionUnlockProgressInfo(self.m_RequestUnlockFasionId)
    if extInfo.type==EnumAppearanceFashionUnlockType.eItem then
        if progress<=0 then
            self.m_TextLabel.text = g_MessageMgr:FormatMessage("Appearance_Fashion_Unlock_Confirm", fashionName)
        else
            local score = math.floor(progress*fashionData.AppearanceScore30/30)
            self.m_TextLabel.text = g_MessageMgr:FormatMessage("Appearance_Fashion_Unlock_Remain_Durability_Confirm", fashionName, score)
        end
    else
        if progress<=fashionData.Durability then
            self.m_TextLabel.text = g_MessageMgr:FormatMessage("Appearance_Fashion_Unlock_Confirm", fashionName)
        else
            local score = math.floor((progress-fashionData.Durability)*fashionData.AppearanceScore30/30)
            self.m_TextLabel.text = g_MessageMgr:FormatMessage("Appearance_Fashion_Unlock_Remain_Durability_Confirm", fashionName, score)
        end
    end
    local qualitySprite = LuaAppearancePreviewMgr:GetFashionQualityBorder(fashionData.Quality)
    if fashionData.Multicolor==0 then
        self.m_BigItemIcon.gameObject:SetActive(true)
        self.m_SmallItemIcon.gameObject:SetActive(false)
        self.m_BigItemIcon:LoadMaterial(LuaAppearancePreviewMgr:GetFashionBigIcon(fashionData))
        self.m_BigItemBorder.spriteName = qualitySprite
    else
        self.m_BigItemIcon.gameObject:SetActive(false)
        self.m_SmallItemIcon.gameObject:SetActive(true)
        self.m_SmallItemIcon:LoadMaterial(fashionData.Icon)
        self.m_SmallItemBorder.spriteName = qualitySprite
    end
    self.m_ItemIcon:LoadMaterial(fashionData.Icon)
    self.m_ItemBorder.spriteName = qualitySprite
    self.m_ItemNameLabel.text = fashionName

    self.m_PlayerInfo.transform:Find("Icon"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(mainplayer.Class, mainplayer.Gender, -1), false)
    self.m_PlayerInfo.transform:Find("Name/Label"):GetComponent(typeof(UILabel)).text = mainplayer.Name
    self.m_PlayerInfo.transform:Find("Icon/LevelLabel"):GetComponent(typeof(UILabel)).text = tostring(mainplayer.Level)
    self.m_PlayerInfo.transform:Find("ID/Label"):GetComponent(typeof(UILabel)).text = tostring(mainplayer.Id)
    self.m_PlayerInfo.transform:Find("Server/Label"):GetComponent(typeof(UILabel)).text = mainplayer:GetMyServerName()
end

function LuaAppearanceFashionUnlockWnd:LoadModel(fashionId, fashionPos)
    self.m_ModelTexture.mainTexture = CUIManager.CreateModelTexture("__AppearanceFashionUnlockWnd__", self:GetModelLoader(fashionId, fashionPos),
        180, 0.05, -1, 4.66, false, true, 1.0, true, false)
end

function LuaAppearanceFashionUnlockWnd:GetModelLoader(fashionId, fashionPos)
    return LuaDefaultModelTextureLoader.Create(function (ro)
        local fakeAppear = LuaAppearancePreviewMgr:GetPreviewFashionAppear(fashionId, fashionPos)
        CClientMainPlayer.LoadResource(ro, fakeAppear, true, 1.0, 0, false, 0, false, LuaAppearancePreviewMgr:GetFashionUnlockExpression(), false, nil, false, false)
    end)
end

function LuaAppearanceFashionUnlockWnd:OnDestroy()
    CUIManager.DestroyModelTexture("__AppearanceFashionUnlockWnd__")
end

function LuaAppearanceFashionUnlockWnd:OnEnable()
    g_ScriptEvent:AddListener("OnUnLockFashionSuccess_Permanent",self,"OnUnLockFashionSuccess_Permanent")
end

function LuaAppearanceFashionUnlockWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnUnLockFashionSuccess_Permanent",self,"OnUnLockFashionSuccess_Permanent")
end

function LuaAppearanceFashionUnlockWnd:OnUnLockFashionSuccess_Permanent(fashionId)
    if fashionId == self.m_RequestUnlockFasionId then
        self:LoadModel(self.m_RequestUnlockFasionId, self.m_RequestUnlockFasionPos)
        self.m_Ani:Play("appearancefashionunlockwnd_confirm")
    end
end

function LuaAppearanceFashionUnlockWnd:Close()
    self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaAppearanceFashionUnlockWnd:OnOkButtonClick()
    local extInfo = LuaAppearancePreviewMgr.m_RequestUnlockFasionExtInfo
    if extInfo.type==EnumAppearanceFashionUnlockType.eItem then
        LuaAppearancePreviewMgr:ConfirmUnLockFashionByItem_Permanent(self.m_RequestUnlockFasionId, extInfo.itemId, extInfo.place, extInfo.pos)
    else
        LuaAppearancePreviewMgr:RequestUnLockFashion_Permanent(self.m_RequestUnlockFasionId)
    end
end

function LuaAppearanceFashionUnlockWnd:OnCancelButtonClick()
    self:Close()
end

function LuaAppearanceFashionUnlockWnd:OnShareButtonClick()
    CUICommonDef.CaptureScreenUIAndShare(CLuaUIResources.AppearanceFashionUnlockWnd, self.m_ShareHideRoot)    
end

function LuaAppearanceFashionUnlockWnd:OnPresentButtonClick()
    --复用同享的按钮用于立即穿上功能
    LuaAppearancePreviewMgr:RequestTakeOnFashion_Permanent(self.m_RequestUnlockFasionId)
    LuaAppearancePreviewMgr:ShowClosetFashionSubview(self.m_RequestUnlockFasionId)
    self:Close()
end
