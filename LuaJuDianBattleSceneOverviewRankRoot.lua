local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"

LuaJuDianBattleSceneOverviewRankRoot = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaJuDianBattleSceneOverviewRankRoot, "RankRoot", "RankRoot", GameObject)
RegistChildComponent(LuaJuDianBattleSceneOverviewRankRoot, "NoneBangHuiHint", "NoneBangHuiHint", GameObject)

--@endregion RegistChildComponent end

function LuaJuDianBattleSceneOverviewRankRoot:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaJuDianBattleSceneOverviewRankRoot:OnRankData(rankData)
    -- 显示排名
    self.RankRoot:SetActive(true)

    local selfGuildId = 0
    if CClientMainPlayer.Inst then
        selfGuildId = CClientMainPlayer.Inst.BasicProp.GuildId
    end

    local hasRankShow = false
    for i=1, #rankData do
        local data = rankData[i]
        local rank = data.rankPos
        if rank <= 3 and rank >=1 then
            local item = self.RankRoot.transform:Find("Rank/".. tostring(rank)).gameObject
            self:InitRankItem(item, data)
            hasRankShow = true
        end

        if data.guildId == selfGuildId then
            self:InitSelfRankItem(data)
        end
    end

    self.NoneBangHuiHint:SetActive(not hasRankShow)
    self.RankRoot.transform:Find("Rank").gameObject:SetActive(hasRankShow)
    self.RankRoot.transform:Find("Self").gameObject:SetActive(hasRankShow)
end

function LuaJuDianBattleSceneOverviewRankRoot:InitRankItem(item, data)
    local name = item.transform:Find("Name"):GetComponent(typeof(UILabel))
    local count = item.transform:Find("Count"):GetComponent(typeof(UILabel))

    name.text = data.guildName
    count.text = data.score
end

function LuaJuDianBattleSceneOverviewRankRoot:InitSelfRankItem(data)
    local item = self.RankRoot.transform:Find("Self").gameObject
    self:InitRankItem(item, data)

    -- 处理名次
    local rank = data.rankPos

    for i=1,3 do
        local rankTexture = item.transform:Find("RankTexture/r".. tostring(i)).gameObject
        rankTexture:SetActive(i == rank)
    end

    local rankLabel = item.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    if rank <= 3 then
        rankLabel.text = ""
    else
        rankLabel.text = tostring(rank)
    end
    
end

function LuaJuDianBattleSceneOverviewRankRoot:OnEnable()
    Gac2Gas.GuildJuDianQueryGlobalTopGuildRank()
    g_ScriptEvent:AddListener("GuildJuDianSyncGlobalTopRank", self, "OnRankData")
end

function LuaJuDianBattleSceneOverviewRankRoot:OnDisable()
    g_ScriptEvent:RemoveListener("GuildJuDianSyncGlobalTopRank", self, "OnRankData")
end


--@region UIEvent

--@endregion UIEvent

