local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local Animation = import "UnityEngine.Animation"
local CShopMallMgr = import "L10.UI.CShopMallMgr"

LuaChristmas2023MainWnd = class()
RegistClassMember(LuaChristmas2023MainWnd, "m_Animator")
RegistClassMember(LuaChristmas2023MainWnd, "m_GuaGuaItemShopId") -- 购买刮刮卡itemid

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChristmas2023MainWnd, "TimeLab", "TimeLab", UILabel)
RegistChildComponent(LuaChristmas2023MainWnd, "XueHuaXuYu", "XueHuaXuYu", GameObject)
RegistChildComponent(LuaChristmas2023MainWnd, "GiftForSelf", "GiftForSelf", GameObject)
RegistChildComponent(LuaChristmas2023MainWnd, "GuaGuaKa", "GuaGuaKa", GameObject)
RegistChildComponent(LuaChristmas2023MainWnd, "BingXuePlay", "BingXuePlay", GameObject)
RegistChildComponent(LuaChristmas2023MainWnd, "BingXuePlay1", "BingXuePlay1", GameObject)

--@endregion RegistChildComponent end

function LuaChristmas2023MainWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

    self.m_Animator = self.transform:GetComponent(typeof(Animation))
    self.m_GuaGuaItemShopId = Christmas2023_Setting.GetData().GuaguaItemShopId
    UIEventListener.Get(self.XueHuaXuYu).onClick = DelegateFactory.VoidDelegate(function(go)
        LuaChristmas2023Mgr:TrackToXueHua()
        CUIManager.CloseUI(CLuaUIResources.Christmas2023MainWnd)
    end)
    UIEventListener.Get(self.GiftForSelf).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CLuaUIResources.Christmas2023GiftForSelfEnterWnd)
    end)
    UIEventListener.Get(self.GuaGuaKa).onClick = DelegateFactory.VoidDelegate(function(go)
        CShopMallMgr.ShowLinyuShoppingMall(self.m_GuaGuaItemShopId)
    end)
    UIEventListener.Get(self.BingXuePlay).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CLuaUIResources.Christmas2023BingXuePlayEnterWnd)
    end)
    UIEventListener.Get(self.BingXuePlay1).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CLuaUIResources.Christmas2023BingXuePlayEnterWnd)
    end)
end
function LuaChristmas2023MainWnd:Init()
    if not LuaChristmas2023Mgr.m_IsMainWndOpened then
        self.m_Animator:Play("christmas2022mainwnd_zjmshow")
        LuaChristmas2023Mgr.m_IsMainWndOpened = true
    else
        self.m_Animator:Play("christmas2022mainwnd_zjmshow_1")
    end
    self.TimeLab.text = SafeStringFormat3(LocalString.GetString("活动时间   %s"), Task_JieRiGroup.GetData(43).ActivityTime)
end

--@region UIEvent

--@endregion UIEvent

