require("3rdParty/ScriptEvent")
require("common/common_include")

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CUIManager = import "L10.UI.CUIManager"
local CUIFx = import "L10.UI.CUIFx"
local UITexture = import "UITexture"
local MessageWndManager = import "L10.UI.MessageWndManager"
local GameObject = import "UnityEngine.GameObject"
local Vector3 = import "UnityEngine.Vector3"
local TweenAlpha = import "TweenAlpha"
local Input = import "UnityEngine.Input"
local Mathf = import "UnityEngine.Mathf"
local Quaternion = import "UnityEngine.Quaternion"
local BoxCollider = import "UnityEngine.BoxCollider"

LuaQiqiaobanWnd = class()

RegistChildComponent(LuaQiqiaobanWnd, "closeBtn", GameObject)
RegistChildComponent(LuaQiqiaobanWnd, "chooseArea", GameObject)
RegistChildComponent(LuaQiqiaobanWnd, "desArea", GameObject)
RegistChildComponent(LuaQiqiaobanWnd, "modifyNode", GameObject)
RegistChildComponent(LuaQiqiaobanWnd, "fxNode", CUIFx)
RegistChildComponent(LuaQiqiaobanWnd, "endShowNode", GameObject)
RegistChildComponent(LuaQiqiaobanWnd, "totalNode", GameObject)

RegistClassMember(LuaQiqiaobanWnd, "desNodeTable")
RegistClassMember(LuaQiqiaobanWnd, "chooseNodeTable")
RegistClassMember(LuaQiqiaobanWnd, "nowChooseNodeIndex")
RegistClassMember(LuaQiqiaobanWnd, "nowChooseNode")
RegistClassMember(LuaQiqiaobanWnd, "editMode")
RegistClassMember(LuaQiqiaobanWnd, "rotateBtn")
RegistClassMember(LuaQiqiaobanWnd, "rotateRadius")
RegistClassMember(LuaQiqiaobanWnd, "setedTable")
RegistClassMember(LuaQiqiaobanWnd, "m_Tick")
RegistClassMember(LuaQiqiaobanWnd, "m_EndTick")

function LuaQiqiaobanWnd:LeaveWnd()
	--CUIManager.CloseUI(CUIRes.QiqiaobanWnd)
	CUIManager.CloseUI(CLuaUIResources.QiqiaobanWnd)
end

function LuaQiqiaobanWnd:CheckFinish()
	local count = 0
	for i,v in pairs(self.setedTable) do
		if v then
			count = count + 1
		end
	end
	if count >= 7 then
		Gac2Gas.ReportCompleteQiQiaoBan()
		for i,v in pairs(self.chooseNodeTable) do
			v.transform:Find('light').gameObject:SetActive(true)
		end
		self.fxNode:LoadFx("Fx/UI/Prefab/UI_qiqiaoban.prefab")
		if self.m_Tick then
			UnRegisterTick(self.m_Tick)
		end
		TweenAlpha.Begin(self.totalNode,2,0)
		self.endShowNode:SetActive(false)
		self.modifyNode:SetActive(false)
		self.m_Tick = RegisterTickWithDuration(function ()
			self:LeaveWnd()
		end,7000,7000)
	end
end

function LuaQiqiaobanWnd:DoFinishShow(node,moveNode)
	local normalNode = node.transform:Find('normal').gameObject
	normalNode:SetActive(false)
	--normalNode:GetComponent(typeof(UITexture)).material = moveNode.transform:Find('normal'):GetComponent(typeof(UITexture)).material
	node.transform:Find('light').gameObject:SetActive(true)
	if self.modifyNode.activeSelf then
		self.endShowNode:SetActive(true)

		self.endShowNode.transform.localPosition = self.modifyNode.transform.localPosition
		self.endShowNode.transform.localRotation = self.modifyNode.transform.localRotation

		if self.m_EndTick then
			UnRegisterTick(self.m_EndTick)
		end
		self.m_EndTick = RegisterTickWithDuration(function ()
			self.endShowNode:SetActive(false)
		end,2000,2000)
	end
end

function LuaQiqiaobanWnd:ResetInfo()
	self.modifyNode:SetActive(false)
	self.nowChooseNode = nil
	self.nowChooseNodeIndex = nil
	self.editMode = 'idle'
end

function LuaQiqiaobanWnd:CheckPosValid(desNode)
	local result = false
	if self.nowChooseNode then
		local dis = Vector3.Distance(desNode.transform.localPosition,self.nowChooseNode.transform.localPosition)
		if dis < 50 then
			local rotateDis = desNode.transform.eulerAngles.z - self.nowChooseNode.transform.eulerAngles.z
			local divide = 360
			if self.nowChooseNodeIndex == 5 then
				divide = 90
			elseif self.nowChooseNodeIndex == 6 then
				divide = 180
			end

			local rest = rotateDis % divide
			if rest < 5 and rest > -5 then
				result = true
			end
			if result then
				self.nowChooseNode.transform.localPosition = desNode.transform.localPosition
				self.nowChooseNode.transform.eulerAngles = desNode.transform.eulerAngles
				local normalNode = self.nowChooseNode.transform:Find('normal')
				normalNode:GetComponent(typeof(BoxCollider)).enabled = false
				normalNode:GetComponent(typeof(UITexture)).depth = 5
				self:DoFinishShow(desNode,self.nowChooseNode)
				self:ResetInfo()
			end
		end
	end
	return result
end

function LuaQiqiaobanWnd:UpdateCheck()
	if not self.setedTable then
		self.setedTable = {}
	end

	local desTable = {}
	if self.nowChooseNodeIndex <= 2 then
		desTable = {1,2}
	elseif self.nowChooseNodeIndex <= 4 then
		desTable = {3,4}
	else
		table.insert(desTable,self.nowChooseNodeIndex)
	end

	for i,v in pairs(self.desNodeTable) do
		if not self.setedTable[i] then
			for j,b in pairs(desTable) do
				if b == i then
					local checkResult = self:CheckPosValid(v)
					if checkResult then
						self.setedTable[i] = true
						self:CheckFinish()
						break
					end
				end
			end
		end
	end
end

function LuaQiqiaobanWnd:Update()
	if self.editMode and self.editMode == 'move' then
		local touchWorldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition)
		local localPos = self.desArea.transform:InverseTransformPoint(touchWorldPos)
		localPos.z = 0
		self.modifyNode:SetActive(false)
		--self.modifyNode.transform.localPosition = localPos
		self.nowChooseNode.transform.localPosition = localPos
		self:UpdateCheck()
	elseif self.editMode and self.editMode == 'rotate' then
		local touchWorldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition)
		local localPos = self.desArea.transform:InverseTransformPoint(touchWorldPos)
		localPos.z = 0
		local dis = Vector3.Distance(localPos,self.modifyNode.transform.localPosition)
		if dis > 0 then
			local posX = (localPos.x - self.modifyNode.transform.localPosition.x)/dis*self.rotateRadius
			local posY = (localPos.y - self.modifyNode.transform.localPosition.y)/dis*self.rotateRadius
			localPos = Vector3(posX,posY,0)
			self.rotateBtn.transform.localPosition = localPos--Vector3(localPos.x + self.modifyNode.transform.localPosition.x,localPos.y + self.modifyNode.transform.localPosition.y,0)
			local rotateZ = Mathf.Asin(posX/self.rotateRadius) / 3.14159 * 180

			if self.nowChooseNode then
				if posY > 0 then
					self.nowChooseNode.transform.localRotation = Quaternion.Euler(0,0,360 - rotateZ)
				else
					self.nowChooseNode.transform.localRotation = Quaternion.Euler(0,0,180 + rotateZ)
				end
			end
		end
		self:UpdateCheck()
	else
		if self.nowChooseNode and self.editMode == 'idle' then
				self.modifyNode:SetActive(true)
				if self.endShowNode.activeSelf then
					self.endShowNode:SetActive(false)
				end
				self.modifyNode.transform.localPosition = self.nowChooseNode.transform.localPosition
		end
	end
end

function LuaQiqiaobanWnd:ClickChooseNode()
end

function LuaQiqiaobanWnd:PressChooseNode(index,btn,press)
	if press then
    self.nowChooseNodeIndex = index
		self.nowChooseNode = self.chooseArea.transform:Find(index).gameObject
		--if not self.modifyNode.activeSelf then
			--self.modifyNode:SetActive(true)
			--self.modifyNode.transform.localPosition = self.nowChooseNode.transform.localPosition
		--end
		if self.endShowNode.activeSelf then
			self.endShowNode:SetActive(false)
		end
		self.editMode = 'move'

		if self.nowChooseNode then
			local rAngle = self.nowChooseNode.transform.eulerAngles.z
			local posX = Mathf.Sin(rAngle/180*3.14159) * self.rotateRadius
			local posY = Mathf.Cos(rAngle/180*3.14159) * self.rotateRadius
			self.rotateBtn.transform.localPosition = Vector3(-posX,posY,0)
		end
	else
		self.editMode = 'idle'
	end
end

function LuaQiqiaobanWnd:PressRotateNode(press)
	if press then
		self.editMode = 'rotate'
	else
		self.editMode = 'idle'
	end
end

function LuaQiqiaobanWnd:InitChooseArea()
  local chooseNodeTable = {}
  for i=1,7 do
    local node = self.chooseArea.transform:Find(i)
    if node then
			node:Find('light').gameObject:SetActive(false)
      table.insert(chooseNodeTable,node.gameObject)
    end
  end
  self.chooseNodeTable = chooseNodeTable
  for i,v in pairs(self.chooseNodeTable) do
    local index = i
--  	local onChooseClick = function(go)
--      self.nowChooseNodeIndex = index
--			self.nowChooseNode = self.chooseArea.transform:Find(index).gameObject
--      self:ClickChooseNode()
--  	end
		local touchBtn = v.transform:Find('normal').gameObject
    --CommonDefs.AddOnClickListener(touchBtn,DelegateFactory.Action_GameObject(onChooseClick),false)
		local onPressClick = function(go,sign)
			self:PressChooseNode(index,go,sign)
		end
		CommonDefs.AddOnPressListener(touchBtn,DelegateFactory.Action_GameObject_bool(onPressClick),false)
  end
end

function LuaQiqiaobanWnd:InitDesArea()
  --local desTable = self.desArea.transform:Find("desTable")
  --Extensions.RemoveAllChildren(desTable)
  local desNodeTable = {}
  local chooseIndex = math.random(3)
  for i=1,3 do
    local tableNode = self.desArea.transform:Find('desNode'..i)
    if tableNode then
        if chooseIndex == i then
          tableNode.gameObject:SetActive(true)
        else
          tableNode.gameObject:SetActive(false)
        end
    end
  end

  for i=1,7 do
    local node = self.desArea.transform:Find('desNode'..chooseIndex..'/'..i)
    if node then
			node.transform:Find('bg').gameObject:SetActive(true)
			node.transform:Find('light').gameObject:SetActive(false)
			node.transform:Find('normal').gameObject:SetActive(false)
      table.insert(desNodeTable,node)
    end
  end
  self.desNodeTable = desNodeTable

end

function LuaQiqiaobanWnd:Init()
	local onCloseClick = function(go)
		MessageWndManager.ShowOKCancelMessage(LocalString.GetString("确定要退出吗？"), DelegateFactory.Action(function ()
				self:LeaveWnd()
		end), nil, nil, nil, false)
	end
  CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)
	self.editMode = 'idle'
	self.rotateBtn = self.modifyNode.transform:Find('RotateBtn').gameObject
	local onRotateClick = function(go,sign)
		self:PressRotateNode(sign)
	end
	CommonDefs.AddOnPressListener(self.rotateBtn,DelegateFactory.Action_GameObject_bool(onRotateClick),false)

	self.rotateRadius = Vector3.Distance(self.rotateBtn.transform.localPosition,Vector3.zero)
	self.modifyNode:SetActive(false)
  self:InitChooseArea()
  self:InitDesArea()
	self.endShowNode:SetActive(false)
end

function LuaQiqiaobanWnd:OnDestroy()
	if self.m_Tick then
			UnRegisterTick(self.m_Tick)
	end
	if self.m_EndTick then
		UnRegisterTick(self.m_EndTick)
	end
end

return LuaQiqiaobanWnd
