local Vector3 = import "UnityEngine.Vector3"
local UISprite = import "UISprite"
local UIEventListener = import "UIEventListener"
local Extensions = import "Extensions"
local CScene=import "L10.Game.CScene"
local DelegateFactory = import "DelegateFactory"
local CUIResources = import "L10.UI.CUIResources"
local CUIManager = import "L10.UI.CUIManager"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CFightingSpiritMgr = import "L10.Game.CFightingSpiritMgr"
local Constants = import "L10.Game.Constants"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local CFightingSpiritLiveWatchWndMgr = import "L10.UI.CFightingSpiritLiveWatchWndMgr"

CLuaFightingSpiritLiveWatch_Top = class()
RegistClassMember(CLuaFightingSpiritLiveWatch_Top,"m_ServerLabel1")
RegistClassMember(CLuaFightingSpiritLiveWatch_Top,"m_ServerLabel2")
RegistClassMember(CLuaFightingSpiritLiveWatch_Top,"m_DpsLabel1")
RegistClassMember(CLuaFightingSpiritLiveWatch_Top,"m_DpsLabel2")
RegistClassMember(CLuaFightingSpiritLiveWatch_Top,"m_Hp1Bar")
RegistClassMember(CLuaFightingSpiritLiveWatch_Top,"m_Hp2Bar")
RegistClassMember(CLuaFightingSpiritLiveWatch_Top,"expandButton")
RegistClassMember(CLuaFightingSpiritLiveWatch_Top,"m_FutiGrid1")
RegistClassMember(CLuaFightingSpiritLiveWatch_Top,"m_FutiGrid2")
RegistClassMember(CLuaFightingSpiritLiveWatch_Top,"m_FutiInfoTemplate")
RegistClassMember(CLuaFightingSpiritLiveWatch_Top,"m_KillNum1Label")
RegistClassMember(CLuaFightingSpiritLiveWatch_Top,"m_KillNum2Label")

RegistClassMember(CLuaFightingSpiritLiveWatch_Top,"m_WinPointsNum")
RegistClassMember(CLuaFightingSpiritLiveWatch_Top,"m_LeftWinPoints")
RegistClassMember(CLuaFightingSpiritLiveWatch_Top,"m_RightWinPoints")
RegistChildComponent(CLuaFightingSpiritLiveWatch_Top, "m_WinPoints","WinPoints", GameObject)
RegistChildComponent(CLuaFightingSpiritLiveWatch_Top, "m_LeftPointTemplate","LeftPointTemplate", GameObject)
RegistChildComponent(CLuaFightingSpiritLiveWatch_Top, "m_RightPointTemplate","RightPointTemplate", GameObject)
RegistChildComponent(CLuaFightingSpiritLiveWatch_Top, "m_LeftWinPointsTable","LeftWinPointsTable", UITable)
RegistChildComponent(CLuaFightingSpiritLiveWatch_Top, "m_RightWinPointsTable","RightWinPointsTable", UITable)
RegistChildComponent(CLuaFightingSpiritLiveWatch_Top, "m_RoundLabel","RoundLabel", UILabel)

function CLuaFightingSpiritLiveWatch_Top:Awake()
    self.m_ServerLabel1 = self.transform:Find("Left/ServerLabel"):GetComponent(typeof(UILabel))
    self.m_ServerLabel2 = self.transform:Find("Right/ServerName"):GetComponent(typeof(UILabel))
    self.m_DpsLabel1 = self.transform:Find("Left/ExpLabel1"):GetComponent(typeof(UILabel))
    self.m_DpsLabel2 = self.transform:Find("Right/ExpLabel2"):GetComponent(typeof(UILabel))
    self.m_Hp1Bar = self.transform:Find("Left/HP1Bar"):GetComponent(typeof(UISlider))
    self.m_Hp2Bar = self.transform:Find("Right/HP2Bar"):GetComponent(typeof(UISlider))
    self.expandButton = self.transform:Find("Tip/ExpandButton").gameObject
    self.m_FutiGrid1 = self.transform:Find("Left/Futi/FutiInfoGrid1"):GetComponent(typeof(UIGrid))
    self.m_FutiGrid2 = self.transform:Find("Right/Futi/FutiInfoGrid2"):GetComponent(typeof(UIGrid))
    self.m_FutiInfoTemplate = self.transform:Find("FutiTemplate").gameObject

    self.m_DpsLabel1.text = ""
    self.m_DpsLabel2.text = ""

    self.m_KillNum1Label = self.transform:Find("Left/KillNumLabel"):GetComponent(typeof(UILabel))
    self.m_KillNum1Label.gameObject:SetActive(false)
    self.m_KillNum2Label = self.transform:Find("Right/KillNumLabel"):GetComponent(typeof(UILabel))
    self.m_KillNum2Label.gameObject:SetActive(false)

    self.m_RoundLabel.text = ""
    self.m_RoundLabel.gameObject:SetActive(false)

    self:InitWinPoints()
end

function CLuaFightingSpiritLiveWatch_Top:InitWinPoints()
    self.m_WinPoints:SetActive(false)
    self.m_LeftPointTemplate:SetActive(false)
    self.m_RightPointTemplate:SetActive(false)

    if CLuaStarBiwuMgr:IsInStarBiwuWatch() then
        self.m_WinPointsNum = StarBiWuShow_Setting.GetData().Win_Need_Score
        local battle = CLuaStarBiwuMgr.m_CurrentBattleStatus
        if battle.m_JieShu < 11 then
            self.m_WinPointsNum = 3
        end
    elseif self:IsDouHunTan() then
        self.m_WinPointsNum = DouHunTan_Setting.GetData().WinPointsMax
    elseif CQuanMinPKMgr.Inst.IsInQuanMinPKWatch then
        self.m_WinPointsNum = QuanMinPK_Setting.GetData().Win_Need_Score
    end

    if self.m_WinPointsNum then
        Extensions.RemoveAllChildren(self.m_LeftWinPointsTable.transform)
        Extensions.RemoveAllChildren(self.m_RightWinPointsTable.transform)
        self.m_LeftWinPoints = {}
        self.m_RightWinPoints = {}
        for i = 1, self.m_WinPointsNum do
            local leftgo = NGUITools.AddChild(self.m_LeftWinPointsTable.gameObject, self.m_LeftPointTemplate)
            local rightgo = NGUITools.AddChild(self.m_RightWinPointsTable.gameObject, self.m_RightPointTemplate)
            leftgo:SetActive(true)
            rightgo:SetActive(true)
            local leftPoint = leftgo.transform:Find("Point").gameObject
            local rightPoint = rightgo.transform:Find("Point").gameObject
            leftPoint:SetActive(false)
            rightPoint:SetActive(false)
            table.insert(self.m_LeftWinPoints, leftPoint)
            table.insert(self.m_RightWinPoints, rightPoint)
        end
        self:OnUpdateWinPoints()
    end
end

function CLuaFightingSpiritLiveWatch_Top:UpdateWinPoints(leftNumOfWinPoints, rightNumOfWinPoints)
    if not self.m_LeftWinPoints or not self.m_RightWinPoints then return end
    self.m_WinPoints:SetActive(true)
    for i = 1,#self.m_LeftWinPoints do
        self.m_LeftWinPoints[i]:SetActive(i <= leftNumOfWinPoints)
    end
    for i = 1,#self.m_RightWinPoints do
        self.m_RightWinPoints[i]:SetActive(i <= rightNumOfWinPoints)
    end
end
function CLuaFightingSpiritLiveWatch_Top:OnUpdateWinPoints()
    if CLuaFightingSpiritMgr.m_LeftNumOfWinPoints and CLuaFightingSpiritMgr.m_RightNumOfWinPoints then
        self:UpdateWinPoints(CLuaFightingSpiritMgr.m_LeftNumOfWinPoints, CLuaFightingSpiritMgr.m_RightNumOfWinPoints)

        if CLuaStarBiwuMgr:IsInStarBiwuWatch() then
            self:OnUpdateStarBiwuBattleStatus()
        elseif CQuanMinPKMgr.Inst.IsInQuanMinPKWatch then
            self:OnUpdateQuanMinPKBattleStatus()
        elseif self:IsDouHunTan() then
            self:OnUpdateDouHunStatus()
        end
    end
end

function CLuaFightingSpiritLiveWatch_Top:OnUpdateStarBiwuBattleStatus()
    local text = ""
    local battle = CLuaStarBiwuMgr.m_CurrentBattleStatus
    self.m_RoundLabel.gameObject:SetActive(true)
    local groupList = StarBiWuShow_Setting.GetData().NewGroupName
    if battle then
        if battle.m_JieShu >= 13 then
            if battle.m_Stage == EnumStarBiwuFightStage.eReShenSai or battle.m_Stage == EnumStarBiwuFightStage.eQD_ReShenSai then
                text = LocalString.GetString("热身赛")
            elseif battle.m_Stage == EnumStarBiwuFightStage.eZongJueSai then
                if battle.m_MatchIdx <= 4 then
                    text = LocalString.GetString("胜者组第一轮")
                elseif battle.m_MatchIdx <= 6 then
                    text = LocalString.GetString("败者组第一轮")
                elseif battle.m_MatchIdx <= 8 then
                    text = LocalString.GetString("胜者组第二轮")
                elseif battle.m_MatchIdx <= 10 then
                    text = LocalString.GetString("败者组第二轮")
                elseif battle.m_MatchIdx == 11 then
                    text = LocalString.GetString("胜者组决赛")
                elseif battle.m_MatchIdx == 12 then
                    text = LocalString.GetString("败者组第三轮")
                elseif battle.m_MatchIdx == 13 then
                    text = LocalString.GetString("败者组决赛")
                elseif battle.m_MatchIdx == 14 then
                    text = LocalString.GetString("总决赛")
                end
            elseif battle.m_Stage == EnumStarBiwuFightStage.eJiFenSai then
                if battle.m_Group <= 11 then
                    text = SafeStringFormat3(LocalString.GetString("%s积分赛第%d轮"),groupList[battle.m_Group - 1],battle.m_MatchIdx)
                else
                    text = SafeStringFormat3(LocalString.GetString("普通%d组积分赛第%d轮"),tostring(battle.m_Group - 1),battle.m_MatchIdx)
                end
            else
                self.m_RoundLabel.gameObject:SetActive(false)
            end
        else
            if battle.m_Stage == EnumStarBiwuFightStage.eXiaoZuSai then
                local groupTextArr = {LocalString.GetString("甲"),LocalString.GetString("乙"),LocalString.GetString("丙"),LocalString.GetString("丁")}
                text = SafeStringFormat3(LocalString.GetString("小组赛%s组第%d轮"),groupTextArr[battle.m_Group],battle.m_MatchIdx)
            elseif battle.m_Stage == EnumStarBiwuFightStage.eZongJueSai then
                if battle.m_MatchIdx <= 4 then
                    text = LocalString.GetString("胜者组第一轮")
                elseif battle.m_MatchIdx <= 6 then
                    text = LocalString.GetString("败者组第一轮")
                elseif battle.m_MatchIdx <= 8 then
                    text = LocalString.GetString("胜者组第二轮")
                elseif battle.m_MatchIdx <= 10 then
                    text = LocalString.GetString("败者组第二轮")
                elseif battle.m_MatchIdx <= 13 then
                    text = LocalString.GetString("四强总决赛")
                elseif battle.m_MatchIdx == 14 then
                    text = LocalString.GetString("总决赛")
                end
            elseif battle.m_Stage == EnumStarBiwuFightStage.eJiFenSai then
                text = SafeStringFormat3(LocalString.GetString("积分赛第%d轮"),battle.m_MatchIdx)
            elseif battle.m_Stage == EnumStarBiwuFightStage.eReShenSai or battle.m_Stage == EnumStarBiwuFightStage.eQD_ReShenSai then
                text = LocalString.GetString("热身赛")
            elseif battle.m_Stage == EnumStarBiwuFightStage.eQD_ZhengShiSai then
                if battle.m_MatchIdx <= 8 then
                    text = LocalString.GetString("1/8决赛")
                elseif battle.m_MatchIdx <= 12 then
                    text = LocalString.GetString("1/4决赛")
                elseif battle.m_MatchIdx <= 14 then
                    text = LocalString.GetString("半决赛")
                elseif battle.m_MatchIdx == 15 then
                    text = LocalString.GetString("总决赛")
                end
            else
                self.m_RoundLabel.gameObject:SetActive(false)
            end
        end
    else
        self.m_RoundLabel.gameObject:SetActive(false)
    end
    self.m_RoundLabel.text = text
end

function CLuaFightingSpiritLiveWatch_Top:OnUpdateDouHunStatus()
    local text = CLuaFightingSpiritMgr:GetMatchIndexDesc(CFightingSpiritLiveWatchWndMgr.MatchIndex)
    self.m_RoundLabel.gameObject:SetActive(false)
    if text then
        self.m_RoundLabel.gameObject:SetActive(true)
        self.m_RoundLabel.text = text
    end
end

function CLuaFightingSpiritLiveWatch_Top:OnUpdateQuanMinPKBattleStatus()
    local text = ""
    local battle = CLuaQMPKMgr.m_CurrentBattleStatus
    if battle then
        if battle.m_PlayStage == EnumQmpkPlayStage.eReShenSai then
            text = LocalString.GetString("热身赛")
        elseif battle.m_PlayStage == EnumQmpkPlayStage.eJiFenSai then
            text = SafeStringFormat3(LocalString.GetString("积分赛 第%d轮"), battle.m_PlayIndex)
        elseif battle.m_PlayStage == EnumQmpkPlayStage.eTaoTaiSai then
            local stageNameList = {
                LocalString.GetString("32强赛"),
                LocalString.GetString("16强赛"),
                LocalString.GetString("8强赛"),
                LocalString.GetString("4强赛"),
            }
            text = SafeStringFormat3(LocalString.GetString("淘汰赛 %s"), stageNameList[battle.m_PlayIndex])
        elseif battle.m_PlayStage == EnumQmpkPlayStage.eZongJueSai then
            if battle.m_PlayIndex == 1 or battle.m_PlayIndex == 2 then 
                text = LocalString.GetString("全国总决赛 半决赛")
            elseif battle.m_PlayIndex == 3 then
                text = LocalString.GetString("全国总决赛 季军赛")
            elseif battle.m_PlayIndex == 4 or battle.m_PlayIndex == 5 or battle.m_PlayIndex == 6 then
                text = LocalString.GetString("全国总决赛 冠军赛")
            end
        end
    end
    self.m_RoundLabel.gameObject:SetActive(text ~= "")
    self.m_RoundLabel.text = text
end

function  CLuaFightingSpiritLiveWatch_Top:IsDouHunTan()
    if not CScene.MainScene then return false end
    local gamePlayId = CScene.MainScene.GamePlayDesignId
    return gamePlayId == DouHunTan_Setting.GetData().GamePlayId or CScene.MainScene.SceneTemplateId == Constants.DouHunWatchSceneTemplateId
end

-- Auto Generated!!
function CLuaFightingSpiritLiveWatch_Top:Init( serverName1, serverName2, shenliLv1, futiInfos1, shenliLv2, futiInfos2) 
    CUICommonDef.SetClampLabelText(self.m_ServerLabel1, LocalString.TranslateAndFormatText(serverName1))
    CUICommonDef.SetClampLabelText(self.m_ServerLabel2, LocalString.TranslateAndFormatText(serverName2))
    if self.expandButton ~= nil then
        self.expandButton:SetActive(false)
    end

    self.m_FutiInfoTemplate:SetActive(false)
    Extensions.RemoveAllChildren(self.m_FutiGrid1.transform)
    Extensions.RemoveAllChildren(self.m_FutiGrid2.transform)

    if futiInfos1 ~= nil then
        do
            local i = 0
            while i < futiInfos1.Count do
                local instance = CUICommonDef.InstantiateUniformItem(self.m_FutiInfoTemplate, self.m_FutiGrid1.transform)
                self:InitFutiItem(instance, futiInfos1[i])
                UIEventListener.Get(instance).onClick = DelegateFactory.VoidDelegate(function (go) 
                    CFightingSpiritMgr.Instance:ShowFutiInfo(serverName1, shenliLv1, futiInfos1)
                end)
                i = i + 1
            end
        end
        self.m_FutiGrid1:Reposition()
    end

    if futiInfos2 ~= nil then
        for i = futiInfos2.Count - 1, 0, - 1 do
            local instance = CUICommonDef.InstantiateUniformItem(self.m_FutiInfoTemplate, self.m_FutiGrid2.transform)
            self:InitFutiItem(instance, futiInfos2[i])
            UIEventListener.Get(instance).onClick = DelegateFactory.VoidDelegate(function (go) 
                CFightingSpiritMgr.Instance:ShowFutiInfo(serverName2, shenliLv2, futiInfos2)
            end)
        end
        self.m_FutiGrid2:Reposition()
    end
end
function CLuaFightingSpiritLiveWatch_Top:InitFutiItem( item, futiType) 
    item:SetActive(true)
    local sprite = CommonDefs.GetComponent_GameObject_Type(item, typeof(UISprite))
    if sprite ~= nil then
        sprite.spriteName = self:GetFutiSpriteName(futiType)
    end
end
function CLuaFightingSpiritLiveWatch_Top:GetFutiSpriteName( futiType) 
    repeat
        local default = futiType
        if default == 1 then
            return "dhgz_jing"
        elseif default == 2 then
            return "dhgz_qi"
        elseif default == 3 then
            return "dhgz_shen"
        else
            return "dhgz_jing"
        end
    until 1
end
function CLuaFightingSpiritLiveWatch_Top:UpdateKillNum(killNum1,killNum2)
    self.m_KillNum1Label.gameObject:SetActive(true)
    self.m_KillNum2Label.gameObject:SetActive(true)

    self.m_KillNum1Label.text=tostring(killNum1)
    self.m_KillNum2Label.text=tostring(killNum2)
end
function CLuaFightingSpiritLiveWatch_Top:UpdateHpInfo( hp1, hp2) 
    local  DPS_LIMIT = 1000000000
    self.m_Hp1Bar.value = (1 * hp1 / DPS_LIMIT)
    self.m_DpsLabel1.text = (tostring(hp1))

    self.m_Hp2Bar.value = (1 * hp2 / DPS_LIMIT)
    self.m_DpsLabel2.text = (tostring(hp2))
end
function CLuaFightingSpiritLiveWatch_Top:OnEnable( )
    UIEventListener.Get(self.expandButton).onClick = DelegateFactory.VoidDelegate(function (p) 
        self.expandButton.transform.localEulerAngles = Vector3(0, 0, 0)
        CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
    end)
    -- EventManager.AddListener(EnumEventType.HideTopAndRightTipWnd, MakeDelegateFromCSFunction(self.OnHideTopAndRightTipWnd, Action0, self))
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("UpdateWinPoints", self, "OnUpdateWinPoints")
end
function CLuaFightingSpiritLiveWatch_Top:OnDisable( )
    UIEventListener.Get(self.expandButton).onClick = nil
    -- EventManager.RemoveListener(EnumEventType.HideTopAndRightTipWnd, MakeDelegateFromCSFunction(self.OnHideTopAndRightTipWnd, Action0, self))
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("UpdateWinPoints", self, "OnUpdateWinPoints")
    --这个界面不显示的话，那么tip界面肯定也不显示
    if CUIManager.IsLoaded(CUIResources.TopAndRightTipWnd) then
        CUIManager.CloseUI(CUIResources.TopAndRightTipWnd)
    end
    CLuaFightingSpiritMgr.m_LeftNumOfWinPoints= nil
    CLuaFightingSpiritMgr.m_RightNumOfWinPoints= nil
    CFightingSpiritLiveWatchWndMgr.MatchIndex = -1
end
function CLuaFightingSpiritLiveWatch_Top:OnHideTopAndRightTipWnd( )
    self.expandButton.transform.localEulerAngles = Vector3(0, 0, 180)
end

return CLuaFightingSpiritLiveWatch_Top
