-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CHongBaoDetailNewItem = import "L10.UI.CHongBaoDetailNewItem"
local CHongBaoMgr = import "L10.UI.CHongBaoMgr"
local CHongbaoSnatchRecord = import "L10.UI.CHongbaoSnatchRecord"
local CItem = import "L10.Game.CItem"
local EnumHongbaoType = import "L10.UI.EnumHongbaoType"
local LocalString = import "LocalString"
local NGUIText = import "NGUIText"
local EnumHongBaoEvent = import "L10.UI.EnumHongBaoEvent"
CHongBaoDetailNewItem.m_UpdateData_CS2LuaHook = function (this, data, isSystemHongbao)
    this.m_BestLuckSprite.gameObject:SetActive(false)
    this.m_PlayerNameLabel.text = data.SnatchPlayerName
    local isHuakui = CHongBaoMgr.s_HongBaoEvent == EnumHongBaoEvent.HuaKui or CHongBaoMgr.s_HongBaoEvent == EnumHongBaoEvent.BangHua
    local bestColor = CLuaHongBaoMgr.m_HongBaoColorList[4] or "FF5634"
    --m_PlayerNameLabel.color = Color.white;
    if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Id == data.PlayerId then
        this.m_PlayerNameLabel.text = LocalString.GetString("我的手气")
        this.m_PlayerNameLabel.color = isHuakui and NGUIText.ParseColor24("4aae01", 0) or NGUIText.ParseColor24("9CFF54", 0)
    end
    if CHongBaoMgr.m_HongbaoType == EnumHongbaoType.Gift then
        this.m_MoneyLabel.text = (CItem.GetName(data.ItemId) .. LocalString.GetString("*")) .. tostring(data.ItemNum)
        return
    end
    this.m_MoneyLabel.text = tostring(data.Silver)
    if data.Status == CHongbaoSnatchRecord.CSnatchStatus.Least then
        this.m_MarkLabel.color = isHuakui and NGUIText.ParseColor24("686868", 0) or NGUIText.ParseColor24("C6C6C6", 0)
        this.m_MarkLabel.gameObject:SetActive(true)
        this.m_MarkLabel.text = LocalString.GetString("手气最差")
    elseif data.Status == CHongbaoSnatchRecord.CSnatchStatus.Most then
        this.m_MarkLabel.color = isHuakui and NGUIText.ParseColor24("ff2913", 0) or NGUIText.ParseColor24(bestColor, 0)
        this.m_MarkLabel.gameObject:SetActive(true)
        this.m_MarkLabel.text = LocalString.GetString("手气最佳")
        this.m_BestLuckSprite.gameObject:SetActive(true)
    elseif data.Status == CHongbaoSnatchRecord.CSnatchStatus.CurrentLeast then
        if not isSystemHongbao then
            this.m_MarkLabel.color = isHuakui and NGUIText.ParseColor24("686868", 0) or NGUIText.ParseColor24("C6C6C6", 0)
            this.m_MarkLabel.gameObject:SetActive(true)
            this.m_MarkLabel.text = LocalString.GetString("当前最差")
        else
            this.m_MarkLabel.text = ""
        end
    elseif data.Status == CHongbaoSnatchRecord.CSnatchStatus.CurrentMost then
        this.m_MarkLabel.color = isHuakui and NGUIText.ParseColor24("ff2913", 0) or NGUIText.ParseColor24(bestColor, 0)
        this.m_MarkLabel.gameObject:SetActive(true)
        this.m_MarkLabel.text = LocalString.GetString("当前最佳")
    else
        this.m_MarkLabel.gameObject:SetActive(false)
    end
end
