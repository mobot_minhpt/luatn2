local CButton = import "L10.UI.CButton"
local CUITexture = import "L10.UI.CUITexture"
local QnTableView = import "L10.UI.QnTableView"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local Profession = import "L10.Game.Profession"
local Color = import "UnityEngine.Color"

LuaLiuYi2023DaoDanXiaoGuiStateWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiStateWnd, "ExpandButton", "ExpandButton", CButton)
RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiStateWnd, "FindRoot", "FindRoot", GameObject)
RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiStateWnd, "Portrait", "Portrait", CUITexture)
RegistChildComponent(LuaLiuYi2023DaoDanXiaoGuiStateWnd, "StatesTable", "StatesTable", QnTableView)

--@endregion RegistChildComponent end
RegistClassMember(LuaLiuYi2023DaoDanXiaoGuiStateWnd, "m_Infos")
RegistClassMember(LuaLiuYi2023DaoDanXiaoGuiStateWnd, "m_Tick")

function LuaLiuYi2023DaoDanXiaoGuiStateWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

    UIEventListener.Get(self.ExpandButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnExpandButtonClick()
	end)

    self.StatesTable.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_Infos
        end,
        function(item,index)
            self:InitItem(item,index,self.m_Infos[index+1])
        end
    )
end

function LuaLiuYi2023DaoDanXiaoGuiStateWnd:Init()
    self.FindRoot:SetActive(false)
    self.m_Infos = LuaLiuYi2023Mgr.m_DDXGPlayerInfos
    self.StatesTable:ReloadData(true,false)
    --self:ShowFind(2, 1)
end

--@region UIEvent

--@endregion UIEvent

function LuaLiuYi2023DaoDanXiaoGuiStateWnd:OnExpandButtonClick()
	LuaUtils.SetLocalRotation(self.ExpandButton.transform,0,0,0)
	CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

function LuaLiuYi2023DaoDanXiaoGuiStateWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncDaoDanXiaoGuiPlayInfo", self, "OnSyncDaoDanXiaoGuiPlayInfo")
    g_ScriptEvent:AddListener("LiuYi2023FindDaoDanXiaoGui", self, "OnLiuYi2023FindDaoDanXiaoGui")
end

function LuaLiuYi2023DaoDanXiaoGuiStateWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncDaoDanXiaoGuiPlayInfo", self, "OnSyncDaoDanXiaoGuiPlayInfo")
    g_ScriptEvent:RemoveListener("LiuYi2023FindDaoDanXiaoGui", self, "OnLiuYi2023FindDaoDanXiaoGui")
end

function LuaLiuYi2023DaoDanXiaoGuiStateWnd:OnSyncDaoDanXiaoGuiPlayInfo(playerInfos)
    self.m_Infos = playerInfos
    self.StatesTable:ReloadData(true,false)
end

function LuaLiuYi2023DaoDanXiaoGuiStateWnd:OnLiuYi2023FindDaoDanXiaoGui(playerId, class, gender)
    self:ShowFind(class, gender)
end

function LuaLiuYi2023DaoDanXiaoGuiStateWnd:InitItem(item, index, info)
    local bg            = item:GetComponent(typeof(UITexture))
    local career        = item.transform:Find("Icon"):GetComponent(typeof(UISprite))
    local nameLab       = item.transform:Find("NameLab"):GetComponent(typeof(UILabel))
    local damageLab     = item.transform:Find("DamageLab"):GetComponent(typeof(UILabel))
    local findNumLab    = item.transform:Find("FindNumLab"):GetComponent(typeof(UILabel))

    local class = info.class
    local playerName = info.playerName
    local damage = info.damage
    local findNum = info.findNum

    local cutDownName = nil
    if CommonDefs.StringLength(playerName) > 4 then
        cutDownName = CommonDefs.StringSubstring2(playerName, 0, 4)
    else
        cutDownName = playerName
    end

    bg.enabled = index == 0
    career.spriteName = Profession.GetIconByNumber(class)
    nameLab.text = string.len(cutDownName) == string.len(playerName) and cutDownName or (cutDownName .. "...")
    damageLab.text = damage >= 10000 and SafeStringFormat3(LocalString.GetString("%.f万"), damage / 10000) or SafeStringFormat3("%.f", damage)
    findNumLab.text = findNum

    nameLab.color = index == 0 and Color.green or Color.white
    damageLab.color = index == 0 and Color.green or Color.white
    findNumLab.color = index == 0 and Color.green or Color.white
end

function LuaLiuYi2023DaoDanXiaoGuiStateWnd:ShowFind(class, gender)
    UnRegisterTick(self.m_Tick)
    self.m_Tick = RegisterTickOnce(function() 
        self.FindRoot:SetActive(false)
    end, 2000)

    self.FindRoot:SetActive(true)
    local portraitName = CUICommonDef.GetPortraitName(class, gender, -1)
	self.Portrait:LoadNPCPortrait(portraitName,false)
end

function LuaLiuYi2023DaoDanXiaoGuiStateWnd:OnDestroy()
    UnRegisterTick(self.m_Tick)
    LuaLiuYi2023Mgr.m_DDXGPlayerInfos = {}
end