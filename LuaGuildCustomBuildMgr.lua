local CScene=import "L10.Game.CScene"
local Utility = import "L10.Engine.Utility"
local CTrackMgr = import "L10.Game.CTrackMgr"
local CPos = import "L10.Engine.CPos"

LuaGuildCustomBuildMgr = {}

LuaGuildCustomBuildMgr.s_SwitchOn = false

LuaGuildCustomBuildMgr.mPreTaskStatus = EnumGuildCustomBuildPreTaskStatus.eInit
function LuaGuildCustomBuildMgr.IsNeedShowPreBuildTaskView()
    if not LuaGuildCustomBuildMgr.s_SwitchOn then
        return false
    end
    if CClientMainPlayer.Inst then
        return CClientMainPlayer.Inst:IsInMyGuildCity()
    end
    return false
end

function LuaGuildCustomBuildMgr.OpenPreTaskProgressWnd()
    CUIManager.ShowUI(CLuaUIResources.GuildCustomBuildPreTaskWnd)
end

--前置任务 
function LuaGuildCustomBuildMgr.IsInTanLuPoZhenTask()
    if not LuaGuildCustomBuildMgr.s_SwitchOn then
        return false
    end
    
    --to do gameplayid and stage
    if not CScene.MainScene then return false end
    local gamePlayId = CScene.MainScene.GamePlayDesignId
    local IsInGameplay = gamePlayId == GuildCustomBuildPreTask_GameSetting.GetData().GamePlayId
    return IsInGameplay and LuaGuildCustomBuildMgr.mPreTaskStage==1 
end

function LuaGuildCustomBuildMgr:PreTask1TopRightWnd(wnd)
    --GuildPreTaskBreakSchemeTopRightWnd
    LuaTopAndRightMenuWndMgr:ShowTopRightWnd(wnd, CLuaUIResources.GuildPreTaskBreakSchemeTopRightWnd,function ()
        local isShowWnd = LuaGuildCustomBuildMgr.IsInTanLuPoZhenTask()
        return isShowWnd
    end)
end

function LuaGuildCustomBuildMgr.OnSyncPreTaskStatus(status)
    print("OnSyncPreTaskStatus",status)
    LuaGuildCustomBuildMgr.mPreTaskStatus = status
	g_ScriptEvent:BroadcastInLua("GuildCustomBuildPreTaskSyncStatus",status)
    --task
    local setting = GuildCustomBuildPreTask_GameSetting.GetData()
    print(status,EnumGuildCustomBuildPreTaskStatus.eTask1,status == EnumGuildCustomBuildPreTaskStatus.eTask1)
    if status == EnumGuildCustomBuildPreTaskStatus.eTask1 then
        local desc = setting.PreTaskDesc_Stage_1
        LuaCommonCountDownViewMgr:SetInfo(desc, true, false, nil, nil, nil, nil,nil)
    end
end

LuaGuildCustomBuildMgr.PreTaskPlayInfo = {}
function LuaGuildCustomBuildMgr.OnGuildCustomBuildPreTaskSyncPlayInfo(playInfoUd)
    LuaGuildCustomBuildMgr.PreTaskPlayInfo = {}
    local dict = MsgPackImpl.unpack(playInfoUd)
    if not dict then
        return
    end

    local tbl = LuaGuildCustomBuildMgr.PreTaskPlayInfo
    CommonDefs.DictIterate(dict, DelegateFactory.Action_object_object(function (key, val)
		tbl[key] = val
        if key == "Stage" then
            LuaGuildCustomBuildMgr.mPreTaskStage = tonumber(val)
        end
	end))
    LuaGuildCustomBuildMgr.PreTaskPlayInfo = tbl

    local setting = GuildCustomBuildPreTask_GameSetting.GetData()
    if LuaGuildCustomBuildMgr.mPreTaskStage == 1 then
        local desc = setting.PreTaskDesc_Stage_1
        LuaCommonCountDownViewMgr:SetInfo(desc, true, false, nil, nil, nil, nil,nil)
    elseif LuaGuildCustomBuildMgr.mPreTaskStage == 2 then
        local desc = setting.PreTaskDesc_Stage_2
        LuaCommonCountDownViewMgr:SetInfo(desc, true, false, nil, nil, nil, nil,function()
            local pixelPos = Utility.GridPos2PixelPos(CPos(setting.Task2TrackPos[0], setting.Task2TrackPos[1]))
	        CTrackMgr.Inst:Track(nil, CScene.MainScene.SceneTemplateId, pixelPos, Utility.Grid2Pixel(2.0), nil, nil, nil, nil, true)
        end)
        CUIManager.CloseUI(CLuaUIResources.GuildPreTaskBreakSchemeTopRightWnd)
    elseif LuaGuildCustomBuildMgr.mPreTaskStage == 3 then
        local desc = setting.PreTaskDesc_Stage_3
        local taskInfo = tbl.Task3Info

        if taskInfo then
            local str1 = SafeStringFormat3("(%d/%d)",taskInfo[0],taskInfo[1])
            local str2 = SafeStringFormat3("(%d/%d)",taskInfo[2],taskInfo[3])
            desc = SafeStringFormat3(desc,str1,str2)
            LuaCommonCountDownViewMgr:SetInfo(desc, true, false, nil, nil, nil, nil,nil)
        end

        
        CUIManager.CloseUI(CLuaUIResources.GuildPreTaskBreakSchemeTopRightWnd)
    end
    g_ScriptEvent:BroadcastInLua("GuildCustomBuildPreTaskSyncPlayInfo")
end