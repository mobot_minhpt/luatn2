local EPlayerFightProp=import "L10.Game.EPlayerFightProp"
local CClientMonster=import "L10.Game.CClientMonster"
local CClientObjectMgr=import "L10.Game.CClientObjectMgr"
CLuaHouseWoodPileSwitchLevelWnd = class()
RegistClassMember(CLuaHouseWoodPileSwitchLevelWnd, "m_PropNames1")
RegistClassMember(CLuaHouseWoodPileSwitchLevelWnd, "m_PropNames2")
RegistClassMember(CLuaHouseWoodPileSwitchLevelWnd, "m_PropLabelLookup")


CLuaHouseWoodPileSwitchLevelWnd.m_EngineId = 0
CLuaHouseWoodPileSwitchLevelWnd.m_PileId = 0
CLuaHouseWoodPileSwitchLevelWnd.m_PileTemplateId = 0

function CLuaHouseWoodPileSwitchLevelWnd:Awake()
    self.m_PropLabelLookup = {}

    local propItem = self.transform:Find("PropItem").gameObject
    propItem:SetActive(false)

    local displayNames1 = {
        LocalString.GetString("最大气血上限"),
        LocalString.GetString("物理防御"),
        LocalString.GetString("物理躲避"),
        LocalString.GetString("法术防御"),
        LocalString.GetString("法术躲避"),
        LocalString.GetString("格挡"),
        LocalString.GetString("格挡伤害减免"),
    }
    local displayNames2 = {
        LocalString.GetString("火抗性"),
        LocalString.GetString("风抗性"),
        LocalString.GetString("毒抗性"),
        LocalString.GetString("幻抗性"),
        LocalString.GetString("电抗性"),
        LocalString.GetString("冰抗性"),
        LocalString.GetString("光抗性"),
        LocalString.GetString("水抗性"),
    }
    self.m_PropNames1 = {
        -- "FightProp_Tips_HpFull",
        EPlayerFightProp.HpFull,
        -- "FightProp_Tips_pDef",
        EPlayerFightProp.pDef,
        -- "FightProp_Tips_mDef",
        EPlayerFightProp.pMiss,
        -- "FightProp_Tips_pMiss",
        EPlayerFightProp.mDef,
        -- "FightProp_Tips_mMiss",
        EPlayerFightProp.mMiss,
        -- "FightProp_Tips_Block",
        EPlayerFightProp.Block,
        -- "FightProp_Tips_BlockDamage",
        EPlayerFightProp.BlockDamage,
    }
    self.m_PropNames2 = {
        -- "FightProp_Tips_AntiFire",
        EPlayerFightProp.AntiFire,
        -- "FightProp_Tips_AntiThunder",
        EPlayerFightProp.AntiThunder,
        -- "FightProp_Tips_AntiWind",
        EPlayerFightProp.AntiWind,
        -- "FightProp_Tips_AntiIce",
        EPlayerFightProp.AntiIce,
        -- "FightProp_Tips_AntiPoison",
        EPlayerFightProp.AntiPoison,
        -- "FightProp_Tips_AntiLight",
        EPlayerFightProp.AntiLight,
        -- "FightProp_Tips_AntiIllusion",
        EPlayerFightProp.AntiIllusion,
        -- "FightProp_Tips_AntiWater",
        EPlayerFightProp.AntiWater,
    }
    local grid1 = self.transform:Find("Grid1").gameObject
    local grid2 = self.transform:Find("Grid2").gameObject

    for i,v in ipairs(self.m_PropNames1) do
        local go = NGUITools.AddChild(grid1,propItem)
        go:SetActive(true)
        local label1 = go.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
        local label2 = go.transform:Find("ValueLabel"):GetComponent(typeof(UILabel))
        label1.text = displayNames1[i]
        label2.text = "0"
        self.m_PropLabelLookup[v] = label2
    end
    for i,v in ipairs(self.m_PropNames2) do
        local go = NGUITools.AddChild(grid2,propItem)
        go:SetActive(true)
        local label1 = go.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
        local label2 = go.transform:Find("ValueLabel"):GetComponent(typeof(UILabel))
        label1.text = displayNames2[i]
        label2.text = "0"
        self.m_PropLabelLookup[v] = label2
    end

    Gac2Gas.RequestPileFightProp(CLuaHouseWoodPileSwitchLevelWnd.m_EngineId)
end

function CLuaHouseWoodPileSwitchLevelWnd:Init()

    local radioBox = self.transform:Find("Grid"):GetComponent(typeof(QnRadioBox))
    radioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,idx)
        -- local index = radioBox.CurrentSelectIndex+1
        local index=idx+1
        Gac2Gas.RequestSwitchWoodPileMonsterIdx(CLuaHouseWoodPileSwitchLevelWnd.m_EngineId,index)
        -- Gac2Gas.RequestPileFightProp(CLuaHouseWoodPileSwitchLevelWnd.m_EngineId)
        --请求fightprop
        -- CUIManager.CloseUI(CLuaUIResources.HouseWoodPileSwitchLevelWnd)
    end) 
    -- local button=  self.transform:Find("Button").gameObject
    -- UIEventListener.Get(button).onClick = DelegateFactory.VoidDelegate(function(go)
    --     local index = radioBox.CurrentSelectIndex+1
    --     Gac2Gas.RequestSwitchWoodPileMonsterIdx(CLuaHouseWoodPileSwitchLevelWnd.m_EngineId,index)
    --     CUIManager.CloseUI(CLuaUIResources.HouseWoodPileSwitchLevelWnd)
    -- end)

    local engineId = CLuaHouseWoodPileSwitchLevelWnd.m_EngineId
    local clientObj = CClientObjectMgr.Inst:GetObject(engineId)

    local monster = TypeAs(clientObj,typeof(CClientMonster))
    if monster then
        local tid = monster.TemplateId
        local data = Zhuangshiwu_WoodPile.GetData(CLuaHouseWoodPileSwitchLevelWnd.m_PileTemplateId)
        if data then
            local ids = data.MonsterId
            local index = 0
            for i=1,ids.Length do
                if ids[i-1]==tid then
                    index = i
                    break
                end
            end
            if index>0 then
                radioBox:ChangeTo(index-1,false)
            end
        end
    end
end

function CLuaHouseWoodPileSwitchLevelWnd:OnEnable()
	g_ScriptEvent:AddListener("SendWoodPileFightProp", self, "OnSendWoodPileFightProp")
	g_ScriptEvent:AddListener("AttachWoodPileToMonster", self, "OnAttachWoodPileToMonster")
    
end

function CLuaHouseWoodPileSwitchLevelWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SendWoodPileFightProp", self, "OnSendWoodPileFightProp")
	g_ScriptEvent:RemoveListener("AttachWoodPileToMonster", self, "OnAttachWoodPileToMonster")

end
function CLuaHouseWoodPileSwitchLevelWnd:OnAttachWoodPileToMonster(engineId,pileId,templateId)
    if pileId==CLuaHouseWoodPileSwitchLevelWnd.m_PileId then
        CLuaHouseWoodPileSwitchLevelWnd.m_EngineId = engineId
        Gac2Gas.RequestPileFightProp(engineId)
    end
end

function CLuaHouseWoodPileSwitchLevelWnd:OnSendWoodPileFightProp(engineId,fightProp)
   if CLuaHouseWoodPileSwitchLevelWnd.m_EngineId==engineId then
        for k,v in pairs(self.m_PropLabelLookup) do
            v.text = self:GetParam(fightProp,k)
        end
   end 
end

function CLuaHouseWoodPileSwitchLevelWnd:GetParam(fightProp,type)
    local val=fightProp:GetParam(type)
    return tostring(math.floor(val))
end