local LuaGameObject=import "LuaGameObject"
local MessageMgr=import "L10.Game.MessageMgr"

CLuaFeiShengRuleConfirmWnd=class()

function CLuaFeiShengRuleConfirmWnd:Init()
    LuaGameObject.GetChildNoGC(self.transform,"TitleLabel").label.text=LocalString.GetString("飞升规则")
    LuaGameObject.GetChildNoGC(self.transform,"OkButtonLabel").label.text=LocalString.GetString("领取飞升任务")
    LuaGameObject.GetChildNoGC(self.transform,"RuleConfirmLabel").label.text=LocalString.GetString("我已了解飞升规则")


    LuaGameObject.GetChildNoGC(self.transform,"RuleLabel").label.text = MessageMgr.Inst:FormatMessage("FeiShengRule",{})

    local checkbox=LuaGameObject.GetChildNoGC(self.transform,"AgreeCheckbox").qnCheckbox
    checkbox.Selected=false

    local button=LuaGameObject.GetChildNoGC(self.transform,"OkButton").gameObject
    CommonDefs.AddOnClickListener(button,DelegateFactory.Action_GameObject(function(go)
            if checkbox.Selected then
                Gac2Gas.ConfirmFeiShengRule()
                CUIManager.CloseUI(CLuaUIResources.FeiShengRuleConfirmWnd)
            else
                MessageMgr.Inst:ShowMessage("FeiShengTask_NeedCheck", {})
            end
        end),false)
end
