-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CGuildHorseRaceChampionWnd = import "L10.UI.CGuildHorseRaceChampionWnd"
local CGuildHorseRaceMgr = import "L10.Game.CGuildHorseRaceMgr"
local CMapiOverview = import "L10.Game.CMapiOverview"
local CommonDefs = import "L10.Game.CommonDefs"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local LocalString = import "LocalString"
local Screen = import "UnityEngine.Screen"
CGuildHorseRaceChampionWnd.m_Init_CS2LuaHook = function (this) 
    if CGuildHorseRaceMgr.Inst.championInfo ~= nil then
        this.championInfo = CGuildHorseRaceMgr.Inst.championInfo
        this.scoreLabel.text = System.String.Format(LocalString.GetString("马匹评分  {0}"), tostring(this.championInfo.zuoqiPingfen))
        this.playerName.text = this.championInfo.playerName
        this.guildName.text = this.championInfo.guildName
        this.icon:LoadNPCPortrait(CUICommonDef.GetPortraitName(this.championInfo.playerClass, this.championInfo.playerGender, -1), false)
        this.levelLabel.text = tostring(this.championInfo.playerLevel)
        this.horseName.text = this.championInfo.zuoqiName

        this.modelTexture.mainTexture = nil

        if this.identifier == nil then
            this.identifier = System.String.Format("__{0}__", this:GetInstanceID())
        end
        if this.championInfo.appearance ~= nil then
            this.modelTexture.mainTexture = CUIManager.CreateModelTexture(this.identifier, this, this.modelRotation, 0.05, -1, 4.66, false, true, this.scale)
        end
    end
end
CGuildHorseRaceChampionWnd.m_Load_CS2LuaHook = function (this, renderObj) 
    if this.championInfo ~= nil and this.championInfo.appearance ~= nil then
        local fakeAppearance = this.championInfo.appearance
        CClientMainPlayer.LoadResource(renderObj, fakeAppearance, true, 0.5, this.championInfo.zuoqiTId, false, 0, false, 0, false, this.championInfo.appearance.MapiAppearance, false)
    end
end
CGuildHorseRaceChampionWnd.m_OnDragModel_CS2LuaHook = function (this, go, delta) 
    if this.identifier == nil then
        return
    end
    local deltaVal = - delta.x / Screen.width * 360
    this.curRotation = this.curRotation + deltaVal
    CUIManager.SetModelRotation(this.identifier, this.curRotation)
end
CGuildHorseRaceChampionWnd.m_OnEyeButtonClicked_CS2LuaHook = function (this, go) 
    CZuoQiMgr.Inst.m_OtherPlayerId = math.floor(this.championInfo.playerId)
    CZuoQiMgr.Inst.m_OtherPlayerName = this.championInfo.playerName
    CommonDefs.ListClear(CZuoQiMgr.Inst.m_OtherMapiOverviewList)
    local mapi = CreateFromClass(CMapiOverview)
    mapi.m_Id = this.championInfo.zuoqiId
    mapi.m_Name = this.championInfo.zuoqiName
    mapi.m_Level = this.championInfo.zuoqiLevel
    mapi.m_Quality = this.championInfo.zuoqiQuality
    CommonDefs.ListAdd(CZuoQiMgr.Inst.m_OtherMapiOverviewList, typeof(CMapiOverview), mapi)
    CZuoQiMgr.Inst.m_OtherYumaPlayInfo = this.championInfo.yumaInfo

    CUIManager.ShowUI(CUIResources.MapiLookUpWnd)
end
