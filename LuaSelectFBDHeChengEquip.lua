require("3rdParty/ScriptEvent")
require("common/common_include")
local CQianKunDaiMgr=import "L10.Game.CQianKunDaiMgr"
local LuaGameObject=import "LuaGameObject"
local LocalString = import "LocalString"
local MessageMgr = import "L10.Game.MessageMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local AlignType=import "L10.UI.CItemInfoMgr+AlignType"
local CTooltipAlignType=import "L10.UI.CTooltip+AlignType"
local Vector3 = import "UnityEngine.Vector3"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local GameObject = import "UnityEngine.GameObject"
local QnCheckBox = import "L10.UI.QnCheckBox"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local IdPartition=import "L10.Game.IdPartition"
local CItemAccessListMgr=import "L10.UI.CItemAccessListMgr"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local FeiSheng_QianKunDai = import "L10.Game.FeiSheng_QianKunDai"

CLuaSelectFBDHeChengEquip=class()
RegistClassMember(CLuaSelectFBDHeChengEquip, "mItemTemplate")
RegistClassMember(CLuaSelectFBDHeChengEquip, "mDescLabel")
RegistClassMember(CLuaSelectFBDHeChengEquip, "mNoEquipLabel")
RegistClassMember(CLuaSelectFBDHeChengEquip, "mLianhuaBtn")
RegistClassMember(CLuaSelectFBDHeChengEquip, "mTable")
RegistClassMember(CLuaSelectFBDHeChengEquip, "mScrollView")

RegistClassMember(CLuaSelectFBDHeChengEquip, "mSelectedEquip")
RegistClassMember(CLuaSelectFBDHeChengEquip, "mCheckBoxList")
RegistClassMember(CLuaSelectFBDHeChengEquip, "mBlueTalismanList")


function CLuaSelectFBDHeChengEquip:Awake()
    self.mItemTemplate= LuaGameObject.GetChildNoGC(self.transform, "ItemTemplate").gameObject
    self.mItemTemplate:SetActive(false)
    self.mDescLabel= LuaGameObject.GetChildNoGC(self.transform, "DescLabel").label
    self.mNoEquipLabel= LuaGameObject.GetChildNoGC(self.transform, "NoEquipLabel").label
    self.mLianhuaBtn= LuaGameObject.GetChildNoGC(self.transform, "BtnLianhua").gameObject
    CommonDefs.AddOnClickListener(self.mLianhuaBtn,DelegateFactory.Action_GameObject(function(go)
            self:OnBtnClick(go)
        end),false)
    self.mTable = LuaGameObject.GetChildNoGC(self.transform, "FurnitureScroll").grid
    
    local fs = LuaGameObject.GetChildNoGC(self.transform, "FurnitureScroll")
    self.mScrollView = fs.gameObject:GetComponent(typeof(CUIRestrictScrollView))
end

function CLuaSelectFBDHeChengEquip:OnBtnClick(go)
	if not next(self.mBlueTalismanList) then
		local itemGetId = FeiSheng_QianKunDai.GetData().BlueTalismanItemGetId
		CItemAccessListMgr.Inst:ShowItemAccessInfo(itemGetId, false, go.transform, CTooltipAlignType.Right)
	else
		CommonDefs.ListClear(CQianKunDaiMgr.Lua_SelectedFabaoItems)
		local count = 0
		for itemId, _ in pairs(self.mSelectedEquip) do
			CommonDefs.ListAdd(CQianKunDaiMgr.Lua_SelectedFabaoItems, typeof(cs_string), itemId)
			count = count + 1
		end
		for i = count,  5 do
			CommonDefs.ListAdd(CQianKunDaiMgr.Lua_SelectedFabaoItems, typeof(cs_string), "")
		end
		
		EventManager.BroadcastInternalForLua(EnumEventType.OnFaBaoDingSelectChange,{})
		
		CUIManager.CloseUI(CUIResources.SelectFBDHeChengEquip)
	end
end

function CLuaSelectFBDHeChengEquip:OnEnable()
	g_ScriptEvent:AddListener("SendItem", self, "Init")
	g_ScriptEvent:AddListener("SetItemAt", self, "Init")
end

function CLuaSelectFBDHeChengEquip:OnDisable()
	g_ScriptEvent:RemoveListener("SendItem", self, "Init")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "Init")
end

function CLuaSelectFBDHeChengEquip:IsSelected(itemid)
	for i = 1, CQianKunDaiMgr.Lua_SelectedFabaoItems.Count do
		local sItemId = CQianKunDaiMgr.Lua_SelectedFabaoItems[i-1]
		if sItemId and sItemId ~= "" and sItemId == itemid then
			return true
		end
	end
	return false
end

function CLuaSelectFBDHeChengEquip:Init()
	self.mSelectedEquip = {}
	self.mCheckBoxList = {}
	self.mBlueTalismanList = {}
	Extensions.RemoveAllChildren(self.mTable.transform)
	
	local count = 0
	local itemProp = CClientMainPlayer.Inst.ItemProp
  local bagSize = itemProp:GetPlaceSize(EnumItemPlace.Bag)
  for i = 1, bagSize do
  	local itemId = itemProp:GetItemAt(EnumItemPlace.Bag, i)
  	local item = itemId and CItemMgr.Inst:GetById(itemId)
  	if item and IdPartition.IdIsTalisman(item.TemplateId) and item.Equip.IsBlueEquipment then
  		count = count + 1
  		local itemGO = GameObject.Instantiate(self.mItemTemplate.gameObject)
			itemGO.transform.parent = self.mTable.transform
			itemGO.transform.localScale = Vector3.one
			itemGO:SetActive(true)
			
			table.insert(self.mBlueTalismanList, itemId)
			
			local itemdata = EquipmentTemplate_Equip.GetData(item.TemplateId)
			if itemdata then
				local tex = LuaGameObject.GetChildNoGC(itemGO.transform, "Texture").cTexture
				if tex then
					tex:LoadMaterial(itemdata.Icon)
				end
				
				local qSprite = LuaGameObject.GetChildNoGC(itemGO.transform, "QualitySprite").sprite
				if qSprite then
					qSprite.spriteName = CUICommonDef.GetItemCellBorder(item.Equip.QualityType)
				end
				
				local sSprite = LuaGameObject.GetChildNoGC(itemGO.transform, "BtnSelectSprite").sprite
				if sSprite then
					sSprite.gameObject:SetActive(false)
				end
				
				local bSelected = self:IsSelected(itemId)
				if bSelected then
					self.mSelectedEquip[itemId] = true
				end
				local qCheckGO = LuaGameObject.GetChildNoGC(itemGO.transform, "CheckBox").gameObject
				local qCheck = qCheckGO and qCheckGO:GetComponent(typeof(QnCheckBox))
				if qCheck then
					qCheck:SetSelected(bSelected, true)						
					self.mCheckBoxList[itemId] = qCheck
				end
				
				CommonDefs.AddOnClickListener(itemGO, DelegateFactory.Action_GameObject(function(go)
	            self:OnEquipItemClick(itemId)
	        end),false)
			end
  	end
  end
	self.mTable:Reposition()
	self.mScrollView:ResetPosition()
	
	self.mNoEquipLabel.gameObject:SetActive(count<=0)
	local btnLabel = LuaGameObject.GetChildNoGC(self.mLianhuaBtn.transform, "Label").label
	if count <= 0 then
		btnLabel.text = LocalString.GetString("获得")
	else
		btnLabel.text = LocalString.GetString("确认选择")
	end
	self:ShowDescLabel()
end

function CLuaSelectFBDHeChengEquip:ShowDescLabel()
	local count = 0
	for itemId, _ in pairs(self.mSelectedEquip) do
		count = count + 1
	end
	local color = (count >= 6) and "[c][FFFFFF]" or "[c][FF0000]"
	self.mDescLabel.text = SafeStringFormat3(LocalString.GetString("6件可合成1个法宝鼎  %s%d[-][/c]/6"), color, count)
end

function CLuaSelectFBDHeChengEquip:OnEquipItemClick(itemId)
  local item = CItemMgr.Inst:GetById(itemId)
  if item then
	  if self.mSelectedEquip[itemId] then
				self.mSelectedEquip[itemId] = nil
				if self.mCheckBoxList[itemId] then
					self.mCheckBoxList[itemId]:SetSelected(false, true)
				end
	  else
	  	local count = 0
	  	for _, _ in pairs(self.mSelectedEquip) do
	  		count = count + 1
	  	end
	  	if count >= 6 then
	  		MessageMgr.Inst:ShowMessage("FaBaoDing_HeCheng_Select_Too_Much", {})
	  		return
	  	end
			self.mSelectedEquip[itemId] = true
			if self.mCheckBoxList[itemId] then
				self.mCheckBoxList[itemId]:SetSelected(true, true)
			end
	  end
	  
  	CItemInfoMgr.ShowLinkItemInfo(itemId,false,nil,AlignType.Default, self.mDescLabel.transform.position.x, self.mDescLabel.transform.position.y, self.mDescLabel.width, self.mDescLabel.height)
  	self:ShowDescLabel()
  end
end

return CLuaSelectFBDHeChengEquip
