-- Auto Generated!!
local CCommonRuleMgr = import "L10.UI.CCommonRuleMgr"
local CCommonRuleWnd = import "L10.UI.CCommonRuleWnd"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local CTipTitleItem = import "CTipTitleItem"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local LocalString = import "LocalString"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CCommonRuleWnd.m_Init_CS2LuaHook = function (this) 
    local title = CCommonRuleMgr.Instance:GetCommonRuleTitle()
    if title ~= nil then
        this.TitleLabel.text = title
    end

    this:ParseRuleText()

    CCommonRuleMgr.Instance:OnCommonRuleShowWndAction()
    this:UpdateCommonRuleSign()

    local label = CommonDefs.GetComponent_Component_Type(this.SignUpBtn.transform:GetChild(0), typeof(UILabel))
    if label ~= nil then
        label.text = CCommonRuleMgr.Instance:GetSignupText()
    end
    local cancelLabel = CommonDefs.GetComponent_Component_Type(this.CancelBtn.transform:GetChild(0), typeof(UILabel))
    if cancelLabel ~= nil then
        cancelLabel.text = CCommonRuleMgr.Instance:GetCancelText()
    end
    this.SignUpLabel.text = CCommonRuleMgr.Instance:GetSignedHint()
end
CCommonRuleWnd.m_ParseRuleText_CS2LuaHook = function (this) 
    Extensions.RemoveAllChildren(this.Table.transform)
    local msg = CCommonRuleMgr.Instance:GetCommonRuleMessage()
    if System.String.IsNullOrEmpty(msg) then
        return
    end

    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)

    if info == nil then
        return
    end

    if info.titleVisible then
        local titleGo = CUICommonDef.AddChild(this.Table.gameObject, this.TitleTemplate)
        titleGo:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(titleGo, typeof(CTipTitleItem)):Init(info.title)
    end

    do
        local i = 0
        while i < info.paragraphs.Count do
            local paragraphGo = CUICommonDef.AddChild(this.Table.gameObject, this.ParagraphTemplate)
            paragraphGo:SetActive(true)
            CommonDefs.GetComponent_GameObject_Type(paragraphGo, typeof(CTipParagraphItem)):Init(info.paragraphs[i], 4294967295)
            i = i + 1
        end
    end

    this.ScrollView:ResetPosition()
    this.Indicator:Layout()
    this.Table:Reposition()
end
CCommonRuleWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.CancelBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.CancelBtn).onClick, MakeDelegateFromCSFunction(this.OnCancelBtnClicked, VoidDelegate, this), true)
    UIEventListener.Get(this.SignUpBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.SignUpBtn).onClick, MakeDelegateFromCSFunction(this.OnSignUpBtnClicked, VoidDelegate, this), true)
    UIEventListener.Get(this.CloseBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.CloseBtn).onClick, MakeDelegateFromCSFunction(this.OnCloseClicked, VoidDelegate, this), true)

    EventManager.AddListener(EnumEventType.UpdateCommonRuleSign, MakeDelegateFromCSFunction(this.UpdateCommonRuleSign, Action0, this))
end
CCommonRuleWnd.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.CancelBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.CancelBtn).onClick, MakeDelegateFromCSFunction(this.OnCancelBtnClicked, VoidDelegate, this), false)
    UIEventListener.Get(this.SignUpBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.SignUpBtn).onClick, MakeDelegateFromCSFunction(this.OnSignUpBtnClicked, VoidDelegate, this), false)
    UIEventListener.Get(this.CloseBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.CloseBtn).onClick, MakeDelegateFromCSFunction(this.OnCloseClicked, VoidDelegate, this), false)

    EventManager.RemoveListener(EnumEventType.UpdateCommonRuleSign, MakeDelegateFromCSFunction(this.UpdateCommonRuleSign, Action0, this))
end
CCommonRuleWnd.m_UpdateCommonRuleSign_CS2LuaHook = function (this) 
    local bIsSigned = CCommonRuleMgr.Instance:GetCommonRuleSigned()
    if bIsSigned then
        this.SignUpLabel.gameObject:SetActive(true)
        this.CancelBtn:SetActive(true)
        this.SignUpBtn:SetActive(false)
    else
        this.SignUpLabel.gameObject:SetActive(false)
        this.CancelBtn:SetActive(false)
        this.SignUpBtn:SetActive(true)
    end
end

CCommonRuleMgr.m_ShowCommonRuleWnd_CS2LuaHook = function (this, title, message, onShowWndAction, signupAction, cancelAction, bSigned, signupText, cancelText, signedHint) 
    this.CommonRuleOnShowWndAction = onShowWndAction
    this.CommonRuleCancelAction = cancelAction
    this.CommonRuleSignUpAction = signupAction
    this.CommonRuleTitle = title
    this.CommonRuleMessage = message
    this.CommonRuleIsSigned = bSigned
    if System.String.IsNullOrEmpty(signupText) then
        this.SignupText = LocalString.GetString("报名参加")
    else
        this.SignupText = signupText
    end
    if System.String.IsNullOrEmpty(cancelText) then
        this.CancelText = LocalString.GetString("取消报名")
    else
        this.CancelText = cancelText
    end
    if System.String.IsNullOrEmpty(signedHint) then
        this.SignedHint = LocalString.GetString("已报名")
    else
        this.SignedHint = signedHint
    end
    CUIManager.ShowUI(CUIResources.CommonRuleWnd)
end
CCommonRuleMgr.m_ClearCommonRuleWnd_CS2LuaHook = function (this) 
    this.CommonRuleCancelAction = nil
    this.CommonRuleSignUpAction = nil
    this.CommonRuleOnShowWndAction = nil
    this.CommonRuleTitle = nil
    this.CommonRuleMessage = nil
    this.CommonRuleIsSigned = false
end
