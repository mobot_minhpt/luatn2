local CGroupIMGroupItem = import "L10.UI.CGroupIMGroupItem"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CGroupIMMgr = import "L10.Game.CGroupIMMgr"

CGroupIMGroupItem.m_OnClickButton_CS2LuaHook = function (this,go)
    local items = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
    local item = PopupMenuItemData(LocalString.GetString("群设置"), MakeDelegateFromCSFunction(this.GroupSetting, MakeGenericClass(Action1, Int32), this), false, nil)
    CommonDefs.ListAdd(items, typeof(PopupMenuItemData), item)
    item = PopupMenuItemData(LocalString.GetString("清空聊天"), MakeDelegateFromCSFunction(this.ClearGroupIMMsgs, MakeGenericClass(Action1, Int32), this), false, nil)
    CommonDefs.ListAdd(items, typeof(PopupMenuItemData), item)
    local isShiTuIMId = LuaGroupIMMgr:IsShiTuIMId(this.mGroupIMID)
    if not isShiTuIMId then
        item = PopupMenuItemData(LocalString.GetString("退出此群"), MakeDelegateFromCSFunction(this.LeaveGroupIM, MakeGenericClass(Action1, Int32), this), false, nil)
        CommonDefs.ListAdd(items, typeof(PopupMenuItemData), item)
    end
    local showDismissGroupBtn = true
    if isShiTuIMId then
        local info = CGroupIMMgr.Inst:GetGroupIM(this.mGroupIMID)
        if info then
            local isShiFu = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == info.OwnerId
            showDismissGroupBtn = isShiFu
        end
    end
    if showDismissGroupBtn then
        item = PopupMenuItemData(LocalString.GetString("解散此群"), MakeDelegateFromCSFunction(this.DismissGroupIM, MakeGenericClass(Action1, Int32), this), false, nil)
        CommonDefs.ListAdd(items, typeof(PopupMenuItemData), item)
    end
    CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(items), go.transform, CPopupMenuInfoMgr.AlignType.Right)
end