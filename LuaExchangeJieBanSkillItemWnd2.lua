local UIGrid = import "UIGrid"
local QnTableItem=import "L10.UI.QnTableItem"
local CUITexture = import "L10.UI.CUITexture"
local UITable = import "UITable"
local MessageWndManager = import "L10.UI.MessageWndManager"

local CItemMgr=import "L10.Game.CItemMgr"
local Item_Item=import "L10.Game.Item_Item"
local EnumItemPlace=import "L10.Game.EnumItemPlace"
local CItemCountUpdate=import "L10.UI.CItemCountUpdate"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"

CLuaExchangeJieBanSkillItemWnd2 = class()
-- RegistClassMember(CLuaExchangeJieBanSkillItemWnd2, "m_TableView")
RegistClassMember(CLuaExchangeJieBanSkillItemWnd2, "m_ItemTemplateId")
RegistClassMember(CLuaExchangeJieBanSkillItemWnd2, "m_SkillIds")
RegistClassMember(CLuaExchangeJieBanSkillItemWnd2, "m_Items")
RegistClassMember(CLuaExchangeJieBanSkillItemWnd2, "m_SelectedSkillId")
-- RegistClassMember(CLuaExchangeJieBanSkillItemWnd2, "m_CountLabel")
RegistClassMember(CLuaExchangeJieBanSkillItemWnd2, "m_Button")

function CLuaExchangeJieBanSkillItemWnd2:Init()
    local countUpdate=FindChild(self.transform,"CountLabel"):GetComponent(typeof(CItemCountUpdate))
    countUpdate:UpdateCount()

    self.m_SelectedSkillId=0

    -- self.m_CountLabel=FindChild(self.transform,"CountLabel"):GetComponent(typeof(UILabel))
    self.m_Button = FindChild(self.transform,"Button").gameObject
    -- CUICommonDef.SetActive(self.m_Button,false,true)

    UIEventListener.Get(self.m_Button).onClick=DelegateFactory.VoidDelegate(function(go)
        if self.m_SelectedSkillId>0 then
            local bookItemTempId = self.m_SelectedSkillId
            local msg = g_MessageMgr:FormatMessage("Exchange_LingShouJieBanSkill_Confirm",
                Item_Item.GetData(self.m_ItemTemplateId).Name,
                Item_Item.GetData(bookItemTempId).Name)
            MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function()
                --找青竹竹剪
                local ret, pos ,itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag,self.m_ItemTemplateId)
                if ret then
                    -- local bookItemTempId = self.m_SkillIds[self.m_TableView.currentSelectRow] or 0
                    Gac2Gas.RequestExchangePartnerSkillBook(pos, itemId, bookItemTempId)
                else
                    --没有物品
                    g_MessageMgr:ShowMessage("Exchange_LingShouJieBanSkill_NoMat")
                end
            end), nil, nil, nil, false)
        else
            g_MessageMgr:ShowMessage("Exchange_LingShouJieBanSkill_NeedSelect")
        end
    end)


    local template1=FindChild(self.transform,"Template1").gameObject
    template1:SetActive(false)
    local template2 = FindChild(self.transform,"Template2").gameObject
    template2:SetActive(false)


    local setting = LingShouPartner_Setting.GetData()
    self.m_ItemTemplateId = setting.ProfessionalSkillBookBoxId

    local professionalSkillBooks = setting.ProfessionalSkillBookSet

    local classId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.Class or 0

    local skills1={}
    local skills2={}
    local skills3={}
    for i=1,professionalSkillBooks.Length,2 do
        local skillId = professionalSkillBooks[i-1]
        local cls = professionalSkillBooks[i]
        if cls==classId then
            table.insert( skills1,skillId )
        elseif cls == 0 then
            table.insert( skills2,skillId )
        else
            table.insert( skills3,skillId )
        end
    end
    self.m_SkillIds={}
    self.m_Items = {}
    for i,v in ipairs(skills1) do
        table.insert( self.m_SkillIds, v )
    end
    for i,v in ipairs(skills2) do
        table.insert( self.m_SkillIds, v )
    end
    for i,v in ipairs(skills3) do
        table.insert( self.m_SkillIds, v )
    end

    local contentTable = FindChild(self.transform,"ContentTable").gameObject
    Extensions.RemoveAllChildren(contentTable.transform)

    local func = function(skills,index)
        local go = NGUITools.AddChild(contentTable, template1)
        go:SetActive(true)
        local titleLabel= go.transform:Find("TitleLabel"):GetComponent(typeof(UILabel))
        if index==1 then
            titleLabel.text=LocalString.GetString("增强本职业技能书")
        elseif index==2 then
            titleLabel.text=LocalString.GetString("克制其他职业技能书")
        elseif index==3 then
            titleLabel.text=LocalString.GetString("增强其他职业技能书")
        end

        local grid = go.transform:Find("Grid").gameObject
        for i,v in ipairs(skills) do
            local item = NGUITools.AddChild(grid,template2)
            item:SetActive(true)
            self:InitItem(item.transform,v,index==3)
            table.insert( self.m_Items,item )
            if index==3 then
                UIEventListener.Get(item).onClick=DelegateFactory.VoidDelegate(function(p)
                    CItemInfoMgr.ShowLinkItemTemplateInfo(v, false, nil, AlignType.Default, 0, 0, 0, 0)
                    g_MessageMgr:ShowMessage("Exchange_Forbid_LingShouJieBanSkill")
                end)
            else
                UIEventListener.Get(item).onClick=DelegateFactory.VoidDelegate(function(p)
                    self:OnSelected(p)
                end)
            end
        end
        grid:GetComponent(typeof(UIGrid)):Reposition()
        -- contentTable:GetComponent(typeof(UITable)):Reposition()
    end

    if #skills1>0 then
        func(skills1,1)
    end
    if #skills2>0 then
        func(skills2,2)
    end
    if #skills3>0 then
        func(skills3,3)
    end
    contentTable:GetComponent(typeof(UITable)):Reposition()
end

function CLuaExchangeJieBanSkillItemWnd2:OnSelected(go)
    -- CUICommonDef.SetActive(self.m_Button,true,true)

    for i,v in ipairs(self.m_Items) do
        if v==go then
            v:GetComponent(typeof(QnTableItem)):SetSelected(true,false)
            self.m_SelectedSkillId=self.m_SkillIds[i]
            CItemInfoMgr.ShowLinkItemTemplateInfo(self.m_SelectedSkillId, false, nil, AlignType.Default, 0, 0, 0, 0)
        else
            v:GetComponent(typeof(QnTableItem)):SetSelected(false,false)
        end
    end
end

function CLuaExchangeJieBanSkillItemWnd2:InitItem(transform,id,disable)
    local icon = transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local nameLabel = transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local data = Item_Item.GetData(id)
    if data then
        icon:LoadMaterial(data.Icon)
        nameLabel.text=data.Name
    end

    local mask = transform:Find("Mask").gameObject
    mask:SetActive(disable)
end

