local CScheduleMgr=import "L10.Game.CScheduleMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local GameObject = import "UnityEngine.GameObject"
local CJingLingMgr = import "L10.Game.CJingLingMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CRankData = import "L10.UI.CRankData"

LuaTreasureSeaSignUpWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaTreasureSeaSignUpWnd, "GoButton", "GoButton", QnButton)
RegistChildComponent(LuaTreasureSeaSignUpWnd, "RankButton", "RankButton", GameObject)
RegistChildComponent(LuaTreasureSeaSignUpWnd, "RuleButton", "RuleButton", GameObject)
RegistChildComponent(LuaTreasureSeaSignUpWnd, "ImageRule", "ImageRule", CCommonLuaScript)
RegistChildComponent(LuaTreasureSeaSignUpWnd, "Rewards", "Rewards", Transform)
RegistChildComponent(LuaTreasureSeaSignUpWnd, "TimeLabel", "TimeLabel", UILabel)
RegistChildComponent(LuaTreasureSeaSignUpWnd, "LevelLabel", "LevelLabel", UILabel)
RegistChildComponent(LuaTreasureSeaSignUpWnd, "DescLabel", "DescLabel", UILabel)
RegistChildComponent(LuaTreasureSeaSignUpWnd, "MatchLabel", "MatchLabel", UILabel)
RegistChildComponent(LuaTreasureSeaSignUpWnd, "RewardCountLabel", "RewardCountLabel", UILabel)

--@endregion RegistChildComponent end

RegistClassMember(LuaTreasureSeaSignUpWnd, "m_PlayId")
RegistClassMember(LuaTreasureSeaSignUpWnd, "isInMatching")


function LuaTreasureSeaSignUpWnd:Awake()
    self.m_PlayId = NavalWar_Setting.GetData().PvpSeaPlayId

    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    UIEventListener.Get(self.GoButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        if self.isInMatching then
            Gac2Gas.GlobalMatch_RequestCancelSignUp(self.m_PlayId)
        else
            Gac2Gas.GlobalMatch_RequestSignUp(self.m_PlayId)
        end
    end)
    UIEventListener.Get(self.RankButton).onClick = DelegateFactory.VoidDelegate(function(go)
        LuaTreasureSeaRankRewardDetailWnd.s_Tab = 0
        CUIManager.ShowUI(CLuaUIResources.TreasureSeaRankRewardDetailWnd)
    end)
    UIEventListener.Get(self.RuleButton).onClick = DelegateFactory.VoidDelegate(function(go)
        CJingLingMgr.Inst:ShowJingLingWnd(LocalString.GetString("藏宝海域"), "o_push", true, false, nil, false)
    end)
    --
    -- Gac2Gas.QueryTeamSelectConfirmWaitInfo()


    local HaiZhanPvPRewardItem = ShuJia2023_Setting.GetData("HaiZhanPvPRewardItem").Value
    local items = {}
    for _,id in string.gmatch(HaiZhanPvPRewardItem, "(%d+),(%d+);") do
        table.insert(items,id)
    end
    for index, templateId in ipairs(items) do
        local child = self.Rewards:GetChild(index-1)
        child:Find("Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(Item_Item.GetData(templateId).Icon)
        UIEventListener.Get(child.gameObject).onClick = DelegateFactory.VoidDelegate(function ()
            if index == 1 then
                LuaTreasureSeaRankRewardDetailWnd.s_Tab = 1
                CUIManager.ShowUI(CLuaUIResources.TreasureSeaRankRewardDetailWnd)
            else
                CItemInfoMgr.ShowLinkItemTemplateInfo(templateId, false, nil, AlignType.Default, 0, 0, 0, 0)
            end
        end)
    end

    self.TimeLabel.text = ShuJia2023_Setting.GetData("HaiZhanPvPTime").Value
    self.LevelLabel.text = ShuJia2023_Setting.GetData("HaiZhanPvPLv").Value
    self.DescLabel.text = ShuJia2023_Setting.GetData("HaiZhanPvPTaskDesc").Value

end

function LuaTreasureSeaSignUpWnd:Init()
    local imagePaths = {
        "UI/Texture/FestivalActivity/Festival_ShuJia/ShuJia2023/Material/treasureseaguide_01.mat",
        "UI/Texture/FestivalActivity/Festival_ShuJia/ShuJia2023/Material/treasureseaguide_02.mat",
        "UI/Texture/FestivalActivity/Festival_ShuJia/ShuJia2023/Material/treasureseaguide_03.mat",
        "UI/Texture/FestivalActivity/Festival_ShuJia/ShuJia2023/Material/treasureseaguide_04.mat",
    }
    local msgs = {
        g_MessageMgr:FormatMessage("CBHY_TIPS_1"),
        g_MessageMgr:FormatMessage("CBHY_TIPS_2"),
        g_MessageMgr:FormatMessage("CBHY_TIPS_3"),
        g_MessageMgr:FormatMessage("CBHY_TIPS_4"),
        --g_MessageMgr:FormatMessage("CBHY_TIPS_5"),
        --g_MessageMgr:FormatMessage("CBHY_TIPS_6"),
    }
	self.ImageRule:Init(imagePaths, msgs)

    if CClientMainPlayer.Inst then
        Gac2Gas.GlobalMatch_RequestCheckSignUp(self.m_PlayId, CClientMainPlayer.Inst.Id)
    end
    self.MatchLabel.gameObject:SetActive(false)


    local cnt = CScheduleMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eNavalWarPvpRewardTimes)
    self.RewardCountLabel.text = string.format(LocalString.GetString("每周奖励次数 %d/%d"),cnt, NavalWar_Setting.GetData().PvpRewardWeekLimit)
end

--@region UIEvent

--@endregion UIEvent
function LuaTreasureSeaSignUpWnd:OnEnable()
	g_ScriptEvent:AddListener("GlobalMatch_CheckInMatchingResult", self, "OnGlobalMatch_CheckInMatchingResult")
    g_ScriptEvent:AddListener("GlobalMatch_SignUpPlayResult", self, "OnGlobalMatch_SignUpPlayResult")
    g_ScriptEvent:AddListener("GlobalMatch_CancelSignUpResult", self, "OnGlobalMatch_CancelSignUpResult")
end

function LuaTreasureSeaSignUpWnd:OnDisable()
	g_ScriptEvent:RemoveListener("GlobalMatch_CheckInMatchingResult", self, "OnGlobalMatch_CheckInMatchingResult")
    g_ScriptEvent:RemoveListener("GlobalMatch_SignUpPlayResult", self, "OnGlobalMatch_SignUpPlayResult")
    g_ScriptEvent:RemoveListener("GlobalMatch_CancelSignUpResult", self, "OnGlobalMatch_CancelSignUpResult")
end
function LuaTreasureSeaSignUpWnd:OnGlobalMatch_SignUpPlayResult(playId, success)
    -- print("OnGlobalMatch_SignUpPlayResult",playId, success)
    if playId == self.m_PlayId then
		self.isInMatching = success
		self:UpdateMatchButton()
    end
end
function LuaTreasureSeaSignUpWnd:OnGlobalMatch_CheckInMatchingResult(playerId, playId, isInMatching, resultStr)
    -- print("OnGlobalMatch_CheckInMatchingResult",playId, isInMatching)
    if playId == self.m_PlayId then
		self.isInMatching = isInMatching
        self:UpdateMatchButton()
    end
end

function LuaTreasureSeaSignUpWnd:OnGlobalMatch_CancelSignUpResult(playId, success)
    -- print("OnGlobalMatch_CancelSignUpResult",playId, success)
	if playId == self.m_PlayId and success then
        self.isInMatching = false
		self:UpdateMatchButton()
    end
end

function LuaTreasureSeaSignUpWnd:UpdateMatchButton()
	self.GoButton.gameObject:SetActive(true)
	if self.isInMatching then
		self.GoButton.Text = LocalString.GetString("取消匹配")
    else
		self.GoButton.Text = LocalString.GetString("开始匹配")
    end
    self.MatchLabel.gameObject:SetActive(self.isInMatching)
end