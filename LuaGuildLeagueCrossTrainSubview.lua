local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UITabBar = import "L10.UI.UITabBar"
local UISprite = import "UISprite"
local UIScrollView  = import "UIScrollView"
local UIGrid = import "UIGrid"
local CButton = import "L10.UI.CButton"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Item_Item = import "L10.Game.Item_Item"
local UILongPressButton = import "L10.UI.UILongPressButton"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CAsyncLoadTexture = import "L10.UI.CAsyncLoadTexture"
local CCommonUIFxWnd = import "L10.UI.CCommonUIFxWnd"

LuaGuildLeagueCrossTrainSubview = class()

RegistChildComponent(LuaGuildLeagueCrossTrainSubview, "m_TrainItem", "TrainItem", GameObject)
RegistChildComponent(LuaGuildLeagueCrossTrainSubview, "m_TrainTable", "TrainTable", UIGrid)
RegistChildComponent(LuaGuildLeagueCrossTrainSubview, "m_CurTrainRoot", "CurTrainRoot", Transform)
RegistChildComponent(LuaGuildLeagueCrossTrainSubview, "m_PackageRoot", "PackageRoot", GameObject)
RegistChildComponent(LuaGuildLeagueCrossTrainSubview, "m_PackageItemScrollView", "PackageItemScrollView", UIScrollView)
RegistChildComponent(LuaGuildLeagueCrossTrainSubview, "m_PackageItemGrid", "PackageItemGrid", UIGrid)
RegistChildComponent(LuaGuildLeagueCrossTrainSubview, "m_ItemCellTemplate", "ItemCellTemplate", GameObject)
RegistChildComponent(LuaGuildLeagueCrossTrainSubview, "m_NeedItemLabel", "NeedItemLabel", UILabel)
RegistChildComponent(LuaGuildLeagueCrossTrainSubview, "m_CurSubmitRoot", "CurSubmitRoot", Transform)

RegistClassMember(LuaGuildLeagueCrossTrainSubview, "m_TrainGradientBg")
--CurTrainRoot
RegistClassMember(LuaGuildLeagueCrossTrainSubview, "m_CurTrainNameLabel")
RegistClassMember(LuaGuildLeagueCrossTrainSubview, "m_CurTrainProgressLabel")
RegistClassMember(LuaGuildLeagueCrossTrainSubview, "m_CurTrainEffectLabel")
RegistClassMember(LuaGuildLeagueCrossTrainSubview, "m_CurTrainAwardItemTemplate")
RegistClassMember(LuaGuildLeagueCrossTrainSubview, "m_CurTrainAwardItemTable")

--CurSubmitRoot
RegistClassMember(LuaGuildLeagueCrossTrainSubview, "m_CurrentItemIcon")
RegistClassMember(LuaGuildLeagueCrossTrainSubview, "m_CurrentItemQualitySprite")
RegistClassMember(LuaGuildLeagueCrossTrainSubview, "m_CurrentItemBindSprite")
RegistClassMember(LuaGuildLeagueCrossTrainSubview, "m_CurrentItemNameLabel")
RegistClassMember(LuaGuildLeagueCrossTrainSubview, "m_CurrentItemProcessValueLabel")
RegistClassMember(LuaGuildLeagueCrossTrainSubview, "m_CurrentItemScoreValueLabel")
RegistClassMember(LuaGuildLeagueCrossTrainSubview, "m_IncreaseAndDecreaseButton")
RegistClassMember(LuaGuildLeagueCrossTrainSubview, "m_SubmitButton")
RegistClassMember(LuaGuildLeagueCrossTrainSubview, "m_InfoButton")

RegistClassMember(LuaGuildLeagueCrossTrainSubview, "m_CurTrainType")
RegistClassMember(LuaGuildLeagueCrossTrainSubview, "m_CurItemId")
RegistClassMember(LuaGuildLeagueCrossTrainSubview, "m_TrainLevelRewardDesignInfo")

function LuaGuildLeagueCrossTrainSubview:Awake()
    self.m_CurTrainNameLabel = self.m_CurTrainRoot:Find("NameLabel"):GetComponent(typeof(UILabel))
    self.m_CurTrainProgressLabel = self.m_CurTrainRoot:Find("ProgressLabel"):GetComponent(typeof(UILabel))
    self.m_CurTrainEffectLabel = self.m_CurTrainRoot:Find("EffectLabel"):GetComponent(typeof(UILabel))
    self.m_CurTrainAwardItemTemplate = self.m_CurTrainRoot:Find("Item").gameObject
    self.m_CurTrainAwardItemTable = self.m_CurTrainRoot:Find("Table"):GetComponent(typeof(UITable))
    self.m_TrainGradientBg =  self.transform:Find("Container/GradientBg"):GetComponent(typeof(UIWidget))

    self.m_CurrentItemIcon = self.m_CurSubmitRoot:Find("SelectedItem/IconTexture"):GetComponent(typeof(CUITexture))
    self.m_CurrentItemQualitySprite = self.m_CurSubmitRoot:Find("SelectedItem/QualitySprite"):GetComponent(typeof(UISprite))
    self.m_CurrentItemBindSprite = self.m_CurSubmitRoot:Find("SelectedItem/BindSprite"):GetComponent(typeof(UISprite))
    self.m_CurrentItemNameLabel = self.m_CurSubmitRoot:Find("SelectedItem/NameLabel"):GetComponent(typeof(UILabel))
    self.m_CurrentItemProcessValueLabel = self.m_CurSubmitRoot:Find("Progress/ValueLabel"):GetComponent(typeof(UILabel))
    self.m_CurrentItemScoreValueLabel = self.m_CurSubmitRoot:Find("Score/ValueLabel"):GetComponent(typeof(UILabel))
    self.m_IncreaseAndDecreaseButton = self.m_CurSubmitRoot:Find("QnIncreseAndDecreaseButton"):GetComponent(typeof(QnAddSubAndInputButton))
    self.m_SubmitButton = self.m_CurSubmitRoot:Find("SubmitButton"):GetComponent(typeof(CButton))
    self.m_InfoButton = self.m_CurSubmitRoot:Find("InfoButton").gameObject

    self.m_TrainItem:SetActive(false)
    self.m_ItemCellTemplate:SetActive(false)
    self.m_CurTrainAwardItemTemplate:SetActive(false)

    self.m_IncreaseAndDecreaseButton:SetMinMax(0,0,1)
    self.m_IncreaseAndDecreaseButton.onValueChanged = DelegateFactory.Action_uint(function (value) 
        self:OnNumChanged(value)
    end)

    UIEventListener.Get(self.m_SubmitButton.gameObject).onClick = DelegateFactory.VoidDelegate(function() self:OnSubmitButtonClick() end)
    UIEventListener.Get(self.m_InfoButton).onClick = DelegateFactory.VoidDelegate(function() self:OnInfoButtonClick() end)
end

function LuaGuildLeagueCrossTrainSubview:Init()
    self:LoadDesignData()
    local trainInfo = LuaGuildLeagueCrossMgr.m_TrainInfo
    self:InitTrainTable(trainInfo)
    if self.m_CurTrainType==nil or self.m_CurTrainType==0 then
        self.m_CurTrainType = 1
    end
    local n = self.m_TrainTable.transform.childCount
    if n>=self.m_CurTrainType then
        self:OnTrainItemClick(self.m_TrainTable.transform:GetChild(self.m_CurTrainType-1).gameObject)
    end
end

function LuaGuildLeagueCrossTrainSubview:LoadDesignData()
    self.m_TrainLevelRewardDesignInfo = {}
    GuildLeagueCross_TrainLvReward.Foreach(function(key, data)
        local trainType = math.floor(key/10)
        local index = math.floor(key%10)
        if self.m_TrainLevelRewardDesignInfo[trainType]==nil then
            self.m_TrainLevelRewardDesignInfo[trainType] = {}
        end
        table.insert(self.m_TrainLevelRewardDesignInfo[trainType], index, {item=data.RewardItem[0][0], level=data.TrainLevel})
    end)
end

function LuaGuildLeagueCrossTrainSubview:InitTrainTable(info)
    Extensions.RemoveAllChildren(self.m_TrainTable.transform)

    for i=1,#info.nameInfo do
        local child = CUICommonDef.AddChild(self.m_TrainTable.gameObject, self.m_TrainItem)
        child:SetActive(true)
        local icon = child.transform:GetComponent(typeof(CUITexture))
        local outerCircle = child.transform:Find("OuterCircle"):GetComponent(typeof(UIWidget))
        local innerCircle = child.transform:Find("InnerCircle"):GetComponent(typeof(UIWidget))
        local zhuangshi1 = child.transform:Find("ZhuangShiTexture"):GetComponent(typeof(UIWidget))
        local zhuangshi2 = child.transform:Find("ZhuangShiTexture2"):GetComponent(typeof(UIWidget))
        local levelLabel = child.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
        local effectLabel = child.transform:Find("EffectLabel"):GetComponent(typeof(UILabel))
        icon:LoadMaterial(info.portraitInfo[i], false)
        outerCircle.color = info.colorInfo[i]
        zhuangshi1.color = info.colorInfo[i]
        zhuangshi2.color = info.colorInfo[i]
        local curLevel = info.levelInfo[i]
        levelLabel.text = SafeStringFormat3(LocalString.GetString("%d级"), curLevel)
        local designData = GuildLeagueCross_TrainLevel.GetData(curLevel)
        local nextData = GuildLeagueCross_TrainLevel.Exists(curLevel+1) and GuildLeagueCross_TrainLevel.GetData(curLevel+1) or nil

        local suffix = tostring(i)
        effectLabel.text = SafeStringFormat3(LocalString.GetString("+%d%%"),designData["EffectValue"..suffix])
        outerCircle.fillAmount = info.progressInfo[i]/ designData["UpgradeValue"..suffix]

        UIEventListener.Get(child).onClick = DelegateFactory.VoidDelegate(function() self:OnTrainItemClick(child) end)
    end
    self.m_TrainTable:Reposition()
    self:UpdateTrainLevelAwardAlert()
end

function LuaGuildLeagueCrossTrainSubview:UpdateTrainLevelAwardAlert()
    local n = self.m_TrainTable.transform.childCount
    for i=0,n-1 do
        local child = self.m_TrainTable.transform:GetChild(i)
        local alert = child:Find("Alert").gameObject
        local info = LuaGuildLeagueCrossMgr.m_TrainInfo
        local trainType = i+1
        local curLevel = info.levelInfo[trainType]
        local rewardedIndex = info.trainLevelRewardInfo[trainType]
        local designInfo = self.m_TrainLevelRewardDesignInfo[trainType]
        alert:SetActive(false)
        for j=1,#designInfo do
            if curLevel>=designInfo[j].level and rewardedIndex<j then
                alert:SetActive(true)
            end
        end
    end
end

function LuaGuildLeagueCrossTrainSubview:OnTrainItemClick(go)
    local n = self.m_TrainTable.transform.childCount
    for i=0,n-1 do
        local child = self.m_TrainTable.transform:GetChild(i)
        local selected = child:Find("Selected").gameObject
        if go==child.gameObject then
            selected:SetActive(true)
            selected.transform:GetComponent(typeof(CAsyncLoadTexture)):ShowTexture(i)
            self.m_CurTrainType = i + 1
            self.m_CurItemId = 0
            self:InitTrainInfo(self.m_CurTrainType)
            self:InitPackageInfo(self.m_CurTrainType)
        else
            selected:SetActive(false)
        end
        
    end
end

function LuaGuildLeagueCrossTrainSubview:OnEnable()
    g_ScriptEvent:AddListener("OnSubmitItemToGuildLeagueTrainSuccess", self, "OnSubmitItemToGuildLeagueTrainSuccess")
    g_ScriptEvent:AddListener("RequestGetGuildLeagueCrossTrainLevelRewardSuccess", self, "OnRequestGetGuildLeagueCrossTrainLevelRewardSuccess")
end

function LuaGuildLeagueCrossTrainSubview:OnDisable()
    g_ScriptEvent:RemoveListener("OnSubmitItemToGuildLeagueTrainSuccess", self, "OnSubmitItemToGuildLeagueTrainSuccess")
    g_ScriptEvent:RemoveListener("RequestGetGuildLeagueCrossTrainLevelRewardSuccess", self, "OnRequestGetGuildLeagueCrossTrainLevelRewardSuccess")
end

function LuaGuildLeagueCrossTrainSubview:OnSubmitItemToGuildLeagueTrainSuccess(trainType)

    local trainInfo = LuaGuildLeagueCrossMgr.m_TrainInfo
    self:InitTrainTable(trainInfo)
    if self.m_CurTrainType and self.m_CurTrainType==trainType then
        self:OnTrainItemClick(self.m_TrainTable.transform:GetChild(self.m_CurTrainType-1).gameObject)
    end
   
    local lineTbl = {"vfx_tuoweihong.prefab", "vfx_tuoweihhuang.prefab", "vfx_tuoweilan.prefab"}
    local boomTbl = {"vfx_hongsebaodian.prefab", "vfx_huangsebaodian.prefab", "vfx_lansebaodian.prefab"}
    CCommonUIFxWnd.Instance:PlayLineAni(self.m_SubmitButton.transform.position, 
        self.m_TrainTable.transform:GetChild(trainType-1).position, 
        "fx/vfx/prefab/dynamic/"..lineTbl[trainType], 
        "fx/vfx/prefab/dynamic/"..boomTbl[trainType], 
        0.3,
        nil)
end

function LuaGuildLeagueCrossTrainSubview:OnRequestGetGuildLeagueCrossTrainLevelRewardSuccess(trainType, rewardIdx)
    if trainType==self.m_CurTrainType then
        self:InitTrainInfo(self.m_CurTrainType)
    end
    self:UpdateTrainLevelAwardAlert()
end
-----------------------------------
----当前培养展示
-----------------------------------
function LuaGuildLeagueCrossTrainSubview:InitTrainInfo(trainType)
        local info = LuaGuildLeagueCrossMgr.m_TrainInfo
        local curLevel = info.levelInfo[trainType]
        self.m_TrainGradientBg.color = info.colorInfo[trainType]
        self.m_CurTrainNameLabel.color = info.nameColorInfo[trainType]
        self.m_CurTrainNameLabel.text = info.nameInfo[trainType].." "..SafeStringFormat3(LocalString.GetString("%d级"), curLevel)

        local designData = GuildLeagueCross_TrainLevel.GetData(curLevel)
        local nextData = GuildLeagueCross_TrainLevel.Exists(curLevel+1) and GuildLeagueCross_TrainLevel.GetData(curLevel+1) or nil
        local suffix = tostring(trainType)

        self.m_CurTrainProgressLabel.text = SafeStringFormat3("%d/%d", info.progressInfo[trainType], designData["UpgradeValue"..suffix])

        self.m_CurTrainEffectLabel.text = SafeStringFormat3(LocalString.GetString("跨服帮会联赛期间本服帮会%s[00ff00]+%d%%[-]"), 
                info.nameInfo[trainType], designData["EffectValue"..suffix])

        if nextData and designData["UpgradeValue"..suffix]>0 then
            self.m_CurTrainEffectLabel.text = SafeStringFormat3(LocalString.GetString("跨服帮会联赛期间本服帮会%s[00ff00]+%d%%[-](下级[00ff00]+%d%%[-])"), 
                info.nameInfo[trainType], designData["EffectValue"..suffix], nextData["EffectValue"..suffix])
        else
            local maxLevelData = GuildLeagueCross_TrainLevel.GetData(curLevel - 1)
            self.m_CurTrainProgressLabel.text = SafeStringFormat3("%d/%d", maxLevelData["UpgradeValue"..suffix], maxLevelData["UpgradeValue"..suffix])
        end

        self:InitTrainLevelAwardInfo(trainType)
end

function LuaGuildLeagueCrossTrainSubview:InitTrainLevelAwardInfo(trainType)
    local info = LuaGuildLeagueCrossMgr.m_TrainInfo
    local curLevel = info.levelInfo[trainType]
    local rewardedIndex = info.trainLevelRewardInfo[trainType]--1,2,3,4
    local color = info.colorInfo[trainType]


    local n = self.m_CurTrainAwardItemTable.transform.childCount
    for i=0,n-1 do
        self.m_CurTrainAwardItemTable.transform:GetChild(i).gameObject:SetActive(false)
    end

    local designInfo = self.m_TrainLevelRewardDesignInfo[trainType]
    for i=1,#designInfo do
        local go = nil
        if i<n then
			go = self.m_CurTrainAwardItemTable.transform:GetChild(i-1).gameObject
		else
			go = CUICommonDef.AddChild(self.m_CurTrainAwardItemTable.gameObject, self.m_CurTrainAwardItemTemplate)
		end
        go:SetActive(true)
        local info = designInfo[i]
        local iconTexture = go.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
        local disableSprite = go.transform:Find("DisableSprite").gameObject
        local lockSprite = go.transform:Find("LockSprite").gameObject
        local awardedSprite = go.transform:Find("AwardedSprite").gameObject
        local border = go.transform:Find("Border"):GetComponent(typeof(UIWidget)) 
        local levelLabel = go.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
        local itemDesignData = Item_Item.GetData(info.item)
        iconTexture:LoadMaterial(itemDesignData.Icon)
        disableSprite:SetActive(i<=rewardedIndex or curLevel<info.level)
        lockSprite:SetActive(curLevel<info.level)
        awardedSprite:SetActive(i<=rewardedIndex)
        border.color = color
        levelLabel.text = tostring(info.level)
        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function() self:OnTrainLevelAwardItemClick(trainType, i) end)
    end
    self.m_CurTrainAwardItemTable:Reposition()
end

function LuaGuildLeagueCrossTrainSubview:OnTrainLevelAwardItemClick(trainType, rewardIdx)
    local info = LuaGuildLeagueCrossMgr.m_TrainInfo
    local curLevel = info.levelInfo[trainType]
    local rewardedIndex = info.trainLevelRewardInfo[trainType]
    local designInfo = self.m_TrainLevelRewardDesignInfo[trainType]
    if curLevel>=designInfo[rewardIdx].level and rewardedIndex<rewardIdx then
        LuaGuildLeagueCrossMgr:RequestGetGuildLeagueCrossTrainLevelReward(trainType, rewardIdx)
    else
        CItemInfoMgr.ShowLinkItemTemplateInfo(designInfo[rewardIdx].item, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
    end
end
-----------------------------------
----包裹物品展示
-----------------------------------
function LuaGuildLeagueCrossTrainSubview:InitPackageInfo(trainType)
    local trainItems = LuaGuildLeagueCrossMgr:GetTrainItems(trainType)
    local tbl = {}
    local bExist = false
    local bagSize = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp:GetPlaceSize(EnumItemPlace.Bag) or 0
    for i=1,bagSize do
        local item = CItemMgr.Inst:GetPackageItemAtPos(i)
        if item and trainItems[item.TemplateId] and (not trainItems[item.TemplateId].NeedBind or item.IsBinded) then
            table.insert(tbl, {index=i, data=item, priority = trainItems[item.TemplateId].Priority })
            if self.m_CurItemId == item.Id then
                bExist = true
            end
        end
    end

    table.sort(tbl, function(info1,info2)
        if info1.priority~=info2.priority then
            return info1.priority>info2.priority
        else
            return info1.index<info2.index
        end
    end)

    local childCount = self.m_PackageItemGrid.transform.childCount
    for i=0,childCount-1 do
        self.m_PackageItemGrid.transform:GetChild(i).gameObject:SetActive(false)
    end

    if #tbl>0 then
        self.m_PackageRoot:SetActive(true)
        self.m_NeedItemLabel.gameObject:SetActive(false)
        local index = 0
        for i=1,#tbl do
            local child = nil
            if index<childCount then
                child = self.m_PackageItemGrid.transform:GetChild(index).gameObject
            else
                child = CUICommonDef.AddChild(self.m_PackageItemGrid.gameObject, self.m_ItemCellTemplate)
            end
            index=index+1
            child:SetActive(true)
            local bSelected = false
            if bExist then
                bSelected = (self.m_CurItemId == tbl[i].data.Id)
            else
                bSelected = (i==1 and tbl[i].priority>0)
            end
            self:InitItemCell(child.transform, tbl[i].data, bSelected)
            if bSelected then
                self:InitSelectedItem(tbl[i].data.Id)
            end
        end
        if not CItemMgr.Inst:GetById(self.m_CurItemId) then --没有选中任何目标
            self:InitSelectedItem(nil)
        end  
    else
        self.m_PackageRoot:SetActive(false)
        self.m_NeedItemLabel.gameObject:SetActive(true)
        self.m_NeedItemLabel.text = g_MessageMgr:FormatMessage("GLC_GUILD_LEAGUE_TRAIN_SUBMIT_NEED_ITEM_DESC_"..tostring(trainType))
    end
   
    self.m_PackageItemGrid:Reposition()
    self.m_PackageItemScrollView:ResetPosition()
end

function LuaGuildLeagueCrossTrainSubview:OnPackageItemClick(go, itemId)
    local childCount = self.m_PackageItemGrid.transform.childCount
    for i=0,childCount-1 do
        local child = self.m_PackageItemGrid.transform:GetChild(i)
        if child.gameObject == go then
            child:Find("SelectedSprite").gameObject:SetActive(true)
            self:InitSelectedItem(itemId)
        else
            child:Find("SelectedSprite").gameObject:SetActive(false)
        end
    end
end

function LuaGuildLeagueCrossTrainSubview:InitItemCell(transRoot, item, bSelected)
    local iconTexture = transRoot:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local qualitySprite = transRoot:Find("QualitySprite"):GetComponent(typeof(UISprite))
    local bindSprite = transRoot:Find("BindSprite"):GetComponent(typeof(UISprite))
    local countLabel = transRoot:Find("CountLabel"):GetComponent(typeof(UILabel))
    local selectedSprite = transRoot:Find("SelectedSprite").gameObject
    local longPressCmp = transRoot:GetComponent(typeof(UILongPressButton))

    iconTexture:LoadMaterial(item.Icon)
    qualitySprite.spriteName = item.IsItem and CUICommonDef.GetItemCellBorder(Item_Item.GetData(item.TemplateId), item, false) or CUICommonDef.GetItemCellBorder(item.Equip.QualityType)
    bindSprite.spriteName = item.BindOrEquipCornerMark
    countLabel.text = item.IsItem and item.Item.Count>1 and tostring(item.Item.Count) or ""
    selectedSprite:SetActive(bSelected)

    local id = item.Id
    longPressCmp.OnLongPressDelegate = DelegateFactory.Action(function()
        CItemInfoMgr.ShowLinkItemInfo(id, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
    end)
    longPressCmp.OnClickDelegate = DelegateFactory.Action(function()
        self:OnPackageItemClick(transRoot.gameObject, id)
    end)
end
-----------------------------------
----选中物品提交
-----------------------------------
function LuaGuildLeagueCrossTrainSubview:InitSelectedItem(itemId)
    self.m_CurItemId = itemId
    local item = CItemMgr.Inst:GetById(itemId)
    if item then
        local trainItems = LuaGuildLeagueCrossMgr:GetTrainItems(self.m_CurTrainType)
        local trainInfo = trainItems[item.TemplateId]
        self.m_CurrentItemIcon:LoadMaterial(item.Icon)
        self.m_CurrentItemQualitySprite.spriteName = item.IsItem and CUICommonDef.GetItemCellBorder(Item_Item.GetData(item.TemplateId), item, false) or CUICommonDef.GetItemCellBorder(item.Equip.QualityType)
        self.m_CurrentItemBindSprite.spriteName = item.BindOrEquipCornerMark
        self.m_CurrentItemNameLabel.text = item.ColoredName
        self.m_IncreaseAndDecreaseButton:SetMinMax(1, item.IsItem and item.Item.Count or 1, 1)
        self.m_IncreaseAndDecreaseButton:SetValue(1, false)
        self.m_CurrentItemProcessValueLabel.text = "+"..tostring(trainInfo.TrainValue * self.m_IncreaseAndDecreaseButton:GetValue())
        local maxScore =  math.min(self:GetLeftDailyScore(), trainInfo.TrainScore * self.m_IncreaseAndDecreaseButton:GetValue())
        self.m_CurrentItemScoreValueLabel.text = "+"..tostring(maxScore)
        self.m_SubmitButton.Enabled = true
    else
        self.m_CurrentItemIcon:Clear()
        self.m_CurrentItemQualitySprite.spriteName = ""
        self.m_CurrentItemBindSprite.spriteName = ""
        self.m_CurrentItemNameLabel.text = ""
        self.m_CurrentItemProcessValueLabel.text = ""
        self.m_CurrentItemScoreValueLabel.text = ""
        self.m_IncreaseAndDecreaseButton:SetMinMax(0,0,1)
        self.m_SubmitButton.Enabled = false
    end
end

function LuaGuildLeagueCrossTrainSubview:GetLeftDailyScore()
    local info = LuaGuildLeagueCrossMgr.m_TrainInfo
    local data = GuildLeagueCross_Setting.GetData()
    return data.TrainDailyMaxScore - info.dailyTrainScore
end

function LuaGuildLeagueCrossTrainSubview:OnNumChanged(value)
    local item = CItemMgr.Inst:GetById(self.m_CurItemId)
    if item then
        local trainItems = LuaGuildLeagueCrossMgr:GetTrainItems(self.m_CurTrainType)
        local trainInfo = trainItems[item.TemplateId]
        self.m_CurrentItemProcessValueLabel.text = "+"..tostring(trainInfo.TrainValue * self.m_IncreaseAndDecreaseButton:GetValue())

        local maxScore =  math.min(self:GetLeftDailyScore(), trainInfo.TrainScore * self.m_IncreaseAndDecreaseButton:GetValue())
        self.m_CurrentItemScoreValueLabel.text = "+"..tostring(maxScore)
    end
end

function LuaGuildLeagueCrossTrainSubview:OnSubmitButtonClick()
    local item, place, pos = CItemMgr.Inst:GetItemFromPackage(self.m_CurItemId)
    if not item then return end
    LuaGuildLeagueCrossMgr:SubmitItemToGuildLeagueTrain(self.m_CurTrainType, self.m_CurItemId, EnumToInt(place), pos, self.m_IncreaseAndDecreaseButton:GetValue(), item.TemplateId)
end

function LuaGuildLeagueCrossTrainSubview:OnInfoButtonClick()
    g_MessageMgr:ShowMessage("GLC_GUILD_LEAGUE_TRAIN_SUBMIT_DESC")
end

