-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CLeavingStatusCtrl = import "L10.UI.CLeavingStatusCtrl"
local CStatusMgr = import "L10.Game.CStatusMgr"
local EPropStatus = import "L10.Game.EPropStatus"
local LocalString = import "LocalString"
CLeavingStatusCtrl.m_OnMainPlayerLeavingStatusChanged_CS2LuaHook = function (this) 
    local isLeaving = false
    if CClientMainPlayer.Inst ~= nil then
        local manualLeaving = CStatusMgr.Inst:GetStatus(CClientMainPlayer.Inst, EPropStatus.GetId("Leaving")) == 1
        local tempLeaving = CStatusMgr.Inst:GetStatus(CClientMainPlayer.Inst, EPropStatus.GetId("TempLeaving")) == 1

        isLeaving = manualLeaving or tempLeaving
    end
    local default
    if isLeaving then
        default = this.LeaveSpriteName
    else
        default = this.OnlineSpriteName
    end
    this.statusIcon.spriteName = default
    local extern
    if isLeaving then
        extern = LocalString.GetString("离开")
    else
        extern = LocalString.GetString("在线")
    end
    this.statusText.text = extern
    this.statusText.color = isLeaving and this.leaveColor or this.onlineColor
end
