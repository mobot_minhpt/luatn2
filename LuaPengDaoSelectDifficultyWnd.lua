local QnIncreaseAndDecreaseButton = import "L10.UI.QnIncreaseAndDecreaseButton"
local QnNumberInput=import "L10.UI.QnNumberInput"
local UITable = import "UITable"
local UILabel = import "UILabel"

LuaPengDaoSelectDifficultyWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaPengDaoSelectDifficultyWnd, "LevelLabel", "LevelLabel", UILabel)
RegistChildComponent(LuaPengDaoSelectDifficultyWnd, "StartGameButton", "StartGameButton", GameObject)
RegistChildComponent(LuaPengDaoSelectDifficultyWnd, "RefreshButton", "RefreshButton", GameObject)
RegistChildComponent(LuaPengDaoSelectDifficultyWnd, "RecordEffectLabel", "RecordEffectLabel", UILabel)
RegistChildComponent(LuaPengDaoSelectDifficultyWnd, "RecordInfoLabel", "RecordInfoLabel", UILabel)
RegistChildComponent(LuaPengDaoSelectDifficultyWnd, "ScoreAddLabel", "ScoreAddLabel", UILabel)
RegistChildComponent(LuaPengDaoSelectDifficultyWnd, "Table1", "Table1", UITable)
RegistChildComponent(LuaPengDaoSelectDifficultyWnd, "Table2", "Table2", UITable)
RegistChildComponent(LuaPengDaoSelectDifficultyWnd, "Table3", "Table3", UITable)
RegistChildComponent(LuaPengDaoSelectDifficultyWnd, "MonsterInfoLabelTemplate", "MonsterInfoLabelTemplate", GameObject)
RegistChildComponent(LuaPengDaoSelectDifficultyWnd, "QnIncreseAndDecreaseButton1", "QnIncreseAndDecreaseButton1", QnIncreaseAndDecreaseButton)
RegistChildComponent(LuaPengDaoSelectDifficultyWnd, "QnIncreseAndDecreaseButton2", "QnIncreseAndDecreaseButton2", QnIncreaseAndDecreaseButton)
RegistChildComponent(LuaPengDaoSelectDifficultyWnd, "QnIncreseAndDecreaseButton3", "QnIncreseAndDecreaseButton3", QnIncreaseAndDecreaseButton)
RegistChildComponent(LuaPengDaoSelectDifficultyWnd, "TopLabelWidget", "TopLabelWidget", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaPengDaoSelectDifficultyWnd,"m_SelectDifficulty")
RegistClassMember(LuaPengDaoSelectDifficultyWnd,"m_DifficultyId2MonsterInfoLabelMap")

function LuaPengDaoSelectDifficultyWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.StartGameButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnStartGameButtonClick()
	end)

	UIEventListener.Get(self.RefreshButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRefreshButtonClick()
	end)

    --@endregion EventBind end

	if CommonDefs.IS_VN_CLIENT then
		self.QnIncreseAndDecreaseButton1.transform:Find("Label"):GetComponent(typeof(UILabel)).spacingX = 0
		self.QnIncreseAndDecreaseButton2.transform:Find("Label (1)"):GetComponent(typeof(UILabel)).spacingX = 0
		self.transform:Find("Background/CenterView/3/Label (1)"):GetComponent(typeof(UILabel)).spacingX = 0
	end
end

function LuaPengDaoSelectDifficultyWnd:Init()
	self.m_SelectDifficulty = {1, 1, 1}
	self.LevelLabel.text = SafeStringFormat3(LocalString.GetString("难度选择(当前最高等级%d)"),self:GetMaxDifficulty())
	self:InitRecord()
	self:InitMonsterInfoLabel()
	self:InitQnIncreseAndDecreaseButton(1,1,1)
	self:UpdateScoreAddLabel()
	self:OnRefreshButtonClick()
end

function LuaPengDaoSelectDifficultyWnd:InitQnIncreseAndDecreaseButton(val1, val2, val3)
	local arr = {self.QnIncreseAndDecreaseButton1,self.QnIncreseAndDecreaseButton2,self.QnIncreseAndDecreaseButton3}
	local valArr = {val1, val2, val3}
	for i, btn in pairs(arr) do
		local j = i
		btn.onValueChanged = DelegateFactory.Action_int(function(v)
			self:OnDifficultyChanged(j, v)
		end)
		btn:InitValueList(1, self:GetMaxDifficulty(),nil)
		btn:SetValue(valArr[i])
		local numInput = btn.transform:Find("QnNumberInput"):GetComponent(typeof(QnNumberInput))
		numInput.OnMaxValueButtonClick = DelegateFactory.Action(function () 
            btn:SetValue(self:GetMaxDifficulty())
        end)
		numInput.OnKeyboardClosed = DelegateFactory.Action(function () 
			self:OnNumInputValueChanged(numInput, btn, numInput.Text)
		end)
		numInput.OnValueChanged = DelegateFactory.Action_string(function(v)
			self:OnNumInputValueChanged(numInput, btn, v)
		end)
	end
end

function LuaPengDaoSelectDifficultyWnd:OnNumInputValueChanged(numInput, btn, v)
	local default, value = System.UInt16.TryParse(v)
	local maxDifficulty = self:GetMaxDifficulty()
	if default and value >= 1 and value <= maxDifficulty then
		local newVal = math.min(maxDifficulty,math.max(1,value))
		numInput:ForceSetText(newVal)
		btn:SetValue(newVal)
	else
		local newVal = cs_string.IsNullOrEmpty(v) and 1 or maxDifficulty
		btn:SetValue(newVal)
		numInput:ForceSetText(newVal)
	end
end

function LuaPengDaoSelectDifficultyWnd:GetMaxDifficulty()
	local hasInfo = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo
	return (hasInfo and CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo.MaxDifficulty > 0) and CClientMainPlayer.Inst.PlayProp.PengDaoFuYaoInfo.MaxDifficulty or PengDaoFuYao_Setting.GetData().PengDaoFuYaoInitialMaxDifficultyLv
end

function LuaPengDaoSelectDifficultyWnd:InitMonsterInfoLabel()
	self.m_DifficultyId2MonsterInfoLabelMap = {}
	self.MonsterInfoLabelTemplate.gameObject:SetActive(false)
	local arr = {self.Table1, self.Table2, self.Table3}
	for i, mtable in pairs(arr) do
		Extensions.RemoveAllChildren(mtable.transform)
	end
	PengDaoFuYao_Difficulty.Foreach(function (k, v)
		local mtable = arr[v.DifficultyType]
		local obj = NGUITools.AddChild(mtable.gameObject, self.MonsterInfoLabelTemplate.gameObject)
		obj:SetActive(true)
		local titleLabel = obj:GetComponent(typeof(UILabel))
		local infoLabel = obj.transform:Find("ValLabel"):GetComponent(typeof(UILabel))
		infoLabel.text = ""
		titleLabel.text = v.Description
		self.m_DifficultyId2MonsterInfoLabelMap[k] = infoLabel
	end)
	for i, mtable in pairs(arr) do
		mtable:Reposition()
	end
	self:RefreshMonsterInfoLabel()
end

function LuaPengDaoSelectDifficultyWnd:RefreshMonsterInfoLabel()
	for id, label in pairs(self.m_DifficultyId2MonsterInfoLabelMap) do
		local data = PengDaoFuYao_Difficulty.GetData(id)
		local formula = AllFormulas.Action_Formula[data.FormulaId] and AllFormulas.Action_Formula[data.FormulaId].Formula or nil
		local difficulty = self.m_SelectDifficulty[data.DifficultyType]
		if formula then
			local v = formula(nil, nil, {difficulty})
			label.text = data.IsPercent == 0 and SafeStringFormat3("+%s",v) or SafeStringFormat3("+%d%%", v * 100)
		end
	end
end

function LuaPengDaoSelectDifficultyWnd:UpdateScoreAddLabel()
	local formulaId = PengDaoFuYao_Setting.GetData().PengDaoFuYaoDifficultyScoreFormulaId
	local formula = AllFormulas.Action_Formula[formulaId] and AllFormulas.Action_Formula[formulaId].Formula or nil
	if formula then
		local v = formula(nil, nil, {self.m_SelectDifficulty[1],self.m_SelectDifficulty[2],self.m_SelectDifficulty[3]})
		self.ScoreAddLabel.text = SafeStringFormat3(LocalString.GetString("当前难度积分加成%d%%"), v * 100)
	end
end

function LuaPengDaoSelectDifficultyWnd:InitRecord()
	self.RefreshButton.gameObject:SetActive(false)
	self.RecordEffectLabel.text = ""
	self.RecordInfoLabel.text = ""
end

function LuaPengDaoSelectDifficultyWnd:RefreshRecord(selfData)
	if selfData then
		local a, b, c = selfData.Difficulty1,selfData.Difficulty2,selfData.Difficulty3
		local formulaId = PengDaoFuYao_Setting.GetData().PengDaoFuYaoDifficultyScoreFormulaId
		local formula = AllFormulas.Action_Formula[formulaId] and AllFormulas.Action_Formula[formulaId].Formula or nil
		if a == nil or b == nil or c == nil then
			return
		end
		self:InitQnIncreseAndDecreaseButton(a, b, c)
		self.RefreshButton.gameObject:SetActive(true)
		if formula then
			local v = formula(nil, nil, {a, b, c})
			self.RecordEffectLabel.text = SafeStringFormat3(LocalString.GetString("个人记录对应难度加成%d%%"), v * 100)
		end
		self.RecordInfoLabel.text = SafeStringFormat3(LocalString.GetString("生存%d级 攻击%d级 腐蚀%d级"),a, b, c)
	end
end

function LuaPengDaoSelectDifficultyWnd:OnEnable()
	g_ScriptEvent:AddListener("SeasonRank_QueryRankResult", self, "OnRankData")
end

function LuaPengDaoSelectDifficultyWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SeasonRank_QueryRankResult", self, "OnRankData")
end

function LuaPengDaoSelectDifficultyWnd:OnRankData(rankType, seasonId, selfDataU, rankDataU)
	local selfData = g_MessagePack.unpack(selfDataU)
    if selfData then
		self:RefreshRecord(selfData)
    end
end

--@region UIEvent

function LuaPengDaoSelectDifficultyWnd:OnStartGameButtonClick()
	Gac2Gas.PDFY_SelectDifficulty(self.m_SelectDifficulty[1],self.m_SelectDifficulty[2],self.m_SelectDifficulty[3])
end

function LuaPengDaoSelectDifficultyWnd:OnRefreshButtonClick()
	if CClientMainPlayer.Inst then
		Gac2Gas.SeasonRank_QueryRank(2200 + EnumToInt(CClientMainPlayer.Inst.Class), LuaPengDaoMgr.m_SeasonId)
	end
end

function LuaPengDaoSelectDifficultyWnd:OnDifficultyChanged(type, v)
	self.m_SelectDifficulty[type] = v
	self:RefreshRecord()
	self:RefreshMonsterInfoLabel()
	self:UpdateScoreAddLabel()
end
--@endregion UIEvent

function LuaPengDaoSelectDifficultyWnd:GetGuideGo( methodName )
	if methodName == "GetTopLabelWidget" then
		return self.TopLabelWidget.gameObject
	end
	return nil
end