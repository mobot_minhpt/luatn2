local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CUITexture = import "L10.UI.CUITexture"
local Screen = import "UnityEngine.Screen"
local Quaternion = import "UnityEngine.Quaternion"

LuaNPCShopModelPreviewWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaNPCShopModelPreviewWnd, "Previewer", CUITexture)
RegistChildComponent(LuaNPCShopModelPreviewWnd, "TitleLabel", UILabel)
RegistChildComponent(LuaNPCShopModelPreviewWnd, "RightBtn", GameObject)
RegistChildComponent(LuaNPCShopModelPreviewWnd, "LeftBtn", GameObject)
RegistChildComponent(LuaNPCShopModelPreviewWnd, "DescLabel", UILabel)

--@endregion RegistChildComponent end

function LuaNPCShopModelPreviewWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
	UIEventListener.Get(self.LeftBtn).onPress = DelegateFactory.BoolDelegate(function (go, isPressed)
        self.m_LeftPressed = isPressed
    end)
    UIEventListener.Get(self.RightBtn).onPress = DelegateFactory.BoolDelegate(function (go, isPressed)
        self.m_RightPressed = isPressed
    end)
	self.Rotation = 180
	self.m_StartPressTime = 0
    self.m_DeltaTime = 0.1
	self.m_DeltaRot = -10
end

function LuaNPCShopModelPreviewWnd:Init()
	self.m_IdentifierStr = "__NPCShopModel__"
	self.m_ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        self:LoadModel(ro)
        UIEventListener.Get(self.Previewer.gameObject).onDrag = DelegateFactory.VectorDelegate(function(go, delta)
            self:OnModelDrag(delta)
        end)
    end)
	self.Previewer.mainTexture = CUIManager.CreateModelTexture(self.m_IdentifierStr, self.m_ModelTextureLoader, self.Rotation, 0, -0.67, 4.66, false, true, 0.4, true, false)
end
function LuaNPCShopModelPreviewWnd:Update()
    if self.m_LeftPressed and Time.realtimeSinceStartup - self.m_StartPressTime > self.m_DeltaTime then
        self.Rotation = self.Rotation + self.m_DeltaRot
        self.m_StartPressTime = Time.realtimeSinceStartup
        CUIManager.SetModelRotation(self.m_IdentifierStr, self.Rotation)
    elseif self.m_RightPressed and Time.realtimeSinceStartup - self.m_StartPressTime > self.m_DeltaTime then
        self.Rotation = self.Rotation - self.m_DeltaRot
        self.m_StartPressTime = Time.realtimeSinceStartup
        CUIManager.SetModelRotation(self.m_IdentifierStr, self.Rotation)
    end
end
function LuaNPCShopModelPreviewWnd:LoadModel(ro)
    local fakeAppearance = CClientMainPlayer.Inst and CClientMainPlayer.Inst.AppearanceProp or nil
    if fakeAppearance == nil then return end
    if CLuaNPCShopInfoMgr.m_ModelSet.ItemData then
        -- 信息
        self.TitleLabel.text = SafeStringFormat3(LocalString.GetString("%s（预览）"), CLuaNPCShopInfoMgr.m_ModelSet.ItemData.Name)
        self.DescLabel.gameObject:SetActive(false)
        -- 模型
        if CLuaNPCShopInfoMgr.m_ModelSet.ZuoQi then
            local zuoqiData = ZuoQi_ZuoQi.GetData(CLuaNPCShopInfoMgr.m_ModelSet.ZuoQi)
            CClientMainPlayer.LoadResource(ro, fakeAppearance, true, 1.0, CLuaNPCShopInfoMgr.m_ModelSet.ZuoQi, true, 0, false, 0, false, nil, false)
            ro.VehicleRO:AddLoadAllFinishedListener(
                DelegateFactory.Action_CRenderObject(
                    function(_ro)
                        _ro.transform.localRotation = Quaternion.Euler(zuoqiData.ShopMallMaxPreviewRX, zuoqiData.ShopMallMaxPreviewRY, 0)
                    end
                )
            )
        end
    end
end
function LuaNPCShopModelPreviewWnd:OnModelDrag(delta)
    self.Rotation = self.Rotation - delta.x/Screen.width*360
    CUIManager.SetModelRotation(self.m_IdentifierStr, self.Rotation)
end
--@region UIEvent

--@endregion UIEvent
function LuaNPCShopModelPreviewWnd:OnDestroy()
    CUIManager.DestroyModelTexture(self.m_IdentifierStr)
    CLuaNPCShopInfoMgr.m_ModelSet = {}
end
