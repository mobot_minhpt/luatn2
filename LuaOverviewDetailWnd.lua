require("common/common_include")
local GameObject  = import "UnityEngine.GameObject"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local Vector3 = import "UnityEngine.Vector3"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local CButton = import "L10.UI.CButton"
--local Gac2Gas = import "L10.Game.Gac2Gas"
local UIProgressBar = import "UIProgressBar"
local CUIGameObjectPool = import "L10.UI.CUIGameObjectPool"
local UIGrid = import "UIGrid"
local Time=import "UnityEngine.Time"
local Item_Item = import "L10.Game.Item_Item"
local CUITexture = import "L10.UI.CUITexture"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Object = import "UnityEngine.Object"


CLuaOverviewDetailWnd=class()

RegistChildComponent(CLuaOverviewDetailWnd, "TimeLabel", UILabel)
RegistChildComponent(CLuaOverviewDetailWnd, "ProgressBar", UIProgressBar)
RegistChildComponent(CLuaOverviewDetailWnd, "Background", UISprite)
RegistChildComponent(CLuaOverviewDetailWnd, "Pool", CUIGameObjectPool)
RegistChildComponent(CLuaOverviewDetailWnd, "detailBtn", CButton)
RegistChildComponent(CLuaOverviewDetailWnd, "Table", UIGrid)
RegistChildComponent(CLuaOverviewDetailWnd, "ScrollView", CUIRestrictScrollView)
RegistChildComponent(CLuaOverviewDetailWnd, "Detail", GameObject)
RegistChildComponent(CLuaOverviewDetailWnd, "header", GameObject)
RegistChildComponent(CLuaOverviewDetailWnd, "Gas1", GameObject)
RegistChildComponent(CLuaOverviewDetailWnd, "Gas2", GameObject)

RegistClassMember(CLuaOverviewDetailWnd,"beginTime")
RegistClassMember(CLuaOverviewDetailWnd,"m_endTime")
RegistClassMember(CLuaOverviewDetailWnd,"m_time")
RegistClassMember(CLuaOverviewDetailWnd,"m_day")
RegistClassMember(CLuaOverviewDetailWnd,"m_hour")
RegistClassMember(CLuaOverviewDetailWnd,"m_min")
RegistClassMember(CLuaOverviewDetailWnd,"m_second")
RegistClassMember(CLuaOverviewDetailWnd,"event")
RegistClassMember(CLuaOverviewDetailWnd,"m_serverName1")
RegistClassMember(CLuaOverviewDetailWnd,"m_serverName2")
RegistClassMember(CLuaOverviewDetailWnd,"m_HintDescLabel")
RegistClassMember(CLuaOverviewDetailWnd,"m_CountDwonSprite")


function CLuaOverviewDetailWnd:Awake()
	self.m_HintDescLabel = self.transform:Find("Header/Line2/HintDescLabel")
	self.m_CountDwonSprite = self.transform:Find("Header/Line2/CountDownSprite")

	self:DefaultInit()
	CommonDefs.AddOnClickListener(self.detailBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
		local arrowSprite=self.detailBtn.transform:Find("ArrowSprite").gameObject
		arrowSprite.transform.localRotation=Vector3(0,0,180)
		if not self.Detail.activeSelf then
			self.Detail:SetActive(true)
			self.header.transform.localPosition=Vector3(0,0,0)
			CommonDefs.GetComponent_GameObject_Type(arrowSprite, typeof(UISprite)).flip = UIBasicSprite.Flip.Vertically
			
		else
			self.Detail:SetActive(false)
			self.header.transform.localPosition=Vector3(0,330,0)
			CommonDefs.GetComponent_GameObject_Type(arrowSprite, typeof(UISprite)).flip = UIBasicSprite.Flip.Nothing
		
		end

	end), false)
end

function CLuaOverviewDetailWnd:Init()

end

function CLuaOverviewDetailWnd:Update()
	if not self.m_time  then 
		return
	end
	if self.m_endTime==nil  then 
		return
	end

	local delaytime = Time.realtimeSinceStartup-self.beginTime

	self.m_second = self.m_endTime.Second-self.m_time.Second-delaytime

	if self.m_second<0 then
		self.m_min=self.m_min-1
		self.m_second=self.m_second+60
		self.beginTime=self.beginTime+60
	end
	if self.m_min<0 then
		self.m_min=self.m_min+60
		self.m_hour=self.m_hour-1
	end
	if self.m_hour<0 then
		self.m_hour=self.m_hour+24
		self.m_day=self.m_day-1
	end	

	if self.m_day~=0 then
		self.TimeLabel.text=SafeStringFormat3( LocalString.GetString("%01d天   %02d:%02d:%02d"),self.m_day,self.m_hour,self.m_min,self.m_second)
	else
		self.TimeLabel.text=SafeStringFormat3( LocalString.GetString("         %02d:%02d:%02d"),self.m_hour,self.m_min,self.m_second)
	end

	if self.m_day<0 then
		self.TimeLabel.text=SafeStringFormat3( LocalString.GetString(""))
	elseif self.m_day>=6 then
		self.TimeLabel.text=LocalString.GetString("活动将于下周一开启")
	elseif CLuaMergeBattleMgr.m_bStart==false then
		self.TimeLabel.text=LocalString.GetString("活动将于下周一开启")
	end
end

function CLuaOverviewDetailWnd:InitData(beginTime, endTime,bStart, bEnd, winner, serverName1, serverName2, list1, list2, totalScore,force)
	self.m_serverName1=serverName1
	self.m_serverName2=serverName2
	self.m_time = CServerTimeMgr.Inst:GetZone8Time()
	local CountdownTime = CServerTimeMgr.ConvertTimeStampToZone8Time(endTime-CServerTimeMgr.Inst.timeStamp)
	
	self.m_endTime = CServerTimeMgr.ConvertTimeStampToZone8Time(endTime)
	if endTime-CServerTimeMgr.Inst.timeStamp<0 then
		self.m_day =  -1
		self.m_hour = 0
		self.m_min = 0
		self.m_second = 0
	else
		self.m_day =  CountdownTime.Day-1
		self.m_hour = CountdownTime.Hour-8
		self.m_min = CountdownTime.Minute
		self.m_second = CountdownTime.Second
	end
	local gas1Label = CommonDefs.GetComponent_Component_Type(self.Gas1.gameObject.transform:Find("gas1Label"), typeof(UILabel))
	local gas2Label = CommonDefs.GetComponent_Component_Type(self.Gas2.gameObject.transform:Find("gas2Label"), typeof(UILabel))
	local  gas1 = serverName1
	gas1Label.text=SafeStringFormat3( LocalString.GetString("%s"),gas1)
	local  gas2 = serverName2
	gas2Label.text=SafeStringFormat3( LocalString.GetString("%s"),gas2)

	


	
	local pointsLabel = CommonDefs.GetComponent_Component_Type(self.header.gameObject.transform:Find("PointsLabel"), typeof(UILabel))
	pointsLabel.text=totalScore

	

	local leftTotalScore = 0
	local rightTotalScore = 0

	if list1 and list2 then
		for i = 0, list1.Count - 1, 2 do
			
			local eventProgressBar1 = CommonDefs.GetComponent_Component_Type(self.event[list1[i]].gameObject.transform:Find("ProgressBar1"), typeof(UIProgressBar))
			local eventProgressBar2 = CommonDefs.GetComponent_Component_Type(self.event[list1[i]].gameObject.transform:Find("ProgressBar2"), typeof(UIProgressBar))
			local leftLabel = CommonDefs.GetComponent_Component_Type(self.event[list1[i]].gameObject.transform:Find("left"), typeof(UILabel))
			leftLabel.text=list1[i + 1]

			local rightLabel = CommonDefs.GetComponent_Component_Type(self.event[list1[i]].gameObject.transform:Find("right"), typeof(UILabel))
			rightLabel.text=list2[i + 1]
			if (list1[i + 1]+list2[i + 1])==0 then
				eventProgressBar1.value=0.5
				eventProgressBar2.value=0.5
			else
				eventProgressBar1.value=list1[i + 1]/(list1[i + 1]+list2[i + 1])
				eventProgressBar2.value=list1[i + 1]/(list1[i + 1]+list2[i + 1])
			end
			local lineSprite=self.event[list1[i]].gameObject.transform:Find("line").gameObject
			local background = CommonDefs.GetComponent_Component_Type(eventProgressBar1.gameObject.transform:Find("Background"), typeof(UISprite))
			local x = (eventProgressBar1.value-0.5)*(background.width+6)--+6 是为了避免line在一端时显示长度不一致
			local eventLabel = CommonDefs.GetComponent_Component_Type(self.event[list1[i]].gameObject.transform:Find("event"), typeof(UILabel))
			lineSprite.transform.localPosition=Vector3(eventLabel.transform.localPosition.x+x,lineSprite.transform.localPosition.y,lineSprite.transform.localPosition.z)

			leftTotalScore=leftTotalScore+list1[i + 1]
			rightTotalScore=rightTotalScore+list2[i + 1]
		end
	end

	if leftTotalScore+rightTotalScore==0 then
		self.ProgressBar.value=0.5
	else
		self.ProgressBar.value=leftTotalScore/(leftTotalScore+rightTotalScore)
	end
	local points1Label = CommonDefs.GetComponent_Component_Type(self.Gas1.gameObject.transform:Find("pointsLabel"), typeof(UILabel))
	points1Label.text=leftTotalScore
	local points2Label = CommonDefs.GetComponent_Component_Type(self.Gas2.gameObject.transform:Find("pointsLabel"), typeof(UILabel))
	points2Label.text=rightTotalScore

	if totalScore~=nil then
		self:AddItemToTable(totalScore)
	end

	local progressSprite=self.ProgressBar.transform:Find("sprite").gameObject
	local x = (self.ProgressBar.value-0.5)*self.Background.width
	progressSprite.transform.localPosition=Vector3(300+x,progressSprite.transform.localPosition.y,progressSprite.transform.localPosition.z)--300为中心位置参数，尺寸调整时需调整这里的参数

	if bEnd==true then
		local BattleProgress = self.ScrollView.transform:Find("BattleProgress").gameObject
		local leftServerSprite = CommonDefs.GetComponent_Component_Type(BattleProgress.transform:Find("victorySprite"), typeof(UISprite))--左边服务器胜负图标
		local rightServerSprite = CommonDefs.GetComponent_Component_Type(BattleProgress.transform:Find("defeatSprite"), typeof(UISprite))
		leftServerSprite.gameObject:SetActive(true)
		rightServerSprite.gameObject:SetActive(true)
		if winner==0 then
			leftServerSprite.spriteName="common_fight_result_win"
			rightServerSprite.spriteName="common_fight_result_lose"
		else
			rightServerSprite.spriteName="common_fight_result_win"
			leftServerSprite.spriteName="common_fight_result_lose"
		end
		self.m_CountDwonSprite.gameObject:SetActive(false)
		local pos = self.m_HintDescLabel.localPosition
		self.m_HintDescLabel.localPosition = Vector3(70,pos.y,pos.z)
	end
			

end

function CLuaOverviewDetailWnd:AddItemToTable(totalScore)
	self:ClearList()
	for i = 1, 6 do
		local ddata = MergeBattle_Reward.GetData(i)
		local achievement = self.Pool:GetFromPool(0)
	    achievement.transform.parent = self.Table.transform
	    achievement.transform.localScale = Vector3.one
	    achievement.gameObject:SetActive(true)
	    --local item = Item_Item.GetData(string.gmatch(ddata.Reward,  "%d+")())
	    local item = Item_Item.GetData(ddata.Reward)
	    local itemCell = CommonDefs.GetComponent_Component_Type(achievement.gameObject.transform:Find("ItemCell"), typeof(CUITexture))
	    local progress = CommonDefs.GetComponent_Component_Type(achievement.gameObject.transform:Find("Progress"), typeof(UIProgressBar))
	    local eventLabel = CommonDefs.GetComponent_Component_Type(achievement.gameObject.transform:Find("event"), typeof(UILabel))
	    eventLabel.text=System.String.Format(LocalString.GetString("合服大比拼中累计获得{0}分"), ddata.Score)
	    local slider = CommonDefs.GetComponent_Component_Type(itemCell.gameObject.transform:Find("Slider"), typeof(UISprite))
    	local bg = CommonDefs.GetComponent_Component_Type(itemCell.gameObject.transform:Find("Bg"), typeof(UISprite))
    	local sprite = CommonDefs.GetComponent_Component_Type(itemCell.gameObject.transform:Find("Sprite"), typeof(UISprite))--对勾图标
	    if ddata.Score<=totalScore then
	    	progress.value=1
	    	bg.gameObject:SetActive(true)
			sprite.gameObject:SetActive(true)
		else
			progress.value=totalScore/ddata.Score
			bg.gameObject:SetActive(false)
			sprite.gameObject:SetActive(false)
		end
	    itemCell:LoadMaterial(item.Icon)
	     if item ~= nil then
        	CommonDefs.AddOnClickListener(itemCell.gameObject, DelegateFactory.Action_GameObject(function (go)
        		CItemInfoMgr.ShowLinkItemTemplateInfo(ddata.Reward, true, nil, AlignType.Right,  0, 0, 0, 0)
        	end), false)
    	end
    	slider.spriteName=CUICommonDef.GetItemCellBorder(item, nil, false)
	end
		self.Table:Reposition()

	    self.ScrollView:SetDragAmount(0, 0, false)
	
end

function CLuaOverviewDetailWnd:OnEnable()
	
	g_ScriptEvent:AddListener("QueryMergeBattleOverviewResult", self, "InitData")
	self.beginTime=Time.realtimeSinceStartup+60

	local BattleProgress = self.ScrollView.transform:Find("BattleProgress").gameObject
	local leftServerSprite = CommonDefs.GetComponent_Component_Type(BattleProgress.transform:Find("victorySprite"), typeof(UISprite))--左边服务器胜负图标
	local rightServerSprite = CommonDefs.GetComponent_Component_Type(BattleProgress.transform:Find("defeatSprite"), typeof(UISprite))
	leftServerSprite.gameObject:SetActive(false)
	rightServerSprite.gameObject:SetActive(false)
	
	Gac2Gas.QueryMergeBattleOverview()
end

function CLuaOverviewDetailWnd:OnDisable()
	
	g_ScriptEvent:RemoveListener("QueryMergeBattleOverviewResult", self, "InitData")
end

function CLuaOverviewDetailWnd:ClearList()

	local childList = self.Table:GetChildList()
    do
        local i = 0
        while i < childList.Count do
            local trans = childList[i]
            trans.parent = nil
            Object.Destroy(trans.gameObject)
            i = i + 1
        end
    end
end

function CLuaOverviewDetailWnd:DefaultInit()
	self.event={}
	local BattleProgress = self.ScrollView.transform:Find("BattleProgress").gameObject
	local leftServerSprite = CommonDefs.GetComponent_Component_Type(BattleProgress.transform:Find("victorySprite"), typeof(UISprite))--左边服务器胜负图标
	local rightServerSprite = CommonDefs.GetComponent_Component_Type(BattleProgress.transform:Find("defeatSprite"), typeof(UISprite))
	leftServerSprite.gameObject:SetActive(false)
	rightServerSprite.gameObject:SetActive(false)
	self.TimeLabel.text=SafeStringFormat3( LocalString.GetString(""))
	self:AddItemToTable(0)
	local pointsLabel = CommonDefs.GetComponent_Component_Type(self.header.gameObject.transform:Find("PointsLabel"), typeof(UILabel))
	pointsLabel.text=0
	self.event[1] = self.Detail.gameObject.transform:Find("event1").gameObject.transform
	self.event[2] = self.Detail.gameObject.transform:Find("event2").gameObject.transform
	self.event[3] = self.Detail.gameObject.transform:Find("event3").gameObject.transform
	self.event[4] = self.Detail.gameObject.transform:Find("event4").gameObject.transform
	self.event[5] = self.Detail.gameObject.transform:Find("event5").gameObject.transform
	for i = 1,5 do
			
			local eventProgressBar1 = CommonDefs.GetComponent_Component_Type(self.event[i].gameObject.transform:Find("ProgressBar1"), typeof(UIProgressBar))
			local eventProgressBar2 = CommonDefs.GetComponent_Component_Type(self.event[i].gameObject.transform:Find("ProgressBar2"), typeof(UIProgressBar))
			local leftLabel = CommonDefs.GetComponent_Component_Type(self.event[i].gameObject.transform:Find("left"), typeof(UILabel))
			leftLabel.text=0
			local rightLabel = CommonDefs.GetComponent_Component_Type(self.event[i].gameObject.transform:Find("right"), typeof(UILabel))
			rightLabel.text=0
			eventProgressBar1.value=0.5
			eventProgressBar2.value=0.5
			local lineSprite=self.event[i].gameObject.transform:Find("line").gameObject
			local eventLabel = CommonDefs.GetComponent_Component_Type(self.event[i].gameObject.transform:Find("event"), typeof(UILabel))
			lineSprite.transform.localPosition=Vector3(eventLabel.transform.localPosition.x,lineSprite.transform.localPosition.y,lineSprite.transform.localPosition.z)
	end
	self.ProgressBar.value=0.5
	local progressSprite=self.ProgressBar.transform:Find("sprite").gameObject
	progressSprite.transform.localPosition=Vector3(progressSprite.transform.localPosition.x,progressSprite.transform.localPosition.y,progressSprite.transform.localPosition.z)

	local gas1Label = CommonDefs.GetComponent_Component_Type(self.Gas1.gameObject.transform:Find("gas1Label"), typeof(UILabel))
	local gas2Label = CommonDefs.GetComponent_Component_Type(self.Gas2.gameObject.transform:Find("gas2Label"), typeof(UILabel))
	gas1Label.text=""
	gas2Label.text=""
	local points1Label = CommonDefs.GetComponent_Component_Type(self.Gas1.gameObject.transform:Find("pointsLabel"), typeof(UILabel))
	points1Label.text=0
	local points2Label = CommonDefs.GetComponent_Component_Type(self.Gas2.gameObject.transform:Find("pointsLabel"), typeof(UILabel))
	points2Label.text=0
end
