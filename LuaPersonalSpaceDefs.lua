LuaPersonalSpaceDefs = {}

LuaPersonalSpaceDefs.PUSH = "gdc/qnm.do";
LuaPersonalSpaceDefs.GET_USER_PROFILE = "qnm/getprofile";
LuaPersonalSpaceDefs.GET_LOCATION = "qnm/getlocation";
LuaPersonalSpaceDefs.CHANGE_SIGN = "qnm/changesign";
LuaPersonalSpaceDefs.CHANGE_LOC = "qnm/changeloc";
LuaPersonalSpaceDefs.SET_PHOTO = "qnm/setphoto";
LuaPersonalSpaceDefs.SET_SIGNATUREVOICE = "/qnm/setsignaturevoice";
LuaPersonalSpaceDefs.GET_PHOTO_TOKEN = "qnm/nos/gettoken";
LuaPersonalSpaceDefs.SET_PRIVACY = "qnm/setprivacy";
LuaPersonalSpaceDefs.GET_MOMENTS = "qnm/getmoments";
LuaPersonalSpaceDefs.GET_MOMENT = "qnm/getmoment";
LuaPersonalSpaceDefs.ADD_MOMENT = "qnm/addmoment";
LuaPersonalSpaceDefs.ADD_MOMENT2 = "qnm/addmoment2";
LuaPersonalSpaceDefs.DEL_MOMENT = "qnm/delmoment";
LuaPersonalSpaceDefs.LIKE_MOMENT = "qnm/likemoment";
LuaPersonalSpaceDefs.ADD_COMMENT = "qnm/addcomment";
LuaPersonalSpaceDefs.DEL_COMMENT = "qnm/delcomment";
LuaPersonalSpaceDefs.GET_MESSAGES = "qnm/getmessages";
LuaPersonalSpaceDefs.ADD_MESSAGE = "qnm/addmessage";
LuaPersonalSpaceDefs.DEL_MESSAGE = "qnm/delmessage";
LuaPersonalSpaceDefs.ANSWER_MESSAGE = "qnm/answermessage";
LuaPersonalSpaceDefs.GET_EVENT = "qnm/getevents";
LuaPersonalSpaceDefs.GET_NEW_INFO = "qnm/getnewsnum";
LuaPersonalSpaceDefs.GET_UNREAD_INFO = "qnm/getinforms";
LuaPersonalSpaceDefs.GET_HIS_INFO = "qnm/getallinforms";
LuaPersonalSpaceDefs.GET_MOMENT_BYID = "qnm/getmomentbyid";
LuaPersonalSpaceDefs.SET_EXPRESSION = "qnm/setexpression";
LuaPersonalSpaceDefs.GET_HOT_MOMENTS = "qnm/gethotmoments";
LuaPersonalSpaceDefs.GET_ALL_HOT_MOMENTS = "qnm/getallhotmoments";
LuaPersonalSpaceDefs.ADD_PLAYER_LOCATION = "qnm/location/update";
LuaPersonalSpaceDefs.GET_CLOSE_PLAYER = "qnm/location/nearby/players";
LuaPersonalSpaceDefs.GET_CLOSE_MCDONALD_INFO = "qnm/summerlbs/get_nearby_mc";
LuaPersonalSpaceDefs.DELETE_PLAYER_LOCATION = "qnm/location/delete";
LuaPersonalSpaceDefs.SET_PLAYER_CAPACITY = "qnm/info/set_fighting_capacity";
LuaPersonalSpaceDefs.GET_PLAYER_CAPACITY = "qnm/info/get_fighting_capacity";
LuaPersonalSpaceDefs.FOLLOW_ADD = "qnm/follow/add";
LuaPersonalSpaceDefs.FOLLOW_CANCEL = "qnm/follow/cancel";
LuaPersonalSpaceDefs.GET_LOCATIONS = "qnm/info/get_locations";
LuaPersonalSpaceDefs.GET_COMMENTS = "qnm/comment/more";
LuaPersonalSpaceDefs.CHECK_ENABLE = "qnm/info/is_enable_island";
LuaPersonalSpaceDefs.SAVE_PIC = "qnm/photo/create";
LuaPersonalSpaceDefs.GET_PIC = "qnm/photo/get";
LuaPersonalSpaceDefs.FORWARD_MOMENT = "qnm/moment/forward";
LuaPersonalSpaceDefs.NEW_INFO_REDPOINT_ALERT = "qnm/notify/red_dot";
LuaPersonalSpaceDefs.CLEAN_ALL_INFO = "qnm/informs/read_all";
LuaPersonalSpaceDefs.Jingli_List = "qnm/gm/list";
LuaPersonalSpaceDefs.Jingli_LastStar = "qnm/gm/monthly_star";

LuaPersonalSpaceDefs.HotTalk_GetTopic = "qnm/topic/list"
LuaPersonalSpaceDefs.HotTalk_GetMoments = "qnm/topic/moment/list"

--------------------------------------------------------------------------------
LuaPersonalSpace_Name = {}

LuaPersonalSpace_Name.ROLE_INFO = "roleinfo";
LuaPersonalSpace_Name.SYNC_FRIEND = "syncfriend";
LuaPersonalSpace_Name.ADD_FRIEND = "addfriend";
LuaPersonalSpace_Name.DEL_FRIEND = "delfriend";
LuaPersonalSpace_Name.ROLE_EQUIP = "roleequip";
LuaPersonalSpace_Name.ROLE_LINGSHOU = "rolelingshou";

--------------------------------------------------------------------------------
LuaPersonalSpace_EventType = {}
LuaPersonalSpace_EventType.ePopularity = 1
LuaPersonalSpace_EventType.ePresent = 2
LuaPersonalSpace_EventType.eFlower = 3
LuaPersonalSpace_EventType.eAttention = 4
