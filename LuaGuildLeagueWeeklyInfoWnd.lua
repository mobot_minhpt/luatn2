local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local QnTableView=import "L10.UI.QnTableView"
local Profession = import "L10.Game.Profession"
local QnRadioBox=import "L10.UI.QnRadioBox"
local CSortButton = import "L10.UI.CSortButton"


CLuaGuildLeagueWeeklyInfoWnd=class()
RegistClassMember(CLuaGuildLeagueWeeklyInfoWnd,"m_TableView")
RegistClassMember(CLuaGuildLeagueWeeklyInfoWnd,"m_MainPlayerGuildInfo")
RegistClassMember(CLuaGuildLeagueWeeklyInfoWnd,"m_LeagueInfoList")
RegistClassMember(CLuaGuildLeagueWeeklyInfoWnd,"m_SortRadioBox")
RegistClassMember(CLuaGuildLeagueWeeklyInfoWnd,"m_RadioBox")

RegistClassMember(CLuaGuildLeagueWeeklyInfoWnd,"m_MyId")

RegistClassMember(CLuaGuildLeagueWeeklyInfoWnd,"m_SortFlags")

-- RegistClassMember(CLuaGuildLeagueWeeklyInfoWnd,"sortRadioBox")

function CLuaGuildLeagueWeeklyInfoWnd:Init()
    self.m_SortFlags={-1,-1,-1,-1,-1,-1,-1,-1,-1}


    self.m_MyId = CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Id or 0


    self.m_SortRadioBox = self.transform:Find("Anchor/TableView/Header"):GetComponent(typeof(QnRadioBox))
    self.m_SortRadioBox.OnSelect =DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnSortRadioBoxSelect(btn,index)
    end)
    self.m_TableView=FindChild(self.transform,"TableView"):GetComponent(typeof(QnTableView))

    local function initItem(item,row)
        if row % 2 == 0 then
            item:SetBackgroundTexture(g_sprites.EvenBgSprite)
        else
            item:SetBackgroundTexture(g_sprites.OddBgSpirite)
        end

        if row==0 then
            --自己
            self:InitItem(item.transform,self.m_MainPlayerGuildInfo,row)
        else
            self:InitItem(item.transform,self.m_LeagueInfoList[row],row)
        end
    end
    self.m_TableView.m_DataSource = DefaultTableViewDataSource.CreateByCount(1,initItem)

    self.m_RadioBox=FindChild(self.transform,"RadioBox"):GetComponent(typeof(QnRadioBox))
    self.m_RadioBox.OnSelect=DelegateFactory.Action_QnButton_int(function(btn,index)
        self.m_TableView.m_DataSource.count=0
        if index==0 then
            if not CLuaGuildMgr.m_LeagueInfo1 then
                self.m_LeagueInfoList=nil
                Gac2Gas.RequestGetGuildInfo("GuildLeagueFirst")
            else
                self:InitData(CLuaGuildMgr.m_LeagueInfo1)
            end
        elseif index==1 then
            if not CLuaGuildMgr.m_LeagueInfo2 then
                self.m_LeagueInfoList=nil
                Gac2Gas.RequestGetGuildInfo("GuildLeagueSecond")
            else
                self:InitData(CLuaGuildMgr.m_LeagueInfo2)

            end
        elseif index==2 then
            if not CLuaGuildMgr.m_LeagueInfo3 then
                self.m_LeagueInfoList=nil
                Gac2Gas.RequestGetGuildInfo("GuildLeagueThird")
            else
                self:InitData(CLuaGuildMgr.m_LeagueInfo3)
            end
        end

        self.m_TableView.m_DataSource.count=self.m_LeagueInfoList and #self.m_LeagueInfoList+1 or 1
        self.m_TableView:ReloadData(true, false)
    end)
    self.m_RadioBox:ChangeTo(0,true)
end

function CLuaGuildLeagueWeeklyInfoWnd:OnSortRadioBoxSelect( btn, index) 
    local sortButton = TypeAs(btn, typeof(CSortButton))

    self.m_SortFlags[index+1] = - self.m_SortFlags[index+1]
    for i,v in ipairs(self.m_SortFlags) do
        if i~=index+1 then
            self.m_SortFlags[i] = -1
        end
    end

    sortButton:SetSortTipStatus(self.m_SortFlags[index+1] < 0)
    self:Sort(self.m_LeagueInfoList, index)
    self.m_TableView:ReloadData(true, false)
end

function CLuaGuildLeagueWeeklyInfoWnd:InitData(t)
    local myId = CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Id or 0
    self.m_LeagueInfoList={}
    self.m_MainPlayerGuildInfo=nil
    for k,v in pairs(t) do
        if k==myId then
            self.m_MainPlayerGuildInfo=v
        else
            table.insert( self.m_LeagueInfoList,v )
        end
    end
end

function CLuaGuildLeagueWeeklyInfoWnd:InitItem(transform,info,row)
    -- if not info then return end

    local nameLabel=transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local levelLabel=transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
    local classSprite=transform:Find("ClassSprite"):GetComponent(typeof(UISprite))
    local killCountLabel=transform:Find("KillCountLabel"):GetComponent(typeof(UILabel))
    local dpsLabel=transform:Find("DpsLabel"):GetComponent(typeof(UILabel))
    local healLabel=transform:Find("HealLabel"):GetComponent(typeof(UILabel))
    local controlCountLabel=transform:Find("ControlCountLabel"):GetComponent(typeof(UILabel))
    local underDamageLabel=transform:Find("UnderDamageLabel"):GetComponent(typeof(UILabel))
    local repairCountLabel=transform:Find("RepairCountLabel"):GetComponent(typeof(UILabel))
    local commentLevelLabel=transform:Find("CommentLevelLabel"):GetComponent(typeof(UILabel))

    if info then
        nameLabel.text = info.Name
        classSprite.spriteName = Profession.GetIconByNumber(info.Class)
        CUICommonDef.SetActive(classSprite.gameObject, info.IsOnline > 0, true)

        levelLabel.text = info.XianFanStatus > 0 and SafeStringFormat3( "[c][ff7900]Lv.%d[-][/c]",info.Level ) or SafeStringFormat3( "Lv.%d",info.Level )
        killCountLabel.text=tostring(math.floor(info.KillCount))
        dpsLabel.text=tostring(math.floor(info.Dps))
        healLabel.text=tostring(math.floor(info.Heal))
        controlCountLabel.text=tostring(math.floor(info.ControlCount))
        underDamageLabel.text=tostring(math.floor(info.UnderDamage))
        repairCountLabel.text=tostring(math.floor(info.RepairCount))

        CLuaGuildMgr.SetLeagueCommentLevel(commentLevelLabel,info.CommentLevel)
        -- local green=Color(0,1,0)
        -- local normal=Color(17/256,44/256,76/256)
        -- if info.PlayerId==self.m_MyId then
        --     nameLabel.color=green
        --     levelLabel.color=green
        --     killCountLabel.color=green
        --     dpsLabel.color=green
        --     healLabel.color=green
        --     controlCountLabel.color=green
        --     underDamageLabel.color=green
        --     repairCountLabel.color=green
        -- else
        --     nameLabel.color=normal
        --     levelLabel.color=normal
        --     killCountLabel.color=normal
        --     dpsLabel.color=normal
        --     healLabel.color=normal
        --     controlCountLabel.color=normal
        --     underDamageLabel.color=normal
        --     repairCountLabel.color=normal
        -- end
    else
        nameLabel.text = nil
        classSprite.spriteName =nil
        levelLabel.text=nil
        killCountLabel.text=nil
        dpsLabel.text=nil
        healLabel.text=nil
        controlCountLabel.text=nil
        underDamageLabel.text=nil
        repairCountLabel.text=nil
    end

end

function CLuaGuildLeagueWeeklyInfoWnd:OnEnable()
    g_ScriptEvent:AddListener("SendGuildInfo_GuildLeagueInfo", self, "OnSendGuildInfo_GuildLeagueInfo")
end
function CLuaGuildLeagueWeeklyInfoWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendGuildInfo_GuildLeagueInfo", self, "OnSendGuildInfo_GuildLeagueInfo")

end

function CLuaGuildLeagueWeeklyInfoWnd:OnSendGuildInfo_GuildLeagueInfo(p)
    local radioIndex=self.m_RadioBox.CurrentIndex
    self.m_LeagueInfoList=nil
    if p==1 and radioIndex==0 then
        self:InitData(CLuaGuildMgr.m_LeagueInfo1)
    elseif p==2 and radioIndex==1 then
        self:InitData(CLuaGuildMgr.m_LeagueInfo2)
    elseif p==3 and radioIndex==2 then
        self:InitData(CLuaGuildMgr.m_LeagueInfo3)
    end

    self.m_TableView.m_DataSource.count=self.m_LeagueInfoList and #self.m_LeagueInfoList+1 or 1
    self.m_TableView:ReloadData(true, false)
end


function CLuaGuildLeagueWeeklyInfoWnd:OnDestroy()
    CLuaGuildMgr.m_LeagueInfo1=nil
    CLuaGuildMgr.m_LeagueInfo2=nil
    CLuaGuildMgr.m_LeagueInfo3=nil
end


function CLuaGuildLeagueWeeklyInfoWnd:Sort(t,sortIndex)
    local function defaultSort(a,b)
        if a.IsOnline>0 and b.IsOnline ==0 then
            return true
        elseif a.IsOnline==0 and b.IsOnline >0 then
            return false
        else
            return a.PlayerId<b.PlayerId
        end
    end
    local flag=self.m_SortFlags[sortIndex+1]
    if sortIndex==0 then--cls name
        table.sort( t, function(a,b)
            local ret=flag
            if a.Class<b.Class then
                ret = -flag
            elseif a.Class>b.Class then
                ret=flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif sortIndex==1 then--level
        table.sort( t, function(a,b)
            local ret=flag
            if a.Level<b.Level then
                ret = flag
            elseif a.Level>b.Level then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )

    elseif sortIndex==2 then--kill count
        table.sort( t, function(a,b)
            local ret=flag
            if a.KillCount<b.KillCount then
                ret = flag
            elseif a.KillCount>b.KillCount then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif sortIndex==3 then--dps
        table.sort( t, function(a,b)
            local ret=flag
            if a.Dps<b.Dps then
                ret = flag
            elseif a.Dps>b.Dps then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif sortIndex==4 then--heal
        table.sort( t, function(a,b)
            local ret=flag
            if a.Heal<b.Heal then
                ret = flag
            elseif a.Heal>b.Heal then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif sortIndex==5 then--contorl count
        table.sort( t, function(a,b)
            local ret=flag
            if a.ControlCount<b.ControlCount then
                ret = flag
            elseif a.ControlCount>b.ControlCount then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif sortIndex==6 then--under damage
        table.sort( t, function(a,b)
            local ret=flag
            if a.UnderDamage<b.UnderDamage then
                ret = flag
            elseif a.UnderDamage>b.UnderDamage then
                ret = -flag
            else
                return defaultSort(a,b)
            end
            return ret<0
        end )
    elseif sortIndex==7 then--repair count
        table.sort( t, function(a,b)
            local ret=flag
            if a.RepairCount<b.RepairCount then
                ret = flag
            elseif a.RepairCount>b.RepairCount then
                ret = -flag
            else
                return defaultSort(a,b)
            end
        end )
    elseif sortIndex==8 then--comment level
        table.sort( t, function(a,b)
            local ret=flag
            if a.CommentLevel<b.CommentLevel then
                ret = flag
            elseif a.CommentLevel>b.CommentLevel then
                ret = -flag
            else
                return defaultSort(a,b)
            end
        end )
    end
end
return CLuaGuildLeagueWeeklyInfoWnd