
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local DelegateFactory  = import "DelegateFactory"
local CIMMgr = import "L10.Game.CIMMgr"
local UIEventListener = import "UIEventListener"

LuaQuweiLabaChooseWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaQuweiLabaChooseWnd, "PlayerNameLabel", "PlayerNameLabel", UILabel)
RegistChildComponent(LuaQuweiLabaChooseWnd, "ChooseBtn", "ChooseBtn", GameObject)
RegistChildComponent(LuaQuweiLabaChooseWnd, "CommitBtn", "CommitBtn", GameObject)
RegistChildComponent(LuaQuweiLabaChooseWnd, "RuleDescBtn", "RuleDescBtn", GameObject)
RegistChildComponent(LuaQuweiLabaChooseWnd, "TypeRoot", "TypeRoot", GameObject)
RegistChildComponent(LuaQuweiLabaChooseWnd, "Desc", "Desc", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaQuweiLabaChooseWnd,"m_TypeBtnList")
RegistClassMember(LuaQuweiLabaChooseWnd,"m_PlayerName")
RegistClassMember(LuaQuweiLabaChooseWnd,"m_PlayerId")

function LuaQuweiLabaChooseWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ChooseBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChooseBtnClick()
	end)


	
	UIEventListener.Get(self.CommitBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCommitBtnClick()
	end)


	
	UIEventListener.Get(self.RuleDescBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleDescBtnClick()
	end)


    --@endregion EventBind end
end

function LuaQuweiLabaChooseWnd:Init()
    self.m_PlayerName = nil
    self.m_PlayerId = nil
    self.m_TypeBtnList = {}
    self.Desc.text = g_MessageMgr:FormatMessage("FUXILABA_USE_WND_DESC")

    LuaQuweiLabaMgr.m_Type = 1
    for i=1,3 do
        local typeBtn = self.TypeRoot.transform:Find(tostring(i)):GetComponent(typeof(QnSelectableButton))
        typeBtn:SetSelected(i==1, i==1)
        UIEventListener.Get(typeBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function()
            LuaQuweiLabaMgr.m_Type = i
            for i=1,#self.m_TypeBtnList do
                self.m_TypeBtnList[i]:SetSelected(false, false)
            end
            typeBtn:SetSelected(true, true)
        end)
        table.insert(self.m_TypeBtnList, typeBtn)
    end
end

--@region UIEvent

function LuaQuweiLabaChooseWnd:OnChooseBtnClick()
    local btndata = {
		Pbtn = {
			Name = LocalString.GetString("选择"),
			Func = function(playerId)
                    self.m_PlayerId = playerId
                    local info = CIMMgr.Inst:GetBasicInfo(playerId)
                    if info ~= nil then
                        self.PlayerNameLabel.text = info.Name
                        self.m_PlayerName = info.Name
                    end
                        CUIManager.CloseUI(CLuaUIResources.CommonPlayerListWnd2)
                end
		},
		Fbtn = {
			Name = LocalString.GetString("关闭"),
			Func = function()
                    CUIManager.CloseUI(CLuaUIResources.CommonPlayerListWnd2)
                end
            }
	}
    LuaCommonPlayerListMgr.ShowWnd2(LocalString.GetString("选择对象"),btndata)
end

function LuaQuweiLabaChooseWnd:OnCommitBtnClick()
    if self.m_PlayerId and self.m_PlayerName then
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("QUWEI_LABA_LINGYU_USE_COMFIRM", self.m_PlayerName), function ()
            LuaQuweiLabaMgr:ShowLaBaWnd(self.m_PlayerId, self.m_PlayerName)
            CUIManager.CloseUI(CLuaUIResources.QuweiLabaChooseWnd)
            if CUIManager.IsLoaded(CUIResources.PackageWnd) then
                CUIManager.CloseUI(CUIResources.PackageWnd)
            end
            if CUIManager.IsLoaded(CIndirectUIResources.ItemInfoWnd) then
                CUIManager.CloseUI(CIndirectUIResources.ItemInfoWnd)
            end
        end, nil, nil, nil, false)
    else
        g_MessageMgr:ShowMessage("Guild_Choose_Player")
    end
end

function LuaQuweiLabaChooseWnd:OnRuleDescBtnClick()
    g_MessageMgr:ShowMessage("QUWEI_LABA_RULE_DESC")
end


--@endregion UIEvent

