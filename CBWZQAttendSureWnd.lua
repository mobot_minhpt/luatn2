-- Auto Generated!!
local CBWZQAttendSureWnd = import "L10.UI.CBWZQAttendSureWnd"
local CBWZQMgr = import "L10.Game.CBWZQMgr"
local DelegateFactory = import "DelegateFactory"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local UIEventListener = import "UIEventListener"
CBWZQAttendSureWnd.m_Init_CS2LuaHook = function (this) 
    this.cost1Label.text = tostring(CBWZQMgr.Inst.risePlayerAttendInfo.baomingFee)
    this.cost2Label.text = tostring(CBWZQMgr.Inst.risePlayerAttendInfo.dingjinFee)
    this.moneyCtrl:SetCost(CBWZQMgr.Inst.risePlayerAttendInfo.baomingFee + CBWZQMgr.Inst.risePlayerAttendInfo.dingjinFee)
    this.moneyCtrl:SetType(EnumMoneyType.YinLiang, EnumPlayScoreKey.NONE, true)

    UIEventListener.Get(this.submitBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        if this.moneyCtrl.moneyEnough then
            Gac2Gas.RequestJoinZhaoQin(CBWZQMgr.Inst.risePlayerAttendInfo.playerId)
            this:Close()
        else
            g_MessageMgr:ShowMessage("SILVER_NOT_ENOUGH")
        end
    end)

    UIEventListener.Get(this.cancelBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        this:Close()
    end)
end
