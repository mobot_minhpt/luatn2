require("common/common_include")

local CChatInput = import "L10.UI.CChatInput"
local CButton = import "L10.UI.CButton"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CChatInputMgr = import "L10.UI.CChatInputMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local Constants = import "L10.Game.Constants"
local CBaseWnd = import "L10.UI.CBaseWnd"
local DefaultChatInputListener = import "L10.UI.DefaultChatInputListener"
local EnumRecordContext = import "L10.Game.EnumRecordContext"
local EnumVoicePlatform = import "L10.Game.EnumVoicePlatform"
local CRecordingInfoMgr = import "L10.UI.CRecordingInfoMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local Utility = import "L10.Engine.Utility"
local CBabyModelTextureLoader = import "L10.UI.CBabyModelTextureLoader"
local BabyMgr = import "L10.Game.BabyMgr"
local CUIManager = import "L10.UI.CUIManager"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local UIWidget = import "UIWidget"
local CChatLinkMgr = import "CChatLinkMgr"
local CChatInputMgrEParentType = import "L10.UI.CChatInputMgr+EParentType"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local CChatInputWnd = import "L10.UI.CChatInputWnd"
local UIRoot = import "UIRoot"
local Screen = import "UnityEngine.Screen"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local UITexture = import "UITexture"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"

LuaBabyChatWnd = class()

RegistClassMember(LuaBabyChatWnd, "m_CloseButton")
RegistClassMember(LuaBabyChatWnd, "m_OffsetRoot")
RegistClassMember(LuaBabyChatWnd, "m_BabyTextureLoader")
RegistClassMember(LuaBabyChatWnd, "m_BabyChatRoot")
RegistClassMember(LuaBabyChatWnd, "m_BabyChatBg")
RegistClassMember(LuaBabyChatWnd, "m_BabyChatLabel")
RegistClassMember(LuaBabyChatWnd, "m_ParentChatRoot")
RegistClassMember(LuaBabyChatWnd, "m_ParentChatBg")
RegistClassMember(LuaBabyChatWnd, "m_ParentChatLabel")
RegistClassMember(LuaBabyChatWnd, "m_BabyPoemRoot")
RegistClassMember(LuaBabyChatWnd, "m_BabyPoemBg")
RegistClassMember(LuaBabyChatWnd, "m_BabyPoemTexture")

RegistClassMember(LuaBabyChatWnd, "m_BabyDivinationRoot")

RegistClassMember(LuaBabyChatWnd, "m_ChatInput")
RegistClassMember(LuaBabyChatWnd, "m_KeyboardInput")
RegistClassMember(LuaBabyChatWnd, "m_SwitchButton")
RegistClassMember(LuaBabyChatWnd, "m_VoiceButton")
RegistClassMember(LuaBabyChatWnd, "m_EmoticonButton")
RegistClassMember(LuaBabyChatWnd, "m_HistoryButton")
RegistClassMember(LuaBabyChatWnd, "m_DefaultChatInputListener")

RegistClassMember(LuaBabyChatWnd, "m_BabyModelTextureLoader")

RegistClassMember(LuaBabyChatWnd, "m_UseVoiceInput")
--baby聊天内容动画
RegistClassMember(LuaBabyChatWnd, "m_BabyMsgAniTick")
RegistClassMember(LuaBabyChatWnd, "m_TickIndex")
RegistClassMember(LuaBabyChatWnd, "m_CachedBabyMsg")

RegistClassMember(LuaBabyChatWnd, "m_ChatInputVisible") --ChatInputWnd是否可见
RegistClassMember(LuaBabyChatWnd, "m_PoemContent") --诗的内容
RegistClassMember(LuaBabyChatWnd, "m_CurBabyMsg")

function LuaBabyChatWnd:Init()

	self.m_CloseButton = self.transform:Find("Offset/CloseButton").gameObject
	self.m_OffsetRoot = self.transform:Find("Offset").gameObject
	self.m_ChatInput = self.transform:Find("Offset/ChatInput"):GetComponent(typeof(CChatInput))
	self.m_KeyboardInput = self.transform:Find("Offset/ChatInput/Keyboard").gameObject
	self.m_SwitchButton = self.transform:Find("Offset/ChatInput/SwitchButton"):GetComponent(typeof(CButton))
	self.m_VoiceButton = self.transform:Find("Offset/ChatInput/VoiceButton"):GetComponent(typeof(CButton))
	self.m_EmoticonButton = self.transform:Find("Offset/ChatInput/Keyboard/EmoticonButton").gameObject
	self.m_HistoryButton = self.transform:Find("Offset/ChatInput/HistoryButton").gameObject

	self.m_ChatInput:SetCharLimit(50)

	if not self.m_DefaultChatInputListener then

		self.m_DefaultChatInputListener = DefaultChatInputListener()
		self.m_DefaultChatInputListener:SetOnInputEmoticonFunc(function (prefix)
			self:OnInputEmoticon(prefix)
		end)
	end
	
	self.m_BabyModelTextureLoader = self.transform:Find("Offset/BabyTexture"):GetComponent(typeof(CBabyModelTextureLoader))

	if LuaBabyMgr.CurrentChatBabyContext == EnumShowBabyChatContext.eFromBabyWnd then
		local baby = BabyMgr.Inst:GetBabyById(LuaBabyMgr.CurrentChatBabyId or "")
		if baby then
			self.m_BabyModelTextureLoader.gameObject:SetActive(true)
			self.m_BabyModelTextureLoader:Init(baby, -200, false, 0) --模型侧放，雅文选择的角度
		else
			self.m_BabyModelTextureLoader.gameObject:SetActive(false)
		end
	elseif LuaBabyMgr.CurrentChatBabyContext == EnumShowBabyChatContext.eFromNPC then
		local npc = CClientObjectMgr.Inst:GetObject(LuaBabyMgr.CurrentChatBabyEngineId or 0)
		local babyInfo = npc and npc:GetBabyAppearance()
		if babyInfo then
			self.m_BabyModelTextureLoader.gameObject:SetActive(true)
			self.m_BabyModelTextureLoader:Init(babyInfo, -200, 0, 0) --模型侧放，雅文选择的角度
		else
			self.m_BabyModelTextureLoader.gameObject:SetActive(false)
		end
	else
		self.m_BabyModelTextureLoader.gameObject:SetActive(false)
	end
	

	self.m_BabyChatRoot = self.transform:Find("Offset/Baby").gameObject
	self.m_BabyChatBg = self.transform:Find("Offset/Baby/Bubble"):GetComponent(typeof(UIWidget))
	self.m_BabyChatLabel = self.transform:Find("Offset/Baby/Label"):GetComponent(typeof(UILabel))
	self.m_ParentChatRoot = self.transform:Find("Offset/Parent").gameObject
	self.m_ParentChatBg = self.transform:Find("Offset/Parent/Bubble"):GetComponent(typeof(UIWidget))
	self.m_ParentChatLabel = self.transform:Find("Offset/Parent/Label"):GetComponent(typeof(UILabel))
	self.m_BabyPoemRoot = self.transform:Find("Offset/BabyPoem").gameObject
	self.m_BabyPoemBg = self.transform:Find("Offset/BabyPoem/Bubble"):GetComponent(typeof(UIWidget))
	self.m_BabyPoemTexture = self.transform:Find("Offset/BabyPoem/Texture"):GetComponent(typeof(UITexture))
	self.m_BabyDivinationRoot = self.transform:Find("Offset/BabyDivination").gameObject
	
	self.m_BabyChatRoot:SetActive(false)
	self.m_ParentChatRoot:SetActive(false)
	self.m_BabyPoemRoot:SetActive(false)
	self.m_BabyDivinationRoot:SetActive(false)
	self.m_BabyChatLabel.text = nil
	self.m_ParentChatLabel.text = nil

	self.m_UseVoiceInput = false
	self.m_ChatInputVisible = false
	self:UpdateInputType()

	self.m_ChatInput.OnSend = DelegateFactory.Action_string(function (msg)
		self:OnSendMsg(msg)
	end)

	CommonDefs.AddOnClickListener(self.m_CloseButton, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)
	CommonDefs.AddOnClickListener(self.m_SwitchButton.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnSwitchButtonClick(go) end), false)
	CommonDefs.AddOnPressListener(self.m_VoiceButton.gameObject,DelegateFactory.Action_GameObject_bool(function(go, pressed) self:OnVoiceButtonPress(go, pressed) end),false)
	CommonDefs.AddOnClickListener(self.m_EmoticonButton, DelegateFactory.Action_GameObject(function(go) self:OnEmoticonButtonClick(go) end), false)
	CommonDefs.AddOnClickListener(self.m_HistoryButton, DelegateFactory.Action_GameObject(function(go) self:OnHistoryButtonClick(go) end), false)
	CommonDefs.AddOnClickListener(self.m_BabyPoemTexture.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnPoemTextureClick(go) end), false)
	CommonDefs.AddOnClickListener(self.m_BabyDivinationRoot.transform:Find("Texture").gameObject, DelegateFactory.Action_GameObject(function(go) self:OnDivinationClick(go) end), false)

	self:CancelTick()
	self:TryComsumeUnreadMsg()
end

function LuaBabyChatWnd:TryComsumeUnreadMsg()
	local ret = LuaBabyMgr.GetUnreadMsg(LuaBabyMgr.CurrentChatBabyId)
	if not ret then return end
	local msg = ret.msg
	local skillName = ret.skill
	self:ShowBabyMsg(msg, false, skillName)
	local mainplayer = CClientMainPlayer.Inst
	if not mainplayer then return end
	BabyMgr.Inst:AddMsg(0, LuaBabyMgr.CurrentChatBabyId, self:GetBabyPortrait(), mainplayer.Id, msg, false, 0)

	LuaBabyMgr.ConsumeUnreadMsg(LuaBabyMgr.CurrentChatBabyId)
end

function LuaBabyChatWnd:GetBabyPortrait()
	local portrait = ""
	if LuaBabyMgr.CurrentChatBabyContext == EnumShowBabyChatContext.eFromBabyWnd then
		local baby = BabyMgr.Inst:GetBabyById(LuaBabyMgr.CurrentChatBabyId or "")
		if baby then
			portrait = BabyMgr.Inst:GetBabyPortrait(baby)
		end
	elseif LuaBabyMgr.CurrentChatBabyContext == EnumShowBabyChatContext.eFromNPC then
		local npc = CClientObjectMgr.Inst:GetObject(LuaBabyMgr.CurrentChatBabyEngineId or 0)
		local babyInfo = npc and npc:GetBabyAppearance()
		if babyInfo then
			portrait = BabyMgr.Inst:GetBabyPortrait(babyInfo.Status, babyInfo.Gender, babyInfo.HairStyle)
		end
	end
	return portrait
end

function LuaBabyChatWnd:OnSendMsg(msg)
	--检查是否为空
	msg = CUICommonDef.Trim(msg)
	if msg == nil or msg == "" then return end
	--检查是否有敏感词
	local ret = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(msg, false)
    if ret.msg == nil or ret.shouldBeIgnore then
        g_MessageMgr:ShowMessage("Speech_Violation")
        return
    end

    msg = string.gsub(msg, "#400", "#81")

	local mainplayer = CClientMainPlayer.Inst
	if not mainplayer then return end
	local portrait = CUICommonDef.GetPortraitName(mainplayer.Class, mainplayer.Gender, mainplayer.BasicProp.Expression)

	self:ShowParentMsg(msg)

	local babyMsg = BabyMgr.Inst:AddMsg(1, LuaBabyMgr.CurrentChatBabyId, portrait, mainplayer.Id, msg, false,0)
	local msgArray = Utility.BreakString(msg, 2, 255, nil)
	Gac2Gas.SendChatMessageToBaby(LuaBabyMgr.CurrentChatBabyId or "", babyMsg.UUID, LuaBabyMgr.CurrentChatBabyIsMyBaby or false, msgArray[0], msgArray[1])
	self.m_ChatInput:Clear()
end

function LuaBabyChatWnd:ShowBabyMsg(msg, showAni, skillName)
	self.m_CurBabyMsg = msg		
	if skillName == EnumBabyChatSkill.ePoem then --作诗
		self.m_BabyChatRoot:SetActive(false)
		self.m_BabyPoemRoot:SetActive(true)
		self.m_BabyDivinationRoot:SetActive(false)
		self.m_PoemContent = CChatLinkMgr.TranslateToNGUIText(msg, false)
		self.m_BabyPoemBg:ResetAnchors()
	elseif skillName == EnumBabyChatSkill.eDivination then --占卜
		self.m_BabyChatRoot:SetActive(false)
		self.m_BabyPoemRoot:SetActive(false)
		self.m_BabyDivinationRoot:SetActive(true)
	else --普通消息
		if showAni then
			self:StartTick(msg, skillName)
			return
		end
		self.m_BabyChatRoot:SetActive(true)
		self.m_BabyPoemRoot:SetActive(false)
		self.m_BabyDivinationRoot:SetActive(false)
		
		self.m_BabyChatLabel.text = CChatLinkMgr.TranslateToNGUIText(msg, false)
		CUICommonDef.ResizeLabelByMaxWidth(self.m_BabyChatLabel, 424)
		self.m_BabyChatBg:ResetAnchors()
	end
end

function LuaBabyChatWnd:ShowParentMsg(msg)
	self.m_ParentChatRoot:SetActive(true)
	self.m_ParentChatLabel.text = CChatLinkMgr.TranslateToNGUIText(msg, false)
	CUICommonDef.ResizeLabelByMaxWidth(self.m_ParentChatLabel, 424)
	self.m_ParentChatLabel.transform.localPosition = Vector3(419 - self.m_ParentChatLabel.localSize.x,274,0)
	self.m_ParentChatBg:ResetAnchors()
end

function LuaBabyChatWnd:StartTick(msg, skill)
	self:CancelTick()
	self.m_TickIndex = 0
	self.m_CachedBabyMsg = {cachedMsg=msg, skillName=skill}
	self.m_BabyMsgAniTick = CTickMgr.Register(DelegateFactory.Action(function ()
		self:UpdateBabyMsg()
	end), 0.3 * 1000, ETickType.Loop)
end

function LuaBabyChatWnd:UpdateBabyMsg()
	if self.m_TickIndex == 0 then
		self:ShowBabyMsg(".[00]..[-]", false, EnumBabyChatSkill.eNormal)
	elseif self.m_TickIndex == 1 then
		self:ShowBabyMsg("..[00].[-]", false, EnumBabyChatSkill.eNormal)
	elseif self.m_TickIndex == 2 then
		self:ShowBabyMsg("...", false, EnumBabyChatSkill.eNormal)
	else
		self:ShowBabyMsg(self.m_CachedBabyMsg.cachedMsg, false, self.m_CachedBabyMsg.skillName)
		self:CancelTick()
	end

	self.m_TickIndex = self.m_TickIndex + 1
end

function LuaBabyChatWnd:CancelTick()
	if self.m_BabyMsgAniTick then
		invoke(self.m_BabyMsgAniTick)
		self.m_BabyMsgAniTick = nil
	end
end

function LuaBabyChatWnd:OnInputEmoticon(prefix)
	self.m_ChatInput:AddEmotion(prefix)
end

function LuaBabyChatWnd:OnSwitchButtonClick(go)
	self.m_UseVoiceInput = not self.m_UseVoiceInput
	self:UpdateInputType()
end

function LuaBabyChatWnd:OnVoiceButtonPress(go, pressed)
	if pressed then
        CRecordingInfoMgr.ShowRecordingWnd(EnumRecordContext.BabyChat, EChatPanel.Undefined, go, EnumVoicePlatform.Default, false);
        self.m_VoiceButton.Text = LocalString.GetString("松开结束")
    else
        CRecordingInfoMgr.CloseRecordingWnd(not go:Equals(CUICommonDef.SelectedUI))
        self.m_VoiceButton.Text = LocalString.GetString("按下发言")
    end
end

function LuaBabyChatWnd:OnEmoticonButtonClick(go)
    CChatInputMgr.ShowChatInputWnd(CChatInputMgrEParentType.BabyChat, self.m_DefaultChatInputListener, go, EChatPanel.Undefined, 0, 0)
end

function LuaBabyChatWnd:OnHistoryButtonClick(go)
	CUIManager.ShowUI("BabyChatHistoryWnd")
end

function LuaBabyChatWnd:OnPoemTextureClick(go)
	LuaBabyMgr.ShowPoemWnd(self.m_PoemContent, LuaBabyMgr.CurrentChatBabyId)
end

function LuaBabyChatWnd:OnDivinationClick(go)
	if self.m_CurBabyMsg then
		LuaBabyMgr.ShowBabyDivinationWnd(self.m_CurBabyMsg)
	end
end

function LuaBabyChatWnd:UpdateInputType()
	self.m_KeyboardInput:SetActive(not self.m_UseVoiceInput)
    self.m_VoiceButton.gameObject:SetActive(self.m_UseVoiceInput)
    self.m_VoiceButton.Text = LocalString.GetString("按下发言")
    self.m_SwitchButton:SetBackgroundSprite(self.m_UseVoiceInput and Constants.ChatInput_KeyboardInputIcon or Constants.ChatInput_VoiceInputIcon)
end

function LuaBabyChatWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function LuaBabyChatWnd:OnEnable()
	g_ScriptEvent:AddListener("OnVoiceTranslateFinished", self, "OnVoiceTranslateFinished")
	g_ScriptEvent:AddListener("ReplyChatMessageToBaby", self, "OnReplyChatMessageToBaby")

	g_ScriptEvent:AddListener("BabyUnreadMsgChanged", self, "BabyUnreadMsgChanged")
end

function LuaBabyChatWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnVoiceTranslateFinished", self, "OnVoiceTranslateFinished")
	g_ScriptEvent:RemoveListener("ReplyChatMessageToBaby", self, "OnReplyChatMessageToBaby")

	g_ScriptEvent:RemoveListener("BabyUnreadMsgChanged", self, "BabyUnreadMsgChanged")

	self:CancelTick()
	BabyMgr.Inst:SaveMsgs()

	if LuaBabyMgr.CurrentChatBabyContext == EnumShowBabyChatContext.eFromBabyWnd then
		if BabyMgr.Inst:GetBabyById(LuaBabyMgr.CurrentChatBabyId) then
			Gac2Gas.RequestOpenYangYuWnd()
		end
	end
end

function LuaBabyChatWnd:OnVoiceTranslateFinished(args)
	local context = args[0]
	local voiceId = args[1]
	local result = args[2]
	if context == EnumRecordContext.BabyChat then
		self:OnSendMsg(result)
	end
end

function LuaBabyChatWnd:OnReplyChatMessageToBaby(babyId, success, skillName, originMessageId, message)
	local msg = nil
	if success then
		msg = message
	else
		msg = self:GetLocalBabyAnswer(originMessageId)
	end
	self:ShowBabyMsg(msg, true, skillName)
	local mainplayer = CClientMainPlayer.Inst
	if not mainplayer then return end

	local sourceType = 0
	if skillName == EnumBabyChatSkill.ePoem then
		sourceType = 1
	end
	BabyMgr.Inst:AddMsg(0, LuaBabyMgr.CurrentChatBabyId,  self:GetBabyPortrait(), mainplayer.Id, msg, false, sourceType)
end

function LuaBabyChatWnd:BabyUnreadMsgChanged(babyId)
	if LuaBabyMgr.CurrentChatBabyId == babyId then
		self:TryComsumeUnreadMsg()
	end
end

function LuaBabyChatWnd:GetLocalBabyAnswer(originMessageId)
	
	local msg = BabyMgr.Inst:GetMsg(originMessageId or 0)
	if not msg then return LocalString.GetString("我没有听明白。") end

	if msg.content then

		local answer = ""
		local matchKeywordsNum = 0

		ChatBot_BabyCorpus.Foreach(function (id, data)
			local match = true
			local cnt = 0
			for keyword in string.gmatch(data.Keywords, "([^;]+)") do
				if string.find(msg.content, keyword, 1, true) == nil then
					match = false
					break
				end
				cnt = cnt + 1
			end

			if match and cnt > matchKeywordsNum then
				answer = data.Answer
				matchKeywordsNum = cnt
			end
		end)

		if answer~="" then return answer end
	end
	--随机一个结果
	local defaultAnswer = ""
	math.randomseed(os.time())
	local idx = math.random(1,ChatBot_BabyDeAnswer.GetDataCount())
	local t = 1

	ChatBot_BabyDeAnswer.Foreach(function (id, data)
		if t == idx then
	 		defaultAnswer = data.Answer
	 	end
	 	t = t + 1
	end)
	return defaultAnswer

end

function LuaBabyChatWnd:Update()

	if CChatInputMgr.ParentType ~= CChatInputMgrEParentType.BabyChat then return end

    if CUIManager.IsLoaded(CIndirectUIResources.ChatInputWnd) and not self.m_ChatInputVisible then
        self.m_ChatInputVisible = true

        local rect = CChatInputWnd.Instance.Area
        local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
        local virtualScreenHeight = Screen.height * scale
        local newY = rect.height * virtualScreenHeight / Screen.height
        local pos = self.m_OffsetRoot.transform.localPosition
        pos.y = newY
        self.m_OffsetRoot.transform.localPosition = pos

    elseif self.m_ChatInputVisible and not CUIManager.IsLoaded(CIndirectUIResources.ChatInputWnd) then
    	self.m_ChatInputVisible = false

        local pos = self.m_OffsetRoot.transform.localPosition
        pos.y = 0
        self.m_OffsetRoot.transform.localPosition = pos
    end
end
