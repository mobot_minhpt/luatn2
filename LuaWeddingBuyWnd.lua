local DelegateFactory        = import "DelegateFactory"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CCurrentMoneyCtrl      = import "L10.UI.CCurentMoneyCtrl"
local EnumMoneyType          = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey       = import "L10.Game.EnumPlayScoreKey"
local CItemMgr               = import "L10.Game.CItemMgr"

LuaWeddingBuyWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaWeddingBuyWnd, "title")
RegistClassMember(LuaWeddingBuyWnd, "addAndSub")
RegistClassMember(LuaWeddingBuyWnd, "moneyCtrl")
RegistClassMember(LuaWeddingBuyWnd, "okButton")
RegistClassMember(LuaWeddingBuyWnd, "chatBubbleMale")
RegistClassMember(LuaWeddingBuyWnd, "chatBubbleFemale")
RegistClassMember(LuaWeddingBuyWnd, "guestSide")

function LuaWeddingBuyWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
    self:InitEventListener()
    self:InitActive()
end

-- 初始化UI组件
function LuaWeddingBuyWnd:InitUIComponents()
    local anchor = self.transform:Find("Anchor")
    self.title = anchor:Find("Title"):GetComponent(typeof(UILabel))
    self.addAndSub = anchor:Find("QnIncreseAndDecreaseButton"):GetComponent(typeof(QnAddSubAndInputButton))
    self.moneyCtrl = anchor:Find("QnCostAndOwnMoney"):GetComponent(typeof(CCurrentMoneyCtrl))
    self.okButton = anchor:Find("OKButton")

    self.chatBubbleMale = anchor:Find("ChatBubbleMale").gameObject
    self.chatBubbleFemale = anchor:Find("ChatBubbleFemale").gameObject
end

-- 初始化响应
function LuaWeddingBuyWnd:InitEventListener()
    UIEventListener.Get(self.okButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOKButtonClick()
	end)
end

function LuaWeddingBuyWnd:InitActive()
    self.title.gameObject:SetActive(false)
    self.chatBubbleMale.gameObject:SetActive(false)
    self.chatBubbleFemale.gameObject:SetActive(false)
end

function LuaWeddingBuyWnd:OnEnable()
	g_ScriptEvent:AddListener("NewWeddingSyncGuestSide", self, "OnSyncGuestSide")
end

function LuaWeddingBuyWnd:OnDisable()
	g_ScriptEvent:RemoveListener("NewWeddingSyncGuestSide", self, "OnSyncGuestSide")
end

-- 同步宾客身份 男方宾客(0)/女方宾客(1)
function LuaWeddingBuyWnd:OnSyncGuestSide(guestSide)
    self.guestSide = guestSide
    self:UpdateTitleAndTrumpet()
end


function LuaWeddingBuyWnd:Init()
    self:InitCostAndOwn()
    Gac2Gas.NewWeddingQueryGuestSide()
end

-- 根据宾客身份切换喇叭显示
function LuaWeddingBuyWnd:UpdateTitleAndTrumpet()
    local itemId
    local setting = WeddingIteration_Setting.GetData()

    if self.guestSide == 0 then
        itemId = setting.GroomTrumpetId
        self.chatBubbleMale.gameObject:SetActive(true)
        self.chatBubbleFemale.gameObject:SetActive(false)
    else
        itemId = setting.BrideTrumpetId
        self.chatBubbleMale.gameObject:SetActive(false)
        self.chatBubbleFemale.gameObject:SetActive(true)
    end

    local item = CItemMgr.Inst:GetItemTemplate(itemId)
    self.title.text = item.Name
    self.title.gameObject:SetActive(true)
end

-- 初始化消耗拥有
function LuaWeddingBuyWnd:InitCostAndOwn()
    self.moneyCtrl:SetType(EnumMoneyType.LingYu, EnumPlayScoreKey.NONE, true)
    self.addAndSub:SetValue(1, true)
    self:UpdateCost()

    self.addAndSub.onValueChanged = DelegateFactory.Action_uint(function (value)
        self:UpdateCost()
    end)
end

-- 更新花费数目
function LuaWeddingBuyWnd:UpdateCost()
    local count = self.addAndSub:GetValue()
    self.moneyCtrl:SetCost(WeddingIteration_Setting.GetData().WeddingTrumpetCost * count)
end

--@region UIEvent

function LuaWeddingBuyWnd:OnOKButtonClick()
    if self.guestSide == nil then return end

    local setting = WeddingIteration_Setting.GetData()
    local itemId = self.guestSide == 0 and setting.GroomTrumpetId or setting.BrideTrumpetId
    Gac2Gas.NewWeddingRequestBuyMallItem(0, itemId, self.addAndSub:GetValue())
end

--@endregion UIEvent

