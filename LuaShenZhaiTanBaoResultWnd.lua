local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local Animator = import "UnityEngine.Animator"

LuaShenZhaiTanBaoResultWnd = class()
LuaShenZhaiTanBaoResultWnd.S_bSuccess = nil
LuaShenZhaiTanBaoResultWnd.S_playDuration = nil
LuaShenZhaiTanBaoResultWnd.S_bReward = nil
LuaShenZhaiTanBaoResultWnd.S_bNewRecord = nil

RegistChildComponent(LuaShenZhaiTanBaoResultWnd, "closeBtn", "closeBtn", GameObject)
RegistChildComponent(LuaShenZhaiTanBaoResultWnd, "winNode", "winNode", GameObject)
RegistChildComponent(LuaShenZhaiTanBaoResultWnd, "failNode", "failNode", GameObject)

function LuaShenZhaiTanBaoResultWnd:Awake()
    self:InitUIComponent()
    self:InitUIEvent()
    self:InitUIData()
end

function LuaShenZhaiTanBaoResultWnd:Init()
end

function LuaShenZhaiTanBaoResultWnd:InitUIComponent()
    self.newRecord = self.winNode.transform:Find("NewRecord")
    local rewardNode = self.winNode.transform:Find("RewardNode")
    self.noRewardReminder = rewardNode:Find("NoRewardReminder")
    self.rewardItem = rewardNode:Find("Item1")
    self.consumtTimeText = self.winNode.transform:Find("xinxi/PlayTimeNode/ConsumeTime"):GetComponent(typeof(UILabel))
    self.rechallengeButton = self.failNode.transform:Find("RechallengeButton")

    self.animator = self.transform:GetComponent(typeof(Animator))
end

function LuaShenZhaiTanBaoResultWnd:InitUIEvent()
    UIEventListener.Get(self.rechallengeButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        CUIManager.CloseUI(CLuaUIResources.ShenZhaiTanBaoResultWnd)
        CUIManager.ShowUI(CLuaUIResources.ShenZhaiTanBaoSignUpWnd)
    end)
end

function LuaShenZhaiTanBaoResultWnd:InitUIData()
    self.SZTBConfigData = Double11_SZTB.GetData()

    self.failNode:SetActive(not LuaShenZhaiTanBaoResultWnd.S_bSuccess)
    self.winNode:SetActive(LuaShenZhaiTanBaoResultWnd.S_bSuccess)
    if LuaShenZhaiTanBaoResultWnd.S_bSuccess then
        self.newRecord.gameObject:SetActive(LuaShenZhaiTanBaoResultWnd.S_bNewRecord)
        self.noRewardReminder.gameObject:SetActive(not LuaShenZhaiTanBaoResultWnd.S_bReward)
        self.rewardItem.gameObject:SetActive(LuaShenZhaiTanBaoResultWnd.S_bReward)
        local time = SafeStringFormat3("%02d:%02d", math.floor(LuaShenZhaiTanBaoResultWnd.S_playDuration / 60), LuaShenZhaiTanBaoResultWnd.S_playDuration % 60)
        self.consumtTimeText.text = time
        if LuaShenZhaiTanBaoResultWnd.S_bReward then
            self:InitOneItem(self.rewardItem.gameObject, self.SZTBConfigData.RewardItemId)
        end
        self.animator:Play("shenzhaitanbaoresultwnd_shown", 0, 0)
    else
        self.animator:Play("shenzhaitanbaoresultwnd_sbshown", 0, 0)
    end
end

function LuaShenZhaiTanBaoResultWnd:InitOneItem(curItem, itemID)
    local texture = curItem.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local ItemData = Item_Item.GetData(itemID)
    if ItemData then texture:LoadMaterial(ItemData.Icon) end
    UIEventListener.Get(curItem).onClick = DelegateFactory.VoidDelegate(function (_)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemID, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
    end)
end