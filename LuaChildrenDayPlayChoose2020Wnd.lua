local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local UITexture = import "UITexture"
local DelegateFactory = import "DelegateFactory"
local Time = import "UnityEngine.Time"

LuaChildrenDayPlayChoose2020Wnd = class()
RegistChildComponent(LuaChildrenDayPlayChoose2020Wnd,"endButton", GameObject)
RegistChildComponent(LuaChildrenDayPlayChoose2020Wnd,"goonButton", GameObject)
RegistChildComponent(LuaChildrenDayPlayChoose2020Wnd,"chooseNode", GameObject)
RegistChildComponent(LuaChildrenDayPlayChoose2020Wnd,"timeBar", GameObject)
RegistChildComponent(LuaChildrenDayPlayChoose2020Wnd,"choose1", GameObject)
RegistChildComponent(LuaChildrenDayPlayChoose2020Wnd,"choose2", GameObject)
RegistChildComponent(LuaChildrenDayPlayChoose2020Wnd,"btnNode", GameObject)

--RegistClassMember(LuaChildrenDayPlayChoose2020Wnd, "maxChooseNum")

function LuaChildrenDayPlayChoose2020Wnd:Close()
	CUIManager.CloseUI(CLuaUIResources.ChildrenDayPlayChoose2020Wnd)
end

function LuaChildrenDayPlayChoose2020Wnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateChildrenDayGameVote", self, "InitData")
end

function LuaChildrenDayPlayChoose2020Wnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateChildrenDayGameVote", self, "InitData")
end

function LuaChildrenDayPlayChoose2020Wnd:InitData()
	local selfChoose = 0
	if LuaChildrenDay2020Mgr.gameVoteTable then
		for i,v in ipairs(self.chooseNodeTable) do
			if LuaChildrenDay2020Mgr.gameVoteTable[i] then
				if LuaChildrenDay2020Mgr.gameVoteTable[i][2] == -1 then
					v[1]:SetActive(true)
					v[2]:SetActive(false)
					if LuaChildrenDay2020Mgr.gameVoteTable[i][1] == CClientMainPlayer.Inst.Id then
						selfChoose = -1
					end
				elseif LuaChildrenDay2020Mgr.gameVoteTable[i][2] == 1 then
					v[1]:SetActive(false)
					v[2]:SetActive(true)
					if LuaChildrenDay2020Mgr.gameVoteTable[i][1] == CClientMainPlayer.Inst.Id then
						selfChoose = 1
					end
				else
					v[1]:SetActive(false)
					v[2]:SetActive(false)
				end
			else
				v[1]:SetActive(false)
				v[2]:SetActive(false)
			end
		end
	end

	if selfChoose == -1 then
		self.choose1:SetActive(false)
		self.choose2:SetActive(true)
		self.btnNode:SetActive(false)
	elseif selfChoose == 1 then
		self.choose1:SetActive(true)
		self.choose2:SetActive(false)
		self.btnNode:SetActive(false)
	else
		self.choose1:SetActive(false)
		self.choose2:SetActive(false)
		self.btnNode:SetActive(true)
		self:StartCount()
	end
end

function LuaChildrenDayPlayChoose2020Wnd:StartCount()
  self.remainTime = LuaChildrenDay2020Mgr.gameVoteRemainTime
  self.totalTime = LuaChildrenDay2020Mgr.gameVoteTotalTime
	self.startTime = Time.realtimeSinceStartup
end

function LuaChildrenDayPlayChoose2020Wnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)
	local onGoonClick = function(go)
		Gac2Gas.DanZhuVoteEarlyEnd(false)
		self:Close()
	end
	CommonDefs.AddOnClickListener(self.goonButton,DelegateFactory.Action_GameObject(onGoonClick),false)
	local onEndClick = function(go)
		Gac2Gas.DanZhuVoteEarlyEnd(true)
		self:Close()
	end
	CommonDefs.AddOnClickListener(self.endButton,DelegateFactory.Action_GameObject(onEndClick),false)
	self.timeBarScripts = self.timeBar.transform:Find('c1'):GetComponent(typeof(UITexture))
	self.timeBarMaxLength = self.timeBarScripts.width

	self.chooseNodeTable = {
		{self.chooseNode.transform:Find('b1').gameObject,self.chooseNode.transform:Find('c1')},
		{self.chooseNode.transform:Find('b2').gameObject,self.chooseNode.transform:Find('c2')},
		{self.chooseNode.transform:Find('b3').gameObject,self.chooseNode.transform:Find('c3')},
	}

	self.choose1:SetActive(false)
	self.choose2:SetActive(false)
	self.btnNode:SetActive(false)

	self:InitData()
end

function LuaChildrenDayPlayChoose2020Wnd:Update()
	if self.remainTime and self.totalTime and self.startTime and self.totalTime > 0 then
		local nowTime = Time.realtimeSinceStartup
		local pastTime = nowTime - self.startTime
		local percent = (self.remainTime - pastTime) / self.totalTime
		if percent > 1 then
			percent = 1
		elseif percent < 0 then
			percent = 0
		end
		self.timeBarScripts.width = self.timeBarMaxLength * percent
		if percent == 0 then
			Gac2Gas.DanZhuVoteEarlyEnd(false)
			self.remainTime = nil
			self.totalTime = nil
			self.startTime = nil
			self:Close()
		end
	end
end

return LuaChildrenDayPlayChoose2020Wnd
