local GameObject = import "UnityEngine.GameObject"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CUITexture = import "L10.UI.CUITexture"
local Extensions = import "Extensions"
local Vector3 = import "UnityEngine.Vector3"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemAccessTipMgr = import "L10.UI.CItemAccessTipMgr"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"

LuaCaiPiao2019Wnd = class()
RegistChildComponent(LuaCaiPiao2019Wnd,"closeBtn", GameObject)
RegistChildComponent(LuaCaiPiao2019Wnd,"tab1", GameObject)
RegistChildComponent(LuaCaiPiao2019Wnd,"tab1Normal", GameObject)
RegistChildComponent(LuaCaiPiao2019Wnd,"tab1Light", GameObject)

RegistChildComponent(LuaCaiPiao2019Wnd,"tab2", GameObject)
RegistChildComponent(LuaCaiPiao2019Wnd,"tab2Normal", GameObject)
RegistChildComponent(LuaCaiPiao2019Wnd,"tab2Light", GameObject)

RegistChildComponent(LuaCaiPiao2019Wnd,"node1", GameObject)
RegistChildComponent(LuaCaiPiao2019Wnd,"playerNode", GameObject)
RegistChildComponent(LuaCaiPiao2019Wnd,"playerIconNode", GameObject)

RegistChildComponent(LuaCaiPiao2019Wnd,"node2", GameObject)

RegistClassMember(LuaCaiPiao2019Wnd, "maxChooseNum")
RegistClassMember(LuaCaiPiao2019Wnd, "chooseIconTable")
RegistClassMember(LuaCaiPiao2019Wnd, "chooseIconNodeTable")
RegistClassMember(LuaCaiPiao2019Wnd, "totalBonusNum")
RegistClassMember(LuaCaiPiao2019Wnd, "buyBonusBtn")
RegistClassMember(LuaCaiPiao2019Wnd, "buyBonusResNode")
RegistClassMember(LuaCaiPiao2019Wnd, "buyBonusTip")

RegistClassMember(LuaCaiPiao2019Wnd, "hisTempalteNode")

RegistClassMember(LuaCaiPiao2019Wnd, "hisDataTable")

RegistClassMember(LuaCaiPiao2019Wnd, "totalRewardNum")

function LuaCaiPiao2019Wnd:Close()
	CUIManager.CloseUI(CLuaUIResources.CaiPiao2019Wnd)
end

function LuaCaiPiao2019Wnd:OnEnable()
	g_ScriptEvent:AddListener("LotteryAddSucc", self, "AddLotterySucc")
	g_ScriptEvent:AddListener("LotteryHisInfoBack", self, "InitNode2Data")
	g_ScriptEvent:AddListener("LotteryTotalRewardInfoBack", self, "TotalRewardInfoBack")
end

function LuaCaiPiao2019Wnd:OnDisable()
	g_ScriptEvent:RemoveListener("LotteryAddSucc", self, "AddLotterySucc")
	g_ScriptEvent:RemoveListener("LotteryHisInfoBack", self, "InitNode2Data")
	g_ScriptEvent:RemoveListener("LotteryTotalRewardInfoBack", self, "TotalRewardInfoBack")
end

function LuaCaiPiao2019Wnd:Split(str, pat)
   local t = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
         table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end

function LuaCaiPiao2019Wnd:TotalRewardInfoBack(info)
	local formulaId = ZhouNianQing_Lottery.GetData().JackpotFormulaId
	local giftItems = ZhouNianQing_Lottery.GetData().GiftItemId
	local giftItemTable = self:Split(giftItems,';')
	local valTable = {}
	for i,v in pairs(giftItemTable) do
		local value = tonumber(v)
		if value and value > 0 then
			if CommonDefs.DictContains(info, typeof(String), tostring(value)) then
				valTable[value] = tonumber(info[tostring(value)])
			else
				valTable[value] = 0
			end
		end
	end

	local val = AllFormulas.Action_Formula[formulaId].Formula(nil, nil, {valTable})
	self.totalRewardNum = val
  local totalBonus = self.node2.transform:Find('num'):GetComponent(typeof(UILabel))
	totalBonus.text = self.totalRewardNum
	self.totalBonusNum.text = self.totalRewardNum
end

function LuaCaiPiao2019Wnd:AddLotterySucc()
  self.buyBonusBtn:SetActive(true)
  self:InitNode1Data()
end

function LuaCaiPiao2019Wnd:OnNeedItemClicked(go, itemId)
	CItemAccessTipMgr.Inst:ShowAccessTip(nil, itemId, false, go.transform, CTooltipAlignType.Top)
end

function LuaCaiPiao2019Wnd:InitNeedItem(go, itemId, count)
	local item = Item_Item.GetData(itemId)
	if not item then return end

	local IconTexture = go.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
	local DisableSprite = go.transform:Find("DisableSprite").gameObject
	local GetHint = go.transform:Find("GetHint").gameObject
	local CountLabel = go.transform:Find("CountLabel"):GetComponent(typeof(UILabel))
	--local NameLabel = go.transform:Find("Label"):GetComponent(typeof(UILabel))
	--NameLabel.text = item.Name

	IconTexture:LoadMaterial(item.Icon)
	local bindItemCountInBag, notBindItemCountInBag = CItemMgr.Inst:CalcItemCountInBagByTemplateId(itemId)
	local totalCount = bindItemCountInBag + notBindItemCountInBag
	if totalCount < count then
		DisableSprite:SetActive(true)
		GetHint:SetActive(true)
		CountLabel.text = SafeStringFormat3("[FF0000]%d[-]/%d", totalCount,count)

		local onNeedItemClicked = function (go)
			self:OnNeedItemClicked(go, itemId)
		end
		CommonDefs.AddOnClickListener(IconTexture.gameObject, DelegateFactory.Action_GameObject(onNeedItemClicked), false)
		return false
	else
		DisableSprite:SetActive(false)
		GetHint:SetActive(false)
		CountLabel.text = SafeStringFormat3("%d/%d", totalCount, count)
		return true
	end
end

function LuaCaiPiao2019Wnd:InitSelfChooseNode()
  for i,v in pairs(self.chooseIconNodeTable) do
    v.transform:Find('Mask/Icon').gameObject:SetActive(false)
    v.transform:Find('Mask/question_mark').gameObject:SetActive(true)
  end

  if not self.chooseIconTable then
    self.chooseIconTable = {}
  end
	table.sort(self.chooseIconTable,function(a,b) return a.index < b.index end)

  if self.chooseIconTable then
    for i,v in ipairs(self.chooseIconTable) do
      local data = ZhouNianQing_LotteryIcon.GetData(v.index)
      if data then
        local node = self.chooseIconNodeTable[i]
        node.transform:Find('Mask/Icon').gameObject:SetActive(true)
        node.transform:Find('Mask/question_mark').gameObject:SetActive(false)
        node.transform:Find("Mask/Icon"):GetComponent(typeof(CUITexture)):LoadPortrait(CUICommonDef.GetPortraitName(data.Class,data.Gender),true)
      end
    end
  end
end

function LuaCaiPiao2019Wnd:AddCaiPiaoChooseNode(index,lightNode)
  if not self.chooseIconTable then
    self.chooseIconTable = {}
  end
  if #self.chooseIconTable >= self.maxChooseNum and not lightNode.activeSelf then
		g_MessageMgr:ShowMessage("ZNQ_Lottery_More_Five_Number")
    return
  end

  if lightNode.activeSelf then
    lightNode:SetActive(false)
    for i,v in pairs(self.chooseIconTable) do
      if v.index == index then
        table.remove(self.chooseIconTable,i)
        break
      end
    end
  else
    lightNode:SetActive(true)
    table.insert(self.chooseIconTable,{index = index})
  end

  self:InitSelfChooseNode()
end

function LuaCaiPiao2019Wnd:InitNode1Data()
  for i,v in pairs(self.chooseIconNodeTable) do
    v.transform:Find('Mask/Icon').gameObject:SetActive(false)
    v.transform:Find('Mask/question_mark').gameObject:SetActive(true)
  end
	self.chooseIconTable = {}

	Extensions.RemoveAllChildren(self.playerNode.transform)
  local originPos = Vector3(-569,205,0)
  local width = 160
  local height = 140
  local widthNum = 5

  self:InitNeedItem(self.buyBonusResNode,ZhouNianQing_Lottery.GetData().LotteryItemId,1)

  local count = ZhouNianQing_LotteryIcon.GetDataCount()
  for i=1,count do
		local index = 1000 + i
    local data = ZhouNianQing_LotteryIcon.GetData(index)
    if data then
      local node = NGUITools.AddChild(self.playerNode,self.playerIconNode)
      node:SetActive(true)
      local w = (i - 1) % widthNum
      local h = (i - w - 1) / widthNum
      node.transform.localPosition = Vector3(originPos.x + width * w,originPos.y - height * h,originPos.z)
      node.transform:Find("Mask/Icon"):GetComponent(typeof(CUITexture)):LoadPortrait(CUICommonDef.GetPortraitName(data.Class,data.Gender),true)
      local lightNode = node.transform:Find('light').gameObject
      lightNode:SetActive(false)
      local nodeClickFunc = function(go)
        self:AddCaiPiaoChooseNode(index,lightNode)
      end
      CommonDefs.AddOnClickListener(node,DelegateFactory.Action_GameObject(nodeClickFunc),false)
    end
  end
end

function LuaCaiPiao2019Wnd:InitNode1()
  self.node1:SetActive(true)
  self.node2:SetActive(false)
end

function LuaCaiPiao2019Wnd:InitNode2()
  self.node1:SetActive(false)
  self.node2:SetActive(true)
  local totalSocre = self.node2.transform:Find('score'):GetComponent(typeof(UILabel))
  totalSocre.text = ''
  local bonusNodeTable = {
    self.node2.transform:Find('myChoiceNode/node1'),
    self.node2.transform:Find('myChoiceNode/node2'),
    self.node2.transform:Find('myChoiceNode/node3'),
    self.node2.transform:Find('myChoiceNode/node4'),
    self.node2.transform:Find('myChoiceNode/node5'),
  }
  for i,v in pairs(bonusNodeTable) do
    v.transform:Find('Mask/Icon').gameObject:SetActive(false)
    v.transform:Find('Mask/question_mark').gameObject:SetActive(true)
  end
  Gac2Gas.ZNQLotteryQueryLottery()
end

function LuaCaiPiao2019Wnd:InitNode2Data(data)
  local totalSocre = self.node2.transform:Find('score'):GetComponent(typeof(UILabel))
  totalSocre.text = data.reward

  local bonusNodeTable = {
    self.node2.transform:Find('myChoiceNode/node1'),
    self.node2.transform:Find('myChoiceNode/node2'),
    self.node2.transform:Find('myChoiceNode/node3'),
    self.node2.transform:Find('myChoiceNode/node4'),
    self.node2.transform:Find('myChoiceNode/node5'),
  }
  local resultData = data.result
	local resultTable = {}
	local resultSign = false
  if resultData then
    local count = resultData.Count - 1
    for i = 0, count do
      local index = resultData[i]
      local data = ZhouNianQing_LotteryIcon.GetData(index)
			resultTable[index] = true
      if data then
				resultSign = true
        local node = bonusNodeTable[i+1]
        node.transform:Find('Mask/Icon').gameObject:SetActive(true)
        node.transform:Find('Mask/question_mark').gameObject:SetActive(false)
        node.transform:Find("Mask/Icon"):GetComponent(typeof(CUITexture)):LoadPortrait(CUICommonDef.GetPortraitName(data.Class,data.Gender),true)
      end
    end
  end

  local _table = self.node2.transform:Find('record/scrollview/table'):GetComponent(typeof(UITable))
  local scrollview = self.node2.transform:Find('record/scrollview'):GetComponent(typeof(UIScrollView))

	Extensions.RemoveAllChildren(_table.transform)
  local getRewardBtn = self.node2.transform:Find('GetBonusBtn').gameObject
	if not resultSign then
		getRewardBtn:SetActive(false)
	else
		getRewardBtn:SetActive(true)
	end

  local hisData = data.his
  if hisData then
    local count = hisData.Count - 1

    for i = 0,count do
			local data = hisData[i]
			if data then
				local node = NGUITools.AddChild(_table.gameObject,self.hisTempalteNode)
				node:SetActive(true)

				local timestamp = data.CreateTime
				local timetable = os.date('*t',timestamp)
				local mins = timetable.min
				if mins < 10 then
					mins = '0' .. mins
				end
				local timestring = SafeStringFormat3('%s.%s.%s %s:%s',timetable.year,timetable.month,timetable.day,timetable.hour,mins)
				node.transform:Find('time'):GetComponent(typeof(UILabel)).text = timestring

				local level = 0
				if CommonDefs.DictContains(data, typeof(String), 'Level') then
					level = tonumber(data.Level)
				end
				if not level then
					level = 0
				end
				level = level + 1
				local levelString = {LocalString.GetString('未中奖'),LocalString.GetString('一等奖'),LocalString.GetString('二等奖'),LocalString.GetString('三等奖'),LocalString.GetString('四等奖')}
				if not resultSign then
					node.transform:Find('result'):GetComponent(typeof(UILabel)).text = LocalString.GetString('未开奖')
				else
					node.transform:Find('result'):GetComponent(typeof(UILabel)).text = levelString[level]
				end

				local iconData = data.DataList
				if iconData then
					local bonusNodeTable = {
						node.transform:Find('myChoiceNode/node1'),
						node.transform:Find('myChoiceNode/node2'),
						node.transform:Find('myChoiceNode/node3'),
						node.transform:Find('myChoiceNode/node4'),
						node.transform:Find('myChoiceNode/node5'),
					}
					for i,v in pairs(bonusNodeTable) do
						v.transform:Find('Mask/Icon').gameObject:SetActive(false)
						v.transform:Find('Mask/question_mark').gameObject:SetActive(true)
					end

					local count = iconData.Count - 1
					local indexTable = {}
					for i=0,count do
						local index = tonumber(iconData[i])
						table.insert(indexTable,index)
					end
					table.sort(indexTable)

					for i,v in ipairs(indexTable) do
						local data = ZhouNianQing_LotteryIcon.GetData(v)
						if data then
							local node = bonusNodeTable[i]
							node.transform:Find('Mask/Icon').gameObject:SetActive(true)
							node.transform:Find('Mask/question_mark').gameObject:SetActive(false)
							node.transform:Find("Mask/Icon"):GetComponent(typeof(CUITexture)):LoadPortrait(CUICommonDef.GetPortraitName(data.Class,data.Gender),true)
							if resultTable[v] then
								node.transform:Find('Mask/greenbg').gameObject:SetActive(true)
							else
								node.transform:Find('Mask/greenbg').gameObject:SetActive(false)
							end
						end
					end
				end
			end
    end
    _table:Reposition()
    scrollview:ResetPosition()
  end
end

function LuaCaiPiao2019Wnd:InitTab()
  local onTab1Click = function(go)
    if self.tab1Normal.activeSelf then
      self.tab1Normal:SetActive(false)
      self.tab1Light:SetActive(true)
      self.tab2Normal:SetActive(true)
      self.tab2Light:SetActive(false)
      self:InitNode1()
    else

    end
  end
  local onTab2Click = function(go)
    if self.tab2Normal.activeSelf then
      self.tab1Normal:SetActive(true)
      self.tab1Light:SetActive(false)
      self.tab2Normal:SetActive(false)
      self.tab2Light:SetActive(true)
      self:InitNode2()
    else

    end
  end

	CommonDefs.AddOnClickListener(self.tab1,DelegateFactory.Action_GameObject(onTab1Click),false)
	CommonDefs.AddOnClickListener(self.tab2,DelegateFactory.Action_GameObject(onTab2Click),false)

  self:InitNode1Data()
  onTab1Click()

  self.playerIconNode:SetActive(false)
end

function LuaCaiPiao2019Wnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)
  self.buyBonusBtn = self.node1.transform:Find('GetBonusBtn').gameObject
	local onSubmitCaiPiaoClick = function(go)
    if self.chooseIconTable then
			if #self.chooseIconTable < self.maxChooseNum then
				g_MessageMgr:ShowMessage("ZNQ_Lottery_Less_Five_Number")
				return
			end

      Gac2Gas.ZNQLotteryAddTicket(self.chooseIconTable[1].index, self.chooseIconTable[2].index, self.chooseIconTable[3].index, self.chooseIconTable[4].index, self.chooseIconTable[5].index)

      self.buyBonusBtn:SetActive(false)
    end
	end
	CommonDefs.AddOnClickListener(self.buyBonusBtn,DelegateFactory.Action_GameObject(onSubmitCaiPiaoClick),false)

  local getRewardBtn = self.node2.transform:Find('GetBonusBtn').gameObject
	local onRewardCaiPiaoClick = function(go)
    Gac2Gas.ZNQLotteryGetReward()
	end
	CommonDefs.AddOnClickListener(getRewardBtn,DelegateFactory.Action_GameObject(onRewardCaiPiaoClick),false)

  Gac2Gas.ZNQLotteryQueryPrizePool()

  self.maxChooseNum = 5
  self.chooseIconNodeTable = {
    self.node1.transform:Find('myChoiceNode/node1'),
    self.node1.transform:Find('myChoiceNode/node2'),
    self.node1.transform:Find('myChoiceNode/node3'),
    self.node1.transform:Find('myChoiceNode/node4'),
    self.node1.transform:Find('myChoiceNode/node5'),
  }
  self.totalBonusNum = self.node1.transform:Find('score'):GetComponent(typeof(UILabel))
	self.totalBonusNum.text = ''
  self.buyBonusResNode = self.node1.transform:Find('resNode').gameObject
  self.buyBonusTip = self.node1.transform:Find('tipBtn').gameObject

	local onBonusTipClick = function(go)
		g_MessageMgr:ShowMessage("ZNQ_Lottery_Tips")
	end
	CommonDefs.AddOnClickListener(self.buyBonusTip,DelegateFactory.Action_GameObject(onBonusTipClick),false)

  local awardTime1 = self.node1.transform:Find('awardTime'):GetComponent(typeof(UILabel))
  local awardTime2 = self.node2.transform:Find('awardTime'):GetComponent(typeof(UILabel))
  awardTime1.text = ZhouNianQing_Lottery.GetData().LotteryTime
  awardTime2.text = ZhouNianQing_Lottery.GetData().LotteryTime

  self.hisTempalteNode = self.node2.transform:Find('infoNode').gameObject
  self.hisTempalteNode:SetActive(false)

  self:InitSelfChooseNode()
  self:InitTab()

end

return LuaCaiPiao2019Wnd
