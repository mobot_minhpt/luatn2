-- Auto Generated!!
local CXialvPKMgr = import "L10.Game.CXialvPKMgr"
local CXialvPKSkillItem = import "L10.UI.CXialvPKSkillItem"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
CXialvPKSkillItem.m_Init_CS2LuaHook = function (this, item) 

    this.tempSkill = item

    this:UpdateXialvPkItem()
end
CXialvPKSkillItem.m_UpdateXialvPkItem_CS2LuaHook = function (this) 
    local skill = Skill_AllSkills.GetData(CLuaDesignMgr.Skill_AllSkills_GetSkillId(this.tempSkill.ID, 1))
    if skill ~= nil then
        this.m_Icon:LoadSkillIcon(skill.SkillIcon)
        this.m_NameLabel.text = skill.Name

        if this.colorSystemIcon ~= nil then
            if CXialvPKMgr.Inst:IsColorSystemSkill(this.tempSkill.ID) or CXialvPKMgr.Inst:IsColorSystemBaseSkill(this.tempSkill.ID) then
                this.colorSystemIcon.gameObject:SetActive(true)
                this.colorSystemIcon.spriteName = CXialvPKMgr.Inst:GetColorSystemSkillIcon(this.tempSkill.ID)
            else
                this.colorSystemIcon.gameObject:SetActive(false)
            end
        end
    end
end
CXialvPKSkillItem.m_UpdateColorSkillIcon_CS2LuaHook = function (this, cls, i) 
    if this.tempSkill ~= nil and this.tempSkill.ID == cls then
        local newSkillID = CXialvPKMgr.Inst:GetReplaceColorSystemSkillCls(cls, i)
        this.tempSkill.ID = newSkillID
        this:UpdateXialvPkItem()
        this:OnItemClick(this.gameObject)
    end
end
