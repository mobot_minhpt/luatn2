local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"

LuaDaFuWongSanCaiWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaDaFuWongSanCaiWnd, "DesLabel", "DesLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongSanCaiWnd, "m_PlayerHead")
function LuaDaFuWongSanCaiWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_PlayerHead = self.transform:Find("Anchor/Content/PlayerHead").gameObject
end

function LuaDaFuWongSanCaiWnd:Init()
    local Money = LuaDaFuWongMgr.CurRoundArgs or 0
    self.DesLabel.text = g_MessageMgr:FormatMessage("DaFuWeng_SanCaiDes",Money)
    local totalTime = DaFuWeng_Countdown.GetData(EnumDaFuWengStage.RandomEventAverage).Time 
    local CountDown = totalTime
    local endFunc = function() CUIManager.CloseUI(CLuaUIResources.DaFuWongSanCaiWnd) end
	local durationFunc = function(time) end
    SoundManager.Inst:PlayOneShot("event:/L10/L10_UI/DaFuWeng/UI_DaFuWeng_PoCaiMianZai", Vector3.zero, nil, 0)
	LuaDaFuWongMgr:StartCountDownTick(EnumDaFuWengCountDownStage.RandomEvent,CountDown,durationFunc,endFunc)
    self:ShowPlayerHead()
end
function LuaDaFuWongSanCaiWnd:ShowPlayerHead()
    if LuaDaFuWongMgr.IsMyRound then
        self.m_PlayerHead.gameObject:SetActive(false)
    else
        self.m_PlayerHead.gameObject:SetActive(true)
        local playerData = LuaDaFuWongMgr.PlayerInfo and LuaDaFuWongMgr.PlayerInfo[LuaDaFuWongMgr.CurPlayerRound]
        local Portrait = self.m_PlayerHead.transform:Find("Portrait"):GetComponent(typeof(CUITexture))
        local bg = self.m_PlayerHead.transform:Find("BG"):GetComponent(typeof(CUITexture))
        local Name = self.m_PlayerHead.transform:Find("Name"):GetComponent(typeof(UILabel))
        if playerData then
            Portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(playerData.class, playerData.gender, -1), false)
			bg:LoadMaterial(LuaDaFuWongMgr:GetHeadBgPath(playerData.round))
			Name.transform:Find("Texture"):GetComponent(typeof(CUITexture)):LoadMaterial(LuaDaFuWongMgr:GetNameBgPath(playerData.round))
            Name.text = playerData.name
        end
    end
end
function LuaDaFuWongSanCaiWnd:OnStageUpdate(CurStage)
    if CurStage ~= EnumDaFuWengStage.RandomEventAverage then
        CUIManager.CloseUI(CLuaUIResources.DaFuWongSanCaiWnd)
    end
end

function LuaDaFuWongSanCaiWnd:OnEnable()
    g_ScriptEvent:AddListener("OnDaFuWengStageUpdate",self,"OnStageUpdate")
end

function LuaDaFuWongSanCaiWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnDaFuWengStageUpdate",self,"OnStageUpdate")
    LuaDaFuWongMgr:EndCountDownTick(EnumDaFuWengCountDownStage.RandomEvent)
end
--@region UIEvent

--@endregion UIEvent

