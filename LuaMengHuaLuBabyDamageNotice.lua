require("common/common_include")

local Time = import "UnityEngine.Time"
local Vector3 = import "UnityEngine.Vector3"

LuaMengHuaLuBabyDamageNotice = class()

RegistChildComponent(LuaMengHuaLuBabyDamageNotice, "Template", UILabel)

-- 相关数据
RegistClassMember(LuaMengHuaLuBabyDamageNotice, "Items")
RegistClassMember(LuaMengHuaLuBabyDamageNotice, "AliveDurations")
RegistClassMember(LuaMengHuaLuBabyDamageNotice, "Notices") -- damage

RegistClassMember(LuaMengHuaLuBabyDamageNotice, "NoticeInterval")
RegistClassMember(LuaMengHuaLuBabyDamageNotice, "LastUpdateTime")
RegistClassMember(LuaMengHuaLuBabyDamageNotice, "Inited")
RegistClassMember(LuaMengHuaLuBabyDamageNotice, "NoticeShowInterval")

function LuaMengHuaLuBabyDamageNotice:Init()
	self.Items = {}
	self.AliveDurations = {}

	self.Notices = {}
	self.NoticeInterval = 0.8
	self.NoticeShowInterval = 1
	self.LastUpdateTime = 0

	self.Template.gameObject:SetActive(false)

	self.Inited = true
end

function LuaMengHuaLuBabyDamageNotice:ShowBabyHpChange(grid, hpChange)
	if grid == 0 and hpChange < 0 then
		table.insert(self.Notices, hpChange)
	end
end

function LuaMengHuaLuBabyDamageNotice:GetNotice()
	if self.Notices and #self.Notices > 0 then
		return table.remove(self.Notices, 1)
	end
	return nil
end

function LuaMengHuaLuBabyDamageNotice:OnEnable()
	g_ScriptEvent:AddListener("ShowCharacterHpChange", self, "ShowBabyHpChange")
end

function LuaMengHuaLuBabyDamageNotice:OnDisable()
	g_ScriptEvent:RemoveListener("ShowCharacterHpChange", self, "ShowBabyHpChange")
end

function LuaMengHuaLuBabyDamageNotice:Update()

	if not self.Inited then
		return
	end

	for i = 1, #self.Items do
		self.AliveDurations[i] = self.AliveDurations[i] - Time.deltaTime
		if self.AliveDurations[i] >= 0 then
			local item = self.Items[i]
			local percent = self.AliveDurations[i] / self.NoticeShowInterval
			local positionY = -70 * percent + 30
			local scale = 0.5 * percent + 0.5
			local alpha = percent
			item.transform.localPosition = Vector3(0, positionY, 0)
			item.transform.localScale = Vector3(scale, scale, 1)
			item.alpha = percent
		end
	end

	for i = #self.Items, 1, -1 do
		if self.AliveDurations[i] < 0 then
			local item = table.remove(self.Items, i)
			table.remove(self.AliveDurations, i)
			GameObject.Destroy(item.gameObject)
		end
	end

	if Time.realtimeSinceStartup - self.LastUpdateTime < self.NoticeInterval then
		return
	end
	self.LastUpdateTime = Time.realtimeSinceStartup

	local notice = self:GetNotice()
	if notice then
		local item = NGUITools.AddChild(self.gameObject, self.Template.gameObject)
		item:SetActive(true)
		local itemLabel = item:GetComponent(typeof(UILabel))
		itemLabel.text = tostring(notice)
		table.insert(self.Items, itemLabel)
		table.insert(self.AliveDurations, self.NoticeShowInterval)
	end
end

return LuaMengHuaLuBabyDamageNotice