local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CLoginMgr = import "L10.Game.CLoginMgr"
local ETickType = import "L10.Engine.ETickType"
local CTickMgr = import "L10.Engine.CTickMgr"

CLuaShuangshiyi2020ClearTrolleyResultWnd = class()
RegistClassMember(CLuaShuangshiyi2020ClearTrolleyResultWnd,"m_ShareBtn")
RegistClassMember(CLuaShuangshiyi2020ClearTrolleyResultWnd,"m_PlayerInfoNode")
RegistClassMember(CLuaShuangshiyi2020ClearTrolleyResultWnd,"m_ScoreLabel")
RegistClassMember(CLuaShuangshiyi2020ClearTrolleyResultWnd,"m_RewardText")
RegistClassMember(CLuaShuangshiyi2020ClearTrolleyResultWnd,"m_Tick")

function CLuaShuangshiyi2020ClearTrolleyResultWnd:Init()
    self.m_ShareBtn = self.transform:Find("Anchor/ShareBtn").gameObject
    self.m_PlayerInfoNode = self.transform:Find("Anchor/PlayerInfoNode").gameObject
	self.m_ScoreLabel = self.transform:Find("Anchor/Info/ScoreLabel"):GetComponent(typeof(UILabel))
	self.m_RewardText = self.transform:Find("Anchor/Info/Label"):GetComponent(typeof(UILabel))

	local onShareClick = function(go)
		CUICommonDef.CaptureScreenAndShare()
	end
	CommonDefs.AddOnClickListener(self.m_ShareBtn,DelegateFactory.Action_GameObject(onShareClick),false)

	if CLoginMgr.Inst then
		local myGameServer = CLoginMgr.Inst:GetSelectedGameServer()
		CommonDefs.GetComponent_Component_Type(self.m_PlayerInfoNode.transform:Find("Server"), typeof(UILabel)).text = myGameServer.name
	end

	if CClientMainPlayer.Inst then
		CommonDefs.GetComponent_Component_Type(self.m_PlayerInfoNode.transform:Find("Name"), typeof(UILabel)).text = CClientMainPlayer.Inst.Name
		CommonDefs.GetComponent_Component_Type(self.m_PlayerInfoNode.transform:Find("Icon/Lv"), typeof(UILabel)).text = tostring(CClientMainPlayer.Inst.Level)
		CommonDefs.GetComponent_Component_Type(self.m_PlayerInfoNode.transform:Find("Icon"), typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(CClientMainPlayer.Inst.Class, CClientMainPlayer.Inst.Gender, -1), false)
		CommonDefs.GetComponent_Component_Type(self.m_PlayerInfoNode.transform:Find("ID"), typeof(UILabel)).text = tostring(CClientMainPlayer.Inst.Id)
		CommonDefs.GetComponent_Component_Type(self.m_PlayerInfoNode.transform:Find("Server"), typeof(UILabel)).text = CClientMainPlayer.Inst:GetMyServerName()
	elseif CLuaShuangshiyi2020Mgr.m_Name then
		CommonDefs.GetComponent_Component_Type(self.m_PlayerInfoNode.transform:Find("Name"), typeof(UILabel)).text = CLuaShuangshiyi2020Mgr.m_Name
		CommonDefs.GetComponent_Component_Type(self.m_PlayerInfoNode.transform:Find("Icon/Lv"), typeof(UILabel)).text = tostring(CLuaShuangshiyi2020Mgr.m_Level)
		CommonDefs.GetComponent_Component_Type(self.m_PlayerInfoNode.transform:Find("Icon"), typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(CLuaShuangshiyi2020Mgr.m_Class, CLuaShuangshiyi2020Mgr.m_Gender, -1), false)
		CommonDefs.GetComponent_Component_Type(self.m_PlayerInfoNode.transform:Find("ID"), typeof(UILabel)).text = tostring(CLuaShuangshiyi2020Mgr.m_Id)
		CommonDefs.GetComponent_Component_Type(self.m_PlayerInfoNode.transform:Find("Server"), typeof(UILabel)).text = CLuaShuangshiyi2020Mgr.m_ServerName
	end

	if CLuaShuangshiyi2020Mgr.m_Credit and CLuaShuangshiyi2020Mgr.m_RewardItemId ~= 0 then
		local rewardDic = Double11_Setting.GetData().RewardInfo
		local rewardLevel = 1
		CommonDefs.DictIterate(rewardDic, DelegateFactory.Action_object_object(function(key,val)
			if CLuaShuangshiyi2020Mgr.m_Credit > key then
				rewardLevel = rewardLevel + 1
			end
		end))
		if rewardLevel>5 then
			rewardLevel = 5
		end
		local colorList = {"[FFFE91]", "[FA800A]", "[519FFF]", "[FF5050]", "[FF88FF]"}
		local rewardStr = g_MessageMgr:FormatMessage("CLEARTROLLEY_REWARD0" .. rewardLevel)
		rewardStr = colorList[rewardLevel] .. rewardStr

		self.m_ScoreLabel.text = tostring(CLuaShuangshiyi2020Mgr.m_Credit)
		self.m_RewardText.text = rewardStr
	elseif CLuaShuangshiyi2020Mgr.m_RewardItemId == 0 then
		local rewardStr = g_MessageMgr:FormatMessage("CLEARTROLLEY_REWARD_LIMIT")
		rewardStr = "[FFFE91]" .. rewardStr
		self.m_ScoreLabel.text = tostring(CLuaShuangshiyi2020Mgr.m_Credit)
		self.m_RewardText.text = rewardStr
	end

	local result = CLuaShuangshiyi2020Mgr.m_Credit
	local interval = math.ceil(result / 300 + 1)
	local temp = 1

	self.m_Tick = CTickMgr.Register(DelegateFactory.Action(function () 
		if temp >= result then
			temp = result
			if self.m_Tick ~= nil then
				invoke(self.m_Tick)
			end
		end
		self.m_ScoreLabel.text = tostring(temp)
		temp = temp + interval
	end), 5, ETickType.Loop)
end

function CLuaShuangshiyi2020ClearTrolleyResultWnd:OnDisable()
	if self.m_Tick~=nil then
		UnRegisterTick(self.m_Tick)
	end
end
