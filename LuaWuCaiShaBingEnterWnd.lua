require("common/common_include")

local MessageMgr = import "L10.Game.MessageMgr"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local UIScrollView = import "UIScrollView"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"
local Extensions = import "Extensions"

CLuaWuCaiShaBingEnterWnd = class()

RegistClassMember(CLuaWuCaiShaBingEnterWnd, "m_ApplyBtn")
RegistClassMember(CLuaWuCaiShaBingEnterWnd, "m_DismissBtn")

function CLuaWuCaiShaBingEnterWnd:Init()
	local descLabel = self.transform:Find("Anchor/Desc/Label"):GetComponent(typeof(UILabel))
	descLabel.text = MessageMgr.Inst:FormatMessage("TIP_WUCAISHABING", {})

	-- 初始化规则说明
	local ruleTable = self.transform:Find("Anchor/Rules/ContentScrollView/Table"):GetComponent(typeof(UITable))
	local ruleTemplate = self.transform:Find("Anchor/Rules/ContentScrollView/ParagraphTemplate").gameObject
	local ruleScrollView = self.transform:Find("Anchor/Rules/ContentScrollView"):GetComponent(typeof(UIScrollView))

	Extensions.RemoveAllChildren(ruleTable.transform)
	local ruleMessage = MessageMgr.Inst:FormatMessage("Wucaishabin_Tip", {})
	local ruleTipInfo = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(ruleMessage)
	for i = 0, ruleTipInfo.paragraphs.Count - 1 do
		local template = CUICommonDef.AddChild(ruleTable.gameObject, ruleTemplate)
		template:GetComponent(typeof(CTipParagraphItem)):Init(ruleTipInfo.paragraphs[i], 0xffffffff)
		template:SetActive(true)
	end
	ruleTable:Reposition()
	ruleScrollView:ResetPosition()

	-- 初始化神坛说明
	local templeTable = self.transform:Find("Anchor/Temple/ContentScrollView/Table"):GetComponent(typeof(UITable))
	local templeTemplate = self.transform:Find("Anchor/Temple/ContentScrollView/ParagraphTemplate").gameObject
	local templeScrollView = self.transform:Find("Anchor/Temple/ContentScrollView"):GetComponent(typeof(UIScrollView))

	Extensions.RemoveAllChildren(templeTable.transform)
	local templeMessage = MessageMgr.Inst:FormatMessage("TIP_WUCAISHABING_SKILL", {})
	local templeTipInfo = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(templeMessage)
	for i = 0, templeTipInfo.paragraphs.Count - 1 do
		local template = CUICommonDef.AddChild(templeTable.gameObject, templeTemplate)
		template:GetComponent(typeof(CTipParagraphItem)):Init(templeTipInfo.paragraphs[i], 0xffffffff)
		template:SetActive(true)
	end
	templeTable:Reposition()
	templeScrollView:ResetPosition()

	-- 注册按钮点击事件
	self.m_ApplyBtn = self.transform:Find("Anchor/ApplyView/ApplyBtn").gameObject
	CommonDefs.AddOnClickListener(self.m_ApplyBtn, DelegateFactory.Action_GameObject(function()
		Gac2Gas.RequestSignUpWuCaiShaBing()
	end), false)

	self.m_DismissBtn = self.transform:Find("Anchor/ApplyView/DismissBtn").gameObject
	CommonDefs.AddOnClickListener(self.m_DismissBtn, DelegateFactory.Action_GameObject(function()
		Gac2Gas.RequestCancelSignUpWuCaiShaBing()
	end), false)

	local remainTimes = LuaWuCaiShaBingMgr.m_RemainTimes
	local playStatus = LuaWuCaiShaBingMgr.m_PlayStatus

	local label = self.transform:Find("Anchor/ApplyView/Award/RemainAwardCount"):GetComponent(typeof(UILabel))
	if remainTimes > 0 then
		label.text = SafeStringFormat3("[00FF00]%d[-]", remainTimes)
	else
		label.text = SafeStringFormat3("[FF0000]%d[-]", remainTimes)
	end

	if playStatus then
		self.m_ApplyBtn:SetActive(false)
		self.m_DismissBtn:SetActive(true)
	else
		self.m_ApplyBtn:SetActive(true)
		self.m_DismissBtn:SetActive(false)
	end
end

return CLuaWuCaiShaBingEnterWnd