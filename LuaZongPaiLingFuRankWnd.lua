local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local GameObject = import "UnityEngine.GameObject"
local EnumClass = import "L10.Game.EnumClass"
local Profession = import "L10.Game.Profession"
local AlignType = import "CPlayerInfoMgr+AlignType"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local DelegateFactory = import "DelegateFactory"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaZongPaiLingFuRankWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZongPaiLingFuRankWnd, "ContentView", "ContentView", QnAdvanceGridView)
RegistChildComponent(LuaZongPaiLingFuRankWnd, "RemainTimeLabel", "RemainTimeLabel", UILabel)
RegistChildComponent(LuaZongPaiLingFuRankWnd, "LingFuCountLabel", "LingFuCountLabel", UILabel)
RegistChildComponent(LuaZongPaiLingFuRankWnd, "LingFuBtn", "LingFuBtn", GameObject)
RegistChildComponent(LuaZongPaiLingFuRankWnd, "RuleBtn", "RuleBtn", GameObject)

--@endregion RegistChildComponent end

function LuaZongPaiLingFuRankWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.LingFuBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLingFuBtnClick()
	end)

	UIEventListener.Get(self.RuleBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleBtnClick()
	end)

    --@endregion EventBind end
end

function LuaZongPaiLingFuRankWnd:Init()
	-- 请求
	if CClientMainPlayer.Inst then
		Gac2Gas.QuerySectFuWenRank_Ite(CClientMainPlayer.Inst.BasicProp.SectId)
	end

	self.RankList = {}

    self.ContentView.m_DataSource = DefaultTableViewDataSource.Create(function()
        return #self.RankList
    end, function(item, index)
        local data = self.RankList[index+1]
        self:InitRankItem(item, data, index)
    end)

    -- 设置选中状态
    self.ContentView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        -- 显示玩家信息
        local data = self.RankList[row+1]
        CPlayerInfoMgr.ShowPlayerPopupMenu(data.PlayerId, EnumPlayerInfoContext.Undefined, 
            EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
    end)
end

-- Key:Score Difficulty1 Difficulty2 Difficulty3 playerId name updateTime overdueTime
function LuaZongPaiLingFuRankWnd:InitRankItem(item, data, index)
	local playerNameLabel = item.transform:Find("PlayerNameLabel"):GetComponent(typeof(UILabel))
	local timeLabel = item.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
	local zhiWeiLabel = item.transform:Find("ZhiWeiLabel"):GetComponent(typeof(UILabel))
	local countLabel = item.transform:Find("CountLabel"):GetComponent(typeof(UILabel))
	local selfMark = item.transform:Find("SelfMark").gameObject
	local bg = item.transform:GetComponent(typeof(UISprite))

	local myId = CClientMainPlayer.Inst.Id
	selfMark:SetActive(false)

	if data.PlayerId == myId then
		selfMark:SetActive(true)
		bg.spriteName = "common_bg_mission_background_highlight"
	elseif (tonumber(index) % 2) == 0 then
		bg.spriteName = "common_bg_mission_background_n"
	else
		bg.spriteName = "common_bg_mission_background_s"
	end
    
	playerNameLabel.text = data.Name
	countLabel.text = data.RecentGetCount

	local date = CServerTimeMgr.ConvertTimeStampToZone8Time(data.LastGetTime)
	timeLabel.text = date:ToString("yyyy/MM/dd HH:mm")
	zhiWeiLabel.text = Menpai_Title.GetData(data.ShowTitle).Name
end

function LuaZongPaiLingFuRankWnd:OnData(sectId, rankData, effectExpireTime, todayLingfu)
    self.RankList = {}

	CommonDefs.DictIterate(rankData.Infos, DelegateFactory.Action_object_object(function (___key, ___value)
		table.insert( self.RankList, ___value)
	end)) 

	local myId = CClientMainPlayer.Inst.Id
	-- 排序
	table.sort(self.RankList, function(a,b)
		if a.PlayerId == myId or b.PlayerId == myId then
			return a.PlayerId == myId
		elseif a.RecentGetCount ~= b.RecentGetCount then
			return a.RecentGetCount > b.RecentGetCount
		else
			return a.PlayerId > b.PlayerId
		end
	end)

	self.ContentView:ReloadData(true,false)

	local restTime = effectExpireTime - math.floor(CServerTimeMgr.Inst.timeStamp)

	if restTime >0 then
		local timeText = SafeStringFormat3(LocalString.GetString("%02d:%02d:%02d"), math.floor(restTime / 3600), math.floor((restTime % 3600) / 60), restTime % 60)
		self.RemainTimeLabel.text = timeText
	else
		self.RemainTimeLabel.text = LocalString.GetString("无")
	end

	local times = Menpai_Setting.GetData("LingfuInstanceEnterTimes").Value
	self.LingFuCountLabel.text = SafeStringFormat3("%s/%s", todayLingfu, times)
end

function LuaZongPaiLingFuRankWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncSectFuWenRank_Ite", self, "OnData")
end

function LuaZongPaiLingFuRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncSectFuWenRank_Ite", self, "OnData")
end


--@region UIEvent

function LuaZongPaiLingFuRankWnd:OnLingFuBtnClick()
	if CClientMainPlayer.Inst then
		Gac2Gas.RequestEnterSectLingfuPlay_Ite(CClientMainPlayer.Inst.BasicProp.SectId)
	end
end

function LuaZongPaiLingFuRankWnd:OnRuleBtnClick()
	g_MessageMgr:ShowMessage("ZongPai_LingFu_Wnd_Desc")
end


--@endregion UIEvent

