local CTopAndRightMenuWnd=import "L10.UI.CTopAndRightMenuWnd"
local CScene=import "L10.Game.CScene"
local CIndirectUIResources=import "L10.UI.CIndirectUIResources"
local CQuanMinPKMgr=import "L10.Game.CQuanMinPKMgr"
local CBiWuDaHuiMgr=import "L10.Game.CBiWuDaHuiMgr"
local CUIResources = import "L10.UI.CUIResources"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Constants = import "L10.Game.Constants"
local CFightingSpiritMgr = import "L10.Game.CFightingSpiritMgr"

EnumContentNodeState = {
    Default = 0,
    Show = 1,
    Hide = 2,
}

LuaTopAndRightMenuWndMgr = {}
LuaTopAndRightMenuWndMgr.m_GamePlayId = 0
LuaTopAndRightMenuWndMgr.m_sceneTemplateId = 0
LuaTopAndRightMenuWndMgr.CurrentUI = nil
LuaTopAndRightMenuWndMgr.ContentNodeState = EnumContentNodeState.Default
-- 玩法中的TopAndRight
LuaTopAndRightMenuWndMgr.s_PlayId2TopAndRightWnd = nil
LuaTopAndRightMenuWndMgr.s_SceneTemplateId2TopAndRightWnd = nil
--[[    
    新的TopAndRightwnd请在gameplay中注册
    1. 一个TopAndRightwnd关联一个或多个gameplayID或sceneTemplateID
    2. 保证同一时刻只显示一种TopAndRightWnd
    3. 自己其他的特殊处理可以在玩法GamePlayMgr的OnMainPlayerCreate中处理
]]--
function LuaTopAndRightMenuWndMgr.GetGamePlayUI(wnd)
    local playId = LuaTopAndRightMenuWndMgr.m_GamePlayId
    local sceneTemplateId = LuaTopAndRightMenuWndMgr.m_sceneTemplateId
    
    if  sceneTemplateId > 0 and playId > 0 and LuaTopAndRightMenuWndMgr.s_PlayId2TopAndRightWnd and LuaTopAndRightMenuWndMgr.s_PlayId2TopAndRightWnd[playId] then
        local wndInfo = LuaTopAndRightMenuWndMgr.s_PlayId2TopAndRightWnd[playId]
        if type(wndInfo) == "string" then return wndInfo end
        if wndInfo.showContentNode then LuaTopAndRightMenuWndMgr.ContentNodeState = EnumContentNodeState.Show end
        if wndInfo.ui then return wndInfo.ui end
    end

    if sceneTemplateId > 0 and LuaTopAndRightMenuWndMgr.s_SceneTemplateId2TopAndRightWnd and LuaTopAndRightMenuWndMgr.s_SceneTemplateId2TopAndRightWnd[sceneTemplateId] then
        local wndInfo = LuaTopAndRightMenuWndMgr.s_SceneTemplateId2TopAndRightWnd[sceneTemplateId]
        if type(wndInfo) == "string" then return wndInfo end
        if wndInfo.showContentNode then LuaTopAndRightMenuWndMgr.ContentNodeState = EnumContentNodeState.Show end
        if wndInfo.ui then return wndInfo.ui end
    end

    LuaGuildCustomBuildMgr:PreTask1TopRightWnd(wnd)
    LuaHuLuWa2022Mgr:RuYiDongTopAndRightInit(wnd)
    LuaZongMenMgr:TopAndRightMenuWndInit(wnd)
    LuaGuildTerritorialWarsMgr:ShowTopRightWnd(wnd)
    
    local ui = nil
    for k,v in pairs(g_TopAndRight) do
        local reswnd = v(wnd)
        if reswnd then 
            ui = reswnd
            break
        end
    end
    return ui
end

CTopAndRightMenuWnd.m_TopAndRightMenuWndInit=function(wnd)
    local custom=false
    local gamePlayId=0
    local sceneTemplateId=0
    if CScene.MainScene then
        gamePlayId=CScene.MainScene.GamePlayDesignId
        sceneTemplateId=CScene.MainScene.SceneTemplateId
    end
    LuaTopAndRightMenuWndMgr.m_GamePlayId = gamePlayId
    LuaTopAndRightMenuWndMgr.m_sceneTemplateId = sceneTemplateId

    ----- 特殊处理 -----
    -- 特殊处理，同时打开了两个TopRightUIWnd, 请尽量不要加类似的同时打开两个TopAndRightWnd的操作
    if gamePlayId == 51101100 then
        wnd.contentNode:SetActive(false)
        CUIManager.ShowUI(CLuaUIResources.ChildrenDayPlayTopRightWnd)
        CUIManager.ShowUI(CLuaUIResources.ChildrenDayPlayOperateWnd)
        CUIManager.CloseUI(CUIResources.ThumbnailChatWindow)
    else
        CUIManager.CloseUI(CLuaUIResources.ChildrenDayPlayTopRightWnd)
        CUIManager.CloseUI(CLuaUIResources.ChildrenDayPlayOperateWnd)
        --CUIManager.ShowUI(CUIResources.ThumbnailChatWindow)
    end

    if not CBiWuDaHuiMgr.Inst.inBiWu and not CFightingSpiritMgr.Instance:IsDouhunFighting() then
        LuaColiseumMgr:ShowTopRightWnd(wnd) 
    end  
    LuaXinBaiLianDongMgr:ShowTopRightWnd(wnd) 
    LuaHuanHunShanZhuangMgr:ShowTopRightWnd(wnd)
    LuaWeddingIterationMgr:ShowTopRightWnd(wnd)
    ----- 特殊处理 -----

    LuaTopAndRightMenuWndMgr.ContentNodeState = EnumContentNodeState.Default
    local ui = LuaTopAndRightMenuWndMgr.GetGamePlayUI(wnd)
    if type(ui) == "string" then ui = CLuaUIResources[ui] end
    if ui then
        if LuaTopAndRightMenuWndMgr.ContentNodeState ~= EnumContentNodeState.Show then
            wnd.contentNode:SetActive(false)
        end
        if ui == LuaTopAndRightMenuWndMgr.CurrentUI then
            return
        else
            if LuaTopAndRightMenuWndMgr.CurrentUI then
                CUIManager.CloseUI(LuaTopAndRightMenuWndMgr.CurrentUI)
            end
            CUIManager.ShowUI(ui)
            LuaTopAndRightMenuWndMgr.CurrentUI = ui
        end
        return
    else
        if LuaTopAndRightMenuWndMgr.ContentNodeState == EnumContentNodeState.Hide then
            wnd.contentNode:SetActive(false)
        end
        
        if LuaTopAndRightMenuWndMgr.CurrentUI then
            CUIManager.CloseUI(LuaTopAndRightMenuWndMgr.CurrentUI)
        end
        LuaTopAndRightMenuWndMgr.CurrentUI = nil
        return
    end

end

--@desc 在需要显示福利按钮提示的情况下return true
CTopAndRightMenuWnd.m_hookUpdateSigninAlertFx = function (wnd)
    -- 转职基金
    -- 未购买且需要提示alert
    -- 已购买且有物品可以领取(时间内)
    if LuaProfessionTransferMgr.FundEndTime ~= 0 or LuaProfessionTransferMgr.FundTaskExpireTime ~= 0 then
        if not LuaProfessionTransferMgr.FundPurchased and LuaProfessionTransferMgr.FundShowAlert and CServerTimeMgr.Inst.timeStamp < LuaProfessionTransferMgr.FundEndTime then
            return true
        elseif LuaProfessionTransferMgr.FundPurchased and LuaProfessionTransferMgr.HasFundAwardToTake() and CServerTimeMgr.Inst.timeStamp < LuaProfessionTransferMgr.FundTaskExpireTime then
            return true
        end
    end

    -- 回流基金
    -- 未购买且需要提示alert
    -- 已购买且有物品可以领取（无时间限制）
    if LuaHuiLiuMgr.FundExpiredTime ~= 0 then
        if not LuaHuiLiuMgr.FundPurchased and LuaHuiLiuMgr.FundShowAlert and LuaHuiLiuMgr.FundExpiredTime > CServerTimeMgr.Inst.timeStamp then
            return true
        elseif LuaHuiLiuMgr.FundPurchased and LuaHuiLiuMgr.HasFundAwardToTake() then
            return true
        end
    end

    if LuaWelfareMgr:CanShowUnity2018UpgradeAlert() then
        return true
    end

    if LuaWelfareMgr.IsOpenBigMonthCard and (LuaWelfareMgr:CheckNewMainWelfareAlert() or LuaWelfareMgr:CheckMultipleExpAlert()) then
        return true
    end

    return false
end
-- 上线的时候清理下缓存
function LuaTopAndRightMenuWndMgr:OnPlayerLogin()
    LuaTopAndRightMenuWndMgr.CurrentUI = nil
    LuaTopAndRightMenuWndMgr.ContentNodeState = EnumContentNodeState.Default
end
-- function LuaTopAndRightMenuWndMgr:ShowTopRightWndForGameplay(wnd, uiModule, gamePlayId)
--     self:ShowTopRightWnd(wnd, uiModule,function ()
--         return self.m_GamePlayId == gamePlayId
--     end)
-- end

function LuaTopAndRightMenuWndMgr:ShowTopRightWnd(wnd, uiModule,isShowWndFunc)
    g_TopAndRight[tostring(uiModule.Name)] = function(wnd)
        if isShowWndFunc() then
            return uiModule
        end
    end
end

g_TopAndRight = {}

g_TopAndRight["HideContentNode"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == 51102971 or
    LuaTopAndRightMenuWndMgr.m_GamePlayId == 51102972 then
        LuaTopAndRightMenuWndMgr.ContentNodeState = EnumContentNodeState.Hide
    end
end

g_TopAndRight["FightingSpiritDamageWnd"] = function(wnd)
    local show = false
    local gpid = LuaTopAndRightMenuWndMgr.m_GamePlayId
    if gpid == 51100165 or gpid == 51101092 then
        show = true
    else 
        if CBiWuDaHuiMgr.Inst.inBiWu then
            local stid = LuaTopAndRightMenuWndMgr.m_sceneTemplateId
            if stid ~= Constants.BiWuWatchTemplateSceneId and stid ~= 16000492 then 
                --比武观战场景中排除 16000492场景(跨服观战)中排除，观战中不显示FightingSpiritDamageWnd
                show = true
            end
        end
    end
    if show then
        return CUIResources.FightingSpiritDamageWnd
    end
end

g_TopAndRight["GSNZPlayWnd"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == ZhongYuanJie2022Mgr.GSNZGamePlayId then
        return CLuaUIResources.GSNZPlayWnd
    end
end
-- 端午大作战
g_TopAndRight["DuanWuDaZuoZhanTopRightWnd"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == 51100795 then
        return "DuanWuDaZuoZhanTopRightWnd"
    end
end
-- 儿童节捉鸡比赛
g_TopAndRight["ZhuojiTopRightWnd"] =  function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == 51100803 or LuaTopAndRightMenuWndMgr.m_GamePlayId == 51100804 then
        return "ZhuojiTopRightWnd"
    end
end
-- 五彩沙冰副本
g_TopAndRight["WuCaiShaBingStateWnd"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == 51100816 then
        return "WuCaiShaBingStateWnd"
    end
end
-- 全民pk
g_TopAndRight["QMPK"] = function(wnd)
    if CQuanMinPKMgr.Inst.m_IsQuanMinPKServer then
        LuaTopAndRightMenuWndMgr.ContentNodeState = EnumContentNodeState.Hide
        if LuaTopAndRightMenuWndMgr.m_GamePlayId == 51100444 then
            return CLuaUIResources.QMPKDamageWnd
        elseif LuaTopAndRightMenuWndMgr.m_GamePlayId == 51100102 then
            return nil
        else
            return CIndirectUIResources.QMPKTopRightMenu
        end
    end
end
--关宁校场或训练场
g_TopAndRight["GuanNingStateWnd"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == 51100208 or  LuaTopAndRightMenuWndMgr.m_GamePlayId == 51100101 then
        return CUIResources.GuanNingStateWnd
    end
end
-- 国庆校场
g_TopAndRight["GQJCStateWnd"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == 51100864 then
        return "GQJCStateWnd"
    end
end
-- 连连看
g_TopAndRight["LianLianKanTopRightWnd"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == 51100871 then
        return "LianLianKanTopRightWnd"
    end
end
g_TopAndRight["BaoWeiNanGuaTopRightWnd"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == 51100873 then
        return "BaoWeiNanGuaTopRightWnd"
    end
end

g_TopAndRight["DoubleOneQLDTopRightWnd"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == 51101258 then
        return "DoubleOneQLDTopRightWnd"
    end
end

g_TopAndRight["DoubleOneBaotuanTopRightWnd"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == 51101251 then
        return "DoubleOneBaotuanTopRightWnd"
    end
end

g_TopAndRight["YuanxiaoTopRightWnd"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == 51100922 then
        return "YuanxiaoTopRightWnd"
    end
end

g_TopAndRight["CityWarMonsterSiegeWnd"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == MonsterSiege_Setting.GetData().GamePlayId then
        return "CityWarMonsterSiegeWnd"
    end
end

g_TopAndRight["TianMenShanBattleStateWnd"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == 51100898 then
        return "TianMenShanBattleStateWnd"
    end
end
g_TopAndRight["WaKuangStateWnd"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == 51100996 then
        return "WaKuangStateWnd"
    end
end

g_TopAndRight["ChildrenDayPlayTopRight2020Wnd"] = function(wnd)
    if LuaChildrenDay2020Mgr.InGamePlay() then
        return CLuaUIResources.ChildrenDayPlayTopRight2020Wnd
    end
end
--2019端午节进击的粽子活动
g_TopAndRight["DwZongziTopRightMenu"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == DuanWu_Setting.GetData().ZongZi2019GamePlayId then
        return "DwZongziTopRightMenu"
    end
end
 --无间地狱
g_TopAndRight["WuJianDiYuTopRightWnd"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == LuaWuJianDiYuMgr.gamePlayId then
        return CLuaUIResources.WuJianDiYuTopRightWnd
    end
end
--月神殿
g_TopAndRight["ZQYueShenDianTopRightWnd"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == 51101137 then
        return "ZQYueShenDianTopRightWnd"
    end
end
--万圣节2019 bug围场
g_TopAndRight["BugWeiChangTopRightWnd"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == 51101250 then
        return "BugWeiChangTopRightWnd"
    end
end
--万圣节2019 染色剂大暴走
g_TopAndRight["CrazyDyeTopRightWnd"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == 51101253 then
        return "CrazyDyeTopRightWnd"
    end
end
--拯救圣诞树
g_TopAndRight["RescueChristmasTreeStateWnd"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == 51101263 then
        return "RescueChristmasTreeStateWnd"
    end
end
--礼物大作战
g_TopAndRight["GrabGiftTopRightWnd"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == 51101260 then
        return "GrabGiftTopRightWnd"
    end
end

g_TopAndRight["SanxingTopRightWnd"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == SanXingBattle_Setting.GetData().GamePlayId then
        return CLuaUIResources.SanxingTopRightWnd
    end
end

g_TopAndRight["BaoZhuPlayTopRightWnd"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == Chunjie2021_CareFirework.GetData().PlayDesignId then
        return "BaoZhuPlayTopRightWnd"
    end
end
g_TopAndRight["QingMingNHTQTopRightWnd"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == QingMing2021_Setting.GetData().GamePlayId then
        return CLuaUIResources.QingMingNHTQTopRightWnd
    end
end
g_TopAndRight["OffWorldPlay1TopRightWnd"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == OffWorldPass_Setting.GetData().JiaoshanGameplayId then
        return CLuaUIResources.OffWorldPlay1TopRightWnd
    end
end
g_TopAndRight["TieChunLianTopRightWnd"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == 51101308 then
        return CLuaUIResources.TieChunLianTopRightWnd
    end
end
--元宵2020-怼怼乐
g_TopAndRight["DuiDuiLeGamePlayWnd"] = function(wnd)
    if LuaYuanXiao2020Mgr:CheckInDuiDuiLeGamePlay() then
        return CLuaUIResources.DuiDuiLeGamePlayWnd
    end
end
--  情意相通
g_TopAndRight["QYXTTopRightWnd"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == 51101414 then
        return CLuaUIResources.QYXTTopRightWnd
    end
end
--清明解字消愁
g_TopAndRight["QingMingJieZiTipWnd"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == 51101501 then
        LuaTopAndRightMenuWndMgr.ContentNodeState = EnumContentNodeState.Show
        return CLuaUIResources.QingMingJieZiTipWnd
    end
end
--五一2020-地狱誓空 
g_TopAndRight["DiYuShiKongStateWnd"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == 51101530 then
        return CLuaUIResources.DiYuShiKongStateWnd
    end
end
--五一2020-烽火令
g_TopAndRight["QMHuoYunFHLStateWnd"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == 51101532 then
        return CLuaUIResources.QMHuoYunFHLStateWnd
    end 
end
--安期岛之战
g_TopAndRight["AnQiDaoStateWnd"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == 51101849 then
        return CLuaUIResources.AnQiDaoStateWnd
    end
end
g_TopAndRight["Shuangshiyi2020ClearTrolleyPlayingWnd"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == Double11_Setting.GetData().ClearTrolleyGamePlayId then
        if not CUIManager.IsLoaded(CLuaUIResources.Shuangshiyi2020ClearTrolleyPlayingWnd) then
            return CLuaUIResources.Shuangshiyi2020ClearTrolleyPlayingWnd
        end
    end
end

g_TopAndRight["WuYi2021MingXingPlayingWnd"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == WuYi2021_Setting.GetData().GameplayId then
        return CLuaUIResources.WuYi2021MingXingPlayingWnd
    end
end
--圣诞2020-堆雪人
g_TopAndRight["ChristmasCreateSnowmanStateWnd"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == 51101850 then
        return CLuaUIResources.ChristmasCreateSnowmanStateWnd
    end 
end
g_TopAndRight["PetAdventureStateWnd"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == 51101934 then
        return CLuaUIResources.PetAdventureStateWnd
    end 
end
g_TopAndRight["OffWorldPlay2TopRightWnd"] = function(wnd)
    if LuaOffWorldPassMgr.IsInPlay2() then
        return CLuaUIResources.OffWorldPlay2TopRightWnd
    end
end
-- g_TopAndRight["LuoCha2021GameplayBtnWnd"] = function(wnd)
--     if LuaTopAndRightMenuWndMgr.m_GamePlayId == 51102271 or LuaTopAndRightMenuWndMgr.m_GamePlayId == 51102268 then
--         LuaTopAndRightMenuWndMgr.ContentNodeState = EnumContentNodeState.Show
--         return CLuaUIResources.LuoCha2021GameplayBtnWnd
--     end 
-- end
-- 无量伙伴按钮
g_TopAndRight["WuLiangCompanionBtnWnd"] = function(wnd)
    if LuaWuLiangMgr:IsInWuLiangPrepareScene() then
        LuaTopAndRightMenuWndMgr.ContentNodeState = EnumContentNodeState.Show
        return CLuaUIResources.WuLiangCompanionBtnWnd
    end
end
-- 高昌
g_TopAndRight["GaoChangTopRightWnd"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == GaoChangCross_Setting.GetData().GameplayId then
        return CLuaUIResources.GaoChangTopRightWnd
    end
end
g_TopAndRight["ShuiManJinShanStateWnd"] = function(wnd)
    if LuaShuiManJinShanMgr:IsInGameplay() then
        return CLuaUIResources.ShuiManJinShanStateWnd
    end
end

g_TopAndRight["TurkeyMatchStateWnd"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == Christmas2022_Setting.GetData().GamePlayId and not LuaChristmas2022Mgr:IsTurkeyMatchPlaySecondStage() then
         return CLuaUIResources.TurkeyMatchStateWnd
    end 
 end

 g_TopAndRight["GuildTerritorialWarsChallengeTopRightWnd"] = function(wnd)
    if LuaGuildTerritorialWarsMgr:IsChallengePlayOpen() then
         return CLuaUIResources.GuildTerritorialWarsChallengeTopRightWnd
    end 
 end

g_TopAndRight["ZYPDStateWnd"] = function(wnd)
    if LuaZhongYuanJie2023Mgr:IsInPlay() then
        return CLuaUIResources.ZYPDStateWnd
    end
end

g_TopAndRight["YuanDan2024PVEGridWnd"] = function(wnd)
    if LuaTopAndRightMenuWndMgr.m_GamePlayId == YuanDan2024_Setting.GetData().XNCGGamePlayId then
        return CLuaUIResources.YuanDan2024PVEGridWnd
    end
end
--[[    
    新的TopAndRightwnd请在gameplay中注册
    1. 一个TopAndRightwnd关联一个或多个gameplayID或sceneTemplateID
    2. 保证同一时刻只显示一种TopAndRightWnd
    3. 自己其他的特殊处理可以在玩法GamePlayMgr的OnMainPlayerCreate中处理
]]--
