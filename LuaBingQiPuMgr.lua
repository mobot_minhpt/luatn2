local CCommonItem = import "L10.Game.CCommonItem"
local EquipmentTemplate_Equip=import "L10.Game.EquipmentTemplate_Equip"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CChatMgr = import "L10.Game.CChatMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CPersonalSpace_Equip = import "L10.Game.CPersonalSpace_Equip"
local CPersonalSpace_RoleEquip = import "L10.Game.CPersonalSpace_RoleEquip"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CHTTPForm = import "L10.Game.CHTTPForm"
local Main = import "L10.Engine.Main"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CScheduleMgr = import "L10.Game.CScheduleMgr"
local QnMoneyCostMesssageMgr = import "L10.UI.QnMoneyCostMesssageMgr"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local Json = import "L10.Game.Utils.Json"

EnumBingQiPuMainRankType = {
	eScore	= 1,	-- 分数榜
	eTRenqi	= 2, -- 人气总榜
	eDRenqi	= 3,	-- 每日人气 
}

EnumBingQiPuOpType = {
	eSubmit = 1,
	eQueryForSubmit = 2,
	ePraise = 3,
	eVote = 4,
	eQueryEquip = 5,
	eTryEquip = 6,
	eShareEquip = 7,
	eQueryShare	= 8, -- 查看别人的分享
	eQuerySponso = 9 ,	-- 查看赞助榜(金主榜)
	eInvestSponsor = 10,	-- 投资赞助榜(金主榜)
	eSubimitPrecious = 11,	-- 提交奇珍棒
	eQueryHistory = 12,	-- 查看历史
	eOffShelf = 13, -- 下架
}

EnumBingQiPuStage = {
	eClose = 1,
	eSubmit = 2,
	eVote = 3,
	eSummary = 4,
}

LuaBingQiPuMgr={}

-- 排行榜相关
LuaBingQiPuMgr.m_RankInfo = nil
LuaBingQiPuMgr.m_RankInfo2 = nil
function LuaBingQiPuMgr.GetRank(equipType,weaponType)
	if LuaBingQiPuMgr.m_RankInfo == nil then return nil end
	local edatas = LuaBingQiPuMgr.m_RankInfo[equipType]
	if edatas == nil then return nil end
	if equipType ~= 1 then 
		weaponType = 0
	end
	local wdatas = edatas[weaponType]
	if wdatas == nil then return nil end
    return wdatas
end

--每日推荐
LuaBingQiPuMgr.m_DailyInfo = nil
LuaBingQiPuMgr.m_DailyInfo2 = nil

-- 提交装备相关
LuaBingQiPuMgr.m_EquipToCommit = nil
function LuaBingQiPuMgr.ShowCommitComfirmWnd(equipid,tempid,commitType)
	LuaBingQiPuMgr.m_EquipToCommit ={
		itemId = equipid,
		templateId = tempid,
		CommitType = commitType--0表示普通十大兵器，1表示奇珍
	}
	CUIManager.ShowUI(CLuaUIResources.BQPConfirmCommitWnd)
end

-- 已经提交的数据
LuaBingQiPuMgr.m_EquipsCommited		= {} 		--已提交的十大兵器榜数据,包含普通和奇珍榜

LuaBingQiPuMgr.m_EquipTypesCommited={} -- 已经提交的装备类型
LuaBingQiPuMgr.m_EquipCommitThreshold={}
LuaBingQiPuMgr.m_PreciousRankThreshold = 0

-- 装备类型换算

LuaBingQiPuMgr.m_EquipmentPushCost=nil

-- 装备详情
LuaBingQiPuMgr.m_DetailEquipInfo=nil

-- 点赞相关
LuaBingQiPuMgr.m_LikesLeft = -1
LuaBingQiPuMgr.m_TotleLikes = 0
function LuaBingQiPuMgr.GetLikeStr()
	local str = ""
	if LuaBingQiPuMgr.m_LikesLeft > 0 then
		str = SafeStringFormat3(LocalString.GetString("今日剩余点赞 [00FF00]%d[-]"), LuaBingQiPuMgr.m_LikesLeft)
	else
		str = LocalString.GetString("今日剩余点赞 [FF5050]0[-]")
	end
	return str
end

-- 兵器谱进行的阶段
LuaBingQiPuMgr.m_StageStatus=nil

-- 赞助相关
LuaBingQiPuMgr.m_SponsorList=nil
LuaBingQiPuMgr.m_SelectSponsorItem = nil

-- 推广相关
LuaBingQiPuMgr.m_PromoteEquipInfo=nil
LuaBingQiPuMgr.m_PromoteData = nil
LuaBingQiPuMgr.m_PromoteVoteInfo=nil

-- 兵器谱是否可以评论
LuaBingQiPuMgr.m_CanComment=true

-- 打开助力界面
LuaBingQiPuMgr.m_DonateID = nil
function LuaBingQiPuMgr.ShowDonateWnd(equipid)
	LuaBingQiPuMgr.m_DonateID = equipid
	CUIManager.ShowUI(CLuaUIResources.BQPDonateWnd)
end

-- 数据相关
LuaBingQiPuMgr.EquipTypeNames = nil  --武器大类数据 dic:FstTab-Name
LuaBingQiPuMgr.WeaponTypeCfgs = nil --武器小类数据 dic:SecTab-Name
LuaBingQiPuMgr.m_Equip2TypeId=nil--大小类转id

function LuaBingQiPuMgr.InitEquipTypeCfg(force)
	if not LuaBingQiPuMgr.m_Equip2TypeId or not LuaBingQiPuMgr.EquipTypeNames or force then
		LuaBingQiPuMgr.m_Equip2TypeId = {}
		LuaBingQiPuMgr.EquipTypeNames = {}

		LuaBingQiPuMgr.WeaponTypeCfgs = {}

		LuaBingQiPuMgr.EquipTypeNames[1] = LocalString.GetString("武器")
		BingQiPu_EquipType.Foreach(function (key,typeData)
			local subTypeStr = typeData.SubType
			local eType, eSubType = string.match(subTypeStr, "([^,]+),([^,]+)")
        	eType = tonumber(eType)

        	LuaBingQiPuMgr.m_Equip2TypeId[eType] = LuaBingQiPuMgr.m_Equip2TypeId[eType] or {}
        	if eSubType == "*" then
            	-- 请注意这里!!!
            	LuaBingQiPuMgr.m_Equip2TypeId[eType][0] = key
       		else
           		eSubType = tonumber(eSubType)
            	LuaBingQiPuMgr.m_Equip2TypeId[eType][eSubType] = key
        	end
			if typeData.FstTab ~=1 then
				LuaBingQiPuMgr.EquipTypeNames[typeData.FstTab] = typeData.SecTabName
			else
				LuaBingQiPuMgr.WeaponTypeCfgs[typeData.SecTab] = typeData.SecTabName
			end
		end)
	end
end

--[[
    @desc: 获取装备的大类名称
    author:Codee
    time:2021-11-18 15:14:42
    --@firstType: 大类类型
    @return:firstType<=0 返回奇珍，否则返回表里面大类的名称
]]
function LuaBingQiPuMgr.GetEquipTypeName(firstType)
	if firstType <= 0 then 
		return LocalString.GetString("奇珍")
	end
	LuaBingQiPuMgr.InitEquipTypeCfg(false)
	return LuaBingQiPuMgr.EquipTypeNames[firstType]
end

function LuaBingQiPuMgr.GetBQPEquipTypeWithEquipType(equipType,subtype)
	LuaBingQiPuMgr.InitEquipTypeCfg(false)
	if equipType ~= 1 then
		subtype = 0 
	end
	if LuaBingQiPuMgr.m_Equip2TypeId[equipType] then
		return LuaBingQiPuMgr.m_Equip2TypeId[equipType][subtype]
	end
	return 0
end

function LuaBingQiPuMgr.GetEquipType(templateId)
	LuaBingQiPuMgr.InitEquipTypeCfg(false)

    local designData = templateId and EquipmentTemplate_Equip.GetData(templateId)
    if not designData then return end

    -- 通配符
    local wildcard = 0

    -- 有通配符的,优先通配符
    -- 通配符没有匹配的,再匹配小类
    return LuaBingQiPuMgr.m_Equip2TypeId[designData.Type] and (LuaBingQiPuMgr.m_Equip2TypeId[designData.Type][wildcard] or LuaBingQiPuMgr.m_Equip2TypeId[designData.Type][designData.SubType])

end

------------------------Gac2Gas

-- 请求每日推荐数据
function LuaBingQiPuMgr.QueryBQPRecommendEquip(isPrecious,equipType)
	if isPrecious then equipType = 0 end
	Gac2Gas.BQPQueryRecommendEquip(isPrecious,equipType)
end

function LuaBingQiPuMgr.QueryBQPStage()
	Gac2Gas.QueryBQPStage()
end

-- 请求一个装备的推广信息
function LuaBingQiPuMgr.QueryBQPSponsor(equipId)
	Gac2Gas.QuerySharedBQPInfo(equipId) 
end

--请求某个装备在兵器谱上的详细数据
function LuaBingQiPuMgr.QueryBQPDetailInfo(equipId)
	Gac2Gas.QueryBQPEquipDetails(equipId)
end

--请求玩家提交的装备数据
function LuaBingQiPuMgr.QueryBQPSubmitted()
	Gac2Gas.BQPQuerySubmitted()
end

--点赞
function LuaBingQiPuMgr.QueryBQPPraise(equipId,templateId,bFromDailyReport)
	Gac2Gas.BQPPraise(equipId, templateId, bFromDailyReport,bFromDailyReport)
end

--请求当日剩余点赞次数
function LuaBingQiPuMgr.QueryBQPPraiseCount()
	Gac2Gas.BQPQueryPraiseLeftTimes()
end

-------------------------推送网站组

function LuaBingQiPuMgr.Post(url,form,callBack)
	Main.Inst:StartCoroutine(CPersonalSpaceMgr.Inst:DoPost(url, form, DelegateFactory.Action_bool_string(callBack)))
end
--@region 

-- 推送兵器信息给网站
function LuaBingQiPuMgr.PushInfo()
	local roleEquip = CreateFromClass(CPersonalSpace_RoleEquip)
	local equipInfoList = CreateFromClass(MakeGenericClass(List, CPersonalSpace_Equip))
	local item = CItemMgr.Inst:GetById(LuaBingQiPuMgr.m_EquipToCommit.itemId)
	if not item then return end

	local equipInfo = CPersonalSpaceMgr.Inst:GenBodyEquipInfo(item.Equip, 0)
	equipInfoList:Add(equipInfo)
    roleEquip.equips = equipInfoList:ToArray()
    roleEquip.equipscore = item.Equip.Score
    local rawcontent = CPersonalSpaceMgr.Inst:BuildJsonString(roleEquip)
    local length = string.len(rawcontent)
    rawcontent = string.sub(rawcontent, 1, length-1)
    rawcontent = SafeStringFormat3([=[%s,"equipId":"%s"}]=], rawcontent, LuaBingQiPuMgr.m_EquipToCommit.itemId)

    local roleId = CClientMainPlayer.Inst.Id
    local serverId = CPersonalSpaceMgr.Inst:GetOriginalServerId()
    local action = SafeStringFormat3("%s?roleid=%s&serverid=%s&name=%s", "gdc/qnm.do", tostring(roleId), tostring(serverId), "rolebingqipu")

    local url = LuaBingQiPuMgr.GetPushURL()..action

    local form = CreateFromClass(CHTTPForm)
    local time = math.floor(CServerTimeMgr.Inst.timeStamp * 1000)
    local skey = CPersonalSpaceMgr.Inst.Token

    form:AddField("time", tostring(time))
	form:AddField("roleid", tostring(roleId))
    form:AddField("serverid", tostring(serverId))
    form:AddField("skey", skey)
    form:AddField("name", "rolebingqipu")

    local token = CPersonalSpaceMgr.MakeToken(roleId, time, rawcontent)
    form:AddField("token", token)
    form:AddField("rawcontent", rawcontent)

    local callBack = function (success, text)

	end
	LuaBingQiPuMgr.Post(url,form,callBack)
end

function LuaBingQiPuMgr.GetPushURL()
	-- "http://192.168.131.156:88/"
	return "http://api.hi.163.com/"
end
--@endregion

--@region 

-- 提交评论
function LuaBingQiPuMgr.CreateComment(equipid,msg)
	local url = LuaBingQiPuMgr.GetCreateCommentURL()
	local callBack = function (success, text)
		if success then
			local data = Json.Deserialize(text)
			if data["code"] == 0 then
				g_MessageMgr:ShowMessage("BQP_COMMENT_SUCCESS")
				g_ScriptEvent:BroadcastInLua("BQPCommentCreated",equipid)
			else
				local msg = data["msg"]
				g_MessageMgr:ShowMessage("CUSTOM_STRING2", msg)
			end
		end
	end
	local form = CreateFromClass(CHTTPForm)
	--form:AddField("roleid", CClientMainPlayer.Inst.Id)
	form:AddField("equipId", equipid)
	form:AddField("text", msg)
	LuaBingQiPuMgr.Post(url,form,callBack)
end

-- 需要替换成线上版本
function LuaBingQiPuMgr.GetCreateCommentURL()
	--return "http://192.168.131.156:88/qnm/equip/comments/create"
	return "http://api.hi.163.com/qnm/equip/comments/create"
end

-- 拉取评论
function LuaBingQiPuMgr.GetCommentList(equipid)
	local url = LuaBingQiPuMgr.GetCommentListURL()
	local callBack = function (success, text)
		if success then
			local data = Json.Deserialize(text)
			if data["code"] == 0 then 
				local comments = {}
				local info = data["data"]
				if not info then return end
				local commentList = info["list"]
				if not commentList then return end
				for i = 0, commentList.Count-1 do
					table.insert(comments,commentList[i])
				end
				g_ScriptEvent:BroadcastInLua("BQPCommentListGot",equipid,comments)
			else
				local msg = data["msg"]
				g_MessageMgr:ShowMessage("CUSTOM_STRING2", msg)
			end
		else
			g_MessageMgr:ShowMessage("BQP_DAMMU_GET_FAIL")
		end
	end
	local form = CreateFromClass(CHTTPForm)
	--form:AddField("roleid", CClientMainPlayer.Inst.Id)
	form:AddField("equipId", equipid)
	LuaBingQiPuMgr.Post(url,form,callBack)
end

-- 需要替换成线上版本
function LuaBingQiPuMgr.GetCommentListURL()
	--return "http://192.168.131.156:88/qnm/equip/comments/list"
	return "http://api.hi.163.com/qnm/equip/comments/list" 
end
--@endregion

-------------------------------以下为Gas2Gac方法的处理----------------------------------

-- 参数 praiseTimes 表示累计点赞次数
function Gas2Gac.BQPQueryPraiseLeftTimesResult(leftTimes, praiseTimes)--ok
	LuaBingQiPuMgr.m_LikesLeft = leftTimes
	local aid = 42030057
	if LuaBingQiPuMgr.m_LikesLeft >= 5 then
		CScheduleMgr.Inst:SetAlertState(aid,CScheduleMgr.EnumAlertState.Show,true)
	else
		CScheduleMgr.Inst:SetAlertState(aid,CScheduleMgr.EnumAlertState.Hide,true)
	end
	LuaBingQiPuMgr.m_TotleLikes = praiseTimes
	g_ScriptEvent:BroadcastInLua("UpdateBQPStatus")
end

function Gas2Gac.BQPSubmitEquipPrecheckDone(equipId, tag, discription, bPreciousRank)--ok
	LuaBingQiPuMgr.PushInfo()
	Gac2Gas.BQPSubmitEquip(equipId, tag, discription, bPreciousRank)
end


-- 回复已经提交的装备--不再使用
function Gas2Gac.QueryBQPForSubmitResult(data, thresholdUd)
end


-- 兵器谱排行 --不再使用
function Gas2Gac.SendBingQiPuRank(mainRankType, resultTbl_U)
	
end

-- 兵器谱操作结果
function Gas2Gac.BingQiPuOpResult(bSuccess, opType, enumReason)
	if bSuccess and opType == EnumBingQiPuOpType.eSubmit then
		CUIManager.CloseUI(CLuaUIResources.BQPConfirmCommitWnd)
		CItemInfoMgr.CloseItemInfoWnd()
		CUIManager.CloseUI(CLuaUIResources.BQPCommitEquipWnd)
		CUIManager.ShowUI(CLuaUIResources.BQPCommitEquipWnd)

		if CUIManager.IsLoaded(CLuaUIResources.BQPPromotionWnd) and LuaBingQiPuMgr.m_PromoteEquipInfo then
			Gac2Gas.QuerySharedBQPInfo(LuaBingQiPuMgr.m_PromoteEquipInfo.Id)
		end
	end

	if bSuccess and opType == EnumBingQiPuOpType.eVote then
		if CUIManager.IsLoaded(CLuaUIResources.BQPEquipInfoWnd) then
			g_ScriptEvent:BroadcastInLua("UpdateBQPEquipInfo")
		end

		if CUIManager.IsLoaded(CLuaUIResources.BQPPromotionWnd) and LuaBingQiPuMgr.m_PromoteEquipInfo then
			Gac2Gas.QuerySharedBQPInfo(LuaBingQiPuMgr.m_PromoteEquipInfo.Id)
		end
	end

	-- 点赞成功后，刷新兵器谱详情界面
	if bSuccess and opType == EnumBingQiPuOpType.ePraise then
		if CUIManager.IsLoaded(CLuaUIResources.BQPEquipInfoWnd) then
			g_ScriptEvent:BroadcastInLua("UpdateBQPEquipInfo")
		end
		
		if CUIManager.IsLoaded(CLuaUIResources.BQPPromotionWnd) and LuaBingQiPuMgr.m_PromoteEquipInfo then
			Gac2Gas.QuerySharedBQPInfo(LuaBingQiPuMgr.m_PromoteEquipInfo.Id)
		end
	end

	-- 提交奇珍榜成功后，刷新该界面
	if bSuccess and opType == EnumBingQiPuOpType.eSubimitPrecious then
		Gac2Gas.QueryBQPPreciousRank()
	end

	-- 下架成功后，刷新提交界面
	if bSuccess and opType == EnumBingQiPuOpType.eOffShelf then
		CUIManager.CloseUI(CLuaUIResources.BQPConfirmCommitWnd)
		CItemInfoMgr.CloseItemInfoWnd()
		CUIManager.CloseUI(CLuaUIResources.BQPCommitEquipWnd)
		CUIManager.ShowUI(CLuaUIResources.BQPCommitEquipWnd)
	end

	-- 赞助成功后，关掉自己的赞助界面，并刷新赞助列表
	if bSuccess and opType == EnumBingQiPuOpType.eInvestSponsor then
		CUIManager.CloseUI(CLuaUIResources.BQPMySponsorWnd)
		Gac2Gas.QuerySponsorRank()
	end
end

-- 查看某装备的详细信息的返回
function Gas2Gac.BingQiPuQueryEquipRes(detailUd)--ok
	local list = MsgPackImpl.unpack(detailUd)
	if not list then return end

	LuaBingQiPuMgr.m_DetailEquipInfo={}
	for i = 0, list.Count - 1, 15 do

		local equipId = list[i]
		local rankPos = list[i + 1]
		local ownerId = list[i + 2]
		local ownerName = list[i + 3]
		local ownerGender = list[i + 4]
		local ownerClass = list[i + 5]
		local ownerLevel = list[i + 6]
		local feiShengLevel = list[i + 7]
		local feiShengStage = list[i + 8]
		local tag1 = list[i + 9]
		local like = list[i + 10]
		local discription = list[i +11]
		local equipDataStr = list[i + 12]
		local score = list[i + 13]
		local precious = list[i + 14]
		local equip = CreateFromClass(CCommonItem, false, equipDataStr)

		table.insert(LuaBingQiPuMgr.m_DetailEquipInfo, {
			equipId = equipId,
			rankPos = rankPos,
			ownerId = ownerId,
			ownerName = ownerName,
			ownerGender = ownerGender,
			ownerClass = ownerClass,
			ownerLevel = ownerLevel,
			feiShengLevel = feiShengLevel,
			feiShengStage = feiShengStage,
			tag1 = tag1,
			discription = discription,
			equip = equip,
			Score = score,
			Like = like,
			Precious = precious--是否是奇珍榜
			})
	end

	CUIManager.ShowUI(CLuaUIResources.BQPEquipInfoWnd)
end

-- 将装备分享到世界频道
function Gas2Gac.BroadcastBQPEquipToWorld(equipUd)
	local list = MsgPackImpl.unpack(equipUd)
	if not list then return end

	local equipId = list[0]
	local templateId = list[1]
	local ownerId = list[2]
	local ownerName = list[3]
	local score = list[4]

	local labaType = list[5]
	local clazz = list[6]
	local ownerGender = list[7]
	local equipName = list[8]

	-- 世界频道+喇叭
	local content = g_MessageMgr:FormatMessage("BQP_SHARE_MESSAGE", equipName, equipId)
	CChatMgr.Inst:PlayerShareBQP(ownerName, ownerId, clazz, ownerGender, labaType, content, "", 0, 0, 0, nil, false, true, false) --TODO
end

--助力喇叭
function Gas2Gac.BroadcastBQPRewardToWorld(equipUd)
	local list = MsgPackImpl.unpack(equipUd)
	if not list then return end

	local doPlayerName = list[0]
	local tarPlayerName = list[1]
	local equipName = list[2]
	local equipid = list[3]
	local jade = list[4]

	local content = g_MessageMgr:FormatMessage("BQP_DONATE_BROAD", doPlayerName, tarPlayerName,equipName,equipid,jade)
	CChatMgr.Inst:PlayerShareBQP(doPlayerName, 0, 0, 0, 3, content, "", 0, 0, 0, nil, false, true, false) --TODO
end

-- Gac2Gas.BQPQueryShareCost
function Gas2Gac.BQPQueryShareCostResult(equipId, needSilver)
	local item = CItemMgr.Inst:GetById(equipId)
	if not item then return end
	local msg = SafeStringFormat3(LocalString.GetString("确定要将%s推广至世界频道吗？"), item.ColoredName)
	local cost = needSilver
	local okAction = DelegateFactory.Action(function()
		Gac2Gas.ShareBQPToWorld(equipId)
	end)
	QnMoneyCostMesssageMgr.ShowMoneyCostMessageBox(EnumMoneyType.YinLiang, msg, cost, okAction, nil, false, false, EnumPlayScoreKey.NONE, false)
end

-- 点击链接
function Gas2Gac.SendBqpQueryShareRes(equipId, rankPos, extraRewardIdx, praiserNameInfoU, equipDataStr, usedPraisedTimes, bPreciousRank)
	local list = MsgPackImpl.unpack(praiserNameInfoU)
	if not list then return end

	LuaBingQiPuMgr.m_PromoteVoteInfo={}
	for i = 0, list.Count - 1, 2 do
		local praiserId = list[i]
		local praiserName = list[i+1]

		table.insert(LuaBingQiPuMgr.m_PromoteVoteInfo, {
			rank = math.floor(i / 2) + 1,
			praiserId = praiserId,
			praiserName = praiserName,
			})
	end

	local equip = CreateFromClass(CCommonItem, false, equipDataStr)
	LuaBingQiPuMgr.m_PromoteEquipInfo = equip
	LuaBingQiPuMgr.m_PromoteData = {}
	LuaBingQiPuMgr.m_PromoteData.ExtraReward = extraRewardIdx
	LuaBingQiPuMgr.m_PromoteData.UsedPraisedTimes = usedPraisedTimes
	LuaBingQiPuMgr.m_PromoteData.RankPos = rankPos
	LuaBingQiPuMgr.m_PromoteData.BPrecious = bPreciousRank

	if CUIManager.IsLoaded(CLuaUIResources.BQPPromotionWnd) then
		g_ScriptEvent:BroadcastInLua("UpdateBQPPromotionInfo")
	else
		CUIManager.ShowUI(CLuaUIResources.BQPPromotionWnd)
	end
end

-- 同步兵器谱阶段信息
function Gas2Gac.SyncBQPStage(stage)--ok
	LuaBingQiPuMgr.m_StageStatus = stage
	g_ScriptEvent:BroadcastInLua("UpdateBQPStatus")
end

-- 服务器返回玩家提交的数据
-- thresholdUd 表示装备类型对应的300名的值
-- preciousRankThreshold 表示奇珍榜100名的值
function Gas2Gac.BQPQuerySubmittedResult(submittedUd, thresholdUd, preciousRankThreshold)
	local list = MsgPackImpl.unpack(submittedUd)
	if not list then return end

	LuaBingQiPuMgr.m_EquipsCommited={}
	
	LuaBingQiPuMgr.m_EquipTypesCommited={}
	LuaBingQiPuMgr.m_EquipCommitThreshold={}

	for i = 0, list.Count - 1, 5 do
		local equipId 			= list[i]
		local bPresiuocRank 	= list[i+1] -- 是否奇珍榜
		local equipDataString 	= list[i+2]
		local rankPos 			= list[i+3] -- 榜上排名
		local renqi 			= list[i+4] -- 人气

		local equip = nil
		if not equipDataString or equipDataString == "" then
			equip = CItemMgr.Inst:GetById(equipId)
		else 
			-- 可能玩家上架到商店了
			equip = CreateFromClass(CCommonItem, false, equipDataString)
		end

		if equip then
			local tb = {
				Item = equip,
				Rank = rankPos,
				Score = renqi,
				Precious = bPresiuocRank
			}

			table.insert(LuaBingQiPuMgr.m_EquipsCommited,tb)

			local designData = EquipmentTemplate_Equip.GetData(equip.Equip.TemplateId)
    		if designData then
				table.insert(LuaBingQiPuMgr.m_EquipTypesCommited, designData.Type)
			end
		end
	end
	
	local thresholdList = MsgPackImpl.unpack(thresholdUd)
	if not thresholdList then return end

	for i = 0, thresholdList.Count - 1, 1 do
		table.insert(LuaBingQiPuMgr.m_EquipCommitThreshold, thresholdList[i])
	end

	LuaBingQiPuMgr.m_PreciousRankThreshold = preciousRankThreshold

	g_ScriptEvent:BroadcastInLua("QueryBQPForSubmitResult")
end

-- 十大装备排行榜
function Gas2Gac.BQPQueryTopTenRankResult(equipmentType, equipmentSubType, rankType, eachDataLen, rankUd, selfSubmittedUd)
	-- 排行榜上的每一个装备数据
	local list = MsgPackImpl.unpack(rankUd)
	if not list then return end

	if LuaBingQiPuMgr.m_RankInfo == nil then
		LuaBingQiPuMgr.m_RankInfo = {}
	end
	if LuaBingQiPuMgr.m_RankInfo[equipmentType] == nil then
		LuaBingQiPuMgr.m_RankInfo[equipmentType] = {}
	end

	if equipmentType ~= 1 then 
		equipmentSubType = 0 
	end
		
	LuaBingQiPuMgr.m_RankInfo[equipmentType][equipmentSubType] = {}

	for i = 0, list.Count - 1, eachDataLen do
		local rankPos 			= list[i]
		local equipId 			= list[i+1]
		local templateId 		= list[i+2]
		local ownerId 			= list[i+3]
		local isFake 			= list[i+4] -- 1表示盗版装备
		local ownerName 		= list[i+5]
		local equipScore 		= list[i+6] -- 装评
		local renqi 			= list[i+7] -- 人气
		local equipName 		= list[i+8] -- 装备名，如果不是空字符串，就用这个作为名字

		-- 剩下是每一类排行榜上的得分
		local ranks = {}
		local leftCount = eachDataLen - 9
		for j = 0, leftCount-1, 2 do
			local rtype = list[i+9+j] -- 对应 BingQiPu_RankType.Type
			local rvalue = list[i+9+j+1]
			ranks[rtype] = rvalue
		end
		local td = {
			rankPos = rankPos,
			equipId = equipId,
			templateId = templateId,
			ownerId = ownerId,
			isFake = isFake,
			ownerName = ownerName,
			equipScore = equipScore,
			renqiRank = renqi,
			ranks = ranks,
			equipName = equipName
		}
		
		table.insert(LuaBingQiPuMgr.m_RankInfo[equipmentType][equipmentSubType], td)
	end

	-- 提交提交了哪几类装备
	local list2 = MsgPackImpl.unpack(selfSubmittedUd)
	if not list2 then return end

	LuaBingQiPuMgr.m_EquipTypesCommited = {}
	for i = 0, list2.Count - 1, 2 do
		local equipId = list2[i]
		local equipType = list2[i+1]
		local ecfg = BingQiPu_EquipType.GetData(equipType)
		table.insert(LuaBingQiPuMgr.m_EquipTypesCommited,ecfg.FstTab)
	end

	g_ScriptEvent:BroadcastInLua("SendBingQiPuRank")
end

--奇珍排行榜
function Gas2Gac.BQPQueryPreciousRankResult(rankUd)
	local list = MsgPackImpl.unpack(rankUd)
	if not list then return end

	LuaBingQiPuMgr.m_RankInfo2 = {}
	for i = 0, list.Count - 1, 9 do
		local rankPos 				= list[i]
		local equipId 			= list[i+1]
		local templateId 		= list[i+2]
		local ownerId 			= list[i+3]
		local isFake 				= list[i+4] -- 1表示盗版装备
		local ownerName 		= list[i+5]
		local renqi 			= list[i+6] -- 人气
		local tag 			    = list[i+7] -- tag
		local equipName 	    = list[i+8] -- 装备名，如果非空字符串，则采用这个作为装备名

		local td = {
			rankPos = rankPos,
			equipId = equipId,
			templateId = templateId,
			ownerId = ownerId,
			isFake = isFake,
			ownerName = ownerName,
			equipScore = 0,
			renqiRank = renqi,
			tag = tag,
			equipName = equipName
		}

		table.insert(LuaBingQiPuMgr.m_RankInfo2,td)
	end
	g_ScriptEvent:BroadcastInLua("SendBingQiPuRank")
end

-- 日报信息
function Gas2Gac.SendBQPRecommendEquip(bPreciousRank, equipType, recommendEquipUd)--ok
	local list = MsgPackImpl.unpack(recommendEquipUd)
	if not list then return end
	local dailyInfo = nil
	if bPreciousRank then
		LuaBingQiPuMgr.m_DailyInfo2 = {}
		dailyInfo = LuaBingQiPuMgr.m_DailyInfo2
	else
		LuaBingQiPuMgr.m_DailyInfo = {}
		dailyInfo = LuaBingQiPuMgr.m_DailyInfo
	end
	for i = 0, list.Count - 1, 10 do
		local equipId = list[i]
		-- local subRankId = list[i + 1]
		local templateId = list[i + 1]
		local tag1 = list[i + 2]
		local tag2 = list[i + 3]
		local score = list [i + 4]
		local ownerId = list [i + 5]
		local ownerName = list [i + 6]
		local isPraised = list [i + 7] -- 第二届新增，是否点赞过,1表示点赞过
		local isShenBing = list[i + 8] -- 是否神兵, 1表示是神兵
		local equipName = list[i + 9] -- 如果非空字符串，则用这个作为装备名
		-- local equip = CreateFromClass(CCommonItem, false, equipDataStr)

		table.insert(dailyInfo, {
			equipId = equipId,
			-- subRankId = subRankId,
			templateId = templateId,
			tag1 = tag1,
			tag2 = tag2,
			score = score,
			ownerId = ownerId,
			ownerName = ownerName,
			-- equip = equip,
			isPraised = isPraised,
			equipName = equipName,
			isShenBing = isShenBing > 0
			})
	end
	g_ScriptEvent:BroadcastInLua("UpdateBQPDaily")

end

-- 兵器谱赞助上榜
function Gas2Gac.SendBQPSponsorRank(rankUd)
	local list = MsgPackImpl.unpack(rankUd)
	if not list then return end

	LuaBingQiPuMgr.m_SponsorList={}

	for i = 0, list.Count - 1, 7 do
		local rankPos = list[i]
		local templateId = list[i+1]
		local equipId = list[i+2]
		local ownerId = list[i+3]
		local ownerName = list[i+4]
		local invest = list[i+5]
		local equipName = list[i+6] -- 如果非空字符串，则用这个作为装备名
		if rankPos > 0 then
			table.insert(LuaBingQiPuMgr.m_SponsorList, {
				rankPos = rankPos,
				templateId = templateId,
				equipId = equipId,
				ownerId = ownerId,
				ownerName = ownerName,
				invest = invest,
				equipName = equipName
			})
		end
	end

	if CUIManager.IsLoaded(CLuaUIResources.BQPSponsorListWnd) then
		g_ScriptEvent:BroadcastInLua("BQPUpdateSponsorList")
	else
		CUIManager.ShowUI(CLuaUIResources.BQPSponsorListWnd)
	end
	
end

-- 奇珍榜--不再使用
function Gas2Gac.SendBQPPreciousRank(rankUd)
	
end

function Gas2Gac.SendBQPHistory(rankUd)
	local list = MsgPackImpl.unpack(rankUd)
	if not list then return end

	local historyList = {}

	for i = 0, list.Count - 1, 7 do
		local rankPos = list[i]
		local templateId = list[i+1]
		local ownerName = list[i+2]
		local praise = list[i+3]
		local equipId = list[i+4]
		local historyId = list[i+5]
		local equipName = list[i+6] -- 如果非空字符串，则用这个作为装备名
		
		table.insert(historyList, {
			rankPos = rankPos,
			templateId = templateId,
			ownerName = ownerName,
			praise = praise,
			equipId = equipId,
			historyId = historyId,
			equipName = equipName
			})
	end

	g_ScriptEvent:BroadcastInLua("UpdateBQPResultList", historyList)
end

--点赞返回数据
function Gas2Gac.SyncBQPPraiseInfo(expireTime, equipType, times)
end

function Gas2Gac.BQPSubmitReplaceConfirm(equipId, equipType)
	
end

-- 历史详情(请用histodyId来发QueryBingQiPuHistoryDetail请求)
function Gas2Gac.SyncBQPHistoryDetail(equipDataStr)
	local equip = CreateFromClass(CCommonItem, false, equipDataStr)

	if not equip then return end
	CItemInfoMgr.ShowLinkItemInfo(equip, false, nil, AlignType.Default, 0, 0, 0, 0, 0)
	
end

-- 预留给patch
function Gas2Gac.SyncBQPMisc(infoType, dataString)
end

function Gas2Gac.SyncCheckBQPComment(bCanComment)
	LuaBingQiPuMgr.m_CanComment = bCanComment
end

-- 点赞成功
function Gas2Gac.BQPPraiseDone(equipId, leftTimes)
	LuaBingQiPuMgr.m_LikesLeft = leftTimes
	g_ScriptEvent:BroadcastInLua("BQPPraiseDone",equipId)
end
