local GameObject = import "UnityEngine.GameObject"
local Animation = import "UnityEngine.Animation"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"

local EShareType = import "L10.UI.EShareType"
local CUICommonDef = import "L10.UI.CUICommonDef"

LuaQiXi2023QueQiaoXianQuResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaQiXi2023QueQiaoXianQuResultWnd, "Btn01", "Btn01", GameObject)
RegistChildComponent(LuaQiXi2023QueQiaoXianQuResultWnd, "CloseBtn", "CloseBtn", GameObject)
RegistChildComponent(LuaQiXi2023QueQiaoXianQuResultWnd, "MyIcon", "MyIcon", GameObject)
RegistChildComponent(LuaQiXi2023QueQiaoXianQuResultWnd, "TeammateIcon", "TeammateIcon", GameObject)

--@endregion RegistChildComponent end

function LuaQiXi2023QueQiaoXianQuResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end

    if CommonDefs.IS_VN_CLIENT then
        self.Btn01:SetActive(false)
    end
end

function LuaQiXi2023QueQiaoXianQuResultWnd:Init()
    if LuaQiXi2021Mgr.playResult then
        self.transform:GetComponent(typeof(Animation)):Play("qixi2023queqiaoxianquresultwnd_win")
    else
        self.transform:GetComponent(typeof(Animation)):Play("qixi2023queqiaoxianquresultwnd_lose")
    end
    
    UIEventListener.Get(self.Btn01).onClick = DelegateFactory.VoidDelegate(function (go)
        self.Btn01:SetActive(false)
        self.CloseBtn:SetActive(false)
        CUICommonDef.CaptureScreen("screenshot", false, false, nil,
            DelegateFactory.Action_string_bytes(function(fullPath, jpgBytes)
                self.Btn01:SetActive(true)
                self.CloseBtn:SetActive(true)
                ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
            end), false)
    end)
    
    self:InitMe()
    self:InitPartner()
end

--@region UIEvent

--@endregion UIEvent

function LuaQiXi2023QueQiaoXianQuResultWnd:InitPartner()
    if LuaQiXi2021Mgr.playResultParnter then
        self.TeammateIcon.transform:Find("Name"):GetComponent(typeof(UILabel)).text = LuaQiXi2021Mgr.playResultParnter.name
        self.TeammateIcon:GetComponent(typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(LuaQiXi2021Mgr.playResultParnter.class, LuaQiXi2021Mgr.playResultParnter.gender, -1), false)
    end
end

function LuaQiXi2023QueQiaoXianQuResultWnd:InitMe()
    self.MyIcon.transform:Find("Name"):GetComponent(typeof(UILabel)).text = LuaQiXi2021Mgr.MyInfo.Name
    self.MyIcon:GetComponent(typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(LuaQiXi2021Mgr.MyInfo.Class, LuaQiXi2021Mgr.MyInfo.Gender, -1), false)
end

