-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local CItemMgr = import "L10.Game.CItemMgr"
local CLivingSkillMgr = import "L10.Game.CLivingSkillMgr"
local CLivingSkillUseWnd = import "L10.UI.CLivingSkillUseWnd"
local CScene = import "L10.Game.CScene"
local CUIManager = import "L10.UI.CUIManager"
local LifeSkill_Item = import "L10.Game.LifeSkill_Item"
local LifeSkill_SkillType = import "L10.Game.LifeSkill_SkillType"
local LocalString = import "LocalString"
CLivingSkillUseWnd.m_Init_CS2LuaHook = function (this) 
    this.iconTexture.mainTexture = nil
    this.iconTexture.material = nil
    this.nameLabel.text = ""


    local data = LifeSkill_Item.GetData(CLivingSkillUseWnd.itemId)
    local skillTypeData = LifeSkill_SkillType.GetData(data.SkillId)
    if skillTypeData ~= nil then
        repeat
            local default = skillTypeData.Type
            if default == 1 then
                this.nameLabel.text = LocalString.GetString("钓鱼")
                this.isFish = true
                break
            elseif default == 2 then
                this.nameLabel.text = LocalString.GetString("采集")
                break
            end
        until 1
    end

    local template = CItemMgr.Inst:GetItemTemplate(data.ID)
    if template ~= nil then
        this.iconTexture:LoadMaterial(template.Icon)
    else
        this.iconTexture:Clear()
    end
end
CLivingSkillUseWnd.m_OnApplyButtonClick_CS2LuaHook = function (this, go) 
    --采集
    --采集
    local data = LifeSkill_Item.GetData(CLivingSkillUseWnd.itemId)
    CLivingSkillMgr.Inst:Produce(CLivingSkillUseWnd.itemId)

    CUIManager.CloseUI(CIndirectUIResources.LivingSkillUseWnd)
end
CLivingSkillUseWnd.m_Update_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst == nil then
        this:Close()
        return
    end
    --监测角色位置，是否在钓鱼的位置
    local data = LifeSkill_Item.GetData(CLivingSkillUseWnd.itemId)
    if data == nil or data.SceneId ~= CScene.MainScene.SceneTemplateId then
        this:Close()
        return
    end
    local mainPlayer = CClientMainPlayer.Inst
    if this.isFish then
        if not CScene.MainScene:IsInFishingRegion(mainPlayer) then
            this:Close()
            return
        end
    else
        if not CScene.MainScene:IsInPlantingRegion(mainPlayer) then
            this:Close()
            return
        end
    end
end
