local UISprite = import "UISprite"
local UILabel = import "UILabel"
local QnButton = import "L10.UI.QnButton"
local UIScrollViewIndicator = import "UIScrollViewIndicator"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CUITexture = import "L10.UI.CUITexture"
local Item_Item = import "L10.Game.Item_Item"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CQnSymbolParser = import "CQnSymbolParser"
local UIGrid = import "UIGrid"
local CUIFx = import "L10.UI.CUIFx"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local PathType = import "DG.Tweening.PathType"
local PathMode = import "DG.Tweening.PathMode"
local Ease = import "DG.Tweening.Ease"
local Vector4 = import "UnityEngine.Vector4"
local CUICommonDef = import "L10.UI.CUICommonDef"
local LuaTweenUtils = import "LuaTweenUtils"
local UITable = import "UITable"
local UIPanel = import "UIPanel"
local CMailMgr = import "L10.Game.CMailMgr"
local QnTableView = import "L10.UI.QnTableView"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local QnMoneyCostMesssageMgr = import "L10.UI.QnMoneyCostMesssageMgr"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local CShopMallMgr = import "L10.UI.CShopMallMgr"

CLuaQMPKHandBookWnd = class()

RegistClassMember(CLuaQMPKHandBookWnd,"m_TableView")
RegistClassMember(CLuaQMPKHandBookWnd,"m_DataSource")
RegistClassMember(CLuaQMPKHandBookWnd,"m_Grid")
RegistClassMember(CLuaQMPKHandBookWnd,"m_PersonalRecordBtn")
RegistClassMember(CLuaQMPKHandBookWnd,"m_ViewAgendaBtn")
RegistClassMember(CLuaQMPKHandBookWnd,"m_CreditHelpBtn")
RegistClassMember(CLuaQMPKHandBookWnd,"m_PersonalRecordWnd")
RegistClassMember(CLuaQMPKHandBookWnd,"m_HasPersonalRecordData")
RegistClassMember(CLuaQMPKHandBookWnd,"m_Panel")
RegistClassMember(CLuaQMPKHandBookWnd,"m_LeftRewardButton")
RegistClassMember(CLuaQMPKHandBookWnd,"m_RightRewardButton")
RegistClassMember(CLuaQMPKHandBookWnd,"m_AgendaLabel")
RegistClassMember(CLuaQMPKHandBookWnd,"m_CreditLabel")
RegistClassMember(CLuaQMPKHandBookWnd,"m_RewardData")
RegistClassMember(CLuaQMPKHandBookWnd,"m_RewardButtonBound")
RegistClassMember(CLuaQMPKHandBookWnd,"m_FxTable")
RegistClassMember(CLuaQMPKHandBookWnd,"m_RecordTypeTable")
RegistClassMember(CLuaQMPKHandBookWnd,"m_ShowRewardFxTick")
RegistClassMember(CLuaQMPKHandBookWnd,"m_PurchaseBtn")
RegistClassMember(CLuaQMPKHandBookWnd,"m_PurchaseBtnFx")
RegistClassMember(CLuaQMPKHandBookWnd,"m_BgFx")
RegistClassMember(CLuaQMPKHandBookWnd,"m_ActivityTimeLab")
RegistClassMember(CLuaQMPKHandBookWnd,"m_Award_dict")
RegistClassMember(CLuaQMPKHandBookWnd,"m_Score")
RegistClassMember(CLuaQMPKHandBookWnd,"m_IsUnLock")
RegistClassMember(CLuaQMPKHandBookWnd,"m_CanOpItemIndex")
RegistClassMember(CLuaQMPKHandBookWnd,"m_CurrentHighValueIndex")
RegistClassMember(CLuaQMPKHandBookWnd,"m_NeedJump")
RegistClassMember(CLuaQMPKHandBookWnd,"m_Panel")
RegistClassMember(CLuaQMPKHandBookWnd,"m_beenRewarded")
--积分同步功能
RegistClassMember(CLuaQMPKHandBookWnd,"m_SyncCreditsBtn")
RegistClassMember(CLuaQMPKHandBookWnd,"m_SyncCreditsBtnAlert")
RegistClassMember(CLuaQMPKHandBookWnd, "m_MailListenTick")
RegistClassMember(CLuaQMPKHandBookWnd, "m_OnSyncPlayerCredits")
RegistClassMember(CLuaQMPKHandBookWnd, "m_CurrentSendingMail")

RegistClassMember(CLuaQMPKHandBookWnd,"m_LeftArrow")
RegistClassMember(CLuaQMPKHandBookWnd,"m_RightArrow")
RegistClassMember(CLuaQMPKHandBookWnd,"m_CurScrollIndex")
CLuaQMPKHandBookWnd.m_Reward2IdOffset = 1000 --与服务器约定的常量用于复用rpc
CLuaQMPKHandBookWnd.m_Opened = false

function CLuaQMPKHandBookWnd:Init()
    CLuaQMPKHandBookWnd.m_Opened = true

    UIEventListener.Get(self.m_PersonalRecordBtn).onClick = DelegateFactory.VoidDelegate(function(g)
        if(self.m_HasPersonalRecordData)then
            self.m_PersonalRecordWnd:SetActive(true)
            local record_table = self.transform:Find("PersonalRecordPanel/ScrollView/Table").gameObject
            record_table:GetComponent(typeof(UITable)):Reposition()
        else
            g_MessageMgr:ShowMessage("QMPK_HANDBOOK_NO_PERSONAL_RECORD")
        end
    end)

    UIEventListener.Get(self.m_CreditHelpBtn).onClick = DelegateFactory.VoidDelegate(function(g)
        g_MessageMgr:ShowMessage("QMPK_HANDBOOK_SCORE_RULE")
    end)

    UIEventListener.Get(self.m_ViewAgendaBtn).onClick = DelegateFactory.VoidDelegate(function(g)
        CLuaQMPKAgendaWnd.StartWnd = 1
        CUIManager.ShowUI(CLuaUIResources.QMPKAgendaWnd)
    end)

    UIEventListener.Get(self.m_PurchaseBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(g)
        CUIManager.ShowUI(CLuaUIResources.QMPHKUnlockZhanLingWnd)
    end)

    local personal_record_bg = self.m_PersonalRecordWnd.transform:Find("PersonalRecordBg").gameObject
    UIEventListener.Get(personal_record_bg).onClick = DelegateFactory.VoidDelegate(function(g)
        self.m_PersonalRecordWnd:SetActive(false)
    end)

    local BuyLiBaoBtn = self.transform:Find("Anchor/BottomView/BuyPackageButton").gameObject
    UIEventListener.Get(BuyLiBaoBtn).onClick = DelegateFactory.VoidDelegate(function(g)
        CShopMallMgr.ShowLinyuShoppingMall(QuanMinPK_Setting.GetData().HandBookLiBaoMailItemId)
    end)
    self.m_CurScrollIndex = 0
    self.m_OnSyncPlayerCredits = false
    self.m_MailListenTick = nil
    self:InitRewardPreview()
    --告知服务器窗口已打开
    Gac2Gas.OpenQmpkHandBook()
end

function CLuaQMPKHandBookWnd:OnDestroy()
    CLuaQMPKHandBookWnd.m_Opened = false
end

function CLuaQMPKHandBookWnd:FormatDate(_date)
    local return_val = _date.Year
    if(_date.Month < 10)then
        return_val = return_val.."0"
    end
    return_val = return_val.._date.Month
    if(_date.Day < 10)then
        return_val = return_val.."0"
    end
    return_val = return_val.._date.Day
    return return_val
end

function CLuaQMPKHandBookWnd:FormatDataDate(_date)
    if(_date and _date ~= "")then
        local return_val = ""
        local words = {}
        for w in string.gmatch(_date, "%d+") do
            table.insert(words, w)
        end
        return_val = words[1]..words[2]..words[3]
        return return_val
    end
end

function CLuaQMPKHandBookWnd:FormatDataDateString(_date)
    if(_date and _date ~= "")then
        local return_val = ""
        local words = {}
        for w in string.gmatch(_date, "%d+") do
            table.insert(words, w)
        end
        return_val =  System.String.Format(LocalString.GetString("{0}年{1}月{2}日"),words[1],tonumber(words[2]),words[3])
        return return_val
    end
end

function CLuaQMPKHandBookWnd:DelayShowFx()
    if(self.m_ShowRewardFxTick)then
        UnRegisterTick(self.m_ShowRewardFxTick)
        self.m_ShowRewardFxTick = nil
    end
    for k, v in ipairs(self.m_FxTable) do
        v:SetActive(false)
    end
    self.m_ShowRewardFxTick = RegisterTickOnce(function()
        for k, v in ipairs(self.m_FxTable) do
            v:SetActive(true)
        end
    end, 500)
end

function CLuaQMPKHandBookWnd:OnEnable()
    g_ScriptEvent:AddListener("ReplyQmpkHandBookInfo",self,"OnReceiveHandBookInfo")

    self.m_Grid = self.transform:Find("Anchor/BottomSliderView/Tab/TabGrid"):GetComponent(typeof(UIGrid))
    self.m_TableView = self.transform:Find("Anchor/BottomSliderView/Tab"):GetComponent(typeof(QnTableView))
    self.m_Panel = self.transform:Find("Anchor/BottomSliderView/Tab"):GetComponent(typeof(UIPanel))
    self.m_PersonalRecordBtn = self.transform:Find("Anchor/Achievements/PersonalRecordsButtom").gameObject
    self.m_PersonalRecordWnd = self.transform:Find("PersonalRecordPanel").gameObject
    self.m_PersonalRecordWnd:SetActive(false)
    self.m_ViewAgendaBtn = self.transform:Find("Anchor/Achievements/ViewAgendaButtom").gameObject
    self.m_CreditHelpBtn = self.transform:Find("Anchor/BottomView/TipButton").gameObject
    self.m_CreditLabel = self.transform:Find("Anchor/Achievements/CreditLabel"):GetComponent(typeof(UILabel))
    self.m_AgendaLabel = self.transform:Find("Anchor/Achievements/InfoLabel"):GetComponent(typeof(UILabel))
    self.m_ActivityTimeLab = self.transform:Find("Anchor/BottomView/ActivityTime/TimeLab"):GetComponent(typeof(UILabel))
    self.m_PurchaseBtn = self.transform:Find("Anchor/BottomView/PurchaseButton"):GetComponent(typeof(QnButton))
    self.m_PurchaseBtnFx = self.transform:Find("Anchor/BottomView/PurchaseButton/Fx"):GetComponent(typeof(CUIFx))
    self.m_BgFx = self.transform:Find("BgFx"):GetComponent(typeof(CUIFx))
    self.m_PurchaseBtnFx:LoadFx("fx/ui/prefab/UI_zbsjf_goumaianniu.prefab")
    self.m_BgFx:LoadFx("fx/ui/prefab/UI_zbsjf_fenwei.prefab")

    self.m_LeftRewardButton = self.transform:Find("Anchor/BottomSliderView/Indicators/LeftButton").gameObject
    self.m_RightRewardButton = self.transform:Find("Anchor/BottomSliderView/Indicators/RightButton").gameObject


    self.m_NeedJump = true
    self.m_HasPersonalRecordData = false
    self.m_ShowRewardFxTick = nil

    --积分同步功能
    self.m_SyncCreditsBtn = self.transform:Find("Anchor/Achievements/SyncCreditButtom").gameObject
    self.m_SyncCreditsBtnAlert = self.transform:Find("Anchor/Achievements/SyncCreditButtom/Alert").gameObject
    self.m_OnSyncPlayerCredits = false
    self.m_MailListenTick = nil
    self.m_CurrentSendingMail = nil

    self.m_RewardData = {}
    self.m_FxTable = {}
    self.m_RecordTypeTable = {}
    self.m_LeftArrow = self.transform:Find("Anchor/BottomSliderView/Indicators/LeftLocateBtn").gameObject
    self.m_RightArrow = self.transform:Find("Anchor/BottomSliderView/Indicators/RightLocateBtn").gameObject
    UIEventListener.Get(self.m_LeftArrow.gameObject).onClick = DelegateFactory.VoidDelegate(function(g)
        self:ScrollTo(self.m_CurScrollIndex)
    end)

    UIEventListener.Get(self.m_RightArrow.gameObject).onClick = DelegateFactory.VoidDelegate(function(g)
        self:ScrollTo(self.m_CurScrollIndex)
    end)
    --初始化数据,并生成NextHighValue映射
    local needQuery = true
    local nextHighValue = 1
    QuanMinPK_HandBook.ForeachKey(function (key)
        local data = QuanMinPK_HandBook.GetData(key)
        if needQuery then
            nextHighValue = self:FindNextHighValue(key)
            needQuery = false
        end
        if data.HighValue == true then
            needQuery = true
        end
        table.insert(self.m_RewardData, { k = key,
            item1 = {credit = data.Score, id = data.Item[0], num = data.Item[1], inited = false, bind = data.Item[2]},
            item2 = {credit = data.Score, id = data.Item2[0], num = data.Item2[1], inited = false, bind = data.Item2[2]},
            nextHighValueKey = nextHighValue} )

    end)

    QuanMinPK_HandBookTarget.ForeachKey(function (key)
        local data = QuanMinPK_HandBookTarget.GetData(key)
        table.insert(self.m_RecordTypeTable, {description = data.Description, score = data.Score})
    end)

    --获取当日赛程
    do
        self.m_AgendaLabel.text = LocalString.GetString("活动未开始")
        local now = CServerTimeMgr.Inst:GetZone8Time()
        local formatted_date = tonumber(self:FormatDate(now))

        --formatted_date = tonumber("20190819")
        local last_date_str = nil
        local found = false
        local not_start = false
        local startTime = nil
        local endTime = nil
        QuanMinPK_Schedule.Foreach(function (key, data)
            if(data.Date and data.Date ~= "")then
                endTime = tostring(data.Date)
            end
            if(not (found or not_start))then
                --local data = QuanMinPK_Schedule.GetData(key)
                if(data.Date and data.Date ~= "")then
                    if not startTime then
                        startTime = tostring(data.Date)
                    end
                    --endTime = tostring(data.Date)

                    local data_date = tonumber(self:FormatDataDate(tostring(data.Date)))
                    if(formatted_date == data_date)then
                        self.m_AgendaLabel.text = CQnSymbolParser.ConvertQnTextToNGUIText(data.Description)
                        found = true
                    elseif(formatted_date < data_date)then
                        self.m_AgendaLabel.text = LocalString.GetString("活动尚未开始")
                        not_start = true
                    end
                else
                    last_date_str = CQnSymbolParser.ConvertQnTextToNGUIText(data.Description)
                end
            end
        end)
        if(not (found or not_start))then
            self.m_AgendaLabel.text = last_date_str
        end

        self.m_ActivityTimeLab.text = self:FormatDataDateString(startTime) .. LocalString.GetString("—") .. self:FormatDataDateString(endTime)
    end

    self.m_CreditLabel.text = tostring(0)

    do
        --初始化奖励界面
        local objectSprite = self.transform:Find("Anchor/BottomSliderView/Pool/0/ObjectSprite").gameObject
        local b = NGUIMath.CalculateRelativeWidgetBounds(objectSprite.transform)
        self.m_RewardButtonBound = CUICommonDef.GetWayPoints(Vector4(b.min.x, b.max.y, b.size.x, b.size.y), 0, false)

        self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create(
            function ()
                return #self.m_RewardData
            end,
            function (item, index)
                self:InitItem(item, index + 1)
            end
        )
        self.m_TableView:ReloadData(true, true)
        self:UpdateImportant(false)
    end

    local scroll_view = self.transform:Find("PersonalRecordPanel/ScrollViewIndicator"):GetComponent(typeof(UIScrollViewIndicator))
    scroll_view:Layout()
    self:UpdateIndicator(1)

    --初始化同步积分按钮
    do
        local mail = self:FindNextMail()
        self.m_SyncCreditsBtnAlert:SetActive(mail ~= nil)
        UIEventListener.Get(self.m_SyncCreditsBtn).onClick = DelegateFactory.VoidDelegate(function(g)
            self:OnStartSyncCredits()
        end)
    end

end

function CLuaQMPKHandBookWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ReplyQmpkHandBookInfo",self,"OnReceiveHandBookInfo")
    if(self.m_ShowRewardFxTick)then
        UnRegisterTick(self.m_ShowRewardFxTick)
        self.m_ShowRewardFxTick = nil
    end
    if(self.m_MailListenTick)then
        UnRegisterTick(self.m_MailListenTick)

        self.m_MailListenTick = nil
    end
end

function CLuaQMPKHandBookWnd:OnDragStart(_go)
    for k, v in ipairs(self.m_FxTable) do
        v:SetActive(false)
    end
end

function CLuaQMPKHandBookWnd:OnDragEnd(_go)
    self:DelayShowFx()
end

function CLuaQMPKHandBookWnd:ScrollTo(index)
    if index > QuanMinPK_HandBook.GetDataCount() - 5 then
        index = QuanMinPK_HandBook.GetDataCount() - 5
    end

    self.m_TableView:ScrollToRow(index+4)
end

function CLuaQMPKHandBookWnd:FindNextHighValue(start)
    for i = start, QuanMinPK_HandBook.GetDataCount()  do
        if QuanMinPK_HandBook.GetData(i).HighValue then
            return i
        end
    end
    return #self.m_RewardData
end

function CLuaQMPKHandBookWnd:FindNextMail()
    local setting = QuanMinPK_Setting.GetData()
    local mail = nil
    do
        local mails = CMailMgr.Inst:GetMails()
        local i = 0
        while (i < mails.Count) do

            if(tonumber(mails[i].TemplateId) == 11110729)then
                mail = mails[i].MailId
                break
            end
            i = i + 1
        end
    end
    return mail
end

function CLuaQMPKHandBookWnd:TickSyncCredits()
    if(self.m_OnSyncPlayerCredits)then
        local mail = self:FindNextMail()
        if(mail ~= nil)then
            if(mail ~= self.m_CurrentSendingMail)then --还有新的邮件
                self.m_CurrentSendingMail = mail

                CMailMgr.Inst:HandleMail(self.m_CurrentSendingMail, "")
            end
        else
            if(self.m_MailListenTick)then
                UnRegisterTick(self.m_MailListenTick)

                self.m_MailListenTick = nil
            end

            self.m_CurrentSendingMail = nil
            self.m_SyncCreditsBtnAlert:SetActive(false)
            CLuaQMPKMgr.ProcessHandBookRedDot()
            g_MessageMgr:ShowMessage("Qmpk_Synchro_Score")
            Gac2Gas.OpenQmpkHandBook()
            self.m_OnSyncPlayerCredits = false --更新图标，显示提示 TODO
        end
    end
end

function CLuaQMPKHandBookWnd:OnStartSyncCredits()
    if(not self.m_OnSyncPlayerCredits)then
        self.m_OnSyncPlayerCredits = true
        local mail = self:FindNextMail()
        if(mail ~= nil)then     --找到邮件
            self.m_CurrentSendingMail = mail

            CMailMgr.Inst:HandleMail(self.m_CurrentSendingMail, "")
            if(self.m_MailListenTick)then
                UnRegisterTick(self.m_MailListenTick)

                self.m_MailListenTick = nil
            end

            self.m_MailListenTick = RegisterTick(function()
                if(CMailMgr.Inst.MailUpdated)then
                    CMailMgr.Inst.MailUpdated = false
                    self:TickSyncCredits()
                end
            end, 10)
        else            --未找到邮件
            self.m_OnSyncPlayerCredits = false

            g_MessageMgr:ShowMessage("Qmpk_Synchro_Score_None")
        end
    end
end

function CLuaQMPKHandBookWnd:UpdateIndicator(index)
    local lbutton = self.m_LeftRewardButton
    lbutton:SetActive(true)
    local rbutton = self.m_RightRewardButton
    rbutton:SetActive(true)
    if(index <= 1)then
        lbutton:SetActive(false)
    elseif(index + 4 >= #self.m_RewardData)then
        rbutton:SetActive(false)
    end
    self.m_LeftArrow:SetActive(false)
    self.m_RightArrow:SetActive(false)
    if not self.m_CurScrollIndex then return end
    if index + 5 < self.m_CurScrollIndex then
        self.m_RightArrow:SetActive(true)
    elseif math.max(index - 1,0) > self.m_CurScrollIndex then
        self.m_LeftArrow:SetActive(true)
    end
end

function CLuaQMPKHandBookWnd:Update()
    self:UpdateImportant(false)
end

function CLuaQMPKHandBookWnd:OnReceiveHandBookInfo(score, record_list, award_dict, isUnLock)
    --更新分数

    self.m_CreditLabel.text = tostring(score)
    self.m_Score = score
    self.m_Award_dict = award_dict
    self.m_IsUnLock = isUnLock
    self.m_FxTable = {}
    --更新奖励状态

    self.m_TableView:ReloadData(true, true)
    self:DelayShowFx()
    self:UpdateImportant(true)
    if isUnLock == 1 then
        self.m_PurchaseBtn.Text = LocalString.GetString("已购买高级战令")
        self.m_PurchaseBtn.Enabled = false
        self.m_PurchaseBtnFx:DestroyFx()
        self.transform:Find("Anchor/BottomSliderView/RewardTitle/Texture2/Lock").gameObject:SetActive(false)
    end

    --跳转至可领奖的位置,第一次接受数据才会跳转
    if self.m_NeedJump then
        --没有设置跳转，且没有领过奖励，跳转至起始位置
        if not self.m_CanOpItemIndex and not self.m_beenRewarded then
            self.m_CanOpItemIndex = 0
        end
        --没有设置跳转，且领过奖励，跳转至终点位置
        self:ScrollTo(self.m_CanOpItemIndex and self.m_CanOpItemIndex or QuanMinPK_HandBook.GetDataCount() - 5)
        self.m_NeedJump = false
    end

    --更新战绩
    do
        --获取战绩template及table
        local title_template = self.transform:Find("PersonalRecordPanel/Templates/TitleTemplate").gameObject
        local record_template = self.transform:Find("PersonalRecordPanel/Templates/RecordTemplate").gameObject
        title_template:SetActive(false)
        record_template:SetActive(false)
        local record_table = self.transform:Find("PersonalRecordPanel/ScrollView/Table").gameObject
        Extensions.RemoveAllChildren(record_table.transform)
        local title = NGUITools.AddChild(record_table,title_template)
        title:SetActive(true)
        --插入个人战绩数据
        local personal_record_num = math.min(10, #record_list)
        self.m_HasPersonalRecordData = personal_record_num > 0
        for i = 1, personal_record_num, 1 do
            local record_date = record_list[i].date
            local record_type = self.m_RecordTypeTable[record_list[i].id]
            if(record_date and record_type)then
                local new_record = NGUITools.AddChild(record_table,record_template)
                new_record:SetActive(true)
                local time = new_record.transform:Find("Time"):GetComponent(typeof(UILabel))
                local content = new_record.transform:Find("Content"):GetComponent(typeof(UILabel))
                local credits = new_record.transform:Find("Credits"):GetComponent(typeof(UILabel))
                local text = record_type.score..LocalString.GetString("[-]积分")
                --if(record_type.score > 0)then
                text = "[ffff00]+"..text
                -- else
                --     text = "[ffff00]"..text
                -- end
                credits.text = LocalString.GetString(text)
                time.text = ToStringWrap(CServerTimeMgr.ConvertTimeStampToZone8Time(record_date), LocalString.GetString("MM月dd日 HH:mm"))
                content.text = LocalString.GetString(record_type.description)
            end
        end
        record_table:GetComponent(typeof(UITable)):Reposition()
    end
end

function CLuaQMPKHandBookWnd:UpdateImportant(ForceUpdate)
    local k = math.ceil((-self.m_TableView.transform.localPosition.x - 1)/ (self.m_Grid.cellWidth)) + 1
    k = math.max(1, k)
    k = math.min(#self.m_RewardData, k)

    self:UpdateIndicator(k)--用一下k来更新指示器

    k = self.m_RewardData[k].nextHighValueKey
    if not ForceUpdate and self.m_CurrentHighValueIndex and k == self.m_CurrentHighValueIndex then    --一样就无需更新了
        return
    end
    self.m_CurrentHighValueIndex = k

    local go = self.transform:Find("Anchor/BottomSliderView/ImportantReward").gameObject
    local data = self.m_RewardData[k]

    go.transform:Find("Label"):GetComponent(typeof(UILabel)).text = System.String.Format(LocalString.GetString("达到{0}积分"),data.item1.credit)
    local item1 = go
    self:InitReward(item1, k, data.item1, 0, true, false)
    local item2 = go.transform:Find("RewardObject2").gameObject
    self:InitReward(item2, k, data.item2, CLuaQMPKHandBookWnd.m_Reward2IdOffset, true, true)
end

function CLuaQMPKHandBookWnd:InitItem(go, index)
    local data = self.m_RewardData[index]
    go.transform:Find("Label"):GetComponent(typeof(UILabel)).text = data.item1.credit .. LocalString.GetString("积分")
    local item1 = go
    self:InitReward(item1, data.k, data.item1, 0, false, false)
    local item2 = go.transform:Find( "RewardObject2").gameObject
    self:InitReward(item2, data.k, data.item2, CLuaQMPKHandBookWnd.m_Reward2IdOffset, false, true)
end

function CLuaQMPKHandBookWnd:InitReward(dst_item, k, v, idOffset, isHighValue, isNeedPass)
    local dst_item_cross = dst_item.transform:Find("StateObject/CrossSprite").gameObject
    local dst_item_lock = dst_item.transform:Find("StateObject/LockSprite").gameObject
    local dst_item_mask = dst_item.transform:Find("StateObject/Mask").gameObject
    local dst_item_icon = dst_item.transform:Find("ObjectSprite").gameObject
    local isShowBorder = self.m_Score and v.credit <= self.m_Score and (not self.m_RewardData[k+1] or self.m_RewardData[k+1].item1.credit > self.m_Score)
    if isShowBorder then self.m_CurScrollIndex = k end
    
    if not v.inited or isHighValue then     --初始化
        local item_num = dst_item.transform:Find("ObjectSprite/NumberLable"):GetComponent(typeof(UILabel))
        local item_bind = dst_item.transform:Find("ObjectSprite/BindSprite").gameObject
        local icon = dst_item.transform:Find("ObjectSprite"):GetComponent(typeof(CUITexture))
        local qualitySprite = dst_item.transform:Find("ObjectSprite/QualitySprite"):GetComponent(typeof(UISprite))
        local item = Item_Item.GetData(v.id)

        icon:LoadMaterial(item.Icon)
        qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(item.NameColor)
        item_num.text = v.num > 1 and v.num or ""

        if(v.bind == 1)then
            item_bind:SetActive(true)
        else
            item_bind:SetActive(false)
        end

        dst_item_cross:SetActive(false)
        dst_item_lock:SetActive(true)
        dst_item_mask:SetActive(true)

        UIEventListener.Get(dst_item_icon).onClick = DelegateFactory.VoidDelegate(function(g)
            CItemInfoMgr.ShowLinkItemTemplateInfo(v.id, false, nil, AlignType.Default, 0, 0, 0, 0)
        end)

        if not isHighValue then     --highValue不会被拖动
            if not isNeedPass then  --isNeedPass为false说明为根节点,需要设置下背景的拖拽事件
                UIEventListener.Get(dst_item.gameObject).onDragEnd = DelegateFactory.VoidDelegate(function(g)
                    self:OnDragEnd(g)
                end)
                UIEventListener.Get(dst_item.gameObject).onDragStart = DelegateFactory.VoidDelegate(function(g)
                    self:OnDragStart(g)
                end)
            end

            UIEventListener.Get(dst_item_icon).onDragEnd = DelegateFactory.VoidDelegate(function(g)
                self:OnDragEnd(g)
            end)
            UIEventListener.Get(dst_item_icon).onDragStart = DelegateFactory.VoidDelegate(function(g)
                self:OnDragStart(g)
            end)
        end

        v.inited = true
    end

    if v.inited then                        --更新
        local dst_item_fx = dst_item.transform:Find("FxOffset/Fx"):GetComponent(typeof(CUIFx))
        dst_item_fx:DestroyFx()
        LuaTweenUtils.DOKill(dst_item_fx.transform, false)

        local rpcKey = k + idOffset
        if (self.m_Award_dict and self.m_Award_dict[rpcKey]) then
            dst_item_cross:SetActive(true)
            dst_item_lock:SetActive(false)
            dst_item_mask:SetActive(true)
            UIEventListener.Get(dst_item_icon).onClick = DelegateFactory.VoidDelegate(function(g)
                CItemInfoMgr.ShowLinkItemTemplateInfo(v.id, false, nil, AlignType.Default, 0, 0, 0, 0)
            end)
            self.m_beenRewarded = true
        else
            if(self.m_Score and v.credit <= self.m_Score and (not isNeedPass or self.m_IsUnLock == 1))then
                dst_item_cross:SetActive(false)
                dst_item_lock:SetActive(false)
                dst_item_mask:SetActive(false)

                UIEventListener.Get(dst_item_icon).onClick = DelegateFactory.VoidDelegate(function(g)
                    local this_id = rpcKey
                    Gac2Gas.ReceiveQmpkHandBookAward(this_id)
                end)

                if not isHighValue then                     --高价值不会移动,无需额外控制
                    table.insert(self.m_FxTable, dst_item_fx.gameObject)
                end

                dst_item_fx:LoadFx(CUIFxPaths.CommonYellowLineFxPath)
                dst_item_fx.transform.localPosition = self.m_RewardButtonBound[0]
                LuaTweenUtils.DOLuaLocalPath(dst_item_fx.transform, self.m_RewardButtonBound, 2, PathType.Linear, PathMode.Ignore, 2, - 1, Ease.Linear)
            else
                dst_item_cross:SetActive(false)
                dst_item_lock:SetActive(true)
                dst_item_mask:SetActive(false)
                UIEventListener.Get(dst_item_icon).onClick = DelegateFactory.VoidDelegate(function(g)
                    CItemInfoMgr.ShowLinkItemTemplateInfo(v.id, false, nil, AlignType.Default, 0, 0, 0, 0)
                end)
            end

            if self.m_NeedJump and self.m_Award_dict and not self.m_CanOpItemIndex and not isHighValue and (not isNeedPass or self.m_IsUnLock == 1) then--用于打开时的自动跳转
                self.m_CanOpItemIndex = k
            end
        end

        if not isHighValue and not isNeedPass then  --当前分数奖励边框高亮
            local dst_item_border = dst_item.transform:Find("Border").gameObject
            dst_item_border:SetActive(isShowBorder)
        end
    end
    
end


function CLuaQMPKHandBookWnd:InitRewardPreview()
    local RewardPreview = self.transform:Find("Anchor/RewardPreview")
    local templateItem = RewardPreview.transform:Find("ItemCell").gameObject
    local table = RewardPreview.transform:Find("Scroll View/Table"):GetComponent(typeof(UITable))
    local itemList = QuanMinPK_Setting.GetData().HandBookRewardPreviewItemID
    Extensions.RemoveAllChildren(table.transform)
    for i = 0,itemList.Length - 1 do
        local item = CUICommonDef.AddChild(table.gameObject,templateItem.gameObject)
        local icon = item.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
        local qualitySprite = item.transform:Find("QualitySprite"):GetComponent(typeof(UISprite))
        local preciousSprite = item.transform:Find("PreciousSprite"):GetComponent(typeof(UISprite))
        local count = item.transform:Find("Count"):GetComponent(typeof(UILabel))
        local itemData = Item_Item.GetData(itemList[i])
        local isPrecious = itemData.ValuablesFloorPrice > 0 and itemData.PlayerShopPrice == 0
        count.text = ""
        icon:LoadMaterial(itemData.Icon)
        qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(itemData.NameColor)
        preciousSprite.gameObject:SetActive(isPrecious)
        UIEventListener.Get(item).onClick = DelegateFactory.VoidDelegate(function(g)
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemList[i], false, nil, AlignType.Default, 0, 0, 0, 0)
        end)
    end
    templateItem.gameObject:SetActive(false)
    table:Reposition()

    local endTimeStr = QuanMinPK_Setting.GetData().HandBookLiBaoOutTime
    local timeEnd = CServerTimeMgr.Inst:GetTimeStampByStr(endTimeStr)
    local curTimeStamp = CServerTimeMgr.Inst.timeStamp
    self.transform:Find("Anchor/BottomView/BuyPackageButton").gameObject:SetActive(curTimeStamp < timeEnd)
end
