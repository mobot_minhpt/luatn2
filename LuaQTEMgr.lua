local CommonDefs = import "L10.Game.CommonDefs"
local Vector3 = import "UnityEngine.Vector3"
local NGUITools = import "NGUITools"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local Object = import "System.Object"
local SoundManager = import "SoundManager"
local PlayerSettings = import "L10.Game.PlayerSettings"
local CQTEMgr = import "L10.Game.CutScene.CQTEMgr"

LuaQTEMgr = {}
LuaQTEMgr.CurrentQTEType = nil
LuaQTEMgr.CurrentQTEInfo = nil
LuaQTEMgr.QTEWndRoot = nil
LuaQTEMgr.QTEFinishCallback = nil
LuaQTEMgr.SoundInstance = nil
LuaQTEMgr.m_DelayFinishTick = nil
LuaQTEMgr.m_DelaySeconds = 1.0

function LuaQTEMgr.SetQTEWndGO(go)
    LuaQTEMgr.QTEWndRoot = go
end

function LuaQTEMgr.InitQTE(qteID, duration, timeOffset, isNormalSceneQte, callback)
    LuaQTEMgr.ParseQTEInfo(qteID)
    UnRegisterTick(LuaQTEMgr.m_DelayFinishTick)
    if LuaQTEMgr.CurrentQTEInfo then
        LuaQTEMgr.CurrentQTEInfo.isNormalSceneQte = isNormalSceneQte
        LuaQTEMgr.CurrentQTEInfo.qteDuration = duration
        LuaQTEMgr.CurrentQTEInfo.qteTimeOffset = timeOffset
        LuaQTEMgr.CurrentQTEType = LuaQTEMgr.CurrentQTEInfo.Type

        --在PC和editor下将摇一摇QTE替换为单次点击
        if LuaQTEMgr.CurrentQTEType == EnumQTEType.Shake and CommonDefs.IsPCGameMode() then
            LuaQTEMgr.CurrentQTEType = EnumQTEType.SingleClick
            LuaQTEMgr.CurrentQTEInfo.Type = EnumQTEType.SingleClick
        end

        if LuaQTEMgr.QTEWndRoot then
            LuaQTEMgr.QTEWndRoot:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:InitOneWnd(LuaQTEMgr.CurrentQTEType, LuaQTEMgr.CurrentQTEInfo.Message)
        end

        LuaQTEMgr.QTEFinishCallback = callback

        --播放QTE音效
        LuaQTEMgr.StartQTESound()
    end
end

-- 读表
function LuaQTEMgr.ParseQTEInfo(qteID)
    --[[ID,Type,qteDuration,qteTimeOffset, Sound
    Shake: {xPos,yPos}
    SingleClick: {xPos,yPos}
    ConsecutiveClick: {xPos,yPos,initProgress,decreaseRate,increaseRate}
    SequentialClick: {iconList = {{xPos,yPos,duration,timeDelta},...}}
    Slide: {xPos,yPos,slideDirection}
    --]]

    local rawInfo = QTE_QTE.GetData(qteID)

    if not (rawInfo and rawInfo.ID and rawInfo.Params and rawInfo.Type) then
        LuaQTEMgr.CurrentQTEInfo = nil
        return
    end

    LuaQTEMgr.CurrentQTEInfo = {}
    local infoTable = LuaQTEMgr.CurrentQTEInfo

    infoTable.ID = rawInfo.ID
    infoTable.Type = rawInfo.Type

    infoTable.Sound = rawInfo.Sound or ""
    infoTable.Message = rawInfo.Message or ""
    infoTable.HintFx = rawInfo.HintFx
    infoTable.SuccessFx = rawInfo.SuccessFx ~= "" and SafeStringFormat3("Fx/UI/Prefab/%s.prefab", rawInfo.SuccessFx) or nil
    infoTable.FailedFx = rawInfo.FailedFx ~= "" and SafeStringFormat3("Fx/UI/Prefab/%s.prefab", rawInfo.FailedFx) or nil

    local validParams = true

    if infoTable.Type == EnumQTEType.Shake then
        infoTable.xPos, infoTable.yPos = string.match(rawInfo.Params, "([%.%d]+),([%.%d]+)")
        if not (infoTable.xPos and infoTable.yPos) then
            validParams = false
        end

    elseif infoTable.Type == EnumQTEType.SingleClick then
        infoTable.xPos, infoTable.yPos = string.match(rawInfo.Params, "([%.%d]+),([%.%d]+)")

        if not (infoTable.xPos and infoTable.yPos) then
            validParams = false
        end

    elseif infoTable.Type == EnumQTEType.ConsecutiveClick then
        infoTable.xPos, infoTable.yPos, infoTable.initProgress, infoTable.decreaseRate, infoTable.increaseRate =
            string.match(rawInfo.Params, "([%.%d]+),([%.%d]+),([%.%d]+),([%.%d]+),([%.%d]+)")
        if
            not (infoTable.xPos and infoTable.yPos and infoTable.initProgress and infoTable.decreaseRate and
                infoTable.increaseRate)
         then
            validParams = false
        end

    elseif infoTable.Type == EnumQTEType.SequentialClick then
        infoTable.iconList = {}
        for iconInfo in string.gmatch(rawInfo.Params, "([^;]+)") do
            local curInfo = {}
            curInfo.xPos, curInfo.yPos, curInfo.duration =
                string.match(iconInfo, "([%.%d]+),([%.%d]+),([%.%d]+)")
            table.insert(infoTable.iconList, curInfo)
            if not (curInfo.xPos and curInfo.yPos and curInfo.duration) then validParams = false end
        end
        if #infoTable.iconList == 0 then validParams = false end

    elseif infoTable.Type == EnumQTEType.Slide then
        infoTable.xPos, infoTable.yPos, infoTable.slideDirection =
            string.match(rawInfo.Params, "([%.%d]+),([%.%d]+),([%.%d]+)")
        if not (infoTable.xPos and infoTable.yPos and infoTable.slideDirection) then validParams = false end

    else
        validParams = false
    end

    if not validParams then
        LuaQTEMgr.CurrentQTEInfo = nil
    end
end

function LuaQTEMgr.StartQTESound()
    local soundName = LuaQTEMgr.CurrentQTEInfo.Sound
    LuaQTEMgr.StopQTESound()
    if soundName and soundName ~= "" and PlayerSettings.SoundEnabled and SoundManager.Inst then
        LuaQTEMgr.SoundInstance = SoundManager.Inst:PlaySound(soundName, Vector3.zero, 1.0, nil, 0)
    end
end

function LuaQTEMgr.StopQTESound()
    if LuaQTEMgr.SoundInstance and SoundManager.Inst then
        SoundManager.Inst:StopSound(LuaQTEMgr.SoundInstance)
    end
    LuaQTEMgr.SoundInstance = nil
end

function LuaQTEMgr.FinishCurrentQTE(success, ignoreNotifyServer)
    --非过场动画的QTE结束后有个结束特效，为了确保特效可以被看到，这里延迟一段时间再关闭窗口
    --主要是通过延长rpc发送来达到此目的，如果rpc不延迟会有很多奇怪问题出现
    UnRegisterTick(LuaQTEMgr.m_DelayFinishTick)
    if LuaQTEMgr.CurrentQTEInfo.isNormalSceneQte and not ignoreNotifyServer then
       
        LuaQTEMgr.m_DelayFinishTick = RegisterTickOnce(function ()
            LuaQTEMgr.FinishCurrentQTEInternal(success, ignoreNotifyServer)
            LuaQTEMgr.m_DelayFinishTick = nil
        end, LuaQTEMgr.m_DelaySeconds * 1000)
    else
        LuaQTEMgr.FinishCurrentQTEInternal(success, ignoreNotifyServer)
    end
end
--不要直接调用此方法FinishCurrentQTEInternal
function LuaQTEMgr.FinishCurrentQTEInternal(success, ignoreNotifyServer)
    if not ignoreNotifyServer then
        LuaQTEMgr.OnQteFinished(LuaQTEMgr.CurrentQTEInfo.ID, success)
    end

    LuaQTEMgr.StopQTESound()
    
    if LuaQTEMgr.QTEFinishCallback then
        GenericDelegateInvoke(LuaQTEMgr.QTEFinishCallback, Table2ArrayWithCount({success}, 1, MakeArrayClass(Object)))
    end
    LuaQTEMgr.QTEFinishCallback = nil
    LuaQTEMgr.CurrentQTEInfo = nil --结束时清除数据
end

function LuaQTEMgr.OnQTETimeLimitExceeded()
    LuaQTEMgr.OnQteFinished(LuaQTEMgr.CurrentQTEInfo.ID, false)

    if LuaQTEMgr.QTEWndRoot then
        LuaQTEMgr.QTEWndRoot:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:MakeCurQTETimeExceed()
    end

    LuaQTEMgr.StopQTESound()

    if LuaQTEMgr.QTEFinishCallback then
        GenericDelegateInvoke(LuaQTEMgr.QTEFinishCallback, Table2ArrayWithCount({false}, 1, MakeArrayClass(Object)))
    end
    LuaQTEMgr.QTEFinishCallback = nil
end

function LuaQTEMgr.ResetAllQTE()
    LuaQTEMgr.StopQTESound()
    LuaQTEMgr.QTEFinishCallback = nil
    if LuaQTEMgr.QTEWndRoot then
        LuaQTEMgr.QTEWndRoot:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:ResetAllQTEWnd()
    end
end

function LuaQTEMgr.TriggerQTEFinishFx(success, position)
    LuaQTEMgr.QTEWndRoot:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:TriggerFinishFx(success, position)
end

-- 将表中的坐标转化为给定窗口的localPosition
function LuaQTEMgr.ParsePosition(go, xPos, yPos)
    local viewPortVec = Vector3(xPos / 1920, yPos / 1080)
    local camera = NGUITools.FindCameraForLayer(go.layer)
    local worldPosition = camera:ViewportToWorldPoint(viewPortVec)
    local rp = go.transform.parent:InverseTransformPoint(worldPosition)
    return Vector3(rp.x, rp.y, go.transform.localPosition.z)
end

function LuaQTEMgr.TriggerNormalSceneQTE(qteId, duration, usedInMainUI)
    if not QTE_QTE.Exists(qteId) then return end
    CQTEMgr.Inst:TriggerNormalSceneQTE(qteId, duration or 10,usedInMainUI)
end

function LuaQTEMgr.CloseNormalSceneQTE(qteId)
    --如果QTE窗口尚未准备完毕，停止加载
    if CQTEMgr.Inst.normalSceneQTELoadingCoroutine then
        CQTEMgr.Inst:CancelNormalSceneQTECoroutine()
    --如果QTE窗口已经准备完毕且qteId满足rpc参数，则关闭
    elseif LuaQTEMgr.CurrentQTEInfo and LuaQTEMgr.CurrentQTEInfo.ID and LuaQTEMgr.CurrentQTEInfo.ID == qteId then
        LuaQTEMgr.FinishCurrentQTE(false, true) --结果不通知服务器，服务器端已按失败处理
    end
end

function LuaQTEMgr.OnQteFinished(qteId, bSuccess)
    g_ScriptEvent:BroadcastInLua("OnQteFinished", bSuccess)
    if bSuccess then
        Gac2Gas.OnQteSuccess(qteId)
    else
        Gac2Gas.OnQteFailed(qteId)
    end
end
--------------------- 封装RPC，在Cs代码下调用

function LuaQTEMgr.OnGameVideoQteSuccess(name)
    Gac2Gas.OnGameVideoQteSuccess(name)
end

function LuaQTEMgr.OnGameVideoQteFailed(name)
    Gac2Gas.OnGameVideoQteFailed(name)
end

