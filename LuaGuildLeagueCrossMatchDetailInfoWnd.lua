local DelegateFactory  = import "DelegateFactory"
local UISprite = import "UISprite"
local UIScrollView  = import "UIScrollView"
local UITable = import "UITable"
local Extensions = import "Extensions"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CButton = import "L10.UI.CButton"
local UIScrollViewIndicator = import "UIScrollViewIndicator"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"

LuaGuildLeagueCrossMatchDetailInfoWnd = class()

RegistClassMember(LuaGuildLeagueCrossMatchDetailInfoWnd, "m_Server1Root")
RegistClassMember(LuaGuildLeagueCrossMatchDetailInfoWnd, "m_Server2Root")
RegistClassMember(LuaGuildLeagueCrossMatchDetailInfoWnd, "m_TrainAdditionButton1")
RegistClassMember(LuaGuildLeagueCrossMatchDetailInfoWnd, "m_TrainAdditionButton2")


RegistClassMember(LuaGuildLeagueCrossMatchDetailInfoWnd, "m_GuildBattleTemplate")
RegistClassMember(LuaGuildLeagueCrossMatchDetailInfoWnd, "m_ScrollView")
RegistClassMember(LuaGuildLeagueCrossMatchDetailInfoWnd, "m_Table")
RegistClassMember(LuaGuildLeagueCrossMatchDetailInfoWnd, "m_RefreshButton")
RegistClassMember(LuaGuildLeagueCrossMatchDetailInfoWnd, "m_ShareButton")

function LuaGuildLeagueCrossMatchDetailInfoWnd:Awake()
    self.m_Server1Root = self.transform:Find("Anchor/Header/Server1")
    self.m_Server2Root = self.transform:Find("Anchor/Header/Server2")
    self.m_TrainAdditionButton1 = self.transform:Find("Anchor/Header/TrainAddition1").gameObject
    self.m_TrainAdditionButton2 = self.transform:Find("Anchor/Header/TrainAddition2").gameObject
    self.m_GuildBattleTemplate = self.transform:Find("Anchor/ScrollView/Item").gameObject
    self.m_ScrollView = self.transform:Find("Anchor/ScrollView"):GetComponent(typeof(UIScrollView))
    self.m_Table = self.transform:Find("Anchor/ScrollView/Table"):GetComponent(typeof(UITable))
    self.m_RefreshButton = self.transform:Find("Anchor/Buttons/RefreshButton"):GetComponent(typeof(CButton))
    self.m_ShareButton = self.transform:Find("Anchor/Buttons/ShareButton"):GetComponent(typeof(CButton))
    self.m_GuildBattleTemplate:SetActive(false)
    UIEventListener.Get(self.m_TrainAdditionButton1).onClick = DelegateFactory.VoidDelegate(function() self:OnTrainAddtionButtonClick() end)
    UIEventListener.Get(self.m_TrainAdditionButton2).onClick = DelegateFactory.VoidDelegate(function() self:OnTrainAddtionButtonClick() end)
    UIEventListener.Get(self.m_RefreshButton.gameObject).onClick = DelegateFactory.VoidDelegate(function() self:OnRefreshButtonClick() end)
    UIEventListener.Get(self.m_ShareButton.gameObject).onClick = DelegateFactory.VoidDelegate(function() self:OnShareButtonClick() end)
    local scrollViewIndicator = self.transform:Find("Anchor/ScrollViewIndicator"):GetComponent(typeof(UIScrollViewIndicator))
    scrollViewIndicator.disableLayout = true
end

function LuaGuildLeagueCrossMatchDetailInfoWnd:Init()
    self:LoadData()
    LuaGuildLeagueCrossMgr:QueryGLCServerMatchInfo()
end

function LuaGuildLeagueCrossMatchDetailInfoWnd:OnEnable()
    g_ScriptEvent:AddListener("OnQueryGLCServerMatchInfoResultSuccess", self, "OnQueryGLCServerMatchInfoResultSuccess")
end

function LuaGuildLeagueCrossMatchDetailInfoWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnQueryGLCServerMatchInfoResultSuccess", self, "OnQueryGLCServerMatchInfoResultSuccess")

end

function LuaGuildLeagueCrossMatchDetailInfoWnd:OnQueryGLCServerMatchInfoResultSuccess()
    self:LoadData()
end

function LuaGuildLeagueCrossMatchDetailInfoWnd:OnTrainAddtionButtonClick()
    local data = LuaGuildLeagueCrossMgr.m_ServerMatchDetailInfo
    if data then
        LuaGuildLeagueCrossMgr:ShowTrainAdditionWnd(data.serverName1, data.serverName2, data.trainAdditionInfo)
    end
end

function LuaGuildLeagueCrossMatchDetailInfoWnd:OnRefreshButtonClick()
    LuaGuildLeagueCrossMgr:QueryGLCServerMatchInfo()
end

function LuaGuildLeagueCrossMatchDetailInfoWnd:OnShareButtonClick()
    local data = LuaGuildLeagueCrossMgr.m_ServerMatchDetailInfo or nil
    local myServerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst:GetMyServerId() or 0
    
    if data and myServerId>0 then
        local otherServerName = " "
        if myServerId == data.serverId1 then
            otherServerName = data.serverName2
        else
            otherServerName = data.serverName1
        end
        local score1 = data.score1 and data.score1 or 0
        local score2 = data.score2 and data.score2 or 0
        local round, index1, index2 = LuaGuildLeagueCrossMgr:GetRoundInfoByMatchIndex(data.matchIndex)

        local shareText = g_MessageMgr:FormatMessage("GLC_Match_Detail_Info_Share_Text", otherServerName, 
            self:GetRoundName(round, index1, index2), tostring(score1), tostring(score2), data.matchIndex)
        LuaInGameShareMgr.Share(EnumInGameShareChannelDefine.eGuildLeagueCrossShare, function()
            return shareText
        end, self.m_ShareButton.transform, CPopupMenuInfoMgr.AlignType.Top, nil)
    end   
end

function LuaGuildLeagueCrossMatchDetailInfoWnd:GetRoundName(round, index1, index2)
    if round==1 then return LocalString.GetString("小组赛第一轮") end
    if round==2 then return LocalString.GetString("小组赛第二轮") end
    if round==3 then return LocalString.GetString("小组赛第三轮") end
    if round==4 then return LocalString.GetString("排位赛第一轮") end
    if round==5 then return LocalString.GetString("排位赛第二轮") end
    if round==6 then return LocalString.GetString("排位赛第三轮") end
    if round==7 then return LocalString.GetString("决赛") end
    return " "
end

function LuaGuildLeagueCrossMatchDetailInfoWnd:LoadData()
    local data = LuaGuildLeagueCrossMgr.m_ServerMatchDetailInfo or nil
    self.m_TrainAdditionButton1:SetActive(data~=nil)
    self.m_TrainAdditionButton2:SetActive(data~=nil)
    if not data then return end
    local allowWatch = LuaGuildLeagueCrossMgr:IsQualifiedWatchPlayer()
    self.m_RefreshButton.gameObject:SetActive(allowWatch)
    local needShowScore = data.score1~=nil and data.score2~=nil and (data.score1>0 or data.score2>0)
    local existInProgressBattle = false
    self:InitOneServer(self.m_Server1Root, data.serverId1, data.serverName1, data.score1, data.winServerId, needShowScore)
    self:InitOneServer(self.m_Server2Root, data.serverId2, data.serverName2, data.score2, data.winServerId, needShowScore)
    Extensions.RemoveAllChildren(self.m_Table.transform)
    if data.battleInfo~=nil then
        for i=1, #data.battleInfo do
            local child = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_GuildBattleTemplate)
            child:SetActive(true)
            local info = data.battleInfo[i]
            self:InitOneGuildBattle(i, child.transform, info.guildId1, info.guildId2, info.guildName1, info.guildName2, info.winSide, info.score, info.rank1, info.rank2, allowWatch)
            if i % 2 == 1 then
                child:GetComponent(typeof(CButton)):SetBackgroundSprite(g_sprites.OddBgSpirite)
            else
                child:GetComponent(typeof(CButton)):SetBackgroundSprite(g_sprites.EvenBgSprite)
            end
            UIEventListener.Get(child.gameObject).onClick = DelegateFactory.VoidDelegate(function() self:GuildBattleItemClick(child.gameObject, info) end)
            --存在正在进行的对战
            if info.guildId1 and info.guildId1>0 and info.guildId2 and info.guildId2>0 and info.winSide==0 then
                existInProgressBattle = true
            end
        end
        self.m_Table:Reposition()
        self.m_ScrollView:ResetPosition()
    end
    --仅当玩家在非跨服，对战服务器之一是本服并且有正在进行中的对战时才显示分享按钮
    local myServerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst:GetMyServerId() or 0
    self.m_ShareButton.gameObject:SetActive(not CClientMainPlayer.Inst.IsCrossServerPlayer and existInProgressBattle and myServerId>0 and (myServerId == data.serverId1 or myServerId == data.serverId2))
end

function LuaGuildLeagueCrossMatchDetailInfoWnd:InitOneServer(transRoot, serverId, serverName, score, winServerId, needShowScore)
    local serverResultIcon = transRoot:Find("ResultIcon"):GetComponent(typeof(UISprite))
    local serverNameLabel = transRoot:Find("NameLabel"):GetComponent(typeof(UILabel))
    local serverScoreLabel = transRoot:Find("ScoreLabel"):GetComponent(typeof(UILabel))

    if serverId == nil then
        serverNameLabel.text = ""
        serverResultIcon.spriteName = ""
        serverScoreLabel.gameObject:SetActive(false)
        return
    end

    local isMyServer = CClientMainPlayer.Inst and CClientMainPlayer.Inst:GetMyServerId() == serverId or false

    serverNameLabel.text = SafeStringFormat3("%s%s", isMyServer and "[79ff7c]" or "", serverName)
    serverScoreLabel.gameObject:SetActive(needShowScore)
    serverScoreLabel.text = tostring(score)
    if winServerId>0 then --比赛已结束
        serverResultIcon.gameObject:SetActive(true)
        serverResultIcon.spriteName = (winServerId == serverId) and "common_fight_result_win" or "common_fight_result_lose"
    else --比赛进行中或尚未开始
        serverResultIcon.gameObject:SetActive(false)
    end

    CUICommonDef.SetGrey(transRoot.gameObject, winServerId>0 and winServerId~=serverId)
end

function LuaGuildLeagueCrossMatchDetailInfoWnd:InitOneGuildBattle(index, transRoot,guildId1, guildId2, guildName1, guildName2, winSide, score, rank1, rank2, allowWatch)
    local score1Label = transRoot:Find("Score1Label"):GetComponent(typeof(UILabel))
    local score2Label = transRoot:Find("Score2Label"):GetComponent(typeof(UILabel))
    local name1Label = transRoot:Find("Name1Label"):GetComponent(typeof(UILabel))
    local name2Label = transRoot:Find("Name2Label"):GetComponent(typeof(UILabel))
    local result1Icon = transRoot:Find("Result1Icon"):GetComponent(typeof(UISprite))
    local result2Icon = transRoot:Find("Result2Icon"):GetComponent(typeof(UISprite))
    local battleIcon = transRoot:Find("BattleIcon"):GetComponent(typeof(UISprite))
    local rank1Label = transRoot:Find("Rank1Label"):GetComponent(typeof(UILabel))
    local rank2Label = transRoot:Find("Rank2Label"):GetComponent(typeof(UILabel))
    local watchButton = transRoot:Find("WatchButton").gameObject

    battleIcon.spriteName = guildId1==nil or guildId1==0 and winSide==0 and "fightingspiritwnd_attack" or "common_vs"

    local scoreText = winSide>0 and SafeStringFormat3(LocalString.GetString("+%d分"), score) or SafeStringFormat3(LocalString.GetString("%d分"), score)
    if winSide>0 then --比出了结果
        score1Label.text = winSide==1 and scoreText or ""
        score2Label.text = winSide==2 and scoreText or ""
        score1Label.alpha = 1
        score2Label.alpha = 1
        result1Icon.spriteName = winSide==1 and "common_fight_result_win" or "common_fight_result_lose"
        result2Icon.spriteName = winSide==2 and "common_fight_result_win" or "common_fight_result_lose"
        battleIcon.spriteName = "common_vs"
    else
        score1Label.text = scoreText
        score2Label.text = scoreText
        score1Label.alpha = 0.3
        score2Label.alpha = 0.3
        result1Icon.spriteName = ""
        result2Icon.spriteName = ""
    end

    if guildId1==nil or guildId1==0 then -- 还没有对阵信息
        name1Label.text = LocalString.GetString("暂未公布")
        name2Label.text = LocalString.GetString("暂未公布")
        name1Label.alpha = 0.3
        name2Label.alpha = 0.3
    else
        local myGuildId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.GuildId or -1
        name1Label.text = SafeStringFormat3("%s%s", myGuildId == guildId1 and "[00ff60]" or "", guildName1)
        name2Label.text = SafeStringFormat3("%s%s", myGuildId == guildId2 and "[00ff60]" or "", guildName2)
        name1Label.alpha = 1
        name2Label.alpha = 1
    end
    rank1Label.text = tostring(rank1)
    rank2Label.text = tostring(rank2)

    watchButton:SetActive(guildId1>0 and winSide==0 and LuaGuildLeagueCrossMgr:IsQualifiedWatchPlayer())
    UIEventListener.Get(watchButton).onClick = DelegateFactory.VoidDelegate(function()
        LuaGuildLeagueCrossMgr:RequestWatchGuildLeagueCrossPlay(LuaGuildLeagueCrossMgr.m_ServerMatchDetailInfo.matchIndex, index)
    end)
end

function LuaGuildLeagueCrossMatchDetailInfoWnd:GuildBattleItemClick(go, info)
    local n = self.m_Table.transform.childCount
    for i=0,n-1 do
        local child = self.m_Table.transform:GetChild(i)
        local button = child:GetComponent(typeof(CButton))
        if child.gameObject == go then
            button.Selected = true
        else
            button.Selected = false
        end
    end
end

