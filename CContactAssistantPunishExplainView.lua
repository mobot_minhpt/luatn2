-- Auto Generated!!
local CContactAssistantPunishExplainView = import "L10.UI.CContactAssistantPunishExplainView"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local ContactAssistantDataMgr = import "L10.Game.ContactAssistantDataMgr"
local DelegateFactory = import "DelegateFactory"
local ENUM_AVATAR_RESULT = import "ENUM_AVATAR_RESULT"
local Gac2Gas = import "L10.Game.Gac2Gas"
local HTTPHelper = import "L10.Game.HTTPHelper"
local LocalString = import "LocalString"
local Main = import "L10.Engine.Main"
local NativeHandle = import "NativeHandle"
local Random = import "UnityEngine.Random"
local String = import "System.String"
local UIInput = import "UIInput"
CContactAssistantPunishExplainView.m_init_CS2LuaHook = function (this) 
    this.titleLabel.text = ""
    CommonDefs.GetComponent_Component_Type(this.titleLabel.parent, typeof(UIInput)).value = ""
    this.desLabel.text = ""
    CommonDefs.GetComponent_Component_Type(this.desLabel.parent, typeof(UIInput)).value = ""
    this.punishTypeLabel.text = this.defaultPunishType
    this.nameLabel.text = LocalString.GetString("非必填")
    CommonDefs.GetComponent_Component_Type(this.nameLabel.parent, typeof(UIInput)).value = ""
    this.phoneLabel.text = LocalString.GetString("非必填")
    CommonDefs.GetComponent_Component_Type(this.phoneLabel.parent, typeof(UIInput)).value = ""

    this.m_Native = CreateFromClass(NativeHandle)
    this.m_texture = nil
    this.uploadPicTexture.mainTexture = nil
end
CContactAssistantPunishExplainView.m_OnAvaterCallBack_CS2LuaHook = function (this, strResult) 
    if (strResult == ToStringWrap(ENUM_AVATAR_RESULT.eResult_Success)) then
        -- 成功
        this:StartCoroutine(this:LoadTexture("upload_image.png"))
    elseif (strResult == ToStringWrap(ENUM_AVATAR_RESULT.eResult_Cancel)) then
        -- 取消
    elseif (strResult == ToStringWrap(ENUM_AVATAR_RESULT.eResult_Failed)) then
        -- 失败
    end
end
CContactAssistantPunishExplainView.m_SplitStringToS_CS2LuaHook = function (this, info) 
    local MaxByteNum = 255
    local result = CreateFromClass(MakeGenericClass(List, String))

    if CommonDefs.UTF8Encoding_GetByteCount(info) <= MaxByteNum then
        CommonDefs.ListAdd(result, typeof(String), info)
    else
        local index = 0
        local count = 1
        do
            local i = 0
            while i < CommonDefs.StringLength(info) - 1 do
                local str1 = CommonDefs.StringSubstring2(info, index, count)
                local str2 = CommonDefs.StringSubstring2(info, index, count + 1)
                count = count + 1
                if CommonDefs.UTF8Encoding_GetByteCount(str1) <= MaxByteNum and CommonDefs.UTF8Encoding_GetByteCount(str2) > MaxByteNum then
                    CommonDefs.ListAdd(result, typeof(String), str1)
                    index = i + 1
                    count = 1
                elseif i == CommonDefs.StringLength(info) - 2 then
                    CommonDefs.ListAdd(result, typeof(String), str2)
                end
                i = i + 1
            end
        end
    end

    return result
end
CContactAssistantPunishExplainView.m_SubmitInfoToGM_CS2LuaHook = function (this, go) 
    if ContactAssistantDataMgr.Inst.MaintenanceZoneUniToken == nil then
        return
    end

    local titleText = StringTrim(this.titleLabel.text)

    if CommonDefs.StringLength(titleText) <= 0 then
        g_MessageMgr:ShowMessage("Title_Cannot_Be_Empty")
        return
    end

    local desText = StringTrim(this.desLabel.text)

    if CommonDefs.StringLength(desText) <= 0 then
        g_MessageMgr:ShowMessage("Content_Cannot_Be_Empty")
        return
    end

    if (this.punishTypeLabel.text == this.defaultPunishType) then
        g_MessageMgr:ShowMessage("Selection_Type")
        return
    end

    local picId = ""

    local urs_type = ""
    --TODO check ios platform 
    if not (CLoginMgr.Inst.m_UniSdkLoginInfo.UniSdk_ChannelName == "netease") then
        urs_type = "qnm"
    end

    local token = math.floor((Random.value * 100000))

    if this.m_texture ~= nil then
        Main.Inst:StartCoroutine(HTTPHelper.UploadFile(this.m_texture, DelegateFactory.Action_string_string_string(function (code, imageId, message) 
            --Debug.LogError("code" + code + " imageId:" + imageId + " message:" + message);
            picId = imageId
            Gac2Gas.SubmitMaintenanceZoneQuestionBegin(token)

            local desList = this:SplitStringToS(desText)
            do
                local i = 0
                while i < desList.Count do
                    --Debug.Log(desList[i]);
                    Gac2Gas.SubmitMaintenanceZoneQuestion(token, desList[i])
                    i = i + 1
                end
            end
            Gac2Gas.SubmitAppealQuestionEnd(token, titleText, this.nameLabel.text, this.phoneLabel.text, picId, this.punishTypeLabel.text)
        end)))
    else
        Gac2Gas.SubmitMaintenanceZoneQuestionBegin(token)

        local desList = this:SplitStringToS(desText)
        do
            local i = 0
            while i < desList.Count do
                --Debug.Log(desList[i]);
                Gac2Gas.SubmitMaintenanceZoneQuestion(token, desList[i])
                i = i + 1
            end
        end
        Gac2Gas.SubmitAppealQuestionEnd(token, titleText, this.nameLabel.text, this.phoneLabel.text, picId, this.punishTypeLabel.text)
    end
end
