require("ui/starbiwu/LuaStarBiwuJingCaiItem")
local UILabel = import "UILabel"
local UIEventListener = import "UIEventListener"
local MsgPackImpl = import "MsgPackImpl"
local UITable = import "UITable"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Extensions = import "Extensions"
local Object=import "System.Object"
local QnButton = import "L10.UI.QnButton"

CLuaStarBiwuFinalJingCaiWnd=class()
CLuaStarBiwuFinalJingCaiWnd.Path = "ui/starbiwu/LuaStarBiwuFinalJingCaiWnd"

RegistClassMember(CLuaStarBiwuFinalJingCaiWnd,"m_currentBetPoolLabel")
RegistClassMember(CLuaStarBiwuFinalJingCaiWnd,"m_targetDateLabel")
RegistClassMember(CLuaStarBiwuFinalJingCaiWnd,"m_targetStageNameLabel")
RegistClassMember(CLuaStarBiwuFinalJingCaiWnd,"m_maxBetValueLabel")
RegistClassMember(CLuaStarBiwuFinalJingCaiWnd,"m_OperateBtn")                     --投注按钮
RegistClassMember(CLuaStarBiwuFinalJingCaiWnd,"m_BottomRoot")                     --底部节点
RegistClassMember(CLuaStarBiwuFinalJingCaiWnd,"m_FinishLabel")
RegistClassMember(CLuaStarBiwuFinalJingCaiWnd,"m_ContentRoot")
RegistClassMember(CLuaStarBiwuFinalJingCaiWnd,"m_ItemTemplate")
RegistClassMember(CLuaStarBiwuFinalJingCaiWnd,"m_CostAndOwnMoney")               --价格
RegistClassMember(CLuaStarBiwuFinalJingCaiWnd,"m_TipButton")
RegistClassMember(CLuaStarBiwuFinalJingCaiWnd,"m_TabView")
RegistClassMember(CLuaStarBiwuFinalJingCaiWnd,"m_AdvView")
RegistClassMember(CLuaStarBiwuFinalJingCaiWnd,"m_RecordTipLabel")
RegistClassMember(CLuaStarBiwuFinalJingCaiWnd,"m_AwardtButton")
RegistClassMember(CLuaStarBiwuFinalJingCaiWnd,"m_AwardtLabel")
RegistClassMember(CLuaStarBiwuFinalJingCaiWnd,"m_tabAlert")
RegistClassMember(CLuaStarBiwuFinalJingCaiWnd,"m_btnAlert")
RegistClassMember(CLuaStarBiwuFinalJingCaiWnd,"m_AddSubAndInputButton")
RegistClassMember(CLuaStarBiwuFinalJingCaiWnd,"NormalRoot")
RegistClassMember(CLuaStarBiwuFinalJingCaiWnd,"NoRpcRoot")

RegistClassMember(CLuaStarBiwuFinalJingCaiWnd,"m_currentBetPool")         --奖池
RegistClassMember(CLuaStarBiwuFinalJingCaiWnd,"m_targetDate")             --比赛日
RegistClassMember(CLuaStarBiwuFinalJingCaiWnd,"m_targetStageName")        --赛程
RegistClassMember(CLuaStarBiwuFinalJingCaiWnd,"m_maxBetValue")            --投注上限
RegistClassMember(CLuaStarBiwuFinalJingCaiWnd,"m_canBet")                 --能否参与竞猜
RegistClassMember(CLuaStarBiwuFinalJingCaiWnd,"m_phaseBet")
RegistClassMember(CLuaStarBiwuFinalJingCaiWnd,"m_jingcaiPhaseNum")
RegistClassMember(CLuaStarBiwuFinalJingCaiWnd,"m_jingcaiItemTblUd")       --竞猜项
RegistClassMember(CLuaStarBiwuFinalJingCaiWnd,"m_resultTable")            --竞猜结果
RegistClassMember(CLuaStarBiwuFinalJingCaiWnd,"m_canBetValue")              --份数上限（由投注上限和已投注数量决定）
RegistClassMember(CLuaStarBiwuFinalJingCaiWnd,"m_XiaZhuUnit")               --每份数量（读表）
RegistClassMember(CLuaStarBiwuFinalJingCaiWnd,"m_Cost")
RegistClassMember(CLuaStarBiwuFinalJingCaiWnd,"m_minXiazhu")
RegistClassMember(CLuaStarBiwuFinalJingCaiWnd,"m_RecordTable")
RegistClassMember(CLuaStarBiwuFinalJingCaiWnd,"m_canGetReward")

RegistClassMember(CLuaStarBiwuFinalJingCaiWnd, "m_VSLabel")
RegistClassMember(CLuaStarBiwuFinalJingCaiWnd, "m_MatchInfoUd")

function CLuaStarBiwuFinalJingCaiWnd:Awake()
    self.m_minXiazhu = 1
    self.NormalRoot = self.transform:Find("QnTabView/JingCai/Normal").gameObject
    self.NormalRoot:SetActive(true)
    self.NoRpcRoot = self.transform:Find("QnTabView/JingCai/NoRpc").gameObject
    self.NoRpcRoot:SetActive(false)
    self.m_currentBetPoolLabel = self.transform:Find("QnTabView/JingCai/Normal/Top/Label/Label"):GetComponent(typeof(UILabel))
    self.m_targetDateLabel = self.transform:Find("QnTabView/JingCai/Normal/Top/Time/Label"):GetComponent(typeof(UILabel))
    self.m_targetStageNameLabel = self.transform:Find("QnTabView/JingCai/Normal/Top/Progress/Label"):GetComponent(typeof(UILabel))

    self.m_VSLabel = self.transform:Find("QnTabView/JingCai/Normal/Top/VS/Label"):GetComponent(typeof(UILabel))

    self.m_maxBetValueLabel = self.transform:Find("QnTabView/JingCai/Normal/Top/Limit/Label"):GetComponent(typeof(UILabel))
    self.m_ContentRoot = self.transform:Find("QnTabView/JingCai/Normal/ContentRoot"):GetComponent(typeof(UITable))
    self.m_BottomRoot = self.transform:Find("QnTabView/JingCai/Normal/Bottom").gameObject
    self.m_CostAndOwnMoney = self.transform:Find("QnTabView/JingCai/Normal/Bottom/QnCostAndOwnMoney"):GetComponent(typeof(CCurentMoneyCtrl))

    self.m_AddSubAndInputButton = self.transform:Find("QnTabView/JingCai/Normal/Bottom/QnIncreaseAndDecreaseButton_3"):GetComponent(typeof(QnAddSubAndInputButton))
    self.m_AddSubAndInputButton.onValueChanged = DelegateFactory.Action_uint(function (value)
        self.m_Cost = self.m_XiaZhuUnit*value
        self.m_CostAndOwnMoney:SetCost(self.m_XiaZhuUnit*value)
    end)

    self.m_FinishLabel = self.transform:Find("QnTabView/JingCai/Normal/FinishLabel").gameObject
    self.m_ItemTemplate = self.transform:Find("QnTabView/JingCai/Normal/ItemTemplate").gameObject

    self.m_OperateBtn = self.transform:Find("QnTabView/JingCai/Normal/Bottom/OperateBtn").gameObject
    self.m_TipButton = self.transform:Find("QnTabView/JingCai/Normal/TipButton").gameObject

    UIEventListener.Get(self.m_TipButton).onClick = DelegateFactory.VoidDelegate(function (p)
        g_MessageMgr:ShowMessage("StarBiwuFinalJingcaiRule")
    end)
end

function CLuaStarBiwuFinalJingCaiWnd:Init()
    self:StarBiwuSendCurrentJingCaiItem()
end

-- 竞猜查询当前竞猜的返回
function CLuaStarBiwuFinalJingCaiWnd:StarBiwuSendCurrentJingCaiItem()
    self.NormalRoot:SetActive(true)
    self.NoRpcRoot:SetActive(false)

    self.m_jingcaiItemTblUd = CLuaStarBiwuMgr.FinalJingCaiData[1]
    self.m_canBet = CLuaStarBiwuMgr.FinalJingCaiData[2]
    self.m_targetDate = CLuaStarBiwuMgr.FinalJingCaiData[3]
    self.m_targetStageName = CLuaStarBiwuMgr.FinalJingCaiData[4]
    self.m_maxBetValue = CLuaStarBiwuMgr.FinalJingCaiData[5]
    self.m_currentBetPool = CLuaStarBiwuMgr.FinalJingCaiData[6]
    self.m_jingcaiPhaseNum = CLuaStarBiwuMgr.FinalJingCaiData[7]
    self.m_phaseBet = CLuaStarBiwuMgr.FinalJingCaiData[8]
    self.m_canGetReward = CLuaStarBiwuMgr.FinalJingCaiData[9]
    self.m_MatchInfoUd = CLuaStarBiwuMgr.FinalJingCaiData[10]

    self.m_XiaZhuUnit = StarBiWuShow_Setting.GetData().XiaZhuUnit

    if  self.m_maxBetValue - self.m_phaseBet == 0 then
        self.m_canBetValue = self.m_minXiazhu
        self.m_OperateBtn.transform:GetComponent(typeof(QnButton)).Enabled = false
    else
        self.m_OperateBtn.transform:GetComponent(typeof(QnButton)).Enabled = true
        self.m_canBetValue = (self.m_maxBetValue - self.m_phaseBet)/self.m_XiaZhuUnit
    end

    self:UpdatePanel()
end

function CLuaStarBiwuFinalJingCaiWnd:UpdatePanel()
    self.m_currentBetPoolLabel.text = LocalString.GetString(self.m_currentBetPool)
    self.m_targetDateLabel.text = LocalString.GetString(self.m_targetDate)
    --self.m_targetStageNameLabel.text = LocalString.GetString(self.m_targetStageName)
    self.m_maxBetValueLabel.text = LocalString.GetString(self.m_maxBetValue)

    local matchInfoList = MsgPackImpl.unpack(self.m_MatchInfoUd)
    if matchInfoList[0] == "" or matchInfoList[1] == "" then
        self.m_VSLabel.text = LocalString.GetString("无")
    else
        self.m_VSLabel.text = SafeStringFormat3(LocalString.GetString("%s[ffed5f]VS[-]%s"), matchInfoList[0], matchInfoList[1])
    end

    self.m_BottomRoot:SetActive(self.m_canBet)
    self.m_FinishLabel:SetActive(not self.m_canBet)

    UIEventListener.Get(self.m_OperateBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        for i = 0,#self.m_resultTable do
            if self.m_resultTable[i] == -1 then
                g_MessageMgr:ShowMessage("Star_Biwu_Jingcai_Not_Open")
                return
            end
        end

        local msg = g_MessageMgr:FormatMessage("Star_Biwu_Jingcai_Confirm",self.m_Cost)
        MessageWndManager.ShowConfirmMessage(msg,15,false,DelegateFactory.Action(function ()

            local cslist = Table2List(self.m_resultTable, MakeGenericClass(List, Object))
            local data = MsgPackImpl.pack(cslist)
            Gac2Gas.StarBiwuRequestJingCaiBet(self.m_jingcaiPhaseNum,self.m_Cost,data)-- 请求竞猜 jingcaiPhaseNum, jingcaiBetCount, jingcaiChoiceTblUd
        end), nil)
    end)
    self.m_Cost = self.m_minXiazhu * self.m_XiaZhuUnit

    self.m_AddSubAndInputButton:SetValue(self.m_minXiazhu, true)
    self.m_AddSubAndInputButton:SetMinMax(self.m_minXiazhu, self.m_canBetValue, 1)

    self:UpdateJingCaiItem()
end

function CLuaStarBiwuFinalJingCaiWnd:UpdateJingCaiItem()
    self.m_ItemTemplate:SetActive(false)
    self.m_resultTable = {}
    Extensions.RemoveAllChildren(self.m_ContentRoot.transform)
    local item = self.m_ItemTemplate
	local gridList = MsgPackImpl.unpack(self.m_jingcaiItemTblUd)
	if gridList then
        for i = 0, gridList.Count - 1, 1 do
            self.m_resultTable[i+1] = -1
            local inst = LuaStarBiwuJingCaiItem:new()
            inst:Init(self.m_ContentRoot.gameObject,item,gridList,i)
            inst.m_Callback = function (i,c)
                self.m_resultTable[i+1] = c
            end
		end
    end
    self.m_ContentRoot:Reposition()
end

function CLuaStarBiwuFinalJingCaiWnd:StarBiwuJingCaiBetResult(phaseBet)

    self.m_phaseBet = phaseBet
    if  self.m_maxBetValue - self.m_phaseBet == 0 then
        self.m_canBetValue = self.m_minXiazhu
        self.m_OperateBtn.transform:GetComponent(typeof(QnButton)).Enabled = false
    else
        self.m_OperateBtn.transform:GetComponent(typeof(QnButton)).Enabled = true
        self.m_canBetValue = (self.m_maxBetValue - self.m_phaseBet)/self.m_XiaZhuUnit
    end
    self.m_AddSubAndInputButton:SetMinMax(self.m_minXiazhu, self.m_canBetValue, 1)
    self:UpdateJingCaiItem()
end
