LuaXYLPTipWnd = class()

RegistChildComponent(LuaXYLPTipWnd, "m_NameLabel","NameLabel", UILabel)
RegistChildComponent(LuaXYLPTipWnd, "m_TipLabel","TipLabel", UILabel)

function LuaXYLPTipWnd:Init()
    local data = ZhongYuanJie_XYLPClues.GetData(ZhongYuanJie2020Mgr.m_TipWnd_taskID)
    self.m_TipLabel.text = data.PromptMessage
    self.m_NameLabel.text = data.Name
end