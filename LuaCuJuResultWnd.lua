local DelegateFactory = import "DelegateFactory"
local ShareBoxMgr     = import "L10.UI.ShareBoxMgr"
local EShareType      = import "L10.UI.EShareType"
local CLoginMgr       = import "L10.Game.CLoginMgr"
local Profession      = import "L10.Game.Profession"
local Animator        = import "UnityEngine.Animator"

LuaCuJuResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaCuJuResultWnd, "result")
RegistClassMember(LuaCuJuResultWnd, "score")
RegistClassMember(LuaCuJuResultWnd, "sideInfo")
RegistClassMember(LuaCuJuResultWnd, "rankButton")
RegistClassMember(LuaCuJuResultWnd, "shareButton")
RegistClassMember(LuaCuJuResultWnd, "closeButton")

function LuaCuJuResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    self:InitUIComponents()

    UIEventListener.Get(self.rankButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRankButtonClick()
	end)

    UIEventListener.Get(self.shareButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareButtonClick()
	end)
end

function LuaCuJuResultWnd:InitUIComponents()
    self.score = self.transform:Find("xinxi/Score"):GetComponent(typeof(UILabel))
    self.rankButton = self.transform:Find("xinxi/RankButton").gameObject
    self.shareButton = self.transform:Find("xinxi/ShareButton").gameObject
    self.closeButton = self.transform:Find("xinxi/CloseButton").gameObject

    local blue = self.transform:Find("xinxi/Blue")
    local red = self.transform:Find("xinxi/Red")
    local tbl = {blue, red}
    self.sideInfo = {}
    for i = 1, 2 do
        local parent = tbl[i]
        self.sideInfo[i] = {}
        self.sideInfo[i].myTeam = parent:Find("MyTeam").gameObject
        self.sideInfo[i].goal = parent:Find("Goal"):GetComponent(typeof(UILabel))
        self.sideInfo[i].template = parent:Find("Template").gameObject
        self.sideInfo[i].table = parent:Find("Table"):GetComponent(typeof(UITable))

        self.sideInfo[i].myTeam:SetActive(false)
        self.sideInfo[i].template:SetActive(false)
        self.sideInfo[i].goal.text = ""
    end
end

function LuaCuJuResultWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdateTempPlayTimesWithKey", self, "OnUpdateTempPlayTimesWithKey")
end

function LuaCuJuResultWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateTempPlayTimesWithKey", self, "OnUpdateTempPlayTimesWithKey")
end

function LuaCuJuResultWnd:OnUpdateTempPlayTimesWithKey(args)
    local key = args[0]
    if key == EnumPlayTimesKey_lua.eWorldCup2022JZLY_DailyScore then
        self:UpdateScore()
    end
end

function LuaCuJuResultWnd:UpdatePlayerPlayInfo()
    for _, data in pairs(LuaCuJuMgr.ResultInfo.scoreInfo) do
        local force, score = data[1], data[2]

        if force == EnumCommonForce_lua.eDefend then
            self.sideInfo[1].goal.text = score
        elseif force == EnumCommonForce_lua.eAttack then
            self.sideInfo[2].goal.text = score
        end
    end

    self:UpdateTable()
    self:UpdateResult()
end

function LuaCuJuResultWnd:UpdateTable()
    for i = 1, 2 do
        local table = self.sideInfo[i].table
        local template = self.sideInfo[i].template

        Extensions.RemoveAllChildren(table.transform)
        for j = 1, #LuaCuJuMgr.ResultInfo.playerInfo[i] do
            local info = LuaCuJuMgr.ResultInfo.playerInfo[i][j]
            local child = NGUITools.AddChild(table.gameObject, template)
            child:SetActive(true)

            child.transform:Find("Job"):GetComponent(typeof(UISprite)).spriteName = Profession.GetIconByNumber(info.class)

            local name = child.transform:Find("Name"):GetComponent(typeof(UILabel))
            local goal = child.transform:Find("Goal"):GetComponent(typeof(UILabel))
            local steal = child.transform:Find("Steal"):GetComponent(typeof(UILabel))
            name.text = info.name
            goal.text = info.goal
            steal.text = info.steal

            if info.playerId == CClientMainPlayer.Inst.Id then
                name.color = NGUIText.ParseColor24("00FF00", 0)
                goal.color = NGUIText.ParseColor24("00FF00", 0)
                steal.color = NGUIText.ParseColor24("00FF00", 0)
                self.sideInfo[i].myTeam:SetActive(true)
            end
        end
        table:Reposition()
    end
end

function LuaCuJuResultWnd:UpdateResult()
    local animator = self.transform:GetComponent(typeof(Animator))
    if LuaCuJuMgr.ResultInfo.winForce == EnumCommonForce_lua.eNeutral then
        animator:Play("common_pingju")
    elseif LuaCuJuMgr.ResultInfo.winForce == LuaCuJuMgr.ResultInfo.myForce then
        animator:Play("common_shengli")
    else
        animator:Play("common_shibai")
    end
end


function LuaCuJuResultWnd:Init()
    self:InitPlayerInfo()
    self:UpdateScore()
    self:UpdatePlayerPlayInfo()
end

function LuaCuJuResultWnd:UpdateScore()
    local todayScore = CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eWorldCup2022JZLY_DailyScore)
    local maxTodayScore = CuJu_Setting.GetData().MaxTodayScore
    self.score.text = SafeStringFormat3(LocalString.GetString("今日累计积分 %d/%d"), todayScore, maxTodayScore)
end

function LuaCuJuResultWnd:InitPlayerInfo()
    local playerInfo = self.transform:Find("xinxi/PlayerInfoNode")
    playerInfo:Find("Icon"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(CClientMainPlayer.Inst.Class, CClientMainPlayer.Inst.Gender, -1), false)
    playerInfo:Find("Icon/Level"):GetComponent(typeof(UILabel)).text = tostring(CClientMainPlayer.Inst.Level)
    playerInfo:Find("ID"):GetComponent(typeof(UILabel)).text = tostring(CClientMainPlayer.Inst.Id)
    playerInfo:Find("Name"):GetComponent(typeof(UILabel)).text = CClientMainPlayer.Inst.BasicProp.Name

    local myGameServer = CLoginMgr.Inst:GetSelectedGameServer()
    playerInfo:Find("Server"):GetComponent(typeof(UILabel)).text = myGameServer.name
end

-- 分享时不显示几个按键
function LuaCuJuResultWnd:SetButtonActive(active)
    self.shareButton:SetActive(active)
    self.rankButton:SetActive(active)
    self.closeButton:SetActive(active)
end

--@region UIEvent

function LuaCuJuResultWnd:OnRankButtonClick()
    CUIManager.ShowUI(CLuaUIResources.CuJuRankWnd)
end

function LuaCuJuResultWnd:OnShareButtonClick()
    self:SetButtonActive(false)

    CUICommonDef.CaptureScreen("screenshot", false, false, nil,
        DelegateFactory.Action_string_bytes(function(fullPath, jpgBytes)
            self:SetButtonActive(true)
            ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
        end), false)
end

--@endregion UIEvent
