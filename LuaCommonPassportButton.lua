
LuaCommonPassportButton = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaCommonPassportButton, "alert")
RegistClassMember(LuaCommonPassportButton, "levelLabel")

RegistClassMember(LuaCommonPassportButton, "passId")

function LuaCommonPassportButton:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaCommonPassportButton:OnEnable()
    g_ScriptEvent:AddListener("UpdatePassDataWithId", self, "OnUpdatePassDataWithId")
end

function LuaCommonPassportButton:OnDisable()
    g_ScriptEvent:RemoveListener("UpdatePassDataWithId", self, "OnUpdatePassDataWithId")
end

function LuaCommonPassportButton:OnUpdatePassDataWithId(id)
    if self.passId == id then
        self:UpdateAlert()
        self:UpdateLevel()
    end
end

function LuaCommonPassportButton:Init(args)
    self.passId = args[0]
    self.transform:GetComponent(typeof(CUITexture)):LoadMaterial(Pass_ClientSetting.GetData(self.passId).Icon)

    self.alert = self.transform:Find("Alert").gameObject
    self.levelLabel = self.transform:Find("Level"):GetComponent(typeof(UILabel))
    self:UpdateAlert()
    self:UpdateLevel()

    UIEventListener.Get(self.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnButtonClick()
    end)
end

function LuaCommonPassportButton:UpdateAlert()
    self.alert:SetActive(LuaCommonPassportMgr:HasAward(self.passId))
end

function LuaCommonPassportButton:UpdateLevel()
    self.levelLabel.text = SafeStringFormat3("lv.%d", LuaCommonPassportMgr:GetLevel(self.passId))
end

--@region UIEvent

function LuaCommonPassportButton:OnButtonClick()
    LuaCommonPassportMgr:UpdateTaskRedDot(self.passId)
    self:UpdateAlert()
    LuaCommonPassportMgr.passId = self.passId
    CUIManager.ShowUI(Pass_ClientSetting.GetData(self.passId).MainWndName)
end

--@endregion UIEvent
