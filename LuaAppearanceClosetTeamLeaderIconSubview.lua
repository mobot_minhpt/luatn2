local CUITexture = import "L10.UI.CUITexture"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local EnumGender = import "L10.Game.EnumGender"
local UISprite = import "UISprite"
local LuaTweenUtils = import "LuaTweenUtils"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"

LuaAppearanceClosetTeamLeaderIconSubview = class()

RegistChildComponent(LuaAppearanceClosetTeamLeaderIconSubview,"m_TeamLeaderIconItem", "CommonClosetSingleLineItemCell", GameObject)
RegistChildComponent(LuaAppearanceClosetTeamLeaderIconSubview,"m_ItemDisplay", "AppearanceCommonButtonDisplayView", CCommonLuaScript)
RegistChildComponent(LuaAppearanceClosetTeamLeaderIconSubview,"m_TeamLeaderDisplaySubview", "AppearanceTeamLeaderDisplaySubview", CCommonLuaScript)
RegistChildComponent(LuaAppearanceClosetTeamLeaderIconSubview,"m_ContentTable", "ContentTable", UITable)
RegistChildComponent(LuaAppearanceClosetTeamLeaderIconSubview,"m_ContentScrollView", "ContentScrollView", UIScrollView)

RegistClassMember(LuaAppearanceClosetTeamLeaderIconSubview, "m_AllIcons")
RegistClassMember(LuaAppearanceClosetTeamLeaderIconSubview, "m_SelectedDataId")

function LuaAppearanceClosetTeamLeaderIconSubview:Awake()
end

function LuaAppearanceClosetTeamLeaderIconSubview:Init()
    self:PlayAppearAnimation()
    self:SetDefaultSelection()
    self:LoadData()
end

function LuaAppearanceClosetTeamLeaderIconSubview:PlayAppearAnimation()
    self.m_TeamLeaderDisplaySubview.gameObject:SetActive(true)
    LuaTweenUtils.TweenAlpha(self.m_TeamLeaderDisplaySubview.transform:GetComponent(typeof(UIWidget)), 0, 1, 0.8)
    self.m_TeamLeaderDisplaySubview:Init()
end

function LuaAppearanceClosetTeamLeaderIconSubview:LoadData()
    self.m_AllIcons = LuaAppearancePreviewMgr:GetAllTeamLeaderIconInfo(true)

    Extensions.RemoveAllChildren(self.m_ContentTable.transform)
    self.m_ContentTable.gameObject:SetActive(true)

    for i = 1, # self.m_AllIcons do
        local child = CUICommonDef.AddChild(self.m_ContentTable.gameObject, self.m_TeamLeaderIconItem)
        child:SetActive(true)
        self:InitItem(child, self.m_AllIcons[i])
    end
    self.m_ContentTable:Reposition()
    self.m_ContentScrollView:ResetPosition()
    self:InitSelection()
end

function LuaAppearanceClosetTeamLeaderIconSubview:SetDefaultSelection()
    self.m_SelectedDataId = LuaAppearancePreviewMgr:GetCurrentInUseTeamLeaderIcon()
end

function LuaAppearanceClosetTeamLeaderIconSubview:InitSelection()
    if self.m_SelectedDataId == 0 then
        self:OnItemClick(nil)
        return
    end

    local childCount = self.m_ContentTable.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentTable.transform:GetChild(i).gameObject
        local appearanceData = self.m_AllIcons[i+1]
        if appearanceData.id == self.m_SelectedDataId then
            self:OnItemClick(childGo)
            break
        end
    end
end

function LuaAppearanceClosetTeamLeaderIconSubview:InitItem(itemGo, appearanceData)
    local iconTexture = itemGo.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local cornerGo = itemGo.transform:Find("Item/Corner").gameObject
    local disabledGo = itemGo.transform:Find("Item/Disabled").gameObject
    local nameLabel = itemGo.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local conditionLabel = itemGo.transform:Find("ConditionLabel"):GetComponent(typeof(UILabel))
    iconTexture:LoadMaterial(appearanceData.icon)
    nameLabel.text = appearanceData.name
    conditionLabel.text = LuaAppearancePreviewMgr:GetTeamLeaderIconConditionText(appearanceData.id)
    disabledGo:SetActive(not LuaAppearancePreviewMgr:MainPlayerHasTeamLeaderIcon(appearanceData.id))
    cornerGo:SetActive(LuaAppearancePreviewMgr:IsCurrentInUseTeamLeaderIcon(appearanceData.id))
    itemGo:GetComponent(typeof(CButton)).Selected = (self.m_SelectedDataId and self.m_SelectedDataId==appearanceData.id or false)

    UIEventListener.Get(itemGo).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnItemClick(go)
    end)
end

function LuaAppearanceClosetTeamLeaderIconSubview:OnItemClick(go)
    local childCount = self.m_ContentTable.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentTable.transform:GetChild(i).gameObject
        if childGo == go then
            childGo.transform:GetComponent(typeof(CButton)).Selected = true
            self.m_SelectedDataId = self.m_AllIcons[i+1].id
        else
            childGo.transform:GetComponent(typeof(CButton)).Selected = false
        end
    end
    self:UpdateItemDisplay()
end

function LuaAppearanceClosetTeamLeaderIconSubview:UpdateItemDisplay()
    local id = self.m_SelectedDataId and self.m_SelectedDataId or 0
    if id==0 then
        self.m_TeamLeaderDisplaySubview:SetLeaderIcon("")
        self.m_TeamLeaderDisplaySubview:SetItemName("")
        self.m_ItemDisplay.gameObject:SetActive(false)
        return
    end
    local appearanceData = nil
    for i=1,#self.m_AllIcons do
        if self.m_AllIcons[i].id == id then
            appearanceData = self.m_AllIcons[i]
            break
        end
    end
    self.m_TeamLeaderDisplaySubview:SetLeaderIcon(appearanceData.sprite)
    self.m_TeamLeaderDisplaySubview:SetItemName(appearanceData.name)
    self.m_ItemDisplay.gameObject:SetActive(true)
    local buttonTbl = {}

    local inUse = LuaAppearancePreviewMgr:IsCurrentInUseTeamLeaderIcon(appearanceData.id)
 
    if inUse then
        table.insert(buttonTbl, {text=LocalString.GetString("脱下"), isYellow=false, action=function(go) self:OnRemoveButtonClick() end})
    else
        table.insert(buttonTbl, {text=LocalString.GetString("更换"), isYellow=false, action=function(go) self:OnApplyButtonClick() end})
    end

    self.m_ItemDisplay:Init(buttonTbl)
end

function LuaAppearanceClosetTeamLeaderIconSubview:OnApplyButtonClick()
    local exist = LuaAppearancePreviewMgr:MainPlayerHasTeamLeaderIcon(self.m_SelectedDataId)
    if exist then
        LuaAppearancePreviewMgr:RequestSetTeamLeaderIcon(self.m_SelectedDataId)
    end
end

function LuaAppearanceClosetTeamLeaderIconSubview:OnRemoveButtonClick()
    LuaAppearancePreviewMgr:RequestSetTeamLeaderIcon(0)
end

function  LuaAppearanceClosetTeamLeaderIconSubview:OnEnable()
    g_ScriptEvent:AddListener("OnUnLockTeamLeaderIconSuccess", self, "OnUnLockTeamLeaderIconSuccess")
    g_ScriptEvent:AddListener("OnUseTeamLeaderIconSuccess", self, "OnUseTeamLeaderIconSuccess")
end

function  LuaAppearanceClosetTeamLeaderIconSubview:OnDisable()
    g_ScriptEvent:RemoveListener("OnUnLockTeamLeaderIconSuccess", self, "OnUnLockTeamLeaderIconSuccess")
    g_ScriptEvent:RemoveListener("OnUseTeamLeaderIconSuccess", self, "OnUseTeamLeaderIconSuccess")
end

function LuaAppearanceClosetTeamLeaderIconSubview:OnUnLockTeamLeaderIconSuccess(id)
    self:LoadData()
end

function LuaAppearanceClosetTeamLeaderIconSubview:OnUseTeamLeaderIconSuccess(id)
    self:SetDefaultSelection()
    self:LoadData()
end