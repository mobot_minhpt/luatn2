local UISlider = import "UISlider"

local CUITexture = import "L10.UI.CUITexture"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Animation = import "UnityEngine.Animation"
local AnchorUpdate = import "UIRect.AnchorUpdate"

LuaNanDuFanHuaBargainWnd = class()
LuaNanDuFanHuaBargainWnd.bargainId = nil
--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaNanDuFanHuaBargainWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaNanDuFanHuaBargainWnd, "LeftPlate", "LeftPlate", GameObject)
RegistChildComponent(LuaNanDuFanHuaBargainWnd, "RightPlate", "RightPlate", GameObject)
RegistChildComponent(LuaNanDuFanHuaBargainWnd, "BuyBtn", "BuyBtn", GameObject)
RegistChildComponent(LuaNanDuFanHuaBargainWnd, "MinOffer", "MinOffer", UILabel)
RegistChildComponent(LuaNanDuFanHuaBargainWnd, "MaxOffer", "MaxOffer", UILabel)
RegistChildComponent(LuaNanDuFanHuaBargainWnd, "OfferLabel", "OfferLabel", UILabel)
RegistChildComponent(LuaNanDuFanHuaBargainWnd, "Slider2", "Slider2", UISlider)
RegistChildComponent(LuaNanDuFanHuaBargainWnd, "FailObj", "FailObj", GameObject)
RegistChildComponent(LuaNanDuFanHuaBargainWnd, "SuccObj", "SuccObj", GameObject)
RegistChildComponent(LuaNanDuFanHuaBargainWnd, "MoneyTex", "MoneyTex", CUITexture)
RegistChildComponent(LuaNanDuFanHuaBargainWnd, "LeftCharacter", "LeftCharacter", GameObject)
RegistChildComponent(LuaNanDuFanHuaBargainWnd, "RightCharacter", "RightCharacter", GameObject)
RegistChildComponent(LuaNanDuFanHuaBargainWnd, "OwnLabel", "OwnLabel", UILabel)
RegistChildComponent(LuaNanDuFanHuaBargainWnd, "Des", "Des", GameObject)

--@endregion RegistChildComponent end

function LuaNanDuFanHuaBargainWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    if LuaNanDuFanHuaBargainWnd.bargainId then
        self.OwnLabel.transform:Find("AddTongBaoButton"):GetComponent(typeof(UIWidget)).updateAnchors = AnchorUpdate.OnUpdate
        self.RightCharacter.transform:Find("Texture/Sprite"):GetComponent(typeof(UIWidget)).updateAnchors = AnchorUpdate.OnUpdate
        self.RightCharacter.transform:Find("Texture/Label (1)"):GetComponent(typeof(UIWidget)).updateAnchors = AnchorUpdate.OnUpdate

        self:InitWndData()
        self:RefreshConstUI()
        self:InitUIEvent()
    end
end

function LuaNanDuFanHuaBargainWnd:Init()

end

--@region UIEvent

--@endregion UIEvent

function LuaNanDuFanHuaBargainWnd:InitWndData()
    self.cfgData = NanDuFanHua_Bargain.GetData(LuaNanDuFanHuaBargainWnd.bargainId)

    local lordData = {}
    if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey) then
        local playData = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey)
        if playData.ExpiredTime > CServerTimeMgr.Inst.timeStamp and playData.Data and playData.Data.Data then
            local data = g_MessagePack.unpack(playData.Data.Data)
            lordData = data[1]
        end
    end
    self.moneyCnt = lordData[LuaYiRenSiMgr.LordPropertyKey.moneyIndex] and lordData[LuaYiRenSiMgr.LordPropertyKey.moneyIndex] or 0
end

function LuaNanDuFanHuaBargainWnd:RefreshConstUI()
    local isPlayerBuyer = self.cfgData.IsBuy == 1
    self.LeftPlate.transform:Find("leftdongxi/Seller").gameObject:SetActive(isPlayerBuyer)
    self.LeftPlate.transform:Find("leftdongxi/Buyer").gameObject:SetActive(not isPlayerBuyer)
    self.RightPlate.transform:Find("rightdongxi/Seller").gameObject:SetActive(not isPlayerBuyer)
    self.RightPlate.transform:Find("rightdongxi/Buyer").gameObject:SetActive(isPlayerBuyer)
    self.MinOffer.text = self.cfgData.Lowest
    self.MaxOffer.text = self.cfgData.Highest
    
    self.MoneyTex:LoadMaterial(self.cfgData.Icon)
    self.OfferLabel.text = self.cfgData.Lowest
    self.Slider2.value = 0
    self.Des.transform.localPosition = Vector3(self.Slider2.transform:Find("Thumb").localPosition.x, 38, 0)

    local npc = NPC_NPC.GetData(self.cfgData.NpcId)
    self.LeftCharacter.transform:Find("ItemCell/bg/paizi/Tex"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(npc.Portrait)
    self.transform:Find("vfx_nandufanhuabargainwnd_biaoqian_green/Name"):GetComponent(typeof(UILabel)).text = isPlayerBuyer and LocalString.GetString("买方") or LocalString.GetString("卖方")

    self.RightCharacter.transform:Find("ItemCell/bg/youpaizi/Tex"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetDieKeSpecialPortraitName(CClientMainPlayer.Inst))
    self.transform:Find("vfx_nandufanhuabargainwnd_biaoqian_red/Name"):GetComponent(typeof(UILabel)).text = isPlayerBuyer and LocalString.GetString("卖方") or LocalString.GetString("买方")
    
    self.OwnLabel.text = self.moneyCnt
    
    local bubbleDialogs = self.cfgData.Bubble
    local bubbleList = g_LuaUtil:StrSplit(bubbleDialogs,";")
    self.RightCharacter.transform:Find("Bubble/BubbleBg/Label"):GetComponent(typeof(UILabel)).text = bubbleList[math.random(1, #bubbleList)]
    --self.RightCharacter.transform:Find("BubbleBg").gameObject:SetActive(true)
    --self.m_HideBubbleTick = RegisterTickOnce(function ()
    --    if not CommonDefs.IsNull(self.gameObject) then
    --        self.RightCharacter.transform:Find("BubbleBg").gameObject:SetActive(false)
    --    end
    --end, 5 * 1000)
    
    self.FailObj:SetActive(false)
    self.SuccObj:SetActive(false)
end 

function LuaNanDuFanHuaBargainWnd:OnDisable()
    --UnRegisterTick(self.m_HideBubbleTick)
    UnRegisterTick(self.m_RecoverTick)
end

function LuaNanDuFanHuaBargainWnd:InitUIEvent()
    local ThumbTrans = self.Slider2.transform:Find("Thumb")
    self.Slider2.OnChangeValue = DelegateFactory.Action_float(function(value)
        self.OfferLabel.text = self:CalculateSliderValue(value)
        self.Des.transform.localPosition = Vector3(ThumbTrans.localPosition.x, 38, 0)
    end)

    UIEventListener.Get(self.BuyBtn).onClick = DelegateFactory.VoidDelegate(function (_)
        self:OnClickBuyBtn()
    end)

    UIEventListener.Get(self.OwnLabel.transform:Find("AddTongBaoButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        CUIManager.ShowUI(CLuaUIResources.NanDuFanHuaExchangeMoneyWnd)
    end)
end 

function LuaNanDuFanHuaBargainWnd:CalculateSliderValue(value)
    return math.floor(value * (self.cfgData.Highest-self.cfgData.Lowest) + self.cfgData.Lowest)
end

function LuaNanDuFanHuaBargainWnd:ChangeSliderAndButtonState(isOn)
    --这个给动效控制了
    --self.Slider2.gameObject:SetActive(isOn)
    --self.MinOffer.gameObject:SetActive(isOn)
    --self.MaxOffer.gameObject:SetActive(isOn)
    --self.BuyBtn:SetActive(isOn)
end

function LuaNanDuFanHuaBargainWnd:OnClickBuyBtn()
    local isPlayerBuyer = self.cfgData.IsBuy == 1

    local sliderValue = self.Slider2.value
    local offerMoney = self:CalculateSliderValue(sliderValue)
    if offerMoney > self.moneyCnt and isPlayerBuyer then
        --钱不够
        g_MessageMgr:ShowMessage("NANDU_TASK_BARGAIN_MONEY_NOT_ENOUGH")
        return
    end
    
    local isSuccessful = false
    if isPlayerBuyer then
        isSuccessful = offerMoney >= self.cfgData.LowThresh
    else
        isSuccessful = self.cfgData.HighThresh == 0 and true or offerMoney < self.cfgData.HighThresh
    end
    
    -- 0 balance, -1 zuodao, 1youdao
    local playAnimationType = 0
    local aniComp = self.transform:GetComponent(typeof(Animation))
    if isPlayerBuyer then
        --playZuoDaoAnimation = isSuccessful
        local lowThresh = self.cfgData.LowThresh
        local highThresh = self.cfgData.HighThresh
        if lowThresh == highThresh and lowThresh == 0 then
            playAnimationType = 0
        else
            if offerMoney > highThresh then
                playAnimationType = -1
            elseif offerMoney < lowThresh then
                playAnimationType = 1
            else
                playAnimationType = 0
            end 
        end
    else
        local lowThresh = self.cfgData.LowThresh
        local highThresh = self.cfgData.HighThresh
        if offerMoney < lowThresh then
            playAnimationType = -1
        else    
            playAnimationType = 0
        end
    end
    self:ChangeSliderAndButtonState(false)

    if isSuccessful then
        RegisterTickOnce(function ()
            if not CommonDefs.IsNull(self.gameObject) then
                self.SuccObj:SetActive(true)
            end
        end, 1.2 * 1000)
        RegisterTickOnce(function ()
            if not CommonDefs.IsNull(self.gameObject) then
                Gac2Gas.NanDuTaskBargains(LuaNanDuFanHuaBargainWnd.bargainId, offerMoney)
            end
        end, 3 * 1000)
    else
        RegisterTickOnce(function ()
            if not CommonDefs.IsNull(self.gameObject) then
                self.FailObj:SetActive(true)
            end
        end, 1.2 * 1000)
        self.m_RecoverTick = RegisterTickOnce(function ()
            if not CommonDefs.IsNull(self.gameObject) then
                self:ChangeSliderAndButtonState(true)
                self.FailObj:SetActive(false)


                if playAnimationType == -1 then
                    aniComp:Play("nandufanhuabargainwnd_zuodaohui")
                elseif playAnimationType == 0 then
                    aniComp:Play("nandufanhuabargainwnd_pinghenghui")
                elseif playAnimationType == 1 then
                    aniComp:Play("nandufanhuabargainwnd_youdaohui")
                end
            end
        end, 3 * 1000) 
    end
    
    --播动画
    if playAnimationType == -1 then
        aniComp:Play("nandufanhuabargainwnd_zuodao")
    elseif playAnimationType == 0 then
        aniComp:Play("nandufanhuabargainwnd_pingheng")
    elseif playAnimationType == 1 then
        aniComp:Play("nandufanhuabargainwnd_youdao")
    end
end 

function LuaNanDuFanHuaBargainWnd:OnEnable()
    g_ScriptEvent:AddListener("NanDuTaskBargainsSuccess", self, "OnBargainSuccess")
    g_ScriptEvent:AddListener("UpdateTempPlayDataWithKey", self, "OnUpdatePlayProp")
end 

function LuaNanDuFanHuaBargainWnd:OnDisable()
    g_ScriptEvent:RemoveListener("NanDuTaskBargainsSuccess", self, "OnBargainSuccess")
    g_ScriptEvent:RemoveListener("UpdateTempPlayDataWithKey", self, "OnUpdatePlayProp")
end 

function LuaNanDuFanHuaBargainWnd:OnBargainSuccess()
    CUIManager.CloseUI(CLuaUIResources.NanDuFanHuaBargainWnd)
end 

function LuaNanDuFanHuaBargainWnd:OnUpdatePlayProp()
    local lordData = {}
    if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey) then
        local playData = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, LuaYiRenSiMgr.LordTempPlayDataKey)
        if playData.ExpiredTime > CServerTimeMgr.Inst.timeStamp and playData.Data and playData.Data.Data then
            local data = g_MessagePack.unpack(playData.Data.Data)
            lordData = data[1]
        end
    end
    self.moneyCnt = lordData[LuaYiRenSiMgr.LordPropertyKey.moneyIndex] and lordData[LuaYiRenSiMgr.LordPropertyKey.moneyIndex] or 0
    self.OwnLabel.text = self.moneyCnt
end