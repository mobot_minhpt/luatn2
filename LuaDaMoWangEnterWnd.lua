local UILabel = import "UILabel"
local UITable = import "UITable"
local GameObject = import "UnityEngine.GameObject"
local CButton = import "L10.UI.CButton"
local QnButton = import "L10.UI.QnButton"
local CTooltip = import "L10.UI.CTooltip"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CUITexture = import "L10.UI.CUITexture"
local CUICommonDef = import "L10.UI.CUICommonDef"
local UISprite = import "UISprite"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"

LuaDaMoWangEnterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaDaMoWangEnterWnd, "ActivityTime", "ActivityTime", UILabel)
RegistChildComponent(LuaDaMoWangEnterWnd, "AwardTable", "AwardTable", UITable)
RegistChildComponent(LuaDaMoWangEnterWnd, "ItemTemplate", "ItemTemplate", GameObject)
RegistChildComponent(LuaDaMoWangEnterWnd, "RuleTipButton", "RuleTipButton", QnButton)
RegistChildComponent(LuaDaMoWangEnterWnd, "BossIcon", "BossIcon", CUITexture)
RegistChildComponent(LuaDaMoWangEnterWnd, "BossName", "BossName", UILabel)
RegistChildComponent(LuaDaMoWangEnterWnd, "SkillTable", "SkillTable", UITable)
RegistChildComponent(LuaDaMoWangEnterWnd, "TemplateSkillItem", "TemplateSkillItem", GameObject)
RegistChildComponent(LuaDaMoWangEnterWnd, "BossButton", "BossButton", CButton)
RegistChildComponent(LuaDaMoWangEnterWnd, "ChallengerButton", "ChallengerButton", CButton)
RegistChildComponent(LuaDaMoWangEnterWnd, "QueueLabel", "QueueLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaMoWangEnterWnd, "m_BossSkillGoTable")
RegistClassMember(LuaDaMoWangEnterWnd, "m_BossBtnLable")
RegistClassMember(LuaDaMoWangEnterWnd, "m_BossBtnSprite")
RegistClassMember(LuaDaMoWangEnterWnd, "m_BossBtnRecommendLabel")
RegistClassMember(LuaDaMoWangEnterWnd, "m_ChallengerBtnLable")
RegistClassMember(LuaDaMoWangEnterWnd, "m_ChallengerBtnSprite")
RegistClassMember(LuaDaMoWangEnterWnd, "m_ChallengerBtnRecommendLabel")
RegistClassMember(LuaDaMoWangEnterWnd, "m_CountDownTick")
RegistClassMember(LuaDaMoWangEnterWnd, "m_QueueFormula")

function LuaDaMoWangEnterWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.RuleTipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleTipButtonClick()
	end)
	
	UIEventListener.Get(self.BossButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBossButtonClick()
	end)


	
	UIEventListener.Get(self.ChallengerButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChallengerButtonClick()
	end)


    --@endregion EventBind end
end

function LuaDaMoWangEnterWnd:Init()
	LuaShuJia2021Mgr.isSignUp = EnumTravestyRole.eNone

	Gac2Gas.TravestyRole_CheckSignUp()
	LuaShuJia2021Mgr.InitInfo()

	self.m_BossBtnLable = self.BossButton.transform:Find("Label"):GetComponent(typeof(UILabel))
	self.m_BossBtnRecommendLabel = self.BossButton.transform:Find("RecommendLabel").gameObject
	self.m_BossBtnSprite = self.BossButton.gameObject:GetComponent(typeof(UISprite))

	self.m_ChallengerBtnLable = self.ChallengerButton.transform:Find("Label"):GetComponent(typeof(UILabel))
	self.m_ChallengerBtnRecommendLabel = self.ChallengerButton.transform:Find("RecommendLabel").gameObject
	self.m_ChallengerBtnSprite = self.ChallengerButton.gameObject:GetComponent(typeof(UISprite))
	-- 活动时间读表
	self.ActivityTime.text = ShuJia_TravestyRole.GetData().Time

	self.m_QueueFormula = AllFormulas.Action_Formula[ShuJia_TravestyRole.GetData().QueueingFormula]
	self.BossIcon.gameObject:SetActive(false)
	self.BossName.text = LuaShuJia2021Mgr.bossName
	self.QueueLabel.gameObject:SetActive(false)
	
	self.BossButton.gameObject:SetActive(false)
	self.ChallengerButton.gameObject:SetActive(false)
	self:InitBossSkillInfo()
	self:InitAwardInfo()
	if self.m_CountDownTick then 
		UnRegisterTick(self.m_CountDownTick)
		self.m_CountDownTick = nil
	end
	self.m_CountDownTick = RegisterTick(function() Gac2Gas.TravestyRole_CheckSignUp() end, 30000)
end
-- 初始化今日奖励
function LuaDaMoWangEnterWnd:InitAwardInfo()
	for i=1,3 do
		local awardItem = self.AwardTable.gameObject.transform:Find("ItemTemplate"..i.."/AwardItemCell"):GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
		awardItem:Init(LuaShuJia2021Mgr.awardIdTable[i])
		local isComplete = LuaShuJia2021Mgr.completeTable[i]
		awardItem:SetAwardComplete(isComplete) 	-- 完成情况
	end
end

function LuaDaMoWangEnterWnd:UpdateAwardInfo()
	for i=1,3 do
		local awardItem = self.AwardTable.gameObject.transform:Find("ItemTemplate"..i.."/AwardItemCell"):GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
		local isComplete = LuaShuJia2021Mgr.completeTable[i]
		awardItem:SetAwardComplete(isComplete) 	-- 完成情况
	end
end

function LuaDaMoWangEnterWnd:InitBossSkillInfo()
	Extensions.RemoveAllChildren(self.SkillTable.transform)
	self.m_BossSkillGoTable = {}
	for i=1,5 do
		local templateGo = CUICommonDef.AddChild(self.SkillTable.gameObject, self.TemplateSkillItem)
		table.insert(self.m_BossSkillGoTable,templateGo)
		templateGo.transform:Find("SkillName"):GetComponent(typeof(UILabel)).text = ""
		templateGo:SetActive(true)
	end
	if LuaShuJia2021Mgr.bossId ~= 0 then 
		self:UpdateBossSkillInfo()
	end
end

function LuaDaMoWangEnterWnd:UpdateBossSkillInfo()
	-- boss技能
	local skillIds = nil
	if not self.m_BossSkillGoTable then return end
	if LuaShuJia2021Mgr.bossSkillDict and CommonDefs.DictContains_LuaCall(LuaShuJia2021Mgr.bossSkillDict,tostring(LuaShuJia2021Mgr.bossId)) then
		skillIds = CommonDefs.DictGetValue_LuaCall(LuaShuJia2021Mgr.bossSkillDict,tostring(LuaShuJia2021Mgr.bossId))
	end
	if not skillIds then return end
	--local skillIds = LuaShuJia2021Mgr.bossSkillId --读表得SkillId
	for i=1, #self.m_BossSkillGoTable do
		local skill = Skill_AllSkills.GetData(Skill_AllSkills.GetSkillId(skillIds[i], 1))
		if skill ~= nil then
			local templateGo = self.m_BossSkillGoTable[i]
			templateGo:SetActive(true)
			templateGo.transform:Find("Mask/SkillIcon"):GetComponent(typeof(CUITexture)):LoadSkillIcon(skill.SkillIcon)
			templateGo.transform:Find("SkillName"):GetComponent(typeof(UILabel)).text = skill.Name

			UIEventListener.Get(templateGo.transform:Find("Mask/SkillIcon").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
				--if LuaShuJia2021Mgr.bossSkillIntroduce and CommonDefs.DictContains_LuaCall(LuaShuJia2021Mgr.bossSkillIntroduce,skillIds[i]) then
					--CSkillInfoMgr.ShowSkillInfoWnd(skillIds[i], true, 0, 0, nil)
					local SkillDescribe = ShuJia_TravestyRoleSkillDescribe.GetData(skillIds[i]).Describe
					if not SkillDescribe then return end
					CTooltip.Show(SkillDescribe,go.transform,CTooltip.AlignType.Top)
				--end
			end)
		else 
			self.m_BossSkillGoTable[i]:SetActive(false)
		end		
	end
end
--@region UIEvent
function LuaDaMoWangEnterWnd:UpdateInfo()

	self:UpdateButtonsStatus()
	self:UpdateQueueInfo()
	self:UpdateAwardInfo()
	self:UpdateBossSkillInfo()
	-- boss头像
	self.BossIcon:LoadNPCPortrait(LuaShuJia2021Mgr.bossIcon)
	self.BossIcon.gameObject:SetActive(true)
	-- boss名称
	self.BossName.text = LuaShuJia2021Mgr.bossName
	self.BossButton.gameObject:SetActive(true)
	self.ChallengerButton.gameObject:SetActive(true)
end


function LuaDaMoWangEnterWnd:UpdateButtonsStatus()
	local notSignUp = LuaShuJia2021Mgr.isSignUp == EnumTravestyRole.eNotSignUp
	self.m_BossBtnRecommendLabel:SetActive(false)
	self.m_ChallengerBtnRecommendLabel:SetActive(false)
	self.BossButton.Enabled = true
	self.ChallengerButton.Enabled = true
	if notSignUp then
		self.m_BossBtnLable.text = LocalString.GetString("扮演魔王")
		self.m_ChallengerBtnLable.text = LocalString.GetString("挑战魔王")
		self.m_BossBtnSprite.spriteName = "common_btn_01_yellow"
		self.m_ChallengerBtnSprite.spriteName = "common_btn_01_yellow"
		self.m_BossBtnRecommendLabel:SetActive(LuaShuJia2021Mgr.isBossEasyQueue)
		self.m_ChallengerBtnRecommendLabel:SetActive(LuaShuJia2021Mgr.isChallengerEasyQueue)

	elseif LuaShuJia2021Mgr.isSignUp == EnumTravestyRole.eBoss then 
		self.ChallengerButton.Enabled = false
		self.m_BossBtnSprite.spriteName = "common_btn_01_blue"
		self.m_BossBtnLable.text = LocalString.GetString("取消匹配")

	elseif LuaShuJia2021Mgr.isSignUp == EnumTravestyRole.ePlayer then
		self.BossButton.Enabled = false
		self.m_ChallengerBtnSprite.spriteName = "common_btn_01_blue"
		self.m_ChallengerBtnLable.text = LocalString.GetString("取消匹配")
	end
end

function LuaDaMoWangEnterWnd:UpdateQueueInfo()
	local notSignUp = LuaShuJia2021Mgr.isSignUp == EnumTravestyRole.eNotSignUp
	if notSignUp then self.QueueLabel.gameObject:SetActive(false) return end

	local queueInfo = 0
	if self.m_QueueFormula ~= nil then
		if LuaShuJia2021Mgr.isSignUp == EnumTravestyRole.eBoss then
			queueInfo = self.m_QueueFormula.Formula(nil,nil,{LuaShuJia2021Mgr.queueInfoBoss})
		elseif LuaShuJia2021Mgr.isSignUp == EnumTravestyRole.ePlayer then
			queueInfo = self.m_QueueFormula.Formula(nil,nil,{LuaShuJia2021Mgr.queueInfoPlayer})
		end
	end
	if queueInfo > 0 then
		local queueMeassage = LocalString.GetString(SafeStringFormat3( "FANCHUANBOSS_QUEUE_%d",queueInfo ))
		self.QueueLabel.gameObject:SetActive(true)
		self.QueueLabel.text =  g_MessageMgr:FormatMessage(queueMeassage)
	else
		self.QueueLabel.gameObject:SetActive(false)
	end
end

function LuaDaMoWangEnterWnd:OnRuleTipButtonClick()
	g_MessageMgr:ShowMessage(ShuJia_TravestyRole.GetData().GameplayConfirm)
end

function LuaDaMoWangEnterWnd:OnBossButtonClick()
	if LuaShuJia2021Mgr.isSignUp == EnumTravestyRole.eBoss then
		Gac2Gas.TravestyRole_CancelSignUp()
	else
		LuaShuJia2021Mgr.signUpClick = EnumTravestyRole.eBoss
		Gac2Gas.TravestyRole_SignUpPlay(EnumTravestyRole.eBoss)
	end
	--Gac2Gas.TravestyRole_CheckSignUp()
end

function LuaDaMoWangEnterWnd:OnChallengerButtonClick()
	if LuaShuJia2021Mgr.isSignUp == EnumTravestyRole.ePlayer then
		Gac2Gas.TravestyRole_CancelSignUp()
	else
		-- challenger匹配
		LuaShuJia2021Mgr.signUpClick = EnumTravestyRole.ePlayer
		Gac2Gas.TravestyRole_SignUpPlay(EnumTravestyRole.ePlayer)
	end
	--Gac2Gas.TravestyRole_CheckSignUp()
end


function LuaDaMoWangEnterWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateDaMoWangSignUpInfo",self, "UpdateInfo")
end

function LuaDaMoWangEnterWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateDaMoWangSignUpInfo",self, "UpdateInfo")
	if self.m_CountDownTick then 
		UnRegisterTick(self.m_CountDownTick)
		self.m_CountDownTick = nil
	end
end


--@endregion UIEvent

