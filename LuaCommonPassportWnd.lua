local UITabBar       = import "L10.UI.UITabBar"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local QnTabButton    = import "L10.UI.QnTabButton"
local CShopMallMgr   = import "L10.UI.CShopMallMgr"

LuaCommonPassportWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaCommonPassportWnd, "levelLabel")
RegistClassMember(LuaCommonPassportWnd, "needExpLabel")
RegistClassMember(LuaCommonPassportWnd, "expIcon")
RegistClassMember(LuaCommonPassportWnd, "expSlider")
RegistClassMember(LuaCommonPassportWnd, "additionLabel")
RegistClassMember(LuaCommonPassportWnd, "additionBg")
RegistClassMember(LuaCommonPassportWnd, "additionDetail")
RegistClassMember(LuaCommonPassportWnd, "additionDetail_ClickBg")
RegistClassMember(LuaCommonPassportWnd, "additionDetail_Bg")
RegistClassMember(LuaCommonPassportWnd, "additionDetail_Template")
RegistClassMember(LuaCommonPassportWnd, "additionDetail_Grid")
RegistClassMember(LuaCommonPassportWnd, "timeLabel")
RegistClassMember(LuaCommonPassportWnd, "buyLevelButton")
RegistClassMember(LuaCommonPassportWnd, "tabBar")
RegistClassMember(LuaCommonPassportWnd, "taskView")
RegistClassMember(LuaCommonPassportWnd, "rewardView")
RegistClassMember(LuaCommonPassportWnd, "otherView")
RegistClassMember(LuaCommonPassportWnd, "rewardTabRedDot")
RegistClassMember(LuaCommonPassportWnd, "taskTabRedDot")

RegistClassMember(LuaCommonPassportWnd, "passId")
RegistClassMember(LuaCommonPassportWnd, "additionDetailTbl")

RegistClassMember(LuaCommonPassportWnd, "level")
RegistClassMember(LuaCommonPassportWnd, "maxLevel")
RegistClassMember(LuaCommonPassportWnd, "upgradeProgressInfo")
RegistClassMember(LuaCommonPassportWnd, "isVIP1Open")
RegistClassMember(LuaCommonPassportWnd, "isVIP2Open")

function LuaCommonPassportWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    self:InitComponents()
    self:InitEventListener()
end

function LuaCommonPassportWnd:InitComponents()
    local anchor = self.transform:Find("Anchor")

    self.levelLabel = anchor:Find("ExpLevel/Level"):GetComponent(typeof(UILabel))
    self.needExpLabel = anchor:Find("ExpLevel/NeedExp"):GetComponent(typeof(UILabel))
    self.expIcon = anchor:Find("ExpLevel/NeedExp/ExpIcon"):GetComponent(typeof(UIWidget))
    self.expSlider = anchor:Find("ExpLevel/ExpSlider"):GetComponent(typeof(UISlider))
    self.additionLabel = anchor:Find("ExpLevel/Addition"):GetComponent(typeof(UILabel))
    self.additionBg = anchor:Find("ExpLevel/Addition/Bg"):GetComponent(typeof(UIWidget))
    self.additionDetail = anchor:Find("ExpLevel/AdditionDetail")
    self.additionDetail_ClickBg = self.additionDetail:Find("ClickBg").gameObject
    self.additionDetail_Bg = self.additionDetail:Find("Bg"):GetComponent(typeof(UIWidget))
    self.additionDetail_Template = self.additionDetail:Find("Template").gameObject
    self.additionDetail_Grid = self.additionDetail:Find("Grid"):GetComponent(typeof(UIGrid))
    self.additionDetail_Template:SetActive(false)
    self.additionDetail.gameObject:SetActive(false)

    self.timeLabel = anchor:Find("Time"):GetComponent(typeof(UILabel))
    self.buyLevelButton = anchor:Find("BuyLevelButton").gameObject
    self.tabBar = anchor:Find("Tabs"):GetComponent(typeof(UITabBar))
    self.taskView = anchor:Find("TaskView").gameObject
    self.rewardView = anchor:Find("RewardView").gameObject
    self.otherView = anchor:Find("OtherView").gameObject
    self.rewardTabRedDot = self.tabBar.transform:Find("RewardTab/Alert").gameObject
    self.taskTabRedDot = self.tabBar.transform:Find("TaskTab/Alert").gameObject
end

function LuaCommonPassportWnd:InitEventListener()
    UIEventListener.Get(self.buyLevelButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnBuyLevelButtonClick()
    end)

    local tipButton = self.transform:Find("Anchor/TipButton").gameObject
    UIEventListener.Get(tipButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnTipButtonClick()
    end)
end

function LuaCommonPassportWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdatePassDataWithId", self, "OnUpdatePassDataWithId")
end

function LuaCommonPassportWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdatePassDataWithId", self, "OnUpdatePassDataWithId")
end

function LuaCommonPassportWnd:OnUpdatePassDataWithId(id)
    if self.passId == id then
        self:UpdateData()
        self:UpdateExpLevel()
        self:UpdateTabRedDot()
        self:UpdateTime()
    end
end

function LuaCommonPassportWnd:Init()
    self:UpdateData()
    self:UpdateExpLevel()
    self:InitTabs()
    self:UpdateTabRedDot()
    self:UpdateTime()
    Gac2Gas.RequestPlayerOpenWndForPass("") -- 通知服务器更新一下任务信息
end

function LuaCommonPassportWnd:UpdateData()
    self.passId = LuaCommonPassportMgr.passId
    local level = LuaCommonPassportMgr:GetLevel(self.passId)
    self.maxLevel = LuaCommonPassportMgr:GetMaxPassLevel(self.passId)
    self.upgradeProgressInfo = LuaCommonPassportMgr:GetUpgradeProgressInfo(self.passId)
    self.isVIP1Open = LuaCommonPassportMgr:GetIsVIP1Open(self.passId)
    self.isVIP2Open = LuaCommonPassportMgr:GetIsVIP2Open(self.passId)

    -- 在通行证购买等级界面时等级发生变化，需要重新载入一下这个界面
    if self.level and self.level ~= level then
        self.level = level
        if CUIManager.IsLoaded(CLuaUIResources.CommonTongXingZhengBuyLevelWnd) then
            CUIManager.CloseUI(CLuaUIResources.CommonTongXingZhengBuyLevelWnd)
            if level < self.maxLevel then
                self:OnBuyLevelButtonClick()
            end
        end
    else
        self.level = level
    end
end

-- 更新经验条等信息
function LuaCommonPassportWnd:UpdateExpLevel()
    self.levelLabel.text = self.level
    local passNames = Pass_ClientSetting.GetData(self.passId).BattlePassNames
    if self.level >= self.maxLevel then
        self.needExpLabel.text = SafeStringFormat3(LocalString.GetString("%s已满级"), passNames[0])
        self.expIcon.gameObject:SetActive(false)
        self.expSlider.value = 1
        self.buyLevelButton:SetActive(false)
    else
        self.needExpLabel.text = SafeStringFormat3(LocalString.GetString("升至下级还需%d"), self.upgradeProgressInfo.needProgress)
        self.expIcon:ResetAndUpdateAnchors()
        self.expSlider.value = self.upgradeProgressInfo.hasProgress / self.upgradeProgressInfo.totalProgress
    end

    self.additionDetailTbl = {}
    local finalRate = 0
    local progressAwardsTbl = LuaCommonPassportMgr:ParseProgressRewards(self.passId)
    if self.isVIP1Open and progressAwardsTbl[1].rate > 0 then
        local reason = SafeStringFormat3(LocalString.GetString("%s加成"), passNames[1])
        table.insert(self.additionDetailTbl, {reason = reason, rate = progressAwardsTbl[1].rate})
        finalRate = finalRate + progressAwardsTbl[1].rate
    end
    if self.isVIP2Open and progressAwardsTbl[2].rate > 0 then
        local reason = SafeStringFormat3(LocalString.GetString("%s加成"), passNames[2])
        table.insert(self.additionDetailTbl, {reason = reason, rate = progressAwardsTbl[2].rate})
        finalRate = finalRate + progressAwardsTbl[2].rate
    end

    if Pass_Addition.GetData(self.passId).NewServerAddType == 2 then
        local newServerAddRateTbl = LuaCommonPassportMgr:ParseOpenServerTime2AddRate(self.passId)
        local mainPlayer = CClientMainPlayer.Inst
        local serverId = mainPlayer and mainPlayer:GetMyServerId() or 0
        local newServerAddRate = newServerAddRateTbl[serverId] or 0
        if newServerAddRate > 0 then
            finalRate = finalRate + newServerAddRate
            table.insert(self.additionDetailTbl, {reason = LocalString.GetString("新服加成"), rate = newServerAddRate})
        end
    end

    self.additionLabel.gameObject:SetActive(finalRate > 0)
    if finalRate > 0 then
        self.additionLabel.text = SafeStringFormat3(LocalString.GetString("+%d%%任务经验获得"), math.floor(finalRate * 100))
        self.additionBg:ResetAndUpdateAnchors()
        UIEventListener.Get(self.additionLabel.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnAdditionClick()
        end)
    end
end

-- 初始化Tab（奖励、任务和其他）
function LuaCommonPassportWnd:InitTabs()
    local otherTabText = Pass_ClientSetting.GetData(self.passId).OtherTabText
    local otherTab = self.tabBar.transform:Find("OtherTab")
    if not System.String.IsNullOrEmpty(otherTabText) then
        otherTab.gameObject:SetActive(true)
        otherTab:GetComponent(typeof(QnTabButton)).Text = otherTabText
    else
        otherTab.gameObject:SetActive(false)
    end
    self.tabBar:ReloadTabButtons()
    self.tabBar.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
		self:OnTabChange(index + 1)
    end)
    if LuaCommonPassportMgr:HasTaskAward(self.passId) and not LuaCommonPassportMgr:HasPassAward(self.passId) then
        self.tabBar:ChangeTab(1, false)
    else
        self.tabBar:ChangeTab(0, false)
    end
end

-- 更新奖励和任务上的红点
function LuaCommonPassportWnd:UpdateTabRedDot()
    self.rewardTabRedDot:SetActive(LuaCommonPassportMgr:HasPassAward(self.passId))
    self.taskTabRedDot:SetActive(LuaCommonPassportMgr:HasTaskAward(self.passId))
end

-- 更新倒计时时间
function LuaCommonPassportWnd:UpdateTime()
    local endTime = Pass_Setting.GetData(self.passId).EndTime
    local leftSeconds = CServerTimeMgr.Inst:GetTimeStampByStr(endTime) - CServerTimeMgr.Inst.timeStamp
    self.timeLabel.text = SafeStringFormat3(LocalString.GetString("%s后活动结束"), LuaCommonPassportMgr:GetLeftTimeText(leftSeconds))
end

-- 更新任务经验加成的信息
function LuaCommonPassportWnd:UpdateAdditionDetail()
    UIEventListener.Get(self.additionDetail_ClickBg).onClick = DelegateFactory.VoidDelegate(function(go)
        self.additionDetail.gameObject:SetActive(false)
    end)

    Extensions.RemoveAllChildren(self.additionDetail_Grid.transform)
    for _, data in ipairs(self.additionDetailTbl) do
        local child = NGUITools.AddChild(self.additionDetail_Grid.gameObject, self.additionDetail_Template)
        child:SetActive(true)
        child.transform:Find("Reason"):GetComponent(typeof(UILabel)).text = data.reason
        child.transform:Find("Rate"):GetComponent(typeof(UILabel)).text = SafeStringFormat3("+%d%%", math.floor(data.rate * 100))
    end
    self.additionDetail_Grid:Reposition()

    local bounds = NGUIMath.CalculateRelativeWidgetBounds(self.additionDetail_Grid.transform)
    self.additionDetail_Bg.height = bounds.size.y + 40
end

--@region UIEvent

function LuaCommonPassportWnd:OnBuyLevelButtonClick()
    local rewardDesignData = LuaCommonPassportMgr:GetRewardDesignData(self.passId)
    local rewardList = {}
    local lastLevel = self.level
    for _, data1 in ipairs(rewardDesignData) do
        if data1.level > self.level then
            local rewardData = Pass_Reward.GetData(data1.id)
            local needProgress = rewardData.Progress
            local itemList = {}
            local itemRewardsTbl = LuaCommonPassportMgr:ParseItemRewards(data1.id)
            for typeId, data2 in ipairs(itemRewardsTbl) do
                if not (typeId == 2 and not self.isVIP1Open) and not (typeId == 3 and not self.isVIP2Open) then
                    for _, data3 in ipairs(data2) do
                        local weightData = Pass_ItemWeight.GetData(data3.itemId)
                        table.insert(itemList, {
                            ItemID = data3.itemId,
                            Count = data3.count,
                            IsBind = data3.isBind,
                            weight = weightData and weightData.Weight * -1 or 0,
                        })
                    end
                end
            end
            rewardList[data1.level] = {needExp = needProgress, ItemList = itemList}

            for i = lastLevel + 1, data1.level - 1 do
                rewardList[i] = {needExp = needProgress}
            end
            lastLevel = data1.level
        end
    end

    local oneJadeAddProgress = LuaCommonPassportMgr:GetOneJadeAddProgress(self.passId)
    local buyCallBack = function(cost, exp, level)
        self:BuyLevelCallBack(cost, exp, level)
    end
    local expIcon = Pass_ClientSetting.GetData(self.passId).ExpIcon
    LuaCommonTongXingZhengBuyLevelMgr:ShowBuyLevelWnd(self.level, self.maxLevel, self.upgradeProgressInfo.hasProgress, rewardList, oneJadeAddProgress, expIcon, buyCallBack)
end

function LuaCommonPassportWnd:OnTipButtonClick()
    g_MessageMgr:ShowMessage(Pass_ClientSetting.GetData(self.passId).MainWndTips)
end

function LuaCommonPassportWnd:OnTabChange(index)
    self.rewardView:SetActive(index == 1)
    self.taskView:SetActive(index == 2)
    self.otherView:SetActive(index == 3)

    if index == 1 then
        self.rewardView.transform:GetComponent(typeof(CCommonLuaScript)):Init({})
    elseif index == 2 then
        self.taskView.transform:GetComponent(typeof(CCommonLuaScript)):Init({})
    end
end

function LuaCommonPassportWnd:OnAdditionClick()
    self.additionDetail.gameObject:SetActive(true)
    self:UpdateAdditionDetail()
end

function LuaCommonPassportWnd:BuyLevelCallBack(cost, exp, level)
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer then
        if mainPlayer.Jade < cost then -- 绑定灵玉不可用
            g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("JADE_PAY_NOTICE"), function ()
                CShopMallMgr.ShowChargeWnd()
            end, nil, nil, nil, false)
        else
            Gac2Gas.RequestBuyPassLevel(self.passId, level + self.level)
            CUIManager.CloseUI(CLuaUIResources.CommonTongXingZhengBuyLevelWnd)
        end
    end
end

--@endregion UIEvent
