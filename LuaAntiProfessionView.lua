local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UICircleGrid = import "L10.UI.UICircleGrid"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local QnButton = import "L10.UI.QnButton"
local EnumClass = import "L10.Game.EnumClass"
local Profession = import "L10.Game.Profession"
local CUITexture = import "L10.UI.CUITexture"
local CUIFx = import "L10.UI.CUIFx"
local QnTableView = import "L10.UI.QnTableView"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Constants = import "L10.Game.Constants"

LuaAntiProfessionView = class()

RegistChildComponent(LuaAntiProfessionView, "Circle", "Circle", UICircleGrid)
RegistChildComponent(LuaAntiProfessionView, "LvLab", "LvLab", UILabel)
RegistChildComponent(LuaAntiProfessionView, "AntiTemplate", "AntiTemplate", GameObject)
RegistChildComponent(LuaAntiProfessionView, "OpBtn", "OpBtn", QnButton)
RegistChildComponent(LuaAntiProfessionView, "TipBtn", "TipBtn", QnButton)
RegistChildComponent(LuaAntiProfessionView, "NoEffectTip", "NoEffectTip", GameObject)

RegistClassMember(LuaAntiProfessionView, "m_CurSlotNum")
RegistClassMember(LuaAntiProfessionView, "m_EffectsTable")
RegistClassMember(LuaAntiProfessionView, "m_EffectsInfos")
RegistClassMember(LuaAntiProfessionView, "m_FxNeedDestroy")

function LuaAntiProfessionView:Awake()
    self.AntiTemplate:SetActive(false)
    self.m_FxNeedDestroy = {}
end

function LuaAntiProfessionView:Start()
    self:InitComponents()

    self.OpBtn.OnClick = DelegateFactory.Action_QnButton(function (btn)
        if #self.m_EffectsInfos == 0 then
            g_MessageMgr:ShowMessage("ANTIPROFESSION_NOT_LEARN")
            return
        end
        CUIManager.ShowUI(CLuaUIResources.AntiProfessionMakeWnd)
    end)

    self.TipBtn.OnClick = DelegateFactory.Action_QnButton(function (btn)
        g_MessageMgr:ShowMessage("ANTIPROFESSION_VIEW_TIP")
    end)

    self:LoadAntiProfessions()

    self:InitEffects()
    
    CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.AntiProfessionGuide)
end

--@region UIEvent

--@endregion

function LuaAntiProfessionView:InitComponents()
    self.m_EffectsTable = self.transform:Find("Anchor/Right/RightTop/Effects/EffectsTable"):GetComponent(typeof(QnTableView))
end

function LuaAntiProfessionView:OnEnable()
    g_ScriptEvent:AddListener("AntiProfessionUnlockSlotResult", self, "OnAntiProfessionUnlockSlotResult")
    g_ScriptEvent:AddListener("AntiProfessionUpgradeSkillResult", self, "OnAntiProfessionUpgradeSkillResult")
    g_ScriptEvent:AddListener("AntiProfessionBreakLimitResult", self, "OnAntiProfessionUpgradeSkillResult")
end

function LuaAntiProfessionView:OnDisable()
    g_ScriptEvent:RemoveListener("AntiProfessionUnlockSlotResult", self, "OnAntiProfessionUnlockSlotResult")
    g_ScriptEvent:RemoveListener("AntiProfessionUpgradeSkillResult", self, "OnAntiProfessionUpgradeSkillResult")
    g_ScriptEvent:RemoveListener("AntiProfessionBreakLimitResult", self, "OnAntiProfessionUpgradeSkillResult")

    for k, v in pairs(self.m_FxNeedDestroy) do
        v:DestroyFx()
    end
    self.m_FxNeedDestroy = {}
end

function LuaAntiProfessionView:OnAntiProfessionUnlockSlotResult(bSuccess, Index, Profession, MaxLevel, FailTimes)
    if bSuccess == true then
        self:LoadAntiProfessions()
        self:RefreshEffects()
        local item = self.Circle.transform:GetChild(Index - 1)
        local fx = item.transform:Find("Fx"):GetComponent(typeof(CUIFx))
        fx:LoadFx("fx/ui/prefab/UI_zhiyekefu_tupo.prefab")
        table.insert(self.m_FxNeedDestroy, fx)
    end
end

function LuaAntiProfessionView:OnAntiProfessionUpgradeSkillResult()
    self:LoadAntiProfessions()
    self:RefreshEffects()
end

function LuaAntiProfessionView:LoadAntiProfessions()
    local needReposition = false
    for i = 1, (EnumToInt(EnumClass.Undefined) - 1) - self.Circle.transform.childCount do
        local go = NGUITools.AddChild(self.Circle.gameObject, self.AntiTemplate)
        go:SetActive(true)
        needReposition = true
    end
    if needReposition then
        self.Circle:Reposition()
    end

    self.m_CurSlotNum = CClientMainPlayer.Inst.SkillProp.AntiProfessionSlot.CurSlotNum

    for i = 0, self.Circle.transform.childCount - 1 do
        local item = self.Circle.transform:GetChild(i)
        local info = CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.SkillProp.AntiProfessionSkill, i + 1) and CClientMainPlayer.Inst.SkillProp.AntiProfessionSkill[i + 1] or nil

        self:InitItem(i, item, info)
    end
    
    self.LvLab.text = SafeStringFormat3(LocalString.GetString("符力 %.1f"), LuaZongMenMgr:CalMainPlayerAntiProfessionLevel())

    self.NoEffectTip:SetActive(self.m_CurSlotNum == 0)
end

function LuaAntiProfessionView:InitItem(index, item, info)
    local Btn       = item:GetComponent(typeof(QnButton))
    local NameLab   = item:Find("NameLab"):GetComponent(typeof(UILabel))
    local SeqLab    = item:Find("SeqLab"):GetComponent(typeof(UILabel))
    local LvLab     = item:Find("LvLab"):GetComponent(typeof(UILabel))
    local Lock      = item:Find("Lock").gameObject
    local Icon      = item:Find("Icon"):GetComponent(typeof(CUITexture))
    local Unlockable= item:Find("Unlockable").gameObject
    local Alert     = item:Find("Alert").gameObject
    local Fx        = item:Find("Fx"):GetComponent(typeof(CUIFx))
    local Slider    = item:Find("CircleSlider"):GetComponent(typeof(UISlider))

    if index < self.m_CurSlotNum then
        local curLevel = info.CurLevel
        local maxLevel = info.MaxLevel
        local profession = info.Profession
        local upgradeFailTimes = info.UpgradeFailTimes
        local proClass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), profession)
        local proName = Profession.GetName(proClass)

        if CClientMainPlayer.Inst.IsInXianShenStatus then
            local index = index + 1
            local oriLv = curLevel
            local adjustLv = LuaZongMenMgr:GetAdjustAntiProfessionSkillLevel(index, oriLv)
            if oriLv ~= adjustLv then
                LvLab.text = SafeStringFormat3("[c][%s]lv.%d%%[-][/c]", Constants.ColorOfFeiSheng, adjustLv)
            else
                LvLab.text = SafeStringFormat3("lv.%d%%", oriLv)
            end
        else
            LvLab.text = SafeStringFormat3("lv.%d%%", curLevel)
        end

        NameLab.text = LocalString.GetString("克") .. proName
        SeqLab.text = index + 1
        LvLab.gameObject:SetActive(true)
        LvLab.text = "lv." .. curLevel
        Lock:SetActive(false)
        Icon.gameObject:SetActive(true)
        Icon:LoadMaterial(Profession.GetLargeIcon(proClass))
        Unlockable:SetActive(false)
        Alert:SetActive(self:ShowAlert(index + 1))
        self:SetSliderProgress(Slider, index + 1, profession)

        Btn.OnClick = DelegateFactory.Action_QnButton(function (btn)
            Alert:SetActive(false)
            LuaZongMenMgr.m_AntiProfessionClickedSlot[index + 1] = true
            LuaZongMenMgr.m_SelectedAntiProfessionIndex = index + 1
            CUIManager.ShowUI(CLuaUIResources.AntiProfessionLevelUpWnd)
        end)
    elseif index == self.m_CurSlotNum then
        NameLab.text = ""
        SeqLab.text = index + 1
        LvLab.gameObject:SetActive(false)
        Lock:SetActive(false)
        Icon.gameObject:SetActive(false)
        Unlockable:SetActive(true)
        Alert:SetActive(false)
        Slider.gameObject:SetActive(false)
        Slider.value = 0

        Btn.OnClick = DelegateFactory.Action_QnButton(function (btn)
            CUIManager.ShowUI(CLuaUIResources.AntiProfessionUnlockWnd)
        end)
    else
        NameLab.text = ""
        SeqLab.text = index + 1
        LvLab.gameObject:SetActive(false)
        Lock:SetActive(true)
        Icon.gameObject:SetActive(false)
        Unlockable:SetActive(false)
        Alert:SetActive(false)
        Slider.gameObject:SetActive(false)
        Slider.value = 0

        Btn.OnClick = DelegateFactory.Action_QnButton(function (btn)
            g_MessageMgr:ShowMessage("ANTIPROFESSION_SEQUENCEUNLOCK_TIP")
        end)
    end

    local dir = CommonDefs.op_Subtraction_Vector3_Vector3(self.Circle.transform.position, Icon.transform.position)
    local offset = CommonDefs.op_Multiply_Vector3_Single(dir, 0.3)
    NameLab.transform.position = CommonDefs.op_Addition_Vector3_Vector3(Icon.transform.position, offset)
end

function LuaAntiProfessionView:ShowAlert(index)
    local canTupo = self:CanTupo(index)

    return (not LuaZongMenMgr.m_AntiProfessionClickedSlot[index]) and canTupo
end

function LuaAntiProfessionView:CanTupo(index)
    local info = CClientMainPlayer.Inst.SkillProp.AntiProfessionSkill[index]

    if info.CurLevel >= info.MaxLevel  then
        local curMaxLv = info.MaxLevel
        local nextMaxLv = curMaxLv
        for i = curMaxLv + 1, SoulCore_AntiProfessionLevel.GetDataCount() do
            local data = SoulCore_AntiProfessionLevel.GetData(i)
            if data.IsBreakLimitLevel == 1 then
                nextMaxLv = i
                local skillPrivateExp = CClientMainPlayer.Inst.SkillProp.SkillPrivateExp
                local exp = CClientMainPlayer.Inst.PlayProp.Exp
                local freeSilver = CClientMainPlayer.Inst.FreeSilver
                local silver = CClientMainPlayer.Inst.Silver

                local expEnough = (skillPrivateExp + exp) >= data.BreakLimitExp
                local moneyEnough = (freeSilver + silver >= 0) and ((freeSilver + silver) >= data.BreakLimitMoney)

                return expEnough and moneyEnough
            end
        end
    end

    return false
end

function LuaAntiProfessionView:InitEffects()
    self.m_EffectsTable.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_EffectsInfos
        end,
        function(item, index)
            self:InitEffect(item, self.m_EffectsInfos[index + 1])
        end
    )

    self:RefreshEffects()
end

function LuaAntiProfessionView:RefreshEffects()
    self.m_EffectsInfos = {}

    CommonDefs.DictIterate(CClientMainPlayer.Inst.SkillProp.AntiProfessionSkill, DelegateFactory.Action_object_object(function(___key, ___value)
        table.insert(self.m_EffectsInfos, {key = ___key, value = ___value})
    end))
    
    self.m_EffectsTable:ReloadData(true, false)
end

function LuaAntiProfessionView:InitEffect(item, info)
    local lab = item.transform:Find("Lab"):GetComponent(typeof(UILabel))

    local profession = info.value.Profession
    local proClass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), profession)

    local oriData = SoulCore_AntiProfessionLevel.GetData(info.value.CurLevel)
    local oriMul =  oriData and oriData.AntiProfessionMul or 0
    local effectStr
    
    if CClientMainPlayer.Inst.IsInXianShenStatus then
        local index = info.key
        local oriLv = info.value.CurLevel
        local adjustLv = LuaZongMenMgr:GetAdjustAntiProfessionSkillLevel(index, oriLv)
        if oriLv ~= adjustLv then
            local adjustData = SoulCore_AntiProfessionLevel.GetData(adjustLv)
            local adjustMul =  adjustData and adjustData.AntiProfessionMul or 0
            effectStr = SafeStringFormat3("[c][%s]+%.1f%%[-][/c] (%.1f%%)", Constants.ColorOfFeiSheng, adjustMul * 100, oriMul * 100)
        else
            effectStr = SafeStringFormat3("+%.1f%%", oriMul * 100)
        end
    else
        effectStr = SafeStringFormat3("+%.1f%%", oriMul * 100)
    end

    lab.text = SafeStringFormat3(LocalString.GetString("对%s伤害 %s"), Profession.GetFullName(proClass) , effectStr)
end

function LuaAntiProfessionView:GetGuideGo(methodName)
    if methodName=="GetUnlockButton" then
		local curSlotNum = CClientMainPlayer.Inst.SkillProp.AntiProfessionSlot.CurSlotNum
        if 0 <= curSlotNum and curSlotNum <= self.Circle.transform.childCount - 1 then
            return self.Circle.transform:GetChild(curSlotNum).gameObject
        end
	end
	return nil
end

function LuaAntiProfessionView:SetSliderProgress(Slider, index, profession)
    local info = CClientMainPlayer.Inst.SkillProp.AntiProfessionSkill[index]
    if info.CurLevel >= SoulCore_AntiProfessionLevel.GetDataCount() then
        --满级
        Slider.gameObject:SetActive(false)
        Slider.value = 1
    elseif info.CurLevel >= info.MaxLevel then
        --可突破
        Slider.gameObject:SetActive(true)
        Slider.value = 1
    else
        --可以升级
        local nextSkillId = index * 100 + info.CurLevel + 1
        local nextSkillData = SoulCore_AntiProfessionSkills.GetData(nextSkillId)
        local needScore = nextSkillData.InScore

        local ownScore = CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.SkillProp.AntiProfessionScore, profession) and CClientMainPlayer.Inst.SkillProp.AntiProfessionScore[profession] or 0

        Slider.gameObject:SetActive(true)
        Slider.value = needScore ~= 0 and Mathf.Clamp(ownScore / needScore, 0, 1) or 1
    end
end
