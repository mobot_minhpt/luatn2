local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"

LuaGuildTerritorialWarsView = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaGuildTerritorialWarsView, "Label", "Label", UILabel)

--@endregion RegistChildComponent end

function LuaGuildTerritorialWarsView:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    local season = GuildTerritoryWar_Setting.GetData().CurrentSeasonId
    self.Label.text = SafeStringFormat3(LocalString.GetString("天地棋局 • 第%s赛季"),season)
    UIEventListener.Get(self.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnViewClick()
	end)
end
--@region UIEvent

--@endregion UIEvent

function LuaGuildTerritorialWarsView:OnViewClick()
    LuaJuDianBattleMgr.m_ShowTianDiEnterWnd = true
    LuaJuDianBattleMgr:OpenGuildWarsEnterWnd()
end
