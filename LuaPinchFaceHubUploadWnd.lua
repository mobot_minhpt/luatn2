local UIInput = import "UIInput"
local UITexture = import "UITexture"
local QnCheckBox = import "L10.UI.QnCheckBox"
local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local CPinchFaceCinemachineCtrlMgr = import "L10.Game.CPinchFaceCinemachineCtrlMgr"
local CPinchFaceHubMgr = import "L10.Game.CPinchFaceHubMgr"
local CFacialMgr = import "L10.Game.CFacialMgr"
local DelegateFactory  = import "DelegateFactory"
local Texture2D = import "UnityEngine.Texture2D"
local ImageConversion = import "UnityEngine.ImageConversion"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local Screen = import "UnityEngine.Screen"
local LoadPicMgr = import "L10.Game.LoadPicMgr"
local Texture2D = import "UnityEngine.Texture2D"
local TextureFormat = import "UnityEngine.TextureFormat"
local Screen = import "UnityEngine.Screen"
local Rect = import "UnityEngine.Rect"
local TextureWrapMode = import "UnityEngine.TextureWrapMode"
local RenderTexture = import "UnityEngine.RenderTexture"
local RenderTextureFormat = import "UnityEngine.RenderTextureFormat"
local RenderTextureReadWrite = import "UnityEngine.RenderTextureReadWrite"
local CFuxiFaceDNAMgr = import "L10.Game.CFuxiFaceDNAMgr"
local BoxCollider = import "UnityEngine.BoxCollider"
local Constants = import "L10.Game.Constants"

LuaPinchFaceHubUploadWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaPinchFaceHubUploadWnd, "TitleInput", "TitleInput", UIInput)
RegistChildComponent(LuaPinchFaceHubUploadWnd, "DescInput", "DescInput", UIInput)
RegistChildComponent(LuaPinchFaceHubUploadWnd, "Texture", "Texture", UITexture)
RegistChildComponent(LuaPinchFaceHubUploadWnd, "VisibilityCheckBox", "VisibilityCheckBox", QnCheckBox)
RegistChildComponent(LuaPinchFaceHubUploadWnd, "UploadBtn", "UploadBtn", GameObject)
RegistChildComponent(LuaPinchFaceHubUploadWnd, "EnterHubBtn", "EnterHubBtn", GameObject)
RegistChildComponent(LuaPinchFaceHubUploadWnd, "TitleLabel", "TitleLabel", GameObject)

--@endregion RegistChildComponent end

function LuaPinchFaceHubUploadWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.UploadBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnUploadBtnClick()
	end)


	
	UIEventListener.Get(self.EnterHubBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnEnterHubBtnClick()
	end)


    --@endregion EventBind end

    self.DescLabel = self.transform:Find("DescLabel").gameObject
end

function LuaPinchFaceHubUploadWnd:Init()
    self.EnterHubBtn.gameObject:SetActive(false)
    self.UploadBtn.gameObject:SetActive(true)

    if not CPinchFaceCinemachineCtrlMgr.Inst then return end
    local camera = CPinchFaceCinemachineCtrlMgr.Inst.m_Cam

    self.m_Tex = CUICommonDef.DoCaptureWithClip(camera, 4/3)
    self.m_Tex.wrapMode = TextureWrapMode.Clamp

    self.Texture.mainTexture = self.m_Tex

    -- 获取一下当前的个人作品信息
    CPinchFaceHubMgr.Inst:GetSelfList()

    -- 默认私有
    self.VisibilityCheckBox.Selected = true
end

function LuaPinchFaceHubUploadWnd:OnDestroy()
    GameObject.Destroy(self.m_Tex)
    self.Texture.mainTexture = nil
end

--@region UIEvent

function LuaPinchFaceHubUploadWnd:OnUploadBtnClick()
    if self.m_IsUploading then
        g_MessageMgr:ShowMessage("PinchFaceHub_IsUploading")
        return
    end

    -- 判断作品列表是否已满
    local usage = CPinchFaceHubMgr.Inst.m_SelfUsage
    if usage.max > 0 and usage.current >= usage.max then
        g_MessageMgr:ShowMessage("PinchFaceHub_UploadLimit")
        return
    end

    local token = CPinchFaceHubMgr.Inst.Token
    local title = self.TitleInput.value
    if title == nil or title == "" then
        title = LocalString.GetString("无题")
    end

    local desc = self.DescInput.value
    if desc == nil or desc == "" then
        desc = LocalString.GetString("我发布了新的设计，快来看看吧！")
    end

    local data = nil
    if LuaPinchFaceMgr.m_RO ~= nil then
        data = CFacialMgr.GenerateHubData(LuaPinchFaceMgr.m_RO, EnumToInt(LuaPinchFaceMgr.m_CurEnumClass), EnumToInt(LuaPinchFaceMgr.m_CurEnumGender),
            LuaPinchFaceMgr.m_HairId, LuaPinchFaceMgr.m_HeadId)
    end

    local roleName = LocalString.GetString("佚名")
    if CClientMainPlayer.Inst ~= nil then
        roleName = CClientMainPlayer.Inst.Name
    end

    local titleCheck = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(title, false)
    if titleCheck.msg == nil or titleCheck.shouldBeIgnore then 
        g_MessageMgr:ShowMessage("Speech_Violation")
        return
    end

    local descCheck = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(desc, false)
    if descCheck.msg == nil or descCheck.shouldBeIgnore then 
        g_MessageMgr:ShowMessage("Speech_Violation")
        return
    end

    local byte = nil
    
    -- 编码时候 降低质量
    if self.m_Tex then
        byte = ImageConversion.EncodeToJPG(self.m_Tex, 25)
        print("byte.Length", byte.Length)
    end

    local vis = 0
    if self.VisibilityCheckBox.Selected then
        vis = 1
    end

    -- 尝试上传作品
    if token ~= nil and token ~= "" and byte then
        CFuxiFaceDNAMgr.Inst:TryReportApplyLog(CFuxiFaceDNAMgr.Inst.m_IsRandom and "RandomUpload" or "PhotoUpload")
        self.m_IsUploading = true
        CPinchFaceHubMgr.Inst:UploadWork(byte, data, title, desc, roleName, EnumToInt(LuaPinchFaceMgr.m_CurEnumGender),
            vis, 0, EnumToInt(LuaPinchFaceMgr.m_CurEnumClass), 
        DelegateFactory.Action(function()
            self.m_IsUploading = false
            g_MessageMgr:ShowMessage("PinchFaceHub_UploadFailed")
        end), 
        
        DelegateFactory.Action_CPinchFaceHub_AddWork_Ret(function(ret)
            self.m_IsUploading = false
            if vis == 1 then
                g_MessageMgr:ShowMessage("PinchFaceHub_UploadSuccess_Not_Public")
            else
                g_MessageMgr:ShowMessage("PinchFaceHub_UploadSuccess_Public")
            end

            -- 成功上传
            if CUIManager.IsLoaded(CLuaUIResources.PinchFaceHubUploadWnd) then
                self.EnterHubBtn.gameObject:SetActive(true)
                self.UploadBtn.gameObject:SetActive(false)

                -- 隐藏说明元素
                self.DescInput.gameObject:SetActive(false)
                self.TitleInput.gameObject:SetActive(false)
                self.VisibilityCheckBox.gameObject:SetActive(false)
                self.TitleLabel.gameObject:SetActive(false)
                self.DescLabel.gameObject:SetActive(false)
            end
            
        end))
    else
        g_MessageMgr:ShowMessage("PinchFaceHub_UploadFailed")
    end
end


function LuaPinchFaceHubUploadWnd:OnEnterHubBtnClick()
    CUIManager.CloseUI(CLuaUIResources.PinchFaceHubUploadWnd)
    CUIManager.ShowUI(CLuaUIResources.PinchFaceHubWnd)
end


--@endregion UIEvent

