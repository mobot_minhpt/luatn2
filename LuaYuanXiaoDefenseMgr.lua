local CScene = import "L10.Game.CScene"
local CSkillButtonBoardWnd = import "L10.UI.CSkillButtonBoardWnd"
local CTeamMgr = import "L10.Game.CTeamMgr"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local CRenderScene = import "L10.Engine.Scene.CRenderScene"
local GameObject = import "UnityEngine.GameObject"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CEffectMgr = import "L10.Game.CEffectMgr"
local Ease = import "DG.Tweening.Ease"
local CResourceMgr = import "L10.Engine.CResourceMgr"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local EnumObjectType = import "L10.Game.EnumObjectType"
local Quaternion = import "UnityEngine.Quaternion"
local CPos = import "L10.Engine.CPos"
local CParticleEffect = import "L10.Game.CParticleEffect"
local CWorldPositionFX = import "L10.Game.CWorldPositionFX"
local CMainCamera = import "L10.Engine.CMainCamera"
local MouseInputHandler = import "L10.Engine.MouseInputHandler"
local CTaskAndTeamWnd = import "L10.UI.CTaskAndTeamWnd"
local CClientOtherPlayer = import "L10.Game.CClientOtherPlayer"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local CClientObject = import "L10.Game.CClientObject"
local CRenderObject = import "L10.Engine.CRenderObject"
local EKickOutType = import "L10.Engine.EKickOutType"
local CSkillAcquisitionMgr = import "L10.UI.CSkillAcquisitionMgr"
local CClientNpc = import "L10.Game.CClientNpc"
local NPC_NPC = import "L10.Game.NPC_NPC"

LuaYuanXiaoDefenseMgr = {}


if rawget(_G, "LuaYuanXiaoDefenseMgr") then
    g_ScriptEvent:RemoveListener("ClientObjCreate", LuaYuanXiaoDefenseMgr, "OnClientObjCreate")
    g_ScriptEvent:RemoveListener("ClientObjDestroy", LuaYuanXiaoDefenseMgr, "OnClientObjDestroy")
end

g_ScriptEvent:AddListener("ClientObjCreate", LuaYuanXiaoDefenseMgr, "OnClientObjCreate")
g_ScriptEvent:AddListener("ClientObjDestroy", LuaYuanXiaoDefenseMgr, "OnClientObjDestroy")


LuaYuanXiaoDefenseMgr.Width = 15
LuaYuanXiaoDefenseMgr.Height = 6
LuaYuanXiaoDefenseMgr.Origin = {x = 45, y = 17, z = 86} -- from leftBottom to rightTop, x+ z-, x for Width, z for Height

LuaYuanXiaoDefenseMgr.TeamScore = 0
LuaYuanXiaoDefenseMgr.PlayerScore = {}

LuaYuanXiaoDefenseMgr.GridStatus = {} -- 0:NormalGrid, 1:BadGrid(NotDropped), 2:BadGrid(Dropped)
LuaYuanXiaoDefenseMgr.GridObjs = {}
LuaYuanXiaoDefenseMgr.GridTweeners = {}
LuaYuanXiaoDefenseMgr.GridTex = {}
LuaYuanXiaoDefenseMgr.GridRes = {}

LuaYuanXiaoDefenseMgr.FxDict = {}
LuaYuanXiaoDefenseMgr.ShowHideYuanXiaoTick = nil
LuaYuanXiaoDefenseMgr.IsHeadFxLoaded = {} 

--LuaYuanXiaoDefenseMgr.SubmitNpcRO = nil

LuaYuanXiaoDefenseMgr.IsInited = false

function LuaYuanXiaoDefenseMgr:GetGridIndex(x, y)
    return (y - 1) * self.Width + x
end

function LuaYuanXiaoDefenseMgr:GetGridPos(idx)
    return math.fmod((idx - 1), self.Width) + 1, math.ceil((idx - 1) / self.Width) + 1
end

function LuaYuanXiaoDefenseMgr:OnMainPlayerCreated()
    CLuaScreenCaptureWnd.CameraFollowResetToDefaultOnDestroy = false
    CUIManager.ShowUI(CLuaUIResources.YuanXiaoDefenseTopRightWnd)
    CUIManager.CloseUI(CLuaUIResources.YuanXiaoDefenseResultWnd)

	LuaGamePlayMgr:SetSkillButtonBoardState({ 
        showGuaJiBtn = false,
        showSwitchTargetBtn = false,
        showHpRecoverBtn = false,
    }, true)

    local setting = YuanXiao2023_NaoYuanXiao.GetData()
    --local submitNpc = setting.SubmitNpc

    UnRegisterTick(self.ShowHideYuanXiaoTick)
    self.ShowHideYuanXiaoTick = RegisterTick(function() 
        if not CClientMainPlayer.Inst then return end
        --local fndSubmitNpc = false
        CClientObjectMgr.Inst:ForeachObject(DelegateFactory.Action_CClientObject(function(obj)
			if obj.ObjectType == EnumObjectType.NPC then
                if obj.TemplateId == 20007187 then
                    obj.RO.gameObject:SetActive(false)
                elseif obj.TemplateId == 20007186 then
                    for i = 0, obj.RO.transform.childCount - 1 do
                        local child = obj.RO.transform:GetChild(i).gameObject
                        if string.find(child.name, "znpc_yuanxiao_td") then
                            child:SetActive(false)
                            break
                        end
                    end
                --elseif obj.TemplateId == submitNpc[0] then
                --    fndSubmitNpc = true
                --    if not CommonDefs.IsNull(self.SubmitNpcRO) then
                --        self.SubmitNpcRO:Destroy()
                --    end
                --    self.SubmitNpcRO = nil
                end
            elseif obj.ObjectType == EnumObjectType.Player and not self.IsHeadFxLoaded[obj.EngineId] then
                if obj.RO.transform.childCount > 0 then
                    local trans = obj.RO.transform:GetChild(0)
                    local offset
                    if string.find(trans.gameObject.name, "znpc079") then 
                        offset = setting.Znpc079XYZ
                    elseif string.find(trans.gameObject.name, "znpc064") then 
                        offset = setting.Znpc064XYZ
                    elseif string.find(trans.gameObject.name, "znpc068") then 
                        offset = setting.Znpc068XYZ 
                    end
                    if offset then
                        local fxId = setting.TopYuanXiaoFx
                        local fx = CEffectMgr.Inst:AddObjectFX(fxId, obj.RO, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, DelegateFactory.Action_GameObject(
                            function(go)
                                go.transform:GetChild(0).localPosition = Vector3(offset[0], offset[1], offset[2])
                            end
                        ))
                        obj.RO:AddFX(fxId, fx, -1)
                        self.IsHeadFxLoaded[obj.EngineId] = true
                    end
                end
            end
		end))
        --if not fndSubmitNpc and not self.SubmitNpcRO then
        --    self.SubmitNpcRO = self:CreateLocalNpc(submitNpc)
        --end
    end, 100)

    LuaCommonGamePlayTaskViewMgr:AppendGameplayInfo(setting.GameplayId, 
        true, nil, false, 
        true, function() return Gameplay_Gameplay.GetData(setting.GameplayId).Name end, 
        true, function() return CUICommonDef.TranslateToNGUIText(setting.GameplayStageInfo) end,
        true, nil, nil,
        true, nil, function() g_MessageMgr:ShowMessage("YuanXiao2023_NaoYuanXiao_Rule") end)

    CTaskAndTeamWnd.Instance:ChangeToTaskTab()
end

function LuaYuanXiaoDefenseMgr:OnMainPlayerDestroyed()
    CLuaScreenCaptureWnd.CameraFollowResetToDefaultOnDestroy = true
    CUIManager.CloseUI(CLuaUIResources.YuanXiaoDefenseTopRightWnd)

    self.IsInited = false

    --if not CommonDefs.IsNull(self.SubmitNpcRO) then
    --    self.SubmitNpcRO:Destroy()
    --end
    --self.SubmitNpcRO = nil

    for _, v in pairs(self.FxDict) do
        UnRegisterTick(v.tick)
        if not CommonDefs.IsNull(v.fx) then
            v.fx:GetFX():Destroy()
        end
    end
    self.FxDict = {}
    self.IsHeadFxLoaded = {}
    UnRegisterTick(self.ShowHideYuanXiaoTick)
    self.ShowHideYuanXiaoTick = nil

    self.GridRes = {}
    self:ClearGridObjs()
end

function LuaYuanXiaoDefenseMgr:CreateLocalNpc(npc)
    local data = NPC_NPC.GetData(npc[0])
    if data and CRenderScene.Inst then
        local obj = CreateFromClass(GameObject, data.Name)
        obj.transform.parent = CRenderScene.Inst.transform
        local ro = obj:AddComponent(typeof(CRenderObject))
        ro.IsImportant = false
        ro.HasFeisheng = false
        ro.IsKickOuting = false
        ro.CanBeJob = false
        obj.transform.position = Vector3(npc[1] / 64, 19.317, npc[2] / 64)
        LuaUtils.SetLocalRotationY(obj.transform, -90)
        LuaUtils.SetLocalScale(obj.transform, 1.2, 1.2, 1.2)
        ro:LoadMain(CClientNpc.GetNPCPrefabPath(data))
        return ro
    end
end

function LuaYuanXiaoDefenseMgr:InitGridObjs()
    if not self.IsInited then
        local gridsRoot = CRenderScene.Inst.transform:Find("OtherObjects/GridsRoot")
        if not gridsRoot then
            gridsRoot = CreateFromClass(GameObject, "GridsRoot")
            gridsRoot.transform.parent = CRenderScene.Inst.transform:Find("OtherObjects")
        end

        for i = 1, self.Width do
            for j = 1, self.Height do
                local idx = self:GetGridIndex(i, j)
                local template = self.GridRes[self.GridTex[idx]]
                if not CommonDefs.IsNull(template) then
                    local grid = CommonDefs.Object_Instantiate(template)
                    grid:SetActive(true)
                    grid.name = "Grid"..self.GridTex[idx].."_"..i.."_"..j
                    grid.transform.parent = gridsRoot.transform
                    local pos =  Utility.GridPos2WorldPos(self.Origin.x + (i - 1) * 3, self.Origin.z - (j - 1) * 3)
                    grid.transform.position = Vector3(pos.x + 1.5, self.GridStatus[idx] == 2 and 0 or self.Origin.y, pos.z + 1.5)
                    self.GridObjs[self:GetGridIndex(i, j)] = grid
                end
            end
        end
        self.IsInited = true
    end
end

function LuaYuanXiaoDefenseMgr:ClearGridObjs()
    for _, tweener in pairs(self.GridTweeners) do
        LuaTweenUtils.Kill(tweener, false)
    end
    self.GridTweeners = {}
    for _, obj in pairs(self.GridObjs) do
        if not CommonDefs.IsNull(obj) then
            CResourceMgr.Inst:Destroy(obj)
        end
    end
    self.GridObjs = {}
end


function LuaYuanXiaoDefenseMgr:OnClientObjCreate(args)
    if not self:IsInYuanXiaoDefense() then return end
	local engineId = args[0]
	local obj = CClientObjectMgr.Inst:GetObject(engineId)
    if obj then
        local setting = YuanXiao2023_NaoYuanXiao.GetData()
        --local submitNpc = setting.SubmitNpc
        if obj.ObjectType == EnumObjectType.NPC then
            if obj.TemplateId == 20007187 then
                obj.RO.gameObject:SetActive(false)
            --elseif obj.TemplateId == submitNpc[0] then
            --    if not CommonDefs.IsNull(self.SubmitNpcRO) then
            --        self.SubmitNpcRO:Destroy()
            --    end
            --    self.SubmitNpcRO = nil
            end
        elseif obj.ObjectType == EnumObjectType.Player and not self.IsHeadFxLoaded[engineId] then
            if obj.RO.transform.childCount > 0 then
                local trans = obj.RO.transform:GetChild(0)
                local offset
                if string.find(trans.gameObject.name, "znpc079") then 
                    offset = setting.Znpc079XYZ
                elseif string.find(trans.gameObject.name, "znpc064") then 
                    offset = setting.Znpc064XYZ
                elseif string.find(trans.gameObject.name, "znpc068") then 
                    offset = setting.Znpc068XYZ 
                end
                if offset then
                    local fxId = setting.TopYuanXiaoFx
                    local fx = CEffectMgr.Inst:AddObjectFX(fxId, obj.RO, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, DelegateFactory.Action_GameObject(
                        function(go)
                            go.transform:GetChild(0).localPosition = Vector3(offset[0], offset[1], offset[2])
                        end
                    ))
                    obj.RO:AddFX(fxId, fx, -1)
                    self.IsHeadFxLoaded[engineId] = true
                end
            end
        end
    end
end

function LuaYuanXiaoDefenseMgr:OnClientObjDestroy(args)
    if not self:IsInYuanXiaoDefense() then return end
    local engineId = args[0]
    self.IsHeadFxLoaded[engineId] = false
    local obj = CClientObjectMgr.Inst:GetObject(engineId) 
    --local submitNpc = YuanXiao2023_NaoYuanXiao.GetData().SubmitNpc
    --if obj and obj.ObjectType == EnumObjectType.NPC and obj.TemplateId == submitNpc[0] and not self.SubmitNpcRO then
    --    self.SubmitNpcRO = self:CreateLocalNpc(submitNpc)
    --end
end

function LuaYuanXiaoDefenseMgr:IsInYuanXiaoDefense()
    local gamePlayId = CScene.MainScene and CScene.MainScene.GamePlayDesignId
    return YuanXiao2023_NaoYuanXiao.GetData().GameplayId == gamePlayId
end

function LuaYuanXiaoDefenseMgr:NaoYuanXiaoPlayResult(teamScore, todayTotalScore, playerScoreInfo_U, remainNumTbl_U, totalRewardNumTbl_U, rewardItems_U)
    LuaYuanXiaoDefenseResultWnd.s_TeamScore = teamScore

    LuaYuanXiaoDefenseResultWnd.s_PlayerScore = {}
    local ps = g_MessagePack.unpack(playerScoreInfo_U)
    for i = 1, #ps, 3 do
        table.insert(LuaYuanXiaoDefenseResultWnd.s_PlayerScore, { 
            name = ps[i],
            score = ps[i+1], 
            --percent = teamScore == 0 and 0 or ps[i+1] / teamScore
        }) 
    end

    LuaYuanXiaoDefenseResultWnd.s_RewardItems = {}
    local ri = g_MessagePack.unpack(rewardItems_U)
    for i = 1, #ri, 2 do
        table.insert(LuaYuanXiaoDefenseResultWnd.s_RewardItems, ri[i])
    end
    LuaYuanXiaoDefenseResultWnd.s_RemainRewardTimes = g_MessagePack.unpack(remainNumTbl_U)
    CUIManager.ShowUI(CLuaUIResources.YuanXiaoDefenseResultWnd)
end

function LuaYuanXiaoDefenseMgr:SyncNaoYuanXiaoTeamPlayInfo(teamScore, playerScoreInfo_U)
    self.TeamScore = teamScore
    self.PlayerScore = {}
    local ps = g_MessagePack.unpack(playerScoreInfo_U)
    local dict = {}
    local rank = 1
    for i = 1, #ps, 3 do
        table.insert(self.PlayerScore, {name = ps[i], score = ps[i+1], playerId = ps[i+2]})
        if rank <= 3 then
            dict[ps[i+2]] = {
                spriteName = "headinfownd_jiashang_0"..rank,
            }
        end
        rank = rank + 1
    end
    LuaGamePlayMgr:SetPlayerHeadSpeIconInfos(dict, false, true)
    g_ScriptEvent:BroadcastInLua("SyncNaoYuanXiaoTeamPlayInfo", self.TeamScore, self.PlayerScore)
end

function LuaYuanXiaoDefenseMgr:SyncNaoYuanXiaoGridStatus(gridInfo_U, gridTextureInfo_U) --进场景先发+格子变化时发
    self.GridStatus = {}
    local badGrids = g_MessagePack.unpack(gridInfo_U) 
    for i = 1, self.Width do
        for j = 1, self.Height do
            self.GridStatus[self:GetGridIndex(i, j)] = 0
        end
    end
    for i = 1, #badGrids, 2 do
		local x, y = badGrids[i], badGrids[i + 1]
        local idx = self:GetGridIndex(x, y)
		self.GridStatus[idx] = 1

        if self.FxDict[idx] then
            UnRegisterTick(self.FxDict[idx].tick)
            if not CommonDefs.IsNull(self.FxDict[idx].fx) then
                self.FxDict[idx].fx:GetFX():Destroy()
            end
            self.FxDict[idx] = nil
        end
	end

    self.GridTex = {}
    local gridTexture = g_MessagePack.unpack(gridTextureInfo_U) 
    for i = 1, #gridTexture, 3 do
		local x, y, tex = gridTexture[i], gridTexture[i + 1], gridTexture[i + 2]
		self.GridTex[self:GetGridIndex(x, y)] = tex
	end

    self.GridRes = {
        CRenderScene.Inst.transform:Find("Models/dj_2023yuanxiao_001_001_a").gameObject,
        CRenderScene.Inst.transform:Find("Models/dj_2023yuanxiao_001_001_b").gameObject,
        CRenderScene.Inst.transform:Find("Models/dj_2023yuanxiao_001_001_c").gameObject,
        CRenderScene.Inst.transform:Find("Models/dj_2023yuanxiao_001_001_d").gameObject,
    }
    for i = 1, #self.GridRes do self.GridRes[i]:SetActive(false) end
end

function LuaYuanXiaoDefenseMgr:NaoYuanXiaoGridStatus(gridDropStatus_U) --进场景后发+格子掉下去时发
    local gridDropStatus = g_MessagePack.unpack(gridDropStatus_U)
    for i = 1, #gridDropStatus, 3 do
		local x, y, status = gridDropStatus[i], gridDropStatus[i + 1], gridDropStatus[i + 2]
        status = status and 2 or 1
        local idx = self:GetGridIndex(x, y)
        if self.IsInited then
            if status == 1 then -- 没掉
                if self.GridTweeners[idx] then 
                    LuaTweenUtils.Kill(self.GridTweeners[idx], false) 
                    self.GridTweeners[idx] = nil
                end
                LuaUtils.SetLocalPositionY(self.GridObjs[idx].transform, self.Origin.y)
            elseif status == 2 and self.GridStatus[idx] ~= status then -- 掉了
                if self.GridTweeners[idx] then 
                    LuaTweenUtils.Kill(self.GridTweeners[idx], false)
                end
                self.GridTweeners[idx] = LuaTweenUtils.SetEase(LuaTweenUtils.TweenPositionY(self.GridObjs[idx].transform, 0, 3), Ease.InQuad)
            end
        end
        if self.GridStatus[idx] ~= 0 then
		    self.GridStatus[idx] = status
        end
	end

    self:InitGridObjs()
end

function LuaYuanXiaoDefenseMgr:NaoYuanXiaoPlayerCollisionOthers(playerId, dir) -- dir:0~360(逆时针)
    local positiveDir = {Vector3(1, 0, 0),Vector3(0, 0, -1)} -- AxisX and AxisY
    local player = CClientPlayerMgr.Inst:GetPlayer(playerId)
    if player then
        local forward = player.RO.transform.forward
        local angle = Vector3.Angle(positiveDir[1], forward)
        if Vector3.Angle(positiveDir[2], forward) < 90 then
            angle = 360 - angle
        end
        angle = dir - angle
        if math.abs(angle) < 30 then --前倾
            player:ShowExpressionActionState(47001095)
        elseif math.abs(angle) < 150 then --左右
            if angle > 0 then
                player:ShowExpressionActionState(47001097) -- 左倾
            else
                player:ShowExpressionActionState(47001096) -- 右倾
            end
        else -- 后仰
            player:ShowExpressionActionState(47001094)
        end
    end
end

function LuaYuanXiaoDefenseMgr:NaoYuanXiaoReturnStartPos(playerId)
    local obj = CClientPlayerMgr.Inst:GetPlayer(playerId)
    if obj then
        obj:ShowExpressionActionState(0)
    end
end

function LuaYuanXiaoDefenseMgr:ShowWorldPositionFx(fxId, x, z)
    local idx = self:GetGridIndex(x, z)
    if self.GridStatus[idx] == 0 then
        local pos =  Utility.GridPos2PixelPos(CPos(self.Origin.x + (x - 1) * 3 + 1, self.Origin.z - (z - 1) * 3 + 1))
        if not self.FxDict[idx] or CommonDefs.IsNull(self.FxDict[idx].fx) or CommonDefs.IsNull(self.FxDict[idx].effect) then
            CEffectMgr.Inst:AddWorldPositionFX(fxId,pos,0,0,1,-1,EnumWarnFXType.None,nil,0,0,DelegateFactory.Action_GameObject(
                function(fxGo) 
                    if self.FxDict[idx] then
                        UnRegisterTick(self.FxDict[idx].tick)
                        if not CommonDefs.IsNull(self.FxDict[idx].fx) then
                            self.FxDict[idx].fx:GetFX():Destroy()
                        end
                    end
                    self.FxDict[idx] = { 
                        fx = fxGo:GetComponent(typeof(CWorldPositionFX)),
                        effect = fxGo.transform:GetChild(0):GetComponent(typeof(CParticleEffect)),
                        tick = RegisterTickOnce(function()
                            if not CommonDefs.IsNull(self.FxDict[idx].fx) then
                                self.FxDict[idx].fx.gameObject:SetActive(false)
                            end
                        end, YuanXiao2023_NaoYuanXiao.GetData().FxDuration * 1000)
                    }
                    UnRegisterTick(self.FxDict[idx].fx.m_Tick)
                    self.FxDict[idx].effect.m_ParticleSystem:Simulate(1)
                    self.FxDict[idx].fx:SetPaused(true)
                end)
            )
        else
            if not self.FxDict[idx].fx.gameObject.activeSelf then
                self.FxDict[idx].fx.gameObject:SetActive(true)
                self.FxDict[idx].fx:Play()
                UnRegisterTick(self.FxDict[idx].fx.m_Tick)
                self.FxDict[idx].effect.m_ParticleSystem:Simulate(1)
                self.FxDict[idx].fx:SetPaused(true)
            end
            UnRegisterTick(self.FxDict[idx].tick)
            self.FxDict[idx].tick = RegisterTickOnce(function()
                if not CommonDefs.IsNull(self.FxDict[idx].fx) then
                    self.FxDict[idx].fx.gameObject:SetActive(false)
                end
            end, YuanXiao2023_NaoYuanXiao.GetData().FxDuration * 1000)
        end
    end
end

function LuaYuanXiaoDefenseMgr:NaoYuanXiaoPlayersGridPos(playersGrid_U) 
    local grids = g_MessagePack.unpack(playersGrid_U)
    local fxId = YuanXiao2023_NaoYuanXiao.GetData().WorldPositionFx
    for i = 1, #grids, 2 do
        local x, z = grids[i], grids[i+1]
        self:ShowWorldPositionFx(fxId, x, z)
    end
end

function LuaYuanXiaoDefenseMgr:NaoYuanXiaoPlayerOnGoodGrid(x, z)
    if x < 1 or x > self.Width or z < 1 or z > self.Height then
        return
    end
    self:ShowWorldPositionFx(YuanXiao2023_NaoYuanXiao.GetData().WorldPositionFx, x, z)
end

function LuaYuanXiaoDefenseMgr:NaoYuanXiaoPlayerOnBadGrid(playerId)
    if CClientPlayerMgr.Inst ~= nil then
        local obj = CClientPlayerMgr.Inst:GetPlayer(playerId)
        if obj and not CommonDefs.IsNull(obj.RO) then
            obj.RO:DoAni("fall01_start", false, 0, 1, 0.15, true, 1)
            RegisterTickOnce(function()
                local obj = CClientPlayerMgr.Inst:GetPlayer(playerId)
                if obj then
                    obj.RO:DoAni("fall01_loop", false, 0, 1, 0.15, true, 1)
                end
            end, 1100)
        end
    end
end

function LuaYuanXiaoDefenseMgr:ReturnNaoYuanXiaoScoreQuery(todayTotalSwwwcore)
    print(todayTotalSwwwcore)
end

function LuaYuanXiaoDefenseMgr:ReturnNaoYuanXiaoRemainRewardTimes(todayRemainRewardTimes_U)
    g_ScriptEvent:BroadcastInLua("ReturnNaoYuanXiaoRemainRewardTimes", g_MessagePack.unpack(todayRemainRewardTimes_U))
end

function LuaYuanXiaoDefenseMgr:NaoYuanXiaoCommitFx()
    g_ScriptEvent:BroadcastInLua("NaoYuanXiaoCommitFx")
end

function LuaYuanXiaoDefenseMgr:NaoYuanXiaoShowUI()
    CUIManager.ShowUI(CLuaUIResources.YuanXiaoDefenseTopRightWnd)
	local list = CreateFromClass(MakeGenericClass(List, UInt32))
    CommonDefs.ListAdd(list, typeof(UInt32), 980431 * 100 + 1)
	CommonDefs.ListAdd(list, typeof(UInt32), 980432 * 100 + 1)
    CommonDefs.ListAdd(list, typeof(UInt32), 980480 * 100 + 1)
	CommonDefs.ListAdd(list, typeof(UInt32), 980481 * 100 + 1)
    CSkillAcquisitionMgr.ShowTempSkillsTip(list)
end
