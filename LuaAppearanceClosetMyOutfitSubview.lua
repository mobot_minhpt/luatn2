local CUITexture = import "L10.UI.CUITexture"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local UISprite = import "UISprite"
local CInputBoxMgr = import "L10.UI.CInputBoxMgr"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"

LuaAppearanceClosetMyOutfitSubview = class()

RegistChildComponent(LuaAppearanceClosetMyOutfitSubview,"m_ClosetOutfitItem", "ClosetOutfitItem", GameObject)
RegistChildComponent(LuaAppearanceClosetMyOutfitSubview,"m_ContentTable", "ContentTable", UITable)
RegistChildComponent(LuaAppearanceClosetMyOutfitSubview,"m_ContentGrid", "ContentGrid", UIGrid)
RegistChildComponent(LuaAppearanceClosetMyOutfitSubview,"m_ContentScrollView", "ContentScrollView", UIScrollView)
RegistChildComponent(LuaAppearanceClosetMyOutfitSubview,"m_SaveButton", "SaveButton", GameObject)
RegistChildComponent(LuaAppearanceClosetMyOutfitSubview,"m_ApplyButton", "ApplyButton", GameObject)

RegistClassMember(LuaAppearanceClosetMyOutfitSubview, "m_MyOutfits")
RegistClassMember(LuaAppearanceClosetMyOutfitSubview, "m_SelectedDataId")

function LuaAppearanceClosetMyOutfitSubview:Awake()
    UIEventListener.Get(self.m_SaveButton).onClick = DelegateFactory.VoidDelegate(function(go) self:OnSaveButtonClick() end)
    UIEventListener.Get(self.m_ApplyButton).onClick = DelegateFactory.VoidDelegate(function(go) self:OnApplyButtonClick() end)
end

function LuaAppearanceClosetMyOutfitSubview:Init()
    self.m_SelectedDataId=0
    self:LoadData()
end

function LuaAppearanceClosetMyOutfitSubview:LoadData()
    self.m_MyOutfits = LuaAppearancePreviewMgr:GetAllOutfitInfo()
    Extensions.RemoveAllChildren(self.m_ContentTable.transform)
    self.m_ContentTable.gameObject:SetActive(true)
    self.m_ContentGrid.gameObject:SetActive(false)

    for i = 1, #self.m_MyOutfits do
        local child = CUICommonDef.AddChild(self.m_ContentTable.gameObject, self.m_ClosetOutfitItem)
        child:SetActive(true)
        self:InitItem(child, self.m_MyOutfits[i])
    end
   
    self.m_ContentTable:Reposition()
    self.m_ContentScrollView:ResetPosition()
    self:InitSelection()
end

function LuaAppearanceClosetMyOutfitSubview:InitSelection()
    if self.m_SelectedDataId == 0 then
        self:OnItemClick(nil)
        return
    end

    local childCount = self.m_ContentTable.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentTable.transform:GetChild(i).gameObject
        local outfitData = self.m_MyOutfits[i+1]
        if outfitData.id == self.m_SelectedDataId then
            self:OnItemClick(childGo)
            break
        end
    end
end

function LuaAppearanceClosetMyOutfitSubview:InitItem(itemGo, outfitData)
    local nameLabel = itemGo.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local root = itemGo.transform:Find("Table")
    local changeNameButton = itemGo.transform:Find("ChangeNameButton").gameObject
    nameLabel.text = outfitData.name
    
    local n = root.childCount
    for i=0, n-1 do
        local child = root:GetChild(i)
        local iconTexture = child:Find("Icon"):GetComponent(typeof(CUITexture))
        local expiredGo = child:Find("DisableSprite").gameObject
        iconTexture:LoadMaterial(outfitData.icons[i+1])
        expiredGo:SetActive(not LuaAppearancePreviewMgr:IsMainPlayerFashionAvailable(outfitData.fashions[i+1]))
        UIEventListener.Get(child.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnItemClick(itemGo) --选中整个条目
            -- tip功能暂时屏蔽，将来看需求是否开放
            -- if outfitData.itemIds[i+1]>0 then
            --     CItemInfoMgr.ShowLinkItemTemplateInfo(outfitData.itemIds[i+1], false, nil, AlignType.Default, 0, 0, 0, 0)
            -- end
        end)
    end
    UIEventListener.Get(changeNameButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnChangeNameButtonClick(outfitData.id)
    end)

    UIEventListener.Get(itemGo).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnItemClick(go)
    end)
end

function LuaAppearanceClosetMyOutfitSubview:OnChangeNameButtonClick(outfitId)
    CInputBoxMgr.ShowInputBox(LocalString.GetString("请输入搭配模板名称，最多输入7个字"), DelegateFactory.Action_string(function (val)
        local ret = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(val, true)
        if (not ret.msg) or ret.shouldBeIgnore then return end
        if System.String.IsNullOrEmpty(ret.msg) then
            g_MessageMgr:ShowMessage("Content_Cannot_Be_Empty")
            return
        end
        LuaAppearancePreviewMgr:RequestModifyFashionDaPeiName_Permanent(outfitId, ret.msg)
      end), 14, true, nil, LocalString.GetString("点此输入名称"))
end

function LuaAppearanceClosetMyOutfitSubview:OnItemClick(go)
    local childCount = self.m_ContentTable.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentTable.transform:GetChild(i).gameObject
        if childGo == go then
            childGo.transform:Find("Selected").gameObject:SetActive(true)
            self.m_SelectedDataId = self.m_MyOutfits[i+1].id
            self:PreviewOutfit(self.m_MyOutfits[i+1])
        else
            childGo.transform:Find("Selected").gameObject:SetActive(false)
        end
    end

    if self.m_SelectedDataId>0 then
        self.m_SaveButton:SetActive(true)
        self.m_ApplyButton:SetActive(true)
    else
        self.m_SaveButton:SetActive(false)
        self.m_ApplyButton:SetActive(false)
    end
end

function LuaAppearanceClosetMyOutfitSubview:PreviewOutfit(outfitData)
    local headId = 0
    local bodyId = 0
    local weaponId = 0
    local backPendantId = 0
    
    if LuaAppearancePreviewMgr:IsMainPlayerFashionAvailable(outfitData.fashions[1]) or
        LuaAppearancePreviewMgr:IsMainPlayerFashionAvailable(outfitData.fashions[2]) or
        LuaAppearancePreviewMgr:IsMainPlayerFashionAvailable(outfitData.fashions[3]) or
        LuaAppearancePreviewMgr:IsMainPlayerFashionAvailable(outfitData.fashions[4]) then
            headId = outfitData.fashions[1]
            bodyId = outfitData.fashions[2]
            weaponId = outfitData.fashions[3]
            backPendantId = outfitData.fashions[4]
    end

    g_ScriptEvent:BroadcastInLua("RequestPreviewMainPlayerFashionOutfit", headId,bodyId,weaponId,backPendantId)
end

function LuaAppearanceClosetMyOutfitSubview:OnSaveButtonClick()
    if self.m_SelectedDataId>0 then
        LuaAppearancePreviewMgr:RequestSaveFashionDaPeiContent_Permanent(self.m_SelectedDataId)
    end
end

function LuaAppearanceClosetMyOutfitSubview:OnApplyButtonClick()
    if self.m_SelectedDataId>0 then
        LuaAppearancePreviewMgr:RequestUseFashionDaPei_Permanent(self.m_SelectedDataId)
    end
end

function  LuaAppearanceClosetMyOutfitSubview:OnEnable()
    g_ScriptEvent:AddListener("OnFashionDaPeiInfoUpdate",self,"OnFashionDaPeiInfoUpdate")
end

function  LuaAppearanceClosetMyOutfitSubview:OnDisable()
    g_ScriptEvent:RemoveListener("OnFashionDaPeiInfoUpdate",self,"OnFashionDaPeiInfoUpdate")
end

function LuaAppearanceClosetMyOutfitSubview:OnFashionDaPeiInfoUpdate()
    self:LoadData()
end
