local MessageWndManager = import "L10.UI.MessageWndManager"
-- local CChuanjiabaoInfoWindow=import "L10.UI.CChuanjiabaoInfoWindow"
local CClientHouseMgr=import "L10.Game.CClientHouseMgr"
local CCJBMgr=import "L10.Game.CCJBMgr"
local CItemMgr=import "L10.Game.CItemMgr"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local EChuanjiabaoType = import "L10.Game.EChuanjiabaoType"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CClientChuanJiaBaoMgr = import "L10.Game.CClientChuanJiaBaoMgr"
local CCommonLuaScript=import "L10.UI.CCommonLuaScript"


CLuaChuanJiaBaoWnd = class()
RegistClassMember(CLuaChuanJiaBaoWnd,"currentShelf")
RegistClassMember(CLuaChuanJiaBaoWnd,"shelfLabel")
RegistClassMember(CLuaChuanJiaBaoWnd,"qifuButton")
RegistClassMember(CLuaChuanJiaBaoWnd,"qifuTips")
RegistClassMember(CLuaChuanJiaBaoWnd,"panciSprite")
RegistClassMember(CLuaChuanJiaBaoWnd,"panciBg")
RegistClassMember(CLuaChuanJiaBaoWnd,"shelves")
RegistClassMember(CLuaChuanJiaBaoWnd,"shelfWindow")
RegistClassMember(CLuaChuanJiaBaoWnd,"detailWindow")
RegistClassMember(CLuaChuanJiaBaoWnd,"detailWindowBg")
RegistClassMember(CLuaChuanJiaBaoWnd,"detailScrollviewBg")
RegistClassMember(CLuaChuanJiaBaoWnd,"shaozhiButton")
RegistClassMember(CLuaChuanJiaBaoWnd,"shareButton")
RegistClassMember(CLuaChuanJiaBaoWnd,"selectIndex")

RegistClassMember(CLuaChuanJiaBaoWnd,"m_DaZaoButton")
RegistClassMember(CLuaChuanJiaBaoWnd,"m_ZhenZhaiYuBtn")

function CLuaChuanJiaBaoWnd:Awake()
    self.selectIndex=0
    self.currentShelf = self.transform:Find("ShelfWindow/CurrentShelf").gameObject
    self.shelfLabel = self.transform:Find("ShelfWindow/CurrentShelf/Label"):GetComponent(typeof(UILabel))
    self.qifuButton = self.transform:Find("ShelfWindow/Qiyuan").gameObject
    self.qifuTips = self.transform:Find("ShelfWindow/Qiyuan/TodayInfo/Label"):GetComponent(typeof(UILabel))
    self.panciSprite = self.transform:Find("ShelfWindow/Qiyuan/TodayInfo/Panci"):GetComponent(typeof(UISprite))
    self.panciBg = self.transform:Find("ShelfWindow/Qiyuan/TodayInfo"):GetComponent(typeof(UISprite))
    self.m_ZhenZhaiYuBtn = self.transform:Find("ZhenZhaiYuBtn"):GetComponent(typeof(CUIFx))
    self.m_ZhenZhaiYuBtn:LoadFx("fx/ui/prefab/UI_zhenzhaiyurukou.prefab")
    self.m_ZhenZhaiYuBtn.gameObject:SetActive(LuaSeaFishingMgr.s_SeaFishingSwith)
    self.shelves = {}
    local tf = self.transform:Find("Shelf")
    local script = tf:Find("Tian"):GetComponent(typeof(CCommonLuaScript))
    script:Awake()
    self.shelves[1] = script.m_LuaSelf
    self.shelves[1].type = EChuanjiabaoType.TianGe
    local script = tf:Find("Di"):GetComponent(typeof(CCommonLuaScript))
    script:Awake()
    self.shelves[2] = script.m_LuaSelf
    self.shelves[2].type = EChuanjiabaoType.DiGe
    local script = tf:Find("Ren"):GetComponent(typeof(CCommonLuaScript))
    script:Awake()
    self.shelves[3] = script.m_LuaSelf
    self.shelves[3].type = EChuanjiabaoType.RenGe

    self.shelfWindow = self.transform:Find("ShelfWindow").gameObject
    local script = self.transform:Find("ChuanjiabaoInfoWindow"):GetComponent(typeof(CCommonLuaScript))
    script:Awake()
    self.detailWindow = script.m_LuaSelf
    
    self.detailWindowBg = self.transform:Find("ChuanjiabaoInfoWindow/Container").gameObject
    self.detailScrollviewBg = self.transform:Find("ChuanjiabaoInfoWindow/Info/Tips/Container").gameObject
    self.shaozhiButton = self.transform:Find("ShelfWindow/Shaozhi").gameObject
    self.shareButton = self.transform:Find("ChuanjiabaoInfoWindow/ShareButton").gameObject

    self.m_DaZaoButton = self.transform:Find("ChuanjiabaoInfoWindow/DaZaoButton").gameObject
    UIEventListener.Get(self.m_DaZaoButton).onClick = DelegateFactory.VoidDelegate(function(go)
        --如果大于6星则可以打造
        local chuanjiabaoId =  self.shelves[self.selectIndex+1].chuanjiabaoId
        local commonItem = CItemMgr.Inst:GetById(chuanjiabaoId)
        if commonItem then
            local data = commonItem.Item.ChuanjiabaoItemInfo
            if CLuaZhuShaBiXiLianMgr.CanXiLian(data.WordInfo) then
                if CClientHouseMgr.Inst:IsInOwnHouse() then
                    CLuaZhuShaBiProcessWnd.m_SelectedItemId = self.shelves[self.selectIndex+1].chuanjiabaoId
                    CUIManager.ShowUI(CLuaUIResources.ZhuShaBiProcessWnd)
                else
                    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("OPERATION_REQUEST_GO_HOME"), DelegateFactory.Action(function () 
                        Gac2Gas.RequestEnterHome()
                    end), nil, nil, nil, false)
                end
            else
                g_MessageMgr:ShowMessage("WASH_CHUANJIABAO_LIMIT")
            end
        end
    end)

    UIEventListener.Get(self.qifuButton).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CUIResources.QiYuanWnd)  
    end)
    
    UIEventListener.Get(self.shaozhiButton).onClick = DelegateFactory.VoidDelegate(function(go)
        CCJBMgr.Inst:StartMakeCJB()
    end)
    
    UIEventListener.Get(self.currentShelf).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnCurrentShelfClick(go)
    end)
    
    UIEventListener.Get(self.detailWindowBg).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnDetailWindowBgClick(go)
    end)
    
    UIEventListener.Get(self.detailScrollviewBg).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnDetailWindowBgClick(go)
    end)
    
    UIEventListener.Get(self.shareButton).onClick = DelegateFactory.VoidDelegate(function(go)
        if self.selectIndex >= 0 and self.selectIndex < #self.shelves then
            local showData = self.shelves[self.selectIndex+1]:GetShowData()
            if showData then
                showData:ShareCJB()
            end
        end
    end)

    UIEventListener.Get(self.m_ZhenZhaiYuBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CLuaUIResources.ZhenZhaiYuWnd)
    end)    
end

function CLuaChuanJiaBaoWnd:OnEnable( )
    g_ScriptEvent:AddListener("QueryChuanjiabaoInHouseResult",self,"QueryChuanjiabaoResultReturn")
    g_ScriptEvent:AddListener("OnQueryPlayerPrayInfoResult",self,"QueryChuanjiabaoResultReturn")

    Gac2Gas.QueryChuanjiabaoInHouse()
    Gac2Gas.QueryPlayerPrayInfo()
    CUIManager.ModelLightEnabled = false
end
function CLuaChuanJiaBaoWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("QueryChuanjiabaoInHouseResult",self,"QueryChuanjiabaoResultReturn")
    g_ScriptEvent:RemoveListener("OnQueryPlayerPrayInfoResult",self,"QueryChuanjiabaoResultReturn")

    CUIManager.CloseUI(CUIResources.ChuanjiabaoSelectWnd)
    CUIManager.ModelLightEnabled = true
end
function CLuaChuanJiaBaoWnd:QueryChuanjiabaoResultReturn( )
    self.shelfWindow:SetActive(true)
    self.detailWindow.gameObject:SetActive(false)
    self:OnDetailWindowBgClick(nil)
    self:InitQifu()
    self:InitShelf()
end

function CLuaChuanJiaBaoWnd:OnCurrentShelfClick( go) 
    local data = CreateFromClass(MakeArrayClass(PopupMenuItemData), 2)

    local callback = DelegateFactory.Action_int(function(index)
        self:OnCurrentShelfChange(index)
    end)

    data[0] = PopupMenuItemData(LocalString.GetString("博古架一"), callback, false, nil)
    data[1] = PopupMenuItemData(LocalString.GetString("博古架二"), callback, false, nil)
    CPopupMenuInfoMgr.ShowPopupMenu(data, go.transform.position, CPopupMenuInfoMgr.AlignType.Bottom)
end
function CLuaChuanJiaBaoWnd:OnCurrentShelfChange( index) 
    if CClientChuanJiaBaoMgr.Inst.maxChuanjiabaoNum == 1 and index == 1 then
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("你没有开启博古架二"))
    else
        Gac2Gas.SetActiveChuanjiabaoSuite(index + 1)
    end
end
function CLuaChuanJiaBaoWnd:InitShelf( )
    self.shelfLabel.text = CClientChuanJiaBaoMgr.Inst.currentSelectSuite == 1 and LocalString.GetString("博古架一") or LocalString.GetString("博古架二")
    local ids = CClientChuanJiaBaoMgr.Inst.currentChuanjiabaoIds
    for i = 1, 3 do
        self.shelves[i]:CleanShowNode()
        local index = math.floor(EnumToInt(self.shelves[i].type) / 100)
        self.shelves[i]:Init(ids[index])
        self.shelves[i].OnCheckChuanjiabaoInfo = function(type)
            self:CheckChuanjiabaoInfo(type)
        end
        self.shelves[i]:AddListener()
    end
end
function CLuaChuanJiaBaoWnd:InitQifu( )
    local oldPrayResult = CClientChuanJiaBaoMgr.Inst.oldPrayResult
    local txt = (oldPrayResult==nil or oldPrayResult=="") and LocalString.GetString("点击开始祈愿") or LocalString.GetString("当前祈愿")
    self.qifuTips.text = LocalString.StrH2V(txt,false)

    if oldPrayResult and oldPrayResult~="" then
        self.panciSprite.gameObject:SetActive(true)
        self.panciSprite.spriteName = CClientChuanJiaBaoMgr.Inst:GetPanciIcon(CClientChuanJiaBaoMgr.Inst.oldPrayResult)
        self.panciSprite:MakePixelPerfect()
        if not CommonDefs.IS_VN_CLIENT then
            self.panciBg.height = self.qifuTips.height + self.panciSprite.height + 120
        end
    else
        self.panciSprite.gameObject:SetActive(false)
        if not CommonDefs.IS_VN_CLIENT then
            self.panciBg.height = self.qifuTips.height + 120
        end
    end
end
function CLuaChuanJiaBaoWnd:CheckChuanjiabaoInfo( type) 
    self.shelfWindow:SetActive(false)
    for i,v in ipairs(self.shelves) do
        v.gameObject:SetActive(v.type == type)
        if v.type == type then
            self.selectIndex = i-1
            v.tPos:PlayForward()
            v.tScale:PlayForward()
            v:RemoveListener()
        end
    end

    self.detailWindow.gameObject:SetActive(true)
    local chuanjiabaoId =  self.shelves[self.selectIndex+1].chuanjiabaoId
    self.detailWindow:Init(chuanjiabaoId, nil)
end
function CLuaChuanJiaBaoWnd:OnDetailWindowBgClick( go) 
    self.detailWindow.gameObject:SetActive(false)
    self.shelfWindow:SetActive(true)
    for i,v in ipairs(self.shelves) do
        v.gameObject:SetActive(true)
        if i == self.selectIndex+1 then
            v.tPos:PlayReverse()
            v.tScale:PlayReverse()
            v:AddListener()
        end
    end
end

