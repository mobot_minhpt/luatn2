-- Auto Generated!!
local CGiftTemplate = import "L10.UI.CGiftTemplate"
local CUICommonDef = import "L10.UI.CUICommonDef"
local GiftRecieveStatus = import "L10.UI.GiftRecieveStatus"
CGiftTemplate.m_UpdateGiftInfo_CS2LuaHook = function (this, iconpath, quality, count, status)
    this.m_Texture:LoadMaterial(iconpath)
    this.m_Label.text = tostring(count)
    this.m_HasRecievedLabel.gameObject:SetActive(status == GiftRecieveStatus.Recieved)
    this.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(quality)
end
