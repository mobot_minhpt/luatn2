local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"

LuaJuDianBattleFeildInfoRoot = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaJuDianBattleFeildInfoRoot, "TableView", "TableView", QnAdvanceGridView)
RegistChildComponent(LuaJuDianBattleFeildInfoRoot, "MainPlayerInfo", "MainPlayerInfo", GameObject)
RegistChildComponent(LuaJuDianBattleFeildInfoRoot, "Top", "Top", GameObject)
RegistChildComponent(LuaJuDianBattleFeildInfoRoot, "ZhuDianBtn", "ZhuDianBtn", GameObject)
RegistChildComponent(LuaJuDianBattleFeildInfoRoot, "BenFuBtn", "BenFuBtn", GameObject)
RegistChildComponent(LuaJuDianBattleFeildInfoRoot, "WorldMapBtn", "WorldMapBtn", GameObject)

--@endregion RegistChildComponent end

function LuaJuDianBattleFeildInfoRoot:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ZhuDianBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnZhuDianBtnClick()
	end)


	
	UIEventListener.Get(self.BenFuBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnBenFuBtnClick()
	end)


	
	UIEventListener.Get(self.WorldMapBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnWorldMapBtnClick()
	end)


    --@endregion EventBind end
end

function LuaJuDianBattleFeildInfoRoot:InitWnd()
    self.m_DataList = {}

	self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_DataList
        end,

        function(item,index)
            item:SetBackgroundTexture(index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)
            self:InitItem(item, index, self.m_DataList[index+1])
        end
    )
end

function LuaJuDianBattleFeildInfoRoot:InitItem(item, index, data)
	local guild = item.transform:Find("GuildNameLabel"):GetComponent(typeof(UILabel))
	local server = item.transform:Find("ServerNameLabel"):GetComponent(typeof(UILabel))
	local people = item.transform:Find("PeopleLabel"):GetComponent(typeof(UILabel))
	local kill = item.transform:Find("KillLabel"):GetComponent(typeof(UILabel))
	local occupy = item.transform:Find("OccupyLabel"):GetComponent(typeof(UILabel))
	local rankSp = item.transform:Find("RankImage"):GetComponent(typeof(UISprite))
	local rankLabel = item.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
	local judianLabel = item.transform:Find("JuDianCount"):GetComponent(typeof(UILabel))
	local flagLabel = item.transform:Find("FlagCount"):GetComponent(typeof(UILabel))
	local infoBtn = item.transform:Find("Btn").gameObject

    guild.text = data.guildName
    server.text = data.serverName
    people.text = data.playerCount
    kill.text = data.kill
    occupy.text = data.score
    judianLabel.text = data.juDianCount
    flagLabel.text = data.flagCount

    UIEventListener.Get(infoBtn).onClick = DelegateFactory.VoidDelegate(function (go)
	    if data.juDianCount == 0 and data.flagCount == 0 then
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("该帮会暂无占领信息"))
        else
            LuaJuDianBattleMgr:OpenGuildDetailWnd(data.guildId, data.guildName)
        end
	end)

    local rank = data.rankPos

    local rankImgList = {"Rank_No.1", "Rank_No.2png", "Rank_No.3png"}

    rankLabel.gameObject:SetActive(true)
    if rank <= 3 then
        rankLabel.text = ""
        rankSp.gameObject:SetActive(true)
        rankSp.spriteName = rankImgList[rank]
    else
        rankLabel.text = tostring(rank)
        rankSp.gameObject:SetActive(false)
    end
end

function LuaJuDianBattleFeildInfoRoot:OnGuildData(bShowFx, topList, guildList)
    print("OnGuildData", bShowFx, topList, #guildList)
    for i = EnumGuildJuDianTopType.Kill, EnumGuildJuDianTopType.Heal do
        local item = self.Top.transform:Find(tostring(i)).gameObject
        self:InitTopItem(item, topList[i])
    end

    self.m_DataList = guildList
    -- 刷新数据
	self.TableView:ReloadData(true,false)

    local selfGuildId = 0
    if CClientMainPlayer.Inst then
        selfGuildId = CClientMainPlayer.Inst.BasicProp.GuildId
    end

    -- 设置本帮数据
    for i = 1, #guildList do
        local data = guildList[i]
        if selfGuildId == data.guildId then
            self.MainPlayerInfo:SetActive(true)
            self:InitItem(self.MainPlayerInfo, 0, data)

            -- 弹出排名
            if bShowFx and data.rankPos <=8 then
                LuaUtils.RankNumWndValue = data.rankPos
                CUIManager.ShowUI(CUIResources.RankNumWnd)
            end
        end
    end
end

function LuaJuDianBattleFeildInfoRoot:InitTopItem(item, topData)
    local count = item.transform:Find("Count"):GetComponent(typeof(UILabel))
    local player = item.transform:Find("Player"):GetComponent(typeof(UILabel))
    local guild = item.transform:Find("Guild"):GetComponent(typeof(UILabel))
    local guild = item.transform:Find("Guild"):GetComponent(typeof(UILabel))
    local texture = item.transform:Find("Icon/Texture"):GetComponent(typeof(UITexture))
    local ctexture = item.transform:Find("Icon/Texture"):GetComponent(typeof(CUITexture))

    if topData then
        player.text = topData.topPlayerName

        local countStr = tostring(topData.topValue)
        if topData.topValue > 10000 then
            countStr = math.floor(topData.topValue/10000) .. LocalString.GetString("万")
        end

        count.text = countStr
        guild.text = topData.topGuildName
        texture.color = Color(1,1,1,1)
        player.color = Color(1,1,1,1)
        ctexture:LoadNPCPortrait(CUICommonDef.GetPortraitName(topData.class, topData.gender, -1), false)
    else
        texture.color = Color(1,1,1,0.3)
        player.color = Color(1,1,1,0.3)
        player.text = LocalString.GetString("暂无")
        count.text = 0
        guild.text = LocalString.GetString("暂无")
    end
end

function LuaJuDianBattleFeildInfoRoot:OnEnable()
    self:InitWnd()
    -- 请求前四名帮会
    if LuaJuDianBattleMgr.IsResultShow and LuaJuDianBattleMgr.TopList and LuaJuDianBattleMgr.GuildList then
        self:OnGuildData(true, LuaJuDianBattleMgr.TopList, LuaJuDianBattleMgr.GuildList)
    else
        -- 请求本帮数据
        local selfGuildId = 0
        if CClientMainPlayer.Inst then
            selfGuildId = CClientMainPlayer.Inst.BasicProp.GuildId
            print("Gac2Gas.GuildJuDianQueryBattleFieldRank(selfGuildId)", selfGuildId)
            Gac2Gas.GuildJuDianQueryBattleFieldRank(selfGuildId)
        end
    end

    g_ScriptEvent:AddListener("GuildJuDianSyncBattleFieldRank", self, "OnGuildData")
end

function LuaJuDianBattleFeildInfoRoot:OnDisable()
    LuaJuDianBattleMgr.IsResultShow = false
    g_ScriptEvent:RemoveListener("GuildJuDianSyncBattleFieldRank", self, "OnGuildData")
end

--@region UIEvent

function LuaJuDianBattleFeildInfoRoot:OnZhuDianBtnClick()
    -- 回到驻点
    Gac2Gas.GuildJuDianRequestGoToOccupation()
end

function LuaJuDianBattleFeildInfoRoot:OnBenFuBtnClick()
    -- 回到本服
    LuaJuDianBattleMgr:RequestBackSelfSever()
end

function LuaJuDianBattleFeildInfoRoot:OnWorldMapBtnClick()
    LuaJuDianBattleMgr:OpenWorldMap()
end


--@endregion UIEvent

