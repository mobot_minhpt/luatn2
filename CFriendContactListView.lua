-- Auto Generated!!
local AlignType = import "CPlayerInfoMgr+AlignType"
local Boolean = import "System.Boolean"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CExpertTeamMgr = import "L10.Game.CExpertTeamMgr"
local CFriendContactListView = import "L10.UI.CFriendContactListView"
local CFriendGroupMenuMgr = import "L10.UI.CFriendGroupMenuMgr"
local CFriendListItem = import "L10.UI.CFriendListItem"
local CFriendView = import "L10.UI.CFriendView"
local CIMMgr = import "L10.Game.CIMMgr"
local CJieBaiMgr = import "L10.Game.CJieBaiMgr"
local CJingLingMgr = import "L10.Game.CJingLingMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CShiTuMgr = import "L10.Game.CShiTuMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CWeddingMgr = import "L10.Game.CWeddingMgr"
local DelegateFactory = import "DelegateFactory"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumEventType = import "EnumEventType"
local EnumGmStatus = import "L10.Game.EnumGmStatus"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EventManager = import "EventManager"
local FriendItemData = import "FriendItemData"
local GameSetting_Common_Wapper = import "L10.Game.GameSetting_Common_Wapper"
local GroupInfo = import "L10.UI.CFriendContactListView+GroupInfo"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local Object = import "System.Object"
local RelationshipType = import "L10.Game.RelationshipType"
local String = import "System.String"
local StringBuilder = import "System.Text.StringBuilder"
local UIBasicSprite = import "UIBasicSprite"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local UInt64 = import "System.UInt64"
local Vector3 = import "UnityEngine.Vector3"
local Vector4 = import "UnityEngine.Vector4"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
CFriendContactListView.m_CellForRowAtIndex_CS2LuaHook = function (this, index) 

    if index < 0 and index >= this.allData.Count then
        return nil
    end
    local cellIdentifier = "FriendListItemCell"
    local cell = this.tableView:DequeueReusableCellWithIdentifier(cellIdentifier)
    if cell == nil then
        cell = this.tableView:AllocNewCellWithIdentifier(this.friendItemTemplate, cellIdentifier)
    end

    local item = CommonDefs.GetComponent_GameObject_Type(cell, typeof(CFriendListItem))

    local OpButtonVisible = true
    local data = this.allData[index]
    if data.type ~= CFriendListItem.ItemType.Friend then
        OpButtonVisible = false
    end
    item:Init(data, false, OpButtonVisible)
    this:UpdateInfoText(item, data.playerId)
    if OpButtonVisible then
        item.OnOperationButtonClick = MakeDelegateFromCSFunction(this.OnExtendMenuButtonClick, MakeGenericClass(Action1, UInt64), this)
    end
    if item.FriendItemType == CFriendListItem.ItemType.JingLing then
        item.AlertVisible = CJingLingMgr.Inst.ExistsUnreadMsgs or CExpertTeamMgr.Inst.ExistsNewAnswer
    else
        item.AlertVisible = CIMMgr.Inst:ShowNewMessageAlert(item.PlayerId)
    end
    return cell
end
CFriendContactListView.m_OnRowSelected_CS2LuaHook = function (this, index) 

    local go = this.tableView:GetCellByIndex(index)
    if go ~= nil then
        local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CFriendListItem))
        if this.OnItemClickDelegate ~= nil then
            do
                local i = 0
                while i < this.groups.Count do
                    if i == this.selectedGroupIndex then
                        GenericDelegateInvoke(this.OnItemClickDelegate, Table2ArrayWithCount({this.groups[i].groupId, item}, 2, MakeArrayClass(Object)))
                        break
                    end
                    i = i + 1
                end
            end
        end
        if item.FriendItemType == CFriendListItem.ItemType.JingLing then
            this.tableView.SelectedIndex = - 1
        elseif item.FriendItemType == CFriendListItem.ItemType.CloseFriend then
            this.tableView.SelectedIndex = - 1
        end
    end
end
CFriendContactListView.m_Start_CS2LuaHook = function (this) 
    UIEventListener.Get(this.curGroupBtn.gameObject).onClick = MakeDelegateFromCSFunction(this.OnCurrentGroupButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.editGroupBtn.gameObject).onClick = MakeDelegateFromCSFunction(this.OnEditGroupButtonClick, VoidDelegate, this)
end
CFriendContactListView.m_OnEnable_CS2LuaHook = function (this) 

    this:Init()
    EventManager.AddListenerInternal(EnumEventType.OnIMMsgReadStatusChanged, MakeDelegateFromCSFunction(this.UpdateFriendListAlert_WithPlayerId, MakeGenericClass(Action1, UInt64), this))
    EventManager.AddListener(EnumEventType.MainplayerRelationshipPropUpdate, MakeDelegateFromCSFunction(this.OnMainPlayerRelationshipPropUpdate, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.MainplayerRelationshipBasicInfoUpdate, MakeDelegateFromCSFunction(this.MainplayerRelationshipBasicInfoUpdate, MakeGenericClass(Action1, UInt64), this))
    EventManager.AddListener(EnumEventType.OnUpdateSpecialFriendInfo, MakeDelegateFromCSFunction(this.OnUpdateSpecialFriendInfo, Action0, this))
    EventManager.AddListener(EnumEventType.OnJingLingIMReadStatusChanged, MakeDelegateFromCSFunction(this.OnJingLingIMReadStatusChanged, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.OnShiTuRelationshipChanged, MakeDelegateFromCSFunction(this.OnShiTuRelationshipChanged, MakeGenericClass(Action1, UInt64), this))
    EventManager.AddListenerInternal(EnumEventType.OnWeddingRelationshipChanged, MakeDelegateFromCSFunction(this.OnWeddingRelationshipChanged, MakeGenericClass(Action1, UInt64), this))

    EventManager.AddListenerInternal(EnumEventType.MainPlayerRelationship_AddFriend, MakeDelegateFromCSFunction(this.OnAddFriend, MakeGenericClass(Action1, UInt64), this))
    EventManager.AddListenerInternal(EnumEventType.MainPlayerRelationship_AddBlack, MakeDelegateFromCSFunction(this.OnAddBlacklist, MakeGenericClass(Action1, UInt64), this))
    EventManager.AddListenerInternal(EnumEventType.MainPlayerRelationship_AddEnemy, MakeDelegateFromCSFunction(this.OnAddEnemy, MakeGenericClass(Action1, UInt64), this))
    EventManager.AddListenerInternal(EnumEventType.MainPlayerRelationship_DelFriend, MakeDelegateFromCSFunction(this.OnDeleteFriend, MakeGenericClass(Action1, UInt64), this))
    EventManager.AddListenerInternal(EnumEventType.MainPlayerRelationship_DelBlack, MakeDelegateFromCSFunction(this.OnDeleteBlacklist, MakeGenericClass(Action1, UInt64), this))
    EventManager.AddListenerInternal(EnumEventType.MainPlayerRelationship_DelEnemy, MakeDelegateFromCSFunction(this.OnDeleteEnemy, MakeGenericClass(Action1, UInt64), this))
    EventManager.AddListenerInternal(EnumEventType.MainPlayerRelationship_InformOnline, MakeDelegateFromCSFunction(this.InformOnline, MakeGenericClass(Action2, UInt64, Boolean), this))
    EventManager.AddListenerInternal(EnumEventType.MainPlayerRelationship_AddFriendSpecialId, MakeDelegateFromCSFunction(this.OnAddFriendSpecialId, MakeGenericClass(Action1, UInt64), this))
    EventManager.AddListenerInternal(EnumEventType.MainPlayerRelationship_DelFriendSpecialId, MakeDelegateFromCSFunction(this.OnDeleteFriendSpecialId, MakeGenericClass(Action1, UInt64), this))
    EventManager.AddListenerInternal(EnumEventType.MainPlayerRelationship_SetFriendGroupName, MakeDelegateFromCSFunction(this.OnSetFriendGroupName, MakeGenericClass(Action2, Int32, String), this))
    EventManager.AddListenerInternal(EnumEventType.MainPlayerRelationship_AdjustFriendGroup, MakeDelegateFromCSFunction(this.OnAdjustFriendGroup, MakeGenericClass(Action2, UInt64, Int32), this))
    EventManager.AddListenerInternal(EnumEventType.MainPlayerRelationship_AdjustFriendGroupWithMultiPlayer, MakeDelegateFromCSFunction(this.OnAdjustFriendGroupWithMultiPlayer, MakeGenericClass(Action2, MakeGenericClass(List, Object), Int32), this))

    EventManager.AddListenerInternal(EnumEventType.UpdateFriendOfflineTime, MakeDelegateFromCSFunction(this.OnUpdateFriendOfflineTime, MakeGenericClass(Action2, UInt64, UInt32), this))

    EventManager.AddListenerInternal(EnumEventType.MainPlayerRelationship_UpdateFriendServerGroup, MakeDelegateFromCSFunction(this.OnUpdateFriendServerGroup, MakeGenericClass(Action1, UInt64), this))

    EventManager.AddListener(EnumEventType.OnReceiveCloseFriendList, MakeDelegateFromCSFunction(this.OnReceiveCloseFriendList, Action0, this))

    LuaFriendContactListView:OnEnable(this)
end
CFriendContactListView.m_OnDisable_CS2LuaHook = function (this) 
    CFriendGroupMenuMgr.Inst:Close()
    EventManager.RemoveListenerInternal(EnumEventType.OnIMMsgReadStatusChanged, MakeDelegateFromCSFunction(this.UpdateFriendListAlert_WithPlayerId, MakeGenericClass(Action1, UInt64), this))
    EventManager.RemoveListener(EnumEventType.MainplayerRelationshipPropUpdate, MakeDelegateFromCSFunction(this.OnMainPlayerRelationshipPropUpdate, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.MainplayerRelationshipBasicInfoUpdate, MakeDelegateFromCSFunction(this.MainplayerRelationshipBasicInfoUpdate, MakeGenericClass(Action1, UInt64), this))
    EventManager.RemoveListener(EnumEventType.OnUpdateSpecialFriendInfo, MakeDelegateFromCSFunction(this.OnUpdateSpecialFriendInfo, Action0, this))
    EventManager.RemoveListener(EnumEventType.OnJingLingIMReadStatusChanged, MakeDelegateFromCSFunction(this.OnJingLingIMReadStatusChanged, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.OnShiTuRelationshipChanged, MakeDelegateFromCSFunction(this.OnShiTuRelationshipChanged, MakeGenericClass(Action1, UInt64), this))
    EventManager.RemoveListenerInternal(EnumEventType.OnWeddingRelationshipChanged, MakeDelegateFromCSFunction(this.OnWeddingRelationshipChanged, MakeGenericClass(Action1, UInt64), this))

    EventManager.RemoveListenerInternal(EnumEventType.MainPlayerRelationship_AddFriend, MakeDelegateFromCSFunction(this.OnAddFriend, MakeGenericClass(Action1, UInt64), this))
    EventManager.RemoveListenerInternal(EnumEventType.MainPlayerRelationship_AddBlack, MakeDelegateFromCSFunction(this.OnAddBlacklist, MakeGenericClass(Action1, UInt64), this))
    EventManager.RemoveListenerInternal(EnumEventType.MainPlayerRelationship_AddEnemy, MakeDelegateFromCSFunction(this.OnAddEnemy, MakeGenericClass(Action1, UInt64), this))
    EventManager.RemoveListenerInternal(EnumEventType.MainPlayerRelationship_DelFriend, MakeDelegateFromCSFunction(this.OnDeleteFriend, MakeGenericClass(Action1, UInt64), this))
    EventManager.RemoveListenerInternal(EnumEventType.MainPlayerRelationship_DelBlack, MakeDelegateFromCSFunction(this.OnDeleteBlacklist, MakeGenericClass(Action1, UInt64), this))
    EventManager.RemoveListenerInternal(EnumEventType.MainPlayerRelationship_DelEnemy, MakeDelegateFromCSFunction(this.OnDeleteEnemy, MakeGenericClass(Action1, UInt64), this))
    EventManager.RemoveListenerInternal(EnumEventType.MainPlayerRelationship_InformOnline, MakeDelegateFromCSFunction(this.InformOnline, MakeGenericClass(Action2, UInt64, Boolean), this))
    EventManager.RemoveListenerInternal(EnumEventType.MainPlayerRelationship_AddFriendSpecialId, MakeDelegateFromCSFunction(this.OnAddFriendSpecialId, MakeGenericClass(Action1, UInt64), this))
    EventManager.RemoveListenerInternal(EnumEventType.MainPlayerRelationship_DelFriendSpecialId, MakeDelegateFromCSFunction(this.OnDeleteFriendSpecialId, MakeGenericClass(Action1, UInt64), this))
    EventManager.RemoveListenerInternal(EnumEventType.MainPlayerRelationship_SetFriendGroupName, MakeDelegateFromCSFunction(this.OnSetFriendGroupName, MakeGenericClass(Action2, Int32, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.MainPlayerRelationship_AdjustFriendGroup, MakeDelegateFromCSFunction(this.OnAdjustFriendGroup, MakeGenericClass(Action2, UInt64, Int32), this))
    EventManager.RemoveListenerInternal(EnumEventType.MainPlayerRelationship_AdjustFriendGroupWithMultiPlayer, MakeDelegateFromCSFunction(this.OnAdjustFriendGroupWithMultiPlayer, MakeGenericClass(Action2, MakeGenericClass(List, Object), Int32), this))

    EventManager.RemoveListenerInternal(EnumEventType.UpdateFriendOfflineTime, MakeDelegateFromCSFunction(this.OnUpdateFriendOfflineTime, MakeGenericClass(Action2, UInt64, UInt32), this))

    EventManager.RemoveListenerInternal(EnumEventType.MainPlayerRelationship_UpdateFriendServerGroup, MakeDelegateFromCSFunction(this.OnUpdateFriendServerGroup, MakeGenericClass(Action1, UInt64), this))

    EventManager.RemoveListener(EnumEventType.OnReceiveCloseFriendList, MakeDelegateFromCSFunction(this.OnReceiveCloseFriendList, Action0, this))
    LuaFriendContactListView:OnDisable()
end
CFriendContactListView.m_OnCurrentGroupButtonClick_CS2LuaHook = function (this, go) 

    local texts = CreateFromClass(MakeGenericClass(List, String))
    do
        local i = 0
        while i < this.groups.Count do
            CommonDefs.ListAdd(texts, typeof(String), this.groups[i].groupName)
            i = i + 1
        end
    end
    CFriendGroupMenuMgr.Inst:ShowOrCloseMenu(this.selectedGroupIndex, texts, this)
end
CFriendContactListView.m_OnEditGroupButtonClick_CS2LuaHook = function (this, go) 

    CUIManager.ShowUI(CUIResources.FriendGroupModifyWnd)
end
CFriendContactListView.m_Init_CS2LuaHook = function (this) 

    CommonDefs.ListClear(this.groups)
    do
        local i = 0
        while i < CIMMgr.MAX_FRIEND_GROUP_NUM do
            local groupName = CIMMgr.Inst:GetFriendGroupName(i + 1)
            local info = CreateFromClass(GroupInfo, i + 1, groupName, RelationshipType.Friend)
            CommonDefs.ListAdd(this.groups, typeof(GroupInfo), info)
            i = i + 1
        end
    end
    if CIMMgr.Inst:EnableCloseFriend() then
        CommonDefs.ListAdd(this.groups, typeof(GroupInfo), CreateFromClass(GroupInfo, 0, LocalString.GetString("亲密好友"), RelationshipType.CloseFriend))
    end
    --好友分组添加NPC好友分组，注意RelationshipType中新加一种好友类型
    if LuaNPCChatMgr:EnableNpcChat() then
        CommonDefs.ListAdd(this.groups, typeof(GroupInfo), CreateFromClass(GroupInfo, 0, LocalString.GetString("NPC好友"), RelationshipType.NPCFriend))
    end
    CommonDefs.ListAdd(this.groups, typeof(GroupInfo), CreateFromClass(GroupInfo, 0, LocalString.GetString("跨服好友"), RelationshipType.CrossServerFriend))
    CommonDefs.ListAdd(this.groups, typeof(GroupInfo), CreateFromClass(GroupInfo, 0, LocalString.GetString("仇敌"), RelationshipType.Enemy))
    CommonDefs.ListAdd(this.groups, typeof(GroupInfo), CreateFromClass(GroupInfo, 0, LocalString.GetString("黑名单"), RelationshipType.Blacklist))

    this.tableView.dataSource = this
    this.tableView.eventDelegate = this
    this.tableView:Clear()
    this.friendNumDisplayLabel.text = nil
    this.arrow.flip = UIBasicSprite.Flip.Nothing
    this:OnGroupSelected(0)
end
CFriendContactListView.m_OnGroupSelected_CS2LuaHook = function (this, index) 

    do
        local i = 0
        while i < this.groups.Count do
            if i == index then
                this.curGroupBtn.Text = this.groups[i].groupName
                this.selectedGroupIndex = i
                this.m_NeedRequestCloseFriendData = true
                LuaFriendContactListView:SetContactListViewScale(this.groups[i].type)
                this:LoadContactList(this.groups[i])
                break
            end
            i = i + 1
        end
    end
end
CFriendContactListView.m_LoadContactList_CS2LuaHook = function (this, info) 

    this.tableView:Clear()
    CommonDefs.ListClear(this.allData)
    this.friendNumDisplayLabel.text = nil
    if CClientMainPlayer.Inst ~= nil then
        repeat
            local default = info.type
            if default == RelationshipType.Friend then
                local friends = CIMMgr.Inst:GetFriendsByGroup(info.groupId)
                CommonDefs.ListIterate(friends, DelegateFactory.Action_object(function (___value) 
                    local playerId = ___value
                    local im = CIMMgr.Inst:GetBasicInfo(playerId)
                    if im then
                        local data = CreateFromClass(FriendItemData, im)
                        if data then
                            CommonDefs.ListAdd(this.allData, typeof(FriendItemData), data)
                        end
                    end
                end))
                this.friendNumDisplayLabel.text = System.String.Format("{0}/{1}/{2}", friends.Count, CIMMgr.Inst.Friends.Count, CClientMainPlayer.Inst.RelationshipProp.CurFriendLimit)
                break
            elseif default == RelationshipType.Blacklist then
                local blacklist = CIMMgr.Inst.Blacklist
                CommonDefs.EnumerableIterate(blacklist, DelegateFactory.Action_object(function (___value) 
                    local playerId = ___value
                    local im = CIMMgr.Inst:GetBasicInfo(playerId)
                    if im then
                        local data = CreateFromClass(FriendItemData, im)
                        if data then
                            CommonDefs.ListAdd(this.allData, typeof(FriendItemData), data)
                        end
                    end
                end))
                this.friendNumDisplayLabel.text = System.String.Format("{0}/{1}", blacklist.Count, CIMMgr.MAX_BLACKLIST_NUM)
                break
            elseif default == RelationshipType.Enemy then
                local enemies = CIMMgr.Inst.Enemies
                CommonDefs.EnumerableIterate(enemies, DelegateFactory.Action_object(function (___value) 
                    local playerId = ___value
                    local im = CIMMgr.Inst:GetBasicInfo(playerId)
                    if im then
                        local data = CreateFromClass(FriendItemData, im)
                        if data then
                            CommonDefs.ListAdd(this.allData, typeof(FriendItemData), data)
                        end
                    end
                end))
                this.friendNumDisplayLabel.text = System.String.Format("{0}/{1}", enemies.Count, CIMMgr.MAX_ENEMY_NUM)
                break
            elseif default == RelationshipType.CrossServerFriend then
                local crossServerFriends = CIMMgr.Inst.Friends
                CommonDefs.EnumerableIterate(crossServerFriends, DelegateFactory.Action_object(function (___value) 
                    local playerId = ___value
                    if not CIMMgr.Inst:IsSameServerFriend(playerId) then
                        local im = CIMMgr.Inst:GetBasicInfo(playerId)
                        if im then
                            local data = CreateFromClass(FriendItemData, im)
                            if data then
                                CommonDefs.ListAdd(this.allData, typeof(FriendItemData), data)
                            end
                        end
                    end
                end))
                this.friendNumDisplayLabel.text = System.String.Format("{0}/{1}/{2}", this.allData.Count, CIMMgr.Inst.Friends.Count, CClientMainPlayer.Inst.RelationshipProp.CurFriendLimit)
                break
            elseif default == RelationshipType.CloseFriend then
                if this.m_NeedRequestCloseFriendData then
                    this.m_NeedRequestCloseFriendData = false
                    CIMMgr.Inst:QueryCloseFriendList()
                    return
                else
                    local closeFriends = CIMMgr.Inst.CloseFriendList
                    CommonDefs.ListIterate(closeFriends, DelegateFactory.Action_object(function (___value) 
                        local playerId = ___value
                        if CIMMgr.Inst:ExistBasicInfo(playerId) then
                            local im = CIMMgr.Inst:GetBasicInfo(playerId)
                            if im then
                                local data = CreateFromClass(FriendItemData, im)
                                if data then
                                    CommonDefs.ListAdd(this.allData, typeof(FriendItemData), data)
                                end
                            end
                        end
                    end))
                    this.friendNumDisplayLabel.text = System.String.Format("{0}/{1}/{2}", this.allData.Count, CIMMgr.Inst.Friends.Count, CClientMainPlayer.Inst.RelationshipProp.CurFriendLimit)
                end
                break
            elseif default == RelationshipType.NPCFriend then
                if CIMMgr.Inst:IsMyFriendSpecialId(Constants.YangyangID) then
                    --杨洋好友
                    local data = CreateFromClass(FriendItemData, Constants.YangyangID, 1, GameSetting_Common_Wapper.Inst.YangYangDisplayName, GameSetting_Common_Wapper.Inst.YangYangPortrait, CFriendListItem.ItemType.ChatBot, true)
                    if data then
                        CommonDefs.ListInsert(this.allData, 0, typeof(FriendItemData), data)
                    end
                end
                NpcChat_ChatNpc.ForeachKey(function(key) 
                    local NPC = NPC_NPC.GetData(key)
                    local isFriend = CClientMainPlayer.Inst.RelationshipProp.NpcFriendSet:GetBit(NpcChat_ChatNpc.GetData(key).SN) 
                    if NPC and isFriend then
                        local name = NPC.Name -- 读表NPC名称
                        local portraitName = NPC.Portrait  -- 读表读NPC头像路径
                        local Data =  CreateFromClass(FriendItemData,NPC.ID,1,name, portraitName, CFriendListItem.ItemType.ChatBot, true)
                        if Data then
                            CommonDefs.ListAdd(this.allData, typeof(FriendItemData), Data)
                        end
                    end
                end)
                break
            else
                return
            end
        until 1

        --对于好友的排序规则： 1. 在线不在线区分(注意区分单向好友) 2.按字母表排序
        local list1 = CreateFromClass(MakeGenericClass(List, FriendItemData))
        local list2 = CreateFromClass(MakeGenericClass(List, FriendItemData))
        do
            local i = 0
            while i < this.allData.Count do
                if CIMMgr.Inst:IsOnline(this.allData[i].playerId) then
                    CommonDefs.ListAdd(list1, typeof(FriendItemData), this.allData[i])
                else
                    CommonDefs.ListAdd(list2, typeof(FriendItemData), this.allData[i])
                end
                i = i + 1
            end
        end
        CommonDefs.ListSort1(list1, typeof(FriendItemData), DelegateFactory.Comparison_FriendItemData(function (item1, item2) 
            local result = NumberCompareTo(this:GetShiTuCompareVal(item1.playerId), this:GetShiTuCompareVal(item2.playerId))
            if result ~= 0 then
                return result
            end
            result = NumberCompareTo(item2.friendliness, item1.friendliness)
            if result ~= 0 then
                return result
            end
            return System.String.CompareOrdinal(item1.name, item2.name)
        end))
        CommonDefs.ListSort1(list2, typeof(FriendItemData), DelegateFactory.Comparison_FriendItemData(function (item1, item2) 
            local result = NumberCompareTo(this:GetShiTuCompareVal(item1.playerId), this:GetShiTuCompareVal(item2.playerId))
            if result ~= 0 then
                return result
            end
            result = NumberCompareTo(item2.friendliness, item1.friendliness)
            if result ~= 0 then
                return result
            end
            return System.String.CompareOrdinal(item1.name, item2.name)
        end))
        CommonDefs.ListClear(this.allData)
        CommonDefs.ListAddRange(this.allData, list1)
        CommonDefs.ListAddRange(this.allData, list2)
        --好友列表的第一个分组才会显示
        if info.type == RelationshipType.Friend and info.groupId == 1 then

            if CIMMgr.Inst:IsMyFriendSpecialId(Constants.YangyangID) and not LuaNPCChatMgr:EnableNpcChat() then
                --杨洋好友
                local data = CreateFromClass(FriendItemData, Constants.YangyangID, 1, GameSetting_Common_Wapper.Inst.YangYangDisplayName, GameSetting_Common_Wapper.Inst.YangYangPortrait, CFriendListItem.ItemType.ChatBot, true)
                CommonDefs.ListInsert(this.allData, 0, typeof(FriendItemData), data)
            end

            if CIMMgr.Inst.HasVipGmSession then
                local data = CreateFromClass(FriendItemData, CIMMgr.IM_VIP_GM_ID, 1, CIMMgr.IM_VIP_GM_NAME, CIMMgr.Inst:GetVIPGMPortrait(), CFriendListItem.ItemType.GM, true)
                data.isOnline = CIMMgr.Inst.VipGmStatus:Equals(EnumGmStatus.Online)
                CommonDefs.ListInsert(this.allData, 0, typeof(FriendItemData), data)
            end

            if CIMMgr.Inst.HasGmSession then
                local data = CreateFromClass(FriendItemData, CIMMgr.IM_GM_ID, 1, LocalString.GetString("心易专属"), CIMMgr.Inst:GetGMPortrait(), CFriendListItem.ItemType.GM, true)
                data.isOnline = CIMMgr.Inst.GmStatus:Equals(EnumGmStatus.Online)
                CommonDefs.ListInsert(this.allData, 0, typeof(FriendItemData), data)
            end

            if GameSetting_Common_Wapper.Inst.JingLingEnabled then
                local data2 = CreateFromClass(FriendItemData, CIMMgr.IM_JINGLING_ID, 1, GameSetting_Common_Wapper.Inst.JingLingDisplayName, GameSetting_Common_Wapper.Inst.JingLingPortrait, CFriendListItem.ItemType.JingLing, true)
                CommonDefs.ListInsert(this.allData, 0, typeof(FriendItemData), data2)
            end

            local data1 = CreateFromClass(FriendItemData, CIMMgr.IM_SYSTEM_ID, 1, LocalString.GetString("系统消息"), Constants.IM_ICON_SYSTEM, CFriendListItem.ItemType.System, true)
            CommonDefs.ListInsert(this.allData, 0, typeof(FriendItemData), data1)
        elseif info.type == RelationshipType.CloseFriend then
            --亲密好友动态
            local data = CreateFromClass(FriendItemData, 0, 1, CIMMgr.IM_CLOSE_FRIEND_ITEM_NAME, Constants.IM_ICON_CLOSE_FRIEND, CFriendListItem.ItemType.CloseFriend, true)
            CommonDefs.ListInsert(this.allData, 0, typeof(FriendItemData), data)
        end


        this.tableView:LoadData(0, false)

        local bFound = false
        do
            local i = 0
            while i < this.allData.Count do
                if this.allData[i].playerId == CFriendView.ChatOppositeId then
                    this.tableView.SelectedIndex = i
                    bFound = true
                    break
                end
                i = i + 1
            end
        end
        if not bFound and this.OnItemClickDelegate ~= nil then
            GenericDelegateInvoke(this.OnItemClickDelegate, Table2ArrayWithCount({0, nil}, 2, MakeArrayClass(Object)))
        end
    end
end
CFriendContactListView.m_GetShiTuCompareVal_CS2LuaHook = function (this, playerId) 
    --从小到大排序，相公显示在娘子前，有婚姻关系的显示在没有婚姻关系的玩家前
    if CWeddingMgr.Inst:IsMyHusband(playerId) then
        return - 5
    elseif CWeddingMgr.Inst:IsMyWife(playerId) then
        return - 4
    elseif CJieBaiMgr.Inst:GetExistJieBaiRelationByID(playerId, false) ~= nil then
        return - 3
    elseif CShiTuMgr.Inst:IsCurrentShiFu(playerId) then
        return - 2
    elseif CShiTuMgr.Inst:IsCurrentTuDi(playerId) then
        return - 1
    else
        return 0
    end
end
CFriendContactListView.m_UpdateFriendListAlert_WithPlayerId_CS2LuaHook = function (this, playerId) 

    do
        local i = 0
        while i < this.allData.Count do
            if this.allData[i].playerId == playerId then
                local go = this.tableView:GetCellByIndex(i)
                if go ~= nil then
                    local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CFriendListItem))
                    item.AlertVisible = CIMMgr.Inst:ShowNewMessageAlert(item.PlayerId)
                end
                break
            end
            i = i + 1
        end
    end
end
CFriendContactListView.m_UpdateFriendListAlert_CS2LuaHook = function (this) 

    do
        local i = 0
        while i < this.allData.Count do
            local continue
            repeat
                local go = this.tableView:GetCellByIndex(i)
                if go == nil then
                    continue = true
                    break
                end
                local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CFriendListItem))
                if item.FriendItemType == CFriendListItem.ItemType.JingLing then
                    item.AlertVisible = CJingLingMgr.Inst.ExistsUnreadMsgs or CExpertTeamMgr.Inst.ExistsNewAnswer
                else
                    item.AlertVisible = CIMMgr.Inst:ShowNewMessageAlert(item.PlayerId)
                end
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end
end
CFriendContactListView.m_OnExtendMenuButtonClick_CS2LuaHook = function (this, playerId) 

    local curType = RelationshipType.Friend
    do
        local i = 0
        while i < this.groups.Count do
            if i == this.selectedGroupIndex then
                curType = this.groups[i].type
                break
            end
            i = i + 1
        end
    end

    local msgs = CIMMgr.Inst:GetOppositeIMMsgsByID(playerId, 4)
    local nearestMsgs = Table2ArrayWithCount({"", "", ""}, 3, MakeArrayClass(System.String))
    do
        local i = 1
        while i < msgs.Count do
            if i <= nearestMsgs.Length then
                nearestMsgs[i - 1] = msgs[i].Content
            end
            i = i + 1
        end
    end
    local default
    if msgs.Count > 0 then
        default = msgs[0].Content
    else
        default = nil
    end
    local curMsg = default

    repeat
        local extern = curType
        if extern == RelationshipType.Friend then
            CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.FriendInFriendWnd, EChatPanel.Undefined, curMsg, nearestMsgs, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
            break
        elseif extern == RelationshipType.Enemy then
            CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.EnemyInFriendWnd, EChatPanel.Undefined, curMsg, nearestMsgs, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
            break
        elseif extern == RelationshipType.Blacklist then
            CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.BlacklistInFriendWnd, EChatPanel.Undefined, curMsg, nearestMsgs, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
            break
        elseif extern == RelationshipType.CrossServerFriend then
            CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.FriendInFriendWnd, EChatPanel.Undefined, curMsg, nearestMsgs, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
            break
        elseif extern == RelationshipType.CloseFriend then
            CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.CloseFriendInFriendWnd, EChatPanel.Undefined, curMsg, nearestMsgs, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
            break
        end
    until 1
end
CFriendContactListView.m_OnMainPlayerRelationshipPropUpdate_CS2LuaHook = function (this) 

    this:ReloadData()
end
CFriendContactListView.m_MainplayerRelationshipBasicInfoUpdate_CS2LuaHook = function (this, playerId) 
    local info = CIMMgr.Inst:GetBasicInfo(playerId)
    if info == nil then
        return
    end
    do
        local i = 0
        while i < this.allData.Count do
            local continue
            repeat
                if this.allData[i].playerId == playerId then
                    this.allData[i] = CreateFromClass(FriendItemData, info)
                end
                local go = this.tableView:GetCellByIndex(i)
                if go == nil then
                    continue = true
                    break
                end
                local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CFriendListItem))
                if item.PlayerId == playerId then
                    local data = this.allData[i]
                    item:RefreshBasicInfo(playerId, data.isOnline, data.name, data.portraitName, data.level, data.isInXianShenStatus)
                end
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end
end
CFriendContactListView.m_OnUpdateSpecialFriendInfo_CS2LuaHook = function (this) 

    this:ReloadData()
end
CFriendContactListView.m_OnJingLingIMReadStatusChanged_CS2LuaHook = function (this) 

    do
        local i = 0
        while i < this.allData.Count do
            local continue
            repeat
                local go = this.tableView:GetCellByIndex(i)
                if go == nil then
                    continue = true
                    break
                end
                local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CFriendListItem))
                if item.FriendItemType == CFriendListItem.ItemType.JingLing then
                    item.AlertVisible = CJingLingMgr.Inst.ExistsUnreadMsgs or CExpertTeamMgr.Inst.ExistsNewAnswer
                    break
                end
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end
end
CFriendContactListView.m_OnShiTuRelationshipChanged_CS2LuaHook = function (this, playerId) 

    do
        local i = 0
        while i < this.allData.Count do
            local continue
            repeat
                local go = this.tableView:GetCellByIndex(i)
                if go == nil then
                    continue = true
                    break
                end
                local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CFriendListItem))
                if item.PlayerId == playerId then
                    this:UpdateInfoText(item, playerId)
                end
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end
end
CFriendContactListView.m_OnWeddingRelationshipChanged_CS2LuaHook = function (this, playerId) 

    do
        local i = 0
        while i < this.allData.Count do
            local continue
            repeat
                local go = this.tableView:GetCellByIndex(i)
                if go == nil then
                    continue = true
                    break
                end
                local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CFriendListItem))
                if item.PlayerId == playerId then
                    this:UpdateInfoText(item, playerId)
                end
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end
end
CFriendContactListView.m_UpdateInfoText_CS2LuaHook = function (this, item, playerId) 

    if item.PlayerId == CIMMgr.IM_GM_ID then
        local time = CClientMainPlayer.Inst == nil and 0 or CClientMainPlayer.Inst.RelationshipProp.LastReceiveMsgFromGmTime
        if time > System.Double.Epsilon then
            item.InfoText = System.String.Format(LocalString.GetString("[7F7F7F]回复时间 {0}[-]"), ToStringWrap(CServerTimeMgr.ConvertTimeStampToZone8Time(time), "yyyy-MM-dd"))
        else
            item.InfoText = LocalString.GetString("客服")
        end
        return
    end

    do
        local i = 0
        while i < this.groups.Count do
            if i == this.selectedGroupIndex then
                if this.groups[i].type == RelationshipType.CrossServerFriend then
                    item.InfoText = CIMMgr.Inst:GetFriendServerName(playerId)
                    return
                end
            end
            i = i + 1
        end
    end

    local builder = NewStringBuilderWraper(StringBuilder)

    if CWeddingMgr.Inst:IsMyHusband(playerId) then
        builder:Append(LocalString.GetString("[FFAEF2]夫君[-]"))
    elseif CWeddingMgr.Inst:IsMyWife(playerId) then
        builder:Append(LocalString.GetString("[FFAEF2]娘子[-]"))
    elseif CShiTuMgr.Inst:IsCurrentShiFu(playerId) then
        builder:Append(LocalString.GetString("师父"))
    elseif CShiTuMgr.Inst:IsCurrentTuDi(playerId) then
        builder:Append(LocalString.GetString("徒弟"))
    else
        local text = CJieBaiMgr.Inst:GetExistJieBaiRelationByID(playerId, false)
        if not System.String.IsNullOrEmpty(text) then
            builder:Append(text)
        end
    end

    if CIMMgr.Inst:IsMyFriend(playerId) and not CIMMgr.Inst:IsOnline(playerId) then
        local offlineTime = CIMMgr.Inst:GetFriendOfflineTime(playerId)
        if offlineTime > 0 then
            if builder.Length > 0 then
                builder:Append("    ")
            end
            builder:Append(LocalString.GetString("[7F7F7F]离线 "))
            builder:Append(ToStringWrap(CServerTimeMgr.ConvertTimeStampToZone8Time(offlineTime), "yyyy-MM-dd"))
            builder:Append("[-]")
        end
    end

    item.InfoText = ToStringWrap(builder)
end
CFriendContactListView.m_OnAddFriend_CS2LuaHook = function (this, playerId) 
    do
        local i = 0
        while i < this.groups.Count do
            if i == this.selectedGroupIndex then
                if this.groups[i].type ~= RelationshipType.Friend and this.groups[i].type ~= RelationshipType.CrossServerFriend then
                    return
                end
                break
            end
            i = i + 1
        end
    end
    this:LocalAddAndRefresh(playerId)
end
CFriendContactListView.m_OnDeleteFriend_CS2LuaHook = function (this, playerId) 
    do
        local i = 0
        while i < this.groups.Count do
            if i == this.selectedGroupIndex then
                if this.groups[i].type ~= RelationshipType.Friend and this.groups[i].type ~= RelationshipType.CrossServerFriend then
                    return
                end
                break
            end
            i = i + 1
        end
    end
    this:LocalDeleteAndRefresh(playerId)
end
CFriendContactListView.m_OnAddBlacklist_CS2LuaHook = function (this, playerId) 
    do
        local i = 0
        while i < this.groups.Count do
            if i == this.selectedGroupIndex then
                if this.groups[i].type ~= RelationshipType.Blacklist then
                    if this.groups[i].type == RelationshipType.CloseFriend then
                        this:OnGroupSelected(this.selectedGroupIndex)
                        --亲密好友将人加入黑名单时，强制刷新一下列表
                    end
                    return
                end
                break
            end
            i = i + 1
        end
    end
    this:LocalAddAndRefresh(playerId)
end
CFriendContactListView.m_OnDeleteBlacklist_CS2LuaHook = function (this, playerId) 
    do
        local i = 0
        while i < this.groups.Count do
            if i == this.selectedGroupIndex then
                if this.groups[i].type ~= RelationshipType.Blacklist then
                    return
                end
                break
            end
            i = i + 1
        end
    end
    this:LocalDeleteAndRefresh(playerId)
end
CFriendContactListView.m_OnAddEnemy_CS2LuaHook = function (this, playerId) 
    do
        local i = 0
        while i < this.groups.Count do
            if i == this.selectedGroupIndex then
                if this.groups[i].type ~= RelationshipType.Enemy then
                    return
                end
                break
            end
            i = i + 1
        end
    end
    this:LocalAddAndRefresh(playerId)
end
CFriendContactListView.m_OnDeleteEnemy_CS2LuaHook = function (this, playerId) 
    do
        local i = 0
        while i < this.groups.Count do
            if i == this.selectedGroupIndex then
                if this.groups[i].type ~= RelationshipType.Enemy then
                    return
                end
                break
            end
            i = i + 1
        end
    end
    this:LocalDeleteAndRefresh(playerId)
end
CFriendContactListView.m_InformOnline_CS2LuaHook = function (this, playerId, online) 
    do
        local i = 0
        while i < this.allData.Count do
            local continue
            repeat
                local go = this.tableView:GetCellByIndex(i)
                if go == nil then
                    continue = true
                    break
                end
                local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CFriendListItem))
                if item.PlayerId == playerId then
                    local data = this.allData[i]
                    data.isOnline = online
                    this.allData[i] = data
                    item:UpdateOnlineStatus(data.portraitName, data.isOnline)
                    this:UpdateInfoText(item, playerId)
                end
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end
end
CFriendContactListView.m_OnUpdateFriendOfflineTime_CS2LuaHook = function (this, playerId, offlineTime) 
    do
        local i = 0
        while i < this.allData.Count do
            local continue
            repeat
                local go = this.tableView:GetCellByIndex(i)
                if go == nil then
                    continue = true
                    break
                end
                local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CFriendListItem))
                if item.PlayerId == playerId then
                    this:UpdateInfoText(item, playerId)
                end
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end
end
CFriendContactListView.m_OnUpdateFriendServerGroup_CS2LuaHook = function (this, playerId) 
    do
        local i = 0
        while i < this.allData.Count do
            local continue
            repeat
                local go = this.tableView:GetCellByIndex(i)
                if go == nil then
                    continue = true
                    break
                end
                local item = CommonDefs.GetComponent_GameObject_Type(go, typeof(CFriendListItem))
                if item.PlayerId == playerId then
                    this:ReloadData()
                    break
                end
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end
end
CFriendContactListView.m_OnAddFriendSpecialId_CS2LuaHook = function (this, playerId) 
    do
        local i = 0
        while i < this.groups.Count do
            if i == this.selectedGroupIndex then
                if this.groups[i].type ~= RelationshipType.Friend then
                    return
                end
                break
            end
            i = i + 1
        end
    end
    this:ReloadData()
end
CFriendContactListView.m_OnDeleteFriendSpecialId_CS2LuaHook = function (this, playerId) 
    do
        local i = 0
        while i < this.groups.Count do
            if i == this.selectedGroupIndex then
                if this.groups[i].type ~= RelationshipType.Friend then
                    return
                end
                break
            end
            i = i + 1
        end
    end
    this:ReloadData()
end
CFriendContactListView.m_OnSetFriendGroupName_CS2LuaHook = function (this, group, name) 
    do
        local i = 0
        while i < CIMMgr.MAX_FRIEND_GROUP_NUM do
            if i + 1 == group then
                local info = this.groups[i]
                info.groupName = CIMMgr.Inst:GetFriendGroupName(i + 1)
                this.groups[i] = info
            end
            i = i + 1
        end
    end
    do
        local i = 0
        while i < this.groups.Count do
            if i == this.selectedGroupIndex then
                this.curGroupBtn.Text = this.groups[i].groupName
                break
            end
            i = i + 1
        end
    end
end
CFriendContactListView.m_OnAdjustFriendGroup_CS2LuaHook = function (this, playerId, group) 
    do
        local i = 0
        while i < this.groups.Count do
            if i == this.selectedGroupIndex then
                if this.groups[i].type ~= RelationshipType.Friend then
                    return
                end
                break
            end
            i = i + 1
        end
    end
    this:ReloadData()
end
CFriendContactListView.m_OnAdjustFriendGroupWithMultiPlayer_CS2LuaHook = function (this, playerdata, group) 
    do
        local i = 0
        while i < this.groups.Count do
            if i == this.selectedGroupIndex then
                if this.groups[i].type ~= RelationshipType.Friend then
                    return
                end
                break
            end
            i = i + 1
        end
    end
    this:ReloadData()
end
CFriendContactListView.m_LocalDeleteAndRefresh_CS2LuaHook = function (this, playerId) 

    do
        local i = 0
        while i < this.groups.Count do
            if i == this.selectedGroupIndex then
                this:UpdateFriendNumDisplay(this.groups[i])
                break
            end
            i = i + 1
        end
    end

    local firstVisible = this.tableView:GetFirstVisibleIndex()
    local firstVisiblePlayerId = 0
    local deletedIndex = - 1
    do
        local i = 0
        while i < this.allData.Count do
            local data = this.allData[i]
            if data.playerId == playerId then
                deletedIndex = i
            end
            i = i + 1
        end
    end
    if deletedIndex == - 1 then
        return
    end

    if firstVisible >= 0 and firstVisible < this.allData.Count then
        firstVisiblePlayerId = this.allData[firstVisible].playerId
    end

    CommonDefs.ListRemoveAt(this.allData, deletedIndex)

    if firstVisiblePlayerId > 0 then
        local startIndex = 0
        local selectedIndex = - 1
        do
            local i = 0
            while i < this.allData.Count do
                local data = this.allData[i]
                if data.playerId == firstVisiblePlayerId then
                    startIndex = i
                end
                if data.playerId == CFriendView.ChatOppositeId then
                    selectedIndex = i
                end
                i = i + 1
            end
        end
        this.tableView:RefreshData(startIndex, selectedIndex)
        if selectedIndex == - 1 and this.OnItemClickDelegate ~= nil then
            GenericDelegateInvoke(this.OnItemClickDelegate, Table2ArrayWithCount({0, nil}, 2, MakeArrayClass(Object)))
        end
    else
        do
            local i = 0
            while i < this.groups.Count do
                if i == this.selectedGroupIndex then
                    this:LoadContactList(this.groups[i])
                    break
                end
                i = i + 1
            end
        end
    end
end
CFriendContactListView.m_LocalAddAndRefresh_CS2LuaHook = function (this, playerId) 
    this:ReloadData()
end
CFriendContactListView.m_ReloadData_CS2LuaHook = function (this) 
    do
        local i = 0
        while i < this.groups.Count do
            if i == this.selectedGroupIndex then
                this:LoadContactList(this.groups[i])
                break
            end
            i = i + 1
        end
    end
end
CFriendContactListView.m_OnReceiveCloseFriendList_CS2LuaHook = function (this) 
    do
        local i = 0
        while i < this.groups.Count do
            if i == this.selectedGroupIndex then
                if this.groups[i].type ~= RelationshipType.CloseFriend then
                    return
                end
                break
            end
            i = i + 1
        end
    end
    this:ReloadData()
end
CFriendContactListView.m_UpdateFriendNumDisplay_CS2LuaHook = function (this, info) 
    if CClientMainPlayer.Inst ~= nil then
        repeat
            local default = info.type
            if default == RelationshipType.Friend then
                local friends = CIMMgr.Inst:GetFriendsByGroup(info.groupId)
                this.friendNumDisplayLabel.text = System.String.Format("{0}/{1}/{2}", friends.Count, CIMMgr.Inst.Friends.Count, CClientMainPlayer.Inst.RelationshipProp.CurFriendLimit)
                break
            elseif default == RelationshipType.Blacklist then
                local blacklist = CIMMgr.Inst.Blacklist
                this.friendNumDisplayLabel.text = System.String.Format("{0}/{1}", blacklist.Count, CIMMgr.MAX_BLACKLIST_NUM)
                break
            elseif default == RelationshipType.Enemy then
                local enemies = CIMMgr.Inst.Enemies
                this.friendNumDisplayLabel.text = System.String.Format("{0}/{1}", enemies.Count, CIMMgr.MAX_ENEMY_NUM)
                break
            elseif default == RelationshipType.CrossServerFriend then
                local crossServerFriends = CIMMgr.Inst.Friends
                local n = 0
                CommonDefs.EnumerableIterate(crossServerFriends, DelegateFactory.Action_object(function (___value) 
                    local playerId = ___value
                    if not CIMMgr.Inst:IsSameServerFriend(playerId) then
                        n = n + 1
                    end
                end))
                this.friendNumDisplayLabel.text = System.String.Format("{0}/{1}/{2}", n, CIMMgr.Inst.Friends.Count, CClientMainPlayer.Inst.RelationshipProp.CurFriendLimit)
                break
            elseif default == RelationshipType.CloseFriend then
                local closeFriends = CIMMgr.Inst.CloseFriendList
                local num = 0
                CommonDefs.ListIterate(closeFriends, DelegateFactory.Action_object(function (___value) 
                    local playerId = ___value
                    if CIMMgr.Inst:ExistBasicInfo(playerId) then
                        num = num + 1
                    end
                end))
                this.friendNumDisplayLabel.text = System.String.Format("{0}/{1}/{2}", num, CIMMgr.Inst.Friends.Count, CClientMainPlayer.Inst.RelationshipProp.CurFriendLimit)
                break
            else
                this.friendNumDisplayLabel.text = nil
                return
            end
        until 1
    end
end

LuaFriendContactListView = {}
LuaFriendContactListView.m_View = nil
-- NPC好友列表不显示下方在线状态，需要调整下相关Panel大小
function LuaFriendContactListView:SetContactListViewScale(listType)
    if not self.m_View then return end
    local StatusPanel = self.m_View.transform:Find("Panel")
    local ScrollView = self.m_View.transform:Find("ScrollView"):GetComponent(typeof(UIScrollView))
    local ScrollViewIndicator = self.m_View.transform:Find("ScrollViewIndicator")
    local ClipRegion = Vector4(-33,-25,620,654)
    local newClipRegion = Vector4(-33,-50,620,704)
    local IndicatorYPos = 331
    local newIndicatorYPos = 281
    local ScrollViewPanel
    if listType == RelationshipType.NPCFriend then
        ScrollView.panel.baseClipRegion = newClipRegion
        LuaUtils.SetLocalPositionY(ScrollViewIndicator,newIndicatorYPos)
        StatusPanel.gameObject:SetActive(false)
    else
        ScrollView.panel.baseClipRegion = ClipRegion
        LuaUtils.SetLocalPositionY(ScrollViewIndicator,IndicatorYPos)
        StatusPanel.gameObject:SetActive(true)
    end
end

function LuaFriendContactListView:OnEnable(View)
    self.m_View = View
end

function LuaFriendContactListView:OnDisable()
    self.m_View = nil
end
