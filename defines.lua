local EnumItemType = import "L10.Game.EnumItemType"
local EnumTempPlayDataKey = import "L10.Game.EnumTempPlayDataKey"
local EnumFurnitureType = import "L10.Game.EnumFurnitureType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
EnumFurnitureType_lua = {
    eUnknown = EnumToInt(EnumFurnitureType.eUnknown),
    eChuangta = EnumToInt(EnumFurnitureType.eChuangta),
    eChugui = EnumToInt(EnumFurnitureType.eChugui),
    eZhuoan = EnumToInt(EnumFurnitureType.eZhuoan),
    eZuoju = EnumToInt(EnumFurnitureType.eZuoju),
    ePingfeng = EnumToInt(EnumFurnitureType.ePingfeng),
    eZawu = EnumToInt(EnumFurnitureType.eZawu),
    eGuashi = EnumToInt(EnumFurnitureType.eGuashi),
    eQinggong = EnumToInt(EnumFurnitureType.eQinggong),
    eChuanjiabao = EnumToInt(EnumFurnitureType.eChuanjiabao),
    eXiaojing = EnumToInt(EnumFurnitureType.eXiaojing),
    eHuamu = EnumToInt(EnumFurnitureType.eHuamu),
    eJianzhu = EnumToInt(EnumFurnitureType.eJianzhu),
    eNpc = EnumToInt(EnumFurnitureType.eNpc),
    eLingshou = EnumToInt(EnumFurnitureType.eLingshou),
    ePuppet = EnumToInt(EnumFurnitureType.ePuppet),
    eMapi = EnumToInt(EnumFurnitureType.eMapi),
    eSkyBox = EnumToInt(EnumFurnitureType.eSkyBox),
    eZswTemplate = EnumToInt(EnumFurnitureType.eZswTemplate),
    eWallPaper = EnumToInt(EnumFurnitureType.eWallPaper),
    ePoolFurniture = EnumToInt(EnumFurnitureType.ePoolFurniture),
    eMaxType = EnumToInt(EnumFurnitureType.eMaxType)
}
EnumItemPlace_lua = {
    Bag = EnumToInt(EnumItemPlace.Bag),
    Body = EnumToInt(EnumItemPlace.Body),
    Task = EnumToInt(EnumItemPlace.Task),
    LingShou = EnumToInt(EnumItemPlace.LingShou),
    Recycle = EnumToInt(EnumItemPlace.Recycle),
    ZuoQi = EnumToInt(EnumItemPlace.ZuoQi)
}
EnumItemType_lua = {
    Silver = EnumToInt(EnumItemType.Silver),
    Drug = EnumToInt(EnumItemType.Drug),
    Food = EnumToInt(EnumItemType.Food),
    Task = EnumToInt(EnumItemType.Task),
    Gem = EnumToInt(EnumItemType.Gem),
    WushanStone = EnumToInt(EnumItemType.WushanStone),
    Diamond = EnumToInt(EnumItemType.Diamond),
    Book = EnumToInt(EnumItemType.Book),
    Chest = EnumToInt(EnumItemType.Chest),
    Pet = EnumToInt(EnumItemType.Pet),
    Map = EnumToInt(EnumItemType.Map),
    Fashion = EnumToInt(EnumItemType.Fashion),
    Mount = EnumToInt(EnumItemType.Mount),
    Guild = EnumToInt(EnumItemType.Guild),
    Forge = EnumToInt(EnumItemType.Forge),
    Gather = EnumToInt(EnumItemType.Gather),
    Fishing = EnumToInt(EnumItemType.Fishing),
    Pharmacy = EnumToInt(EnumItemType.Pharmacy),
    Cooking = EnumToInt(EnumItemType.Cooking),
    GiftBag = EnumToInt(EnumItemType.GiftBag),
    Other = EnumToInt(EnumItemType.Other),
    PetItem = EnumToInt(EnumItemType.PetItem),
    FreeSilver = EnumToInt(EnumItemType.FreeSilver),
    ExpDrug = EnumToInt(EnumItemType.ExpDrug),
    PuzzleMap = EnumToInt(EnumItemType.PuzzleMap),
    Precious = EnumToInt(EnumItemType.Precious),
    GreenHandGift = EnumToInt(EnumItemType.GreenHandGift),
    Jueji = EnumToInt(EnumItemType.Jueji),
    StatusMedichine = EnumToInt(EnumItemType.StatusMedichine),
    GameTool = EnumToInt(EnumItemType.GameTool),
    Expression = EnumToInt(EnumItemType.Expression),
    Zhuangshiwu = EnumToInt(EnumItemType.Zhuangshiwu),
    DaijianZhuangshiwu = EnumToInt(EnumItemType.DaijianZhuangshiwu),
    Craft = EnumToInt(EnumItemType.Craft),
    HouseProduct = EnumToInt(EnumItemType.HouseProduct),
    EvaluatedZhushabi = EnumToInt(EnumItemType.EvaluatedZhushabi),
    UnEvaluateZhushabi = EnumToInt(EnumItemType.UnEvaluateZhushabi),
    Chuanjiabao = EnumToInt(EnumItemType.Chuanjiabao),
    UnEvaluateChuanjiabao = EnumToInt(EnumItemType.UnEvaluateChuanjiabao),
    MaPiLingFu = EnumToInt(EnumItemType.MaPiLingFu),
    WenShi = EnumToInt(EnumItemType.WenShi),
    TokenMoney = EnumToInt(EnumItemType.TokenMoney),
    Invalid = EnumToInt(EnumItemType.Invalid)
}
EnumTempPlayDataKey_lua = {
    eGnjcDoubleScore = EnumToInt(EnumTempPlayDataKey.eGnjcDoubleScore),
    eGnjcWeeklyScore = EnumToInt(EnumTempPlayDataKey.eGnjcWeeklyScore),
    eQianDaoDailyTimes = EnumToInt(EnumTempPlayDataKey.eQianDaoDailyTimes),
    eQianDaoExtraTimes = EnumToInt(EnumTempPlayDataKey.eQianDaoExtraTimes),
    eQianDaoTotalTimes = EnumToInt(EnumTempPlayDataKey.eQianDaoTotalTimes),
    eCurrentGuildFreightItems = EnumToInt(EnumTempPlayDataKey.eCurrentGuildFreightItems),
    eNextGuildFreightItems = EnumToInt(EnumTempPlayDataKey.eNextGuildFreightItems),
    eMPTZ = EnumToInt(EnumTempPlayDataKey.eMPTZ),
    eKeJuXiangShiScore = EnumToInt(EnumTempPlayDataKey.eKeJuXiangShiScore),
    eKeJuXiangShiQuestionNum = EnumToInt(EnumTempPlayDataKey.eKeJuXiangShiQuestionNum),
    eKeJuXiangShiQuestionId = EnumToInt(EnumTempPlayDataKey.eKeJuXiangShiQuestionId),
    eEquipExchangeMFTimes = EnumToInt(EnumTempPlayDataKey.eEquipExchangeMFTimes),
    eKeJuXiangShiAnswerIdx = EnumToInt(EnumTempPlayDataKey.eKeJuXiangShiAnswerIdx),
    eHolidayQianDaoTimes = EnumToInt(EnumTempPlayDataKey.eHolidayQianDaoTimes),
    eHolidayQianDaoTotalTimes = EnumToInt(EnumTempPlayDataKey.eHolidayQianDaoTotalTimes),
    ePublicRechargeActivity = EnumToInt(EnumTempPlayDataKey.ePublicRechargeActivity),
    eDayOnlineTime = EnumToInt(EnumTempPlayDataKey.eDayOnlineTime),
    eQnmNightVoteResult = EnumToInt(EnumTempPlayDataKey.eQnmNightVoteResult),
    eHolidayOnlineTime = EnumToInt(EnumTempPlayDataKey.eHolidayOnlineTime),
    eHolidayQianDaoExtraSignTimes = EnumToInt(EnumTempPlayDataKey.eHolidayQianDaoExtraSignTimes),
    eMonthRechargeMoney = EnumToInt(EnumTempPlayDataKey.eMonthRechargeMoney),
    ePersonalSpaceToken = EnumToInt(EnumTempPlayDataKey.ePersonalSpaceToken),
    eWordFilterSyncOnReconnect = EnumToInt(EnumTempPlayDataKey.eWordFilterSyncOnReconnect),
    eExchangeMFDailyNum = EnumToInt(EnumTempPlayDataKey.eExchangeMFDailyNum),
    eShiTuFengNianGuoJieSendItemPlayers = EnumToInt(EnumTempPlayDataKey.eShiTuFengNianGuoJieSendItemPlayers),
    eTempFashion = EnumToInt(EnumTempPlayDataKey.eTempFashion),
    eQiXiFestivalInfo = EnumToInt(EnumTempPlayDataKey.eQiXiFestivalInfo),
    eLogWhenFirstOfflinePerDay = EnumToInt(EnumTempPlayDataKey.eLogWhenFirstOfflinePerDay),
    ePraiseLoveLetterId = EnumToInt(EnumTempPlayDataKey.ePraiseLoveLetterId),
    eQingYiLimitPerWeek = EnumToInt(EnumTempPlayDataKey.eQingYiLimitPerWeek),
    eHuiLiuTotalGrade = EnumToInt(EnumTempPlayDataKey.eHuiLiuTotalGrade),
    eHuiLiuReward = EnumToInt(EnumTempPlayDataKey.eHuiLiuReward),
    eCommonPlantFlower = EnumToInt(EnumTempPlayDataKey.eCommonPlantFlower),
    eMskkPlayTimes = EnumToInt(EnumTempPlayDataKey.eMskkPlayTimes),
    eQiangqin = EnumToInt(EnumTempPlayDataKey.eQiangqin),
    eWeddingParade = EnumToInt(EnumTempPlayDataKey.eWeddingParade),
    eShiTuQingFinishData = EnumToInt(EnumTempPlayDataKey.eShiTuQingFinishData),
    eFenJie4HoleCount = EnumToInt(EnumTempPlayDataKey.eFenJie4HoleCount),
    eInvitedHuiLiuPlayer = EnumToInt(EnumTempPlayDataKey.eInvitedHuiLiuPlayer),
    eInvitedHuiLiuPlayerRewardPlayer = EnumToInt(EnumTempPlayDataKey.eInvitedHuiLiuPlayerRewardPlayer),
    eQingYuanPlay = EnumToInt(EnumTempPlayDataKey.eQingYuanPlay),
    eSetDuoHunFanCountPerDay = EnumToInt(EnumTempPlayDataKey.eSetDuoHunFanCountPerDay),
    eMPTZLastChallengeTime = EnumToInt(EnumTempPlayDataKey.eMPTZLastChallengeTime),
    eXiangShiDailyRightNum = EnumToInt(EnumTempPlayDataKey.eXiangShiDailyRightNum),
    ePK = EnumToInt(EnumTempPlayDataKey.ePK),
    eCuJuPracticeTime = EnumToInt(EnumTempPlayDataKey.eCuJuPracticeTime),
    eAdjustMonthMallPresentLimit = EnumToInt(EnumTempPlayDataKey.eAdjustMonthMallPresentLimit),
    eFamilyTreeLogToday = EnumToInt(EnumTempPlayDataKey.eFamilyTreeLogToday),
    eGongJianJiaYuan = EnumToInt(EnumTempPlayDataKey.eGongJianJiaYuan),
    eYuanDanQiuQian = EnumToInt(EnumTempPlayDataKey.eYuanDanQiuQian),
    eChristmasCardCountInfo = EnumToInt(EnumTempPlayDataKey.eChristmasCardCountInfo),
    eDouHunVote = EnumToInt(EnumTempPlayDataKey.eDouHunVote),
    eHmtShare = EnumToInt(EnumTempPlayDataKey.eHmtShare),
    eEquipBaptizeBasicTimes = EnumToInt(EnumTempPlayDataKey.eEquipBaptizeBasicTimes),
    eEquipBaptizeWordTimes = EnumToInt(EnumTempPlayDataKey.eEquipBaptizeWordTimes),
    eFabaoChongzhuTimes = EnumToInt(EnumTempPlayDataKey.eFabaoChongzhuTimes),
    eFabaoLianhuaTimes = EnumToInt(EnumTempPlayDataKey.eFabaoLianhuaTimes),
    eFabaoTilianTimes = EnumToInt(EnumTempPlayDataKey.eFabaoTilianTimes),
    eKaitianshu = EnumToInt(EnumTempPlayDataKey.eKaitianshu),
    eWashLingshou = EnumToInt(EnumTempPlayDataKey.eWashLingshou),
    eLingshouWuxing = EnumToInt(EnumTempPlayDataKey.eLingshouWuxing),
    ePGQYPlayTimes = EnumToInt(EnumTempPlayDataKey.ePGQYPlayTimes),
    eShengDanMailTimes = EnumToInt(EnumTempPlayDataKey.eShengDanMailTimes),
    eChunJiePrayTimes = EnumToInt(EnumTempPlayDataKey.eChunJiePrayTimes),
    eValentineFlowerData = EnumToInt(EnumTempPlayDataKey.eValentineFlowerData),
    eChunJieJiaoZiTimes = EnumToInt(EnumTempPlayDataKey.eChunJieJiaoZiTimes),
    eLogWhenFirstOnlinePerDay = EnumToInt(EnumTempPlayDataKey.eLogWhenFirstOnlinePerDay),
    eDivination = EnumToInt(EnumTempPlayDataKey.eDivination),
    eTaoHuaBaoXiaFlowerData = EnumToInt(EnumTempPlayDataKey.eTaoHuaBaoXiaFlowerData),
    eDuoMaoMaoDailyTimes = EnumToInt(EnumTempPlayDataKey.eDuoMaoMaoDailyTimes),
    eKeJuReviewQuestionId = EnumToInt(EnumTempPlayDataKey.eKeJuReviewQuestionId),
    eTowerDefensePlayAwardInfo = EnumToInt(EnumTempPlayDataKey.eTowerDefensePlayAwardInfo),
    eZhouNianQingShareAwardInfo = EnumToInt(EnumTempPlayDataKey.eZhouNianQingShareAwardInfo),
    eJXYSRewardTimes = EnumToInt(EnumTempPlayDataKey.eJXYSRewardTimes),
    eQmpkPlayData = EnumToInt(EnumTempPlayDataKey.eQmpkPlayData),
    eYiTiaoLongEnterPlayTimes = EnumToInt(EnumTempPlayDataKey.eYiTiaoLongEnterPlayTimes),
    eDetectFoodLbsInfo = EnumToInt(EnumTempPlayDataKey.eDetectFoodLbsInfo),
    eDetectFoodGotDailyTimes = EnumToInt(EnumTempPlayDataKey.eDetectFoodGotDailyTimes),
    eDetectFoodNotGotTimes = EnumToInt(EnumTempPlayDataKey.eDetectFoodNotGotTimes),
    eCurrentDetectFoodDrugId = EnumToInt(EnumTempPlayDataKey.eCurrentDetectFoodDrugId),
    eDetectFoodGotSpecialData = EnumToInt(EnumTempPlayDataKey.eDetectFoodGotSpecialData),
    eSendFoodDetectItemGotDailyTimes = EnumToInt(EnumTempPlayDataKey.eSendFoodDetectItemGotDailyTimes),
    eReceiveFoodDetectItemGotDailyTimes = EnumToInt(EnumTempPlayDataKey.eReceiveFoodDetectItemGotDailyTimes),
    eLastBiDongTime = EnumToInt(EnumTempPlayDataKey.eLastBiDongTime),
    eQmpkWatchAwardData = EnumToInt(EnumTempPlayDataKey.eQmpkWatchAwardData),
    eYiTiaoLongSubTaskIdx2SubmitTimes = EnumToInt(EnumTempPlayDataKey.eYiTiaoLongSubTaskIdx2SubmitTimes),
    eQmpkJingCaiBetData = EnumToInt(EnumTempPlayDataKey.eQmpkJingCaiBetData),
    eMPTYSDailyReward = EnumToInt(EnumTempPlayDataKey.eMPTYSDailyReward),
    eYBZDDailyAwardTimesInfo = EnumToInt(EnumTempPlayDataKey.eYBZDDailyAwardTimesInfo),
    eLBSAwardTimes = EnumToInt(EnumTempPlayDataKey.eLBSAwardTimes),
    eMPTZChallengeCooldown = EnumToInt(EnumTempPlayDataKey.eMPTZChallengeCooldown),
    eLianLianKanPrePlayTime = EnumToInt(EnumTempPlayDataKey.eLianLianKanPrePlayTime),
    eDailyGiveExpFromKillMonsterLimit = EnumToInt(EnumTempPlayDataKey.eDailyGiveExpFromKillMonsterLimit),
    eDouHunTrainSubmit = EnumToInt(EnumTempPlayDataKey.eDouHunTrainSubmit),
    eDuoMaoMaoWeekScore = EnumToInt(EnumTempPlayDataKey.eDuoMaoMaoWeekScore),
    eFeiShengPublicSceneInfo = EnumToInt(EnumTempPlayDataKey.eFeiShengPublicSceneInfo),
    ePGQYScore = EnumToInt(EnumTempPlayDataKey.ePGQYScore),
    ePGQYDouble = EnumToInt(EnumTempPlayDataKey.ePGQYDouble),
    eLiaoluowanInfo = EnumToInt(EnumTempPlayDataKey.eLiaoluowanInfo),
    eDouHunTrainMonthScore = EnumToInt(EnumTempPlayDataKey.eDouHunTrainMonthScore),
    eLastFeiShengEnterPlayTime = EnumToInt(EnumTempPlayDataKey.eLastFeiShengEnterPlayTime),
    eHongLvZuoZhanDailyTimes = EnumToInt(EnumTempPlayDataKey.eHongLvZuoZhanDailyTimes),
    eXueQiuWinRewardDailyTimes = EnumToInt(EnumTempPlayDataKey.eXueQiuWinRewardDailyTimes),
    eVoiceLoveBossRewardDailyTimes = EnumToInt(EnumTempPlayDataKey.eVoiceLoveBossRewardDailyTimes),
    eNewYearQianDaoTimes = EnumToInt(EnumTempPlayDataKey.eNewYearQianDaoTimes),
    eNewYearQianDaoTotalTimes = EnumToInt(EnumTempPlayDataKey.eNewYearQianDaoTotalTimes),
    eNewYearQianDaoExtraSignTimes = EnumToInt(EnumTempPlayDataKey.eNewYearQianDaoExtraSignTimes),
    eCcChatToken = EnumToInt(EnumTempPlayDataKey.eCcChatToken),

    eQingMing2020PveData = 219,
    eHouseCompetitionFanPaiZiData = 231,
    eJiaNianHua2020PersonalInfo = 246,
    eSelfEditedOffWorldCreature = 265,
    eLastChangeWuXingTime = 264,
    eShuiGuoPaiDuiMaxScore = 272,
    eShuiGuoPaiDuiReward = 276,
    eOpenJiaNianHuaTicketSellWndTime = 279,

    eYuanDan2022XinYuanData = 291,
    eQmpkSignUpData = 326,
    eQiXi2022SFEQ_EndingData = 328,
    eQiXi2022YYS = 334,
    eQiXi2022YYS_Daily = 335,
    eProtectCampfire = 347,
    eGuildLeagueCrossGambleDaiBi = 364, -- 跨服联赛竞猜代币
    eDuanWu2023PVPVEDailyScore = 389, -- 端午 2023 PVPVE 玩法每日获得积分
    eBigMonthCardRewardtime = 398, -- 大月卡奖励时间
    eAsianGames2023Data = 399, -- 亚运会2023
    eXianHaiTongXingData = 402, -- 仙海同行
    eCarnival2023CollectData = 413, --2023嘉年华集章数据
    
    eDouble11RechargeData = 420, -- 2023双11充值抽奖活动充值数据
    eDouble11LotteryData = 421, -- 2023双11充值抽奖活动抽奖数据
    eDouble11ExteriorDiscountCouponsData = 422, -- 2023双11外观折扣券数据

    eChristmas2023GiftData = 423, -- 2023圣诞节 给自己的礼物 卡片合成/礼物领取数据
    
    eGuideManualSearchHistory = 434, --新手指引手册搜索历史记录
}
local EHouseEditableOperation = import "L10.Game.EHouseEditableOperation"
EHouseEditableOperation_lua = {
    EnterZhengwu = EnumToInt(EHouseEditableOperation.EnterZhengwu),
    OpYardFurniture = EnumToInt(EHouseEditableOperation.OpYardFurniture),
    OpRoomFurniture = EnumToInt(EHouseEditableOperation.OpRoomFurniture),
    UpgradeBuilding = EnumToInt(EHouseEditableOperation.UpgradeBuilding),
    LingshouDongfang = EnumToInt(EHouseEditableOperation.LingshouDongfang),
    ExchangeTalisman = EnumToInt(EHouseEditableOperation.ExchangeTalisman)
}
local EHouseProgress = import "L10.Game.EHouseProgress"
EHouseProgress_lua = {
    eZhengwuProgressId = EnumToInt(EHouseProgress.eZhengwuProgressId),
    eCangkuProgressId = EnumToInt(EHouseProgress.eCangkuProgressId),
    eXiangfangProgressId = EnumToInt(EHouseProgress.eXiangfangProgressId),
    eShuiyuProgressId = EnumToInt(EHouseProgress.eShuiyuProgressId),
    eGardenProgressBase = EnumToInt(EHouseProgress.eGardenProgressBase),
    eCropProgressBase = EnumToInt(EHouseProgress.eCropProgressBase),
    eKuozhangProgressId = EnumToInt(EHouseProgress.eKuozhangProgressId)
}
local EnumBodyPosition = import "L10.Game.EnumBodyPosition"
EnumBodyPosition_lua = {
    Undefined = EnumToInt(EnumBodyPosition.Undefined),
    Weapon = EnumToInt(EnumBodyPosition.Weapon),
    Shield = EnumToInt(EnumBodyPosition.Shield),
    Casque = EnumToInt(EnumBodyPosition.Casque),
    Armour = EnumToInt(EnumBodyPosition.Armour),
    Belt = EnumToInt(EnumBodyPosition.Belt),
    Gloves = EnumToInt(EnumBodyPosition.Gloves),
    Shoes = EnumToInt(EnumBodyPosition.Shoes),
    Ring = EnumToInt(EnumBodyPosition.Ring),
    Bracelet = EnumToInt(EnumBodyPosition.Bracelet),
    Necklace = EnumToInt(EnumBodyPosition.Necklace),
	BackPendant = EnumToInt(EnumBodyPosition.BackPendant),
    TalismanQian = EnumToInt(EnumBodyPosition.TalismanQian),
    TalismanXun = EnumToInt(EnumBodyPosition.TalismanXun),
    TalismanKan = EnumToInt(EnumBodyPosition.TalismanKan),
    TalismanGen = EnumToInt(EnumBodyPosition.TalismanGen),
    TalismanKun = EnumToInt(EnumBodyPosition.TalismanKun),
    TalismanZhen = EnumToInt(EnumBodyPosition.TalismanZhen),
    TalismanLi = EnumToInt(EnumBodyPosition.TalismanLi),
    TalismanDui = EnumToInt(EnumBodyPosition.TalismanDui)
}
local SkillButtonSkillType = import "L10.Game.SkillButtonSkillType"
SkillButtonSkillType_lua = {
    NoSkill = EnumToInt(SkillButtonSkillType.NoSkill)
}
local EnumFurnitureSubType = import "L10.Game.EnumFurnitureSubType"
EnumFurnitureSubType_lua = {
    eUnknown = EnumToInt(EnumFurnitureSubType.eUnknown),
    eChuangta = EnumToInt(EnumFurnitureSubType.eChuangta),
    eChugui = EnumToInt(EnumFurnitureSubType.eChugui),
    eZhuoan = EnumToInt(EnumFurnitureSubType.eZhuoan),
    eZuoju = EnumToInt(EnumFurnitureSubType.eZuoju),
    ePingfeng = EnumToInt(EnumFurnitureSubType.ePingfeng),
    eZawu = EnumToInt(EnumFurnitureSubType.eZawu),
    eGuashi = EnumToInt(EnumFurnitureSubType.eGuashi),
    eQinggong = EnumToInt(EnumFurnitureSubType.eQinggong),
    eChuanjiabao = EnumToInt(EnumFurnitureSubType.eChuanjiabao),
    eXiaojing = EnumToInt(EnumFurnitureSubType.eXiaojing),
    eHuamu = EnumToInt(EnumFurnitureSubType.eHuamu),
    eZhengwu = EnumToInt(EnumFurnitureSubType.eZhengwu),
    eCangku = EnumToInt(EnumFurnitureSubType.eCangku),
    eXiangfang = EnumToInt(EnumFurnitureSubType.eXiangfang),
    eMiaopu = EnumToInt(EnumFurnitureSubType.eMiaopu),
    eNpc = EnumToInt(EnumFurnitureSubType.eNpc),
    eWeiqiang = EnumToInt(EnumFurnitureSubType.eWeiqiang),
    eLingshou = EnumToInt(EnumFurnitureSubType.eLingshou),
    ePuppet = EnumToInt(EnumFurnitureSubType.ePuppet),
    eMapi = EnumToInt(EnumFurnitureSubType.eMapi),
    eSkyBox = EnumToInt(EnumFurnitureSubType.eSkyBox),
    eZswTemplate = EnumToInt(EnumFurnitureSubType.eZswTemplate),
    eWallPaper = EnumToInt(EnumFurnitureSubType.eWallPaper),
    ePoolFurniture = EnumToInt(EnumFurnitureSubType.ePoolFurniture),
    eFish = EnumToInt(EnumFurnitureSubType.eFish),
    eRollerCoasterCabin = EnumToInt(EnumFurnitureSubType.eRollerCoasterCabin),
    eRollerCoasterTrack = EnumToInt(EnumFurnitureSubType.eRollerCoasterTrack),
    eRollerCoasterStation = EnumToInt(EnumFurnitureSubType.eRollerCoasterStation),
    eInstrument = EnumToInt(EnumFurnitureSubType.eInstrument),
    eMaxType = EnumToInt(EnumFurnitureSubType.eMaxType),
}
local EnumHouseSceneType = import "L10.Game.EnumHouseSceneType"
EnumHouseSceneType_lua = {
    eAllScene = EnumToInt(EnumHouseSceneType.eAllScene),
    eRoom = EnumToInt(EnumHouseSceneType.eRoom),
    eYard = EnumToInt(EnumHouseSceneType.eYard),
    eXiangfang = EnumToInt(EnumHouseSceneType.eXiangfang)
}
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
EnumPlayerInfoContext_lua = {
    Undefined = EnumToInt(EnumPlayerInfoContext.Undefined),
    ChatLink = EnumToInt(EnumPlayerInfoContext.ChatLink),
    PlayerInChannel = EnumToInt(EnumPlayerInfoContext.PlayerInChannel),
    FriendInFriendWnd = EnumToInt(EnumPlayerInfoContext.FriendInFriendWnd),
    EnemyInFriendWnd = EnumToInt(EnumPlayerInfoContext.EnemyInFriendWnd),
    RecentInFriendWnd = EnumToInt(EnumPlayerInfoContext.RecentInFriendWnd),
    RecentInteractionInFriendWnd = EnumToInt(EnumPlayerInfoContext.RecentInteractionInFriendWnd),
    BlacklistInFriendWnd = EnumToInt(EnumPlayerInfoContext.BlacklistInFriendWnd),
    CloseFriendInFriendWnd = EnumToInt(EnumPlayerInfoContext.CloseFriendInFriendWnd),
    CurrentTarget = EnumToInt(EnumPlayerInfoContext.CurrentTarget),
    RankList = EnumToInt(EnumPlayerInfoContext.RankList),
    GuildMember = EnumToInt(EnumPlayerInfoContext.GuildMember),
    GuildApplyMember = EnumToInt(EnumPlayerInfoContext.GuildApplyMember),
    MPTZ = EnumToInt(EnumPlayerInfoContext.MPTZ),
    FamilyTree = EnumToInt(EnumPlayerInfoContext.FamilyTree),
    LBS = EnumToInt(EnumPlayerInfoContext.LBS),
    Team = EnumToInt(EnumPlayerInfoContext.Team),
    DouhunTeam = EnumToInt(EnumPlayerInfoContext.DouhunTeam),
    DouhunSelfTeam = EnumToInt(EnumPlayerInfoContext.DouhunSelfTeam),
    QianNvZhiDao = EnumToInt(EnumPlayerInfoContext.QianNvZhiDao),
    QMPK = EnumToInt(EnumPlayerInfoContext.QMPK),
    QMPKSelfTeam = EnumToInt(EnumPlayerInfoContext.QMPKSelfTeam),
    Keju = EnumToInt(EnumPlayerInfoContext.Keju),
    PersonalSpace = EnumToInt(EnumPlayerInfoContext.PersonalSpace),
    TeamGroup = EnumToInt(EnumPlayerInfoContext.TeamGroup)
}
local EnumGender = import "L10.Game.EnumGender"
EnumGender_lua = {
    Male = EnumToInt(EnumGender.Male),
    Female = EnumToInt(EnumGender.Female),
    Undefined = EnumToInt(EnumGender.Undefined)
}
local EnumPlayTimesKey = import "L10.Game.EnumPlayTimesKey"
EnumPlayTimesKey_lua = {
    eJuQingDaily = EnumToInt(EnumPlayTimesKey.eJuQingDaily),
    eGuildShouWei = EnumToInt(EnumPlayTimesKey.eGuildShouWei),
    eGnjcPlay = EnumToInt(EnumPlayTimesKey.eGnjcPlay),
    eGuildFreightGiveHelp = EnumToInt(EnumPlayTimesKey.eGuildFreightGiveHelp),
    eGuildFreightHelpByOther = EnumToInt(EnumPlayTimesKey.eGuildFreightHelpByOther),
    eMPTZ = EnumToInt(EnumPlayTimesKey.eMPTZ),
    eCarnivalJadeUsed = EnumToInt(EnumPlayTimesKey.eCarnivalJadeUsed),
    eCarnivalLotteryNumberUsed = EnumToInt(EnumPlayTimesKey.eCarnivalLotteryNumberUsed),
    eNationalDayParadeRewardFlag = EnumToInt(EnumPlayTimesKey.eNationalDayParadeRewardFlag),
    eTianJiangFuDanHachTimes = EnumToInt(EnumPlayTimesKey.eTianJiangFuDanHachTimes),
    eQMQDTimes = EnumToInt(EnumPlayTimesKey.eQMQDTimes),
    eNSDZZReward = EnumToInt(EnumPlayTimesKey.eNSDZZReward),
    eHuiLiuCloseFriendFlag = EnumToInt(EnumPlayTimesKey.eHuiLiuCloseFriendFlag),
    eSendChristmasGift = EnumToInt(EnumPlayTimesKey.eSendChristmasGift),
    eSendChristmasSmallGift = EnumToInt(EnumPlayTimesKey.eSendChristmasSmallGift),
    eNationalDayTXZRRewardTimes = EnumToInt(EnumPlayTimesKey.eNationalDayTXZRRewardTimes),
    eSendJueJiDisplayMailFlag = EnumToInt(EnumPlayTimesKey.eSendJueJiDisplayMailFlag),
    eQingMing2020PvpRewardTimes = EnumToInt(EnumPlayTimesKey.eQingMing2020PvpRewardTimes),
    eShuangshiyi2020ZhongCao = 141,
    eShuangshiyi2020Voucher= 143,
    eShuangshiyi2020ClearTrolley = 140,
    eShuangshiyi2020LingyuConsumption = 144,
    eShengDanXueRenPlayTimes = 151, -- 圣诞雪人
    eUseKunXianSuoTimes = 156,
    eZNQZhiBo_Appointment = 162,--周年庆直播预约数据
    eAntiProfessionChangeScoreNum = 168, -- 职业克符每周转换积分数值
    eHouseFishing = 173,--海钓技能周熟练度上限
    eZhuangShiWuExchangeTimes = 179,	-- 废旧家具材料兑换主题家具次数
    eDouble11KYDRewardTimes = 186, -- 砍一刀奖励次数
    eXinShengHuaJiTimes = 194, -- 心生画集玩法上传画像次数
    eXinShengHuaJiAdjustLimit = 195, -- 心生画集玩法上传画像上限增加次数
    eDreamWish = 199, -- 梦境识愿
	eDreamWishAward = 200, -- 梦境识愿奖励
    eChristmas2021CardPutTimes = 206, -- 圣诞2021贺卡发送次数
	eXingyuEnterTimes = 207, -- 圣诞心语活动进入次数
    eRiddle2022GuessRemainTimes = 210, -- 元宵2022灯谜剩余次数
    eRiddle2022GuessCorrectTimes = 211, -- 元宵2022灯谜答对次数
    eRiddle2022TitleRewardTimes = 212, -- 元宵2022灯谜号奖励次数
    eSnowballFightRewardTimes = 213, -- 打雪仗奖励次数
    eTangYuan2022DailyRewardTimes = 218,--狂奔的汤圆
    eQingMing2022PVEAwardScore = 226, -- 2022清明 奖励积分
    eXinShengHuaJi_JoinFlag = 222, -- 心生画集参与标记
    eXinShengHuaJi_VoteNum = 224, -- 心生画集现存投票次数
    eShuiManJinShanPassTimes = 234, -- 水漫金山-通关次数
    eShuiManJinShanPassThisWeek = 235, -- 水漫金山-本周通关次数
    eXinShengHuaJi_RankPicShareTimes = 237, --心生画集排行榜画作分享次数
    eQiXi2022SFEQ_DailyEnterTimes = 243, --2022七夕随风而去玩家每天进入玩法次数
    eQiXi2022SFEQ_FinalRewardTime = 244, --2022七夕随风而去玩家获得最终奖励次数
    eShuiManJinShanPassTimes_1001 = 246, -- 水漫金山-单阵营通关次数_1001
    eShuiManJinShanPassTimes_1002 = 247, -- 水漫金山-单阵营通关次数_1002
    eZhongQiu2022JYDSY_PartakeReward = 251, -- 2022中秋副本每日参与奖
    eZhongQiu2022JYDSY_WinReward = 252, -- 2022中秋副本每日胜利奖
    eDouble11MQHXJoinRewardTimes = 255, -- 2022双十一猛犬横行参与获奖次数
    eDouble11MQHXScoreRewardTimes = 256, -- 2022双十一猛犬横行积分获奖次数
    eWorldCup2022GJZL_RewardTime = 257, -- 2022世界杯副本冠军之路奖励次数
    eWorldCup2022JZLY_DailyScore = 260, -- 2022世界杯决战绿茵每日积分
    eDouble11SZTBRewardTimes = 261,  -- 2022双十一深宅探宝获奖次数
    eJinLuHunYuanZhanTimes = 263,  -- 2022国庆PVP玩法参与次数
    eXiangYaoYuQiuSi = 264, -- 2022GuoQing
    eDouble11WebPayZhongCaoRewardTimes = 266, -- 2022双十一Web支付种草礼包获取次数
    eDouble11ZhongCaoAdjustLimit = 267, -- 2022双十一种草额外投票次数
    eSpring2023SLTT_DailyRewardTimes = 274, -- 春节2023-狩猎饕餮每日奖励次数
	eSpring2023SLTT_DifficultRewardTimes = 275, -- 春节2023-狩猎饕餮困难模式奖励次数
	eSpring2023TTSY_ChampionTimes = 277, -- 春节2023-饕餮盛宴冠军奖励标记
    eSpring2023TTSY_DailyRewardTimes_1 = 278, -- 春节2023-饕餮盛宴每日奖励次数_1
	eSpring2023TTSY_DailyRewardTimes_2 = 279, -- 春节2023-饕餮盛宴每日奖励次数_2
	eSpring2023TTSY_DailyRewardTimes_3 = 280, -- 春节2023-饕餮盛宴每日奖励次数_3
	eSpring2023_HongBaoJade = 283, -- 春节2023-红包计数
	eQingMing2023PVPEngageAward = 296,  -- 清明2023PVP参与奖获得次数
	eQingMing2023PVPWinnerAward = 297,  -- 清明2023PVP优胜奖获得次数
	eQingMing2023PVP1st = 298,  -- 清明2023PVP第一名次数
	eQingMing2023PVE = 299,  -- 清明2023PVE奖励次数
    eEndlessChallengeBaseDailyAwardTimes = 303, -- 端午2023 无尽挑战每日基础奖励次数
	eEndlessChallengeAdvancedDailyAwardTimes = 304, -- 端午2023 无尽挑战每日进阶奖励次数
    eAnniv2023ZaiZai_DailyRewardTimes = 305, -- 2023周年庆崽崽玩法每日奖励次数
    eAnniv2023ZaiZai_DailySurpriseRewardTimes = 306, -- 2023周年庆崽崽玩法每日触发彩蛋次数
    eFenYongZhenXianWinTimes = 309,	-- 胜败奖次数
    eFenYongZhenXianExtraTimes = 311,	-- 卓越奖次数
    eXinXiangShiChengExchangeTimes = 312,  -- 2023五一心想事成兑换金币次数
    eAnniv2023FamilyTreeShowConfidantFlag = 313,
    eNavalWarPveEasyRewardTimes = 315,  -- 海战PVE奖励次数

	eNavalWarPvpElo = 318, -- 海战pvp副本匹配分
    eAsianGames2023JueDouDailyAwardTimes = 320, -- 亚运会2023 每日决斗奖励获得次数
    eZhongYuanPuDuNormalRewardTimes = 321, -- 中元节2023中元普渡普通模式每日奖励次数
	eZhongYuanPuDuHardRewardTimes = 322, -- 中元节2023中元普渡困难模式每日奖励次数
	eNavalWarPvpRewardTimes = 323, -- 海战pvp副本周奖励次数
    eWTSSEasyTimes = 328, 
    eWTSSHardTimes = 329, 
    eNavalWarPveHardRewardTimes = 330, 
    eGuildPinTuMonthlyTimes = 334,  -- 帮会拼图每月拼图次数
    eYaoYeManJuan_DailyRewardTimes = 335, -- 妖叶漫卷每日奖励次数
    eDouble11MQLDDailyAwardTimes = 336,
    eDouble11MQLDEliteAwardTimes = 337,
    eDouble11BYDLYDailyAwardTimes = 338, -- 2023双十一宝屿夺灵玉每日奖励次数
    eDouble11BYDLYHeroAwardTimes = 339, -- 2023双十一宝屿夺灵玉英雄难度奖励次数
    eDouble11BYDLYEliteAwardTimes = 340, -- 2023双十一宝屿夺灵玉精英奖励次数
    eDouble11BYDLYHeroWeekJoinTimes = 341, -- 2023双十一宝屿夺灵玉每周英雄难度参与次数

    eDouble11RechargeLotteryIsSkipAnimation = 344, -- 2023双十一充值抽奖是否跳过动画
    eDouble11CouponsSingleDrawTimes = 345, -- 2023双十一外观折扣券单抽次数
    eYaoYeManJuan_WeeklyRewardTimes = 348, -- 妖叶漫卷每周奖励次数
    
	eCandyDeliveryJoinDailyAwardTimes = 352, -- 2023万圣节糖果外卖玩法参与奖每日获奖次数
	eCandyDeliveryWinnerDailyAwardTimes = 353, -- 2023万圣节糖果外卖玩法最佳骑手奖每日获奖次数

    eChristmas2023GetCard = 346, -- 2023圣诞节 给自己的礼物 每日合成卡片奖励
    eBingXueBingWorldPlayDailyAwardTimes = 350, -- 2023圣诞节 冰雪大世界 每日获奖次数
    eChristmas2023GuaGuaKaTimes = 356, -- 2023圣诞节 刮刮卡 刮卡数
    eChristmas2023GuaGuaKaBaoDiTimes = 357, -- 2023圣诞节 刮刮卡 领取保底奖励次数
    eHanJia2024InvertedWorld_EntrustScore = 358,-- 2024寒假倒置世界委托积分
    eHanJia2024InvertedWorld_ResetTimes = 359, -- 2024寒假倒置世界委托任务重置次数
    eHanJia2024FurySnowman_RewardTimes = 361, -- 2024寒假不甘心的雪人每周奖励次数
    eHanJia2024FurySnowman_PassStatus = 362, -- 2024寒假不甘心的雪人通关状态，0未通关 1通关普通 2通关困难
	eXNCGDailyAwardTimes = 363 -- 元旦新年闯关每日奖励次数

}
local EnumHongbaoType = import "L10.UI.EnumHongbaoType"
EnumHongbaoType_lua = {
    WorldHongbao = EnumToInt(EnumHongbaoType.WorldHongbao),
    GuildHongbao = EnumToInt(EnumHongbaoType.GuildHongbao),
    SystemHongbao = EnumToInt(EnumHongbaoType.SystemHongbao),
    Gift = EnumToInt(EnumHongbaoType.Gift),
    SystemGuildHongbao = EnumToInt(EnumHongbaoType.SystemGuildHongbao),
    SectHongbao = EnumToInt(EnumHongbaoType.SectHongbao)
}
local AISkillType = import "L10.Game.AISkillType"
AISkillType_lua = {
    MPTZ = EnumToInt(AISkillType.MPTZ),
    MPTZ_YLMale = 2,
    MPTZ_YLFemale = 3,
    MPTZ_YLMonster = 4,
    MAX = EnumToInt(AISkillType.MAX)
}
local SkillKind = import "L10.Game.SkillKind"
SkillKind_lua = {
    CommonSkill = EnumToInt(SkillKind.CommonSkill),
    TianFuSkill = EnumToInt(SkillKind.TianFuSkill)
}
local EnumMapiGender = import "L10.Game.EnumMapiGender"
EnumMapiGender_lua = {
    eMale = EnumToInt(EnumMapiGender.eMale),
    eFemale = EnumToInt(EnumMapiGender.eFemale)
}
local EnumMapiQingxu = import "L10.Game.EnumMapiQingxu"
EnumMapiQingxu_lua = {
    eAngry = EnumToInt(EnumMapiQingxu.eAngry),
    eUnhappy = EnumToInt(EnumMapiQingxu.eUnhappy),
    eNormal = EnumToInt(EnumMapiQingxu.eNormal),
    eHappy = EnumToInt(EnumMapiQingxu.eHappy)
}
local EnumCommonForce = import "L10.UI.EnumCommonForce"
EnumCommonForce_lua = {
    eDefend = EnumToInt(EnumCommonForce.eDefend),
    eAttack = EnumToInt(EnumCommonForce.eAttack),
    eNeutral = EnumToInt(EnumCommonForce.eNeutral)
}
local LivingSkillType = import "L10.Game.LivingSkillType"
LivingSkillType_lua = {
    Fishing = EnumToInt(LivingSkillType.Fishing),
    Collect = EnumToInt(LivingSkillType.Collect),
    Makedrug = EnumToInt(LivingSkillType.Makedrug),
    Cooking = EnumToInt(LivingSkillType.Cooking),
    Craft = EnumToInt(LivingSkillType.Craft)
}
local EnumChuanjiabaoKind = import "L10.Game.EnumChuanjiabaoKind"
EnumChuanjiabaoKind_lua = {
    Simple = EnumToInt(EnumChuanjiabaoKind.Simple),
    Level1 = EnumToInt(EnumChuanjiabaoKind.Level1),
    Level2 = EnumToInt(EnumChuanjiabaoKind.Level2),
    Level3 = EnumToInt(EnumChuanjiabaoKind.Level3),
    Level4 = EnumToInt(EnumChuanjiabaoKind.Level4)
}
local EnumGuildLeagueCallUpType = import "L10.Game.EnumGuildLeagueCallUpType"
EnumGuildLeagueCallUpType_lua = {
    eVice = EnumToInt(EnumGuildLeagueCallUpType.eVice),
    eMain = EnumToInt(EnumGuildLeagueCallUpType.eMain),
    eBoth = EnumToInt(EnumGuildLeagueCallUpType.eBoth)
}
local EGuildJob = import "L10.Game.EGuildJob"
EGuildJob_lua = {
    BangZhu = EnumToInt(EGuildJob.BangZhu),
    FuBangZhu = EnumToInt(EGuildJob.FuBangZhu),
    ZhangLao = EnumToInt(EGuildJob.ZhangLao),
    TangZhu = EnumToInt(EGuildJob.TangZhu),
    XiangZhu = EnumToInt(EGuildJob.XiangZhu),
    JingYing = EnumToInt(EGuildJob.JingYing),
    BangZhong = EnumToInt(EGuildJob.BangZhong),
    HuFa = EnumToInt(EGuildJob.HuFa),
    XueTu = EnumToInt(EGuildJob.XueTu)
}
local ESortType = import "L10.UI.CGuildLeagueBattleResultInfoPane+ESortType"
ESortType_lua = {
    Level = EnumToInt(ESortType.Level),
    KillNum = EnumToInt(ESortType.KillNum),
    DieNum = EnumToInt(ESortType.DieNum),
    BaoShiNum = EnumToInt(ESortType.BaoShiNum),
    FuHuoNum = EnumToInt(ESortType.FuHuoNum),
    ControlNum = EnumToInt(ESortType.ControlNum),
    RmControlNum = EnumToInt(ESortType.RmControlNum),
    DpsNum = EnumToInt(ESortType.DpsNum),
    UnderDamage = EnumToInt(ESortType.UnderDamage),
    HealNum = EnumToInt(ESortType.HealNum),
    RepairNum = EnumToInt(ESortType.RepairNum),
    Default = EnumToInt(ESortType.Default)
}
local ESortType_CGuildLeagueConveneSortMgr = import "L10.Game.CGuildLeagueConveneSortMgr+ESortType"
ESortType_CGuildLeagueConveneSortMgr_lua = {
    ClsName = EnumToInt(ESortType_CGuildLeagueConveneSortMgr.ClsName),
    Level = EnumToInt(ESortType_CGuildLeagueConveneSortMgr.Level),
    Title = EnumToInt(ESortType_CGuildLeagueConveneSortMgr.Title),
    Xiuwei = EnumToInt(ESortType_CGuildLeagueConveneSortMgr.Xiuwei),
    Xiulian = EnumToInt(ESortType_CGuildLeagueConveneSortMgr.Xiulian),
    EquipScore = EnumToInt(ESortType_CGuildLeagueConveneSortMgr.EquipScore),
    ConveneState = EnumToInt(ESortType_CGuildLeagueConveneSortMgr.ConveneState),
    Default = EnumToInt(ESortType_CGuildLeagueConveneSortMgr.Default)
}
local EShopMallRegion = import "L10.UI.EShopMallRegion"
EShopMallRegion_lua = {
    ENewPlayerRecommand = EnumToInt(EShopMallRegion.ENewPlayerRecommand),
    ELingyuMall = EnumToInt(EShopMallRegion.ELingyuMall),
    ELingyuMallLimit = EnumToInt(EShopMallRegion.ELingyuMallLimit),
    EYuanBaoMall = EnumToInt(EShopMallRegion.EYuanBaoMall),
    EYuanbaoMarket = EnumToInt(EShopMallRegion.EYuanbaoMarket)
}
local TaskTrackDestEvent = import "L10.Game.TaskTrackDestEvent"
TaskTrackDestEvent_lua = {
    None = EnumToInt(TaskTrackDestEvent.None),
    KillMonster = EnumToInt(TaskTrackDestEvent.KillMonster),
    InteractWithNpc = EnumToInt(TaskTrackDestEvent.InteractWithNpc),
    UseItem = EnumToInt(TaskTrackDestEvent.UseItem),
    SubmitItem = EnumToInt(TaskTrackDestEvent.SubmitItem),
    BuyItem = EnumToInt(TaskTrackDestEvent.BuyItem),
    InteractWithGuildNpc = EnumToInt(TaskTrackDestEvent.InteractWithGuildNpc),
    BuyItemFromPlayerShop = EnumToInt(TaskTrackDestEvent.BuyItemFromPlayerShop),
    BuyItemFromMarket = EnumToInt(TaskTrackDestEvent.BuyItemFromMarket),
    FindNpcFirst = EnumToInt(TaskTrackDestEvent.FindNpcFirst),
    AnswerQuestion = EnumToInt(TaskTrackDestEvent.AnswerQuestion),
    ShowExpression = EnumToInt(TaskTrackDestEvent.ShowExpression),
    WipeGlass = EnumToInt(TaskTrackDestEvent.WipeGlass),
    BlowToNpc = EnumToInt(TaskTrackDestEvent.BlowToNpc),
    AnswerVoiceQuestion = EnumToInt(TaskTrackDestEvent.AnswerVoiceQuestion),
    MakeGesture = EnumToInt(TaskTrackDestEvent.MakeGesture),
    DoEvent = EnumToInt(TaskTrackDestEvent.DoEvent),
    WipeGlassNoSnow = EnumToInt(TaskTrackDestEvent.WipeGlassNoSnow),
    OpenWndUntilEventDone = EnumToInt(TaskTrackDestEvent.OpenWndUntilEventDone),
    BuyItemFromMall = EnumToInt(TaskTrackDestEvent.BuyItemFromMall),
    BuyItemFromYuanBaoMall = EnumToInt(TaskTrackDestEvent.BuyItemFromYuanBaoMall)
}
local EnumCommonPlayerListUpdateType = import "L10.Game.EnumCommonPlayerListUpdateType"
EnumCommonPlayerListUpdateType_lua = {
    Default = EnumToInt(EnumCommonPlayerListUpdateType.Default),
    AtGuildMember = EnumToInt(EnumCommonPlayerListUpdateType.AtGuildMember)
}
local EnumQueryOnlineGuildMembersContext = import "L10.Game.EnumQueryOnlineGuildMembersContext"
EnumQueryOnlineGuildMembersContext_lua = {
    GuildCC = EnumToInt(EnumQueryOnlineGuildMembersContext.GuildCC),
    AtGuildMember = EnumToInt(EnumQueryOnlineGuildMembersContext.AtGuildMember)
}
local EnumMapiLingfuPos = import "L10.Game.EnumMapiLingfuPos"
EnumMapiLingfuPos_lua = {
    eAll = EnumToInt(EnumMapiLingfuPos.eAll),
    eTou = EnumToInt(EnumMapiLingfuPos.eTou),
    eJing = EnumToInt(EnumMapiLingfuPos.eJing),
    eTun = EnumToInt(EnumMapiLingfuPos.eTun),
    eWei = EnumToInt(EnumMapiLingfuPos.eWei),
    eTui = EnumToInt(EnumMapiLingfuPos.eTui),
    eTi = EnumToInt(EnumMapiLingfuPos.eTi),
    ePosCount = EnumToInt(EnumMapiLingfuPos.ePosCount)
}
local EnumFurniturePlace = import "L10.Game.EnumFurniturePlace"
EnumFurniturePlace_lua = {
    eUnknown = EnumToInt(EnumFurniturePlace.eUnknown),
    eBag = EnumToInt(EnumFurniturePlace.eBag),
    eRepository = EnumToInt(EnumFurniturePlace.eRepository),
    eFurniture = EnumToInt(EnumFurniturePlace.eFurniture),
    eDecoration = EnumToInt(EnumFurniturePlace.eDecoration),
    eQiShuFurnitureStore = EnumToInt(EnumFurniturePlace.eQiShuFurnitureStore),
    eDiscard = EnumToInt(EnumFurniturePlace.eDiscard),
    eChuanjiabao = EnumToInt(EnumFurniturePlace.eChuanjiabao),
    eHuapen = EnumToInt(EnumFurniturePlace.eHuapen)
}
local JingLingEvaluateType = import "L10.Game.JingLingEvaluateType"
JingLingEvaluateType_lua = {
    None = EnumToInt(JingLingEvaluateType.None),
    Helpful = EnumToInt(JingLingEvaluateType.Helpful),
    Helpless = EnumToInt(JingLingEvaluateType.Helpless)
}
local EnumGameSetting = import "L10.Game.EnumGameSetting"
EnumGameSetting_lua = {
    AutoHeal = EnumToInt(EnumGameSetting.AutoHeal),
    IpRegion = EnumToInt(EnumGameSetting.IpRegion),
    ShenBingColorationHideFx = EnumToInt(EnumGameSetting.ShenBingColorationHideFx),
    ShenBingColorationHideWing = EnumToInt(EnumGameSetting.ShenBingColorationHideWing),
    ShenBingColorationHideEquip = EnumToInt(EnumGameSetting.ShenBingColorationHideEquip),
    eMultipleExpAutoUseForYiTiaoLong = EnumToInt(EnumGameSetting.eMultipleExpAutoUseForYiTiaoLong),
    eCheckShenbinHideButton = EnumToInt(EnumGameSetting.eCheckShenbinHideButton),
    eTripleExpAutoUseForYiTiaoLong = EnumToInt(EnumGameSetting.eTripleExpAutoUseForYiTiaoLong),
    eUnLockXiaoYaoExpression = EnumToInt(EnumGameSetting.eUnLockXiaoYaoExpression),
    eMaxSize = EnumToInt(EnumGameSetting.eMaxSize)
}
local EnumPersistPlayDataKey = import "L10.Game.EnumPersistPlayDataKey"
EnumPersistPlayDataKey_lua = {
    eLingShouFreshPlay = EnumToInt(EnumPersistPlayDataKey.eLingShouFreshPlay),
    eKaiFuActivityInfo = EnumToInt(EnumPersistPlayDataKey.eKaiFuActivityInfo),
    eLingShouWash = EnumToInt(EnumPersistPlayDataKey.eLingShouWash),
    ePlayerShopCollections = EnumToInt(EnumPersistPlayDataKey.ePlayerShopCollections),
    ePaoShangVehicleInfo = EnumToInt(EnumPersistPlayDataKey.ePaoShangVehicleInfo),
    eGuideInfo = EnumToInt(EnumPersistPlayDataKey.eGuideInfo),
    eTotalLoginDaysInfo = EnumToInt(EnumPersistPlayDataKey.eTotalLoginDaysInfo),
    eLastRenameTimeByUsingItem = EnumToInt(EnumPersistPlayDataKey.eLastRenameTimeByUsingItem),
    eCcFollowing = EnumToInt(EnumPersistPlayDataKey.eCcFollowing),
    eSpecialFaBaoFX = EnumToInt(EnumPersistPlayDataKey.eSpecialFaBaoFX),
    eAnnouncementMailReadList = EnumToInt(EnumPersistPlayDataKey.eAnnouncementMailReadList),
    eShiJian = EnumToInt(EnumPersistPlayDataKey.eShiJian),
    eUnLockExpressionGroupInfo = EnumToInt(EnumPersistPlayDataKey.eUnLockExpressionGroupInfo),
    eSecondaryTalisman = EnumToInt(EnumPersistPlayDataKey.eSecondaryTalisman),
    eDieHaveReliveSelfBuff = EnumToInt(EnumPersistPlayDataKey.eDieHaveReliveSelfBuff),
    eExpressionAlert = EnumToInt(EnumPersistPlayDataKey.eExpressionAlert),
    eYXJWJRing = EnumToInt(EnumPersistPlayDataKey.eYXJWJRing),
    eShimenNeverRemindTrackToNextRound = EnumToInt(EnumPersistPlayDataKey.eShimenNeverRemindTrackToNextRound),
    ePlayerShopTemplateCollections = EnumToInt(EnumPersistPlayDataKey.ePlayerShopTemplateCollections),
    eCommonGameSetting = EnumToInt(EnumPersistPlayDataKey.eCommonGameSetting),
    eSpecialFaBaoFXTransfered = EnumToInt(EnumPersistPlayDataKey.eSpecialFaBaoFXTransfered),
    eIsLingShouFuTi = EnumToInt(EnumPersistPlayDataKey.eIsLingShouFuTi),

    eTransferTime = 34,
    eUnintermittentHellGuideCount = 163,
    eUnLockSpecialFaBaoFxLevelUp = 182,
    eTransferAntiProfession = 190,
	eAllowBigAmountPay = 191,
    eGuanTianXiangSkillData = 193,

    eShiMenMiShuSkillConsumeType = 194,
	eUnLockPaiZhaoLvJing = 208,
    eMeiXiangLou = 210, -- 媚香楼音游
    eMonitorChannel = 215,
    eHMTBindReward = 218,
    eProtectCampfire = 222,
    eMeiXiangLouOnceAward = 226,---- 媚香楼音游国手奖励
    eStoreRoomTaskProgress = 233,--府库建设进度
    eNavalWarTrainingReward = 235,--海战训练副本奖励
    eQiWeiFuLiGuanData = 244, -- 企微福利官相关数据
    eClubHousePassWord = 245, -- 上次使用的茶室密码
	eLuoXiaFengUnLockMap = 246, -- 落霞锋解锁地图
    eSwitchZuoQiPassengerModeData = 247, --单人双人坐骑切换
}
local EnumValentinePlayState = import "L10.UI.EnumValentinePlayState"
EnumValentinePlayState_lua = {
    eWaitForGirlAnswer = EnumToInt(EnumValentinePlayState.eWaitForGirlAnswer),
    eWaitForBoyAnswer = EnumToInt(EnumValentinePlayState.eWaitForBoyAnswer)
}
local EnumQueryOnlineReverseFriendsContext = import "L10.Game.EnumQueryOnlineReverseFriendsContext"
EnumQueryOnlineReverseFriendsContext_lua = {
    Undefined = EnumToInt(EnumQueryOnlineReverseFriendsContext.Undefined),
    GroupIM = EnumToInt(EnumQueryOnlineReverseFriendsContext.GroupIM),
    LingYuTradeSend = EnumToInt(EnumQueryOnlineReverseFriendsContext.LingYuTradeSend),
    MonthCardGiven = EnumToInt(EnumQueryOnlineReverseFriendsContext.MonthCardGiven),
    LoudSpeakerAtPlayer = 4,
    ShiTuWuZiQi = EnumToInt(EnumQueryOnlineReverseFriendsContext.ShiTuWuZiQi),
    --C#下面请顺序用小于1000的数字，Lua下请用1000以上的数字,C#登记在EnumQueryOnlineReverseFriendsContext
    AppearanceFashionPresent = 1000,
}
local EnumTradeSellType = import "L10.Game.EnumTradeSellType"
EnumTradeSellType_lua = {
    Silver = EnumToInt(EnumTradeSellType.Silver),
    Precious = EnumToInt(EnumTradeSellType.Precious)
}
local EnumQualityType = import "L10.Game.EnumQualityType"
EnumQualityType_lua = {
    None = EnumToInt(EnumQualityType.None),
    Dark = EnumToInt(EnumQualityType.Dark),
    White = EnumToInt(EnumQualityType.White),
    Yellow = EnumToInt(EnumQualityType.Yellow),
    Orange = EnumToInt(EnumQualityType.Orange),
    Blue = EnumToInt(EnumQualityType.Blue),
    Red = EnumToInt(EnumQualityType.Red),
    Purple = EnumToInt(EnumQualityType.Purple),
    Ghost = EnumToInt(EnumQualityType.Ghost),
    DarkBlue = EnumToInt(EnumQualityType.DarkBlue),
    DarkRed = EnumToInt(EnumQualityType.DarkRed),
    DarkPurple = EnumToInt(EnumQualityType.DarkPurple),
    DarkGhost = EnumToInt(EnumQualityType.DarkGhost)
}
local EnumBabyStatus = import "L10.Game.EnumBabyStatus"
EnumBabyStatus_lua = {
    eBorn = EnumToInt(EnumBabyStatus.eBorn),
    eChild = EnumToInt(EnumBabyStatus.eChild),
    eYoung = EnumToInt(EnumBabyStatus.eYoung)
}
local EnumPaizhaoRelation = import "CPaiZhaoMgr+EnumPaizhaoRelation"
EnumPaizhaoRelation_lua = {
    eAll = EnumToInt(EnumPaizhaoRelation.eAll),
    ePartner = EnumToInt(EnumPaizhaoRelation.ePartner),
    eBrother = EnumToInt(EnumPaizhaoRelation.eBrother),
    eMaster = EnumToInt(EnumPaizhaoRelation.eMaster),
    eApprentice = EnumToInt(EnumPaizhaoRelation.eApprentice),
    eFriend = EnumToInt(EnumPaizhaoRelation.eFriend),
    eGuildMember = EnumToInt(EnumPaizhaoRelation.eGuildMember),
    eOther = EnumToInt(EnumPaizhaoRelation.eOther)
}
local EnumPlayerLimitRpcOperationCode = import "L10.Game.EnumPlayerLimitRpcOperationCode"
EnumPlayerLimitRpcOperationCode_lua = {
    eEnter = EnumToInt(EnumPlayerLimitRpcOperationCode.eEnter),
    eLeave = EnumToInt(EnumPlayerLimitRpcOperationCode.eLeave)
}
local EnumSecondaryPasswordOperationType = import "L10.Game.EnumSecondaryPasswordOperationType"
EnumSecondaryPasswordOperationType_lua = {
    eSet = EnumToInt(EnumSecondaryPasswordOperationType.eSet),
    eClear = EnumToInt(EnumSecondaryPasswordOperationType.eClear),
    eUpdate = EnumToInt(EnumSecondaryPasswordOperationType.eUpdate),
    eVerify = EnumToInt(EnumSecondaryPasswordOperationType.eVerify),
    eClose = EnumToInt(EnumSecondaryPasswordOperationType.eClose),
    eProtectContent = EnumToInt(EnumSecondaryPasswordOperationType.eProtectContent)
}
local EPushType = import "L10.Game.EPushType"
EPushType_lua = {
    eFriend = EnumToInt(EPushType.eFriend)
}
local EnumQingQiuPlayState = import "L10.UI.EnumQingQiuPlayState"
EnumQingQiuPlayState_lua = {
    Idle = EnumToInt(EnumQingQiuPlayState.Idle),
    Part_1 = EnumToInt(EnumQingQiuPlayState.Part_1),
    Part_2 = EnumToInt(EnumQingQiuPlayState.Part_2),
    BossKilled = EnumToInt(EnumQingQiuPlayState.BossKilled)
}
local EChuanjiabaoType = import "L10.Game.EChuanjiabaoType"
EChuanjiabaoType_lua = {
    RenGe = EnumToInt(EChuanjiabaoType.RenGe),
    TianGe = EnumToInt(EChuanjiabaoType.TianGe),
    DiGe = EnumToInt(EChuanjiabaoType.DiGe)
}
local SearchOption = import "L10.UI.SearchOption"
SearchOption_lua = {
    All = EnumToInt(SearchOption.All),
    Normal = EnumToInt(SearchOption.Normal),
    Valueable = EnumToInt(SearchOption.Valueable),
    AllPublicity = EnumToInt(SearchOption.AllPublicity)
}
local EnumBuyItemFromPlayerShopFailedReason = import "L10.Game.EnumBuyItemFromPlayerShopFailedReason"
EnumBuyItemFromPlayerShopFailedReason_lua = {
    eCheckOperatePlayerShop = EnumToInt(EnumBuyItemFromPlayerShopFailedReason.eCheckOperatePlayerShop),
    eNotEnoughSpace = EnumToInt(EnumBuyItemFromPlayerShopFailedReason.eNotEnoughSpace),
    eNotEnoughSilver = EnumToInt(EnumBuyItemFromPlayerShopFailedReason.eNotEnoughSilver),
    eForbidUseSilverInTrade = EnumToInt(EnumBuyItemFromPlayerShopFailedReason.eForbidUseSilverInTrade),
    eSecondaryPasswordProtect = EnumToInt(EnumBuyItemFromPlayerShopFailedReason.eSecondaryPasswordProtect),
    eGmForbidBuyFromShop = EnumToInt(EnumBuyItemFromPlayerShopFailedReason.eGmForbidBuyFromShop),
    eNoShop = EnumToInt(EnumBuyItemFromPlayerShopFailedReason.eNoShop),
    eNoItem = EnumToInt(EnumBuyItemFromPlayerShopFailedReason.eNoItem),
    ePriceChange = EnumToInt(EnumBuyItemFromPlayerShopFailedReason.ePriceChange),
    eItemNumNotEnough = EnumToInt(EnumBuyItemFromPlayerShopFailedReason.eItemNumNotEnough),
    eShopPreciousItemTradeFundAbnormal = EnumToInt(EnumBuyItemFromPlayerShopFailedReason.eShopPreciousItemTradeFundAbnormal),
    eShopItemLocked = EnumToInt(EnumBuyItemFromPlayerShopFailedReason.eShopItemLocked),
    eCommonDeviceLimit = EnumToInt(EnumBuyItemFromPlayerShopFailedReason.eCommonDeviceLimit),
    ePropertyProtect = EnumToInt(EnumBuyItemFromPlayerShopFailedReason.ePropertyProtect),
    eItemExpired = EnumToInt(EnumBuyItemFromPlayerShopFailedReason.eItemExpired)
}
local PkProtect = import "L10.Game.PkProtect"
PkProtect_lua = {
    Team = EnumToInt(PkProtect.Team),
    Guild = EnumToInt(PkProtect.Guild),
    GreenName = EnumToInt(PkProtect.GreenName),
    Grade = EnumToInt(PkProtect.Grade),
    OtherServer = EnumToInt(PkProtect.OtherServer),
    Group = EnumToInt(PkProtect.Group),
    Sect = EnumToInt(PkProtect.Sect),
}
local EnumPropertyAppearanceSettingBit = import "L10.Game.CPropertyAppearance+EnumPropertyAppearanceSettingBit"
EnumPropertyAppearanceSettingBit_lua = {
    HideSuitIdToOtherPlayer = EnumToInt(EnumPropertyAppearanceSettingBit.HideSuitIdToOtherPlayer),
    HideGemFxToOtherPlayer = EnumToInt(EnumPropertyAppearanceSettingBit.HideGemFxToOtherPlayer),
    HideHeadFashionEffect = EnumToInt(EnumPropertyAppearanceSettingBit.HideHeadFashionEffect),
    HideBodyFashionEffect = EnumToInt(EnumPropertyAppearanceSettingBit.HideBodyFashionEffect),
    HideWing = EnumToInt(EnumPropertyAppearanceSettingBit.HideWing),
    HideWeaponFashionEffect = EnumToInt(EnumPropertyAppearanceSettingBit.HideWeaponFashionEffect),
    HideHelmetEffect = EnumToInt(EnumPropertyAppearanceSettingBit.HideHelmetEffect),
    FeiShengAppearanceXianFanStatus = EnumToInt(EnumPropertyAppearanceSettingBit.FeiShengAppearanceXianFanStatus),
    FeiShengPlayXianFanStatus = EnumToInt(EnumPropertyAppearanceSettingBit.FeiShengPlayXianFanStatus),
    HideDunPai = EnumToInt(EnumPropertyAppearanceSettingBit.HideDunPai),
    HideShenBingEffect = EnumToInt(EnumPropertyAppearanceSettingBit.HideShenBingEffect),
    HideShenBingClothes = EnumToInt(EnumPropertyAppearanceSettingBit.HideShenBingClothes),
    HideShenBingHeadwear = EnumToInt(EnumPropertyAppearanceSettingBit.HideShenBingHeadwear),
    IsNewHeadId = EnumToInt(EnumPropertyAppearanceSettingBit.IsNewHeadId),
    Hide150GhostClothes = EnumToInt(EnumPropertyAppearanceSettingBit.Hide150GhostClothes),
    Hide150GhostHeadwear = EnumToInt(EnumPropertyAppearanceSettingBit.Hide150GhostHeadwear),
    HideShenBingBackOrnament = EnumToInt(EnumPropertyAppearanceSettingBit.HideShenBingBackOrnament),
    ShowShenBingBeiShiColor = EnumToInt(EnumPropertyAppearanceSettingBit.ShowShenBingBeiShiColor),
    HideBackPendant = EnumToInt(EnumPropertyAppearanceSettingBit.HideBackPendant),
    HideTouSha = EnumToInt(EnumPropertyAppearanceSettingBit.HideTouSha),
    HideBackPendantFashionEffect = EnumToInt(EnumPropertyAppearanceSettingBit.HideBackPendantFashionEffect),
    HideSubHandWeaponFashionEffect = EnumToInt(EnumPropertyAppearanceSettingBit.HideSubHandWeaponFashionEffect),
    HideZhanKuangMask = EnumToInt(EnumPropertyAppearanceSettingBit.HideZhanKuangMask),
    HideSkillAppearance = EnumToInt(EnumPropertyAppearanceSettingBit.HideSkillAppearance),
    HideTalismanAppearance = EnumToInt(EnumPropertyAppearanceSettingBit.HideTalismanAppearance),

}
local EnumLingShouQuality = import "L10.Game.EnumLingShouQuality"
EnumLingShouQuality_lua = {
    Ji = EnumToInt(EnumLingShouQuality.Ji),
    Wu = EnumToInt(EnumLingShouQuality.Wu),
    Ding = EnumToInt(EnumLingShouQuality.Ding),
    Bing = EnumToInt(EnumLingShouQuality.Bing),
    Yi = EnumToInt(EnumLingShouQuality.Yi),
    Jia = EnumToInt(EnumLingShouQuality.Jia)
}
local EnumStatType = import "L10.Game.EnumStatType"
EnumStatType_lua = {
    Undefined = EnumToInt(EnumStatType.Undefined),
    eDps = EnumToInt(EnumStatType.eDps),
    eHeal = EnumToInt(EnumStatType.eHeal),
    eSuffer = EnumToInt(EnumStatType.eSuffer),
    eGuildHero = EnumToInt(EnumStatType.eGuildHero)
}
local EnumFurnitureLocation = import "L10.Game.EnumFurnitureLocation"
EnumFurnitureLocation_lua = {
    eUnknown = EnumToInt(EnumFurnitureLocation.eUnknown),
    eYardLand = EnumToInt(EnumFurnitureLocation.eYardLand),
    eRoomLand = EnumToInt(EnumFurnitureLocation.eRoomLand),
    eLand = EnumToInt(EnumFurnitureLocation.eLand),
    eRoomWall = EnumToInt(EnumFurnitureLocation.eRoomWall),
    eDesk = EnumToInt(EnumFurnitureLocation.eDesk),
    eXiangfangLand = EnumToInt(EnumFurnitureLocation.eXiangfangLand),
    eDeskOrLand = EnumToInt(EnumFurnitureLocation.eDeskOrLand),
    eYardFenceAndRoomWall = EnumToInt(EnumFurnitureLocation.eYardFenceAndRoomWall),
    eWater = EnumToInt(EnumFurnitureLocation.eWater),
    ePool = EnumToInt(EnumFurnitureLocation.ePool),
    eWarmPool = EnumToInt(EnumFurnitureLocation.eWarmPool),
}
local EnumHongBaoEvent = import "L10.UI.EnumHongBaoEvent"
EnumHongBaoEvent_lua = {
    None = EnumToInt(EnumHongBaoEvent.None),
    BangHua = EnumToInt(EnumHongBaoEvent.BangHua),
    HuaKui = EnumToInt(EnumHongBaoEvent.HuaKui),
    SystemForPlayer = EnumToInt(EnumHongBaoEvent.SystemForPlayer)
}

local EnumLibaoType = import "L10.UI.EnumLibaoType"
EnumLibaoType_lua = {
    WorldLibao = EnumToInt(EnumLibaoType.WorldLibao),
    GuildLibao = EnumToInt(EnumLibaoType.GuildLibao),
    SystemLibao = EnumToInt(EnumLibaoType.SystemLibao)
}
local EnumGuildFreightStatus = import "L10.Game.EnumGuildFreightStatus"
EnumGuildFreightStatus_lua = {
    eNotFilled = EnumToInt(EnumGuildFreightStatus.eNotFilled),
    eFilled = EnumToInt(EnumGuildFreightStatus.eFilled),
    eNotFilledNeedHelp = EnumToInt(EnumGuildFreightStatus.eNotFilledNeedHelp),
    eFilledByHelp = EnumToInt(EnumGuildFreightStatus.eFilledByHelp)
}
local EnumItemAccessType = import "L10.Game.EnumItemAccessType"
EnumItemAccessType_lua = {
    PlayerShop = EnumToInt(EnumItemAccessType.PlayerShop),
    LingyuMall = EnumToInt(EnumItemAccessType.LingyuMall),
    YuanbaoMall = EnumToInt(EnumItemAccessType.YuanbaoMall),
    LifeSkill = EnumToInt(EnumItemAccessType.LifeSkill),
    Yishi = EnumToInt(EnumItemAccessType.Yishi),
    Monster = EnumToInt(EnumItemAccessType.Monster),
    NPCShop = EnumToInt(EnumItemAccessType.NPCShop),
    MPTZShop = EnumToInt(EnumItemAccessType.MPTZShop),
    GuanningShop = EnumToInt(EnumItemAccessType.GuanningShop),
    Lingshou = EnumToInt(EnumItemAccessType.Lingshou),
    CWJXAct = EnumToInt(EnumItemAccessType.CWJXAct),
    QingyiShop = EnumToInt(EnumItemAccessType.QingyiShop),
    CustomMessage = EnumToInt(EnumItemAccessType.CustomMessage),
    DuobaoShop = EnumToInt(EnumItemAccessType.DuobaoShop),
    HouseFarm = EnumToInt(EnumItemAccessType.HouseFarm),
    ZhulongshiCompose = EnumToInt(EnumItemAccessType.ZhulongshiCompose),
    LingshouDuihuan = EnumToInt(EnumItemAccessType.LingshouDuihuan),
    ZuowuHecheng = EnumToInt(EnumItemAccessType.ZuowuHecheng),
    FestivalPointShop = EnumToInt(EnumItemAccessType.FestivalPointShop),
    DisassembleGhostEquip = EnumToInt(EnumItemAccessType.DisassembleGhostEquip),
    HousePointShop = EnumToInt(EnumItemAccessType.HousePointShop),
    SchoolContribution = 22,
    PGQY = 23,
    JiaYuanTuZhi = 24,
    JiaYuanTuZhiSpecialScore = 25,
    GlobalSpokesmanScore = 26,
    SpokesmanHaiTangHua = 27,
    LianHua = 32,
    OfficialServerCustomMessage = EnumToInt(EnumItemAccessType.OfficialServerCustomMessage),
}

local EnumMoneyType = import "L10.Game.EnumMoneyType"
EnumMoneyType_lua = {
    Undefined = EnumToInt(EnumMoneyType.Undefined),
    YinLiang = EnumToInt(EnumMoneyType.YinLiang),--银两
    YinPiao = EnumToInt(EnumMoneyType.YinPiao),--银票
    YuanBao = EnumToInt(EnumMoneyType.YuanBao),--元宝
    LingYu = EnumToInt(EnumMoneyType.LingYu),--灵玉
    Score = EnumToInt(EnumMoneyType.Score),--积分
    Exp = EnumToInt(EnumMoneyType.Exp),--经验
    PrivateExp = EnumToInt(EnumMoneyType.PrivateExp),--专有经验
    GuildContri = EnumToInt(EnumMoneyType.GuildContri),--帮贡
    QnPoint = EnumToInt(EnumMoneyType.QnPoint),--倩女点
    Huoli = EnumToInt(EnumMoneyType.Huoli),--活力
    Xingmang = EnumToInt(EnumMoneyType.Xingmang),--
    QianShiExp = EnumToInt(EnumMoneyType.QianShiExp),--
    LingYu_WithBind = EnumToInt(EnumMoneyType.LingYu_WithBind),--包含绑定灵玉在内的所有灵玉
    LingYu_OnlyBind = EnumToInt(EnumMoneyType.LingYu_OnlyBind),--绑定灵玉
    CityWarMaterial = EnumToInt(EnumMoneyType.CityWarMaterial),-- 城战的资材
    MingWang = EnumToInt(EnumMoneyType.MingWang),--
    TiLi = EnumToInt(EnumMoneyType.TiLi),--宝宝的体力
    SkillPrivateSilver = EnumToInt(EnumMoneyType.SkillPrivateSilver),--修为专用银票
    NanDuTongBao = EnumToInt(EnumMoneyType.NanDuTongBao),--南都通宝
}

local EnumReportId = import "L10.Game.EnumReportId"
EnumReportId_lua = {
    eDefault = EnumToInt(EnumReportId.eDefault),
    eMengDaoInfo = EnumToInt(EnumReportId.eMengDaoInfo), -- 梦岛信息
    eMengDaoMoment = EnumToInt(EnumReportId.eMengDaoMoment), -- 梦岛心情
    eMengDaoMsg = EnumToInt(EnumReportId.eMengDaoMsg), -- 梦岛留言
    ePlotDiscuss = EnumToInt(EnumReportId.ePlotDiscuss), -- 剧情讨论
}

-- 临时积分 从1开始 当前最大值64
EnumTempPlayScoreKey_lua = {
	None = 0,
	Chunjie2021 = 1, -- 2021-3-5 00:00 这个数据过期
    QmpkJingCai = 2,
    --GLCTrainScore = 3, -- 持续存在两周 每一届跨服联赛均可使用
    SnowballFightScore = 4,
    GLCTrainScore = 129, --洪磊把服务器端的key换掉了，客户端保持一致
    ZhanLingScore = 130,--葫芦娃战令积分
    DuanWu2022FKPP = 131, -- 疯狂泡泡积分
    WorldCup2022JZLY = 132, -- 2022世界杯积分
    HanJiaScore = 133, --2023寒假积分
    DuanWu2023Score = 134, -- 2023 端午活动积分
    ShuJia2023Score = 135, -- 2023 暑假活动积分
    Christmas2023Score = 136, -- 2023圣诞节冰雪大世界积分
	Max = 256,
}

EnumJieLiangYuanStage = {
	ePrepare = 0,
	eNpcPuzzle = 1,
	ePickPuzzle = 2,
	eFindXinNiang = 3,
	eKaiBaoXiang = 4,
	eVoteNeiGui = 5,
	eFinish = 6,
}

EnumPlayResult = {
    eVictory = 0,
    eLose = 1,
    eDraw = 2,
}

EnumSnowballFightPlayResult = {
	Tie = 0,
	Win = 1,
	Lose = 2,
}

EnumCurlingState = {
	Idle = 101,
	WaitThrow = 102,
	WaitRoundEnd = 103,
	RoundEnd = 104,
	Reward = 105,
	End = 106,
}

EnumSnowballFightState = {
	Idle = 101,
	Playing = 102,
	Reward = 103,
	End = 104,
}

EnumFireworkPartyStatus = {
    eLocked = 1,
	eWaiting = 2,
	eStart = 3,
	eEnd = 4,
}

--meixianglou
EnumButtonPressPerform = {
	Miss = 1,
	Ok = 2,
	Good = 3,
	Perfect = 4,
    Ignore = 5,
}

EnumStarBiwuFightStage = {
	eInit 			= 0,
	eInPaimai 		= 1,
	ePaimaiEnd 		= 2,
	eBiwuBegin 		= 3,
	eBiwuEnd 		= 4,
	eQieCuoSai 		= 5,
	ePiPeiSai   	= 6,
	eJiFenSai   	= 7,
	eXiaoZuSai  	= 8,
	eZongJueSai 	= 9,
	eReShenSai  	= 10,
	eQD_ReShenSai  	= 11, -- 渠道热身赛
	eQD_ZhengShiSai = 12, -- 渠道正式赛
}local EnumReliveStatus = import "L10.Game.EnumReliveStatus"
EnumReliveStatus_lua = {
    eOnline = EnumToInt(EnumReliveStatus.eOnline), 
    eReLogin = EnumToInt(EnumReliveStatus.eReLogin)
}
local EnumSceneReliveType = import "L10.Game.EnumSceneReliveType"
EnumSceneReliveType_lua = {
    eInPlaceAndDefault = EnumToInt(EnumSceneReliveType.eInPlaceAndDefault), 
    eNoReborn = EnumToInt(EnumSceneReliveType.eNoReborn), 
    eNoChoice = EnumToInt(EnumSceneReliveType.eNoChoice), 
    eInPlace = EnumToInt(EnumSceneReliveType.eInPlace), 
    eDefault = EnumToInt(EnumSceneReliveType.eDefault), 
    eOnlyUseSkill = EnumToInt(EnumSceneReliveType.eOnlyUseSkill)
}


EnumDouHunLocalStage = {
	eSignUp = 1,
	eVote = 2,
	eChlgPrepare = 3,
	eChlgStart = 4,
	eChlgFinish = 5,
	eTrainStart = 6,
	eTrainFinish = 7,
	eFuTiStart = 8,
	eFinish = 9,
}

EnumDouHunCrossStage = {
	eEnd = 0,
	eWaiKaPrepare = 101,
	eWaiKa = 102,
	eWaiKaEnd = 103,
	eMainPrepare = 104,
	eMain = 105,
}

EnumDouHunStage = {
	SignUp = 1,
	Vote = 2,
	Select = 3,
	Foster = 4,
	WildCard = 5,
	MainRace = 6,
	Champion = 7,
}

EnumDouble11MQHXPlayState = {
    Idle = 1001,
    Normal = 1002,
    Warning = 1003,
    Crazy = 1004,
    Reward = 1005,
    GameFinish = 1006,
}

EnumDouble11MQLDPlayState = {
    ePrepare = 1,
    eFirstStage = 2,
    eSecondStage = 3,
    eEnd = 4
}

EnumDouble11SZTBPlayState = {
    Idle = 1001,
    Stage_1_Scenario = 1011,
    Stage_1_Prepare = 1012,
    Stage_1_Playing = 1013,
    Stage_1_Finish = 1014,
    Stage_2 = 1021,
    Stage_2_Scenario = 1022,
    Stage_3 = 1031,
    Reward = 1041,
    GameFinish = 1051,
}

EnumDouble11MQHXAction = {
    Fly = 1,
}

EnumStarBiwuFightActivityStatus = {
    eInSignUpNoRight    = 1, -- 报名阶段，无权限
    eInSignUpHasRight   = 2, -- 报名阶段，有权限
    eAfterSignupNoMatch = 3, -- 报名结束,无比赛
    eAfterSignupInMatch = 4, -- 报名结束,有比赛
}

EnumSectXiuxingPlayStage = {
    eSignup     = 1, -- 报名阶段
    eRunning    = 2, -- 进行中
    eBoss       = 3, -- BOSS战
    eEnd        = 4, -- 活动结束
}

EnumTurkeyMatchPlayState = {
    ePrepare = 1,
    eCountDown = 2,
    eFirstStage = 3,
    eFirstStageExtra = 4,
    eInterval = 5,
    eSecondStage = 6,
    eFinish = 7
}

EnumClientLogCountKey = {
    TeamRecruit_Check = 1,
    TeamRecruit_Contact = 2,
    RecommendSkills = 3,
}

EnumStarBiwuFightCommonResult = {
	eNotBegin 			= 0, -- 尚未开始
	eNoResultYet 		= 1, -- 正在打，结果没有出
	eHasReult           = 2, -- 结果出来了
	eWinByFight			= 3, -- 没人弃权,打赢了
	eLossByFight		= 4, -- 打输了
	eEvenBothQuit 		= 5, -- 平了
	eWinByEnemyQuit		= 6, -- 赢了,对手弃权
	eLossByQuit 		= 7, -- 输了,自己弃权
}

EnumXianHaiTongXingDataType = {
    eWeek = 1,
    eReward = 2,
    eLeaveTime = 3,
    eTaskCount = 4,
    eTaskStatus = 5,
    eOldProgress = 6,
    eTeamId = 7,
    eIsLeader = 8,
}

EnumYaoYeManJuanPlayState = {
	Idle    = 1001,     --初始
	Stage_1 = 1002,	    --阶段1
	Stage_2 = 1003,	    --阶段2
	Reward  = 1004,	    --玩法成功，发奖
	Failed  = 1005,		--玩法失败
}

EnumAsianGames2023DataType = {
    eQuestion = 1,
    eGamble = 2,
    eJueDouWinNum = 3,
    eJueDouNum = 4,
    eHuiJuan = 5,
	eIsGetAllGambleReward = 6,
	eHuiJuanRewardStatus = 7,
}

EnumHengYuDangKouLeaderStage = {
    eNone = 0,          -- 未开始
    eRunFor = 1,        -- 竞选
    eSelect = 2,        -- 选择出战
    eLeaderConfirm = 3, -- 队长确认出战
    eMemberConfirm = 4, -- 队员确认出战
    eFight = 5,         -- 战斗中
}