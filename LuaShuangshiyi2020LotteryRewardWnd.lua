local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CUITexture = import "L10.UI.CUITexture"

CLuaShuangshiyi2020LotteryRewardWnd = class()
RegistClassMember(CLuaShuangshiyi2020LotteryRewardWnd,"m_ShareBtn")
RegistClassMember(CLuaShuangshiyi2020LotteryRewardWnd,"m_RewardLabel")
RegistClassMember(CLuaShuangshiyi2020LotteryRewardWnd,"m_DescLabel")
RegistClassMember(CLuaShuangshiyi2020LotteryRewardWnd,"m_RewardIcon")

function CLuaShuangshiyi2020LotteryRewardWnd:Init()
    self.m_ShareBtn = self.transform:Find("Anchor/ShareBtn").gameObject
	self.m_DescLabel = self.transform:Find("Anchor/Info/Desc"):GetComponent(typeof(UILabel))
	self.m_RewardLabel = self.transform:Find("Anchor/Info/RewardLabel"):GetComponent(typeof(UILabel))
	self.m_RewardIcon = self.transform:Find("Anchor/Info/RewardIcon/Texture"):GetComponent(typeof(CUITexture))

	if CLuaShuangshiyi2020Mgr.m_RewardFirstShowMark and CLuaShuangshiyi2020Mgr.m_RewardLevel then
		CLuaShuangshiyi2020Mgr.m_RewardFirstShowMark = false
	else
		CUIManager.CloseUI(CLuaUIResources.Shuangshiyi2020LotteryRewardWnd)
	end

	local onShareClick = function(go)
		CUICommonDef.CaptureScreenAndShare()
	end
	CommonDefs.AddOnClickListener(self.m_ShareBtn,DelegateFactory.Action_GameObject(onShareClick),false)

	local rewardLevel = CLuaShuangshiyi2020Mgr.m_RewardLevel

	local rewardInfo = {LocalString.GetString("特等奖"),LocalString.GetString("一等奖"),LocalString.GetString("二等奖"),LocalString.GetString("三等奖")}
	local data = Double11_LotterySetting.GetData(rewardLevel)

	self.m_RewardLabel.text = rewardInfo[rewardLevel]
	self.m_DescLabel.text = SafeStringFormat3(LocalString.GetString("返还消费灵玉的[FFFF00]%d%%"), data.RewardPerc)
	local rewardMat = "UI/Texture/Transparent/Material/bangdinglingyu_%d.mat"

	self.m_RewardIcon:LoadMaterial(SafeStringFormat3(rewardMat, 5 - rewardLevel))
end
