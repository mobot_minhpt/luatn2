local QnCheckBox = import "L10.UI.QnCheckBox"

local UILabel = import "UILabel"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local Animation = import "UnityEngine.Animation"
local PlayerSettings = import "L10.Game.PlayerSettings"
local SoundManager = import "SoundManager"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"
local CItemAccessListMgr=import "L10.UI.CItemAccessListMgr"
LuaShuangshiyi2023LotteryWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShuangshiyi2023LotteryWnd, "DrawRemainLabel", "DrawRemainLabel", UILabel)
RegistChildComponent(LuaShuangshiyi2023LotteryWnd, "DrawTips", "DrawTips", UILabel)
RegistChildComponent(LuaShuangshiyi2023LotteryWnd, "Forbidden", "Forbidden", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryWnd, "ForbiddenItem1", "ForbiddenItem1", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryWnd, "ForbiddenItem2", "ForbiddenItem2", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryWnd, "ForbiddenItem3", "ForbiddenItem3", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryWnd, "Bottom", "Bottom", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryWnd, "Reward", "Reward", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryWnd, "RewardItem1", "RewardItem1", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryWnd, "RewardItem2", "RewardItem2", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryWnd, "RewardItem3", "RewardItem3", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryWnd, "RewardItem4", "RewardItem4", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryWnd, "RewardItem5", "RewardItem5", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryWnd, "RewardItem6", "RewardItem6", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryWnd, "RewardItem7", "RewardItem7", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryWnd, "RewardItem8", "RewardItem8", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryWnd, "AllItemsButton", "AllItemsButton", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryWnd, "ChargeRewardButton", "ChargeRewardButton", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryWnd, "ChangeNextButton", "ChangeNextButton", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryWnd, "OneHundredButton", "OneHundredButton", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryWnd, "OneDrawButton", "OneDrawButton", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryWnd, "TenDrawButton", "TenDrawButton", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryWnd, "RuleButton", "RuleButton", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryWnd, "SkipAnimation", "SkipAnimation", QnCheckBox)
RegistChildComponent(LuaShuangshiyi2023LotteryWnd, "OneHundred", "OneHundred", GameObject)
RegistChildComponent(LuaShuangshiyi2023LotteryWnd, "OneHundredItem", "OneHundredItem", GameObject)

--@endregion RegistChildComponent end

function LuaShuangshiyi2023LotteryWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self:InitWndData()
    self:RefreshConstUI()
    self:RefreshVariableUI()
    self:InitUIEvent()
end

function LuaShuangshiyi2023LotteryWnd:Init()

end

--@region UIEvent

--@endregion UIEvent


function LuaShuangshiyi2023LotteryWnd:InitWndData()
    self.price2Ticket = Double11_RechargeLottery.GetData().Price2Ticket
    local representItemsStr = Double11_RechargeLottery.GetData().PrizePoolRepresentItems
    self.representLotteryIdList = {}
    local itemList = g_LuaUtil:StrSplit(representItemsStr,",")
    for i = 1, #itemList do
        if itemList[i] ~= "" then
            table.insert(self.representLotteryIdList, tonumber(itemList[i]))
        end
    end
    self.representItemObjList = {}
    for i = 1, 8 do
        local rewardItemObj = self["RewardItem"..i]
        table.insert(self.representItemObjList, rewardItemObj)
        rewardItemObj:SetActive(i <= #self.representLotteryIdList)
    end

    self.rechargeData = {}
    self.lotteryData = {}
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer and CommonDefs.DictContains_LuaCall(mainPlayer.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11RechargeData) then
        local playDataU = CommonDefs.DictGetValue_LuaCall(mainPlayer.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11RechargeData)
        local list = g_MessagePack.unpack(playDataU.Data.Data)
        self.rechargeData = list
    end
    if mainPlayer and CommonDefs.DictContains_LuaCall(mainPlayer.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11LotteryData) then
        local playDataU = CommonDefs.DictGetValue_LuaCall(mainPlayer.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11LotteryData)
        local list = g_MessagePack.unpack(playDataU.Data.Data)
        self.lotteryData = list
    end
    
    self.aniComp = self.transform:GetComponent(typeof(Animation))
end 

function LuaShuangshiyi2023LotteryWnd:RefreshConstUI()
    
    --self.DrawTips.text = g_MessageMgr:FormatMessage("Double11_2023Lottery_JifenTips", self.price2Ticket)
    self.DrawTips.text = g_MessageMgr:FormatMessage("Double11_2023Lottery_Rule")

    --show represent items
    for i = 1, #self.representLotteryIdList do
        if self.representItemObjList[i] then
            self:InitOneItem(self.representItemObjList[i], self.representLotteryIdList[i])
        end
    end
    for i = #self.representLotteryIdList + 1, #self.representItemObjList do
        if self.representItemObjList[i] then
            self.representItemObjList[i]:SetActive(false)
        end
    end

    --self.skipAnimationValue = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eDouble11RechargeLotteryIsSkipAnimation) or 0
    --self.SkipAnimation:SetSelected(self.skipAnimationValue == 1, false)
end

function LuaShuangshiyi2023LotteryWnd:RefreshVariableUI()
    if (self.rechargeData[LuaShuangshiyi2023Mgr.KeyTbl.eCurLotteryTimes] and self.rechargeData[LuaShuangshiyi2023Mgr.KeyTbl.eCurLotteryTimes] or 0) > 0 then
        LuaShuangshiyi2023Mgr.m_ShowLotteryRemainTimes = false
    end
    
    self.ChargeRewardButton.transform:Find("Sprite").gameObject:SetActive(LuaShuangshiyi2023Mgr:IsRechargeRewardToGet())
    
    self.DrawRemainLabel.text = LocalString.GetString("剩余抽奖次数：") .. 
            (self.rechargeData[LuaShuangshiyi2023Mgr.KeyTbl.eCurLotteryTimes] and self.rechargeData[LuaShuangshiyi2023Mgr.KeyTbl.eCurLotteryTimes] or 0)
    self.ChangeNextButton.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("避雷券 ") ..
            (self.rechargeData[LuaShuangshiyi2023Mgr.KeyTbl.eCurReplacementTimes] and self.rechargeData[LuaShuangshiyi2023Mgr.KeyTbl.eCurReplacementTimes] or 0)
    self.OneHundredButton.transform:Find("Label"):GetComponent(typeof(UILabel)).text = LocalString.GetString("探囊取物券 ") ..
            (self.rechargeData[LuaShuangshiyi2023Mgr.KeyTbl.eCurMustGetTimes] and self.rechargeData[LuaShuangshiyi2023Mgr.KeyTbl.eCurMustGetTimes] or 0)
    
    local mustGetItemId = self.lotteryData[LuaShuangshiyi2023Mgr.KeyTbl.eMustGetItem] and self.lotteryData[LuaShuangshiyi2023Mgr.KeyTbl.eMustGetItem] or 0
    if mustGetItemId ~= 0 then
        --如果有必中数据, 展示必中数据
        self.Forbidden:SetActive(false)
        self.OneHundred:SetActive(true)
        self:InitOneItem(self.OneHundredItem, mustGetItemId)
    else
        --如果无必中数据
        local forbidItemList = self.lotteryData[LuaShuangshiyi2023Mgr.KeyTbl.eReplacementItems] and self.lotteryData[LuaShuangshiyi2023Mgr.KeyTbl.eReplacementItems] or {}
        local forbidItemLength = #forbidItemList
        self.Forbidden:SetActive(forbidItemLength > 0)
        self.OneHundred:SetActive(false)

        for i = 1, forbidItemLength do
            local forbidItemObj = self["ForbiddenItem"..i]
            self:InitOneItem(forbidItemObj, forbidItemList[i])
            forbidItemObj.transform:Find("XSign").gameObject:SetActive(true)
        end
        for i = forbidItemLength + 1, 3 do
            local forbidItemObj = self["ForbiddenItem"..i]
            forbidItemObj.transform:Find("XSign").gameObject:SetActive(false)
            forbidItemObj.transform:Find("BindSprite").gameObject:SetActive(false)
            forbidItemObj.transform:Find("Border"):GetComponent(typeof(UISprite)).spriteName = ""
            forbidItemObj.transform:Find("AmountLabel"):GetComponent(typeof(UILabel)).text = ""

            local texture = forbidItemObj.transform:Find("Item"):GetComponent(typeof(UITexture))
            texture.material = nil
            UIEventListener.Get(forbidItemObj).onClick = DelegateFactory.VoidDelegate(function (_)
                --cancel
            end)
        end
    end
end

function LuaShuangshiyi2023LotteryWnd:InitUIEvent()
    UIEventListener.Get(self.OneDrawButton).onClick = DelegateFactory.VoidDelegate(function (_)
        --检查次数
        local lotteryTime = (self.rechargeData[LuaShuangshiyi2023Mgr.KeyTbl.eCurLotteryTimes] and self.rechargeData[LuaShuangshiyi2023Mgr.KeyTbl.eCurLotteryTimes] or 0)
        if lotteryTime >= 1 then
            self.aniComp:Stop()
            self.aniComp:Play("shuangshiyi2023lotterywnd_once")
            Gac2Gas.Request2023Double11RechargeLotteryByTimes(1)
            if PlayerSettings.SoundEnabled then
                local sound = "event:/L10/L10_UI/2023ShiZhuangChouJiang/2023ShiZhuangChouJiang_6s"
                if self.m_LotteryResultSound then SoundManager.Inst:StopSound(self.m_LotteryResultSound) end
                self.m_LotteryResultSound = SoundManager.Inst:PlayOneShot(sound, Vector3.zero, nil, 0)
            end
        else
            g_MessageMgr:ShowMessage("DOUBLE11_RECHARGE_LOTTERY_TIMES_NOT_ENOUGH")
        end
    end)
    UIEventListener.Get(self.TenDrawButton).onClick = DelegateFactory.VoidDelegate(function (_)
        --检查次数
        
        --已选三个避雷
        local forbidItemList = self.lotteryData[LuaShuangshiyi2023Mgr.KeyTbl.eReplacementItems] and self.lotteryData[LuaShuangshiyi2023Mgr.KeyTbl.eReplacementItems] or {}
        local forbidItemLength = #forbidItemList
        if forbidItemLength >= 1 then
            g_MessageMgr:ShowMessage("Double11_2023Lottery_Replacement_CANNOT_Ten_Draw")
            return
        end

        --已选一个必中
        local mustGetItemId = self.lotteryData[LuaShuangshiyi2023Mgr.KeyTbl.eMustGetItem] and self.lotteryData[LuaShuangshiyi2023Mgr.KeyTbl.eMustGetItem] or 0
        if mustGetItemId ~= 0 then
            g_MessageMgr:ShowMessage("Double11_2023Lottery_MustGet_CANNOT_Ten_Draw")
            return
        end
        
        local lotteryTime = (self.rechargeData[LuaShuangshiyi2023Mgr.KeyTbl.eCurLotteryTimes] and self.rechargeData[LuaShuangshiyi2023Mgr.KeyTbl.eCurLotteryTimes] or 0)
        if lotteryTime >= 10 then
            self.aniComp:Stop()
            self.aniComp:Play("shuangshiyi2023lotterywnd_ten")
            Gac2Gas.Request2023Double11RechargeLotteryByTimes(10)
            if PlayerSettings.SoundEnabled then
                local sound = "event:/L10/L10_UI/2023ShiZhuangChouJiang/2023ShiZhuangChouJiang_6s"
                if self.m_LotteryResultSound then SoundManager.Inst:StopSound(self.m_LotteryResultSound) end
                self.m_LotteryResultSound = SoundManager.Inst:PlayOneShot(sound, Vector3.zero, nil, 0)
            end
        else
            g_MessageMgr:ShowMessage("DOUBLE11_RECHARGE_LOTTERY_TIMES_NOT_ENOUGH")
        end
    end)
    UIEventListener.Get(self.ChangeNextButton).onClick = DelegateFactory.VoidDelegate(function (_)
        --已选三个避雷
        local forbidItemList = self.lotteryData[LuaShuangshiyi2023Mgr.KeyTbl.eReplacementItems] and self.lotteryData[LuaShuangshiyi2023Mgr.KeyTbl.eReplacementItems] or {}
        local forbidItemLength = #forbidItemList
        if forbidItemLength >= 3 then
            g_MessageMgr:ShowMessage("Double11_2023Lottery_Replacement_Full_Tips")
            return
        end
        
        --已选一个必中
        local mustGetItemId = self.lotteryData[LuaShuangshiyi2023Mgr.KeyTbl.eMustGetItem] and self.lotteryData[LuaShuangshiyi2023Mgr.KeyTbl.eMustGetItem] or 0
        if mustGetItemId ~= 0 then
            g_MessageMgr:ShowMessage("Double11_2023Lottery_MustGet_Full_Tips")
            return
        end

        self:OnClickChangeNext()
    end)
    UIEventListener.Get(self.ChargeRewardButton).onClick = DelegateFactory.VoidDelegate(function (_)
        CUIManager.ShowUI(CLuaUIResources.Shuangshiyi2023ChargeRewardWnd)
    end)
    UIEventListener.Get(self.AllItemsButton).onClick = DelegateFactory.VoidDelegate(function (_)
        CUIManager.ShowUI(CLuaUIResources.Shuangshiyi2023LotteryOverviewWnd)
    end)
    UIEventListener.Get(self.OneHundredButton).onClick = DelegateFactory.VoidDelegate(function (_)
        --已选至少一个避雷
        local forbidItemList = self.lotteryData[LuaShuangshiyi2023Mgr.KeyTbl.eReplacementItems] and self.lotteryData[LuaShuangshiyi2023Mgr.KeyTbl.eReplacementItems] or {}
        local forbidItemLength = #forbidItemList
        if forbidItemLength >= 1 then
            g_MessageMgr:ShowMessage("Double11_2023Lottery_Replacement_Select_Before_Tips")
            return
        end
        
        local mustGetItemId = self.lotteryData[LuaShuangshiyi2023Mgr.KeyTbl.eMustGetItem] and self.lotteryData[LuaShuangshiyi2023Mgr.KeyTbl.eMustGetItem] or 0
        if mustGetItemId ~= 0 then
            g_MessageMgr:ShowMessage("Double11_2023Lottery_MustGet_Full_Tips")
            return
        end
        
        self:OnClickMustGet()
    end)
    UIEventListener.Get(self.RuleButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        g_MessageMgr:ShowMessage("Double11_2023Lottery_JifenTips")
    end)
    --SkipAnimation
    --self.SkipAnimation.OnValueChanged = DelegateFactory.Action_bool(function(value)
    --    Gac2Gas.Request2023Double11SetRechargeLotteryIsSkipAnimation(value and 1 or 0)
    --end)
end

function LuaShuangshiyi2023LotteryWnd:UpdateUI()
    self.rechargeData = {}
    self.lotteryData = {}
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer and CommonDefs.DictContains_LuaCall(mainPlayer.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11RechargeData) then
        local playDataU = CommonDefs.DictGetValue_LuaCall(mainPlayer.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11RechargeData)
        local list = g_MessagePack.unpack(playDataU.Data.Data)
        self.rechargeData = list
    end
    if mainPlayer and CommonDefs.DictContains_LuaCall(mainPlayer.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11LotteryData) then
        local playDataU = CommonDefs.DictGetValue_LuaCall(mainPlayer.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eDouble11LotteryData)
        local list = g_MessagePack.unpack(playDataU.Data.Data)
        self.lotteryData = list
    end
    self:RefreshVariableUI()
end

function LuaShuangshiyi2023LotteryWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdateTempPlayDataWithKey", self, "UpdateUI")
    g_ScriptEvent:AddListener("OnDelayShowCommonGetRewardWnd", self, "OnDelayShowCommonGetRewardWnd")
end 

function LuaShuangshiyi2023LotteryWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateTempPlayDataWithKey", self, "UpdateUI")
    g_ScriptEvent:RemoveListener("OnDelayShowCommonGetRewardWnd", self, "OnDelayShowCommonGetRewardWnd")

    if self.m_LotteryResultSound then SoundManager.Inst:StopSound(self.m_LotteryResultSound) end
end

function LuaShuangshiyi2023LotteryWnd:OnDelayShowCommonGetRewardWnd()
    self.aniComp:Play("shuangshiyi2023lotterywnd_show")
end

function LuaShuangshiyi2023LotteryWnd:InitOneItem(curItem, lotteryID)
    local texture = curItem.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local lotteryCfg = Double11_RechargeLotteryItems.GetData(lotteryID)
    local itemID = lotteryCfg.ItemId
    local ItemData = Item_Item.GetData(itemID)
    if ItemData then texture:LoadMaterial(ItemData.Icon) end

    local isBind = (lotteryCfg.Bind == 1) or (ItemData.Lock == 1)
    curItem.transform:Find("BindSprite").gameObject:SetActive(false)
    curItem.transform:Find("Border"):GetComponent(typeof(UISprite)).spriteName = CUICommonDef.GetItemCellBorder(ItemData.NameColor)
    curItem.transform:Find("AmountLabel"):GetComponent(typeof(UILabel)).text = lotteryCfg.Amount > 1 and lotteryCfg.Amount or ""

    UIEventListener.Get(curItem).onClick = DelegateFactory.VoidDelegate(function (_)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemID, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
    end)
end

function LuaShuangshiyi2023LotteryWnd:OnClickChangeNext()
    --1.是否有道具
    --2.三个替换券是否都已使用
    --3.打开替换界面
    
    local remainReplacementTime = (self.rechargeData[LuaShuangshiyi2023Mgr.KeyTbl.eCurReplacementTimes] and self.rechargeData[LuaShuangshiyi2023Mgr.KeyTbl.eCurReplacementTimes] or 0)
    if remainReplacementTime > 0 then
        --有道具
        local forbidItemList = self.lotteryData[LuaShuangshiyi2023Mgr.KeyTbl.eReplacementItems] and self.lotteryData[LuaShuangshiyi2023Mgr.KeyTbl.eReplacementItems] or {}
        local forbidItemLength = #forbidItemList
        if forbidItemLength >= 3 then
            --次数满了
            g_MessageMgr:ShowMessage("Double11_2023Lottery_Replacement_Full_Tips")
        else
            CUIManager.ShowUI(CLuaUIResources.Shuangshiyi2023LotteryReplacementWnd)
        end
    else
        --没道具, 弹出获得渠道
        local itemId = 21021450
        CItemAccessListMgr.Inst:ShowItemAccessInfo(itemId, false, self.ChangeNextButton.transform, CTooltipAlignType.Right)
        --CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, true)
    end
end

function LuaShuangshiyi2023LotteryWnd:OnClickMustGet()
    local mustGetTime = (self.rechargeData[LuaShuangshiyi2023Mgr.KeyTbl.eCurMustGetTimes] and self.rechargeData[LuaShuangshiyi2023Mgr.KeyTbl.eCurMustGetTimes] or 0)
    if mustGetTime > 0 then
        CUIManager.ShowUI(CLuaUIResources.Shuangshiyi2023LotteryMustGetWnd)
    else
        --没道具, 弹出获得渠道
        local itemId = 21021451
        CItemAccessListMgr.Inst:ShowItemAccessInfo(itemId, false, self.OneHundredButton.transform, CTooltipAlignType.Right)
        --CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, true)
    end
end


