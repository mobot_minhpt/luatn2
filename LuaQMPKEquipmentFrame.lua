local UIEventListener = import "UIEventListener"
local L10 = import "L10"
local IdPartition = import "L10.Game.IdPartition"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local EnumItemPlaceSize = import "L10.Game.EnumItemPlaceSize"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CStatusMgr = import "L10.Game.CStatusMgr"
-- local CQMPKEquipmentFrame = import "L10.CQMPKEquipmentFrame"
local CPlayerProSaveMgr = import "L10.Game.CPlayerProSaveMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CItemMgr = import "L10.Game.CItemMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CBodyEquipSlot = import "L10.UI.CBodyEquipSlot"
local EnumQualityType = import "L10.Game.EnumQualityType"
local CCommonItem = import "L10.Game.CCommonItem"

CLuaQMPKEquipmentFrame = class()
RegistClassMember(CLuaQMPKEquipmentFrame,"m_EquipmentClickAction")
RegistClassMember(CLuaQMPKEquipmentFrame,"equipSlots")
RegistClassMember(CLuaQMPKEquipmentFrame,"inited")
RegistClassMember(CLuaQMPKEquipmentFrame,"m_IsOtherPlayer")
RegistClassMember(CLuaQMPKEquipmentFrame,"m_OtherPlayerInfo")

function CLuaQMPKEquipmentFrame:Awake()
    self.m_IsOtherPlayer = CLuaQMPKMgr.s_IsOtherPlayer
    self.m_OtherPlayerInfo = CLuaQMPKMgr.s_PlayerInfo
    self.inited = false
    self.equipSlots={}
--self.m_EquipmentClickAction = ?
--self.equipSlots = ?
--self.inited = ?

end

-- Auto Generated!!
function CLuaQMPKEquipmentFrame:OnEnable( )
    self:BindSlots()
    self:InitEquipmentSlots()

    for i,v in ipairs(self.equipSlots) do
        UIEventListener.Get(v.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnEquipmentSlotClick(go)
        end)
    end
    if not self.m_IsOtherPlayer then
        g_ScriptEvent:AddListener("SetItemAt", self, "OnSetItemAt")
        g_ScriptEvent:AddListener("UpdateEquipDisable", self, "OnUpdateEquipDisable")
        --数据变化
        g_ScriptEvent:AddListener("MainPlayerEquipDurationUpdate", self, "OnEquipDurationUpdate")
        g_ScriptEvent:AddListener("SendItem", self, "OnSendItem")
        g_ScriptEvent:AddListener("MainPlayerTotalEquipScoreUpdate", self, "UpdateEquipScore")
    end
end
function CLuaQMPKEquipmentFrame:OnDisable( )
    if not self.m_IsOtherPlayer then
        g_ScriptEvent:RemoveListener("SetItemAt", self, "OnSetItemAt")
        g_ScriptEvent:RemoveListener("UpdateEquipDisable", self, "OnUpdateEquipDisable")
        --数据变化
        g_ScriptEvent:RemoveListener("MainPlayerEquipDurationUpdate", self, "OnEquipDurationUpdate")
        g_ScriptEvent:RemoveListener("SendItem", self, "OnSendItem")
        g_ScriptEvent:RemoveListener("MainPlayerTotalEquipScoreUpdate", self, "UpdateEquipScore")
    end
end
function CLuaQMPKEquipmentFrame:BindSlots( )
    if self.inited then
        return
    end
    self.inited = true
    -- CommonDefs.ListClear(self.equipSlots)
    self.equipSlots={}
    --现在关联了十二件装备， 对应策划表EquipmentTemplate_Type
    self:AddSlot("WeaponSlot", self.equipSlots)
    self:AddSlot("ShieldSlot", self.equipSlots)
    self:AddSlot("HeadwearSlot", self.equipSlots)
    self:AddSlot("ClothesSlot", self.equipSlots)
    self:AddSlot("BeltSlot", self.equipSlots)
    self:AddSlot("GlovesSlot", self.equipSlots)
    self:AddSlot("ShoesSlot", self.equipSlots)
    self:AddSlot("LeftRingSlot", self.equipSlots)
    self:AddSlot("LeftBraceletSlot", self.equipSlots)
    --手镯8
    self:AddSlot("NecklaceSlot", self.equipSlots)
    self:AddSlot("RightRingSlot", self.equipSlots)
    self:AddSlot("RightBraceletSlot", self.equipSlots)
    --手镯11

    self:AddSlot("BackPendantSlot", self.equipSlots)
end
function CLuaQMPKEquipmentFrame:AddSlot( goName, equipSlots) 
    local slot = CommonDefs.GetComponent_Component_Type(self.transform:Find(goName), typeof(CBodyEquipSlot))
    if slot == nil then
        L10.CLogMgr.LogError("Equipment Slot Reference Missing!")
    else
        -- CommonDefs.ListAdd(equipSlots, typeof(CBodyEquipSlot), slot)
        table.insert( equipSlots, slot )
    end
end
function CLuaQMPKEquipmentFrame:OnSetItemAt( args ) 
    local place =args[0]
    local position =args[1]
    local oldItemId=args[2]
    local  newItemId=args[3]
    self:UpdateEquipmentSlot(place, position)
end
function CLuaQMPKEquipmentFrame:OnUpdateEquipDisable( args) 
    local equipId=args[0]
    local count = EnumItemPlaceSize.GetPlaceSize(EnumItemPlace.Body)
    do
        local i = 1
        while i <= count do
            local itemId = self:GetItemAt(EnumItemPlace.Body, i)
            if equipId == itemId then
                self:UpdateEquipmentSlot(EnumItemPlace.Body, i)
                return
            end
            i = i + 1
        end
    end
end
function CLuaQMPKEquipmentFrame:OnEquipDurationUpdate( args) 
    local place=args[0]
    local position=args[1]
    if place ~= EnumItemPlace.Body then
        return
    end
    local count = EnumItemPlaceSize.GetPlaceSize(EnumItemPlace.Body)
    local equipId = self:GetItemAt(place, position)
    do
        local i = 1
        while i <= count do
            local itemId = self:GetItemAt(EnumItemPlace.Body, i)
            if equipId == itemId then
                self:UpdateEquipmentSlot(EnumItemPlace.Body, i)
                return
            end
            i = i + 1
        end
    end
end
function CLuaQMPKEquipmentFrame:OnSendItem( args) 
    local id=args[0]
    local item = self:GetById(id)
    if item == nil or not item.IsEquip then
        return
    end
    local count = EnumItemPlaceSize.GetPlaceSize(EnumItemPlace.Body)

    do
        local i = 1
        while i <= count do
            local continue
            repeat
                local itemId = self:GetItemAt(EnumItemPlace.Body, i)
                if System.String.IsNullOrEmpty(itemId) then
                    continue = true
                    break
                end
                if itemId == id then
                    self:UpdateEquipmentSlot(EnumItemPlace.Body, i)
                    break
                end
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end
end
function CLuaQMPKEquipmentFrame:InitEquipmentSlots( )
    for i,v in ipairs(self.equipSlots) do
        v:Init(nil,EnumQualityType.None,false,nil,i)
    end

    local count = EnumItemPlaceSize.GetPlaceSize(EnumItemPlace.Body)

    for i=1,count do
        local itemId = self:GetItemAt(EnumItemPlace.Body, i)
        if System.String.IsNullOrEmpty(itemId) then
            if i == EnumBodyPosition_lua.Shield then
                self.equipSlots[i].DisableSpriteVisible = self:NeedShowDisableSprite(i)
            end
        else
            local item = self:GetById(itemId)
            --TODO 这里先排除了法宝 防止报错
            if item ~= nil and not IdPartition.IdIsTalisman(item.TemplateId) then
                local template = EquipmentTemplate_Equip.GetData(item.TemplateId)
                if i <= #self.equipSlots then
                    self.equipSlots[i]:Init(item.Equip.BigIcon, item.Equip.Color, 
                    i == EnumBodyPosition_lua.Weapon or i == EnumBodyPosition_lua.Shield or i == EnumBodyPosition_lua.Armour, 
                    item.Equip, i)
                    --1，2，4位置为武器、盾牌和甲胄
                end
                self.equipSlots[i].DisableSpriteVisible = self:NeedShowDisableSprite(i)
            end
        end

    end
end
function CLuaQMPKEquipmentFrame:NeedShowDisableSprite( position) 
    local needShowDisableSprite = false

    local equipId = self:GetItemAt(EnumItemPlace.Body, position)
    if not System.String.IsNullOrEmpty(equipId) then
        local equip = self:GetById(equipId)
        needShowDisableSprite = (equip.Equip.Disable == 1) or (equip.Equip.IsLostSoul == 1)
    end

    if not needShowDisableSprite and (position == EnumBodyPosition_lua.Shield) then
        local weaponId = self:GetItemAt(EnumItemPlace.Body, EnumBodyPosition_lua.Weapon)
        if not System.String.IsNullOrEmpty(weaponId) then
            local weapon = self:GetById(weaponId)
            if weapon ~= nil then
                needShowDisableSprite = (EquipmentTemplate_Equip.GetData(weapon.TemplateId).BothHand == 1)
            end
        end
    end

    return needShowDisableSprite
end
function CLuaQMPKEquipmentFrame:UpdateEquipmentSlot( place, position) 
    if place ~= EnumItemPlace.Body then
        return
    end
    if position > #self.equipSlots then
        return
    end

    local itemId = self:GetItemAt(place, position)

    self.equipSlots[position]:Init(nil, EnumQualityType.None, false, nil, position)

    local item = nil
    if not System.String.IsNullOrEmpty(itemId) then
        item = self:GetById(itemId)
    end
    if item ~= nil then
        local template = EquipmentTemplate_Equip.GetData(item.TemplateId)
        self.equipSlots[position]:Init(item.Equip.BigIcon, item.Equip.Color, 
        position == EnumBodyPosition_lua.Weapon or position == EnumBodyPosition_lua.Shield or position == EnumBodyPosition_lua.Armour, 
        item.Equip, position)
        --1，2，4位置为武器、盾牌和甲胄
    end
    self.equipSlots[position].DisableSpriteVisible = self:NeedShowDisableSprite(position)
    if position == EnumBodyPosition_lua.Weapon then
        self.equipSlots[EnumBodyPosition_lua.Shield].DisableSpriteVisible = self:NeedShowDisableSprite(EnumBodyPosition_lua.Shield)
    end
end
function CLuaQMPKEquipmentFrame:OnEquipmentSlotClick( go) 
    if CStatusMgr.Inst == nil then
        return
    end

    for i,v in ipairs(self.equipSlots) do
        if go:Equals(v.gameObject) then
            local count = EnumItemPlaceSize.GetPlaceSize(EnumItemPlace.Body)
            --EnumItemPlace.Body索引是从1开始的
            if i <= count then
                local itemId = self:GetItemAt(EnumItemPlace.Body, i)
                if not System.String.IsNullOrEmpty(itemId) then
                    -- if nil ~= self.m_EquipmentClickAction then
                    --     -- GenericDelegateInvoke(self.m_EquipmentClickAction, Table2Array({itemId}, MakeArrayClass(Object)))
                    --     self:m_EquipmentClickAction(itemId)
                    -- end
                    --原有的回调方法改成消息方式
	                g_ScriptEvent:BroadcastInLua("QMPKEquipmentClick",itemId)
                end
            end
            break
        end
    end
end
function CLuaQMPKEquipmentFrame:UpdateEquipScore( )
    CPlayerProSaveMgr.Inst:updatePro()
    CPlayerProSaveMgr.Inst:savePro()
end

function CLuaQMPKEquipmentFrame:GetItemAt(place, position)
    if self.m_IsOtherPlayer then
        local equip = self.m_OtherPlayerInfo.Pos2Equip[position]
        return (equip or {}).Id
    else
        local itemProp = CClientMainPlayer.Inst.ItemProp
        return itemProp:GetItemAt(place, position)
    end
end

function CLuaQMPKEquipmentFrame:GetById(itemId)
    if self.m_IsOtherPlayer then
        local equip = self.m_OtherPlayerInfo.ItemId2Equip[itemId]
        return CreateFromClass(CCommonItem, equip)
    else
        return CItemMgr.Inst:GetById(itemId)
    end
end

return CLuaQMPKEquipmentFrame
