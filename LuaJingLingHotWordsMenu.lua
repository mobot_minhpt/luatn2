local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UIEventListener = import "UIEventListener"
local UICamera = import "UICamera"
local Vector3 = import "UnityEngine.Vector3"
local TouchScreenKeyboard = import "UnityEngine.TouchScreenKeyboard"
local NGUITools = import "NGUITools"

LuaJingLingHotWordsMenu = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistClassMember(LuaJingLingHotWordsMenu, "chatInput")             --文本输入框
RegistClassMember(LuaJingLingHotWordsMenu, "chatInputSprite")       --文本输入框
RegistClassMember(LuaJingLingHotWordsMenu, "background")            --背景
RegistClassMember(LuaJingLingHotWordsMenu, "btnsGrid")              --按钮网格
RegistClassMember(LuaJingLingHotWordsMenu, "btnTemplate")           --按钮模板
RegistClassMember(LuaJingLingHotWordsMenu, "btns")                  --热词按钮表
RegistClassMember(LuaJingLingHotWordsMenu, "m_MaxLength")           --热词最大长度
RegistClassMember(LuaJingLingHotWordsMenu, "m_lastInput")           --上次查询内容
RegistClassMember(LuaJingLingHotWordsMenu, "m_lastIsShow")          --上次是否显示
RegistClassMember(LuaJingLingHotWordsMenu, "m_method")              --提问来源

--@endregion RegistChildComponent end

function LuaJingLingHotWordsMenu:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.gameObject:SetActive(false)
    self.m_lastInput = nil
    self.m_MaxLength = 8

    self.chatInput = self.transform.parent:Find("Input"):GetComponent(typeof(UIInput))
    self.chatInputSprite = self.transform.parent:Find("Input"):GetComponent(typeof(UISprite))
    self.background = self.transform:Find("Background"):GetComponent(typeof(UISprite))
    self.btnsGrid = self.transform:Find("Background/Scroll View/Grid"):GetComponent(typeof(UIGrid))
    self.btnTemplate = self.transform:Find("Background/Scroll View/Button").gameObject

    self.btns = {}
    for i = 1, 5 do
        local go = NGUITools.AddChild(self.btnsGrid.gameObject, self.btnTemplate)
        self:InitHowWordItemBtn(go)
    end
    self.btnsGrid:Reposition()
    self.btnTemplate:SetActive(false)
end

function LuaJingLingHotWordsMenu:Update()
    if Input.GetMouseButtonDown(0) then
        --屏幕点击热词框隐藏
        local corners = self.background.worldCorners
        local lastPos = UICamera.lastWorldPosition;
        --热词框外区域
        if lastPos.x < corners[0].x or lastPos.x > corners[2].x or lastPos.y < corners[0].y or lastPos.y > corners[2].y then
            if CommonDefs.IsIOSPlatform() and TouchScreenKeyboard.visible then
                --键盘上方区域
                if lastPos.y > corners[2].y then
                    self.gameObject:SetActive(false)
                end
            else
                corners = self.chatInputSprite.worldCorners
                --输入框外区域
                if lastPos.x < corners[0].x or lastPos.x > corners[2].x or lastPos.y < corners[0].y or lastPos.y > corners[2].y then
                    self.gameObject:SetActive(false)
                end
            end
        end
    end
end

--@region UIEvent

--热词按钮初始化
function LuaJingLingHotWordsMenu:InitHowWordItemBtn(go)
    local btn = {}
    btn.gameObject = go
    btn.Text = btn.gameObject.transform:Find("Label"):GetComponent(typeof(UILabel))
    btn.Mark = btn.gameObject.transform:Find("Sprite").gameObject
    btn.question = nil
    table.insert(self.btns, btn)
    UIEventListener.Get(btn.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnHotWordsClick(go, btn)
    end)
end

--根据输入显示热词
function LuaJingLingHotWordsMenu:ShowHotWords(inputText)
    inputText = Extensions.RemoveBlank(inputText)
    --显示上次结果，不再重复获取热词
    if self.m_lastInput == inputText then
        self.gameObject:SetActive(self.m_lastIsShow)
        return
    end

    if inputText == nil or inputText == "" then
        --显示推荐词
        self:UpdateHotWordsBtnText(LuaJingLingMgr.m_RecommendedHotWordsList, true)
    else
        --显示联想词
        LuaJingLingMgr:RequestAssociativeHotWords(inputText)
    end
    self.m_lastInput = inputText
end

function LuaJingLingHotWordsMenu:RecordHotWordsActiveStatus(isactive)
    self.gameObject:SetActive(isactive)
    self.m_lastIsShow = isactive
end

--推荐角标的显示
function LuaJingLingHotWordsMenu:SetRecommendedWordTipActive(isactive)
    for i, btn in ipairs(self.btns) do
        btn.Mark:SetActive(isactive)
    end
end

--热词显示
function LuaJingLingHotWordsMenu:UpdateHotWordsBtnText(list, showRecommendedTip)
    if list ~= nil and list.Count > 0 then
        self:SetRecommendedWordTipActive(showRecommendedTip)
        self.m_method = showRecommendedTip and "explore" or "seach_recommend"
        -- 控制各条目是否出现，并更新内容
        for i = 1, list.Count do
            self.btns[i].gameObject:SetActive(true)
            self.btns[i].question = list[i-1]

            if CommonDefs.StringLength(list[i-1]) > self.m_MaxLength then
                self.btns[i].Text.text = CommonDefs.StringSubstring2(list[i-1], 0, self.m_MaxLength - 1) .. "..."
            else
                self.btns[i].Text.text = list[i-1]
            end
        end
        for i = list.Count+1, 5 do
            self.btns[i].gameObject:SetActive(false)
        end
        self:RecordHotWordsActiveStatus(true)
    else
        self:RecordHotWordsActiveStatus(false)
    end
end

--点击热词条目内容查询
function LuaJingLingHotWordsMenu:OnHotWordsClick(go, btn)
    g_ScriptEvent:BroadcastInLua("OnJingLingHotWordsItemClick", btn.question, self.m_method)
    self.gameObject:SetActive(false)
end
--@endregion UIEvent