
if rawget(_G, "LuaTianJiangBaoXiangMgr") then
    g_ScriptEvent:RemoveListener("GasDisconnect", LuaTianJiangBaoXiangMgr, "OnDisconnect")
end

LuaTianJiangBaoXiangMgr = {}
EnumTianJiangBaoXiangDoor = {
    None = 0,   -- 无
    ChiTong = 1,    -- 赤铜
    FeiCui = 2,     -- 翡翠
}
LuaTianJiangBaoXiangMgr.ShowLimitInfo = false   --是否显示所剩无多提示
LuaTianJiangBaoXiangMgr.NowStep = 1 -- 当前阶段，1为1级界面，2为2级界面
LuaTianJiangBaoXiangMgr.InitWnd = false -- 是否已初始化界面
LuaTianJiangBaoXiangMgr.RewardTbl = nil -- 奖励列表
LuaTianJiangBaoXiangMgr.ShowBossEntrance = false -- 显示挑战入口
LuaTianJiangBaoXiangMgr.HasEnterDoor = {}   -- 玩家已开门次数
LuaTianJiangBaoXiangMgr.KeyNum = {} -- 开门需要消耗的钥匙数
LuaTianJiangBaoXiangMgr.YuanBaoAmount = 0   -- 消耗元宝数量

LuaTianJiangBaoXiangMgr.StartPlayTimeStamp = 0   --玩法开始时间戳
LuaTianJiangBaoXiangMgr.IsPlayOpen = false
LuaTianJiangBaoXiangMgr.PlayOpenTime = 2*60*60        -- 玩法开启时长，玩法开启两个小时
LuaTianJiangBaoXiangMgr.ShowTimeTickTime = 3*60    -- 玩法结束显示玩法倒计时的时间，玩法结束前3分钟显示倒计时

LuaTianJiangBaoXiangMgr.PlayerChooseDoor = EnumTianJiangBaoXiangDoor.None     -- 玩家选择的门 
LuaTianJiangBaoXiangMgr.ShowTheFirstDoor = EnumTianJiangBaoXiangDoor.FeiCui  -- 玩家进入第一界面的默认门（考虑从包裹中打开使用钥匙时的情况

LuaTianJiangBaoXiangMgr.totalAddKeysCount = 0
LuaTianJiangBaoXiangMgr.totalKeys_FeiCui = 0
LuaTianJiangBaoXiangMgr.totalKeys_ChiTong = 0

g_ScriptEvent:AddListener("GasDisconnect", LuaTianJiangBaoXiangMgr, "OnDisconnect")  

-- NowStep 当前阶段，为1时打开第一界面，为2时打开第二界面
function LuaTianJiangBaoXiangMgr.OpenTianJiangBaoXiangWnd(NowStep)
    LuaTianJiangBaoXiangMgr.NowStep = NowStep
    if not LuaTianJiangBaoXiangMgr.InitWnd then 
        CUIManager.ShowUI(CLuaUIResources.TianJiangBaoXiangWnd)
    else    
        g_ScriptEvent:BroadcastInLua("ShowTiangJiangBaoXiangWnd")
    end
end
-- 钥匙数量信息
function LuaTianJiangBaoXiangMgr.GetKeyNumInfo(keyNumList)
    LuaTianJiangBaoXiangMgr.KeyNum[EnumTianJiangBaoXiangDoor.FeiCui] = keyNumList[EnumTianJiangBaoXiangDoor.FeiCui - 1]
    LuaTianJiangBaoXiangMgr.KeyNum[EnumTianJiangBaoXiangDoor.ChiTong] = keyNumList[EnumTianJiangBaoXiangDoor.ChiTong - 1]
end
-- 开门次数信息
function LuaTianJiangBaoXiangMgr.GetOpenTimesInfo(openTimesList)
    LuaTianJiangBaoXiangMgr.HasEnterDoor[EnumTianJiangBaoXiangDoor.FeiCui] = openTimesList[EnumTianJiangBaoXiangDoor.FeiCui - 1]
    LuaTianJiangBaoXiangMgr.HasEnterDoor[EnumTianJiangBaoXiangDoor.ChiTong] = openTimesList[EnumTianJiangBaoXiangDoor.ChiTong - 1]
end

-- 断线重连时清理一下相关数据缓存,重置客户端数据
function LuaTianJiangBaoXiangMgr.OnDisconnect()
    if LuaTianJiangBaoXiangMgr.StartPlayTimeStamp == 0 then return end
    LuaTianJiangBaoXiangMgr.ShowLimitInfo = false   --是否显示所剩无多提示
    LuaTianJiangBaoXiangMgr.NowStep = 1 -- 当前阶段，1为1级界面，2为2级界面
    LuaTianJiangBaoXiangMgr.InitWnd = false -- 是否已初始化界面
    LuaTianJiangBaoXiangMgr.RewardTbl = nil -- 奖励列表
    LuaTianJiangBaoXiangMgr.ShowBossEntrance = false -- 显示挑战入口
    LuaTianJiangBaoXiangMgr.HasEnterDoor = {}   -- 玩家已开门次数
    LuaTianJiangBaoXiangMgr.KeyNum = {} -- 开门需要消耗的钥匙数
    LuaTianJiangBaoXiangMgr.YuanBaoAmount = 0   -- 消耗元宝数量

    LuaTianJiangBaoXiangMgr.StartPlayTimeStamp = 0   --玩法开始时间戳
    LuaTianJiangBaoXiangMgr.PlayerChooseDoor = EnumTianJiangBaoXiangDoor.None     -- 玩家选择的门 
    LuaTianJiangBaoXiangMgr.ShowTheFirstDoor = EnumTianJiangBaoXiangDoor.FeiCui  -- 玩家进入第一界面的默认门（考虑从包裹中打开使用钥匙时的情况
end