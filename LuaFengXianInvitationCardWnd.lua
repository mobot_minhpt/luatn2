require("common/common_include")

local QnModelPreviewer = import "L10.UI.QnModelPreviewer"
local CButton = import "L10.UI.CButton"

LuaFengXianInvitationCardWnd = class()

RegistChildComponent(LuaFengXianInvitationCardWnd, "ModelPreviewer", QnModelPreviewer)
RegistChildComponent(LuaFengXianInvitationCardWnd, "EnterButton", CButton)
RegistChildComponent(LuaFengXianInvitationCardWnd, "PlayerNameLabel", UILabel)

function LuaFengXianInvitationCardWnd:Init()

	if not CLuaXianzhiMgr.m_InvitationAppearance then
		CUIManager.CloseUI(CLuaUIResources.FengXianInvitationCardWnd)
		return
	end

	self.ModelPreviewer:PreviewFakeAppear(CLuaXianzhiMgr.m_InvitationAppearance)
	self.PlayerNameLabel.text = tostring(CLuaXianzhiMgr.m_InvitationAppearance.m_FakeAppear.Name)

	CommonDefs.AddOnClickListener(self.EnterButton.gameObject, DelegateFactory.Action_GameObject(function (go)
		self:OnEnterButtonClicked(go)
	end), false)
end

function LuaFengXianInvitationCardWnd:OnEnterButtonClicked(go)
	Gac2Gas.RequestWatchFengXian(CLuaXianzhiMgr.m_InvitationCardPlayerId, CLuaXianzhiMgr.m_InvitationCardPlayId)
end

function LuaFengXianInvitationCardWnd:OnEnable()
	-- g_ScriptEvent:AddListener("AcceptTaskFromBabySuccess", self, "UpdateDiaryAlert")
end

function LuaFengXianInvitationCardWnd:OnDisable()
	-- g_ScriptEvent:RemoveListener("AcceptTaskFromBabySuccess", self, "UpdateDiaryAlert")
end

return LuaFengXianInvitationCardWnd