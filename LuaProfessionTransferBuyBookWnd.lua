local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnRadioBox = import "L10.UI.QnRadioBox"
local UITable = import "UITable"
local UILabel = import "UILabel"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local QnTableView = import "L10.UI.QnTableView"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local AlignType = import "CPlayerInfoMgr+AlignType"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local Vector3 = import "UnityEngine.Vector3"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Profession = import "L10.Game.Profession"
local CUITexture = import "L10.UI.CUITexture"
local Item_Item = import "L10.Game.Item_Item"
local CItem = import "L10.Game.CItem"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Animation = import "UnityEngine.Animation"
local LuaTweenUtils = import "LuaTweenUtils"
local Ease = import "DG.Tweening.Ease"
local Vector2 = import "UnityEngine.Vector2"
local Vector4 = import "UnityEngine.Vector4"
local PathMode = import "DG.Tweening.PathMode"
local PathType = import "DG.Tweening.PathType"
LuaProfessionTransferBuyBookWnd = class()

--@region RegistChildComponent: Dont Modify Manually!


--@endregion RegistChildComponent end

function LuaProfessionTransferBuyBookWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaProfessionTransferBuyBookWnd:Init()
    self:InitWndData()
    self:InitUIEvent()

    self.transform:Find("RadioBox"):GetComponent(typeof(QnRadioBox)):ChangeTo(0, false)
    self.rankView:SetActive(true)
    self.awardView:SetActive(false)
    self.furnaceView:SetActive(false)
    self:RefreshAwardTotalRedDot()
    if self.refreshRankConstUI == false then
        self:RefreshConstUI(1)
        self.refreshRankConstUI = true
    end
    Gac2Gas.SeasonRank_QueryRank(self.rankType, self.rankSeasonId)
end

--@region UIEvent

--@endregion UIEvent

function LuaProfessionTransferBuyBookWnd:InitWndData()
    --init components
    self.radioBox = self.transform:Find("RadioBox"):GetComponent(typeof(QnRadioBox))
    
    self.curOpenViewIdx = 1
    
    self.refreshRankConstUI = false
    self.rankType = 1007
    self.rankSeasonId = 1001
    self.rankView = self.transform:Find("RankView").gameObject
    self.myRankItem = self.rankView.transform:Find("RankList/myRankItem")
    self.rankTable = self.rankView.transform:Find("RankList/TableBody/Table"):GetComponent(typeof(UITable))
    self.rankTemplate = self.rankView.transform:Find("RankList/TableBody/Pool/ItemTemplate")
    self.rankTipText = self.rankView.transform:Find("Tip"):GetComponent(typeof(UILabel))
    self.rankEmptyListText = self.rankView.transform:Find("DefaultLable")
    self.remainText = self.rankView.transform:Find("RemainText"):GetComponent(typeof(UILabel))
    self.buyBtn = self.rankView.transform:Find("BuyBtn").gameObject
    self.tipBtn = self.rankView.transform:Find("TipBtn").gameObject
    self.rankBody = self.rankView.transform:Find("RankList/TableBody"):GetComponent(typeof(QnTableView))
    self.rankBody.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_RankData
        end,
        function(item, index)
            item:SetBackgroundTexture(index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)
            self:InitItem(item, index+1, self.m_RankData[index+1])
        end
    )

    self.rankBody.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        local data = self.m_RankData[row+1]
        if data then
            CPlayerInfoMgr.ShowPlayerPopupMenu(data.playerId, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,AlignType.Default)
        end
    end)
    
    self.refreshAwardConstUI = false
    self.awardView = self.transform:Find("AwardView").gameObject
    self.awardTipText = self.awardView.transform:Find("TabBar/Tip"):GetComponent(typeof(UILabel))
    self.awardRemainText = self.awardView.transform:Find("TabBar/RemainText"):GetComponent(typeof(UILabel))
    self.awardTimeGoList = {}
    self.awardTime = self.awardView.transform:Find("TimesAward")
    for i = 1, 4 do
        table.insert(self.awardTimeGoList, self.awardTime:Find("TimesAwardItem" .. tostring(i)))
    end
    self.awardRankTable = self.awardView.transform:Find("RankList/TableBody/Table"):GetComponent(typeof(UITable))
    self.awardRankTemplate = self.awardView.transform:Find("RankList/TableBody/Pool/ItemTemplate")
    self.awardRankTemplate.gameObject:SetActive(false)
    
    self.refreshFurnaceConstUI = false
    self.furnaceView = self.transform:Find("FurnaceView").gameObject
    self.furnaceTipText = self.furnaceView.transform:Find("TabBar/Tip"):GetComponent(typeof(UILabel))
    self.furnaceRemainText = self.furnaceView.transform:Find("TabBar/RemainText"):GetComponent(typeof(UILabel))
    self.furnaceProgress = self.furnaceView.transform:Find("Center/IconTexture/ProgressTex"):GetComponent(typeof(UITexture))
    self.furnaceNormalBook = self.furnaceView.transform:Find("Center/IconTexture/BookNormal").gameObject
    self.furnaceHighlightBook = self.furnaceView.transform:Find("Center/IconTexture/BookHighlight").gameObject
    self.furnaceScoreText = self.furnaceView.transform:Find("FurnaceScore"):GetComponent(typeof(UILabel))
    self.furnaceExchangeBtn = self.furnaceView.transform:Find("ExchangeBtn").gameObject
    self.furnaceTipBtn = self.furnaceView.transform:Find("TipBtn").gameObject
    self.scorePerBook = ProfessionTransfer_Setting.GetData().TianShuScore
end

function LuaProfessionTransferBuyBookWnd:RefreshConstUI(myViewIndex)
    self:RefreshRemainTime()
    self.curOpenViewIdx = myViewIndex
    if myViewIndex == 1 then
        self.rankTipText.text = g_MessageMgr:FormatMessage("BUYBOOK_Rank_Tip")
    elseif myViewIndex == 2 then
        --刷新一些奖励的固定信息
        self.awardTipText.text = g_MessageMgr:FormatMessage("BUYBOOK_Rank_Tip")

        --次数奖励
        for i = 1, #self.awardTimeGoList do
            self.awardTimeGoList[i].gameObject:SetActive(false)
        end
        ProfessionTransfer_KaiShuTimesRewad.Foreach(function (k, v)
            if self.awardTimeGoList[k] then
                local go = self.awardTimeGoList[k].gameObject
                go:SetActive(true)
                local iconTexture = go.transform:Find("TextureMask/IconTexture"):GetComponent(typeof(CUITexture))
                local timesText = go.transform:Find("Info/Times"):GetComponent(typeof(UILabel))
                local lockIcon = go.transform:Find("Info/LockIcon")
                local checkIcon = go.transform:Find("Info/CheckIcon")
                local redDotGo = go.transform:Find("Info/RedDot").gameObject

                local lineDecoration = go.transform:Find("Line")

                lockIcon.gameObject:SetActive(false)
                checkIcon.gameObject:SetActive(false)
                redDotGo:SetActive(false)
                lineDecoration.gameObject:SetActive(k ~= 1)
                timesText.text = LocalString.GetString("开") .. v.Times .. LocalString.GetString("次")
                local ItemData = Item_Item.GetData(v.DisplayItems)
                if ItemData then iconTexture:LoadMaterial(ItemData.Icon) end
                UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function (_)
                    self:OnClickTimeAwardItem(go, k)
                end)
            end
        end)
        
        --排名奖励
        Extensions.RemoveAllChildren(self.awardRankTable.transform)
        ProfessionTransfer_KaiShuListAward.Foreach(function(k, v)
            local go = CUICommonDef.AddChild(self.awardRankTable.gameObject, self.awardRankTemplate.gameObject)
            go:SetActive(true)
            local rankLabel = go.transform:Find("Table/rankLabel"):GetComponent(typeof(UILabel))
            local rankDetailLabel = go.transform:Find("Table/rankLabel/Label"):GetComponent(typeof(UILabel))

            rankLabel.text = v.AwardDescription
            rankDetailLabel.text = v.Paiming
            local rankItemListTbl = go.transform:Find("Table/ItemList")
            local rankItemString = v.Award
            local itemPairList = g_LuaUtil:StrSplit(rankItemString,";")
            local len = #itemPairList-1
            for i = 1, len do
                local itemGo = rankItemListTbl:Find("Item" .. tostring(i))
                itemGo.gameObject:SetActive(true)
                local itemPair = g_LuaUtil:StrSplit(itemPairList[i],",")
                local itemId = tonumber(itemPair[1])
                local itemCnt = tonumber(itemPair[2])

                local iconTexture = itemGo.transform:Find("Normal/IconTexture"):GetComponent(typeof(CUITexture))
                local qualitySprite = itemGo:Find("QualitySprite"):GetComponent(typeof(UISprite))
                local descLabel = itemGo:Find("Normal/DescLabel"):GetComponent(typeof(UILabel))
                
                local ItemData = Item_Item.GetData(itemId)
                if ItemData then iconTexture:LoadMaterial(ItemData.Icon) end
                qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(CItem.GetQualityType(itemId))
                descLabel.text = itemCnt
                descLabel.gameObject:SetActive(itemCnt > 1)
                UIEventListener.Get(itemGo.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
                    CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
                end)
            end
            for i = len+1, 5 do
                local itemGo = rankItemListTbl:Find("Item" .. tostring(i))
                itemGo.gameObject:SetActive(false)
            end
        end)
        self.awardRankTable:Reposition()
    elseif myViewIndex == 3 then
        self.furnaceTipText.text = g_MessageMgr:FormatMessage("BUYBOOK_FURNACE_Tip")
    end
end

function LuaProfessionTransferBuyBookWnd:RefreshRemainTime()
    self:SetRemainTimeStr(self.remainText)
    self:SetRemainTimeStr(self.awardRemainText)
    self:SetRemainTimeStr(self.furnaceRemainText)
end

function LuaProfessionTransferBuyBookWnd:SetRemainTimeStr(labelComp)
    local SECONDS_PER_DAY = 24*60*60
    local curTime = CServerTimeMgr.Inst.timeStamp
    local endTime =  CServerTimeMgr.Inst:GetTimeStampByStr(ProfessionTransfer_Setting.GetData().KaiShuEndTime)
    if curTime >= endTime then
        labelComp.text = LocalString.GetString("本次活动已结束")
        labelComp.color = NGUIText.ParseColor24("FF5050", 0)
    else
        if endTime-curTime >= 3*SECONDS_PER_DAY then
            labelComp.color = NGUIText.ParseColor24("FFFFFF", 0)
        else
            labelComp.color = NGUIText.ParseColor24("FFFF00", 0)
        end

        if endTime-curTime > SECONDS_PER_DAY then
            local day = math.floor((endTime-curTime)/SECONDS_PER_DAY)
            local hour = math.floor((endTime-curTime)%SECONDS_PER_DAY/3600)
            if hour == 0 then
                labelComp.text = day .. LocalString.GetString("天")
            else
                labelComp.text = day .. LocalString.GetString("天") .. hour .. LocalString.GetString("小时")
            end
        else
            local hour = math.floor((endTime-curTime)/3600)
            local min = math.floor((endTime-curTime)%3600/60)
            local sec = math.floor((endTime-curTime)%60)
            labelComp.text = hour .. ":" .. min .. ":" .. sec
        end
    end
end


function LuaProfessionTransferBuyBookWnd:RefreshTimesAward()
    local openTime = LuaProfessionTransferMgr.kaishuData.kaiShuTimes and LuaProfessionTransferMgr.kaishuData.kaiShuTimes or 0
    local openRewardTbl = LuaProfessionTransferMgr.kaishuData.rewardData and LuaProfessionTransferMgr.kaishuData.rewardData or {}
    
    self:RefreshAwardTotalRedDot()
    local bigAwardTimes = 0
    for i = 1, #self.awardTimeGoList do
        local go = self.awardTimeGoList[i].gameObject
        local dataCfg = ProfessionTransfer_KaiShuTimesRewad.GetData(i)
        local lockIcon = go.transform:Find("Info/LockIcon")
        local checkIcon = go.transform:Find("Info/CheckIcon")
        local lineGo = go.transform:Find("Line")
        local redDotGo = go.transform:Find("Info/RedDot").gameObject

        local fxComp = go.transform:Find("Info/Fx"):GetComponent(typeof(CUIFx))
        local showRedDot = openTime >= dataCfg.Times and (not (openRewardTbl[i] and openRewardTbl[i] or false))
        
        if showRedDot then
            fxComp.gameObject:SetActive(true)
            LuaTweenUtils.DOKill(fxComp.transform, false)
            fxComp.transform.localPosition = Vector3(0, 0, 0)
            local b = NGUIMath.CalculateRelativeWidgetBounds(go.transform:Find("TextureMask/IconTexture"))
            local waypoints = CUICommonDef.GetWayPoints(Vector4(b.min.x, b.max.y, b.size.x, b.size.y), 1, true)
            fxComp.transform.localPosition = waypoints[0]
            fxComp:LoadFx("fx/ui/prefab/UI_kuang_blue02.prefab")
            LuaTweenUtils.DOLuaLocalPath(fxComp.transform, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, -1, Ease.Linear)
        else
            fxComp.gameObject:SetActive(false)
        end
        
        bigAwardTimes = math.max(bigAwardTimes, dataCfg.Times)
        redDotGo:SetActive(showRedDot)
        lockIcon.gameObject:SetActive(dataCfg.Times > openTime)
        checkIcon.gameObject:SetActive(openRewardTbl[i] and openRewardTbl[i] or false)
        lineGo.gameObject:SetActive(i ~= 1)
        lineGo:GetComponent(typeof(UISprite)).color = openTime >= dataCfg.Times and NGUIText.ParseColor24("63A4FF", 0) or NGUIText.ParseColor24("304369", 0)
    end
    
    if openTime < bigAwardTimes then
        self.awardView.transform:Find("TimesAward/Label/Times"):GetComponent(typeof(UILabel)).text = LocalString.GetString("当前累计") ..
                openTime .. LocalString.GetString("次")
    else
        self.awardView.transform:Find("TimesAward/Label/Times"):GetComponent(typeof(UILabel)).text = LocalString.GetString("已累计达到") ..
                bigAwardTimes .. LocalString.GetString("次")
    end    
end

function LuaProfessionTransferBuyBookWnd:RefreshAwardTotalRedDot()
    local openTime = LuaProfessionTransferMgr.kaishuData.kaiShuTimes and LuaProfessionTransferMgr.kaishuData.kaiShuTimes or 0
    local openRewardTbl = LuaProfessionTransferMgr.kaishuData.rewardData and LuaProfessionTransferMgr.kaishuData.rewardData or {}
    local showTotalRedDot = false
    for i = 1, #self.awardTimeGoList do
        local dataCfg = ProfessionTransfer_KaiShuTimesRewad.GetData(i)
        showTotalRedDot = showTotalRedDot or (openTime >= dataCfg.Times and (not (openRewardTbl[i] and openRewardTbl[i] or false)))
    end
    self.radioBox.transform:Find("Grid/Award/RedDot").gameObject:SetActive(showTotalRedDot)
end

function LuaProfessionTransferBuyBookWnd:OnClickTimeAwardItem(go, awardId)
    local openTime = LuaProfessionTransferMgr.kaishuData.kaiShuTimes and LuaProfessionTransferMgr.kaishuData.kaiShuTimes or 0
    local openRewardTbl = LuaProfessionTransferMgr.kaishuData.rewardData and LuaProfessionTransferMgr.kaishuData.rewardData or {}
    local dataCfg = ProfessionTransfer_KaiShuTimesRewad.GetData(awardId)
    
    if openTime >= dataCfg.Times then
        --检查是否已经领取过
        if openRewardTbl[awardId] and openRewardTbl[awardId] or false then
            --已领取过
            CItemInfoMgr.ShowLinkItemTemplateInfo(dataCfg.DisplayItems, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
        else
            --未领取过
            Gac2Gas.QMKS_RequestKaiShuReward(awardId)
        end
    else    
        --锁状态
        CItemInfoMgr.ShowLinkItemTemplateInfo(dataCfg.DisplayItems, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
    end
end

function LuaProfessionTransferBuyBookWnd:RefreshFurnaceScore()
    self.lastShowFurnaceScore = LuaProfessionTransferMgr.kaishuData.rongLuScore and LuaProfessionTransferMgr.kaishuData.rongLuScore or 0
    self.furnaceScoreText.text = self.lastShowFurnaceScore .. "/" .. self.scorePerBook .. LocalString.GetString("积分")
    self.furnaceProgress.fillAmount = (self.lastShowFurnaceScore/self.scorePerBook)
end

function LuaProfessionTransferBuyBookWnd:InitUIEvent()
    self.radioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn, index)
        if index == 0 then
            self.rankView:SetActive(true)
            self.awardView:SetActive(false)
            self.furnaceView:SetActive(false)
            if self.refreshRankConstUI == false then
                self:RefreshConstUI(index+1)
                self.refreshRankConstUI = true
            end
            Gac2Gas.SeasonRank_QueryRank(self.rankType, self.rankSeasonId)
        elseif index == 1 then
            self.rankView:SetActive(false)
            self.awardView:SetActive(true)
            self.furnaceView:SetActive(false)
            if self.refreshAwardConstUI == false then
                self:RefreshConstUI(index+1)
                self.refreshAwardConstUI = true
            end
            self:RefreshTimesAward()
        elseif index == 2 then
            self.rankView:SetActive(false)
            self.awardView:SetActive(false)
            self.furnaceView:SetActive(true)

            local aniComp = self.furnaceView.transform:GetComponent(typeof(Animation))
            aniComp:Play("buybookwnd_reset")
            
            if self.refreshFurnaceConstUI == false then
                self:RefreshConstUI(index+1)
                self.refreshFurnaceConstUI = true
            end
            self:RefreshFurnaceScore()
        end
    end)

    UIEventListener.Get(self.buyBtn).onClick = DelegateFactory.VoidDelegate(function (_)
        --点击可快速跳转至商城，并自动选中遁甲天书道具
        CShopMallMgr.ShowLinyuShoppingMall(Constants.DunJiaTianShuTemplateId)
    end)

    UIEventListener.Get(self.furnaceTipBtn).onClick = DelegateFactory.VoidDelegate(function (_)
        g_MessageMgr:ShowMessage("BUYBOOK_FURNACE_RULE")
    end)

    UIEventListener.Get(self.tipBtn).onClick = DelegateFactory.VoidDelegate(function (_)
        g_MessageMgr:ShowMessage("BUYBOOK_RANK_RULE")
    end)

    UIEventListener.Get(self.awardView.transform:Find("TimesAward/TipBtn").gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
        g_MessageMgr:ShowMessage("BUYBOOK_AWARD_RULE")
    end)

    UIEventListener.Get(self.furnaceExchangeBtn).onClick = DelegateFactory.VoidDelegate(function (_)
        CUIManager.ShowUI(CLuaUIResources.ProfessionTransferSkillbookItemSelectWnd)
    end)
end 


function LuaProfessionTransferBuyBookWnd:OnEnable()
    g_ScriptEvent:AddListener("SeasonRank_QueryRankResult", self, "OnSeasonRank_QueryRankResult")
    g_ScriptEvent:AddListener("QuanMinKaiShu_SyncPlayData", self, "OnSyncKaiShuPlayData")
    g_ScriptEvent:AddListener("QuanMinKaiShu_PlayAnimation", self, "OnPlayFurnaceAnimation")
    g_ScriptEvent:AddListener("SeasonRank_ReportRankDataSuccess", self, "OnSeasonRank_ReportRankDataSuccess")

end 

function LuaProfessionTransferBuyBookWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SeasonRank_QueryRankResult", self, "OnSeasonRank_QueryRankResult")
    g_ScriptEvent:RemoveListener("QuanMinKaiShu_SyncPlayData", self, "OnSyncKaiShuPlayData")
    g_ScriptEvent:RemoveListener("QuanMinKaiShu_PlayAnimation", self, "OnPlayFurnaceAnimation")
    g_ScriptEvent:RemoveListener("SeasonRank_ReportRankDataSuccess", self, "OnSeasonRank_ReportRankDataSuccess")

    LuaTweenUtils.Kill(self.addProgressTween, false)
end

function LuaProfessionTransferBuyBookWnd:OnSeasonRank_ReportRankDataSuccess(rankType, seasonId, data_U)
    local selectSeasonId = self.rankSeasonId
    local selectRankType = self.rankType
    if rankType == selectRankType and seasonId == selectSeasonId then
        Gac2Gas.SeasonRank_QueryRank(self.rankType, self.rankSeasonId)
    end
end

function LuaProfessionTransferBuyBookWnd:OnSeasonRank_QueryRankResult(rankType, seasonId, selfData, rankData)
    local showRankNum = 50
    self.m_RankData = {}
    local myId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    local myrank = 0
    local selectSeasonId = self.rankSeasonId
    local selectRankType = self.rankType
    if rankType == selectRankType and seasonId == selectSeasonId then
        local rawData = MsgPackImpl.unpack(selfData)
        local myRankData = {}
        if CommonDefs.IsDic(rawData) then
            local t = {}
            CommonDefs.DictIterate(rawData, DelegateFactory.Action_object_object(function (___key, ___value)
                t[tostring(___key)] = ___value
            end))
            myRankData = t
        end

        local rawData2 = MsgPackImpl.unpack(rankData)
        if rawData2.Count > 0 then
            for i = 1, math.min(rawData2.Count, showRankNum) do
                local rawData3 = rawData2[i-1]
                local t = {}
                if CommonDefs.IsDic(rawData3) then
                    CommonDefs.DictIterate(rawData3, DelegateFactory.Action_object_object(function (___key, ___value)
                        t[tostring(___key)] = ___value
                    end))
                end
                if t.playerId and t.playerId == myId then
                    myrank = i
                end
                table.insert(self.m_RankData, t)
            end
        end
        self.rankEmptyListText.gameObject:SetActive(rawData2.Count == 0)
        self:InitItem(self.myRankItem, myrank, myRankData, true)
        self.rankBody:ReloadData(true,false)
    end
end

function LuaProfessionTransferBuyBookWnd:InitItem(item, rank, info, isMyRankItem)
    local tf = item.transform
    local rankLabel = tf:Find("rank"):GetComponent(typeof(UILabel))
    local rankSprite = tf:Find("rank/rankImage"):GetComponent(typeof(UISprite))
    local professionSprite = tf:Find("profession"):GetComponent(typeof(UISprite))
    local nameLabel = tf:Find("name"):GetComponent(typeof(UILabel))
    local book100Label = tf:Find("book100Number"):GetComponent(typeof(UILabel))
    local book70Label = tf:Find("book70Number"):GetComponent(typeof(UILabel))
    local scoreLabel = tf:Find("score"):GetComponent(typeof(UILabel))
    if isMyRankItem == nil then
        isMyRankItem = false
    end

    if isMyRankItem then
        local myProfession = 0
        local myName = ""
        if CClientMainPlayer.Inst then
            myProfession = EnumToInt(CClientMainPlayer.Inst.Class)
            myName = CClientMainPlayer.Inst.Name
        end
        if info and info.name == nil then
            --从来没有挑战过
            rankLabel.text = LocalString.GetString("未上榜")
            rankSprite.spriteName = ""
            nameLabel.text = myName
            professionSprite.spriteName = Profession.GetIconByNumber(myProfession)
            book100Label.text = 0
            book70Label.text = 0
            scoreLabel.text = 0
        else
            --有数据
            rankLabel.text = ""
            rankSprite.spriteName = ""
            if rank == 1 then
                rankSprite.spriteName = "Rank_No.1"
            elseif rank == 2 then
                rankSprite.spriteName = "Rank_No.2png"
            elseif rank == 3 then
                rankSprite.spriteName = "Rank_No.3png"
            else
                rankLabel.text = rank > 0 and tostring(rank) or LocalString.GetString("未上榜")
            end
            nameLabel.text = info.name
            professionSprite.spriteName = Profession.GetIconByNumber(myProfession)
            book100Label.text = info.jueji100
            book70Label.text = info.jueji70
            scoreLabel.text = info.score
        end
        item.gameObject:SetActive(true)
    else
        rankLabel.text = ""
        rankSprite.spriteName = ""
        if rank == 1 then
            rankSprite.spriteName = "Rank_No.1"
        elseif rank == 2 then
            rankSprite.spriteName = "Rank_No.2png"
        elseif rank == 3 then
            rankSprite.spriteName = "Rank_No.3png"
        else
            rankLabel.text = rank > 0 and tostring(rank) or LocalString.GetString("未上榜")
        end

        rankLabel.color = NGUIText.ParseColor24("FFFFFF",0)
        nameLabel.text = info.name
        
        professionSprite.spriteName = Profession.GetIconByNumber(info.class)
        book100Label.text = info.jueji100
        book70Label.text = info.jueji70
        scoreLabel.text = info.score
    end
end

function  LuaProfessionTransferBuyBookWnd:OnSyncKaiShuPlayData()
    --在哪个界面刷新哪个界面的可变信息
    if self.curOpenViewIdx == 1 then
    elseif self.curOpenViewIdx == 2 then
        self:RefreshTimesAward()
    elseif self.curOpenViewIdx == 3 then    
        --self:RefreshFurnaceScore()
    end
end 

function LuaProfessionTransferBuyBookWnd:OnPlayFurnaceAnimation(aniType, addScore)
    if self.curOpenViewIdx == 3 then
        local aniComp = self.furnaceView.transform:GetComponent(typeof(Animation))
        if aniType == 1 then
            aniComp:Play("buybookwnd_in01")
        else
            aniComp:Play("buybookwnd_in02")
        end

        LuaTweenUtils.Kill(self.addProgressTween, false)
        if self.lastShowFurnaceScore == nil then
            self.lastShowFurnaceScore = 0
        end
        self.addProgressTween = LuaTweenUtils.TweenInt(self.lastShowFurnaceScore, self.lastShowFurnaceScore+addScore, 0.5, function ( val )
            --doNothing
            self.furnaceScoreText.text = (val%self.scorePerBook) .. "/" .. self.scorePerBook .. LocalString.GetString("积分")
            self.furnaceProgress.fillAmount = ((val%self.scorePerBook)/self.scorePerBook)
        end)
        LuaTweenUtils.SetDelay(self.addProgressTween, 0.3)
        LuaTweenUtils.OnComplete(self.addProgressTween, function()
            self.lastShowFurnaceScore = (self.lastShowFurnaceScore + addScore) % self.scorePerBook
        end)
        LuaTweenUtils.SetEase(self.addProgressTween, Ease.OutQuad)

    end
end 