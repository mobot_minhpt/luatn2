local DelegateFactory  = import "DelegateFactory"
local UIScrollView  = import "UIScrollView"
local UITable = import "UITable"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local AlignType = import "CPlayerInfoMgr+AlignType"
local Vector3 = import "UnityEngine.Vector3"
local CUITexture = import "L10.UI.CUITexture"
local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
--从原LuaGuildLeagueCrossTrainRankWnd修改而来
LuaGuildLeagueCrossZhanLingRankWnd = class()

RegistClassMember(LuaGuildLeagueCrossZhanLingRankWnd, "m_Template")
RegistClassMember(LuaGuildLeagueCrossZhanLingRankWnd, "m_ScrollView")
RegistClassMember(LuaGuildLeagueCrossZhanLingRankWnd, "m_Table")
RegistClassMember(LuaGuildLeagueCrossZhanLingRankWnd, "m_EmptyLabel")
RegistClassMember(LuaGuildLeagueCrossZhanLingRankWnd, "m_ItemCell")
RegistClassMember(LuaGuildLeagueCrossZhanLingRankWnd, "m_ItemGrid")

function LuaGuildLeagueCrossZhanLingRankWnd:Awake()
    self.m_Template = self.transform:Find("Anchor/ScrollView/Item").gameObject
    self.m_ScrollView = self.transform:Find("Anchor/ScrollView"):GetComponent(typeof(UIScrollView))
    self.m_Table = self.transform:Find("Anchor/ScrollView/Table"):GetComponent(typeof(UITable))
    self.m_EmptyLabel = self.transform:Find("Anchor/EmptyLabel").gameObject
    self.m_ItemCell = self.transform:Find("Anchor/ItemCell").gameObject
    self.m_ItemGrid = self.transform:Find("Anchor/ItemGrid"):GetComponent(typeof(UIGrid))
    self.m_Template:SetActive(false)
    self.m_ItemCell:SetActive(false)
end

function LuaGuildLeagueCrossZhanLingRankWnd:Init()
    self:InitItem()
    self:LoadData({})
    LuaGuildLeagueCrossMgr:QueryGuildLeagueCrossZhanLingRank()
end

function LuaGuildLeagueCrossZhanLingRankWnd:InitItem()
    Extensions.RemoveAllChildren(self.m_ItemGrid.transform)
    local items = GuildLeagueCross_Setting.GetData().ZhanLingAwardItem
    for i=0,items.Length-1,2 do
        local text,itemId = items[i], tonumber(items[i+1])
        local child = CUICommonDef.AddChild(self.m_ItemGrid.gameObject, self.m_ItemCell)
        child:SetActive(true)
        local iconTexture = child.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
        local textLabel = child.transform:Find("Label"):GetComponent(typeof(UILabel))
        iconTexture:LoadMaterial(Item_Item.GetData(itemId).Icon)
        textLabel.text = text
        UIEventListener.Get(child).onClick = DelegateFactory.VoidDelegate(function() self:OnItemCellClick(child, itemId) end)
    end
    self.m_ItemGrid:Reposition()
end

function LuaGuildLeagueCrossZhanLingRankWnd:OnEnable()
    g_ScriptEvent:AddListener("OnQueryGuildLeagueCrossZhanLingRankResult", self, "OnQueryGuildLeagueCrossZhanLingRankResult")
end

function LuaGuildLeagueCrossZhanLingRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnQueryGuildLeagueCrossZhanLingRankResult", self, "OnQueryGuildLeagueCrossZhanLingRankResult")
end

function LuaGuildLeagueCrossZhanLingRankWnd:OnQueryGuildLeagueCrossZhanLingRankResult(rankInfoTbl)
    self:LoadData(rankInfoTbl)
end

function LuaGuildLeagueCrossZhanLingRankWnd:LoadData(rankInfoTbl)
    self.m_EmptyLabel:SetActive(#rankInfoTbl==0)
    Extensions.RemoveAllChildren(self.m_Table.transform)

    for i=1, #rankInfoTbl do
        local child = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_Template)
        child:SetActive(true)
        local info = rankInfoTbl[i]
        self:InitOneRankInfo(child.transform, info)
        if i % 2 == 1 then
            child:GetComponent(typeof(CButton)):SetBackgroundSprite(g_sprites.OddBgSpirite)
        else
            child:GetComponent(typeof(CButton)):SetBackgroundSprite(g_sprites.EvenBgSprite)
        end
        UIEventListener.Get(child.gameObject).onClick = DelegateFactory.VoidDelegate(function() self:OnRankItemClick(child.gameObject, info) end)
    end
    self.m_Table:Reposition()
    self.m_ScrollView:ResetPosition()
end

function LuaGuildLeagueCrossZhanLingRankWnd:InitOneRankInfo(transRoot, rankInfo)
    transRoot:Find("Icon").gameObject:SetActive(rankInfo.isMe)
    local rankLabel = transRoot:Find("RankLabel"):GetComponent(typeof(UILabel))
    local rankIcon = transRoot:Find("RankLabel/RankIcon"):GetComponent(typeof(UISprite))
    local playerNameLabel = transRoot:Find("PlayerNameLabel"):GetComponent(typeof(UILabel))
    local professionIcon = transRoot:Find("PlayerNameLabel/ProfessionIcon"):GetComponent(typeof(UISprite))
    local zhanlingLevelLabel = transRoot:Find("ZhanLingLevelLabel"):GetComponent(typeof(UILabel))
    local scoreLabel = transRoot:Find("ScoreLabel"):GetComponent(typeof(UILabel)) 
    local color = rankInfo.isMe and NGUIText.ParseColor24("04DC16", 0) or NGUIText.ParseColor24("FFFFFF", 0)
    rankLabel.text = rankInfo.rank > 0 and tostring(rankInfo.rank) or LocalString.GetString("未上榜")
    rankLabel.color = color
    rankIcon.spriteName = self:GetRankSpriteName(rankInfo.rank)
    playerNameLabel.text = rankInfo.playerName
    professionIcon.spriteName = rankInfo.professionIcon
    playerNameLabel.color = color
    zhanlingLevelLabel.text = tostring(rankInfo.zhanlingLv)
    zhanlingLevelLabel.color = color
    scoreLabel.text = tostring(rankInfo.jade)
    scoreLabel.color = color
end

function LuaGuildLeagueCrossZhanLingRankWnd:GetRankSpriteName(rank)
    if rank == 1 then
        return g_sprites.RankFirstSpriteName
    elseif rank == 2 then
        return g_sprites.RankSecondSpriteName
    elseif rank == 3 then
        return g_sprites.RankThirdSpriteName
    else
        return ""
    end
end

function LuaGuildLeagueCrossZhanLingRankWnd:OnRankItemClick(go, info)
    CPlayerInfoMgr.ShowPlayerPopupMenu(info.playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, Vector3.zero, AlignType.Default)
end

function LuaGuildLeagueCrossZhanLingRankWnd:OnItemCellClick(go,itemId)
    CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, AlignType2.Default, 0, 0, 0, 0)
end



