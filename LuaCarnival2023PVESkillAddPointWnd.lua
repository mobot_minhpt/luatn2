local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"

LuaCarnival2023PVESkillAddPointWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaCarnival2023PVESkillAddPointWnd, "topLabel", "TopLabel", UILabel)
RegistChildComponent(LuaCarnival2023PVESkillAddPointWnd, "grid", "Grid", UIGrid)
RegistChildComponent(LuaCarnival2023PVESkillAddPointWnd, "okButton", "OkButton", GameObject)
--@endregion RegistChildComponent end

RegistClassMember(LuaCarnival2023PVESkillAddPointWnd, "buffData")
RegistClassMember(LuaCarnival2023PVESkillAddPointWnd, "childTable")
RegistClassMember(LuaCarnival2023PVESkillAddPointWnd, "addTextColor")

function LuaCarnival2023PVESkillAddPointWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.okButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOkButtonClick()
	end)
    --@endregion EventBind end
end

function LuaCarnival2023PVESkillAddPointWnd:OnEnable()
	g_ScriptEvent:AddListener("Carnival2023RequestPlayDataResult", self, "OnCarnival2023RequestPlayDataResult")
end

function LuaCarnival2023PVESkillAddPointWnd:OnDisable()
	g_ScriptEvent:RemoveListener("Carnival2023RequestPlayDataResult", self, "OnCarnival2023RequestPlayDataResult")
end

function LuaCarnival2023PVESkillAddPointWnd:OnCarnival2023RequestPlayDataResult(mReminTime, bReminTime, rReminTime)
	self.grid.gameObject:SetActive(true)
	self:InitData()
	self:InitChildTable()
end

function LuaCarnival2023PVESkillAddPointWnd:Init()
	self.addTextColor = "ACF9FF"
	if LuaCarnival2023Mgr.pveSkillInfo.needQueryData then
		self.topLabel.text = ""
		self.grid.gameObject:SetActive(false)
		Gac2Gas.Carnival2023RequestPlayData()
	else
		self:InitData()
		self:InitChildTable()
	end
end

function LuaCarnival2023PVESkillAddPointWnd:InitData()
	local normalData = Carnival2023_Normal.GetData()
	local skillInfo = LuaCarnival2023Mgr.pveSkillInfo
	local buffIds = {normalData.AttBuffCls, normalData.DefBuffCls, normalData.SpeedBuffCls}
	local buffInfos = {normalData.AttBuffInfo, normalData.DefBuffInfo, normalData.SpeedBuffInfo}
	local maxLevels = {normalData.AttBuffMaxLevel, normalData.DefBuffMaxLevel, normalData.SpeedBuffMaxLevel}
	local curLevels = {skillInfo.attackBuffLevel, skillInfo.defBuffLevel, skillInfo.speedBuffLevel}

	self.buffData = {}
	for i = 1, 3 do
		local tbl = {}
		tbl.buffId = buffIds[i] * 100 + 1
		tbl.curLevel = curLevels[i]
		tbl.maxLevel = maxLevels[i]

		local info = {}
		for desc, percent in string.gmatch(buffInfos[i], "([^,^;]+),(%d+)") do
			table.insert(info, {
				desc = desc,
				percent = tonumber(percent)
			})
		end
		tbl.buffInfo = info
		self.buffData[i] = tbl
	end
end

function LuaCarnival2023PVESkillAddPointWnd:InitChildTable()
	self.childTable = {}
	local matPaths = {
		"UI/Texture/Transparent/Material/pengdao_icon_gongji.mat",
		"UI/Texture/Transparent/Material/pengdao_icon_shengcun.mat",
		"UI/Texture/FestivalActivity/Festival_Carnival/Carnival2023/Material/carnival2023pveenterwnd_icon_minjie.mat",
	}
	for i = 1, 3 do
		local data = self.buffData[i]
		local child = self.grid.transform:GetChild(i - 1)
		child:Find("Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(matPaths[i])
		local addAndSubButton = child:Find("AddAndSubButton"):GetComponent(typeof(QnAddSubAndInputButton))
		self.childTable[i] = {
			levelLabel = child:Find("Level"):GetComponent(typeof(UILabel)),
			addAndSubButton = addAndSubButton,
			gain1Label = child:Find("Gain1"):GetComponent(typeof(UILabel)),
			gain2Label = child:Find("Gain2"):GetComponent(typeof(UILabel)),
		}
		addAndSubButton:SetMinMax(0, 1000, 1)
		addAndSubButton:SetValue(0, false)
		addAndSubButton.onValueChanged = DelegateFactory.Action_uint(function(value)
			self:OnValueChanged(i, value)
		end)

		child:Find("FullLevel").gameObject:SetActive(data.curLevel == data.maxLevel)
		addAndSubButton.gameObject:SetActive(data.curLevel ~= data.maxLevel)
	end

	for i = 1, 3 do
		self:UpdateLevel(i, 0)
	end
	self:UpdateLeftPoint()
end

-- 更新等级和增幅显示
function LuaCarnival2023PVESkillAddPointWnd:UpdateLevel(i, value)
	local data = self.buffData[i]
	local addLevelStr = value > 0 and SafeStringFormat3("[%s](+%d)[-]", self.addTextColor, value) or ""
	self.childTable[i].levelLabel.text = SafeStringFormat3(LocalString.GetString("%d%s级"), data.curLevel, addLevelStr)

	local gain1Info = data.buffInfo[1]
	local addGain1Str = value > 0 and SafeStringFormat3("[%s](+%d)[-]", self.addTextColor, value * gain1Info.percent) or ""
	self.childTable[i].gain1Label.text = SafeStringFormat3(LocalString.GetString("%s   %d%s%%"), gain1Info.desc, data.curLevel * gain1Info.percent, addGain1Str)

	local gain2Info = data.buffInfo[2]
	local addGain2Str = value > 0 and SafeStringFormat3("[%s](+%d)[-]", self.addTextColor, value * gain2Info.percent) or ""
	local str = i == 1 and LocalString.GetString("点") or "%"
	self.childTable[i].gain2Label.text = SafeStringFormat3(LocalString.GetString("%s   %d%s%s"), gain2Info.desc, data.curLevel * gain2Info.percent, addGain2Str, str)
end

-- 更新剩余点数和最大的加点数量显示
function LuaCarnival2023PVESkillAddPointWnd:UpdateLeftPoint()
	local usedPoint = 0
	for i = 1, 3 do
		usedPoint = usedPoint + self.childTable[i].addAndSubButton:GetValue()
	end
	local totalPoint = LuaCarnival2023Mgr.pveSkillInfo.skillPoint
	self.topLabel.text = SafeStringFormat3(LocalString.GetString("本次已分配点数 [%s]%d[-]/%d"), self.addTextColor, usedPoint, totalPoint)
	local leftPoint = totalPoint - usedPoint
	for i = 1, 3 do
		local addAndSubButton = self.childTable[i].addAndSubButton
		local maxLevel = self.buffData[i].maxLevel
		local curAddPoint = addAndSubButton:GetValue()
		local maxAddPoint = math.min(maxLevel - self.buffData[i].curLevel - curAddPoint, leftPoint)
		addAndSubButton:SetMinMax(0, curAddPoint + maxAddPoint, 1)
	end
end


--@region UIEvent

function LuaCarnival2023PVESkillAddPointWnd:OnOkButtonClick()
	local points = {}
	local hasValidPointNum = false
	for i = 1, 3 do
		points[i] = self.childTable[i].addAndSubButton:GetValue()
		if points[i] > 0 then
			hasValidPointNum = true
		end
	end

	if hasValidPointNum then
		Gac2Gas.Carnival2023RequestUpgradeSkill(points[1], points[2], points[3])
	end
	CUIManager.CloseUI(CLuaUIResources.Carnival2023PVESkillAddPointWnd)
end

function LuaCarnival2023PVESkillAddPointWnd:OnValueChanged(i, value)
	self:UpdateLevel(i, value)
	self:UpdateLeftPoint()
end

--@endregion UIEvent
