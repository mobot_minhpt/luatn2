local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local UIInput=import "UIInput"

LuaCarnival2020RealNameWnd = class()
RegistChildComponent(LuaCarnival2020RealNameWnd,"NameInput", GameObject)
RegistChildComponent(LuaCarnival2020RealNameWnd,"CardInput", GameObject)
RegistChildComponent(LuaCarnival2020RealNameWnd,"EnsureBtn", GameObject)
RegistChildComponent(LuaCarnival2020RealNameWnd,"CancelBtn", GameObject)
RegistChildComponent(LuaCarnival2020RealNameWnd,"CloseButton", GameObject)

RegistClassMember(LuaCarnival2020RealNameWnd, "maxChooseNum")

function LuaCarnival2020RealNameWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.Carnival2020RealNameWnd)
end

function LuaCarnival2020RealNameWnd:OnEnable()
	--g_ScriptEvent:AddListener("", self, "")
end

function LuaCarnival2020RealNameWnd:OnDisable()
	--g_ScriptEvent:RemoveListener("", self, "")
end

function LuaCarnival2020RealNameWnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.CloseButton,DelegateFactory.Action_GameObject(onCloseClick),false)

  self.nameInputS = self.NameInput.transform:Find('Input'):GetComponent(typeof(UIInput))
  self.cardInputS = self.CardInput.transform:Find('Input'):GetComponent(typeof(UIInput))

  self:InitButton()
end

function LuaCarnival2020RealNameWnd:InitButton()
	local onSubmitClick = function(go)
      local nameText = StringTrim(self.nameInputS.value)
      local cardText = StringTrim(self.cardInputS.value)
      if not nameText or nameText == "" then
        g_MessageMgr:ShowMessage("RM_ENTER_COMPLETE_IFO")
        return
      end
      if not cardText or cardText == "" then
        g_MessageMgr:ShowMessage("RM_ENTER_COMPLETE_IFO")
        return
      end

      local ensureText = LocalString.GetString('真实姓名：%s\n身份证号：%s\n[c][FF0000]以上信息一经提交将无法修改，请确保为您本人的真实信息并填写无误。如该信息与本手游账号登记的实名信息不一致，系统会在您付款成功后24小时内为您全额退款。现场需出示证件原件，人证合一方可兑票和检票入场，人证信息不一致将谢绝入内，敬请理解！[-]')
      local showText = SafeStringFormat3(ensureText,nameText,cardText)
      g_MessageMgr:ShowOkCancelMessage(showText, function ()
        Gac2Gas.SubmitJiaNianHua2020PersonalInfo(nameText,cardText)
        self:Close()
      end, nil,
      LocalString.GetString("确认提交"), LocalString.GetString("重新填写"), false)
	end
	CommonDefs.AddOnClickListener(self.EnsureBtn,DelegateFactory.Action_GameObject(onSubmitClick),false)
	local onCancelClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.CancelBtn,DelegateFactory.Action_GameObject(onCancelClick),false)
end

return LuaCarnival2020RealNameWnd
