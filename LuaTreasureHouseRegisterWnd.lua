require("3rdParty/ScriptEvent")
require("common/common_include")

local MessageMgr = import "L10.Game.MessageMgr"
local UIGrid = import "UIGrid"
local Cangbaoge_Condition = import "L10.Game.Cangbaoge_Condition"

LuaTreasureHouseRegisterWnd=class()

RegistClassMember(LuaTreasureHouseRegisterWnd, "InfoTable")
RegistClassMember(LuaTreasureHouseRegisterWnd, "ParagraphTemplate")
RegistClassMember(LuaTreasureHouseRegisterWnd, "RegisterBtn")

RegistClassMember(LuaTreasureHouseRegisterWnd, "RegisterConditionTable")
RegistClassMember(LuaTreasureHouseRegisterWnd, "CanRegister")

RegistClassMember(LuaTreasureHouseRegisterWnd, "BangDingConditionId")

function LuaTreasureHouseRegisterWnd:Init()
	self.BangDingConditionId = 9
	self:InitClassMembers()
	self:UpdateRequestInfos(LuaTreasureHouseMgr.RegisterInfo)
end

function LuaTreasureHouseRegisterWnd:InitClassMembers()
	self.InfoTable = self.transform:Find("Anchor/bg/ContentScrollView/InfoTable"):GetComponent(typeof(UIGrid))
	self.ParagraphTemplate = self.transform:Find("Anchor/bg/Templates/ParagraphTemplate").gameObject
	self.ParagraphTemplate:SetActive(false)
	self.RegisterBtn = self.transform:Find("Anchor/Buttons/RegisterBtn").gameObject

	self.RegisterConditionTable = {}
	self.CanRegister = false

	local onRegisterBtnClicked = function (go)
		self:OnRegisterBtnClicked(go)
	end
	CommonDefs.AddOnClickListener(self.RegisterBtn,DelegateFactory.Action_GameObject(onRegisterBtnClicked),false)
end

function LuaTreasureHouseRegisterWnd:OnEnable()
	g_ScriptEvent:AddListener("CBGSendRegisterConditionsInfo", self, "UpdateRequestInfos")
end

function LuaTreasureHouseRegisterWnd:OnDisable()
	g_ScriptEvent:RemoveListener("CBGSendRegisterConditionsInfo", self, "UpdateRequestInfos")
end

-- 更新登记要求
function LuaTreasureHouseRegisterWnd:UpdateRequestInfos(infos)
	if not infos then 
		CUIManager.CloseUI(CLuaUIResources.TreasureHouseRegisterWnd)
		return
	end

	self.RegisterConditionTable = infos
	local canRegister = true
	CUICommonDef.ClearTransform(self.InfoTable.transform)

	for k, v in ipairs(self.RegisterConditionTable) do

		local condition = Cangbaoge_Condition.GetData(v.conditionIdx)
		-- 部分条件隐藏不显示
		if condition.Hide == 0 then
			local go = NGUITools.AddChild(self.InfoTable.gameObject, self.ParagraphTemplate)
			go:SetActive(true)
			self:InitRequestInfo(go, k, v)
		end
		
		canRegister = canRegister  and v.ret
	end
	self.CanRegister = canRegister
	self.InfoTable:Reposition()
end

function LuaTreasureHouseRegisterWnd:InitRequestInfo(go, index, info)

	local requestLabel = go.transform:Find("Label"):GetComponent(typeof(UILabel))
	local resultSuccess = go.transform:Find("ResultSuccess").gameObject
	local resultFail = go.transform:Find("ResultFail").gameObject
	local bindPhoneBtn = go.transform:Find("BindPhoneBtn").gameObject

	resultSuccess:SetActive(false)
	resultFail:SetActive(false)
	bindPhoneBtn:SetActive(false)

	if not info then return end

	local condition = Cangbaoge_Condition.GetData(info.conditionIdx)
	if not condition then return end

	if info.ret then
		resultSuccess:SetActive(true)
		requestLabel.text = SafeStringFormat3("[FFFFFF]%s[-]", condition.Value)
	else
		resultFail:SetActive(true)
		requestLabel.text = SafeStringFormat3("[FF0000]%s[-]", condition.Value)

		if info.conditionIdx == self.BangDingConditionId then
			bindPhoneBtn:SetActive(true)
			resultFail:SetActive(false)
			local onBindPhoneBtnClicked = function (go)
				self:OnBindPhoneBtnClicked(go)
			end
			CommonDefs.AddOnClickListener(bindPhoneBtn,DelegateFactory.Action_GameObject(onBindPhoneBtnClicked), false)
		end
	end
end

function LuaTreasureHouseRegisterWnd:OnBindPhoneBtnClicked(go)
	Gac2Gas.CBG_RequestOnShelfSmsCode(CClientMainPlayer.Inst.Id, "")
end

function LuaTreasureHouseRegisterWnd:OnRegisterBtnClicked(go)
	if self.CanRegister then
		-- 请求注册
		Gac2Gas.CBG_RequestRegister(CClientMainPlayer.Inst.Id, "")
		CUIManager.CloseUI(CLuaUIResources.TreasureHouseRegisterWnd)
	else
		-- 提示无法注册的信息
		MessageMgr.Inst:ShowMessage("CBG_CANNOT_REGISTER", {})
	end
end

return LuaTreasureHouseRegisterWnd
