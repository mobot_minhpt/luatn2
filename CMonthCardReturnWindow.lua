-- Auto Generated!!
local CButton = import "L10.UI.CButton"
local CChargeWnd = import "L10.UI.CChargeWnd"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CMonthCardReturnWindow = import "L10.UI.CMonthCardReturnWindow"
local CPropertyItem = import "L10.Game.CPropertyItem"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local CWelfareMgr = import "L10.UI.CWelfareMgr"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local LocalString = import "LocalString"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local CChargeMgr = import "L10.Game.CChargeMgr"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local Object = import "System.Object"
CMonthCardReturnWindow.m_Init_CS2LuaHook = function (this)

    this.titleLabel.text = ""
    this.leftdaysLabel.text = ""
    this.slider.value = 0
    this.buttonLabel.text = LocalString.GetString("领   取")
    this.getBtn.Enabled = false
    this.cardInfo = CClientMainPlayer.Inst.ItemProp.MonthCard
    this.titleLabel.text = g_MessageMgr:FormatMessage("MonthCard_FuLi_Tip")
    if this.cardInfo ~= nil and this.cardInfo.BuyTime ~= 0 then
        local leftDay = CChargeWnd.MonthCardLeftDay(this.cardInfo)
        if leftDay > 0 then
            this:InitMonthCard(leftDay)
            return
        end
    end
    this:InitGoToCharge()
end
CMonthCardReturnWindow.m_InitMonthCard_CS2LuaHook = function (this, leftDay)
    UIEventListener.Get(this.getBtn.gameObject).onClick = MakeDelegateFromCSFunction(this.OnGetBtnClick, VoidDelegate, this)
    local hasGetToday = CPropertyItem.IsSamePeriod(this.cardInfo.LastReturnTime, "d")
    if leftDay > 10 then
        this.less10Days:SetActive(false)
        this.getBtn.gameObject:SetActive(true)
        this.leftdaysLabel.text = System.String.Format(LocalString.GetString("剩余{0}/30天"), leftDay)
        this.getBtn.Enabled = not hasGetToday
        this.alertButton1.enabled = not hasGetToday or CWelfareMgr.ShowMonthCardAlert()
        this.sliderForeground.spriteName = this.normalSlider
    else
        this.leftdaysLabel.text = System.String.Format(LocalString.GetString("剩余[c][FF0000]{0}[-][/c]/30天"), leftDay)
        this.less10Days:SetActive(true)
        this.getBtn.gameObject:SetActive(false)
        this.getButton2.Enabled = not hasGetToday
        this.alertButton2.enabled = not hasGetToday
        this.sliderForeground.spriteName = this.urgentSlider
    end
    this.slider.value = leftDay / 30
end
CMonthCardReturnWindow.m_InitGoToCharge_CS2LuaHook = function (this)
    if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.MaxLevel >= 1 then
        --renewalTipsLabel.text = "";
        this.leftdaysLabel.text = LocalString.GetString("剩余[c][FF0000]0[-][/c]/30天")
        this.slider.value = 0
        this.buttonLabel.text = LocalString.GetString("前往充值")
        CommonDefs.GetComponent_Component_Type(this.getBtn, typeof(CButton)).Enabled = true
        this.alertButton1.enabled = CWelfareMgr.ShowMonthCardAlert()
        UIEventListener.Get(this.getBtn.gameObject).onClick = MakeDelegateFromCSFunction(this.OnGoChargeBtnClick, VoidDelegate, this)
    end
end
CMonthCardReturnWindow.m_OnGoChargeBtnClick_CS2LuaHook = function (this, go)
    this.alertButton1.enabled = false
    CWelfareMgr.SetMonthCardLastCheckTime()
    EventManager.Broadcast(EnumEventType.OnMonthCardAlertStatusChange)
    CShopMallMgr.ShowChargeWnd()
end

CMonthCardReturnWindow.m_OnEnable_CS2LuaHook = function (this)
    this:Init()
    EventManager.AddListener(EnumEventType.ItemPropUpdated, MakeDelegateFromCSFunction(this.Init, Action0, this))
    UIEventListener.Get(this.getButton2.gameObject).onClick = MakeDelegateFromCSFunction(this.OnGetBtnClick, VoidDelegate, this)
    UIEventListener.Get(this.chargeButton2.gameObject).onClick = MakeDelegateFromCSFunction(this.OnGoChargeBtnClick, VoidDelegate, this)
    if this.sendButton then
        this.sendButton.gameObject:SetActive(CSwitchMgr.EnableBuyMonthCardForFriend)
        UIEventListener.Get(this.sendButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            local vipLevel = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.Vip.Level or 0
            if vipLevel < 6 then
                g_MessageMgr:ShowMessage("Yueka_VIP_Not_Enough", vipLevel)
                return
            end
            CChargeMgr.Inst:BuyMonthCardForFriend()
        end)
    end
    CChargeMgr.EnableBuyMonthCardForFriend = false
end

CMonthCardReturnWindow.m_OnDisable_CS2LuaHook = function (this)
    EventManager.RemoveListener(EnumEventType.ItemPropUpdated, MakeDelegateFromCSFunction(this.Init, Action0, this))
    CChargeMgr.EnableBuyMonthCardForFriend = true
end

CMonthCardReturnWindow.m_hookOnGetBtnClick = function (this, go)
    local leftDay = CChargeWnd.MonthCardLeftDay(this.cardInfo)
    Gac2Gas.RequestGetMonthCardReturn()
    if leftDay == 1 then
        local msg = g_MessageMgr:FormatMessage("CUSTOM_STRING1", LocalString.GetString("月卡已到期，是否续费？"))
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
            CChargeMgr.Inst:BuyMonthCardForMyself()
            local dict = CreateFromClass(MakeGenericClass(Dictionary, String, Object))
            Gac2Gas.RequestRecordClientLog("LastDayGetBtn_BuyMonthCardForSelf", MsgPackImpl.pack(dict))
        end), nil, LocalString.GetString("确定"), LocalString.GetString("取消"))
    end
end