luaFamilyMgr = {}

luaFamilyMgr.m_TagData = {}         --标签数据
luaFamilyMgr.m_TagNameData = {}     --标签Name数据
luaFamilyMgr.m_TagIndexData = {}     --通过Index索引标签数据
luaFamilyMgr.DelTagId = ""          --要删除的标签，在删除标签时赋值

luaFamilyMgr.m_selfTagId = ""       --当前家谱自己写的标签ID（没有则为""）
luaFamilyMgr.m_favoredTagId = ""    --当前家谱点过赞的标签（没有则为""）
luaFamilyMgr.TagCenterPlayerID = 0  --当前家谱数据拥有者

luaFamilyMgr.ColorTable = {"499dee","6833ff","d051ff","ff008e","ff3600","cc6001","ffa800","fff263","20bc62","1c9689"}

luaFamilyMgr.TagRpcBack = false
--查询标签信息
function Gas2Gac.SendPlayerFamilyTreeTags(playerId, param, tagInfoUD, selfTagId, favorListUD)
    luaFamilyMgr.TagCenterPlayerID = playerId
    luaFamilyMgr.TagRpcBack = true
    luaFamilyMgr.m_selfTagId = selfTagId
	local favorList = MsgPackImpl.unpack(favorListUD)
	if favorList and favorList.Count > 0 then
		luaFamilyMgr.m_favoredTagId = favorList[0]
	end

    local tagInfo = MsgPackImpl.unpack(tagInfoUD)
	if tagInfo then
		for i = 0, tagInfo.Count - 1, 4 do
            local t = {}
			t.tagId = tagInfo[i]
			t.tagName = tagInfo[i + 1]
            t.favorNum = tagInfo[i + 2]
			t.createTime = tagInfo[i + 3]
            luaFamilyMgr.m_TagNameData[t.tagName] = 1  --缓存一下用来检测有无重复标签Name
            luaFamilyMgr.m_TagIndexData[t.tagId] = t
            table.insert(luaFamilyMgr.m_TagData,t)
		end
    end
    if LuaShiTuMgr.m_FamilyTreeShiMenInfo and LuaShiTuMgr.m_FamilyTreeShiMenInfo.isBestTudi then
        local t = {tagId = 0,tagName = LocalString.GetString("优秀弟子"),favorNum = 0,createTime = 0}
        table.insert(luaFamilyMgr.m_TagData, t)
        luaFamilyMgr.m_TagNameData[t.tagName] = 1
        luaFamilyMgr.m_TagIndexData[0] = t
    end
    table.sort(luaFamilyMgr.m_TagData, function(a, b)
		if a.favorNum == b.favorNum then
			return a.createTime < b.createTime 
		else
			return a.favorNum > b.favorNum
		end
	end)

    g_ScriptEvent:BroadcastInLua("SendPlayerFamilyTreeTags")
end

luaFamilyMgr.DetailTable = {}           --标签详细界面数据
luaFamilyMgr.FavorNumLimit = 0
function Gas2Gac.SendPlayerFamilyTreeTagDetail(playerId, tagId, tagName, writerId, writerName, favorNum, favorPlayerListUD, favored)
    if luaFamilyMgr.FavorNumLimit == 0 then
        luaFamilyMgr.FavorNumLimit = FamilyTree_Setting.GetData().FavorPlayerNameNumLimit
    end
	-- print("SendPlayerFamilyTreeTagDetail", playerId, tagId, tagName, writerId, writerName, favorNum, favored,luaFamilyMgr.FavorNumLimit)
    local list = MsgPackImpl.unpack(favorPlayerListUD)
    local favorTable = {}
	if list then
        for i = 0, list.Count - 1, 2 do
            local t = {}
			t.id = list[i]
            t.name = list[i + 1]
            table.insert( favorTable, t)
		end
    end
    luaFamilyMgr.DetailTable = {}           --标签详细界面数据
    luaFamilyMgr.DetailTable.playerId           = playerId
    luaFamilyMgr.DetailTable.tagId              = tagId
    luaFamilyMgr.DetailTable.tagName            = tagName
    luaFamilyMgr.DetailTable.writerId           = writerId
    luaFamilyMgr.DetailTable.writerName         = writerName
    luaFamilyMgr.DetailTable.favorNum           = favorNum
    luaFamilyMgr.DetailTable.favored            = favored
    luaFamilyMgr.DetailTable.favorTable         = favorTable

    CUIManager.ShowUI(CLuaUIResources.FamilyTagDetailWnd)
end

function Gas2Gac.OpenAddPlayerFamilyTreeTagWnd(playerId)
    -- print("OpenAddPlayerFamilyTreeTagWnd", playerId)
    if luaFamilyMgr.TagRpcBack then
        CUIManager.ShowUI(CLuaUIResources.LuaAddTagWnd)
    end
end

function Gas2Gac.TagPlayerFamilyTreeSuccess(playerId, tag, tagId, opTagId, opType, favorNum, createTime)
    if luaFamilyMgr.TagCenterPlayerID ~= playerId then return end
    if opType == EnumFamilyTreeTagOperationType.eDel or opType == EnumFamilyTreeTagOperationType.eDelOther or opType == EnumFamilyTreeTagOperationType.eModify then
        for i=#luaFamilyMgr.m_TagData, 1,-1 do
            if luaFamilyMgr.m_TagData[i].tagId == opTagId then
                table.remove( luaFamilyMgr.m_TagData, i)
            end
        end
        luaFamilyMgr.m_TagNameData[luaFamilyMgr.m_TagIndexData[opTagId].tagName] = nil
        luaFamilyMgr.m_TagIndexData[opTagId] = nil
        if luaFamilyMgr.m_selfTagId == opTagId then
            luaFamilyMgr.m_selfTagId = ""
        end
        if luaFamilyMgr.m_favoredTagId == opTagId then
            luaFamilyMgr.m_favoredTagId = ""    
        end
    end

    if opType == EnumFamilyTreeTagOperationType.eAdd or opType == EnumFamilyTreeTagOperationType.eModify then
        local t = {}
        t.tagId = tagId
        t.tagName = tag
        t.favorNum = favorNum
		t.createTime = createTime
        table.insert(luaFamilyMgr.m_TagData,t)
        luaFamilyMgr.m_TagIndexData[t.tagId] = t
        luaFamilyMgr.m_TagNameData[t.tagName] = 1  --缓存一下用来检测有无重复标签Name
        luaFamilyMgr.m_selfTagId = tagId
    end
    table.sort(luaFamilyMgr.m_TagData, function(a, b)
		if a.favorNum == b.favorNum then
			return a.createTime < b.createTime 
		else
			return a.favorNum > b.favorNum
		end
	end)

    g_ScriptEvent:BroadcastInLua("TagPlayerFamilyTreeSuccess")
end

function Gas2Gac.FavorPlayerFamilyTreeTagSuccess(playerId, tagId, favor, favorNum)
    if luaFamilyMgr.TagCenterPlayerID ~= playerId then return end
    local t = {}
    t.id = CClientMainPlayer.Inst.Id
    t.name = CClientMainPlayer.Inst.RealName

    if favor then
        luaFamilyMgr.m_favoredTagId = tagId
        if #luaFamilyMgr.DetailTable.favorTable >= luaFamilyMgr.FavorNumLimit then
            table.remove(luaFamilyMgr.DetailTable.favorTable)
            table.insert(luaFamilyMgr.DetailTable.favorTable,1,t)
        else
            table.insert(luaFamilyMgr.DetailTable.favorTable,1,t)
        end
    else
        luaFamilyMgr.m_favoredTagId = ""
        for i=#luaFamilyMgr.DetailTable.favorTable,1,-1 do
            if luaFamilyMgr.DetailTable.favorTable[i].id == t.id then
                table.remove( luaFamilyMgr.DetailTable.favorTable, i)
            end
        end
    end
    luaFamilyMgr.m_TagIndexData[tagId].favorNum = favorNum
    g_ScriptEvent:BroadcastInLua("FavorPlayerFamilyTreeTagSuccess",playerId, tagId, favor, favorNum)
end

function luaFamilyMgr:ClearData()
    luaFamilyMgr.m_TagData = {}         --标签数据
    luaFamilyMgr.m_TagNameData = {}     --标签Name数据
    luaFamilyMgr.m_TagIndexData = {}    --通过Index索引标签数据
    luaFamilyMgr.DelTagId = ""          --要删除的标签，在删除标签时赋值
    luaFamilyMgr.m_selfTagId = ""       --当前家谱自己写的标签ID（没有则为""）
    luaFamilyMgr.m_favoredTagId = ""    --当前家谱点过赞的标签（没有则为""）
    luaFamilyMgr.TagCenterPlayerID = 0
    luaFamilyMgr.TagRpcBack = false
end
