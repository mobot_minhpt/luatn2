local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnTableView = import "L10.UI.QnTableView"
local DefaultTableViewDataSource = import "L10.UI.DefaultTableViewDataSource"
local CUITexture = import "L10.UI.CUITexture"
local UILabel = import "UILabel"
local CExpressionActionMgr = import "L10.Game.CExpressionActionMgr"

LuaCommonExpressionSelectionWnd = class()
RegistClassMember(LuaCommonExpressionSelectionWnd, "m_TitleLabel")
RegistClassMember(LuaCommonExpressionSelectionWnd, "m_TableView")
RegistClassMember(LuaCommonExpressionSelectionWnd, "m_ConfirmButton")
RegistClassMember(LuaCommonExpressionSelectionWnd, "m_DataList")
RegistClassMember(LuaCommonExpressionSelectionWnd, "m_SelectedInfo")

function LuaCommonExpressionSelectionWnd:Awake()
    self.m_TitleLabel   = self.transform:Find("Anchor/TitleLabel"):GetComponent(typeof(UILabel))
    self.m_TableView    = self.transform:Find("Anchor/Expressions"):GetComponent(typeof(QnTableView)) 
    self.m_ConfirmButton  = self.transform:Find("Anchor/Button").gameObject

    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create2(
        function()
            return self:NumberOfRows()
        end,
        function(view, index)
            return self:ItemAt(view, index)
        end
    )

	CommonDefs.AddOnClickListener(self.m_ConfirmButton, DelegateFactory.Action_GameObject(function(go) self:OnConfirmButtonClick() end), false)
end

function LuaCommonExpressionSelectionWnd:Init()
    local wndInfo = LuaExpressionMgr.m_CommonExpressionSelectionWndInfo
    self.m_TitleLabel.text = wndInfo.title
    self.m_DataList = {}
    if wndInfo.includeFxExpression then
        local fuxiDatas = LuaFuxiAniMgr.GetAvaliableFuxiExpressionData()
        if fuxiDatas then
            for __, fuxiData in pairs(fuxiDatas) do
                table.insert(self.m_DataList, {isRegular = false, info = fuxiData})
            end
        end
    end
    local regularDatas = CExpressionActionMgr.Inst:GetAvaliableExpressionActionInfos()
    if regularDatas then
        for i=0,regularDatas.Count-1 do
            local data = Expression_Define.GetData(regularDatas[i]:GetID())
            if data then
                if (not wndInfo.includeDoublePlayerExpression and data.TargetPlayerDist <= 0) or wndInfo.includeDoublePlayerExpression then
                    table.insert(self.m_DataList, {isRegular = true, info = regularDatas[i]})
                end
            end
        end
    end

    self.m_TableView:ReloadData(true, false)
end

function LuaCommonExpressionSelectionWnd:NumberOfRows()
    return #self.m_DataList
end

function LuaCommonExpressionSelectionWnd:ItemAt(view, index)
    local data = self.m_DataList[index+1]
    if data.isRegular then   
        local item = view:GetFromPool(0)
        self:InitRegularExpression(item, data)
        return item
    else
        local item = view:GetFromPool(1)
        self:InitFuXiExpression(item, data)
        return item
    end
end

function LuaCommonExpressionSelectionWnd:InitRegularExpression(item, data)
    local expressionActionInfo = data.info
    local iconTexture = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    iconTexture:LoadMaterial(expressionActionInfo:GetIcon())
    nameLabel.text = expressionActionInfo:GetName()
    item.OnSelected = DelegateFactory.Action_GameObject(function (go)
        self.m_SelectedInfo = data
    end)
end

function LuaCommonExpressionSelectionWnd:InitFuXiExpression(item, data)
    local fxActionInfo = data.info
    local bigNameLabel = item.transform:Find("Icon/BigNameLabel"):GetComponent(typeof(UILabel))
    local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    bigNameLabel.text = CommonDefs.StringAt(fxActionInfo.motionName,0)
    nameLabel.text = fxActionInfo.motionName
    item.OnSelected = DelegateFactory.Action_GameObject(function (go)
        self.m_SelectedInfo = data
    end)
end

function LuaCommonExpressionSelectionWnd:OnConfirmButtonClick()
    if not self.m_SelectedInfo then
        g_MessageMgr:ShowMessage("COMMON_EXPRESSION_SELECTION_WND_NO_SELECTION")
        return
    end
    LuaExpressionMgr:OnCommonExpressionSelectionWndCallback(self.m_SelectedInfo)
    CUIManager.CloseUI("CommonExpressionSelectionWnd")
end

function LuaCommonExpressionSelectionWnd:OnDestroy()
    --关闭窗口时释放callback引用，避免相关资源无法及时回收
    local wndInfo = LuaExpressionMgr.m_CommonExpressionSelectionWndInfo
    if wndInfo.callback then
        wndInfo.callback = nil
    end
end
