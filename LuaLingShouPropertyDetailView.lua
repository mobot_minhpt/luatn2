local CGuideMgr=import "L10.Game.Guide.CGuideMgr"
local CItemMgr=import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CScene=import "L10.Game.CScene"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local Collider=import "UnityEngine.Collider"
local Skill_AllSkills=import "L10.Game.Skill_AllSkills"
local CSkillInfoMgr=import "L10.UI.CSkillInfoMgr"
local CUITexture=import "L10.UI.CUITexture"
-- local UILabel = import "UILabel"
local MessageWndManager = import "L10.UI.MessageWndManager"
local LingShou_LingShou = import "L10.Game.LingShou_LingShou"
-- local EventManager = import "EventManager"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
-- local EnumEventType = import "EnumEventType"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local CLingShouMgr = import "L10.Game.CLingShouMgr"
local CInputBoxMgr = import "L10.UI.CInputBoxMgr"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"

local CLingShouModelTextureLoader=import "L10.UI.CLingShouModelTextureLoader"
local CCurentMoneyCtrl=import "L10.UI.CCurentMoneyCtrl"
local QnTabView=import "L10.UI.QnTabView"
local Constants = import "L10.Game.Constants"


CLuaLingShouPropertyDetailView = class()
RegistClassMember(CLuaLingShouPropertyDetailView,"skills")
RegistClassMember(CLuaLingShouPropertyDetailView,"detailsDisplay")
RegistClassMember(CLuaLingShouPropertyDetailView,"renameBtn")
RegistClassMember(CLuaLingShouPropertyDetailView,"textureLoader")
-- RegistClassMember(CLuaLingShouPropertyDetailView,"qnTabView")
RegistClassMember(CLuaLingShouPropertyDetailView,"fangshengBtn")
RegistClassMember(CLuaLingShouPropertyDetailView,"xiuxiBtn")
RegistClassMember(CLuaLingShouPropertyDetailView,"addExpBtn")
RegistClassMember(CLuaLingShouPropertyDetailView,"addShouMingBtn")
RegistClassMember(CLuaLingShouPropertyDetailView,"lingShouTable")
RegistClassMember(CLuaLingShouPropertyDetailView,"tipButton")
RegistClassMember(CLuaLingShouPropertyDetailView,"contentGo")
RegistClassMember(CLuaLingShouPropertyDetailView,"exchangeFurnitureGO")
RegistClassMember(CLuaLingShouPropertyDetailView,"moneyBox")
RegistClassMember(CLuaLingShouPropertyDetailView,"questionBtn")
RegistClassMember(CLuaLingShouPropertyDetailView,"exchangeBtn")
RegistClassMember(CLuaLingShouPropertyDetailView,"m_LeaveStartTime")
RegistClassMember(CLuaLingShouPropertyDetailView,"m_LeaveDuration")
RegistClassMember(CLuaLingShouPropertyDetailView,"m_Tick")
RegistClassMember(CLuaLingShouPropertyDetailView,"m_WndTabView")
RegistClassMember(CLuaLingShouPropertyDetailView,"m_LevelLimitButton")

function CLuaLingShouPropertyDetailView:Awake()
    self.m_WndTabView=self.transform.parent:GetComponent(typeof(QnTabView))
    local script = self.transform:Find("Content/Property"):GetComponent(typeof(CCommonLuaScript))
    script:Awake()
    self.detailsDisplay = script.m_LuaSelf

    self.renameBtn = self.transform:Find("Content/RenameBtn").gameObject
    self.textureLoader = self.transform:Find("Content/Texture"):GetComponent(typeof(CLingShouModelTextureLoader))
    -- self.qnTabView = self.transform:Find("Content/TabBar"):GetComponent(typeof(QnTabView))
    self.fangshengBtn = self.transform:Find("Content/Normal/FangshengBtn").gameObject
    self.xiuxiBtn = self.transform:Find("Content/Normal/Buttons/XiuxiBtn").gameObject
    self.addExpBtn = self.transform:Find("Content/Property/BaseProperty/ExpSlider/AddExpBtn").gameObject
    self.addShouMingBtn = self.transform:Find("Content/Property/BaseProperty/ShouMingSlider/AddShouMingBtn").gameObject
    self.lingShouTable = self.transform.parent:Find("LingShouTable")
    self.tipButton = self.transform:Find("Content/Property/DetailProperty/Panel/Group1/TipButton").gameObject
    self.contentGo = self.transform:Find("Content").gameObject
    self.exchangeFurnitureGO = self.transform:Find("Content/Normal/ExchangeFurniture").gameObject
    self.moneyBox = self.transform:Find("Content/Normal/ExchangeFurniture/QnCostAndOwnMoney"):GetComponent(typeof(CCurentMoneyCtrl))
    self.questionBtn = self.transform:Find("Content/Normal/ExchangeFurniture/BtnQuestion").gameObject
    self.exchangeBtn = self.transform:Find("Content/Normal/ExchangeFurniture/BtnExchange").gameObject
    self.m_LevelLimitButton = self.transform:Find("Content/LevelLimitButton").gameObject
    self.m_LeaveStartTime = 0
    self.m_LeaveDuration = 0
    self.m_Tick = nil

    self:InitNull()

    UIEventListener.Get(self.renameBtn).onClick = DelegateFactory.VoidDelegate(function (p) self:Rename(p) end)

    UIEventListener.Get(self.fangshengBtn).onClick = DelegateFactory.VoidDelegate(function (p) self:FangSheng(p) end)

    UIEventListener.Get(self.addExpBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        CLuaLingShouUseWnd.useType = 0--CLingShouUseWnd.UseType.Exp
        CUIManager.ShowUI(CUIResources.LingShouUseWnd)
    end)
    UIEventListener.Get(self.addShouMingBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        CLuaLingShouUseWnd.useType = 1
        CUIManager.ShowUI(CUIResources.LingShouUseWnd)
    end)

    UIEventListener.Get(self.tipButton).onClick = DelegateFactory.VoidDelegate(function (p)
        g_MessageMgr:ShowMessage("LINGSHOU_NENGLI_TIP")
    end)

    self.m_LevelLimitButton:SetActive(false)
    if IsLingShouLevelLimitOpen() then
        UIEventListener.Get(self.m_LevelLimitButton).onClick = DelegateFactory.VoidDelegate(function(go)
            LuaLingShouLevelLimitWnd.s_SelectLingShouId = CLingShouMgr.Inst.selectedLingShou
            CUIManager.ShowUI(CLuaUIResources.LingShouLevelLimitWnd)
        end)
    end
    self:Init4ExchangeFurniture()

    self:InitButtons()
end
function CLuaLingShouPropertyDetailView:Init4ExchangeFurniture( )
    if CLingShouMgr.Inst.isExchangeFurniture then
        self.fangshengBtn:SetActive(false)
        self.renameBtn:SetActive(false)
        self.addExpBtn:SetActive(false)
        self.addShouMingBtn:SetActive(false)
        self.exchangeFurnitureGO:SetActive(true)
        UIEventListener.Get(self.questionBtn).onClick = DelegateFactory.VoidDelegate(function (p) g_MessageMgr:ShowMessage("Lingshou_Exchange_Furniture_Readme") end)
        UIEventListener.Get(self.exchangeBtn).onClick = DelegateFactory.VoidDelegate(function (p) self:OnExchangeClick(p) end)

        local silverNeed = Zhuangshiwu_Setting.GetData().ExchangeLingshouFurnitureSilver
        self.moneyBox:SetType(EnumMoneyType.YinLiang, EnumPlayScoreKey.NONE, true)
        self.moneyBox:SetCost(silverNeed)
    else
        self.renameBtn:SetActive(true)
        self.addExpBtn:SetActive(true)
        self.addShouMingBtn:SetActive(true)
        self.fangshengBtn:SetActive(true)
        self.exchangeFurnitureGO:SetActive(false)
    end
end
function CLuaLingShouPropertyDetailView:OnExchangeClick( go)
    local details = CLingShouMgr.Inst:GetLingShouDetails(CLingShouMgr.Inst.selectedLingShou)
    if details == nil then
        return
    end
    if CLingShouMgr.Inst:IsOnAssist(CLingShouMgr.Inst.selectedLingShou) or CLingShouMgr.Inst:IsOnBattle(CLingShouMgr.Inst.selectedLingShou) then
        g_MessageMgr:ShowMessage("Lingshou_Exchange_Furniture_Under_Fight")
        return
    end

    local msg = g_MessageMgr:FormatMessage("Lingshou_Exchange_Furniture_Info", details.data.Name)
    MessageWndManager.ShowDelayOKCancelMessage(msg, DelegateFactory.Action(function ()
        if not System.String.IsNullOrEmpty(CLingShouMgr.Inst.selectedLingShou) then
            Gac2Gas.RequestExchangeLingshouToFurniture(CLingShouMgr.Inst.selectedLingShou)
        end
    end), nil, 3)
end
function CLuaLingShouPropertyDetailView:FangSheng( go)
    -- 特殊处理一下全民PK服，屏蔽灵兽放生功能
    if CQuanMinPKMgr.Inst.m_IsQuanMinPKServer then
        g_MessageMgr:ShowMessage("NOT_ALLOW_IN_QUANMINPK")
        return
    end
    if CLingShouMgr.Inst:IsFuTi(CLingShouMgr.Inst.selectedLingShou) then
        g_MessageMgr:ShowMessage("HuaLing_LingShou_CanNot_Do_This")
        return
    end
    if CLingShouMgr.Inst:IsOnJieBan(CLingShouMgr.Inst.selectedLingShou) then
        g_MessageMgr:ShowMessage("CANNOT_FREE_PARTNER_LINGSHOU")
        return
    end

    local details = CLingShouMgr.Inst:GetLingShouDetails(CLingShouMgr.Inst.selectedLingShou)

    if details == nil then
        return
    end
    if CLingShouMgr.Inst:IsOnAssist(CLingShouMgr.Inst.selectedLingShou) or CLingShouMgr.Inst:IsOnBattle(CLingShouMgr.Inst.selectedLingShou) then
        g_MessageMgr:ShowMessage("LINGSHOU_LIANHUA_UNDER_FIGHT")
        return
    end

    --丁级或丁级以上
    --4个技能以上
    --悟性不为0
    --修为不为0
    local data = LingShou_LingShou.GetData(details.data.TemplateId)
    if not CLingShouMgr.Inst.fromNPC and (CLuaLingShouMgr.IsSpecial(details.data) or CLuaLingShouMgr.IsShenShou(details.data)) then
        g_MessageMgr:ShowMessage("LINGSHOU_LIANHUA_FORBIDEN")
    else
        local price = tostring(CLingShouMgr.Inst.lianhuaPrice)
        if details.data.Level < 30 then
            price = "0"
        end

        local msg = g_MessageMgr:FormatMessage("LINGSHOU_LIANHUA_TIP", price)
        msg = CUICommonDef.TranslateToNGUIText(msg)
        MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
            --炼化
            if not System.String.IsNullOrEmpty(CLingShouMgr.Inst.selectedLingShou) then
                Gac2Gas.RequestLingShouLianhua(CLingShouMgr.Inst.selectedLingShou)
            end
        end), nil, nil, nil, false)
    end
end
function CLuaLingShouPropertyDetailView:XiuXi( go)
    --休息或者出战,等服务器返回消息之后在确定
    local label = CommonDefs.GetComponentInChildren_GameObject_Type(self.xiuxiBtn, typeof(UILabel))
    if label.text == LocalString.GetString("休息") then
        --灵兽休息
        Gac2Gas.RequestPackLingShou()
        --休息
        Gac2Gas.RequestLingShouDetails(CLingShouMgr.Inst.selectedLingShou, 0)
    elseif label.text == LocalString.GetString("出战") then
        --出战
        --之前那只出战的灵兽需要重新请求数据了
        if CClientMainPlayer.Inst.CurrentLingShou ~= nil then
            local detail = CLingShouMgr.Inst:GetLingShouDetails(CClientMainPlayer.Inst.CurrentLingShou.ID)
            detail.dirty = true
        end

        Gac2Gas.RequestSummonLingShou(CLingShouMgr.Inst.selectedLingShou)
    end
end
function CLuaLingShouPropertyDetailView:Rename( go)
    local uplimit = CLingShouMgr.Inst.maxNameLength
    if CommonDefs.IsSeaMultiLang() then
        -- 灵兽名STRING(40)
        uplimit = Constants.SeaMaxCharLimitForLingShouName
    end

    local content = SafeStringFormat3(LocalString.GetString("请输入新的灵兽名字，最多输入%d个字符(%d个汉字)"), uplimit * 2, uplimit)
    if CommonDefs.IsSeaMultiLang() and (LocalString.language == "en" or LocalString.language == "ina") then
        content = SafeStringFormat3(LocalString.GetString("请输入新的灵兽名字，最多输入%d个字母"), uplimit)
    end

    CInputBoxMgr.ShowInputBox(content, DelegateFactory.Action_string(function (str)
        if CommonDefs.IsSeaMultiLang() then
            if CommonDefs.StringLength(str) > uplimit then
                g_MessageMgr:ShowMessage("LINGSHOU_NAME_TOO_LONG")
                return
            end
        else
            if CUICommonDef.GetStrByteLength(str) > uplimit * 2 then
                g_MessageMgr:ShowMessage("LINGSHOU_NAME_TOO_LONG")
                return
            end
        end
        if str==nil or str=="" then
            g_MessageMgr:ShowMessage("LINGSHOU_NAME_NOT_NULL")
            return
        elseif not CWordFilterMgr.Inst:CheckName(str, LocalString.GetString("灵兽名")) then
            g_MessageMgr:ShowMessage("LingShou_Name_Violation")
            return
        end
        --采用新的名字
        if CLingShouMgr.Inst.selectedLingShou then
            Gac2Gas.RequestRenameLingShou(CLingShouMgr.Inst.selectedLingShou, str)
        end
        CUIManager.CloseUI(CIndirectUIResources.InputBox)
    end), CLingShouMgr.Inst.maxNameLength * 5, true, nil, nil)
    --名字输入时不做限制
end
function CLuaLingShouPropertyDetailView:OnEnable( )
    self.lingShouTable.gameObject:SetActive(true)
    g_ScriptEvent:AddListener("SelectLingShouAtRow", self, "SelectLingShouAtRow")
    g_ScriptEvent:AddListener("GetLingShouDetails", self, "GetLingShouDetails")

    g_ScriptEvent:AddListener("LingShouDongFangFinish", self, "OnLingShouDongFangFinish")
    g_ScriptEvent:AddListener("LingShouEnterDongFangState", self, "OnLingShouEnterDongFangState")

    local selectedLingShou = CLingShouMgr.Inst.selectedLingShou
    if selectedLingShou and selectedLingShou~="" then
        CLingShouMgr.Inst:RequestLingShouDetails(selectedLingShou)
    end


    self:UpdateAdjustBtn()

    g_ScriptEvent:AddListener("MainPlayerLevelChange", self, "OnMainPlayerLevelChange")
    g_ScriptEvent:AddListener("UpdateAssistLingShouId", self, "OnUpdateAssistLingShouId")
    g_ScriptEvent:AddListener("UpdateFuTiLingShouId", self, "OnUpdateFuTiLingShouId")
    g_ScriptEvent:AddListener("UpdateJieBanLingShouId", self, "OnUpdateJieBanLingShouId")
	g_ScriptEvent:AddListener("SendItem", self, "OnSendItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "OnSetItemAt")
end
function CLuaLingShouPropertyDetailView:OnDisable( )
    g_ScriptEvent:RemoveListener("SelectLingShouAtRow", self, "SelectLingShouAtRow")
    g_ScriptEvent:RemoveListener("GetLingShouDetails", self, "GetLingShouDetails")

    g_ScriptEvent:RemoveListener("LingShouDongFangFinish", self, "OnLingShouDongFangFinish")
    g_ScriptEvent:RemoveListener("LingShouEnterDongFangState", self, "OnLingShouEnterDongFangState")

    if self.m_Tick ~= nil then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
    g_ScriptEvent:RemoveListener("MainPlayerLevelChange", self, "OnMainPlayerLevelChange")
    g_ScriptEvent:RemoveListener("UpdateAssistLingShouId", self, "OnUpdateAssistLingShouId")
    g_ScriptEvent:RemoveListener("UpdateFuTiLingShouId", self, "OnUpdateFuTiLingShouId")
    g_ScriptEvent:RemoveListener("UpdateJieBanLingShouId", self, "OnUpdateJieBanLingShouId")
    g_ScriptEvent:RemoveListener("SendItem", self, "OnSendItem")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "OnSetItemAt")
end
function CLuaLingShouPropertyDetailView:OnSendItem(args)
    local itemId = args[0]
    local item = CItemMgr.Inst:GetById(itemId)
    local templateId = LingShou_Setting.GetData().CangHuaZhuItemId
    if item and item.TemplateId==templateId then
        self:RefreshLevelLimitButton()
    end
end

function CLuaLingShouPropertyDetailView:OnSetItemAt(args)
    local oldItemId = args[2]
    local newItemId = args[3]
    local needRefresh = false
    local templateId = LingShou_Setting.GetData().CangHuaZhuItemId

    if oldItemId and oldItemId~="" then
        local item = CItemMgr.Inst:GetById(oldItemId)
        if item and item.TemplateId==templateId then
            needRefresh=true
        end
    end
    if newItemId and newItemId~="" then
        local item = CItemMgr.Inst:GetById(newItemId)
        if item and item.TemplateId==templateId then
            needRefresh=true
        end
    end
    if needRefresh then
        self:RefreshLevelLimitButton()
    end
end
function CLuaLingShouPropertyDetailView:OnLingShouEnterDongFangState(lingShouId)
    if lingShouId == CLingShouMgr.Inst.selectedLingShou then
        Gac2Gas.RequestLingShouDetails(lingShouId, 0)
    end
end

function CLuaLingShouPropertyDetailView:OnLingShouDongFangFinish( lingShouId)
    if lingShouId == CLingShouMgr.Inst.selectedLingShou then
        Gac2Gas.RequestLingShouDetails(lingShouId, 0)
    end
end
function CLuaLingShouPropertyDetailView:SelectLingShouAtRow( row )
    -- local row = args[0]
    --CLingShouMgr.Inst.selectedSkillId = 0;
    --请求详细信息
    --如果管理器有他的详情的话，就不用再请求了
    local list = CLingShouMgr.Inst:GetLingShouOverviewList()
    if row < list.Count then
        local overviewData = list[row]
        CLingShouMgr.Inst.selectedLingShou = list[row].id
        local lingShouId = list[row].id

        --RefreshXiuxiBtnState();

        CLingShouMgr.Inst:RequestLingShouDetails(lingShouId)
    else
        self:InitNull()
    end

    -- self:RefreshLevelLimitButton()
    self:RefreshXiuxiBtnState()
end
function CLuaLingShouPropertyDetailView:RefreshLevelLimitButton()

    local selectedLingShou = CLingShouMgr.Inst.selectedLingShou
    local isDongFangOrLeave = false
    local details = CLingShouMgr.Inst:GetLingShouDetails(selectedLingShou)
    if details then
        local marryInfo = details.data.Props.MarryInfo
        local isDongFnag = marryInfo.IsInDongfang>0
        local isLeave = (marryInfo.LeaveStartTime + marryInfo.LeaveDuration) > CServerTimeMgr.Inst.timeStamp
        isDongFangOrLeave = isDongFnag or isLeave
    end


    local list = CLingShouMgr.Inst:GetLingShouOverviewList()
    for i=1,list.Count do
        local overviewData = list[i-1]
        if CLingShouMgr.Inst.selectedLingShou==overviewData.id then
            if IsLingShouLevelLimitOpen() then
                if overviewData.level>=CLuaLingShouMgr.GetTrainConditionLevel() then
                    self.m_LevelLimitButton:SetActive(true)
                    if isDongFangOrLeave then
                        CUICommonDef.SetActive(self.m_LevelLimitButton, false, true)
                    else
                        CUICommonDef.SetActive(self.m_LevelLimitButton, true, true)

                        local have = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, LingShou_Setting.GetData().CangHuaZhuItemId)
                        if not have then
                            have = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, LingShou_Setting.GetData().ExChengzhangItemTempId)
                            if not have then
                                have = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, LingShou_Setting.GetData().ExPropsItemTempId)
                            end
                        end
                        local arrow = self.m_LevelLimitButton.transform:Find("Arrow").gameObject
                        if have then
                            CUICommonDef.SetActive(arrow, true, true)
                        else
                            CUICommonDef.SetActive(arrow, false, true)
                        end
                        CLuaGuideMgr.TryTriggerLingShouLevelLimitGuide()
                    end
                else
                    self.m_LevelLimitButton:SetActive(false)
                    if CGuideMgr.Inst:IsInPhase(EnumGuideKey.LingShouLevelLimit) then
                        CGuideMgr.Inst:EndCurrentPhase()
                    end
                end
            end
        end
    end
end

function CLuaLingShouPropertyDetailView:GetLingShouDetails( args )
    if self.m_WndTabView.CurrentSelectTab ~= 0 then return end
    local lingShouId, details=args[0],args[1]
    if lingShouId == CLingShouMgr.Inst.selectedLingShou then
        --更新界面
        if details ~= nil then
            self.contentGo:SetActive(true)
            self.detailsDisplay:Fill(details)
            self.textureLoader:Init(details)

            self:InitSkills(details.data.Props)

            local  marryInfo = details.data.Props.MarryInfo
            local isDongFnag = marryInfo.IsInDongfang>0
            local isLeave = (marryInfo.LeaveStartTime + marryInfo.LeaveDuration) > CServerTimeMgr.Inst.timeStamp

            if isDongFnag or isLeave then
                local marryInfo = details.data.Props.MarryInfo
                self.m_LeaveStartTime = marryInfo.LeaveStartTime
                self.m_LeaveDuration = marryInfo.LeaveDuration
                self:SetDongFangOrLeaveState(true, isDongFnag, isLeave)
            else
                self:SetDongFangOrLeaveState(false, false, false)
            end

            self:RefreshLevelLimitButton()
        else
            self:InitNull()
        end
    end

    self:UpdateAdjustBtn()
end
function CLuaLingShouPropertyDetailView:InitNull( )
    self.contentGo:SetActive(false)
    self.detailsDisplay:Fill(nil)
    self.textureLoader:InitNull()
    self:InitSkills(nil)

    self.m_LevelLimitButton:SetActive(false)

    self:SetDongFangOrLeaveState(false, false, false)
end
function CLuaLingShouPropertyDetailView:SetDongFangOrLeaveState( active, isDongFang, isLeave)
    CUICommonDef.SetActive(self.renameBtn, not active, true)
    CUICommonDef.SetActive(self.addExpBtn, not active, true)
    CUICommonDef.SetActive(self.addShouMingBtn, not active, true)

    CUICommonDef.GetChild(self.transform, "Normal").gameObject:SetActive(not active)
    local tf2 = CUICommonDef.GetChild(self.transform, "DongFangOrLeave")
    tf2.gameObject:SetActive(active)

    CUICommonDef.SetActive(self.detailsDisplay.babyBtn, not active, true)
    CUICommonDef.SetActive(self.detailsDisplay.genderSprite.gameObject, not active, true)

    if active then
        if isDongFang then
            tf2:Find("WarningLabel"):GetComponent(typeof(UILabel)).text = LocalString.GetString("当前灵兽生宝宝中，无法进行灵兽功能相应的操作")
            tf2:Find("CountdownLabel").gameObject:SetActive(false)
        elseif isLeave then
            tf2:Find("WarningLabel"):GetComponent(typeof(UILabel)).text = LocalString.GetString("当前灵兽离家出走中，无法进行灵兽功能相应的操作")
            local label = tf2:Find("CountdownLabel"):GetComponent(typeof(UILabel))
            label.gameObject:SetActive(true)
            self:UpdateCountdown(label)
            if self.m_Tick ~= nil then
                UnRegisterTick(self.m_Tick)
                self.m_Tick = nil
            end

            self.m_Tick = RegisterTick(function()
                self:UpdateCountdown(label)
            end,1000)
        end
    end

    self.transform.parent:Find("DongFangOrLeave").gameObject:SetActive(false)
end
function CLuaLingShouPropertyDetailView:UpdateCountdown( label)
    local count = math.floor((self.m_LeaveStartTime + self.m_LeaveDuration - CServerTimeMgr.Inst.timeStamp))
    if count > 0 then
        local day = math.floor(math.floor(count / 3600) / 24)
        local hour = math.floor(count % (86400 ) / 3600)
        local minute = math.floor(count % (86400) % 3600 / 60)

        if day > 0 then
            label.text =  SafeStringFormat3(LocalString.GetString("回来倒计时%d天%02d:%02d"), day, hour, minute)
        else
            label.text = SafeStringFormat3(LocalString.GetString("回来倒计时%02d:%02d"), hour, minute)
        end
    else
        if self.m_Tick ~= nil then
            label.text = ""
            UnRegisterTick(self.m_Tick)
            self.m_Tick = nil
            --到时间了再请求一遍
            -- self:TryRefresh()
            Gac2Gas.RequestLingShouDetails(CLingShouMgr.Inst.selectedLingShou, 0)
        end
    end
end

function CLuaLingShouPropertyDetailView:InitSkills(prop)
    if not prop then return end

    local transform = self.transform:Find("Content/Slots")

    local first = transform:Find("Grid/1").gameObject
    local grid = transform:Find("Grid"):GetComponent(typeof(UIGrid))
    local babySkillSlot1 = transform:Find("BabySkillSlot1")
    local babySkillSlot2 = transform:Find("BabySkillSlot2")

    local bindPartenerLingShouId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.BindedPartenerLingShouId or nil
    local isJieBan = bindPartenerLingShouId == CLingShouMgr.Inst.selectedLingShou 

    if not isJieBan and prop and prop.Baby ~= nil and prop.Baby.BornTime > 0 then
        if babySkillSlot1 ~= nil and babySkillSlot2 ~= nil then
            babySkillSlot1.gameObject:SetActive(true)
            local skillId1 = CLuaLingShouMgr.ProcessSkillId(prop.Baby.Quality,prop.Baby.SkillList[1])
            self:InitSkillSlot(babySkillSlot1,skillId1)

            babySkillSlot2.gameObject:SetActive(true)
            local skillId2 = CLuaLingShouMgr.ProcessSkillId(prop.Baby.Quality,prop.Baby.SkillList[2])
            self:InitSkillSlot(babySkillSlot2,skillId2)

            grid.transform.localPosition = Vector3(20, 0,0)
        end
    else
        if babySkillSlot1 ~= nil and babySkillSlot2 ~= nil then
            babySkillSlot1.gameObject:SetActive(false)
            babySkillSlot2.gameObject:SetActive(false)
            grid.transform.localPosition = Vector3(70, 0,0)
        end
    end

    if grid.transform.childCount==1 then
        for i = 0, 6 do
            NGUITools.AddChild(grid.gameObject,first)
        end
        grid:Reposition()
    end

    -- local bindPartenerLingShouId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.BindedPartenerLingShouId or nil
    if isJieBan then
        local partnerInfo = prop.PartnerInfo
        local normalSkills = partnerInfo.PartnerNormalSkills
        local professionalSkills = partnerInfo.PartnerProfessionalSkills
        for i=1,5 do
            local tf = grid.transform:GetChild(i-1)
            self:InitSkillSlot(tf,normalSkills[i])
        end
        for i=1,3 do
            local tf = grid.transform:GetChild(i+5-1)
            self:InitSkillSlot(tf,professionalSkills[i])
        end

    else
        local skillList = prop and prop.SkillListForSave or nil
        for i=1,8 do
            local tf = grid.transform:GetChild(i-1)
            self:InitSkillSlot(tf,skillList and skillList[i] or 0)
        end
    end
end

function CLuaLingShouPropertyDetailView:InitSkillSlot(transform,  skillId)
    local icon = transform:Find("HasSkill/Texture"):GetComponent(typeof(CUITexture))
    local levelLabel = transform:Find("HasSkill/LevelLabel"):GetComponent(typeof(UILabel))
    -- local toggle = transform:GetComponent(typeof(UIToggle))
    local hasSkillTf = transform:Find("HasSkill")

    local template = Skill_AllSkills.GetData(skillId)
    if template ~= nil then
        hasSkillTf.gameObject:SetActive(true)
        icon:LoadSkillIcon(template.SkillIcon)
        levelLabel.text = "Lv." .. template.Level

        local col = transform:GetComponent(typeof(Collider))
        if col ~= nil then
            col.enabled = true
        end

        local noskill = transform:Find("NoSkill")
        if noskill ~= nil then
            noskill.gameObject:SetActive(false)
        end
    else
        local col = transform:GetComponent(typeof(Collider))
        if col ~= nil then
            col.enabled = false
        end
        hasSkillTf.gameObject:SetActive(false)
        icon.material = nil
        levelLabel.text = ""

        local noskill = transform:Find("NoSkill")
        if noskill ~= nil then
            noskill.gameObject:SetActive(true)
        end
    end

    if skillId>0 then
        UIEventListener.Get(transform.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
            CSkillInfoMgr.ShowSkillInfoWnd(skillId,true,0,0,nil)
        end)
    else
        UIEventListener.Get(transform.gameObject).onClick=nil
    end
end

function CLuaLingShouPropertyDetailView:InitButtons()
    local transform = self.transform:Find("Content/Normal/Buttons")
    local stateLabel = transform:Find("Label"):GetComponent(typeof(UILabel))
    local xiuxiBtn = transform:Find("XiuxiBtn").gameObject
    local xiuxiLabel = transform:Find("XiuxiBtn/Label"):GetComponent(typeof(UILabel))
    local adjustBtn = transform:Find("AdjustBtn").gameObject
    local adjustLabel = transform:Find("AdjustBtn/AdjustLabel"):GetComponent(typeof(UILabel))

    if CLingShouMgr.Inst.fromNPC then
        xiuxiBtn:SetActive(false)
    end

    local function setRest()
        Gac2Gas.RequestPackLingShou()
        --休息
        local selectedLingShou = CLingShouMgr.Inst.selectedLingShou
        if selectedLingShou and selectedLingShou~="" then
            local detail = CLingShouMgr.Inst:GetLingShouDetails(selectedLingShou)
            if detail ~= nil then
                detail.dirty = true
            end
            Gac2Gas.RequestLingShouDetails(selectedLingShou, 0)
        end
    end

    UIEventListener.Get(xiuxiBtn).onClick = DelegateFactory.VoidDelegate(function(p)
        --休息或者出战,等服务器返回消息之后在确定
        -- local label = CommonDefs.GetComponentInChildren_GameObject_Type(self.xiuxiBtn, typeof(UILabel))
        local text = xiuxiLabel.text
        if text == LocalString.GetString("休息") then
            setRest()
        elseif text == LocalString.GetString("化灵") then
            if CClientMainPlayer.Inst.CurrentLingShou ~= nil then
                local detail = CLingShouMgr.Inst:GetLingShouDetails(CClientMainPlayer.Inst.CurrentLingShou.ID)
                detail.dirty = true
            end
            Gac2Gas.RequestSwitchFuTiLingShou(CLingShouMgr.Inst.selectedLingShou)
        elseif text == LocalString.GetString("出战") then
            --出战
            --之前那只出战的灵兽需要重新请求数据了
            if CClientMainPlayer.Inst.CurrentLingShou ~= nil then
                local detail = CLingShouMgr.Inst:GetLingShouDetails(CClientMainPlayer.Inst.CurrentLingShou.ID)
                detail.dirty = true
            end
            Gac2Gas.RequestSummonLingShou(CLingShouMgr.Inst.selectedLingShou)
        end
    end)
    UIEventListener.Get(adjustBtn).onClick = DelegateFactory.VoidDelegate(function (p)
        if adjustLabel.text == LocalString.GetString("休息") then
            --灵兽休息
            --如果是战斗状态
            --如果是助战装填
            if CLingShouMgr.Inst:IsOnAssist(CLingShouMgr.Inst.selectedLingShou) then
                Gac2Gas.RequestSetAssistLingShou("")
            elseif CLingShouMgr.Inst:IsOnJieBan(CLingShouMgr.Inst.selectedLingShou) then
                Gac2Gas.RequestCancelBindPartner_LingShou(CLingShouMgr.Inst.selectedLingShou)
            else
                setRest()
            end
        else
            CUIManager.ShowUI(CUIResources.LingShouBattleConfigWnd)
        end
    end)
    self:UpdateAdjustBtn()
end

function CLuaLingShouPropertyDetailView:RefreshXiuxiBtnState( )
    local transform = self.transform:Find("Content/Normal/Buttons")
    local stateLabel = transform:Find("Label"):GetComponent(typeof(UILabel))
    local xiuxiLabel = transform:Find("XiuxiBtn/Label"):GetComponent(typeof(UILabel))
    local adjustLabel = transform:Find("AdjustBtn/AdjustLabel"):GetComponent(typeof(UILabel))

    local isLingShouHuaLingMode = false
    if CScene.MainScene ~= nil then
        isLingShouHuaLingMode = CSwitchMgr.EnableLingShouHuaLing and CScene.MainScene.LingShouMode == 1
    end
    local lingshouId = CLingShouMgr.Inst.selectedLingShou
    if CLingShouMgr.Inst:IsOnBattle(lingshouId) then
        xiuxiLabel.text = LocalString.GetString("休息")
        stateLabel.text = ""

        adjustLabel.text = LocalString.GetString("休息")
    elseif CLingShouMgr.Inst:IsOnAssist(lingshouId) then
        xiuxiLabel.text = LocalString.GetString("休息")
        stateLabel.text = ""

        adjustLabel.text = LocalString.GetString("休息")
    elseif CLingShouMgr.Inst:IsOnJieBan(lingshouId) then
        xiuxiLabel.text = LocalString.GetString("休息")
        stateLabel.text = ""

        adjustLabel.text = LocalString.GetString("休息")
    else
        xiuxiLabel.text = LocalString.GetString("出战")
        stateLabel.text = LocalString.GetString("休息中...")
    end

    if isLingShouHuaLingMode then
        if CLingShouMgr.Inst:IsFuTi(lingshouId) then
            xiuxiLabel.text = LocalString.GetString("休息")
            stateLabel.text = ""

            adjustLabel.text = LocalString.GetString("休息")
        elseif CLingShouMgr.Inst:IsOnBattle(lingshouId) or CLingShouMgr.Inst:IsOnAssist(lingshouId) then
        else
            xiuxiLabel.text = LocalString.GetString("化灵")
            stateLabel.text = LocalString.GetString("休息中...")
        end
    end

    local ownBabyId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.OwnBabyId or nil
    local haveOwnBaby = ownBabyId~=nil and ownBabyId~=""
    if CClientMainPlayer.Inst ~= nil then
        if CClientMainPlayer.Inst.MaxLevel < 70 and not haveOwnBaby then
            adjustLabel.text = LocalString.GetString("角色70级开放")
        end

        if CClientMainPlayer.Inst.IsLingShouFuTi then
        end
    else
        adjustLabel.text = LocalString.GetString("角色70级开放")
    end
end

function CLuaLingShouPropertyDetailView:UpdateAdjustBtn( )
    local transform = self.transform:Find("Content/Normal/Buttons")
    local stateLabel = transform:Find("Label"):GetComponent(typeof(UILabel))
    local xiuxiLabel = transform:Find("XiuxiBtn/Label"):GetComponent(typeof(UILabel))
    local adjustLabel = transform:Find("AdjustBtn/AdjustLabel"):GetComponent(typeof(UILabel))

    local xiuxiBtn = transform:Find("XiuxiBtn").gameObject
    local adjustBtn = transform:Find("AdjustBtn").gameObject

    local ownBabyId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.OwnBabyId or nil
    local haveOwnBaby = ownBabyId~=nil and ownBabyId~=""

    if CClientMainPlayer.Inst ~= nil then
        if CClientMainPlayer.Inst.MaxLevel >= 70 or haveOwnBaby then
            CUICommonDef.SetActive(adjustBtn, true, true)
            adjustLabel.text = LocalString.GetString("上阵调整")
            xiuxiBtn:SetActive(false)
            stateLabel.gameObject:SetActive(true)
            --如果已经出战了
        else
            CUICommonDef.SetActive(adjustBtn, false, true)
            adjustLabel.text = LocalString.GetString("角色70级开放")
            xiuxiBtn:SetActive(true)
            stateLabel.gameObject:SetActive(false)
        end
    else
        CUICommonDef.SetActive(adjustBtn, false, true)
        adjustLabel.text = LocalString.GetString("角色70级开放")
        xiuxiBtn:SetActive(true)
        stateLabel.gameObject:SetActive(false)
    end
    self:RefreshXiuxiBtnState()
end

function CLuaLingShouPropertyDetailView:OnMainPlayerLevelChange()
    self:UpdateAdjustBtn()
end
function CLuaLingShouPropertyDetailView:OnUpdateAssistLingShouId()
    self:UpdateAdjustBtn()
end
function CLuaLingShouPropertyDetailView:OnUpdateFuTiLingShouId()
    self:UpdateAdjustBtn()
end
function CLuaLingShouPropertyDetailView:OnUpdateJieBanLingShouId()
    self:UpdateAdjustBtn()
    local details = CLingShouMgr.Inst:GetLingShouDetails(CLingShouMgr.Inst.selectedLingShou)
    if details then
        self:InitSkills(details.data.Props)
        self.detailsDisplay:Fill(details)
    end
end
