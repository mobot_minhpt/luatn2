-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CEquipmentHoleTransferOptionsItem = import "L10.UI.CEquipmentHoleTransferOptionsItem"
local CEquipmentHoleTransferOptionsWnd = import "L10.UI.CEquipmentHoleTransferOptionsWnd"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CProfessionTransferMgr = import "L10.Game.CProfessionTransferMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local Extensions = import "Extensions"
local LocalString = import "LocalString"
local NGUIMath = import "NGUIMath"
local StringActionKeyValuePair = import "L10.Game.StringActionKeyValuePair"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CEquipmentHoleTransferOptionsWnd.m_Init_CS2LuaHook = function (this) 

    this.selectedItemId = nil
    this.sourceEquipId = CProfessionTransferMgr.Inst.SourceEquipId
    this.sourceEquipPos = CProfessionTransferMgr.Inst.SourceEquipPos
    Extensions.RemoveAllChildren(this.grid.transform)
    CommonDefs.ListClear(this.items)

    if System.String.IsNullOrEmpty(this.sourceEquipId) then
        CProfessionTransferMgr.Inst:QueryCanTransferHolesSourceEquip()
        this.titleLabel.text = LocalString.GetString("选择原装备")
    else
        CProfessionTransferMgr.Inst:QueryCanTransferHolesTargetEquip(this.sourceEquipPos)
        this.titleLabel.text = LocalString.GetString("选择目标装备")
    end
end
CEquipmentHoleTransferOptionsWnd.m_LoadData_CS2LuaHook = function (this, itemIds) 

    Extensions.RemoveAllChildren(this.grid.transform)
    CommonDefs.ListClear(this.items)
    do
        local i = 0
        while i < itemIds.Count do
            local obj = CUICommonDef.AddChild(this.grid.gameObject, this.itemTpl)
            obj:SetActive(true)
            local item = CommonDefs.GetComponent_GameObject_Type(obj, typeof(CEquipmentHoleTransferOptionsItem))
            item:Init(itemIds[i])
            UIEventListener.Get(obj).onClick = MakeDelegateFromCSFunction(this.OnItemClick, VoidDelegate, this)
            CommonDefs.ListAdd(this.items, typeof(CEquipmentHoleTransferOptionsItem), item)
            i = i + 1
        end
    end
    this.grid:Reposition()
    this.scrollView:ResetPosition()

    if this.items.Count > 0 then
        this:OnItemClick(this.items[0].gameObject)
    end
end
CEquipmentHoleTransferOptionsWnd.m_OnItemClick_CS2LuaHook = function (this, go) 
    do
        local i = 0
        while i < this.items.Count do
            local selected = (this.items[i].gameObject == go)
            this.items[i].Selected = selected
            if selected then
                this.selectedItemId = this.items[i].ItemId
                local b1 = NGUIMath.CalculateRelativeWidgetBounds(this.background.transform)
                local height = b1.size.y
                local worldCenterY = this.background.transform:TransformPoint(b1.center).y
                local width = b1.size.x
                local worldCenterX = this.background.transform:TransformPoint(b1.center).x

                CItemInfoMgr.ShowLinkItemInfo(this.items[i].ItemId, false, this, CItemInfoMgr.AlignType.Right, worldCenterX, worldCenterY, width, height)
            end
            i = i + 1
        end
    end
end
CEquipmentHoleTransferOptionsWnd.m_GetActionPairs_CS2LuaHook = function (this, itemId, templateId) 
    if CClientMainPlayer.Inst == nil or System.String.IsNullOrEmpty(itemId) then
        return nil
    end
    local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, itemId)
    local item = CItemMgr.Inst:GetById(itemId)
    if pos == 0 or item == nil then
        return nil
    end
    local chooseAction = CreateFromClass(StringActionKeyValuePair, LocalString.GetString("选择"), MakeDelegateFromCSFunction(this.OnChoose, Action0, this))
    local actionPairs = CreateFromClass(MakeGenericClass(List, StringActionKeyValuePair))
    CommonDefs.ListAdd(actionPairs, typeof(StringActionKeyValuePair), chooseAction)
    return actionPairs
end
CEquipmentHoleTransferOptionsWnd.m_OnChoose_CS2LuaHook = function (this) 

    local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, this.selectedItemId)

    if pos > 0 then
        if System.String.IsNullOrEmpty(this.sourceEquipId) then
            CProfessionTransferMgr.Inst:CanSelectTransferHoleSourceEquip(pos)
        else
            CProfessionTransferMgr.Inst:CanSelectTransferHoleTargetEquip(this.sourceEquipPos, pos)
        end
        CItemInfoMgr.CloseItemInfoWnd()
        this:Close()
    end
end
