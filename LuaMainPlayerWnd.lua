local GameObject = import "UnityEngine.GameObject"
local CAchievementMgr = import "L10.Game.CAchievementMgr"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"

LuaMainPlayerWnd = class()
RegistClassMember(LuaMainPlayerWnd, "m_ForceShowAchievementAlert") -- 是否强制显示成就红点，兼容http://l10.leihuo.netease.com:8210/issues/162579
--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaMainPlayerWnd, "Tab3AlertSprite", "Tab3AlertSprite", GameObject)
RegistChildComponent(LuaMainPlayerWnd, "Tab2AlertSprite", "Tab2AlertSprite", GameObject)

--@endregion RegistChildComponent end

function LuaMainPlayerWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_ForceShowAchievementAlert = false
    self.Tab2AlertSprite:SetActive(false)
    self:UpdateAlert()
end

function LuaMainPlayerWnd:OnEnable()
    g_ScriptEvent:AddListener("OnPlotDiscussionEnable", self, "OnPlotDiscussionEnable")
    g_ScriptEvent:AddListener("OnPlotCommentShowRedAlert", self, "OnPlotCommentShowRedAlert")
    g_ScriptEvent:AddListener("GetAchievementAwardSuccess", self, "OnAchievementUpdate")
    g_ScriptEvent:AddListener("OnBiWuShopAlertUpdate", self, "OnBiWuShopAlertUpdate")
    LuaPlotDiscussionMgr:CheckIsEnable()
end

function LuaMainPlayerWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnPlotDiscussionEnable", self, "OnPlotDiscussionEnable")
    g_ScriptEvent:RemoveListener("OnPlotCommentShowRedAlert", self, "OnPlotCommentShowRedAlert")
    g_ScriptEvent:RemoveListener("GetAchievementAwardSuccess", self, "OnAchievementUpdate")
    g_ScriptEvent:RemoveListener("OnBiWuShopAlertUpdate", self, "OnBiWuShopAlertUpdate")
end

function LuaMainPlayerWnd:OnBiWuShopAlertUpdate(tf)
    self:UpdateAlert2(tf)
end

function LuaMainPlayerWnd:OnPlotDiscussionEnable()
    g_ScriptEvent:RemoveListener("OnPlotDiscussionEnable", self, "OnPlotDiscussionEnable")
    local isEnable = LuaPlotDiscussionMgr:IsOpen()
    if isEnable then
        LuaPlotDiscussionMgr:RequestRedDotInfo()
    end
end

function LuaMainPlayerWnd:OnPlotCommentShowRedAlert(haoYiXingCardId, yaoGuiHuCardId, isClear)
    if isClear then
        self.m_ForceShowAchievementAlert = false
        self:UpdateAlert()
    end
    if yaoGuiHuCardId ~= 0 then
        self.m_ForceShowAchievementAlert = true
        self:UpdateAlert()
    end
end

function LuaMainPlayerWnd:OnAchievementUpdate(args)
    self:UpdateAlert()
end

function LuaMainPlayerWnd:UpdateAlert()
    self.Tab3AlertSprite:SetActive(self.m_ForceShowAchievementAlert or CAchievementMgr.Inst:CheckAchievementAvailable())

    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Level > 69 then
        local record = PlayerPrefs.GetInt("mainplayerwnd_biwushop_alert")
        if record and record == 1 then
            self.Tab2AlertSprite:SetActive(false)
        else
            self.Tab2AlertSprite:SetActive(true)
        end
    end
end

function LuaMainPlayerWnd:UpdateAlert2(tf)
    self.Tab2AlertSprite:SetActive(tf)
end
--@region UIEvent

--@endregion UIEvent
