-- Auto Generated!!
local CChatLinkMgr = import "CChatLinkMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CIndirectUIResources = import "L10.UI.CIndirectUIResources"
local CMergeServerVoteMgr = import "L10.UI.CMergeServerVoteMgr"
local CMergeServerVoteWnd = import "L10.UI.CMergeServerVoteWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CUIManager = import "L10.UI.CUIManager"
local LocalString = import "LocalString"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
local StringBuilder = import "System.Text.StringBuilder"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CMergeServerVoteWnd.m_Init_CS2LuaHook = function (this) 
    local sb = NewStringBuilderWraper(StringBuilder)
    do
        local i = 0
        while i < CMergeServerVoteMgr.Inst.m_OtherServerNameList.Count do
            if i ~= 0 then
                sb:Append(LocalString.GetString("、"))
            end
            sb:Append(ToStringWrap(CMergeServerVoteMgr.Inst.m_OtherServerNameList[i]))
            i = i + 1
        end
    end

    this.m_EndTimeLabel.text = CMergeServerVoteMgr.Inst.m_VoteEndTimeStamp

    local hasBattle = System.String.IsNullOrEmpty(CMergeServerVoteMgr.Inst.m_MergeToServerName)
    this.m_NoBattleRoot:SetActive(not hasBattle)
    this.m_BattleRoot:SetActive(hasBattle)
    if hasBattle then
        this.m_Question2Label.text = g_MessageMgr:FormatMessage("MERGE_VOTE_DIALOG1", CMergeServerVoteMgr.Inst.m_MyServerName, ToStringWrap(sb))
        this.m_BattleTipLabel.text = CChatLinkMgr.TranslateToNGUIText(g_MessageMgr:FormatMessage("MergeBattle_Rule1"), false)
        UIEventListener.Get(this.m_BattleTipLabel.gameObject).onClick = MakeDelegateFromCSFunction(this.OnTipLabelClick, VoidDelegate, this)
    else
        this.m_QuestionLabel.text = g_MessageMgr:FormatMessage("MERGE_VOTE_DIALOG", CMergeServerVoteMgr.Inst.m_MyServerName, ToStringWrap(sb), CMergeServerVoteMgr.Inst.m_MergeToServerName)
    end
end
CMergeServerVoteWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.m_AgreeBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_AgreeBtn).onClick, MakeDelegateFromCSFunction(this.OnAgreeBtnClick, VoidDelegate, this), true)
    UIEventListener.Get(this.m_DisagreeBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_DisagreeBtn).onClick, MakeDelegateFromCSFunction(this.OnDisagreeBtnClick, VoidDelegate, this), true)
    UIEventListener.Get(this.m_CloseBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_CloseBtn).onClick, MakeDelegateFromCSFunction(this.OnCloseBtnClick, VoidDelegate, this), true)
    UIEventListener.Get(this.m_TipBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_TipBtn).onClick, MakeDelegateFromCSFunction(this.OnTipBtnClick, VoidDelegate, this), true)
end
CMergeServerVoteWnd.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.m_AgreeBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_AgreeBtn).onClick, MakeDelegateFromCSFunction(this.OnAgreeBtnClick, VoidDelegate, this), false)
    UIEventListener.Get(this.m_DisagreeBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_DisagreeBtn).onClick, MakeDelegateFromCSFunction(this.OnDisagreeBtnClick, VoidDelegate, this), false)
    UIEventListener.Get(this.m_CloseBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_CloseBtn).onClick, MakeDelegateFromCSFunction(this.OnCloseBtnClick, VoidDelegate, this), false)
    UIEventListener.Get(this.m_TipBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_TipBtn).onClick, MakeDelegateFromCSFunction(this.OnTipBtnClick, VoidDelegate, this), false)
end

CMergeServerVoteMgr.m_TryShowWnd_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst == nil then
        this.m_NeedToShowWnd = true
    else
        CUIManager.ShowUI(CIndirectUIResources.MergeServerVoteWnd)
    end
end
CMergeServerVoteMgr.m_OnMainPlayerCreated_CS2LuaHook = function (this) 
    if this.m_NeedToShowWnd then
        this.m_NeedToShowWnd = false
        CUIManager.ShowUI(CIndirectUIResources.MergeServerVoteWnd)
    end
end
Gas2Gac.InviteVote = function (myServerName, otherServerNamesData, mergeToServerName, voteEndTimeStamp) 
    CMergeServerVoteMgr.Inst.m_MyServerName = myServerName
    CMergeServerVoteMgr.Inst.m_OtherServerNameList = TypeAs(MsgPackImpl.unpack(otherServerNamesData), typeof(MakeGenericClass(List, Object)))
    CMergeServerVoteMgr.Inst.m_MergeToServerName = mergeToServerName
    CMergeServerVoteMgr.Inst.m_VoteEndTimeStamp = ToStringWrap(CServerTimeMgr.ConvertTimeStampToZone8Time(voteEndTimeStamp), "yyyy-MM-dd,HH:mm")
    CMergeServerVoteMgr.Inst:TryShowWnd()
end
