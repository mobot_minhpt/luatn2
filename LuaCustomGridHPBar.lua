local NGUIText = import "NGUIText"

--@class LuaCustomGridHPBar 用于处理格状血条
LuaCustomGridHPBar = class()

RegistChildComponent(LuaCustomGridHPBar, "Background", UISprite)
RegistChildComponent(LuaCustomGridHPBar, "HPGrid", UIGrid)
RegistChildComponent(LuaCustomGridHPBar, "HPItem", GameObject)

RegistClassMember(LuaCustomGridHPBar, "CurrentValue")
RegistClassMember(LuaCustomGridHPBar, "MaxValue")
RegistClassMember(LuaCustomGridHPBar, "HasAddHp")
RegistClassMember(LuaCustomGridHPBar, "HPItemList")

RegistClassMember(LuaCustomGridHPBar, "MemberId")

function LuaCustomGridHPBar:Awake()
    self.CurrentValue = 0
    self.MaxValue = 3
    self.HasAddHp = false
    self.HPItemList = {}
    self.HPItem:SetActive(false)
    CUICommonDef.ClearTransform(self.HPGrid.transform)
    self:UpdateValue(3, 3)
end

--@desc 更新HP
function LuaCustomGridHPBar:UpdateValue(curValue, maxValue, hasAddHp)
    if self.MaxValue and self.MaxValue <= 0 then return end

    self.CurrentValue = curValue
    self.MaxValue = maxValue
    self.HasAddHp = hasAddHp
    
    local gridWidth = self.Background.width / self.MaxValue
    local padding = 2
    
    self.HPGrid.cellWidth = gridWidth

    CUICommonDef.ClearTransform(self.HPGrid.transform)
    self.HPItemList = {}

    for i = 1, curValue do
        local go = NGUITools.AddChild(self.HPGrid.gameObject, self.HPItem)
        self:InitHPItem(go, gridWidth - padding, 1, i)
        go:SetActive(true)
        table.insert(self.HPItemList, go)
    end
    self.HPGrid:Reposition()
end

--@desc 初始化血量格子
function LuaCustomGridHPBar:InitHPItem(item, width, alpha, index)
    local sprite = item.transform:GetComponent(typeof(UISprite))
    sprite.width = width
    sprite.alpha = alpha
    sprite.color = self:GetColor(index)
end

--@desc 获得血量格子的颜色
function LuaCustomGridHPBar:GetColor(index)
    if not index then return NGUIText.ParseColor("80E200", 0) end
    
    if index == self.CurrentValue and self.HasAddHp then
        return NGUIText.ParseColor("FAE86E", 0)
    else
        return NGUIText.ParseColor("80E200", 0)
    end
end

function LuaCustomGridHPBar:UpdateGameplayHP(args)
    if args.Length < 3 then return end
    local current = args[0]
    local full = args[1]
    local hasAddHp = args[2]
    self:UpdateValue(current, full, hasAddHp)
end

function LuaCustomGridHPBar:OnEnable()
    g_ScriptEvent:AddListener("UpdateGameplayHP", self, "UpdateGameplayHP")
end

function LuaCustomGridHPBar:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateGameplayHP", self, "UpdateGameplayHP")
end

return LuaCustomGridHPBar