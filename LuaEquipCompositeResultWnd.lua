local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CEquipmentBaptizeMgr = import "L10.Game.CEquipmentBaptizeMgr"
local PlayerShowDifProType = import "L10.Game.PlayerShowDifProType"
local CPropertyDifMgr = import "L10.UI.CPropertyDifMgr"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local Animator = import "UnityEngine.Animator"
local Animation = import "UnityEngine.Animation"
local SoundManager = import "SoundManager"
local CPreciousItemShareMgr = import "L10.Game.CPreciousItemShareMgr"

LuaEquipCompositeResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaEquipCompositeResultWnd, "Root1", "Root1", GameObject)
RegistChildComponent(LuaEquipCompositeResultWnd, "DescItem", "DescItem", GameObject)
RegistChildComponent(LuaEquipCompositeResultWnd, "Root2", "Root2", GameObject)
RegistChildComponent(LuaEquipCompositeResultWnd, "Root3", "Root3", GameObject)
RegistChildComponent(LuaEquipCompositeResultWnd, "SelectNewBtn", "SelectNewBtn", GameObject)
RegistChildComponent(LuaEquipCompositeResultWnd, "SelectOldBtn", "SelectOldBtn", GameObject)
RegistChildComponent(LuaEquipCompositeResultWnd, "ShareBtn", "ShareBtn", GameObject)
RegistChildComponent(LuaEquipCompositeResultWnd, "CompareBtn", "CompareBtn", GameObject)
RegistChildComponent(LuaEquipCompositeResultWnd, "CloseButton", "CloseButton", GameObject)

--@endregion RegistChildComponent end

function LuaEquipCompositeResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    self.ShareBtn:SetActive(CPreciousItemShareMgr.Inst:EnableForumShareForEquipBaptize())


    UIEventListener.Get(self.SelectNewBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSelectNewBtnClick()
	end)


	
	UIEventListener.Get(self.SelectOldBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSelectOldBtnClick()
	end)


	
	UIEventListener.Get(self.ShareBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareBtnClick()
	end)


	
	UIEventListener.Get(self.CompareBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCompareBtnClick()
	end)


	
	UIEventListener.Get(self.CloseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseButtonClick()
	end)


    --@endregion EventBind end
end

function LuaEquipCompositeResultWnd:Init()

    if CUIManager.IsLoaded(CUIResources.NewAchievementGetWnd) then
        CUIManager.instance:Push(CUIResources.NewAchievementGetWnd)
    end

    self.m_OpenFx = self.transform:Find("GuiZhuang_Vx"):GetComponent(typeof(Animator))
    self.m_ChooseFx = self.transform:Find("common_fx_xuanze"):GetComponent(typeof(Animator))
    self.m_ShowFx = self.transform:Find("xinxi"):GetComponent(typeof(Animation))

    self:InitWord()

    if CLuaEquipMgr.m_PassCompositeFx then
        self.m_OpenFx.gameObject:SetActive(false)
        self.m_OpenFx.enabled = false
        self.m_ShowFx.enabled = false
    else
        self:InitFx()
    end
end

function LuaEquipCompositeResultWnd:InitFx()
    local itemId = CLuaEquipMgr.m_ResultEquipItemId
    local item = CItemMgr.Inst:GetById(itemId)
	local color = EnumToInt(item.Equip.Color)

    self.m_OpenFx.gameObject:SetActive(true)
    self.m_OpenFx.enabled = true
    if color == 5 then
        self.m_OpenFx:Play("equipcompositeresultwnd_hongzhuang",0,0)
    elseif color == 6 then
        self.m_OpenFx:Play("equipcompositeresultwnd_zizhuang",0,0)
    elseif color == 7 then
        self.m_OpenFx:Play("equipcompositeresultwnd_guizhuang02",0,0)
    end

    local leftBg = self.Root1.transform:Find("Bg").gameObject
    local rightBg = self.Root3.transform:Find("Bg").gameObject

    UIEventListener.Get(leftBg).onHover = DelegateFactory.BoolDelegate(function (isOver)
	    self.m_ChooseFx.transform.localPosition = Vector3(0,0,0)
        self.m_ChooseFx.gameObject:SetActive(not isOver)
	end)

    UIEventListener.Get(rightBg).onHover = DelegateFactory.BoolDelegate(function (isOver)
	    self.m_ChooseFx.transform.localPosition = Vector3(1005,0,0)
        self.m_ChooseFx.gameObject:SetActive(not isOver)
	end)

    -- 播一个音效
    SoundManager.Inst:PlayOneShot("event:/L10/L10_UI/UI_RongLian", Vector3.zero, nil, 0)
end

function LuaEquipCompositeResultWnd:InitWord()
    self.DescItem:SetActive(false)
    local testLabel = self.DescItem.transform:Find("DescLabel"):GetComponent(typeof(UILabel))

    local itemId = CLuaEquipMgr.m_ResultEquipItemId

    local item = CItemMgr.Inst:GetById(itemId)
	local equip = item.Equip
    local itemData = EquipmentTemplate_Equip.GetData(item.TemplateId)

    -- 需要展示的main和mat数据
    local mainWordList = {}
    local matWordList = {}
    local resultWordList = {}

    -- 原装备词条数量
    local mainWordCount = equip.WordsCount

    local heightList = {}
    -- 遍历
    local lockData = {}
    CommonDefs.DictIterate(equip.TempCompositeLockWordPositions, DelegateFactory.Action_object_object(function(key,val)
            local data = {}
            local index = tonumber(key)
            if index < 0 then
                data.isExtra = true
            else
                data.isExtra = false
            end
            data.index = math.abs(index)
            
            local mark = tonumber(val)
            data.isMain = (mark==0)

            table.insert(lockData, data)
	end))

    local resultIndex = 1
    -- 合成装备固定词条
    local resultFixedList = equip:GetTempFixedWordArray()
    for i =0, resultFixedList.Length -1 do
        local id = resultFixedList[i]

        if id > 0 then
            local data = {}
            data.id = id
            data.index = i + 1
            local state = CLuaEquipMgr.EnumCompositWordTipState.Normal
            data.state = state
            table.insert(resultWordList, data)

            -- 设置高
            local word = Word_Word.GetData(data.id)
            testLabel.text = word.Description
            testLabel:UpdateNGUIText()
            local height = testLabel.height
            if resultIndex <= #heightList and height > heightList[resultIndex] then
                heightList[resultIndex] = height
            else
                table.insert(heightList, height)
            end
            resultIndex = resultIndex + 1
        end
    end

    -- 合成装备额外词条
    local resultExtraList = equip:GetTempExtWordArray()
    for i =0, resultExtraList.Length -1 do
        local id = resultExtraList[i]

        if id > 0 then
            local data = {}
            data.id = id
            data.index = i + 1

            -- 状态有三种 一种是正常 一种是已锁 一种是置灰
            local state = CLuaEquipMgr.EnumCompositWordTipState.Normal
            data.state = state
            table.insert(resultWordList, data)

            -- 设置高
            local word = Word_Word.GetData(data.id)
            testLabel.text = word.Description
            testLabel:UpdateNGUIText()
            local height = testLabel.height
            if resultIndex <= #heightList and height > heightList[resultIndex] then
                heightList[resultIndex] = height
            else
                table.insert(heightList, height)
            end
            resultIndex = resultIndex + 1
        end
    end

    local mainIndex = 1
    -- 主装备固定词条
    local mainFixedList = equip:GetFixedWordList(false)
    for i =0, mainFixedList.Count -1 do
        local id = mainFixedList[i]

        if id > 0 then
            local data = {}
            data.id = id
            data.index = i + 1
    
            -- 状态有三种 一种是正常 一种是已锁 一种是置灰
            local state = CLuaEquipMgr.EnumCompositWordTipState.Normal
    
            for k, lock in ipairs(lockData) do
                if not lock.isExtra and lock.index == i +1 then
                    if lock.isMain then
                        state = CLuaEquipMgr.EnumCompositWordTipState.Locked
                    else
                        state = CLuaEquipMgr.EnumCompositWordTipState.Disable
                    end
                end 
            end

            if resultWordList[mainIndex].id ~= id then
                state = CLuaEquipMgr.EnumCompositWordTipState.Disable
            end

            data.state = state
            table.insert(mainWordList, data)
    
            -- 设置高
            local word = Word_Word.GetData(data.id)
            testLabel.text = word.Description
            testLabel:UpdateNGUIText()

            local height = testLabel.height
            if mainIndex <= #heightList and height > heightList[mainIndex] then
                heightList[mainIndex] = height
            else
                table.insert(heightList, height)
            end

            mainIndex = mainIndex + 1
        end
    end

    -- 主装备额外词条
    local mainExtraList = equip:GetExtWordList(false)
    for i =0, mainExtraList.Count -1 do
        local id = mainExtraList[i]

        if id > 0 then
            local data = {}
            data.id = id
            data.index = i + 1
            -- 状态有三种 一种是正常 一种是已锁 一种是置灰
            local state = CLuaEquipMgr.EnumCompositWordTipState.Normal

            for k, lock in ipairs(lockData) do
                if lock.isExtra and lock.index == i +1 then
                    if lock.isMain then
                        state = CLuaEquipMgr.EnumCompositWordTipState.Locked
                    else
                        state = CLuaEquipMgr.EnumCompositWordTipState.Disable
                    end
                end 
            end
            if resultWordList[mainIndex].id ~= id then
                state = CLuaEquipMgr.EnumCompositWordTipState.Disable
            end

            data.state = state
            table.insert(mainWordList, data)

            -- 设置高
            local word = Word_Word.GetData(data.id)
            testLabel.text = word.Description
            testLabel:UpdateNGUIText()
            
            local height = testLabel.height
            if mainIndex <= #heightList and height > heightList[mainIndex] then
                heightList[mainIndex] = height
            else
                table.insert(heightList, height)
            end

            mainIndex = mainIndex + 1
        end
    end

    local matIndex = 1
    -- 材料装备固定词条
    local matFixedList = equip.TempCompositeSourceFixedWords

    for i =0, matFixedList.Length -1 do
        local id = matFixedList[i]
        print(id)
        if id > 0 then
            local data = {}
            data.id = id
            data.index = matIndex

            -- 状态有三种 一种是正常 一种是已锁 一种是置灰
            local state = CLuaEquipMgr.EnumCompositWordTipState.Normal

            for k, lock in ipairs(lockData) do
                if not lock.isExtra and lock.index == matIndex then
                    if not lock.isMain then
                        state = CLuaEquipMgr.EnumCompositWordTipState.Locked
                    else
                        state = CLuaEquipMgr.EnumCompositWordTipState.Disable
                    end
                end 
            end
            
            if resultWordList[matIndex].id ~= id then
                state = CLuaEquipMgr.EnumCompositWordTipState.Disable
            end

            data.state = state
            table.insert(matWordList, data)

            -- 设置高
            local word = Word_Word.GetData(data.id)
            testLabel.text = word.Description
            testLabel:UpdateNGUIText()
            local height = testLabel.height
            if matIndex <= #heightList and height > heightList[matIndex] then
                heightList[matIndex] = height
            else
                table.insert(heightList, height)
            end
            matIndex = matIndex + 1
        end
    end

    -- 材料装备额外词条
    local matExtraIndex = 1
    local matExtraList = equip.TempCompositeSourceExtraWords
    print(matExtraList, matExtraList.Length)
    for i =0, matExtraList.Length -1 do
        local id = matExtraList[i]

        if id > 0 then
            local data = {}
            data.id = id
            data.index = matExtraIndex
    
            -- 状态有三种 一种是正常 一种是已锁 一种是置灰
            local state = CLuaEquipMgr.EnumCompositWordTipState.Normal
    
            for k, lock in ipairs(lockData) do
                if lock.isExtra and lock.index == matExtraIndex then
                    if not lock.isMain then
                        state = CLuaEquipMgr.EnumCompositWordTipState.Locked
                    else
                        state = CLuaEquipMgr.EnumCompositWordTipState.Disable
                    end
                end 
            end

            if resultWordList[matIndex] == nil or resultWordList[matIndex].id ~= id then
                state = CLuaEquipMgr.EnumCompositWordTipState.Disable
            end

            data.state = state
            matExtraIndex = matExtraIndex + 1
            table.insert(matWordList, data)

            -- 设置高
            local word = Word_Word.GetData(data.id)
            testLabel.text = word.Description
            testLabel:UpdateNGUIText()
            local height = testLabel.height
            if matIndex <= #heightList and height > heightList[matIndex] then
                heightList[matIndex] = height
            else
                table.insert(heightList, height)
            end
            matIndex = matIndex + 1
        end
    end


    -- 处理显示和回调
    CLuaEquipMgr:InitWord(itemId, self.Root1.transform:Find("Desc/WordTable"):GetComponent(typeof(UITable)), self.DescItem, mainWordList, heightList, nil)
    CLuaEquipMgr:InitWord(itemId, self.Root2.transform:Find("Desc/WordTable"):GetComponent(typeof(UITable)), self.DescItem, matWordList, heightList, nil)
    CLuaEquipMgr:InitWord(itemId, self.Root3.transform:Find("Desc/WordTable"):GetComponent(typeof(UITable)), self.DescItem, resultWordList, heightList, nil)

    print(self.Root1.transform:Find("Desc"):GetComponent(typeof(CUIRestrictScrollView)))
    self.Root1.transform:Find("Desc"):GetComponent(typeof(CUIRestrictScrollView)):ResetPosition()
    self.Root2.transform:Find("Desc"):GetComponent(typeof(CUIRestrictScrollView)):ResetPosition()
    self.Root3.transform:Find("Desc"):GetComponent(typeof(CUIRestrictScrollView)):ResetPosition()

    -- 显示评分
    self.Root1.transform:Find("ScoreLabel"):GetComponent(typeof(UILabel)).text = tostring(CEquipmentBaptizeMgr.FormatScore(equip.Score))
    self.Root3.transform:Find("ScoreLabel"):GetComponent(typeof(UILabel)).text = tostring(CEquipmentBaptizeMgr.FormatScore(equip.TempScore))
    -- 不显示评分
    self.Root2.transform:Find("ScoreLabel").gameObject:SetActive(false)

    -- 不在装备的时候 不显示比较
    local place, pos = CLuaEquipMgr:GetItemPlaceAndPos(CLuaEquipMgr.m_ResultEquipItemId)

    if not item.OnBody then
        self.CompareBtn:SetActive(false)
    end
end


function LuaEquipCompositeResultWnd:OnShowDif(args)
    local obj = args[0]
    local list = TypeAs(obj, typeof(MakeGenericClass(List, PlayerShowDifProType)))
    if list == nil then
        return
    end
    if list.Length == 0 then
        g_MessageMgr:ShowMessage("BaptizeWordResultNoPropDif")
        return
    end
    CPropertyDifMgr.ShowPlayerProDifWithEquip(list)
end

function LuaEquipCompositeResultWnd:OnEnable()
    g_ScriptEvent:AddListener("TryConfirmBaptizeWordResult", self, "OnShowDif")
end

function LuaEquipCompositeResultWnd:OnDisable()
    CLuaEquipMgr.m_PassCompositeFx = false
    g_ScriptEvent:RemoveListener("TryConfirmBaptizeWordResult", self, "OnShowDif")
end

--@region UIEvent

function LuaEquipCompositeResultWnd:OnSelectNewBtnClick()
    self.m_ChooseFx.transform.localPosition = Vector3(1003,0,0)
    self.m_ChooseFx.gameObject:SetActive(true)
    self.m_ChooseFx.enabled = true
    self.m_ChooseFx:Play("equipcompositeresultwnd_xuanzhong01",0,0)
    
    local itemId = CLuaEquipMgr.m_ResultEquipItemId
    local place, pos = CLuaEquipMgr:GetItemPlaceAndPos(itemId)

    UnRegisterTick(self.m_Tick)
    self.m_Tick = RegisterTickOnce(function()
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("Equip_Composite_Confirm_Choose_New_Words"), function()
            Gac2Gas.CompositeExtraEquipWordResultConfirm(place, pos, itemId, true)
            CUIManager.CloseUI(CLuaUIResources.EquipCompositeResultWnd)
        end, nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
    end, 700)
end

function LuaEquipCompositeResultWnd:OnSelectOldBtnClick()
    self.m_ChooseFx.transform.localPosition = Vector3(0,0,0)
    self.m_ChooseFx.gameObject:SetActive(true)
    self.m_ChooseFx.enabled = true
    self.m_ChooseFx:Play("equipcompositeresultwnd_xuanzhong01",0,0)

    local itemId = CLuaEquipMgr.m_ResultEquipItemId
    local place, pos = CLuaEquipMgr:GetItemPlaceAndPos(itemId)

    UnRegisterTick(self.m_Tick)
    self.m_Tick = RegisterTickOnce(function()
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("Equip_Composite_Confirm_Choose_Old_Words"), function()
            Gac2Gas.CompositeExtraEquipWordResultConfirm(place, pos, itemId, false)
            CUIManager.CloseUI(CLuaUIResources.EquipCompositeResultWnd)
        end, nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
    end, 700)
end

function LuaEquipCompositeResultWnd:OnShareBtnClick()
    CUICommonDef.CaptureScreenAndShare()
end

function LuaEquipCompositeResultWnd:OnCompareBtnClick()
    local itemId = CLuaEquipMgr.m_ResultEquipItemId
    local place, pos = CLuaEquipMgr:GetItemPlaceAndPos(itemId)

    print("TryConfirmCompositeExtraEquipWord", place, pos, itemId)
    Gac2Gas.TryConfirmCompositeExtraEquipWord(place, pos, itemId)
end

function LuaEquipCompositeResultWnd:OnCloseButtonClick()
    g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("Equip_Composite_Confirm_Exit"), function()
        CUIManager.CloseUI(CLuaUIResources.EquipCompositeResultWnd)
    end, nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
end


--@endregion UIEvent

