local MsgPackImpl=import "MsgPackImpl"
local CGuildLeagueMgr = import "L10.Game.CGuildLeagueMgr"
local Extensions = import "Extensions"
local Profession = import "L10.Game.Profession"
local EnumClass = import "L10.Game.EnumClass"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CGuildMgr = import "L10.Game.CGuildMgr"
local CShopMallMgr   = import "L10.UI.CShopMallMgr"

LuaGuildLeagueCrossMgr = {}
LuaGuildLeagueCrossMgr.m_ScheduleInfo = nil
LuaGuildLeagueCrossMgr.m_ScheduleRedAlertInfo = nil --标记红点是否已读的信息，运行时有效
LuaGuildLeagueCrossMgr.m_ServerMatchQueryIndex = nil
LuaGuildLeagueCrossMgr.m_ServerMatchDetailInfo = {}
LuaGuildLeagueCrossMgr.m_ServerGuildInfo = nil
LuaGuildLeagueCrossMgr.m_GuildPosExchangeInfo = nil
LuaGuildLeagueCrossMgr.m_TrainAdditionInfo = nil --两个对手服的培养增益信息
LuaGuildLeagueCrossMgr.m_TrainInfo = nil
LuaGuildLeagueCrossMgr.m_TrainType2ItemTbl = nil
LuaGuildLeagueCrossMgr.m_BuffTaskAlertVisible = true --默认显示红点，玩法开启期间每次重启客户端红点生效
LuaGuildLeagueCrossMgr.m_QualifiedWatchIdTbl = {} --合法的可以进行观战的玩家ID，通过patch来修改，需要服务器和客户端同时进行patch
LuaGuildLeagueCrossMgr.m_WatchInfo = {}
LuaGuildLeagueCrossMgr.m_GambleInfo = {}
LuaGuildLeagueCrossMgr.m_GambleBetInfo = {}
LuaGuildLeagueCrossMgr.m_GambleGetRewardResult = {}
LuaGuildLeagueCrossMgr.m_AidEvaluationInfo = {}
LuaGuildLeagueCrossMgr.m_CachedZhanLingId = nil
LuaGuildLeagueCrossMgr.m_ZhanLingAwardPoolInfo = nil
LuaGuildLeagueCrossMgr.m_ZhanLingExtInfo = nil

LuaGuildLeagueCrossMgr.m_MAX_GROUP_COUNT = 4

EnumGuildLeagueCrossRound = {--对应服务器端EnumGuildLeagueCrossRound,以及策划表Agenda表单Key
	eEnd = 0,
	eGroupRound1 = 1,
	eGroupRound2 = 2,
	eGroupRound3 = 3,
	eRankRound1 = 4,
	eRankRound2 = 5,
	eRankRound3 = 6,
	eRankRound4 = 7,
}

EnumGuildLeagueCrossPlayStatus = { --对应服务器端EnumPlayStatus
	eBeiZhan = 0,
    ePrepare = 1,
    eStart = 2,
    eFinish = 3,
}

EnumGuildLeagueCrossGambleType = { --对应服务器端EnumGuildLeagueCrossGambleType
	eNone = 0,
	eFirst = 1,
	eSecond = 2,
	eThird = 3,
	eFourth = 4,
	eRank1 = 5,
	eRank2 = 6,
	eRank3 = 7,
}

EnumGuildLeagueCrossGambleChoiceInfoKey = { --对应服务器端EnumGuildLeagueCrossGambleChoiceInfoKey
	eServerName = 1,
	eServerSilver = 2,
	eServerOdds = 3,
	eRank = 4,
	eGroup = 5,
	eResult = 6,
}

EnumGuildLeagueCrossScheduleTempAlert ={
	eTrainAlert = 0,
	eGambleAlert = 1,
	-- 外援申请不做处理，有申请时一直保留红点提示
}

EnumQueryGLCServerGuildInfoContext = {
	eDefault = 0,
	eGamble = 1,
}

EnumGuildLeagueCrossGambleStatus = {
	eNotStarted = 0, --未开始
	eOpen = 1,	--进行中
	eClosed = 2, --已封盘
}

EnunGuildLeagueCrossTrainType = { --对应服务器EnunGuildLeagueCrossTrainType
	ePlayerHp = 1,
	eTowerHp = 2,
	eMonsterHp = 3,
}

EnumZhanLingAwardPoolWndOpenType = {
	eDefault = 0,
	eZhanLing = 1,
	eGamble = 2,
}
--显示赛制日程
function LuaGuildLeagueCrossMgr:ShowAgendaWnd()
	CUIManager.ShowUI("GuildLeagueCrossAgendaWnd")
end
--显示本服代表
function LuaGuildLeagueCrossMgr:ShowSelfGuildListWnd()
	CUIManager.ShowUI("GuildLeagueCrossSelfServerGuildListWnd")
end
--显示对阵信息
function LuaGuildLeagueCrossMgr:ShowMatchInfoWnd()
	CUIManager.ShowUI("GuildLeagueCrossMatchInfoWnd")
end
--显示奖励一览
function LuaGuildLeagueCrossMgr:ShowAwardOverviewWnd()
	CUIManager.ShowUI("GuildLeagueCrossAwardOverviewWnd")
end
--显示比赛结果
function LuaGuildLeagueCrossMgr:ShowResultWnd()
	CUIManager.ShowUI("GuildLeagueCrossResultWnd")
end
--显示两组服务器的对战详情
function LuaGuildLeagueCrossMgr:ShowServerMatchDetailInfoWnd(matchIndex)
	self.m_ServerMatchQueryIndex = matchIndex
	self.m_ServerMatchDetailInfo = {}
	CUIManager.ShowUI("GuildLeagueCrossMatchDetailInfoWnd")
end
--显示观战
function LuaGuildLeagueCrossMgr:RequestWatchGuildLeagueCrossPlay(matchIdx, playIndex)
	Gac2Gas.RequestWatchGuildLeagueCrossPlay(matchIdx, playIndex)
end

--显示帮会位置互换
function LuaGuildLeagueCrossMgr:ShowGuildPosExchangeWnd(requestInfoTbl, guildInfoTbl)
	self.m_GuildPosExchangeInfo = {}
	self.m_GuildPosExchangeInfo.requestInfoTbl = requestInfoTbl
	self.m_GuildPosExchangeInfo.guildInfoTbl = guildInfoTbl
	CUIManager.ShowUI("GuildLeagueCrossPosExchangeWnd")
end
---------------------------------------------------------------------
--查询活动日程界面上的活动进展信息
function LuaGuildLeagueCrossMgr:QueryForOpenGLCWnd()
	Gac2Gas.QueryForOpenGLCWnd()
end
--查询本服代表信息
function LuaGuildLeagueCrossMgr:QueryGLCJoinCrossGuildInfo()
	Gac2Gas.QueryGLCJoinCrossGuildInfo()
end
--查询小组赛/排位赛对阵信息
function LuaGuildLeagueCrossMgr:QueryGLCMatchInfo(bGroupMatch)
	Gac2Gas.QueryGLCMatchInfo(bGroupMatch)
end
--查询小组赛积分信息
function LuaGuildLeagueCrossMgr:QueryGLCGroupScoreInfo()
	Gac2Gas.QueryGLCGroupScoreInfo()
end

--查询排位赛对阵信息

--查询两组服务器对阵信息
function LuaGuildLeagueCrossMgr:QueryGLCServerMatchInfo()
	if self.m_ServerMatchQueryIndex then
		Gac2Gas.QueryGLCServerMatchInfo(self.m_ServerMatchQueryIndex)
	end
end

--查询并打开服务器参赛帮会界面 !!! RPC被复用到了竞猜中，通过context区分
function LuaGuildLeagueCrossMgr:QueryGLCServerGuildInfo(serverId, serverName, context)
	self.m_ServerGuildInfo = {}
	self.m_ServerGuildInfo.serverId = serverId
	self.m_ServerGuildInfo.serverName = serverName
	local ctx = (context==nil and EnumQueryGLCServerGuildInfoContext.eDefault or context)
	Gac2Gas.QueryGLCServerGuildInfo(serverId, ctx)
end

function LuaGuildLeagueCrossMgr:QueryGuildLeagueCrossServerMatchRecord(serverId)
	Gac2Gas.QueryGuildLeagueCrossServerMatchRecord(serverId)
end

-- 换位出战：申请换位出战
function LuaGuildLeagueCrossMgr:RequestGLCExchangeGuildPos(destGuildId)
	Gac2Gas.RequestGLCExchangeGuildPos(destGuildId)
end
-- 换位出战：撤回换位申请
function LuaGuildLeagueCrossMgr:CancelGLCExchangeGuildPos(destGuildId)
	Gac2Gas.CancelGLCExchangeGuildPos(destGuildId)
	--按洪磊的说法服务器一定会移除成功，所以没有给回调，这里客户端自行处理一下
	local guildInfoTbl = self.m_GuildPosExchangeInfo.guildInfoTbl
	if guildInfoTbl then
		for __,info in pairs(guildInfoTbl) do
			if info.guildId == destGuildId then
				info.hasApply = false
				g_ScriptEvent:BroadcastInLua("OnCancelGLCExchangeGuildPosSuccess")
				break
			end
		end
	end
end
-- 换位出战：同意换位申请
function LuaGuildLeagueCrossMgr:ConfirmGLCExchangeGuildPos(srcGuildId, srcIndex)
	Gac2Gas.ConfirmGLCExchangeGuildPos(srcGuildId, srcIndex)
end
--换位出战：拒绝换位申请
function LuaGuildLeagueCrossMgr:RefuseGLCExchangeGuildPos(srcGuildId)
	Gac2Gas.RefuseExchangeGuildLeagueCrossGuildPos(srcGuildId)
	--服务器端没有回调，这里约定操作会成功，所以直接客户端修改一下数据
	local tbl = LuaGuildLeagueCrossMgr.m_GuildPosExchangeInfo.requestInfoTbl
	for index, info in pairs(tbl) do
		if info.guildId == srcGuildId then
			table.remove(tbl, index)
			break
		end
	end
	g_ScriptEvent:BroadcastInLua("OnRefuseGLCExchangeGuildPosSuccess")
end
--换位出战：换位记录
function LuaGuildLeagueCrossMgr:QueryGuildLeagueCrossExchangePosRecord()
	Gac2Gas.QueryGuildLeagueCrossExchangePosRecord()
end

function LuaGuildLeagueCrossMgr:QueryGLCGuildPosInfo()
	Gac2Gas.QueryGLCGuildPosInfo() --回调成功会会打开帮会位置互换界面
end

function LuaGuildLeagueCrossMgr:QueryGuildLeagueTrainInfo()
	Gac2Gas.OpenGuildLeagueTrainWnd()
end

function LuaGuildLeagueCrossMgr:QueryGuildLeagueBuffTaskInfo()
	Gac2Gas.OpenGuildLeagueBuffTaskWnd()
end

function LuaGuildLeagueCrossMgr:CheckAndOpenJoinGuildWndForTrain()
	if CClientMainPlayer.Inst == nil or not CClientMainPlayer.Inst:IsInGuild() then
        g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("GLC_NO_GUILD_FOR_TRAIN_CONFIRM"), function ()
            CGuildMgr.Inst:OnOpenGuildWnd()
        end, nil, nil, nil, false)
        return true
    end
	return false
end

--培养活动和日常活动：服务器触发打开培养界面
function LuaGuildLeagueCrossMgr:OpenGuildLeagueTrainWnd(info_U)

	-- local ret = {
	-- 	curScore,
	-- 	totalScore,
	-- 	g_GuildLeagueCrossMgr:GetTrainRankMinScore(),
	-- 	g_GuildLeagueCrossMgr:GetPlayerTrainRank(playerId),
	-- 	g_GuildLeagueCrossMgr.m_TrainScoreRankNum,
	-- 	g_GuildLeagueCrossMgr.m_TrainType2Level,
	-- 	g_GuildLeagueCrossMgr.m_TrainType2Value,
	-- 	dailyTrainScore,
	--  bOpen,
	--  trainLevelRewardInfo,
	-- }
	if self:CheckAndOpenJoinGuildWndForTrain() then
        return
    end
	local tmp = g_MessagePack.unpack(info_U)
	local trainInfoTbl = {}
	trainInfoTbl.isBuffTask = false
	trainInfoTbl.nameInfo = {LocalString.GetString("人物气血"),LocalString.GetString("箭塔气血"),LocalString.GetString("神兽/主炮气血")}
	trainInfoTbl.portraitInfo = {"UI/Texture/Transparent/Material/GuildLeagueCrossTrainWnd_icon_qixue.mat","UI/Texture/Transparent/Material/GuildLeagueCrossTrainWnd_icon_jianta.mat","UI/Texture/Transparent/Material/GuildLeagueCrossTrainWnd_icon_paotai.mat"}
	trainInfoTbl.colorInfo = {NGUIText.ParseColor24("FF816C", 0),NGUIText.ParseColor24("FFD48A", 0),NGUIText.ParseColor24("99F2FF", 0)}
	trainInfoTbl.nameColorInfo = {NGUIText.ParseColor24("F06258", 0),NGUIText.ParseColor24("FFB95F", 0),NGUIText.ParseColor24("88D5F5", 0)}
	if tmp and #tmp>0 then
		trainInfoTbl.curScore = tonumber(tmp[1])
		trainInfoTbl.totalScore = tonumber(tmp[2])
		trainInfoTbl.rankMinScore = tonumber(tmp[3])
		trainInfoTbl.playerRank = tonumber(tmp[4]) --排行榜名次
		trainInfoTbl.playerRankMax = tonumber(tmp[5]) --排行榜最大数量
		trainInfoTbl.levelInfo = {tmp[6][1], tmp[6][2], tmp[6][3]}
		trainInfoTbl.progressInfo = {tmp[7][1], tmp[7][2], tmp[7][3]}
		trainInfoTbl.dailyTrainScore =  tonumber(tmp[8])
		trainInfoTbl.bOpen =  tmp[9]
		local rewardInfo = tonumber(tmp[10])
		local function getGLCTrainLevelRewardIndexByTrainType(trainType, rewardInfo)
			if trainType == EnunGuildLeagueCrossTrainType.ePlayerHp then
				return rewardInfo % 10
			elseif trainType == EnunGuildLeagueCrossTrainType.eTowerHp then
				return math.floor(rewardInfo / 10) % 10
			elseif trainType == EnunGuildLeagueCrossTrainType.eMonsterHp then
				return math.floor(rewardInfo / 100)
			end
			return -1
		end
		trainInfoTbl.trainLevelRewardInfo = {getGLCTrainLevelRewardIndexByTrainType(1,rewardInfo), getGLCTrainLevelRewardIndexByTrainType(2,rewardInfo), getGLCTrainLevelRewardIndexByTrainType(3,rewardInfo)}
		self.m_TrainInfo = trainInfoTbl
		CUIManager.ShowUI("GuildLeagueCrossTrainWnd")
	end
end

--培养活动和日常活动：服务器触发打开增益任务界面，客户端复用培养界面
function LuaGuildLeagueCrossMgr:OpenGuildLeagueBuffTaskWnd(info_U)
	-- ret = {
	-- 	finishTaskNum,
	-- 	limitTaskNum,
	-- 	g_GuildLeagueCrossMgr.m_BuffTaskType2Level,
	-- 	g_GuildLeagueCrossMgr.m_BuffTaskType2Value,
	--  g_GuildLeagueCrossMgr.m_TrainType2Level,
	--  contribution,
	--	g_GuildLeagueCrossMgr:GetTrainRankMinScore(),
	--	g_GuildLeagueCrossMgr:GetPlayerTrainRank(playerId),
	--	g_GuildLeagueCrossMgr.m_TrainContriRankNum,
	--	bOpen,
	--	temp,
	-- }
	if self:CheckAndOpenJoinGuildWndForTrain() then
        return
    end
	local tmp = g_MessagePack.unpack(info_U)
	local trainInfoTbl = {}
	trainInfoTbl.isBuffTask = true
	trainInfoTbl.nameInfo = {LocalString.GetString("人物气血"),LocalString.GetString("攻/防增益初始数")}
	trainInfoTbl.portraitInfo = {"UI/Texture/Transparent/Material/GuildLeagueCrossTrainWnd_icon_qixue.mat","UI/Texture/Transparent/Material/GuildLeagueCrossTrainWnd_icon_gongfang.mat"}
	trainInfoTbl.colorInfo = {NGUIText.ParseColor24("FF816C", 0),NGUIText.ParseColor24("FACCFF", 0)}
	trainInfoTbl.nameColorInfo = {NGUIText.ParseColor24("F06258", 0),NGUIText.ParseColor24("BE81D4", 0)}
	if tmp and #tmp>0 then
		trainInfoTbl.bt_finishTaskNum = tonumber(tmp[1])
		trainInfoTbl.bt_limitTaskNum = tonumber(tmp[2])
		trainInfoTbl.bt_levelInfo = {tmp[3][1], tmp[3][2]}
		trainInfoTbl.bt_progressInfo = {tmp[4][1], tmp[4][2]}
		trainInfoTbl.bt_trainLevelInfo = {tmp[5][1], tmp[5][2]}
		trainInfoTbl.totalScore = tonumber(tmp[6])
		trainInfoTbl.rankMinScore = tonumber(tmp[7])
		trainInfoTbl.playerRank = tonumber(tmp[8]) --排行榜名次
		trainInfoTbl.playerRankMax = tonumber(tmp[9]) --排行榜最大数量
		trainInfoTbl.bOpen =  tmp[10]
		trainInfoTbl.guildBuffTaskRank = {}
		--{times, guildId, leaderName, guildName}
		for i=1, #tmp[11] do
            local oneGuildRankInfo = tmp[11][i]
			table.insert(trainInfoTbl.guildBuffTaskRank, {times=oneGuildRankInfo[1], guildId=oneGuildRankInfo[2], leaderName=oneGuildRankInfo[3], guildName=oneGuildRankInfo[4]})
		end
		self.m_TrainInfo = trainInfoTbl
		CUIManager.ShowUI("GuildLeagueCrossTrainWnd")
	end
end

function LuaGuildLeagueCrossMgr:ShowTrainAdditionWnd(server1Name, server2Name, twoServerBuffInfoTbl)
	self.m_TrainAdditionInfo = {}
	self.m_TrainAdditionInfo.server1Name = server1Name
	self.m_TrainAdditionInfo.server2Name = server2Name
	self.m_TrainAdditionInfo.additionInfo = twoServerBuffInfoTbl -- {sever1BuffInfo, server2BuffInfo}
	CUIManager.ShowUI("GuildLeagueCrossTrainAdditionWnd")
end

function LuaGuildLeagueCrossMgr:ShowTrainShopWnd()
	CLuaNPCShopInfoMgr.ShowScoreShop("GLCTrainScore") --对应shop表定义
end

--培养活动和日常活动：查询本服和对手服培养加成对比信息
function LuaGuildLeagueCrossMgr:QueryGuildLeagueCrossMatchBuffInfo()
	Gac2Gas.QueryGuildLeagueCrossMatchBuffInfo()
end

--培养活动和日常活动：培养活动提交资源
function LuaGuildLeagueCrossMgr:SubmitItemToGuildLeagueTrain(trainType, itemId, place, pos, count, templateId)
	Gac2Gas.SubmitItemToGuildLeagueTrain(trainType, itemId, place, pos, count, templateId)
end

--培养活动和日常活动：查询排行榜
function LuaGuildLeagueCrossMgr:QueryGuildLeagueTrainRank()
	Gac2Gas.QueryGuildLeagueTrainRank()
end

--培养活动和日常活动：请求领取增益任务
function LuaGuildLeagueCrossMgr:RequestAcceptGuildLeagueBuffTask(buffTaskType)
	Gac2Gas.RequestAcceptGuildLeagueBuffTask(buffTaskType)
end

--是否显示任务培养的tab红点
function LuaGuildLeagueCrossMgr:NeedShowBuffTaskAlert()
	return self.m_BuffTaskAlertVisible and CServerTimeMgr.Inst.timeStamp<self:GetTrainEndTime()
end
--将任务培养tab红点标记为已读并隐藏
function LuaGuildLeagueCrossMgr:MarkBuffTaskAlertHidden()
	self.m_BuffTaskAlertVisible = false
end

-- 领取全服培养等级个人奖励
function LuaGuildLeagueCrossMgr:RequestGetGuildLeagueCrossTrainLevelReward(trainType, rewardIdx)
	Gac2Gas.RequestGetGuildLeagueCrossTrainLevelReward(trainType, rewardIdx)
end

function LuaGuildLeagueCrossMgr:RequestGetGuildLeagueCrossTrainLevelRewardSuccess(trainType, rewardIdx)
	local info = LuaGuildLeagueCrossMgr.m_TrainInfo
	info.trainLevelRewardInfo[trainType] = rewardIdx
	g_ScriptEvent:BroadcastInLua("RequestGetGuildLeagueCrossTrainLevelRewardSuccess", trainType, rewardIdx)
end

-- 查询培养排行榜排名奖励
function LuaGuildLeagueCrossMgr:QueryGuildLeagueCrossTrainRankRewardInfo()
	Gac2Gas.QueryGuildLeagueCrossTrainRankRewardInfo()
end

function LuaGuildLeagueCrossMgr:SendGuildLeagueCrossTrainRankRewardInfo(ret_U)
	local tmp = g_MessagePack.unpack(ret_U)
	local rankInfo = {}
	for index,rank in pairs(tmp) do
		rankInfo[index] = rank --根据和洪磊的约定，这里rankInfo是个list，并且随着结算次数容量从0增加到4
	end
	g_ScriptEvent:BroadcastInLua("SendGuildLeagueCrossTrainRankRewardInfo", rankInfo)
end
-------------
----战令
-------------
function LuaGuildLeagueCrossMgr:GetZhanLingId()
	if self.m_CachedZhanLingId==nil then
		self.m_CachedZhanLingId = GuildLeagueCross_Setting.GetData().ZhanLingId
	end
	return self.m_CachedZhanLingId
end

function LuaGuildLeagueCrossMgr:GetCurZhanLingLevel()
	return LuaCommonPassportMgr:GetLevel(self:GetZhanLingId())
end

function LuaGuildLeagueCrossMgr:GetCurZhanLingRewardLevel()
	return LuaCommonPassportMgr:GetRewardLevel(self:GetZhanLingId())
end

function LuaGuildLeagueCrossMgr:HasZhanLingReward()
	return LuaCommonPassportMgr:GetHasPassAward(self:GetZhanLingId())
end

function LuaGuildLeagueCrossMgr:GetZhanLingMaxLevel()
	return LuaCommonPassportMgr:GetMaxPassLevel(self:GetZhanLingId())
end

function LuaGuildLeagueCrossMgr:GetZhanLingUpgradeNeedExp()
	local progressInfo = LuaCommonPassportMgr:GetUpgradeProgressInfo(self:GetZhanLingId())
	return progressInfo.needProgress
end

function LuaGuildLeagueCrossMgr:GetZhanLingBuyLevelJadeCost(furtureLevel, curLevel, oneJadeAddValue, needExpTbl)
	local expCost = 0
    local progressInfo = LuaCommonPassportMgr:GetUpgradeProgressInfo(self:GetZhanLingId())
    for i=curLevel+1, furtureLevel do
        if needExpTbl[i] then
            --到下一级可能已经积累了一些经验，所以所需经验不能直接加needExpTbl[i]
            expCost = (i==curLevel+1 ) and expCost+progressInfo.needProgress  or expCost+needExpTbl[i]
        end
    end
    return math.ceil(expCost/oneJadeAddValue)
end

function LuaGuildLeagueCrossMgr:IsZhanLingRewardReceived(level)
	return LuaCommonPassportMgr:IsPassAwardReceived(
		LuaCommonPassportMgr:GetPassInfo(self:GetZhanLingId()), level, 1)
end

--仿照LuaCommonPassportRewardView:OnOneKeyButtonClick，一键领取
function LuaGuildLeagueCrossMgr:GetAllZhanLingRewards()
	Gac2Gas.RequestGetPassReward(self:GetZhanLingId(), 0, 0)
end
--仿照LuaCommonPassportRewardView:OnItemClick
function LuaGuildLeagueCrossMgr:GetOneZhanLingReward(level)
	Gac2Gas.RequestGetPassReward(self:GetZhanLingId(), level, 1)
end

function LuaGuildLeagueCrossMgr:GetJadeCostAndExpProgressInfoForBuyZhanLingLevel()
	local oneJadeAddValue = LuaCommonPassportMgr:GetOneJadeAddProgress(self:GetZhanLingId())
	local rewardDesignData = LuaCommonPassportMgr:GetRewardDesignData(self:GetZhanLingId())
    local needExpTbl = {}
	local curLevel = LuaCommonPassportMgr:GetLevel(self:GetZhanLingId())
    local lastLevel = curLevel
    for _, data1 in ipairs(rewardDesignData) do
        if data1.level >curLevel then
            local rewardData = Pass_Reward.GetData(data1.id)
            local needProgress = rewardData.Progress
            needExpTbl[data1.level] = needProgress

            for i = lastLevel + 1, data1.level - 1 do
                needExpTbl[i] = needProgress
            end
            lastLevel = data1.level
        end
    end
	return oneJadeAddValue, needExpTbl
end

function LuaGuildLeagueCrossMgr:BuyZhanLingLevel(futureLv, cost)
	--仿照LuaCommonPassportWnd:BuyLevelCallBack(cost, exp, level)
	local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer then
        if mainPlayer.Jade + mainPlayer.BindJade < cost then
            g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("JADE_PAY_NOTICE"), function ()
                CShopMallMgr.ShowChargeWnd()
            end, nil, nil, nil, false)
        else
            Gac2Gas.RequestBuyPassLevel(self:GetZhanLingId(), futureLv)
        end
    end
end

function LuaGuildLeagueCrossMgr:GetZhanLingAwardInfo()
	local designInfo = LuaCommonPassportMgr:GetRewardDesignData(self:GetZhanLingId())
	local info = {}
	local maxAllowFutureDisplayCount = GuildLeagueCross_Setting.GetData().ZhanLingMaxVisibleFutureLevelItem
	if maxAllowFutureDisplayCount<0 then
		maxAllowFutureDisplayCount = 65536 --尽可能大的一个数字即可，全允许显示
	end
	local curLevel = self:GetCurZhanLingLevel()
	local cnt = 0
	for __,designData in ipairs(designInfo) do
		local data = {}
		data.id = designData.id
		data.level = designData.level
		data.progress = designData.progress
		data.totalProgress = designData.totalProgress
		local items = LuaCommonPassportMgr:ParseItemRewardsForOneType(designData.id, 1)
		data.itemId = items[1].itemId
		table.insert(info, data)
		if data.level>curLevel then
			cnt = cnt+1
			if cnt>=maxAllowFutureDisplayCount then
				break
			end
		end
	end
	return info
end

function LuaGuildLeagueCrossMgr:GetZhanLingTaskInfo()
	local allTaskInfo = LuaCommonPassportMgr:GetOpenTaskData(self:GetZhanLingId())
	local info = {}
	for category,taskInfos in pairs(allTaskInfo) do
		for __, taskInfo in pairs(taskInfos) do
			local data = {}
			local designData = Pass_Task.GetData(taskInfo.id)
			data.id = taskInfo.id
			data.desc = designData.Desc
			data.progress = taskInfo.progress
			data.needProgress = designData.Target
			data.reward = designData.ProgressReward
			data.finishTimes = taskInfo.finishTimes
			data.totalTimes = designData.FinishTimes
			data.refeshText = (category==1 and LocalString.GetString("每日刷新")) or
							(category==2 and LocalString.GetString("每周刷新")) or ""
			data.isFinished = taskInfo.isFinished --是否全部已完成
			data.action = designData.Action
			table.insert(info, data)
		end
	end
	return info
end

function LuaGuildLeagueCrossMgr:QueryGuildLeagueCrossZhanLingExtInfo()
	Gac2Gas.QueryGuildLeagueCrossZhanLingExtInfo()
end

function LuaGuildLeagueCrossMgr:QueryGuildLeagueCrossZhanLingExtInfoResult(myRank, lingyuPoolAmount)
	self.m_ZhanLingExtInfo = {}
	self.m_ZhanLingExtInfo.myRank = myRank
	self.m_ZhanLingExtInfo.lingyuPoolAmount = lingyuPoolAmount
    CUIManager.ShowUI("GuildLeagueCrossZhanLingWnd")
end

function LuaGuildLeagueCrossMgr:ShowZhanLingRankWnd()
	CUIManager.ShowUI("GuildLeagueCrossZhanLingRankWnd")
end

function LuaGuildLeagueCrossMgr:QueryGuildLeagueCrossZhanLingRank()
	Gac2Gas.QueryGuildLeagueCrossZhanLingRank()
end

function LuaGuildLeagueCrossMgr:QueryGuildLeagueCrossZhanLingRankResult(ret_U, zhanlingLv, zhanlingLingYu)
	local tmp = g_MessagePack.unpack(ret_U)
	--playerId, totalJade, zhanLingLv, class, playerName
	local mainPlayerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
	local rankInfoTbl = {}
	if tmp and #tmp>0 then
		local hasMainPlayerData = false
		for i=1, #tmp do
			local rankInfo = tmp[i]
			local data = {
				rank=i,
				playerId = tonumber(rankInfo[1]), 
				jade = tonumber(rankInfo[2]), 
				zhanlingLv = tonumber(rankInfo[3]),
				professionIcon = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass),tonumber(rankInfo[4]))), 
				playerName = tostring(rankInfo[5]), 
			}
				
			data.isMe = data.playerId == mainPlayerId
			if data.isMe then
				table.insert(rankInfoTbl, 1, data)
				hasMainPlayerData = true
			else
				table.insert(rankInfoTbl, data)
			end
		end
		--将主角未上榜数据塞在最前面
		if not hasMainPlayerData and mainPlayerId>0 then
			local data = {
				rank=-1,
				playerId = mainPlayerId, 
				jade = zhanlingLingYu, 
				zhanlingLv = zhanlingLv,
				professionIcon = Profession.GetIcon(CClientMainPlayer.Inst.Class), 
				playerName = tostring(CClientMainPlayer.Inst.Name), 
				isMe = true
			}
			table.insert(rankInfoTbl, 1, data)
		end
		g_ScriptEvent:BroadcastInLua("OnQueryGuildLeagueCrossZhanLingRankResult", rankInfoTbl)
	end
end

function LuaGuildLeagueCrossMgr:QueryGuildLeagueCrossRewardPoolInfo(type)
	Gac2Gas.QueryGuildLeagueCrossRewardPoolInfo(type)
end

function LuaGuildLeagueCrossMgr:QueryGuildLeagueCrossRewardPoolInfoResult(type, lingyuPoolAmount, yinliangPoolAmount)
	self.m_ZhanLingAwardPoolInfo = {}
	self.m_ZhanLingAwardPoolInfo.type = type
	self.m_ZhanLingAwardPoolInfo.desc = g_MessageMgr:FormatMessage("GLC_ZHANLING_AWARD_POOL_DESC")
	self.m_ZhanLingAwardPoolInfo.lingyuPool = lingyuPoolAmount
	self.m_ZhanLingAwardPoolInfo.yinliangPool = yinliangPoolAmount
	CUIManager.ShowUI("GuildLeagueCrossZhanLingAwardPoolWnd")
end

---------------------------------------------------------------------
--竞猜相关
--竞猜信息
function LuaGuildLeagueCrossMgr:QueryGuildLeagueCrossGambleInfo()
	Gac2Gas.QueryGuildLeagueCrossGambleInfo()
end
--押注
function LuaGuildLeagueCrossMgr:RequestBetGuildLeagueCrossGamble(gambleType, serverId, silver)
	--第三个参数意义变成了投注份数
	Gac2Gas.RequestBetGuildLeagueCrossGamble(gambleType, serverId, silver)
end
--竞猜记录
function LuaGuildLeagueCrossMgr:QueryGuildLeagueCrossGambleRecord()
	Gac2Gas.QueryGuildLeagueCrossGambleRecord()
end
--领奖
function LuaGuildLeagueCrossMgr:RequestGuildLeagueCrossGambleReward()
	Gac2Gas.RequestGuildLeagueCrossGambleReward()
end

function LuaGuildLeagueCrossMgr:ShowGambleBetWnd(gambleType, choiceRankOrServerId, serverName, odds)
	self.m_GambleBetInfo = {}
	self.m_GambleBetInfo.gambleType = gambleType
	self.m_GambleBetInfo.serverId = choiceRankOrServerId
	self.m_GambleBetInfo.serverName = serverName
	self.m_GambleBetInfo.odds = odds
	CUIManager.ShowUI("GuildLeagueCrossGambleBetWnd")
end
---------------------------------------------------------------------
function LuaGuildLeagueCrossMgr:IsGroupMatchOver()
	--客户端通过Agenda表单计算出的粗略排位赛开始时间，实际上可能有误差，最精确的方式是通过服务器
	local result = false
    GuildLeagueCross_Agenda.Foreach(function(key, data)
        if data.Type==2 and data.Round==1 then --排位赛第一轮
			local beginTimestamp = CServerTimeMgr.Inst:GetTimeStampByStr(data.BeginTime)
			local enterTimestamp = beginTimestamp - GuildLeagueCross_Setting.GetData().PrepareTime
			result = enterTimestamp<CServerTimeMgr.Inst.timeStamp
		end
	end)
	return result
end

function LuaGuildLeagueCrossMgr:GetDefultRankMatchInfo()
	local rankMatchInfoTbl = {}
	local defaultTbl =
	{
		--1-8名
		LocalString.GetString("麒麟第1"), LocalString.GetString("龙马第2"), LocalString.GetString("第1场胜者"), LocalString.GetString("第3场胜者"), LocalString.GetString("第5场败者"), LocalString.GetString("第6场败者"), --三四名之战
		LocalString.GetString("凤鸟第1"), LocalString.GetString("元龟第2"), LocalString.GetString("第2场胜者"), LocalString.GetString("第4场胜者"), LocalString.GetString("第7场胜者"), LocalString.GetString("第8场胜者"), --五六名之战
		LocalString.GetString("元龟第1"), LocalString.GetString("凤鸟第2"), LocalString.GetString("第4场败者"), LocalString.GetString("第2场败者"), LocalString.GetString("第7场败者"), LocalString.GetString("第8场败者"), --七八名之战
		LocalString.GetString("龙马第1"), LocalString.GetString("麒麟第2"), LocalString.GetString("第3场败者"), LocalString.GetString("第1场败者"), LocalString.GetString("第5场胜者"), LocalString.GetString("第6场胜者"), --冠亚军之战
		--9-16名
		LocalString.GetString("麒麟第3"), LocalString.GetString("龙马第4"), LocalString.GetString("第1场胜者"), LocalString.GetString("第3场胜者"), LocalString.GetString("第5场胜者"), LocalString.GetString("第6场胜者"), -- 九十名之战
		LocalString.GetString("凤鸟第3"), LocalString.GetString("元龟第4"), LocalString.GetString("第2场胜者"), LocalString.GetString("第4场胜者"), LocalString.GetString("第5场败者"), LocalString.GetString("第6场败者"), --十一十二名之战
		LocalString.GetString("元龟第3"), LocalString.GetString("凤鸟第4"), LocalString.GetString("第4场败者"), LocalString.GetString("第2场败者"), LocalString.GetString("第7场胜者"), LocalString.GetString("第8场胜者"), --十三十四名之战
		LocalString.GetString("龙马第3"), LocalString.GetString("麒麟第4"), LocalString.GetString("第3场败者"), LocalString.GetString("第1场败者"), LocalString.GetString("第7场败者"), LocalString.GetString("第8场败者"), --十五十六名之战
	}
	for rankPage = 0,1 do --两个子页面
		local pageTbl = {}
		rankMatchInfoTbl[rankPage+1] = pageTbl 
		for column=0,2 do
			pageTbl[column+1] = {}
			for row = 0,3 do
				local index = rankPage * 24 +  row * 6 + column * 2 + 1
				table.insert(pageTbl[column+1], {
					matchIndex = nil,
					serverName1 = defaultTbl[index],
					serverName2 = defaultTbl[index+1],
				})
			end
		end
	end
	return rankMatchInfoTbl
end

function LuaGuildLeagueCrossMgr:GetRankMatchRound3TitleInfo()
	local agenda1 = GuildLeagueCross_Agenda.GetData(EnumGuildLeagueCrossRound.eRankRound3)
	local agenda2 = GuildLeagueCross_Agenda.GetData(EnumGuildLeagueCrossRound.eRankRound4)
	local beginTimestamp1 = CServerTimeMgr.Inst:GetTimeStampByStr(agenda1.BeginTime)
	local beginTime1 =  CServerTimeMgr.ConvertTimeStampToZone8Time(beginTimestamp1)
	local timeStr1 = ToStringWrap(beginTime1, " HH:mm")
	local beginTimestamp2 = CServerTimeMgr.Inst:GetTimeStampByStr(agenda2.BeginTime)
	local beginTime2 =  CServerTimeMgr.ConvertTimeStampToZone8Time(beginTimestamp2)
	local timeStr2 = ToStringWrap(beginTime2, " HH:mm")

	local tbl = 
	{
		{
			{title = LocalString.GetString("三四名之战")..timeStr1,  color = NGUIText.ParseColor24("ACF8FF", 0), showBg = false},
			{title = LocalString.GetString("五六名之战")..timeStr1,  color = NGUIText.ParseColor24("ACF8FF", 0), showBg = false},
			{title = LocalString.GetString("七八名之战")..timeStr1,  color = NGUIText.ParseColor24("ACF8FF", 0), showBg = false},
			{title = LocalString.GetString("冠亚军之战")..timeStr2,  color = NGUIText.ParseColor24("FFFE91", 0), showBg = true},
		},
		{
			{title = LocalString.GetString("九十名之战")..timeStr1,  color = NGUIText.ParseColor24("ACF8FF", 0), showBg = false},
			{title = LocalString.GetString("十一十二名之战")..timeStr1,  color = NGUIText.ParseColor24("ACF8FF", 0), showBg = false},
			{title = LocalString.GetString("十三十四名之战")..timeStr1,  color = NGUIText.ParseColor24("ACF8FF", 0), showBg = false},
			{title = LocalString.GetString("十五十六名之战")..timeStr1,  color = NGUIText.ParseColor24("ACF8FF", 0), showBg = false},
		}
	}
	return tbl
end

function LuaGuildLeagueCrossMgr:IsQualifiedWatchPlayer()
	if self.m_QualifiedWatchIdTbl and CClientMainPlayer.Inst and self.m_QualifiedWatchIdTbl[CClientMainPlayer.Inst.Id] then
		return true
	else
		return false
	end
end

function LuaGuildLeagueCrossMgr:GetTrainEndTimeStr(format)
	local timestamp = self:GetTrainEndTime()
	local dtTime =  CServerTimeMgr.ConvertTimeStampToZone8Time(timestamp)
	return ToStringWrap(dtTime, format)
end

function LuaGuildLeagueCrossMgr:GetTrainEndTime()
	return CServerTimeMgr.Inst:GetTimeStampByStr(GuildLeagueCross_Setting.GetData().TrainCloseTime)
end

function LuaGuildLeagueCrossMgr:GetTrainItems(trainType)
	if not self.m_TrainType2ItemTbl then
		self.m_TrainType2ItemTbl = {}
		GuildLeagueCross_TrainSubmit.Foreach(function(key, data)
			if data.Status == 0 then
				if self.m_TrainType2ItemTbl[data.TrainType] == nil then
					self.m_TrainType2ItemTbl[data.TrainType] = {}
				end
				local templateId = (EquipmentTemplate_Equip.Exists(key) or Item_Item.Exists(key)) and key or math.floor(key/10)
				self.m_TrainType2ItemTbl[data.TrainType][templateId] = {["TrainValue"] = data.TrainValue, ["TrainScore"] = data.TrainScore, ["Priority"] = data.Priority, ["NeedBind"] = (data.NeedBind==1)}
			end
		end)
	end
	return self.m_TrainType2ItemTbl[trainType]
end
--培养活动排名奖励对应的四个阶段时间信息
function  LuaGuildLeagueCrossMgr:GetTrainRankAwardTimeInfo()
	local ret = {} -- 根据约定写死固定四组
	local roundCronIds = {1, 5, 7, 14} -- 小组赛第一轮开始，小组赛第三轮开始，排位赛第一轮开始，决赛结束
    for i=1, #roundCronIds do
        local cronStr = GuildLeagueCross_RoundCron.GetData(roundCronIds[i]).CronStr
        local minute,hour,day,month,year = string.match(cronStr, "(%d+)%s(%d+)%s(%d+)%s(%d+)%s%*%s(%d+)")
        local timeInfo = {}
        timeInfo.timeStamp = CServerTimeMgr.Inst:GetTimeStampByStr(SafeStringFormat3("%d-%d-%d %d:%d", year, month, day, hour, minute))
        timeInfo.timeStr = SafeStringFormat3(LocalString.GetString("%02d月%02d日%02d时%02d分"), month, day, hour,minute)
        table.insert(ret, timeInfo)
    end
	return ret
end
--培养活动距离下次奖励结算剩余时间
function  LuaGuildLeagueCrossMgr:GetNextTrainRankAwardTimeStr()
	local timeInfo = self:GetTrainRankAwardTimeInfo()
	local hour = 0
	local minute = 0
	for i=1,#timeInfo do
		if CServerTimeMgr.Inst.timeStamp<timeInfo[i].timeStamp then
			local timeDiff = timeInfo[i].timeStamp - CServerTimeMgr.Inst.timeStamp
			hour = math.floor(timeDiff /3600)
			minute = math.ceil(timeDiff % 3600 /60)
		end
	end
    if hour>=24 then
        return SafeStringFormat3(LocalString.GetString("%d天%d小时"), math.floor(hour/24),  math.floor(hour%24))
    else
	    return SafeStringFormat3(LocalString.GetString("%d小时%d分"), hour, minute)
    end
end

function LuaGuildLeagueCrossMgr:IsMyGuild(guildId)
	return guildId>0 and CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.GuildId == guildId or false
end

function LuaGuildLeagueCrossMgr:IsMyServer(serverId)
	return serverId>0 and CClientMainPlayer.Inst and CClientMainPlayer.Inst:GetMyServerId() == serverId or false
end

function LuaGuildLeagueCrossMgr:GetGuildLeagueRankStr(level, rank)
	return SafeStringFormat3(LocalString.GetString("%s第%s"), LuaGuildLeagueMgr.GetLevelStr(level), Extensions.ConvertToChineseString(rank))
end

function LuaGuildLeagueCrossMgr:IsOpenedOnMyServer()
	local myServerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst:GetMyServerId() or nil
	if not myServerId then return false end
	local myServerIsInList =  GuildLeagueCross_GuanWangServer.Exists(myServerId) or GuildLeagueCross_YingHeServer.Exists(myServerId)
	if not myServerIsInList then return false end
	return self:IsDuringOpenTime()
end

function LuaGuildLeagueCrossMgr:IsDuringOpenTime()
	local startTime = CServerTimeMgr.Inst:GetTimeStampByStr(GuildLeagueCross_Setting.GetData().StartTime)
	local endTime = CServerTimeMgr.Inst:GetTimeStampByStr(GuildLeagueCross_Setting.GetData().EndTime)
	local curTime = CServerTimeMgr.Inst.timeStamp
	return curTime>startTime and curTime<endTime
end

function LuaGuildLeagueCrossMgr:GetServerName(serverId)
	--这个接口有一定的风险，参见调用方法的说明
	return CommonDefs.GetServerNameByServerGroupId(serverId)
end

function LuaGuildLeagueCrossMgr:FormatTimestamp(timestamp, dtFormatStr)
	local dtTime =  CServerTimeMgr.ConvertTimeStampToZone8Time(timestamp)
	return ToStringWrap(dtTime, dtFormatStr)
end

function LuaGuildLeagueCrossMgr:CanShowScheduleAlert(alertType)
	if self.m_ScheduleRedAlertInfo and self.m_ScheduleRedAlertInfo[alertType]~=nil then
		return false
	else
		return true
	end
end

function LuaGuildLeagueCrossMgr:MarkScheduleAlertHidden(alertType)
	if not self.m_ScheduleRedAlertInfo then
		self.m_ScheduleRedAlertInfo = {}
	end
	if self.m_ScheduleRedAlertInfo[alertType]==nil then
		self.m_ScheduleRedAlertInfo[alertType] = true
		g_ScriptEvent:BroadcastInLua("OnGLCScheduleAlertVisibilityChanged")
	end
end

function LuaGuildLeagueCrossMgr:NeedShowTrainAlert()
	local scheduleInfo = LuaGuildLeagueCrossMgr.m_ScheduleInfo
    return scheduleInfo and self:CanShowScheduleAlert(EnumGuildLeagueCrossScheduleTempAlert.eTrainAlert) and scheduleInfo.isTrainOpen 
			and (CClientMainPlayer.Inst and CClientMainPlayer.Inst:IsInGuild()) or false
end

function LuaGuildLeagueCrossMgr:NeedShowForeignAidAlert()
	local scheduleInfo = LuaGuildLeagueCrossMgr.m_ScheduleInfo
    return scheduleInfo and scheduleInfo.hasAidManagePermission and scheduleInfo.hasAidApplication
end

function LuaGuildLeagueCrossMgr:NeedShowGambleAlert()
	local scheduleInfo = LuaGuildLeagueCrossMgr.m_ScheduleInfo
	if scheduleInfo then
		if scheduleInfo.hasReward then return true end
		if scheduleInfo.openGambleTypeTbl and self:CanShowScheduleAlert(EnumGuildLeagueCrossScheduleTempAlert.eGambleAlert) then
			for gambleType,isOpen in pairs(scheduleInfo.openGambleTypeTbl) do
				if isOpen then return true end
			end
		end
	end
	return false
end

function LuaGuildLeagueCrossMgr:NeedShowZhanLingAlert()
	local NThMatch = GuildLeagueCross_Setting.GetData().NThMatch
	local settingVal = CLuaPlayerSettings.Get("GLC_ZhanLing_Alert", 0)
	if settingVal and settingVal == NThMatch then
		return false
	else
		return true
	end
end

function LuaGuildLeagueCrossMgr:MarkZhanLingAlertHidden()
	CLuaPlayerSettings.Set("GLC_ZhanLing_Alert", GuildLeagueCross_Setting.GetData().NThMatch)
end

-------------
----RPC回调
-------------

function LuaGuildLeagueCrossMgr:QueryForOpenGLCWndResult(info_U, bTrainOpen, betInfo_U)
	local t = g_MessagePack.unpack(info_U)
	local infoTbl = {}
	if t and #t>0 then
		infoTbl.isQualifiedServer = t[1] -- bool 本服是否有有资格
		infoTbl.isPlayStart = t[2]  -- bool 活动是否进行中
		infoTbl.round = tonumber(t[3])
		infoTbl.playStatus = tonumber(t[4])
		infoTbl.champtionServerId = tonumber(t[5]) --冠军服务器ID
		infoTbl.champtionServerName = tostring(t[6]) --冠军服务器名

		local myServerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst:GetMyServerId() or 0
		local myServerRank = 0
		local levelRankInfo = {} --跨服最终排名   key {level}  value {index, serverId}
		for i=1, #t[7] do
			local rankInfo = {}
			local rankList = t[7][i]
			for j=1,#rankList do
				local _serverId = tonumber(rankList[j])
				table.insert(rankInfo, {index = j, serverId = _serverId, serverName = self:GetServerName(_serverId)})
				if myServerId == _serverId then
					myServerRank = j
				end
			end
			table.insert(levelRankInfo, rankInfo)
		end
		infoTbl.levelRankHistoryIndex = GuildLeagueCross_Setting.GetData().NThMatch --当前一届
		infoTbl.levelRankInfo = levelRankInfo
		infoTbl.openGambleTypeTbl = t[8]
		infoTbl.isGuildHasQualification = t[9] --是否是参赛帮会
		infoTbl.hasAidManagePermission = t[10] --是否有权限管理外援
		infoTbl.hasAidApplication = t[11] --是否有外援申请

		infoTbl.isTrainOpen = bTrainOpen
		-- {serverId, silver, os.time(), false}
		local hasReward = false
		if infoTbl.champtionServerId>0 then
			local betInfoTbl = g_MessagePack.unpack(betInfo_U)
			for gambleType,betTbl in pairs(betInfoTbl) do
				local isRankGamble = self:IsRankGamble(gambleType)
				for __, data in pairs(betTbl) do
					local serverId = data[1]
					local rewarded = data[4]
					local betSuccess = false
					if isRankGamble then
						betSuccess = serverId == myServerRank
					else
						betSuccess = serverId == infoTbl.champtionServerId
					end
					if betSuccess and (not rewarded) then
						hasReward = true
						break
					end
				end
			end
		end
		infoTbl.hasReward = hasReward
		self.m_ScheduleInfo = infoTbl
	end
	
	g_ScriptEvent:BroadcastInLua("OnQueryForOpenGLCWndResult")
end

function LuaGuildLeagueCrossMgr:QueryGLCJoinCrossGuildInfoResult(guildInfo_U)
	local guildInfoList = MsgPackImpl.unpack(guildInfo_U)
	local guildTbl = {}
	if guildInfoList and guildInfoList.Count>0 then
		for i=0, guildInfoList.Count-1 do
			local guildInfo = guildInfoList[i]
			table.insert(guildTbl, {guildId = tonumber(guildInfo[0]), guildName = tostring(guildInfo[1]), rankStr = self:GetGuildLeagueRankStr(tonumber(guildInfo[2]), tonumber(guildInfo[3]))})
		end

		g_ScriptEvent:BroadcastInLua("OnQueryGLCJoinCrossGuildInfoSuccess", guildTbl)
	end
end

function LuaGuildLeagueCrossMgr:QueryGLCGroupMatchInfoResult(belongGroup, info_U)
	--table.insert(ret[group][round], {matchIndex, serverId1, serverName1, serverId2, serverName2, hasInfo, winServerId, score1, score2})
	local infoList = MsgPackImpl.unpack(info_U)
	local groupMatchTbl = {}
	for i=0, infoList.Count-1 do
		local roundList = infoList[i]
		local roundTbl = {}
		for j=0, roundList.Count-1 do
			local matchList = roundList[j]
			local matchTbl = {}
			for k=0,matchList.Count-1 do
				local list = matchList[k]
				local matchData = {
					matchIndex = tonumber(list[0]),
					serverId1 = tonumber(list[1]),
					serverName1 = tostring(list[2]),
					serverId2 = tonumber(list[3]),
					serverName2 = tostring(list[4]),
					hasInfo = list[5] and tonumber(list[5])>0 or false,
					winServerId = tonumber(list[6]),
					score1 = tonumber(list[7]),
					score2 = tonumber(list[8]),
				}
				table.insert(matchTbl, matchData)
			end
			table.insert(roundTbl, matchTbl)
		end
		table.insert(groupMatchTbl, roundTbl)
	end
	g_ScriptEvent:BroadcastInLua("QueryGLCGroupMatchInfoResult", belongGroup, groupMatchTbl)
end

function LuaGuildLeagueCrossMgr:QueryGLCRankMatchInfoResult(info_U)
	--table.insert(ret[round], {matchIndex, index1, onGoing, serverId1, serverId2, serverName1, serverName2, score1, score2, winServerId})
	local infoDict = MsgPackImpl.unpack(info_U)
	local infoTbl = self:GetDefultRankMatchInfo()
	if infoDict.Count>0 then
		CommonDefs.DictIterate(infoDict, DelegateFactory.Action_object_object(function(key,val)
			local round = tonumber(key)
			local list = val
			for i=0, list.Count-1 do
				local data = {
					matchIndex = list[i][0],
					index1 = list[i][1],
					onGoing = list[i][2]>0,
					serverId1 = list[i][3],
					serverId2 = list[i][4],
					serverName1 = tostring(list[i][5]),
					serverName2 = tostring(list[i][6]),
					score1 = list[i][7],
					score2 = list[i][8],
					winServerId = list[i][9],
				}
				
				if round == EnumGuildLeagueCrossRound.eRankRound1 then
					if i<4 then
						infoTbl[1][1][i+1] = data
					else
						infoTbl[2][1][i-4+1] = data
					end
				elseif round == EnumGuildLeagueCrossRound.eRankRound2 then
					if i<4 then
						infoTbl[1][2][i+1] = data
					else
						infoTbl[2][2][i-4+1] = data
					end

				elseif round == EnumGuildLeagueCrossRound.eRankRound3 then
					if i<3 then
						infoTbl[1][3][i+1] = data
					else
						infoTbl[2][3][i-3+1] = data
					end

				elseif round == EnumGuildLeagueCrossRound.eRankRound4 then
					infoTbl[1][3][4] = data
				end
			end
		end))
		
	end
	g_ScriptEvent:BroadcastInLua("QueryGLCRankMatchInfoResultSuccess", infoTbl)
end

function LuaGuildLeagueCrossMgr:QueryGLCGroupScoreInfoResult(scoreInfo_U)
	--table.insert(ret[group], {index, serverId, self:GetServerName(serverId), self.m_GroupScoreInfo[serverId].winCount, self.m_GroupScoreInfo[serverId].score})
	local infoList = MsgPackImpl.unpack(scoreInfo_U)
	local scoreInfoTbl = {}
	for i=0, infoList.Count-1 do
		local rankList = infoList[i]
		local rankTbl = {}
		for j=0, rankList.Count-1 do
			local list = rankList[j]
			local rankData = {
				index = tonumber(list[0]),
				serverId = tonumber(list[1]),
				serverName = tostring(list[2]),
				winCount = tonumber(list[3]),
				score = tonumber(list[4]),
			}
			table.insert(rankTbl, rankData)
		end
		table.insert(scoreInfoTbl, rankTbl)
	end
	g_ScriptEvent:BroadcastInLua("QueryGLCGroupScoreInfoResultSuccess", scoreInfoTbl)
end

function LuaGuildLeagueCrossMgr:QueryGLCServerMatchInfoResult(info_U ,buffInfo_U)
	local infoTbl = g_MessagePack.unpack(info_U)
	self.m_ServerMatchDetailInfo = {
		matchIndex = infoTbl[1], --主要是用于验证
		ownServerId = infoTbl[2],
		serverId1 = infoTbl[3],
		serverName1 = tostring(infoTbl[4]),
		serverId2 = infoTbl[5],
		serverName2 = tostring(infoTbl[6]),
		winServerId = infoTbl[7],
		score1 = infoTbl[8],
		score2 = infoTbl[9],
		["battleInfo"] = {},
		["trainAdditionInfo"] = {},
	}
	for i=10,#infoTbl do
		local tbl = infoTbl[i]
		table.insert(self.m_ServerMatchDetailInfo.battleInfo, {
			guildId1 = tbl[1],
			guildName1 = tostring(tbl[2]),
			guildId2 = tbl[3],
			guildName2 = tostring(tbl[4]),
			winSide = tbl[5],
			score = tbl[6],
			rank1 = tbl[7],
			rank2 = tbl[8],
		})
	end
	local buffInfoTbl = g_MessagePack.unpack(buffInfo_U)
	if #buffInfoTbl~=2 then
		print("QueryGLCServerMatchInfoResult buffInfo_U data invalid")
	else
		local tbl = {}
		for i=1,#buffInfoTbl do
			table.insert(tbl, self:ParseServerTrainBuffInfo(buffInfoTbl[i]))
		end
		self.m_ServerMatchDetailInfo.trainAdditionInfo = tbl
	end
	g_ScriptEvent:BroadcastInLua("OnQueryGLCServerMatchInfoResultSuccess")
end

--培养活动的查询洪磊改了返回数据格式，客户端对应调整一下，解析方式参考了LuaGuildLeagueCrossMgr:ParseServerTrainBuffInfo以及服务器的CGuildLeagueCrossMgr:UpdateGuildInfoToHostServer()
function LuaGuildLeagueCrossMgr:ParseServerTrainBuffInfoForTrainWnd(trainType2Level, buffTaskType2Level)
	local ret = {}
    --没有数据时伪造一个空数据格式，数据都为负数
    if trainType2Level==nil or #trainType2Level==0 then
        for j=1,3 do
            ret[j] = {}
            ret[j].level = -1
            ret[j].effect = -1
        end
        ret[4] = {}
        ret[4].level = -1
        ret[4].effect = -1
        ret[5] = {}
        ret[5].level = -1
        ret[5].effect = -1
        return ret
    end

	--train info
	for j=1,3 do
		ret[j] = {}
		local level = trainType2Level[j]%100
		ret[j].level = level
		local suffix = tostring(j)
		ret[j].effect = GuildLeagueCross_TrainLevel.GetData(level)["EffectValue"..suffix]
	end
	--buff task info
	ret[4] = {}
    local level = buffTaskType2Level[1]%100
	ret[4].level = level
	ret[4].effect = GuildLeagueCross_BuffTaskLevel.GetData(level).EffectValue
	ret[5] = {}
    local level = buffTaskType2Level[2]%100
	ret[5].level = level
	ret[5].effect = GuildLeagueCross_BuffTaskLevel.GetData(level).AddPlayBuffNum
	return ret
end

function LuaGuildLeagueCrossMgr:ParseServerTrainBuffInfo(trainBuffInfo)
	--trainBuffInfo是个list，长度为6，前三个是train的buffId,第四个是增益任务1的buffId，第五个是第二个增益任务2的addNum值，第六个是增益任务2的等级，洪磊定的规则
	local ret = {}
    --没有数据时伪造一个空数据格式，数据都为负数
    if trainBuffInfo==nil or #trainBuffInfo==0 then
        for j=1,3 do
            ret[j] = {}
            ret[j].level = -1
            ret[j].effect = -1
        end
        ret[4] = {}
        ret[4].level = -1
        ret[4].effect = -1
        ret[5] = {}
        ret[5].level = -1
        ret[5].effect = -1
        return ret
    end

	--train info
	for j=1,3 do
		ret[j] = {}
		local level = trainBuffInfo[j]%100
		ret[j].level = level
		local suffix = tostring(j)
		ret[j].effect = GuildLeagueCross_TrainLevel.GetData(level)["EffectValue"..suffix]
	end
	--buff task info
	local level = trainBuffInfo[4]%100
	ret[4] = {}
	ret[4].level = level
	ret[4].effect = GuildLeagueCross_BuffTaskLevel.GetData(level).EffectValue
	ret[5] = {}
	ret[5].level = trainBuffInfo[6]
	ret[5].effect = trainBuffInfo[5]
	return ret
end

function LuaGuildLeagueCrossMgr:QueryGLCServerGuildInfoResult(info_U, context)
	local infoList = MsgPackImpl.unpack(info_U)
	-- TODO 这里应该检查一下severId
	if not self.m_ServerGuildInfo  or not self.m_ServerGuildInfo.serverId then return end
	self.m_ServerGuildInfo.detailInfo = {}
	for i=0,infoList.Count-1 do
		--table.insert(ret, {level, rank, guildName, leaderName, leaderClass, memberNum, maxSize, equipScore})
		local list = infoList[i]
		table.insert(self.m_ServerGuildInfo.detailInfo, {
			rankStr = self:GetGuildLeagueRankStr(list[0], list[1]),
			guildName = tostring(list[2]),
			ownerName = tostring(list[3]),
			professionIcon = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), list[4])),
			memberCount = list[5],
			maxMemberCount = list[6],
			score = list[7],
		})
	end
	if context == EnumQueryGLCServerGuildInfoContext.eDefault then
		CUIManager.ShowUI("GuildLeagueCrossServerGuildInfoWnd")
	elseif context == EnumQueryGLCServerGuildInfoContext.eGamble then
		CUIManager.ShowUI("GuildLeagueCrossGambleServerInfoWnd")
	end
end

function LuaGuildLeagueCrossMgr:SendGuildLeagueCrossServerMatchRecord(serverId, record_U)
	local t = g_MessagePack.unpack(record_U)
	local recordTbl = {}
	for i, data in pairs(t) do
		table.insert(recordTbl, {
			matchIndex = data[1], round = data[2], serverId1 = data[3], serverName1 = data[4], serverScore1 = data[5],
			serverId2 = data[6], serverName2 = data[7], serverScore2 = data[8], winServerId = data[9], 
		})
	end
	g_ScriptEvent:BroadcastInLua("OnSendGuildLeagueCrossServerMatchRecord", recordTbl)
end

function LuaGuildLeagueCrossMgr:QueryGLCGuildPosInfoResult(ownGuilId, exchangeRequest_U, guildInfo_U)
	local requestInfoList = MsgPackImpl.unpack(exchangeRequest_U)
	local guildInfoList = MsgPackImpl.unpack(guildInfo_U)

	local requestInfoTbl = {}
	if requestInfoList then
		for i=0,requestInfoList.Count-1 do
			local data = requestInfoList[i]
			local info = {}
			info.guildId = tonumber(data[0])
			info.guildName = tostring(data[1])
			--level & rank
			info.rankStr = self:GetGuildLeagueRankStr(tonumber(data[2]), tonumber(data[3]))
			info.index = tonumber(data[4])
			table.insert(requestInfoTbl, info)
		end
	end

	local guildInfoTbl = {}
	if guildInfoList then
		for i=0,guildInfoList.Count-1 do
			local data = guildInfoList[i]
			local info = {}
			info.guildId = tonumber(data[0])
			info.guildName = tostring(data[1])
			info.leaderId = tonumber(data[2])
			info.leaderName = tostring(data[3])
			--level & rank
			info.rankStr = self:GetGuildLeagueRankStr(tonumber(data[4]), tonumber(data[5]))
			info.equipScore = tonumber(data[6])
			info.hasApply = data[7] -- boolean value
			table.insert(guildInfoTbl, info)
		end
	end
	LuaGuildLeagueCrossMgr:ShowGuildPosExchangeWnd(requestInfoTbl, guildInfoTbl)
	g_ScriptEvent:BroadcastInLua("OnQueryGLCGuildPosInfoResult")
end

function LuaGuildLeagueCrossMgr:RequestGLCExchangeGuildPosSuccess(destGuildId)
	local guildInfoTbl = self.m_GuildPosExchangeInfo.guildInfoTbl
	if guildInfoTbl then
		for __,info in pairs(guildInfoTbl) do
			info.hasApply = (info.guildId == destGuildId)
		end
		g_ScriptEvent:BroadcastInLua("OnRequestGLCExchangeGuildPosSuccess")
	end
end

function LuaGuildLeagueCrossMgr:SendGuildLeagueCrossMatchBuffInfo(info_U)
	local t = g_MessagePack.unpack(info_U)
	local server1Id, server2Id, server1Name, server2Name, trainType2Level1, buffTaskType2Level1, trainType2Level2, buffTaskType2Level2 = t[1], t[2], t[3], t[4], t[5], t[6], t[7], t[8]
	if server1Id==0 or trainType2Level1==nil or #trainType2Level1==0 then --允许server2Id为0，此时没有对手服
		print("SendGuildLeagueCrossMatchBuffInfo data info invalid")
		return
	end

	local buffInfo1 = self:ParseServerTrainBuffInfoForTrainWnd(trainType2Level1, buffTaskType2Level1)
	local buffInfo2 = self:ParseServerTrainBuffInfoForTrainWnd(trainType2Level2, buffTaskType2Level2)

	LuaGuildLeagueCrossMgr:ShowTrainAdditionWnd(server1Name, server2Name, {buffInfo1, buffInfo2})
end

function LuaGuildLeagueCrossMgr:SubmitItemToGuildLeagueTrainSuccess(ret_u)
	--local ret = {trainType, curRank, curScore, rankMinScoce, value, totalScore, g_GuildLeagueCrossMgr.m_TrainType2Level[trainType], g_GuildLeagueCrossMgr.m_TrainType2Value[trainType]}
	local list = MsgPackImpl.unpack(ret_u)
	if list and self.m_TrainInfo and not self.m_TrainInfo.isBuffTask then
		local trainType, curRank, curScore, rankMinScore, value, totalScore, level, progress = list[0], list[1], list[2], list[3], list[4], list[5], list[6], list[7]

		self.m_TrainInfo.curScore = curScore
		self.m_TrainInfo.totalScore = totalScore
		self.m_TrainInfo.rankMinScore = rankMinScore
		self.m_TrainInfo.playerRank = curRank
		self.m_TrainInfo.levelInfo[trainType] = level
		self.m_TrainInfo.progressInfo[trainType] = progress
		if list.Count >= 9 then
			self.m_TrainInfo.dailyTrainScore =  tonumber(list[8])
		end
		g_ScriptEvent:BroadcastInLua("OnSubmitItemToGuildLeagueTrainSuccess", trainType)
	end
end

function LuaGuildLeagueCrossMgr:QueryGuildLeagueTrainRankResult(rankInfo_U, guildName, totalScore)
	-- playerId, score, class, playerName, guildName
	local mainPlayerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
	local rankInfoList = MsgPackImpl.unpack(rankInfo_U)
	local rankInfoTbl = {}
	if rankInfoList and rankInfoList.Count>0 then
		local hasMainPlayerData = false
		for i=0, rankInfoList.Count-1 do
			local rankInfo = rankInfoList[i]
			local data = {
				rank=i+1,
				playerId = tonumber(rankInfo[0]), 
				score = tostring(rankInfo[1]), 
				professionIcon = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass),tonumber(rankInfo[2]))), 
				playerName = tostring(rankInfo[3]), 
				guildName = tostring(rankInfo[4])}
			data.isMyGuild = data.playerId == mainPlayerId
			if data.isMyGuild then
				table.insert(rankInfoTbl, 1, data)
			else
				table.insert(rankInfoTbl, data)
			end
			if data.isMyGuild then
				hasMainPlayerData = true
			end
		end
		--将主角未上榜数据塞在最前面
		if not hasMainPlayerData then
			local data = {
				rank=-1,
				playerId = mainPlayerId, 
				score = tostring(totalScore), 
				professionIcon = Profession.GetIcon(CClientMainPlayer.Inst.Class), 
				playerName = tostring(CClientMainPlayer.Inst.Name), 
				guildName = tostring(guildName),
				isMyGuild = true
			}
			table.insert(rankInfoTbl, 1, data)
		end
		g_ScriptEvent:BroadcastInLua("OnQueryGuildLeagueTrainRankResult", rankInfoTbl)
	end
end

---------观战
function LuaGuildLeagueCrossMgr:GetRoundInfoByMatchIndex(matchIndex)
	if not matchIndex then return 0,0,0 end
	--根据服务器端的CGuildLeagueCrossMgr:BuildMatchIndex(level, group, round, index1, index2)反算比赛轮次
	local round = math.floor(matchIndex %100000 /10000)
	local index1 = math.floor(matchIndex %10000 /100)
	local index2 = math.floor(matchIndex %100)
	return round, index1, index2
end

function LuaGuildLeagueCrossMgr:InitGuildLeagueWatchHeadWndInfo(matchIndex, info_U)
	--force guildname servername
	local infoList = MsgPackImpl.unpack(info_U)
	if infoList and infoList.Count == 2 then
		self.m_WatchInfo = {}
		--根据服务器端的CGuildLeagueCrossMgr:BuildMatchIndex(level, group, round, index1, index2)反算比赛轮次
		local round, index1, index2 = self:GetRoundInfoByMatchIndex(matchIndex)
		self.m_WatchInfo.round = round
		self.m_WatchInfo.index1 = index1
		self.m_WatchInfo.index2 = index2
		self.m_WatchInfo.forceInfo = {}
		self.m_WatchInfo.forceInfo[tonumber(infoList[0][0])] = {guildName = tostring(infoList[0][1]), serverName = tostring(infoList[0][2])}
		self.m_WatchInfo.forceInfo[tonumber(infoList[1][0])] = {guildName = tostring(infoList[1][1]), serverName = tostring(infoList[1][2])}
		CUIManager.ShowUI("GuildLeagueCrossWatchWnd")
		CUIManager.CloseUI("ScheduleWnd")
	end
end

LuaGuildLeagueCrossMgr.m_defendTowerNum = 0
LuaGuildLeagueCrossMgr.m_attackTowerNum = 0
function LuaGuildLeagueCrossMgr:SyncGuildLeagueTowerCountToWatcher(defendTowerNum, attackTowerNum)
	LuaGuildLeagueCrossMgr.m_defendTowerNum = defendTowerNum
	LuaGuildLeagueCrossMgr.m_attackTowerNum = attackTowerNum
	g_ScriptEvent:BroadcastInLua("OnSyncGuildLeagueTowerCountToWatcher", defendTowerNum, attackTowerNum)
end

function LuaGuildLeagueCrossMgr:SyncGuildLeagueBossHpInfoToWatcher(info_U)
	-- force, monster:GetTemplateId(), monster:GetHp(), monster:GetHpFull()
	local monsterInfoList =  MsgPackImpl.unpack(info_U)
	local monsterInfoTbl = {}
	if monsterInfoList and monsterInfoList.Count>0 then
		for i=0, monsterInfoList.Count-1 do
			local force = tonumber(monsterInfoList[i][0])
			if monsterInfoTbl[force] == nil then
				monsterInfoTbl[force] = {}
			end
			table.insert(monsterInfoTbl[force], {
				templateId = tonumber(monsterInfoList[i][1]),
				hp = tonumber(monsterInfoList[i][2]),
				hpFull = tonumber(monsterInfoList[i][3]),
			})
		end
		for __, data in pairs(monsterInfoTbl) do
			table.sort(monsterInfoTbl, function(a,b)
				return a.templateId<b.templateId
			end)
		end
	end
	g_ScriptEvent:BroadcastInLua("OnSyncGuildLeagueBossHpInfoToWatcher", monsterInfoTbl)
end

function LuaGuildLeagueCrossMgr:QueryGuildLeaguePlayerPosResult(posInfo_U)
	local posList =  MsgPackImpl.unpack(posInfo_U)
	local posInfoTbl = {}
	if posList and posList.Count%5==0 then
		for i=0,posList.Count-1,5 do
			table.insert(posInfoTbl, {
				playerId = tonumber(posList[i]),
				engineId = tonumber(posList[i+1]),
				posX = tonumber(posList[i+2]),
				posY = tonumber(posList[i+3]),
				force = tonumber(posList[i+4]),
			})
		end
	end
	g_ScriptEvent:BroadcastInLua("OnQueryGuildLeaguePlayerPosResult", posInfoTbl)
end

LuaGuildLeagueCrossMgr.m_defendKillNum = 0
LuaGuildLeagueCrossMgr.m_attackKillNum = 0

function LuaGuildLeagueCrossMgr:SyncGuildLeagueKillNumToWatcher(defendKillNum, attackKillNum)
	self.m_defendKillNum = defendKillNum
	self.m_attackKillNum = attackKillNum
	g_ScriptEvent:BroadcastInLua("SyncGuildLeagueKillNumToWatcher", defendKillNum, attackKillNum)
end

function LuaGuildLeagueCrossMgr:SendMsgToGuildLeagueWatcher(time, msg, context_U, msg2)
	local t = g_MessagePack.unpack(context_U)
	local playerClass, playerGender, monsterTemplateId = t[1], t[2], t[3]
	msg = g_MessageMgr:Format(msg)
	msg2 = g_MessageMgr:Format(msg2)
	g_ScriptEvent:BroadcastInLua("OnSendMsgToGuildLeagueWatcher", time, msg, msg2, playerClass, playerGender, monsterTemplateId)
end

function LuaGuildLeagueCrossMgr:SyncPlayerHpToWatcher(hpInfo_U)
	local t = g_MessagePack.unpack(hpInfo_U)
	for i=1,#t,3 do
		local clientObj = CClientObjectMgr.Inst:GetObject(t[i])
        if clientObj then
            clientObj.HpFull = t[i+2]
            clientObj.Hp = t[i+1]
			EventManager.BroadcastInternalForLua(EnumEventType.HpUpdate,{ clientObj.EngineId })
		end
	end
end

function LuaGuildLeagueCrossMgr:SendGuildLeagueCrossExchangePosRecord(record_U)
	--[[local record = {
		os.time(),
		srcGuildId,
		g_GuildMgr:GetGuildName(srcGuildId),
		self.m_JoinCrossGuildBasicInfo[srcGuildId][1],
		srcIndex,
		destGuildId,
		g_GuildMgr:GetGuildName(destGuildId),
		self.m_JoinCrossGuildBasicInfo[destGuildId][1],
		destIndex,
	}]]
	local t = g_MessagePack.unpack(record_U)
	local recordTbl = {}
	for i, data in pairs(t) do
		table.insert(recordTbl, {
			idx = i, time = data[1],
			srcGuildId = data[2], srcGuildName = data[3], srcRank= data[4], srcIndex = data[5], 
			dstGuildId = data[6], dstGuildName = data[7], dstRank= data[8], dstIndex = data[9], 
		})
	end

	table.sort(recordTbl, function(a, b)
		if a.time~=b.time then return
			a.time>b.time
		else
			return a.idx<b.idx
		end
	end)
	g_ScriptEvent:BroadcastInLua("OnSendGuildLeagueCrossExchangePosRecord", recordTbl)
end

function LuaGuildLeagueCrossMgr:SendGuildLeagueCrossHistoryRankInfo(index, rankInfo_U)
	local t = g_MessagePack.unpack(rankInfo_U)
	local levelRankInfo = {} --跨服最终排名   key {level}  value {index, serverId}
	for i=1, #t do
		local rankInfo = {}
		local rankList = t[i]
		for j=1,#rankList do
			local _serverId = tonumber(rankList[j])
			table.insert(rankInfo, {index = j, serverId = _serverId, serverName = self:GetServerName(_serverId)})
		end
		table.insert(levelRankInfo, rankInfo)
	end
	if not self.m_ScheduleInfo then
		self.m_ScheduleInfo = {}
	end
	self.m_ScheduleInfo.levelRankHistoryIndex = index>0 and index or GuildLeagueCross_Setting.GetData().NThMatch
	self.m_ScheduleInfo.levelRankInfo = levelRankInfo
	self:ShowResultWnd()
end

function LuaGuildLeagueCrossMgr:ParseGambleInfoAndBetRecord(gambleInfo_U, betRecord_U)
	--self.m_Level2GambleInfo[level][gambleType][serverId] ={self:GetServerName(serverId), g_GuildLeagueCrossMgr.m_GambleServerInitSilver, g_GuildLeagueCrossMgr.m_GambleInitOdds, rank, group
	--betInfo table.insert(betInfo, {serverId, betSilver, os.time(), false, gambleType})
	local gambleInfoTbl = g_MessagePack.unpack(gambleInfo_U)
	local betInfoTbl = g_MessagePack.unpack(betRecord_U)
	local gambleTbl = {}
	local betRecordTbl = {}
	for gambleType, servers in pairs(gambleInfoTbl) do
		gambleTbl[gambleType] = {}
		if self:IsRankGamble(gambleType) then
			for playerServerId, info in pairs(servers) do
				--这里只要处理自己服的数据就行了，其他服的都跳过
				if CClientMainPlayer.Inst and CClientMainPlayer.Inst:GetMyServerId()==playerServerId then
					local serverName, silverTbl, oddsTbl, rank, group, betResult = info[1], info[2], info[3], info[4], info[5], info[6]
					local startIndex = 1
					local endIndex = #silverTbl
					--这里有个特殊处理：本服排名竞猜的第三场，根据自己服是前八名还是后八名，只能竞猜一半的内容，因此下面对第三场做过滤
					if gambleType == EnumGuildLeagueCrossGambleType.eRank3 then
						local playerServerBeforeEight = rank<=8
						startIndex = playerServerBeforeEight and 1 or 9
						endIndex = playerServerBeforeEight and 8 or #silverTbl
					end
					for i=startIndex,endIndex do
						table.insert(gambleTbl[gambleType], { serverId = playerServerId, serverName = serverName, silver = silverTbl[i], odds = oddsTbl[i], rank = rank, group = group, aliasName = self:GetRankGambleChoiceName(i), choiceRank = i, betResult = betResult })
					end
				end
			end
			--排名竞猜按排名顺序显示,无需再排序
		else
			for serverId, info in pairs(servers) do 
				table.insert(gambleTbl[gambleType], { serverId = serverId, serverName = info[1], silver = info[2], odds = info[3], rank = info[4], group = info[5] })
			end
			--冠军竞猜排序规则：赔率越低越靠前、其次服务器分组越小越靠前、再次服务器排名越大越靠前、最后服务器ID越小越靠前
			table.sort(gambleTbl[gambleType], function(a, b)
				if a.odds~=b.odds then
					return a.odds<b.odds
				elseif a.group~=b.group then
					return a.group<b.group
				elseif a.rank~=b.rank then
					return a.rank<b.rank
				else
					return a.serverId<b.serverId
				end
			end)
		end
	end
	for __, tbl in pairs(betInfoTbl) do
		for __,data in pairs(tbl) do
			--当gambleType为rank时，serverId对应表示竞猜的排名信息
			local choiceRankOrServerId, betSilver, time, rewarded, gambleType = data[1], data[2], data[3], data[4], data[5]
			if self:IsRankGamble(gambleType) then
				local records = gambleTbl[gambleType] --从gambleTbl读取，更方便

				local serverName = nil
				local odds = nil
				local totalSilver = 0 --奖池金额
				for __, record in pairs(records) do
					totalSilver = totalSilver + record.silver
					if choiceRankOrServerId == record.choiceRank then
						serverName = record.serverName
						odds = record.odds
					end
				end
				table.insert(betRecordTbl, { gambleType = gambleType, serverId = choiceRankOrServerId, silver = betSilver, time = time,  rewarded = rewarded, serverName = serverName, totalSilver=totalSilver, odds=odds })
			else
				local servers = gambleInfoTbl[gambleType]
				local serverInfo = servers[choiceRankOrServerId]
				local serverName = serverInfo[1]
				local odds = serverInfo[3]

				local totalSilver = 0 --奖池金额
				for serverId, info in pairs(servers) do
					totalSilver = totalSilver + info[2]
				end
				table.insert(betRecordTbl, { gambleType = gambleType, serverId = choiceRankOrServerId, silver = betSilver, time = time,  rewarded = rewarded, serverName = serverName, totalSilver=totalSilver, odds=odds })
			end
		end
	end
	table.sort(betRecordTbl, function(a, b)
		if a.time~=b.time then
			return a.time>b.time
		else
			return a.serverId<b.serverId
		end
	end)
	
	return gambleTbl, betRecordTbl
end
--获取排名竞猜的结果
function LuaGuildLeagueCrossMgr:GetRankGambleResult(gambleType)
	if self.m_GambleInfo and self.m_GambleInfo.gambleTbl and self.m_GambleInfo.gambleTbl[gambleType] then
		return self.m_GambleInfo.gambleTbl[gambleType][1].betResult --取其中一个就行
	end
	return 0
end

function LuaGuildLeagueCrossMgr:GetOpenGamble(forRankGamble, openGambleTypeTbl)
	if not forRankGamble then
		for i=EnumGuildLeagueCrossGambleType.eFirst, EnumGuildLeagueCrossGambleType.eFourth do
			if openGambleTypeTbl[i] then
				return i
			end
		end
	else
		for i=EnumGuildLeagueCrossGambleType.eRank1, EnumGuildLeagueCrossGambleType.eRank3 do
			if openGambleTypeTbl[i] then
				return i
			end
		end
	end
	return 0
end

function LuaGuildLeagueCrossMgr:GetRankGambleChoiceName(choiceRank)
	if choiceRank == 1 then return LocalString.GetString("冠军") end
	if choiceRank == 2 then return LocalString.GetString("亚军") end
	if choiceRank == 3 then return LocalString.GetString("季军") end
	return SafeStringFormat3(LocalString.GetString("第%s名"), Extensions.ConvertToChineseString(choiceRank))
end

function LuaGuildLeagueCrossMgr:IsRankGamble(gambleType)
	return gambleType >= EnumGuildLeagueCrossGambleType.eRank1 and gambleType <= EnumGuildLeagueCrossGambleType.eRank3
end

--openGambleTypeTbl 开放的场次信息
--curType 当前场次
function LuaGuildLeagueCrossMgr:GetGambleTypeStatus(curType)
	local openGambleTypeTbl = self.m_GambleInfo.openGambleTypeTbl
	if not curType or openGambleTypeTbl==nil then return EnumGuildLeagueCrossGambleStatus.eNotStarted end
	if not self:IsRankGamble(curType) then
		--不正确的参数
		if curType<EnumGuildLeagueCrossGambleType.eFirst or curType>EnumGuildLeagueCrossGambleType.eFourth then
			return EnumGuildLeagueCrossGambleStatus.eNotStarted
		end
		--与已开启的场次进行比较
		for i=EnumGuildLeagueCrossGambleType.eFirst, EnumGuildLeagueCrossGambleType.eFourth do
			if openGambleTypeTbl[i] then
				if i==curType then
					return EnumGuildLeagueCrossGambleStatus.eOpen
				elseif i>curType then
					return EnumGuildLeagueCrossGambleStatus.eClosed
				else
					return EnumGuildLeagueCrossGambleStatus.eNotStarted
				end
			end
		end
	else
		--不正确的参数
		if curType<EnumGuildLeagueCrossGambleType.eRank1 or curType>EnumGuildLeagueCrossGambleType.eRank3 then
			return EnumGuildLeagueCrossGambleStatus.eNotStarted
		end
		--与已开启的场次进行比较
		for i=EnumGuildLeagueCrossGambleType.eRank1, EnumGuildLeagueCrossGambleType.eRank3 do
			if openGambleTypeTbl[i] then
				if i==curType then
					return EnumGuildLeagueCrossGambleStatus.eOpen
				elseif i>curType then
					return EnumGuildLeagueCrossGambleStatus.eClosed
				else
					return EnumGuildLeagueCrossGambleStatus.eNotStarted
				end
			end
		end
	end

	--没有场次开启的情况下额外判断
	local curTime = CServerTimeMgr.Inst.timeStamp
	if curTime>=CServerTimeMgr.Inst:GetTimeStampByStr(GuildLeagueCross_GambleCron.GetData(curType).CloseTime) then
		return EnumGuildLeagueCrossGambleStatus.eClosed
	elseif curTime<CServerTimeMgr.Inst:GetTimeStampByStr(GuildLeagueCross_GambleCron.GetData(curType).OpenTime) then
		return EnumGuildLeagueCrossGambleStatus.eNotStarted
	else
		return EnumGuildLeagueCrossGambleStatus.eOpen
	end
end

function LuaGuildLeagueCrossMgr:GetRemainingPortionForGamble(gambleType, gambleInfo)
    local isOpen = self:GetGambleTypeStatus(gambleType) == EnumGuildLeagueCrossGambleStatus.eOpen
    if isOpen then
        -- 计算份额是否已满
        local myInvestment = 0
        for __,record in pairs(gambleInfo.betRecordTbl) do
            if record.gambleType == gambleType  then
                myInvestment = myInvestment + record.silver
			end
		end
		return GuildLeagueCross_Setting.GetData().MaxbetSilverNum - myInvestment
	end
	return 0
end

function LuaGuildLeagueCrossMgr:SendGuildLeagueCrossGambleInfo(openGambleType_U, gambleInfo_U, championServerId, betRecord_U)
	--openGambleType在CGuildLeagueCrossMgr:OnGambleCloseCron时会设置为0，影响客户端的连续性判断，当openGambleType为0时，需要判断是未开始还是全部结束
	local tmp = g_MessagePack.unpack(openGambleType_U)
	local gambleTbl, betRecordTbl = self:ParseGambleInfoAndBetRecord(gambleInfo_U, betRecord_U)
	self.m_GambleInfo = {}
	self.m_GambleInfo.openGambleTypeTbl = tmp
	self.m_GambleInfo.gambleTbl = gambleTbl
	self.m_GambleInfo.championServerId = championServerId
	self.m_GambleInfo.betRecordTbl = betRecordTbl
	
	CUIManager.ShowUI("GuildLeagueCrossGambleWnd")

end

function LuaGuildLeagueCrossMgr:BetGuildLeagueCrossGambleFailed(gambleType, serverId, totalSilver, curDaiBiNum)
	g_MessageMgr:ShowMessage("GUILD_LEAGUE_CROSS_BET_FAILED") -- 服务器有条GUILD_LEAGUE_CROSS_BET_SUCCESS，命名保持一致
end

function LuaGuildLeagueCrossMgr:SendGuildLeagueCrossGambleRecord(championServerId, gambleInfo_U, betRecord_U)
	local gambleTbl, betRecordTbl = self:ParseGambleInfoAndBetRecord(gambleInfo_U, betRecord_U)
	self.m_GambleInfo.gambleTbl = gambleTbl
	self.m_GambleInfo.championServerId = championServerId
	self.m_GambleInfo.betRecordTbl = betRecordTbl
	
	g_ScriptEvent:BroadcastInLua("OnSendGuildLeagueCrossGambleRecord")
end

function LuaGuildLeagueCrossMgr:SendGuildLeagueCrossGambleOpenType(opengambleTypeTbl)
	--暂时无用
end

function LuaGuildLeagueCrossMgr:SendGuildLeagueCrossGambleResult(rewardSilver)
	self.m_GambleGetRewardResult = {}
	self.m_GambleGetRewardResult.rewardSilver = rewardSilver
	CUIManager.ShowUI("GuildLeagueCrossGambleRewardWnd")
	--领奖完毕再去查询一下，刷新界面红点及其他数据
	self:QueryGuildLeagueCrossGambleRecord()
	--领奖成功时，刷新一下活动页面的红点信息
	LuaGuildLeagueCrossMgr:QueryForOpenGLCWnd()
end

function LuaGuildLeagueCrossMgr:QueryGuildLeagueCrossAidEvaluteInfoResult(evaluteInfo_U)
	-- playerId, playerName, class, foreignAidType, levels
	local tmp = g_MessagePack.unpack(evaluteInfo_U)
	local evaluationInfo = {}
	for i, info in pairs(tmp) do
		table.insert(evaluationInfo, {index = i, playerId = info[1], playerName = info[2], playerClass = info[3], aidType = info[4], levels=info[5]})
	end
	--列表默认按照联赛评价进行排行，整体评价越高则显示越靠上
	table.sort(evaluationInfo, function(a, b)
		local sumOfA = 0
		for __,val in pairs(a.levels) do
			sumOfA = sumOfA + val
		end
		local sumOfB = 0
		for __,val in pairs(b.levels) do
			sumOfB = sumOfB + val
		end
		if sumOfA~=sumOfB then
			return sumOfA>sumOfB
		else
			return a.index<b.index
		end
	end)
	self.m_AidEvaluationInfo = evaluationInfo
	CUIManager.ShowUI("GuildLeagueCrossAidEvaluationWnd")
end
