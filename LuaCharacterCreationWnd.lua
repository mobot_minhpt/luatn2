local StreamingController = import "UnityEngine.StreamingController"
local CMainCamera = import "L10.Engine.CMainCamera"
local UIRoot=import "UIRoot"
local Application = import "UnityEngine.Application"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientVersion = import "L10.Engine.CClientVersion"
local CExistingPlayerInfo = import "L10.Game.CLoginMgr+CExistingPlayerInfo"
local CLoadingWnd = import "L10.UI.CLoadingWnd"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CObjectFX = import "L10.Game.CObjectFX"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local CPropertyAppearance = import "L10.Game.CPropertyAppearance"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local CRenderObject = import "L10.Engine.CRenderObject"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local DelegateFactory = import "DelegateFactory"
local EnumClass = import "L10.Game.EnumClass"
local EnumEventType = import "EnumEventType"
local EnumGender = import "L10.Game.EnumGender"
local EventDelegate = import "EventDelegate"
local EventManager = import "EventManager"
local Gac2Login = import "L10.Game.Gac2Login"
local GameObject = import "UnityEngine.GameObject"
local GestureRecognizer = import "L10.Engine.GestureRecognizer"
local GestureType = import "L10.Engine.GestureType"
local LayerDefine = import "L10.Engine.LayerDefine"
local LocalString = import "LocalString"
local LoginModelRoot = import "L10.UI.LoginModelRoot"
local MessageWndManager = import "L10.UI.MessageWndManager"
local NGUIText = import "NGUIText"
local PlayerSettings = import "L10.Game.PlayerSettings"
local Profession = import "L10.Game.Profession"
local Random = import "UnityEngine.Random"
local RuntimePlatform = import "UnityEngine.RuntimePlatform"
local Screen = import "UnityEngine.Screen"
local SoundManager = import "SoundManager"
local System = import "System"
local UIEventListener = import "UIEventListener"
local Vector3 = import "UnityEngine.Vector3"
local CProfessionListView = import "L10.UI.CProfessionListView"
local CButton = import "L10.UI.CButton"
local UIToggle = import "UIToggle"
local CCharacterHairCell = import "L10.UI.CCharacterHairCell"
local CExistingHeadPortrait = import "L10.UI.CExistingHeadPortrait"
local CUITexture = import "L10.UI.CUITexture"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local LuaTweenUtils = import "LuaTweenUtils"
local Ease = import "DG.Tweening.Ease"
local Setting = import "L10.Engine.Setting"
local CoreScene = import "L10.Engine.Scene.CoreScene"
local EShadowType = import "L10.Engine.EShadowType"
local Shader = import "UnityEngine.Shader"
local SystemInfo = import "UnityEngine.SystemInfo"
local EWeaponVisibleReason = import "L10.Engine.EWeaponVisibleReason"
local CIKMgr = import "L10.Game.CIKMgr"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local NativeTools = import "L10.Engine.NativeTools"
local DRPFMgr = import "L10.Game.DRPFMgr"
local CUIFx = import "L10.UI.CUIFx"
local TweenAlpha = import "TweenAlpha"
local CPropertySkill = import "L10.Game.CPropertySkill"
local CAppClient = import "L10.Engine.CAppClient"
local C3DTouchMgr = import "L10.UI.C3DTouchMgr"
local Enum3DTouchStatus = import "L10.UI.Enum3DTouchStatus"
local CDeepLinkMgr = import "L10.Game.CDeepLinkMgr"
local QnModelPreviewer = import "L10.UI.QnModelPreviewer"
local CAnimationMgr = import "L10.Game.CAnimationMgr"
local Animation_Animation = import "L10.Game.Animation_Animation"
local AvatarHelper = import "L10.Game.AvatarHelper"
LuaCharacterCreationWnd = class()

RegistClassMember(LuaCharacterCreationWnd, "m_BackButton")
RegistClassMember(LuaCharacterCreationWnd, "m_CreatingRoleRoot")
RegistClassMember(LuaCharacterCreationWnd, "m_ExistingRoleRoot")

--existing role
RegistClassMember(LuaCharacterCreationWnd, "m_EnterGameBtnForExistingRole")
RegistClassMember(LuaCharacterCreationWnd, "m_DeleteRoleBtn")
RegistClassMember(LuaCharacterCreationWnd, "m_ExistingPortraits")
RegistClassMember(LuaCharacterCreationWnd, "m_RoleNameLabel")
RegistClassMember(LuaCharacterCreationWnd, "m_RoleProfessionTexture")
RegistClassMember(LuaCharacterCreationWnd, "m_RoleLevelLabel")
RegistClassMember(LuaCharacterCreationWnd, "m_PreviousPageBtn")
RegistClassMember(LuaCharacterCreationWnd, "m_NextPageBtn")
RegistClassMember(LuaCharacterCreationWnd, "m_PageLabel")
RegistClassMember(LuaCharacterCreationWnd, "m_OpenTimeLabel")
RegistClassMember(LuaCharacterCreationWnd, "m_TeamAppointmentButton")
RegistClassMember(LuaCharacterCreationWnd, "m_OriginOpenTimeLabelPos")

--creating role
RegistClassMember(LuaCharacterCreationWnd, "m_CreateRoleMasterRoot")
RegistClassMember(LuaCharacterCreationWnd, "m_CreateRoleDetailRoot")
RegistClassMember(LuaCharacterCreationWnd, "m_MasterLeftOffset")
RegistClassMember(LuaCharacterCreationWnd, "m_MasterRightOffset")
RegistClassMember(LuaCharacterCreationWnd, "m_DetailLeftOffset")
RegistClassMember(LuaCharacterCreationWnd, "m_DetailRightOffset")

--creating role master left
RegistClassMember(LuaCharacterCreationWnd, "m_ProfessionListView")
RegistClassMember(LuaCharacterCreationWnd, "m_GenderTable")
RegistClassMember(LuaCharacterCreationWnd, "m_GenderCells")

--creating role master right
RegistClassMember(LuaCharacterCreationWnd, "m_MasterProfessionTexture")
RegistClassMember(LuaCharacterCreationWnd, "m_MasterFeatureTexture")
RegistClassMember(LuaCharacterCreationWnd, "m_DescTexture")
RegistClassMember(LuaCharacterCreationWnd, "m_RadarMapTexture")
RegistClassMember(LuaCharacterCreationWnd, "m_DetailButton")
RegistClassMember(LuaCharacterCreationWnd, "m_QYCodeUseButton")

RegistClassMember(LuaCharacterCreationWnd, "m_MasterProfessionAboveFx")
RegistClassMember(LuaCharacterCreationWnd, "m_MasterProfessionBelowFx")

--creating role detail left
RegistClassMember(LuaCharacterCreationWnd, "m_XianFanButtonRoot")
RegistClassMember(LuaCharacterCreationWnd, "m_XianShenButton")
RegistClassMember(LuaCharacterCreationWnd, "m_FanShenButton")

--creating role detail right
RegistClassMember(LuaCharacterCreationWnd, "m_DetailProfessionTexture")
RegistClassMember(LuaCharacterCreationWnd, "m_DetailFeatureTexture")
RegistClassMember(LuaCharacterCreationWnd, "m_RandomNameButton")
RegistClassMember(LuaCharacterCreationWnd, "m_EnterGameBtnForNewRole")
RegistClassMember(LuaCharacterCreationWnd, "m_NameInput")
RegistClassMember(LuaCharacterCreationWnd, "m_HairCells")
RegistClassMember(LuaCharacterCreationWnd, "m_UseTokenToggle")


RegistClassMember(LuaCharacterCreationWnd, "m_CurPageIndex")
RegistClassMember(LuaCharacterCreationWnd, "m_MaxCharactersPerPage")
RegistClassMember(LuaCharacterCreationWnd, "m_ModelRoot")
RegistClassMember(LuaCharacterCreationWnd, "m_CurClass")
RegistClassMember(LuaCharacterCreationWnd, "m_CurGender")
RegistClassMember(LuaCharacterCreationWnd, "m_CurHairId")
RegistClassMember(LuaCharacterCreationWnd, "m_CurEquipInfos")
RegistClassMember(LuaCharacterCreationWnd, "m_RO")
RegistClassMember(LuaCharacterCreationWnd, "m_ForceCreateNewRole")
RegistClassMember(LuaCharacterCreationWnd, "m_FxScale")
RegistClassMember(LuaCharacterCreationWnd, "m_LiangXiangFX")
RegistClassMember(LuaCharacterCreationWnd, "m_Sound")


RegistClassMember(LuaCharacterCreationWnd, "m_Binded")

RegistClassMember(LuaCharacterCreationWnd, "m_CameraScript")

RegistClassMember(LuaCharacterCreationWnd, "m_SwipeGestureListener")
RegistClassMember(LuaCharacterCreationWnd, "m_EnterGameForExistingRoleTick")
RegistClassMember(LuaCharacterCreationWnd, "m_EnterGameForCreateRoleTick")
RegistClassMember(LuaCharacterCreationWnd, "m_XianShenSelected")
RegistClassMember(LuaCharacterCreationWnd, "m_NeedPlayXianFanTransform")

RegistClassMember(LuaCharacterCreationWnd, "m_PrepareAniTick")

RegistClassMember(LuaCharacterCreationWnd, "m_YingLingRadarMapTick")
RegistClassMember(LuaCharacterCreationWnd, "m_AppearanceData")

function LuaCharacterCreationWnd:BindComponentsAndInitFields()

	if self.m_Binded then return end

	self.m_BackButton = self.transform:Find("Panel/BackButton").gameObject

	self.m_CreatingRoleRoot = self.transform:Find("CreatingRole").gameObject
	self.m_ExistingRoleRoot = self.transform:Find("ExistingRole").gameObject
	self.m_EnterGameBtnForExistingRole = self.transform:Find("ExistingRole/EnterButton"):GetComponent(typeof(CButton))
	self.m_DeleteRoleBtn = self.transform:Find("ExistingRole/ExistingRoleInfo/DeleteBtn").gameObject
	--遍历 existing portrait
	self.m_ExistingPortraits = {}
	local existingPortraitRoot = self.transform:Find("ExistingRole/ExistingRoleList").transform
	for i=0,existingPortraitRoot.childCount-1 do
		table.insert(self.m_ExistingPortraits, existingPortraitRoot:GetChild(i):GetComponent(typeof(CExistingHeadPortrait)))
	end

	self.m_RoleNameLabel = self.transform:Find("ExistingRole/ExistingRoleInfo/NameLabel"):GetComponent(typeof(UILabel))
	self.m_RoleProfessionTexture = self.transform:Find("ExistingRole/ExistingRoleInfo/Profession"):GetComponent(typeof(CUITexture))
	self.m_RoleLevelLabel = self.transform:Find("ExistingRole/ExistingRoleInfo/LevelLabel"):GetComponent(typeof(UILabel))
	self.m_PreviousPageBtn = self.transform:Find("ExistingRole/Container/PreviousBtn"):GetComponent(typeof(CButton))
	self.m_NextPageBtn = self.transform:Find("ExistingRole/Container/NextBtn"):GetComponent(typeof(CButton))
	self.m_PageLabel = self.transform:Find("ExistingRole/Container/PageLabel"):GetComponent(typeof(UILabel))
	self.m_OpenTimeLabel = self.transform:Find("ExistingRole/OpenTimeLabel"):GetComponent(typeof(UILabel))
    self.m_TeamAppointmentButton = self.transform:Find("ExistingRole/TeamAppointmentButton").gameObject
    self.m_OriginOpenTimeLabelPos = self.m_OpenTimeLabel.transform.localPosition
    self.m_CreateRoleMasterRoot = self.m_CreatingRoleRoot.transform:Find("Master").transform
    self.m_CreateRoleDetailRoot = self.m_CreatingRoleRoot.transform:Find("Detail").transform
    self.m_CreateRoleMasterRoot.gameObject:SetActive(true)
    self.m_CreateRoleDetailRoot.gameObject:SetActive(true)
    self.m_MasterLeftOffset = self.m_CreateRoleMasterRoot:Find("Left/Offset").transform
    self.m_MasterRightOffset = self.m_CreateRoleMasterRoot:Find("Right/Offset").transform
    self.m_DetailLeftOffset = self.m_CreateRoleDetailRoot:Find("Left/Offset").transform
    self.m_DetailRightOffset = self.m_CreateRoleDetailRoot:Find("Right/Offset").transform

    self.m_ProfessionListView = self.m_MasterLeftOffset:Find("ProfessionList"):GetComponent(typeof(CProfessionListView))
    self.m_GenderTable = self.m_MasterLeftOffset:Find("Gender"):GetComponent(typeof(UITable))
    --遍历gender cell
    self.m_GenderCells = {}
    for i=0,self.m_GenderTable.transform.childCount-1 do
        table.insert(self.m_GenderCells, self.m_GenderTable.transform:GetChild(i):GetComponent(typeof(CButton)))
    end

    self.m_MasterProfessionTexture = self.m_MasterRightOffset:Find("RoleInfo/Top/FeatureBg/Profession"):GetComponent(typeof(CUITexture))
    self.m_MasterFeatureTexture = self.m_MasterRightOffset:Find("RoleInfo/Top/FeatureBg/Feature"):GetComponent(typeof(CUITexture))
    self.m_DescTexture = self.m_MasterRightOffset:Find("RoleInfo/Top/DescTexture"):GetComponent(typeof(CUITexture))
    self.m_RadarMapTexture = self.m_MasterRightOffset:Find("RoleInfo/Top/Radar/Texture"):GetComponent(typeof(CUITexture))
    self.m_DetailButton = self.m_MasterRightOffset:Find("DetailButton").gameObject
    self.m_QYCodeUseButton = self.m_MasterRightOffset:Find("RoleInfo/Top/QYCodeUseButton").gameObject

    self.m_MasterProfessionAboveFx = self.m_MasterProfessionTexture.transform:Find("AboveFx"):GetComponent(typeof(CUIFx))
    self.m_MasterProfessionBelowFx = self.m_MasterProfessionTexture.transform:Find("BelowFx"):GetComponent(typeof(CUIFx))

    self.m_DetailProfessionTexture = self.m_DetailRightOffset:Find("Top/Info/Profession"):GetComponent(typeof(CUITexture))
    self.m_DetailFeatureTexture = self.m_DetailRightOffset:Find("Top/Info/Feature"):GetComponent(typeof(CUITexture))
    self.m_XianFanButtonRoot = self.m_CreateRoleMasterRoot:Find("Bottom")
    self.m_XianShenButton = self.m_CreateRoleMasterRoot:Find("Bottom/XianShenButton"):GetComponent(typeof(CButton))
    self.m_FanShenButton = self.m_CreateRoleMasterRoot:Find("Bottom/FanShenButton"):GetComponent(typeof(CButton))

    self.m_RandomNameButton = self.m_DetailRightOffset:Find("RandomButton").gameObject
    self.m_EnterGameBtnForNewRole = self.m_DetailRightOffset:Find("EnterButton"):GetComponent(typeof(CButton))
    self.m_NameInput = self.m_DetailRightOffset:Find("Input"):GetComponent(typeof(UIInput))
    --遍历 hair cell
    self.m_HairCells = {}
    local haircellRoot = self.m_DetailRightOffset:Find("Hair/Table").transform
    for i=0,haircellRoot.childCount-1 do
        table.insert(self.m_HairCells, haircellRoot:GetChild(i):GetComponent(typeof(CCharacterHairCell)))
    end
    self.m_UseTokenToggle = self.m_DetailRightOffset:Find("Checkbox"):GetComponent(typeof(UIToggle))



	self.m_CameraScript = self.transform:Find("CameraCtrl"):GetComponent(typeof(CCommonLuaScript))

	self.m_CurPageIndex = 0
	self.m_MaxCharactersPerPage = 4
	self.m_ModelRoot = nil
	self.m_CurClass = EnumClass.FangShi
	self.m_CurGender = EnumGender.Male
	self.m_CurHairId = 0 --下标从0开始
	self.m_RO = nil
	self.m_CurEquipInfos = nil
	self.m_ForceCreateNewRole = false
	self.m_FxScale = 1.0
	self.m_LiangXiangFX = nil
	self.m_Sound = nil

    self:SetXianShenSelected(false, false)

	self.m_Binded = true
end

function LuaCharacterCreationWnd:SetXianShenSelected(xianshenSelected, changeModel)
	local xianfanChanged = self.m_XianShenSelected ~= xianshenSelected

	self.m_XianShenButton.Selected = xianshenSelected or false
	self.m_FanShenButton.Selected = not xianshenSelected
	self.m_XianShenSelected = xianshenSelected or false
	if xianfanChanged and changeModel then
		if self.m_NeedPlayXianFanTransform and self.m_RO ~= nil and self.m_RO:IsAllFinished() then
			self:PlayXianFanTransformFadeOut(self.m_RO, function() self:ChangeCandidateRole(self.m_CurClass, self.m_CurGender, self.m_CurHairId, true, -1, false, true) end)
		else
			self:ChangeCandidateRole(self.m_CurClass, self.m_CurGender, self.m_CurHairId, true, -1, false, false)
		end
		self.m_NeedPlayXianFanTransform = false
	end
end

function LuaCharacterCreationWnd:Awake()

	self:BindComponentsAndInitFields()

	local platform = Application.platform
	if platform == RuntimePlatform.Android or platform == RuntimePlatform.IPhonePlayer then
        EventDelegate.Add(self.m_NameInput.onSubmit, DelegateFactory.Callback(function ()
            self:OnInputChange()
        end))
    else
        EventDelegate.Add(self.m_NameInput.onChange, DelegateFactory.Callback(function ()
            self:OnInputChange()
        end))
    end
    if CommonDefs.IS_KR_CLIENT then
        self.m_RandomNameButton:SetActive(false)
        self.m_UseTokenToggle.gameObject:SetActive(false)
    end

    CommonDefs.AddOnClickListener(self.m_EnterGameBtnForExistingRole.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnExistingEnterGameButtonClick(go) end), false)
    CommonDefs.AddOnClickListener(self.m_EnterGameBtnForNewRole.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnEnterGameButtonClick(go) end), false)
    CommonDefs.AddOnClickListener(self.m_BackButton, DelegateFactory.Action_GameObject(function(go) self:OnBackButtonClick(go) end), false)
    CommonDefs.AddOnClickListener(self.m_RandomNameButton, DelegateFactory.Action_GameObject(function(go) self:OnRandomNameButtonClick(go) end), false)
    CommonDefs.AddOnClickListener(self.m_TeamAppointmentButton, DelegateFactory.Action_GameObject(function(go) self:OnTeamAppointmentButtonClick(go) end), false)

    for i=1, #self.m_GenderCells do
        CommonDefs.AddOnClickListener(self.m_GenderCells[i].gameObject, DelegateFactory.Action_GameObject(function(go) self:OnGenderClick(go) end), false)
    end

    for i=1, #self.m_HairCells do
        CommonDefs.AddOnClickListener(self.m_HairCells[i].gameObject, DelegateFactory.Action_GameObject(function(go) self:OnHairClick(go) end), false)
    end

    CommonDefs.AddOnClickListener(self.m_DetailButton, DelegateFactory.Action_GameObject(function(go) self:OnDetailButtonClick(go) end), false)
    CommonDefs.AddOnClickListener(self.m_XianShenButton.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnXianShenButtonClick(go) end), false)
    CommonDefs.AddOnClickListener(self.m_FanShenButton.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnFanShenButtonClick(go) end), false)
    CommonDefs.AddOnClickListener(self.m_QYCodeUseButton.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnQYCodeUseButtonClick(go) end), false)

    -- 更新lastServerVersion
    if PlayerSettings.LastClientVersion ~= CClientVersion.g_VersionInfo then
        PlayerSettings.AppStoreReviewed = 1
        PlayerSettings.LastClientVersion = CClientVersion.g_VersionInfo
    end
    self:SetSceneParams()

    if CLoginMgr.Inst.m_ExistingPlayerList.Count == 0 and IsQYCodeUseOpen() then
        local message = g_MessageMgr:FormatMessage("QYCODE_USE_CONFIRM")
        MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function ()
            self:ShowQYCodeInputWnd()
        end), nil, nil, nil, false)
    end
end

--有些全局参数需要借助corescene设置一下，这里仿照CRenderScene里面的PreviewScene处理一下
function LuaCharacterCreationWnd:SetSceneParams()

    local deviceLevel = g_DeviceMgr:GetDeviceLevel()
    if deviceLevel>=4 then
        Shader.globalMaximumLOD = 600
    else
        Shader.globalMaximumLOD = 200
    end
    --Shader.globalMaximumLOD = 600 --萌萌建议值600，默认可能是500
end

function LuaCharacterCreationWnd:Start()
    CommonDefs.AddOnClickListener(self.m_DeleteRoleBtn.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnDeleteRoleButtonClick(go) end), false)
    CommonDefs.AddOnClickListener(self.m_PreviousPageBtn.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnPreviousPageButtonClick(go) end), false)
    CommonDefs.AddOnClickListener(self.m_NextPageBtn.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnNextPageButtonClick(go) end), false)

    if UIRoot.EnableIPhoneXAdaptation then
    	if Application.platform == RuntimePlatform.Android then
            local deviceModelToLower = string.lower(SystemInfo.deviceModel)
    		if NativeTools.IsHideNotchSwitchOpen(deviceModelToLower) then
               	return --隐藏刘海的情况下，这里不进行左移动
            end
    	else
    		local sprite = self.m_BackButton:GetComponent(typeof(UISprite))
            sprite.leftAnchor.absolute = 28 - UIRoot.IPhoneXWidthMargin
            sprite.rightAnchor.absolute = 206 - UIRoot.IPhoneXWidthMargin
            sprite:ResetAndUpdateAnchors()
    	end
    end
end

function LuaCharacterCreationWnd:OnPreviousPageButtonClick()
	if self.m_CurPageIndex > 0 then
        self.m_CurPageIndex = self.m_CurPageIndex - 1
        self:ShowPage()
    end
end

function LuaCharacterCreationWnd:OnNextPageButtonClick()
    local list = CLoginMgr.Inst.m_ExistingPlayerList or CreateFromClass(MakeGenericClass(List, CExistingPlayerInfo))
    local pageCount = math.ceil(list.Count * 1 / self.m_MaxCharactersPerPage)
    if CLoginMgr.Inst.m_ExistingPlayerList ~= nil and self.m_CurPageIndex < pageCount - 1 then
        self.m_CurPageIndex = self.m_CurPageIndex + 1
        self:ShowPage()
    end
end

function LuaCharacterCreationWnd:ShowPage()
    local list = CLoginMgr.Inst.m_ExistingPlayerList or CreateFromClass(MakeGenericClass(List, CExistingPlayerInfo))
    local pageCount = math.ceil(list.Count * 1 / self.m_MaxCharactersPerPage)
    if self.m_CurPageIndex >= 0 and self.m_CurPageIndex < pageCount then
        self.m_PageLabel.text = System.String.Format("{0}/{1}", self.m_CurPageIndex + 1, pageCount)
        self:InitModelRoot()
    else
        self.m_CurPageIndex = 0
        self.m_PageLabel.text = System.String.Format("{0}/{1}", 0, pageCount)
        self:InitModelRoot()
    end
    self.m_PreviousPageBtn.gameObject:SetActive(pageCount > 1)
    self.m_NextPageBtn.gameObject:SetActive(pageCount > 1)
    self.m_PageLabel.enabled = (pageCount > 1)
    self.m_PreviousPageBtn.Enabled = (pageCount > 1 and self.m_CurPageIndex > 0)
    self.m_NextPageBtn.Enabled = (pageCount > 1 and self.m_CurPageIndex < pageCount - 1)
end

function LuaCharacterCreationWnd:OnDestroy()
	self:DestoryRO()
	if self.m_LiangXiangFX~=nil then
		self.m_LiangXiangFX:Destroy()
		self.m_LiangXiangFX = nil
	end
	if self.m_Sound~=nil then
		SoundManager.Inst:StopSound(self.m_Sound)
		self.m_Sound = nil
	end
end

function LuaCharacterCreationWnd:OnServerNotifyRetryLogin(...)
	self.m_EnterGameBtnForNewRole.Enabled = true
    self.m_EnterGameBtnForExistingRole.Enabled = true
end

function LuaCharacterCreationWnd:OnStartUnloadScene(args)
    local sceneName = args[0] or ""
	if sceneName == Setting.sDengLu_Level then
        self:DestoryRO()
    end
end

function LuaCharacterCreationWnd:IsLoginExistingCharacter()
	if self.m_ForceCreateNewRole then
        return false
    end
    return (CLoginMgr.Inst.m_ExistingPlayerList.Count > 0)
end

function LuaCharacterCreationWnd:InitModelRoot()
	CObjectFX.s_FixFxScale = false
    if LoginModelRoot.Inst ~= nil then
        --Scene中的ModelRoot会被打上static标记，导致角色模型二次载入后也会被标记会static，从而导致武器的MeshFilter会变成Combined
        local go = GameObject.Find("__ModelRoot__")
        if go == nil then
            self.m_ModelRoot = CreateFromClass(GameObject, "__ModelRoot__").transform
            self.m_ModelRoot.transform.position = LoginModelRoot.Inst.transform.position
            self.m_ModelRoot.transform.localScale = LoginModelRoot.Inst.transform.localScale
            self.m_FxScale = self.m_ModelRoot.transform.localScale.x
            self.m_ModelRoot.transform.localRotation = LoginModelRoot.Inst.transform.localRotation
        else
            self.m_ModelRoot = go.transform
        end
    end
    if self.m_ModelRoot ~= nil then
        self:InitCandidateRoles()
    end
end

function LuaCharacterCreationWnd:MinRequiredLevelToDeleteRole()
	return GameSetting_Common.GetData().MinRequiredLevelToDeleteRole
end

function LuaCharacterCreationWnd:Init()
	self.m_NameInput.value = nil
    self.m_UseTokenToggle.value = false
    local existRole = self:IsLoginExistingCharacter()
    self.m_CreatingRoleRoot:SetActive(not existRole)
    self.m_ExistingRoleRoot:SetActive(existRole)
    self.m_ExistingPortraits[1].transform.parent.gameObject:SetActive(not CLoginMgr.Inst.PreLoginMode)
    self.m_OpenTimeLabel.gameObject:SetActive(CLoginMgr.Inst.PreLoginMode and CLoginMgr.Inst.PreLoginModeCharacterExists)
    self.m_DeleteRoleBtn:SetActive(not CLoginMgr.Inst.PreLoginMode)
    self.m_OpenTimeLabel.text = CLoginMgr.Inst:GetLatestSelectedGameServer().openTime
    self.m_TeamAppointmentButton:SetActive(self:CurrentServerTeamAppointmentEnabled() and CLoginMgr.Inst.PreLoginModeCharacterExists)
    self.m_ProfessionListView.OnProfessionSelected = DelegateFactory.Action_uint(function (cls)
        self:OnProfessionSelected(cls)
    end)
    --TODO
    self.m_CurPageIndex = 0
    self:ShowPage()

    if self.m_CreatingRoleRoot.activeSelf then
        self:DoCreateRoleTransition(false, false)
        DRPFMgr.Inst:EnterCreateRole(CLoginMgr.Inst.m_ExistingPlayerList.Count)
    end

    -- 特殊处理一下全民PK服，使用验证的角色名
    if CQuanMinPKMgr.Inst.m_IsQuanMinPKServer and not System.String.IsNullOrEmpty(CLoginMgr.Inst.QmpkSignUpCharacterName) then
        self.m_NameInput.value = CLoginMgr.Inst.QmpkSignUpCharacterName
        CommonDefs.ListIterate(CLoginMgr.Inst.m_ExistingPlayerList, DelegateFactory.Action_object(function (___value)
            local player = ___value
            if player.m_BasicProp.Name == CLoginMgr.Inst.QmpkSignUpCharacterName then
                local name = CLoginMgr.Inst.QmpkSignUpCharacterName .. string.char(UnityEngine_Random(32, 126))
                if CUICommonDef.GetStrByteLength(name) > Constants.MaxCharLimitForPlayerName then
                    self.m_NameInput.value = ""
                else
                    self.m_NameInput.value = name
                end
                return
            end
        end))
    end

    -- 账号下无角色且开启了迁移码使用
    if CLoginMgr.Inst.m_ExistingPlayerList.Count == 0 and IsQYCodeUseOpen() then
        self.m_QYCodeUseButton:SetActive(true)
    else
        self.m_QYCodeUseButton:SetActive(false)
    end

	LuaLoginMgr.TryConfirmShowDaShenAIDetectUrl()
	Gac2Login.EnterCreatingPlayerDone("")

    self:HandleAutoLoginByCmdLineArgs()
end

function LuaCharacterCreationWnd:OnLoadingSceneFinished(...)
    if not CLoadingWnd.Inst:IsLoadingUI() then
        if not self:HandleDeepLink() then
            self:Handle3DTouch()
        end
    end
end

function LuaCharacterCreationWnd:HandleAutoLoginByCmdLineArgs()
    if CLoginMgr.Inst:NeedAutoLoginByCmdLineArgs() then
        CLoginMgr.Inst:ClearAutoLoginByCmdLineArgs()
        if  CLoginMgr.Inst.m_ExistingPlayerList.Count == 0 then
            math.randomseed(os.time()) --不添加这一句每次运行客户端到这里得到的随机结果是一样的，导致新建角色会重名
            local classval = math.random(1, EnumToInt(EnumClass.Undefined)-1)
            local genderval = math.random(0, 1)
            local charStr = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
            local suffix = ""
            for i=1,6 do
                local idx = math.random(1, #charStr)
                suffix = suffix..string.sub(charStr, idx, idx)
            end
            local name = "whlaa"..tostring(classval*100+genderval)..suffix
            local class =  CommonDefs.ConvertIntToEnum(typeof(EnumClass), classval)
            local gender = CommonDefs.ConvertIntToEnum(typeof(EnumGender),genderval)
            local headId = 0
            self:RequestCreatePlayer(name,  class, gender, headId)
        else
            --self:OnEnterGameWithLastLoginCharacter()
            local list = CLoginMgr.Inst.m_ExistingPlayerList
            local bFound = false
            for i=0,list.Count-1 do
                if CLoginMgr.Inst.m_LastLoginPlayerId == list[i].m_BasicProp.Id then
                    self:EnterGameWithExistingRole(i)
                    bFound = true
                    break
                end
            end
            if not bFound then
                self:EnterGameWithExistingRole(0) --如果服务器没有返回上次登录角色，默认登录第一个
            end
        end
    end
end

function LuaCharacterCreationWnd:Handle3DTouch()
	if CommonDefs.IS_CN_CLIENT and Application.platform == RuntimePlatform.IPhonePlayer then
		if C3DTouchMgr.Inst:Check3DTouchStatus() == Enum3DTouchStatus.OpenWnd and CAppClient.CurrentLevelName == Setting.sDengLu_Level then
			self:OnEnterGameWithLastLoginCharacter()
			C3DTouchMgr.Inst.m_IsFirstIntoGame = true
		end
	end
end

function LuaCharacterCreationWnd:On3DTouchTriggered(...)
	if not CLoadingWnd.Inst:IsLoadingUI() and not CLoadingWnd.Inst:IsLoadingScene() then
        self:Handle3DTouch()
    end
end

function LuaCharacterCreationWnd:OnLaunchAppFromDeepLink(...)
    self:HandleDeepLink()
end

function LuaCharacterCreationWnd:HandleDeepLink()
    if CDeepLinkMgr.Inst:HasInGameAction() then
        self:OnEnterGameWithLastLoginCharacter()
        return true
    else
        return false
    end
end

function LuaCharacterCreationWnd:OnEnterGameWithLastLoginCharacter()
	local list = CLoginMgr.Inst.m_ExistingPlayerList
	for i=0,list.Count-1 do
		if CLoginMgr.Inst.m_LastLoginPlayerId == list[i].m_BasicProp.Id then
            self:EnterGameWithExistingRole(i)
            break
        end
	end
end

function LuaCharacterCreationWnd:EnterGameWithExistingRole(index)
	if index < CLoginMgr.Inst.m_ExistingPlayerList.Count then
        local existingCharacterInfo = CLoginMgr.Inst.m_ExistingPlayerList[index]
        local playerId = tonumber(existingCharacterInfo.m_BasicProp.Id)
        CLoginMgr.Inst:RequestLoginExistPlayer(playerId)
        EventManager.Broadcast(EnumEventType.PlayerLogin)
        CLoginMgr.Inst.IsCreateRole = false
        self.m_EnterGameBtnForExistingRole.Enabled = false
        self:RegisterEnterGameForExistingRoleTick()
    end
end

function LuaCharacterCreationWnd:OnExistingRoleButtonClick(go)
    self.m_RoleProfessionTexture:Clear()
    self.m_RoleNameLabel.text = nil
    self.m_RoleLevelLabel.text = nil
    self.m_DeleteRoleBtn:SetActive(false)

    local headPortrait = nil
    for i=1, #self.m_ExistingPortraits do
    	self.m_ExistingPortraits[i].Selected = false
        if go == self.m_ExistingPortraits[i].gameObject then
            headPortrait = self.m_ExistingPortraits[i]
        end
    end

    if headPortrait == nil then
        return
    end

    local idx = headPortrait.IndexOfExistingCharacters
    if idx >= 0 and CLoginMgr.Inst.m_ExistingPlayerList.Count > idx then
        headPortrait.Selected = true

        local playerInfo = CLoginMgr.Inst.m_ExistingPlayerList[idx]
        if playerInfo ~= nil then
            local cls = CommonDefs.ConvertIntToEnum(typeof(EnumClass), playerInfo.m_BasicProp.Class)
            local gender = CommonDefs.ConvertIntToEnum(typeof(EnumGender), playerInfo.m_BasicProp.Gender)
            self:ChangeCandidateRole(cls, gender, playerInfo.m_AppearProp.HeadId, true, idx, true)

            local init = Initialization_Init.GetData(playerInfo.m_BasicProp.Class*100 + playerInfo.m_BasicProp.Gender)
            self.m_RoleProfessionTexture:LoadMaterial(init.ProfessionTexture)
            self.m_RoleNameLabel.text = playerInfo.m_BasicProp.Name
            local default
            if playerInfo.m_AppearProp.FeiShengPlayXianFanStatus > 0 then
                default = Constants.ColorOfFeiSheng
            else
                default = NGUIText.EncodeColor24(self.m_RoleLevelLabel.color)
            end
            self.m_RoleLevelLabel.text = System.String.Format(LocalString.GetString("[c][{0}]{1}级[-][/c]"), default, playerInfo.m_BasicProp.Level)

            if CSwitchMgr.DeleteCharacterEnabled and (not CLoginMgr.Inst.PreLoginMode) then
                if playerInfo.m_BasicProp.Level < self.MinRequiredLevelToDeleteRole() then
                    self.m_DeleteRoleBtn:SetActive(true)
                end
            end
            -- 上架藏宝阁, 不允许登录
            --[[if playerInfo.m_BasicProp.CangbaogeInfo.CBGStatus ~= 0 and playerInfo.m_BasicProp.CangbaogeInfo.CBGStatus ~= 1 then
                if self.m_EnterGameBtnForExistingRole.label ~= nil then
                    self.m_EnterGameBtnForExistingRole.label.text = LocalString.GetString("藏宝阁下架")
                end
            end--]]
        end
    else
		-- 请求开始新建角色
		if CLoginMgr.Inst:CheckLoginConnection() then
			Gac2Login.RequestBeginCreatePlayer()
		else
			CLoginMgr.Inst:_TryBackToLoginScene()
		end
    end
end

function LuaCharacterCreationWnd:BeginCreatePlayer()
	-- 新建角色
	self.m_ForceCreateNewRole = true
	self:Init()
	self.m_ForceCreateNewRole = false
end

function LuaCharacterCreationWnd:GetSelectedExistingPortrait()
	for i=1,#self.m_ExistingPortraits do
		if self.m_ExistingPortraits[i].Selected then
			return self.m_ExistingPortraits[i]
		end
	end
	return nil
end
function LuaCharacterCreationWnd:OnExistingEnterGameButtonClick(go)

    local headPortrait = self:GetSelectedExistingPortrait()

    if headPortrait ~= nil then
        local idx = headPortrait.IndexOfExistingCharacters
        if idx < CLoginMgr.Inst.m_ExistingPlayerList.Count then
            local existingCharacterInfo = CLoginMgr.Inst.m_ExistingPlayerList[idx]
            local playerId = tonumber(existingCharacterInfo.m_BasicProp.Id)

            --[[if existingCharacterInfo.m_BasicProp.CangbaogeInfo.CBGStatus ~= 0 and existingCharacterInfo.m_BasicProp.CangbaogeInfo.CBGStatus ~= 1 then
                local msg = g_MessageMgr:FormatMessage("CBG_UNSHELF_AND_LOGIN")
                MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
                    Gac2Login.CBG_PlayerRequestOffShelf(playerId, "")
                end), nil, nil, nil, false)
            else--]]
                CLoginMgr.Inst:RequestLoginExistPlayer(playerId)
                EventManager.Broadcast(EnumEventType.PlayerLogin)
                CLoginMgr.Inst.IsCreateRole = false
                self.m_EnterGameBtnForExistingRole.Enabled = false
                self:RegisterEnterGameForExistingRoleTick()
            --end
        end
    end
end
function LuaCharacterCreationWnd:OnBackButtonClick(go)

    if self.m_CreatingRoleRoot.activeSelf and self.m_ShowDetail then
        self:DoCreateRoleTransition(false, true)
        return
    end
    --如果当前在创建角色界面并且账号下存在其他角色，则返回角色选择界面
    if self.m_CreatingRoleRoot.activeSelf and self:IsLoginExistingCharacter() then
        self:Init()
    else
        CLoginMgr.Inst:Disconnect()
        self:DestoryRO()
        CUIManager.ShowUIGroup(CUIManager.EUIModuleGroup.EServerWndUI)
    end
end
function LuaCharacterCreationWnd:InitCandidateRoles()

    if self:IsLoginExistingCharacter() then
        -- 已有角色
        local hasSelected = false

        for i=1, math.min(self.m_MaxCharactersPerPage, #self.m_ExistingPortraits) do

	        local existingCharacterInfo = nil
	        local headPortrait = self.m_ExistingPortraits[i]
	        headPortrait:SetPortrait(nil)
	        --默认不显示头像
	        headPortrait.gameObject:SetActive(true)
	        UIEventListener.Get(self.m_ExistingPortraits[i].gameObject).onClick = DelegateFactory.VoidDelegate(function ()
	            self:OnExistingRoleButtonClick(self.m_ExistingPortraits[i].gameObject)
	        end)
	        local index = self.m_CurPageIndex * self.m_MaxCharactersPerPage + (i-1)
	        if CLoginMgr.Inst.m_ExistingPlayerList.Count > index then
	            existingCharacterInfo = CLoginMgr.Inst.m_ExistingPlayerList[index]
	            local cls = existingCharacterInfo.m_BasicProp.Class
	            local gender = existingCharacterInfo.m_BasicProp.Gender
	            local data = Initialization_Init.GetData((cls * 100 + gender))

	            if data ~= nil then
	                headPortrait.IndexOfExistingCharacters = index
	                headPortrait:SetPortrait(data.ResName)
	                if CLoginMgr.Inst.m_LastLoginPlayerId == existingCharacterInfo.m_BasicProp.Id then
	                    hasSelected = true
	                    self:OnExistingRoleButtonClick(self.m_ExistingPortraits[i].gameObject)
	                end
	            end
	        else
	            headPortrait.IndexOfExistingCharacters = - 1
	            --超出角色上限时不显示创建角色入口
	            if CLoginMgr.Inst.m_ExistingPlayerList.Count > self.m_MaxCharactersPerPage then
	                headPortrait.gameObject:SetActive(false)
	            end
	        end

        end
        if not hasSelected then
            self:OnExistingRoleButtonClick(self.m_ExistingPortraits[1].gameObject)
        end

        return
    else
        if self.m_ProfessionListView.gameObject.activeSelf then
            self.m_CurClass = EnumClass.Undefined
            --置为空
            self.m_ProfessionListView:Init(CLoginMgr.Inst:GetRandomClass())
        end
    end
end
function LuaCharacterCreationWnd:OnHairClick(go)

	for i=1,#self.m_HairCells do

		if go == self.m_HairCells[i].gameObject then
            self.m_HairCells[i].Selected = true
            local data = Initialization_Init.GetData(EnumToInt(self.m_CurClass) * 100 + EnumToInt(self.m_CurGender))
            if data ~= nil and self.m_CurHairId+1 ~= i then
                self.m_CurHairId = i-1
				-- data.NewBaseCloth always 1
                -- 全部都采用了NewBaseCloth，这里去掉之前的旧分支
                CClientMainPlayer.ChangeHairColorIndex(self.m_RO, self.m_CurClass, self.m_CurGender, self.m_CurHairId)
            end
        else
            self.m_HairCells[i].Selected = false
        end
	end
end
function LuaCharacterCreationWnd:OnGenderClick(go)
	for i=1,#self.m_GenderCells do

        if go == self.m_GenderCells[i].gameObject then
            self.m_GenderCells[i].Selected = true
            local gender = (i == 1) and EnumGender.Male or EnumGender.Female
            local data = Initialization_Init.GetData(EnumToInt(self.m_CurClass) * 100 + EnumToInt(gender))
            if data ~= nil and self.m_CurGender ~= gender then
                --初始化发色
                for j=1,#self.m_HairCells do
                    if j <= #self.m_HairCells then
                    	self.m_HairCells[j].HairColor = NGUIText.ParseColor24(data.HairColor[j-1], 0)
                    else
                        self.m_HairCells[j].HairColor  = Color.black
                    end
                    self.m_HairCells[j].Selected = false
                end
                self.m_CurHairId = 0
                self.m_HairCells[self.m_CurHairId+1].Selected = true

				self:SetXianShenSelected(false, false)
				self.m_NeedPlayXianFanTransform = true
                self:ChangeCandidateRole(self.m_CurClass, gender, self.m_CurHairId, false, -1, true)
            end
        else
            self.m_GenderCells[i].Selected = false
        end
    end
end
function LuaCharacterCreationWnd:OnInputChange()
    local str = self.m_NameInput.value
    if CUICommonDef.GetStrByteLength(str) > Constants.MaxCharLimitForPlayerName then
        repeat
            str = CommonDefs.StringSubstring2(str, 0, CommonDefs.StringLength(str) - 1)
        until not (CUICommonDef.GetStrByteLength(str) > Constants.MaxCharLimitForPlayerName)
        self.m_NameInput.value = str
    end
end
function LuaCharacterCreationWnd:OnProfessionSelected(cls)
    if EnumToInt(self.m_CurClass) ~= cls then
        local maleCharacter = Initialization_Init.GetData(cls * 100 + EnumGender_lua.Male)
        local femaleCharacter = Initialization_Init.GetData(cls * 100 + EnumGender_lua.Female)
        if maleCharacter ~= nil and (maleCharacter.Status ~= 0 or maleCharacter.NewRole~=1) then
            maleCharacter = nil
        end
        if femaleCharacter ~= nil and (femaleCharacter.Status ~= 0 or femaleCharacter.NewRole~=1) then
            femaleCharacter = nil
        end
        if maleCharacter == nil and femaleCharacter == nil then
            return
        end

        --随机选男女
        local character = nil
        if maleCharacter ~= nil and femaleCharacter ~= nil then
            local default
            if Random.value >= 0.5 then
                default = maleCharacter
            else
                default = femaleCharacter
            end
            character = default
        elseif maleCharacter ~= nil then
            character = maleCharacter
        else
            character = femaleCharacter
        end

        --初始化性别
        local maleIndex = 1
        local femaleIndex = 2
        self.m_GenderCells[maleIndex].gameObject:SetActive(maleCharacter ~= nil)
        self.m_GenderCells[femaleIndex].gameObject:SetActive(femaleCharacter ~= nil)
        self.m_GenderCells[maleIndex].Selected = (character.Gender == EnumGender_lua.Male)
        self.m_GenderCells[femaleIndex].Selected = (character.Gender == EnumGender_lua.Female)
        self.m_GenderTable:Reposition()
        --初始化发色
        for j=1,#self.m_HairCells do
            if j <= #self.m_HairCells then
            	self.m_HairCells[j].HairColor = NGUIText.ParseColor24(character.HairColor[j-1], 0)
            else
                self.m_HairCells[j].HairColor  = Color.black
            end
            self.m_HairCells[j].Selected = false
        end
        self.m_CurHairId = 0
        self.m_HairCells[self.m_CurHairId+1].Selected = true
		self:SetXianShenSelected(false, false)
		self.m_NeedPlayXianFanTransform = true
        self:ChangeCandidateRole(CommonDefs.ConvertIntToEnum(typeof(EnumClass), cls),
        	CommonDefs.ConvertIntToEnum(typeof(EnumGender), character.Gender), self.m_CurHairId, true, -1, true)
    end
end
function LuaCharacterCreationWnd:OnRandomNameButtonClick(go)
    local name = nil
    for i=1,5 do --最多尝试随机五次名字
        name = CLoginMgr.Inst:GetRandomName(self.m_CurGender, self.m_UseTokenToggle.value)
        if self:CheckNameValid(name,false) then
            break
        end
    end
    self.m_NameInput.value = name
    self:OnInputChange()
    --避免随机取名超出长度限制
    self:DoRandomAnimation(go, 0)
end
function LuaCharacterCreationWnd:OnTeamAppointmentButtonClick(go)
    CommonDefs.OpenURL(TeamReverse_Setting.GetData().ExternalBrowserWebsite)
end
function LuaCharacterCreationWnd:DoRandomAnimation(go, times)
	local v = go.transform.eulerAngles
	v.z = v.z - 180
	LuaTweenUtils.OnComplete(LuaTweenUtils.TweenRotationZ(go.transform, v.z, 0.05), function ()
		if times<6 then
			self:DoRandomAnimation(go, times+1)
		end
	end)
end
function LuaCharacterCreationWnd:CheckNameValid(nameStr, showSign)
    if CommonDefs.IsSeaMultiLang() then
        local length = CommonDefs.StringLength(nameStr)
        if length < Constants.SeaMinCharLimitForPlayerName or length > Constants.SeaMaxCharLimitForPlayerName then
            if showSign then
                g_MessageMgr:ShowMessage("Sea_Login_Create_Role_Failed_Name_Length_Not_Fit", Constants.SeaMinCharLimitForPlayerName, Constants.SeaMaxCharLimitForPlayerName)
            end
            return false
        end
    else
        local length = CUICommonDef.GetStrByteLength(nameStr)
        if length < Constants.MinCharLimitForPlayerName or CUICommonDef.GetStrByteLength(nameStr) > Constants.MaxCharLimitForPlayerName then
            if showSign then
                g_MessageMgr:ShowMessage("Login_Create_Role_Failed_Name_Length_Not_Fit")
            end
            return false
        end
    end
	if System.Text.RegularExpressions.Regex.IsMatch(nameStr, "^[0-9]+$") then
		if showSign then
			g_MessageMgr:ShowMessage("Login_Create_Role_Failed_Name_Cannot_Be_All_Numbers")
		end
		return false
	end
	if not CWordFilterMgr.Inst:CheckName(nameStr) then
		if showSign then
			g_MessageMgr:ShowMessage("Name_Violation")
		end
		return false
	end

	return true
end

function LuaCharacterCreationWnd:OnEnterGameButtonClick(go)

    local nameStr = string.gsub(self.m_NameInput.value, " ", "")

		if not self:CheckNameValid(nameStr,true) then
			return
		end

    self.m_NameInput.value = nameStr
    self:RequestCreatePlayer(self.m_NameInput.value, self.m_CurClass, self.m_CurGender, self.m_CurHairId)
end

function LuaCharacterCreationWnd:RequestCreatePlayer(name, class, gender, headId)
    CLoginMgr.Inst.Name = name
    CLoginMgr.Inst.Class = class
    CLoginMgr.Inst.Gender = gender
    CLoginMgr.Inst.HeadId = headId
    CLoginMgr.Inst:RequestCreatePlayer()
    EventManager.Broadcast(EnumEventType.PlayerLogin)
    CLoginMgr.Inst.IsCreateRole = true

    self.m_EnterGameBtnForNewRole.Enabled = false
    self:RegisterEnterGameForCreateRoleTick()
end

function LuaCharacterCreationWnd:ChangeCandidateRole(eClass, eGender, hairId, force, loginPlayerIdx, playLiangxiang, playXianFanTransform)

    if not force and self.m_CurClass == eClass and self.m_CurGender == eGender then
        return
    end
    local professionChanged = (self.m_CurClass~=eClass)
    self.m_CurClass = eClass
    self.m_CurGender = eGender
    self.m_CurHairId = hairId
    local data = Initialization_Init.GetData(EnumToInt(self.m_CurClass) * 100 + EnumToInt(self.m_CurGender))
    self.m_CurrentEquipInfos = data.NewRoleEquip3

    self:CancelPrepareAniTick()
    self:DestoryRO()

    self.m_RO = CRenderObject.CreateRenderObject(self.m_ModelRoot.gameObject, "RenderObject")
    self.m_RO.Layer = LayerDefine.ModelForNGUI_3D
    self.m_RO.ShadowType = EShadowType.Simple
    self.m_RO.CanBeJob = false

    if CClientMainPlayer.s_UseHighAccuracy and CommonDefs.IsAndroidPlatform() then
        self.m_RO.UseHighAccuracy = true
    end

    if loginPlayerIdx >= 0 then
        local playerInfo = CLoginMgr.Inst.m_ExistingPlayerList[loginPlayerIdx]
        local appearanceData = playerInfo.m_AppearProp
        self.m_CurrentEquipInfos = appearanceData.Equipment
        self:LoadPlayerRes(self.m_RO, self.m_CurClass, self.m_CurGender, appearanceData, playLiangxiang, playXianFanTransform)
    else
        local shenBingFirstModelId = nil
        local shenBingSecondModelId = nil
        local shenBingTrainLvFxId = nil
        local shenBingRanSeData = nil
        self.m_CurrentEquipInfos, shenBingFirstModelId, shenBingSecondModelId, shenBingTrainLvFxId, shenBingRanSeData = self:GetShenBingEquipInfo(data, self.m_CurrentEquipInfos, self.m_XianShenSelected)
        local appearanceData = CPropertyAppearance.GetDataFromCustom(self.m_CurClass, self.m_CurGender, CPropertySkill.GetDefaultYingLingStateByGender(self.m_CurGender), self.m_CurrentEquipInfos, nil, 0, self.m_CurHairId, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, shenBingFirstModelId, shenBingSecondModelId, shenBingTrainLvFxId, shenBingRanSeData, 0, 0, 0, 0, 0, 0, nil)
        if self.m_XianShenSelected then
            appearanceData.FeiShengAppearanceXianFanStatus = 1 --展示仙身模型
        end
        self:LoadPlayerRes(self.m_RO, self.m_CurClass, self.m_CurGender, appearanceData, playLiangxiang, playXianFanTransform)
    end
    self:OnLoadRoleModelFinish(self.m_RO.transform, professionChanged)

	self.m_CameraScript.m_LuaSelf:SetFollowRO(self.m_RO)
	self.m_CameraScript.m_LuaSelf:SetDeltaHeightFactor(data.LoginCameraDeltaHeight)
	self.m_CameraScript.m_LuaSelf:UpdateEyeAnchor(data.EyeOffset[3], data.EyeOffset[4], data.EyeOffset[5], data.EyeOffset[0], data.EyeOffset[1], data.EyeOffset[2])
end

function LuaCharacterCreationWnd:GetShenBingEquipInfo(initialization_data, curEquipInfos, xianshenSelected)
    if xianshenSelected and initialization_data.ShenBingWaiGuan then
        local equipInfos = {}
        local shenBingFirstModelId = {}
        local shenBingSecondModelId = {}
        local shenBingTrainLvFxId = {}
        local shenBingRanSeData = {}
        for i=0,initialization_data.ShenBingWaiGuan.Length-1 do
            local equipTemplateId = initialization_data.ShenBingWaiGuan[i][0]
            local model1Id = initialization_data.ShenBingWaiGuan[i][1]
            local model2Id = initialization_data.ShenBingWaiGuan[i][2]
            local fxId = initialization_data.ShenBingWaiGuan[i][3]
            table.insert(equipInfos, equipTemplateId)
            table.insert(shenBingFirstModelId, model1Id)
            table.insert(shenBingSecondModelId, model2Id)
            table.insert(shenBingTrainLvFxId, fxId)
            table.insert(shenBingRanSeData, 0)
        end
        return Table2Array(equipInfos, MakeArrayClass(uint)), Table2Array(shenBingFirstModelId, MakeArrayClass(uint)), Table2Array(shenBingSecondModelId, MakeArrayClass(uint)),
                    Table2Array(shenBingTrainLvFxId, MakeArrayClass(uint)), Table2Array(shenBingRanSeData, MakeArrayClass(uint))
    end
    return curEquipInfos, nil ,nil, nil, nil
end

function LuaCharacterCreationWnd:OnLoadRoleModelFinish(root, professionChanged)

    local data = Initialization_Init.GetData(EnumToInt(self.m_CurClass) * 100 + EnumToInt(self.m_CurGender))

    self.m_MasterProfessionTexture:LoadMaterial(data.ProfessionTexture)
    self.m_MasterFeatureTexture:LoadMaterial(data.FeatureTexture)
    self.m_DescTexture:LoadMaterial(data.IntroductionTexture)

    self.m_DetailProfessionTexture:LoadMaterial(data.ProfessionTexture)
    self.m_DetailFeatureTexture:LoadMaterial(data.FeatureTexture)

    if self.m_CurClass == EnumClass.YingLing then
        self.m_RadarMapTexture:Clear()
        self:RegisterYingLingRadarMapTick()
    else
        self:CancelYingLingRadarMapTick()
        self.m_RadarMapTexture.texture.alpha = 1
        self.m_RadarMapTexture:LoadMaterial(Profession.GetRadarMap(self.m_CurClass))
    end

    if professionChanged then
        self.m_MasterProfessionAboveFx:DestroyFx()
        self.m_MasterProfessionBelowFx:DestroyFx()
        self.m_MasterProfessionAboveFx:LoadFx("Fx/UI/Prefab/UI_zhiyemingcheng.prefab")
        self.m_MasterProfessionBelowFx:LoadFx("Fx/UI/Prefab/UI_zhiyemingcheng_shuimo.prefab")
        local tweenalpha = self.m_MasterProfessionTexture.gameObject:GetComponent(typeof(TweenAlpha))
        tweenalpha:ResetToBeginning()
        tweenalpha:PlayForward()
    end
end

function LuaCharacterCreationWnd:RegisterYingLingRadarMapTick( ... )
    self:CancelYingLingRadarMapTick()
    local i = 0
    local updateFunc = function ()
        if self.m_RadarMapTexture ~= nil then
            self.m_RadarMapTexture:LoadMaterial(Profession.GetRadarMap(self.m_CurClass, i%3 + 1))
            i = i + 1
            LuaTweenUtils.DOKill(self.m_RadarMapTexture.transform, false)
            local fadeInTweener = LuaTweenUtils.TweenAlpha(self.m_RadarMapTexture.texture, self.m_RadarMapTexture.transform, 0,1,0.5)
            local fadeOutTweener = LuaTweenUtils.TweenAlpha(self.m_RadarMapTexture.texture, self.m_RadarMapTexture.transform,1,0,0.5)
            LuaTweenUtils.SetDelay(fadeOutTweener, 2)
        end
    end
    self.m_YingLingRadarMapTick = RegisterTick(function ( ... )
           updateFunc()
        end, 3000)
    updateFunc()--先执行一下
end

function LuaCharacterCreationWnd:CancelYingLingRadarMapTick( ... )
    if self.m_YingLingRadarMapTick then
        UnRegisterTick(self.m_YingLingRadarMapTick)
        self.m_YingLingRadarMapTick = nil
        LuaTweenUtils.DOKill(self.m_RadarMapTexture.transform, false)
    end
end

function LuaCharacterCreationWnd:OnEnable()
    self.m_SwipeGestureListener = DelegateFactory.Action_GenericGesture(function (param0) self:OnSwipe(param0) end)
    GestureRecognizer.AddGestureListener(GestureType.Swipe, self.m_SwipeGestureListener)

    g_ScriptEvent:AddListener("OnExistingRoleInfoUpdate", self, "OnExistingRoleInfoUpdate")
    g_ScriptEvent:AddListener("LoginSceneLoaded", self, "LoginSceneLoaded")
    g_ScriptEvent:AddListener("StartUnloadScene", self, "OnStartUnloadScene")
    g_ScriptEvent:AddListener("ServerNotifyRetryLogin", self, "OnServerNotifyRetryLogin")
    g_ScriptEvent:AddListener("LoadingSceneFinished", self, "OnLoadingSceneFinished")
    g_ScriptEvent:AddListener("MsgFrom3DTouch", self, "On3DTouchTriggered")
    g_ScriptEvent:AddListener("OnLaunchAppFromDeepLink", self, "OnLaunchAppFromDeepLink")
    g_ScriptEvent:AddListener("QMPKCharacterNameExist", self, "QMPKCharacterNameExist")
    g_ScriptEvent:AddListener("EnterPreLoginMode", self, "OnEnterPreLoginMode")
    g_ScriptEvent:AddListener("PreLoginModeCharacterExists", self, "OnPreLoginModeCharacterExists")
    g_ScriptEvent:AddListener("BeginCreatePlayer", self, "BeginCreatePlayer")

    if CLoginMgr.Inst.PreLoginMode then
        self:OnEnterPreLoginMode()
    end

		Shader.SetGlobalColor("_UI_AmbientColor", LuaLightMgr.m_UIAmbientColorIntensity_CharacterCreate)
		CUIManager.ModelLightEnabled = false

    CMainCamera.Inst:SetCullingGroup(false)

    -- 设置texture streaming
    local cameraObj = GameObject.Find("SceneRoot/Camera")
	local ctrl = cameraObj:GetComponent(typeof(StreamingController))
	if ctrl == nil then
		ctrl = cameraObj:AddComponent(typeof(StreamingController))
	end
	ctrl.streamingMipmapBias = -7
end

function LuaCharacterCreationWnd:LoginSceneLoaded( ... )
    self:ShowPage()
end

function LuaCharacterCreationWnd:OnDisable()

    GestureRecognizer.RemoveGestureListener(GestureType.Swipe, self.m_SwipeGestureListener)

    g_ScriptEvent:RemoveListener("OnExistingRoleInfoUpdate", self, "OnExistingRoleInfoUpdate")
    g_ScriptEvent:RemoveListener("LoginSceneLoaded", self, "LoginSceneLoaded")
    g_ScriptEvent:RemoveListener("StartUnloadScene", self, "OnStartUnloadScene")
    g_ScriptEvent:RemoveListener("ServerNotifyRetryLogin", self, "OnServerNotifyRetryLogin")
    g_ScriptEvent:RemoveListener("LoadingSceneFinished", self, "OnLoadingSceneFinished")
    g_ScriptEvent:RemoveListener("MsgFrom3DTouch", self, "On3DTouchTriggered")
    g_ScriptEvent:RemoveListener("OnLaunchAppFromDeepLink", self, "OnLaunchAppFromDeepLink")
    g_ScriptEvent:RemoveListener("QMPKCharacterNameExist", self, "QMPKCharacterNameExist")
    g_ScriptEvent:RemoveListener("EnterPreLoginMode", self, "OnEnterPreLoginMode")
    g_ScriptEvent:RemoveListener("PreLoginModeCharacterExists", self, "OnPreLoginModeCharacterExists")
    g_ScriptEvent:RemoveListener("BeginCreatePlayer", self, "BeginCreatePlayer")

    self:CancelEnterGameForExistingRoleTick()
    self:CancelEnterGameForCreateRoleTick()
    self:CancelPrepareAniTick()
    self:CancelYingLingRadarMapTick()

		Shader.SetGlobalColor("_UI_AmbientColor", LuaLightMgr.m_UIAmbientColorIntensity)
		CUIManager.ModelLightEnabled = true

    CMainCamera.Inst:SetCullingGroup(true)
end
function LuaCharacterCreationWnd:OnEnterPreLoginMode(...)
    self.m_EnterGameBtnForNewRole.Text = LocalString.GetString("预约")
    self.m_EnterGameBtnForExistingRole.Text = LocalString.GetString("预约")

    if CLoginMgr.Inst.PreLoginModeCharacterExists then
        self:OnPreLoginModeCharacterExists()
    end
end

function LuaCharacterCreationWnd:CurrentServerTeamAppointmentEnabled()
    local serverId  = CLoginMgr.Inst:GetSelectedGameServer().id
    local teamAppointmentEnabled = CLoginMgr.Inst:CheckTeamAppointment(serverId)
    return teamAppointmentEnabled
end

function LuaCharacterCreationWnd:OnPreLoginModeCharacterExists(...)
    --新服预约状态下，如果存在角色，则隐藏创建和进入游戏按钮
    local preloginCharacterExists = CLoginMgr.Inst.PreLoginModeCharacterExists
    self.m_EnterGameBtnForExistingRole.gameObject:SetActive(not preloginCharacterExists)
    self.m_EnterGameBtnForNewRole.gameObject:SetActive(not preloginCharacterExists)
    self.m_TeamAppointmentButton:SetActive(preloginCharacterExists and self:CurrentServerTeamAppointmentEnabled())

    if self.m_TeamAppointmentButton.activeSelf then
        self.m_OpenTimeLabel.transform.localPosition = self.m_OriginOpenTimeLabelPos
    else
        local pos = self.m_OriginOpenTimeLabelPos
        self.m_OpenTimeLabel.transform.localPosition = Vector3(pos.x, pos.y - 74 , pos.z)
    end
end
function LuaCharacterCreationWnd:OnDeleteRoleButtonClick(go)

    if not CSwitchMgr.DeleteCharacterEnabled then
        return
    end

    for i=1,#self.m_ExistingPortraits do
    	if self.m_ExistingPortraits[i].Selected then
            local idx = self.m_ExistingPortraits[i].IndexOfExistingCharacters
            if idx >= 0 and CLoginMgr.Inst.m_ExistingPlayerList.Count > idx then
                local playerInfo = CLoginMgr.Inst.m_ExistingPlayerList[idx]
                if playerInfo ~= nil then
                    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("Delete_Character"), DelegateFactory.Action(function ()
                        Gac2Login.RequestDeleteCharacter(playerInfo.m_BasicProp.Id)
                    end), nil, nil, nil, false)
                end
            end
            break
        end
    end
end
function LuaCharacterCreationWnd:OnSwipe(gesture)
	local delta = - (gesture.swipeVector).x / Screen.width
	self:RotateRoleModel(360 * delta)
end

function LuaCharacterCreationWnd:RotateRoleModel(degree)
    if self.m_ModelRoot == nil or self.m_ModelRoot.childCount == 0 then
        return
    end

    local t = self.m_ModelRoot:GetChild(0)
    if t ~= nil and t.transform.gameObject.activeSelf then
		local newEulerAngleY = (t.transform.localEulerAngles.y + degree + 180) % 360 - 180
		if self.m_ShowDetail then
			local data = Initialization_Init.GetData(EnumToInt(self.m_CurClass) * 100 + EnumToInt(self.m_CurGender))
			local minAngle = data and data.LoginRotationRange[0] or -50
			local maxAngle = data and data.LoginRotationRange[1] or 50
			newEulerAngleY = math.min(math.max(newEulerAngleY, minAngle), maxAngle)
		end
		if newEulerAngleY ~= t.transform.localEulerAngles.y then
			t.transform.localEulerAngles = Vector3(0, newEulerAngleY, 0)
		end
    end
end

function LuaCharacterCreationWnd:OnExistingRoleInfoUpdate(...)
    if self.m_ExistingRoleRoot.activeSelf then
        self:Init()
    end
end

function LuaCharacterCreationWnd:QMPKCharacterNameExist(args)
    local characterName = args[0] or ""
    local newName = characterName .. string.char(UnityEngine_Random(32, 126))
    if CUICommonDef.GetStrByteLength(newName) > Constants.MaxCharLimitForPlayerName then
        self.m_NameInput.value = ""
    else
        self.m_NameInput.value = newName
    end
end

function LuaCharacterCreationWnd:LoadPlayerRes(renderObj, cls, gender, appearanceData, playLiangxiang, playXianFanTransform)
    local enablecloth = QnModelPreviewer.EnableCloth
    CClientMainPlayer.LoadResource(renderObj, appearanceData, true, self.m_FxScale, 0, false, 0, false, 0, false, nil, true, enablecloth)
    self.m_AppearanceData = appearanceData
    self.m_RO.EnableDelayLoad = false
    self.m_RO.Visible = false
    self.m_RO:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function (ro)
        self.m_RO.Visible = true
        if playLiangxiang then
            local supportFeiSheng = (appearanceData.FeiShengAppearanceXianFanStatus > 0 and CClientMainPlayer.IsFashionSupportFeiSheng(appearanceData.HeadFashionId, appearanceData.BodyFashionId, appearanceData.HideHeadFashionEffect, appearanceData.HideBodyFashionEffect))
            self:PlayLiangXiang(self.m_RO, CommonDefs.EnumToEnum(appearanceData.Class, typeof(EnumClass)), CommonDefs.EnumToEnum(appearanceData.Gender, typeof(EnumGender)), appearanceData.Equipment, appearanceData.HideWeaponFashionEffect == 1 and 0 or appearanceData.WeaponFashionId, supportFeiSheng)
		elseif playXianFanTransform then
			self:PlayXianFanTransformFadeIn(self.m_RO)
        end
    end))
end

function LuaCharacterCreationWnd:PlayLiangXiang(ro, cls, gender, equipInfos, weaponFashionId, feisheng)
    --为了解决LooK At问题，先播0.1s的站立动作
    ro.AniEndCallback = nil
    ro:DoAni(ro.NewRoleStandAni, true, 0, 1, 0.15, true, 1)

    self:CancelPrepareAniTick()
    self.m_PrepareAniTick = RegisterTickOnce(function ( )
        if ro.CurrentAni == ro.NewRoleStandAni then
           self:PlayLiangXiang_Delay(ro, cls, gender, equipInfos, weaponFashionId, feisheng)
        end
        self.m_PrepareAniTick = nil
    end, 100)
end

function LuaCharacterCreationWnd:CancelPrepareAniTick()
    if self.m_PrepareAniTick then
        UnRegisterTick(self.m_PrepareAniTick)
        self.m_PrepareAniTick = nil
    end
end

function LuaCharacterCreationWnd:PlayLiangXiang_Delay(ro, cls, gender, equipInfos, weaponFashionId, feisheng)
    --播特效
    local liangxiangAni = CLoginMgr.Inst:GetLiangxiangAni(cls, gender, equipInfos, weaponFashionId)
    local fxId = CLoginMgr.Inst:GetLiangxiangFx(cls, gender, equipInfos, weaponFashionId, feisheng)
    if self.m_LiangXiangFX ~= nil then
        self.m_LiangXiangFX:Destroy()
        self.m_LiangXiangFX = nil
    end
    local zhanKuangWithoutMask = CommonDefs.IsZhanKuang(cls) and not CClientMainPlayer.HaveZhanKuangMask(self.m_AppearanceData)
    if zhanKuangWithoutMask then
        fxId = 0
        liangxiangAni = ro.NewRoleStandAni
    end

    self.m_LiangXiangFX = CEffectMgr.Inst:AddObjectFX(fxId, ro, 0, self.m_FxScale, 1, nil, false, EnumWarnFXType.None, CommonDefs.GetDefaultValue(typeof(Vector3)), CommonDefs.GetDefaultValue(typeof(Vector3)), nil)
    if self.m_LiangXiangFX ~= nil then
        ro:AddFX("liangxiangFx", self.m_LiangXiangFX, -1)
    end
    if not zhanKuangWithoutMask then
        self:CheckLiangXiangWeaponFx(ro, cls, gender, equipInfos, weaponFashionId,false)
    end
    --播音效
    if self.m_Sound~=nil then
        SoundManager.Inst:StopSound(self.m_Sound)
        self.m_Sound = nil
    end
    local data = Initialization_Init.GetData(EnumToInt(cls) * 100 + EnumToInt(gender))
    if data ~= nil and not System.String.IsNullOrEmpty(data.Sound) then
        self.m_Sound = SoundManager.Inst:PlayOneShot(data.Sound, Vector3.zero, nil, 0)
    end
    --播动作
	ro.AniEndCallback = nil
    ro:DoAni(liangxiangAni, false, 0, 1, 0.15, true, 1)
    ro.AniEndCallback = DelegateFactory.Action(function ()
        ro.AniEndCallback = nil
        ro:DoAni(ro.NewRoleStandAni, true, 0, 1, 0.15, true, 1)
        self:CheckLiangXiangWeaponFx(ro, cls, gender, equipInfos, weaponFashionId,true)
    end)
end

function LuaCharacterCreationWnd:PlayXianFanTransformFadeOut(ro, endCallback)
	ro:SetWeaponVisible(EWeaponVisibleReason.Expression, false)
	self:CancelPrepareAniTick()
	ro.AniEndCallback = nil
	ro:DoAni("transform_start", false, 0, 1, 0.15, true, 1)
    ro.AniEndCallback = DelegateFactory.Action(function()
        --播放切换特效
        local fxId = (self.m_CurGender == EnumGender.Female) and 88801201 or 88801200
        local pos = ro.transform.position
        pos.y = pos.y + 1.2 --特效上移
        pos.z = pos.z - 0.1 --特效移到人物前方
        local fx = CEffectMgr.Inst:AddWorldPositionFX(fxId,pos,0,0,1.0,-1,EnumWarnFXType.None,nil,0,0, nil)
        --播放切换音效
        SoundManager.Inst:PlaySound("event:/L10/L10_UI/UI_XianFanQieHuan",Vector3.zero, 1, nil, 0)
		ro.AniEndCallback = nil
		if endCallback then endCallback() end
    end)
end

function LuaCharacterCreationWnd:PlayXianFanTransformFadeIn(ro)
	ro:SetWeaponVisible(EWeaponVisibleReason.Expression, false)
	ro.AniEndCallback = nil
	ro:DoAni("transform_end", false, 0, 1, 0, true, 1)
	ro.AniEndCallback = DelegateFactory.Action(function()
		ro.AniEndCallback = nil
		ro:SetWeaponVisible(EWeaponVisibleReason.Expression, true)
		ro:DoAni(ro.NewRoleStandAni, true, 0, 1, 0.15, true, 1)
    end)
end

function LuaCharacterCreationWnd:CheckLiangXiangWeaponFx(ro, cls, gender, equipInfos, weaponFashionId,isEnd)
    if not ro then return end
    local tags = {"WeaponGemGroup","DunGemGroup","ShenbingWeaponFx","ShenbingWeaponFx_dup","ShenbingWeaponFx2","ShenbingWeaponFx2_dup"}
    local aniId = CAnimationMgr.Inst:GetAniId(cls, gender, equipInfos, weaponFashionId)
    local data = Animation_Animation.GetData(aniId)
    if data and data.LiangXiangDisableWeaponFx > 0 then
        for i=1,#tags do
            ro:RefreshFxVisible(tags[i],isEnd)
        end
    end
end

function LuaCharacterCreationWnd:DestoryRO()
	if self.m_RO~=nil then
		self.m_RO:Destroy()
		self.m_RO = nil
	end
end

function LuaCharacterCreationWnd:OnDetailButtonClick(go)
	if self.m_RO ~= nil and self.m_RO:IsAllFinished() and self.m_PrepareAniTick == nil then
		self:DoCreateRoleTransition(true, true)
	end
end

function LuaCharacterCreationWnd:DoCreateRoleTransition(showDetail, animated)
    self.m_ShowDetail = showDetail or false
    self.m_XianFanButtonRoot.gameObject:SetActive(not self.m_ShowDetail)--详情处隐藏仙凡切换
    local moveSpace = 2000
    if animated then
        LuaTweenUtils.SetEase(LuaTweenUtils.TweenPositionX(self.m_DetailLeftOffset.transform, self.m_ShowDetail and 0 or -moveSpace, 1), Ease.OutCubic)
        LuaTweenUtils.SetEase(LuaTweenUtils.TweenPositionX(self.m_DetailRightOffset.transform, self.m_ShowDetail and 0 or moveSpace, 1), Ease.OutCubic)
        LuaTweenUtils.SetEase(LuaTweenUtils.TweenPositionX(self.m_MasterLeftOffset.transform, self.m_ShowDetail and -moveSpace or 0, 1), Ease.OutCubic)
        LuaTweenUtils.SetEase(LuaTweenUtils.TweenPositionX(self.m_MasterRightOffset.transform, self.m_ShowDetail and moveSpace or 0, 1), Ease.OutCubic)
        if showDetail then
            self.m_CameraScript.m_LuaSelf:ForwardToFace()
			-- 停掉亮相动作
			if self.m_RO ~= nil and (self.m_PrepareAniTick or self.m_RO.CurrentAni ~= self.m_RO.NewRoleStandAni) then
                self:CheckLiangXiangWeaponFx(self.m_RO, CommonDefs.EnumToEnum(self.m_AppearanceData.Class, typeof(EnumClass)), CommonDefs.EnumToEnum(self.m_AppearanceData.Gender, typeof(EnumGender)), self.m_AppearanceData.Equipment, self.m_AppearanceData.HideWeaponFashionEffect == 1 and 0 or self.m_AppearanceData.WeaponFashionId,true)
				self:CancelPrepareAniTick()
				self.m_RO.AniEndCallback = nil
				self.m_RO:DoAni(self.m_RO.NewRoleStandAni, true, 0, 1, 0.3, true, 1)
			end
			-- 停掉特效
			if self.m_LiangXiangFX ~= nil then
				self.m_LiangXiangFX:Destroy()
				self.m_LiangXiangFX = nil
			end

			self:RotateRoleModel(0)
			self:SetXianShenSelected(false, true)
            CIKMgr.Inst:SetRenderObjectIKLookAtTo(self.m_RO, self.m_CameraScript.m_LuaSelf.m_Camera.transform)
        else
            self.m_CameraScript.m_LuaSelf:BackwardToBody()
            CIKMgr.Inst:SetRenderObjectIKLookAtTo(self.m_RO, nil)
        end
    else
        Extensions.SetLocalPositionX(self.m_DetailLeftOffset.transform, self.m_ShowDetail and 0 or -moveSpace)
        Extensions.SetLocalPositionX(self.m_DetailRightOffset.transform, self.m_ShowDetail and 0 or moveSpace)
        Extensions.SetLocalPositionX(self.m_MasterLeftOffset.transform, self.m_ShowDetail and -moveSpace or 0)
        Extensions.SetLocalPositionX(self.m_MasterRightOffset.transform, self.m_ShowDetail and moveSpace or 0)
    end
end

function LuaCharacterCreationWnd:OnXianShenButtonClick(go)
    if self.m_RO ~= nil and self.m_RO:IsAllFinished() then
        self:SetXianShenSelected(true, true)
        self.m_CameraScript.m_LuaSelf:BackwardToBody()
    end
end

function LuaCharacterCreationWnd:OnFanShenButtonClick(go)
    if self.m_RO ~= nil and self.m_RO:IsAllFinished() then
        self:SetXianShenSelected(false, true)
    end
end

function LuaCharacterCreationWnd:OnQYCodeUseButtonClick(go)
    self:ShowQYCodeInputWnd()
end

function LuaCharacterCreationWnd:ShowQYCodeInputWnd()
    CUIManager.ShowUI(CLuaUIResources.QYCodeInputBoxWnd)
end

------Begin Tick处理
function LuaCharacterCreationWnd:RegisterEnterGameForExistingRoleTick( )

    self:CancelEnterGameForExistingRoleTick()
    self.m_EnterGameForExistingRoleTick = RegisterTickOnce(function ( ... )
            if self.m_EnterGameBtnForExistingRole ~= nil then
                self.m_EnterGameBtnForExistingRole.Enabled = true
            end
        end, 5000)
end
function LuaCharacterCreationWnd:CancelEnterGameForExistingRoleTick( )
    if self.m_EnterGameForExistingRoleTick then
        UnRegisterTick(self.m_EnterGameForExistingRoleTick)
        self.m_EnterGameForExistingRoleTick = nil
    end
end

function LuaCharacterCreationWnd:RegisterEnterGameForCreateRoleTick( )

    self:CancelEnterGameForCreateRoleTick()
    self.m_EnterGameForCreateRoleTick = RegisterTickOnce(function ( ... )
            if self.m_EnterGameBtnForNewRole ~= nil then
                self.m_EnterGameBtnForNewRole.Enabled = true
            end
        end, 3000)
end
function LuaCharacterCreationWnd:CancelEnterGameForCreateRoleTick( )
    if self.m_EnterGameForCreateRoleTick then
        UnRegisterTick(self.m_EnterGameForCreateRoleTick)
        self.m_EnterGameForCreateRoleTick = nil
    end
end
------End   Tick处理
