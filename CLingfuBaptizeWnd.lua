-- Auto Generated!!
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CEquipBaptizeDescItem = import "L10.UI.CEquipBaptizeDescItem"
local CEquipmentBaptizeMgr = import "L10.Game.CEquipmentBaptizeMgr"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CLingfuBaptizeWnd = import "L10.UI.CLingfuBaptizeWnd"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CTooltip = import "L10.UI.CTooltip"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local DelegateFactory = import "DelegateFactory"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EnumLingfuBaptizeType = import "L10.UI.EnumLingfuBaptizeType"
local EnumQualityType = import "L10.Game.EnumQualityType"
local EventDelegate = import "EventDelegate"
local Extensions = import "Extensions"
local Gac2Gas = import "L10.Game.Gac2Gas"
local Int32 = import "System.Int32"
local Item_Item = import "L10.Game.Item_Item"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local QualityColor = import "L10.Game.QualityColor"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UInt32 = import "System.UInt32"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local Word_Word = import "L10.Game.Word_Word"
local ZuoQi_LingfuQianghua = import "L10.Game.ZuoQi_LingfuQianghua"
local ZuoQi_MapiTaozhuang = import "L10.Game.ZuoQi_MapiTaozhuang"
local ZuoQi_Setting = import "L10.Game.ZuoQi_Setting"

CLingfuBaptizeWnd.m_Init_CS2LuaHook = function (this) 

    if CLingfuBaptizeWnd.sCurBaptizeLingfuId == nil then
        return
    end

    local item = CItemMgr.Inst:GetById(CLingfuBaptizeWnd.sCurBaptizeLingfuId)
    if item == nil or item.Item.LingfuItemInfo == nil then
        return
    end

    this.mCurLingfuQuality = item.Item.LingfuItemInfo.Quality

    local templateId = item.Item.TemplateId
    local itemdata = Item_Item.GetData(templateId)
    if itemdata ~= nil then
        this.lingfuTexture:LoadMaterial(itemdata.Icon)
    end

    local setting = ZuoQi_Setting.GetData()
    if CLingfuBaptizeWnd.sType == EnumLingfuBaptizeType.eWord then
        this.title.text = LocalString.GetString("词条洗炼")
        this.preWashType.text = LocalString.GetString("原属性")
        this.postWashType.text = LocalString.GetString("新属性")

        this.preScoreLabel.text = CEquipmentBaptizeMgr.FormatScore(CZuoQiMgr.GetMapiLingfoScore(item.Item.LingfuItemInfo))
        this.postScoreLabel.text = CEquipmentBaptizeMgr.FormatScore(CZuoQiMgr.GetMapiLingfoLastScore(item.Item.LingfuItemInfo))

        this.mNeedItemId = setting.BaptizeItemID[0]
        this.mNeedItemCount = setting.WordBaptizeNeedNum[item.Item.LingfuItemInfo.Quality - 1]
        this.mNeedJadeCount = setting.WordBaptizeNeedJade[item.Item.LingfuItemInfo.Quality - 1]

        local extraLvl = 0
        local qianghuaData = ZuoQi_LingfuQianghua.GetData(item.Item.LingfuItemInfo.QianghuaJieshu)
        if qianghuaData ~= nil and qianghuaData.WordAdd.Length >= item.Item.LingfuItemInfo.QianghuaXingji and item.Item.LingfuItemInfo.QianghuaXingji > 0 then
            extraLvl = qianghuaData.WordAdd[item.Item.LingfuItemInfo.QianghuaXingji - 1]
        end

        local words = CreateFromClass(MakeGenericClass(List, UInt32))
        CommonDefs.DictIterate(item.Item.LingfuItemInfo.Words, DelegateFactory.Action_object_object(function (___key, ___value) 
            local p = {}
            p.Key = ___key
            p.Value = ___value
            CommonDefs.ListAdd(words, typeof(UInt32), p.Key)
        end))
        this:ShowPreWordList(words, this.mCurLingfuQuality, extraLvl)

        local postWords = CreateFromClass(MakeGenericClass(List, UInt32))
        CommonDefs.DictIterate(item.Item.LingfuItemInfo.LastWords, DelegateFactory.Action_object_object(function (___key, ___value) 
            local p = {}
            p.Key = ___key
            p.Value = ___value
            CommonDefs.ListAdd(postWords, typeof(UInt32), p.Key)
        end))
        this:ShowPostWordList(postWords, this.mCurLingfuQuality, extraLvl)

        CommonDefs.GetComponentInChildren_GameObject_Type(this.baptizeBtn, typeof(UILabel)).text = LocalString.GetString("洗词条")
        CommonDefs.GetComponentInChildren_GameObject_Type(this.replaceBtn, typeof(UILabel)).text = LocalString.GetString("替换原属性")
    elseif CLingfuBaptizeWnd.sType == EnumLingfuBaptizeType.eTaozhuang then
        this.title.text = LocalString.GetString("套装洗炼")
        this.preWashType.text = LocalString.GetString("原套装")
        this.postWashType.text = LocalString.GetString("新套装")

        local taozhuangId = item.Item.LingfuItemInfo.TaozhuangId
        local taozhuangData = ZuoQi_MapiTaozhuang.GetData(taozhuangId)
        local name = ""
        if taozhuangData ~= nil then
            name = taozhuangData.Name
        end
        this.preScoreLabel.text = name

        this.mNeedItemId = setting.BaptizeItemID[1]
        this.mNeedItemCount = setting.SuitBaptizeNeedNum[item.Item.LingfuItemInfo.Quality - 1]
        this.mNeedJadeCount = setting.SuitBaptizeNeedJade[item.Item.LingfuItemInfo.Quality - 1]

        local extraLvl = 0
        local qianghuaData = ZuoQi_LingfuQianghua.GetData(item.Item.LingfuItemInfo.QianghuaJieshu)
        if qianghuaData ~= nil and qianghuaData.TaozhuangWordAdd.Length >= item.Item.LingfuItemInfo.QianghuaXingji and item.Item.LingfuItemInfo.QianghuaXingji > 0 then
            extraLvl = qianghuaData.TaozhuangWordAdd[item.Item.LingfuItemInfo.QianghuaXingji - 1]
        end

        local words = CreateFromClass(MakeGenericClass(List, UInt32))
        if taozhuangData ~= nil then
            if taozhuangData.Suit2Attr ~= nil then
                do
                    local i = 0
                    while i < taozhuangData.Suit2Attr.Length do
                        CommonDefs.ListAdd(words, typeof(UInt32), taozhuangData.Suit2Attr[i])
                        i = i + 1
                    end
                end
            end
            if taozhuangData.Suit4Attr ~= nil then
                do
                    local i = 0
                    while i < taozhuangData.Suit4Attr.Length do
                        CommonDefs.ListAdd(words, typeof(UInt32), taozhuangData.Suit4Attr[i])
                        i = i + 1
                    end
                end
            end
            if taozhuangData.Suit6Attr ~= nil then
                do
                    local i = 0
                    while i < taozhuangData.Suit6Attr.Length do
                        CommonDefs.ListAdd(words, typeof(UInt32), taozhuangData.Suit6Attr[i])
                        i = i + 1
                    end
                end
            end
        end

        this:ShowPreWordList(words, this.mCurLingfuQuality, extraLvl)


        local postWords = CreateFromClass(MakeGenericClass(List, UInt32))
        local postName = ""
        local postTaozhuangId = item.Item.LingfuItemInfo.LastTaozhuangId
        local postTaozhuangData = ZuoQi_MapiTaozhuang.GetData(postTaozhuangId)
        if postTaozhuangData ~= nil then
            if postTaozhuangData ~= nil then
                postName = postTaozhuangData.Name
            end

            if postTaozhuangData ~= nil then
                if postTaozhuangData.Suit2Attr ~= nil then
                    do
                        local i = 0
                        while i < postTaozhuangData.Suit2Attr.Length do
                            CommonDefs.ListAdd(postWords, typeof(UInt32), postTaozhuangData.Suit2Attr[i])
                            i = i + 1
                        end
                    end
                end
                if postTaozhuangData.Suit4Attr ~= nil then
                    do
                        local i = 0
                        while i < postTaozhuangData.Suit4Attr.Length do
                            CommonDefs.ListAdd(postWords, typeof(UInt32), postTaozhuangData.Suit4Attr[i])
                            i = i + 1
                        end
                    end
                end
                if postTaozhuangData.Suit6Attr ~= nil then
                    do
                        local i = 0
                        while i < postTaozhuangData.Suit6Attr.Length do
                            CommonDefs.ListAdd(postWords, typeof(UInt32), postTaozhuangData.Suit6Attr[i])
                            i = i + 1
                        end
                    end
                end
            end
        end
        this:ShowPostWordList(postWords, this.mCurLingfuQuality, extraLvl)
        this.postScoreLabel.text = postName

        CommonDefs.GetComponentInChildren_GameObject_Type(this.baptizeBtn, typeof(UILabel)).text = LocalString.GetString("洗套装")
        CommonDefs.GetComponentInChildren_GameObject_Type(this.replaceBtn, typeof(UILabel)).text = LocalString.GetString("替换原套装")
    end

    this.moneyCtr:SetCost(this.mNeedJadeCount)

    local count = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, this.mNeedItemId)
    this.countUpdate.count = count
    this.countUpdate.templateId = this.mNeedItemId
    this:OnNeedItemCountChange(count)

    local needItemData = Item_Item.GetData(this.mNeedItemId)
    if needItemData ~= nil then
        this.needItemTexture:LoadMaterial(needItemData.Icon)
        this.needItemName.text = needItemData.Name
    end

    this.autoJadeToggle:Set(this.mbUseJade)
    this.moneyCtr.gameObject:SetActive(this.mbUseJade)
    this.needItemGO:SetActive(not this.mbUseJade)

    this.wordTemplate.gameObject:SetActive(false)
    this:UpdateReplaceBtnState(item.Item.LingfuItemInfo)
end
CLingfuBaptizeWnd.m_ShowPreWordList_CS2LuaHook = function (this, wordList, quality, extraLvl) 
    local c = CLingfuBaptizeWnd.GetWordColor(wordList.Count, quality)
    Extensions.RemoveAllChildren(this.preTable.transform)
    do
        local i = 0
        while i < wordList.Count do
            local itemcell = CommonDefs.Object_Instantiate(this.wordTemplate)
            itemcell.transform.parent = this.preTable.transform
            itemcell.transform.localScale = Vector3.one
            itemcell.gameObject:SetActive(true)

            local simpleItem = CommonDefs.GetComponent_GameObject_Type(itemcell, typeof(CEquipBaptizeDescItem))
            simpleItem:Init(wordList[i])
            simpleItem.color = c
            simpleItem:UpdateExtraDescString(CZuoQiMgr.CalcWordAttrIncreaseStr(wordList[i], extraLvl, 1))
            i = i + 1
        end
    end
    this.preTable:Reposition()
    this.preScrolView:ResetPosition()
end
CLingfuBaptizeWnd.m_GetWordColor_CS2LuaHook = function (wordCount, quality) 
    if CLingfuBaptizeWnd.sType == EnumLingfuBaptizeType.eTaozhuang then
        if quality == 1 then
            return QualityColor.GetRGBValue(EnumQualityType.Orange)
        elseif quality == 2 then
            return QualityColor.GetRGBValue(EnumQualityType.DarkBlue)
        elseif quality == 3 then
            return QualityColor.GetRGBValue(EnumQualityType.DarkRed)
        elseif quality == 4 then
            return QualityColor.GetRGBValue(EnumQualityType.DarkPurple)
        end
    end

    local c = Color.white
    if quality == 1 then
        c = QualityColor.GetRGBValue(EnumQualityType.Orange)
    elseif quality == 2 then
        if wordCount < 3 then
            c = QualityColor.GetRGBValue(EnumQualityType.Blue)
        else
            c = QualityColor.GetRGBValue(EnumQualityType.DarkBlue)
        end
    elseif quality == 3 then
        if wordCount < 4 then
            c = QualityColor.GetRGBValue(EnumQualityType.Red)
        else
            c = QualityColor.GetRGBValue(EnumQualityType.DarkRed)
        end
    elseif quality == 4 then
        if wordCount < 5 then
            c = QualityColor.GetRGBValue(EnumQualityType.Purple)
        else
            c = QualityColor.GetRGBValue(EnumQualityType.DarkPurple)
        end
    end

    return c
end
CLingfuBaptizeWnd.m_ShowPostWordList_CS2LuaHook = function (this, wordList, quality, extraLvl) 
    local c = CLingfuBaptizeWnd.GetWordColor(wordList.Count, quality)

    this.bWashNeedConfirm = false
    local importantCount = 0
    local setting = ZuoQi_Setting.GetData()

    Extensions.RemoveAllChildren(this.postTable.transform)
    do
        local i = 0
        while i < wordList.Count do
            local itemcell = CommonDefs.Object_Instantiate(this.wordTemplate)
            itemcell.transform.parent = this.postTable.transform
            itemcell.transform.localScale = Vector3.one
            itemcell.gameObject:SetActive(true)

            local simpleItem = CommonDefs.GetComponent_GameObject_Type(itemcell, typeof(CEquipBaptizeDescItem))
            simpleItem:Init(wordList[i])
            simpleItem.color = c
            simpleItem:UpdateExtraDescString(CZuoQiMgr.CalcWordAttrIncreaseStr(wordList[i], extraLvl, 1))

            do
                local j = 0
                while j < setting.ImportantWordComfirm.Length do
                    if (math.floor(wordList[i] / 100) == setting.ImportantWordComfirm[j]) then
                        importantCount = importantCount + 1
                    end
                    j = j + 1
                end
            end
            i = i + 1
        end
    end
    this.postTable:Reposition()
    this.postScrolView:ResetPosition()

    if importantCount >= setting.WordComfirmNum then
        this.bWashNeedConfirm = true
    end
end
CLingfuBaptizeWnd.m_UpdateReplaceBtnState_CS2LuaHook = function (this, data) 
    if CLingfuBaptizeWnd.sType == EnumLingfuBaptizeType.eTaozhuang then
        local lastTaozhuangId = data.LastTaozhuangId
        local taozhuangData = ZuoQi_MapiTaozhuang.GetData(lastTaozhuangId)
        CUICommonDef.SetActive(this.replaceBtn, taozhuangData ~= nil, true)
    elseif CLingfuBaptizeWnd.sType == EnumLingfuBaptizeType.eWord then
        CUICommonDef.SetActive(this.replaceBtn, data.LastWords.Count > 0, true)
    end
end
CLingfuBaptizeWnd.m_Awake_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = MakeDelegateFromCSFunction(this.TryClose, VoidDelegate, this)
    --采用新的修饰词
    UIEventListener.Get(this.replaceBtn).onClick = MakeDelegateFromCSFunction(this.TryReplaceCurWord, VoidDelegate, this)
    UIEventListener.Get(this.baptizeBtn).onClick = MakeDelegateFromCSFunction(this.TryBaptize, VoidDelegate, this)
    UIEventListener.Get(this.lingfuTexture.gameObject).onClick = MakeDelegateFromCSFunction(this.OnTextureClick, VoidDelegate, this)
    this.countUpdate.onChange = MakeDelegateFromCSFunction(this.OnNeedItemCountChange, MakeGenericClass(Action1, Int32), this)
    CommonDefs.ListAdd(this.autoJadeToggle.onChange, typeof(EventDelegate), CreateFromClass(EventDelegate, DelegateFactory.Callback(function () 
        this:OnToggleValueChanged(this.autoJadeToggle.value)
    end)))
    UIEventListener.Get(this.needItemTexture.gameObject).onClick = MakeDelegateFromCSFunction(this.OnNeedItemTextureClick, VoidDelegate, this)
end
CLingfuBaptizeWnd.m_OnNeedItemCountChange_CS2LuaHook = function (this, count) 
    if count >= this.mNeedItemCount then
        this.countUpdate.format = ("[c][00ff00]{0}[-]/" .. this.mNeedItemCount) .. "[/c]"
        this.needItemGetGO:SetActive(false)
    else
        this.countUpdate.format = ("[c][ff0000]{0}[-]/" .. this.mNeedItemCount) .. "[/c]"
        this.needItemGetGO:SetActive(true)
    end
    this.countUpdate:Reformat()
end
CLingfuBaptizeWnd.m_OnSendLingfuWashTempProperty_CS2LuaHook = function (this, zuoqiId) 
    if CLingfuBaptizeWnd.sCurBaptizeLingfuId == nil then
        return
    end
    local item = CItemMgr.Inst:GetById(CLingfuBaptizeWnd.sCurBaptizeLingfuId)
    if item == nil or CZuoQiMgr.GetLingfuPosByTemplateId(item.TemplateId) == 0 then
        return
    end
    local lingfuData = item.Item.LingfuItemInfo
    if lingfuData == nil then
        return
    end

    if zuoqiId == CLingfuBaptizeWnd.sCurBaptizeLingfuId then
        if CLingfuBaptizeWnd.sType == EnumLingfuBaptizeType.eWord then
            local extraLvl = 0
            local qianghuaData = ZuoQi_LingfuQianghua.GetData(item.Item.LingfuItemInfo.QianghuaJieshu)
            if qianghuaData ~= nil and qianghuaData.WordAdd.Length >= item.Item.LingfuItemInfo.QianghuaXingji and item.Item.LingfuItemInfo.QianghuaXingji > 0 then
                extraLvl = qianghuaData.WordAdd[item.Item.LingfuItemInfo.QianghuaXingji - 1]
            end

            local pingfen = 0
            local words = CreateFromClass(MakeGenericClass(List, UInt32))
            CommonDefs.DictIterate(lingfuData.LastWords, DelegateFactory.Action_object_object(function (___key, ___value) 
                local p = {}
                p.Key = ___key
                p.Value = ___value
                CommonDefs.ListAdd(words, typeof(UInt32), p.Key)
                if Word_Word.Exists(p.Key) then
                    pingfen = pingfen + math.floor(tonumber(Word_Word.GetData(p.Key).Mark))
                end
            end))
            this:ShowPostWordList(words, this.mCurLingfuQuality, extraLvl)
            this.postScoreLabel.text = System.String.Format(LocalString.GetString("评分：{0}"), pingfen)
        elseif CLingfuBaptizeWnd.sType == EnumLingfuBaptizeType.eTaozhuang then
            local taozhuangId = lingfuData.LastTaozhuangId
            local taozhuangData = ZuoQi_MapiTaozhuang.GetData(taozhuangId)
            if taozhuangData ~= nil then
                local extraLvl = 0
                local qianghuaData = ZuoQi_LingfuQianghua.GetData(item.Item.LingfuItemInfo.QianghuaJieshu)
                if qianghuaData ~= nil and qianghuaData.TaozhuangWordAdd.Length >= item.Item.LingfuItemInfo.QianghuaXingji and item.Item.LingfuItemInfo.QianghuaXingji > 0 then
                    extraLvl = qianghuaData.TaozhuangWordAdd[item.Item.LingfuItemInfo.QianghuaXingji - 1]
                end

                this.postScoreLabel.text = taozhuangData.Name
                local words = CreateFromClass(MakeGenericClass(List, UInt32))
                if taozhuangData ~= nil then
                    if taozhuangData.Suit2Attr ~= nil then
                        do
                            local i = 0
                            while i < taozhuangData.Suit2Attr.Length do
                                CommonDefs.ListAdd(words, typeof(UInt32), taozhuangData.Suit2Attr[i])
                                i = i + 1
                            end
                        end
                    end
                    if taozhuangData.Suit4Attr ~= nil then
                        do
                            local i = 0
                            while i < taozhuangData.Suit4Attr.Length do
                                CommonDefs.ListAdd(words, typeof(UInt32), taozhuangData.Suit4Attr[i])
                                i = i + 1
                            end
                        end
                    end
                    if taozhuangData.Suit6Attr ~= nil then
                        do
                            local i = 0
                            while i < taozhuangData.Suit6Attr.Length do
                                CommonDefs.ListAdd(words, typeof(UInt32), taozhuangData.Suit6Attr[i])
                                i = i + 1
                            end
                        end
                    end
                end
                this:ShowPostWordList(words, this.mCurLingfuQuality, extraLvl)
            end
        end

        this:UpdateReplaceBtnState(lingfuData)
    end
end
CLingfuBaptizeWnd.m_OnReplaceLingfuPropertySuccess_CS2LuaHook = function (this, zuoqiId) 
    if zuoqiId == CLingfuBaptizeWnd.sCurBaptizeLingfuId then
        this:Init()
    end
end
CLingfuBaptizeWnd.m_OnDestroy_CS2LuaHook = function (this) 
    CLingfuBaptizeWnd.sCurBaptizeLingfuId = nil
    CLingfuBaptizeWnd.sCurBaptizeZuoQiId = nil
    CLingfuBaptizeWnd.sType = EnumLingfuBaptizeType.eUnknown

    this.bWashNeedConfirm = false
end
CLingfuBaptizeWnd.m_OnNeedItemTextureClick_CS2LuaHook = function (this, go) 
    if this.mNeedItemId ~= 0 then
        local count = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, this.mNeedItemId)
        if count >= this.mNeedItemCount then
            CItemInfoMgr.ShowLinkItemTemplateInfo(this.mNeedItemId, false, nil, AlignType.Default, 0, 0, 0, 0)
        else
            CItemAccessListMgr.Inst:ShowItemAccessInfo(this.mNeedItemId, false, go.transform, CTooltip.AlignType.Left)
        end
    end
end
CLingfuBaptizeWnd.m_TryClose_CS2LuaHook = function (this, go) 
    local bNeedConfirm = false
    if CLingfuBaptizeWnd.sCurBaptizeLingfuId ~= nil then
        local item = CItemMgr.Inst:GetById(CLingfuBaptizeWnd.sCurBaptizeLingfuId)
        if item ~= nil and CZuoQiMgr.GetLingfuPosByTemplateId(item.TemplateId) ~= 0 and item.Item.LingfuItemInfo ~= nil then
            if CLingfuBaptizeWnd.sType == EnumLingfuBaptizeType.eWord and item.Item.LingfuItemInfo.LastWords.Count ~= 0 then
                bNeedConfirm = true
            elseif CLingfuBaptizeWnd.sType == EnumLingfuBaptizeType.eTaozhuang and item.Item.LingfuItemInfo.LastTaozhuangId ~= 0 then
                bNeedConfirm = true
            end
        end
    end

    if bNeedConfirm then
        local tip = g_MessageMgr:FormatMessage("Mapi_Wash_Lingfu_Close_Confirm")
        MessageWndManager.ShowOKCancelMessage(tip, DelegateFactory.Action(function () 
            this:Close()
        end), nil, nil, nil, false)
    else
        this:Close()
    end
end
CLingfuBaptizeWnd.m_TryReplaceCurWord_CS2LuaHook = function (this, go) 
    local bCanReplace = false
    if CLingfuBaptizeWnd.sCurBaptizeLingfuId ~= nil then
        local item = CItemMgr.Inst:GetById(CLingfuBaptizeWnd.sCurBaptizeLingfuId)
        if item ~= nil and CZuoQiMgr.GetLingfuPosByTemplateId(item.TemplateId) ~= 0 and item.Item.LingfuItemInfo ~= nil then
            if CLingfuBaptizeWnd.sType == EnumLingfuBaptizeType.eWord and item.Item.LingfuItemInfo.LastWords.Count ~= 0 then
                bCanReplace = true
            elseif CLingfuBaptizeWnd.sType == EnumLingfuBaptizeType.eTaozhuang and item.Item.LingfuItemInfo.LastTaozhuangId ~= 0 then
                bCanReplace = true
            end
        end
    end
    if bCanReplace and CLingfuBaptizeWnd.sCurBaptizeLingfuId ~= nil then
        local zqid = (CLingfuBaptizeWnd.sCurBaptizeZuoQiId == nil) and "" or CLingfuBaptizeWnd.sCurBaptizeZuoQiId
        Gac2Gas.RequestReplaceLingfuProperty(CLingfuBaptizeWnd.sCurBaptizeLingfuId, EnumToInt(CLingfuBaptizeWnd.sType), zqid)
    end
end
CLingfuBaptizeWnd.m_TryBaptize_CS2LuaHook = function (this, go) 
    if CLingfuBaptizeWnd.sCurBaptizeLingfuId ~= nil and (CLingfuBaptizeWnd.sType == EnumLingfuBaptizeType.eWord or CLingfuBaptizeWnd.sType == EnumLingfuBaptizeType.eTaozhuang) then
        if this.bWashNeedConfirm then
            local msg = g_MessageMgr:FormatMessage("Lingfu_Baptize_Confirm")
            MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function () 
                Gac2Gas.RequestWashLingfuProperty(CLingfuBaptizeWnd.sCurBaptizeLingfuId, EnumToInt(CLingfuBaptizeWnd.sType), this.autoJadeToggle.value)
            end), nil, nil, nil, false)
        else
            Gac2Gas.RequestWashLingfuProperty(CLingfuBaptizeWnd.sCurBaptizeLingfuId, EnumToInt(CLingfuBaptizeWnd.sType), this.autoJadeToggle.value)
        end
    end
end
