local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local QuanMinPK_Setting = import "L10.Game.QuanMinPK_Setting"
local CChatLinkMgr = import "CChatLinkMgr"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local Item_Item = import "L10.Game.Item_Item"
local CItemMgr = import "L10.Game.CItemMgr"
local MsgPackImpl = import "MsgPackImpl"
local CLingShouModelTextureLoader = import "L10.UI.CLingShouModelTextureLoader"
local MessageWndManager = import "L10.UI.MessageWndManager"
local NGUIText = import "NGUIText"
local Color = import "UnityEngine.Color"
local CItemCountUpdate = import "L10.UI.CItemCountUpdate"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local QnCheckBox = import "L10.UI.QnCheckBox"
local CLoginMgr = import "L10.Game.CLoginMgr"
local Main = import "L10.Engine.Main"
local HTTPHelper = import "L10.Game.HTTPHelper"
local Json = import "L10.Game.Utils.Json"
local ShareMgr = import "ShareMgr"
local CUITexture = import "L10.UI.CUITexture"
local CUIFx = import "L10.UI.CUIFx"
local UIVerticalLabel = import "UIVerticalLabel"
local CGetMoneyMgr = import "L10.UI.CGetMoneyMgr"
local CPopupMenuInfoMgrAlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
CLuaQMPKJiXiangWuWnd = class()
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_CostTemplateIdTbl")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_CanHuaRong")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_TotalProgress")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_Progress")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_Slider")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_ProgressLabel")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_ItemId")

RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_HuaRongSwitchBtn")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_HuaRongButton")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_EditRoot")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_NormalRoot")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_HuaRongRoot")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_EditButton")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_HelpButton")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_FeedButton")

RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_ModelTextureLoader")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_TitleLabel")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_TitleLabel2")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_SwitchButton")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_SwitchTypeLabel")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_DescLabel")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_TipLabel")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_ColorItemTemplate")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_DesignData")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_SliderColors")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_TitleColors")

RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_SkinTemplateId")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_SkinEvolveGrade")

RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_Color")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_WeaponColor")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_EditColor")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_EditWeaponColor")

RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_ColorMap")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_WeaponColorMap")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_CostItem")

RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_ColorPiXiangTbl")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_HuaRongCostTbl")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_ColorNameTbl")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_WeaponColorNameTbl")

RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_TemplateId")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_EvolveGradeForSkin")

RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_Stage")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_Type")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_Type2Name")

RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_TypeLabel")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_ColorLabel")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_WingColorLabel")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_HuaRongCostItem")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_HuaRongMoneyCost")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_DeductionBtn")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_HuaRongButton2")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_LvUpFx")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_ButtonGrid")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_DescBg")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_Tick")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_UrlCoroutine")
RegistClassMember(CLuaQMPKJiXiangWuWnd, "m_ImgUrl")

-- 拿到物品，获取extravardata，解析
function CLuaQMPKJiXiangWuWnd:Init()
    self.m_Stage = 0
    self.m_SliderColors = {NGUIText.ParseColor24("B86FEC", 0), NGUIText.ParseColor24("E46868", 0),
                           NGUIText.ParseColor24("519FFF", 0), NGUIText.ParseColor24("FFFE91", 0), Color.white}
    self.m_TitleColors = {Color.white, NGUIText.ParseColor24("FFFE91", 0), NGUIText.ParseColor24("519FFF", 0),
                          NGUIText.ParseColor24("FF5050", 0), NGUIText.ParseColor24("FF88FF", 0)}

    self.m_ItemId = g_PackageViewItemId
    self.m_CostTemplateIdTbl = {}

    self.m_ModelTextureLoader = FindChild(self.transform, "ModelTexture"):GetComponent(typeof(
        CLingShouModelTextureLoader))
    self.m_LvUpFx = FindChild(self.transform, "LvUpFx"):GetComponent(typeof(CUIFx))

    self.m_EditRoot = FindChild(self.transform, "Edit")
    self.m_NormalRoot = FindChild(self.transform, "Normal")
    self.m_HuaRongRoot = FindChild(self.transform, "HuaRong")
    self.m_NormalRoot.gameObject:SetActive(true)
    self.m_EditRoot.gameObject:SetActive(false)
    self.m_HuaRongRoot.gameObject:SetActive(false)
    self.m_TitleLabel = FindChild(self.m_NormalRoot, "TitleLabel"):GetComponent(typeof(UILabel))
    self.m_TitleLabel2 = FindChild(self.m_HuaRongRoot, "TitleLabel2"):GetComponent(typeof(UILabel))
    self.m_SwitchButton = FindChild(self.m_NormalRoot, "SwitchButton").gameObject
    self.m_SwitchTypeLabel = FindChild(self.m_NormalRoot, "SwitchTypeLab"):GetComponent(typeof(UILabel))
    self.m_DescLabel = FindChild(self.m_NormalRoot, "DescLabel"):GetComponent(typeof(UIVerticalLabel))
    self.m_DescBg = FindChild(FindChild(self.m_NormalRoot, "DescBg"), "Bg"):GetComponent(typeof(UITexture))
    self.m_TipLabel = FindChild(self.m_NormalRoot, "TipLabel"):GetComponent(typeof(UILabel))
    self.m_CostItem = FindChild(self.m_EditRoot, "CostItem").gameObject
    self.m_CostItem:SetActive(false)
    self.m_ButtonGrid = FindChild(self.m_NormalRoot, "ButtonGird"):GetComponent(typeof(UIGrid))
    self.m_HuaRongSwitchBtn = self.m_HuaRongRoot.transform:Find("Left/SwitchButton").gameObject

    local unlockButton = FindChild(self.m_EditRoot, "UnlockButton").gameObject
    UIEventListener.Get(unlockButton).onClick = DelegateFactory.VoidDelegate(function(go)

        local function unlock()
            -- 解锁
            local itemInfo = CItemMgr.Inst:GetItemInfo(self.m_ItemId)
            if itemInfo then
                Gac2Gas.JiXiangWuRequestSetColorAndWeaponColor(EnumToInt(itemInfo.place), itemInfo.pos, self.m_ItemId,
                    self.m_EditColor, self.m_EditWeaponColor)
            end
        end

        if self.m_Stage == 4 then
            local message = g_MessageMgr:FormatMessage("QMPK_UNLOCK_COLOR_CORFIRM")
            MessageWndManager.ShowConfirmMessage(message, 5, false, DelegateFactory.Action(unlock), nil)
        else
            unlock()
        end
    end)
    self.m_Type2Name = {LocalString.GetString("力量型"),LocalString.GetString("智力型"),LocalString.GetString("根骨力量型"),LocalString.GetString("根骨智力型")}
    self.m_EditButton = FindChild(self.m_NormalRoot, "EditButton").gameObject
    UIEventListener.Get(self.m_EditButton).onClick = DelegateFactory.VoidDelegate(function(go)
        -- 进入编辑状态
        self.m_NormalRoot.gameObject:SetActive(false)
        self.m_EditRoot.gameObject:SetActive(true)
        self:RefreshEditUI()
        local texture = self.m_ModelTextureLoader:GetComponent(typeof(UITexture))
        texture.width = 720
        texture.height = 720
    end)

    self.m_HelpButton = FindChild(self.m_NormalRoot, "HelpButton").gameObject
    self.m_HelpButton:SetActive(false)
    UIEventListener.Get(self.m_HelpButton).onClick = DelegateFactory.VoidDelegate(function(go)
        --
        self:OnClickHelpButton(go)

    end)
    UIEventListener.Get(self.m_SwitchButton).onClick = DelegateFactory.VoidDelegate(function(go)
        if CLuaQMPKMgr.s_NewJiXiangWu then
            self:ShowPopupMenu(self.m_SwitchButton.gameObject)
        else
            local targetType = self.m_Type == 1 and 0 or 1

            local itemInfo = CItemMgr.Inst:GetItemInfo(self.m_ItemId)
            Gac2Gas.RequestModifyJiXiangWuType(EnumToInt(itemInfo.place), itemInfo.pos, self.m_ItemId, targetType)
        end

    end)

    self.m_DescLabel.fontColor = NGUIText.ParseColor24("537CB1", 0)
    self.m_DescLabel:Display()

    self:InitNormalUI()
    self:InitEditUI()
    self:InitHuaRongUI()
end
function CLuaQMPKJiXiangWuWnd:OnClickHelpButton(go)
    local popupList = {}

    local data1 = PopupMenuItemData(LocalString.GetString("助力孵蛋"), DelegateFactory.Action_int(function(index)
        -- 分享之前先和网站组那边同步一下进度
        local itemInfo = CItemMgr.Inst:GetItemInfo(self.m_ItemId)
        if itemInfo then
            Gac2Gas.JiXiangWuRequestSyncWxProgress(EnumToInt(itemInfo.place), itemInfo.pos, self.m_ItemId, "share")
        end

        ShareBoxMgr.OpenShareBox(EShareType.JiXiangWuHatchShare, 0, {self.m_ItemId})
    end), false, nil, EnumPopupMenuItemStyle.Light)

    table.insert(popupList, data1)

    local data2 = PopupMenuItemData(LocalString.GetString("同步进度"), DelegateFactory.Action_int(function(index)
        -- 取回网站组那边微信端加的进度
        local itemInfo = CItemMgr.Inst:GetItemInfo(self.m_ItemId)
        if itemInfo then
            Gac2Gas.JiXiangWuRequestSyncWxProgress(EnumToInt(itemInfo.place), itemInfo.pos, self.m_ItemId, "sync")
        end
    end), false, nil, EnumPopupMenuItemStyle.Light)
    table.insert(popupList, data2)

    CPopupMenuInfoMgr.ShowPopupMenu(Table2Array(popupList, MakeArrayClass(PopupMenuItemData)), go.transform,
        CPopupMenuInfoMgr.AlignType.Top, 1, nil, 600, true, 296)
end

function CLuaQMPKJiXiangWuWnd:OnClickHuaRongButton(go)
    -- 进入画容状态
    self.m_NormalRoot.gameObject:SetActive(false)
    self.m_HuaRongRoot.gameObject:SetActive(true)
    self:RefreshHuaRongUI()
    local texture = self.m_ModelTextureLoader:GetComponent(typeof(UITexture))
    texture.width = 720
    texture.height = 720
end

function CLuaQMPKJiXiangWuWnd:InitNormalUI()
    self.m_Slider = FindChild(self.m_NormalRoot, "Slider"):GetComponent(typeof(UISlider))
    self.m_ProgressLabel = FindChild(self.m_Slider.transform, "ProgressLabel"):GetComponent(typeof(UILabel))
    self.m_ProgressLabel.text = ""
    self.m_HuaRongButton = FindChild(self.m_NormalRoot, "HuaRongButton").gameObject
    self.m_HuaRongButton:SetActive(false)
    UIEventListener.Get(self.m_HuaRongButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickHuaRongButton(go)
    end)

    self.m_FeedButton = FindChild(self.transform, "FeedButton").gameObject
    UIEventListener.Get(self.m_FeedButton).onClick = DelegateFactory.VoidDelegate(function(go)
        local t = {}
        local lookup = {}
        for _, costItemId in pairs(self.m_CostTemplateIdTbl) do
            local info = {
                itemTemplateId = costItemId,
                needNum = 1
            }
            table.insert(t, info)
            lookup[costItemId] = info
        end

        local setting = QuanMinPK_Setting.GetData()
        local len = setting.ZhuangTaiYaoID.Length
        local desc = setting.ZhuangTaiYaoDes

        for i = 1, len do
            local id = setting.ZhuangTaiYaoID[i - 1]
            if lookup[id] then
                lookup[id].desc = desc
            end
        end

        -- 排序
        table.sort(t, function(a, b)
            local bindCount1, notbindCount1 = CItemMgr.Inst:CalcItemCountInBagByTemplateId(a.itemTemplateId)
            local bindCount2, notbindCount2 = CItemMgr.Inst:CalcItemCountInBagByTemplateId(b.itemTemplateId)
            if bindCount1 + notbindCount1 > bindCount2 + notbindCount2 then
                return true
            elseif bindCount1 + notbindCount1 < bindCount2 + notbindCount2 then
                return false
            else
                return a.itemTemplateId < b.itemTemplateId
            end
        end)

        CLuaQMPKJiXiangWuFeedWnd.s_TargetTransform = self.m_FeedButton.transform
        CLuaQMPKJiXiangWuFeedWnd.s_Infos = t
        CLuaQMPKJiXiangWuFeedWnd.s_ItemId = self.m_ItemId
        CUIManager.ShowUI(CLuaUIResources.QMPKJiXiangWuFeedWnd)

    end)

    self:RefreshUI()

end
function CLuaQMPKJiXiangWuWnd:InitEditUI()
    local closeBtn = FindChild(self.m_EditRoot, "CloseButton2").gameObject
    UIEventListener.Get(closeBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self.m_NormalRoot.gameObject:SetActive(true)
        self.m_EditRoot.gameObject:SetActive(false)
        self:InitModelTexture(self.m_TemplateId, self.m_EvolveGradeForSkin)
        local texture = self.m_ModelTextureLoader:GetComponent(typeof(UITexture))
        texture.width = 720
        texture.height = 720
    end)
    self.m_ColorItemTemplate = FindChild(self.m_EditRoot, "ItemTemplate").gameObject
    self.m_ColorItemTemplate:SetActive(false)

end
function CLuaQMPKJiXiangWuWnd:InitHuaRongUI()
    self.m_DeductionBtn = FindChild(self.m_HuaRongRoot, "DeductionBtn"):GetComponent(typeof(QnCheckBox))
    self.m_HuaRongButton2 = FindChild(self.m_HuaRongRoot, "HuaRongButton2").gameObject
    self.m_HuaRongSwitchBtn.gameObject:SetActive(CLuaQMPKMgr.s_NewJiXiangWu)
    UIEventListener.Get(self.m_HuaRongButton2).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickHuaRongButton2(go)
    end)

    UIEventListener.Get(self.m_HuaRongSwitchBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:ShowPopupMenu(self.m_HuaRongSwitchBtn)
    end)
    local closeBtn = FindChild(self.m_HuaRongRoot, "CloseButton3").gameObject
    UIEventListener.Get(closeBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self.m_NormalRoot.gameObject:SetActive(true)
        self.m_HuaRongRoot.gameObject:SetActive(false)
        self:InitModelTexture(self.m_TemplateId, self.m_EvolveGradeForSkin)
        local texture = self.m_ModelTextureLoader:GetComponent(typeof(UITexture))
        texture.width = 720
        texture.height = 720
    end)
end
function CLuaQMPKJiXiangWuWnd:OnClickHuaRongButton2(go)
    if self.m_DeductionBtn.Selected then
        if self.m_HuaRongMoneyCost:GetOwn() < self.m_HuaRongMoneyCost:GetCost() then
            CGetMoneyMgr.Inst:GetYinLiang()
            return
        end
    elseif self.m_HuaRongMoneyCost:GetCost() > 0 then
        g_MessageMgr:ShowMessage("QMPK_JiXiangWu_HuaRong_Need_Checkoff_YinLiang")
        return
    end

    MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("QMPK_JiXiangWu_HuaRong_MakeSure",
        self.m_Type2Name[self.m_Type + 1]),
        DelegateFactory.Action(function()
            local itemInfo = CItemMgr.Inst:GetItemInfo(self.m_ItemId)
            if itemInfo then
                CUIManager.CloseUI(CLuaUIResources.QMPKJiXiangWuWnd)
                Gac2Gas.JiXiangWuRequestHuaRong(EnumToInt(itemInfo.place), itemInfo.pos, self.m_ItemId)
            end
        end), nil, LocalString.GetString("化容"), LocalString.GetString("取消"), false)
end
function CLuaQMPKJiXiangWuWnd:OnClickColorItem(go)
    local index = go.transform:GetSiblingIndex() + 1
    self.m_EditColor = index
    self.m_EditWeaponColor = self.m_WeaponColor

    local t = self.m_ColorPiXiangTbl[self.m_Type][self.m_EditColor][self.m_EditWeaponColor]
    self:InitModelTexture(t.TemplateIdForSkin, t.EvolveGradeForSkin)

    local costTbl = self:ParseColorCostItem(self.m_DesignData.ColorCostItem)

    local bottom = FindChild(self.m_EditRoot, "Bottom").gameObject
    local grid = FindChild(bottom.transform, "Grid")

    local grid1 = FindChild(self.m_EditRoot, "Grid1")
    for i = 0, grid1.childCount - 1 do
        local selected = FindChild(grid1:GetChild(i), "Selected").gameObject
        selected:SetActive(self.m_EditColor == i + 1)
    end
    CUICommonDef.ClearTransform(grid)

    if self.m_ColorMap[index] and self.m_ColorMap[index] > 0 then
        bottom:SetActive(false)
        -- 直接请求解锁
        local itemInfo = CItemMgr.Inst:GetItemInfo(self.m_ItemId)
        if itemInfo then
            Gac2Gas.JiXiangWuRequestSetColorAndWeaponColor(EnumToInt(itemInfo.place), itemInfo.pos, self.m_ItemId,
                self.m_EditColor, self.m_EditWeaponColor)
        end
    else
        bottom:SetActive(true)
        for k, v in pairs(costTbl[index]) do
            local go = NGUITools.AddChild(grid.gameObject, self.m_CostItem)
            go:SetActive(true)
            self:InitCostItem(go, k, v)
        end
        grid:GetComponent(typeof(UIGrid)):Reposition()
    end
end
function CLuaQMPKJiXiangWuWnd:InitCostItem(go, templateId, num, changeValCallback)
    local tf = go.transform
    local countUpdate = go:GetComponent(typeof(CItemCountUpdate))
    local mask = go.transform:Find("Mask").gameObject
    countUpdate.templateId = templateId
    countUpdate.format = SafeStringFormat3("{0}%d", num)
    local function OnCountChange(val)
        if val >= num then
            mask:SetActive(false)
            countUpdate.format = SafeStringFormat3("{0}/%d", num)
        else
            mask:SetActive(true)
            countUpdate.format = SafeStringFormat3("[ff0000]{0}[-]/%d", num)
        end

        if changeValCallback then
            changeValCallback(val)
        end
    end
    countUpdate.onChange = DelegateFactory.Action_int(OnCountChange)
    countUpdate:UpdateCount()

    local icon = FindChild(tf, "Icon"):GetComponent(typeof(CUITexture))
    local template = Item_Item.GetData(templateId)
    icon:LoadMaterial(template.Icon)
    UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(go)
        if countUpdate.count >= num then
            CItemInfoMgr.ShowLinkItemTemplateInfo(templateId, false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
        else
            CItemAccessListMgr.Inst:ShowItemAccessInfo(templateId, false, tf, CTooltipAlignType.Right)
        end
    end)
end

function CLuaQMPKJiXiangWuWnd:OnClickWeaponColorItem(go)
    local index = go.transform:GetSiblingIndex() + 1
    self.m_EditWeaponColor = index
    self.m_EditColor = self.m_Color

    local t = self.m_ColorPiXiangTbl[self.m_Type][self.m_EditColor][self.m_EditWeaponColor]
    self:InitModelTexture(t.TemplateIdForSkin, t.EvolveGradeForSkin)

    local costTbl = self:ParseColorCostItem(self.m_DesignData.WeaponColorCostItem)

    local bottom = FindChild(self.m_EditRoot, "Bottom").gameObject
    local grid = FindChild(bottom.transform, "Grid")
    local grid2 = FindChild(self.m_EditRoot, "Grid2")
    for i = 0, grid2.childCount - 1 do
        local selected = FindChild(grid2:GetChild(i), "Selected").gameObject
        selected:SetActive(self.m_EditWeaponColor == i + 1)
    end
    CUICommonDef.ClearTransform(grid)

    if self.m_WeaponColorMap[index] and self.m_WeaponColorMap[index] > 0 then
        bottom:SetActive(false)
        -- 直接请求解锁
        local itemInfo = CItemMgr.Inst:GetItemInfo(self.m_ItemId)
        if itemInfo then
            Gac2Gas.JiXiangWuRequestSetColorAndWeaponColor(EnumToInt(itemInfo.place), itemInfo.pos, self.m_ItemId,
                self.m_EditColor, self.m_EditWeaponColor)
        end
    else
        if costTbl then -- 数据可能为空
            bottom:SetActive(true)
            for k, v in pairs(costTbl[index]) do
                local go = NGUITools.AddChild(grid.gameObject, self.m_CostItem)
                go:SetActive(true)
                self:InitCostItem(go, k, v)
            end
            grid:GetComponent(typeof(UIGrid)):Reposition()
        end
    end
end
function CLuaQMPKJiXiangWuWnd:ParseColorCostItem(str)
    if not str then
        return nil
    end
    local costTbl = {}
    local index = 1
    for costStr in string.gmatch(str, "([^|]+)|?") do
        costTbl[index] = {}
        for itemId, costNum in string.gmatch(costStr, "(%d+),(%d+);?") do
            itemId, costNum = tonumber(itemId), tonumber(costNum)
            costTbl[index][itemId] = costNum
        end
        index = index + 1
    end
    return costTbl
end
function CLuaQMPKJiXiangWuWnd:RefreshEditUI()
    local grid1 = FindChild(self.m_EditRoot, "Grid1")
    local grid2 = FindChild(self.m_EditRoot, "Grid2")
    CUICommonDef.ClearTransform(grid1)
    CUICommonDef.ClearTransform(grid2)

    local right = FindChild(self.m_EditRoot, "Right").gameObject
    local label1 = FindChild(grid1.parent, "Label1").gameObject
    label1:GetComponent(typeof(UILabel)).text = LocalString.GetString("颜色")
    if self.m_DesignData.BodyColor and self.m_DesignData.BodyColor ~= "" then
        right:SetActive(true)
        local t = g_LuaUtil:StrSplit(self.m_DesignData.BodyColor, ",")
        for i, v in ipairs(t) do
            local item = NGUITools.AddChild(grid1.gameObject, self.m_ColorItemTemplate)
            item:SetActive(true)

            local bg = item.transform:Find("Color"):GetComponent(typeof(UITexture))
            bg.color = NGUIText.ParseColor24(v, 0)
            local current = item.transform:Find("Current").gameObject

            if i == self.m_Color then
                current:SetActive(true)
            else
                current:SetActive(false)
            end
            local lock = item.transform:Find("Lock").gameObject
            if self.m_ColorMap[i] and self.m_ColorMap[i] > 0 then
                lock:SetActive(false)
            else
                lock:SetActive(true)
            end
            local selected = item.transform:Find("Selected").gameObject
            if i == self.m_EditColor then
                selected:SetActive(true)
            else
                selected:SetActive(false)
            end

            UIEventListener.Get(item).onClick = DelegateFactory.VoidDelegate(function(go)
                self:OnClickColorItem(go)
            end)
        end
        grid1:GetComponent(typeof(UIGrid)):Reposition()

        local label2 = FindChild(grid2.parent, "Label2").gameObject
        label2:GetComponent(typeof(UILabel)).text = LocalString.GetString("翅膀")
        if self.m_DesignData.WeaponColor and self.m_DesignData.WeaponColor ~= "" then
            label2:SetActive(true)
            local t2 = g_LuaUtil:StrSplit(self.m_DesignData.WeaponColor, ",")
            for i, v in ipairs(t2) do
                local item = NGUITools.AddChild(grid2.gameObject, self.m_ColorItemTemplate)
                item:SetActive(true)
                local bg = item.transform:Find("Color"):GetComponent(typeof(UITexture))
                bg.color = NGUIText.ParseColor24(v, 0)

                local current = item.transform:Find("Current").gameObject
                if i == self.m_WeaponColor then
                    current:SetActive(true)
                else
                    current:SetActive(false)
                end
                local lock = item.transform:Find("Lock").gameObject
                if self.m_WeaponColorMap[i] and self.m_WeaponColorMap[i] > 0 then
                    lock:SetActive(false)
                else
                    lock:SetActive(true)
                end
                local selected = item.transform:Find("Selected").gameObject
                if i == self.m_EditWeaponColor then
                    selected:SetActive(true)
                else
                    selected:SetActive(false)
                end

                UIEventListener.Get(item).onClick = DelegateFactory.VoidDelegate(function(go)
                    self:OnClickWeaponColorItem(go)
                end)
            end
        else
            -- 隐藏
            label2:SetActive(false)
        end
        grid2:GetComponent(typeof(UIGrid)):Reposition()

        FindChild(self.m_EditRoot, "Table"):GetComponent(typeof(UITable)):Reposition()

    else
        -- 隐藏右侧栏吧
        right:SetActive(true)
    end

    local bottom = FindChild(self.m_EditRoot, "Bottom").gameObject
    bottom:SetActive(false)

end

function CLuaQMPKJiXiangWuWnd:RefreshHuaRongUI()
    self.m_TypeLabel = FindChild(self.m_HuaRongRoot, "TypeLabel"):GetComponent(typeof(UILabel))
    self.m_ColorLabel = FindChild(self.m_HuaRongRoot, "ColorLabel"):GetComponent(typeof(UILabel))
    self.m_WingColorLabel = FindChild(self.m_HuaRongRoot, "WingColorLabel"):GetComponent(typeof(UILabel))
    self.m_HuaRongCostItem = FindChild(self.m_HuaRongRoot, "HuaRongCostItem").gameObject
    self.m_HuaRongMoneyCost = FindChild(self.m_HuaRongRoot, "HuaRongMoneyCost"):GetComponent(typeof(CCurentMoneyCtrl))

    local costItemId, costNum, unitPrice = unpack(self.m_HuaRongCostTbl or {})
    if not (costItemId and costNum and unitPrice) then
        return
    end

    local bindItemCountInBag, notBindItemCountInBag = CItemMgr.Inst:CalcItemCountInBagByTemplateId(costItemId)
    local bagNum = bindItemCountInBag + notBindItemCountInBag
    local needNum = math.min(costNum, bagNum)
    local needSilver = (costNum - needNum) * unitPrice

    local name = self.m_DesignData.Name
    local colorName = self.m_ColorNameTbl[self.m_Color] or LocalString.GetString("默认")

    local weaponColorName, weaponColorLabAlpha
    if self.m_Stage == 5 then
        weaponColorName = self.m_WeaponColorNameTbl[self.m_WeaponColor] or LocalString.GetString("默认")
        weaponColorLabAlpha = 1
    else
        weaponColorName = LocalString.GetString("无（三变时解锁）")
        weaponColorLabAlpha = 0.3
    end

    self.m_TypeLabel.text = self.m_Type2Name[self.m_Type + 1]
    self.m_HuaRongSwitchBtn:GetComponent(typeof(UIWidget)):ResetAndUpdateAnchors()
    self.m_ColorLabel.text = colorName
    self.m_WingColorLabel.text = weaponColorName
    self.m_WingColorLabel.alpha = weaponColorLabAlpha
    self:InitCostItem(self.m_HuaRongCostItem, costItemId, costNum)
    self.m_HuaRongMoneyCost:SetCost(needSilver)
end

function CLuaQMPKJiXiangWuWnd:OnEnable()
    g_ScriptEvent:AddListener("SendItem", self, "OnSendItem")

end
function CLuaQMPKJiXiangWuWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendItem", self, "OnSendItem")

end

function CLuaQMPKJiXiangWuWnd:OnSendItem(args)
    local pItemId = args[0]
    if pItemId == self.m_ItemId then
        self:RefreshUI()
    end
end
function CLuaQMPKJiXiangWuWnd:TriggerShare(stage)
    CLuaQMPKMgr.m_ShareTemplateId = self.m_TemplateId
    CLuaQMPKMgr.m_ShareEvolveGradeForSkin = self.m_EvolveGradeForSkin
    CLuaQMPKMgr.m_ShareStage = stage
    CLuaQMPKMgr.m_Type = self.m_Type + 1

    UnRegisterTick(self.m_Tick)
    self.m_Tick = RegisterTickOnce(function()
        self:Share(2)
    end, 2000)
end

function CLuaQMPKJiXiangWuWnd:RefreshUI()
    local item = CItemMgr.Inst:GetById(self.m_ItemId)

    if item then
        local raw = item.Item.ExtraVarData.Data
        if raw then
            local list = MsgPackImpl.unpack(raw)
            local playerId = list[0]

            local itemId = list[1]
            local stage = list[2]
            local progress = list[3]
            local wxProgress = list[4]
            local type = list[5]
            local color = list[6]
            local weaponColor = list[7]
            local templateId = list[8]
            local evolveGradeForSkin = list[9]
            local colorMap = list[10]
            local weaponColorMap = list[11]

            self.m_TemplateId = templateId
            self.m_EvolveGradeForSkin = evolveGradeForSkin

            if stage ~= self.m_Stage then
                CUIManager.CloseUI(CUIResources.ItemListWnd)
            end

            if self.m_Stage > 0 and stage ~= self.m_Stage then
                self.m_LvUpFx:DestroyFx()
                self.m_LvUpFx.m_Scale = 1
                if self.m_Stage == 1 then
                    self.m_LvUpFx:LoadFx("fx/ui/prefab/UI_mgd_jianxindansui.prefab")
                else
                    self.m_LvUpFx:LoadFx("fx/ui/prefab/UI_mgd_jianxinshengji.prefab")
                end
                self:TriggerShare(stage)
            end

            if stage > 1 then
                CLuaGuideMgr.TryTriggerGuide(EnumGuideKey.QMPKJiXiangWuTypeGuide)
            end
            -- 服务器   0 - 力量型 ,1 - 智力型 ,2 - 根骨力量型,3 - 根骨智力型
            -- 网站组   1 - 力量型 ,2 - 智力型 ,3 - 根骨力量型,4 - 根骨智力型 CLuaQMPKMgr.m_Type


            self.m_Stage = stage
            if self.m_Type and self.m_Type ~= type then
                self.m_LvUpFx:DestroyFx()
                self.m_LvUpFx.m_Scale = 0.7
                self.m_LvUpFx:LoadFx("fx/ui/prefab/UI_fazhenlianhua_4.prefab")
                if type == 1 then
                    g_MessageMgr:ShowMessage("CUSTOM_STRING2",
                        LocalString.GetString("已将剑心琉璃切换为智力型"))
                elseif type == 0 then
                    g_MessageMgr:ShowMessage("CUSTOM_STRING2",
                        LocalString.GetString("已将剑心琉璃切换为力量型"))
                elseif type == 3 then
                    g_MessageMgr:ShowMessage("CUSTOM_STRING2",
                        LocalString.GetString("已将剑心琉璃切换为根骨智力型"))
                elseif type == 2 then
                    g_MessageMgr:ShowMessage("CUSTOM_STRING2",
                    LocalString.GetString("已将剑心琉璃切换为根骨力量型"))
                end
            end
            self.m_Type = type

            local designData = QuanMinPK_Stage.GetData(stage)
            self:InitData(stage)
            self.m_Progress = progress + wxProgress
            self.m_SwitchTypeLabel.text = self.m_Type2Name[type + 1]
            self.m_SwitchButton.gameObject:GetComponent(typeof(UIWidget)):ResetAndUpdateAnchors()
            self.m_DescLabel.fontColor = NGUIText.ParseColor24("537CB1", 0)
            self.m_DescLabel.text = CChatLinkMgr.TranslateToNGUIText(designData.Description, false)
            self.m_TipLabel.text = CChatLinkMgr.TranslateToNGUIText(designData.OpTip, false)
            local b = NGUIMath.CalculateRelativeWidgetBounds(self.m_DescLabel.transform)
            self.m_DescBg.height = b.size.x + 35
            if self.m_CanHuaRong then
                self.m_HuaRongButton:SetActive(true)
            else
                self.m_HuaRongButton:SetActive(false)
            end

            self.m_TitleLabel.text = designData.Name
            self.m_TitleLabel2.text = designData.Name
            self.m_TitleLabel.color = self.m_TitleColors[stage]
            self.m_TitleLabel2.color = self.m_TitleColors[stage]

            self.m_ProgressLabel.text = SafeStringFormat3("%d/%d", self.m_Progress, self.m_TotalProgress)
            self.m_Slider.value = self.m_Progress / self.m_TotalProgress
            self.m_Slider.foregroundWidget.color = self.m_SliderColors[stage]
            local label = self.m_Slider.transform:Find("Label"):GetComponent(typeof(UILabel))
            if stage == 1 then
                label.text = LocalString.GetString("孵化度")
                self.m_HelpButton:SetActive(true)
                self.m_SwitchButton:SetActive(false)
                self.m_SwitchTypeLabel.gameObject:SetActive(false)
            else
                label.text = LocalString.GetString("成长度")
                self.m_HelpButton:SetActive(false)
                self.m_SwitchButton:SetActive(true)
                self.m_SwitchTypeLabel.gameObject:SetActive(true)
            end
            if designData.ColorEditable > 0 or designData.WeaponColorEditable > 0 then
                self.m_EditButton:SetActive(true)
            else
                self.m_EditButton:SetActive(false)
            end

            if stage == 5 then -- 最终形态
                self.m_FeedButton:SetActive(false)
                self.m_Slider.gameObject:SetActive(false)
            end

            self:InitModelTexture(templateId, evolveGradeForSkin)

            self.m_ColorMap = {}
            for i = 1, colorMap.Count do
                table.insert(self.m_ColorMap, colorMap[i - 1])
            end
            self.m_WeaponColorMap = {}
            for i = 1, weaponColorMap.Count do
                table.insert(self.m_WeaponColorMap, weaponColorMap[i - 1])
            end

            self.m_Color = color
            self.m_WeaponColor = weaponColor
            self.m_EditColor = color
            self.m_EditWeaponColor = weaponColor

            if self.m_EditRoot.gameObject.activeSelf then
                -- 刷新状态
                self:RefreshEditUI()
            end

            if self.m_HuaRongRoot.gameObject.activeSelf then
                self:RefreshHuaRongUI()
            end

            self.m_ButtonGrid:Reposition()
        end
    end
end

function CLuaQMPKJiXiangWuWnd:InitData(stage)
    local designData = QuanMinPK_Stage.GetData(stage)
    self.m_DesignData = designData

    self.m_ColorNameTbl = designData.ColorDes and g_LuaUtil:StrSplit(designData.ColorDes, ",") or {}
    self.m_WeaponColorNameTbl = designData.WeaponDes and g_LuaUtil:StrSplit(designData.WeaponDes, ",") or {}

    self.m_TotalProgress = designData.TotalProgress

    self.m_CanHuaRong = designData.CanHuaRong > 0

    if self.m_CanHuaRong then
        local huarongCostItemId, num, unitPrice = string.match(designData.HuaRongCost, "(%d+),(%d+),(%d+);?")
        self.m_HuaRongCostTbl = {tonumber(huarongCostItemId), tonumber(num), tonumber(unitPrice)}
    else
        self.m_HuaRongCostTbl = nil
    end

    self.m_CostTemplateIdTbl = {}
    for itemId, prob, progress in string.gmatch(designData.CostItem, "(%d+),([%d.]+),(%d+)") do
        itemId, prob, progress = tonumber(itemId), tonumber(prob), tonumber(progress)
        table.insert(self.m_CostTemplateIdTbl, itemId)
    end

    self.m_ColorPiXiangTbl = {}
    for tp, color, weaponColor, lingshouId, evolveGrade in string.gmatch(designData.Color2PiXiang,
        "(%d+),(%d+),(%d+),(%d+),(%d+);?") do
        tp, color, weaponColor, lingshouId, evolveGrade = tonumber(tp), tonumber(color), tonumber(weaponColor),
            tonumber(lingshouId), tonumber(evolveGrade)
        self.m_ColorPiXiangTbl[tp] = self.m_ColorPiXiangTbl[tp] or {}
        self.m_ColorPiXiangTbl[tp][color] = self.m_ColorPiXiangTbl[tp][color] or {}
        self.m_ColorPiXiangTbl[tp][color][weaponColor] = {
            TemplateIdForSkin = lingshouId,
            EvolveGradeForSkin = evolveGrade
        }
    end
end

function CLuaQMPKJiXiangWuWnd:InitModelTexture(templateId, evolveGradeForSkin)
    if self.m_SkinTemplateId ~= templateId or self.m_SkinEvolveGrade ~= evolveGradeForSkin then
        self.m_SkinTemplateId = templateId
        self.m_SkinEvolveGrade = evolveGradeForSkin
        self.m_ModelTextureLoader:Init(templateId, 0, evolveGradeForSkin)
    end
end
--  网站组 1表示力量型,2表示智力型,3表示根骨智力型,4表示根骨力量型
function CLuaQMPKJiXiangWuWnd:Share(type)
    if self.m_UrlCoroutine ~= nil then
        Main.Inst:StopCoroutine(self.m_UrlCoroutine)
        self.m_UrlCoroutine = nil
    end

    local url = CLuaQMPKMgr.m_ShareUrl .. "shareImg/mascot?"
    local inst = CClientMainPlayer.Inst
    if inst then
        local serverName = CLoginMgr.Inst:GetSelectedGameServer().name
        url = url ..
                  SafeStringFormat3(
                "roleid=%s&rolename=%s&gender=%s&careerid=%s&servername=%s&grade=%s&stageid=%s&type=%s", inst.Id,
                inst.Name, EnumToInt(inst.Gender), EnumToInt(inst.Class), serverName, inst.Level,
                CLuaQMPKMgr.m_ShareStage, CLuaQMPKMgr.m_Type)
    end

    self.m_UrlCoroutine = Main.Inst:StartCoroutine(HTTPHelper.GetUrl(url,
        DelegateFactory.Action_bool_string(function(success, ret)
            if success then
                local dict = Json.Deserialize(ret)
                local code = CommonDefs.DictGetValue_LuaCall(dict, "code")
                if code and code == 1 then
                    -- 成功
                    self.m_ImgUrl = CommonDefs.DictGetValue_LuaCall(dict, "imgurl")
                    if self.m_ImgUrl then
                        if type == 1 then
                            local id = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
                            local origin_id = id
                            CLuaShareMgr.ShareQMPKImage2PersonalSpace(id, origin_id, self.m_ImgUrl)
                        else
                            ShareMgr.ShareWebImage2Other(self.m_ImgUrl)
                        end
                    end
                end
            else
            end
            self.m_UrlCoroutine = nil
        end)))
end

function CLuaQMPKJiXiangWuWnd:GetGuideGo(methodName)
    if methodName == "GetSwitchButton" then
        return self.m_SwitchButton
    end
    return nil
end

function CLuaQMPKJiXiangWuWnd:OnDestroy()
    UnRegisterTick(self.m_Tick)
end

function CLuaQMPKJiXiangWuWnd:ShowPopupMenu(target)
    
    local itemInfo = CItemMgr.Inst:GetItemInfo(self.m_ItemId)
    local btnList = {}
    btnList[1] = {text = LocalString.GetString("力量型"), callbackFunc = function()
        Gac2Gas.RequestModifyJiXiangWuType(EnumToInt(itemInfo.place), itemInfo.pos, self.m_ItemId, 0)
    end}
    btnList[3] = {text = LocalString.GetString("根骨力量型"), callbackFunc = function()
        Gac2Gas.RequestModifyJiXiangWuType(EnumToInt(itemInfo.place), itemInfo.pos, self.m_ItemId, 2)
    end}
    btnList[2] = {text = LocalString.GetString("智力型"), callbackFunc = function()
        Gac2Gas.RequestModifyJiXiangWuType(EnumToInt(itemInfo.place), itemInfo.pos, self.m_ItemId, 1)
    end}
    btnList[4] = {text = LocalString.GetString("根骨智力型"), callbackFunc = function()
        Gac2Gas.RequestModifyJiXiangWuType(EnumToInt(itemInfo.place), itemInfo.pos, self.m_ItemId, 3)
    end}
    LuaCommonHorizontalPopupMenuMgr:ShowHorizontalPopupMenuWnd(btnList, self.m_Type + 1, CPopupMenuInfoMgrAlignType.Right, target,0)
end

return CLuaQMPKJiXiangWuWnd
