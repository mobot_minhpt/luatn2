-- Auto Generated!!
local CommonDefs = import "L10.Game.CommonDefs"
local CTeamQuickJoinDetailItem = import "L10.UI.CTeamQuickJoinDetailItem"
local CTeamQuickJoinInfoMgr = import "L10.UI.CTeamQuickJoinInfoMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local TeamMatch_Activities = import "L10.Game.TeamMatch_Activities"
CTeamQuickJoinDetailItem.m_Init_CS2LuaHook = function (this, info, activityId) 

    this.iconTexture:LoadNPCPortrait(CUICommonDef.GetPortraitName(info.LeaderClass, info.LeaderGender, info.LeaderExpression), false)
    this.expressionTxt:LoadMaterial(CUICommonDef.GetExpressionTxtPath(info.LeaderExpressionTxt))
    this.playerNameLabel.text = info.LeaderName
    this.playerLevelLabel.text = tostring(info.LeaderGrade)
    local activity = TeamMatch_Activities.GetData(activityId)
    this.activityNameLabel.text = (activity == nil and "" or activity.Name)
    this.teamInfoLabel.text = System.String.Format("{0}/{1}", info.TeamMemberCount, info.MaxTeamMemberCount)
    this.progressBar.value = info.TeamMemberCount / math.max(1, info.MaxTeamMemberCount)
    this.teamId = info.TeamId
    this.applyBtn.Enabled = not CommonDefs.DictContains(CTeamQuickJoinInfoMgr.Inst.TeamAppliedDict, typeof(Int32), this.teamId)
    local default
    if this.applyBtn.Enabled then
        default = LocalString.GetString("申请")
    else
        default = LocalString.GetString("已申请")
    end
    this.applyBtn.Text = default
end
CTeamQuickJoinDetailItem.m_OnRequestJoinMatchingTeamCallback_CS2LuaHook = function (this, teamId) 
    if this.teamId == teamId then
        this.applyBtn.Enabled = not CommonDefs.DictContains(CTeamQuickJoinInfoMgr.Inst.TeamAppliedDict, typeof(Int32), teamId)
        local default
        if this.applyBtn.Enabled then
            default = LocalString.GetString("申请")
        else
            default = LocalString.GetString("已申请")
        end
        this.applyBtn.Text = default
    end
end
