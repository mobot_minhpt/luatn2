local UISprite = import "UISprite"
local LocalString = import "LocalString"
local Extensions = import "Extensions"
local CommonDefs = import "L10.Game.CommonDefs"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CButton = import "L10.UI.CButton"
local UITable = import "UITable"
local UIScrollView = import "UIScrollView"
local QnCheckBox = import "L10.UI.QnCheckBox"
local CAchievementMgr = import "L10.Game.CAchievementMgr"
local CUIFx = import "L10.UI.CUIFx"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local Vector4 = import "UnityEngine.Vector4"

LuaAchievementDetailWnd = class()
RegistClassMember(LuaAchievementDetailWnd,"m_TitleLabel")
RegistClassMember(LuaAchievementDetailWnd,"m_HideCheckbox")

RegistClassMember(LuaAchievementDetailWnd,"m_MasterScrollView")
RegistClassMember(LuaAchievementDetailWnd,"m_MasterTable")
RegistClassMember(LuaAchievementDetailWnd,"m_SubTabTemplate")

RegistClassMember(LuaAchievementDetailWnd,"m_DetailScrollView")
RegistClassMember(LuaAchievementDetailWnd,"m_DetailTable")
RegistClassMember(LuaAchievementDetailWnd,"m_AchieveTemplate")
RegistClassMember(LuaAchievementDetailWnd,"m_SubTabTable")

RegistClassMember(LuaAchievementDetailWnd,"m_GroupIdsTbl")
RegistClassMember(LuaAchievementDetailWnd,"m_CurSubTabName")


function LuaAchievementDetailWnd:Awake()
	self.m_TitleLabel = self.transform:Find("Wnd_Bg_Secondary_1/TitleLabel"):GetComponent(typeof(UILabel))
    self.m_HideCheckbox = self.transform:Find("Anchor/MasterView/HideCheckbox"):GetComponent(typeof(QnCheckBox))

    self.m_HideCheckbox.Selected = CAchievementMgr.Inst.HideFinishAchievement
    self.m_HideCheckbox.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:OnHideCheckboxClick(btn)
    end)

    self.m_MasterScrollView = self.transform:Find("Anchor/MasterView/ScrollView"):GetComponent(typeof(UIScrollView))
    self.m_MasterTable = self.m_MasterScrollView.transform:Find("Table"):GetComponent(typeof(UITable))
    self.m_SubTabTemplate = self.m_MasterScrollView.transform:Find("SubTabTemplate").gameObject

    self.m_DetailScrollView = self.transform:Find("Anchor/DetailView/ScrollView"):GetComponent(typeof(UIScrollView))
    self.m_DetailTable = self.m_DetailScrollView.transform:Find("Table"):GetComponent(typeof(UITable))
    self.m_AchieveTemplate = self.m_DetailScrollView.transform:Find("AchieveTemplate").gameObject

    self.m_SubTabTemplate:SetActive(false)
    self.m_AchieveTemplate:SetActive(false)
end

function LuaAchievementDetailWnd:Init()
	self.m_TitleLabel.text = LuaAchievementMgr.m_CurAchieveTab
	self.m_GroupIdsTbl = LuaAchievementMgr:GetSubTabsByCurAchievementTab()

	Extensions.RemoveAllChildren(self.m_MasterTable.transform)
	self.m_SubTabTable = {}
	local defaultSelectedGo = nil
	local defaultData = nil
	local firstGo = nil
	local subTabMoveDis = 0
	for __, data in pairs(self.m_GroupIdsTbl) do
		local go = CUICommonDef.AddChild(self.m_MasterTable.gameObject, self.m_SubTabTemplate)
		go:SetActive(true)
		go:GetComponent(typeof(CButton)).Text = data.key
		CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(function(go) self:OnSubTabClick(go, data) end), false)
		table.insert(self.m_SubTabTable, go)

		if defaultSelectedGo == nil then
			defaultSelectedGo = go
			defaultData = data
			firstGo = go
		end
		--首次打开时选中指定的SubTabName，如果没有就默认选中第一个
		if data.key == LuaAchievementMgr.m_CurAchieveSubTab then
			defaultSelectedGo = go
			defaultData = data
		end
	end
	self.m_MasterTable:Reposition()
	self.m_MasterScrollView:ResetPosition()

	if defaultSelectedGo and firstGo then
		--计算默认选中的SubTab距离第一个subTab的距离
		subTabMoveDis = firstGo.transform.localPosition.y - defaultSelectedGo.transform.localPosition.y
		--移动到默认选中的SubTab
		self.m_MasterScrollView:MoveRelative(Vector3(0, subTabMoveDis, 0))
		self.m_MasterScrollView:RestrictWithinBounds(true, true, true)
	end

	if #self.m_SubTabTable>0 then
		self:OnSubTabClick(defaultSelectedGo, defaultData)
	end
end

function LuaAchievementDetailWnd:OnHideCheckboxClick(btn)
    CAchievementMgr.Inst.HideFinishAchievement = self.m_HideCheckbox.Selected
    self:Refresh()
end

function LuaAchievementDetailWnd:Refresh( )
	if self.m_CurSubTabName and self.m_GroupIdsTbl then

    	for __,data in pairs(self.m_GroupIdsTbl) do
    		if data.key == self.m_CurSubTabName then
    			self:InitGroups(self.m_CurSubTabName, data.value)
    			break
    		end
    	end
    end
end

function LuaAchievementDetailWnd:OnSubTabClick(go, data)
	for __,tabGo in pairs(self.m_SubTabTable) do
		if tabGo == go then
			tabGo:GetComponent(typeof(CButton)).Selected = true
			--初始化列表
			self:InitGroups(data.key, data.value)
		else
			tabGo:GetComponent(typeof(CButton)).Selected = false
		end
	end
end

function LuaAchievementDetailWnd:InitGroups(subTabName, groupIdTbl)
	self.m_CurSubTabName = subTabName

	Extensions.RemoveAllChildren(self.m_DetailTable.transform)

	--重排序
	self:SortGroups(groupIdTbl)

	for __, groupId in pairs(groupIdTbl) do
		if not LuaAchievementMgr:HideFinishedAchievement() or not LuaAchievementMgr:MainPlayerFinishAllAchievementsAndGetAllAwardsInGroup(groupId) then
			local go = CUICommonDef.AddChild(self.m_DetailTable.gameObject, self.m_AchieveTemplate)
			go:SetActive(true)
			self:InitAchievementItem(go, groupId)
		end
	end

	self.m_DetailTable:Reposition()
	self.m_DetailScrollView:ResetPosition()
end

function LuaAchievementDetailWnd:SortGroups(groupIdTbl)
	local playerAchievement = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.Achievement
    table.sort(groupIdTbl,function(id1,id2)

        --把打开窗口时默认要求选中的GroupId对应的内容放在首位
        if id1 == LuaAchievementMgr.m_CurGroupId and id2 ~= LuaAchievementMgr.m_CurGroupId then
            return true
        end
        if id2 == LuaAchievementMgr.m_CurGroupId and id1 ~= LuaAchievementMgr.m_CurGroupId then
            return false
        end
        if not LuaAchievementMgr:ContainsGroup(id1) then
            return false
        end
        if not LuaAchievementMgr:ContainsGroup(id2) then
            return true
        end

        local a1, data1 = LuaAchievementMgr:GetCurrentNoneAwardedAchievement(id1)
        local a2, data2 = LuaAchievementMgr:GetCurrentNoneAwardedAchievement(id2)

        local group1Score = 0
        -- 已完成尚未领奖取值1 ，未完成取值2，已完成已领奖取值3
        if playerAchievement:HasAchievement(a1) then
            if playerAchievement:HasAwardFinished(a1) then
                group1Score = 3
            else
                group1Score = 1
            end
        else
            group1Score = 2
        end

        local group2Score = 0
        -- 已完成尚未领奖取值1 ，未完成取值2，已完成已领奖取值3
        if playerAchievement:HasAchievement(a2) then
            if playerAchievement:HasAwardFinished(a2) then
                group2Score = 3
            else
                group2Score = 1
            end
        else
            group2Score = 2
        end
        --优先已完成尚未领奖,其次进行中，最后是已完成且已领奖
        if group1Score~=group2Score then

            return group1Score<group2Score

        else
            return a1 < a2
        end
    end)
end

function LuaAchievementDetailWnd:InitAchievementItem(go, groupId)
	local bg = go:GetComponent(typeof(UISprite))
	local nameLabel = go.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
	local progressLabel = go.transform:Find("ProgressLabel"):GetComponent(typeof(UILabel))
	local starTable = go.transform:Find("Stars"):GetComponent(typeof(UITable))
	local starTemplate = go.transform:Find("StarTemplate").gameObject
	local awardIcon = go.transform:Find("ProgressBar/AwardIcon"):GetComponent(typeof(UISprite))
	local awardYuanBaoLabel = go.transform:Find("ProgressBar/AwardIcon/YuanBaoLabel"):GetComponent(typeof(UILabel))
	local progressSprite = go.transform:Find("ProgressBar"):GetComponent(typeof(UIWidget))
	local progressFx = go.transform:Find("ProgressBar/Fx"):GetComponent(typeof(CUIFx))
	local conditionLabel = go.transform:Find("ConditionLabel"):GetComponent(typeof(UILabel))
	local titleLabel = go.transform:Find("TitleLabel"):GetComponent(typeof(UILabel))
	local expirationLabel = go.transform:Find("ExpirationLabel"):GetComponent(typeof(UILabel))
	local expirationIcon = go.transform:Find("ExpirationLabel/Icon").gameObject
	local finishedIcon = go.transform:Find("FinishedIcon").gameObject

	starTemplate:SetActive(false)
	expirationLabel.gameObject:SetActive(false)
	finishedIcon:SetActive(false)
	CommonDefs.AddOnClickListener(expirationLabel.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnExpirationTimeInfoClick(go) end), false)
    CommonDefs.AddOnClickListener(expirationIcon, DelegateFactory.Action_GameObject(function(go) self:OnExpirationTimeInfoClick(go) end), false)
    CommonDefs.AddOnClickListener(progressSprite.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnAchievementAwardItemClick(go, groupId) end), false)

    local curAchievementId, achievement = LuaAchievementMgr:GetCurrentNoneAwardedAchievement(groupId)
    local curLevel = LuaAchievementMgr:GetFinishedAchievementCount(groupId)
    if achievement == nil then
        return
    end

    nameLabel.text = achievement.Name
    conditionLabel.text = achievement.RequirementDisplay
	if not System.String.IsNullOrEmpty(achievement.DateDisplay) then
        expirationLabel.gameObject:SetActive(true)
        expirationLabel.text = SafeStringFormat3(LocalString.GetString("有效期 %s"), achievement.DateDisplay)
    end

    --目前仅奖励元宝或者称号
    awardIcon.gameObject:SetActive(true)
    titleLabel.gameObject:SetActive(false)
    awardYuanBaoLabel.text = ""
		if achievement.YueLiReward > 0 then
			if achievement.YuanBaoReward > 0 then
				awardIcon.spriteName = "talismanfxexchangewnd_douyuanbao"
			else
				awardIcon.spriteName = "talismanfxexchangewnd_dou"
			end
			titleLabel.gameObject:SetActive(false)
    elseif achievement.YuanBaoReward>0 then
    	if achievement.TitleReward > 0 then
    		--奖励元宝和称号
    		awardIcon.spriteName = "packagewnd_chenghaoandyuanbao"
    		titleLabel.gameObject:SetActive(true)
    		local title = Title_Title.GetData(achievement.TitleReward)
        	titleLabel.text = title.Name
    	else
    		--奖励元宝
    		awardIcon.spriteName = "packagewnd_yuanbao"
    	end
    	awardYuanBaoLabel.text = tostring(achievement.YuanBaoReward)
		elseif achievement.TitleReward > 0 then
			--奖励称号
			awardIcon.spriteName = "packagewnd_chenghao"
			titleLabel.gameObject:SetActive(true)
			local title = Title_Title.GetData(achievement.TitleReward)
			titleLabel.text = title.Name
		else
    	--没有奖励
    	awardIcon.gameObject:SetActive(false)
    end

    --成就进度
    local progress = LuaAchievementMgr:GetAchievementProgress(curAchievementId)
    local totalProgress = 0
    if not System.String.IsNullOrEmpty(achievement.NeedProgress) then
        local strs = CommonDefs.StringSplit_ArrayChar(CommonDefs.StringSplit_ArrayChar(achievement.NeedProgress, ";")[0], ",")
        totalProgress = System.Int32.Parse(strs[strs.Length - 1])
    end
    local finished = false
    local rewarded = true
    if CClientMainPlayer.Inst.PlayProp.Achievement:HasAchievement(curAchievementId) then
    	--达成成就
    	finished = true
    	rewarded = true
        if not CClientMainPlayer.Inst.PlayProp.Achievement:HasAwardFinished(curAchievementId) then
        	--尚未领取奖励
        	rewarded = false
        else
        	--已经领取奖励
        	rewarded = true
        end
		if achievement.CompletedDisplay and achievement.CompletedDisplay ~= "" then
			if achievement.ID == 61000269 then
				conditionLabel.text = SafeStringFormat3(achievement.CompletedDisplay, tostring(CClientMainPlayer.Inst.PlayProp.FeiShengData.FeiShengLevel))
			else
				conditionLabel.text = achievement.CompletedDisplay
			end
		end
    else
    	--未达成成就
        finished = false
        rewarded = false
    end

    self:InitProgress(progressLabel, progressSprite, finishedIcon, progressFx, progress, totalProgress, finished, rewarded)

    self:InitStar(starTable, starTemplate, LuaAchievementMgr:GetGroupAchievements(groupId).Count, curLevel)
end

function LuaAchievementDetailWnd:InitProgress(progressLabel, progressSprite, finishedIcon, progressFx, progress, totalProgress, finished, rewarded)
	local text = finished and "" or LocalString.GetString("进行中 ")
    if not (totalProgress>0) then totalProgress = 1 end
    if finished then progress = totalProgress end --完成的成就进度服务器会清零，这里需要特殊处理一下
	if (math.floor(progress * 10)) % 10 > 0 then --从之前的逻辑拷贝过来，意义未明
        progressLabel.text = System.String.Format("{0} {1}/{2}", text, NumberComplexToString(progress, typeof(Single), "F1"), totalProgress)
    else
        progressLabel.text = System.String.Format("{0} {1}/{2}", text, math.floor(progress), totalProgress)
    end

    if totalProgress ~= 0 then
        progressSprite.fillAmount = progress / totalProgress
    else
        progressSprite.fillAmount = finished and 1 or 0
    end

    finishedIcon:SetActive(finished)

    if finished and not rewarded then
        local tex = progressFx:GetComponent(typeof(UITexture))
	    progressFx:LoadFx(CUIFxPaths.CommonBlueLineFxPath)
        progressFx.ScrollView = self.m_DetailScrollView
        CUIFx.DoAni(Vector4(-tex.width * 0.5, tex.height*0.5, tex.width, tex.height), false, progressFx)
	else
		progressFx:DestroyFx()
	end

	if finished and rewarded then
		CUICommonDef.SetGrey(progressSprite.gameObject, true)
	else
		CUICommonDef.SetGrey(progressSprite.gameObject, false)
	end
end

function LuaAchievementDetailWnd:InitStar(starTable, starTemplate, totalCount, finishedCount)
	Extensions.RemoveAllChildren(starTable.transform)
	for i=0, totalCount-1 do
        local instance = CUICommonDef.AddChild(starTable.gameObject,  starTemplate)
        instance:GetComponent(typeof(UISprite)).spriteName = i < finishedCount and AllSpriteNames.achievement_star_highlight or AllSpriteNames.achievement_star_normal
        instance:SetActive(true)
    end
    starTable:Reposition()
end

function LuaAchievementDetailWnd:OnExpirationTimeInfoClick(go)
	g_MessageMgr:ShowMessage("Achievement_Expiration_Time_Tips")
end

function LuaAchievementDetailWnd:OnAchievementAwardItemClick(go, groupId)

	local achievementId = LuaAchievementMgr:GetCurrentNoneAwardedAchievement(groupId)

	if CClientMainPlayer.Inst.PlayProp.Achievement:HasAchievement(achievementId) then
		--已完成
        if not CClientMainPlayer.Inst.PlayProp.Achievement:HasAwardFinished(achievementId) then
        	--未领取奖励
            self:RequestGetAwards(achievementId)
        else
        	--已领取奖励
        	self:ShowAwardsTip(go, achievementId)
        end
	else
		self:ShowAwardsTip(go, achievementId)
	end
end

function LuaAchievementDetailWnd:RequestGetAwards(achievementId)
	Gac2Gas.PlayerGetAchievementAward(achievementId, "")
end

function LuaAchievementDetailWnd:ShowAwardsTip(go, achievementId)
	LuaAchievementMgr:ShowAwardsTip(achievementId, false, go.transform.position, 120, 120)
end

function LuaAchievementDetailWnd:OnEnable()
    g_ScriptEvent:AddListener("GetAchievementAwardSuccess", self, "GetAchievementAwardSuccess")
    g_ScriptEvent:AddListener("AddPlayerAchievement", self, "AddPlayerAchievement")
    g_ScriptEvent:AddListener("UpdatePlayerAchievementProgress", self, "UpdatePlayerAchievementProgress")
end

function LuaAchievementDetailWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GetAchievementAwardSuccess", self, "GetAchievementAwardSuccess")
    g_ScriptEvent:RemoveListener("AddPlayerAchievement", self, "AddPlayerAchievement")
    g_ScriptEvent:RemoveListener("UpdatePlayerAchievementProgress", self, "UpdatePlayerAchievementProgress")
end

function LuaAchievementDetailWnd:OnDestroy()
	LuaAchievementMgr:ClearCurAchieveTabInfo()--清理数据
end

function LuaAchievementDetailWnd:GetAchievementAwardSuccess(args)
	local achievementId = args[0]
    local achievement = Achievement_Achievement.GetData(achievementId)
    if achievement.Group == LuaAchievementMgr.m_CurGroupId then
        local id,data = LuaAchievementMgr:GetCurrentNoneAwardedAchievement(achievement.Group)
        if not CAchievementMgr.Inst:MainPlayerHasAwardedAchievement(id) then
            LuaAchievementMgr.m_CurGroupId = 0 --如果已经领取奖励的group恰好是当前指定的置顶group，没有奖励可领取后取消置顶设置
        end
    end
    self:Refresh()
end

function LuaAchievementDetailWnd:AddPlayerAchievement(args)
	local achievementId = args[0]
    self:Refresh()
end

function LuaAchievementDetailWnd:UpdatePlayerAchievementProgress(args)
	local achievementId = args[0]
    self:Refresh()
end
