local Time = import "UnityEngine.Time"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local LocalString = import "LocalString"
local DelegateFactory = import "DelegateFactory"
local CChatMgr = import "L10.Game.CChatMgr"
local SendChatMsgResult = import "L10.Game.SendChatMsgResult"
local CCommonPlayerListMgr = import "L10.Game.CCommonPlayerListMgr"
local CIMMgr = import "L10.Game.CIMMgr"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local EnumJingLingShareType = import "L10.Game.EnumJingLingShareType"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local Vector3 = import "UnityEngine.Vector3"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"
local UICommonDef = import "L10.UI.CUICommonDef"
local MessageMgr = import "L10.Game.MessageMgr"

EnumInGameShareChannelDefine = {
    Undefined = 0,
    eWorld = 0x01,
    eGuild = 0x02,
    eTeam = 0x04,
    eCurrent = 0x08,
    eIM = 0x10,
    ePersonalSpace = 0x20,
    eSnapshot = 0x40,
    eAppShare = 0x80,

    eXingGuanShare = CommonDefs.IS_VN_CLIENT and 0x1f or 0x3f, --(eWorld | eGuild | eTeam | eCurrent | eIM) or (eWorld | eGuild | eTeam | eCurrent | eIM | ePersonalSpace)
    eJingLingShare = CommonDefs.IS_VN_CLIENT and 0x5f or 0x7f,   --(eWorld | eGuild | eTeam | eCurrent | eIM | eSnapshot) or (eWorld | eGuild | eTeam | eCurrent | eIM | ePersonalSpace | eSnapshot)
    eWebLinkShare = CommonDefs.IS_VN_CLIENT and 0x9f or 0xbf,  -- (eWorld | eGuild | eTeam | eCurrent | eIM | eSnapshot | eAppShare) or (eWorld | eGuild | eTeam | eCurrent | eIM | ePersonalSpace | eSnapshot | eAppShare)
    eGuildLeagueCrossShare = 0x03, -- eWorld | eGuild
    eGuildLeagueCrossGambleRewardShare = 0x42, -- eGuild | eSnapshot
}

LuaInGameShareMgr = class()

LuaInGameShareMgr.m_LastShareTime = 0
LuaInGameShareMgr.m_ShareInterval = 1
LuaInGameShareMgr.m_EnableRestrictShareFrequency = true



function LuaInGameShareMgr.CanShare()
    --限制分享的调用频率
    if LuaInGameShareMgr.m_EnableRestrictShareFrequency then
        if Time.realtimeSinceStartup - LuaInGameShareMgr.m_LastShareTime < LuaInGameShareMgr.m_ShareInterval then
            g_MessageMgr:ShowMessage("INGAME_SHARE_TOO_FREQUENT")
            return false
        end
        LuaInGameShareMgr.m_LastShareTime = Time.realtimeSinceStartup
    end
    return true
end

function LuaInGameShareMgr.GetShareToWorldMenu(getShareTextFunc, onShareCompleteFunc)
    return PopupMenuItemData(LocalString.GetString("至世界频道"),
        DelegateFactory.Action_int(function (index)
            if not LuaInGameShareMgr.CanShare() then
                return
            end
            local shareText = getShareTextFunc(EnumInGameShareChannelDefine.eWorld)
            if shareText==nil or shareText=="" then
                    return
            end
            local result = CChatMgr.Inst:SendChatMsg(CChatMgr.CHANNEL_NAME_WORLD, shareText, true, true)
            if result == SendChatMsgResult.Success then
                if onShareCompleteFunc then
                    onShareCompleteFunc(shareText, EnumInGameShareChannelDefine.eWorld)
                end
            end

        end), false, nil)
end

function LuaInGameShareMgr.GetShareToGuildMenu(getShareTextFunc, onShareCompleteFunc)
    return PopupMenuItemData(LocalString.GetString("至帮会频道"),
        DelegateFactory.Action_int(function (index)
            if not LuaInGameShareMgr.CanShare() then
                return
            end
            local shareText = getShareTextFunc(EnumInGameShareChannelDefine.eGuild)
            if shareText==nil or shareText=="" then
                    return
            end
            local result = CChatMgr.Inst:SendChatMsg(CChatMgr.CHANNEL_NAME_ORG, shareText, true, true)
            if result == SendChatMsgResult.Success then
                if onShareCompleteFunc then
                    onShareCompleteFunc(shareText, EnumInGameShareChannelDefine.eGuild)
                end
            end

        end), false, nil)
end

function LuaInGameShareMgr.GetShareToTeamMenu(getShareTextFunc, onShareCompleteFunc)
    return PopupMenuItemData(LocalString.GetString("至队伍频道"),
        DelegateFactory.Action_int(function (index)
            if not LuaInGameShareMgr.CanShare() then
                return
            end
            local shareText = getShareTextFunc(EnumInGameShareChannelDefine.eTeam)
            if shareText==nil or shareText=="" then
                    return
            end
            local result = CChatMgr.Inst:SendChatMsg(CChatMgr.CHANNEL_NAME_TEAM, shareText, true, true)
            if result == SendChatMsgResult.Success then
                if onShareCompleteFunc then
                    onShareCompleteFunc(shareText, EnumInGameShareChannelDefine.eTeam)
                end
            end

        end), false, nil)
end

function LuaInGameShareMgr.GetShareToCurrentMenu(getShareTextFunc, onShareCompleteFunc)
    return PopupMenuItemData(LocalString.GetString("至当前频道"),
        DelegateFactory.Action_int(function (index)
            if not LuaInGameShareMgr.CanShare() then
                return
            end
            local shareText = getShareTextFunc(EnumInGameShareChannelDefine.eCurrent)
            if shareText==nil or shareText=="" then
                    return
            end
            local result = CChatMgr.Inst:SendChatMsg(CChatMgr.CHANNEL_NAME_CURRENT, shareText, true, true)
            if result == SendChatMsgResult.Success then
                if onShareCompleteFunc then
                    onShareCompleteFunc(shareText, EnumInGameShareChannelDefine.eCurrent)
                end
            end

        end), false, nil)
end

function LuaInGameShareMgr.GetShareToFriendMenu(getShareTextFunc, onShareCompleteFunc)
    local shareText = getShareTextFunc(EnumInGameShareChannelDefine.eIM)
    if shareText==nil or shareText=="" then
        return
    end
    return PopupMenuItemData(LocalString.GetString("至我的好友"),
        DelegateFactory.Action_int(function (index)

            CCommonPlayerListMgr.Inst:ShowFriendsToShareJingLingAnswer(DelegateFactory.Action_ulong(function (playerId)

                if not LuaInGameShareMgr.CanShare() then
                    return
                end
                local mainPlayer = CClientMainPlayer.Inst
                if mainPlayer == nil then return end

                CIMMgr.Inst:SendMsg(playerId, mainPlayer.Id, mainPlayer.EngineId, mainPlayer.Name,
                           EnumToInt(mainPlayer.Class), EnumToInt(mainPlayer.Gender), shareText, true)

                if onShareCompleteFunc then
                    onShareCompleteFunc(shareText, EnumInGameShareChannelDefine.eIM)
                end
            end))

        end), false, nil)
end

function LuaInGameShareMgr.GetShareToPersonalSpaceMenu(getShareTextFunc, onShareCompleteFunc)
    return PopupMenuItemData(LocalString.GetString("至梦岛心情"),
        DelegateFactory.Action_int(function (index)
            if not LuaInGameShareMgr.CanShare() then
                return
            end
            local shareText = getShareTextFunc(EnumInGameShareChannelDefine.ePersonalSpace)
            if shareText==nil or shareText=="" then
                    return
            end

            CPersonalSpaceMgr.Inst:ShareTextToPersonalSpace(shareText, DelegateFactory.Action_string(function (text)
                if text == shareText and onShareCompleteFunc then
                    onShareCompleteFunc(shareText, EnumInGameShareChannelDefine.ePersonalSpace)
                end
            end))

        end), false, nil)
end

function LuaInGameShareMgr.GetShareToSnapshotMenu(getShareTextFunc, onShareCompleteFunc, excludedRootForSnapshot)

    return PopupMenuItemData(LocalString.GetString("截图分享"),
        DelegateFactory.Action_int(function (index)
            if not LuaInGameShareMgr.CanShare() then
                return
            end

            UICommonDef.CaptureScreen(
                "screenshot",
                true,
                false,
                excludedRootForSnapshot,
                DelegateFactory.Action_string_bytes(
                    function(fullPath, jpgBytes)
                        ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
                    end
                ),
                false
            )

            --分享截图不处理回调

        end), false, nil)
end

function LuaInGameShareMgr.GetShareToAppMenu(getShareTextFunc, onShareCompleteFunc)
  return PopupMenuItemData(LocalString.GetString("至其他平台"), DelegateFactory.Action_int(function(index)
    local url, title = getShareTextFunc(EnumInGameShareChannelDefine.eAppShare)
    ShareBoxMgr.OpenShareBox(EShareType.CommonUrl, 0, {url, title, title, "share_thumb/duiwai.jpg"})
  end), false, nil)
end

function LuaInGameShareMgr.Share(channels, getShareTextFunc, parent, alignType, onShareCompleteFunc, excludedRootForSnapshot)
    LuaInGameShareMgr.DoShare(channels, getShareTextFunc, parent, alignType, function (shareText, channel)
        g_MessageMgr:ShowMessage("INGAME_SHARE_SUCCESS")
        if onShareCompleteFunc then
            onShareCompleteFunc(shareText, channel)
        end
    end, excludedRootForSnapshot)
end

function LuaInGameShareMgr.DoShare(channels, getShareTextFunc, parent, alignType, onShareCompleteFunc, excludedRootForSnapshot)
    if not getShareTextFunc then return end
    if not CClientMainPlayer.Inst then return end

    local tbl = {}
    if(bit.band(channels, EnumInGameShareChannelDefine.eWorld) ~= 0) then
        local shareToWorld = LuaInGameShareMgr.GetShareToWorldMenu(getShareTextFunc, onShareCompleteFunc)
        table.insert(tbl, shareToWorld)
    end

    if(bit.band(channels, EnumInGameShareChannelDefine.eGuild) ~= 0) then
        local shareToGuild = LuaInGameShareMgr.GetShareToGuildMenu(getShareTextFunc, onShareCompleteFunc)
         table.insert(tbl, shareToGuild)
    end

    if(bit.band(channels, EnumInGameShareChannelDefine.eTeam) ~= 0) then
        local shareToTeam = LuaInGameShareMgr.GetShareToTeamMenu(getShareTextFunc, onShareCompleteFunc)
         table.insert(tbl, shareToTeam)
    end

    if(bit.band(channels, EnumInGameShareChannelDefine.eCurrent) ~= 0) then
        local shareToCurrent = LuaInGameShareMgr.GetShareToCurrentMenu(getShareTextFunc, onShareCompleteFunc)
         table.insert(tbl, shareToCurrent)
    end

    if(bit.band(channels, EnumInGameShareChannelDefine.eIM) ~= 0) then
        local shareToFriend = LuaInGameShareMgr.GetShareToFriendMenu(getShareTextFunc, onShareCompleteFunc)
         table.insert(tbl, shareToFriend)
    end

    if(bit.band(channels, EnumInGameShareChannelDefine.ePersonalSpace) ~= 0) then
        local shareToPersonalSpace = LuaInGameShareMgr.GetShareToPersonalSpaceMenu(getShareTextFunc, onShareCompleteFunc)
         table.insert(tbl, shareToPersonalSpace)
    end

    if(bit.band(channels, EnumInGameShareChannelDefine.eSnapshot) ~= 0) then
        local shareToSnapshot = LuaInGameShareMgr.GetShareToSnapshotMenu(getShareTextFunc, onShareCompleteFunc, excludedRootForSnapshot)
         table.insert(tbl, shareToSnapshot)
    end

    if(bit.band(channels, EnumInGameShareChannelDefine.eAppShare) ~= 0) then
        local shareToApp = LuaInGameShareMgr.GetShareToAppMenu(getShareTextFunc, onShareCompleteFunc)
         table.insert(tbl, shareToApp)
    end

    local array = Table2Array(tbl, MakeArrayClass(PopupMenuItemData))

    if parent then
        CPopupMenuInfoMgr.ShowPopupMenu(array, parent, alignType, 1, nil, 600, true, 320)
    else
        CPopupMenuInfoMgr.ShowPopupMenu(array, Vector3.zero, alignType)
    end
end

function LuaInGameShareMgr.ShareJingLingAnswer(playerId, displayText, askText, parent, alignType)

    LuaInGameShareMgr.Share(EnumInGameShareChannelDefine.eJingLingShare, function (channel)
        return SafeStringFormat3("<link button=%s,JingLingShare,%s,%s,%s>", displayText, tostring(playerId), askText, tostring(channel))
    end,
    parent,
    alignType,
    function (shareText, channel)
        LuaInGameShareMgr.LogJingLingShareInfo(playerId, askText, channel, EnumToInt(EnumJingLingShareType.eMyShare))
    end)
end

function LuaInGameShareMgr.ShareWebLink(url, msg, title, parent, alignType)
  LuaInGameShareMgr.Share(EnumInGameShareChannelDefine.eWebLinkShare, function(channel)
    if channel == EnumInGameShareChannelDefine.eAppShare then
      return url, title
    else
      return msg
    end
  end, parent, alignType, nil)
end

function LuaInGameShareMgr.ShareHouseBuff(msg, parent, alignType)
    --refs #85545 分享祈福状态功能实现 C#代码转Lua
    LuaInGameShareMgr.Share(bit.bor(EnumInGameShareChannelDefine.eWorld, EnumInGameShareChannelDefine.eGuild, EnumInGameShareChannelDefine.eTeam), function (channel)
        return msg
    end, parent, alignType, nil)
end

function LuaInGameShareMgr.ShareXingguan(msg, parent, alignType)
    --refs #100625 星官增加分享和消耗总计查看 C#代码转Lua
    LuaInGameShareMgr.Share(EnumInGameShareChannelDefine.eXingGuanShare, function (channel)
        return msg
    end, parent, alignType, nil)
end

function LuaInGameShareMgr.ShareChatRoom(roomId, roomName, roomPassword, parent, alignType)
    LuaInGameShareMgr.Share(bit.bor(EnumInGameShareChannelDefine.eWorld, EnumInGameShareChannelDefine.eGuild, EnumInGameShareChannelDefine.eTeam, EnumInGameShareChannelDefine.eIM), function (channel)
        local formatStr = MessageMgr.Inst:GetMessageFormat("CLUBHOUSE_SHARE_ROOM_LINK", false)
        local msg = CommonDefs.String_Format_String_ArrayObject(formatStr, {roomName, tostring(roomId), roomName, roomPassword})
        return msg
    end, parent, alignType, nil)
end

function LuaInGameShareMgr.LogJingLingShareInfo(shareId, question, channel, jinglingShareType)
    local channelName = "error"
    if channel == EnumInGameShareChannelDefine.Undefined then channelName = "undefined"
    elseif channel == EnumInGameShareChannelDefine.eWorld then channelName = "world"
    elseif channel == EnumInGameShareChannelDefine.eGuild then channelName = "org"
    elseif channel == EnumInGameShareChannelDefine.eTeam then channelName = "team"
    elseif channel == EnumInGameShareChannelDefine.eCurrent then channelName = "current"
    elseif channel == EnumInGameShareChannelDefine.eIM then channelName = "im"
    elseif channel == EnumInGameShareChannelDefine.eCurrent then channelName = "current"
    elseif channel == EnumInGameShareChannelDefine.ePersonalSpace then channelName = "mengdao"
    elseif channel == EnumInGameShareChannelDefine.eSnapshot then channelName = "snapshot" end
    Gac2Gas.RequestLogJingLingFenXiangInfo(shareId, question, channelName, jinglingShareType)
end

---------------
--Gas2Gac RPC--
---------------
