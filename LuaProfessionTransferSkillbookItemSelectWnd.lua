local UISprite = import "UISprite"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UIGrid = import "UIGrid"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CCommonItemSelectCell = import "L10.UI.CCommonItemSelectCell"
local CCommonItemSelectCellData = import "L10.UI.CCommonItemSelectCellData"
local CCommonItemSelectInfoMgr = import "L10.UI.CCommonItemSelectInfoMgr"
local CCommonItemSelectWnd = import "L10.UI.CCommonItemSelectWnd"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local Extensions = import "Extensions"
local LocalString = import "LocalString"
local NGUIMath = import "NGUIMath"
local Object = import "System.Object"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local CUITexture = import "L10.UI.CUITexture"
local Item_Item = import "L10.Game.Item_Item"
local CItem = import "L10.Game.CItem"
local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"
local ShowType = import "L10.UI.CItemInfoMgr+ShowType"
local LinkItemInfo = import "L10.UI.CItemInfoMgr+LinkItemInfo"
local Vector2 = import "UnityEngine.Vector2"
local Vector3 = import "UnityEngine.Vector3"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local UIRect = import "UIRect"
local QnButton = import "L10.UI.QnButton"
local CNumberKeyBoardMgr = import "CNumberKeyBoardMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"

LuaProfessionTransferSkillbookItemSelectWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaProfessionTransferSkillbookItemSelectWnd, "Background", "Background", UISprite)
RegistChildComponent(LuaProfessionTransferSkillbookItemSelectWnd, "ItemsScrollView", "ItemsScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaProfessionTransferSkillbookItemSelectWnd, "Grid", "Grid", UIGrid)
RegistChildComponent(LuaProfessionTransferSkillbookItemSelectWnd, "ItemCell", "ItemCell", GameObject)
RegistChildComponent(LuaProfessionTransferSkillbookItemSelectWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaProfessionTransferSkillbookItemSelectWnd, "ItemInfoView", "ItemInfoView", GameObject)
RegistChildComponent(LuaProfessionTransferSkillbookItemSelectWnd, "SubmitText", "SubmitText", UILabel)
RegistChildComponent(LuaProfessionTransferSkillbookItemSelectWnd, "SubmitButton", "SubmitButton", GameObject)
RegistChildComponent(LuaProfessionTransferSkillbookItemSelectWnd, "Furnace", "Furnace", UILabel)
RegistChildComponent(LuaProfessionTransferSkillbookItemSelectWnd, "QnIncreseAndDecreaseButton", "QnIncreseAndDecreaseButton", GameObject)
RegistChildComponent(LuaProfessionTransferSkillbookItemSelectWnd, "InputNumberBackground", "InputNumberBackground", GameObject)

--@endregion RegistChildComponent end

function LuaProfessionTransferSkillbookItemSelectWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self:InitWndData()
    self:RefreshConstUI()
    self:RefreshVariableUI()
    self:InitUIEvent()
end

function LuaProfessionTransferSkillbookItemSelectWnd:Init()

end

--@region UIEvent

--@endregion UIEvent

function LuaProfessionTransferSkillbookItemSelectWnd:InitWndData()
    --prepare book data
    local mainplayer = CClientMainPlayer.Inst
    self.bookData = {}
    self.scoreBoard = {}
    local bookTemplateId2BookDataId = {}
    ProfessionTransfer_RongLuScore.Foreach(function(k, v)
        self.scoreBoard[v.ID] = v.Score
    end)
    
    if mainplayer then
        local bagSize = mainplayer.ItemProp:GetPlaceSize(EnumItemPlace.Bag)
        for i = 1, bagSize do
            local itemId = mainplayer.ItemProp:GetItemAt(EnumItemPlace.Bag, i)
            local item = CItemMgr.Inst:GetById(itemId)
            if item and item.IsItem then
                if self.scoreBoard[item.Item.TemplateId] then
                    local templateId = item.Item.TemplateId
                    local itemCnt = item.Amount

                    if bookTemplateId2BookDataId[templateId] == nil then
                        table.insert(self.bookData, {templateId = templateId, itemCnt = itemCnt, itemSelected = 0})
                        bookTemplateId2BookDataId[templateId] = #self.bookData  
                    else    
                        self.bookData[bookTemplateId2BookDataId[templateId]].itemCnt = self.bookData[bookTemplateId2BookDataId[templateId]].itemCnt + itemCnt
                    end
                end
            end
        end
        
        table.sort(self.bookData, function(compA, compB)
            if self.scoreBoard[compA.templateId] ~= self.scoreBoard[compB.templateId] then
                return self.scoreBoard[compA.templateId] > self.scoreBoard[compB.templateId]
            else
                return compA.itemCnt > compB.itemCnt
            end
        end)
    end

    self.m_DecreaseButton   = self.QnIncreseAndDecreaseButton.transform:Find("DecreaseButton"):GetComponent(typeof(QnButton))
    self.m_IncreaseButton   = self.QnIncreseAndDecreaseButton.transform:Find("IncreaseButton"):GetComponent(typeof(QnButton))
    self.m_InputLab         = self.QnIncreseAndDecreaseButton.transform:Find("InputLab"):GetComponent(typeof(UILabel))
    
end 

function LuaProfessionTransferSkillbookItemSelectWnd:RefreshConstUI()
    self.ItemInfoView:SetActive(false)
    self.InputNumberBackground:SetActive(false)
end 

function LuaProfessionTransferSkillbookItemSelectWnd:RefreshVariableUI()
    Extensions.RemoveAllChildren(self.Grid.transform)
    self.ItemCell:SetActive(false)
    self.bookItemCellList = {}
    for i = 1, #self.bookData do
        local go = CUICommonDef.AddChild(self.Grid.gameObject, self.ItemCell)
        go:SetActive(true)
        self:InitItemCell(go, self.bookData[i], i)
        table.insert(self.bookItemCellList, go)
    end
    self.Grid:Reposition()
    self.ItemsScrollView:ResetPosition()
end 

function LuaProfessionTransferSkillbookItemSelectWnd:InitUIEvent()
    UIEventListener.Get(self.m_DecreaseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnDecreaseButtonClick()
    end)

    UIEventListener.Get(self.m_IncreaseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnIncreaseButtonClick()
    end)

    UIEventListener.Get(self.m_InputLab.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnInputLabClick()
    end)

    UIEventListener.Get(self.SubmitButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnSubmitButtonClick()
    end)
end 

function LuaProfessionTransferSkillbookItemSelectWnd:OnSubmitButtonClick()
    local totalScore = 0
    local itemList = {}
    local canSubmit = false
    for i = 1, #self.bookData do
        local data = self.bookData[i]
        if data.itemSelected > 0 then
            itemList[data.templateId] = (itemList[data.templateId] and itemList[data.templateId] or 0) + data.itemSelected
            canSubmit = true

            totalScore = totalScore + data.itemSelected * self.scoreBoard[data.templateId]
        end
    end
    if canSubmit then
        local maxScore = self:GetRongLuMaxScore()
        if totalScore > maxScore then
            g_MessageMgr:ShowMessage("BUYBOOK_RONGLU_SCORE_LIMIT")
            return
        end
        
        Gac2Gas.QMKS_RequestPutSkillBooksToRongLu(g_MessagePack.pack(itemList))
        CUIManager.CloseUI(CLuaUIResources.ProfessionTransferSkillbookItemSelectWnd)
    end
end

function LuaProfessionTransferSkillbookItemSelectWnd:GetRongLuMaxScore()
    local scorePerBook = ProfessionTransfer_Setting.GetData().TianShuScore
    local itemData = Item_Item.GetData(21000855)
    local bookMaxNum = 0
    if itemData then
        bookMaxNum = itemData.Overlap
    end
    return scorePerBook * bookMaxNum
end

function LuaProfessionTransferSkillbookItemSelectWnd:CanSelectMoreBook()
    local selectDifferentBookNum = 0
    for i = 1, #self.bookData do
        if self.bookData[i].itemSelected > 0 then
            selectDifferentBookNum = selectDifferentBookNum + 1
        end
    end
    return selectDifferentBookNum < ProfessionTransfer_Setting.GetData().MaxRongLuBookNumber
end

function LuaProfessionTransferSkillbookItemSelectWnd:InitItemCell(go, data, index)
    local iconTex = go.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    local qualitySprite = go.transform:Find("QualitySprite"):GetComponent(typeof(UISprite))
    local bindSprite = go.transform:Find("BindSprite"):GetComponent(typeof(UISprite))
    local selectedGo = go.transform:Find("SelectedSprite").gameObject
    local numLabel = go.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
    local checkGo = go.transform:Find("CheckIcon").gameObject
    
    --local itemData = CItemMgr.Inst:GetById(data.itemId)
    local itemCfg = Item_Item.GetData(data.templateId)
    iconTex:LoadMaterial(itemCfg.Icon)
    numLabel.text = data.itemCnt > 1 and data.itemCnt or ""
    qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(CItem.GetQualityType(data.templateId))

    selectedGo:SetActive(false)
    checkGo:SetActive(false)
    bindSprite.spriteName = ""

    UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function (_)
        if data.itemSelected == 0 then
            if self:CanSelectMoreBook() then
                --未被选择过
                CItemInfoMgr.showType = ShowType.Link
                CItemInfoMgr.linkItemInfo = LinkItemInfo(data.templateId, true, nil)
                CItemInfoMgr.linkItemInfo.alignType = AlignType2.Default
                CItemInfoMgr.linkItemInfo.targetSize = Vector2(0, 0)
                CItemInfoMgr.linkItemInfo.targetCenterPos = Vector3(0, 0, 0)
                CItemInfoMgr.linkItemInfo.dragAmount = 0

                self.ItemInfoView:SetActive(true)
                self.ItemInfoView.transform:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf:Init()
                self.ItemInfoView.transform:Find("Table/Background"):GetComponent(typeof(UIRect)):ResetAndUpdateAnchors()
                self.InputNumberBackground:SetActive(true)
                self.m_InputNumMax = data.itemCnt
                self.m_InputIndex = index
                if self:CheckIsSpecialSkillbook(data.templateId) then
                    self.m_InputNum = 1
                    self:SetInputNum(1)
                    data.itemSelected = 1
                else
                    self.m_InputNum = data.itemCnt
                    self:SetInputNum(data.itemCnt)
                    data.itemSelected = data.itemCnt
                end
                self.Furnace.text = LocalString.GetString("每个可兑换") .. self.scoreBoard[data.templateId] .. LocalString.GetString("积分")
                --update numLabel
                checkGo:SetActive(true)
                for i = 1, #self.bookItemCellList do
                    self.bookItemCellList[i].transform:Find("SelectedSprite").gameObject:SetActive(i == index)
                end
            else
                g_MessageMgr:ShowMessage("PROFESSION_TRANSFER_CANNOT_SELECT_MORE_BOOK")
            end
        else
            --取消选择
            self.ItemInfoView:SetActive(false)
            self.InputNumberBackground:SetActive(false)
            checkGo:SetActive(false)
            selectedGo:SetActive(false)
            data.itemSelected = 0
            numLabel.text = data.itemCnt > 1 and data.itemCnt or ""
            self:UpdateAllScore()
        end
    end)
end 

function LuaProfessionTransferSkillbookItemSelectWnd:CheckIsSpecialSkillbook(templateId)
    local rongLuScore = self.scoreBoard[templateId] and self.scoreBoard[templateId] or 0
    local scorePerBook = ProfessionTransfer_Setting.GetData().TianShuScore
    return rongLuScore >= scorePerBook
end

function LuaProfessionTransferSkillbookItemSelectWnd:OnDecreaseButtonClick()
    self:SetInputNum(self.m_InputNum - 1)
end 

function LuaProfessionTransferSkillbookItemSelectWnd:OnIncreaseButtonClick()
    self:SetInputNum(self.m_InputNum + 1)
end 

function LuaProfessionTransferSkillbookItemSelectWnd:OnInputLabClick()
    CNumberKeyBoardMgr.ShowNumberKeyBoardWithRange(1, self.m_InputNumMax, self.m_InputNumMax, 100, DelegateFactory.Action_int(function (val)
        self.m_InputLab.text = val
    end), DelegateFactory.Action_int(function (val)
        self:SetInputNum(val)
    end), self.m_InputLab, AlignType.Right, true)
end

function LuaProfessionTransferSkillbookItemSelectWnd:SetInputNum(num)
    if num < 0 then
        num = 0
    end

    if num > self.m_InputNumMax then
        num = self.m_InputNumMax
    end

    self.bookData[self.m_InputIndex].itemSelected = num
    self.m_InputNum = num
    self.m_InputLab.text = num

    self.m_IncreaseButton.Enabled = self.m_InputNumMax - num > 0
    self.m_DecreaseButton.Enabled = num > 0

    --self:UpdateItemNumLabel(numLabel, data.itemSelected, data.itemCnt)
    
    local labelComp = self.bookItemCellList[self.m_InputIndex].transform:Find("NumLabel"):GetComponent(typeof(UILabel))
    self:UpdateItemNumLabel(labelComp, num, self.bookData[self.m_InputIndex].itemCnt)

    self.bookItemCellList[self.m_InputIndex].transform:Find("CheckIcon").gameObject:SetActive(num > 0)
    self:UpdateAllScore()
end

function LuaProfessionTransferSkillbookItemSelectWnd:UpdateItemNumLabel(labelComp, selectNum, totalNum)
    if totalNum == 1 then
        -- 不修改
    else
        labelComp.text = "[FFFF00]" .. selectNum .. "[-]/" .. totalNum
    end
end

function LuaProfessionTransferSkillbookItemSelectWnd:UpdateAllScore()
    local totalScore = 0
    for i = 1, #self.bookData do
        totalScore = totalScore + self.bookData[i].itemSelected * self.scoreBoard[self.bookData[i].templateId]
    end
    
    self.SubmitText.text = LocalString.GetString("投入后可获得积分") .. totalScore
end 