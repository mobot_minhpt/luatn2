local Gac2Gas=import "L10.Game.Gac2Gas"
local UILabel = import "UILabel"
local UIGrid = import "UIGrid"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local GameObject = import "UnityEngine.GameObject"
local CButton = import "L10.UI.CButton"
local MsgPackImpl = import "MsgPackImpl"
local CGamePlayConfirmMemberItem = import "L10.UI.CGamePlayConfirmMemberItem"
local LocalString = import "LocalString"
local CTeamMgr = import "L10.Game.CTeamMgr"
local Extensions = import "Extensions"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local TweenWidth = import "TweenWidth"
local UISprite = import "UISprite"
local Huabi_Setting = import "L10.Game.Huabi_Setting"
local UITexture = import "UITexture"
local Rect = import "UnityEngine.Rect"
local CUICommonDef = import "L10.UI.CUICommonDef"
local Professional = import "L10.Game.Profession"
local EnumClass = import "L10.Game.EnumClass"

CLuaHuaBiConfirmWnd=class()
RegistChildComponent(CLuaHuaBiConfirmWnd, "m_MemberItemTemplate", GameObject)
RegistChildComponent(CLuaHuaBiConfirmWnd, "m_ConfirmHintLbl", UILabel)
RegistChildComponent(CLuaHuaBiConfirmWnd, "m_CancelBtn", CButton)
RegistChildComponent(CLuaHuaBiConfirmWnd, "m_ConfirmBtn", CButton)
RegistChildComponent(CLuaHuaBiConfirmWnd, "m_ConfirmConsumeSilver", CButton)
RegistChildComponent(CLuaHuaBiConfirmWnd, "m_StartScroll", TweenWidth)
RegistChildComponent(CLuaHuaBiConfirmWnd, "m_WordsLeft", UITexture)
RegistChildComponent(CLuaHuaBiConfirmWnd, "m_WordsRight", UITexture)
RegistChildComponent(CLuaHuaBiConfirmWnd, "m_SilverExpInfoLbl", UILabel)
RegistChildComponent(CLuaHuaBiConfirmWnd, "m_WndMain", GameObject)
RegistChildComponent(CLuaHuaBiConfirmWnd, "m_CountdownLbl", UILabel)
RegistChildComponent(CLuaHuaBiConfirmWnd, "m_ClassExpIncLbl", UILabel)
RegistChildComponent(CLuaHuaBiConfirmWnd, "m_FeiShengExpIncLbl", UILabel)
RegistChildComponent(CLuaHuaBiConfirmWnd, "m_TeamTable", UIGrid)
RegistClassMember(CLuaHuaBiConfirmWnd, "m_LeftSeconds")
RegistClassMember(CLuaHuaBiConfirmWnd, "m_Ticks")
RegistClassMember(CLuaHuaBiConfirmWnd, "m_FinishAnim1")
RegistClassMember(CLuaHuaBiConfirmWnd, "m_CostSilver")


function CLuaHuaBiConfirmWnd:Awake()
end

function CLuaHuaBiConfirmWnd:Init()
	self:RefreshTeamInfo()

	CommonDefs.AddOnClickListener(self.m_ConfirmBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
		Gac2Gas.SendHuaBiConfirmInfo(true)
	end), false)

	CommonDefs.AddOnClickListener(self.m_CancelBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
		Gac2Gas.SendHuaBiConfirmInfo(false)
	end), false)

	CommonDefs.AddOnClickListener(self.m_ConfirmConsumeSilver.gameObject, DelegateFactory.Action_GameObject(function (go)
		if CClientMainPlayer.Inst.Silver < (self.m_CostSilver or 9999999) then
			g_MessageMgr:ShowMessage("SILVER_NOT_ENOUGH")
			return
		end
		local msg = g_MessageMgr:FormatMessage("HUABI_SILVER_CONFIRM", (self.m_CostSilver or 9999999))
		g_MessageMgr:ShowOkCancelMessage(msg, 
			function ()
				Gac2Gas.ConfirmHuaBiUseSilver()
			end,
			nil, nil, nil, false)
	end), false)

	self.m_Ticks = {}

	self:StartAnim1()

	self.m_LeftSeconds = 30
	self.m_CountdownLbl.text = tostring(self.m_LeftSeconds)
end


local TeamInfo = {}
local Stage = 0
local ClassIncExpInfo = {}


function CLuaHuaBiConfirmWnd:StartTick()
	local tick = nil
	tick = RegisterTickWithDuration(function ()
		if not self:OnTick() then
			UnRegisterTick(tick)
			self.m_Ticks[tick] = nil
			CUIManager.CloseUI(CUIResources.HuaBiConfirmWnd)
		end
	end, 1000, 30000)
	self.m_Ticks[tick] = true
end

function CLuaHuaBiConfirmWnd:OnTick()
	if self.m_CountdownLbl == nil then return false end

	self.m_LeftSeconds = self.m_LeftSeconds - 1
	self.m_CountdownLbl.text = tostring(self.m_LeftSeconds)
	return true
end


function CLuaHuaBiConfirmWnd:StartAnim1()
	self.m_WndMain:SetActive(false)
	self.m_WordsLeft.gameObject:SetActive(false)
	self.m_WordsRight.gameObject:SetActive(false)

	self.m_FinishAnim1 = false
	self.m_StartScroll:AddOnFinished(DelegateFactory.Callback(function ()
		self.m_FinishAnim1 = true
		self:StartAnim2()
	end))
end

function CLuaHuaBiConfirmWnd:Update()
	if self.m_FinishAnim1 then return end

	local rate = (self.m_StartScroll.value - self.m_StartScroll.from) / (self.m_StartScroll.to - self.m_StartScroll.from)
	local texture = self.m_StartScroll:GetComponent(typeof(UITexture))
	local newRect = CreateFromClass(Rect)
	newRect = texture.uvRect
	newRect.width = rate
	texture.uvRect = newRect
end

function CLuaHuaBiConfirmWnd:StartAnim2()
	self.m_WndMain:SetActive(true)
	self.m_WordsLeft.gameObject:SetActive(true)
	self.m_WordsRight.gameObject:SetActive(true)

	local silverFormulaId = Huabi_Setting.GetData().Silver
	local silverFormula = AllFormulas.Action_Formula[silverFormulaId] and AllFormulas.Action_Formula[silverFormulaId].Formula
	local silver = silverFormula and silverFormula(nil, nil, {Stage}) or 9999999
	silver = math.floor(silver)

	self.m_CostSilver = silver
	self.m_SilverExpInfoLbl.text = SafeStringFormat(LocalString.GetString("消耗银两%d,个人经验加成%d%%"), silver, math.floor(Huabi_Setting.GetData().UserSilverExpInc*100))

	self.m_WndMain:GetComponent(typeof(UISprite)).alpha = 0
	self.m_WordsLeft.alpha = 0
	self.m_WordsRight.alpha = 0

	
	self.m_WordsLeft.fillAmount = 0
	self.m_WordsRight.fillAmount = 0

	local setting = Huabi_Setting.GetData()
	local interval = 50
	local wordTotal, showupTotal = setting.ConfirmWordAnimationDuration * 1000, setting.ConfirmShowDuration * 1000
	local total = math.max(wordTotal, showupTotal)
	local count = 0

	local tick = nil
	tick = RegisterTickWithDuration(function ()
		if self.m_StartScroll == nil then 
			UnRegisterTick(tick)
			self.m_Ticks[tick] = nil
			return 
		end

		if self.m_WndMain.active then
			local mainSprite = self.m_WndMain:GetComponent(typeof(UISprite))
			if mainSprite.alpha < 1 then
				mainSprite.alpha = mainSprite.alpha + (interval/showupTotal)
				self.m_WordsLeft.alpha = self.m_WordsLeft.alpha + (interval/showupTotal)
				self.m_WordsRight.alpha = self.m_WordsRight.alpha + (interval/showupTotal)
			end
		end

		if self.m_WordsRight.fillAmount < (1 - interval/wordTotal*2) then
			self.m_WordsRight.fillAmount = self.m_WordsRight.fillAmount + interval/wordTotal*2
		else
			self.m_WordsLeft.fillAmount = self.m_WordsLeft.fillAmount + interval/wordTotal*2
		end
		count = count + interval
		if count == total then
			self:StartTick()
		end
	end, interval, total)
	self.m_Ticks[tick] = true
end

function CLuaHuaBiConfirmWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateTeamConfirmInfo", self, "OnUpdateTeamConfirmInfo")
end

function CLuaHuaBiConfirmWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateTeamConfirmInfo", self, "OnUpdateTeamConfirmInfo")
	for tick, _ in pairs(self.m_Ticks) do
		UnRegisterTick(tick)
	end
	self.m_Ticks = {}
end

function CLuaHuaBiConfirmWnd:OnUpdateTeamConfirmInfo()
	self:RefreshTeamInfo()
end

function CLuaHuaBiConfirmWnd:RefreshTeamInfo()
	local teamInfo = TeamInfo
	TeamInfo = {}

	local isSelfConfirmed, isSelfBuyExpInc = false, false
	local confirmNum, totalNum = 0, 0

	Extensions.RemoveAllChildren(self.m_TeamTable.transform)
	for memberId, memberInfo in pairs(teamInfo) do
		local go = CUICommonDef.AddChild(self.m_TeamTable.gameObject, self.m_MemberItemTemplate)
		go:SetActive(true)
		local item = go:GetComponent(typeof(CGamePlayConfirmMemberItem))
		if item then
			item:Init(memberInfo.Name, 
				memberInfo.IsAccept, 
				memberInfo.IsAccept and LocalString.GetString("已确认") or LocalString.GetString("等待确认"), 
				CUICommonDef.GetPortraitName(memberInfo.Class, memberInfo.Gender, memberInfo.Expression), 
				CUICommonDef.GetExpressionTxtPath(memberInfo.ExpressionTxt),
				nil, -- 头像框这里先不处理
				CTeamMgr.Inst:IsTeamLeader(memberId))

			if memberInfo.IsFeiSheng then
				item:SetShowLevel(g_MessageMgr:DecorateTextWithFeiShengColor(SafeStringFormat(LocalString.GetString("Lv:%d"), memberInfo.Level)))
			else
				item:SetShowLevel(SafeStringFormat(LocalString.GetString("Lv:%d"),memberInfo.Level))
			end

			if memberInfo.IsUseSilver then
				item:ShowSpecialBgEffect()
			else
				item:HideSpecialBgEffect()
			end
			item:SetExtraTxt(SafeStringFormat(LocalString.GetString("剩余%d次"), memberInfo.LeftTimes))
		end

		if memberId == CClientMainPlayer.Inst.Id then
			isSelfConfirmed = memberInfo.IsAccept
			isSelfBuyExpInc = memberInfo.IsUseSilver
		end

		if memberInfo.IsAccept then
			confirmNum = confirmNum + 1
		end
		totalNum = totalNum + 1
	end

	self.m_TeamTable:Reposition()

	if isSelfConfirmed then
		self.m_CancelBtn.gameObject:SetActive(false)
		self.m_ConfirmBtn.gameObject:SetActive(false)
	else
		self.m_CancelBtn.gameObject:SetActive(true)
		self.m_ConfirmBtn.gameObject:SetActive(true)
	end

	if isSelfBuyExpInc then
		self.m_ConfirmConsumeSilver.gameObject:SetActive(false)
	else
		self.m_ConfirmConsumeSilver.gameObject:SetActive(true)
	end

	local classNameTbl = {}
	for _, classId in pairs(ClassIncExpInfo.List) do
		local enumClass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), classId)
		local name = Professional.GetFullName(enumClass)
		table.insert(classNameTbl, name)
	end


	if not next(classNameTbl) then
		self.m_ClassExpIncLbl.gameObject:SetActive(false)
	else
		self.m_ClassExpIncLbl.gameObject:SetActive(true)
		self.m_ClassExpIncLbl.text = SafeStringFormat(LocalString.GetString("队伍中包含%s  全队经验加成%d%%"), table.concat(classNameTbl, LocalString.GetString("、")), math.floor(ClassIncExpInfo.Inc*100))
	end

	self.m_FeiShengExpIncLbl.text = SafeStringFormat(LocalString.GetString("所有玩家经验加成%d%%"), math.floor(Huabi_Setting.GetData().FeiShengExpInc*100))

	self.m_ConfirmHintLbl.text = SafeStringFormat(LocalString.GetString("第%d层  已确认 [c][E43203]%d[-][/c]/%d"), Stage, confirmNum, totalNum)
end


function Gas2Gac.UpdateTeamConfirmInfo(infoUD)
	local list = MsgPackImpl.unpack(infoUD)
	if not list  then return end

	TeamInfo = {}
	local segNum = 11 
	Stage = list[0]
	ClassIncExpInfo.Inc = list[1]
	ClassIncExpInfo.List = {}
	for i = 0, list[2].Count-1 do
		table.insert(ClassIncExpInfo.List, list[2][i])
	end

	for i = 3, list.Count-1, segNum do
		local memberId = list[i]
		local memberName = list[i+1]
		local memberClass = list[i+2]
		local memberGender = list[i+3]
		local memberLevel = list[i+4]
		local isAccept = list[i+5]
		local isUseSilver = list[i+6]
		local memberExpression = list[i+7]
		local expressionTxt = list[i+8]
		local leftTimes = list[i+9]
		local isFeiSheng = list[i+10]
		
		TeamInfo[memberId] = {
			Name = memberName,
			Class = memberClass,
			Gender = memberGender,
			Level = memberLevel,
			IsAccept = isAccept, 
			IsUseSilver = isUseSilver,
			Expression = memberExpression,
			ExpressionTxt = expressionTxt,
			LeftTimes = leftTimes,
			IsFeiSheng = isFeiSheng,
		}
	end
	
	g_ScriptEvent:BroadcastInLua("UpdateTeamConfirmInfo")
end

function Gas2Gac.ShowTeamConfirmInfoWnd()
	CUIManager.ShowUI(CUIResources.HuaBiConfirmWnd)
end
function Gas2Gac.CloseTeamConfirmInfoWnd()
	CUIManager.CloseUI(CUIResources.HuaBiConfirmWnd)
end

