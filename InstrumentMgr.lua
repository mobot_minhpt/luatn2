local CClientFurnitureMgr = import "L10.Game.CClientFurnitureMgr"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Vector3 = import "UnityEngine.Vector3"
local CEffectMgr = import "L10.Game.CEffectMgr"
local ClientAction = import "L10.UI.ClientAction"

--乐器管理
InstrumentMgr = {}

InstrumentMgr.FurnitureID = 0 --当前乐器ID
InstrumentMgr.InstrumentID = 0

g_ScriptEvent:AddListener("PlayerAttachFurniture", InstrumentMgr, "OnPlayerAttach")
g_ScriptEvent:AddListener("PlayerDetachFurniture", InstrumentMgr, "OnPlayerDetach")

function InstrumentMgr:OnPlayerAttach(playerId, furnitureId, slotIdx)
    local fur = CClientFurnitureMgr.Inst:GetFurniture(furnitureId)
    if not fur then
        return
    end
    local type = fur:GetSubType()
    if type ~= EnumFurnitureSubType_lua.eInstrument then
        return
    end

    local z2i = Zhuangshiwu_Instrument.GetData(fur.TemplateId)
    if not z2i then
        return
    end

    local data = Instrument_Instrument.GetData(z2i.InstrumentId)
    if not data then
        return
    end

    InstrumentMgr.FurnitureID = furnitureId
    InstrumentMgr.InstrumentID = z2i.InstrumentId

    if fur.RO then
        fur.RO:DoAni(data.UseAni, true, 0, 1, 0.15, true, 1)
    end

    local player = CClientPlayerMgr.Inst:GetPlayer(playerId)
    if player and player.RO then
        player.RO:RemoveFX("instrumentFx")
        local fx = CEffectMgr.Inst:AddObjectFX(data.Fx, player.RO, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero)
        player.RO:AddFX("instrumentFx", fx, -1)
    end

    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == playerId then --玩家自己使用乐器
        ClientAction.DoAction(data.Action)
    end
end

function InstrumentMgr:OnPlayerDetach(playerId, furnitureId)
    InstrumentMgr.FurnitureID = 0
    InstrumentMgr.InstrumentID = 0
    local fur = CClientFurnitureMgr.Inst:GetFurniture(furnitureId)
    if not fur then
        return
    end
    local type = fur:GetSubType()
    if type ~= EnumFurnitureSubType_lua.eInstrument then
        return
    end

    local z2i = Zhuangshiwu_Instrument.GetData(fur.TemplateId)
    if not z2i then
        return
    end

    local instrumentdata = Instrument_Instrument.GetData(z2i.InstrumentId)
    if not instrumentdata then
        return
    end

    if fur.RO then
        fur.RO:DoAni(instrumentdata.IdleAni, true, 0, 1, 0.15, true, 1)
    end

    local player = CClientPlayerMgr.Inst:GetPlayer(playerId)
    if player and player.RO then
        player.RO:RemoveFX("instrumentFx")
    end
end

--[[
    @desc: 播放一个声音
    author:Codee
    time:2022-06-14 15:03:47
    --@interval:
	--@scale: 
    @return:
]]
function InstrumentMgr.PlaySound(interval, scale)
    local instype = InstrumentMgr.InstrumentID
    if instype <= 0 then
        return
    end

    local data = Instrument_Instrument.GetData(instype)
    if data == nil then
        return
    end
    local path = data.Path .. data.ShortName .. "_" .. interval .. scale
    SoundManager.Inst:PlayOneShot(path, Vector3.zero, nil, 0)
end
