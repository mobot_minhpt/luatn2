-- Auto Generated!!
local CGuidelineWnd = import "L10.UI.CGuidelineWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
CGuidelineWnd.m_OnTabChange_CS2LuaHook = function (this, go, index) 

    this.titleLabel.text = string.gsub(CommonDefs.GetComponentInChildren_GameObject_Type(go, typeof(UILabel)).text, "\n", CommonDefs.IS_VN_CLIENT and " " or "")

    this.levelUpGuideView.gameObject:SetActive(index == 0)

    if index == 0 then
        this.levelUpGuideView:Init()
    end
end

