-- Auto Generated!!
local CFirstChargeGiftCell = import "L10.UI.CFirstChargeGiftCell"
local CFirstChargeLevelTemplate = import "L10.UI.CFirstChargeLevelTemplate"
local CFirstChargeWnd = import "L10.UI.CFirstChargeWnd"
local Charge_ChargeSetting = import "L10.Game.Charge_ChargeSetting"
local Charge_VipInfo = import "L10.Game.Charge_VipInfo"
local CLingShouBaseMgr = import "L10.Game.CLingShouBaseMgr"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local Gac2Gas = import "L10.Game.Gac2Gas"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
CFirstChargeWnd.m_GetMaxCharge_CS2LuaHook = function (this) 
    local result = 88000
    local maxLevel = 0
    Charge_VipInfo.ForeachKey(DelegateFactory.Action_object(function (key) 
        if key > maxLevel then
            maxLevel = key
        end
    end))
    local info = Charge_VipInfo.GetData(maxLevel)
    if info ~= nil then
        result = info.ChargeCount
    end
    return result
end
CFirstChargeWnd.m_ShowLingshouTips_CS2LuaHook = function (this) 
    if CFirstChargeWnd.showTips and CLingShouBaseMgr.ShowLingshouByPlayConfig() then
        this.tips.text = g_MessageMgr:FormatMessage("TEST_CHONGZHI_FANHUAN")
    else
        this.tips.text = ""
    end
end
CFirstChargeWnd.m_InitCharge_CS2LuaHook = function (this) 
    Extensions.RemoveAllChildren(this.levelTable.transform)
    this.levelTemplate:SetActive(false)
    do
        local i = 0
        while i < this.chargeLevel.Count do
            local instance = NGUITools.AddChild(this.levelTable.gameObject, this.levelTemplate)
            instance:SetActive(true)
            CommonDefs.GetComponent_GameObject_Type(instance, typeof(CFirstChargeLevelTemplate)):Init(this.chargeLevel[i], i)
            i = i + 1
        end
    end
    this.levelTable:Reposition()
    this.chargeBg:SetActive(true)
    this.firstChargeBg:SetActive(false)
end
CFirstChargeWnd.m_InitFirstCharge_CS2LuaHook = function (this) 
    local chargeSetting = Charge_ChargeSetting.GetData("SecondChargeGift")
    this.giftInfo = CommonDefs.StringSplit_ArrayChar(chargeSetting.Value, ";")
    Extensions.RemoveAllChildren(this.table1.transform)
    Extensions.RemoveAllChildren(this.table2.transform)
    this.giftTemplate:SetActive(false)
    if this.giftInfo ~= nil and this.giftInfo.Length >= 9 then
        local firstCell = nil
        for i = 0, 4 do
            local info = CommonDefs.StringSplit_ArrayChar(this.giftInfo[i], ",")
            if info ~= nil and info.Length == 2 then
                local gift = NGUITools.AddChild(this.table1.gameObject, this.giftTemplate)
                CommonDefs.GetComponent_GameObject_Type(gift, typeof(CFirstChargeGiftCell)):Init(System.UInt32.Parse(info[0]), System.UInt32.Parse(info[1]))
                if i == 0 then
                    firstCell = CommonDefs.GetComponent_GameObject_Type(gift, typeof(CFirstChargeGiftCell))
                end
                gift:SetActive(true)
            end
        end
        for i = 5, 8 do
            local info = CommonDefs.StringSplit_ArrayChar(this.giftInfo[i], ",")
            if info ~= nil and info.Length == 2 then
                local gift = NGUITools.AddChild(this.table2.gameObject, this.giftTemplate)
                CommonDefs.GetComponent_GameObject_Type(gift, typeof(CFirstChargeGiftCell)):Init(System.UInt32.Parse(info[0]), System.UInt32.Parse(info[1]))
                gift:SetActive(true)
            end
        end
        this.table1:Reposition()
        this.table2:Reposition()
        firstCell:ShowFx()
    end
    this.chargeBg:SetActive(false)
    this.firstChargeBg:SetActive(true)
end
CFirstChargeWnd.m_OnChargeButtonClick_CS2LuaHook = function (this, go) 
    if this.chargeLabel.text == LocalString.GetString("领取奖励") then
        if this.chargeInfo.GiftReceivedTime == 0 and this.vipInfo.TotalCharge > 0 then
            Gac2Gas.RequestGetFirstChargeGift()
        end
        if this.chargeInfo.AdditionalGiftReceivedTime == 0 and this.vipInfo.TotalCharge >= CFirstChargeWnd.secondChargeAmount then
            Gac2Gas.RequestGetAdditionalFirstChargeGift()
        end
    else
        CShopMallMgr.ShowChargeWnd()
    end
end
