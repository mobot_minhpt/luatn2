
----  For Gac use
g_FurnitureScene = nil
local Zhuangshiwu_Zhuangshiwu = import "L10.Game.Zhuangshiwu_Zhuangshiwu"
local FurnitureScene = import "L10.Game.FurnitureScene"
local CPoolType = import "L10.Game.CPoolType"
local CClientFurnitureMgr=import "L10.Game.CClientFurnitureMgr"

local FurnitureScene_GetInfoById = function(id)
		local res = FurnitureScene.GetInfoById(id)
		if res == nil then
			return nil
		end
		local ret = {
			data = LoadFurnitureDesignData(res[0]),
			x = res[1],
			z = res[2],
			rot = res[3],
			scale = res[4],
		}
		return ret
	end
	
local FurnitureScene_GetGridBarrier = function(x, z)
		return FurnitureScene.GetGridBarrier(x, z)
	end
	
local FurnitureScene_SetGridBarrier = function(x, z, barrier)
		FurnitureScene.SetGridBarrier(x, z, barrier)
	end
	
local FurnitureScene_GetOriginalGridBarrierAndInfo = function(x, z)
		local res = FurnitureScene.GetOriginalGridBarrierAndInfo(x, z)
		return res[0], res[1]
	end
local FurnitureScene_GetOriginalGridBarrierAndInfoWithPoolBarrier = function(x, z)
	local res = FurnitureScene.GetOriginalGridBarrierAndInfo(x, z)
	local barrier = res[0]
	--要考虑水池逻辑造成的障碍
	if barrier==0 then
		barrier = CClientFurnitureMgr.Inst:IsJiaYuanPoolBarrier(x,z) and 2 or 0
	end
	return barrier, res[1]
end

local FurnitureScene_IsAnythingInGrid = function(x, z)
	return FurnitureScene.IsAnythingInGrid(x, z)
end
	
local FurnitureScene_IsMainPlayerInGrid = function(x, z)
	return FurnitureScene.IsMainPlayerInGrid(x, z)
end
	
local FurnitureScene_IDInGrid = function(x,z)
		return FurnitureScene.IDInGrid(x,z)
	end
	
local FurnitureScene_GetGridBarrierAndOriginalGridBarrier = function(x, z)
		local res = FurnitureScene.GetGridBarrierAndOriginalGridBarrier(x, z)
		return res[0], res[1]
	end
local FurnitureScene_GetGridBarrierAndInfo = function(x, z)
		local res = FurnitureScene.GetGridBarrierAndInfo(x, z)
		return res[0], res[1]
	end
			
FurnitureScene.Lua_CreateFurnitureScene = function(gridWidth, gridHeight, furList, notInYard, kuozhangLvl)

		local invalid = {}
		local list = {}
		for i = 0, furList.Count - 1 do
			table.insert(list, furList[i])
		end
		g_FurnitureScene = CFurnitureScene:new(gridWidth, gridHeight, list, invalid, notInYard, kuozhangLvl, FurnitureScene_GetInfoById, FurnitureScene_GetBarrier, FurnitureScene_SetGridBarrier, FurnitureScene_GetOriginalGridBarrierAndInfo)
		return Table2Array(invalid, MakeArrayClass(uint))
	end
	
FurnitureScene.Lua_DestroyFurnitureScene = function()

		g_FurnitureScene = nil
	end

DefaultNormalPool, DefaultMingYuanPool, DefaultMingYuanWarmPool = nil, nil, nil
function GetDefaultNormalPool()
	if not DefaultNormalPool then
		DefaultNormalPool = CPoolType()
		local setting = House_Setting.GetData()
		local area = setting.DefaultNormalPool 
		local basex, basey = GetPoolBase(false, false)
		local heigh, width = setting.PoolMaxSize[0], setting.PoolMaxSize[1]
		for i = 0, area.Length-1, 4 do
			local sx, sy, ex, ey = area[i], area[i+1], area[i+2], area[i+3]
			sx, ex = math.min(sx, ex), math.max(sx, ex)
			sy, ey = math.min(sy, ey), math.max(sy, ey)
			for x = sx, ex do
				for y = sy, ey do
					DefaultNormalPool.PoolMark:SetBit((y-basey)*width + (x-basex), true)
				end
			end
		end
	end
	return DefaultNormalPool
end

function GetDefaultMingYuanPool(isWarmPool)
	if isWarmPool and DefaultMingYuanWarmPool then return DefaultMingYuanWarmPool end
	if not isWarmPool and DefaultMingYuanPool then return DefaultMingYuanPool end

	local pool = CPoolType()
	local setting = House_Setting.GetData()
	local area = isWarmPool and setting.DefaultMingYuanWarmPool or setting.DefaultMingYuanPool
	local basex, basey = GetPoolBase(true, isWarmPool)
	local heigh, width = setting.MingYuanPoolMaxSize[0], setting.MingYuanPoolMaxSize[1]
	for i = 0, area.Length-1, 4 do
		local sx, sy, ex, ey = area[i], area[i+1], area[i+2], area[i+3]
		sx, ex = math.min(sx, ex), math.max(sx, ex)
		sy, ey = math.min(sy, ey), math.max(sy, ey)
		for x = sx, ex do
			for y = sy, ey do
				pool.PoolMark:SetBit((y-basey)*width + (x-basex), true)
			end
		end
	end
	if isWarmPool then
		DefaultMingYuanWarmPool = pool
	else
		DefaultMingYuanPool = pool
	end
	return pool
end


local _MingYuanWarmPoolBaseX = 0
local _MingYuanWarmPoolBaseZ = 0
local _MingYuanPoolBaseX = 0
local _MingYuanPoolBaseZ = 0
local _NormalPoolBaseX = 0
local _NormalPoolBaseZ = 0
function GetPoolBase(isMingYuan, isWarmPool)
	if isMingYuan and isWarmPool then
		if _MingYuanWarmPoolBaseX==0 then
			local setting = House_Setting.GetData()
			_MingYuanWarmPoolBaseX,_MingYuanWarmPoolBaseZ=setting.MingYuanWarmPoolBase[0], setting.MingYuanWarmPoolBase[1]
		end
		return _MingYuanWarmPoolBaseX,_MingYuanWarmPoolBaseZ
	elseif isMingYuan then
		if _MingYuanPoolBaseX==0 then
			local setting = House_Setting.GetData()
			_MingYuanPoolBaseX,_MingYuanPoolBaseZ = setting.MingYuanPoolBase[0], setting.MingYuanPoolBase[1]
		end
		return _MingYuanPoolBaseX,_MingYuanPoolBaseZ
	else
		if _NormalPoolBaseX==0 then
			local setting = House_Setting.GetData()
			_NormalPoolBaseX,_NormalPoolBaseZ = setting.NormalPoolBase[0], setting.NormalPoolBase[1]
		end
		return _NormalPoolBaseX,_NormalPoolBaseZ
	end
end
local _MingYuanPoolMaxSizeX = 0
local _MingYuanPoolMaxSizeZ = 0
local _PoolMaxSizeX = 0
local _PoolMaxSizeZ = 0
function GetPoolSize(isMingYuan)
	if isMingYuan then
		if _MingYuanPoolMaxSizeX==0 then
			local setting = House_Setting.GetData()
			_MingYuanPoolMaxSizeX,_MingYuanPoolMaxSizeZ = setting.MingYuanPoolMaxSize[0], setting.MingYuanPoolMaxSize[1]
		end
		return _MingYuanPoolMaxSizeX,_MingYuanPoolMaxSizeZ
	else
		if _PoolMaxSizeX==0 then
			local setting = House_Setting.GetData()
			_PoolMaxSizeX,_PoolMaxSizeZ = setting.PoolMaxSize[0], setting.PoolMaxSize[1]
		end
		return _PoolMaxSizeX,_PoolMaxSizeZ
	end
end


local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local _poolmark_cache_close_ = nil
local _poolmark_cache_ = nil
function GetPoolMark(notInYard)
	if notInYard then
		if _poolmark_cache_close_ == nil then
			_poolmark_cache_close_ = {
				IsPoolAt = function() return false end,
				IsWarmPoolAt = function() return false end,
				IsWaterAt = function() return false end,
			}
		end
		return _poolmark_cache_close_
	else
		local isMingYuan = CClientFurnitureMgr.Inst.m_IsMingYuan
		local pool = CClientFurnitureMgr.Inst.m_Pool
		local defaultPool = GetDefaultNormalPool()
		pool = pool.PoolInited == 0 and defaultPool or pool
		if _poolmark_cache_==nil then
			_poolmark_cache_ = {
				IsPoolAt = function (self, x, y)
					local dx, dy = x - self.m_BaseX, y - self.m_BaseY
					if dx>=0 and dy>=0 then
						local mark = self.m_Pool.PoolMark
						return mark:GetBit(dy*self.m_Width + dx)
					else
						return false
					end
				end,
				IsWarmPoolAt = function (self, x, y)
					--普通水池也有可能放了温泉池装饰物
					local ret = CClientFurnitureMgr.Inst:IsSpecialPoolAt(x,y)
					if ret then return true end
					
					if not self.m_WarmPool then return false end
					local dx2, dy2 = x - self.m_WarmBaseX, y - self.m_WarmBaseY
					if dx2>=0 and dy2>=0 then
						local warmmark = self.m_WarmPool.PoolMark
						return warmmark:GetBit(dy2*self.m_Width + dx2)
					else
						return false
					end
				end,
				IsWaterAt = function (self, x, y)
					return self:IsWarmPoolAt(x,y) or self:IsPoolAt(x,y)
				end,
			}
		end

		_poolmark_cache_.IsEnable = CClientFurnitureMgr.s_PoolSwicthOn
		local height,width = GetPoolSize(isMingYuan)
		_poolmark_cache_.m_Width = width

		local basex,basey = GetPoolBase(isMingYuan, false)
		_poolmark_cache_.m_BaseX = basex
		_poolmark_cache_.m_BaseY = basey
		if isMingYuan then
			local basex2,basey2 = GetPoolBase(isMingYuan, true)
			local warmpool = CClientFurnitureMgr.Inst.m_WarmPool
			if warmpool.PoolInited == 0 then
				warmpool = GetDefaultMingYuanPool(true)
			end
			_poolmark_cache_.m_WarmBaseX = basex2
			_poolmark_cache_.m_WarmBaseY = basey2
			_poolmark_cache_.m_WarmPool = warmpool
		else
			_poolmark_cache_.m_WarmPool = nil
		end
		_poolmark_cache_.m_Pool = pool

		return _poolmark_cache_
	end
end

function IsAnythingOnWenQuanChi(id,line,column)
	local fur = CClientFurnitureMgr.inst:GetFurniture(id)
	if fur then
		local oldRot = (fur.ServerRotateY+360)%360
		local oldX = math.floor(fur.ServerPos.x)
		local oldZ = math.floor(fur.ServerPos.z)
		local xCount, zCount = line,column--designData.Space.line, designData.Space.column
		local xCenter, zCenter = math.floor(xCount / 2), math.floor(zCount / 2)
		local rotate

		if oldRot%360 == 90 then
			rotate = function(x, z)
				return z - zCenter, xCount - 1 - x - xCenter
			end
		elseif oldRot%360 == 180 then
			rotate = function(x, z)
				return xCount - 1 - x - xCenter, zCount - 1 - z - zCenter
			end
		elseif oldRot%360 == 270 then
			rotate = function(x, z)
				return zCount - 1 - z - zCenter, x - xCenter
			end
		else
			rotate = function(x, z)
				return x - xCenter, z - zCenter
			end
		end
		local ispool = Zhuangshiwu_Setting.GetData().WenQuanPoolSize


		local lookup = {}
		local index = 0
		-- local str=""
		for i = 0, zCount - 1 do
			for j = 0, xCount - 1 do
				local val = ispool[index]
				if val > 0 then--水池区域
					local xx, zz = rotate(j, i)
					xx = xx+oldX
					zz = zz+oldZ
					lookup[xx*1000+zz] = true
					-- str = str..","..tostring(xx*1000+zz)
				end
				index = index + 1
			end
		end
		-- print(str)
		local pass=true
		--看看水池区域内有没有其他装饰物
		CommonDefs.DictIterate(CClientFurnitureMgr.Inst.FurnitureList, DelegateFactory.Action_object_object(function (k,v) 
			if k~=id then
				-- if v.UseType==4 or v.UseType==5 then
					local kk = math.floor(v.ServerPos.x)*1000+math.floor(v.ServerPos.z)
					if lookup[kk] then
						pass = false
					end
				-- end
			end
		end))
		if not pass then
			return true
		end
	end
	return false
end
--温泉池的水域不能和其他水域相交
function IsWenQuanChiOnPool(x,z,rot,line,column)
	local oldRot = (rot+360)%360
	local oldX = x
	local oldZ = z
	local xCount, zCount = line,column
	local xCenter, zCenter = math.floor(xCount / 2), math.floor(zCount / 2)
	local rotate

	if oldRot%360 == 90 then
		rotate = function(x, z)
			return z - zCenter, xCount - 1 - x - xCenter
		end
	elseif oldRot%360 == 180 then
		rotate = function(x, z)
			return xCount - 1 - x - xCenter, zCount - 1 - z - zCenter
		end
	elseif oldRot%360 == 270 then
		rotate = function(x, z)
			return zCount - 1 - z - zCenter, x - xCenter
		end
	else
		rotate = function(x, z)
			return x - xCenter, z - zCenter
		end
	end
	local ispool = Zhuangshiwu_Setting.GetData().WenQuanPoolSize
	local index = 0
	for i = 0, zCount - 1 do
		for j = 0, xCount - 1 do
			local val = ispool[index]
			if val > 0 then--水池区域不能是水
				local xx, zz = rotate(j, i)
				xx = xx+oldX
				zz = zz+oldZ
				local ret = CClientFurnitureMgr.Inst:IsPoolAt(xx,zz)
				if ret then
					return true
				end
			end
			index = index + 1
		end
	end
	return false
end

FurnitureScene.Lua_CanMoveFurniture = function(id, tid, x, z, rot, scale, notInYard, kuozhangLvl)

		if g_FurnitureScene then
			local designData = LoadFurnitureDesignData(tid)
			if designData then
				if tid==9448 then--温泉池 看下是否有温泉装饰物在上面
					if IsAnythingOnWenQuanChi(id,designData.Space.line, designData.Space.column) then
						return false
					end
					if IsWenQuanChiOnPool(x, z,rot,designData.Space.line, designData.Space.column) then
						return false
					end
				end

				local poolmark = GetPoolMark(notInYard)

				local getOriginalGridBarrierAndInfo = nil
				--桥或台阶不考虑水池边缘的障碍
				if designData.UseType == EnumFurnitureUseType.eFurnitureOnWater or designData.UseType == EnumFurnitureUseType.eFurnitureInWater then
					getOriginalGridBarrierAndInfo = FurnitureScene_GetOriginalGridBarrierAndInfo
				else
					getOriginalGridBarrierAndInfo = FurnitureScene_GetOriginalGridBarrierAndInfoWithPoolBarrier
				end
				if x < 1 then
					return false
				end
				local multiEditingDict = CLuaHouseMgr.MultipleRoot and CLuaHouseMgr.MultipleRoot.mEditingDict or nil
				local bRes = g_FurnitureScene:CanMoveFurniture(id, designData, x, z, rot, scale, notInYard, kuozhangLvl, FurnitureScene_GetInfoById, getOriginalGridBarrierAndInfo, FurnitureScene_IsAnythingInGrid, nil, poolmark,multiEditingDict)
				return bRes
			end
		end

		return false
	end

function FurnitureScene_CanMoveTempFurniture(tid, x, z, rot, scale, notInYard, kuozhangLvl)
	if g_FurnitureScene then
		local designData = LoadFurnitureDesignData(tid)
		if designData then
			local poolmark = GetPoolMark(notInYard)

			local getOriginalGridBarrierAndInfo = nil
				--桥或台阶不考虑水池边缘的障碍
			if designData.UseType == EnumFurnitureUseType.eFurnitureOnWater or designData.UseType == EnumFurnitureUseType.eFurnitureInWater then
				getOriginalGridBarrierAndInfo = FurnitureScene_GetOriginalGridBarrierAndInfo
			else
				getOriginalGridBarrierAndInfo = FurnitureScene_GetOriginalGridBarrierAndInfoWithPoolBarrier
			end
			local bRes = g_FurnitureScene:CanMoveFurniture(0, designData, x, z, rot, scale, notInYard, kuozhangLvl, FurnitureScene_GetInfoById, getOriginalGridBarrierAndInfo, FurnitureScene_IsAnythingInGrid,true, poolmark)
			return bRes
		end
	end
	return false
end


function FurnitureScene_AddTempFurniture(tid, x, z, rot, scale)
	if g_FurnitureScene then
		local ddata = LoadFurnitureDesignData(tid)
		local bRes = g_FurnitureScene:AddTempFurniture(ddata, x, z, rot, scale, FurnitureScene_GetInfoById, FurnitureScene_GetGridBarrierAndOriginalGridBarrier, FurnitureScene_SetGridBarrier)
		return
	end
end

--移动守卫塔
FurnitureScene.Lua_CanMoveTower = function(id, tid, x, z, rot, scale)
	if g_FurnitureScene then
		local designData = LoadTowerDesignData(tid)
		
		if designData then
			local bRes = g_FurnitureScene:CanMoveTower(id, designData, x, z, rot, scale, FurnitureScene_GetInfoById, FurnitureScene_GetGridBarrierAndInfo, FurnitureScene_IsMainPlayerInGrid, FurnitureScene_IDInGrid)
			return bRes
		end
	end
	return false
end

FurnitureScene.Lua_MoveFurnitureChangeAppearance = function (id, oldTid, removeX, removeZ, removeR, newTid, newX, newZ, newR)
	if g_FurnitureScene then
		local designData = LoadFurnitureDesignData(oldTid)
		if designData and designData.FenceGuashiPos then
			g_FurnitureScene:ClearFenceGuashiPos()
		end

		designData = LoadFurnitureDesignData(newTid)
		if designData and designData.FenceGuashiPos then
			g_FurnitureScene:LoadFenceGuashiPos(designData.FenceGuashiPos)
		end
	end
end
	
local function GetLogicHeightList(data, rot, logicHeight)
	if not (rot and data) then
		return {}
	end
	local xCount, zCount = data.Space.line, data.Space.column
	local xCenter, zCenter = math.floor(xCount / 2), math.floor(zCount / 2)
	local rotate
	if rot%360 == 90 then
		rotate = function(x, z)
			return z - zCenter, xCount - 1 - x - xCenter
		end
	elseif rot%360 == 180 then
		rotate = function(x, z)
			return xCount - 1 - x - xCenter, zCount - 1 - z - zCenter
		end
	elseif rot%360 == 270 then
		rotate = function(x, z)
			return zCount - 1 - z - zCenter, x - xCenter
		end
	else
		rotate = function(x, z)
			return x - xCenter, z - zCenter
		end
	end
	local ret = {}

	for i = 0, zCount - 1 do
		for j = 0, xCount - 1 do
			local xx, zz = rotate(j, i)
			local hy = i*2+1
			local hx = j*2+1
			local lh = logicHeight[(xCount*2+1)*hy+hx + 1]
			--周围8个点的逻辑高
			lh = lh + logicHeight[(xCount*2+1)*hy+(hx+1) + 1]
			lh = lh + logicHeight[(xCount*2+1)*hy+(hx-1) + 1]
			lh = lh + logicHeight[(xCount*2+1)*(hy+1)+(hx) + 1]
			lh = lh + logicHeight[(xCount*2+1)*(hy-1)+(hx) + 1]
			lh = lh + logicHeight[(xCount*2+1)*(hy+1)+(hx+1) + 1]
			lh = lh + logicHeight[(xCount*2+1)*(hy-1)+(hx+1) + 1]
			lh = lh + logicHeight[(xCount*2+1)*(hy+1)+(hx-1) + 1]
			lh = lh + logicHeight[(xCount*2+1)*(hy-1)+(hx-1) + 1]

			if lh>0 then
				table.insert(ret, {x = xx, z = zz})
			end
		end
	end
	return ret
end
local function AddFurnitureToMap(id,ddata,x,z,rot,scale)
	--路
	if ddata.SubType == EnumFurnitureSubType.eXiaojing and ddata.UseType == EnumFurnitureUseType.eFurnitureGround then
		--根据逻辑高的部位
		if ddata.LogicHeight then
			local lhlist = GetLogicHeightList(ddata,rot,ddata.LogicHeight)
			for _, s in ipairs(lhlist) do
				local xx, zz = x + s.x, z + s.z
				if not g_FurnitureScene.m_Furnitures[xx] then
					g_FurnitureScene.m_Furnitures[xx] = {}
				end
				if not g_FurnitureScene.m_Furnitures[xx][zz] then
					g_FurnitureScene.m_Furnitures[xx][zz] = {}
				end
				g_FurnitureScene.m_Furnitures[xx][zz][id] = true
			end
		end
	--地毯
	elseif (ddata.SubType == EnumFurnitureSubType.eZawu and ddata.UseType == EnumFurnitureUseType.eFurnitureGround) then
		local spaceList = CFurnitureScene.GetSpaceList(ddata, rot, scale)
		for _, s in ipairs(spaceList) do
			local xx, zz = x + s.x, z + s.z

			if not g_FurnitureScene.m_Furnitures[xx] then
				g_FurnitureScene.m_Furnitures[xx] = {}
			end
			if not g_FurnitureScene.m_Furnitures[xx][zz] then
				g_FurnitureScene.m_Furnitures[xx][zz] = {}
			end
			g_FurnitureScene.m_Furnitures[xx][zz][id] = true
		end
	end
end
local function RemoveFurnitureFromMap(id,ddata,x,z,rot,scale)
	--路
	if ddata.SubType == EnumFurnitureSubType.eXiaojing and ddata.UseType == EnumFurnitureUseType.eFurnitureGround then
		--根据逻辑高的部位
		if ddata.LogicHeight then
			local lhlist = GetLogicHeightList(ddata,rot,ddata.LogicHeight)
			for _, s in ipairs(lhlist) do
				local xx, zz = x + s.x, z + s.z
				if g_FurnitureScene.m_Furnitures[xx] and g_FurnitureScene.m_Furnitures[xx][zz] then
					g_FurnitureScene.m_Furnitures[xx][zz][id] = nil
				end
			end
		end
	--地毯
	elseif ddata.SubType == EnumFurnitureSubType.eZawu and ddata.UseType == EnumFurnitureUseType.eFurnitureGround then
		local spaceList = CFurnitureScene.GetSpaceList(ddata, rot,scale)
		for _, s in ipairs(spaceList) do
			local xx, zz = x + s.x, z + s.z
			if g_FurnitureScene.m_Furnitures[xx] and g_FurnitureScene.m_Furnitures[xx][zz] then
				g_FurnitureScene.m_Furnitures[xx][zz][id] = nil
			end
		end
	end
end

FurnitureScene.Lua_MoveFurniture = function(id, removedFurTid, removedFurX, removedFurZ, removedRot, removedScale, tid, x, z, rot, scale)

		if g_FurnitureScene then
			local removedData = LoadFurnitureDesignData(removedFurTid)
			local ddata = LoadFurnitureDesignData(tid)
			local bRes = g_FurnitureScene:MoveFurniture(id, removedData, removedFurX, removedFurZ, removedRot, removedScale, ddata, x, z, rot, scale, FurnitureScene_GetInfoById, FurnitureScene_GetGridBarrierAndOriginalGridBarrier, FurnitureScene_SetGridBarrier)

			if removedData then
				RemoveFurnitureFromMap(id,removedData,removedFurX,removedFurZ,removedRot,removedScale)
			end
			if ddata then
				AddFurnitureToMap(id,ddata,x,z,rot,scale)
			end
			return
		end

	end

FurnitureScene.Lua_MoveTower = function(id, removedFurTid, removedFurX, removedFurZ, removedRot, removedScale, tid, x, z, rot, scale)
	if g_FurnitureScene then
		local removedData = LoadTowerDesignData(removedFurTid)
		local ddata = LoadTowerDesignData(tid)
		local bRes = g_FurnitureScene:MoveFurniture(id, removedData, removedFurX, removedFurZ, removedRot, removedScale, ddata, x, z, rot, scale, FurnitureScene_GetInfoById, FurnitureScene_GetGridBarrierAndOriginalGridBarrier, FurnitureScene_SetGridBarrier)
		return
	end
end

Zhuangshiwu = import "L10.Game.Zhuangshiwu_Zhuangshiwu"

__Zhuangshiwu_Cache__ = {}

function LoadFenceGuashiPos(tid)

	local guashiPos = Zhuangshiwu_FenceGuashiPos.GetData(tid)
	if not (guashiPos and guashiPos.PosArea) then return nil end

	local areaStr = guashiPos.PosArea

	local function parse_begin_end(str)
		if string.find(str, "-", 1, true) then
			local begin, finish = string.match(str, "(%d+)-(%d+)")
			begin, finish = tonumber(begin), tonumber(finish)
			begin, finish = math.min(begin, finish), math.max(begin, finish)
			return begin, finish
		else
			local n = tonumber(str)
			return n, n
		end
	end

	local areas = {}
	for segment in string.gmatch(areaStr, "([^;]+)") do
		for xPart, zPart in string.gmatch(segment, "([%d-]+),([%d-]+)") do
			local xStart, xEnd = parse_begin_end(xPart)
			local zStart, zEnd = parse_begin_end(zPart)
			table.insert(areas, {XStart = xStart, XEnd = xEnd, ZStart = zStart, ZEnd = zEnd})
		end
	end

	return areas
end

function LoadFurnitureDesignData(tid)
	local ret = __Zhuangshiwu_Cache__[tid]
	if ret then
		return ret
	end

	local data = Zhuangshiwu.GetData(tid)
	if not data then
		return
	end

	local designBarrier = data.Barrier
	local barrier = nil
	local barrierCount = 0
	if designBarrier then
		barrierCount = designBarrier.Length
		barrier = {}
		for i = 1, barrierCount do
			barrier[i] = designBarrier[i - 1]
		end
	end

	local line, column = data.SpaceWidth,data.SpaceHeight--string.match(data.Space, "(%d+)%*(%d+)")
	line, column = tonumber(line), tonumber(column)
	if not (line and column and line * column == barrierCount) then
		return
	end

	local scaleInterval = Zhuangshiwu_Setting.GetData().FurnitureScaleInterval
	ret = {
		Barrier = barrier,
		Space = {
			line = line,
			column = column,
		},
		UseType = data.UseType,
		Location = data.Location,
		Type = data.Type,
		SubType = data.SubType,
		FenceGuashiPos = LoadFenceGuashiPos(tid),
		maxScale = scaleInterval[0],
		minScale = scaleInterval[1],
	}
	--地面记录logicheight
	if ret.SubType == EnumFurnitureSubType.eXiaojing and ret.UseType == EnumFurnitureUseType.eFurnitureGround then
		local logicHeight = data.LogicHeight
		if logicHeight then
			if (line*2+1)*(column*2+1)==logicHeight.Length then
			local t = {}
			for i=1,logicHeight.Length do
				table.insert( t, logicHeight[i-1] )
			end
			ret.LogicHeight = t
			end
		end
	end

	__Zhuangshiwu_Cache__[tid] = ret
	return ret
end

TowerDefense_Tower = import "L10.Game.TowerDefense_Tower"

__TowerDefense_Tower_Cache__ = {}

function LoadTowerDesignData(tid)
	local ret = __TowerDefense_Tower_Cache__[tid]
	if ret then
		return ret
	end

	local data = TowerDefense_Tower.GetData(tid)
	if not data then 
		return LoadFurnitureDesignData(tid)
		--data = Zhuangshiwu_Zhuangshiwu.GetData(tid)
	end
	if not data then
		return
	end
	local line, column = data.SpaceWidth,data.SpaceHeight--string.match(data.Space, "(%d+)%*(%d+)")
	line, column = tonumber(line), tonumber(column)
	if not (line and column) then
		return
	end
	local barrierCount=line*column
	local barrier = {}
	for i=1,barrierCount do
		barrier[i]=1
	end

	local scaleInterval = Zhuangshiwu_Setting.GetData().FurnitureScaleInterval
	ret = {
		Barrier = barrier,
		Space = {
			line = line,
			column = column,
		},
		-- UseType = data.UseType,
		-- Location = data.Location,
		-- Type = data.Type,
		-- SubType = data.SubType,
		maxScale = scaleInterval[0],
		minScale = scaleInterval[1],
	}
	__TowerDefense_Tower_Cache__[tid] = ret
	return ret
end

require "common/house/furnituresceneinc"
