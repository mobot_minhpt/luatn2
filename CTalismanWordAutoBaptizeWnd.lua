-- Auto Generated!!
local CAutoBaptizeConditionItem = import "L10.UI.CAutoBaptizeConditionItem"
local CItemMgr = import "L10.Game.CItemMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local CPropertyDifMgr = import "L10.UI.CPropertyDifMgr"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local CTalismanWordAutoBaptizeWnd = import "L10.UI.CTalismanWordAutoBaptizeWnd"
local CTalismanWordBaptizeMgr = import "L10.UI.CTalismanWordBaptizeMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EventManager = import "EventManager"
local Gac2Gas = import "L10.Game.Gac2Gas"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Object = import "System.Object"
local PlayerShowDifProType = import "L10.Game.PlayerShowDifProType"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CTalismanWordAutoBaptizeWnd.m_Init_CS2LuaHook = function (this) 
    CTalismanWordBaptizeMgr.InitConditionWord()
    this.wordList:Init()
    this.costDetailView:Init()
    this.speedLabel.text = Constants.SpeedLevel1

    UIEventListener.Get(this.lookupDifBtn).onClick = MakeDelegateFromCSFunction(this.OnClickLookupDifButton, VoidDelegate, this)
end
CTalismanWordAutoBaptizeWnd.m_OnClickLookupDifButton_CS2LuaHook = function (this, go) 
    local talisman = CTalismanWordBaptizeMgr.selectTalisman
    local equipListOnBody = CItemMgr.Inst:GetPlayerEquipmentListAtPlace(EnumItemPlace.Body)

    local find = false
    do
        local i = 0
        while i < equipListOnBody.Count do
            if equipListOnBody[i].itemId == talisman.itemId then
                find = true
                break
            end
            i = i + 1
        end
    end
    if find then
        --是否重铸
        if CTalismanWordBaptizeMgr.baseWordBaptize then
            Gac2Gas.TryConfirmResetTalismanBasicWord(EnumToInt(talisman.place), talisman.pos, talisman.itemId)
        else
            --炼化
            Gac2Gas.TryConfirmResetTalismanExtraWord(EnumToInt(talisman.place), talisman.pos, talisman.itemId)
        end
    else
        g_MessageMgr:ShowMessage("Equip_Compare_NotWear_Tips")
    end
end
CTalismanWordAutoBaptizeWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.raceButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.raceButton).onClick, MakeDelegateFromCSFunction(this.OnRaceButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.baptizeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.baptizeButton).onClick, MakeDelegateFromCSFunction(this.OnBaptizeButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.targetButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.targetButton).onClick, MakeDelegateFromCSFunction(this.OnTargetButtonClick, VoidDelegate, this), true)

    EventManager.AddListener(EnumEventType.ResetTalismanWordSuccess, MakeDelegateFromCSFunction(this.OnSuccess, Action0, this))
    EventManager.AddListener(EnumEventType.ResetTalismanWordFailed, MakeDelegateFromCSFunction(this.OnFail, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.TryConfirmBaptizeWordResult, MakeDelegateFromCSFunction(this.OnTryConfirmBaptizeWordResult, MakeGenericClass(Action1, Object), this))
end
CTalismanWordAutoBaptizeWnd.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.raceButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.raceButton).onClick, MakeDelegateFromCSFunction(this.OnRaceButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.baptizeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.baptizeButton).onClick, MakeDelegateFromCSFunction(this.OnBaptizeButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.targetButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.targetButton).onClick, MakeDelegateFromCSFunction(this.OnTargetButtonClick, VoidDelegate, this), false)

    EventManager.RemoveListener(EnumEventType.ResetTalismanWordSuccess, MakeDelegateFromCSFunction(this.OnSuccess, Action0, this))
    EventManager.RemoveListener(EnumEventType.ResetTalismanWordFailed, MakeDelegateFromCSFunction(this.OnFail, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.TryConfirmBaptizeWordResult, MakeDelegateFromCSFunction(this.OnTryConfirmBaptizeWordResult, MakeGenericClass(Action1, Object), this))
end
CTalismanWordAutoBaptizeWnd.m_OnTryConfirmBaptizeWordResult_CS2LuaHook = function (this, obj) 
    local list = TypeAs(obj, typeof(MakeGenericClass(List, PlayerShowDifProType)))
    if list == nil then
        return
    end
    if list.Count == 0 then
        g_MessageMgr:ShowMessage("BaptizeWordResultNoPropDif")
        return
    end
    CPropertyDifMgr.ShowPlayerProDifWithEquip(list)
end
CTalismanWordAutoBaptizeWnd.m_OnRaceButtonClick_CS2LuaHook = function (this, go) 
    if this.speedLabel.text == Constants.SpeedLevel1 then
        this.speedLabel.text = Constants.SpeedLevel2
        this.RequestRate = this.InitRate / 2
    elseif this.speedLabel.text == Constants.SpeedLevel2 then
        this.speedLabel.text = Constants.SpeedLevel3
        this.RequestRate = this.InitRate / 4
    elseif this.speedLabel.text == Constants.SpeedLevel3 then
        this.speedLabel.text = Constants.SpeedLevel1
        this.RequestRate = this.InitRate
    end
end
CTalismanWordAutoBaptizeWnd.m_OnBaptizeButtonClick_CS2LuaHook = function (this, go) 
    --if (autoBaptizeRoutine != null)
    --{
    --    StopCoroutine(autoBaptizeRoutine);
    --    autoBaptizeRoutine = null;
    --}
    --开始洗炼
    if not this.requesting then
        local hasReplaced = true
        local talisman = CItemMgr.Inst:GetById(CTalismanWordBaptizeMgr.selectTalisman.itemId)
        local wordContents = talisman.Equip:GetTempExtWordArray()
        do
            local i = 0
            while i < wordContents.Length do
                if wordContents[i] ~= 0 then
                    hasReplaced = false
                    break
                end
                i = i + 1
            end
        end
        if this.achieveTheGoal and not hasReplaced then
            local tip = g_MessageMgr:FormatMessage("Already_Reach_Target")
            MessageWndManager.ShowOKCancelMessage(tip, DelegateFactory.Action(function () 
                this:TryBaptize()
            end), nil, nil, nil, false)
        else
            this:TryBaptize()
        end
    else
        --EndRequest();
        --ClearActions();
        this.requesting = false
    end
end
CTalismanWordAutoBaptizeWnd.m_EndRequest_CS2LuaHook = function (this) 
    g_MessageMgr:ShowMessage("Stop_Refinery")
    this.requesting = false
    --所有的toggle恢复操作
    local cmps = CommonDefs.GetComponentsInChildren_Component_Type(this.transform, typeof(CAutoBaptizeConditionItem))
    CommonDefs.EnumerableIterate(cmps, DelegateFactory.Action_object(function (___value) 
        local item = ___value
        CUICommonDef.SetActive(item.gameObject, true, true)
    end))

    --成功一次就行了
    if this.successOnce then
        this.wordList:ShowReplaceButton(true)
    end

    this.startBaptizeLabel.text = LocalString.GetString("开始洗炼")
end
CTalismanWordAutoBaptizeWnd.m_TryBaptize_CS2LuaHook = function (this) 
    if this.costDetailView.countAvailable and this.costDetailView.yinliangAvailable then
        if CTalismanWordBaptizeMgr.selectTalisman ~= nil then
            --炼化消耗条件满足
            this.autoBaptizeRoutine = this:StartCoroutine(this:StartAutoBaptizeRoutine())
        end
    elseif not this.costDetailView.yinliangAvailable then
        g_MessageMgr:ShowMessage("NotEnough_Silver")
    elseif not this.costDetailView.countAvailable then
        if not this.costDetailView.useLingyu then
            g_MessageMgr:ShowMessage("Talisman_Refinery_No_Item")
        elseif this.costDetailView.lingyuAvailable then
            --炼化消耗条件满足
            this.autoBaptizeRoutine = this:StartCoroutine(this:StartAutoBaptizeRoutine())
        else
            CShopMallMgr.TryCheckEnoughJade(this.costDetailView.lingyuCost, this.costDetailView.TemplateId)
        end
    end
end
CTalismanWordAutoBaptizeWnd.m_BaptizeTalisman_CS2LuaHook = function (this) 
    if this.costDetailView.countAvailable and this.costDetailView.yinliangAvailable then
        if CTalismanWordBaptizeMgr.selectTalisman ~= nil then
            Gac2Gas.RequestResetTalismanExtraWord(EnumToInt(CTalismanWordBaptizeMgr.selectTalisman.place), CTalismanWordBaptizeMgr.selectTalisman.pos, CTalismanWordBaptizeMgr.selectTalisman.itemId, this.costDetailView.useLingyu)
            return true
        end
    elseif not this.costDetailView.yinliangAvailable then
        g_MessageMgr:ShowMessage("NotEnough_Silver")
    elseif not this.costDetailView.countAvailable then
        if not this.costDetailView.useLingyu then
            g_MessageMgr:ShowMessage("Talisman_Refinery_No_Item")
        elseif this.costDetailView.lingyuAvailable then
            Gac2Gas.RequestResetTalismanExtraWord(EnumToInt(CTalismanWordBaptizeMgr.selectTalisman.place), CTalismanWordBaptizeMgr.selectTalisman.pos, CTalismanWordBaptizeMgr.selectTalisman.itemId, this.costDetailView.useLingyu)
            return true
        else
            CShopMallMgr.TryCheckEnoughJade(this.costDetailView.lingyuCost, this.costDetailView.TemplateId)
        end
    end
    return false
end
CTalismanWordAutoBaptizeWnd.m_ProcessUpdateViewEvent_CS2LuaHook = function (this, eventType) 
    --看一下这个窗口的类型
    --看一下这个窗口的类型
    local top = CUIManager.instance:GetTopPopUI()
    if top ~= nil then
        if top.Name == CUIResources.EquipWordAutoBaptizeWnd.Name then
            this.pause = false
        else
            this.pause = true
        end
    end
end
CTalismanWordAutoBaptizeWnd.m_OnFail_CS2LuaHook = function (this) 
    if this.onFail ~= nil then
        invoke(this.onFail)
    end
end
CTalismanWordAutoBaptizeWnd.m_OnSuccess_CS2LuaHook = function (this) 
    if this.onSuccess ~= nil then
        invoke(this.onSuccess)
    end
end
CTalismanWordAutoBaptizeWnd.m_BeginRequest_CS2LuaHook = function (this) 
    g_MessageMgr:ShowMessage("Start_Refinery")
    this.achieveTheGoal = false
    this.successOnce = false
    this.wordList.needConfirm = false
    this.requestCount = 0
    --baptizeResult = new CEquipmentBaptizeMgr.BaptizeResult();
    this.requesting = true

    this.wordList:ShowReplaceButton(false)

    this.startBaptizeLabel.text = LocalString.GetString("停止洗炼")
end
