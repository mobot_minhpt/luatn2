-- Auto Generated!!
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local BoxCollider = import "UnityEngine.BoxCollider"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CInviteFriendMgr = import "L10.Game.CInviteFriendMgr"
local CInviteFriendWnd = import "L10.UI.CInviteFriendWnd"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CUITexture = import "L10.UI.CUITexture"
local DelegateFactory = import "DelegateFactory"
local Extensions = import "Extensions"
local Item_Item = import "L10.Game.Item_Item"
local L10 = import "L10"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local Vector3 = import "UnityEngine.Vector3"
CInviteFriendWnd.m_AddRewardItem_CS2LuaHook = function (this, rewardInfo, getSign) 
    local itemTemplate = Item_Item.GetData(rewardInfo.ItemID)
    local itemNode = NGUITools.AddChild(this.RewardTable.gameObject, this.RewardItemTemplate)
    itemNode:SetActive(true)
    local button = itemNode.transform:Find("button").gameObject
    CommonDefs.GetComponent_Component_Type(itemNode.transform:Find("icon"), typeof(CUITexture)):LoadMaterial(itemTemplate.Icon)
    UIEventListener.Get(itemNode.transform:Find("icon/button").gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(itemNode.transform:Find("icon/button").gameObject).onClick, DelegateFactory.VoidDelegate(function (p) 
        CItemInfoMgr.ShowLinkItemTemplateInfo(rewardInfo.ItemID, false, nil, AlignType.Default, 0, 0, 0, 0)
    end), true)

    CommonDefs.GetComponent_Component_Type(itemNode.transform:Find("label"), typeof(UILabel)).text = (LocalString.GetString("邀请") .. tostring(rewardInfo.Value)) .. LocalString.GetString("人达到50级")

    if CInviteFriendMgr.Inst.AchieveNum >= rewardInfo.Value then
        button:SetActive(true)
        if getSign then
            button.transform.position = Vector3(button.transform.position.x, button.transform.position.y, - 1)
            CommonDefs.GetComponent_Component_Type(button.transform:Find("label"), typeof(UILabel)).text = LocalString.GetString("已领取")
            CommonDefs.GetComponent_GameObject_Type(button, typeof(BoxCollider)).enabled = false
        else
            CommonDefs.GetComponent_Component_Type(button.transform:Find("label"), typeof(UILabel)).text = LocalString.GetString("领取")
            UIEventListener.Get(button).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(button).onClick, DelegateFactory.VoidDelegate(function (p) 
                Gac2Gas.RequestInvitationAward(rewardInfo.Value)
            end), true)
        end
    else
        button:SetActive(true)
        button.transform.position = Vector3(button.transform.position.x, button.transform.position.y, - 1)
        CommonDefs.GetComponent_Component_Type(button.transform:Find("label"), typeof(UILabel)).text = LocalString.GetString("未达成")
        CommonDefs.GetComponent_GameObject_Type(button, typeof(BoxCollider)).enabled = false
    end
end
CInviteFriendWnd.m_OnReply_CS2LuaHook = function (this) 
    if CInviteFriendMgr.Inst.InviterId ~= 0 then
        this.CanInviteNode:SetActive(false)
        this.CantInviteNode:SetActive(true)
        CommonDefs.GetComponent_Component_Type(this.CantInviteNode.transform:Find("Name"), typeof(UILabel)).text = CInviteFriendMgr.Inst.InviterName
        CommonDefs.GetComponent_Component_Type(this.CantInviteNode.transform:Find("ID"), typeof(UILabel)).text = "ID " .. tostring(CInviteFriendMgr.Inst.InviterId)
    else
        this.CanInviteNode:SetActive(true)
        this.CantInviteNode:SetActive(false)
        UIEventListener.Get(this.CanInviteNode.transform:Find("Button").gameObject).onClick = DelegateFactory.VoidDelegate(function (p) 
            if (CommonDefs.GetComponent_Component_Type(this.CanInviteNode.transform:Find("Input"), typeof(UILabel)).text == "") then
                --Please_Type_Invitation_Code
                g_MessageMgr:ShowMessage("Please_Type_Invitation_Code")
                return
            end
            local playerId = math.floor(tonumber(CommonDefs.GetComponent_Component_Type(this.CanInviteNode.transform:Find("Input"), typeof(UILabel)).text or 0))
            if playerId ~= nil then
                Gac2Gas.SubmitInvitationCode(playerId)
            end
        end)
    end

    this.RewardItemTemplate:SetActive(false)
    Extensions.RemoveAllChildren(this.RewardTable.transform)

    do
        local i = 0
        while i < CInviteFriendMgr.Inst.RewardList.Count do
            local rewardInfo = CInviteFriendMgr.Inst.RewardList[i]
            if CInviteFriendMgr.Inst.ReceiveTbl ~= nil then
                local getSign = false
                CommonDefs.EnumerableIterate(CInviteFriendMgr.Inst.ReceiveTbl, DelegateFactory.Action_object(function (___value) 
                    local b = ___value
                    if rewardInfo.Value == b then
                        getSign = true
                        return
                    end
                end))
                this:AddRewardItem(rewardInfo, getSign)
            else
                this:AddRewardItem(rewardInfo, false)
            end
            i = i + 1
        end
    end

    this.RewardTable:Reposition()
    this.RewardScrollView:ResetPosition()

    UIEventListener.Get(this.RankButton).onClick = DelegateFactory.VoidDelegate(function (p) 
        Gac2Gas.QueryInvitationRankInfo()
    end)

    UIEventListener.Get(this.TotalInviteInfoButton).onClick = DelegateFactory.VoidDelegate(function (p) 
        Gac2Gas.QueryInvitedPlayerInfo()
    end)

    this.InviteText.text = (LocalString.GetString("将[06D767]角色ID（") .. tostring(CClientMainPlayer.Inst.Id)) .. LocalString.GetString("）[FFFFFF]告知好友就可以邀请他一起加入游戏哦！赶快点击查看可以邀请的好友吧！")
end
CInviteFriendWnd.m_Awake_CS2LuaHook = function (this) 
    if CInviteFriendWnd.Instance ~= nil then
        L10.CLogMgr.LogError("more than one instance running!")
    end
    CInviteFriendWnd.Instance = this
end
