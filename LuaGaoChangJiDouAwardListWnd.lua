local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItem = import "L10.Game.CItem"

LuaGaoChangJiDouAwardListWnd = class()

LuaGaoChangJiDouAwardListWnd.s_Reward = nil

RegistClassMember(LuaGaoChangJiDouAwardListWnd, "RewardGrid")
RegistClassMember(LuaGaoChangJiDouAwardListWnd, "RewardTemplate")
RegistClassMember(LuaGaoChangJiDouAwardListWnd, "ItemTemplate")


function LuaGaoChangJiDouAwardListWnd:Awake()
    self.RewardGrid = self.transform:Find("Anchor/RewardView/Grid"):GetComponent(typeof(UIGrid))
    self.RewardTemplate = self.transform:Find("Anchor/RewardView/RewardTemplate").gameObject
    self.ItemTemplate = self.transform:Find("Anchor/RewardView/RewardTemplate/ItemTemplate").gameObject

    self.RewardTemplate:SetActive(false)
    self.ItemTemplate:SetActive(false)
end

function LuaGaoChangJiDouAwardListWnd:Init()
    local mailIds = {}
    for str in string.gmatch(LuaGaoChangJiDouAwardListWnd.s_Reward, "([^,]+)") do
        table.insert(mailIds, tonumber(str))
    end
    Extensions.RemoveAllChildren(self.RewardGrid.transform)
    if mailIds then
        local preRank = 0
        for i = 1, 10, 2 do
            local mailId = mailIds[i + 1]
            local rank = mailIds[i]
            local reward = NGUITools.AddChild(self.RewardGrid.gameObject, self.RewardTemplate)
            reward:SetActive(true)
            local label = reward.transform:Find("RewardLabel"):GetComponent(typeof(UILabel))
            local selected = reward.transform:Find("Selected").gameObject
            local selectedIcon = reward.transform:Find("RewardLabel/Icon").gameObject
            label.text = LocalString.GetString("高昌激斗第")..(preRank+1).."-"..rank..LocalString.GetString("名")
            if LuaGaoChangJiDouMgr.MyInfo.rank and preRank < LuaGaoChangJiDouMgr.MyInfo.rank and rank >= LuaGaoChangJiDouMgr.MyInfo.rank then
                selected:SetActive(true)
                selectedIcon:SetActive(true)
                label.color = NGUIText.ParseColor24("00ff60", 0)
            else
                selected:SetActive(false)
                selectedIcon:SetActive(false)
            end
            preRank = rank

            local grid = reward.transform:Find("Grid"):GetComponent(typeof(UIGrid))
            local maildata = Mail_Mail.GetData(mailId)
            local itemstr = maildata.Items
            for cronStr in string.gmatch(itemstr, "([^;]+);?") do
                local itemId, count, bind = string.match(cronStr, "(%d+),(%d+),(%d+)")
                itemId, count, bind = tonumber(itemId), tonumber(count), tonumber(bind)
                local item = Item_Item.GetData(itemId)
                local award = NGUITools.AddChild(grid.gameObject, self.ItemTemplate)
                award:SetActive(true)
                award.transform:Find("Item"):GetComponent(typeof(CUITexture)):LoadMaterial(item.Icon)
                award.transform:Find("Count"):GetComponent(typeof(UILabel)).text = count
                award.transform:Find("Border"):GetComponent(typeof(UISprite)).spriteName = CUICommonDef.GetItemCellBorder(CItem.GetQualityType(item.ID))
                UIEventListener.Get(award).onClick = DelegateFactory.VoidDelegate(function (go)
                    CItemInfoMgr.ShowLinkItemTemplateInfo(itemId,false,nil,AlignType.Default, 0, 0, 0, 0)
                end)
            end
            grid:Reposition()
        end
	end
    self.RewardGrid:Reposition()
end
