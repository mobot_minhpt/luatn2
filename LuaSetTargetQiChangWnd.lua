local UITable = import "UITable"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaSetTargetQiChangWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSetTargetQiChangWnd, "UnActivePreTable", "UnActivePreTable", UITable)
RegistChildComponent(LuaSetTargetQiChangWnd, "UnActivePreTemplate", "UnActivePreTemplate", GameObject)
RegistChildComponent(LuaSetTargetQiChangWnd, "TipLabel", "TipLabel", UILabel)
RegistChildComponent(LuaSetTargetQiChangWnd, "CuJieBanLabel", "CuJieBanLabel", UILabel)
RegistChildComponent(LuaSetTargetQiChangWnd, "CommitButton", "CommitButton", GameObject)
RegistChildComponent(LuaSetTargetQiChangWnd, "CancelBtn", "CancelBtn", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaSetTargetQiChangWnd,"mQiChangId")
RegistClassMember(LuaSetTargetQiChangWnd,"SelectedBaby")
function LuaSetTargetQiChangWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.CommitButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCommitButtonClick()
	end)


	
	UIEventListener.Get(self.CancelBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCancelBtnClick()
	end)


    --@endregion EventBind end
	self.UnActivePreTemplate:SetActive(false)
end

function LuaSetTargetQiChangWnd:Init()
	self.mQiChangId = LuaBabyMgr.mSetTargetWndQiChangId
	self.SelectedBaby = LuaBabyMgr.m_ChosenBaby

	local activedQiChang = self.SelectedBaby.Props.QiChangData.ActivedQiChang

	Extensions.RemoveAllChildren(self.UnActivePreTable.transform)

	local unActivePres = {}
	--所有前置
	local targetData = Baby_QiChang.GetData(self.mQiChangId)
	local data = targetData
	while data and data.PreQichang>0 do
		local isActive =  CommonDefs.DictContains_LuaCall(activedQiChang, data.PreQichang)
		if not isActive then
			table.insert(unActivePres,data.PreQichang)
		end
		data = Baby_QiChang.GetData(data.PreQichang)	
	end

	for i,id in ipairs(unActivePres) do
		local go = CUICommonDef.AddChild(self.UnActivePreTable.gameObject, self.UnActivePreTemplate)
		local onUnActivePreTemplateClick = function(go) 
			g_ScriptEvent:BroadcastInLua("FindAndGotoQiChang", id)
			CUIManager.CloseUI(CLuaUIResources.SetTargetQiChangWnd)
		end
		CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(onUnActivePreTemplateClick), false)
		go:SetActive(true)

		local Label = go.transform:Find("Label"):GetComponent(typeof(UILabel))
		local idata = Baby_QiChang.GetData(id)
		Label.text = SafeStringFormat3(LocalString.GetString("[%s]【%s】[-]"), LuaBabyMgr.GetQiChangNameColor(idata.Quality-1), self:GetQiChangName(idata))
	end
	self.UnActivePreTable:Reposition()

	--label
	self.TipLabel.text = SafeStringFormat3(LocalString.GetString("仍要选择[%s]【%s】[-]作为目标气场吗？"),LuaBabyMgr.GetQiChangNameColor(targetData.Quality-1), self:GetQiChangName(targetData))

	local jdata = Baby_QiChang.GetData(CClientMainPlayer.Inst.PlayProp.JieBanQiChangId)
	if not jdata then
		self.CuJieBanLabel.text = ""
	else
		self.CuJieBanLabel.text = SafeStringFormat3(LocalString.GetString("当前结伴气场为[%s]【%s】[-]"),LuaBabyMgr.GetQiChangNameColor(jdata.Quality-1), self:GetQiChangName(jdata))
	end
end

function LuaSetTargetQiChangWnd:GetQiChangName(qichang)
    local name = qichang.NameM
    if self.SelectedBaby.Gender == 1 then
        name = qichang.NameF
    end
    return name
end

--@region UIEvent

function LuaSetTargetQiChangWnd:OnCommitButtonClick()
	local babyId = self.SelectedBaby.Id
    Gac2Gas.RequestSetTargetQiChang(babyId, self.mQiChangId)
    CUIManager.CloseUI(CLuaUIResources.BabyQiChangRequestTip)
	self.CommitButton:SetActive(false)
end

function LuaSetTargetQiChangWnd:OnCancelBtnClick()
	CUIManager.CloseUI(CLuaUIResources.SetTargetQiChangWnd)
end


--@endregion UIEvent

