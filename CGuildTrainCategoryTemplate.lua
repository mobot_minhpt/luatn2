-- Auto Generated!!
local CGuildTrainCategoryTemplate = import "L10.UI.CGuildTrainCategoryTemplate"
local CGuildTrainMgr = import "L10.Game.CGuildTrainMgr"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CUICommonDef = import "L10.UI.CUICommonDef"
local DelegateFactory = import "DelegateFactory"
local UILabel = import "UILabel"
CGuildTrainCategoryTemplate.m_Init_CS2LuaHook = function (this, index) 
    this.categoryIndex = index
    this.nameLabel.text = ""
    this.setCountLabel.text = ""

    this.trainItemList:Init(index)
    this.trainItemList.OnSettingChanged = MakeDelegateFromCSFunction(this.InitCountLabel, Action0, this)
    local baseInfo = CGuildTrainMgr.Inst.trainCategoryList[index]
    this.nameLabel.text = baseInfo.categoryName
    this.setCountLabel.text = System.String.Format("{0}/{1}", baseInfo.selectedCount, baseInfo.totalAvailableCount)
    if baseInfo.totalAvailableCount ~= 0 then
        this.setCountLabel.color = Color.green
        this.categoryNameLabel.color = Color.green
    else
        this.setCountLabel.color = Color.white
        this.categoryNameLabel.color = Color.white

        --所有内容置灰，不可点击
        CUICommonDef.SetActive(this.gameObject, false, true)
        local labels = CommonDefs.GetComponentsInChildren_GameObject_Type(this.trainItemList.gameObject, typeof(UILabel))
        CommonDefs.EnumerableIterate(labels, DelegateFactory.Action_object(function (___value) 
            item = ___value
            item.color = Color.gray
        end))
    end
    local row = math.floor(math.floor((baseInfo.trainItemList.Count - 1) / this.table.columns)) + 1
    this.background.height = row * this.trainItemCellBackground.height + (row * 2 + 1) * math.floor(this.table.padding.y) + this.offset
end
CGuildTrainCategoryTemplate.m_InitCountLabel_CS2LuaHook = function (this) 
    local baseInfo = CGuildTrainMgr.Inst.trainCategoryList[this.categoryIndex]
    this.setCountLabel.text = System.String.Format("{0}/{1}", baseInfo.selectedCount, baseInfo.totalAvailableCount)
    if baseInfo.totalAvailableCount ~= 0 then
        this.setCountLabel.color = Color.green
        this.categoryNameLabel.color = Color.green
    else
        this.setCountLabel.color = Color.white
        this.categoryNameLabel.color = Color.white
    end
end
