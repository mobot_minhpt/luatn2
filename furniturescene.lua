
-- furniture scene for gas and gac usage

function CFurnitureScene.IsOpen360Rotation() -- 是否开启有障碍装饰物Y轴360°旋转
	return true
end

function CFurnitureScene.IsOpenObstacleScaling() -- 是否开启有障碍装饰物缩放
	return true
end

function CFurnitureScene:Ctor(gridWidth, gridHeight, list, invalid, notInYard, kuozhangLvl, getInfoById, getGridBarrier, setGridBarrier, getOriginalGridBarrierAndInfo, posholder, poolmark)
	self.m_GridWidth, self.m_GridHeight = gridWidth, gridHeight
	self.m_Line, self.m_Column = {}, {}
	self.m_TempLine, self.m_TempColumn = {}, {}
	self.m_TempInfo = {}
	self.TempId = 0
	self.m_Furnitures = {}

	for _, id in ipairs(list) do
		local info = getInfoById(id)
		if info then
			local barrierList = CFurnitureScene.GetBarrierList(info.data, info.rot, info.scale)
			if invalid and not self:CanMoveFurniture(id, info.data, info.x, info.z, info.rot, info.scale, notInYard, info.kuozhangLvl or  kuozhangLvl, getInfoById, getOriginalGridBarrierAndInfo, nil, nil, poolmark) then
				table.insert(invalid, id)
			else
				self:AddFurnitureToMap(id, barrierList, info.x, info.z)
				for _, b in ipairs(barrierList) do
					local x, z, val = info.x + b.x, info.z + b.z, b.val
					local cb = getGridBarrier(x, z)
					if cb ~= EBarrierType.eBT_OutRange and val ~= EBarrierType.eBT_LowBarrier and val > cb then
						setGridBarrier(x, z, val)
					end
				end
			end

			if info.data and info.data.FenceGuashiPos then
				self:LoadFenceGuashiPos(info.data.FenceGuashiPos)
			end
		end
	end
end

--是否可以设置水池
function CFurnitureScene:CanSetPool(x,z)
	if self.m_Furnitures[x] and self.m_Furnitures[x][z] then
		if next(self.m_Furnitures[x][z]) then
			return true
		end
	end
	return false
end

--region 一键摆放
function CFurnitureScene:GenerateTempId()
	self.TempId = self.TempId + 1
	return self.TempId
end
function CFurnitureScene:GetTempInfoById(tempid)
	return self.m_TempInfo[tempid]
end
function CFurnitureScene:AddTempFurnitureToMap(id, barrierList, x, z)
	local left, right, top, bottom = self:GetGridRect(barrierList, x, z)
	local line = self.m_TempLine
	for i = left, right do
		if not line[i] then
			line[i] = {}
		end
		line[i][id] = true
	end
	local column = self.m_TempColumn
	for i = top, bottom do
		if not column[i] then
			column[i] = {}
		end
		column[i][id] = true
	end
end
--添加临时家具，用来支持一键摆放，一键摆放是客户端先测试摆放的
function CFurnitureScene:AddTempFurniture(data, x, z, rot, scale, getInfoById, getGridBarrierAndOriginalGridBarrier, setGridBarrier)
	if rot and data and data.Barrier then
		local barrierList = CFurnitureScene.GetBarrierList(data, rot, scale)
		local tempid = self:GenerateTempId()
		self.m_TempInfo[tempid] = {
			data = data,
			x = x,
			z = z,
			rot = rot,
			scale = scale,
		}
		self:AddTempFurnitureToMap(tempid,barrierList, x, z)
	end
end
function CFurnitureScene:ClearTempFurnitures()
	self.m_TempLine, self.m_TempColumn = {}, {}
	self.m_TempInfo = {}
	self.TempId = 0
end
--end

function CFurnitureScene:CanMoveFurniture(id, data, x, z, rot, scale, notInYard, kuozhangLvl, getInfoById, getOriginalGridBarrierAndInfo, isAnythingInGrid, checkTemp, poolmark,multiEditingDict)
	if not (rot and scale and data) then
		return true
	end

	if data.SubType == EnumFurnitureSubType.eWeiqiang then
		return true
	end
	local barrierList = nil
	if data.Barrier then
		barrierList = CFurnitureScene.GetBarrierList(data, rot, scale)
	end

	-- 美术刷的自定义6，对应ERoadInfo.eRI_DIGGING禁止玩家摆放装饰物
	local invalid = bit.lshift(1, ERoadInfo.eRI_DIGGING)

	local checkPlaceArea = nil
	local validArea
	if data.Type == EnumFurnitureType.eJianzhu or data.SubType == EnumFurnitureSubType.eRollerCoasterStation then
		local validRoadTypes = {ERoadInfo.eRI_PLAY2, ERoadInfo.eRI_UNUSED6, ERoadInfo.eRI_HUNTING, ERoadInfo.eRI_UNUSED8, ERoadInfo.eRI_UNUSED9, ERoadInfo.eRI_LOGGING}
		validArea = 0
		for _, order in pairs(validRoadTypes) do
				validArea = bit.bor(validArea, bit.lshift(1, order))
		end
		checkPlaceArea = function(t)
				return (bit.band(validArea, t) ~= 0)
			end
	end

	local canput
	if data.Location == EnumFurnitureLocation.eYardFenceAndRoomWall then
		local walltype = nil
		if notInYard  then
			if rot == 270 then
				walltype = bit.lshift(1, ERoadInfo.eRI_PLAY2)
			elseif rot == 180 then
				walltype = bit.lshift(1, ERoadInfo.eRI_UNUSED6)
			elseif rot == 0 then
				walltype = bit.lshift(1, ERoadInfo.eRI_UNUSED8)
			elseif rot == 90 then
				walltype = bit.lshift(1, ERoadInfo.eRI_HUNTING)
			else
				return false
			end
		else
			if rot ~= 270 and rot ~= 180 and rot ~= 0 then
				return false
			end
		end
		if not notInYard then invalid = 0 end --庭院中挂饰位置不受障碍影响
		canput = function(x, z, b, t)
			if notInYard then
				if bit.band(t, walltype) ~= 0 then return true end
			else
				if not self.m_FenceGuashiPos then return false end
				x, z = math.floor(x), math.floor(z)
				for _, area in pairs(self.m_FenceGuashiPos) do
					if x >= area.XStart and x <= area.XEnd and z >= area.ZStart and z <= area.ZEnd then
						return true
					end
				end
			end

			return false
		end
	elseif data.Location == EnumFurnitureLocation.eRoomWall then
		if not notInYard then return false end
		local walltype
		if rot == 270 then
			walltype = bit.lshift(1, ERoadInfo.eRI_PLAY2)
		elseif rot == 180 then
			walltype = bit.lshift(1, ERoadInfo.eRI_UNUSED6)
		elseif rot == 0 then
			walltype = bit.lshift(1, ERoadInfo.eRI_UNUSED8)
		elseif rot == 90 then
			walltype = bit.lshift(1, ERoadInfo.eRI_HUNTING)
		else
			return false
		end
		canput = function(x, z, b, t)
			return bit.band(t, walltype) ~= 0
		end
	elseif data.UseType == EnumFurnitureUseType.eFurnitureNormal or data.UseType == EnumFurnitureUseType.eFurnitureAnimal then
		canput = function(x, z, b, t)
			if checkPlaceArea and not checkPlaceArea(t) then
				return false
			end
			-- if poolmark and poolmark.IsEnable and poolmark:IsWaterAt(x, z) then return false end
			return (b == EBarrierType.eBT_NoBarrier or b == EBarrierType.eBT_LowBarrier) and not (isAnythingInGrid and isAnythingInGrid(x, z))
		end
	elseif data.UseType == EnumFurnitureUseType.eFurnitureOnWater or data.UseType == EnumFurnitureUseType.eFurnitureInWater then
		local iswaterfunc = nil
		if poolmark and poolmark.IsEnable then
			if data.Location == EnumFurnitureLocation.ePool then
				iswaterfunc = poolmark and poolmark.IsPoolAt
			elseif data.Location == EnumFurnitureLocation.eWarmPool then
				iswaterfunc = poolmark and poolmark.IsWarmPoolAt
			elseif data.Location == EnumFurnitureLocation.eWater then
				iswaterfunc = poolmark and poolmark.IsWaterAt
			elseif data.Location == EnumFurnitureLocation.eYardLand then--桥
			else
				 LogCallContext_lua()
			end
		end
		validArea = bit.bor(0, bit.lshift(1, ERoadInfo.eRI_PLAY5))
		canput = function(x, z, b, t,fb)
				if poolmark and poolmark.IsEnable then
					if fb==7 then--标记7，必须在陆地上
						local iswater = poolmark and poolmark:IsWaterAt(x, z)
						if not iswater then
							return (b == EBarrierType.eBT_NoBarrier or b == EBarrierType.eBT_LowBarrier) and not (isAnythingInGrid and isAnythingInGrid(x, z))
						else
							return false--是水，就返回false
						end
					end
					if iswaterfunc then
						return iswaterfunc(poolmark, x, z)
					end
					--	桥的摆放位置不能有障碍，并且也不能有人
					return (b == EBarrierType.eBT_NoBarrier or b == EBarrierType.eBT_LowBarrier) and not (isAnythingInGrid and isAnythingInGrid(x, z))
				else
					return (bit.band(validArea, t) ~= 0) 
				end
			end
		local ob, t = getOriginalGridBarrierAndInfo(x, z)
		if not canput(x, z, ob, t) then
			return false
		end
	else
		canput = function(x, z, b, t)
			if checkPlaceArea and not checkPlaceArea(t) then
				return false
			end
			return b == EBarrierType.eBT_NoBarrier or b == EBarrierType.eBT_LowBarrier --and not (poolmark and poolmark:IsWaterAt(x, z))
		end
	end


	local ignore = EBarrierType.eBT_NoBarrier
	if data.Location == EnumFurnitureLocation.eRoomWall then
		ignore = EBarrierType.eBT_HighBarrier
	end

	local CheckKuozhangLvl = nil
	if not notInYard then
		local notExtendedMask = 0
		local extOrders = {ERoadInfo.eRI_PLAY2, ERoadInfo.eRI_UNUSED6, ERoadInfo.eRI_HUNTING, ERoadInfo.eRI_UNUSED8, ERoadInfo.eRI_UNUSED9}
		for lvl = 1, 5 do
			if (kuozhangLvl-1) < lvl then
				notExtendedMask = bit.bor(notExtendedMask, bit.lshift(1, extOrders[lvl]))
			end
		end

		CheckKuozhangLvl = function(t)
				return bit.band(notExtendedMask, t) == 0
			end
	end
	--如果装饰物没有障碍，需要检查一下能不能摆放
	if not barrierList or #barrierList==0 then
		local _, t = getOriginalGridBarrierAndInfo(x, z)
		if CheckKuozhangLvl and not CheckKuozhangLvl(t) then
			return false
		end
	end
	if not barrierList then
		--没有障碍，应该直接就能摆
		return true
	end

	--站台不能放水里
	if data.SubType == EnumFurnitureSubType.eRollerCoasterStation then
		if poolmark and poolmark.IsEnable then
			local iswater = poolmark and poolmark:IsWaterAt(x, z)
			if iswater then
				return false
			end
		end
		for _, b in ipairs(barrierList) do
			local xx, zz = x + b.x, z + b.z
			if poolmark and poolmark.IsEnable then
				local iswater = poolmark and poolmark:IsWaterAt(xx, zz)
				if iswater then
					return false
				end
			end
			local ob, t = getOriginalGridBarrierAndInfo(xx, zz)
			if checkPlaceArea and not checkPlaceArea(t) then
				return false
			end
		end
	end

	for _, b in ipairs(barrierList) do
		local xx, zz = x + b.x, z + b.z
		local ob, t = getOriginalGridBarrierAndInfo(xx, zz)
		if bit.band(t, invalid) ~= 0 or not canput(xx, zz, ob, t, b.val) then
			return false
		end
		if CheckKuozhangLvl and not CheckKuozhangLvl(t) then
			return false
		end
	end

	--地毯不能超出边界或穿墙
	if data.SubType == EnumFurnitureSubType.eZawu and data.UseType == EnumFurnitureUseType.eFurnitureGround then
		local spaceList = CFurnitureScene.GetSpaceList(data, rot, scale)
		for _, s in ipairs(spaceList) do
			local xx, zz = x + s.x, z + s.z
			local ob, t = getOriginalGridBarrierAndInfo(xx, zz)
			if ob>=EBarrierType.eBT_HighBarrier then--高障，视为超出边界了
				return false
			end
		end
	end

	-- filter conflict furniture
	local function isConflictType(t1, t2)
		return t1 == t2 or (t1 == EnumFurnitureUseType.eFurnitureNormal and t2 == EnumFurnitureUseType.eFurnitureAnimal) or (t1 == EnumFurnitureUseType.eFurnitureAnimal and t2 == EnumFurnitureUseType.eFurnitureNormal)
			or (t1 == EnumFurnitureUseType.eFurnitureOnWater or t2 == EnumFurnitureUseType.eFurnitureOnWater)--其他装饰和桥都是冲突的
		end
	local left, right, top, bottom = self:GetGridRect(barrierList, x, z)
	local idsline, ids = {}, {}
	local line = self.m_Line
	for i = left, right do
		if line[i] then
			for oid, _ in pairs(line[i]) do
				if oid ~= id then
					if not multiEditingDict or not CommonDefs.DictContains_LuaCall(multiEditingDict, id) or not CommonDefs.DictContains_LuaCall(multiEditingDict, oid) then
						local info = getInfoById(oid)
						if info and info.data and isConflictType(info.data.UseType, data.UseType) then
							idsline[oid] = true
						end
					end
				end
			end
		end
	end
	local column = self.m_Column
	for i = top, bottom do
		if column[i] then
			for oid, _ in pairs(column[i]) do
				if oid ~= id then
					if not multiEditingDict or not CommonDefs.DictContains_LuaCall(multiEditingDict, id) or not CommonDefs.DictContains_LuaCall(multiEditingDict, oid) then
						local info = getInfoById(oid)
						if info and info.data and isConflictType(info.data.UseType, data.UseType) then
							if idsline[oid] then
								ids[oid] = true
							end
						end
					end
				end
			end
		end
	end
	-- check is conflict with other furniture
	for oid, _ in pairs(ids) do
		local info = getInfoById(oid)
		local barrierMap = CFurnitureScene.GetBarrierMap(info.data, info.rot, info.scale)
		for _, b in ipairs(barrierList) do
			local xx, zz = x + b.x - info.x, z + b.z - info.z
			if barrierMap[xx] and barrierMap[xx][zz] and barrierMap[xx][zz] > ignore
				then
				return false
			end
		end
	end

	--处理临时家具，for 一键摆放
	if checkTemp then
	local idsline, ids = {}, {}
	local line = self.m_TempLine
	for i = left, right do
		if line[i] then
			for oid, _ in pairs(line[i]) do
				if oid ~= id then
					local info = self:GetTempInfoById(oid)
					if info and info.data and isConflictType(info.data.UseType, data.UseType) then
						idsline[oid] = true
					end
				end
			end
		end
	end
	local column = self.m_TempColumn
	for i = top, bottom do
		if column[i] then
			for oid, _ in pairs(column[i]) do
				if oid ~= id then
					local info = self:GetTempInfoById(oid)
					if info and info.data and isConflictType(info.data.UseType, data.UseType) then
						if idsline[oid] then
							ids[oid] = true
						end
					end
				end
			end
		end
	end
	-- check is conflict with other furniture
	for oid, _ in pairs(ids) do
		local info = self:GetTempInfoById(oid)
		local barrierMap = CFurnitureScene.GetBarrierMap(info.data, info.rot, info.scale)
		for _, b in ipairs(barrierList) do
			local xx, zz = x + b.x - info.x, z + b.z - info.z
			if barrierMap[xx] and barrierMap[xx][zz] and barrierMap[xx][zz] > ignore then
				return false
			end
		end
	end
	end
	return true
end

function CFurnitureScene:MoveFurniture(id, odata, ox, oz, orot, oscale, data, x, z, rot, scale, getInfoById, getGridBarrierAndOriginalGridBarrier, setGridBarrier)
	if orot and oscale and odata and odata.Barrier then
		local barrierList = CFurnitureScene.GetBarrierList(odata, orot, oscale)
		self:RemoveFurnitureFromMap(id, barrierList, ox, oz)
		if getGridBarrierAndOriginalGridBarrier and setGridBarrier then
			for _, b in ipairs(barrierList) do
				local xx, zz, val = ox + b.x, oz + b.z, b.val
				local cb, ob = getGridBarrierAndOriginalGridBarrier(xx, zz)
				if cb ~= EBarrierType.eBT_OutRange then
					setGridBarrier(xx, zz, ob)
				end
			end
		end
	end
	if rot and scale and data and data.Barrier then
		local barrierList = CFurnitureScene.GetBarrierList(data, rot, scale)
		self:AddFurnitureToMap(id, barrierList, x, z)
		if getGridBarrierAndOriginalGridBarrier and setGridBarrier then
			for _, b in ipairs(barrierList) do
				local xx, zz, val = x + b.x, z + b.z, b.val
				local cb = getGridBarrierAndOriginalGridBarrier(xx, zz)
				if cb ~= EBarrierType.eBT_OutRange then
					if val==7 then--桥的两端，不需要设置障碍
					elseif val>=4 then
						val = val-4
						setGridBarrier(xx, zz, val)
					elseif val > cb then --  -- 看看在架桥的时候，有可能把高障变成低障
						setGridBarrier(xx, zz, val)
					end
				end
			end
		end
	end
end

function CFurnitureScene.DistanceCanInteract(data, x, z, rot, px, pz) --看起来没有用了
	local function canInteract(x, z, left, right, top, bottom)
		return x >= left and x <= right and z >= top and z <= bottom
	end

	local left1, right1, top1, bottom1 = px - 1, px + 1, pz - 2, pz + 2
	local left2, right2, top2, bottom2 = px - 2, px + 2, pz - 1, pz + 1
	local barrierList = CFurnitureScene.GetBarrierList(data, rot, 127)
	for _, b in ipairs(barrierList) do
		local xx, zz = x + b.x, z + b.z
		if canInteract(xx, zz, left1, right1, top1, bottom1) or canInteract(xx, zz, left2, right2, top2, bottom2) then
			return true
		end
	end
	return false
end

function CFurnitureScene:GetGridRect(barrierList, x, z)
	local left, right, top, bottom
	for _, b in ipairs(barrierList) do
		local xx, zz = x + b.x, z + b.z
		if not left then
			left, right, top, bottom = xx, xx, zz, zz
		else
			left = math.min(left, xx)
			right = math.max(right, xx)
			top = math.min(top, zz)
			bottom = math.max(bottom, zz)
		end
	end
	if not left then
		return 1, 0, 1, 0
	end
	local width, height = self.m_GridWidth, self.m_GridHeight
	return math.floor(left / width), math.floor(right / width), math.floor(top / height), math.floor(bottom / height)
end

function CFurnitureScene:AddFurnitureToMap(id, barrierList, x, z)
	--如果这个装饰物不带障碍，则不会加入到m_Furnitures里面。忽略这个装饰物对水池编辑的影响。
	if not barrierList or #barrierList==0 then return end
	local left, right, top, bottom = self:GetGridRect(barrierList, x, z)
	local line = self.m_Line
	for i = left, right do
		if not line[i] then
			line[i] = {}
		end
		line[i][id] = true
	end
	local column = self.m_Column
	for i = top, bottom do
		if not column[i] then
			column[i] = {}
		end
		column[i][id] = true
	end

	if not self.m_Furnitures[x] then
		self.m_Furnitures[x] = {}
	end
	if not self.m_Furnitures[x][z] then
		self.m_Furnitures[x][z] = {}
	end
	self.m_Furnitures[x][z][id] = true
end

function CFurnitureScene:RemoveFurnitureFromMap(id, barrierList, x, z)
	local left, right, top, bottom = self:GetGridRect(barrierList, x, z)
	local line = self.m_Line
	for i = left, right do
		if line[i] then
			line[i][id] = nil
		end
	end
	local column = self.m_Column
	for i = top, bottom do
		if column[i] then
			column[i][id] = nil
		end
	end

	if self.m_Furnitures[x] and self.m_Furnitures[x][z] then
		self.m_Furnitures[x][z][id] = nil
	end
end

function CFurnitureScene.GetBarrierBox(data, rot) --看起来没有用了
	if not (rot and data and data.Barrier) then return end

	local xCount, zCount = data.Space.line, data.Space.column
	local xCenter, zCenter = math.floor(xCount / 2), math.floor(zCount / 2)
	local rotate
	
	if rot%360 == 90 then
		return -zCenter, -xCenter, zCount-1-zCenter, xCount-1-xCenter
	elseif rot%360 == 180 then
		return -xCenter, -zCenter, xCount-1-xCenter, zCount-1-zCenter
	elseif rot%360 == 270 then
		return -zCenter, -xCenter, zCount-1-zCenter, xCount-1-xCenter
	else
		return -xCenter, -zCenter, xCount-1-xCenter, zCount-1-zCenter
	end
end

function CFurnitureScene.GetScaleUI8ToFloat(byteScale, min, max)
	local res = 1
	if byteScale <= 127 then
		res = (1 - min) * byteScale / 127 + min
	else
		res = (max - 1) * (byteScale - 127) / 127 + 1
	end
	return res
end

function CFurnitureScene.GetScaleFloatToUI8(floatScale, min, max)
	local res = 127
	if floatScale <= 1 then
		res = math.floor(127 - (1 - floatScale) / (1 - min) * 127)
	else
		res = math.floor(127 + (floatScale - 1) / (max - 1) * 127)
	end
	return res
end

local dir = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}}
local dir8 = {{1, 0}, {0, 1}, {-1, 0}, {0, -1}, {1, 1}, {-1, -1}, {-1, 1}, {1, -1}}

function CFurnitureScene.GetBarrier(data, rot, scale, foo, usage)
	if not (rot and data and data.Barrier) then
		return {}
	end

	local xCount, zCount = data.Space.line, data.Space.column
	local xCenter, zCenter = math.floor(xCount / 2), math.floor(zCount / 2)
	local rotate

	scale = scale and CFurnitureScene.GetScaleUI8ToFloat(scale, data.minScale, data.maxScale) or 1
	
	if not CFurnitureScene.IsOpen360Rotation() then
		if rot%360 == 90 then
			rotate = function(x, z)
				return z - zCenter, xCount - 1 - x - xCenter
			end
		elseif rot%360 == 180 then
			rotate = function(x, z)
				return xCount - 1 - x - xCenter, zCount - 1 - z - zCenter
			end
		elseif rot%360 == 270 then
			rotate = function(x, z)
				return zCount - 1 - z - zCenter, x - xCenter
			end
		else
			rotate = function(x, z)
				return x - xCenter, z - zCenter
			end
		end
	else
		-- rot: 顺时针旋转的度数，[0,360)
		-- 公式的角度是逆时针
		rot = -rot
		local sina = math.sin(math.rad(rot))
		local cosa = math.cos(math.rad(rot))
		
		if not CFurnitureScene.IsOpenObstacleScaling() then
			rotate = function(x, z) -- 此处坐标均以家具自身左下为原点
				local pos = {}
				
				local barrier = data.Barrier[z * xCount + x + 1]
				if not barrier then return pos end
				if barrier < 4 then
					local nx = math.floor((x - xCenter) * cosa - (z - zCenter) * sina + xCenter + 0.5)
					local nz = math.floor((x - xCenter) * sina + (z - zCenter) * cosa + zCenter + 0.5)

					-- 某些家具旋转得到的坐标会出现负数
					pos[(nx + 100) * 10000 + nz + 100] = barrier
					
					for k = 1, 4 do
						local xx = x + dir[k][1]
						local zz = z + dir[k][2]
						barrier = data.Barrier[zz * xCount + xx + 1]
						if xx >= 0 and xx < xCount and zz >= 0 and zz < zCount and (data.GetSpace or barrier and barrier > EBarrierType.eBT_NoBarrier and barrier < 4) then
							local nxx = math.floor(((x + xx) / 2 - xCenter) * cosa - ((z + zz) / 2 - zCenter) * sina + xCenter + 0.5)
							local nzz = math.floor(((x + xx) / 2 - xCenter) * sina + ((z + zz) / 2 - zCenter) * cosa + zCenter + 0.5)
							pos[(nxx + 100) * 10000 + nzz + 100] = barrier
						end
					end
				else
					local nx = math.floor((x - xCenter) * cosa - (z - zCenter) * sina + xCenter + 0.5)
					local nz = math.floor((x - xCenter) * sina + (z - zCenter) * cosa + zCenter + 0.5)
					pos[(nx + 100) * 10000 + nz + 100] = barrier
				end
				
				return pos
			end
		else
			rotate = function(x, z) 
				local barrier = data.Barrier[z * xCount + x + 1]
				if not barrier then return end

				local pos = {}
				local addPoint = function(x, z, b) 
					-- 某些家具旋转得到的坐标会出现负数
					pos[(math.floor(x + 0.5) + 100) * 10000 + math.floor(z + 0.5) + 100] = b
				end
				local isObstacle = function(x, z)
					local barrier = data.Barrier[z * xCount + x + 1]
					return x >= 0 and x < xCount and z >= 0 and z < zCount and (data.GetSpace or barrier and barrier > EBarrierType.eBT_NoBarrier and barrier < 4)
				end
				local transformPoint = function(x, z)
					return ((x - xCenter) * cosa - (z - zCenter) * sina) * scale + xCenter, ((x - xCenter) * sina + (z - zCenter) * cosa) * scale + zCenter
				end
				local expandPoint = function(x, z, b)
					-- 1:topRight 2:bottomLeft 3:topLeft 4:bottomRight
					-- 1->3:topBorder 1->4:rightBorder 
					-- 2->3:leftBorder 2->4:bottomBorder
					local vx, vz, v2x, v2z, v3x, v3z, v4x, v4z
					for k = 1, 4 do
						local cornerX, cornerZ = transformPoint(x + dir8[4 + k][1] * 0.5, z + dir8[4 + k][2] * 0.5)
						if k == 1 then vx, vz = cornerX, cornerZ 
						elseif k == 2 then v2x, v2z = cornerX, cornerZ
						elseif k == 3 then v3x, v3z = cornerX, cornerZ
						else v4x, v4z = cornerX, cornerZ end
					end
					
					-- rowUnit: unit vector in the row direction
					local rowUnitX, rowUnitZ = v3x - vx, v3z - vz
					local magnitude = math.sqrt(rowUnitX * rowUnitX + rowUnitZ * rowUnitZ)
					rowUnitX, rowUnitZ = rowUnitX / magnitude, rowUnitZ / magnitude
	
					-- colUnit: unit vector in the col direction
					local colUnitX, colUnitZ = v4x - vx, v4z - vz
					--magnitude = math.sqrt(colUnitX * colUnitX + colUnitZ * colUnitZ)
					colUnitX, colUnitZ = colUnitX / magnitude, colUnitZ / magnitude

					vx, vz = vx + rowUnitX * 0.5 + colUnitX * 0.5, vz + rowUnitZ * 0.5 + colUnitZ * 0.5 
					v2x, v2z = v2x - rowUnitX * 0.5 - colUnitX * 0.5, v2z - rowUnitZ * 0.5 - colUnitZ * 0.5 
					v3x, v3z = v3x - rowUnitX * 0.5 + colUnitX * 0.5, v3z - rowUnitZ * 0.5 + colUnitZ * 0.5 
					v4x, v4z = v4x + rowUnitX * 0.5 - colUnitX * 0.5, v4z + rowUnitZ * 0.5 - colUnitZ * 0.5 

					--local isLeftObstacle = b < 4 and isObstacle(x - 1, z)
					--local isDownObstacle = b < 4 and isObstacle(x, z - 1)
					local isRightObstacle = b < 4 and isObstacle(x + 1, z)
					local isTopObstacle = b < 4 and isObstacle(x, z + 1)
					local isTopRightObstacle = b < 4 and isObstacle(x + 1, z + 1)

					if isTopRightObstacle then
						addPoint(vx + colUnitX * -0.5 + rowUnitX * -0.5, vz + colUnitZ * -0.5 + rowUnitZ * -0.5, b)
					end

					-- 每列起点若反方向有障碍则加入边缘点
					for i = isTopObstacle and -0.5 or 0, scale - 1, 0.5 do -- at row i, (row from Top to Down
						local rowX, rowZ = vx + colUnitX * i, vz + colUnitZ * i
						-- 每行起点若反方向有障碍则加入边缘点
						for j = (i >= 0 and isRightObstacle) and -0.5 or 0, scale - 1, 0.5 do -- at col j, (col from Right to Left
							addPoint(rowX + rowUnitX * j, rowZ + rowUnitZ * j, b)
						end
						-- 每行末尾填满空隙
						addPoint(v3x + colUnitX * i, v3z + colUnitZ * i, b)
					end
					-- 每列末尾填满空隙
					for j = isRightObstacle and -0.5 or 0, scale - 1, 0.5 do
						addPoint(v4x + rowUnitX * j, v4z + rowUnitZ * j, b)
					end
					addPoint(v2x, v2z, b)
				end
				
				if scale <= 1 then
					local nx = ((x - xCenter) * cosa - (z - zCenter) * sina) * scale + xCenter
					local nz = ((x - xCenter) * sina + (z - zCenter) * cosa) * scale + zCenter
					addPoint(nx, nz, barrier)
					if barrier < 4 then
						for k = 1, 4 do
							local xx = x + dir[k][1]
							local zz = z + dir[k][2]
							barrier = data.Barrier[zz * xCount + xx + 1]
							if isObstacle(xx, zz) then
								local nxx = (((x + xx) / 2 - xCenter) * cosa - ((z + zz) / 2 - zCenter) * sina) * scale + xCenter
								local nzz = (((x + xx) / 2 - xCenter) * sina + ((z + zz) / 2 - zCenter) * cosa) * scale + zCenter
								addPoint(nxx, nzz, barrier)
							end
						end
					end
				else
					expandPoint(x, z, barrier)
				end

				return pos
			end
		end
	end
	
	local ignore = EBarrierType.eBT_NoBarrier

	local ret = {}
	local chk = {}
	local index = 1
	for i = 0, zCount - 1 do
		for j = 0, xCount - 1 do
			local val = data.Barrier[index]
			if data.GetSpace or
				val > ignore or-- and val~=4 or --标记为4的不考虑
				data.Location == EnumFurnitureLocation.eRoomWall or 
				data.Location == EnumFurnitureLocation.eYardFenceAndRoomWall then
				-- 旋转后的坐标以中心为原点
				if not CFurnitureScene.IsOpen360Rotation() then
					local xx, zz = rotate(j, i)
					foo(ret, xx, zz, val)
				else
					local pos = rotate(j, i)
					if pos then
						for k, v in pairs(pos) do
							if not chk[k] then
								foo(ret, math.floor(k / 10000) - 100 - xCenter, math.fmod(k, 10000) - 100 - zCenter, val)
								chk[k] = val
							end
						end
					end
				end
			end
			index = index + 1
		end
	end
	return ret
end

function CFurnitureScene.GetBarrierMap(data, rot, scale)
	return CFurnitureScene.GetBarrier(data, rot, scale, function(ret, x, z, val)
		if not ret[x] then
			ret[x] = {}
		end
		ret[x][z] = val
	end, "checkfurniture")
end

function CFurnitureScene.GetBarrierList(data, rot, scale)
	return CFurnitureScene.GetBarrier(data, rot, scale, function(ret, x, z, val)
		table.insert(ret, {x = x, z = z, val = val})
	end)
end

--地毯类的装饰物不能挪出室内范围，这里获取地毯的占地
function CFurnitureScene.GetSpaceList(data, rot, scale)
	data.GetSpace = true
	return CFurnitureScene.GetBarrier(data, rot, scale, function(ret, x, z, val)
		table.insert(ret, {x = x, z = z})
	end)
end

function CFurnitureScene:CanMoveTower(id, data, x, z, rot, scale, getInfoById, getOriginalGridBarrierAndInfo, isAnythingInGrid, idInGrid)
	if not (rot and data and data.Barrier) then
		return true
	end

	local barrierList = CFurnitureScene.GetBarrierList(data, rot, scale)

	--
	local invalid = bit.lshift(1, ERoadInfo.eRI_PLAY2)

	--无障和低障的才能摆放
	local canput = function(x, z, b, t)
			if idInGrid ~= nil then
				if b ~= EBarrierType.eBT_NoBarrier then
					local gridId = idInGrid(x,z)
					if (gridId == id and id > 0) or gridId == 0 then
						return true
					end

				end
			end
			return (b == EBarrierType.eBT_NoBarrier) and not (isAnythingInGrid and isAnythingInGrid(x, z))
		end
	-- end


	local ignore = EBarrierType.eBT_NoBarrier

	for _, b in ipairs(barrierList) do
		local xx, zz = x + b.x, z + b.z
		local ob, t = getOriginalGridBarrierAndInfo(xx, zz)
		if bit.band(t, invalid) ~= 0 or not canput(xx, zz, ob, t) then
			return false
		end
	end

	local left, right, top, bottom = self:GetGridRect(barrierList, x, z)
	local idsline, ids = {}, {}
	local line = self.m_Line
	for i = left, right do
		if line[i] then
			for oid, _ in pairs(line[i]) do
				if oid ~= id then
					local info = getInfoById(oid)
					if info and info.data then
						idsline[oid] = true
					end
				end
			end
		end
	end
	local column = self.m_Column
	for i = top, bottom do
		if column[i] then
			for oid, _ in pairs(column[i]) do
				if oid ~= id then
					local info = getInfoById(oid)
					if info and info.data then
						if idsline[oid] then
							ids[oid] = true
						end
					end
				end
			end
		end
	end

	-- check is conflict with other furniture
	for oid, _ in pairs(ids) do
		local info = getInfoById(oid)
		local barrierMap = CFurnitureScene.GetBarrierMap(info.data, info.rot, info.scale)
		for _, b in ipairs(barrierList) do
			local xx, zz = x + b.x - info.x, z + b.z - info.z
			if barrierMap[xx] and barrierMap[xx][zz] and barrierMap[xx][zz] > ignore
				then
				return false
			end
		end
	end
	return true
end

function CFurnitureScene:ClearFenceGuashiPos()
	self.m_FenceGuashiPos = nil
end

function CFurnitureScene:LoadFenceGuashiPos(areaInfo)
	self.m_FenceGuashiPos = areaInfo
end

