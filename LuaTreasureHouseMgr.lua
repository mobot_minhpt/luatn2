require("common/common_include")

EnumCangbaogeStatus = {
	eInit = 0, -- 初始状态
	eRegistered = 1, -- 登记成功后
	eGatherRoleInfoAndLocked = 2,
	eRequestCBGOnShelf = 3,
	eCBGOnShelfSuccess = 4,
	eCBGOnShelfFailed = 5,
}

LuaTreasureHouseMgr={}

-- 0表示未开始，1表示进行中，2表示结束
LuaTreasureHouseMgr.SendAllPlayerInfo_Status=0
-- 藏宝阁待售区所有玩家信息
LuaTreasureHouseMgr.AllPlayerInfos=nil


-- 藏宝阁登记条件
LuaTreasureHouseMgr.RegisterInfo=nil
-- 藏宝阁上架条件
LuaTreasureHouseMgr.ShelfInfo=nil

-- 藏宝阁展示天数
LuaTreasureHouseMgr.MinShowDays = 1
LuaTreasureHouseMgr.MaxShowDays = 30


