require("common/common_include")

local CUIManager = import "L10.UI.CUIManager"

LuaChristmasTreeAddItemMgr = {}

LuaChristmasTreeAddItemMgr.ItemID = -1
LuaChristmasTreeAddItemMgr.targetWorldPos = Vector3.zero
LuaChristmasTreeAddItemMgr.targetWidth = 0
LuaChristmasTreeAddItemMgr.targetHeight = 0
LuaChristmasTreeAddItemMgr.alignType = nil


LuaChristmasTreeAddItemMgr.ShowWnd = function(ItemID, targetWorldPos, targetWidth, targetHeight, alignType)
	LuaChristmasTreeAddItemMgr.ItemID = ItemID
	LuaChristmasTreeAddItemMgr.targetWorldPos = targetWorldPos
	LuaChristmasTreeAddItemMgr.targetWidth = targetWidth
	LuaChristmasTreeAddItemMgr.targetHeight = targetHeight
	LuaChristmasTreeAddItemMgr.alignType = alignType
	CUIManager.ShowUI("ChristmasTreeAddItemWnd")
end
