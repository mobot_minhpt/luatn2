local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Animation = import "UnityEngine.Animation"

LuaFSCPrepareRareCardWnd = class()

LuaFSCPrepareRareCardWnd.s_SpecialCards = {}
LuaFSCPrepareRareCardWnd.s_CountDownEndTime = 0
LuaFSCPrepareRareCardWnd.s_Panel = nil

RegistClassMember(LuaFSCPrepareRareCardWnd, "m_Grid")
RegistClassMember(LuaFSCPrepareRareCardWnd, "m_Empty")
RegistClassMember(LuaFSCPrepareRareCardWnd, "m_Tick")
RegistClassMember(LuaFSCPrepareRareCardWnd, "m_InitTick")
RegistClassMember(LuaFSCPrepareRareCardWnd, "m_Card2Npc")
RegistClassMember(LuaFSCPrepareRareCardWnd, "m_IsInit")
RegistClassMember(LuaFSCPrepareRareCardWnd, "m_PlayList")

RegistClassMember(LuaFSCPrepareRareCardWnd, "m_CountdownLabel")
RegistClassMember(LuaFSCPrepareRareCardWnd, "m_AbandonBtn")
RegistClassMember(LuaFSCPrepareRareCardWnd, "m_ReadyBtn")
RegistClassMember(LuaFSCPrepareRareCardWnd, "m_PlayerReady")
RegistClassMember(LuaFSCPrepareRareCardWnd, "m_WaitForEnemy")


function LuaFSCPrepareRareCardWnd:InitCard(idx, cardId, isEmpty)
    if not (idx >= 1 and idx <= self.m_Grid.childCount) then return end
    
    if isEmpty then
        self.m_Grid:GetChild(idx - 1).gameObject:SetActive(false)
        local hint1 = self.m_Empty[idx]:Find("Hint"):GetComponent(typeof(UILabel))
        local hint2 = self.m_Empty[idx]:Find("Hint (2)").gameObject

        local data = NPC_NPC.GetData(self.m_Card2Npc[cardId])
        if LuaFSCPrepareRareCardWnd.s_CountDownEndTime > 0 and data then
            hint2:SetActive(false)
            hint1.gameObject:SetActive(true)
            hint1.text = LocalString.GetString("战胜") .. "\n" ..data.Name
        else
            hint2:SetActive(true)
            hint1.gameObject:SetActive(false)
        end
    else
        self.m_Empty[idx].gameObject:SetActive(false)
        local card = self.m_Grid:GetChild(idx - 1):GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
        if LuaFSCPrepareRareCardWnd.s_CountDownEndTime > 0 then    
            card:Init(cardId, true)
        else -- 局内珍稀牌界面的情况
            local rareStatus = LuaFourSeasonCardMgr.SelfSpecialCards[cardId]
            if rareStatus then
                local source = rareStatus[1]
                local status = rareStatus[2]
                if status == EnumFSC_SpCardStatus.NotUsed then
                    card:Init(cardId, true, EnumFSC_CardStatus.NotInEffect, source == EnumFSC_SpCardSrcType.Borrowed)
                elseif status == EnumFSC_SpCardStatus.Interrupted then
                    card:Init(cardId, true, EnumFSC_CardStatus.Banned, source == EnumFSC_SpCardSrcType.Borrowed)
                elseif source == EnumFSC_SpCardSrcType.Replicate then
                    card:Init(cardId, true, EnumFSC_CardStatus.Copy, source == EnumFSC_SpCardSrcType.Borrowed)
                else
                    card:Init(cardId, true, nil, source == EnumFSC_SpCardSrcType.Borrowed)
                end
            end
        end
    end
end

function LuaFSCPrepareRareCardWnd:Awake()
    self.m_PlayList = {}

    self.m_Card2Npc = {}
    ZhouNianQing2023_PlayWithNpc.Foreach(function(k, v) 
        self.m_Card2Npc[v.RewardSpCard] = k
    end)

    self.m_Grid = self.transform:Find("Anchor/Grid (1)")
    self.m_CountdownLabel = self.transform:Find("Bg/Title/Countdown/Sec"):GetComponent(typeof(UILabel))
    self.m_AbandonBtn = self.transform:Find("Bg/BottomView/kgd/AbandonBtn").gameObject
    self.m_ReadyBtn = self.transform:Find("Bg/BottomView/kgd/ReadyBtn").gameObject
    self.m_PlayerReady = self.transform:Find("Bg/BottomView/PlayerReady").gameObject
    self.m_WaitForEnemy = self.transform:Find("Bg/BottomView/WaitForEnemy").gameObject
    self.m_Empty = {}
    for i = 0, 7 do
        self.m_Empty[i + 1] = self.transform:Find("Bg/Empty"..(i > 0 and " ("..i..")" or ""))
    end

    self.transform:Find("Bg/BottomView/kgd/HintLabel"):GetComponent(typeof(UILabel)).text = ZhouNianQing2023_Setting.GetData().PVPCostInstruction

    UIEventListener.Get(self.m_AbandonBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        Gac2Gas.Anniv2023FSC_RequestLeaveGame()
    end)
    UIEventListener.Get(self.m_ReadyBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        Gac2Gas.Anniv2023FSC_RequestSetReady()
    end)
end

function LuaFSCPrepareRareCardWnd:OnEnable()
    LuaFSCPrepareRareCardWnd.s_Panel = self.transform:GetComponent(typeof(UIPanel))
    self:SyncPlayerInfo()
    g_ScriptEvent:AddListener("FSC_SyncPlayerInfo" ,self, "SyncPlayerInfo")
end

function LuaFSCPrepareRareCardWnd:OnDisable()
    LuaFSCPrepareRareCardWnd.s_Panel = nil
    g_ScriptEvent:RemoveListener("FSC_SyncPlayerInfo" ,self, "SyncPlayerInfo")
end

function LuaFSCPrepareRareCardWnd:SyncPlayerInfo()
    local sInfo = LuaFourSeasonCardMgr.SelfInfo
    local oInfo = LuaFourSeasonCardMgr.OpponentInfo

    if sInfo and sInfo.status and sInfo.status > EnumFSC_PlayerStatus.ePrepare then
        self.m_AbandonBtn:SetActive(false)
        self.m_ReadyBtn:SetActive(false)
        self.m_PlayerReady:SetActive(false)
        self.m_WaitForEnemy:SetActive(true)
    elseif oInfo and oInfo.status and oInfo.status > EnumFSC_PlayerStatus.ePrepare then
        self.m_AbandonBtn:SetActive(true)
        self.m_ReadyBtn:SetActive(true)
        self.m_PlayerReady:SetActive(true)
        self.m_WaitForEnemy:SetActive(false)

        if oInfo.isPlayer then
            self.m_PlayerReady.transform:Find("Label"):GetComponent(typeof(UILabel)).text = (oInfo.name or "")
            self.m_PlayerReady.transform:Find("Avatar"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(oInfo.cls, oInfo.sex, -1), false)
        elseif oInfo.npcId then
            local npc = NPC_NPC.GetData(oInfo.npcId)
            if npc then
                self.m_PlayerReady.transform:Find("Label"):GetComponent(typeof(UILabel)).text = npc.Name
                self.m_PlayerReady.transform:Find("Avatar"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(npc.Portrait, false)
            end
        end

        self:PlayAfterInit("fscpreparerarecardwnd_zhunei")        
    else
        self.m_AbandonBtn:SetActive(true)
        self.m_ReadyBtn:SetActive(true)
        self.m_PlayerReady:SetActive(false)
        self.m_WaitForEnemy:SetActive(false)
    end
end

function LuaFSCPrepareRareCardWnd:PlayAfterInit(animName)
    if self.m_IsInit then
        self.transform:GetComponent(typeof(Animation)):Play(animName)
    else
        table.insert(self.m_PlayList, animName)
    end
end

function LuaFSCPrepareRareCardWnd:Init()
    local exist = {}
    local idx = 1
    for cardId, _ in pairs(LuaFSCPrepareRareCardWnd.s_SpecialCards) do 
         exist[cardId] = true
         self:InitCard(idx, cardId)
         idx = idx + 1
    end
    for i = 1, 8 do
        if not exist[i] then
            self:InitCard(idx, i, true)
            idx = idx + 1
        end
    end

    if LuaFSCPrepareRareCardWnd.s_CountDownEndTime > 0 then
        local count = math.ceil(LuaFSCPrepareRareCardWnd.s_CountDownEndTime - CServerTimeMgr.Inst.timeStamp)
        if count > 0 then
            self.m_CountdownLabel.text = count
            if self.m_Tick then UnRegisterTick(self.m_Tick) end
            self.m_Tick = RegisterTick(function()
                local count = math.ceil(LuaFSCPrepareRareCardWnd.s_CountDownEndTime - CServerTimeMgr.Inst.timeStamp)
                if count > 0 then 
                    self.m_CountdownLabel.text = count
                else
                    CUIManager.CloseUI(CLuaUIResources.FSCPrepareRareCardWnd)
                end
            end, 500)
        else
            self.m_CountdownLabel.text = "0"
        end

        UIEventListener.Get(self.transform:Find("Bg/CloseButton").gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            Gac2Gas.Anniv2023FSC_RequestLeaveGame()
        end)
    else
        self.transform:Find("Bg/Title/Countdown").gameObject:SetActive(false)
        self.transform:Find("Bg/BottomView").gameObject:SetActive(false)
    end

    self.transform:GetComponent(typeof(Animation)):Play("fscpreparerarecardwnd_show")
    UnRegisterTick(self.m_InitTick)
    self.m_InitTick = RegisterTickOnce(function()
        self.m_IsInit = true
        for _, animName in ipairs(self.m_PlayList) do
            self.transform:GetComponent(typeof(Animation)):Play(animName)
        end
        self.m_PlayList = {}
    end, 500)
end

function LuaFSCPrepareRareCardWnd:OnDestroy()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
    if self.m_InitTick then
        UnRegisterTick(self.m_InitTick)
        self.m_InitTick = nil
    end
    CUIManager.CloseUI(CLuaUIResources.FSCRareCardInfoWnd)

    if LuaFourSeasonCardMgr.WaitForNxtGuide and LuaFourSeasonCardMgr.GuidingStage == 1 then
        LuaFourSeasonCardMgr.WaitForNxtGuide = false
        Gac2Gas.Anniv2023FSC_IncGuidingStage()
        LuaFourSeasonCardMgr.GuidingStage = LuaFourSeasonCardMgr.GuidingStage + 1
        LuaFourSeasonCardMgr:TryTriggerGuide()
    end
end


function LuaFSCPrepareRareCardWnd:GetGuideGo(methodName)
    if methodName == "GetCard1" then
        local card = self.m_Grid:GetChild(0) 
        return card and card.gameObject
    end
    return nil
end