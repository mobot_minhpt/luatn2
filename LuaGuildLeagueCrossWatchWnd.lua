local DelegateFactory  = import "DelegateFactory"
local CGamePlayMgr = import "L10.Game.CGamePlayMgr"
local CUIResources = import "L10.UI.CUIResources"
local UISlider = import "UISlider"
local CUITexture = import "L10.UI.CUITexture"
local CUIFx = import "L10.UI.CUIFx"
local Ease = import "DG.Tweening.Ease"
local Object=import "UnityEngine.Object"
local CMainCamera = import "L10.Engine.CMainCamera"
local UISimpleTableView = import "L10.UI.UISimpleTableView"
local DefaultUISimpleTableViewDataSource = import "L10.UI.DefaultUISimpleTableViewDataSource"
local CJoystickWnd = import "L10.UI.CJoystickWnd"

LuaGuildLeagueCrossWatchWnd = class()
RegistClassMember(LuaGuildLeagueCrossWatchWnd,"m_TitleLabel")

RegistClassMember(LuaGuildLeagueCrossWatchWnd,"m_AttRoot")
RegistClassMember(LuaGuildLeagueCrossWatchWnd,"m_DefRoot")
RegistClassMember(LuaGuildLeagueCrossWatchWnd,"m_AttHpValueLabel")
RegistClassMember(LuaGuildLeagueCrossWatchWnd,"m_AttHpBar")
RegistClassMember(LuaGuildLeagueCrossWatchWnd,"m_AttTowerNumLabel")
RegistClassMember(LuaGuildLeagueCrossWatchWnd,"m_AttKillNumLabel")
RegistClassMember(LuaGuildLeagueCrossWatchWnd,"m_AttBossTable")
RegistClassMember(LuaGuildLeagueCrossWatchWnd,"m_DefHpValueLabel")
RegistClassMember(LuaGuildLeagueCrossWatchWnd,"m_DefHpBar")
RegistClassMember(LuaGuildLeagueCrossWatchWnd,"m_DefTowerNumLabel")
RegistClassMember(LuaGuildLeagueCrossWatchWnd,"m_DefKillNumLabel")
RegistClassMember(LuaGuildLeagueCrossWatchWnd,"m_DefBossTable")

RegistClassMember(LuaGuildLeagueCrossWatchWnd,"m_QuickMsgTemplate")
RegistClassMember(LuaGuildLeagueCrossWatchWnd,"m_QuickMsgRoot")
RegistClassMember(LuaGuildLeagueCrossWatchWnd, "m_MsgView")
RegistClassMember(LuaGuildLeagueCrossWatchWnd, "m_MsgViewCloseButton")
RegistClassMember(LuaGuildLeagueCrossWatchWnd, "m_MsgViewOffset")
RegistClassMember(LuaGuildLeagueCrossWatchWnd, "m_SimpleTableView")
RegistClassMember(LuaGuildLeagueCrossWatchWnd, "m_MsgItemTemplate")
RegistClassMember(LuaGuildLeagueCrossWatchWnd, "m_DefaultSimpleTableViewDataSource")


RegistClassMember(LuaGuildLeagueCrossWatchWnd,"m_LeaveButton")
RegistClassMember(LuaGuildLeagueCrossWatchWnd,"m_SettingButton")
RegistClassMember(LuaGuildLeagueCrossWatchWnd,"m_SituationButton")
RegistClassMember(LuaGuildLeagueCrossWatchWnd,"m_MsgButton")
RegistClassMember(LuaGuildLeagueCrossWatchWnd,"m_MatchInfoButton")

RegistClassMember(LuaGuildLeagueCrossWatchWnd,"m_HiddenUIs")
RegistClassMember(LuaGuildLeagueCrossWatchWnd,"m_BossInfoTbl")
RegistClassMember(LuaGuildLeagueCrossWatchWnd,"m_DelayShowBossInfoTick")

RegistClassMember(LuaGuildLeagueCrossWatchWnd,"m_QuickMsgs")


function LuaGuildLeagueCrossWatchWnd:Awake()
    self.m_TitleLabel = self.transform:Find("TitleLabel"):GetComponent(typeof(UILabel))

    self.m_AttRoot = self.transform:Find("Top/Left")
    self.m_DefRoot = self.transform:Find("Top/Right")

    self.m_AttHpValueLabel = self.m_AttRoot:Find("HPValueLabel"):GetComponent(typeof(UILabel))
    self.m_AttHpBar = self.m_AttRoot:Find("HPBar"):GetComponent(typeof(UISlider))
    self.m_AttTowerNumLabel = self.m_AttRoot:Find("TowerNumLabel"):GetComponent(typeof(UILabel))
    self.m_AttKillNumLabel = self.m_AttRoot:Find("KillNumLabel"):GetComponent(typeof(UILabel))
    self.m_AttBossTable = self.m_AttRoot:Find("Table"):GetComponent(typeof(UITable))
   
    self.m_DefHpValueLabel = self.m_DefRoot:Find("HPValueLabel"):GetComponent(typeof(UILabel))
    self.m_DefHpBar = self.m_DefRoot:Find("HPBar"):GetComponent(typeof(UISlider))
    self.m_DefTowerNumLabel = self.m_DefRoot:Find("TowerNumLabel"):GetComponent(typeof(UILabel))
    self.m_DefKillNumLabel = self.m_DefRoot:Find("KillNumLabel"):GetComponent(typeof(UILabel))
    self.m_DefBossTable = self.m_DefRoot:Find("Table"):GetComponent(typeof(UITable))

    self.m_QuickMsgTemplate = self.transform:Find("Center/Alert").gameObject
    self.m_QuickMsgRoot = self.transform:Find("Center/AlertNode")
    self.m_MsgView = self.transform:Find("MsgView").gameObject
    self.m_MsgViewCloseButton = self.transform:Find("MsgView/Offset/Container/CloseButton").gameObject
    self.m_MsgViewOffset = self.transform:Find("MsgView/Offset").transform
    self.m_SimpleTableView = self.transform:Find("MsgView"):GetComponent(typeof(UISimpleTableView))
    self.m_MsgItemTemplate = self.transform:Find("MsgView/Offset/ScrollView/MsgTemplate").gameObject

    self.m_LeaveButton = self.transform:Find("TopRight/LeaveButton").gameObject
    self.m_SettingButton = self.transform:Find("BottomLeft/SettingButton").gameObject
    self.m_SituationButton = self.transform:Find("BottomRight/SituationButton").gameObject
    self.m_MsgButton = self.transform:Find("BottomRight/MsgButton").gameObject
    self.m_MatchInfoButton = self.transform:Find("BottomRight/MatchInfoButton").gameObject

    UIEventListener.Get(self.m_LeaveButton).onClick = DelegateFactory.VoidDelegate(function() self:OnLeaveButtonClick(self.m_LeaveButton) end)
    UIEventListener.Get(self.m_SettingButton).onClick = DelegateFactory.VoidDelegate(function() self:OnSettingButtonClick(self.m_SettingButton) end)
    UIEventListener.Get(self.m_SituationButton).onClick = DelegateFactory.VoidDelegate(function() self:OnSituationButtonClick(self.m_SituationButton) end)
    UIEventListener.Get(self.m_MsgButton).onClick = DelegateFactory.VoidDelegate(function() self:OnMsgButtonClick(self.m_MsgButton) end)
    UIEventListener.Get(self.m_MatchInfoButton).onClick = DelegateFactory.VoidDelegate(function() self:OnMatchInfoButtonClick(self.m_MatchInfoButton) end)
    UIEventListener.Get(self.m_MsgViewCloseButton).onClick = DelegateFactory.VoidDelegate(function() self:OnMsgViewCloseButtonClick(self.m_MsgViewCloseButton) end)

    if not self.m_DefaultSimpleTableViewDataSource then

		self.m_DefaultSimpleTableViewDataSource = DefaultUISimpleTableViewDataSource.Create(function ()
			return self:NumberOfRows()
		end,
		function ( index )
			return self:CellForRowAtIndex(index)
		end)
    end
    self.m_MsgItemTemplate:SetActive(false)
    self.m_MsgView:SetActive(false)

    self.m_HiddenUIs = {
        CUIResources.MainPlayerFrame,
        CUIResources.CurrentTarget,
        CUIResources.TopRightMenu,
        CUIResources.RightMenuWnd,
        CUIResources.SkillButtonBoard,
        CUIResources.TaskAndTeam,
        CUIResources.ThumbnailChatWindow,
        CUIResources.ExpProgressBar,
    }

    self.m_BossInfoTbl = nil

    self.m_DelayShowBossInfoTick = {}
    self.m_DelayShowBossInfoTick[EnumCommonForce_lua.eAttack] = nil
    self.m_DelayShowBossInfoTick[EnumCommonForce_lua.eDefend] = nil
    self.m_QuickMsgs = {}
end

function LuaGuildLeagueCrossWatchWnd:Init()
    local forceInfo = LuaGuildLeagueCrossMgr.m_WatchInfo.forceInfo
    self.m_TitleLabel.text = self:GetTitleName()
    self:InitOneForceInfo(self.m_AttRoot,  forceInfo and forceInfo[EnumCommonForce_lua.eAttack] or nil)
    self:InitOneForceInfo(self.m_DefRoot, forceInfo and forceInfo[EnumCommonForce_lua.eDefend] or nil)
    self:InitHomeHpInfo()
    self:InitTowerNum(LuaGuildLeagueCrossMgr.m_defendTowerNum, LuaGuildLeagueCrossMgr.m_attackTowerNum)
    self:InitKillNum(LuaGuildLeagueCrossMgr.m_defendKillNum, LuaGuildLeagueCrossMgr.m_attackKillNum)
    self:InitBossInfo(nil)
    self.m_QuickMsgTemplate:SetActive(false)
end

function LuaGuildLeagueCrossWatchWnd:GetTitleName()
    local round = LuaGuildLeagueCrossMgr.m_WatchInfo.round
    local index1 = LuaGuildLeagueCrossMgr.m_WatchInfo.index1
    local index2 = LuaGuildLeagueCrossMgr.m_WatchInfo.index2
    if round==1 then return LocalString.GetString("小组赛第一轮") end
    if round==2 then return LocalString.GetString("小组赛第二轮") end
    if round==3 then return LocalString.GetString("小组赛第三轮") end
    if round==4 then return LocalString.GetString("排位赛第一轮") end
    if round==5 then return LocalString.GetString("排位赛第二轮") end
    if index1==1 or index1==2 then return LocalString.GetString("冠亚军之战") end
    return SafeStringFormat3(LocalString.GetString("%s-%s名之战"), index1, index2)
end

function LuaGuildLeagueCrossWatchWnd:InitOneForceInfo(transRoot, forceInfo)
    local serverNameLabel = transRoot:Find("ServerNameLabel"):GetComponent(typeof(UILabel))
    local guildNameLabel = transRoot:Find("GuildNameLabel"):GetComponent(typeof(UILabel))
    serverNameLabel.text = forceInfo and forceInfo.serverName or ""
    guildNameLabel.text = forceInfo and forceInfo.guildName or ""
end

function LuaGuildLeagueCrossWatchWnd:InitHomeHpInfo()
    local homeHpInfo = LuaGuildLeagueMgr.m_HomeHpInfo
    if homeHpInfo==nil then
        self.m_AttHpBar.value = 0
        self.m_DefHpBar.value = 0
        self.m_AttHpValueLabel.text = ""
        self.m_DefHpValueLabel.text = ""
    --将军威
    elseif homeHpInfo.force1 == EnumCommonForce_lua.eAttack then
        self.m_AttHpBar.value = homeHpInfo.curHp1 / homeHpInfo.fullHp1
        self.m_DefHpBar.value = homeHpInfo.curHp2 / homeHpInfo.fullHp2
        self.m_AttHpValueLabel.text = tostring(homeHpInfo.curHp1)
        self.m_DefHpValueLabel.text = tostring(homeHpInfo.curHp2)
    else
        self.m_AttHpBar.value = homeHpInfo.curHp2 / homeHpInfo.fullHp2
        self.m_DefHpBar.value = homeHpInfo.curHp1 / homeHpInfo.fullHp1
        self.m_AttHpValueLabel.text = tostring(homeHpInfo.curHp2)
        self.m_DefHpValueLabel.text = tostring(homeHpInfo.curHp1)
    end
end

function LuaGuildLeagueCrossWatchWnd:InitBossInfo(bossInfoTbl)
    local attforce = EnumCommonForce_lua.eAttack
    local defForce = EnumCommonForce_lua.eDefend
    local ori_list = self.m_BossInfoTbl and self.m_BossInfoTbl[attforce] or nil
    local cur_list = bossInfoTbl and bossInfoTbl[attforce] or nil
    self:InitOneSideBossInfo(self.m_AttBossTable, ori_list, cur_list, attforce)
    local ori_list = self.m_BossInfoTbl and self.m_BossInfoTbl[defForce] or nil
    local cur_list = bossInfoTbl and bossInfoTbl[defForce] or nil
    self:InitOneSideBossInfo(self.m_DefBossTable, ori_list, cur_list, defForce)
    self.m_BossInfoTbl = bossInfoTbl
end
function LuaGuildLeagueCrossWatchWnd:StopDelayShowBossInfoTick(force)
    if self.m_DelayShowBossInfoTick[force] then
        UnRegisterTick(self.m_DelayShowBossInfoTick[force])
        self.m_DelayShowBossInfoTick[force] = nil
    end
end

function LuaGuildLeagueCrossWatchWnd:InitOneSideBossInfo(table, ori_list, cur_list, force)
    local disappearBoss = {} --刷新后消失的boss
    local appearBoss = {} --刷新后出现的boss
    if ori_list then
        for index, value in pairs(ori_list) do
            disappearBoss[value.templateId] = index
        end
        if cur_list then
            for index2, value2 in pairs(cur_list) do
                if disappearBoss[value2.templateId] then
                    disappearBoss[value2.templateId] = nil --boss之前有，现在也有
                else
                    appearBoss[value2.templateId] = index2 --boss之前没有，现在新出现
                end
            end
        end
    else
        if cur_list then --全部新出现
            for index, value in pairs(cur_list) do
                appearBoss[value.templateId] = index
            end
        end
    end

    self:StopDelayShowBossInfoTick(force)

    --动画显示消失出现过程 0-0.5s
    local exist = false
    for templateId, index in pairs(disappearBoss) do
        exist = true
        local child = table.transform:GetChild(index-1)
        if child then
            --播放一个消失特效
            local fx = child:Find("Fx"):GetComponent(typeof(CUIFx))
            fx:LoadFx("Assets/Res/Fx/UI/Prefab/UI_kuafubanghuiliansai_jisha.prefab")
        end
    end
   
    if exist then
        self.m_DelayShowBossInfoTick[force] = RegisterTickOnce(function()
            self:ShowOneSideBossInfo(table, cur_list, appearBoss)
        end, 500) 
    else
        self:ShowOneSideBossInfo(table, cur_list, appearBoss)
    end    
end

function LuaGuildLeagueCrossWatchWnd:ShowOneSideBossInfo(table, cur_list, appearBoss)
    local n = table.transform.childCount
    for i=0,n-1 do
        local child = table.transform:GetChild(i)
        child.gameObject:SetActive(false)
    end
    if not cur_list then return end
    local idx = 0
    for __, data in pairs(cur_list) do
        local child = table.transform:GetChild(idx)
        child.gameObject:SetActive(true)
        idx = idx+1
        local portrait = child:Find("Portrait"):GetComponent(typeof(CUITexture))
        local slider = child:Find("Slider"):GetComponent(typeof(UISlider))
        local fx = child:Find("Fx"):GetComponent(typeof(CUIFx))
        local monster = Monster_Monster.GetData(data.templateId)
        portrait:LoadNPCPortrait(monster.HeadIcon, false)
        slider.value = data.hpFull>0 and data.hp / data.hpFull or 0
        if appearBoss[data.templateId] then
            fx:LoadFx("Assets/Res/Fx/UI/Prefab/UI_kuafubanghuiliansai_zhaohuan.prefab")
        else
            fx:DestroyFx()
        end
    end
    table:Reposition()
end

function LuaGuildLeagueCrossWatchWnd:InitTowerNum(defendTowerNum, attackTowerNum)
    self.m_DefTowerNumLabel.text = tostring(defendTowerNum)
    self.m_AttTowerNumLabel.text = tostring(attackTowerNum)
end

function LuaGuildLeagueCrossWatchWnd:InitKillNum(defendKillNum, attackKillNum)
    self.m_DefKillNumLabel.text = tostring(defendKillNum)
    self.m_AttKillNumLabel.text = tostring(attackKillNum)
end

function LuaGuildLeagueCrossWatchWnd:OnEnable()
    if CMainCamera.Inst then
        CMainCamera.Inst:ChangeOverrideFov(60, false)
    end
    for __,module in pairs(self.m_HiddenUIs) do
        CUIManager.SetUIVisibility(module, false, "guildleaguecorsswatch")
    end
    if CJoystickWnd.Instance then
        CJoystickWnd.Instance:SetMountButtonVisible(false, "guildleaguecorsswatch")
    end
    g_ScriptEvent:AddListener("OnUpdateGuildLeagueHomeHp", self, "OnUpdateGuildLeagueHomeHp")
    g_ScriptEvent:AddListener("OnSyncGuildLeagueTowerCountToWatcher", self, "OnSyncGuildLeagueTowerCountToWatcher")
    g_ScriptEvent:AddListener("OnSyncGuildLeagueBossHpInfoToWatcher", self, "OnSyncGuildLeagueBossHpInfoToWatcher")
    g_ScriptEvent:AddListener("SyncGuildLeagueKillNumToWatcher", self, "InitKillNum")
    g_ScriptEvent:AddListener("OnSendMsgToGuildLeagueWatcher", self, "OnSendMsgToGuildLeagueWatcher")
    g_ScriptEvent:AddListener("OnScreenChange", self, "__UpdateAnchors")
end

function LuaGuildLeagueCrossWatchWnd:OnDisable()
    if CMainCamera.Inst then
        CMainCamera.Inst:ChangeOverrideFov(0, true)
    end
    for __,module in pairs(self.m_HiddenUIs) do
        CUIManager.SetUIVisibility(module, true, "guildleaguecorsswatch")
    end
    if CJoystickWnd.Instance then
        CJoystickWnd.Instance:SetMountButtonVisible(true, "guildleaguecorsswatch")
    end
    g_ScriptEvent:RemoveListener("OnUpdateGuildLeagueHomeHp", self, "OnUpdateGuildLeagueHomeHp")
    g_ScriptEvent:RemoveListener("OnSyncGuildLeagueTowerCountToWatcher", self, "OnSyncGuildLeagueTowerCountToWatcher")
    g_ScriptEvent:RemoveListener("OnSyncGuildLeagueBossHpInfoToWatcher", self, "OnSyncGuildLeagueBossHpInfoToWatcher")
    g_ScriptEvent:RemoveListener("SyncGuildLeagueKillNumToWatcher", self, "InitKillNum")
    g_ScriptEvent:RemoveListener("OnSendMsgToGuildLeagueWatcher", self, "OnSendMsgToGuildLeagueWatcher")
    g_ScriptEvent:RemoveListener("OnScreenChange", self, "__UpdateAnchors")

    self:StopDelayShowBossInfoTick(EnumCommonForce_lua.eAttack)
    self:StopDelayShowBossInfoTick(EnumCommonForce_lua.eDefend)
end

function LuaGuildLeagueCrossWatchWnd:OnUpdateGuildLeagueHomeHp()
    self:InitHomeHpInfo()
end

function LuaGuildLeagueCrossWatchWnd:OnSyncGuildLeagueTowerCountToWatcher(defendTowerNum, attackTowerNum)
    self:InitTowerNum(defendTowerNum, attackTowerNum)
end

function LuaGuildLeagueCrossWatchWnd:OnSyncGuildLeagueBossHpInfoToWatcher(bossInfoTbl)
    self:InitBossInfo(bossInfoTbl)
end

--改编自LuaGuanNingStateWnd
function LuaGuildLeagueCrossWatchWnd:OnSendMsgToGuildLeagueWatcher(time, msg, msg2, playerClass, playerGender, monsterTemplateId)

    table.insert(self.m_QuickMsgs, {time=time, msg=msg})
    if self.m_MsgView.activeSelf then
        self:LoadMsgData()
    end
    for i=0,self.m_QuickMsgRoot.childCount-1 do
        local child = self.m_QuickMsgRoot:GetChild(i)
        LuaUtils.SetLocalPositionY(child, child.localPosition.y+64) --整体上移
    end

    local go= CUICommonDef.AddChild(self.m_QuickMsgRoot.gameObject, self.m_QuickMsgTemplate)
    go:SetActive(true)
    local label = go.transform:Find("Node/Label"):GetComponent(typeof(UILabel))
    local icon = go.transform:Find("Node/Icon"):GetComponent(typeof(CUITexture))

    label.text = msg2
    icon:Clear()
    local monster = Monster_Monster.GetData(monsterTemplateId)
    if monster then
        icon:LoadPortrait(monster.HeadIcon,false)
    else
        local initData=Initialization_Init.GetData(playerClass*100+playerGender)
        if initData then
            icon:LoadPortrait(initData.ResName,false)
        end
    end

    local texture= go.transform:Find("Node/Texture"):GetComponent(typeof(CUITexture))
    texture.gameObject:SetActive(false) --这个暂时无用

    local node = go.transform:Find("Node")
    local bg = go.transform:Find("Bg")

    --总时长4.3s
    local delayTime = 4
    local fadeInTime = 0.3
    local fadeOutTime = 0.3

    Object.Destroy(go,delayTime + fadeOutTime)

    LuaTweenUtils.SetEase(LuaTweenUtils.TweenScale(go.transform, Vector3(0.5,0.5,1),Vector3(1,1,1), fadeInTime), Ease.OutBack)
    LuaTweenUtils.TweenAlpha(node, 0,1,fadeInTime)
    LuaTweenUtils.SetDelay(LuaTweenUtils.TweenAlpha(label.transform, 0,1,0.1),0.1)

    LuaTweenUtils.TweenAlpha(bg, 0,0.75,fadeInTime)
    LuaTweenUtils.TweenScale(bg, Vector3(0,0,1),Vector3(1,1,1),0.1)
    LuaTweenUtils.SetDelay(LuaTweenUtils.TweenAlpha(node,1,0,fadeOutTime),delayTime) 
    LuaTweenUtils.SetDelay(LuaTweenUtils.TweenAlpha(bg,0.75,0,fadeOutTime),delayTime) 
            
    LuaTweenUtils.SetDelay(
        LuaTweenUtils.SetEase(
            LuaTweenUtils.TweenPositionY(go.transform,100,fadeOutTime),Ease.InExpo),delayTime)
end

function LuaGuildLeagueCrossWatchWnd:OnLeaveButtonClick(go)
    CGamePlayMgr.Inst:LeavePlay()
end

function LuaGuildLeagueCrossWatchWnd:OnSettingButtonClick(go)
    if CUIManager.IsLoaded(CLuaUIResources.CheckboxPopupMenu) then
        CUIManager.CloseUI(CLuaUIResources.CheckboxPopupMenu)
        return
    end
    local tbl = {}
    table.insert(tbl, {
        text= LocalString.GetString("屏蔽血条"), 
        action = function(selected) self:OnHideGLCPlayerHPBarSettingChanged(selected) end, 
        defaultSelected = CLuaPlayerSettings.GetValueForGLCWatchSettingWnd("HideGLCPlayerHPBarForWatch")})
    table.insert(tbl, {
            text= LocalString.GetString("屏蔽灵兽、召唤物"), 
            action = function(selected) self:OnHidePetSettingChanged(selected) end, 
            defaultSelected = CLuaPlayerSettings.GetValueForGLCWatchSettingWnd("HidePetEnabled")})
    table.insert(tbl, {
        text= LocalString.GetString("屏蔽技能特效"), 
        action = function(selected) self:OnHideSkillEffectSettingChanged(selected) end, 
        defaultSelected = CLuaPlayerSettings.GetValueForGLCWatchSettingWnd("HideSkillEffect")})
    g_PopupMenuMgr:ShowCheckboxPopupMenuByTransform(nil, tbl, go.transform, EnumCheckboxPopupMenuAlginType.Right, 0)
end

function LuaGuildLeagueCrossWatchWnd:OnSituationButtonClick(go)
    LuaGuildLeagueMgr:ShowGuildLeagueSituationWnd(true) --复用联赛战况界面
end

function LuaGuildLeagueCrossWatchWnd:OnMsgButtonClick(go)
    self:SetMsgViewVisible(true)
    self:LoadMsgData()
end

function LuaGuildLeagueCrossWatchWnd:OnMatchInfoButtonClick(go)
    LuaGuildLeagueCrossMgr:ShowMatchInfoWnd()
end

function LuaGuildLeagueCrossWatchWnd:OnHideGLCPlayerHPBarSettingChanged(value)
    --没找到合适的刷新时机，目前的逻辑为界面上不显示血条，但是会处理相关的逻辑
    CLuaPlayerSettings.SetValueForGLCWatchSettingWnd("HideGLCPlayerHPBarForWatch", value)
end

function LuaGuildLeagueCrossWatchWnd:OnHidePetSettingChanged(value)
    CLuaPlayerSettings.SetValueForGLCWatchSettingWnd("HidePetEnabled", value)
end

function LuaGuildLeagueCrossWatchWnd:OnHideSkillEffectSettingChanged(value)
    CLuaPlayerSettings.SetValueForGLCWatchSettingWnd("HideSkillEffect", value)
end

--消息记录呈现
function LuaGuildLeagueCrossWatchWnd:OnMsgViewCloseButtonClick(go)
    self:SetMsgViewVisible(false)
end
function LuaGuildLeagueCrossWatchWnd:LoadMsgData()
    self.m_SimpleTableView.scrollView.panel:ResetAndUpdateAnchors()
    self.m_SimpleTableView.table.transform.localPosition = Extensions.GetTopPos(self.m_SimpleTableView.scrollView)
    self.m_SimpleTableView:Clear()
    self.m_SimpleTableView.dataSource = self.m_DefaultSimpleTableViewDataSource
	self.m_SimpleTableView:LoadDataFromTail()
end

function LuaGuildLeagueCrossWatchWnd:SetMsgViewVisible(visible)
    if visible then
        Extensions.SetLocalPositionX(self.m_MsgViewOffset, 0)
        self.m_MsgView:SetActive(true)
    else
        local tweener = LuaTweenUtils.TweenPositionX(self.m_MsgViewOffset, -600, 0.2)
        tweener = LuaTweenUtils.SetEase(tweener, Ease.Linear)
        LuaTweenUtils.OnComplete(tweener, function()
            self.m_MsgView:SetActive(false)
        end)
    end
end

function LuaGuildLeagueCrossWatchWnd:NumberOfRows()
	return #self.m_QuickMsgs
end

function LuaGuildLeagueCrossWatchWnd:CellForRowAtIndex(index)
	if index < 0 and index > #self.m_QuickMsgs then
        return nil
    end
    local cellIdentifier = "MsgItemTemplate"
    local cell = self.m_SimpleTableView:DequeueReusableCellWithIdentifier(cellIdentifier)
    if cell == nil then
        cell = self.m_SimpleTableView:AllocNewCellWithIdentifier(self.m_MsgItemTemplate, cellIdentifier)
    end

    local timeLabel = cell.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
    local msgLabel = cell.transform:Find("MsgLabel"):GetComponent(typeof(UILabel))
    timeLabel.text = LuaGuildLeagueCrossMgr:FormatTimestamp(self.m_QuickMsgs[index+1].time, "HH:mm")
    msgLabel.text = self.m_QuickMsgs[index+1].msg
    return cell
end

function LuaGuildLeagueCrossWatchWnd:__UpdateAnchors()
    RegisterTickOnce(function() --暂时延迟1帧解决获取的topPos数值不能及时更新问题，感觉和时序有关系
        if self.m_MsgView.activeSelf then
            self:LoadMsgData()
        end
    end, 1)
end

