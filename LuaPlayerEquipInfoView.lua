local EnumQualityType=import "L10.Game.EnumQualityType"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
-- local CPlayerEquipInfoView = import "L10.UI.CPlayerEquipInfoView"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CCommonItem = import "L10.Game.CCommonItem"
local CBodyEquipSlot = import "L10.UI.CBodyEquipSlot"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local EnumClass = import "L10.Game.EnumClass"
local CUIMaterialPaths=import "L10.UI.CUIMaterialPaths"

CLuaPlayerEquipInfoView = class()
RegistClassMember(CLuaPlayerEquipInfoView,"equipSlots")
RegistClassMember(CLuaPlayerEquipInfoView,"inited")
RegistClassMember(CLuaPlayerEquipInfoView,"pos2EquipDict")
RegistClassMember(CLuaPlayerEquipInfoView,"transform")
RegistClassMember(CLuaPlayerEquipInfoView,"lastClass")

function CLuaPlayerEquipInfoView:Init(transform)
    self.transform=transform
    self.equipSlots={}
end

function CLuaPlayerEquipInfoView:BindSlots( )
    if self.inited then
        return
    end
    self.inited = true
    self.equipSlots={}
    local addSlot = function(goName)
        local slot = self.transform:Find("EquipmentFrame/" .. goName).gameObject:GetComponent(typeof(CBodyEquipSlot))
        if slot then
            table.insert( self.equipSlots, slot )
        end
    end
    addSlot("WeaponSlot")
    addSlot("ShieldSlot")
    addSlot("HeadwearSlot")
    addSlot("ClothesSlot")
    addSlot("BeltSlot")
    addSlot("GlovesSlot")
    addSlot("ShoesSlot")
    addSlot("LeftRingSlot")
    addSlot("LeftBraceletSlot")
    addSlot("NecklaceSlot")
    addSlot("RightRingSlot")
    addSlot("RightBraceletSlot")
    addSlot("BackPendantSlot")

    for i,v in ipairs(self.equipSlots) do
        UIEventListener.Get(v.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnEquipmentSlotClick(go)
        end)
    end
end

function CLuaPlayerEquipInfoView:InitEquipSlots( )
    self.pos2EquipDict={}
    self:BindSlots()
    do
        local i = 0
        while i < #self.equipSlots do
            self.equipSlots[i+1]:Init(nil, EnumQualityType.None, false, nil, (i + 1))
            self.equipSlots[i+1].DisableSpriteVisible = false
            i = i + 1
        end
    end
end

function CLuaPlayerEquipInfoView:UpdateSlot(pos, equip, class) 
    self:BindSlots()
    self.pos2EquipDict[pos] = equip
    do
        local i = 1
        while i <= #self.equipSlots do
            if i == pos then
                local icon = equip.BigIcon
                self.equipSlots[i]:Init(icon, equip.Color, 
                    i == EnumBodyPosition_lua.Weapon or i == EnumBodyPosition_lua.Shield or i == EnumBodyPosition_lua.Armour, 
                    equip, i)
                --1，2，4位置为武器、盾牌和甲胄

                if i == EnumBodyPosition_lua.Weapon then
                    if EquipmentTemplate_Equip.GetData(equip.TemplateId).BothHand == 1 then
                        self.equipSlots[EnumBodyPosition_lua.Shield].DisableSpriteVisible = true
                    end
                end

                self.equipSlots[i].DisableSpriteVisible = self:NeedShowDisableSprite(i, equip, class)
            end
            i = i + 1
        end
    end
    if not self.lastClass and CommonDefs.IsZhanKuang(CommonDefs.ConvertIntToEnum(typeof(EnumClass), class)) then
        self.lastClass = class
        -- 战狂副手底图为武器
        if not self.pos2EquipDict[EnumBodyPosition_lua.Shield] then
            self.equipSlots[EnumBodyPosition_lua.Shield]:Init(CUIMaterialPaths.EquipSlotWeapon, EnumQualityType.None, true, nil, EnumBodyPosition_lua.Shield)
        end
        -- 战狂副手翻转
        self.equipSlots[EnumBodyPosition_lua.Shield]:SetHFlip(true)
        -- 战狂副手不禁用
        self.equipSlots[EnumBodyPosition_lua.Shield].DisableSpriteVisible = self:NeedShowDisableSprite(EnumBodyPosition_lua.Shield, equip, class)
    end
end
function CLuaPlayerEquipInfoView:NeedShowDisableSprite( position, equip, class) 
    local needShowDisableSprite = false
    if equip ~= nil then
        needShowDisableSprite = (equip.Disable == 1) or (equip.IsLostSoul == 1)
    end
    if not needShowDisableSprite and (position == EnumBodyPosition_lua.Shield) and (not CommonDefs.IsZhanKuang(CommonDefs.ConvertIntToEnum(typeof(EnumClass), class))) then
        if self.pos2EquipDict[EnumBodyPosition_lua.Weapon] then
            local weapon = self.pos2EquipDict[EnumBodyPosition_lua.Weapon]
            needShowDisableSprite = (weapon ~= nil and EquipmentTemplate_Equip.GetData(weapon.TemplateId).BothHand == 1)
        end
    end
    return needShowDisableSprite
end
function CLuaPlayerEquipInfoView:OnEquipmentSlotClick( go) 
    do
        local i = 0
        while i < #self.equipSlots do
            if go:Equals(self.equipSlots[i+1].gameObject) then
                if self.pos2EquipDict[i + 1] then
                    local item  = CCommonItem(self.pos2EquipDict[i+1])
                    CItemInfoMgr.ShowLinkItemInfo(item, true, nil, AlignType.Default, 0, 0, 0, 0, 0)
                end
                break
            end
            i = i + 1
        end
    end
end
