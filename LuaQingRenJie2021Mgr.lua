CLuaQingRenJie2021Mgr = {}
CLuaQingRenJie2021Mgr.m_OthersInfo = {}
CLuaQingRenJie2021Mgr.m_SelfInfo = {}

function CLuaQingRenJie2021Mgr.ReplyYiAiZhiMingSendFlowerRank(selfData, rankData)
    local list = MsgPackImpl.unpack(selfData)

    CLuaQingRenJie2021Mgr.m_OthersInfo = {}
    CLuaQingRenJie2021Mgr.m_SelfInfo = {
        num = {list[0],list[1],list[2],list[3]}, 
    }
    local rankList = MsgPackImpl.unpack(rankData)

    local mainPlayerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id

    for i = 0, rankList.Count-1, 8 do
        local rank = rankList[i]
        local playerId = rankList[i+1]
        table.insert(CLuaQingRenJie2021Mgr.m_OthersInfo, {
            rank = rank, 
            playerId = playerId,
            playerClass = rankList[i+2], 
            playerName = rankList[i+3], 
            num = {rankList[i+4],rankList[i+5],rankList[i+6],rankList[i+7]},
        })
        if playerId == mainPlayerId then
            CLuaQingRenJie2021Mgr.m_SelfInfo.rank = rank
        end
    end
    CUIManager.ShowUI(CLuaUIResources.QingRenJie2021RankWnd)
end