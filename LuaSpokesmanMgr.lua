
CLuaSpokesmanMgr = {}

CLuaSpokesmanMgr.eSpokesmanPawnTime = 133
CLuaSpokesmanMgr.currentItemId = 21005442

-- itemsInfo = {itemId1, pos1, itemId2, pos2, ...}
function Gas2Gac.GlobalSpokesmanSendAvailablePawn(itemsInfo)
    local list=MsgPackImpl.unpack(itemsInfo)
    if not list then return end

    local datas = {}

    for i = 1, list.Count, 2 do
        local data = {
            itemId = tostring(list[i-1]),
            pos = tonumber(list[i])
        }

        table.insert(datas, data)
    end

    g_ScriptEvent:BroadcastInLua("GlobalSpokesmanSendAvailablePawn", datas)
end


CLuaSpokesmanMgr.m_SpokesmanDialogWndState = 0
CLuaSpokesmanMgr.m_SpokesmanDialogWndMessage = ""
CLuaSpokesmanMgr.m_SpokesmanPreviewWndPreviewCardID = 0
CLuaSpokesmanMgr.m_SpokesmanCardsComposeNum = 0
CLuaSpokesmanMgr.m_SpokesmanCardsComposeResultList = {}

function CLuaSpokesmanMgr:SyncSpokesmanTCGData(composeTimes, dataUD)
    if not CUIManager.IsLoaded(CLuaUIResources.SpokesmanCardsWnd) then
        CUIManager.ShowUI(CLuaUIResources.SpokesmanCardsWnd)
        return
    end
    self.m_SpokesmanCardsComposeNum = composeTimes
    local data = {}
    data.list = {}
    SpokesmanTCG_TCG.ForeachKey(function (key)
        table.insert(data.list,{id = key,owner = false,isnew = false,num = 0})
    end)
    if dataUD then
        local argvs = MsgPackImpl.unpack(dataUD)
        if argvs and argvs.Count > 0 then
            for i= 0, argvs.Count - 1, 3 do
                local cardId = argvs[i] -- 已激活了的卡牌ID
                local redundantCount = argvs[i+1] -- 多余的数量
                local isNew = argvs[i+2] -- 是否是新获得的
                data.list[cardId].num = redundantCount + 1
                data.list[cardId].isnew = isNew
                data.list[cardId].owner = true
            end
        end
    end
    g_ScriptEvent:BroadcastInLua("SpokesmanCardsWnd_LoadData",data)
end

function CLuaSpokesmanMgr:SyncSpokesmanTCGComposeResult(dataUD)
    if dataUD then
        local argvs = MsgPackImpl.unpack(dataUD)
        if argvs and argvs.Count > 0 then
            self.m_SpokesmanCardsComposeResultList = {}
            for i= 0, argvs.Count - 1, 2 do
                local cardId = argvs[i] -- 获得了的卡牌ID
                local isNewGet = argvs[i+1] -- 是否是第一次获得此卡牌
                table.insert(self.m_SpokesmanCardsComposeResultList, {id = cardId, owner = true, isnew = isNewGet})
            end
            CUIManager.ShowUI(CLuaUIResources.SpokesmanCardsComposeResultWnd)
        end
    end
    g_ScriptEvent:BroadcastInLua("SpokesmanCardsWnd_UpdateData")
end

function CLuaSpokesmanMgr:OpenSpokesmanHireContractWnd()
    Gac2Gas.QuerySpokesmanContractSign(false)
end

function CLuaSpokesmanMgr:QuerySpokesmanContractSignResult(signed)
    self.m_SpokesmanDialogWndState = signed and 1 or 0
    self.m_SpokesmanDialogWndMessage =  signed and "SpokesmanDialogWnd_Message2" or "SpokesmanDialogWnd_Message1"
    if signed then
        g_ScriptEvent:RemoveListener("SpokesmanHireContractWnd_LoadData",self,"LoadData")
        g_ScriptEvent:AddListener("SpokesmanHireContractWnd_LoadData",self,"LoadData")
        Gac2Gas.QuerySpokesmanContractDetail()
    else
        CUIManager.ShowUI(CLuaUIResources.SpokesmanDialogWnd)
    end
end

function CLuaSpokesmanMgr:LoadData(data)
    g_ScriptEvent:RemoveListener("SpokesmanHireContractWnd_LoadData",self,"LoadData")
    local allAwarded = data.allAwarded
    local allFinished = true
    local len = #data.list
    for i = 1,len do
        local d = data.list[i]
        local id = d.contractId
        local contract = Spokesman_Contract.GetData(id)
        local isLock = d.progress < contract.NeedProgress
        local isFinish = d.isAwarded
        allFinished = allFinished and (not isLock)
        if (not isFinish) and (not isLock) then
            CUIManager.ShowUI(CLuaUIResources.SpokesmanDialogWnd)
            return
        end
    end
    local canGetAllFinishedReward = allFinished and (not allAwarded)
    if canGetAllFinishedReward then
        CUIManager.ShowUI(CLuaUIResources.SpokesmanDialogWnd)
        return
    end
    CUIManager.ShowUI(CLuaUIResources.SpokesmanHireContractWnd)
end

function CLuaSpokesmanMgr:SignSpokesmanContractSuccess()
    CUIManager.ShowUI(CLuaUIResources.SpokesmanHireContractWnd)
end

function CLuaSpokesmanMgr:UpdateSpokesmanContractProgress(contractId, progress)
    if CUIManager.IsLoaded(CLuaUIResources.SpokesmanHireContractWnd) then
        g_ScriptEvent:BroadcastInLua("SpokesmanHireContractWnd_UpdatenContract",contractId, progress)
    elseif not CUIManager.IsLoaded(CLuaUIResources.SpokesmanDialogWnd) then
        local contract = Spokesman_Contract.GetData(contractId)
        if progress == contract.NeedProgress then
            Gac2Gas.QuerySpokesmanContractSign(false)
        end
    end
end

function CLuaSpokesmanMgr:QuerySpokesmanContractDetailResult(progressListUD, awarded, allAwarded)
    local progressList = MsgPackImpl.unpack(progressListUD)
    if not progressList then return end
    local data = {}
    data.list = {}
    data.allAwarded = allAwarded
    Spokesman_Contract.ForeachKey(function (key)
        table.insert(data.list,{contractId = key, progress = 0, isAwarded = false})
    end)
    for i = 0, progressList.Count - 1, 2 do
        local contractId = progressList[i]
        local progress = progressList[i+1]
        local isAwarded = bit.band(awarded, bit.lshift(1, contractId - 1)) > 0
        data.list[contractId].progress = progress
        data.list[contractId].isAwarded = isAwarded
    end
    g_ScriptEvent:BroadcastInLua("SpokesmanHireContractWnd_LoadData",data)
end

function CLuaSpokesmanMgr:SendFinishSpokesmanContractAwardSuccess(contractId)
    g_ScriptEvent:BroadcastInLua("SpokesmanHireContractWnd_ContractAwardSuccess",contractId)
end

function CLuaSpokesmanMgr:SendFinishAllSpokesmanContractAwardSuccess()
    g_ScriptEvent:BroadcastInLua("SpokesmanHireContractWnd_AllContractAwardSuccess")
end

