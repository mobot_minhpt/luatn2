local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local AlignType = import "CPlayerInfoMgr+AlignType"
local Object = import "System.Object"

LuaCHDayRankWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaCHDayRankWnd, "TableView", QnAdvanceGridView)
RegistChildComponent(LuaCHDayRankWnd, "BottomLabel", UILabel)
RegistChildComponent(LuaCHDayRankWnd, "RuleBtn", GameObject)
--@endregion RegistChildComponent end

function LuaCHDayRankWnd:Awake()
    UIEventListener.Get(self.RuleBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnRuleBtnClick()
    end)
end

function LuaCHDayRankWnd:Init()
    self.BottomLabel.text = g_MessageMgr:FormatMessage("ClubHouse_Day_Rank_Bottom_Tip")
    -- 设置排名显示
    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(function()
        return math.min(#self.m_RankList, 20)
    end, function(item, index)
        item:SetBackgroundTexture(index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)
        self:InitItem(item, index + 1, self.m_RankList[index + 1])
    end)
    -- 设置选中状态，显示玩家信息
    self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function (index)
        local data = self.m_RankList[index + 1]
        local extraInfo = CreateFromClass(MakeGenericClass(Dictionary, String, Object))
        CommonDefs.DictAdd_LuaCall(extraInfo, "AccId", data.member.AccId)
        CommonDefs.DictAdd_LuaCall(extraInfo, "AppName", data.member.AppName)
        CPlayerInfoMgr.ShowPlayerPopupMenu(data.member.PlayerId, EnumPlayerInfoContext.ChatRoom, EChatPanel.Undefined, 
            nil, nil, extraInfo, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
    end)
    self:InitRankData()
end

function LuaCHDayRankWnd:InitRankData()
    self.m_RankList = LuaClubHouseMgr.m_CurRoomInfo.PresentRankTbl
    self.TableView:ReloadData(true, false)
end

function LuaCHDayRankWnd:InitItem(item, rank, info)
    -- 排名
    local rankLabel = item.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    local rankImage = item.transform:Find("RankImage"):GetComponent(typeof(UISprite))
    local rankImgList = {g_sprites.RankFirstSpriteName, g_sprites.RankSecondSpriteName, g_sprites.RankThirdSpriteName}
    rankImage.spriteName = rankImgList[rank] 
    rankLabel.text = rank > 0 and tostring(rank) or LocalString.GetString("未上榜")
    -- 其他信息
    item.transform:Find("My").gameObject:SetActive(info.member.IsMe)
    local nameLabel = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local valueLabel = item.transform:Find("ValueLabel"):GetComponent(typeof(UILabel))
    nameLabel.color = info.member.IsMe and Color.green or Color.white
    valueLabel.color = info.member.IsMe and Color.green or Color.white
    nameLabel.text = info.member.PlayerName
    valueLabel.text = tostring(info.value)
end
--@region UIEvent

function LuaCHDayRankWnd:OnRuleBtnClick()
    g_MessageMgr:DisplayMessage("ClubHouse_Day_Rank_Tip")
end
--@endregion UIEvent

