local UILabel = import "UILabel"
CLuaCommonConfirmWithInfoWnd = class()

function CLuaCommonConfirmWithInfoWnd:Init()
    local transNameTbl = {"TextLabel", "Node/CostTitleLabel", "Node/CostLabel", "Node/OwnTitleLabel", "Node/OwnLabel"}
    local valueTbl = {CLuaCommonConfirmWithInfoWndMgr.m_Text, CLuaCommonConfirmWithInfoWndMgr.m_InfoTitle1, CLuaCommonConfirmWithInfoWndMgr.m_InfoValue1, CLuaCommonConfirmWithInfoWndMgr.m_InfoTitle2, CLuaCommonConfirmWithInfoWndMgr.m_InfoValue2}
    for i = 1, #transNameTbl do
        self.transform:Find("Anchor/"..transNameTbl[i]):GetComponent(typeof(UILabel)).text = valueTbl[i]
    end
    UIEventListener.Get(self.transform:Find("Anchor/OkButton").gameObject).onClick = LuaUtils.VoidDelegate(function()
        if CLuaCommonConfirmWithInfoWndMgr.m_OkFunc then
            CLuaCommonConfirmWithInfoWndMgr.m_OkFunc()
        end
        CUIManager.CloseUI(CLuaUIResources.CommonConfirmWithInfoWnd)
    end)
    UIEventListener.Get(self.transform:Find("Anchor/CancelButton").gameObject).onClick = LuaUtils.VoidDelegate(function()
        CUIManager.CloseUI(CLuaUIResources.CommonConfirmWithInfoWnd)
    end)
end

CLuaCommonConfirmWithInfoWndMgr =  {}
CLuaCommonConfirmWithInfoWndMgr.m_Text = nil
CLuaCommonConfirmWithInfoWndMgr.m_InfoTitle1 = nil
CLuaCommonConfirmWithInfoWndMgr.m_InfoValue1 = nil
CLuaCommonConfirmWithInfoWndMgr.m_InfoTitle2 = nil
CLuaCommonConfirmWithInfoWndMgr.m_InfoValue2 = nil
CLuaCommonConfirmWithInfoWndMgr.m_OkFunc = nil
function CLuaCommonConfirmWithInfoWndMgr:ShowWnd(text, infoTitle1, infoValue1, infoTitle2, infoValue2, okFunc)
    CLuaCommonConfirmWithInfoWndMgr.m_Text = text
    CLuaCommonConfirmWithInfoWndMgr.m_InfoTitle1 = infoTitle1
    CLuaCommonConfirmWithInfoWndMgr.m_InfoValue1 = infoValue1
    CLuaCommonConfirmWithInfoWndMgr.m_InfoTitle2 = infoTitle2
    CLuaCommonConfirmWithInfoWndMgr.m_InfoValue2 = infoValue2
    CLuaCommonConfirmWithInfoWndMgr.m_OkFunc = okFunc
    CUIManager.ShowUI(CLuaUIResources.CommonConfirmWithInfoWnd)
end
