-- Auto Generated!!
local CLianShenSkillItem = import "L10.UI.CLianShenSkillItem"
local CUITexture = import "L10.UI.CUITexture"
local Extensions = import "Extensions"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
CLianShenSkillItem.m_Init_CS2LuaHook = function (this, cls, level, unlocked) 
    this.cls = cls
    this.level = level
    local skillID = CLuaDesignMgr.Skill_AllSkills_GetSkillId(cls, 1)
    if Skill_AllSkills.Exists(skillID) then
        this.skillIcon:LoadSkillIcon(Skill_AllSkills.GetData(skillID).SkillIcon)
    else
        this.skillIcon:Clear()
    end
    Extensions.SetLocalPositionZ(this.skillIcon.transform, level > 0 and 0 or - 1)
    this.lockIcon.gameObject:SetActive(not unlocked)
    if this.relativeConditionLine ~= nil then
        local default
        if level > 0 then
            default = this.lightLineMatPath
        else
            default = this.darkLineMatPath
        end
        CommonDefs.GetComponent_GameObject_Type(this.relativeConditionLine, typeof(CUITexture)):LoadMaterial(default)
    end

    if this.unlockLine ~= nil and this.lockLine ~= nil then
        this.unlockLine:SetActive(unlocked)
        this.lockLine:SetActive(not unlocked)
    end
end
