-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CNationalDayMgr = import "L10.UI.CNationalDayMgr"
local CNationalDayRankWnd = import "L10.UI.CNationalDayRankWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local EnumClass = import "L10.Game.EnumClass"
local Extensions = import "Extensions"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local Profession = import "L10.Game.Profession"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
CNationalDayRankWnd.m_Init_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        this:Close()
    end)

    UIEventListener.Get(this.tipNode).onClick = DelegateFactory.VoidDelegate(function (p) 
        g_MessageMgr:ShowMessage("GuoQing_PaiHangBang")
    end)

    this.templdateNode:SetActive(false)

    this:InitList()
end
CNationalDayRankWnd.m_GetTimeString_CS2LuaHook = function (this, time) 
    if time <= 0 then
        return System.String.Empty
    end

    local returnString = ""

    local min = math.floor(time / 60)
    if min > 0 then
        returnString = returnString .. (tostring(min) .. LocalString.GetString("分"))
    end

    local sec = time - min * 60
    returnString = returnString .. (tostring(sec) .. LocalString.GetString("秒"))

    return returnString
end
CNationalDayRankWnd.m_SetEmptySelfInfo_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst == nil then
        return
    end

    CommonDefs.GetComponent_Component_Type(this.selfNode.transform:Find("Name/text"), typeof(UILabel)).text = CClientMainPlayer.Inst.Name
    CommonDefs.GetComponent_Component_Type(this.selfNode.transform:Find("Name/JobIcon"), typeof(UISprite)).spriteName = Profession.GetIcon(CClientMainPlayer.Inst.Class)

    if CNationalDayMgr.Inst.m_SelfRankInfo ~= nil then
        CommonDefs.GetComponent_Component_Type(this.selfNode.transform:Find("time"), typeof(UILabel)).text = this:GetTimeString(CNationalDayMgr.Inst.m_SelfRankInfo.time)
    else
        CommonDefs.GetComponent_Component_Type(this.selfNode.transform:Find("time"), typeof(UILabel)).text = ""
    end
    CommonDefs.GetComponent_Component_Type(this.selfNode.transform:Find("rank"), typeof(UILabel)).text = LocalString.GetString("未上榜")
end
CNationalDayRankWnd.m_InitSelfInfo_CS2LuaHook = function (this, info, index) 
    CommonDefs.GetComponent_Component_Type(this.selfNode.transform:Find("Name/text"), typeof(UILabel)).text = info.playerName
    CommonDefs.GetComponent_Component_Type(this.selfNode.transform:Find("Name/JobIcon"), typeof(UISprite)).spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), (info.clazz)))
    CommonDefs.GetComponent_Component_Type(this.selfNode.transform:Find("time"), typeof(UILabel)).text = this:GetTimeString(info.time)
    CommonDefs.GetComponent_Component_Type(this.selfNode.transform:Find("rank"), typeof(UILabel)).text = tostring(index)
end
CNationalDayRankWnd.m_InitList_CS2LuaHook = function (this) 
    this:SetEmptySelfInfo()
    Extensions.RemoveAllChildren(this.table.transform)
    if CNationalDayMgr.Inst.m_RankList.Count > 0 then
        do
            local i = 0
            while i < CNationalDayMgr.Inst.m_RankList.Count do
                local info = CNationalDayMgr.Inst.m_RankList[i]
                local node = NGUITools.AddChild(this.table.gameObject, this.templdateNode)
                node:SetActive(true)

                CommonDefs.GetComponent_Component_Type(node.transform:Find("Name/text"), typeof(UILabel)).text = info.playerName
                CommonDefs.GetComponent_Component_Type(node.transform:Find("Name/JobIcon"), typeof(UISprite)).spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), (info.clazz)))
                CommonDefs.GetComponent_Component_Type(node.transform:Find("time"), typeof(UILabel)).text = this:GetTimeString(info.time)
                CommonDefs.GetComponent_Component_Type(node.transform:Find("rank"), typeof(UILabel)).text = tostring((i + 1))

                if info.playerId == CClientMainPlayer.Inst.Id then
                    this:InitSelfInfo(info, i + 1)
                end
                i = i + 1
            end
        end
        this.table:Reposition()
        this.scrollView:ResetPosition()
    end
end
