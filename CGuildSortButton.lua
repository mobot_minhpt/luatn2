-- Auto Generated!!
local CGuildSortButton = import "L10.UI.CGuildSortButton"
local LocalString = import "LocalString"
CGuildSortButton.m_SetSortTipStatus_CS2LuaHook = function (this, Increase) 
    local str = string.gsub(this.m_Label.text, LocalString.GetString("▲"), "")
    str = string.gsub(str, LocalString.GetString("▼"), "")
    if Increase then
        this.m_Label.text = str .. LocalString.GetString("▲")
    else
        this.m_Label.text = str .. LocalString.GetString("▼")
    end
end
