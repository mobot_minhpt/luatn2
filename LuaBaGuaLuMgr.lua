require("common/common_include")

local MessageMgr = import "L10.Game.MessageMgr"

EnumBaGuaLuStatus = {
	eEnd = 1,
	eZhuRu = 2,
	eChouJiang = 3,
	eFinish = 4,
}

LuaBaGuaLuMgr = {}

-- 八卦炉主界面
LuaBaGuaLuMgr.m_BaGuaLuStatus = 0
LuaBaGuaLuMgr.m_BaGuaLuStartTime = 0
LuaBaGuaLuMgr.m_ZhuGuaPosList = nil
LuaBaGuaLuMgr.m_KeGuaPosList = nil

LuaBaGuaLuMgr.m_SelectedDivineIndex = 0

-- 注入界面
LuaBaGuaLuMgr.m_TotalZhuRu = 0
LuaBaGuaLuMgr.m_PlayerZhuGua = 0
LuaBaGuaLuMgr.m_PlayerKeGua = 0

LuaBaGuaLuMgr.m_CurrentPosTotalZhuRu = 0
LuaBaGuaLuMgr.m_CurrentPosZhuRuInfos = nil

-- 抽奖结果
LuaBaGuaLuMgr.m_KeGuaResult = 0
LuaBaGuaLuMgr.m_ZhuGuaResult = 0

-- 解卦相关
LuaBaGuaLuMgr.m_DivineResult = 0
LuaBaGuaLuMgr.m_winnerId = 0
LuaBaGuaLuMgr.m_WinnerName = ""
LuaBaGuaLuMgr.m_WinnerAward = 0

function LuaBaGuaLuMgr.ZhuRuSelectedDivine(index)
 	LuaBaGuaLuMgr.m_SelectedDivineIndex = index
 	Gac2Gas.RequestOpenBaGuaLuPos(index)
end

function LuaBaGuaLuMgr.ShowJieGua(result, winnerId, winnerName, winnerAward)
	LuaBaGuaLuMgr.m_DivineResult = result
	LuaBaGuaLuMgr.m_winnerId = winnerId
	LuaBaGuaLuMgr.m_WinnerName = winnerName
	LuaBaGuaLuMgr.m_WinnerAward = winnerAward
	CUIManager.ShowUI(CLuaUIResources.BaGuaLuJieGuaWnd)
end

function LuaBaGuaLuMgr.ShowBaGuaLuWnd(bagualuStatus, playStartTime, needOpen)
 	LuaBaGuaLuMgr.m_BaGuaLuStatus = bagualuStatus
 	LuaBaGuaLuMgr.m_BaGuaLuStartTime = playStartTime

 	if bagualuStatus == EnumBaGuaLuStatus.eZhuRu then
 		LuaBaGuaLuMgr.m_ZhuGuaPosList = {}
 		LuaBaGuaLuMgr.m_KeGuaPosList = {}
 	end

 	if not needOpen then
 		g_ScriptEvent:BroadcastInLua("UpdateBaGuaLuPlayStatus")
 	else
 		if not CUIManager.IsLoaded(CLuaUIResources.BaGuaLuWnd) then
 			CUIManager.ShowUI(CLuaUIResources.BaGuaLuWnd)
 		end
 	end
end

------------------------- Gas2Gac -------------------------

-- 打开界面和注入成功之后的回调
function Gas2Gac.RequestOpenBaGuaLuPosDone(posIndex, totalZhuRu, playerZhuGua, playerKeGua)

	if posIndex ~= LuaBaGuaLuMgr.m_SelectedDivineIndex then
		return
	end
	LuaBaGuaLuMgr.m_TotalZhuRu = totalZhuRu
	LuaBaGuaLuMgr.m_PlayerZhuGua = playerZhuGua
	LuaBaGuaLuMgr.m_PlayerKeGua = playerKeGua
	if CUIManager.IsLoaded(CLuaUIResources.BaGuaLuLingQiZhuRuWnd) then
		g_ScriptEvent:BroadcastInLua("RequestOpenBaGuaLuPosDone")
	else
		CUIManager.ShowUI(CLuaUIResources.BaGuaLuLingQiZhuRuWnd)
	end
	
end

-- 仅用来同步他人的消息
function Gas2Gac.SyncBaGuaLuZhuRuInfo(posIndex, zhuRuInfo_U, lingqi)

	local zhuRuList = MsgPackImpl.unpack(zhuRuInfo_U)
	if not zhuRuList then return end

	LuaBaGuaLuMgr.m_CurrentPosTotalZhuRu = lingqi
	LuaBaGuaLuMgr.m_CurrentPosZhuRuInfos = {}

	for i = 0, zhuRuList.Count - 1 do
		local info = zhuRuList[i]
		local playerId = tonumber(info[0])

		if playerId ~= CClientMainPlayer.Inst.Id then
			table.insert(LuaBaGuaLuMgr.m_CurrentPosZhuRuInfos, {
			PlayerId = playerId,
			PlayerName = info[1],
			LingQi = tonumber(info[2]),
			IsZhuGua = info[3],
			})
		end
	end

	g_ScriptEvent:BroadcastInLua("SyncBaGuaLuZhuRuInfo")
end

function Gas2Gac.SyncBaGuaLuPlayStatus(playStatus, playStatusStartTime)

	LuaBaGuaLuMgr.ShowBaGuaLuWnd(playStatus, playStatusStartTime, false)
end

function Gas2Gac.SyncBaGuaLuOpenInfo(playStatus, playStatusStartTime)
	-- 如果打开的时候在抽奖，提示玩家
	if playStatus == EnumBaGuaLuStatus.eChouJiang then
		MessageMgr.Inst:ShowMessage("BAGUALU_IS_SHOUJIANG", {})
	end
	LuaBaGuaLuMgr.ShowBaGuaLuWnd(playStatus, playStatusStartTime, true)
end

function Gas2Gac.BaGuaLuFinishChouJiang(zhuGuaIndex, keGuaIndex)
	LuaBaGuaLuMgr.m_KeGuaResult = keGuaIndex
	LuaBaGuaLuMgr.m_ZhuGuaResult = zhuGuaIndex
	g_ScriptEvent:BroadcastInLua("BaGuaLuFinishChouJiang", zhuGuaIndex, keGuaIndex)
end

-- 打开八卦解卦界面
function Gas2Gac.BaGuaLuShowResult(winnerId, winnerName, winnerAward)
	if CUIManager.IsLoaded(CLuaUIResources.BaGuaLuWnd) then
		local divineResultId = (LuaBaGuaLuMgr.m_KeGuaResult - 1) * 8 + LuaBaGuaLuMgr.m_ZhuGuaResult
		LuaBaGuaLuMgr.ShowJieGua(divineResultId, winnerId, winnerName, winnerAward)
	end
end

-- 同步选中的主卦和客卦的位置
function Gas2Gac.SyncPlayerTotalZhuRuInfo(zhuGuaPosTblUD, keGuaPosTblUD)
	local zhuGuaList = MsgPackImpl.unpack(zhuGuaPosTblUD)
	local keGuaList = MsgPackImpl.unpack(keGuaPosTblUD)

	if not zhuGuaList or not keGuaList then return end

	LuaBaGuaLuMgr.m_ZhuGuaPosList = {}
	LuaBaGuaLuMgr.m_KeGuaPosList = {}

	for i = 0, zhuGuaList.Count - 1 do
		table.insert(LuaBaGuaLuMgr.m_ZhuGuaPosList, zhuGuaList[i])
	end

	for i = 0, keGuaList.Count - 1 do
		table.insert(LuaBaGuaLuMgr.m_KeGuaPosList, keGuaList[i])
	end
	g_ScriptEvent:BroadcastInLua("SyncPlayerTotalZhuRuInfo")
end