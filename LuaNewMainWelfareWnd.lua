local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CChargeWnd = import "L10.UI.CChargeWnd"
local CPropertyItem = import "L10.Game.CPropertyItem"
local CChargeMgr = import "L10.Game.CChargeMgr"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local CWelfareMgr = import "L10.UI.CWelfareMgr"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local Item_Item = import "L10.Game.Item_Item"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CSigninMgr = import "L10.Game.CSigninMgr"
local CPayMgr = import "L10.Game.CPayMgr"
local SdkU3d = import "NtUniSdk.Unity3d.SdkU3d"

LuaNewMainWelfareWnd = class()
RegistClassMember(LuaNewMainWelfareWnd, "m_Grid")
RegistClassMember(LuaNewMainWelfareWnd, "m_BigMonthCard")
RegistClassMember(LuaNewMainWelfareWnd, "m_SignIn")
RegistClassMember(LuaNewMainWelfareWnd, "m_MonthCard")

RegistClassMember(LuaNewMainWelfareWnd, "m_DiscountTick")
RegistClassMember(LuaNewMainWelfareWnd, "m_MonthCardHintTick")


function LuaNewMainWelfareWnd:Awake()
    self.m_Grid = self.transform:Find("Grid"):GetComponent(typeof(UIGrid))
    self.m_BigMonthCard = self.transform:Find("Grid/BigMonthCard")
    self.m_SignIn = self.transform:Find("Grid/SignIn")
    self.m_MonthCard = self.transform:Find("Grid/MonthCard")
end

function LuaNewMainWelfareWnd:OnEnable()
    self:InitAll()

    g_ScriptEvent:AddListener("ItemPropUpdated", self, "InitAll")
    CChargeMgr.EnableBuyMonthCardForFriend = false
end

function LuaNewMainWelfareWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ItemPropUpdated", self, "InitAll")
    CChargeMgr.EnableBuyMonthCardForFriend = true

    UnRegisterTick(self.m_DiscountTick)
    self.m_DiscountTick = nil
    UnRegisterTick(self.m_MonthCardHintTick)
    self.m_MonthCardHintTick = nil
end

function LuaNewMainWelfareWnd:ShowAChu(label)
    UnRegisterTick(self.m_MonthCardHintTick)

    local inPromotion, time1, time2, msg1, msg2
    if LuaWelfareMgr.IsOpenBigMonthCard and CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.BigMonthCardInfo.BuyTime == 0 then
        time1, msg1, time2, msg2 = string.match(Charge_ChargeSetting.GetData("BigMonthCardDiscountAchuHint").Value, "(.+),(.+);(.+),(.+);")
        time1 = CServerTimeMgr.ConvertTimeStampToZone8Time(CServerTimeMgr.Inst:GetTimeStampByStr(time1))
        time2 = CServerTimeMgr.ConvertTimeStampToZone8Time(CServerTimeMgr.Inst:GetTimeStampByStr(time2))
    end
    local hints
    local cur = 1

    local tickFunc = function()
        if inPromotion ~= false then
            local now = CServerTimeMgr.Inst:GetZone8Time()
            if now:CompareTo(time1) < 0 then
                inPromotion = true
                label.text = g_MessageMgr:FormatMessage(msg1)
            elseif now:CompareTo(time2) < 0 then
                inPromotion = true
                label.text = g_MessageMgr:FormatMessage(msg2)
            else
                hints = LuaWelfareMgr:GetMonthCardHintPlayList()
                inPromotion = false
            end
        end
        if inPromotion then
            label.transform.parent.gameObject:SetActive(true)
            return 
        end
        
        if hints and #hints > 0 then
            label.transform.parent.gameObject:SetActive(true)
            local curHint = hints[cur]
            label.text = curHint.content
            if curHint.tick < curHint.interval then
                curHint.tick = curHint.tick + 1
            else
                if cur == #hints then
                    for i = 1, cur do
                        hints[i].tick = 0
                    end
                    cur = 1
                else
                    cur = cur + 1
                end
                curHint = hints[cur]
                label.text = curHint.content
                curHint.tick = curHint.tick + 1
            end
        else
            label.transform.parent.gameObject:SetActive(false)
        end
    end
    tickFunc()
    self.m_MonthCardHintTick = RegisterTick(tickFunc, 1000)
end

function LuaNewMainWelfareWnd:InitAll()
    self.m_SignIn.gameObject:SetActive(false)
    self.m_MonthCard.gameObject:SetActive(false)
    self.m_BigMonthCard.gameObject:SetActive(false)

    if CommonDefs.IS_VN_CLIENT and CommonDefs.IsInMobileDevice() then
        local monthCardIdList = {1, LuaWelfareMgr.BigMonthCardId}
        local pidStr = ""
        for i = 1, #monthCardIdList do
            local data = Charge_Charge.GetData(monthCardIdList[i])
            if i ~= 1 then
                pidStr = pidStr .. ","
            end
            pidStr = pidStr .. "\"" .. data.PID .. "\""
        end
        SdkU3d.ntExtendFunc("{\"methodId\":\"vngProductInfo\", \"items\":["..pidStr.."]}")
        g_MessageMgr:ShowMessage('CUSTOM_STRING2',LocalString.GetString(pidStr))
    end
    
    --self:InitSignIn()
    self:InitMonthCard()
    --if LuaWelfareMgr.IsOpenBigMonthCard then
    self:InitBigMonthCard()
    --end

    --self.m_Grid.cellWidth = 580 --LuaWelfareMgr.IsOpenBigMonthCard and 528 or 580
    self.m_Grid:Reposition()
end

function LuaNewMainWelfareWnd:InitSignIn()
    local now = LuaWelfareMgr.ServerDateTime
    if not now then return end

    self.m_SignIn.gameObject:SetActive(true)
    local title = self.m_SignIn:Find("Title"):GetComponent(typeof(UILabel))
    local dtLb = self.m_SignIn:Find("DateTime"):GetComponent(typeof(UILabel))
    local dayLb = self.m_SignIn:Find("DateTime/Day"):GetComponent(typeof(UILabel))
    local resignBtn = self.m_SignIn:Find("ReSignInBtn").gameObject
    local resignRemainLb = self.m_SignIn:Find("ReSignInBtn/Remain"):GetComponent(typeof(UILabel))
    local lunarLb = self.m_SignIn:Find("Lunar"):GetComponent(typeof(UILabel))
    local suitableLb = self.m_SignIn:Find("Lunar/Row1/Content"):GetComponent(typeof(UILabel))
    local luckTendencyLb = self.m_SignIn:Find("Lunar/Row2/Content"):GetComponent(typeof(UILabel))
    local rewardGrid = self.m_SignIn:Find("TodayReward/Grid"):GetComponent(typeof(UIGrid))
    local rewardTemplate = self.m_SignIn:Find("TodayReward/ItemTemplate").gameObject
    rewardTemplate:SetActive(false)
    local signBtn = self.m_SignIn:Find("SignInBtn").gameObject
    local signAlert = self.m_SignIn:Find("SignInBtn/Alert").gameObject
    local progress = self.m_SignIn:Find("Progress"):GetComponent(typeof(UISlider))
    local progressFx = self.m_SignIn:Find("Progress/fx_liuguang (1)").gameObject
    local giftpack = self.m_SignIn:Find("Progress/Giftpack").gameObject
    giftpack:SetActive(true)
    UIEventListener.Get(giftpack).onClick = DelegateFactory.VoidDelegate(function()
        g_MessageMgr:ShowMessage("QIAN_DAO_SURPRISE_GIFTPACK_HINT")
    end)
    local progressLb1 = self.m_SignIn:Find("Progress/Label01"):GetComponent(typeof(UILabel))
    local progressLb2 = self.m_SignIn:Find("Progress/Label02"):GetComponent(typeof(UILabel))

    progressLb1.text = SafeStringFormat3(LocalString.GetString("已累计连续签到%d天"), CSigninMgr.Inst.signedDays)

    local rewardDt = now
    if CSigninMgr.Inst.hasSignedToday then
        rewardDt = now:AddDays(1)
    end
    local dailyQianDao = QianDao_DailyQianDao.GetData(rewardDt.Month * 100 + rewardDt.Day)
    local holidayQianDao = QianDao_HolidayQianDao.GetData(SafeStringFormat3("%04d-%02d-%02d", rewardDt.Year, rewardDt.Month, rewardDt.Day))
    local items = {}
    local hasFeiSheng = CClientMainPlayer.Inst and CClientMainPlayer.Inst.HasFeiSheng
    if holidayQianDao then
        title.gameObject:SetActive(true)
        title.text = holidayQianDao.Name
        local item = hasFeiSheng and holidayQianDao.FeiShengItem or holidayQianDao.Item
        for i = 0, item.Length - 1 do
            table.insert(items, {
                id = item[i][0],
                cnt = item[i][1],
            })
        end
    elseif dailyQianDao then
        title.gameObject:SetActive(false)
        table.insert(items, {
            id = hasFeiSheng and dailyQianDao.FeiShengItemID or dailyQianDao.ItemID,
            cnt = hasFeiSheng and dailyQianDao.FeiShengItemCount or dailyQianDao.Count,
        })
    end
    if CSigninMgr.Inst.signedDays % 7 == 0 and CSigninMgr.Inst.signedDays > 0 then
        progress.value = 1
        progressFx:SetActive(true)
        progressLb2.text = "7/7"
    else
        if CSigninMgr.Inst.signedDays % 7 == 6 then
            table.insert(items, {
                id = QianDao_Setting.GetData().SevenDaysGiftId,
                cnt = 1,
            })
        end
        progress.value = (CSigninMgr.Inst.signedDays % 7) / 7
        progressFx:SetActive(false)
        progressLb2.text = (CSigninMgr.Inst.signedDays % 7).."/7"
    end
    
    dtLb.text = now.Year..LocalString.GetString("年")..now.Month..LocalString.GetString("月")
    dayLb.text = SafeStringFormat3("%02d", now.Day)
    local lunarCalendar = LuaWelfareMgr:GetLunarCalendar(now.Year, now.Month, now.Day)

    local lunarDict = { 
        { LocalString.GetString("一"), LocalString.GetString("二"), LocalString.GetString("三"), LocalString.GetString("四"), LocalString.GetString("五"), LocalString.GetString("六"), LocalString.GetString("七"), LocalString.GetString("八"), LocalString.GetString("九"), LocalString.GetString("十"), LocalString.GetString("十一"), LocalString.GetString("腊") },
        { LocalString.GetString("十"), LocalString.GetString("廿"), LocalString.GetString("三") },
    }
    lunarDict[1][0] = LocalString.GetString("日")
    lunarDict[2][0] = LocalString.GetString("初")
    local lunarDay
    if lunarCalendar.day == 10 then lunarDay = LocalString.GetString("初十")
    elseif lunarCalendar.day == 20 then lunarDay = LocalString.GetString("二十")
    elseif lunarCalendar.day == 30 then lunarDay = LocalString.GetString("三十") 
    else lunarDay = lunarDict[2][math.floor(lunarCalendar.day / 10)]..lunarDict[1][lunarCalendar.day % 10] end
    lunarLb.text = LocalString.GetString(lunarCalendar.leap and LocalString.GetString("闰") or "")..SafeStringFormat3(LocalString.GetString("%s月%s"), lunarDict[1][lunarCalendar.month], lunarDay)..LocalString.GetString(" 星期")..lunarDict[1][EnumToInt(now.DayOfWeek)]
    suitableLb.text = LuaWelfareMgr:GetDailySuitable()
    luckTendencyLb.text = LuaWelfareMgr:GetDailyLuckTendency()

    signBtn.transform:Find("Label"):GetComponent(typeof(UILabel)).text = CSigninMgr.Inst.hasSignedToday and LocalString.GetString("今日已签") or LocalString.GetString("签到")
    CUICommonDef.SetActive(signBtn, not CSigninMgr.Inst.hasSignedToday, true)
    UIEventListener.Get(signBtn).onClick = DelegateFactory.VoidDelegate(function()
        Gac2Gas.RequestQianDao_New()
    end)
    signAlert:SetActive(not CSigninMgr.Inst.hasSignedToday)
    self.m_SignIn:Find("TodayReward/Label"):GetComponent(typeof(UILabel)).text = CSigninMgr.Inst.hasSignedToday and LocalString.GetString("明日奖励") or LocalString.GetString("今日奖励")

    Extensions.RemoveAllChildren(rewardGrid.transform)
    for _, item in ipairs(items) do
        local go = NGUITools.AddChild(rewardGrid.gameObject, rewardTemplate)
        go:SetActive(true)
        self:InitRewardItem(go.transform, item.id, item.cnt)
    end
    rewardGrid:Reposition()

    local remain = CSigninMgr.Inst.retroactiveTotalTimes - CSigninMgr.Inst.retroactiveUsedTimes
    resignRemainLb.text = LocalString.GetString("余")..remain..LocalString.GetString("次")
    UIEventListener.Get(resignBtn).onClick = DelegateFactory.VoidDelegate(function()
        if remain > 0 and CSigninMgr.Inst.nextBuQianTime > 0 then
            local dt = CServerTimeMgr.ConvertTimeStampToZone8Time(CSigninMgr.Inst.nextBuQianTime)
            LuaReSignInConfirmWnd.s_Desc = SafeStringFormat3(LocalString.GetString("是否补签领取%s月%s日的签到奖励"), dt.Month, dt.Day)

            local dailyQianDao = QianDao_DailyQianDao.GetData(dt.Month * 100 + dt.Day)
            local holidayQianDao = QianDao_HolidayQianDao.GetData(SafeStringFormat3("%04d-%02d-%02d", dt.Year, dt.Month, dt.Day))
            local items = {}
            local hasFeiSheng = CClientMainPlayer.Inst and CClientMainPlayer.Inst.HasFeiSheng
            if holidayQianDao then
                local item = hasFeiSheng and holidayQianDao.FeiShengItem or holidayQianDao.Item
                for i = 0, item.Length - 1 do
                    table.insert(items, {
                        id = item[i][0],
                        cnt = item[i][1],
                    })
                end
            elseif dailyQianDao then
                table.insert(items, {
                    id = hasFeiSheng and dailyQianDao.FeiShengItemID or dailyQianDao.ItemID,
                    cnt = hasFeiSheng and dailyQianDao.FeiShengItemCount or dailyQianDao.Count,
                })
            end
            if CSigninMgr.Inst.signedDays % 7 == 6 then
                table.insert(items, {
                    id = QianDao_Setting.GetData().SevenDaysGiftId,
                    cnt = 1,
                })
                LuaReSignInConfirmWnd.s_Desc = LuaReSignInConfirmWnd.s_Desc..LocalString.GetString("及连续七日签到惊喜礼盒")
            end

            LuaReSignInConfirmWnd.s_Items = items
            CUIManager.ShowUI(CLuaUIResources.ReSignInConfirmWnd)
        else
            Gac2Gas.RequestBuQianDao_New()
        end
    end)
end

function LuaNewMainWelfareWnd:AdjustRebatePosition(labelComp)
    if CommonDefs.IS_VN_CLIENT then
        if LocalString.language == "cn" then
            labelComp.transform.localPosition = Vector3(0, 40, 0)
        elseif LocalString.language == "vn" then
            labelComp.transform.localPosition = Vector3(0, -36, 0)
        elseif LocalString.language == "th" then
            labelComp.transform.localPosition = Vector3(0, -36, 0)
        elseif LocalString.language == "ina" then
            labelComp.transform.localPosition = Vector3(0, 57, 0)
        elseif LocalString.language == "en" then
            labelComp.transform.localPosition = Vector3(0, 40, 0)
        end
    end
end

function LuaNewMainWelfareWnd:InitMonthCard()
    self.m_MonthCard.gameObject:SetActive(true)
    local achu = self.m_MonthCard:Find("Hint").gameObject
    local achuLabel = self.m_MonthCard:Find("Hint/Label"):GetComponent(typeof(UILabel))
    local lbColor = "[733620]"
    local fgColor = "CC975B"
    local fg2Color = "FF7070"
    local progress = self.m_MonthCard:Find("Progress"):GetComponent(typeof(UISlider))
    local returnHint = self.m_MonthCard:Find("Progress/Label01"):GetComponent(typeof(UILabel))
    local progressLb = self.m_MonthCard:Find("Progress/Label02"):GetComponent(typeof(UILabel))
    local progressFg = self.m_MonthCard:Find("Progress/Foreground"):GetComponent(typeof(UISprite))
    local getBtn = self.m_MonthCard:Find("GetBtn").gameObject
    local getBtnLb = self.m_MonthCard:Find("GetBtn/Label"):GetComponent(typeof(UILabel))
    local getBtnAlert = self.m_MonthCard:Find("GetBtn/Alert").gameObject
    local giftBtn = self.m_MonthCard:Find("GiftBtn").gameObject
    local rebate = self.m_MonthCard:Find("Return/Label"):GetComponent(typeof(UILabel))
    self:AdjustRebatePosition(rebate)
    
    local less10DaysBtns = self.m_MonthCard:Find("Less10DaysBtns").gameObject
    local less10DaysGetBtn = self.m_MonthCard:Find("Less10DaysBtns/GetBtn").gameObject
    local less10DaysGetBtnLb = self.m_MonthCard:Find("Less10DaysBtns/GetBtn/Label"):GetComponent(typeof(UILabel))
    local less10DaysGetBtnAlert = self.m_MonthCard:Find("Less10DaysBtns/GetBtn/Alert").gameObject
    local less10DaysChargeBtn = self.m_MonthCard:Find("Less10DaysBtns/ChargeBtn").gameObject
    local buyGetGrid = self.m_MonthCard:Find("BuyGet/Grid"):GetComponent(typeof(UIGrid))
    local buyGetItemTemplate = self.m_MonthCard:Find("BuyGet/ItemTemplate").gameObject
    buyGetItemTemplate:SetActive(false)
    
    achu:SetActive(not LuaWelfareMgr.IsOpenBigMonthCard)
    giftBtn:SetActive(CSwitchMgr.EnableBuyMonthCardForFriend)
    UIEventListener.Get(giftBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        local vipLevel = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.Vip.Level or 0
        if vipLevel < 6 then
            g_MessageMgr:ShowMessage("Yueka_VIP_Not_Enough", vipLevel)
            return
        end
        CChargeMgr.Inst:BuyMonthCardForFriend()
    end)
    UIEventListener.Get(less10DaysChargeBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        CWelfareMgr.SetMonthCardLastCheckTime()
        EventManager.Broadcast(EnumEventType.OnMonthCardAlertStatusChange)
        CShopMallMgr.ShowChargeWnd()
    end)

    Extensions.RemoveAllChildren(buyGetGrid.transform)
    local items = Charge_ChargeSetting.GetData("MonthCardBuyGetItem").Value
    for item in string.gmatch(items, "([^;]+)") do
		for itemId, count in string.gmatch(item, "([%d-]+),([%d-]+)") do
            itemId = tonumber(itemId)
            count = tonumber(count)
			local go = NGUITools.AddChild(buyGetGrid.gameObject, buyGetItemTemplate)
            go:SetActive(true)
            self:InitRewardItem(go.transform,itemId, count)
        end
	end
    buyGetGrid:Reposition()

    rebate.text = Charge_ChargeSetting.GetData("MonthCardRebateFactor").Value
    returnHint.text = Charge_ChargeSetting.GetData("MonthCardReturnHint").Value

    local info = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.MonthCard
	if info then
        if not LuaWelfareMgr.IsOpenBigMonthCard then
            self:ShowAChu(achuLabel)
        end
        if info.BuyTime ~= 0 then
            local today = CPropertyItem.IsSamePeriod(info.LastReturnTime, "d")
            local leftDay = CChargeWnd.MonthCardLeftDay(info)
            if leftDay > 0 then 
                progress.value = leftDay / 30
                local fmtStr = lbColor..LocalString.GetString("剩余")..(leftDay <= 10 and "["..fg2Color.."]%d[-]" or "%d").."/%d"..LocalString.GetString("天") 
                progressLb.color = Color.white
                progressLb.text = SafeStringFormat3(fmtStr, math.max(leftDay, 0), 30)
                progressFg.color = NGUIText.ParseColor24(leftDay<=10 and fg2Color or fgColor, 0)
                getBtn:SetActive(leftDay>10)
                less10DaysBtns:SetActive(leftDay<=10)
                local btn = leftDay > 10 and getBtn or less10DaysGetBtn
                local label = leftDay > 10 and getBtnLb or less10DaysGetBtnLb
                local alert = leftDay > 10 and getBtnAlert or less10DaysGetBtnAlert
                label.text = today and LocalString.GetString("今日已领") or LocalString.GetString("领 取")
                CUICommonDef.SetActive(btn, not today, true)
                alert:SetActive(not today)
                UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(function(go)
                    Gac2Gas.RequestGetMonthCardReturn()
                end)

                return
            end
        end
        progress.value = 0
        progressLb.color = Color.white
        progressLb.text = lbColor..SafeStringFormat3(LocalString.GetString("剩余").."["..fg2Color.."]%d[-]/%d"..LocalString.GetString("天"), 0, 30)
        progressFg.color = NGUIText.ParseColor24(fgColor, 0)
        getBtn:SetActive(true)
        less10DaysBtns:SetActive(false)
        local data = Charge_Charge.GetData(1)
        if CommonDefs.IS_CN_CLIENT then
            getBtnLb.text = data.Price..LocalString.GetString("元购买")
        elseif CommonDefs.IS_VN_CLIENT then
            if CommonDefs.Is_PC_PLATFORM() or CommonDefs.Is_UNITY_STANDALONE_WIN() then
                local originalShowPrice = data.ShowPrice
                local priceList = g_LuaUtil:StrSplit(originalShowPrice, ";")
                LuaSEASdkMgr:AddProductPrice(data.PID, priceList[LocalString.languageId+1])
            end
            local showPrice = LuaSEASdkMgr.cachePriceData and (LuaSEASdkMgr.cachePriceData[data.PID] and LuaSEASdkMgr.cachePriceData[data.PID]) or ""
            getBtnLb.text = showPrice
            if showPrice == "" then
                RegisterTickOnce(function ()
                    showPrice = LuaSEASdkMgr.cachePriceData and (LuaSEASdkMgr.cachePriceData[data.PID] and LuaSEASdkMgr.cachePriceData[data.PID]) or data.ShowPrice
                    if not CommonDefs.IsNull(getBtnLb) then
                        getBtnLb.text = showPrice
                    end
                end, 300)
            end
        else
            getBtnLb.text = data.ShowPrice
        end
        getBtnAlert:SetActive(false)
        UIEventListener.Get(getBtn).onClick = DelegateFactory.VoidDelegate(function(go)
            CChargeMgr.Inst:BuyMonthCardForMyself()
        end)
	end
end

function LuaNewMainWelfareWnd:InitBigMonthCard()
    self.m_BigMonthCard.gameObject:SetActive(true)
    local achu = self.m_BigMonthCard:Find("Hint").gameObject
    local achuLabel = self.m_BigMonthCard:Find("Hint/Label"):GetComponent(typeof(UILabel))
    local lbColor = "[361877]"
    local fgColor = "BA88DB"
    local fg2Color = "FF7070"
    local cdColor = "FFE0B0"
    local progress = self.m_BigMonthCard:Find("Progress"):GetComponent(typeof(UISlider))
    local returnHint = self.m_BigMonthCard:Find("Progress/Label01"):GetComponent(typeof(UILabel))
    local progressLb = self.m_BigMonthCard:Find("Progress/Label02"):GetComponent(typeof(UILabel))
    local progressFg = self.m_BigMonthCard:Find("Progress/Foreground"):GetComponent(typeof(UISprite))
    local getBtn = self.m_BigMonthCard:Find("GetBtn").gameObject
    local getBtnLb = self.m_BigMonthCard:Find("GetBtn/Label"):GetComponent(typeof(UILabel))
    local getBtnAlert = self.m_BigMonthCard:Find("GetBtn/Alert").gameObject
    local giftBtn = self.m_BigMonthCard:Find("GiftBtn").gameObject
    local rebate = self.m_BigMonthCard:Find("Return/Label"):GetComponent(typeof(UILabel))
    self:AdjustRebatePosition(rebate)
    
    local less10DaysBtns = self.m_BigMonthCard:Find("Less10DaysBtns").gameObject
    local less10DaysGetBtn = self.m_BigMonthCard:Find("Less10DaysBtns/GetBtn").gameObject
    local less10DaysGetBtnLb = self.m_BigMonthCard:Find("Less10DaysBtns/GetBtn/Label"):GetComponent(typeof(UILabel))
    local less10DaysGetBtnAlert = self.m_BigMonthCard:Find("Less10DaysBtns/GetBtn/Alert").gameObject
    local less10DaysChargeBtn = self.m_BigMonthCard:Find("Less10DaysBtns/ChargeBtn").gameObject
    local buyGetGrid = self.m_BigMonthCard:Find("BuyGet/Grid"):GetComponent(typeof(UIGrid))
    local buyGetItemTemplate = self.m_BigMonthCard:Find("BuyGet/ItemTemplate").gameObject
    buyGetItemTemplate:SetActive(false)
    
    local settingBtn = self.m_BigMonthCard:Find("SettingButton").gameObject
    local discountCd = self.m_BigMonthCard:Find("Bottom/Countdown"):GetComponent(typeof(UILabel))
    local discountHint = self.m_BigMonthCard:Find("Bottom/DiscountHint"):GetComponent(typeof(UILabel))

    UIEventListener.Get(settingBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CLuaUIResources.ReceiveGiftPackSettingWnd)
    end)

    local infos = Charge_ChargeSetting.GetData("BigMonthCardDiscountInfo").Value
    local tickFunc = function()
        local rewardtime = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp:GetTempPlayStringData(EnumTempPlayDataKey_lua.eBigMonthCardRewardtime)
        local isDiscount = false
        if not rewardtime then
            local now = CServerTimeMgr.Inst:GetZone8Time()
            for info in string.gmatch(infos, "([^;]+)") do
                local time, days, msg = string.match(info, "(.+),(.+),(.+)")
                time = CServerTimeMgr.ConvertTimeStampToZone8Time(CServerTimeMgr.Inst:GetTimeStampByStr(time))
                if now:CompareTo(time) < 0 then
                    discountHint.transform.parent.gameObject:SetActive(true)
                    discountHint.text = g_MessageMgr:FormatMessage(msg)
                    local cd = CommonDefs.op_Subtraction_DateTime_DateTime(time, now)
                    if cd.TotalDays < 1 then
                        discountCd.text = LocalString.GetString("倒计时：")..cd:ToString("hh\\:mm\\:ss")
                    else
                        discountCd.text = LocalString.GetString("倒计时：")..math.floor(cd.TotalDays)..LocalString.GetString("天")..cd:ToString("hh\\:mm\\:ss")
                    end
                    discountCd.color = NGUIText.ParseColor24(cd.TotalDays > 3 and cdColor or fg2Color, 0)  
                    isDiscount = true
                    break
                end
            end
        end
        if not isDiscount then
            discountHint.transform.parent.gameObject:SetActive(false)
            UnRegisterTick(self.m_DiscountTick)
        end
    end
    tickFunc()
    UnRegisterTick(self.m_DiscountTick)
    self.m_DiscountTick = RegisterTick(function()
        tickFunc()
    end, 1000)
    
    achu:SetActive(LuaWelfareMgr.IsOpenBigMonthCard)
    giftBtn:SetActive(CSwitchMgr.EnableBuyMonthCardForFriend)
    UIEventListener.Get(giftBtn).onClick = DelegateFactory.VoidDelegate(function (go)
        local vipLevel = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.Vip.Level or 0
        if vipLevel < 6 then
            g_MessageMgr:ShowMessage("Big_Yueka_VIP_Not_Enough", vipLevel)
            return
        end
        LuaWelfareMgr.BuyBigMonthCardForFriend()
    end)
    UIEventListener.Get(less10DaysChargeBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        CWelfareMgr.SetMonthCardLastCheckTime()
        EventManager.Broadcast(EnumEventType.OnMonthCardAlertStatusChange)
        CShopMallMgr.ShowChargeWnd()
    end)

    Extensions.RemoveAllChildren(buyGetGrid.transform)
    local items = Charge_ChargeSetting.GetData("BigMonthCardBuyGetItem").Value
    for item in string.gmatch(items, "([^;]+)") do
		for itemId, count in string.gmatch(item, "([%d-]+),([%d-]+)") do
            itemId = tonumber(itemId)
            count = tonumber(count)
			local go = NGUITools.AddChild(buyGetGrid.gameObject, buyGetItemTemplate)
            go:SetActive(true)
            self:InitRewardItem(go.transform,itemId, count)
        end
	end
    buyGetGrid:Reposition()

    rebate.text = Charge_ChargeSetting.GetData("BigMonthCardRebateFactor").Value
    returnHint.text = Charge_ChargeSetting.GetData("BigMonthCardReturnHint").Value

    local info = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.BigMonthCardInfo
	if info then
        if LuaWelfareMgr.IsOpenBigMonthCard then
            self:ShowAChu(achuLabel)
        end
        if info.BuyTime ~= 0 then
            local today = CPropertyItem.IsSamePeriod(info.LastReturnTime, "d")
            local leftDay = LuaWelfareMgr:MonthCardLeftDay(info)
            if leftDay > 0 then 
                progress.value = leftDay / 30
                local fmtStr = lbColor..LocalString.GetString("剩余")..(leftDay <= 10 and "["..fg2Color.."]%d[-]" or "%d").."/%d"..LocalString.GetString("天") 
                progressLb.color = Color.white
                progressLb.text = SafeStringFormat3(fmtStr, math.max(leftDay, 0), 30)
                progressFg.color = NGUIText.ParseColor24(leftDay<=10 and fg2Color or fgColor, 0)
                getBtn:SetActive(leftDay>10)
                less10DaysBtns:SetActive(leftDay<=10)
                local btn = leftDay > 10 and getBtn or less10DaysGetBtn
                local label = leftDay > 10 and getBtnLb or less10DaysGetBtnLb
                local alert = leftDay > 10 and getBtnAlert or less10DaysGetBtnAlert
                label.text = today and LocalString.GetString("今日已领") or LocalString.GetString("领 取")
                CUICommonDef.SetActive(btn, not today, true)
                alert:SetActive(not today)
                UIEventListener.Get(btn).onClick = DelegateFactory.VoidDelegate(function(go)
                    if info.DefaultItemId and info.DefaultItemId ~= 0 then
                        Gac2Gas.RequestGetBigMonthCardDailyGift(info.DefaultItemId)
                    else
                        CUIManager.ShowUI(CLuaUIResources.BigMonthCardSelectItemWnd)
                    end
                end)

                return
            end
        end
        progress.value = 0
        progressLb.color = Color.white
        progressLb.text = lbColor..SafeStringFormat3(LocalString.GetString("剩余").."["..fg2Color.."]%d[-]/%d"..LocalString.GetString("天"), 0, 30)
        progressFg.color = NGUIText.ParseColor24(fgColor, 0)
        getBtn:SetActive(true)
        less10DaysBtns:SetActive(false)
        local data = Charge_Charge.GetData(LuaWelfareMgr.BigMonthCardId)
        if CommonDefs.IS_CN_CLIENT then
            getBtnLb.text = data.Price..LocalString.GetString("元购买")
        elseif CommonDefs.IS_VN_CLIENT then
            if CommonDefs.Is_PC_PLATFORM() or CommonDefs.Is_UNITY_STANDALONE_WIN() then
                local originalShowPrice = data.ShowPrice
                local priceList = g_LuaUtil:StrSplit(originalShowPrice, ";")
                LuaSEASdkMgr:AddProductPrice(data.PID, priceList[LocalString.languageId+1])
            end
            local showPrice = LuaSEASdkMgr.cachePriceData and (LuaSEASdkMgr.cachePriceData[data.PID] and LuaSEASdkMgr.cachePriceData[data.PID]) or ""
            getBtnLb.text = showPrice
            if showPrice == "" then
                RegisterTickOnce(function ()
                    showPrice = LuaSEASdkMgr.cachePriceData and (LuaSEASdkMgr.cachePriceData[data.PID] and LuaSEASdkMgr.cachePriceData[data.PID]) or data.ShowPrice
                    if not CommonDefs.IsNull(getBtnLb) then
                        getBtnLb.text = showPrice
                    end
                end, 300)
            end
        else
            getBtnLb.text = data.ShowPrice
        end
        getBtnAlert:SetActive(false)
        UIEventListener.Get(getBtn).onClick = DelegateFactory.VoidDelegate(function(go)
            LuaWelfareMgr.BuyBigMonthCardForMyself()
        end)
	end
end

function LuaNewMainWelfareWnd:InitRewardItem(trans, itemId, count)
    local iconTexture = trans:Find("IconTexture"):GetComponent(typeof(CUITexture))
    iconTexture:Clear()
    local qualitySprite = trans:Find("QualitySprite"):GetComponent(typeof(UISprite))
    qualitySprite.spriteName = nil
    local amountLabel = trans:Find("AmountLabel"):GetComponent(typeof(UILabel))
    amountLabel.text = nil

    local itemTemplate = Item_Item.GetData(itemId)
    if not itemTemplate then return end

    iconTexture:LoadMaterial(itemTemplate.Icon)
    if count > 1 then
        amountLabel.text = tostring(count)
    end
    qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(itemTemplate, nil, false)

    CommonDefs.AddOnClickListener(trans.gameObject, DelegateFactory.Action_GameObject(function (go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
    end), false)
end
