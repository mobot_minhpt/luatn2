local UIGrid = import "UIGrid"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local UIInput = import "UIInput"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CShiTuMgr = import "L10.Game.CShiTuMgr"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"

LuaShiTuChooseWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShiTuChooseWnd, "ChooseBtn1", "ChooseBtn1", GameObject)
RegistChildComponent(LuaShiTuChooseWnd, "ChooseBtn2", "ChooseBtn2", GameObject)
RegistChildComponent(LuaShiTuChooseWnd, "ChooseBtn3", "ChooseBtn3", GameObject)
RegistChildComponent(LuaShiTuChooseWnd, "QuestionLabel", "QuestionLabel", UILabel)
RegistChildComponent(LuaShiTuChooseWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaShiTuChooseWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaShiTuChooseWnd, "BottomLeftLabel", "BottomLeftLabel", UILabel)
RegistChildComponent(LuaShiTuChooseWnd, "BottomRightLabel", "BottomRightLabel", UILabel)
RegistChildComponent(LuaShiTuChooseWnd, "SubmitButton", "SubmitButton", GameObject)
RegistChildComponent(LuaShiTuChooseWnd, "Input", "Input", UIInput)
RegistChildComponent(LuaShiTuChooseWnd, "SelectTagView", "SelectTagView", GameObject)
RegistChildComponent(LuaShiTuChooseWnd, "TagGrid", "TagGrid", UIGrid)
RegistChildComponent(LuaShiTuChooseWnd, "TagTemplate", "TagTemplate", GameObject)
RegistChildComponent(LuaShiTuChooseWnd, "RefreshButton", "RefreshButton", GameObject)
RegistChildComponent(LuaShiTuChooseWnd, "SelectTagLabel", "SelectTagLabel", UILabel)
RegistChildComponent(LuaShiTuChooseWnd, "SelectTagGrid", "SelectTagGrid", UIGrid)
RegistChildComponent(LuaShiTuChooseWnd, "EmptyTagTemplate", "EmptyTagTemplate", GameObject)
RegistChildComponent(LuaShiTuChooseWnd, "SelectTagTemplate", "SelectTagTemplate", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaShiTuChooseWnd, "m_TotalQuestionCount")
RegistClassMember(LuaShiTuChooseWnd, "m_CurrentQuestionIndex")
RegistClassMember(LuaShiTuChooseWnd, "m_CurrentData")
RegistClassMember(LuaShiTuChooseWnd, "m_RandomTagIdList")
RegistClassMember(LuaShiTuChooseWnd, "m_CurSelectTagList")
RegistClassMember(LuaShiTuChooseWnd, "m_Id2SelectTagBtnMap")

function LuaShiTuChooseWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.ChooseBtn1.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChooseBtn1Click()
	end)

	UIEventListener.Get(self.ChooseBtn2.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChooseBtn2Click()
	end)

	UIEventListener.Get(self.ChooseBtn3.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnChooseBtn3Click()
	end)

	UIEventListener.Get(self.CloseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseButtonClick()
	end)

	UIEventListener.Get(self.SubmitButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSubmitButtonClick()
	end)

	UIEventListener.Get(self.RefreshButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRefreshButtonClick()
	end)

    --@endregion EventBind end
end

function LuaShiTuChooseWnd:Init()
	self.SubmitButton.gameObject:SetActive(false)
	self.Input.gameObject:SetActive(false)
	self.SelectTagView.gameObject:SetActive(false)
	self.TagTemplate.gameObject:SetActive(false)
	self.EmptyTagTemplate.gameObject:SetActive(false)
	self.SelectTagTemplate.gameObject:SetActive(false)
	Extensions.RemoveAllChildren(self.SelectTagGrid.transform)
	self.m_CurSelectTagList = {}
	self.Input.defaultText = CShiTuMgr.Inst.CurrentEnterType == CShiTuMgr.ShiTuChooseType.SHI and LocalString.GetString("点击输入收徒宣言(选填,限制15个汉字)") or LocalString.GetString("点击输入求师宣言(选填,限制15个汉字)")
	self.BottomLeftLabel.gameObject:SetActive(true)
	self.BottomLeftLabel.text = g_MessageMgr:FormatMessage(CShiTuMgr.Inst.CurrentEnterType == CShiTuMgr.ShiTuChooseType.SHI and "ShiTuChooseWnd_ShouTu_ReadMe" or "ShiTuChooseWnd_QiuShi_ReadMe")
	self.TitleLabel.text = CShiTuMgr.Inst.CurrentEnterType == CShiTuMgr.ShiTuChooseType.SHI and LocalString.GetString("我要收徒") or LocalString.GetString("我要求师")
	self.m_TotalQuestionCount = LuaShiTuMgr:GetShiTuChooseWndQuestionCount(CShiTuMgr.Inst.CurrentEnterType)
	LuaShiTuMgr:LoadShiTuChooseWndQuestionList()
	self.m_CurrentQuestionIndex = 0
	self:SetNextQuestion()

	--self:ParseCacheData()
end

function LuaShiTuChooseWnd:SetNextQuestion()
	self.BottomRightLabel.text = SafeStringFormat3(self.m_CurrentQuestionIndex == self.m_TotalQuestionCount and LocalString.GetString("[acf8ff]页数 [ffffff]%d/%d") or LocalString.GetString("[acf8ff]页数 [ffffff]%d/%d"),self.m_CurrentQuestionIndex + 1,self.m_TotalQuestionCount + ((CShiTuMgr.Inst.ShiTuAskStage == 0 and LuaShiTuMgr.m_OpenNewSystem) and 1 or 0))
	if self.m_CurrentQuestionIndex >= self.m_TotalQuestionCount then
		if CShiTuMgr.Inst.ShiTuAskStage == 1 or not LuaShiTuMgr.m_OpenNewSystem then
			self:OnSubmitButtonClick()
			return
		end
		self.ChooseBtn1:SetActive(false)
		self.ChooseBtn2:SetActive(false)
		self.ChooseBtn3:SetActive(false)
		self.BottomLeftLabel.text = ""
		--self.Input.gameObject:SetActive(true)
		self.SubmitButton.gameObject:SetActive(true)
		self:InitSelectTagView()
		self.QuestionLabel.text = LocalString.GetString("添加我的个性化标签")
        return
    end

	local data = self:GetCurData()
	self.m_CurrentData = data
    if data == nil then
        return
    end

	self.QuestionLabel.text = data.Question
	self.ChooseBtn1:SetActive(not System.String.IsNullOrEmpty(data.Option1))
	CommonDefs.GetComponent_Component_Type(self.ChooseBtn1.transform:Find("text"), typeof(UILabel)).text = data.Option1
	self.ChooseBtn2:SetActive(not System.String.IsNullOrEmpty(data.Option2))
	CommonDefs.GetComponent_Component_Type(self.ChooseBtn2.transform:Find("text"), typeof(UILabel)).text = data.Option2
	self.ChooseBtn3:SetActive(not System.String.IsNullOrEmpty(data.Option3))
	CommonDefs.GetComponent_Component_Type(self.ChooseBtn3.transform:Find("text"), typeof(UILabel)).text = data.Option3

	self.m_CurrentQuestionIndex = self.m_CurrentQuestionIndex + 1
end

function LuaShiTuChooseWnd:InitSelectTagView()
	self.SelectTagView.gameObject:SetActive(true)
	Extensions.RemoveAllChildren(self.TagGrid.transform)
	self:GetRandomTagIdList()
	local set = {}
	for i = 1, #self.m_CurSelectTagList do
		set[self.m_CurSelectTagList[i]] = true
	end	
	self.m_Id2SelectTagBtnMap = {}
	for i = 1, #self.m_RandomTagIdList do
		local obj = NGUITools.AddChild(self.TagGrid.gameObject, self.TagTemplate.gameObject)
		obj.gameObject:SetActive(true)
		local id = self.m_RandomTagIdList[i]
		local tagData = ShiTu_Label.GetData(id)
		local type = tagData.type
		local color = NGUIText.ParseColor32(ShiTu_Setting.GetData().LabelColor[type -1],0)
		obj.transform:Find("DeleteButton").gameObject:SetActive(false)
		obj.transform:Find("Label"):GetComponent(typeof(UILabel)).text = tagData.text
		obj.transform:Find("bg_Texture"):GetComponent(typeof(UITexture)).color = color
		local btn = obj:GetComponent(typeof(QnSelectableButton))
		btn:SetSelected(set[id] and true or false,false)
		btn.OnButtonSelected = DelegateFactory.Action_bool(function(b) 
			self:OnTagTemplateSelect(btn, id, b) 
		end)
		self.m_Id2SelectTagBtnMap[id] = btn
	end
	self.TagGrid:Reposition()
	self:InitCurSelectTagView()
end

function LuaShiTuChooseWnd:OnTagTemplateSelect(btn, id, select)
	if select then
		if #self.m_CurSelectTagList == 4 then
			g_MessageMgr:ShowMessage("ShiTuChooseWnd_Selected_Failed")
			btn:SetSelected(false, false)
			return
		end
		table.insert(self.m_CurSelectTagList, id)
	else
		for i = 1, #self.m_CurSelectTagList do
			if self.m_CurSelectTagList[i] == id then
				table.remove(self.m_CurSelectTagList, i)
			end
		end	
	end
	self:InitCurSelectTagView()
end

function LuaShiTuChooseWnd:InitCurSelectTagView()
	Extensions.RemoveAllChildren(self.SelectTagGrid.transform)
	for i= 1, #self.m_CurSelectTagList do
		local id = self.m_CurSelectTagList[i]
		local obj = NGUITools.AddChild(self.SelectTagGrid.gameObject, self.SelectTagTemplate.gameObject)
		obj.gameObject:SetActive(true)
		local tagData = ShiTu_Label.GetData(id)
		local type = tagData.type
		local color = NGUIText.ParseColor32(ShiTu_Setting.GetData().LabelColor[type -1],0)	
		obj.transform:Find("DeleteButton").gameObject:SetActive(true)
		obj.transform:Find("Label"):GetComponent(typeof(UILabel)).text = tagData.text
		obj.transform:Find("bg_Texture"):GetComponent(typeof(UITexture)).color = color
		local j = i
		UIEventListener.Get(obj.transform:Find("DeleteButton").gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			local btn = self.m_Id2SelectTagBtnMap[id]
			if btn then
				btn:SetSelected(false, false)
			else
				table.remove(self.m_CurSelectTagList, j)
				self:InitCurSelectTagView()
			end
		end)
	end
	for i = #self.m_CurSelectTagList + 1, 4 do
		local obj = NGUITools.AddChild(self.SelectTagGrid.gameObject, self.EmptyTagTemplate.gameObject)
		obj.gameObject:SetActive(true)
	end
	self.SelectTagGrid:Reposition()
	self.SelectTagLabel.text = SafeStringFormat3("%d/4", #self.m_CurSelectTagList)
end

function LuaShiTuChooseWnd:GetRandomTagIdList()
	local tagIdArr = {}
	local set = {}
	for i = 1, #self.m_CurSelectTagList do
		set[self.m_CurSelectTagList[i]] = true
	end	
	ShiTu_Label.Foreach(function (k,v)
		if not set[k] and CommonDefs.HashSetContains(v.belong, typeof(Int32), (CShiTuMgr.Inst.CurrentEnterType == CShiTuMgr.ShiTuChooseType.SHI) and 1 or 2) then
			table.insert(tagIdArr, k)
		end
	end)
	self.m_RandomTagIdList = {}
	for i = 1, 12 do
		local r = math.random(1,#tagIdArr)
		table.insert(self.m_RandomTagIdList, tagIdArr[r])
		table.remove(tagIdArr,r)
	end
end

function LuaShiTuChooseWnd:GetCurData()
	local data = nil
	if CShiTuMgr.Inst.ShiTuAskStage == 1 and CShiTuMgr.EnableAIRecommend then
        if CShiTuMgr.Inst.ExtraQuestionList.Count <= self.m_CurrentQuestionIndex then
            return data
        end
        data = CShiTuMgr.Inst.ExtraQuestionList[self.m_CurrentQuestionIndex]
    elseif CShiTuMgr.Inst.ShiTuAskStage == 0 then
        if CShiTuMgr.Inst.QuestionList.Count <= self.m_CurrentQuestionIndex then
            return data
        end
        data = CShiTuMgr.Inst.QuestionList[self.m_CurrentQuestionIndex]
    end
	return data
end

function LuaShiTuChooseWnd:GetCacheDataKey()
	local key = CShiTuMgr.Inst.CurrentEnterType == CShiTuMgr.ShiTuChooseType.SHI and "ShiTuChooseWnd_ShouTuData_" or "ShiTuChooseWnd_QiuShiData_" 
	return key .. tostring(CClientMainPlayer.Inst.Id)
end

function LuaShiTuChooseWnd:ParseCacheData()
	local key = self:GetCacheDataKey()
	local res = PlayerPrefs.GetString(key)
	if String.IsNullOrEmpty(res) then return end
	local arr = g_LuaUtil:StrSplit(res,",")
	for key, val in pairs(arr) do
		if not String.IsNullOrEmpty(val) then 
			CommonDefs.DictSet(CShiTuMgr.Inst.SaveResultValue, typeof(Int32), key, typeof(Int32),tonumber(val))
		end
	end
	for i = 2,#arr do
		self:SetNextQuestion()
	end
end

function LuaShiTuChooseWnd:SaveCacheData()
	local key = self:GetCacheDataKey()
	local arr = {}
	CommonDefs.DictIterate(CShiTuMgr.Inst.SaveResultValue, DelegateFactory.Action_object_object(function(index,val)
		arr[index] = val
	end))
	if #arr == 0 then return end
	local res = ""
	for i = 1,#arr do
		res = res .. arr[i] .. ","
	end
	PlayerPrefs.SetString(key, res)
	local tagStr = ""
	for k, v in pairs(self.m_CurSelectTagList) do
		if v then
			tagStr = tagStr .. k .. ","
		end
	end
	PlayerPrefs.SetString(key .. "Tag", tagStr)
end

function LuaShiTuChooseWnd:ClearCacheData()
	local key = self:GetCacheDataKey()
	PlayerPrefs.SetString(key, "")
end

--@region UIEvent

function LuaShiTuChooseWnd:OnChooseBtnClick(index)
	local data = self.m_CurrentData
	if CShiTuMgr.Inst.ShiTuAskStage == 0 then
		CommonDefs.DictSet(CShiTuMgr.Inst.SaveResultValue, typeof(Int32), data.ID, typeof(Int32), index)
	elseif CShiTuMgr.Inst.ShiTuAskStage == 1 then
		CommonDefs.DictSet(CShiTuMgr.Inst.SaveExtraResultValue, typeof(Int32), data.ID, typeof(Int32), index)
	end
	self:SetNextQuestion()
end

function LuaShiTuChooseWnd:OnChooseBtn1Click()
	self:OnChooseBtnClick(1)
end

function LuaShiTuChooseWnd:OnChooseBtn2Click()
	self:OnChooseBtnClick(2)
end

function LuaShiTuChooseWnd:OnChooseBtn3Click()
	self:OnChooseBtnClick(3)
end

function LuaShiTuChooseWnd:OnCloseButtonClick()
	self:SaveCacheData()
	CShiTuMgr.Inst:ClearData()
	CUIManager.CloseUI(CUIResources.ShiTuChooseWnd)
	LuaShiTuMgr.m_IsShouTuWaiMen = false
end

function LuaShiTuChooseWnd:OnSubmitButtonClick()
	local text = self.Input.text
	if not CWordFilterMgr.Inst:CheckLinkAndMatch(text) then
		g_MessageMgr:ShowMessage("Speech_Violation")		
		return
	end
	if CShiTuMgr.Inst.ShiTuAskStage == 0 then
		LuaShiTuMgr.m_ShiTuChooseWndTagList = self.m_CurSelectTagList
	end
	self:SaveCacheData()
	CUIManager.ShowUI(CUIResources.ShiTuResultWnd)
	CUIManager.CloseUI(CUIResources.ShiTuChooseWnd)
end


function LuaShiTuChooseWnd:OnRefreshButtonClick()
	self:InitSelectTagView()
end


--@endregion UIEvent
