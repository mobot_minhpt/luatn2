local MessageWndManager = import "L10.UI.MessageWndManager"
local UIPanel = import "UIPanel"
local TweenAlpha = import "TweenAlpha"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"

LuaHeShengZiJinWnd = class()

RegistChildComponent(LuaHeShengZiJinWnd, "m_CloseButton","CloseButton",GameObject)
RegistChildComponent(LuaHeShengZiJinWnd, "m_HeartAni","HeartAni",TweenAlpha)
RegistChildComponent(LuaHeShengZiJinWnd, "m_HeadAni","HeadAni",TweenAlpha)
RegistChildComponent(LuaHeShengZiJinWnd, "m_HandAni","HandAni",TweenAlpha)
RegistChildComponent(LuaHeShengZiJinWnd, "m_Heart","Heart",GameObject)
RegistChildComponent(LuaHeShengZiJinWnd, "m_Head","Head",GameObject)
RegistChildComponent(LuaHeShengZiJinWnd, "m_Hand","Hand",GameObject)
RegistChildComponent(LuaHeShengZiJinWnd, "m_HeartFx","HeartFx",CUIFx)
RegistChildComponent(LuaHeShengZiJinWnd, "m_HeadFx","HeadFx",CUIFx)
RegistChildComponent(LuaHeShengZiJinWnd, "m_HandFx","HandFx",CUIFx)
RegistChildComponent(LuaHeShengZiJinWnd, "m_BottomLabel","BottomLabel",UILabel)

RegistClassMember(LuaHeShengZiJinWnd, "m_Panel")
RegistClassMember(LuaHeShengZiJinWnd, "m_Sign")
RegistClassMember(LuaHeShengZiJinWnd, "m_Finished")
RegistClassMember(LuaHeShengZiJinWnd, "m_Tween")

function LuaHeShengZiJinWnd:Init()
    self.m_Panel = self.gameObject:GetComponent(typeof(UIPanel))
    self.m_Panel.IgnoreIphoneXMargin = true
    self.m_Sign = 0
    UIEventListener.Get(self.m_CloseButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClose()
    end)
    UIEventListener.Get(self.m_Heart.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnHeartClick()
    end)
    UIEventListener.Get(self.m_Head.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnHeadClick()
    end)
    UIEventListener.Get(self.m_Hand.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnHandClick()
    end)
    self.m_BottomLabel.text = g_MessageMgr:FormatMessage("HeShengZiJinWnd_ReadMe")
    self.m_HeartFx:LoadFx("Fx/UI/Prefab/UI_heshengzijin_hudiefeiwu.prefab")
    self.m_HeadFx:LoadFx("Fx/UI/Prefab/UI_heshengzijin_hudiefeiwu.prefab")
    self.m_HandFx:LoadFx("Fx/UI/Prefab/UI_heshengzijin_hudiefeiwu.prefab")
end

function LuaHeShengZiJinWnd:OnClose()
    local msg = g_MessageMgr:FormatMessage("HeShengZiJinWnd_CloseWnd_Confirm")
    MessageWndManager.ShowDelayOKCancelMessage(msg, DelegateFactory.Action(function()
        CUIManager.CloseUI(CLuaUIResources.HeShengZiJinWnd)
    end), nil,3)
end

function LuaHeShengZiJinWnd:OnHeartClick()
    self.m_HeartAni.enabled = true
    self.m_Sign = bit.bor (1, self.m_Sign)
    g_MessageMgr:ShowMessage("HeShengZiJinWnd_ClickHeartTip")
    self.m_HeartFx:LoadFx("Fx/UI/Prefab/UI_heshengzijin_hudiesankai.prefab")
    self:OnTaskFinished()
end

function LuaHeShengZiJinWnd:OnHeadClick()
    self.m_HeadAni.enabled = true
    self.m_Sign = bit.bor (2, self.m_Sign)
    g_MessageMgr:ShowMessage("HeShengZiJinWnd_ClickHeadTip")
    self.m_HeadFx:LoadFx("Fx/UI/Prefab/UI_heshengzijin_hudiesankai.prefab")
    self:OnTaskFinished()
end

function LuaHeShengZiJinWnd:OnHandClick()
    self.m_HandAni.enabled = true
    self.m_Sign = bit.bor (4, self.m_Sign)
    g_MessageMgr:ShowMessage("HeShengZiJinWnd_ClickHandTip")
    self.m_HandFx:LoadFx("Fx/UI/Prefab/UI_heshengzijin_hudiesankai.prefab")
    self:OnTaskFinished()
end

function LuaHeShengZiJinWnd:OnTaskFinished()
    if self.m_Finished then return end
    if self.m_Sign == 7 then
        self.m_Finished = true

        local empty = CreateFromClass(MakeGenericClass(List, Object))
        Gac2Gas.FinishClientTaskEvent(CLuaTaskMgr.m_HeShengZiJinWnd_TaskId,"HeShengZiJinWnd",MsgPackImpl.pack(empty))
        RegisterTickOnce(function ()
            self.m_Tween = LuaTweenUtils.TweenFloat(1 ,0, 1, function(value)
                if self.m_Panel then
                    self.m_Panel.alpha = value
                    if value == 0 then
                        CUIManager.CloseUI(CLuaUIResources.HeShengZiJinWnd)
                    end
                end
            end)
        end,2000)
    end
end

function LuaHeShengZiJinWnd:OnDisable()
    if self.m_Tween then
        LuaTweenUtils.Kill(self.m_Tween, true)
        self.m_Tween = nil
    end
end