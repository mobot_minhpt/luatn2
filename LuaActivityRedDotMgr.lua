local CScheduleMgr = import "L10.Game.CScheduleMgr"
local DateTime = import "System.DateTime"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CDailyScheduleInfo = import "L10.Game.CDailyScheduleInfo"
local CTaskMgr = import "L10.Game.CTaskMgr"
local EnumActivityType = import "L10.Game.EnumActivityType"
local CBiWuDaHuiMgr = import "L10.Game.CBiWuDaHuiMgr"
local UIRedDotClick = import "L10.UI.UIRedDotClick"
local UIRedDotAlert = import "L10.UI.UIRedDotAlert"

if rawget(_G, "LuaActivityRedDotMgr") then
    g_ScriptEvent:RemoveListener("UpdateActivity", LuaActivityRedDotMgr, "OnUpdateActivity")
    g_ScriptEvent:RemoveListener("MainPlayerLevelChange", LuaActivityRedDotMgr, "OnUpdateActivity")
    g_ScriptEvent:RemoveListener("PlayerLogin", LuaActivityRedDotMgr, "OnPlayerLogin")
    g_ScriptEvent:RemoveListener("ShowUIPreDraw", LuaActivityRedDotMgr, "OnChangeView")
end

LuaActivityRedDotMgr = {}

g_ScriptEvent:AddListener("UpdateActivity", LuaActivityRedDotMgr, "OnUpdateActivity")
g_ScriptEvent:AddListener("MainPlayerLevelChange", LuaActivityRedDotMgr, "OnUpdateActivity")
g_ScriptEvent:AddListener("PlayerLogin", LuaActivityRedDotMgr, "OnPlayerLogin")
g_ScriptEvent:AddListener("ShowUIPreDraw", LuaActivityRedDotMgr, "OnChangeView")


LuaEnumRedDotType = {
    Default = 0, -- id = Task_JieRiRedDot.Id
    JieRi = 1, -- id = Task_JieRiGroup.ID
    Schedule = 2, -- id = Task_Schedule.ID
}

LuaActivityRedDotMgr.m_UpdateTick = nil
LuaActivityRedDotMgr.UpdateInterval = 1 -- in seconds

LuaActivityRedDotMgr.m_CachedRedDot = {}
LuaActivityRedDotMgr.m_CachedCheckFunc = {}
LuaActivityRedDotMgr.m_CachedCheckTime = {}

LuaActivityRedDotMgr.m_Type2RedDot = {}
LuaActivityRedDotMgr.m_Wnd2RedDot = {}

LuaActivityRedDotMgr.m_RedDotState = {}

LuaActivityRedDotMgr.m_ScheduleDict = {}

function LuaActivityRedDotMgr:OnPlayerLogin() -- 重新登录要清数据
    LuaActivityRedDotMgr.m_RedDotState = {}
    LuaActivityRedDotMgr.m_WaitForUpdate = {}
end

function LuaActivityRedDotMgr:OnMainPlayerDestroyed()
    self:CancelUpdateTick()
end

function LuaActivityRedDotMgr:OnMainPlayerCreated()
    self:StartCheck()
end

function LuaActivityRedDotMgr:OnChangeView(args)
    local show, name = args[0], args[1]
    if show then
        for id, _ in pairs(self.m_Wnd2RedDot[name] or {}) do
            self:OnRedDotClicked(id)
        end
    end
end

UIRedDotClick.m_hookStart = function(this)
    this.onClick = DelegateFactory.VoidDelegate(function()
        LuaActivityRedDotMgr:OnRedDotClicked(this.redDotId, this.redDotType)
    end)
end

UIRedDotAlert.m_hookOnEnable = function(this)
    LuaActivityRedDotMgr:Register(this.wndName, nil, this.gameObject, this.redDotId, this.redDotType)
end

UIRedDotAlert.m_hookOnDisable = function(this)
    LuaActivityRedDotMgr:UnRegister(this.wndName, this.redDotId, this.redDotType)
end

function LuaActivityRedDotMgr:OnUpdateActivity()
    CLuaScheduleMgr.BuildInfos()
    local rawlist1 = CScheduleMgr.Inst.rawInfoNoTimeRestriction--日常
    local rawlist2 = CScheduleMgr.Inst.rawInfoTimeRestriction--限时
    self.m_ScheduleDict = {}
    
    if rawlist1 then
        for i = 1, rawlist1.Count do
            local info = rawlist1[i - 1]
            if not self.m_ScheduleDict[info.activityId] then
                self.m_ScheduleDict[info.activityId] = {info}
            else
                table.insert(self.m_ScheduleDict[info.activityId], info)
            end
        end
    end

    if rawlist2 then
        for i = 1, rawlist2.Count do
            local info = rawlist2[i - 1]
            if not self.m_ScheduleDict[info.activityId] then
                self.m_ScheduleDict[info.activityId] = {info}
            else
                table.insert(self.m_ScheduleDict[info.activityId], info)
            end
        end
    end
end

function LuaActivityRedDotMgr:LoadDesignTable()
    self.m_CachedRedDot = {}
    self.m_CachedCheckFunc = {}
    self.m_CachedCheckTime = {}
    self.m_Type2RedDot = {}
    self.m_Wnd2RedDot = {}
    for _, v in pairs(LuaEnumRedDotType) do
        self.m_Type2RedDot[v] = {}
    end

    --self.m_Id2Cron = {}
    
    Task_JieRiRedDot.Foreach(function(k, v)
        local data = Task_JieRiRedDot.GetData(k) -- 要存起来的数据不能直接用v,会导致存的全是最后一个数据的引用
        self.m_CachedRedDot[k] = data
        --table.insert(self.m_CachedRedDot, data)

        if v.Festival then
            for i = 0, v.Festival.Length - 1 do
                if not self.m_Type2RedDot[LuaEnumRedDotType.JieRi][v.Festival[i]] then
                    self.m_Type2RedDot[LuaEnumRedDotType.JieRi][v.Festival[i]] = {}
                end
                table.insert(self.m_Type2RedDot[LuaEnumRedDotType.JieRi][v.Festival[i]], data)
            end
        end
        if v.Activity then
            for i = 0, v.Activity.Length - 1 do
                if not self.m_Type2RedDot[LuaEnumRedDotType.Schedule][v.Activity[i]] then
                    self.m_Type2RedDot[LuaEnumRedDotType.Schedule][v.Activity[i]] = {}
                end
                table.insert(self.m_Type2RedDot[LuaEnumRedDotType.Schedule][v.Activity[i]], data)
            end
        end
        if v.BindWnd then
            for i = 0, v.BindWnd.Length - 1 do
                local wndName = v.BindWnd[i]
                if not self.m_Wnd2RedDot[wndName] then
                    self.m_Wnd2RedDot[wndName] = {}
                end
                self.m_Wnd2RedDot[wndName][k] = true
            end
        end
        

        --self.m_Id2Cron[k] = {}
        --table.insert(self.m_Id2Cron, {})
        --if v.CheckTime and v.CheckTime ~= "" then
        --    for cronStr in string.gmatch(v.CheckTime, "[^;]+") do
        --        table.insert(self.m_Id2Cron[k], self:GetCronData(cronStr))
        --    end
        --end

        self.m_CachedCheckFunc[k] = {}
        --table.insert(self.m_CachedCheckFunc, {})
        if v.CheckFunc and v.CheckFunc ~= "" then
            for func in string.gmatch(v.CheckFunc, "[^;]+") do
                local _, idx, funcName = string.find(func, "(.+)%(")
                funcName = string.gsub(funcName, "^%s*(.-)%s*$", "%1")
                local args = {}
                for arg in string.gmatch(string.sub(func, idx), "[^,%s%(%)]+") do
                    table.insert(args, arg)
                end
                table.insert(self.m_CachedCheckFunc[k], {name = funcName, args = args})
            end
        end

        self.m_CachedCheckTime[k] = {
            startTime = System.String.IsNullOrEmpty(data.StartTime) and 0 or CServerTimeMgr.Inst:GetTimeStampByStr(data.StartTime),
            endTime = System.String.IsNullOrEmpty(data.EndTime) and 9999999999 or CServerTimeMgr.Inst:GetTimeStampByStr(data.EndTime)
        }
    end)
end

function LuaActivityRedDotMgr:StartCheck()
    self:CancelUpdateTick()
    self:LoadDesignTable()
    self:UpdateTickFunc()
    self.m_UpdateTick = RegisterTick(function()
        self:UpdateTickFunc()
    end, self.UpdateInterval * 1000) 
end

function LuaActivityRedDotMgr:CancelUpdateTick()
    if self.m_UpdateTick then
        invoke(self.m_UpdateTick)
        self.m_UpdateTick = nil
    end
end

--@region Cron
--[[
LuaActivityRedDotMgr.m_Id2Cron = {}

function LuaActivityRedDotMgr:GetCronData(DateString)
    local DateStringFields = {}
	for DateFieldString in string.gmatch(DateString, "([%d*,/-]+)") do
		table.insert(DateStringFields, DateFieldString)
	end

	local DateFields = {}
	
	if #DateStringFields < 5 then
		return DateFields
	end

	DateFields[1] = self:PreParseDateField(DateStringFields[1], 0, 59)
	DateFields[2] = self:PreParseDateField(DateStringFields[2], 0, 23)
	DateFields[3] = self:PreParseDateField(DateStringFields[3], 1, 31)
	DateFields[4] = self:PreParseDateField(DateStringFields[4], 1, 12)
	DateFields[5] = self:PreParseDateField(DateStringFields[5], 0, 6)
	DateFields[6] = DateStringFields[6] and self:PreParseDateField(DateStringFields[6], 1970, 3000)

	return DateFields
end

function LuaActivityRedDotMgr:PreParseDateField(DateFieldString, RestrictLowerBound, RestrictUpperBound)
	local Fields = {}
	for s in string.gmatch(DateFieldString, "([%d*/-]+),*") do
		for	ss1, ss2, ss3 in string.gmatch(s, "([%d*]+)-*(%d*)/*(%d*)") do			
			local LowerBound, UpperBound, Interval
			if ss1 == "*" then
				LowerBound, UpperBound = RestrictLowerBound, RestrictUpperBound
			else
				LowerBound = tonumber(ss1) or RestrictLowerBound
				UpperBound = tonumber(ss2) or tonumber(ss1) or RestrictUpperBound
			end
			Interval = tonumber(ss3) or 1
			table.insert(Fields, {
				LowerBound = LowerBound,
				UpperBound = UpperBound,
				Interval = Interval,
			})
		end
	end
	return Fields
end

function LuaActivityRedDotMgr:_DateMatch(Date, DateFields)
	if #DateFields < 5 then
		return false
	end

	local WdayMatched
	local wday = EnumToInt(Date.DayOfWeek)
	wday = wday - 1
	if wday < 0 then
		wday = 0
	end

	if #DateFields == 6 then
		return self:_DateFieldMatch(Date.Minute, DateFields[1])
				and self:_DateFieldMatch(Date.Hour, DateFields[2])
				and self:_DateFieldMatch(Date.Day, DateFields[3])
				and self:_DateFieldMatch(Date.Month, DateFields[4])
				and self:_DateFieldMatch(wday, DateFields[5])
				and self:_DateFieldMatch(Date.Year, DateFields[6])
	end

	return self:_DateFieldMatch(Date.Minute, DateFields[1])
			and self:_DateFieldMatch(Date.Hour, DateFields[2])
			and self:_DateFieldMatch(Date.Day, DateFields[3])
			and self:_DateFieldMatch(Date.Month, DateFields[4])
			and self:_DateFieldMatch(wday, DateFields[5])
end

function LuaActivityRedDotMgr:_DateFieldMatch(DateField, DateFieldCondition)
	if type(DateFieldCondition) ~= "table" then
		return false
	end
	
	for k, v in pairs(DateFieldCondition) do
		if DateField>=v.LowerBound and DateField <= v.UpperBound and DateField % v.Interval == 0 then
			return true
		end
	end
	return false
end
--]]
--@endregion



--[[ 
[红点的显示]
    旧机制：
        1.红点状态数据CScheduleMgr.alertStates (大多)
            方式：Remind=1,在CScheduleMgr.UpdateAlertStart里判断 [or] Remind=0,手动设置红点状态
        2.红点状态数据CLuaScheduleMgr.m_AlertSchedules (极少)
            方式：Remind=0,手动设置红点状态
    新机制：红点状态数据LuaActivityRedDotMgr.m_RedDotState, 方式：CheckFunc控制showhide
[红点的点击]
    旧机制：需要显示红点的情况下，每次登录都会重新显示，点击一次后消失
    新机制：表里EveryLoginRemind=1则兼容旧机制, 否则服务器会记录点击次数，clickedCnt>=DefaultLimit后红点才消失
    Clicked 后RedDotState值不可修改(除非红点数据过期)

**LuaActivityRedDotMgr.m_RedDotState保存的仅是新机制的结果**
**使用LuaActivityRedDotMgr:IsRedDot得到兼容后的结果(所有机制的红点状态中有一个为show，则结果为show)**
**红点在所有关联活动中保持一致**
--]]

function LuaActivityRedDotMgr:SetRedDotState(id, state, force)
    local hasChanged = false
    if self.m_RedDotState[id] == LuaEnumAlertState.Clicked then
        if force then 
            if self.m_RedDotState[id] ~= state then hasChanged = true end
            self.m_RedDotState[id] = state 
        end
    else
        if self.m_RedDotState[id] ~= state then hasChanged = true end
        self.m_RedDotState[id] = state
    end
    return hasChanged
end

function LuaActivityRedDotMgr:GetRedDotState(id, default)
    if self.m_RedDotState[id] then
        return self.m_RedDotState[id]
    end
    return default or LuaEnumAlertState.Hide
end

---- tabType∈LuaEnumActivityTabType
function LuaActivityRedDotMgr:IsTabTypeRedDot(tabType)
    if tabType ~= 0 and self.m_Type2RedDot[LuaEnumRedDotType.Schedule] then 
        for id, redDotLs in pairs(self.m_Type2RedDot[LuaEnumRedDotType.Schedule]) do
            local infos = LuaActivityRedDotMgr.m_ScheduleDict[id]
            if infos and #infos > 0 then
                local info = infos[1]
                if info and CScheduleMgr.ScheduleInfoFilter(info, 0, false, false) then --TODO
                    local schedule = info:GetSchedleInfo()
                    for i = 1, #redDotLs do -- 日程对应的所有红点取或
                        if self:GetRedDotState(redDotLs[i].Id) == LuaEnumAlertState.Show then
                            if tabType == 1 then 
                                if redDotLs[i].ActivityBtnReminder == 1 then
                                    return true
                                end 
                            elseif tabType == 2 then 
                                if schedule.Recommend > 0 then
                                    return true
                                end
                            else
                                -- LuaEnumActivityTabType = 表中的TaskTabType + 2
                                for i = 0, schedule.TaskTabType.Length - 1 do
                                    if tabType == schedule.TaskTabType[i] + 2 then
                                        return true
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
    end
    return CScheduleMgr.Inst:NeedShowAlert(tabType)
end

----活动（节日+日程）红点更新Tick，兼容旧的日程红点
function LuaActivityRedDotMgr:UpdateTickFunc()
    if not CClientMainPlayer.Inst then return end

    self:LoadWaitForUpdate()

    --local DateNow = CServerTimeMgr.Inst:GetZone8Time()
    local StampNow = CServerTimeMgr.Inst.timeStamp

    local changeList = {}

    for _, data in pairs(self.m_CachedRedDot) do
        local id = data.Id
        if self.m_CachedCheckTime[id].startTime and self.m_CachedCheckTime[id].startTime <= StampNow and self.m_CachedCheckTime[id].endTime and self.m_CachedCheckTime[id].endTime > StampNow then
            local info = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.ReddotEventData, id) or {ExpiredTime = 0, FinishTimes = 0}

            -- EveryLoginRemind ~= 1 表示需要使用服务器数据
    
            -- 没过期 且 点击次数达到上限时, 视为点掉红点
            if data.EveryLoginRemind ~= 1 and StampNow < info.ExpiredTime and info.FinishTimes >= data.DefaultLimit then
                if self:SetRedDotState(id, LuaEnumAlertState.Clicked)  then
                    table.insert(changeList, id)
                end
            end
    
            -- 红点未点掉 或 点掉但数据已过期时, 才需要判断是否显示
            if self:GetRedDotState(id) ~= LuaEnumAlertState.Clicked or data.EveryLoginRemind ~= 1 and StampNow >= info.ExpiredTime then 
                local show = true
                if #self.m_CachedCheckFunc[id] > 0 then -- CheckFunc取与
                    for _, func in ipairs(self.m_CachedCheckFunc[id]) do
                        --print(func.name, unpack(func.args))
                        if g_ActivityRedDotCheckFunc[func.name] and not g_ActivityRedDotCheckFunc[func.name](data, info, unpack(func.args)) then
                            show = false
                            break
                        end
                    end
                end
    
                -- 过期的红点要强制设置 
                if self:SetRedDotState(id, show and LuaEnumAlertState.Show or LuaEnumAlertState.Hide, data.EveryLoginRemind ~= 1 and StampNow >= info.ExpiredTime) then
                    table.insert(changeList, id)
                end
            end
        else
            self:SetRedDotState(id, LuaEnumAlertState.Hide) 
        end
    end

    if changeList then
        for _, id in ipairs(changeList) do
            if self.m_RegisteredAlert[id] then
                for _, alerts in pairs(self.m_RegisteredAlert[id]) do
                    for _, alert in ipairs(alerts) do
                        if not CommonDefs.IsNull(alert) then
                            alert:SetActive(self:GetRedDotState(id) == LuaEnumAlertState.Show)
                        end
                    end
                end
            end
        end
    end

    g_ScriptEvent:BroadcastInLua("UpdateActivityRedDot", #changeList > 0 and changeList)
end

LuaActivityRedDotMgr.m_WaitForUpdate = {}
function LuaActivityRedDotMgr:LoadWaitForUpdate()
    for i = 1, #self.m_WaitForUpdate do
        local info = self.m_WaitForUpdate[i]
        local redDotInfo = CReddotEventInfo()
        redDotInfo.FinishTimes = info.finishTimes
        redDotInfo.ExpiredTime = info.expiredTime
        if CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.ReddotEventData, info.id) then
            CommonDefs.DictSet_LuaCall(CClientMainPlayer.Inst.PlayProp.ReddotEventData, info.id, redDotInfo)
        else
            CommonDefs.DictAdd_LuaCall(CClientMainPlayer.Inst.PlayProp.ReddotEventData, info.id, redDotInfo)
        end
    end
    self.m_WaitForUpdate = {}
end

function LuaActivityRedDotMgr:UpdateReddotDataWithKey(key, times, expiredTime)
    --print("UpdateReddotDataWithKey", key, times, expiredTime)
    if CClientMainPlayer.Inst then
        self:LoadWaitForUpdate()
        
        local redDotInfo = CReddotEventInfo()
        redDotInfo.FinishTimes = times
        redDotInfo.ExpiredTime = expiredTime
        if CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.ReddotEventData, key) then
            CommonDefs.DictSet_LuaCall(CClientMainPlayer.Inst.PlayProp.ReddotEventData, key, redDotInfo)
        else
            CommonDefs.DictAdd_LuaCall(CClientMainPlayer.Inst.PlayProp.ReddotEventData, key, redDotInfo)
        end
        self:UpdateTickFunc()
    else
        self.m_WaitForUpdate[#self.m_WaitForUpdate + 1] = {
            id = key,
            finishTimes = times,
            expiredTime = expiredTime,
        }
	end
end

-- 旧的日程红点机制
CScheduleMgr.m_hookUpdateAlertStart = function(this)
    this.canJoinScheduleList = nil

    if CClientMainPlayer.Inst == nil then return end
    if this.rawInfoTimeRestriction == nil then return end     

    local now = CServerTimeMgr.Inst:GetZone8Time()
    if now.Day ~= this.cacheDay then
        Gac2Gas.QueryDayActivityInfo()
        this.cacheDay = now.Day
        this.alertStates:Clear()
        return
    end

    local nowTime = (now.Hour * 60 + now.Minute) * 60 + now.Second
    local infos = CreateFromClass(MakeGenericClass(List, CDailyScheduleInfo))
    local segments = this:GetSegmentedActivity()
    for i = 0, this.rawInfoTimeRestriction.Count - 1 do
        local info = this.rawInfoTimeRestriction[i]
        if CScheduleMgr.ScheduleInfoFilter(info, 0, true, false) then  
            infos:Add(info)
        end
    end
    for i = 0, segments.Count - 1 do
        local info = segments[i]
        if CScheduleMgr.ScheduleInfoFilter(info, 0, false, false) then  
            infos:Add(info)
        end
    end

    for i = 0, infos.Count - 1 do
        local info = infos[i]
        local startTime = info.StartTime
        local endTime = info.EndTime
        local preTime = 0
        local schedule = info:GetSchedleInfo()
        if schedule ~= nil and schedule.Remind > 0 then --需要提示
            if schedule ~= nil then 
                preTime = tonumber(schedule.AheadBroadcastTime) * 60 --基本上没用
            end
            --以秒为单位
            if nowTime >= startTime - preTime and nowTime <= endTime then --提前10分钟提示
                if info.FinishedTimes > 0 or CTaskMgr.Inst:IsInProgress(info.taskId) then
                    this:SetAlertState(info.activityId, CScheduleMgr.EnumAlertState.Hide, false)
                else
                    --检查同步的消息
                    if this.Patch_CheckTask and preTime == 0 then --排除掉提前提示红点的情况
                        if CTaskMgr.Inst:CheckTaskTime(info.taskId) then
                            this:SetAlertState(info.activityId, CScheduleMgr.EnumAlertState.Show, false) --需要提示
                        end
                    else
                        this:SetAlertState(info.activityId, CScheduleMgr.EnumAlertState.Show, false) --需要提示
                    end
                end
            else
                this:SetAlertState(info.activityId, CScheduleMgr.EnumAlertState.Hide, false)
            end

            if schedule.Type == "Guild" then--EnumActivityType.Guild:ToString() then --是帮会活动
                if CClientMainPlayer.Inst ~= nil then
                    if not CClientMainPlayer.Inst:IsInGuild() then
                        this:SetAlertState(info.activityId, CScheduleMgr.EnumAlertState.Hide, false)
                    end
                end
            elseif schedule.Type == "BiWu" then--EnumActivityType.BiWu:ToString() then
                if CBiWuDaHuiMgr.Inst.ApplyState then --如果已经报名了
                    this:SetAlertState(info.activityId, CScheduleMgr.EnumAlertState.Hide, false)
                end
            elseif schedule.Type == "ZXJY" then--EnumActivityType.ZXJY.ToString() then --在线经验
                if CClientMainPlayer.Inst ~= nil then
                    if CClientMainPlayer.Inst.expRainStart then
                        this:SetAlertState(info.activityId, CScheduleMgr.EnumAlertState.Show, true)
                    else
                        this:SetAlertState(info.activityId, CScheduleMgr.EnumAlertState.Hide, true)
                    end
                end
            end
        end
    end
    EventManager.Broadcast(EnumEventType.UpdateScheduleAlert)
end

function LuaActivityRedDotMgr:_OnRedDotClicked(data)
    if data then
        -- 红点亮起时才会触发点击事件
        if self:GetRedDotState(data.Id) ~= LuaEnumAlertState.Show then
            return
        end
        if data.EveryLoginRemind == 1 then
            self:SetRedDotState(data.Id, LuaEnumAlertState.Clicked)
            --TODO 可优化避免多次发送事件
            g_ScriptEvent:BroadcastInLua("UpdateActivityRedDot", {data.Id})
        else
            Gac2Gas.OnClickFestivalReddot(data.Id)
        end
    end
end

--@region PublicMethod

function LuaActivityRedDotMgr:IsRedDot(id, type)
    if not id then return false end
    type = type or LuaEnumRedDotType.Default

    if type == LuaEnumRedDotType.Default then
        return self.m_RedDotState[id] == LuaEnumAlertState.Show
    end

    local redDotLs = self.m_Type2RedDot[type] and self.m_Type2RedDot[type][id]
    if redDotLs then
        -- 对应多个红点时取或
        for i = 1, #redDotLs do
            if self.m_RedDotState[redDotLs[i].Id] == LuaEnumAlertState.Show then
                return true
            end
        end
    end

    -- 兼容旧机制
    if type == LuaEnumRedDotType.Schedule and (CScheduleMgr.Inst:GetAlertState(id) == CScheduleMgr.EnumAlertState.Show or CLuaScheduleMgr.IsScheduleNeedShowAlert(id)) then
        return true
    end
    
    return false
end

function LuaActivityRedDotMgr:OnRedDotClicked(id, type) 
    if not id then return end
    type = type or LuaEnumRedDotType.Default

    if type == LuaEnumRedDotType.Default then 
        self:_OnRedDotClicked(self.m_CachedRedDot[id])
    else
        local redDotLs = self.m_Type2RedDot[type] and self.m_Type2RedDot[type][id] 
        if redDotLs then
            for i = 1, #redDotLs do 
                if type ~= LuaEnumRedDotType.JieRi or redDotLs[i].FestivalBtnClick == 1 then
                    self:_OnRedDotClicked(redDotLs[i])
                end
            end
        end
    end
end

function LuaActivityRedDotMgr:GetRedDotInfo(id) 
    return CClientMainPlayer.Inst and CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.ReddotEventData, id) or {ExpiredTime = 0, FinishTimes = 0}
end

LuaActivityRedDotMgr.m_RegisteredClickDelegate = {}
LuaActivityRedDotMgr.m_RegisteredClickObj = {}
LuaActivityRedDotMgr.m_RegisteredAlert = {}

function LuaActivityRedDotMgr:Register(wnd, objToClick, alertToUpdate, id, type)
    type = type or LuaEnumRedDotType.Default
    
    if not self.m_RegisteredClickDelegate[type] then self.m_RegisteredClickDelegate[type] = {} end
    if not self.m_RegisteredClickDelegate[type][id] then self.m_RegisteredClickDelegate[type][id] = {} end
    if not self.m_RegisteredClickDelegate[type][id][wnd] then -- 保证点击红点delegate的唯一性
        self.m_RegisteredClickDelegate[type][id][wnd] = DelegateFactory.VoidDelegate(function() self:OnRedDotClicked(id, type) end)
    end

    if not self.m_RegisteredClickObj[type] then self.m_RegisteredClickObj[type] = {} end
    if not self.m_RegisteredClickObj[type][id] then self.m_RegisteredClickObj[type][id] = {} end
    if not self.m_RegisteredClickObj[type][id][wnd] then self.m_RegisteredClickObj[type][id][wnd] = {} end
    
    if objToClick then 
        table.insert(self.m_RegisteredClickObj[type][id][wnd], objToClick)
        UIEventListener.Get(objToClick).onClick = CommonDefs.CombineListner_VoidDelegate(
            UIEventListener.Get(objToClick).onClick, self.m_RegisteredClickDelegate[type][id][wnd], true) 
    end

    if alertToUpdate then
        if type == LuaEnumRedDotType.Default then
            if not self.m_RegisteredAlert[id] then self.m_RegisteredAlert[id] = {} end
            if not self.m_RegisteredAlert[id][wnd] then self.m_RegisteredAlert[id][wnd] = {} end
            alertToUpdate:SetActive(self:IsRedDot(id))
            table.insert(self.m_RegisteredAlert[id][wnd], alertToUpdate)
        else
            local redDotLs = self.m_Type2RedDot[type] and self.m_Type2RedDot[type][id]
            if redDotLs then
                for i = 1, #redDotLs do
                    if not self.m_RegisteredAlert[redDotLs[i].Id] then self.m_RegisteredAlert[redDotLs[i].Id] = {} end
                    if not self.m_RegisteredAlert[redDotLs[i].Id][wnd] then self.m_RegisteredAlert[redDotLs[i].Id][wnd] = {} end
                    alertToUpdate:SetActive(self:IsRedDot(redDotLs[i].Id))
                    table.insert(self.m_RegisteredAlert[redDotLs[i].Id][wnd], alertToUpdate)
                end
            end
        end
    end
end

function LuaActivityRedDotMgr:UnRegister(wnd, id, type)
    type = type or LuaEnumRedDotType.Default

    if self.m_RegisteredClickDelegate[type] and self.m_RegisteredClickDelegate[type][id] and self.m_RegisteredClickDelegate[type][id][wnd] and
        self.m_RegisteredClickObj[type] and self.m_RegisteredClickObj[type][id] and self.m_RegisteredClickObj[type][id][wnd] then
        for _, obj in ipairs(self.m_RegisteredClickObj[type][id][wnd]) do
            UIEventListener.Get(obj).onClick = CommonDefs.CombineListner_VoidDelegate(
                UIEventListener.Get(obj).onClick, self.m_RegisteredClickDelegate[type][id][wnd], false) 
        end
        self.m_RegisteredClickDelegate[type][id][wnd] = nil
        self.m_RegisteredClickObj[type][id][wnd] = {}
    end

    if type == LuaEnumRedDotType.Default then
        if self.m_RegisteredAlert[id] then
            self.m_RegisteredAlert[id][wnd] = {}
        end
    else
        local redDotLs = self.m_Type2RedDot[type] and self.m_Type2RedDot[type][id]
        if redDotLs then
            for i = 1, #redDotLs do
                if self.m_RegisteredAlert[redDotLs[i].Id] then
                    self.m_RegisteredAlert[redDotLs[i].Id][wnd] = {}
                end
            end
        end
    end
end

--@endregion



---- 自定义红点检查函数. **推荐只对玩法数据进行条件判断, 数据的计算和更新请在别处处理**
g_ActivityRedDotCheckFunc = {}
--return true means: show RedDot unless clicked

---------- 通用检查函数 ----------

g_ActivityRedDotCheckFunc["ScheduleNotFinished"] = function(data, redDotInfo, ...)
    local scheduleId = select(1, ...)
    scheduleId = tonumber(scheduleId)
    local schedule = CLuaScheduleMgr:GetScheduleInfo(scheduleId)
    return schedule and not schedule:IsFinished()
end

g_ActivityRedDotCheckFunc["ScheduleNotJoined"] = function(data, redDotInfo, ...)
    local scheduleId = select(1, ...)
    scheduleId = tonumber(scheduleId)
    local schedule = CLuaScheduleMgr:GetScheduleInfo(scheduleId)
    return schedule and schedule.FinishedTimes == 0 and not CTaskMgr.Inst:IsInProgress(schedule.taskId)
end

-- 参数1: true,判断是否有红点; false,检查是否无红点
-- 参数2..N: redDotId, 填多个时取或 
-- 为了避免死循环, 可能会延迟
g_ActivityRedDotCheckFunc["IsRedDot"] = function(data, redDotInfo, ...)
    local argc = select('#', ...)
    local state = select(1, ...)
    if state == "true" then state = true
    elseif state == "false" then state = false
    else return false end
    for i = 2, argc do 
        local redDotId = select(i, ...)
        redDotId = tonumber(redDotId)
        if state == LuaActivityRedDotMgr:IsRedDot(redDotId) then
            return true
        end
    end
    return false
end

-- 无参数；当关联的日程已开放、即等级和时间等条件满足时显示红点(注意：关联多个日程时取或，即只要满足其中一个日程开放)
g_ActivityRedDotCheckFunc["BindActivity"] = function(data, redDotInfo, ...) 
    if not CScheduleMgr.Inst then return false end     

    local now = CServerTimeMgr.Inst:GetZone8Time()
    if now.Day ~= CScheduleMgr.Inst.cacheDay then
        Gac2Gas.QueryDayActivityInfo()
        CScheduleMgr.Inst.cacheDay = now.Day
        CScheduleMgr.Inst.alertStates:Clear()
        return false
    end

    local nowTime = (now.Hour * 60 + now.Minute) * 60 + now.Second
    for i = 0, data.Activity.Length - 1 do 
        local infos = LuaActivityRedDotMgr.m_ScheduleDict[data.Activity[i]]
        if infos then
            for j = 1, #infos do
                local info = infos[j]
                if info and CScheduleMgr.ScheduleInfoFilter(info, 0, false, false) and not CScheduleMgr.Inst:NoNeedToShowAlert(info) then 
                    local startTime = info.StartTime
                    local endTime = info.EndTime
                    local task = info:GetTaskInfo()
                    if task then
                        -- Task_Task.StartTime不填视为全天开放

                        if System.String.IsNullOrEmpty(task.StartTime) then
                            return true
                        -- 日程应该只在开放日内会推下来
                        elseif nowTime >= startTime and nowTime <= endTime then
                            if CScheduleMgr.Patch_CheckTask then
                                if CTaskMgr.Inst:CheckTaskTime(info.taskId) then
                                    return true --需要提示
                                end
                            else
                                return true --需要提示
                            end 
                        end
                    end
                end
            end
        end
    end
    return false
end

local BINARY_OP = function(op, left, right)
	left = tonumber(left)
	right = tonumber(right)
	if op == "<" then
		return left < right
	elseif op == ">" then
		return left > right
	elseif op == "<=" then
		return left <= right
	elseif op == ">=" then
		return left >= right
	else
		return left == right
	end
end

g_ActivityRedDotCheckFunc["Level"] = function(data, redDotInfo, ...)
    local param = select(1, ...)
    if param then
        local op, rightVal = string.match(param, "([<>=%%]*)%s*(%d+)")
        return BINARY_OP(op, CClientMainPlayer.Inst.MaxLevel, rightVal)
    end
    return true
end

--------- 世界杯 ---------------
g_ActivityRedDotCheckFunc["WorldCup2022GJZLCheck"] = function(data, redDotInfo, ...)
    return LuaWorldCup2022Mgr:WorldCup2022GJZLCheck()
end

g_ActivityRedDotCheckFunc["WorldCup2022JZLYCheck"] = function(data, redDotInfo, ...)
    return LuaWorldCup2022Mgr:WorldCup2022JZLYCheck()
end

g_ActivityRedDotCheckFunc["WorldCup2022TTJCCheck"] = function(data, redDotInfo, ...)
    return LuaWorldCup2022Mgr:WorldCup2022TTJCCheck()
end

g_ActivityRedDotCheckFunc["WorldCup2022QDYLCheck"] = function(data, redDotInfo, ...)
    return LuaWorldCup2022Mgr:WorldCup2022QDYLCheck()
end
--------------------------------

--------- 国庆2022 ---------------
g_ActivityRedDotCheckFunc["GuoQing2022PVPCheck"] = function(data, redDotInfo, ...)
    if CClientMainPlayer.Inst == nil then
        return false
    end
    local setting = GuoQing2022_JinLuHunYuanZhan.GetData()
    local awardlimit = setting.AwardLimit
    local playtimes = CClientMainPlayer.Inst.PlayProp:GetPlayTimes(EnumPlayTimesKey_lua.eJinLuHunYuanZhanTimes)
    local Level = CClientMainPlayer.Inst.Level
    local now = CServerTimeMgr.Inst:GetZone8Time()
    return Level >= 50 and playtimes < awardlimit and (now.Hour >= 14 and now.Hour < 18 or now.Hour >= 21 and now.Hour < 23)
end
--------------------------------
--------- 万圣节2022 -----------
g_ActivityRedDotCheckFunc["Halloween2022Check"] = function(data, redDotInfo, ...)
    return LuaHalloween2022Mgr.RedDotCheck()
end
g_ActivityRedDotCheckFunc["Halloween2022MuTouRenCheck"] = function ( data , redDotId, ...)
    return LuaHalloween2022Mgr.IsInTrafficLightPlayTime()
end
--------------------------------

--------- 双十一2022 -----------
g_ActivityRedDotCheckFunc["DoubleOne2022SZTBCheck"] = function(data, redDotInfo, ...)
    return LuaShuangshiyi2022Mgr:DoubleOne2022SZTBCheck()
end
-------------------------------

----------春节2023-------------
g_ActivityRedDotCheckFunc["ChunJie2023ZJNHCheck"] = function(data, redDotInfo, ...)
    return LuaChunJie2023Mgr:ChunJie2023ZJNHCheck()
end
g_ActivityRedDotCheckFunc["ChunJie2023TTSYCheck"] = function (data , redDotId, ...)
    return LuaChunJie2023Mgr:ChunJie2023TTSYCheck()
end
-------------------------------

----------清明2023-------------
g_ActivityRedDotCheckFunc["QingMing2023PVPCheck"] = function(data, redDotInfo, ...)
	return LuaQingMing2023Mgr:QingMing2023PVPCheck()
end
-------------------------------

----------七夕2023-------------
g_ActivityRedDotCheckFunc["QiXi2023PlantTreeRewardCheck"] = function(data, redDotInfo, ...)
    return LuaQiXi2022Mgr:QiXi2023PlantTreeRewardCheck()
end
-------------------------------

----------嘉年华2023-------------
g_ActivityRedDotCheckFunc["CarnivalCollectTicketCheck"] = function(data, redDotInfo, ...)
    return LuaCarnivalCollectMgr:CarnivalCollectTicketCheck()
end

g_ActivityRedDotCheckFunc["Carnival2023BossOpenCheck"] = function(data, redDotInfo, ...)
    return LuaCarnival2023Mgr:Carnival2023BossOpenCheck()
end
-------------------------------
-----------亚运会2023-----------
g_ActivityRedDotCheckFunc["YaYunHui2023HangZhouScrollRewardCheck"] = function(data, redDotInfo, ...)
    if CClientMainPlayer.Inst == nil then return false end
    if CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eAsianGames2023Data) then
        local playDataU = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, EnumTempPlayDataKey_lua.eAsianGames2023Data)
        local tab = g_MessagePack.unpack(playDataU.Data.Data)
        local ScrollData = tab and tab[EnumAsianGames2023DataType.eHuiJuan] or {}
		local RewardData = tab and tab[EnumAsianGames2023DataType.eHuiJuanRewardStatus] or 0
        local cnt = 0
        for k,v in pairs(ScrollData) do
            if v == 100 then return true end
            if v > 100 then cnt = cnt + 1 end
        end
        local awardList = AsianGames2023_Setting.GetData().HuiJuanAwards
        for i = 0,awardList.Length - 1 do
            local num = awardList[i][0]
            local isGet = bit.band(RewardData ,bit.lshift(1, i)) > 0  
            if not isGet and num <= cnt then return true end
        end
    end
    return false
end
-------------------------------
-----------中元节2023-----------
g_ActivityRedDotCheckFunc["ZhongYuanJie2023SignUpCheck"] = function(data, redDotInfo, ...)
    return LuaZhongYuanJie2023Mgr:ZhongYuanJie2023SignUpCheck()
end
-------------------------------
-----------双十一2023-----------
g_ActivityRedDotCheckFunc["DoubleOneDiscountFreeOneDraw"] = function()
    return LuaShuangshiyi2023Mgr:IsDiscountFreeOneDraw()
end

g_ActivityRedDotCheckFunc["DoubleOneLotteryTimesRemain"] = function()
    return LuaShuangshiyi2023Mgr:IsLotteryDrawTimesRemain()
end

g_ActivityRedDotCheckFunc["DoubleOneChargeRewardsRemain"] = function()
    return LuaShuangshiyi2023Mgr:IsRechargeRewardToGet()
end