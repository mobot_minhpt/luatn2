local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UITable = import "UITable"
local CButton = import "L10.UI.CButton"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CServerTimeMgr  = import "L10.Game.CServerTimeMgr"
LuaDaFuWongItemUseWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaDaFuWongItemUseWnd, "Table", "Table", UITable)
RegistChildComponent(LuaDaFuWongItemUseWnd, "OKBtn", "OKBtn", CButton)
RegistChildComponent(LuaDaFuWongItemUseWnd, "CancelButton", "CancelButton", CButton)
RegistChildComponent(LuaDaFuWongItemUseWnd, "PlayerTemplate", "PlayerTemplate", GameObject)
RegistChildComponent(LuaDaFuWongItemUseWnd, "TitleName", "TitleName", UILabel)
RegistChildComponent(LuaDaFuWongItemUseWnd, "Desc", "Desc", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongItemUseWnd, "m_CountDown")
RegistClassMember(LuaDaFuWongItemUseWnd, "m_SelectPlayerIndex")
RegistClassMember(LuaDaFuWongItemUseWnd, "m_ItemId")
RegistClassMember(LuaDaFuWongItemUseWnd, "m_PlayerView")
RegistClassMember(LuaDaFuWongItemUseWnd, "m_RankBg")
RegistClassMember(LuaDaFuWongItemUseWnd, "m_HighLightRankBg")
RegistClassMember(LuaDaFuWongItemUseWnd, "m_Bg")
RegistClassMember(LuaDaFuWongItemUseWnd, "m_HighLightBg")
function LuaDaFuWongItemUseWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.OKBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOKBtnClick()
	end)


	
	UIEventListener.Get(self.CancelButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCancelButtonClick()
	end)


    --@endregion EventBind end
	self.m_CountDownTick = nil
	self.m_CountDown = 0
	self.m_PlayerView = nil
	self.m_SelectPlayerIndex = 0
	self.m_RankBg = {"dafuwongItemusewnd_yellow_n_02","dafuwongItemusewnd_red_n_02","dafuwongItemusewnd_purple_n_02","dafuwongItemusewnd_green_n_02"}
	self.m_HighLightRankBg = {"dafuwongItemusewnd_yellow_s_02","dafuwongItemusewnd_red_s_02","dafuwongItemusewnd_purple_s_02","dafuwongItemusewnd_green_s_02"}
	self.m_Bg = {"dafuwongItemusewnd_yellow_n_01","dafuwongItemusewnd_red_n_01","dafuwongItemusewnd_purple_n_01","dafuwongItemusewnd_green_n_01"}
	self.m_HighLightBg = {"dafuwongItemusewnd_yellow_s_01","dafuwongItemusewnd_red_s_01","dafuwongItemusewnd_purple_s_01","dafuwongItemusewnd_green_s_01"}
	self.nameColor = {"8b5b31","8b3131","7236b5","317823"}
	self.PlayerTemplate.gameObject:SetActive(false)
end

function LuaDaFuWongItemUseWnd:Init()
	self.m_ItemId = LuaDaFuWongMgr.CurItemId
	local data = DaFuWeng_Card.GetData(self.m_ItemId)
	if not data then return end

	self.m_CountDown = DaFuWeng_Countdown.GetData(EnumDaFuWengStage.RoundCard).Time
	self:SetCountDownTick()
	self:InitTitle(data.Name,data.SelectDesc)
	local playerList = {}
	if LuaDaFuWongMgr.PlayerInfo then 
		for k,v in pairs(LuaDaFuWongMgr.PlayerInfo) do
			if CClientMainPlayer.Inst and k == CClientMainPlayer.Inst.Id then
				if data.SelectSelf ~= 1 and data.SelectSelf ~= 2 then
					table.insert(playerList,{id = k,name = CClientMainPlayer.Inst.RealName, class = CClientMainPlayer.Inst.Class, gender = CClientMainPlayer.Inst.Gender,round = v.round,rank = v.Rank,isMe = true})
				end
			elseif not v.IsOut then	-- 玩家没被淘汰
				local buffList = data.CannotSelectBuff
				local HasBuff = false
				if buffList then
					for i = 0,buffList.Length - 1 do
						local id = buffList[i]
						if v.BuffInfo[id] and v.BuffInfo[id] > 0 then
                            HasBuff = true
                        end
					end 
				end
				if not HasBuff then 
					table.insert(playerList,{id = k,name = v.name,class = v.class, gender = v.gender,round = v.round,rank = v.Rank,isMe = false})
				end
			end
		end
	end
	self:InitPlayerInfo(playerList)
end
function LuaDaFuWongItemUseWnd:InitTitle(title, desc)
	self.TitleName.text = title
	self.Desc.text = SafeStringFormat3("[6B310E]%s[-]",g_MessageMgr:Format(desc))
end

function LuaDaFuWongItemUseWnd:InitPlayerInfo(playerList)
	Extensions.RemoveAllChildren(self.Table.transform)
	self.m_PlayerView = {}
	for i = 1 ,#playerList do
		local go = CUICommonDef.AddChild(self.Table.gameObject,self.PlayerTemplate)
		go.gameObject:SetActive(true)
		local bg = go.transform:Find("bg"):GetComponent(typeof(CUITexture))
		bg:LoadMaterial(self:GetBgPath(false,playerList[i].round,false))
		local rank = go.transform:Find("rank"):GetComponent(typeof(UILabel))
		rank.text = playerList[i].rank
		local rankBg = rank.transform:Find("Texture"):GetComponent(typeof(UISprite))
		local highlightBg = bg.transform:Find("HighlightBg").gameObject
		highlightBg:SetActive(false)
		rankBg.spriteName = self:GetBgPath(true,playerList[i].round,false)
		go.transform:Find("Portrait"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(playerList[i].class, playerList[i].gender, -1), false)
		go.transform:Find("MyIcon").gameObject:SetActive(playerList[i].isMe)
		local name = go.transform:Find("Name"):GetComponent(typeof(UILabel))
		name.text = playerList[i].name
		name.color = NGUIText.ParseColor24(self.nameColor[playerList[i].round], 0)
		self.m_PlayerView[i] = {go = go,bg = bg,rankBg = rankBg, id = playerList[i].id, round = playerList[i].round, highlightBg = highlightBg}
		UIEventListener.Get(bg.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
			self:SetPlayerSelect(i)
		end)
		
	end
	self.Table:Reposition()
	if #playerList == 3 then
		local center = (self.m_PlayerView[1].go.transform.localPosition.x + self.m_PlayerView[2].go.transform.localPosition.x) / 2
		local Tween = LuaTweenUtils.DOLocalMoveX(self.m_PlayerView[3].go.transform,center,0.02,false)
	end
	if #playerList >=1 then self:SetPlayerSelect(1) end
end

function LuaDaFuWongItemUseWnd:SetPlayerSelect(index)
	local selectView = self.m_PlayerView[index]
	local curView = self.m_PlayerView[self.m_SelectPlayerIndex]
	if curView then
		curView.rankBg.spriteName = self:GetBgPath(true,curView.round,false)
		curView.bg:LoadMaterial(self:GetBgPath(false,curView.round,false))
		curView.highlightBg.gameObject:SetActive(false)
	end
	if selectView then
		selectView.rankBg.spriteName = self:GetBgPath(true,selectView.round,true)
		selectView.bg:LoadMaterial(self:GetBgPath(false,selectView.round,true))
		selectView.highlightBg.gameObject:SetActive(true)
	end
	self.m_SelectPlayerIndex = index
end

function LuaDaFuWongItemUseWnd:GetBgPath(isrank,index,isHighlight)
	local name = ""
	if isrank then
		name = self.m_RankBg[index]
		if isHighlight then
			name = self.m_HighLightRankBg[index]
		end
	else
		name = self.m_Bg[index]
		if isHighlight then
			name = self.m_HighLightBg[index]
		end
		name = SafeStringFormat3("UI/Texture/FestivalActivity/Festival_DaFuWong/Material/%s.mat",name)
	end
    return name
end

function LuaDaFuWongItemUseWnd:SetCountDownTick()
	local countDown = self.m_CountDown
	local endFunc = function() self:OnCloseButtonClick() end
	local durationFunc = function(time) if self.CancelButton then self.CancelButton.transform:Find("Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("取消(%d)"),time)  end end
	LuaDaFuWongMgr:StartCountDownTick(EnumDaFuWengCountDownStage.CardUse,countDown,durationFunc,endFunc)
end

--@region UIEvent

function LuaDaFuWongItemUseWnd:OnOKBtnClick()
	-- 发RPC
	if self.m_SelectPlayerIndex == 0 then
		g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("请选择使用对象"))
	else
		local id = self.m_PlayerView[self.m_SelectPlayerIndex].id
		local index = LuaDaFuWongMgr.PlayerInfo[id].round
		Gac2Gas.DaFuWengUseCard(self.m_ItemId,index)
		CUIManager.CloseUI(CLuaUIResources.DaFuWongItemUseWnd)
	end
	
end

function LuaDaFuWongItemUseWnd:OnCancelButtonClick()
	self:OnCloseButtonClick()
end

--@endregion UIEvent
function LuaDaFuWongItemUseWnd:OnDisable()
	LuaDaFuWongMgr:EndCountDownTick(EnumDaFuWengCountDownStage.CardUse)
end

function LuaDaFuWongItemUseWnd:OnCloseButtonClick()
	CUIManager.CloseUI(CLuaUIResources.DaFuWongItemUseWnd)
end
