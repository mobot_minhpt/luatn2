-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CellIndex = import "L10.UI.UITableView+CellIndex"
local CellIndexType = import "L10.UI.UITableView+CellIndexType"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local CTeamMatchMgr = import "L10.Game.CTeamMatchMgr"
local CTeamQuickJoinActivityItem = import "L10.UI.CTeamQuickJoinActivityItem"
local CTeamQuickJoinMasterView = import "L10.UI.CTeamQuickJoinMasterView"
local CUIResources = import "L10.UI.CUIResources"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local LocalString = import "LocalString"
local Object = import "System.Object"
local TeamMatch_Activities = import "L10.Game.TeamMatch_Activities"
local TeamMatchActivitySection = import "L10.Game.TeamMatchActivitySection"
local UILabel = import "UILabel"
local CQuanMinPKMgr=import "L10.Game.CQuanMinPKMgr"
CTeamQuickJoinMasterView.m_Init_CS2LuaHook = function (this, activityId) 
    if not CQuanMinPKMgr.Inst.m_IsQuanMinPKServer then
        CTeamMatchMgr.Inst:FillMatchActivitySections(this.matchActivitySections)
    end
    local nearby = CreateFromClass(TeamMatchActivitySection)
    nearby.activityId = Constants.NearbyActivityId
    nearby.type = 0
    nearby.name = LocalString.GetString("附近的队伍")
    CommonDefs.ListInsert(this.matchActivitySections, 0, typeof(TeamMatchActivitySection), nearby)

    local foundActivity = false
    local selectedIndex = CreateFromClass(CellIndex, 0, 0, CellIndexType.Section)
    if CTeamQuickJoinMasterView.s_EnableRandomAccessActivity and activityId > 0 then
        do
            local i = 0
            while i < this.matchActivitySections.Count do
                local section = this.matchActivitySections[i]
                if section.activityId == activityId then
                    foundActivity = true
                    selectedIndex = CreateFromClass(CellIndex, i, 0, CellIndexType.Section)
                elseif section.activities.Count > 0 then
                    do
                        local j = 0
                        while j < section.activities.Count do
                            if section.activities[j] == activityId then
                                foundActivity = true
                                selectedIndex = CreateFromClass(CellIndex, i, j, CellIndexType.Row)
                                break
                            end
                            j = j + 1
                        end
                    end
                end
                if foundActivity then
                    break
                end
                i = i + 1
            end
        end
    end

    this:LoadData(selectedIndex, true)
end
CTeamQuickJoinMasterView.m_OnSectionClicked_CS2LuaHook = function (this, index, expanded) 
    if index.type == CellIndexType.Section then
        local activityId = this.matchActivitySections[index.section].activityId
        if activityId > 0 then
            if this.OnActivityClick ~= nil then
                GenericDelegateInvoke(this.OnActivityClick, Table2ArrayWithCount({activityId}, 1, MakeArrayClass(Object)))
            end
        elseif CClientMainPlayer.Inst ~= nil and expanded then
            --找到最接近玩家等级的活动，列表假定按等级升序排列
            local section = this.matchActivitySections[index.section]
            if section.activities.Count > 1 then
                local lastRow = 0
                do
                    local i = 1
                    while i < section.activities.Count do
                        local id = section.activities[i]
                        local data = TeamMatch_Activities.GetData(id)
                        if data.MinPlayerLevel > CClientMainPlayer.Inst.Level then
                            break
                        end
                        lastRow = i
                        i = i + 1
                    end
                end
                this:SetRowSelected(CreateFromClass(CellIndex, index.section, lastRow, CellIndexType.Row))
            end
        end

        --#region Guide
        EventManager.BroadcastInternalForLua(EnumEventType.Guide_ChangeView, {CUIResources.TeamQuickJoinWnd.Name})
        --#endregion
    end
end
CTeamQuickJoinMasterView.m_OnRowSelected_CS2LuaHook = function (this, index) 
    if index.type == CellIndexType.Row then
        local activityId = this.matchActivitySections[index.section].activities[index.row]
        if this.OnActivityClick ~= nil then
            GenericDelegateInvoke(this.OnActivityClick, Table2ArrayWithCount({activityId}, 1, MakeArrayClass(Object)))
        end
    end
end
CTeamQuickJoinMasterView.m_NumberOfRowsInSection_CS2LuaHook = function (this, section) 
    if this.matchActivitySections.Count <= section then
        return 0
    end
    local data = this.matchActivitySections[section]
    if data.activityId == 0 then
        return data.activities.Count
    else
        return 0
    end
end
CTeamQuickJoinMasterView.m_RefreshCellForRowAtIndex_CS2LuaHook = function (this, cell, index) 
    if index.type == CellIndexType.Section then
        local section = this.matchActivitySections[index.section]
        CommonDefs.GetComponent_GameObject_Type(cell, typeof(CTeamQuickJoinActivityItem)):Init(section.name, section.activityId, section.type)
    else
        local activityId = this.matchActivitySections[index.section].activities[index.row]
        local data = TeamMatch_Activities.GetData(activityId)
        CommonDefs.GetComponent_GameObject_Type(cell, typeof(CTeamQuickJoinActivityItem)):Init(data.Name, activityId, data.Type)
    end
end
CTeamQuickJoinMasterView.m_GetYiTiaoLong_CS2LuaHook = function (this) 
    local tf = this.transform:Find("Scroll View/Table")
    do
        local i = 0
        while i < tf.childCount do
            local go = tf:GetChild(i).gameObject
            local label = CommonDefs.GetComponentInChildren_GameObject_Type(go, typeof(UILabel))
            if label.text == LocalString.GetString("一条龙") then
                return go
            end
            i = i + 1
        end
    end
    return nil
end
CTeamQuickJoinMasterView.m_GetTabIndentifier_CS2LuaHook = function (this) 
    local go = this:GetCellByIndex(this.SelectedIndex)
    local label = CommonDefs.GetComponentInChildren_GameObject_Type(go, typeof(UILabel))
    if label ~= nil then
        return label.text
    end
    return ""
end
