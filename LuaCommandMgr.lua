LuaCommandMgr = {}
LuaCommandMgr.m_CommandList = {}
LuaCommandMgr.m_NowIdx = 0
LuaCommandMgr.m_MaxCommandLength = 0

function LuaCommandMgr:Clear(maxlen)
    self.m_CommandList = {}
    self.m_NowIdx = 0
    self.m_MaxCommandLength = maxlen
    g_ScriptEvent:BroadcastInLua("OnCommandUpdate")
end

function LuaCommandMgr:GetCurCommandInfo()
    if self.m_NowIdx <= 0 then
        return nil
    end
    local command = self.m_CommandList[self.m_NowIdx]
    return command.info
end

function LuaCommandMgr:Regist(executeAction, undoAction, info)
    self.m_NowIdx = self.m_NowIdx + 1
    local command = {executeAction = executeAction, undoAction = undoAction, info = info}
    self.m_CommandList[self.m_NowIdx] = command
    if command and command.executeAction then
        command.executeAction()
    end
    for i = self.m_NowIdx + 1, #self.m_CommandList do
        self.m_CommandList[i] = nil
    end
    if self.m_NowIdx > self.m_MaxCommandLength then
        local move = self.m_NowIdx - self.m_MaxCommandLength
        for i = 1, self.m_MaxCommandLength do
            self.m_CommandList[i] = self.m_CommandList[i + move]
        end
        for i = self.m_MaxCommandLength + 1, self.m_NowIdx do
            self.m_CommandList[i] = nil
        end
        self.m_NowIdx = self.m_MaxCommandLength
    end
    g_ScriptEvent:BroadcastInLua("OnCommandUpdate")
end

function LuaCommandMgr:Undo()
    if self.m_NowIdx <= 0 then
        return
    end
    local command = self.m_CommandList[self.m_NowIdx]
    if command and command.undoAction then
        command.undoAction()
    end
    self.m_NowIdx = self.m_NowIdx - 1
    local command = self.m_CommandList[self.m_NowIdx]
    if command and command.executeAction then
        command.executeAction()
    end
    g_ScriptEvent:BroadcastInLua("OnCommandUpdate")
end

function LuaCommandMgr:Redo()
    if self.m_NowIdx + 1 > #self.m_CommandList then
        return
    end
    self.m_NowIdx = self.m_NowIdx + 1
    local command = self.m_CommandList[self.m_NowIdx]
    if command and command.executeAction then
        command.executeAction()
    end
    g_ScriptEvent:BroadcastInLua("OnCommandUpdate")
end

function LuaCommandMgr:HasUndoCommand()
    return self.m_NowIdx > 0
end

function LuaCommandMgr:HasRedoCommand()
    return self.m_NowIdx < #self.m_CommandList
end