local UITable = import "UITable"
local UIScrollView = import "UIScrollView"
local UILabel = import "UILabel"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local QnCheckBox = import "L10.UI.QnCheckBox"
local UILongPressButton = import "L10.UI.UILongPressButton"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"

LuaWuYi2023XXSCRewardSelectWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaWuYi2023XXSCRewardSelectWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaWuYi2023XXSCRewardSelectWnd, "OkButton", "OkButton", GameObject)
RegistChildComponent(LuaWuYi2023XXSCRewardSelectWnd, "ItemNumLabel", "ItemNumLabel", UILabel)
RegistChildComponent(LuaWuYi2023XXSCRewardSelectWnd, "Table", "Table", UITable)
RegistChildComponent(LuaWuYi2023XXSCRewardSelectWnd, "ScrollView", "ScrollView", UIScrollView)
RegistChildComponent(LuaWuYi2023XXSCRewardSelectWnd, "RewardsTemplate", "RewardsTemplate", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaWuYi2023XXSCRewardSelectWnd, "m_Data")
RegistClassMember(LuaWuYi2023XXSCRewardSelectWnd, "m_SelectNumList")
RegistClassMember(LuaWuYi2023XXSCRewardSelectWnd, "m_CheckBoxInfo")

function LuaWuYi2023XXSCRewardSelectWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTipButtonClick()
	end)


	
	UIEventListener.Get(self.OkButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOkButtonClick()
	end)


    --@endregion EventBind end
end

function LuaWuYi2023XXSCRewardSelectWnd:Init()
	self.RewardsTemplate.gameObject:SetActive(false)

	self.m_Data = {}
	WuYi2023_AdvancedRewards.Foreach(function(key, data)
		if self.m_Data[data.Category] == nil then
			self.m_Data[data.Category] = {}
		end
		table.insert(self.m_Data[data.Category], key)
	end)
	self.m_SelectNumList = {}
	self.m_CheckBoxInfo = {}
	Extensions.RemoveAllChildren(self.Table.transform)
	WuYi2023_RewardsSelect.ForeachKey(function(key)
		self:InitRewardsTemplate(key)
	end)
	self:UpdateSelectNumLabel()
	self.Table:Reposition()
    self.ScrollView:ResetPosition()
end

function LuaWuYi2023XXSCRewardSelectWnd:InitRewardsTemplate(key)
	local data = WuYi2023_RewardsSelect.GetData(key)

	local obj = NGUITools.AddChild(self.Table.gameObject, self.RewardsTemplate.gameObject)
	obj:SetActive(true)

	local titleLabel = obj.transform:Find("TitleLabel"):GetComponent(typeof(UILabel))
	local selectNumLabel = obj.transform:Find("TitleLabel/SelectNumLabel"):GetComponent(typeof(UILabel))
	local grid = obj.transform:Find("Grid"):GetComponent(typeof(UIGrid))
	local itemTemplate = obj.transform:Find("ItemTemplate")

	itemTemplate.gameObject:SetActive(false)
	titleLabel.text = data.Desc
	self.m_SelectNumList[key] = {num = 0, label = selectNumLabel, data = data}
	Extensions.RemoveAllChildren(grid.transform)
	local list = self.m_Data[key]
	for i = 1, #list do
		local itemId = list[i]
		local item = NGUITools.AddChild(grid.gameObject, itemTemplate.gameObject)
		item:SetActive(true)
		self:InitItem(item, itemId, key)
	end
	grid:Reposition()
end

function LuaWuYi2023XXSCRewardSelectWnd:InitItem(item, itemId, key)
	local itemData = Item_Item.GetData(itemId)
	local data = WuYi2023_AdvancedRewards.GetData(itemId)

	local iconTexture = item.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
	local numLabel = item.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
	--local checkBox = item:GetComponent(typeof(QnCheckBox))
	local qualitySprite = item.transform:Find("QualitySprite"):GetComponent(typeof(UISprite))
	local longPressBtn = item:GetComponent(typeof(UILongPressButton))

	iconTexture:LoadMaterial(itemData.Icon)
	numLabel.text = data.Amount > 1 and data.Amount or ""
	local fashionData = Fashion_Fashion.GetDataBySubKey("ItemID",itemId)
	if fashionData then
		numLabel.text = SafeStringFormat3(LocalString.GetString("%d天"), fashionData.AvailableTime)
	end
	local zuoqiData = ZuoQi_ZuoQi.GetDataBySubKey("ItemID",itemId)
	if zuoqiData then
		numLabel.text = SafeStringFormat3(LocalString.GetString("%d天"), zuoqiData.AvailableTime)
	end
	-- checkBox.OnValueChanged = DelegateFactory.Action_bool(
    --     function(select)
    --         self:OnCheckBoxValueChanged(item, itemId, key, select)
    --     end
    -- )
	longPressBtn.OnLongPressDelegate = DelegateFactory.Action(function()
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemId, false, nil, CItemInfoMgrAlignType.Default, 0, 0, 0, 0)
    end)
	longPressBtn.OnClickDelegate = DelegateFactory.Action(function ()
		if self.m_CheckBoxInfo[key] == nil then
			self.m_CheckBoxInfo[key] = {}
		end
		self:OnCheckBoxValueChanged(item, itemId, key, not self.m_CheckBoxInfo[key][itemId])
	end)
	self:OnCheckBoxValueChanged(item, itemId, key, false)
	--checkBox:SetSelected(false, false)
	qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(itemData.Quality)
end

function LuaWuYi2023XXSCRewardSelectWnd:UpdateSelectNumLabel()
	local maxNum = 0
	local selectNum = 0
	for _, info in pairs(self.m_SelectNumList) do 
		maxNum = maxNum + info.data.number
		selectNum = selectNum + info.num
		info.label.text = SafeStringFormat3(LocalString.GetString("可选%s%d[ffffff]/%d"), info.num < info.data.number and "[ff5050]" or "[ffffff]", info.num, info.data.number)
	end
	self.ItemNumLabel.text = SafeStringFormat3(LocalString.GetString("%s%d[ffffff]/%d"), selectNum < maxNum and "[ff5050]" or "[ffffff]", selectNum, maxNum)
end

--@region UIEvent

function LuaWuYi2023XXSCRewardSelectWnd:OnTipButtonClick()
	g_MessageMgr:ShowMessage("WuYi2023XXSCRewardSelectWnd_ReadMe")
end

function LuaWuYi2023XXSCRewardSelectWnd:OnOkButtonClick()
	local maxNum = 0
	local selectNum = 0
	for _, info in pairs(self.m_SelectNumList) do 
		maxNum = maxNum + info.data.number
		selectNum = selectNum + info.num
	end
	if selectNum < maxNum then
		g_MessageMgr:ShowMessage("WuYi2023XXSCRewardSelectWnd_SelectNum_NotEnough")
		return
	end
	local msg = g_MessageMgr:FormatMessage("WuYi2023XXSCRewardSelectWnd_SelectItems_Confirm")
	local t = {}
	for key, info in pairs(self.m_CheckBoxInfo) do
		for itemId, select in pairs(info) do
			if select then
				table.insert(t, itemId)
			end
		end
	end
	MessageWndManager.ShowDelayOKCancelMessage(msg, DelegateFactory.Action(function () 
		Gac2Gas.XinXiangShiChengSelectItems(g_MessagePack.pack(t))
	end), nil, 5)
end

function LuaWuYi2023XXSCRewardSelectWnd:OnCheckBoxValueChanged(item, itemId, key, select)

	if self.m_CheckBoxInfo[key] == nil then
		self.m_CheckBoxInfo[key] = {}
	end

	local selectedNum = 0
	for itemId, _select in pairs(self.m_CheckBoxInfo[key]) do
		if _select then
			selectedNum = selectedNum + 1
		end
	end

	local rewardsSelectData = WuYi2023_RewardsSelect.GetData(key)

	if not self.m_CheckBoxInfo[key][itemId] and select then
		if selectedNum == rewardsSelectData.number then
			g_MessageMgr:ShowMessage("WuYi2023XXSCRewardSelectWnd_Forbid_Select")
			return
		end
		selectedNum = selectedNum + 1
	elseif self.m_CheckBoxInfo[key][itemId] and not select then
		selectedNum = selectedNum - 1
	end
	self.m_CheckBoxInfo[key][itemId] = select

	local selectedSprite = item.transform:Find("SelectedSprite")
	selectedSprite.gameObject:SetActive(select)

	self.m_SelectNumList[key].num = selectedNum
	self:UpdateSelectNumLabel()
end
--@endregion UIEvent

