local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local DefaultItemActionDataSource = import "L10.UI.DefaultItemActionDataSource"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemAccessListMgr=import "L10.UI.CItemAccessListMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"

local CUITexture = import "L10.UI.CUITexture"
local CClientHouseMgr=import "L10.Game.CClientHouseMgr"

local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local QnTableView = import "L10.UI.QnTableView"

local DelegateFactory  = import "DelegateFactory"

LuaHouseExchangeTerrainWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaHouseExchangeTerrainWnd, "Button", "Button", GameObject)
RegistChildComponent(LuaHouseExchangeTerrainWnd, "ButtonLabel", "ButtonLabel", UILabel)
RegistChildComponent(LuaHouseExchangeTerrainWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaHouseExchangeTerrainWnd, "PreviewTexture", "PreviewTexture", CUITexture)
RegistChildComponent(LuaHouseExchangeTerrainWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaHouseExchangeTerrainWnd, "NameLabel", "NameLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaHouseExchangeTerrainWnd,"m_Keys")

LuaHouseExchangeTerrainWnd.s_Channel = 0
LuaHouseExchangeTerrainWnd.s_Type = 0

function LuaHouseExchangeTerrainWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.Button.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButtonClick()
	end)


    --@endregion EventBind end

    if LuaHouseExchangeTerrainWnd.s_Channel==0 then
        self.TitleLabel.text = LocalString.GetString("更换笔刷·基础")
    else
        self.TitleLabel.text = LocalString.GetString("更换笔刷")
    end
end

function LuaHouseExchangeTerrainWnd:Init()
    self.m_Keys = {}
    if LuaHouseExchangeTerrainWnd.s_Channel>0 then
        table.insert( self.m_Keys,0 )
    else
        --看看当前是不是基础通道
        if LuaHouseExchangeTerrainWnd.s_Type>0 then
            table.insert( self.m_Keys,0 )
        end
    end
    House_AllGround.ForeachKey(function(key)
        if key>0 then
            table.insert(self.m_Keys,key)
        end
    end)

    self.TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_Keys
        end,
        function(item,row)
            self:InitItem(item,self.m_Keys[row+1])
        end)

    self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function(index)
        self:OnSelectAtRow(index)
    end)
    self.TableView:ReloadData(true,false)

    local bindedBursh = CClientHouseMgr.Inst.mFurnitureProp3.GroundContainer.BindedBrush
    local type = bindedBursh[LuaHouseExchangeTerrainWnd.s_Channel+1]
    local selectedRow = 0
    for i,v in ipairs(self.m_Keys) do
        if v==type then
            selectedRow = i-1
            break
        end
    end
    self.TableView:SetSelectRow(selectedRow,true)
end

function LuaHouseExchangeTerrainWnd:InitItem(item,key)
    local tf = item.transform
    local nameLabel = tf:Find("NameLabel"):GetComponent(typeof(UILabel))
    local lock = tf:Find("Lock").gameObject
    local kongdi = tf:Find("KongDi").gameObject
    local icon = tf:Find("Icon"):GetComponent(typeof(CUITexture))
    if key>0 then
        nameLabel.text = House_AllGround.GetData(key).Name
        kongdi:SetActive(false)
    else
        if LuaHouseExchangeTerrainWnd.s_Channel==0 then
            --默认地表
            nameLabel.text = House_AllGround.GetData(key).Name
            kongdi:SetActive(false)
        else
            nameLabel.text = LocalString.GetString("空")
            kongdi:SetActive(true)
        end
    end
    icon:LoadMaterial(House_AllGround.GetData(key).Icon)

    if key>0 then
        local enabledBrush = CClientHouseMgr.Inst.mFurnitureProp3.GroundContainer.EnabledBrush
        local enabled = enabledBrush:GetBit(key)
        lock:SetActive(not enabled)
    else
        lock:SetActive(false)
    end
end
function LuaHouseExchangeTerrainWnd:OnSelectAtRow(row)
    local key = self.m_Keys[row+1]
    if key>0 then
        self.NameLabel.text = House_AllGround.GetData(key).Name
    else
        self.NameLabel.text = LocalString.GetString("空")
    end

    if LuaHouseExchangeTerrainWnd.s_Type==key then
        self.Button:SetActive(false)
    elseif key>0 then
        self.Button:SetActive(true)
        local enabledBrush = CClientHouseMgr.Inst.mFurnitureProp3.GroundContainer.EnabledBrush
        local enabled = enabledBrush:GetBit(key)
        self.ButtonLabel.text = enabled and LocalString.GetString("更换") or LocalString.GetString("解锁")
    else
        self.Button:SetActive(true)
        self.ButtonLabel.text = LocalString.GetString("更换")
    end
    
    self.PreviewTexture:LoadMaterial(House_AllGround.GetData(key).PreviewPic)
end

--@region UIEvent

function LuaHouseExchangeTerrainWnd:OnButtonClick()
    local type = self.m_Keys[self.TableView.currentSelectRow+1]
    if self.ButtonLabel.text==LocalString.GetString("更换") then
        local name1 = House_AllGround.GetData(LuaHouseExchangeTerrainWnd.s_Type).Name
        local name2 = House_AllGround.GetData(type).Name
        local msg = g_MessageMgr:FormatMessage("House_Ground_Change_Confirm",name1,name2)
        if type==0 then
            msg = g_MessageMgr:FormatMessage("Clear_DiBiao_Confirm")
        end
        g_MessageMgr:ShowOkCancelMessage(msg,function()
            Gac2Gas.RequestBindHouseGroundBrush(LuaHouseExchangeTerrainWnd.s_Channel+1,type)
        end, nil, nil, nil, false)

    else
        local templateId = House_AllGround.GetData(type).ItemID
        local find, pos, itemId = CItemMgr.Inst:FindItemByTemplateId(EnumItemPlace.Bag, templateId)
        if find then
            local action = DefaultItemActionDataSource.Create(1,
                { function() 
                    CItemInfoMgr.CloseItemInfoWnd()
                    Gac2Gas.RequestUseItem(1, pos, itemId, "") 
                end },
                { LocalString.GetString("使用") })

            CItemInfoMgr.ShowLinkItemInfo(itemId, false, action, AlignType2.Default, 0,0,0,0)
        else
            CItemAccessListMgr.Inst:ShowItemAccessInfo(templateId, false, self.Button.transform, AlignType.Right)
        end
    end
end


--@endregion UIEvent

function LuaHouseExchangeTerrainWnd:OnEnable()
    g_ScriptEvent:AddListener("BindHouseGroundBrushType", self, "OnBindHouseGroundBrushType")
    g_ScriptEvent:AddListener("EnableHouseGroundBrushType", self, "OnEnableHouseGroundBrushType")
end
function LuaHouseExchangeTerrainWnd:OnDisable()
    g_ScriptEvent:RemoveListener("BindHouseGroundBrushType", self, "OnBindHouseGroundBrushType")
    g_ScriptEvent:RemoveListener("EnableHouseGroundBrushType", self, "OnEnableHouseGroundBrushType")
end

function LuaHouseExchangeTerrainWnd:OnBindHouseGroundBrushType(brushPos, brushType)
    --更换完之后直接关闭？
    CUIManager.CloseUI(CLuaUIResources.HouseExchangeTerrainWnd)
end
function LuaHouseExchangeTerrainWnd:OnEnableHouseGroundBrushType(brushType)
    local targetIdx = -1
    for i,v in ipairs(self.m_Keys) do
        if v==brushType then
            targetIdx = i-1
        end
    end
    if targetIdx>=0 then
        local item = self.TableView:GetItemAtRow(targetIdx)
        self:InitItem(item,brushType)
        self:OnSelectAtRow(targetIdx)
    end
end

function LuaHouseExchangeTerrainWnd:OnDestroy()
    LuaHouseExchangeTerrainWnd.s_Channel = 0
    LuaHouseExchangeTerrainWnd.s_Type = 0
end
