local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
local CScene = import "L10.Game.CScene"
local CPayMgr=import "L10.Game.CPayMgr"
local CScheduleMgr = import "L10.Game.CScheduleMgr"

LuaOffWorldPassMgr = {}
LuaOffWorldPassMgr.MiddleStageData = nil
LuaOffWorldPassMgr.OpenTabIndex = nil
LuaOffWorldPassMgr.IsInPlay = function()
	if CScene.MainScene then
		local gamePlayId = CScene.MainScene.GamePlayDesignId
		if gamePlayId == OffWorldPass_Setting.GetData().JiaoshanGameplayId then
			return true
		end
	end
	return false
end

function LuaOffWorldPassMgr:ShowRuleWnd()
    local imagePaths = {
		"UI/Texture/NonTransparent/Material/offworldplay2tipwnd_bg_01.mat",
		"UI/Texture/NonTransparent/Material/offworldplay2tipwnd_bg_02.mat",
		"UI/Texture/NonTransparent/Material/offworldplay2tipwnd_bg_03.mat",
		"UI/Texture/NonTransparent/Material/offworldplay2tipwnd_bg_04.mat",
	  }
	  local msgs = {
		  g_MessageMgr:FormatMessage("CaiDie_Guide_Message1"),
		  g_MessageMgr:FormatMessage("CaiDie_Guide_Message2"),
		  g_MessageMgr:FormatMessage("CaiDie_Guide_Message3"),
		  g_MessageMgr:FormatMessage("CaiDie_Guide_Message4"),
	  }
	  LuaImageRuleMgr:ShowCommonImageRuleWnd(imagePaths, msgs)
end


LuaOffWorldPassMgr.IsInPlay2 = function()
	if CScene.MainScene then
		local gamePlayId = CScene.MainScene.GamePlayDesignId
		if gamePlayId == OffWorldPass_Setting.GetData().CaidiezhenGameplayId then
			return true
		end
	end
	return false
end

LuaOffWorldPassMgr.CheckSWWeapon = function(checkId)
	if CClientMainPlayer.Inst ~= nil and CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.TempPlayData, typeof(UInt32), EnumTempPlayDataKey_lua.eSelfEditedOffWorldCreature) then
		local playDataU = CommonDefs.DictGetValue(CClientMainPlayer.Inst.PlayProp.TempPlayData, typeof(UInt32), EnumTempPlayDataKey_lua.eSelfEditedOffWorldCreature)
		local info = MsgPackImpl.unpack(playDataU.Data.Data)
		if info and info.Count > 0 then
			local fashionId = info[4]
			if fashionId == checkId then
				return true
			end
		end
	end
	return false
end

LuaOffWorldPassMgr.GetSWWeaponData = function(checkId)
	if CClientMainPlayer.Inst ~= nil and CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.TempPlayData, typeof(UInt32), EnumTempPlayDataKey_lua.eSelfEditedOffWorldCreature) then
		local playDataU = CommonDefs.DictGetValue(CClientMainPlayer.Inst.PlayProp.TempPlayData, typeof(UInt32), EnumTempPlayDataKey_lua.eSelfEditedOffWorldCreature)
		local info = MsgPackImpl.unpack(playDataU.Data.Data)
		if info and info.Count > 0 then
			local fashionId = info[4]
			if fashionId == checkId then
				return info
			end
		end
	end
end

LuaOffWorldPassMgr.GetSWWeaponType = function(checkId)
	if CClientMainPlayer.Inst ~= nil and CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.TempPlayData, typeof(UInt32), EnumTempPlayDataKey_lua.eSelfEditedOffWorldCreature) then
		local playDataU = CommonDefs.DictGetValue(CClientMainPlayer.Inst.PlayProp.TempPlayData, typeof(UInt32), EnumTempPlayDataKey_lua.eSelfEditedOffWorldCreature)
		local info = MsgPackImpl.unpack(playDataU.Data.Data)
		if info and info.Count > 0 then
			local fashionId = info[4]
			if fashionId == checkId then
				return info[5]
			end
		end
	end
	return 0
end

LuaOffWorldPassMgr.GetWeaponShowArgs = function(data)
	--data.weaponType data.wuxing
	local dataTable = {
		{90,0,0.69,4.87, 0, 90, 90},
		{-59,0,-0.2,2.87, 0,-115,-90},
		{79,0,0.5,2.58, 0, 90, 90},
		{20,0,0,2.44, 0,58,90},
		{90,0,0.3,1.71, 0, 90, 90},
	}

	return dataTable[data.weaponType]
end

LuaOffWorldPassMgr.DoOffWorldPassPlayInfo = function(playInfoU, renew)
	LuaOffWorldPassMgr.page1data = {}
	LuaOffWorldPassMgr.page2data = {}
	LuaOffWorldPassMgr.page3data = nil

  local totalInfo = MsgPackImpl.unpack(playInfoU)
  if not totalInfo then
    return
  end

  local list = TypeAs(totalInfo, typeof(MakeGenericClass(List, Object)))

  if list then
    local info1 = list[0]
    if info1 then
      local level = tonumber(info1[0])
      local coin = tonumber(info1[1])
      local pass1 = tonumber(info1[2])
      local pass2 = tonumber(info1[3])
      local pass3 = tonumber(info1[4])
      local pass2enable = tonumber(info1[5])
      local pass3enable = tonumber(info1[6])
      LuaOffWorldPassMgr.page1data = {level = level, coin = coin,pass1 = pass1,pass2 = pass2,pass3 = pass3,pass2enable = pass2enable,pass3enable = pass3enable}
    end
    local info2 = list[1]
    if info2 then
      local passtime = tonumber(info2[0])
      local fuben1coin = tonumber(info2[1])
	  -- 副本2就是彩蝶镇 目前只有副本2的信息是有效的
      local fuben2coin = tonumber(info2[2])
      local fuben1get = tonumber(info2[3])
	  -- 副本 是否已经领过奖励了
      local fuben2get = tonumber(info2[4])
      local fubenCount = tonumber(info2[5])
      LuaOffWorldPassMgr.page2data = {passtime = passtime,fuben1coin = fuben1coin,fuben2coin= fuben2coin,fuben1get=fuben1get,fuben2get=fuben2get, fubenCount = fubenCount}
    end
		--

		if CClientMainPlayer.Inst ~= nil and CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.TempPlayData, typeof(UInt32), EnumTempPlayDataKey_lua.eSelfEditedOffWorldCreature) then
			local playDataU = CommonDefs.DictGetValue(CClientMainPlayer.Inst.PlayProp.TempPlayData, typeof(UInt32), EnumTempPlayDataKey_lua.eSelfEditedOffWorldCreature)
			local info = MsgPackImpl.unpack(playDataU.Data.Data)
			if info and info.Count > 0 then
				LuaOffWorldPassMgr.page3data = {}
				local weaponType = info[0]
				local wuxing = info[1]
				local name = info[2]
				local status = info[3]
				local fashionId = info[4]
				local swstage = info[5]

				LuaOffWorldPassMgr.page3data = {swstage = swstage,weaponType = weaponType, wuxing = wuxing, name = name, status = status, fashionId = fashionId}
			else
				LuaOffWorldPassMgr.page3data = {swstage = 0}
			end
		end
		if not LuaOffWorldPassMgr.page3data then
--      神武进度(0未领取虚态,1虚态,2实态)
      LuaOffWorldPassMgr.page3data = {swstage = 0}
		end

		if CUIManager.IsLoaded(CLuaUIResources.OffWorldPassMainWnd) then
			g_ScriptEvent:BroadcastInLua("UpdateBuyOffWorldPassResult")
		end

		if renew then
			CUIManager.ShowUI(CLuaUIResources.OffWorldPassMainWnd)
		end
	end
end
-- 返回界面信息
-- @param playInfoU    {
-- 		[1] = {				-- 对应服务器的EnumOffWorldDataIdx
-- 			当前等级,
-- 			当前代币,
--			当前普通通行证领取状况 bitmap, 最低位是1级奖励
--			当前豪华通行证领取状况 bitmap,
--			当前闪耀通行证领取状况 bitmap,
--			是否解锁豪华通行证,
--			是否解锁闪耀通行证,
--          上次打开界面时代币数量,
-- 		},
-- 		[2] = { 			-- 对应服务器的EnumOffWorldWeekDataIdx
-- 			本周数据过期时间,
--			蛟山副本本周获得代币,
--			彩蝶副本本周获得代币,
--			蛟山副本本周全完成是否领取,
--			彩蝶副本本周全完成是否领取,
--         	本周彩蝶镇参与次数，
-- 		}
-- 		[3] = {chestIdx1 = count1, ...}
--    [4] = ,      -- 龙血池商店解锁进度, 这边用不上
--    [5] = {
--      神武进度(0未领取虚态,1虚态,2实态)
--    }
-- 	}
function Gas2Gac.OffWorldPlayerQueryPlayInfoRet(playInfoU)
  --local temp = bit.band(rewardBitmap, bit.lshift(1, key-1))
	LuaOffWorldPassMgr.DoOffWorldPassPlayInfo(playInfoU, true)
end

-- 请求购买不同品质通行证返回结果
-- @param bSuccess 是否成功 目前有返回就是true
-- @param passId   通行证品质Id 奢华 = 2, 闪耀 = 3
-- EnumOffWorldPassType = { basic = 1, luxury = 2, epic = 3 }
function Gas2Gac.ReplyBuyOffWorldPassResult(bSuccess, passId)
	if bSuccess then
		local passData = OffWorldPass_RmbPass.GetData(passId)
		local pid=CPayMgr.Inst:PIDConverter(passData.PID)
		CPayMgr.Inst:BuyProduct(pid,0)
	end
end

-- 获得通行证奖励结果
-- @param playInfoU 	同OffWorldPlayerQueryPlayInfoRet
-- @param rewardItemsU 	{itemId1, itemId2, ...}
function Gas2Gac.OffWorldCollectRewardSuccess(playInfoU, rewardItemsU)
	LuaOffWorldPassMgr.DoOffWorldPassPlayInfo(playInfoU)

  local rewardInfo = MsgPackImpl.unpack(rewardItemsU)
  if not rewardInfo then
    return
  end
  local list = TypeAs(rewardInfo, typeof(MakeGenericClass(List, Object)))

	if list then
		local level = tonumber(list[0])
		local passType = tonumber(list[1])

    local data = OffWorldPass_PassReward.GetData(level)
		local itemTable = {data.basic,data.luxury,data.epic}
		local items = itemTable[passType]

		if items.Length >0 then
			local itemId = items[0]
			if itemId then
				local item = Item_Item.GetData(itemId)
				g_MessageMgr:ShowMessage('OffWorldPass_Reward_Send',item.Name)
				g_ScriptEvent:BroadcastInLua("UpdateSingleBuyOffWorldPassResult",level,passType)
			end
		end
	end
end

-- 玩家通行证升级
-- @param bPassRedDot 通行证有未领取奖励
-- @param bPlayRedDot 副本本周全完成 有未领取奖励
function Gas2Gac.OffWorldPassNeedRedDot(bPassRedDot, bPlayRedDot)
	if bPassRedDot or bPlayRedDot then
		CScheduleMgr.Inst:SetAlertState(42010064, CScheduleMgr.EnumAlertState.Show, true)
		CLuaScheduleMgr.UpdateAlertStatus(42010064, LuaEnumAlertState.Show)
	else
		CScheduleMgr.Inst:SetAlertState(42010064, CScheduleMgr.EnumAlertState.Hide, true)
		CLuaScheduleMgr.UpdateAlertStatus(42010064, LuaEnumAlertState.Hide)
	end
	g_ScriptEvent:BroadcastInLua("OffWorldPassNeedRedDot", bPassRedDot, bPlayRedDot)
end

-- 成功解锁不同品质的通行证
-- @param passId   通行证品质Id 奢华 = 2, 闪耀 = 3
function Gas2Gac.UseOffWorldPassRMBPackageSuccess(passId)
	if LuaOffWorldPassMgr.page1data then
		if passId == 2 then
			LuaOffWorldPassMgr.page1data.pass2enable = 1
		elseif passId == 3 then
			LuaOffWorldPassMgr.page1data.pass3enable = 1
		end
    g_ScriptEvent:BroadcastInLua("UpdateBuyOffWorldPassResult")
	Gac2Gas.GetOffWorldPlayInfo()
	end
end

-- 触发点npc视野变化
-- @param curr  当前视野内人数
-- @param total 场景内总人数
function Gas2Gac.JiaoShanTrapAOIChange(engineId, curr, total)
end

-- 更新血精石血量
-- @param engineId  血精石npc或者南宫长英Boss,传0可能是boss还没刷出来
-- @param hp        血精石血量
function Gas2Gac.SendJiaoShanBloodstoneHp(engineId, hp)
    LuaOffWorldPassMgr.TotalHp = hp
    g_ScriptEvent:BroadcastInLua("UpdateJiaoShanBloodstoneHp")
end

-- 接收玩法最终结果, 打开界面
-- @param playResultInfo   {第一层血精石血量, 第二层, 第三层, 理论获得代币}
-- @param playResultInfo   {进副本前代币, 增加代币, 通行证是否已经满级(1是,0不是)}
function Gas2Gac.SendJiaoShanPlayResult(playResultInfoUD, playerScoreInfoUD)
    local playInfo   = MsgPackImpl.unpack(playResultInfoUD)
    local playerInfo = MsgPackImpl.unpack(playerScoreInfoUD)
    LuaOffWorldPassMgr.GameResult = {playInfo[0], playInfo[1], playInfo[2], playInfo[3], playerInfo[0], playerInfo[1], playerInfo[2]}
    -- print("SendJiaoShanPlayResult 1", playInfo[0], playInfo[1], playInfo[2], playInfo[3])
    -- print("SendJiaoShanPlayResult 2", playerInfo[0], playerInfo[1], playerInfo[2])
	CUIManager.ShowUI(CLuaUIResources.OffWorldPlay1ResultWnd)
end

-- 播放掉落特效
function Gas2Gac.ShowJiaoShanDropFx(npcEngineId)

end

-- 解锁龙血池进度
-- @param progress  当前进度, 达到策划表里BloodShopUnlockRequire时即解锁成功
-- @param count     注入了多少
function Gas2Gac.UnlockOffWorldBloodShopRet(progress, count)
	LuaOffWorldPassMgr.NowProgress = progress
	if CUIManager.IsLoaded(CLuaUIResources.OffWorldPlay1ShopWnd) then
		g_ScriptEvent:BroadcastInLua("UpdateOffWorldPlay1ShopWnd")
	else
		CUIManager.ShowUI(CLuaUIResources.OffWorldPlay1ShopWnd)
	end
end

-- 界面信息
-- @param progress  当前进度, 达到策划表里BloodShopUnlockRequire时即解锁成功
-- @param onShelfUD 上架物品信息 {1, itemId, 是否购买, 2, itemId, 是否购买, 3, itemId, 是否购买}
function Gas2Gac.GetOffWorldBloodShopInfoRet(progress, onShelfUD)
	LuaOffWorldPassMgr.ShopInfo = nil
	local onShelf = MsgPackImpl.unpack(onShelfUD)
	if onShelf then
		local res = {}
		if progress == OffWorldPass_Setting.GetData().BloodShopUnlockRequire then
			for i = 0, 2 do
				res[onShelf[i*3]] = {onShelf[i*3+1],onShelf[i*3+2]}
			end
			LuaOffWorldPassMgr.ShopInfo = res
		end
	end
	LuaOffWorldPassMgr.NowProgress = progress
	g_ScriptEvent:BroadcastInLua("UpdateOffWorldPlay1ShopWnd")
	if not CUIManager.IsLoaded(CLuaUIResources.OffWorldPlay1ShopWnd) then
		CUIManager.ShowUI(CLuaUIResources.OffWorldPlay1ShopWnd)
	end
end


function Gas2Gac.CaiDieSignUpResult(bSuccess)
    -- print("CaiDieSignUpResult", bSuccess)
end

-- 进入彩蝶镇副本的规则说明
function Gas2Gac.CaiDieShowTips()
	LuaOffWorldPassMgr:ShowRuleWnd()
end

-- 彩蝶镇玩法信息

-- @param destructionPrec  冥婚现场破坏进度
function Gas2Gac.SyncCaiDieDestructionInfo(destructionPrec, cursedPlayerId)
	LuaOffWorldPassMgr.Play2StageData = {1, destructionPrec, cursedPlayerId}
	LuaOffWorldPassMgr.cursedPlayerId = cursedPlayerId
	g_ScriptEvent:BroadcastInLua("UpdateOffWorldPlay2Data")
	g_ScriptEvent:BroadcastInLua("UpdateOffWorldPlay2TaskData")
end

-- @param stone 玩家自己获得的血精石数量
function Gas2Gac.SyncCaiDiePlayerStoneInfo(stone)
	LuaOffWorldPassMgr.Play2Stome = stone

	g_ScriptEvent:BroadcastInLua("UpdateOffWorldPlay2StoneData")
end

-- 进入打boss阶段, 同步boss血量
function Gas2Gac.SyncCaiDieBossInfo(bossHpPerc)
	LuaOffWorldPassMgr.Play2StageData = {2, bossHpPerc}
	g_ScriptEvent:BroadcastInLua("UpdateOffWorldPlay2Data")
end

-- 彩蝶镇最终结果
function Gas2Gac.SendCaiDiePlayResult(score, playerScoreInfo)
    local playerInfo = MsgPackImpl.unpack(playerScoreInfo)
    LuaOffWorldPassMgr.Game2Result = {score, playerInfo[0], playerInfo[1], playerInfo[2]}
    -- print("SendJiaoShanPlayResult 1", playInfo[0], playInfo[1], playInfo[2], playInfo[3])
    -- print("SendJiaoShanPlayResult 2", playerInfo[0], playerInfo[1], playerInfo[2])
		CUIManager.ShowUI(CLuaUIResources.OffWorldPlay2ResultWnd)
end

-- 同步领取的任务
-- @param taskType  任务类型, 对应OffWorldPass_CaiDieTask.type, 为0则代表无任务
-- @param bStart    是否需要打开小玩法界面, 领到任务时是false, 和npc对话开始任务时为true
function Gas2Gac.SyncCaiDieTaskInfo(taskType, bStart)
	LuaOffWorldPassMgr.Play2TaskData = {taskType, bStart}
	if bStart then
		if taskType == 1 or taskType == 2 then
			LuaOffWorldPassMgr.Play2Game1Type = taskType
			CUIManager.ShowUI(CLuaUIResources.OffWorldPlay2Game1Wnd)
		elseif taskType == 4 then
			CUIManager.ShowUI(CLuaUIResources.OffWorldPlay2Game2Wnd)
		end
	end
	g_ScriptEvent:BroadcastInLua("UpdateOffWorldPlay2TaskData")
end

-- 返回界面信息
-- @param playDataU    {
--    [1] = weaponType,   -- 类型 1: 不归, 2 ...
--    [2] = wuxing,       -- 五行 1: 金 2: 木...
--    [3] = name,         -- 玩家自定义名字
--    [4] = status,       -- 状态 参考服务器EnumOffWorldFashionStatus
--    [5] = fashionId,    -- 玩家时装库里的神武武器拓本时装id
--    [6] = weaponStatus, -- 对应EnumOffWorldCreatureStatus 1虚态 2实态
--  }
-- function Gas2Gac.SyncOffWorldFashion(playDataU)
--   local playData = MsgPackImpl.unpack(playDataU)
--   print("SyncOffWorldFashion", playData[0], playData[1], playData[2], playData[3], playData[4])
--   LuaOffWorldPassMgr.fashionId = playData[4]
-- end


function Gas2Gac.CaiDieOnPlayerBeginCurse(cursedPlayerId)
	CUIManager.CloseUI(CLuaUIResources.OffWorldPlay2Game1Wnd)
	CUIManager.CloseUI(CLuaUIResources.OffWorldPlay2Game2Wnd)
	CUIManager.CloseUI("CommonImageRuleWnd")
end

-- @param itemIdsSent  {itemId1, count1, itemId2, Count2, ...}
function Gas2Gac.OffWorldCollectWeekReward(itemIdsSent)
	local itemTable = {}
	local itemList = TypeAs(MsgPackImpl.unpack(itemIdsSent), typeof(MakeGenericClass(List, Object)))
	do
		local i = 0
		while i < itemList.Count do
			local itemId = tonumber(itemList[i])
			local itemNum = tonumber(itemList[i+1])
			table.insert(itemTable,{itemId,itemNum})
			i = i + 2
		end
	end

	LuaOffWorldPassMgr.BonusItemTable = itemTable
	CUIManager.ShowUI(CLuaUIResources.OffWorldPassReward1Wnd)

	LuaOffWorldPassMgr.GetBonusPlayType = 2
	g_ScriptEvent:BroadcastInLua("UpdateOffWorldPlayBonusType")
end

-- @param weaponType    类型 1: 不归, 2 ...
-- @param wuxing        五行 1: 金 2: 木...
function Gas2Gac.OffWorldGetRealCreature(weaponType, wuxing)
	LuaOffWorldPassMgr.WDCreate = {weaponType, wuxing}
	CUIManager.ShowUI(CLuaUIResources.OffWorldPassReward2Wnd)
end

LuaOffWorldPassMgr.m_CurrScore = nil

function Gas2Gac.SyncJiaoShanCurrScore(currScore)
	LuaOffWorldPassMgr.m_CurrScore = currScore
	g_ScriptEvent:BroadcastInLua("UpdateJiaoShanStoneCount", currScore)
end
