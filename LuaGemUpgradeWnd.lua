local LocalString = import "LocalString"
local CCommonLuaScript=import "L10.UI.CCommonLuaScript"

CLuaGemUpgradeWnd = class()
RegistClassMember(CLuaGemUpgradeWnd,"closeBtn")
RegistClassMember(CLuaGemUpgradeWnd,"updateScrollView")
RegistClassMember(CLuaGemUpgradeWnd,"updateDetailView")
RegistClassMember(CLuaGemUpgradeWnd,"m_TitleLabel")
CLuaGemUpgradeWnd.m_HoleSetIndex=0

function CLuaGemUpgradeWnd:Awake()
    self.closeBtn = self.transform:Find("Wnd_Bg_Secondary_1/CloseButton").gameObject
    local script = self.transform:Find("Anchor/Offset/GemScrollView"):GetComponent(typeof(CCommonLuaScript))
    script:Awake()
    self.updateScrollView = script.m_LuaSelf
    script = self.transform:Find("Anchor/Offset/GemDetailView"):GetComponent(typeof(CCommonLuaScript))
    script:Awake()
    self.updateDetailView = script.m_LuaSelf

    self.m_TitleLabel = self.transform:Find("Wnd_Bg_Secondary_1/TitleLabel"):GetComponent(typeof(UILabel))

end

function CLuaGemUpgradeWnd:Init( )
    if CLuaEquipMgr.m_GemUpdateSelectedEquipment == nil or CLuaEquipMgr.m_GemUpdateSelectedEquipment.Equip == nil then
        return
    end
    if CLuaEquipMgr.m_GemUpdateSelectedEquipment.Equip.IsFaBao then
        self.m_TitleLabel.text = LocalString.GetString("纹饰升级")
    else
        self.m_TitleLabel.text = LocalString.GetString("宝石升级")
    end
    self.updateScrollView.StoneSelected = function(info)
        self.updateDetailView:Init(info)
    end
    self.updateScrollView:Init()
end

function CLuaGemUpgradeWnd:OnDestroy()
    CLuaGemUpgradeWnd.m_HoleSetIndex=0
end