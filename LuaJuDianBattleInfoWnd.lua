local QnTabView = import "L10.UI.QnTabView"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaJuDianBattleInfoWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaJuDianBattleInfoWnd, "ShowArea", "ShowArea", QnTabView)

--@endregion RegistChildComponent end

function LuaJuDianBattleInfoWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaJuDianBattleInfoWnd:Init()
    if LuaJuDianBattleMgr.InfoWndTab and LuaJuDianBattleMgr.InfoWndTab<=2 and LuaJuDianBattleMgr.InfoWndTab>=0 then
        self.ShowArea:ChangeTo(LuaJuDianBattleMgr.InfoWndTab)
    else
        self.ShowArea:ChangeTo(0)
    end
end

function LuaJuDianBattleInfoWnd:OnEnable()
end

function LuaJuDianBattleInfoWnd:OnDisable()
    LuaJuDianBattleMgr.InfoWndTab = 0
end

--@region UIEvent

--@endregion UIEvent

