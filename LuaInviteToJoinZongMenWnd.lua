local QnCheckBox = import "L10.UI.QnCheckBox"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local AlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local CIMMgr = import "L10.Game.CIMMgr"
local GroupInfo = import "L10.UI.CFriendContactListView+GroupInfo"
local RelationshipType = import "L10.Game.RelationshipType"
local FriendItemData = import "FriendItemData"
local CGuildMgr = import "L10.Game.CGuildMgr"
local GuildDefine = import "L10.Game.GuildDefine"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"

LuaInviteToJoinZongMenWnd = class()

RegistChildComponent(LuaInviteToJoinZongMenWnd,"m_TopTabs","TopTabs", QnRadioBox)
RegistChildComponent(LuaInviteToJoinZongMenWnd,"m_InviteFriendView","InviteFriendView", GameObject)
RegistChildComponent(LuaInviteToJoinZongMenWnd,"m_InviteFriendViewQnRadioBox","InviteFriendViewQnRadioBox", QnRadioBox)
RegistChildComponent(LuaInviteToJoinZongMenWnd,"m_InviteGuildView","InviteGuildView", GameObject)
RegistChildComponent(LuaInviteToJoinZongMenWnd,"m_InviteGuildViewQnRadioBox","InviteGuildViewQnRadioBox", QnRadioBox)
RegistChildComponent(LuaInviteToJoinZongMenWnd,"m_ScrollView","ScrollView", UIScrollView)
RegistChildComponent(LuaInviteToJoinZongMenWnd,"m_Grid","Grid", UIGrid)
RegistChildComponent(LuaInviteToJoinZongMenWnd,"m_Template","Template", GameObject)
RegistChildComponent(LuaInviteToJoinZongMenWnd,"m_NoneFriendsLabel","NoneFriendsLabel", GameObject)
RegistChildComponent(LuaInviteToJoinZongMenWnd,"m_NoneGuildFriendsLabel","NoneGuildFriendsLabel", GameObject)
RegistChildComponent(LuaInviteToJoinZongMenWnd,"m_AllChooseQnCheckBox","AllChooseQnCheckBox", QnCheckBox)
RegistChildComponent(LuaInviteToJoinZongMenWnd,"m_ChooseNumLabel","ChooseNumLabel", UILabel)
RegistChildComponent(LuaInviteToJoinZongMenWnd,"m_InviteButton","InviteButton", GameObject)
RegistChildComponent(LuaInviteToJoinZongMenWnd,"m_ChooseNumLabel","ChooseNumLabel", UILabel)
RegistChildComponent(LuaInviteToJoinZongMenWnd,"m_GuildSelectBtn","GuildSelectBtn", GameObject)
RegistChildComponent(LuaInviteToJoinZongMenWnd,"m_SelectSorts","SelectSorts", QnSelectableButton)
RegistChildComponent(LuaInviteToJoinZongMenWnd,"m_ArrowSprite","ArrowSprite", GameObject)
RegistChildComponent(LuaInviteToJoinZongMenWnd,"m_MembersView","MembersView", QnTableView)

RegistClassMember(LuaInviteToJoinZongMenWnd, "m_Friends")
RegistClassMember(LuaInviteToJoinZongMenWnd, "m_SelectedList")
RegistClassMember(LuaInviteToJoinZongMenWnd, "m_SelectedNum")
RegistClassMember(LuaInviteToJoinZongMenWnd, "m_AllGuildMemberInfos")
RegistClassMember(LuaInviteToJoinZongMenWnd, "m_GuildViewIndex")
RegistClassMember(LuaInviteToJoinZongMenWnd, "m_FriendViewIndex")
RegistClassMember(LuaInviteToJoinZongMenWnd, "m_invitedList")
RegistClassMember(LuaInviteToJoinZongMenWnd, "m_IsFriendView")
RegistClassMember(LuaInviteToJoinZongMenWnd, "m_GuildSortIndex")
RegistClassMember(LuaInviteToJoinZongMenWnd, "m_List")
RegistClassMember(LuaInviteToJoinZongMenWnd, "m_AllSectMemberIds")
RegistClassMember(LuaInviteToJoinZongMenWnd, "m_SectInviteCd")
RegistClassMember(LuaInviteToJoinZongMenWnd, "m_LastInviteFriendTime")
RegistClassMember(LuaInviteToJoinZongMenWnd, "m_LastInviteGuildTime")

function LuaInviteToJoinZongMenWnd:Init()
    self.m_ChooseNumLabel.text = SafeStringFormat3(LocalString.GetString("已选%d人"),0)
    self.m_SelectedList = {}
    self.m_SelectedNum = 0
    self.m_invitedList = {}
    self.m_Template:SetActive(false)
    self.m_NoneFriendsLabel:SetActive(false)
    self.m_NoneGuildFriendsLabel:SetActive(false)
    self.m_GuildSelectBtn:SetActive(false)
    if CClientMainPlayer.Inst then
        self.m_GuildSelectBtn:SetActive(CClientMainPlayer.Inst.BasicProp.GuildId ~= 0)
    end
    self.m_TopTabs.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnTopTabsSelect(btn,index)
    end)
    self.m_AllChooseQnCheckBox.OnValueChanged = DelegateFactory.Action_bool(
        function(select)
            self:OnSelectAll(select)
        end
    )
    self.m_AllChooseQnCheckBox:SetSelected(false, false)
    self.m_AllChooseQnCheckBox.gameObject:SetActive(false)
    UIEventListener.Get(self.m_InviteButton).onClick = DelegateFactory.VoidDelegate(function (p)
        self:OnInviteButtonClicked()
    end)
    self.m_SelectSorts.OnButtonSelected = DelegateFactory.Action_bool(function (selected )
        self:OnSelectSortsSelected(selected)
    end)
    self.m_MembersView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return self:NumberOfRows()
        end,
        function(item, index)
            return self:ItemAt(item,index)
        end)
    -- local callback = DelegateFactory.Action_int(function(row) self:OnSelectAtRow(row) end)
    -- self.m_MembersView.OnSelectAtRow = callback
    if LuaZongMenMgr.m_IsOpen and CClientMainPlayer.Inst then
        Gac2Gas.QueryAllSectMemberId(CClientMainPlayer.Inst.BasicProp.SectId)
    end
end

function LuaInviteToJoinZongMenWnd:NumberOfRows()
    return self.m_List and #self.m_List or 0
end

function LuaInviteToJoinZongMenWnd:ItemAt(item,index)
    if not self.m_List then return end
    local data = self.m_List[index+1]
    if self.m_IsFriendView then
        self:InitFriendTemplate(item.gameObject, data)
    else
        self:InitGuildFriendTemplate(item.gameObject, data)
    end
end

function LuaInviteToJoinZongMenWnd:OnEnable() 
	g_ScriptEvent:AddListener("GuildInfoReceived", self, "OnGuildInfoReceived")
    g_ScriptEvent:AddListener("OnSyncSectAllMemberId", self, "OnSyncSectAllMemberId")
    g_ScriptEvent:AddListener("OnReplySectInviteImMessage", self, "OnReplySectInviteImMessage")
    CGuildMgr.Inst:GetGuildMemberInfo()
end

function LuaInviteToJoinZongMenWnd:OnDisable()
	g_ScriptEvent:RemoveListener("GuildInfoReceived", self, "OnGuildInfoReceived")
    g_ScriptEvent:RemoveListener("OnSyncSectAllMemberId", self, "OnSyncSectAllMemberId")
    g_ScriptEvent:RemoveListener("OnReplySectInviteImMessage", self, "OnReplySectInviteImMessage")
end

function LuaInviteToJoinZongMenWnd:OnReplySectInviteImMessage(invitedIds)
    self.m_invitedList = {}
    for _, pIds in ipairs(invitedIds) do
        self.m_invitedList[pIds] = true
    end
    self.m_MembersView:ReloadData(true, true)
end

function LuaInviteToJoinZongMenWnd:OnSyncSectAllMemberId(t,sectInviteCd,lastInviteFriendTime,lastInviteGuildTime)
    self.m_AllSectMemberIds = t
    self.m_SectInviteCd = sectInviteCd
    self.m_LastInviteFriendTime = lastInviteFriendTime
    self.m_LastInviteGuildTime = lastInviteGuildTime
    self:InitFriends()
    self:InitGuildFriends()
    self.m_InviteFriendViewQnRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnSelectFriendView(index)
    end)
    self.m_InviteGuildViewQnRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        self:OnSelectGuildView(index)
    end)
    self.m_TopTabs:ChangeTo(0, true)
end

function LuaInviteToJoinZongMenWnd:OnGuildInfoReceived()
    if not self.m_AllSectMemberIds then return end
    self:InitGuildFriends()
end

function LuaInviteToJoinZongMenWnd:OnTopTabsSelect(btn,index)
    self.m_IsFriendView = index == 0
    self.m_InviteFriendView:SetActive(index == 0)
    self.m_InviteGuildView:SetActive(index ~= 0)
    if index == 0 then
        self.m_InviteFriendViewQnRadioBox:ChangeTo(0, true)
    else
        self.m_InviteGuildViewQnRadioBox:ChangeTo(0, true)
    end
end

function LuaInviteToJoinZongMenWnd:InitFriends()
    self.m_Friends = {}
    for i = 0,CIMMgr.MAX_FRIEND_GROUP_NUM - 1 do
        local groupName = CIMMgr.Inst:GetFriendGroupName(i + 1)
        local info = CreateFromClass(GroupInfo, i + 1, groupName, RelationshipType.Friend)
        local friends = CIMMgr.Inst:GetFriendsByGroup(info.groupId)
        self.m_Friends[info.groupId] = {}
        CommonDefs.ListIterate(friends, DelegateFactory.Action_object(function (___value) 
            local playerId = ___value
            if not self.m_AllSectMemberIds[playerId] then
                local relationshipBasicPlayerInfo = CreateFromClass(FriendItemData, CIMMgr.Inst:GetBasicInfo(playerId))
                if relationshipBasicPlayerInfo and relationshipBasicPlayerInfo.level >= 50 then
                    table.insert(self.m_Friends[info.groupId], relationshipBasicPlayerInfo)
                end
            end
        end))
        if #self.m_Friends[info.groupId] > 1 then
            table.sort(self.m_Friends[info.groupId],function(a,b) 
                if a.isOnline and not b.isOnline then 
                    return true 
                elseif b.isOnline and not a.isOnline then 
                    return false
                else 
                    return a.friendliness > b.friendliness
                end
            end)
        end
    end
end

function LuaInviteToJoinZongMenWnd:InitGuildFriends()
    self.m_AllGuildMemberInfos = {}
    local myId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    if CGuildMgr.Inst.m_GuildMemberInfo ~= nil then
        CommonDefs.DictIterate(CGuildMgr.Inst.m_GuildMemberInfo.Infos, DelegateFactory.Action_object_object(function (___key, ___value)
            local memberId = ___key
            local memberInfo = ___value
            if not self.m_AllSectMemberIds[memberInfo.PlayerId] then
                self.m_AllGuildMemberInfos[memberId] = {memberInfo = memberInfo}
                if  myId ~=memberId and CommonDefs.DictContains(CGuildMgr.Inst.m_GuildMemberInfo.DynamicInfos, typeof(UInt64), memberId) then
                    local infoDynamic = CommonDefs.DictGetValue(CGuildMgr.Inst.m_GuildMemberInfo.DynamicInfos, typeof(UInt64), memberId)
                    if infoDynamic.Level >= 50 then
                        self.m_AllGuildMemberInfos[memberId].infoDynamic = infoDynamic
                    end
                end
            end
        end))
    end
end

function LuaInviteToJoinZongMenWnd:OnSelectFriendView(index)
    local list = {}
    self.m_FriendViewIndex = index
    for groupId, dataList in pairs(self.m_Friends) do
        for i, data in pairs(dataList) do
            if index == 0 or groupId == index then
                table.insert(list,data)
            end
        end
    end
    table.sort(list,function(a,b) 
        if a.isOnline and not b.isOnline then 
            return true 
        elseif b.isOnline and not a.isOnline then 
            return false
        else 
            return a.friendliness > b.friendliness
        end
    end)
    self.m_List = list
    self.m_SelectedList = {}
    self.m_SelectedNum = 0
    self.m_MembersView:ReloadData(true, true)
    -- Extensions.RemoveAllChildren(self.m_Grid.transform)
    -- for i, data in pairs(list) do
    --     self:InitFriendTemplate(data)
    -- end
    -- self.m_Grid:Reposition()
    -- self.m_AllChooseQnCheckBox:SetSelected(false, false)
end

function LuaInviteToJoinZongMenWnd:OnSelectGuildView(index, sortIdex)
    self.m_GuildViewIndex = index
    local list = {}
    for memberId, t in pairs(self.m_AllGuildMemberInfos) do
        if (t.memberInfo.Title < 30 and index < 2) or (t.memberInfo.Title >= 30 and index ~= 1) then
            if t.infoDynamic then
                table.insert(list, t)
            end
        end
    end
    if not sortIdex then sortIdex = 1 end
    self:SortGuildMembers(list, sortIdex)
    self.m_List = list
    self.m_SelectedList = {}
    self.m_SelectedNum = 0
    self.m_MembersView:ReloadData(true, true)
    -- Extensions.RemoveAllChildren(self.m_Grid.transform)
    -- self.m_Grid:Reposition()
    -- self.m_AllChooseQnCheckBox:SetSelected(false, false)
    -- for i, data in pairs(list) do
    --     self:InitGuildFriendTemplate(data)
    -- end
    -- self.m_Grid:Reposition()
    -- self.m_AllChooseQnCheckBox:SetSelected(false, false)
end

function LuaInviteToJoinZongMenWnd:SortGuildMembers(list, sortIndex)
    self.m_GuildSortIndex = sortIndex
    local flag = 1
    local myId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or 0
    local dynamicInfos = {}
    if CGuildMgr.Inst.m_GuildMemberInfo then
        CommonDefs.DictIterate(CGuildMgr.Inst.m_GuildMemberInfo.DynamicInfos, DelegateFactory.Action_object_object(function (___key, ___value)
            dynamicInfos[___key]=___value
        end))
    end

    table.sort(list, function(a,b)
        local a1,b1 =dynamicInfos[a.memberInfo.PlayerId],dynamicInfos[b.memberInfo.PlayerId]
        if a1.LastOnlineTime == 0 and b1.LastOnlineTime ~= 0 then
            return true
        elseif a1.LastOnlineTime ~= 0 and b1.LastOnlineTime == 0 then
            return false
        elseif sortIndex == 1 and a1.Class ~= b1.Class  then
            local ret = a1.Class>b1.Class and flag or -flag
            return ret < 0
        elseif sortIndex == 2 and a1.Level ~= b1.Level  then
            local ret = a1.Level<b1.Level and flag or -flag
            return ret < 0
        elseif sortIndex == 3 and a.memberInfo.Title ~= b.memberInfo.Title  then
            local ret = a.memberInfo.Title<b.memberInfo.Title and -flag or flag
            return ret < 0
        else
            local ret=flag
            if a1.LastOnlineTime > b1.LastOnlineTime then
                ret = flag
            elseif a1.LastOnlineTime < b1.LastOnlineTime then
                ret = -flag
            elseif a.memberInfo.Title < b.memberInfo.Title then
                ret = flag
            elseif a.memberInfo.Title > b.memberInfo.Title then
                ret = -flag
            else
                return a1.PlayerId<b1.PlayerId
            end
            return ret<0
        end
    end )
end

function LuaInviteToJoinZongMenWnd:OnSelectAll(select)
    for i = 1, self.m_Grid.transform.childCount do
        local tf = self.m_Grid.transform:GetChild(i - 1)
        if tf then
            local checkbox =  tf.transform:Find("QnCheckBox"):GetComponent(typeof(QnCheckBox))
            if checkbox then
                checkbox:SetSelected(select, false)
            end
        end
    end
end

function LuaInviteToJoinZongMenWnd:OnSelectOne(select, playerId)
    self.m_SelectedList[playerId] = select
    self.m_SelectedNum = 0
    for id , selected in pairs(self.m_SelectedList) do
        self.m_SelectedNum =  self.m_SelectedNum + (selected and 1 or 0)
    end
    self.m_ChooseNumLabel.text = SafeStringFormat3(LocalString.GetString("已选%d人"),self.m_SelectedNum)
end

function LuaInviteToJoinZongMenWnd:OnInviteButtonClicked()
    local zongMenName = LuaZongMenMgr.m_ZongMenName
    if zongMenName and CClientMainPlayer.Inst then
        local link = g_MessageMgr:FormatMessage("ZongMen_IvitationLetter",zongMenName,CClientMainPlayer.Inst.BasicProp.SectId,CClientMainPlayer.Inst.Id)
        local list = {}
        for id , selected in pairs(self.m_SelectedList) do
            if selected then
                table.insert(list, id)
                --CChatHelper.SendMsgWithFilterOption(EChatPanel.Friend,link,id, true)
            end
        end
        if #list > 0 then
            local u = MsgPackImpl.pack(Table2List(list, MakeGenericClass(List, Object)))
            -- 新增参数,区分是邀请好友还是邀请帮会成员，这两个按钮分别用了两个CD，1是好友，2是帮会成员
            Gac2Gas.SendSectInviteImMessage(CClientMainPlayer.Inst.BasicProp.SectId, u, false, link, self.m_IsFriendView and 1 or 2)
        else
            g_MessageMgr:ShowMessage("InviteToJoinZongMenWnd_NoneSelect")
        end
    end
    if self.m_IsFriendView then
        if CServerTimeMgr.Inst.timeStamp <= (self.m_LastInviteFriendTime + self.m_SectInviteCd) then
            self.m_invitedList = {}
        end
        self:OnSelectFriendView(self.m_FriendViewIndex)
    else
        if CServerTimeMgr.Inst.timeStamp <= (self.m_LastInviteGuildTime + self.m_SectInviteCd) then
            self.m_invitedList = {}
        end
        self:OnSelectGuildView(self.m_GuildViewIndex, self.m_GuildSortIndex)
    end
end

function LuaInviteToJoinZongMenWnd:OnSelectSortsSelected(selected)
    local textArray = {LocalString.GetString("角色职业、名称"),LocalString.GetString("等级"),LocalString.GetString("职位")}
    Extensions.SetLocalRotationZ(self.m_ArrowSprite.transform, selected and 180 or 0)
    if not selected then return end
    local t = {}
    for k,text in pairs(textArray) do
        local item=PopupMenuItemData(text,DelegateFactory.Action_int(function (idx)
            self.m_SelectSorts.Text = textArray[idx + 1]
            self:OnChooseSortClicked(idx + 1)
        end),false,nil)
        table.insert(t, item)
    end
    local array=Table2Array(t,MakeArrayClass(PopupMenuItemData))
    CPopupMenuInfoMgr.ShowPopupMenu(array, self.m_SelectSorts.m_Label.transform, AlignType.Bottom,1,DelegateFactory.Action(function()
        Extensions.SetLocalRotationZ(self.m_ArrowSprite.transform, 0)
    end),600,true,296)
end

function LuaInviteToJoinZongMenWnd:OnChooseSortClicked(index)
    self:OnSelectGuildView(self.m_GuildViewIndex, index)
end


function LuaInviteToJoinZongMenWnd:InitFriendTemplate(go, data)
    --local go = NGUITools.AddChild(self.m_Grid.gameObject, self.m_Template)
    go.transform:Find("ProfileFrame/LevelLabel"):GetComponent(typeof(UILabel)).text = data.level
    go.transform:Find("ProfileFrame"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(data.portraitName, not data.isOnline)
    go.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = data.name
    go.transform:Find("FriendlyDegreesLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("友好度：%d"),data.friendliness)
    local playerId = data.playerId
    local checkbox =  go.transform:Find("QnCheckBox"):GetComponent(typeof(QnCheckBox))
    local cantInviteLabel = go.transform:Find("CantInviteLabel").gameObject
    cantInviteLabel:SetActive(self.m_invitedList[data.playerId])
    checkbox.gameObject:SetActive(not self.m_invitedList[data.playerId])
    checkbox.OnValueChanged = DelegateFactory.Action_bool(
        function(select)
            self:OnSelectOne(select, playerId)
        end
    )
    checkbox:SetSelected(false, false)
    go:SetActive(true)
end

function LuaInviteToJoinZongMenWnd:InitGuildFriendTemplate(go, data)
    --local go = NGUITools.AddChild(self.m_Grid.gameObject, self.m_Template)
    local officeText = GuildDefine.GetOfficeName(data.memberInfo.Title)
    if data.memberInfo.MiscData.IsNeiWuFuOfficer == 1 then
        officeText = officeText .. ("," .. Guild_Setting.GetData().NeiWuFuOfficerName)
    end
    go.transform:Find("ProfileFrame/LevelLabel"):GetComponent(typeof(UILabel)).text = data.infoDynamic.Level
    local portraitName = CUICommonDef.GetPortraitName(data.infoDynamic.Class, data.infoDynamic.Gender, -1)
    local profileFrame = go.transform:Find("ProfileFrame"):GetComponent(typeof(CUITexture))
    profileFrame:LoadNPCPortrait(portraitName,data.infoDynamic.LastOnlineTime > 0)
    go.transform:Find("NameLabel"):GetComponent(typeof(UILabel)).text = data.infoDynamic.Name
    go.transform:Find("FriendlyDegreesLabel"):GetComponent(typeof(UILabel)).text = officeText
    local playerId = data.infoDynamic.PlayerId
    local checkbox =  go.transform:Find("QnCheckBox"):GetComponent(typeof(QnCheckBox))
    local cantInviteLabel = go.transform:Find("CantInviteLabel").gameObject
    cantInviteLabel:SetActive(self.m_invitedList[data.memberInfo.PlayerId])
    checkbox.gameObject:SetActive(not self.m_invitedList[data.memberInfo.PlayerId])
    checkbox.OnValueChanged = DelegateFactory.Action_bool(
        function(select)
            self:OnSelectOne(select, playerId)
        end
    )
    checkbox:SetSelected(false, false)
    go:SetActive(true)
    --Extensions.SetLocalPositionZ(profileFrame.transform, data.infoDynamic.LastOnlineTime > 0 and - 1 or 0)
end