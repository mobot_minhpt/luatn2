local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local Input = import "UnityEngine.Input"
local Quaternion = import "UnityEngine.Quaternion"
local CCommonUIFxWnd = import "L10.UI.CCommonUIFxWnd"
local Animator = import "UnityEngine.Animator"

LuaShiTuCompassWnd = class()
RegistChildComponent(LuaShiTuCompassWnd, "m_NeedleProgress", "jindu02_Texture", UIWidget)--光效进度
RegistChildComponent(LuaShiTuCompassWnd, "m_CompassBg", "CompassBg", GameObject)--罗盘底座

RegistClassMember(LuaShiTuCompassWnd, "m_Animator")--动画控制器
RegistClassMember(LuaShiTuCompassWnd, "m_PressStartTime")--按下时刻
RegistClassMember(LuaShiTuCompassWnd, "m_IsPressing")--是否处于按下状态
RegistClassMember(LuaShiTuCompassWnd, "m_IsEnding")--是否处于结束状态
RegistClassMember(LuaShiTuCompassWnd, "m_TargetSmallCompassPos")
RegistClassMember(LuaShiTuCompassWnd, "m_CompassLongPressTimeLimit")

function LuaShiTuCompassWnd:Awake()
    self.m_IsEnding = false
    self.m_CompassLongPressTimeLimit = 5
    self.m_Animator = self.transform:GetComponent(typeof(Animator))
    UIEventListener.Get(self.m_CompassBg).onPress = DelegateFactory.BoolDelegate(function (go, isPressed)
        self:OnPressCompassBg(go, isPressed)
    end)
end

function LuaShiTuCompassWnd:Init()
    self.m_OriDir = 0 --垂直
end

function LuaShiTuCompassWnd:OnEnable()
    g_ScriptEvent:AddListener("OnQuerySmallShiTuCompassResult", self, "OnQuerySmallShiTuCompassResult")
end

function LuaShiTuCompassWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnQuerySmallShiTuCompassResult", self, "OnQuerySmallShiTuCompassResult")
end

function LuaShiTuCompassWnd:OnQuerySmallShiTuCompassResult(targetPos)
    self.m_TargetSmallCompassPos = targetPos
end

function LuaShiTuCompassWnd:OnDestroy()

end

function LuaShiTuCompassWnd:OnPressCompassBg(go, isPressed)
    if isPressed then
        self.m_PressStartTime = CServerTimeMgr.Inst.timeStamp
        self.m_IsPressing = true
    else
        self.m_IsPressing = false
    end
end

function LuaShiTuCompassWnd:Update()
    if self.m_IsEnding then return end
    if self.m_IsPressing then
        if not Input.GetMouseButton(0) then
            self.m_IsPressing = false
        else
            local progress = (CServerTimeMgr.Inst.timeStamp - self.m_PressStartTime)/self.m_CompassLongPressTimeLimit
            self.m_NeedleProgress.fillAmount = progress
            if progress>=1 then
                self.m_IsEnding = true
                self:PlayRoundAni()
            end
        end
    else
        self.m_IsPressing = false
        self.m_NeedleProgress.fillAmount = 0
    end
end


function LuaShiTuCompassWnd:PlayRoundAni()
    g_ScriptEvent:BroadcastInLua("QuerySmallShiTuCompass")
    self.m_Animator:Play("shitucompasswnd_show02", -1, 0)
    RegisterTickOnce(function()
        --播放特效，同时关闭窗口
        if CCommonUIFxWnd.Instance and self.m_TargetSmallCompassPos then
            CCommonUIFxWnd.Instance:PlayLineAni(self.m_CompassBg.transform.position, self.m_TargetSmallCompassPos, 0.5, nil)
        end
        CUIManager.CloseUI("ShiTuCompassWnd")
    end, 3000) --对应shitucompasswnd_show02的时长
end

