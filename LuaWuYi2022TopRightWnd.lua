local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UITable = import "UITable"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local CStatusMgr = import "L10.Game.CStatusMgr"
local EPropStatus = import "L10.Game.EPropStatus"

LuaWuYi2022TopRightWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaWuYi2022TopRightWnd, "ExpandButton", "ExpandButton", GameObject)
RegistChildComponent(LuaWuYi2022TopRightWnd, "Table", "Table", UITable)
RegistChildComponent(LuaWuYi2022TopRightWnd, "Template", "Template", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaWuYi2022TopRightWnd, "m_TemplateList")
RegistClassMember(LuaWuYi2022TopRightWnd, "m_Tick")
RegistClassMember(LuaWuYi2022TopRightWnd, "m_Buttons")
RegistClassMember(LuaWuYi2022TopRightWnd, "m_CurButtonEngineId")

function LuaWuYi2022TopRightWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.ExpandButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnExpandButtonClick()
	end)


    --@endregion EventBind end
end

function LuaWuYi2022TopRightWnd:Init()
	self.m_Buttons = {}
	self.Template.gameObject:SetActive(false)
	self.m_TemplateList = {}
	Extensions.RemoveAllChildren(self.Table.transform)
	for i = 1, 10 do
		local obj = NGUITools.AddChild(self.Table.gameObject, self.Template.gameObject)
		obj.gameObject:SetActive(true)
		table.insert(self.m_TemplateList,obj)
	end
	self:Show()
	self:CancelTick()
	self.m_Tick = RegisterTick(function()
        LuaWuYi2022Mgr:ShowTaskView()
		self:ShowButton()
    end,500)
end

function LuaWuYi2022TopRightWnd:CancelTick()
	if self.m_Tick then
		UnRegisterTick(self.m_Tick)
		self.m_Tick = nil
	end
end

function LuaWuYi2022TopRightWnd:OnEnable()
	g_ScriptEvent:AddListener("OnSyncDaGongHunJiTanNum", self, "Show")
	g_ScriptEvent:AddListener("OnEnterDaGongHunBossStage", self, "Show")
end

function LuaWuYi2022TopRightWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnSyncDaGongHunJiTanNum", self, "Show")
	g_ScriptEvent:RemoveListener("OnEnterDaGongHunBossStage", self, "Show")
	self:CancelTick()
end

function LuaWuYi2022TopRightWnd:ShowButton()
	if not CClientMainPlayer.Inst then return end
	local objs = CClientPlayerMgr.Inst.m_Id2VisiblePlayer
	self.m_Buttons = {}
	CommonDefs.DictIterate(objs, DelegateFactory.Action_object_object(function (___key, obj) 
		if CStatusMgr.Inst:GetStatus(obj, EPropStatus.GetId("WuYiJiTan")) == 1 then
			self.m_Buttons[obj.PlayerId] = {x = obj.Pos.x / 64, y = obj.Pos.y / 64}
		end
	end))
	local dis = 99999999
	self.m_CurButtonEngineId = nil
	local mx, my = CClientMainPlayer.Inst.Pos.x/64, CClientMainPlayer.Inst.Pos.y/64
	for engineId ,info in pairs(self.m_Buttons) do
		local newDis = (info.x - mx) * (info.x - mx) + (info.y - my) * (info.y - my)
		if newDis < dis then
			dis = newDis
			self.m_CurButtonEngineId = engineId
		end
	end
	if self.m_CurButtonEngineId and CStatusMgr.Inst:GetStatus(CClientMainPlayer.Inst, EPropStatus.GetId("WuYiJiTan")) ~= 1 then
		LuaWuYi2022Mgr.m_CurButtonEngineId = self.m_CurButtonEngineId
		local info = self.m_Buttons[self.m_CurButtonEngineId]
		if info then
			LuaCommonButtonAtPosWndMgr:ShowButton(LocalString.GetString("解救"), info.x, info.y, 2, "wuyijitan")
		end
	else
		CUIManager.CloseUI(CLuaUIResources.CommonButtonAtPosWnd)
	end
end

function LuaWuYi2022TopRightWnd:Show()
	for i = 1, 10 do
		local obj = self.m_TemplateList[i]
		if obj then
			obj.gameObject:SetActive(LuaWuYi2022Mgr.m_TaskState == 1)
			if LuaWuYi2022Mgr.m_TaskState == 1 then
				obj.transform:Find("ForwardSprite").gameObject:SetActive(i <= (10 - LuaWuYi2022Mgr.m_JiTanNum))
			end
		end
	end
	self.transform:Find("Anchor/Bg").gameObject:SetActive(LuaWuYi2022Mgr.m_TaskState == 1)
	self.Table:Reposition()
end
--@region UIEvent

function LuaWuYi2022TopRightWnd:OnExpandButtonClick()
	self.ExpandButton.transform.localEulerAngles = Vector3(0, 0, 0)
    CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

--@endregion UIEvent

