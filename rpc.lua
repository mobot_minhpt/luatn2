Gas2Gac = {}

function RegisterLuaRpc(conn)
	CSRegisterLuaRpc(conn)
	for rpcName, _ in pairs(Gas2Gac) do
		conn:RegisterRealLuaRPC(rpcName, function (...) Gas2Gac[rpcName](...) end)
	end
end

function RegisterGac2GasNames(conn)
	if DEVELOPMENT_BUILD or UNITY_EDITOR then
		for i,v in ipairs(LuaGac2Gas) do
			conn:RegisterGac2GasName(100+i,v[1])
		end
	end
end

if UNITY_EDITOR then
	require("debug/check_duplicate_definition")
end

local t=require("Gas2GacRpc")
if DEVELOPMENT_BUILD or UNITY_EDITOR then
	LuaGas2Gac = t
end
local CGasConnMgr=import "L10.Game.CGasConnMgr"

local CSGas2Gac=import "L10.Game.Gas2Gac"
function CSRegisterLuaRpc(conn)
	local target=CSGas2Gac.m_Instance
	for i,v in ipairs(t) do
		conn:RegisterLuaRPC(i+100,v[2],target,v[1])
	end
end
local t2=require("Gac2GasRpc")
if DEVELOPMENT_BUILD or UNITY_EDITOR then
	LuaGac2Gas = t2
end
Gac2Gas={}

for i,v in ipairs(t2) do
	if v[3] then
		local internal=tonumber(v[3])
		Gac2Gas[v[1]]=function(...)
			if ENABLE_XLUA then
				CGasConnMgr.SendLuaRPCWithIntervalCheck(100+i,internal,v[4],v[2],...)
			else
				CGasConnMgr.SendLuaRPCWithIntervalCheck(100+i,internal,v[4],v[2],{...})
			end
		end
	else
		Gac2Gas[v[1]]=function(...)
			if ENABLE_XLUA then
				CGasConnMgr.SendLuaRPC(100+i,v[2],...)
			else
				CGasConnMgr.SendLuaRPC(100+i,v[2],{...})
			end
		end
	end
end
