LuaYuanDan2022Mgr = {}

function LuaYuanDan2022Mgr:SyncPlayerYuanDan2022FuDanSignUpResult(isMatching)
    g_ScriptEvent:BroadcastInLua("OnSyncPlayerYuanDan2022FuDanSignUpResult",isMatching)
end

function LuaYuanDan2022Mgr:QueryPlayerYuanDan2022FuDanSignUpInfoResult(playerId, isInMatching)
    print(playerId, isInMatching)
    g_ScriptEvent:BroadcastInLua("OnQueryPlayerYuanDan2022FuDanSignUpInfoResult",playerId, isInMatching)
end

LuaYuanDan2022Mgr.m_FuDanPlayResultData = {}
LuaYuanDan2022Mgr.m_FuDanPlayResult = 0
function LuaYuanDan2022Mgr:SendYuanDan2022FuDanPlayResult(result, playResultUD)
    self.m_FuDanPlayResult = result
    local list = MsgPackImpl.unpack(playResultUD)
    self.m_FuDanPlayResultData = {}
    for i = 0, list.Count - 1, 10 do
        local playerid, name, class, winType, eggs, interrupt, force, gender, picker, fighter = 
            list[i],list[i+1],list[i+2],list[i+3],list[i+4],list[i+5],list[i+6],list[i+7],list[i+8],list[i+9]
        local isPicker,isFighter = picker == playerid, fighter == playerid
        local t = { playerid = playerid, name = name, class = class, winType = winType, eggs = eggs,
            interrupt = interrupt, gender = gender, isPicker = isPicker, isFighter = isFighter}
        self.m_FuDanPlayResultData[force * 2 + (isPicker and 1 or 2)] = t
    end
    if #self.m_FuDanPlayResultData ~= 4 then return end
    CUIManager.ShowUI(CLuaUIResources.YuanDan2022FuDanResultWnd)
end

LuaYuanDan2022Mgr.m_EggsDataInfo = {}
function LuaYuanDan2022Mgr:SyncYuanDan2022PlayInfo(stage, playInfoUD)
    g_ScriptEvent:BroadcastInLua("OnCommonCountDownViewUpdate",
        g_MessageMgr:FormatMessage(stage == 1 and "YuanDan2022View_Text1" or "YuanDan2022View_Text2","%s"),
        true, true, nil, LocalString.GetString("规则"),
        nil,function(go)
            CUIManager.ShowUI(CLuaUIResources.YuanDan2022FuDanReadMeWnd)
        end)
    local list = MsgPackImpl.unpack(playInfoUD)
    if list.Count == 4 then
        local force1, eggs1,force2,eggs2 = list[0],list[1],list[2],list[3]
        self.m_EggsDataInfo[force1] = eggs1
        self.m_EggsDataInfo[force2] = eggs2
        g_ScriptEvent:BroadcastInLua("OnSyncYuanDan2022PlayInfo")
    end
end