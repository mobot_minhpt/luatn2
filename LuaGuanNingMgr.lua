local ShareMgr=import "ShareMgr"
local CGuanNingMgr=import "L10.Game.CGuanNingMgr"
local CLoginMgr=import "L10.Game.CLoginMgr"
local Main = import "L10.Engine.Main"
local HTTPHelper = import "L10.Game.HTTPHelper"
local Json = import "L10.Game.Utils.Json"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CResourceMgr = import "L10.Engine.CResourceMgr"
local Utility = import "L10.Engine.Utility"
local Quaternion = import "UnityEngine.Quaternion"
local Vector3 = import "UnityEngine.Vector3"
local CTickMgr = import "L10.Engine.CTickMgr"
local CClientPlayerMgr = import "L10.Game.CClientPlayerMgr"
local ETickType = import "L10.Engine.ETickType"
local EventManager = import "EventManager"
local EnumEventType = import "EnumEventType"
local CClientObjectMgr=import "L10.Game.CClientObjectMgr"
local CClientNpc = import "L10.Game.CClientNpc"

CLuaGuanNingMgr={}

CLuaGuanNingMgr.m_ScoreRecords={}
CLuaGuanNingMgr.m_LeftDoubleTimes=0 --双倍积分

CLuaGuanNingMgr.m_HuxiaoPlayInfoList={}
CLuaGuanNingMgr.m_LongyinPlayInfoList={}
CLuaGuanNingMgr.m_HuxiaoScore=0
CLuaGuanNingMgr.m_LongyinScore=0
CLuaGuanNingMgr.m_OccupyScores={}

CLuaGuanNingMgr.m_IsGameEnd=false
CLuaGuanNingMgr.m_Score=0--积分

CLuaGuanNingMgr.m_SelectedPlayerId=0

CLuaGuanNingMgr.lastBattleData=nil
CLuaGuanNingMgr.historyMonthData=nil

CLuaGuanNingMgr.m_ClickSignalWorldPos=nil


CLuaGuanNingMgr.ShowLastBattleData=false
--关宁报名界面
CLuaGuanNingMgr.m_Hour=0
CLuaGuanNingMgr.m_Minute=0
CLuaGuanNingMgr.m_CurSignUpIndex=0
CLuaGuanNingMgr.signUpState={}
CLuaGuanNingMgr.signUpCount={}

CLuaGuanNingMgr.m_XunLianPlayIndex=0

CLuaGuanNingMgr.m_LastSendSignalTime=0
CLuaGuanNingMgr.m_PlayerServerNameCache={}
CLuaGuanNingMgr.m_IsSundayPlayOpen = true
CLuaGuanNingMgr.m_ZhanYiProgressValue = 0
CLuaGuanNingMgr.m_ZhiHuiFaZhenProgressValue = 0
CLuaGuanNingMgr.m_BreakRecords = {}
CLuaGuanNingMgr.m_IsInGuanNingWeekendNewModePlay = false

CLuaGuanNingMgr.m_PlayId = nil
CLuaGuanNingMgr.m_ServerGroupId = nil
--CLuaGuanNingMgr.m_ThumbUpCnt = 0
--CLuaGuanNingMgr.m_ThumbUpLimit = 3
CLuaGuanNingMgr.m_PraisedCloseTime = 2 -- 被点赞特效关闭时间
CLuaGuanNingMgr.m_PraisedRefreshInterval = 5 -- 被点赞数据刷新间隔

CLuaGuanNingMgr.m_Status = EnumGuanNingPlayStatus.eBeiZhan
CLuaGuanNingMgr.m_CommanderOpen = false

CLuaGuanNingMgr.m_HuXiaoCommanderId = 0 --force_0; defender
CLuaGuanNingMgr.m_LongYinCommanderId = 0 --force_1; attacker

-- icon的gameObject
-- 1:Commander 2:FaZhen
CLuaGuanNingMgr.m_CommanderMapIcon = {} 
CLuaGuanNingMgr.m_CommanderPopMapIcon = {}

-- 1:号角 2:战鼓
CLuaGuanNingMgr.m_CounterattackMapIcon = {}
CLuaGuanNingMgr.m_CounterattackPopMapIcon = {}
CLuaGuanNingMgr.m_InCounterattackGuide = false

function CLuaGuanNingMgr:IsCommander()
    if CClientMainPlayer.Inst then
		local id = CClientMainPlayer.Inst.Id
        return self:IsPlayerCommander(id)
    end
    return false
end

function CLuaGuanNingMgr:IsPlayerCommander(id)
    return id == self.m_HuXiaoCommanderId or id == self.m_LongYinCommanderId
end

function CLuaGuanNingMgr:GetMyForce()
    return CGuanNingMgr.Inst:GetMyForce() == LocalString.GetString("虎啸骁兵") and 0 or 1
end

function CLuaGuanNingMgr:GetPlayerForce(id)
    return CGuanNingMgr.Inst:GetForceName(id) == LocalString.GetString("虎啸队骁兵") and 0 or 1
end

function CLuaGuanNingMgr:ClearCommanderMapIcon()
    for i = 1, 2 do
        if self.m_CommanderMapIcon[i] then
            CResourceMgr.Inst:Destroy(self.m_CommanderMapIcon[i])
            self.m_CommanderMapIcon[i] = nil
        end
    end
end

function CLuaGuanNingMgr:ClearCommanderPopMapIcon()
    for i = 1, 2 do
        if self.m_CommanderPopMapIcon[i] then
            CResourceMgr.Inst:Destroy(self.m_CommanderPopMapIcon[i])
            self.m_CommanderPopMapIcon[i] = nil
        end
    end
end

function CLuaGuanNingMgr:ClearCounterattackMapIcon()
    for i = 1, 2 do
        if self.m_CounterattackMapIcon[i] then
            CResourceMgr.Inst:Destroy(self.m_CounterattackMapIcon[i])
            self.m_CounterattackMapIcon[i] = nil
        end
    end
end

function CLuaGuanNingMgr:ClearCounterattackPopMapIcon()
    for i = 1, 2 do
        if self.m_CounterattackPopMapIcon[i] then
            CResourceMgr.Inst:Destroy(self.m_CounterattackPopMapIcon[i])
            self.m_CounterattackPopMapIcon[i] = nil
        end
    end
end

function CLuaGuanNingMgr:RequestPerformGnjcZhanYi()
    if not self.m_IsSundayPlayOpen then return end
    if self.m_ZhanYiProgressValue < 1 then
        g_MessageMgr:ShowMessage("GuanNingFightingSpiritButton_LackOfPower")
        return 
    end
    Gac2Gas.RequestPerformGnjcZhanYi()
end

function CLuaGuanNingMgr:RequestPerformGnjcZhiHuiFaZhen()
    if self.m_ZhiHuiFaZhenProgressValue < 1 then
        g_MessageMgr:ShowMessage("GuanNing_Commander_MagicCircle_LackOfPower")
        return 
    end
    --[[
    local tid = self:GetMyForce() == 1 and GuanNing_Setting.GetData().AttackZhiHuiFaZhenNpcId or GuanNing_Setting.GetData().DefendZhiHuiFaZhenNpcId
    local fazhen = {}
    CClientObjectMgr.Inst:ForeachObject(DelegateFactory.Action_CClientObject(function(obj)
        if TypeIs(obj, typeof(CClientNpc)) and obj.TemplateId == tid then
            table.insert(fazhen, obj)
        end
    end))
    for _, v in ipairs(fazhen) do
        if v then
            v:Destroy()
        end
    end
    --]]
    Gac2Gas.RequestPerformGnjcZhiHuiFaZhen()
end

CLuaGuanNingMgr.m_EvaluateZhiHuiAlertParameter = nil
function CLuaGuanNingMgr:RegistEvaluateZhiHuiAlert(playerId, playerName, playerClass, playerGender, showTime)
    self.m_EvaluateZhiHuiAlertParameter = {}
    self.m_EvaluateZhiHuiAlertParameter[1] = playerId
    self.m_EvaluateZhiHuiAlertParameter[2] = playerName
    self.m_EvaluateZhiHuiAlertParameter[3] = playerClass
    self.m_EvaluateZhiHuiAlertParameter[4] = playerGender
    self.m_EvaluateZhiHuiAlertParameter[5] = showTime
end

function CLuaGuanNingMgr:LoadEvaluateZhiHuiAlert()
    if self.m_EvaluateZhiHuiAlertParameter then
        g_ScriptEvent:BroadcastInLua("SendGnjcEvaluateZhiHuiAlert", 
            self.m_EvaluateZhiHuiAlertParameter[1], self.m_EvaluateZhiHuiAlertParameter[2], 
            self.m_EvaluateZhiHuiAlertParameter[3], self.m_EvaluateZhiHuiAlertParameter[4], 
            self.m_EvaluateZhiHuiAlertParameter[5])
        self.m_EvaluateZhiHuiAlertParameter = nil
    end
end

function CLuaGuanNingMgr.OnEnterPlay()
    if CUIManager.IsLoaded(CLuaUIResources.AnQiDaoResultWnd) then
        CUIManager.CloseUI(CLuaUIResources.AnQiDaoResultWnd)
    end

    if CUIManager.IsLoaded(CUIResources.GuanNingResultWnd) then
        CUIManager.CloseUI(CUIResources.GuanNingResultWnd)
    end

    CLuaGuanNingMgr.m_IsGameEnd=false
    CLuaGuanNingMgr.m_ScoreRecords={}
    CLuaGuanNingMgr.m_SelectedPlayerId=0
    CLuaGuanNingMgr.m_Score=0

    CLuaGuanNingMgr.m_LeftDoubleTimes=0 --双倍积分

    CLuaGuanNingMgr.m_HuxiaoPlayInfoList={}
    CLuaGuanNingMgr.m_LongyinPlayInfoList={}
    CLuaGuanNingMgr.m_HuxiaoScore=0
    CLuaGuanNingMgr.m_LongyinScore=0
    CLuaGuanNingMgr.m_OccupyScores={}

    CLuaGuanNingMgr.lastBattleData=nil
    CLuaGuanNingMgr.historyMonthData=nil

    -- 需要在GuanNingResultWnd维护的数据不能在OnLeavePlay的时候清空，GuanNingResultWnd跨场景不销毁
    -- 可以在GuanNingResultWnd的OnDestroy时判断GameEnd销毁
    CLuaGuanNingMgr.m_PlayId = nil 
    CLuaGuanNingMgr.m_ServerGroupId = nil
    --CLuaGuanNingMgr.m_ThumbUpCnt = 0
    --CLuaGuanNingMgr.m_ThumbUpLimit = GuanNing_Setting.GetData().MaxDianZanNum
end

function CLuaGuanNingMgr.OnLeavePlay()
    CLuaGuanNingMgr.m_Status = 0
    CLuaGuanNingMgr.m_EvaluateZhiHuiAlertParameter = nil
    CLuaGuanNingMgr.m_InCounterattackGuide = false
    CLuaGuanNingMgr:ClearCommanderMapIcon()
    CLuaGuanNingMgr:ClearCounterattackMapIcon()
    g_ScriptEvent:BroadcastInLua("SwitchGuanNingPanel",false)
    EventManager.BroadcastInternalForLua(EnumEventType.UpdatePlayerHeadInfoSepIcon, {true})
end


function CLuaGuanNingMgr.GetShareString()
    if CommonDefs.IS_HMT_CLIENT then
        return LocalString.GetString("分享至facebook")
    else
        return LocalString.GetString("分享至其他")
    end
end
function CLuaGuanNingMgr.IsOnlyShare2PersonalSpace()
    if CommonDefs.IS_HMT_CLIENT then
        return true
    end
    return not CLoginMgr.Inst:IsNetEaseOfficialLogin()
end
--type==1 梦岛 type==2 其他
--isAnqidao 是否是安期岛
function CLuaGuanNingMgr.Share2Web(type, isAnqidao)
    -- "guanningshare/data?grade={0}&rolename={1}&roleid={2}&gander={3}&server={4}&camp={5}&professionid={6}&ctime={7}&result={8}&winstreaknum={9}&allscore={10}&gamebest={11}&teambest={12}&rank={13}&kill={14}&die={15}&assist={16}&reborn={17}&achievement={18}&killstreak={19}"

    --QueryGuanNingShareImageUrl

    local lastBattleData=CLuaGuanNingMgr.lastBattleData
    local historyMonthData=CLuaGuanNingMgr.historyMonthData

    local inst=CClientMainPlayer.Inst
    local grade=inst.Level
    local rolename= inst.Name
    local roleid = inst.Id
    local gender = EnumToInt(inst.Gender)
    local server = inst:GetMyServerName()
    local camp = lastBattleData.force
    local professionid = EnumToInt(inst.Class)
    local ctime = lastBattleData.timeStamp
    local result = lastBattleData.result + 1--（1胜 2败 3平）
    local winstreaknum = 0
    if historyMonthData ~= nil then
        winstreaknum = historyMonthData[EnumGuanNingWeekDataKey.eLianShengNum]
        if winstreaknum == nil then 
            winstreaknum =0
        end
    end
    local allscore = lastBattleData.score
    local gamebest = lastBattleData.isMaxScore and 1 or 0

    local teambest = lastBattleData.isMaxScoreInForce and 1 or 0
    local rank = lastBattleData.rank
    local kill = lastBattleData.killNum
    local die = lastBattleData.dieNum
    local assist = lastBattleData.helpNum
    local reborn = lastBattleData.reliveNum
    local liansha = lastBattleData.maxLianSha

    local achievement={}
    for i=1,#lastBattleData.achieveInfo do
        table.insert( achievement, tostring(lastBattleData.achieveInfo[i]))
    end
    achievement=table.concat(achievement, ",")


    local url=CGuanNingMgr.ShareBaseUrl.. SafeStringFormat3("guanningshare/data?grade=%s&rolename=%s&roleid=%s&gander=%s&server=%s&camp=%s&professionid=%s&ctime=%s&result=%s&winstreaknum=%s&allscore=%s&gamebest=%s&teambest=%s&rank=%s&kill=%s&die=%s&assist=%s&reborn=%s&achievement=%s&killstreak=%s",
        grade, rolename, roleid, gender, server, camp, professionid, ctime, result, winstreaknum, allscore, gamebest, teambest, rank, kill, die, assist, reborn, achievement, liansha)

    if isAnqidao == true then
        url = url .. "&sharetype=anqidao"
    end
    
    Main.Inst:StartCoroutine(HTTPHelper.GetUrl(url, DelegateFactory.Action_bool_string(function (success, ret) 
		if success then
			local dict = Json.Deserialize(ret)
			local code = CommonDefs.DictGetValue_LuaCall(dict,"code")
			if code and code==1 then
				--成功
				local imgurl = CommonDefs.DictGetValue_LuaCall(dict,"imgurl")
                if imgurl then
					--分享到梦岛或者分享到其他
                    if CLuaGuanNingMgr.IsOnlyShare2PersonalSpace() then
                        CLuaShareMgr.ShareGuanNingImage2PersonalSpace(imgurl)
                    else
						ShareMgr.ShareWebImage2Other(imgurl)
                    end
                end
            elseif code and code==500 then
                g_MessageMgr:ShowMessage("GUANNING_SHARE_BUSY")
            else
                g_MessageMgr:ShowMessage("GUANNING_SHARE_FAIL")
			end	
		end
    end)))
    
end

function CLuaGuanNingMgr.GetPlayerScoreRecord(playerId)
    -- print("GetPlayerScoreRecord",playerId,CLuaGuanNingMgr.m_ScoreRecords[playerId])
    return CLuaGuanNingMgr.m_ScoreRecords[playerId]
end
function CLuaGuanNingMgr.GetScoreRecordSpriteName(score)
    if score == EnumGuanNingWeekDataKey.eMaxSubmitFlagNumInForce then
        return "common_zhankuangtongji_qi";
    elseif score == EnumGuanNingWeekDataKey.eMaxOccupyScoreInForce then
        return "common_zhankuangtongji_zhan";
    elseif score == EnumGuanNingWeekDataKey.eMaxCtrlNumInForce then
        return "common_zhankuangtongji_kong";
    elseif score == EnumGuanNingWeekDataKey.eMaxBaoShiNumInForce then
        return "common_zhankuangtongji_bao";
    elseif score == EnumGuanNingWeekDataKey.eMaxDpsInForce then
        return "common_zhankuangtongji_shang";
    elseif score == EnumGuanNingWeekDataKey.eMaxHealInForce then
        return "common_zhankuangtongji_zhi";
    elseif score == EnumGuanNingWeekDataKey.eMaxUnderDamageInForce then
        return "common_zhankuangtongji_cheng"
    elseif score == EnumGuanNingWeekDataKey.eMaxCannonPickInForce then
        return "common_zhankuangtongji_tian"
    end
    return ""
end

function CLuaGuanNingMgr:LoadBreakRecords(battleData)
    if not CUIManager.IsLoaded(CUIResources.GuanNingResultWnd) then 
        self.m_BreakRecords = {}
        return 
    end
    self.m_BreakRecords.killNumBreakRecord = battleData.killNum > battleData.lastMaxPlayKillNum
    self.m_BreakRecords.dieNumBreakRecord = battleData.dieNum > battleData.lastMaxPlayDieNum
    self.m_BreakRecords.helpNumBreakRecord = battleData.helpNum > battleData.lastMaxPlayHelpNum
    self.m_BreakRecords.reliveNumBreakRecord = battleData.reliveNum > battleData.lastMaxPlayReliveNum
    self.m_BreakRecords.isLastBattleDataBreakRecord = self.m_BreakRecords.killNumBreakRecord or self.m_BreakRecords.dieNumBreakRecord
    or self.m_BreakRecords.helpNumBreakRecord or self.m_BreakRecords.reliveNumBreakRecord
    if self.m_BreakRecords.isLastBattleDataBreakRecord then
        g_ScriptEvent:BroadcastInLua("GuanNingBreakRecords")
    end
end

function CLuaGuanNingMgr:LoadBreakRecordsWeekData(historyMonthData)
    if not CUIManager.IsLoaded(CUIResources.GuanNingResultWnd) then 
        self.m_BreakRecords = {}
        return 
    end
    self.m_BreakRecords.weekData = {}
    local isBreakRecords = false
    if CClientMainPlayer.Inst then
        local p = CClientMainPlayer.Inst.PlayProp
        if p then
            for k, v in pairs(historyMonthData) do
                local  _, lastMaxData = CommonDefs.DictTryGet(p.GuanNingWeekData.TempData, typeof(Byte), k, typeof(UInt32))
                if (lastMaxData == nil and v > 0) or (v and lastMaxData and v > lastMaxData) then
                    self.m_BreakRecords.weekData[k] = true
                    isBreakRecords = true
                end
            end
        end
    end 
    if isBreakRecords then
        g_ScriptEvent:BroadcastInLua("GuanNingBreakRecords")
    else
        self.m_BreakRecords.weekData = nil
    end
end
