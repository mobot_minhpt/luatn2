local CUITexture=import "L10.UI.CUITexture"
local UIGrid = import "UIGrid"
local EquipmentTemplate_Equip=import "L10.Game.EquipmentTemplate_Equip"
local CButton = import "L10.UI.CButton"
local Time = import "UnityEngine.Time"

LuaBQPPromotionWnd=class()

RegistClassMember(LuaBQPPromotionWnd,"EquipTexture")
RegistClassMember(LuaBQPPromotionWnd,"QualitySprite")
RegistClassMember(LuaBQPPromotionWnd,"NameLabel")
RegistClassMember(LuaBQPPromotionWnd,"ScoreLabel")
RegistClassMember(LuaBQPPromotionWnd,"RankLabel")

RegistClassMember(LuaBQPPromotionWnd,"TitleHint")
RegistClassMember(LuaBQPPromotionWnd,"Grid")
RegistClassMember(LuaBQPPromotionWnd,"Template")

RegistClassMember(LuaBQPPromotionWnd,"DetailBtn")
RegistClassMember(LuaBQPPromotionWnd,"VoteBtn")
RegistClassMember(LuaBQPPromotionWnd,"RemainTimesLabel")

RegistClassMember(LuaBQPPromotionWnd,"EquipInfo")
RegistClassMember(LuaBQPPromotionWnd,"VoteInfo")

RegistClassMember(LuaBQPPromotionWnd,"EvaluateInterval")
RegistClassMember(LuaBQPPromotionWnd,"LastEvaluateTime")

function LuaBQPPromotionWnd:Init()

	self.EquipInfo = LuaBingQiPuMgr.m_PromoteEquipInfo
	self.VoteInfo = LuaBingQiPuMgr.m_PromoteVoteInfo
	self.EvaluateInterval = 2
	self.LastEvaluateTime = 0

	self.EquipTexture = self.transform:Find("Anchor/EquipInfo/Equip/EquipTexture"):GetComponent(typeof(CUITexture))
	self.QualitySprite = self.transform:Find("Anchor/EquipInfo/Equip/QualitySprite"):GetComponent(typeof(UISprite))
	self.NameLabel = self.transform:Find("Anchor/EquipInfo/NameLabel"):GetComponent(typeof(UILabel))
	self.ScoreLabel = self.transform:Find("Anchor/EquipInfo/ScoreLabel"):GetComponent(typeof(UILabel))
	self.RankLanel = self.transform:Find("Anchor/EquipInfo/RankLabel"):GetComponent(typeof(UILabel))

	local template = EquipmentTemplate_Equip.GetData(self.EquipInfo.TemplateId)
	if not template then return end
	self.EquipTexture:LoadMaterial(self.EquipInfo.Icon)
	self.QualitySprite.spriteName = CUICommonDef.GetItemCellBorder(self.EquipInfo.Equip.QualityType)
	local previousAdjusted = self.EquipInfo.Equip.IsFeiShengAdjusted
	self.EquipInfo.Equip.IsFeiShengAdjusted = 0
	self.NameLabel.text = self.EquipInfo.Equip.DisplayName
	self.NameLabel.color = self.EquipInfo.Equip.DisplayColor
	self.ScoreLabel.text = SafeStringFormat3(LocalString.GetString("[91FAFF]装备评分：[-][FFFE91]%d[-]"), self.EquipInfo.Equip.Score)
	self.EquipInfo.Equip.IsFeiShengAdjusted = previousAdjusted

	local etype = template.Type 
	if LuaBingQiPuMgr.m_PromoteData.BPrecious then
		etype = 0 
	end
	local ename = LuaBingQiPuMgr.GetEquipTypeName(etype)
	local rankpos = LuaBingQiPuMgr.m_PromoteData.RankPos
	local rankfmt = LocalString.GetString("[91FAFF]%s排行榜：[-][FFFE91]%s[-]")
	self.RankLanel.text = SafeStringFormat3(rankfmt,ename,rankpos)

	self.TitleHint = self.transform:Find("Anchor/VoteInfo/TitleHint"):GetComponent(typeof(UILabel))
	self.TitleHint.text = SafeStringFormat3(LocalString.GetString("第[FFFE91]%d[-]位点赞可获额外奖励"), LuaBingQiPuMgr.m_PromoteData.ExtraReward)
	
	self.Grid = self.transform:Find("Anchor/VoteInfo/Votes/Table"):GetComponent(typeof(UIGrid))
	self.Template = self.transform:Find("Anchor/VoteInfo/Votes/Template").gameObject
	self.Template:SetActive(false)
	CUICommonDef.ClearTransform(self.Grid.transform)
	
	for k, v in ipairs(self.VoteInfo) do
		local go = NGUITools.AddChild(self.Grid.gameObject,self.Template)
		go:SetActive(true)
        self:InitItem(go, v)
	end
	self.Grid:Reposition()

	self.DetailBtn = self.transform:Find("Anchor/Bottom/DetailBtn").gameObject
	self.VoteBtn = self.transform:Find("Anchor/Bottom/VoteBtn"):GetComponent(typeof(CButton))
	self.RemainTimesLabel = self.transform:Find("Anchor/Bottom/RemainTimesLabel"):GetComponent(typeof(UILabel))

	self:UpdateButton()

	local onDetail = function (go)
		self:OnDetailClick(go)
	end
	CommonDefs.AddOnClickListener(self.DetailBtn,DelegateFactory.Action_GameObject(onDetail),false)

	local onVote = function (go)
		self:OnVoteClick(go)
	end
	CommonDefs.AddOnClickListener(self.VoteBtn.gameObject,DelegateFactory.Action_GameObject(onVote),false)

	LuaBingQiPuMgr.QueryBQPPraiseCount()
end

function LuaBQPPromotionWnd:UpdateButton()
	self.RemainTimesLabel.text = LuaBingQiPuMgr.GetLikeStr()
	self.VoteBtn.Text = LocalString.GetString("点赞")
	self.VoteBtn.Enabled = LuaBingQiPuMgr.m_LikesLeft > 0

	if LuaBingQiPuMgr.m_PromoteData.UsedPraisedTimes > 0 then
		self.VoteBtn.Enabled = false
		self.VoteBtn.Text = LocalString.GetString("已赞")
	end
end

function LuaBQPPromotionWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateBQPPromotionInfo", self, "UpdateBQPPromotionInfo")
	g_ScriptEvent:AddListener("UpdateBQPStatus", self, "UpdateButton")
	g_ScriptEvent:AddListener("BQPPraiseDone",self, "BQPPraiseDone")
end

function LuaBQPPromotionWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateBQPPromotionInfo", self, "UpdateBQPPromotionInfo")
	g_ScriptEvent:RemoveListener("UpdateBQPStatus", self, "UpdateButton")
	g_ScriptEvent:RemoveListener("BQPPraiseDone",self, "BQPPraiseDone")
end

function LuaBQPPromotionWnd:BQPPraiseDone()
	LuaBingQiPuMgr.QueryBQPSponsor(LuaBingQiPuMgr.m_PromoteEquipInfo.Id)
end

function LuaBQPPromotionWnd:UpdateBQPPromotionInfo()
	self:Init()
end

function LuaBQPPromotionWnd:InitItem(go, info)
	local InfoLabel = go.transform:Find("Label"):GetComponent(typeof(UILabel))
	InfoLabel.text = SafeStringFormat3(LocalString.GetString("%s  %s"), tostring(info.rank), info.praiserName)
end


function LuaBQPPromotionWnd:OnDetailClick(go)
	Gac2Gas.QueryBQPEquipDetails(self.EquipInfo.Id)
end

function LuaBQPPromotionWnd:OnVoteClick(go)
	if Time.realtimeSinceStartup - self.LastEvaluateTime < self.EvaluateInterval then
		return
	end
	self.LastEvaluateTime = Time.realtimeSinceStartup
	LuaBingQiPuMgr.QueryBQPPraise(self.EquipInfo.Id, self.EquipInfo.TemplateId,false)	
end

-- 获得玩家每天每类点赞次数
function LuaBQPPromotionWnd:GetVoteTimesByXiuWei(xiuwei)
	BingQiPu_VoteNumLimit.ForeachKey(function (key)
		local data = BingQiPu_VoteNumLimit.GetData(key)
		if xiuwei >= data.XiuweiLimit then
			return data.VoteNum
		end
	end)
	return 0
end
