local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"

LuaDoubleOneQLDChangeSkillWnd = class()
RegistChildComponent(LuaDoubleOneQLDChangeSkillWnd,"closeBtn", GameObject)
RegistChildComponent(LuaDoubleOneQLDChangeSkillWnd,"node1", GameObject)
RegistChildComponent(LuaDoubleOneQLDChangeSkillWnd,"node2", GameObject)
RegistChildComponent(LuaDoubleOneQLDChangeSkillWnd,"node3", GameObject)
RegistChildComponent(LuaDoubleOneQLDChangeSkillWnd,"node4", GameObject)
RegistChildComponent(LuaDoubleOneQLDChangeSkillWnd,"node5", GameObject)
RegistChildComponent(LuaDoubleOneQLDChangeSkillWnd,"newNode", GameObject)
RegistChildComponent(LuaDoubleOneQLDChangeSkillWnd,"oldNode", GameObject)
RegistChildComponent(LuaDoubleOneQLDChangeSkillWnd,"changeBtn", GameObject)

RegistClassMember(LuaDoubleOneQLDChangeSkillWnd, "chooseSkillId")
RegistClassMember(LuaDoubleOneQLDChangeSkillWnd, "chooseLightNode")

function LuaDoubleOneQLDChangeSkillWnd:Close()
	CUIManager.CloseUI("DoubleOneQLDChangeSkillWnd")
end

function LuaDoubleOneQLDChangeSkillWnd:OnEnable()
	g_ScriptEvent:AddListener("DoubleOneQLDChangeSkill", self, "Init")
end

function LuaDoubleOneQLDChangeSkillWnd:OnDisable()
	g_ScriptEvent:RemoveListener("DoubleOneQLDChangeSkill", self, "Init")
end

function LuaDoubleOneQLDChangeSkillWnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	local onSubmitClick = function(go)
		if not self.chooseSkillId then
			return
		end
		Gac2Gas.ReplaceQiLinDongTempSkill(self.chooseSkillId,LuaDoubleOne2019Mgr.QLDNewSkillId)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.changeBtn,DelegateFactory.Action_GameObject(onSubmitClick),false)
  --LuaDoubleOne2019Mgr.QLDSkillTable = skillTable
  --LuaDoubleOne2019Mgr.QLDNewSkillId = skillId
  local skillNodeTable = {self.node1,self.node2,self.node3,self.node4,self.node5}
  for i,v in ipairs(LuaDoubleOne2019Mgr.QLDSkillTable) do
    self:InitSkillNode(skillNodeTable[i],v)
    if i == 1 then
      local highlightNode = skillNodeTable[i].transform:Find('highlight').gameObject
      --self.chooseSkillId = skillId
      self:InitSkillInfoNode(self.oldNode,v)
      self:ClickNode(v,highlightNode)
    end
  end
  self:InitSkillInfoNode(self.newNode,LuaDoubleOne2019Mgr.QLDNewSkillId)
end

function LuaDoubleOneQLDChangeSkillWnd:InitSkillNode(node,skillId)
  local skillInfo = Skill_AllSkills.GetData(skillId)

  node.transform:Find('Mask/Icon'):GetComponent(typeof(CUITexture)):LoadSkillIcon(skillInfo.SkillIcon)
  local highlightNode = node.transform:Find('highlight').gameObject
  highlightNode:SetActive(false)

	local onClick = function(go)
    --CSkillInfoMgr.ShowSkillInfoWnd(skillId, node.transform.position, CSkillInfoMgr.EnumSkillInfoContext.Default)
    self:ClickNode(skillId,highlightNode)
	end
	CommonDefs.AddOnClickListener(node,DelegateFactory.Action_GameObject(onClick),false)
end

function LuaDoubleOneQLDChangeSkillWnd:ClickNode(skillId,highlight)
  if self.chooseLightNode then
    self.chooseLightNode:SetActive(false)
  end
  self.chooseLightNode = highlight
  self.chooseLightNode:SetActive(true)

  self.chooseSkillId = skillId
  self:InitSkillInfoNode(self.oldNode,skillId)
end

function LuaDoubleOneQLDChangeSkillWnd:InitSkillInfoNode(node,skillId)
  local skillInfo = Skill_AllSkills.GetData(skillId)

  node.transform:Find('resNode/IconTexture'):GetComponent(typeof(CUITexture)):LoadSkillIcon(skillInfo.SkillIcon)
  node.transform:Find('des'):GetComponent(typeof(UILabel)).text = skillInfo.Display
  node.transform:Find('name'):GetComponent(typeof(UILabel)).text = skillInfo.Name
end

return LuaDoubleOneQLDChangeSkillWnd
