local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CLoginMgr = import "L10.Game.CLoginMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local Profession = import "L10.Game.Profession"
local ShareMgr = import "ShareMgr"

LuaLuoCha2021ResultWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaLuoCha2021ResultWnd, "ShareBtn", "ShareBtn", GameObject)
RegistChildComponent(LuaLuoCha2021ResultWnd, "PlayerInfoNode", "PlayerInfoNode", GameObject)
RegistChildComponent(LuaLuoCha2021ResultWnd, "MonsterTexture", "MonsterTexture", CUITexture)
RegistChildComponent(LuaLuoCha2021ResultWnd, "LuoChaFail", "LuoChaFail", GameObject)
RegistChildComponent(LuaLuoCha2021ResultWnd, "LuoChaSuccess", "LuoChaSuccess", GameObject)
RegistChildComponent(LuaLuoCha2021ResultWnd, "LuoChaSkillInfo", "LuoChaSkillInfo", GameObject)
RegistChildComponent(LuaLuoCha2021ResultWnd, "LuoChaDefeatLabel", "LuoChaDefeatLabel", UILabel)
RegistChildComponent(LuaLuoCha2021ResultWnd, "RenLeiPlayerInfo", "RenLeiPlayerInfo", GameObject)
RegistChildComponent(LuaLuoCha2021ResultWnd, "RenLeiFail", "RenLeiFail", GameObject)
RegistChildComponent(LuaLuoCha2021ResultWnd, "RenLeiSuccess", "RenLeiSuccess", GameObject)
RegistChildComponent(LuaLuoCha2021ResultWnd, "RenLeiResultLabel", "RenLeiResultLabel", UILabel)
RegistChildComponent(LuaLuoCha2021ResultWnd, "LuoCha", "LuoCha", GameObject)
RegistChildComponent(LuaLuoCha2021ResultWnd, "RenLei", "RenLei", GameObject)
RegistChildComponent(LuaLuoCha2021ResultWnd, "CloseBtn", "CloseBtn", GameObject)
--@endregion RegistChildComponent end

function LuaLuoCha2021ResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.ShareBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareBtnClick()
	end)


    --@endregion EventBind end
end

function LuaLuoCha2021ResultWnd:Init()
	self:InitPlayerInfo()
	self.ShareBtn.gameObject:SetActive(not CommonDefs.IS_VN_CLIENT)
	if LuaLuoCha2021Mgr.m_IsLuoCha then
		self:InitLuoChaInfo()
	else
		self:InitRenLeiInfo()
	end
end

function LuaLuoCha2021ResultWnd:InitRenLeiInfo()
	self.RenLei:SetActive(true)
	self.LuoCha:SetActive(false)

	-- 胜利
	self.RenLeiSuccess:SetActive(LuaLuoCha2021Mgr.m_IsWinner)
	self.RenLeiFail:SetActive(not LuaLuoCha2021Mgr.m_IsWinner)

	-- 得分
	self.RenLeiResultLabel.text = SafeStringFormat3("[FF5050]%s[-]/%s", tostring(LuaLuoCha2021Mgr.m_ResultCount), 
		tostring(LuoChaHaiShi_Setting.GetData().FortNeedPickCount))

	-- 玩家信息
	local extraData = LuaLuoCha2021Mgr.m_ExtraInfo
	local playerData = {}
	for i=0, extraData.Count-1 do
		local infoList = extraData[i]
		local playerId = infoList[0]
		local cls = infoList[1]
		local pickCount = infoList[2]

		local info = {}
		info.playerName = playerId
		info.cls = cls
		info.pickCount = pickCount

		table.insert(playerData, info)
	end

	-- 进行排序
	table.sort(playerData, function(a, b)
		if a.pickCount == b.pickCount then
			return a.playerName > b.playerName
		else
			return a.pickCount > b.pickCount
		end
	end)


	for i=1, 5 do
		local info = self.RenLeiPlayerInfo.transform:Find(tostring(i)).gameObject
		if i <= #playerData then 
			local nameLabel = info.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
			local clsSprite = info.transform:Find("ClsSprite"):GetComponent(typeof(UISprite))
			local rewardLabel = info.transform:Find("RewardLabel"):GetComponent(typeof(UILabel))

			local info = playerData[i]
			local playerId = info.playerName
			local cls = info.cls
			local pickCount = info.pickCount

			local colorTint = "[DAFCFC]"
			if playerId == CClientMainPlayer.Inst.Name then
				colorTint = "[9BF45C]"
			end

			nameLabel.text = colorTint.. tostring(playerId)
			clsSprite.spriteName = Profession.GetIconByNumber(cls)
			rewardLabel.text = colorTint.. "+" .. tostring(pickCount)
		else
			info:SetActive(false)
		end
	end
end

function LuaLuoCha2021ResultWnd:InitLuoChaInfo()
	self.RenLei:SetActive(false)
	self.LuoCha:SetActive(true)

	-- 胜利
	self.LuoChaSuccess:SetActive(LuaLuoCha2021Mgr.m_IsWinner)
	self.LuoChaFail:SetActive(not LuaLuoCha2021Mgr.m_IsWinner)

	-- 得分
	self.LuoChaDefeatLabel.text = SafeStringFormat3("[FF5050]%s[-]/%s", tostring(LuaLuoCha2021Mgr.m_ResultCount), tostring(LuaLuoCha2021Mgr.m_ResultTotalCount))

	-- 玩家信息
	local extraData = LuaLuoCha2021Mgr.m_ExtraInfo

	for i=1,3 do
		local info = self.LuoChaSkillInfo.transform:Find(tostring(i)).gameObject
		local skillLv = info.transform:Find("Plane/SkillLv"):GetComponent(typeof(UILabel))
		local skillIcon = info.transform:Find("Mask/SkillIcon"):GetComponent(typeof(CUITexture))
		local lock = info.transform:Find("Mask/Sprite").gameObject

		if i <= extraData.Count then
			local data = extraData[i-1]
			local skillId = data[0]
			local lv = data[1]

			lock:SetActive(false)
			local skillData = Skill_AllSkills.GetData(skillId)
			if skillData then
				skillIcon:LoadSkillIcon(skillData.SkillIcon)
			end
			skillLv.text = "lv." .. tostring(lv)
		else
			skillLv.gameObject:SetActive(false)
		end
	end
end

function LuaLuoCha2021ResultWnd:InitPlayerInfo()
	if CLoginMgr.Inst then
		local myGameServer = CLoginMgr.Inst:GetSelectedGameServer()
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Server"), typeof(UILabel)).text = myGameServer.name
	end

	if CClientMainPlayer.Inst then
		if LuaLuoCha2021Mgr.m_Name then
			CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Name"), typeof(UILabel)).text = LuaLuoCha2021Mgr.m_Name
		else
			CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Name"), typeof(UILabel)).text = CClientMainPlayer.Inst.Name
		end
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Icon/Lv"), typeof(UILabel)).text = tostring(CClientMainPlayer.Inst.Level)
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Icon"), typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(CClientMainPlayer.Inst.Class, CClientMainPlayer.Inst.Gender, -1), false)
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("ID"), typeof(UILabel)).text = tostring(CClientMainPlayer.Inst.Id)
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Server"), typeof(UILabel)).text = CClientMainPlayer.Inst:GetMyServerName()
	elseif LuaLuoCha2021Mgr.m_Id then
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Name"), typeof(UILabel)).text = LuaLuoCha2021Mgr.m_Name
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Icon/Lv"), typeof(UILabel)).text = tostring(LuaLuoCha2021Mgr.m_Level)
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Icon"), typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(LuaLuoCha2021Mgr.m_Class, LuaLuoCha2021Mgr.m_Gender, -1), false)
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("ID"), typeof(UILabel)).text = tostring(LuaLuoCha2021Mgr.m_Id)
		CommonDefs.GetComponent_Component_Type(self.PlayerInfoNode.transform:Find("Server"), typeof(UILabel)).text = LuaLuoCha2021Mgr.m_ServerName
	end
end

--@region UIEvent

function LuaLuoCha2021ResultWnd:OnShareBtnClick()
	self.CloseBtn.gameObject:SetActive(false)
	CUICommonDef.CaptureScreen("screenshot", true, true, self.ShareBtn, DelegateFactory.Action_string_bytes(function(path,bytes)
		ShareMgr.ShareLocalImage2PersonalSpace(path, nil)
		self.CloseBtn.gameObject:SetActive(true)
	  end))
end

--@endregion UIEvent

