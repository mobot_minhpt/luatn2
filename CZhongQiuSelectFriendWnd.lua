-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CIMMgr = import "L10.Game.CIMMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CRelationshipBasicPlayerInfo = import "L10.Game.CRelationshipBasicPlayerInfo"
local CZhongQiuSelectFriendItem = import "L10.UI.CZhongQiuSelectFriendItem"
local CZhongQiuSelectFriendWnd = import "L10.UI.CZhongQiuSelectFriendWnd"
local DelegateFactory = import "DelegateFactory"
CZhongQiuSelectFriendWnd.m_Init_CS2LuaHook = function (this)

    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer == nil then
        return
    end
    local friends = CIMMgr.Inst.Friends
    CommonDefs.EnumerableIterate(friends, DelegateFactory.Action_object(function (___value)
        local playerId = ___value
        local info = CIMMgr.Inst:GetBasicInfo(playerId)
        --去掉系统消息，倩女小精灵、杨洋
        local online = CIMMgr.Inst:IsOnline(playerId)

        CommonDefs.ListAdd(this.mFriendListInfo, typeof(CRelationshipBasicPlayerInfo), info)
        
    end))
    CommonDefs.ListSort1(this.mFriendListInfo, typeof(CRelationshipBasicPlayerInfo), MakeDelegateFromCSFunction(this.SortFunc, MakeGenericClass(Comparison, CRelationshipBasicPlayerInfo), this))

    this.tableView.m_DataSource = this
    this.tableView:ReloadData(true, false)

    -- 明星邀请赛复用
    if CLuaStarBiwuMgr.m_bSelectFriend then
      this.transform:Find("Wnd_Bg_Secondary_3/TitleLabel"):GetComponent(typeof(UILabel)).text = LocalString.GetString("邀请战队成员")
    end
    --告白烟花复用
    if LuaFireWorkPartyMgr.SelectFriendOpenForFP then
        this.transform:Find("Wnd_Bg_Secondary_3/TitleLabel"):GetComponent(typeof(UILabel)).text = LocalString.GetString("选择好友")
        this.searchButton:SetActive(false)
    end
end
CZhongQiuSelectFriendWnd.m_SortFunc_CS2LuaHook = function (this, p1, p2)
    local mainPlayer = CClientMainPlayer.Inst
    if mainPlayer == nil then
        return 0
    end
    local myGender = EnumToInt(mainPlayer.Gender)
    local online1 = CIMMgr.Inst:IsOnline(p1.ID)
    local online2 = CIMMgr.Inst:IsOnline(p2.ID)
    local friendly1 = CIMMgr.Inst:GetFriendliness(p1.ID)
    local friendly2 = CIMMgr.Inst:GetFriendliness(p2.ID)
    if p1.Gender ~= myGender and p2.Gender == myGender then
        return - 1
    elseif p1.Gender == myGender and p2.Gender ~= myGender then
        return 1
    elseif online1 and not online2 then
        return - 1
    elseif not online1 and online2 then
        return 1
    elseif friendly1 > friendly2 then
        return - 1
    elseif friendly1 < friendly2 then
        return 1
    end
    return 0
end
CZhongQiuSelectFriendWnd.m_NumberOfRows_CS2LuaHook = function (this, view)
    if this.mFriendListInfo ~= nil then
        return this.mFriendListInfo.Count
    end
    return 0
end
CZhongQiuSelectFriendWnd.m_ItemAt_CS2LuaHook = function (this, view, row)
    if this.mFriendListInfo.Count > row then
        local cmp = TypeAs(this.tableView:GetFromPool(0), typeof(CZhongQiuSelectFriendItem))
        if cmp ~= nil then
            cmp:Init(this.mFriendListInfo[row])
            return cmp
        end
    end
    return nil
end
