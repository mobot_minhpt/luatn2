local UISprite = import "UISprite"

local UITable = import "UITable"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local CScene = import "L10.Game.CScene"
local CGamePlayMgr = import "L10.Game.CGamePlayMgr"
local CTickMgr = import "L10.Engine.CTickMgr"
local ETickType = import "L10.Engine.ETickType"
local CUICommonDef = import "L10.UI.CUICommonDef"

LuaShenYaoTaskView = class()
LuaShenYaoTaskView.m_TaskStageGamePlayId = nil
LuaShenYaoTaskView.m_TaskSategTitle = nil
LuaShenYaoTaskView.m_TaskContent = nil

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShenYaoTaskView, "LeftBtn", "LeftBtn", GameObject)
RegistChildComponent(LuaShenYaoTaskView, "RightBtn", "RightBtn", GameObject)
RegistChildComponent(LuaShenYaoTaskView, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaShenYaoTaskView, "ContentLabel", "ContentLabel", UILabel)
RegistChildComponent(LuaShenYaoTaskView, "Table", "Table", UITable)
RegistChildComponent(LuaShenYaoTaskView, "BgTexture", "BgTexture", UISprite)

--@endregion RegistChildComponent end

RegistClassMember(LuaShenYaoTaskView, "m_Content")

function LuaShenYaoTaskView:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.LeftBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnLeftBtnClick()
	end)


	
	UIEventListener.Get(self.RightBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRightBtnClick()
	end)


    --@endregion EventBind end
end

function LuaShenYaoTaskView:Init()
	
end

function LuaShenYaoTaskView:OnEnable()
	self:OnUpdateGamePlayStageInfo(LuaShenYaoTaskView.m_TaskStageGamePlayId , LuaShenYaoTaskView.m_TaskSategTitle, LuaShenYaoTaskView.m_TaskContent)
	g_ScriptEvent:AddListener("UpdateGamePlayStageInfo", self, "OnUpdateGamePlayStageInfo")
	g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "CountdownTickFunc")
end

function LuaShenYaoTaskView:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateGamePlayStageInfo", self, "OnUpdateGamePlayStageInfo")
	g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "CountdownTickFunc")
end

--@region UIEvent

function LuaShenYaoTaskView:OnLeftBtnClick()
	CGamePlayMgr.Inst:LeavePlay()
end

function LuaShenYaoTaskView:OnRightBtnClick()
	g_MessageMgr:ShowMessage(XinBaiLianDong_ShenYaoTips.GetData(CScene.MainScene.GamePlayDesignId).Tips)
end

--@endregion UIEvent

function LuaShenYaoTaskView:GetRemainTimeText(totalSeconds)
	if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        return SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end

function LuaShenYaoTaskView:CountdownTickFunc()
	self.ContentLabel.text = self.m_Content.."\n[c][ACF9FF]"..LocalString.GetString("剩余时间：").."[FFFFFF]"..self:GetRemainTimeText(CScene.MainScene.ShowTime)
end

function LuaShenYaoTaskView:OnUpdateGamePlayStageInfo(gameplayId, title, content)
	self.TitleLabel.text = title and CUICommonDef.TranslateToNGUIText(title) or ""
	self.m_Content = content and CUICommonDef.TranslateToNGUIText(content) or ""

	self:CountdownTickFunc()

	--self.Table:Reposition()
	self.BgTexture.height = math.max(130, NGUIMath.CalculateRelativeWidgetBounds(self.Table.transform).size.y + 30)
end
