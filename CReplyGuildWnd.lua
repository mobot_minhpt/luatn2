-- Auto Generated!!
local CChatHelper = import "L10.UI.CChatHelper"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CGuildCreationInfoRpc_Item = import "L10.Game.CGuildCreationInfoRpc_Item"
local CGuildCreationItem = import "L10.UI.CGuildCreationItem"
local CGuildMgr = import "L10.Game.CGuildMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CReplyGuildWnd = import "L10.UI.CReplyGuildWnd"
local CTickMgr = import "L10.Engine.CTickMgr"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local ETickType = import "L10.Engine.ETickType"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local Object = import "System.Object"
local QnButton = import "L10.UI.QnButton"
local String = import "System.String"
local UIInput = import "UIInput"
CReplyGuildWnd.m_Start_CS2LuaHook = function (this) 
    this.m_GuildAim.Text = ""

    this.m_GuildList.m_DataSource = this
    this.m_GuildList.OnSelectAtRow = CommonDefs.CombineListner_Action_int(this.m_GuildList.OnSelectAtRow, MakeDelegateFromCSFunction(this.OnGuildListSelectAtRow, MakeGenericClass(Action1, Int32), this), true)

    this.m_ReplyButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_ReplyButton.OnClick, MakeDelegateFromCSFunction(this.OnResponseButtonClick, MakeGenericClass(Action1, QnButton), this), true)
    this.m_CancelReplyButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_CancelReplyButton.OnClick, MakeDelegateFromCSFunction(this.OnCancelResponseButtonClick, MakeGenericClass(Action1, QnButton), this), true)
    this.m_ContactMasterButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_ContactMasterButton.OnClick, MakeDelegateFromCSFunction(this.OnContactLeaderButtonClick, MakeGenericClass(Action1, QnButton), this), true)
    this.m_SearchButton.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_SearchButton.OnClick, MakeDelegateFromCSFunction(this.OnSearchButtonClick, MakeGenericClass(Action1, QnButton), this), true)

    this.m_SortButtonGuildId.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_SortButtonGuildId.OnClick, MakeDelegateFromCSFunction(this.OnSortButtonGuildIdClick, MakeGenericClass(Action1, QnButton), this), true)
    this.m_SortButtonResponse.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_SortButtonResponse.OnClick, MakeDelegateFromCSFunction(this.OnSortButtonResponseClick, MakeGenericClass(Action1, QnButton), this), true)
    this.m_SortButtonTimeLeft.OnClick = CommonDefs.CombineListner_Action_QnButton(this.m_SortButtonTimeLeft.OnClick, MakeDelegateFromCSFunction(this.OnSortButtonTimeLeftClick, MakeGenericClass(Action1, QnButton), this), true)

    CommonDefs.AddOnClickListener(this.transform:Find("Offset/ReportButton").gameObject, DelegateFactory.Action_GameObject(function(go)
         local itemData = this:_GetSelectedItemData()
        if itemData ~= nil then
            LuaPlayerReportMgr:ShowGuildReportWnd(itemData.GuildId, itemData.Name, "banghui",
                LocalString.GetString("帮会举报"), LocalString.GetString("举报帮会"))
        else
            g_MessageMgr:ShowMessage("Guild_Report_Need_Choose_Guild")
        end
    end), false)
end
CReplyGuildWnd.m_ItemAt_CS2LuaHook = function (this, view, row) 
    local itemData = this:_GetItemDataAt(row)
    if itemData ~= nil then
        local item = TypeAs(this.m_GuildList:GetFromPool(0), typeof(CGuildCreationItem))
        if item ~= nil then
            item:UpdateData(itemData)
            local default
            if row % 2 == 1 then
                default = g_sprites.OddBgSpirite
            else
                default = g_sprites.EvenBgSprite
            end
            item:SetBackgroundTexture(default)
            return item
        end
    end
    local defaultItem = TypeAs(this.m_GuildList:GetFromPool(0), typeof(CGuildCreationItem))
    local extern
    if row % 2 == 1 then
        extern = g_sprites.OddBgSpirite
    else
        extern = g_sprites.EvenBgSprite
    end
    defaultItem:SetBackgroundTexture(extern)
    return defaultItem
end
CReplyGuildWnd.m_OnEnable_CS2LuaHook = function (this) 
    EventManager.AddListenerInternal(EnumEventType.RequestOperationInGuildSucceed, MakeDelegateFromCSFunction(this.OnRequestOperationInGuildSucceed, MakeGenericClass(Action2, String, String), this))
    EventManager.AddListener(EnumEventType.GuildAllCreationInfoReceived, MakeDelegateFromCSFunction(this.OnGuildAllCreationInfoReceived, Action0, this))
    CGuildMgr.Inst:GetAllGuildsCreation()

    this.m_SearchInputField.OnValueChanged = DelegateFactory.Action_string(function (s) 
        if not Extensions.IsNumeric(s) then
            CommonDefs.GetComponent_Component_Type(this.m_SearchInputField, typeof(UIInput)).value = Extensions.NumericString(s)
            g_MessageMgr:ShowMessage("Input_Only_Number")
        end
    end)

    this.m_CountDownTick = CTickMgr.Register(MakeDelegateFromCSFunction(this.UpdateCountdown, Action0, this), 1000, ETickType.Loop)
end
CReplyGuildWnd.m_UpdateCountdown_CS2LuaHook = function (this) 
    local count = this:_GetItemDataCount()
    do
        local i = 0
        while i < count do
            local item = TypeAs(this.m_GuildList:GetItemAtRow(i), typeof(CGuildCreationItem))
            if item ~= nil then
                item:CountDown()
            end
            i = i + 1
        end
    end
end
CReplyGuildWnd.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListenerInternal(EnumEventType.RequestOperationInGuildSucceed, MakeDelegateFromCSFunction(this.OnRequestOperationInGuildSucceed, MakeGenericClass(Action2, String, String), this))
    EventManager.RemoveListener(EnumEventType.GuildAllCreationInfoReceived, MakeDelegateFromCSFunction(this.OnGuildAllCreationInfoReceived, Action0, this))
    this.searchedGuildId = - 1

    if this.m_CountDownTick ~= nil then
        invoke(this.m_CountDownTick)
    end
end
CReplyGuildWnd.m_OnRequestOperationInGuildSucceed_CS2LuaHook = function (this, operationType, paramStr) 
    if (("ResponseGuild") == operationType) or (("CancelResponseGuild") == operationType) then
        CGuildMgr.Inst:GetAllGuildsCreation()
    end
end
CReplyGuildWnd.m_OnGuildListSelectAtRow_CS2LuaHook = function (this, row) 
    this.currentIndex = row
    local misson = ""
    if this.searchedGuildId > 0 then
        local item = this:_GetSearchedItemData()
        if item ~= nil then
            misson = item.Mission
        end
    elseif CGuildMgr.Inst.AllGuildCreationInfo[row] ~= nil then
        misson = CGuildMgr.Inst.AllGuildCreationInfo[row].Mission
    end

    this.m_GuildAim.Text = misson
end
CReplyGuildWnd.m_OnResponseButtonClick_CS2LuaHook = function (this, button) 
    local itemData = this:_GetSelectedItemData()
    if itemData ~= nil then
        local doubleGuildId = tonumber(itemData.GuildId)
        Gac2Gas.RequestOperationInGuild(doubleGuildId, "ResponseGuild", "", "")
        --CGuildMgr.Inst.GetAllGuildsCreation();
    else
        g_MessageMgr:ShowMessage("GUILD_RESPONSE_CHOOSE_ONE")
    end
end
CReplyGuildWnd.m_OnCancelResponseButtonClick_CS2LuaHook = function (this, button) 
    local itemData = this:_GetSelectedItemData()
    if itemData ~= nil and itemData.Responsed == 1 then
        local doubleGuildId = tonumber(itemData.GuildId)

        local datas = InitializeList(CreateFromClass(MakeGenericClass(List, Object)), Object, itemData.Name)
        local message = g_MessageMgr:FormatMessage("GUILD_RESPONSE_CANCEL_CONFIRM", CommonDefs.ListToArray(datas))
        MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function () 
            Gac2Gas.RequestOperationInGuild(doubleGuildId, "CancelResponseGuild", "", "")
        end), nil, nil, nil, false)
    elseif itemData ~= nil and itemData.Responsed ~= 1 then
        g_MessageMgr:ShowMessage("Guild_Not_Response")
    elseif itemData == nil then
        g_MessageMgr:ShowMessage("GUILD_CHOOSE_ONE")
    elseif CGuildMgr.Inst.AllGuildCreationInfo ~= nil then
        local bNone = true
        do
            local i = 0
            while i < CGuildMgr.Inst.AllGuildCreationInfo.Count do
                local guildCreation = CGuildMgr.Inst.AllGuildCreationInfo[i]
                if guildCreation.Responsed == 1 then
                    bNone = false
                end
                i = i + 1
            end
        end
        if bNone then
            g_MessageMgr:ShowMessage("GUILD_RESPONSE_NONE")
        end
    end
end
CReplyGuildWnd.m_OnContactLeaderButtonClick_CS2LuaHook = function (this, button) 
    local itemData = this:_GetSelectedItemData()
    if itemData ~= nil then
        if CClientMainPlayer.Inst.Id == itemData.FounderId then
            -- 帮主就是本人
            g_MessageMgr:ShowMessage("GUILD_CONTACT_LEADER_SELF")
            return
        end
        CChatHelper.ShowFriendWnd(itemData.FounderId, itemData.FounderName)
    end
end
CReplyGuildWnd.m_OnSortButtonGuildIdClick_CS2LuaHook = function (this, button) 
    if this.searchedGuildId > 0 then
        return
    end
    CommonDefs.ListSort1(CGuildMgr.Inst.AllGuildCreationInfo, typeof(CGuildCreationInfoRpc_Item), DelegateFactory.Comparison_CGuildCreationInfoRpc_Item(function (item1, item2) 
        if item1.GuildId > item2.GuildId then
            return this.sortFlagGuildId
        elseif item1.GuildId < item2.GuildId then
            return - this.sortFlagGuildId
        end
        return 0
    end))
    this.sortFlagGuildId = - this.sortFlagGuildId
    this.m_GuildList:ReloadData(true, true)
end
CReplyGuildWnd.m_OnSortButtonResponseClick_CS2LuaHook = function (this, button) 
    if this.searchedGuildId > 0 then
        return
    end
    CommonDefs.ListSort1(CGuildMgr.Inst.AllGuildCreationInfo, typeof(CGuildCreationInfoRpc_Item), DelegateFactory.Comparison_CGuildCreationInfoRpc_Item(function (item1, item2) 
        if item1.ResponseNum > item2.ResponseNum then
            return this.sortFlagResponse
        elseif item1.ResponseNum < item2.ResponseNum then
            return - this.sortFlagResponse
        end
        return 0
    end))
    this.sortFlagResponse = - this.sortFlagResponse
    this.m_GuildList:ReloadData(true, false)
end
CReplyGuildWnd.m_OnSortButtonTimeLeftClick_CS2LuaHook = function (this, button) 
    if this.searchedGuildId > 0 then
        return
    end
    CommonDefs.ListSort1(CGuildMgr.Inst.AllGuildCreationInfo, typeof(CGuildCreationInfoRpc_Item), DelegateFactory.Comparison_CGuildCreationInfoRpc_Item(function (item1, item2) 
        if item1.TimeLeft < item2.TimeLeft then
            return this.sortFlagTimeLeft
        elseif item1.TimeLeft > item2.TimeLeft then
            return - this.sortFlagTimeLeft
        end
        return 0
    end))
    this.sortFlagTimeLeft = - this.sortFlagTimeLeft
    this.m_GuildList:ReloadData(true, false)
end
CReplyGuildWnd.m__GetItemDataCount_CS2LuaHook = function (this) 
    if CGuildMgr.Inst.AllGuildCreationInfo == nil then
        return 0
    end
    if this.searchedGuildId > 0 then
        local item = this:_GetSearchedItemData()
        if item ~= nil then
            return 1
        else
            local datas = InitializeList(CreateFromClass(MakeGenericClass(List, Object)), Object, LocalString.GetString("该帮会不存在"))
            g_MessageMgr:ShowMessage("CUSTOM_STRING", List2Params(datas))
            this.searchedGuildId = - 1
            return CGuildMgr.Inst.AllGuildCreationInfo.Count
        end
    else
        return CGuildMgr.Inst.AllGuildCreationInfo.Count
    end
end
CReplyGuildWnd.m__GetItemDataAt_CS2LuaHook = function (this, row) 
    if this.searchedGuildId > 0 then
        local item = this:_GetSearchedItemData()
        if item ~= nil then
            return item
        end
    else
        local item = CGuildMgr.Inst.AllGuildCreationInfo[row]
        return item
    end
    return nil
end
CReplyGuildWnd.m__GetSearchedItemData_CS2LuaHook = function (this) 
    do
        local __itr_is_triger_return = nil
        local __itr_result = nil
        CommonDefs.ListIterateWithRet(CGuildMgr.Inst.AllGuildCreationInfo, DelegateFactory.SingleValIterFunc(function (___value) 
            local item = ___value
            if item.GuildId == this.searchedGuildId then
                __itr_is_triger_return = true
                __itr_result = item
                return true
            end
        end))
        if __itr_is_triger_return == true then
            return __itr_result
        end
    end
    return nil
end
CReplyGuildWnd.m__GetSelectedItemData_CS2LuaHook = function (this) 
    if this.searchedGuildId > 0 then
        if this.currentIndex >= 0 then
            return this:_GetSearchedItemData()
        end
    end
    if CGuildMgr.Inst.AllGuildCreationInfo.Count == 0 or this.currentIndex < 0 or this.currentIndex > CGuildMgr.Inst.AllGuildCreationInfo.Count then
        return nil
    end
    return CGuildMgr.Inst.AllGuildCreationInfo[this.currentIndex]
end
