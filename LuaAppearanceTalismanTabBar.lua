local CUITexture = import "L10.UI.CUITexture"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local UISprite = import "UISprite"

LuaAppearanceTalismanTabBar = class()

RegistChildComponent(LuaAppearanceTalismanTabBar,"m_TabRoot", "TabRoot", GameObject)

RegistClassMember(LuaAppearanceTalismanTabBar, "m_SelectedIndex")
RegistClassMember(LuaAppearanceTalismanTabBar, "m_OnTabChangeFunc")


function LuaAppearanceTalismanTabBar:Init(specialAppearInfo, onTabChangeFunc)
    self.m_SelectedIndex = -1
    self.m_OnTabChangeFunc = onTabChangeFunc
    local childCount = self.m_TabRoot.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_TabRoot.transform:GetChild(i).gameObject
        if i<#specialAppearInfo then
            childGo:SetActive(true)
            self:InitItem(childGo, i, specialAppearInfo[i+1].id, specialAppearInfo[i+1].fxId, i+1==#specialAppearInfo)
        else
            childGo:SetActive(false)
        end
    end
end

function LuaAppearanceTalismanTabBar:GetSelectedIndex()
    return self.m_SelectedIndex
end

--切换选中的节点，index下标从0开始, ignoreCallback为true时不会有回调发生
function LuaAppearanceTalismanTabBar:ChangeTab(index, ignoreCallback)
    if index<0 or index>=self.m_TabRoot.transform.childCount then
        return
    end
    local child = self.m_TabRoot.transform:GetChild(index)
    self:OnItemClick(child.gameObject, ignoreCallback)
end

function LuaAppearanceTalismanTabBar:InitItem(itemGo, childIndex, talismanId, talismanFxId, isLast)
    local lineGo = itemGo.transform:Find("Line").gameObject
    local selectedGo = itemGo.transform:Find("Selected").gameObject
    local cornerGo = itemGo.transform:Find("Corner").gameObject

    lineGo:SetActive(not isLast)
    selectedGo:SetActive(childIndex==self.m_SelectedIndex)
    cornerGo:SetActive(LuaAppearancePreviewMgr:IsCurrentInUseTalismanAppear(talismanId))

    local owned = LuaTalismanMgr:CheckContain(talismanId)
    CUICommonDef.SetGrey(itemGo, not owned)

    UIEventListener.Get(itemGo).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnItemClick(itemGo, false)
    end)
end

function LuaAppearanceTalismanTabBar:OnItemClick(itemGo, ignoreCallback)
    local childCount = self.m_TabRoot.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_TabRoot.transform:GetChild(i).gameObject
        if childGo == itemGo then
            childGo.transform:Find("Selected").gameObject:SetActive(true)
            self.m_SelectedIndex = i
            if not ignoreCallback and self.m_OnTabChangeFunc then
                self.m_OnTabChangeFunc(itemGo, self.m_SelectedIndex)
            end
        else
            childGo.transform:Find("Selected").gameObject:SetActive(false)
        end
    end
end