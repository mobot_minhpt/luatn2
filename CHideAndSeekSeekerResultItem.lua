-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CHideAndSeekSeekerResultItem = import "L10.UI.CHideAndSeekSeekerResultItem"
local Color = import "UnityEngine.Color"
local Profession = import "L10.Game.Profession"
CHideAndSeekSeekerResultItem.m_Init_CS2LuaHook = function (this, info, row) 
    if row % 2 == 0 then
        this.bgSprite.alpha = 0
    else
        this.bgSprite.alpha = 1
    end

    this.nameLabel.text = info.name
    this.clsSprite.spriteName = Profession.GetIcon(info.cls)
    this.findCountLabel.text = tostring(info.catchHiderCount)
    this.remainSeekTimeLabel.text = tostring(info.remainSeekTimes)

    if CClientMainPlayer.Inst ~= nil and (info.name == CClientMainPlayer.Inst.Name) then
        
        this.nameLabel.color = Color.green
        this.nameLabel.alpha = 1
        this.findCountLabel.color = Color.green
        this.findCountLabel.alpha = 1
        this.remainSeekTimeLabel.color = Color.green
        this.remainSeekTimeLabel.alpha = 1
    else
        this.nameLabel.color = Color.white
        this.nameLabel.alpha = 1
        this.findCountLabel.color = Color.white
        this.findCountLabel.alpha = 1
        this.remainSeekTimeLabel.color = Color.white
        this.remainSeekTimeLabel.alpha = 1
    end
    
end
