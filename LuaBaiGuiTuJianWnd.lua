
local TweenWidth = import "TweenWidth"
local UITable = import "UITable"
local UIEventListener = import "UIEventListener"
local LuaUtils = import "LuaUtils"

CLuaBaiGuiTuJianWnd = class()
RegistClassMember(CLuaBaiGuiTuJianWnd, "m_Table")
RegistClassMember(CLuaBaiGuiTuJianWnd, "m_TemplateObj")

--Data
RegistClassMember(CLuaBaiGuiTuJianWnd, "m_Obj2IdTable")
RegistClassMember(CLuaBaiGuiTuJianWnd, "m_Obj2LockedTable")
RegistClassMember(CLuaBaiGuiTuJianWnd, "m_Fx")


function CLuaBaiGuiTuJianWnd:Awake( ... )
	local contentRoot = self.transform:Find("Anchor/Content").gameObject
	contentRoot:SetActive(false)
	local tweenWidth = self.transform:Find("Anchor/WndBg/Bg"):GetComponent(typeof(TweenWidth))
	CommonDefs.AddEventDelegate(tweenWidth.onFinished, DelegateFactory.Action(function ( ... )
		contentRoot:SetActive(true)
		self:ShowFx()
	end))
	self.m_Table = self.transform:Find("Anchor/Content/Scroll View/Table"):GetComponent(typeof(UITable))
	self.m_TemplateObj = self.transform:Find("Anchor/Content/Scroll View/Template").gameObject
	self.m_TemplateObj:SetActive(false)
	UIEventListener.Get(self.transform:Find("Anchor/Content/TipBtn").gameObject).onClick = LuaUtils.VoidDelegate(function ( ... )
		g_MessageMgr:ShowMessage("BaiGuiTuJian_Tip")
	end)
end

function CLuaBaiGuiTuJianWnd:ShowFx( ... )
	if self.m_Fx then
		self.m_Fx:LoadFx("Fx/UI/Prefab/UI_baiguitujian_suo.prefab", 1, false)
	end
	self.m_Fx = nil
end

function CLuaBaiGuiTuJianWnd:Init( ... )

end

function CLuaBaiGuiTuJianWnd:OnIconClick(go)

end

function CLuaBaiGuiTuJianWnd:OnDestroy( ... )
	LuaShenbingMgr.m_CurrentTuJianId = 0
end

return CLuaBaiGuiTuJianWnd
