-- Auto Generated!!
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CHouseStuffMakeWnd = import "L10.UI.CHouseStuffMakeWnd"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CLivingItemDisplay = import "L10.UI.CLivingItemDisplay"
local CLivingMakeConsumeItem = import "L10.UI.CLivingMakeConsumeItem"
local CLivingSkillMgr = import "L10.Game.CLivingSkillMgr"
local CLivingSurpriseItem = import "L10.UI.CLivingSurpriseItem"
local CommonDefs = import "L10.Game.CommonDefs"
local Constants = import "L10.Game.Constants"
local CSwitchMgr = import "L10.Engine.CSwitchMgr"
local CTrackMgr = import "L10.Game.CTrackMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local Double = import "System.Double"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Gac2Gas = import "L10.Game.Gac2Gas"
local GameObject = import "UnityEngine.GameObject"
local LifeSkill_Item = import "L10.Game.LifeSkill_Item"
local LifeSkill_SkillType = import "L10.Game.LifeSkill_SkillType"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local PublicMap_PublicMap = import "L10.Game.PublicMap_PublicMap"
local String = import "System.String"
local StringBuilder = import "System.Text.StringBuilder"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local UIScrollView = import "UIScrollView"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CLivingItemDisplay.m_TyCollect_CS2LuaHook = function (this, go) 
    if CLivingItemDisplay.sbDisableCollectCrossServer then
        CUICommonDef.CallMethodConsiderCrossServer(MakeDelegateFromCSFunction(this.Collect, Action0, this), {})
    else
        this:Collect()
    end
end
CLivingItemDisplay.m_OnNoHouse_CS2LuaHook = function (this) 
    local message = g_MessageMgr:FormatMessage("BUY_HOUSE_TIP")
    MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function () 
        CTrackMgr.Inst:FindNPC(Constants.JiayuanNpcId, Constants.HangZhouID, 89, 38, nil, nil)
    end), nil, LocalString.GetString("前往"), LocalString.GetString("取消"), false)
end
CLivingItemDisplay.m_Collect_CS2LuaHook = function (this) 
    local skillTypeData = LifeSkill_SkillType.GetData(this.m_SkillClsId)
    local skillType = skillTypeData.Type

    if skillType == LivingSkillType_lua.Makedrug or skillType == LivingSkillType_lua.Cooking then
        CUIManager.ShowUI(CUIResources.LivingSkillMakeWnd)
        return
    end

    if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.IsInGamePlay then
        g_MessageMgr:ShowMessage("CANNOT_FIND_MAP")
        return
    end
    --先检查一下能不能钓鱼
    local data = LifeSkill_Item.GetData(this.m_TemplateId)
    if not CLivingSkillMgr.Inst:CheckHuoli(data.ID) then
        return
    end

    if not CLivingSkillMgr.Inst:CheckPackage(data.ID) then
        return
    end
    CUIManager.ShowUI(CUIResources.LivingSkillCountInputWnd)
end
CLivingItemDisplay.m_TryMake_CS2LuaHook = function (this, go) 
    if CLivingItemDisplay.sbDisableMakeCrossServer then
        CUICommonDef.CallMethodConsiderCrossServer(MakeDelegateFromCSFunction(this.Make, Action0, this), {})
    else
        this:Make()
    end
end
CLivingItemDisplay.m_TryOpenMakeWnd_CS2LuaHook = function (this, go) 
    if CLivingItemDisplay.sbDisableMakeCrossServer then
        CUICommonDef.CallMethodConsiderCrossServer(MakeDelegateFromCSFunction(this.OpenMakeWnd, Action0, this), {})
    else
        this:OpenMakeWnd()
    end
end
CLivingItemDisplay.m_OpenMakeWnd_CS2LuaHook = function (this) 
    --先处理工艺制作
    local skillTypeData = LifeSkill_SkillType.GetData(this.m_SkillClsId)
    local itemData = LifeSkill_Item.GetData(this.m_TemplateId)

    local skillType = skillTypeData.Type
    if skillType == 5 and itemData.Type ~= 12 then
        if not CSwitchMgr.EnableHouse then
            g_MessageMgr:ShowMessage("FUNCTION_NOT_OPEN")
            return
        end

        CHouseStuffMakeWnd.IsMakeZhuShaBi = CLivingSkillMgr.Inst:IsZhuShaBi(this.m_TemplateId)
        CHouseStuffMakeWnd.ZhuShaBiUpgradeMat = 0
        --初始化为0
        CHouseStuffMakeWnd.TemplateId = this.m_TemplateId
        if CHouseStuffMakeWnd.IsMakeZhuShaBi and CClientMainPlayer.Inst ~= nil and System.String.IsNullOrEmpty(CClientMainPlayer.Inst.ItemProp.HouseId) then
            --没有家园不打开
            this:OnNoHouse()
            return
        end
        CUIManager.ShowUI(CUIResources.HouseStuffMakeWnd)
    end
end
CLivingItemDisplay.m_Make_CS2LuaHook = function (this) 
    --先处理工艺制作
    local canProduceCount = 0
    this.consumedGrid.gameObject:SetActive(true)
    do
        local i = 0
        while i < this.consumedGrid.transform.childCount do
            local obj = this.consumedGrid.transform:GetChild(i).gameObject
            local cmp = CommonDefs.GetComponent_GameObject_Type(obj, typeof(CLivingMakeConsumeItem))
            canProduceCount = canProduceCount + cmp:GetCount()
            i = i + 1
        end
    end

    local data = LifeSkill_Item.GetData(this.m_TemplateId)
    if data ~= nil then
        CLivingSkillMgr.Inst:RequestUseLifeSkill(data.ID, canProduceCount)
    end
end
CLivingItemDisplay.m_OnEnable_CS2LuaHook = function (this) 
    this:InitData()
    this:Refresh()
    EventManager.AddListener(EnumEventType.SelectLivingItem, MakeDelegateFromCSFunction(this.SelectLivingItem, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.UpgradeLifeSkillSuccessed, MakeDelegateFromCSFunction(this.UpgradeLifeSkillSuccessed, MakeGenericClass(Action2, UInt32, UInt32), this))
    EventManager.AddListener(EnumEventType.MainPlayerPlayPropUpdate, MakeDelegateFromCSFunction(this.OnMainPlayerPlayPropUpdate, Action0, this))
    EventManager.AddListenerInternal(EnumEventType.QueryHouseLingqiResult, MakeDelegateFromCSFunction(this.OnQueryHouseLingqiResult, MakeGenericClass(Action2, String, Double), this))
end
CLivingItemDisplay.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListener(EnumEventType.SelectLivingItem, MakeDelegateFromCSFunction(this.SelectLivingItem, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.UpgradeLifeSkillSuccessed, MakeDelegateFromCSFunction(this.UpgradeLifeSkillSuccessed, MakeGenericClass(Action2, UInt32, UInt32), this))
    EventManager.RemoveListener(EnumEventType.MainPlayerPlayPropUpdate, MakeDelegateFromCSFunction(this.OnMainPlayerPlayPropUpdate, Action0, this))
    EventManager.RemoveListenerInternal(EnumEventType.QueryHouseLingqiResult, MakeDelegateFromCSFunction(this.OnQueryHouseLingqiResult, MakeGenericClass(Action2, String, Double), this))
end
CLivingItemDisplay.m_OnQueryHouseLingqiResult_CS2LuaHook = function (this, houseId, lingqi) 
    if CClientMainPlayer.Inst == nil then
        return
    end
    if houseId == CClientMainPlayer.Inst.ItemProp.HouseId then
        this.mLingQi = math.floor(lingqi)
        this.mRequested = true

        local data = LifeSkill_Item.GetData(this.m_TemplateId)
        if CLivingSkillMgr.Inst:IsZhuShaBi(data.ID) then
            local builder = NewStringBuilderWraper(StringBuilder)
            builder:AppendLine(System.String.Format(LocalString.GetString("需求等级：Lv.{0}"), data.SkillLev))
            builder:AppendLine(System.String.Format(LocalString.GetString("消耗灵气：{0}"), data.LingQi))
            builder:AppendLine(System.String.Format(LocalString.GetString("拥有灵气：{0}"), this.mLingQi))
            if data.SceneId > 0 then
                local mapData = PublicMap_PublicMap.GetData(data.SceneId)
                if mapData ~= nil then
                    builder:AppendLine(System.String.Format(LocalString.GetString("产地：{0}"), mapData.Name))
                end
            end
            this.itemDescLabel.text = ToStringWrap(builder)
        end
    end
end
CLivingItemDisplay.m_SelectLivingItem_CS2LuaHook = function (this) 

    this:InitData()
    this:UpdateDetails()
    this:UpdateUIByType()
end
CLivingItemDisplay.m_UpdateUIByType_CS2LuaHook = function (this) 
    local data = LifeSkill_SkillType.GetData(this.m_SkillClsId)
    repeat
        local default = data.Type
        if default == 1 then
            this.collectLabel.text = LocalString.GetString("钓鱼")
            break
        elseif default == 2 then
            this.collectLabel.text = LocalString.GetString("采集")
            break
        elseif default == 3 then
            this.collectLabel.text = LocalString.GetString("开始制作")
            break
        elseif default == 4 then
            this.collectLabel.text = LocalString.GetString("开始烹饪")
            break
        elseif default == 5 then
            this.collectLabel.text = LocalString.GetString("开始制作")
            break
        end
    until 1
    local itemData = LifeSkill_Item.GetData(this.m_TemplateId)
    if not CLivingSkillMgr.Inst:NeedShowMakeWnd(itemData) then
        this.titleLabel.text = LocalString.GetString("原材料")
        UIEventListener.Get(this.collectBtn).onClick = MakeDelegateFromCSFunction(this.TryMake, VoidDelegate, this)
    else
        --工艺品特殊处理
        if data.Type == LivingSkillType_lua.Craft then
            this.titleLabel.text = LocalString.GetString("原材料")
            UIEventListener.Get(this.collectBtn).onClick = MakeDelegateFromCSFunction(this.TryOpenMakeWnd, VoidDelegate, this)
        else
            this.titleLabel.text = LocalString.GetString("可伴生物品")
            UIEventListener.Get(this.collectBtn).onClick = MakeDelegateFromCSFunction(this.TyCollect, VoidDelegate, this)
        end
    end
end
CLivingItemDisplay.m_UpdateDesc_CS2LuaHook = function (this) 

    local data = LifeSkill_Item.GetData(this.m_TemplateId)

    if CLivingSkillMgr.Inst:IsZhuShaBi(data.ID) then
        this.itemDescLabel.text = ""
        if not this.mRequested then
            Gac2Gas.QueryMyHouseLingqi()
        end
        this:OnQueryHouseLingqiResult(CClientMainPlayer.Inst.ItemProp.HouseId, this.mLingQi)
    else
        local builder = NewStringBuilderWraper(StringBuilder)
        builder:AppendLine(System.String.Format(LocalString.GetString("需求等级：Lv.{0}"), data.SkillLev))
        builder:AppendLine(System.String.Format(LocalString.GetString("消耗活力：{0}"), data.HuoLi))
        if CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.PlayProp ~= nil then
            builder:AppendLine(System.String.Format(LocalString.GetString("拥有活力：{0}"), CClientMainPlayer.Inst.PlayProp.HuoLi))
        end
        this.itemDescLabel.text = ToStringWrap(builder)
    end
end
CLivingItemDisplay.m_UpdateDetails_CS2LuaHook = function (this) 
    local data = LifeSkill_Item.GetData(this.m_TemplateId)
    local skillTypeData = LifeSkill_SkillType.GetData(this.m_SkillClsId)

    local template = CItemMgr.Inst:GetItemTemplate(data.ID)
    if template ~= nil then
        this.icon:LoadMaterial(template.Icon)
        this.mTemplateId = template.ID
    else
        this.icon.material = nil
        this.mTemplateId = 0
    end
    UIEventListener.Get(this.icon.gameObject).onClick = DelegateFactory.VoidDelegate(function (p) 
        if this.mTemplateId > 0 then
            CItemInfoMgr.ShowLinkItemTemplateInfo(this.mTemplateId, false, nil, AlignType.Default, 0, 0, 0, 0)
        end
    end)
    this:UpdateDesc()
    this.itemNameLabel.text = data.ProName
    this.itemFullNameLabel.text = CUICommonDef.TranslateToNGUIText(data.ProIntro)



    CUICommonDef.ClearTransform(this.generatedGrid.transform)
    CUICommonDef.ClearTransform(this.consumedGrid.transform)
    if CLivingSkillMgr.Inst:NeedShowMakeWnd(data) then
        this.generatedGrid.gameObject:SetActive(true)
        if data.SurpriseItem ~= nil and data.SurpriseItem.Length > 0 then
            CommonDefs.EnumerableIterate(data.SurpriseItem, DelegateFactory.Action_object(function (___value) 
                local item = ___value
                local newObj = TypeAs(CommonDefs.Object_Instantiate(this.generatedItem.gameObject), typeof(GameObject))
                newObj.transform.parent = this.generatedGrid.transform
                newObj:SetActive(true)
                newObj.transform.localScale = Vector3.one

                CommonDefs.GetComponent_GameObject_Type(newObj, typeof(CLivingSurpriseItem)):Init(item.ID)
            end))
            this.generatedGrid:Reposition()
        else
            --generatedNode.gameObject.SetActive(false);
            --如果是工艺的话 要显示工艺的原材料
            if skillTypeData.Type == LivingSkillType_lua.Craft then
                if data.ConsumeItems ~= nil and data.ConsumeItems.Length > 0 then
                    CommonDefs.EnumerableIterate(data.ConsumeItems, DelegateFactory.Action_object(function (___value) 
                        local item = ___value
                        local newObj = TypeAs(CommonDefs.Object_Instantiate(this.consumeItem.gameObject), typeof(GameObject))
                        newObj.transform.parent = this.generatedGrid.transform
                        newObj:SetActive(true)
                        newObj.transform.localScale = Vector3.one

                        local cmp = CommonDefs.GetComponent_GameObject_Type(newObj, typeof(CLivingMakeConsumeItem))
                        cmp:Init(item.ID, item.count, item.count)
                    end))
                end

                if data.DrawingItem > 0 then
                    local newObj = TypeAs(CommonDefs.Object_Instantiate(this.consumeItem.gameObject), typeof(GameObject))
                    newObj.transform.parent = this.generatedGrid.transform
                    newObj:SetActive(true)
                    newObj.transform.localScale = Vector3.one
                    local cmp = CommonDefs.GetComponent_GameObject_Type(newObj, typeof(CLivingMakeConsumeItem))
                    cmp:Init(data.DrawingItem, 1, 1)
                end
                this.generatedGrid:Reposition()
            end
        end
        local scrollView = CommonDefs.GetComponent_Component_Type(this.generatedGrid.transform.parent, typeof(UIScrollView))
        if scrollView ~= nil then
            scrollView:ResetPosition()
        end
        this.huoGo:SetActive(false)
        --或
        this.consumedGrid.gameObject:SetActive(false)
    else
        --canProduceCount = 0;
        this.consumedGrid.gameObject:SetActive(true)
        if data.ConsumeItems ~= nil and data.ConsumeItems.Length > 0 then
            CommonDefs.EnumerableIterate(data.ConsumeItems, DelegateFactory.Action_object(function (___value) 
                local item = ___value
                local newObj = TypeAs(CommonDefs.Object_Instantiate(this.consumeItem.gameObject), typeof(GameObject))
                newObj.transform.parent = this.consumedGrid.transform
                newObj:SetActive(true)
                newObj.transform.localScale = Vector3.one

                local cmp = CommonDefs.GetComponent_GameObject_Type(newObj, typeof(CLivingMakeConsumeItem))
                cmp:Init(item.ID, item.count, item.count)
            end))
        end

        this.consumedGrid:Reposition()
        if data.ConsumeItem ~= nil and data.ConsumeItems.Length >= 2 then
            this.huoGo:SetActive(true)
        else
            this.huoGo:SetActive(false)
        end
        this.generatedGrid.gameObject:SetActive(false)
    end

    if this.generatedGrid.transform.childCount > 0 or this.consumedGrid.transform.childCount > 0 then
        this.titleLabel.gameObject:SetActive(true)
    else
        this.titleLabel.gameObject:SetActive(false)
    end

    this:Refresh()
end
CLivingItemDisplay.m_Refresh_CS2LuaHook = function (this) 
    local data = LifeSkill_Item.GetData(this.m_TemplateId)
    if data ~= nil then
        local skillTypeData = LifeSkill_SkillType.GetData(data.SkillId)
        local skillType = skillTypeData.Type

        if skillType == LivingSkillType_lua.Makedrug or skillType == LivingSkillType_lua.Cooking or skillType == LivingSkillType_lua.Craft then
            CUICommonDef.SetActive(this.collectBtn, true, true)
        else
            if data.SkillLev > CLivingSkillMgr.Inst:GetSkillLevel(this.m_SkillClsId) then
                CUICommonDef.SetActive(this.collectBtn, false, true)
            else
                CUICommonDef.SetActive(this.collectBtn, true, true)
            end
        end
    end
end
