local LingShou_LingShou=import "L10.Game.LingShou_LingShou"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
local CLingShouMgr = import "L10.Game.CLingShouMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local AlignType = import "L10.UI.CTooltip+AlignType"
local CUITexture=import "L10.UI.CUITexture"
local CItemCountUpdate=import "L10.UI.CItemCountUpdate"
local QnTabView = import "L10.UI.QnTabView"


CLuaLingShouWashDetailView = class()
RegistClassMember(CLuaLingShouWashDetailView,"icon")
RegistClassMember(CLuaLingShouWashDetailView,"matIcon")
RegistClassMember(CLuaLingShouWashDetailView,"washBtn")
RegistClassMember(CLuaLingShouWashDetailView,"autoWashBtn")
RegistClassMember(CLuaLingShouWashDetailView,"matNameLabel")
RegistClassMember(CLuaLingShouWashDetailView,"countUpdate")
RegistClassMember(CLuaLingShouWashDetailView,"tableNode")
RegistClassMember(CLuaLingShouWashDetailView,"getNode")
RegistClassMember(CLuaLingShouWashDetailView,"m_WndTabView")

-- Auto Generated!!
function CLuaLingShouWashDetailView:Awake( )
    self.m_WndTabView=self.transform.parent:GetComponent(typeof(QnTabView))

    self.icon = self.transform:Find("Normal/LingShou/Icon"):GetComponent(typeof(CUITexture))
    self.matIcon = self.transform:Find("Normal/BiLiuLu/Icon"):GetComponent(typeof(CUITexture))
    self.washBtn = self.transform:Find("Normal/WashBtn").gameObject
    self.autoWashBtn = self.transform:Find("Normal/AutoWashBtn").gameObject
    self.matNameLabel = self.transform:Find("Normal/BiLiuLu/NameLabel"):GetComponent(typeof(UILabel))
    self.countUpdate = self.transform:GetComponent(typeof(CItemCountUpdate))
    self.tableNode = self.transform.parent:Find("LingShouTable")
    self.getNode = self.transform:Find("Normal/BiLiuLu/GetNode1").gameObject

    UIEventListener.Get(self.washBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        CUIManager.ShowUI(CUIResources.LingShouWashResultWnd)
    end)
    UIEventListener.Get(self.autoWashBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        CUIManager.ShowUI(CUIResources.LingShouAutoWashWnd)
    end)
    UIEventListener.Get(self.getNode).onClick = DelegateFactory.VoidDelegate(function (p) 
        CItemAccessListMgr.Inst:ShowItemAccessInfo(21000592, false, p.transform, AlignType.Right)
    end)

    self.countUpdate.templateId = 21000592--Constants.BiLiuLuId
    self.countUpdate.excludeExpireTime = true
    --碧柳露名字 

    local template = CItemMgr.Inst:GetItemTemplate(21000592)
    if template ~= nil then
        self.matIcon:LoadMaterial(template.Icon)
        self.matNameLabel.text = template.Name
    else
        self.matNameLabel.text = ""
        self.matIcon.material = nil
    end

    self.countUpdate.format = "{0}/1"
    self.countUpdate.onChange = DelegateFactory.Action_int(function (val) 
        if val == 0 then
            self.countUpdate.format = "[ff0000]{0}[-]/1"
            self.getNode:SetActive(true)
        else
            self.countUpdate.format = "{0}/1"
            self.getNode:SetActive(false)
        end
    end)
    self.countUpdate:UpdateCount()
end
function CLuaLingShouWashDetailView:OnEnable( )
    self.tableNode.gameObject:SetActive(true)

    g_ScriptEvent:AddListener("SelectLingShouAtRow", self, "SelectLingShouAtRow")
    g_ScriptEvent:AddListener("GetLingShouDetails", self, "GetLingShouDetails")

    local selectedLingShou = CLingShouMgr.Inst.selectedLingShou
    if selectedLingShou and selectedLingShou~="" then
        CLingShouMgr.Inst:RequestLingShouDetails(selectedLingShou)
    else
        self.transform:GetChild(0).gameObject:SetActive(false)
    end
end

function CLuaLingShouWashDetailView:OnDisable()
    g_ScriptEvent:RemoveListener("SelectLingShouAtRow", self, "SelectLingShouAtRow")
    g_ScriptEvent:RemoveListener("GetLingShouDetails", self, "GetLingShouDetails")
end

function CLuaLingShouWashDetailView:SelectLingShouAtRow( row) 
    --请求详细信息
    --如果管理器有他的详情的话，就不用再请求了
    local selectedLingShou = CLingShouMgr.Inst.selectedLingShou
    if selectedLingShou and selectedLingShou~="" then
        CLingShouMgr.Inst:RequestLingShouDetails(selectedLingShou)
    else
        self.icon:Clear()
        self:SetDongFangOrLeaveState(false, false, false)
        self.transform:Find("Normal").gameObject:SetActive(false)
    end
end
function CLuaLingShouWashDetailView:SetDongFangOrLeaveState( active, isDongFang, isLeave) 
    self.transform:Find("Normal").gameObject:SetActive(not active)
    self.transform.parent:Find("DongFangOrLeave").gameObject:SetActive(active)

    if active then
        local tf2 = self.transform.parent:Find("DongFangOrLeave")
        if isDongFang then
            tf2:Find("Label"):GetComponent(typeof(UILabel)).text=LocalString.GetString("灵兽生宝宝中，无法进行洗炼相关的操作")
        elseif isLeave then
            tf2:Find("Label"):GetComponent(typeof(UILabel)).text=LocalString.GetString("灵兽离家出走中，无法进行洗炼相关的操作")
        end
    end
end
function CLuaLingShouWashDetailView:GetLingShouDetails( args ) 
    if self.m_WndTabView.CurrentSelectTab ~= 1 then return end 
    local lingShouId, details=args[0],args[1]

    if lingShouId == CLingShouMgr.Inst.selectedLingShou then
        if details ~= nil then
            local templateId = details.data.TemplateId
            local template = LingShou_LingShou.GetData(templateId)
            self.icon:LoadNPCPortrait(template.Portrait, false)

            local  marryInfo = details.data.Props.MarryInfo
            local isDongFnag = marryInfo.IsInDongfang>0
            local isLeave = (marryInfo.LeaveStartTime + marryInfo.LeaveDuration) > CServerTimeMgr.Inst.timeStamp--CLingShouMgr.Inst:IsLeave(details)

            if isDongFnag or isLeave then
                self:SetDongFangOrLeaveState(true, isDongFnag, isLeave)
            else
                self:SetDongFangOrLeaveState(false, false, false)
            end
        else
            self:SetDongFangOrLeaveState(false, false, false)
        end
    end
end
