-- Auto Generated!!
local Camera = import "UnityEngine.Camera"
local CameraClearFlags = import "UnityEngine.CameraClearFlags"
local CHuanLingLineDrawer = import "L10.UI.CHuanLingLineDrawer"
local RenderTexture = import "UnityEngine.RenderTexture"

CHuanLingLineDrawer.m_GetLineTexture_CS2LuaHook = function (this) 

    if this.lineTexture ~= nil and this.rt ~= nil then
        local lineCamera = CommonDefs.GetComponent_Component_Type(this, typeof(Camera))

        if lineCamera ~= nil then
            local tmpFlags = lineCamera.clearFlags
            lineCamera.clearFlags = CameraClearFlags.Color

            lineCamera.targetTexture = this.rt
            lineCamera:Render()

            local old = RenderTexture.active
            RenderTexture.active = this.rt

            this.lineTexture:ReadPixels(this.rect, 0, 0)

            this.lineTexture:Apply()

            lineCamera.clearFlags = tmpFlags
            lineCamera.targetTexture = nil

            this.m_LineScreenTexture.mainTexture = this.lineTexture
            RenderTexture.active = old
        end
    end
    return this.lineTexture
end
