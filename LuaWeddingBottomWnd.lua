local CommonDefs        = import "L10.Game.CommonDefs"
local DelegateFactory   = import "DelegateFactory"
local CGuideMgr         = import "L10.Game.Guide.CGuideMgr"
local CThumbnailChatWnd = import "L10.UI.CThumbnailChatWnd"

LuaWeddingBottomWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaWeddingBottomWnd, "grid")
RegistClassMember(LuaWeddingBottomWnd, "trumpetButton")
RegistClassMember(LuaWeddingBottomWnd, "candyButton")
RegistClassMember(LuaWeddingBottomWnd, "inviteButton")

function LuaWeddingBottomWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
    self:InitEventListener()
    self.inviteButton:SetActive(false)
end

-- 初始化UI组件
function LuaWeddingBottomWnd:InitUIComponents()
    self.grid = self.transform:Find("Anchor/Grid"):GetComponent(typeof(UIGrid))
	self.trumpetButton = self.grid.transform:Find("TrumpetButton").gameObject
	self.candyButton = self.grid.transform:Find("CandyButton").gameObject
	self.inviteButton = self.grid.transform:Find("InviteButton").gameObject
end

function LuaWeddingBottomWnd:InitEventListener()
	UIEventListener.Get(self.trumpetButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnTrumpetButtonClick()
	end)

	UIEventListener.Get(self.candyButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCandyButtonClick()
	end)

	UIEventListener.Get(self.inviteButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnInviteButtonClick()
	end)
end

function LuaWeddingBottomWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncNewWeddingSceneInfo", self, "OnSyncSceneInfo")
    g_ScriptEvent:AddListener("SyncNewWeddingNDFSceneInfo", self, "OnSyncNDFSceneInfo")
    g_ScriptEvent:AddListener("ThumbnailChatExpandChange", self, "OnThumbnailChatExpandChange")
end

function LuaWeddingBottomWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncNewWeddingSceneInfo", self, "OnSyncSceneInfo")
    g_ScriptEvent:RemoveListener("SyncNewWeddingNDFSceneInfo", self, "OnSyncNDFSceneInfo")
    g_ScriptEvent:RemoveListener("ThumbnailChatExpandChange", self, "OnThumbnailChatExpandChange")
end

function LuaWeddingBottomWnd:OnSyncSceneInfo()
    self:UpdateInviteActive()
end

function LuaWeddingBottomWnd:OnSyncNDFSceneInfo()
    self:UpdateInviteActive()
end

-- 缩略聊天界面点击了扩展按键
function LuaWeddingBottomWnd:OnThumbnailChatExpandChange()
    self:UpdateBottomPosition()
end


function LuaWeddingBottomWnd:Init()
	self:UpdateInviteActive()
	self:UpdateBottomPosition()
end

-- 是否显示邀客按钮
function LuaWeddingBottomWnd:UpdateInviteActive()
	if self.inviteButton.active then
		return
	end

    local isGroomOrBride = LuaWeddingIterationMgr:IsGroomOrBride()
	self.inviteButton:SetActive(isGroomOrBride and true or false)
	self.grid:Reposition()

	if isGroomOrBride and LuaWeddingIterationMgr.needInviteGuide then
		CGuideMgr.Inst:StartGuide(EnumGuideKey.WeddingInviteOpenGuide, 0)
        LuaWeddingIterationMgr.needInviteGuide = false
	end
end

-- 新手引导
function LuaWeddingBottomWnd:GetGuideGo(methodName)
    if methodName == "GetInvitateButton" then
        return self.inviteButton
    end
	return nil
end

-- 改变下方按键的位置
function LuaWeddingBottomWnd:UpdateBottomPosition()
    if not CThumbnailChatWnd.Instance then return end

	local offsetY = CThumbnailChatWnd.Instance.transform:Find("Anchor"):GetComponent(typeof(UIWidget)).bottomAnchor.absolute - 59
	local bgScale = CThumbnailChatWnd.Instance.background.transform.localScale
	if bgScale.y > 1 then
		self.grid.transform.localPosition = Vector3(0, 70 + offsetY, 0)
	else
		self.grid.transform.localPosition = Vector3(0, offsetY, 0)
	end
end

--@region UIEvent

function LuaWeddingBottomWnd:OnTrumpetButtonClick()
	CUIManager.ShowUI(CLuaUIResources.WeddingBuyWnd)
end

function LuaWeddingBottomWnd:OnCandyButtonClick()
	local cost = WeddingIteration_Setting.GetData().WeddingCandyCost
	local msg = g_MessageMgr:FormatMessage("WEDDING_SEND_CANDY_CONFIRM", cost)
	MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function ()
		if LuaWeddingIterationMgr.isHongBaoRaining then
			g_MessageMgr:ShowMessage("WEDDING_HONGBAORAIN_IS_GOING")
			return
		end

		Gac2Gas.NewWeddingRequestSendCandy(cost)
	end), nil, nil, nil, false)
end

function LuaWeddingBottomWnd:OnInviteButtonClick()
	CUIManager.ShowUI(CLuaUIResources.WeddingSetInvitationCardWnd)
end

--@endregion UIEvent
