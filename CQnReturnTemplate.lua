-- Auto Generated!!
local CButton = import "L10.UI.CButton"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CommonDefs = import "L10.Game.CommonDefs"
local CPropertyItem = import "L10.Game.CPropertyItem"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local CQnReturnTemplate = import "L10.UI.CQnReturnTemplate"
local CSigninMgr = import "L10.Game.CSigninMgr"
local Extensions = import "Extensions"
local Item_Item = import "L10.Game.Item_Item"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local Promotion_QnReturn = import "L10.Game.Promotion_QnReturn"
local QnChargeReturnAwardInfo = import "L10.Game.QnChargeReturnAwardInfo"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CQnReturnTemplate.m_PreInit_CS2LuaHook = function (this) 
    Extensions.RemoveAllChildren(this.conditionGrid.transform)
    this.conditionTemplate:SetActive(false)
    Extensions.RemoveAllChildren(this.awardTable.transform)
    this.awardTemplate:SetActive(false)
    this.getButton:SetActive(false)
    this.leftDaysLabel.text = ""
end
CQnReturnTemplate.m_Init_CS2LuaHook = function (this, key) 

    this.key = key
    this.titleLabel.text = System.String.Format(LocalString.GetString("返还{0}"), key)
    this:PreInit()

    if CClientMainPlayer.Inst == nil then
        return
    end
    --#region 领取条件
    local qnReturn = Promotion_QnReturn.GetData(key)
    local playerFit = true
    local condition1 = NGUITools.AddChild(this.conditionGrid.gameObject, this.conditionTemplate)
    condition1:SetActive(true)
    CommonDefs.GetComponent_GameObject_Type(condition1, typeof(CButton)).Enabled = true
    CommonDefs.GetComponentInChildren_GameObject_Type(condition1, typeof(UILabel)).text = System.String.Format(LocalString.GetString("端游充值{0}点"), qnReturn.Display)

    if qnReturn.Level > 0 then
        local condition2 = NGUITools.AddChild(this.conditionGrid.gameObject, this.conditionTemplate)
        condition2:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(condition2, typeof(CButton)).Enabled = CClientMainPlayer.Inst.Level >= qnReturn.Level
        CommonDefs.GetComponentInChildren_GameObject_Type(condition2, typeof(UILabel)).text = System.String.Format(LocalString.GetString("等级达到{0}级"), qnReturn.Level)
        if CClientMainPlayer.Inst.Level < qnReturn.Level then
            playerFit = false
        end
    end
    if qnReturn.Recharge > 0 then
        local condition3 = NGUITools.AddChild(this.conditionGrid.gameObject, this.conditionTemplate)
        condition3:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(condition3, typeof(CButton)).Enabled = CSigninMgr.Inst.qnReturnInfo.totalCharge >= qnReturn.Recharge
        CommonDefs.GetComponentInChildren_GameObject_Type(condition3, typeof(UILabel)).text = System.String.Format(LocalString.GetString("充值达到{0}灵玉"), qnReturn.Recharge)
        if CSigninMgr.Inst.qnReturnInfo.totalCharge < qnReturn.Recharge then
            playerFit = false
        end
    end
    if qnReturn.LoginDays > 0 then
        local condition2 = NGUITools.AddChild(this.conditionGrid.gameObject, this.conditionTemplate)
        condition2:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(condition2, typeof(CButton)).Enabled = CSigninMgr.Inst.qnReturnInfo.totalLoginDays >= qnReturn.LoginDays
        CommonDefs.GetComponentInChildren_GameObject_Type(condition2, typeof(UILabel)).text = System.String.Format(LocalString.GetString("累计登录{0}天"), qnReturn.LoginDays)
        if CSigninMgr.Inst.qnReturnInfo.totalLoginDays < qnReturn.LoginDays then
            playerFit = false
        end
    end
    if qnReturn.OnlineTime > 0 then
        local condition2 = NGUITools.AddChild(this.conditionGrid.gameObject, this.conditionTemplate)
        condition2:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(condition2, typeof(CButton)).Enabled = math.floor(CSigninMgr.Inst.qnReturnInfo.onlineTime / 60) >= qnReturn.OnlineTime
        CommonDefs.GetComponentInChildren_GameObject_Type(condition2, typeof(UILabel)).text = System.String.Format(LocalString.GetString("当日在线满{0}分钟"), qnReturn.OnlineTime)
        if math.floor(CSigninMgr.Inst.qnReturnInfo.onlineTime / 60) < qnReturn.OnlineTime then
            playerFit = false
        end
    end
    --DateTime time = CServerTimeMgr.ConvertTimeStampToZone8Time(CSigninMgr.Inst.qnReturnInfo.receiveTbl[(int)key - 1, 1]);
    local time = MultiDimArrayGet(CSigninMgr.Inst.qnReturnInfo.receiveTbl, key - 1, 1)
    local upperLimit = this:CalUpperLimit(key)
    local maxLingyu = math.floor(math.min(upperLimit, qnReturn.MaxLingyu))

    if MultiDimArrayGet(CSigninMgr.Inst.qnReturnInfo.receiveTbl, key - 1, 0) >= maxLingyu then
        this.getButton:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(this.getButton, typeof(CButton)).Enabled = false
        CommonDefs.GetComponentInChildren_GameObject_Type(this.getButton, typeof(UILabel)).text = LocalString.GetString("已领取")
    elseif playerFit and not CPropertyItem.IsSamePeriod(time, "d") then
        this.getButton:SetActive(true)
        CommonDefs.GetComponent_GameObject_Type(this.getButton, typeof(CButton)).Enabled = true
        CommonDefs.GetComponentInChildren_GameObject_Type(this.getButton, typeof(UILabel)).text = LocalString.GetString("领  取")
    elseif key == CQnReturnTemplate.showLeftTimesKey then
        local l = math.floor(MultiDimArrayGet(CSigninMgr.Inst.qnReturnInfo.receiveTbl, key - 1, 0) / this.delta)
        local m = math.ceil(maxLingyu / this.delta)
        this.leftDaysLabel.text = System.String.Format(LocalString.GetString("还剩{0}次"), m - l)
    end
    this.conditionGrid:Reposition()
    --#endregion

    if upperLimit > MultiDimArrayGet(CSigninMgr.Inst.qnReturnInfo.receiveTbl, key - 1, 0) then
        this:InitAwardList(upperLimit - MultiDimArrayGet(CSigninMgr.Inst.qnReturnInfo.receiveTbl, key - 1, 0), qnReturn)
    else
        this:InitAwardList(0, qnReturn)
    end

    UIEventListener.Get(this.getButton).onClick = MakeDelegateFromCSFunction(this.OnGetButtonClick, VoidDelegate, this)
end
CQnReturnTemplate.m_CalUpperLimit_CS2LuaHook = function (this, key) 
    local qnReturn = Promotion_QnReturn.GetData(key)
    if qnReturn == nil then
        return 0
    end
    --DateTime time = CServerTimeMgr.ConvertTimeStampToZone8Time(CSigninMgr.Inst.qnReturnInfo.receiveTbl[(int)key - 1, 1]);
    local upperLimit = math.floor(tonumber((CSigninMgr.Inst.qnReturnInfo.totalConsume - qnReturn.Consume) / 10 or 0))
    if key <= 3 then
        upperLimit = math.floor(tonumber(CSigninMgr.Inst.qnReturnInfo.totalConsume * qnReturn.Factor or 0))
    end
    return upperLimit
end
CQnReturnTemplate.m_InitAwardList_CS2LuaHook = function (this, upperLimit, qnReturn) 
    --uint upperLimit = Convert.ToUInt32((CSigninMgr.Inst.qnReturnInfo.totalConsume - qnReturn.Consume) / 10f);
    --if (key <= 3)
    --    upperLimit = Convert.ToUInt32(CSigninMgr.Inst.qnReturnInfo.totalConsume * qnReturn.Factor);
    local awardList = CommonDefs.StringSplit_ArrayChar(qnReturn.Award, ";")
    local awards = CreateFromClass(MakeGenericClass(List, QnChargeReturnAwardInfo))
    do
        local i = 0
        while i < awardList.Length do
            if not System.String.IsNullOrEmpty(awardList[i]) then
                local awardInfo = CreateFromClass(QnChargeReturnAwardInfo)
                local info = CommonDefs.StringSplit_ArrayChar(awardList[i], ",")
                local result
                local default
                default, result = System.UInt32.TryParse(info[0])
                if default then
                    awardInfo.templateId = result
                end
                local extern
                extern, result = System.UInt32.TryParse(info[1])
                if extern then
                    awardInfo.price = result
                end
                local ref
                ref, result = System.UInt32.TryParse(info[2])
                if ref then
                    awardInfo.amount = result
                end
                CommonDefs.ListAdd(awards, typeof(QnChargeReturnAwardInfo), awardInfo)
            end
            i = i + 1
        end
    end
    if qnReturn.MaxLingyu <= upperLimit then
        do
            local i = 0
            while i < awards.Count do
                local item = Item_Item.GetData(awards[i].templateId)
                local instance = NGUITools.AddChild(this.awardTable.gameObject, this.awardTemplate)
                instance:SetActive(true)
                CommonDefs.GetComponent_GameObject_Type(instance, typeof(CQnReturnAwardTemplate)):Init(item, awards[i].amount)
                i = i + 1
            end
        end
        this.awardTable:Reposition()
    else
        do
            local i = 0
            while i < awards.Count do
                local amount = awards[i].amount
                if awards[i].amount * awards[i].price >= upperLimit then
                    amount = math.floor(math.floor(upperLimit / awards[i].price))
                end
                if amount > 0 then
                    local item = Item_Item.GetData(awards[i].templateId)
                    local instance = NGUITools.AddChild(this.awardTable.gameObject, this.awardTemplate)
                    instance:SetActive(true)
                    CommonDefs.GetComponent_GameObject_Type(instance, typeof(CQnReturnAwardTemplate)):Init(item, amount)
                end
                if amount * awards[i].price >= upperLimit then
                    break
                else
                    upperLimit = upperLimit - (amount * awards[i].price)
                end
                i = i + 1
            end
        end
        this.awardTable:Reposition()
    end
end
