local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local MessageWndManager = import "L10.UI.MessageWndManager"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local EnumClass = import "L10.Game.EnumClass"
local DelegateFactory = import "DelegateFactory"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local CQMPKTitleTemplate = import "L10.UI.CQMPKTitleTemplate"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CButton=import "L10.UI.CButton"
local QnInput=import "L10.UI.QnInput"
local Profession=import "L10.Game.Profession"

CLuaQMPKInfoWnd = class()
RegistClassMember(CLuaQMPKInfoWnd,"m_OperateBtn")
RegistClassMember(CLuaQMPKInfoWnd,"m_PersonalInfoRoot")
RegistClassMember(CLuaQMPKInfoWnd,"m_SearchPlayerRoot")
RegistClassMember(CLuaQMPKInfoWnd,"m_CloseBtn")
RegistClassMember(CLuaQMPKInfoWnd,"m_MyServerName")
RegistClassMember(CLuaQMPKInfoWnd,"m_MyPlayerName")
RegistClassMember(CLuaQMPKInfoWnd,"m_MyClass")
RegistClassMember(CLuaQMPKInfoWnd,"m_MyProvince")
RegistClassMember(CLuaQMPKInfoWnd,"m_MyDayTime")
RegistClassMember(CLuaQMPKInfoWnd,"m_MyHourTime")
RegistClassMember(CLuaQMPKInfoWnd,"m_MyCanCommand")
RegistClassMember(CLuaQMPKInfoWnd,"m_MyReMark")
RegistClassMember(CLuaQMPKInfoWnd,"m_SaveBtn")
RegistClassMember(CLuaQMPKInfoWnd,"m_Class")
RegistClassMember(CLuaQMPKInfoWnd,"m_Province")
RegistClassMember(CLuaQMPKInfoWnd,"m_DayTime")
RegistClassMember(CLuaQMPKInfoWnd,"m_HourTime")
RegistClassMember(CLuaQMPKInfoWnd,"m_CanCommand")
RegistClassMember(CLuaQMPKInfoWnd,"m_HasTeam")
RegistClassMember(CLuaQMPKInfoWnd,"m_SearchBtn")
RegistClassMember(CLuaQMPKInfoWnd,"m_AllLocationList")
RegistClassMember(CLuaQMPKInfoWnd,"m_AllTimeList")
RegistClassMember(CLuaQMPKInfoWnd,"m_AllDayStrList")
RegistClassMember(CLuaQMPKInfoWnd,"m_AllTimeStrList")
RegistClassMember(CLuaQMPKInfoWnd,"m_AllDayPopupList")
RegistClassMember(CLuaQMPKInfoWnd,"m_AllTimePopupList")
RegistClassMember(CLuaQMPKInfoWnd,"m_MyDayPopupList")
RegistClassMember(CLuaQMPKInfoWnd,"m_MyTimePopupList")
RegistClassMember(CLuaQMPKInfoWnd,"m_MaxClass")
RegistClassMember(CLuaQMPKInfoWnd,"m_ClassPopupList")
RegistClassMember(CLuaQMPKInfoWnd,"m_SearchClassPopupList")
RegistClassMember(CLuaQMPKInfoWnd,"m_ClassStrList")
RegistClassMember(CLuaQMPKInfoWnd,"m_SearchClassStrList")
RegistClassMember(CLuaQMPKInfoWnd,"m_CurrentSelectedClassIndex")
RegistClassMember(CLuaQMPKInfoWnd,"m_MyGoodAtClass")
RegistClassMember(CLuaQMPKInfoWnd,"m_SearchGoodAtClass")
RegistClassMember(CLuaQMPKInfoWnd,"m_LocationPopupList")
RegistClassMember(CLuaQMPKInfoWnd,"m_MyLocation")
RegistClassMember(CLuaQMPKInfoWnd,"m_Location")
RegistClassMember(CLuaQMPKInfoWnd,"m_MyDay")
RegistClassMember(CLuaQMPKInfoWnd,"m_Day")
RegistClassMember(CLuaQMPKInfoWnd,"m_MyHour")
RegistClassMember(CLuaQMPKInfoWnd,"m_Hour")
RegistClassMember(CLuaQMPKInfoWnd,"m_CanCommandStrList")
RegistClassMember(CLuaQMPKInfoWnd,"m_CanSearchCommandStrList")
RegistClassMember(CLuaQMPKInfoWnd,"m_CanCommandPopupList")
RegistClassMember(CLuaQMPKInfoWnd,"m_CanSearchCommandPopupList")
RegistClassMember(CLuaQMPKInfoWnd,"m_MyCanCommandIndex")
RegistClassMember(CLuaQMPKInfoWnd,"m_CanCommandIndex")
RegistClassMember(CLuaQMPKInfoWnd,"m_HasTeamStrList")
RegistClassMember(CLuaQMPKInfoWnd,"m_HasTeamPopupList")
RegistClassMember(CLuaQMPKInfoWnd,"m_CurrentHasTeamIndex")
RegistClassMember(CLuaQMPKInfoWnd,"m_CurrentOperationIndex")

function CLuaQMPKInfoWnd:Awake()
    self.m_OperateBtn = {}
    self.m_OperateBtn[1]=FindChild(self.transform,"OperateButton1"):GetComponent(typeof(CButton))
    self.m_OperateBtn[2]=FindChild(self.transform,"OperateButton2"):GetComponent(typeof(CButton))

    self.m_PersonalInfoRoot = self.transform:Find("Anchor/PersonalInfo").gameObject
    self.m_SearchPlayerRoot = self.transform:Find("Anchor/SearchPlayer").gameObject
    self.m_CloseBtn = self.transform:Find("Wnd_Bg_Primary_Normal/CloseButton").gameObject
    self.m_MyServerName = self.transform:Find("Anchor/PersonalInfo/Label/ServerName"):GetComponent(typeof(UILabel))
    self.m_MyServerName.text=nil
    self.m_MyPlayerName = self.transform:Find("Anchor/PersonalInfo/Label (1)/PlayerName"):GetComponent(typeof(UILabel))
    self.m_MyPlayerName.text=nil
    self.m_MyClass = {}
    self.m_MyClass[1]=FindChild(self.transform,"Class1"):GetComponent(typeof(CQMPKTitleTemplate))
    self.m_MyClass[2]=FindChild(self.transform,"Class2"):GetComponent(typeof(CQMPKTitleTemplate))
    self.m_MyClass[3]=FindChild(self.transform,"Class3"):GetComponent(typeof(CQMPKTitleTemplate))


    self.m_MyProvince = self.transform:Find("Anchor/PersonalInfo/Location/TitleTemplate"):GetComponent(typeof(CQMPKTitleTemplate))
    self.m_MyDayTime = self.transform:Find("Anchor/PersonalInfo/Time/TitleTemplate"):GetComponent(typeof(CQMPKTitleTemplate))
    self.m_MyHourTime = self.transform:Find("Anchor/PersonalInfo/Time/TitleTemplate (1)"):GetComponent(typeof(CQMPKTitleTemplate))
    self.m_MyCanCommand = self.transform:Find("Anchor/PersonalInfo/Director/TitleTemplate"):GetComponent(typeof(CQMPKTitleTemplate))
    self.m_MyReMark = self.transform:Find("Anchor/PersonalInfo/Etra/QnInputField"):GetComponent(typeof(QnInput))
    self.m_SaveBtn = self.transform:Find("Anchor/PersonalInfo/SaveBtn").gameObject
    self.m_Class = {}
    self.m_Class[1]=FindChild(self.m_SearchPlayerRoot.transform,"SearchClass1"):GetComponent(typeof(CQMPKTitleTemplate))

    self.m_Province = self.transform:Find("Anchor/SearchPlayer/Location/TitleTemplate"):GetComponent(typeof(CQMPKTitleTemplate))
    self.m_DayTime = self.transform:Find("Anchor/SearchPlayer/Time/TitleTemplate"):GetComponent(typeof(CQMPKTitleTemplate))
    self.m_HourTime = self.transform:Find("Anchor/SearchPlayer/Time/TitleTemplate (1)"):GetComponent(typeof(CQMPKTitleTemplate))
    self.m_CanCommand = self.transform:Find("Anchor/SearchPlayer/Director/TitleTemplate"):GetComponent(typeof(CQMPKTitleTemplate))
    self.m_HasTeam = self.transform:Find("Anchor/SearchPlayer/Team/TitleTemplate"):GetComponent(typeof(CQMPKTitleTemplate))
    self.m_SearchBtn = self.transform:Find("Anchor/SearchPlayer/SearchBtn").gameObject
--self.m_AllLocationList = ?
--self.m_AllTimeList = ?
--self.m_AllDayStrList = ?
--self.m_AllTimeStrList = ?
--self.m_AllDayPopupList = ?
--self.m_AllTimePopupList = ?
--self.m_MyDayPopupList = ?
--self.m_MyTimePopupList = ?
--self.m_MaxClass = ?
--self.m_ClassPopupList = ?
--self.m_SearchClassPopupList = ?
--self.m_ClassStrList = ?
--self.m_SearchClassStrList = ?
--self.m_CurrentSelectedClassIndex = ?
--self.m_MyGoodAtClass = ?
--self.m_SearchGoodAtClass = ?
--self.m_LocationPopupList = ?
--self.m_MyLocation = ?
--self.m_Location = ?
--self.m_MyDay = ?
--self.m_Day = ?
--self.m_MyHour = ?
--self.m_Hour = ?
self.m_CanCommandStrList = {LocalString.GetString("否"),LocalString.GetString("是")}
self.m_CanSearchCommandStrList = {LocalString.GetString("不限"),LocalString.GetString("否"),LocalString.GetString("是")}
self.m_CanCommandPopupList = {}
self.m_CanSearchCommandPopupList = {}
--self.m_MyCanCommandIndex = ?
--self.m_CanCommandIndex = ?
--self.m_HasTeamStrList = ?
--self.m_HasTeamPopupList = ?
--self.m_CurrentHasTeamIndex = ?
--self.m_CurrentOperationIndex = ?

end

function CLuaQMPKInfoWnd:OnEnable( )
    UIEventListener.Get(self.m_CloseBtn).onClick =DelegateFactory.VoidDelegate(function(go) self:OnCloseBtnClicked(go) end)
    for i,v in ipairs(self.m_OperateBtn) do
        UIEventListener.Get(v.gameObject).onClick =DelegateFactory.VoidDelegate(function(go) self:OnOperateBtnClicked(go) end)
    end

    for i,v in ipairs(self.m_MyClass) do
        UIEventListener.Get(v.gameObject).onClick =DelegateFactory.VoidDelegate(function(go) self:OnMyClassClicked(go) end)
    end

    UIEventListener.Get(self.m_Class[1].gameObject).onClick =DelegateFactory.VoidDelegate(function(go) self:OnMyClassClicked(go) end)

    UIEventListener.Get(self.m_SaveBtn).onClick =DelegateFactory.VoidDelegate(function(go) self:OnSaveBtnClicked(go) end)
    UIEventListener.Get(self.m_SearchBtn).onClick =DelegateFactory.VoidDelegate(function(go) self:OnSearchBtnClick(go) end)
    UIEventListener.Get(self.m_MyCanCommand.gameObject).onClick =DelegateFactory.VoidDelegate(function(go) self:OnCommanderClicked(go) end)
    UIEventListener.Get(self.m_CanCommand.gameObject).onClick =DelegateFactory.VoidDelegate(function(go) self:OnCommanderClicked(go) end)
    UIEventListener.Get(self.m_HasTeam.gameObject).onClick =DelegateFactory.VoidDelegate(function(go) self:OnTeamClicked(go) end)
    UIEventListener.Get(self.m_DayTime.gameObject).onClick =DelegateFactory.VoidDelegate(function(go) self:OnDayClicked(go) end)
    UIEventListener.Get(self.m_MyDayTime.gameObject).onClick =DelegateFactory.VoidDelegate(function(go) self:OnDayClicked(go) end)
    UIEventListener.Get(self.m_MyHourTime.gameObject).onClick =DelegateFactory.VoidDelegate(function(go) self:OnTimeClicked(go) end)
    UIEventListener.Get(self.m_HourTime.gameObject).onClick =DelegateFactory.VoidDelegate(function(go) self:OnTimeClicked(go) end)
    UIEventListener.Get(self.m_Province.gameObject).onClick =DelegateFactory.VoidDelegate(function(go) self:OnLocationClicked(go) end)
    UIEventListener.Get(self.m_MyProvince.gameObject).onClick =DelegateFactory.VoidDelegate(function(go) self:OnLocationClicked(go) end)
end
function CLuaQMPKInfoWnd:Init( )
    self.m_AllLocationList ={}-- CreateFromClass(MakeGenericClass(List, CQMPKLocation))
    self.m_AllTimeList ={}-- CreateFromClass(MakeGenericClass(List, CQMPKTime))
    self.m_AllDayStrList ={}-- CreateFromClass(MakeGenericClass(List, String))
    self.m_AllTimeStrList ={}-- CreateFromClass(MakeGenericClass(List, String))
    self.m_AllDayPopupList ={}-- CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
    self.m_AllTimePopupList ={}-- CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
    self.m_MyDayPopupList ={}-- CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
    self.m_MyTimePopupList ={}-- CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
    self.m_ClassPopupList ={}-- CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
    self.m_SearchClassPopupList ={}-- CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
    self.m_ClassStrList ={}-- CreateFromClass(MakeGenericClass(List, String))
    self.m_SearchClassStrList ={}-- CreateFromClass(MakeGenericClass(List, String))
    self.m_LocationPopupList ={}-- CreateFromClass(MakeGenericClass(List, PopupMenuItemData))

    self.m_MaxClass = EnumClass.DieKe
    self.m_MyGoodAtClass = CreateFromClass(MakeArrayClass(System.UInt32), 3)
    self.m_CurrentSelectedClassIndex = - 1
    self.m_SearchGoodAtClass = 0
    self.m_MyLocation = 0
    self.m_Location = 0
    self.m_MyCanCommandIndex = 0
    self.m_CanCommandIndex = - 1
    self.m_CurrentHasTeamIndex = - 1
    self.m_CurrentOperationIndex = - 1

    self.m_HasTeamStrList ={LocalString.GetString("无"), LocalString.GetString("有")}-- InitializeList(CreateFromClass(MakeGenericClass(List, String)), String, LocalString.GetString("无"), LocalString.GetString("有"))
    self.m_HasTeamPopupList ={}-- CreateFromClass(MakeGenericClass(List, PopupMenuItemData))

    do
        local keys ={}
        QuanMinPK_Location.ForeachKey(function (key) 
            table.insert( keys,key )
        end)
        for i,v in ipairs(keys) do
            local data = QuanMinPK_Location.GetData(v)
            table.insert( self.m_AllLocationList,{m_Id=v,m_Name=data.First} )
            local item = PopupMenuItemData(data.First, DelegateFactory.Action_int(function(p0) self:OnLocationSelected(p0) end),false, nil, EnumPopupMenuItemStyle.Light)
            table.insert( self.m_LocationPopupList, item )
        end
    end


    table.insert( self.m_ClassStrList, LocalString.GetString("无"))
    table.insert( self.m_ClassPopupList, PopupMenuItemData(self.m_ClassStrList[1], DelegateFactory.Action_int(function(p0) self:OnMyClassSelected(p0) end),false, nil, EnumPopupMenuItemStyle.Light))

    local allclazz={}
    table.insert( allclazz,EnumClass.SheShou )
    table.insert( allclazz,EnumClass.JiaShi )
    table.insert( allclazz,EnumClass.DaoKe )
    table.insert( allclazz,EnumClass.XiaKe )
    table.insert( allclazz,EnumClass.FangShi )
    table.insert( allclazz,EnumClass.YiShi )
    table.insert( allclazz,EnumClass.MeiZhe )
    table.insert( allclazz,EnumClass.YiRen )
    table.insert( allclazz,EnumClass.YanShi )
    table.insert( allclazz,EnumClass.HuaHun )
    table.insert( allclazz,EnumClass.YingLing )
    table.insert( allclazz,EnumClass.DieKe )

    for i,clazz in ipairs(allclazz) do
        local name = Profession.GetFullName(clazz)
        local data = PopupMenuItemData(name, DelegateFactory.Action_int(function(p0) self:OnMyClassSelected(p0) end),false, nil, EnumPopupMenuItemStyle.Light)
        
        table.insert( self.m_ClassStrList,name )
        table.insert( self.m_ClassPopupList,data )
        table.insert( self.m_SearchClassStrList,name)
        table.insert( self.m_SearchClassPopupList,data )
    end

    do
        local keys ={}-- CreateFromClass(MakeGenericClass(List, UInt32))
        QuanMinPK_OnlineTime.ForeachKey(function (key) 
            table.insert( keys,key )
        end)

        local daystrLookup={}
        local timestrLookup={}

        for i,v in ipairs(keys) do
            local data = QuanMinPK_OnlineTime.GetData(v)
            local time={m_Id=v,m_Day=data.First,m_Time=data.Second}

            table.insert( self.m_AllTimeList,time )

            if not daystrLookup[time.m_Day] then
                daystrLookup[time.m_Day]=true
                table.insert( self.m_AllDayStrList,time.m_Day )

                table.insert( self.m_AllDayPopupList, PopupMenuItemData(time.m_Day, DelegateFactory.Action_int(function(p0) self:OnDaySelected(p0) end),false, nil, EnumPopupMenuItemStyle.Light))

                if data.InfoCanSet > 0 then
                    table.insert( self.m_MyDayPopupList, PopupMenuItemData(time.m_Day, DelegateFactory.Action_int(function(p0) self:OnDaySelected(p0) end),false, nil, EnumPopupMenuItemStyle.Light))
                end
            end
            if not timestrLookup[time.m_Time] then
                timestrLookup[time.m_Time]=true
                table.insert( self.m_AllTimeStrList,time.m_Time )

                table.insert( self.m_AllTimePopupList, PopupMenuItemData(time.m_Time, DelegateFactory.Action_int(function(p0) self:OnTimeSelected(p0) end),false, nil, EnumPopupMenuItemStyle.Light))

                if data.InfoCanSet > 0 then
                    table.insert( self.m_MyTimePopupList, PopupMenuItemData(time.m_Time, DelegateFactory.Action_int(function(p0) self:OnTimeSelected(p0) end),false, nil, EnumPopupMenuItemStyle.Light))
                end
            end
        end
    end

    self.m_OperateBtn[1].Selected = true
    self:OnOperateBtnClicked(self.m_OperateBtn[1].gameObject)
    local info = CQuanMinPKMgr.Inst.m_MyPersonalInfo
    if info ~= nil then
        self.m_MyServerName.text = info.m_FromServerName
        self.m_MyPlayerName.text = info.m_FromCharacterName
        self.m_MyReMark.Text = info.m_Remark
        if info.m_LocationId < 0 then
            info.m_LocationId = 0
        end
        self:OnLocationSelected(info.m_LocationId - 1)
        if info.m_CanCommand < 0 then
            info.m_CanCommand = 0
        end
        self:OnCommanderSelected(info.m_CanCommand)
        if System.String.IsNullOrEmpty(info.m_Remark) then
            self.m_MyReMark.Text = info.m_Remark
        end
        if info.m_OnlineTimeId < 1 then
            info.m_OnlineTimeId = 1
        end
        local data = QuanMinPK_OnlineTime.GetData(info.m_OnlineTimeId)
        if data ~= nil then
            self:OnDaySelected(self:GetDayIndex(data.First))
            self:OnTimeSelected(self:GetHourIndex(data.Second))
        else
            self:OnDaySelected(0)
            self:OnTimeSelected(0)
        end
        if info.m_GoodAtClass < 0 then
            info.m_GoodAtClass = 0
        end
        self.m_CurrentSelectedClassIndex = 0
        for i = 0, 14 do
            if (bit.band(info.m_GoodAtClass, (bit.lshift(1, i)))) > 0 then
                self:OnMyClassSelected(i + 1)
                self.m_CurrentSelectedClassIndex = self.m_CurrentSelectedClassIndex + 1
            end
        end

        do
            local cnt = #self.m_MyClass
            while self.m_CurrentSelectedClassIndex < cnt do
                self:OnMyClassSelected(0)
                self.m_CurrentSelectedClassIndex = self.m_CurrentSelectedClassIndex + 1
            end
        end

    end
    self:InitSearchItem()

    for i = 0, 1 do
        table.insert( self.m_CanCommandPopupList, PopupMenuItemData(self.m_CanCommandStrList[i+1], DelegateFactory.Action_int(function(p0,p1) self:OnCommanderSelected(p0,p1) end),false, nil, EnumPopupMenuItemStyle.Light))
        table.insert( self.m_HasTeamPopupList, PopupMenuItemData(self.m_HasTeamStrList[i+1], DelegateFactory.Action_int(function(p0,p1) self:OnTeamSelected(p0,p1) end),false, nil, EnumPopupMenuItemStyle.Light))
    end
    for i = 0, 2 do
        table.insert( self.m_CanSearchCommandPopupList, PopupMenuItemData(self.m_CanSearchCommandStrList[i+1], DelegateFactory.Action_int(function(p0,p1) self:OnCommanderSelected(p0,p1) end),false, nil, EnumPopupMenuItemStyle.Light))
    end
end
function CLuaQMPKInfoWnd:InitSearchItem( )
    self.m_SearchGoodAtClass =math.random(1,EnumToInt(self.m_MaxClass))
    self.m_Class[1]:Init(self.m_SearchClassStrList[self.m_SearchGoodAtClass])
    self.m_Province:Init(self.m_AllLocationList[1].m_Name)
    self.m_Location = 1
    self.m_DayTime:Init(LocalString.GetString("不限"))
    self.m_Day = LocalString.GetString("不限")
    self.m_HourTime:Init(LocalString.GetString("不限"))
    self.m_Hour = LocalString.GetString("不限")
    self.m_CanCommand:Init(self.m_CanSearchCommandStrList[0+1])
    self.m_CanCommandIndex = - 1
    self.m_HasTeam:Init(self.m_HasTeamStrList[0+1])
    self.m_CurrentHasTeamIndex = 0
end
function CLuaQMPKInfoWnd:GetDayIndex( day) 
    for i,v in ipairs(self.m_AllDayStrList) do
        if v==day then
            return i-1
        end
    end
    return 0
end
function CLuaQMPKInfoWnd:GetHourIndex( hour) 
    for i,v in ipairs(self.m_AllTimeStrList) do
        if v==hour then
            return i-1
        end
    end
    return 0
end
function CLuaQMPKInfoWnd:OnMyClassClicked( go) 
    if self.m_CurrentOperationIndex == 0 then
        for i,v in ipairs(self.m_MyClass) do
            if v.gameObject==go then
                self.m_CurrentSelectedClassIndex = i-1
                v:Expand(true)
                CPopupMenuInfoMgr.ShowPopupMenu(
                    Table2Array(self.m_ClassPopupList,MakeArrayClass(PopupMenuItemData)),
                    go.transform, 
                    CPopupMenuInfoMgr.AlignType.Bottom, 
                    #self.m_ClassPopupList >= 8 and 2 or 1, 
                    DelegateFactory.Action(function () self.m_MyClass[i]:Expand(false) end), 
                    600, 
                    true, 
                    296)
                break
            end
        end

    elseif self.m_CurrentOperationIndex == 1 then
        self.m_Class[1]:Expand(true)
        CPopupMenuInfoMgr.ShowPopupMenu(
            Table2Array(self.m_SearchClassPopupList,MakeArrayClass(PopupMenuItemData)),
            go.transform, 
            CPopupMenuInfoMgr.AlignType.Bottom, 
            #self.m_SearchClassPopupList >= 8 and 2 or 1, 
            DelegateFactory.Action(function () self.m_Class[1]:Expand(false) end), 
            600, 
            true, 
            296)
    end
end
function CLuaQMPKInfoWnd:OnMyClassSelected( index) 
    if self.m_CurrentOperationIndex == 0 then
        self.m_MyClass[self.m_CurrentSelectedClassIndex+1]:Init(self.m_ClassStrList[index+1])
        self.m_MyGoodAtClass[self.m_CurrentSelectedClassIndex] = index
    elseif self.m_CurrentOperationIndex == 1 then
        self.m_Class[1]:Init(self.m_SearchClassStrList[index+1])
        self.m_SearchGoodAtClass = index+1
        --m_GoodAtClass[m_CurrentSelectedClassIndex] = (uint) index;
    end
end
function CLuaQMPKInfoWnd:OnLocationClicked( go) 
    local current =nil
    if self.m_CurrentOperationIndex == 0 then
        current = self.m_MyProvince
    elseif self.m_CurrentOperationIndex == 1 then
        current = self.m_Province
    end
    current:Expand(true)

    local array=Table2Array(self.m_LocationPopupList,MakeArrayClass(PopupMenuItemData))
    CPopupMenuInfoMgr.ShowPopupMenu(
        array, 
        go.transform, 
        CPopupMenuInfoMgr.AlignType.Bottom, 
        #self.m_LocationPopupList >= 8 and 2 or 1, 
        DelegateFactory.Action(function () current:Expand(false)end), 
        600, 
        true, 
        296)

end
function CLuaQMPKInfoWnd:OnLocationSelected( index) 
    if self.m_CurrentOperationIndex == 0 then
        self.m_MyProvince:Init(self.m_AllLocationList[index+1].m_Name)
        self.m_MyLocation = self.m_AllLocationList[index+1].m_Id
    elseif self.m_CurrentOperationIndex == 1 then
        self.m_Province:Init(self.m_AllLocationList[index+1].m_Name)
        self.m_Location = self.m_AllLocationList[index+1].m_Id
    end
end
function CLuaQMPKInfoWnd:OnDayClicked( go) 
    if self.m_CurrentOperationIndex == 0 then
        self.m_MyDayTime:Expand(true)
        CPopupMenuInfoMgr.ShowPopupMenu(
            -- CommonDefs.ListToArray(self.m_MyDayPopupList), 
            Table2Array(self.m_MyDayPopupList,MakeArrayClass(PopupMenuItemData)),
            go.transform, 
            CPopupMenuInfoMgr.AlignType.Bottom, 
            #self.m_MyDayPopupList >= 8 and 2 or 1, 
            DelegateFactory.Action(function () self.m_MyDayTime:Expand(false) end), 
            600, 
            true, 
            296)
    elseif self.m_CurrentOperationIndex == 1 then
        self.m_DayTime:Expand(true)

        local array=Table2Array(self.m_AllDayPopupList,MakeArrayClass(PopupMenuItemData))

        CPopupMenuInfoMgr.ShowPopupMenu(
            array, 
            go.transform, 
            CPopupMenuInfoMgr.AlignType.Bottom, 
            #self.m_AllDayPopupList >= 8 and 2 or 1, 
            DelegateFactory.Action(function () self.m_DayTime:Expand(false) end), 
            600, 
            true, 
            296)
    end
end
function CLuaQMPKInfoWnd:OnDaySelected( index) 
    if self.m_CurrentOperationIndex == 0 then
        self.m_MyDayTime:Init(self.m_AllDayStrList[index+1])
        self.m_MyDay = self.m_AllDayStrList[index+1]
    elseif self.m_CurrentOperationIndex == 1 then
        self.m_DayTime:Init(self.m_AllDayStrList[index+1])
        self.m_Day = self.m_AllDayStrList[index+1]
    end
end
function CLuaQMPKInfoWnd:OnTimeClicked( go) 
    if self.m_CurrentOperationIndex == 0 then
        self.m_MyHourTime:Expand(true)

        CPopupMenuInfoMgr.ShowPopupMenu(
            -- CommonDefs.ListToArray(self.m_MyTimePopupList), 
            Table2Array(self.m_MyTimePopupList,MakeArrayClass(PopupMenuItemData)), 
            go.transform, 
            CPopupMenuInfoMgr.AlignType.Bottom, 
            #self.m_MyTimePopupList >= 8 and 2 or 1, 
            DelegateFactory.Action(function () self.m_MyHourTime:Expand(false) end), 
            600, 
            true, 
            296)
    elseif self.m_CurrentOperationIndex == 1 then
        self.m_HourTime:Expand(true)

        CPopupMenuInfoMgr.ShowPopupMenu(
            Table2Array(self.m_AllTimePopupList,MakeArrayClass(PopupMenuItemData)), 
            go.transform, 
            CPopupMenuInfoMgr.AlignType.Bottom, 
            #self.m_AllTimePopupList >= 8 and 2 or 1, 
            DelegateFactory.Action(function () self.m_HourTime:Expand(false) end), 
            600, 
            true, 
            296)
    end
end
function CLuaQMPKInfoWnd:OnTimeSelected( index) 
    if self.m_CurrentOperationIndex == 0 then
        self.m_MyHourTime:Init(self.m_AllTimeStrList[index+1])
        self.m_MyHour = self.m_AllTimeStrList[index+1]
    elseif self.m_CurrentOperationIndex == 1 then
        self.m_HourTime:Init(self.m_AllTimeStrList[index+1])
        self.m_Hour = self.m_AllTimeStrList[index+1]
    end
end
function CLuaQMPKInfoWnd:OnCommanderClicked( go) 
    if self.m_CurrentOperationIndex == 0 then
        self.m_MyCanCommand:Expand(true)
        CPopupMenuInfoMgr.ShowPopupMenu(
            -- CommonDefs.ListToArray(self.m_CanCommandPopupList), 
            Table2Array(self.m_CanCommandPopupList,MakeArrayClass(PopupMenuItemData)),
            go.transform, 
            CPopupMenuInfoMgr.AlignType.Bottom, 
            1, 
            DelegateFactory.Action(function () self.m_MyCanCommand:Expand(false) end), 
            600, 
            true, 
            296)
    elseif self.m_CurrentOperationIndex == 1 then
        self.m_CanCommand:Expand(true)
        CPopupMenuInfoMgr.ShowPopupMenu(
            -- CommonDefs.ListToArray(self.m_CanSearchCommandPopupList), 
            Table2Array(self.m_CanSearchCommandPopupList,MakeArrayClass(PopupMenuItemData)),
            go.transform, 
            CPopupMenuInfoMgr.AlignType.Bottom, 
            1, 
            DelegateFactory.Action(function () self.m_CanCommand:Expand(false) end), 
            600, 
            true, 
            296)
    end
end
function CLuaQMPKInfoWnd:OnCommanderSelected( index) 
    if self.m_CurrentOperationIndex == 0 then
        self.m_MyCanCommand:Init(self.m_CanCommandStrList[index+1])
        self.m_MyCanCommandIndex = index
    elseif self.m_CurrentOperationIndex == 1 then
        self.m_CanCommand:Init(self.m_CanSearchCommandStrList[index+1])
        self.m_CanCommandIndex = index - 1
    end
end
function CLuaQMPKInfoWnd:OnTeamClicked( go) 
    self.m_HasTeam:Expand(true)
    CPopupMenuInfoMgr.ShowPopupMenu(
        -- CommonDefs.ListToArray(self.m_HasTeamPopupList), 
        Table2Array(self.m_HasTeamPopupList,MakeArrayClass(PopupMenuItemData)),
        go.transform, 
        CPopupMenuInfoMgr.AlignType.Bottom, 
        1, 
        DelegateFactory.Action(function () self.m_HasTeam:Expand(false) end), 
        600, 
        true, 
        296)
end
function CLuaQMPKInfoWnd:OnTeamSelected( index) 
    self.m_HasTeam:Init(self.m_HasTeamStrList[index+1])
    self.m_CurrentHasTeamIndex = index
end
function CLuaQMPKInfoWnd:OnOperateBtnClicked( go) 
    for i,v in ipairs(self.m_OperateBtn) do
        if go==v.gameObject then
            self.m_CurrentOperationIndex = i-1
            if i == 1 then
                self.m_PersonalInfoRoot:SetActive(true)
                self.m_SearchPlayerRoot:SetActive(false)
            elseif i == 2 then
                self.m_PersonalInfoRoot:SetActive(false)
                self.m_SearchPlayerRoot:SetActive(true)
            end
            v.Selected = true
        else
            v.Selected = false
        end
    end

end
function CLuaQMPKInfoWnd:GetGoodAtClass( clazz) 
    local result = 0
    do
        local i = 0 local len = clazz.Length
        while i < len do
            local continue
            repeat
                if clazz[i] <= 0 then
                    continue = true
                    break
                end
                result = bit.bor(result, (bit.lshift(1, (clazz[i] - 1))))
                continue = true
            until 1
            if not continue then
                break
            end
            i = i + 1
        end
    end


    return result
end
function CLuaQMPKInfoWnd:GetTimeId( )
    local timeId = 0
    for i,v in ipairs(self.m_AllTimeList) do
        if v.m_Day==self.m_MyDay and v.m_Time==self.m_MyHour then
            timeId=v.m_Id
            break
        end
    end

    return timeId
end
function CLuaQMPKInfoWnd:OnSaveBtnClicked( go) 
    if CUICommonDef.GetStrByteLength(self.m_MyReMark.Text) > 60 then
        g_MessageMgr:ShowMessage("QMPK_Remark_Too_Long")
        return
    end
    if not CWordFilterMgr.Inst:CheckName(self.m_MyReMark.Text) then
        g_MessageMgr:ShowMessage("QMPK_Name_Slogan_Violation")
        return
    end
    local goodatclass=self:GetGoodAtClass(self.m_MyGoodAtClass)
    -- print(goodatclass,self.m_MyLocation, self:GetTimeId(), self.m_MyCanCommandIndex, self.m_MyReMark.Text)
    Gac2Gas.RequestUpdateQmpkPersonalInfo(goodatclass, self.m_MyLocation, self:GetTimeId(), self.m_MyCanCommandIndex, self.m_MyReMark.Text)
end
function CLuaQMPKInfoWnd:OnCloseBtnClicked( go) 
    if nil == CQuanMinPKMgr.Inst.m_MyPersonalInfo then
        CUIManager.CloseUI(CLuaUIResources.QMPKInfoWnd)
        return
    end
    
    if self:GetGoodAtClass(self.m_MyGoodAtClass) ~= CQuanMinPKMgr.Inst.m_MyPersonalInfo.m_GoodAtClass 
    or self.m_MyLocation ~= CQuanMinPKMgr.Inst.m_MyPersonalInfo.m_LocationId 
    or self:GetTimeId() ~= CQuanMinPKMgr.Inst.m_MyPersonalInfo.m_OnlineTimeId 
    or self.m_MyCanCommandIndex ~= CQuanMinPKMgr.Inst.m_MyPersonalInfo.m_CanCommand 
    or self.m_MyReMark.Text ~= CQuanMinPKMgr.Inst.m_MyPersonalInfo.m_Remark then
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("QMPK_NOT_Save_Personal_Setting"), 
        DelegateFactory.Action(function () 
            self:OnSaveBtnClicked(nil)
            CUIManager.CloseUI(CLuaUIResources.QMPKInfoWnd)
        end), 
        DelegateFactory.Action(function () 
            CUIManager.CloseUI(CLuaUIResources.QMPKInfoWnd)
        end), nil, nil, false)
        return
    end
    -- self:Close()
    CUIManager.CloseUI(CLuaUIResources.QMPKInfoWnd)
end
function CLuaQMPKInfoWnd:OnSearchBtnClick( go) 
    local timeId = 0
    for i,v in ipairs(self.m_AllTimeList) do
        if v.m_Day==self.m_Day and v.m_Time==self.m_Hour then
            timeId=v.m_Id
            break
        end
    end
    CQuanMinPKMgr.Inst.m_SearchClass = self.m_SearchGoodAtClass
    CQuanMinPKMgr.Inst.m_SearchLocation = self.m_Location
    CQuanMinPKMgr.Inst.m_SearchTime = timeId
    CQuanMinPKMgr.Inst.m_SearchCommandIndex = self.m_CanCommandIndex
    CQuanMinPKMgr.Inst.m_SearchTeamIndex = self.m_CurrentHasTeamIndex
    CUIManager.ShowUI(CLuaUIResources.QMPKSearchPlayerResultWnd)
end

return CLuaQMPKInfoWnd
