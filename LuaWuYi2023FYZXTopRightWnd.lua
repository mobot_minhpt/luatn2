local UILabel = import "UILabel"
local UISlider = import "UISlider"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CTopAndRightTipWnd = import "L10.UI.CTopAndRightTipWnd"
local CScene = import "L10.Game.CScene"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CMainCamera = import "L10.Engine.CMainCamera"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local UIProgressBar = import "UIProgressBar"
local Animation = import "UnityEngine.Animation"

LuaWuYi2023FYZXTopRightWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaWuYi2023FYZXTopRightWnd, "RightBtn", "RightBtn", GameObject)
RegistChildComponent(LuaWuYi2023FYZXTopRightWnd, "CoinNumLabel1", "CoinNumLabel1", UILabel)
RegistChildComponent(LuaWuYi2023FYZXTopRightWnd, "RightInfoLabel1", "RightInfoLabel1", UILabel)
RegistChildComponent(LuaWuYi2023FYZXTopRightWnd, "Slider1", "Slider1", UISlider)
RegistChildComponent(LuaWuYi2023FYZXTopRightWnd, "CoinNumLabel2", "CoinNumLabel2", UILabel)
RegistChildComponent(LuaWuYi2023FYZXTopRightWnd, "RightInfoLabel2", "RightInfoLabel2", UILabel)
RegistChildComponent(LuaWuYi2023FYZXTopRightWnd, "Slider2", "Slider2", UISlider)
RegistChildComponent(LuaWuYi2023FYZXTopRightWnd, "LeftTimeLabel", "LeftTimeLabel", UILabel)
RegistChildComponent(LuaWuYi2023FYZXTopRightWnd, "GoldTipView", "GoldTipView", GameObject)
RegistChildComponent(LuaWuYi2023FYZXTopRightWnd, "GoldTipViewLabel", "GoldTipViewLabel", UILabel)
RegistChildComponent(LuaWuYi2023FYZXTopRightWnd, "KillTipView", "KillTipView", GameObject)
RegistChildComponent(LuaWuYi2023FYZXTopRightWnd, "HorseProgressView", "HorseProgressView", GameObject)
RegistChildComponent(LuaWuYi2023FYZXTopRightWnd, "HorseProgressTemplate", "HorseProgressTemplate", GameObject)
RegistChildComponent(LuaWuYi2023FYZXTopRightWnd, "HorseProgressRoot", "HorseProgressRoot", GameObject)
RegistChildComponent(LuaWuYi2023FYZXTopRightWnd, "My1", "My1", GameObject)
RegistChildComponent(LuaWuYi2023FYZXTopRightWnd, "My2", "My2", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaWuYi2023FYZXTopRightWnd,"m_ShowKillTipViewTick")
RegistClassMember(LuaWuYi2023FYZXTopRightWnd,"m_ShowGoldTipViewTick")
RegistClassMember(LuaWuYi2023FYZXTopRightWnd,"m_EngineId2ProgressInfo")

function LuaWuYi2023FYZXTopRightWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.RightBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRightBtnClick()
	end)


    --@endregion EventBind end
end

function LuaWuYi2023FYZXTopRightWnd:Init()
	self.GoldTipView.gameObject:SetActive(false)
	self.GoldTipViewLabel.text = g_MessageMgr:FormatMessage("WuYi2023FYZX_GoldTipViewLabel")
	self.KillTipView.gameObject:SetActive(true)
	self.HorseProgressView.gameObject:SetActive(true)
	self.HorseProgressTemplate.gameObject:SetActive(false)
	self.My1.gameObject:SetActive(false)
	self.My2.gameObject:SetActive(false)
	self.CoinNumLabel1.text = ""
	self.CoinNumLabel2.text = ""
	self.RightInfoLabel1.text = ""
	self.Slider1.value = 0
	self.RightInfoLabel2.text = ""
	self.Slider2.value = 0
	local desc = g_MessageMgr:FormatMessage("WuYi2023FYZX_TaskView", "%s")
    LuaCommonCountDownViewMgr:SetInfo(desc, true, true, nil, LocalString.GetString("规则"), nil, function() 
		LuaWuYi2023Mgr:ShowCommonImageRuleWnd()
	end, nil)
end

function LuaWuYi2023FYZXTopRightWnd:InitHorseProgressTemplate(engineId, destroyTime, isBlue)
	local timeStamp = CServerTimeMgr.Inst.timeStamp
	local deltaTime = destroyTime - timeStamp
	if deltaTime <= 0 then
		return
	end
	local obj = NGUITools.AddChild(self.HorseProgressRoot.gameObject, self.HorseProgressTemplate.gameObject)
	obj:SetActive(true)
	if self.m_EngineId2ProgressInfo == nil then
		self.m_EngineId2ProgressInfo = {}
	end
	self.m_EngineId2ProgressInfo[engineId] = {obj = obj, destroyTime = destroyTime, deltaTime = deltaTime}

	local blueProgress = obj.transform:Find("BlueProgress"):GetComponent(typeof(UIProgressBar))
	local redProgress = obj.transform:Find("RedProgress"):GetComponent(typeof(UIProgressBar))
	local numLabel = obj.transform:Find("NumLabel"):GetComponent(typeof(UILabel))

	numLabel.text = math.floor(deltaTime)
	redProgress.value = 1
	blueProgress.value = 1
	redProgress.gameObject:SetActive(not isBlue)
	blueProgress.gameObject:SetActive(isBlue)
end

function LuaWuYi2023FYZXTopRightWnd:Update()
	if self.m_EngineId2ProgressInfo then
		local timeStamp = CServerTimeMgr.Inst.timeStamp
		local destroyEngineIdList = {}
		for engineId, info in pairs(self.m_EngineId2ProgressInfo) do
			local obj = info.obj
			local destroyTime = info.destroyTime
			if destroyTime > timeStamp then
				local clientObj = CClientObjectMgr.Inst:GetObject(engineId)
				if clientObj and clientObj.RO then
					local anchor = clientObj.RO.transform.position
					anchor = Vector3(anchor.x, anchor.y + 3, anchor.z)
					local viewPos = CMainCamera.Main:WorldToViewportPoint(anchor)
					local screenPos = Vector3.zero
					if viewPos and viewPos.z and viewPos.z > 0 and viewPos.x > 0 and viewPos.x < 1 and viewPos.y > 0 and viewPos.y < 1 then
						screenPos = CMainCamera.Main:WorldToScreenPoint(anchor)
					else  
						screenPos = CommonDefs.op_Multiply_Vector3_Single(Vector3.up, 3000)
					end
					screenPos.z = 0
					local worldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)
					obj.transform.position = worldPos
				end
				local blueProgress = obj.transform:Find("BlueProgress"):GetComponent(typeof(UIProgressBar))
				local redProgress = obj.transform:Find("RedProgress"):GetComponent(typeof(UIProgressBar))
				local numLabel = obj.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
				numLabel.text =  math.floor(destroyTime - timeStamp)
				redProgress.value =  (destroyTime - timeStamp) / info.deltaTime
				blueProgress.value = (destroyTime - timeStamp) / info.deltaTime
			else
				table.insert(destroyEngineIdList, engineId)
			end
		end
		for _,engineId in pairs(destroyEngineIdList) do
			local info = self.m_EngineId2ProgressInfo[engineId]
			local obj = info.obj
			GameObject.Destroy(obj)
			self.m_EngineId2ProgressInfo[engineId] = nil
		end
		local count = 0
		for engineId, info in pairs(self.m_EngineId2ProgressInfo) do
			if info then
				count = count + 1
			end
		end
		if count == 0 then
			self.m_EngineId2ProgressInfo = nil
		end
	end
end

function LuaWuYi2023FYZXTopRightWnd:CancelShowGoldTipViewTick()
	if self.m_ShowGoldTipViewTick then
		UnRegisterTick(self.m_ShowGoldTipViewTick)
		self.m_ShowGoldTipViewTick = nil
	end
end

function LuaWuYi2023FYZXTopRightWnd:CancelShowKillTipViewTick()
	if self.m_ShowKillTipViewTick then
		UnRegisterTick(self.m_ShowKillTipViewTick)
		self.m_ShowKillTipViewTick = nil
	end
end

function LuaWuYi2023FYZXTopRightWnd:InitKillTipView(attackerForce, isJiBai, icon1, icon2)
	local portrait1 = self.KillTipView.transform:Find("KillTipViewdonghua/Portrait1"):GetComponent(typeof(CUITexture))
	local portrait2 = self.KillTipView.transform:Find("KillTipViewdonghua/Portrait2"):GetComponent(typeof(CUITexture))
	portrait1:LoadNPCPortrait(icon1,false)
	portrait2:LoadNPCPortrait(icon2,false)
	portrait1.gameObject:SetActive(true)
	portrait2.gameObject:SetActive(true)
	local ani = self.KillTipView:GetComponent(typeof(Animation))
	local aniName = "wuyi2023fyzxtoprightwnd_jituilan"
	if isJiBai then
		if attackerForce == 0 then
			aniName = "wuyi2023fyzxtoprightwnd_jituilan4"
		else
			aniName = "wuyi2023fyzxtoprightwnd_jituilan3"
		end
	else
		if attackerForce == 0 then
			aniName = "wuyi2023fyzxtoprightwnd_jituilan"
		else
			aniName = "wuyi2023fyzxtoprightwnd_jituilan2"
		end
	end
	ani:Stop()
	ani:Play(aniName)
end

function LuaWuYi2023FYZXTopRightWnd:OnEnable()
	g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
	g_ScriptEvent:AddListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
	g_ScriptEvent:AddListener("OnFenYongZhenXianHorseKilled", self, "OnFenYongZhenXianHorseKilled")
	g_ScriptEvent:AddListener("OnFenYongZhenXianHorseCountDown", self, "OnFenYongZhenXianHorseCountDown")
	g_ScriptEvent:AddListener("OnFenYongZhenXianSyncGameInfo", self, "OnFenYongZhenXianSyncGameInfo")
	g_ScriptEvent:AddListener("OnFenYongZhenXianPlayerKilled", self, "OnFenYongZhenXianPlayerKilled")
	g_ScriptEvent:AddListener("OnFenYongZhenXianReachScoreLimit", self, "OnFenYongZhenXianReachScoreLimit")
end

function LuaWuYi2023FYZXTopRightWnd:OnDisable()
	g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
	g_ScriptEvent:RemoveListener("SceneRemainTimeUpdate", self, "OnSceneRemainTimeUpdate")
	g_ScriptEvent:RemoveListener("OnFenYongZhenXianHorseKilled", self, "OnFenYongZhenXianHorseKilled")
	g_ScriptEvent:RemoveListener("OnFenYongZhenXianHorseCountDown", self, "OnFenYongZhenXianHorseCountDown")
	g_ScriptEvent:RemoveListener("OnFenYongZhenXianSyncGameInfo", self, "OnFenYongZhenXianSyncGameInfo")
	g_ScriptEvent:RemoveListener("OnFenYongZhenXianPlayerKilled", self, "OnFenYongZhenXianPlayerKilled")
	g_ScriptEvent:RemoveListener("OnFenYongZhenXianReachScoreLimit", self, "OnFenYongZhenXianReachScoreLimit")
	self:CancelShowKillTipViewTick()
	self:CancelShowGoldTipViewTick()
end

function LuaWuYi2023FYZXTopRightWnd:OnHideTopAndRightTipWnd()
	LuaUtils.SetLocalRotation(self.RightBtn.transform, 0, 0, 180)
end

function LuaWuYi2023FYZXTopRightWnd:OnSceneRemainTimeUpdate(args)
	local totalSeconds = 0
    if CScene.MainScene then
		totalSeconds = CScene.MainScene.ShowTime
    end
	self.LeftTimeLabel.text = SafeStringFormat3("[ACF9FF]%s", LuaGamePlayMgr:GetRemainTimeText(totalSeconds))
end

function LuaWuYi2023FYZXTopRightWnd:OnFenYongZhenXianHorseKilled(attackerId, attackerForce, attackerClass, attackerGender)
	local horseIcon = attackerForce == 0 and WuYi2023_FenYongZhenXian.GetData().HorsePortrait[0] or WuYi2023_FenYongZhenXian.GetData().HorsePortrait[1]
	self:InitKillTipView(attackerForce, true, CUICommonDef.GetPortraitName(attackerClass, attackerGender,-1), horseIcon)
end

function LuaWuYi2023FYZXTopRightWnd:OnFenYongZhenXianPlayerKilled(attackerId, attackerForce, attackerClass, attackerGender, defenderId, defenderForce, defenderClass, defenderGender)
	self:InitKillTipView(attackerForce, false, CUICommonDef.GetPortraitName(attackerClass, attackerGender,-1), CUICommonDef.GetPortraitName(defenderClass, defenderGender,0))
end

function LuaWuYi2023FYZXTopRightWnd:OnFenYongZhenXianHorseCountDown(engineId, horseRebornTime, horseForce)
	self:InitHorseProgressTemplate(engineId, horseRebornTime, horseForce == LuaWuYi2023Mgr.m_SelfForce) 
end 

function LuaWuYi2023FYZXTopRightWnd:OnFenYongZhenXianSyncGameInfo(selfForce, gameInfo)
	local force1, force2 = 0, 1
	self.My1.gameObject:SetActive(force1 == selfForce)
	self.My2.gameObject:SetActive(force2 == selfForce)
	local info1, info2 = LuaWuYi2023Mgr.m_GameInfo[force1], LuaWuYi2023Mgr.m_GameInfo[force2]
	self.CoinNumLabel1.text = info1.score
	self.CoinNumLabel2.text = info2.score
	local totalMeters1, totalMeters2 =  info1.totalMeters, info2.totalMeters
	if info1.remain > 0 then
		self.RightInfoLabel1.text = SafeStringFormat3(LocalString.GetString("剩[ffff00]%d[ffffff]米"), info1.remain)
		self.Slider1.value = (totalMeters1 - info1.remain) / totalMeters1
	else
		local totalSeconds = info1.usetime
		self.RightInfoLabel1.text = SafeStringFormat3(LocalString.GetString("用时[ffff00]%02d:%02d"), math.floor(totalSeconds / 60), totalSeconds % 60)
		self.Slider1.value = 1
	end
	if info2.remain > 0 then
		self.RightInfoLabel2.text = SafeStringFormat3(LocalString.GetString("剩[ffff00]%d[ffffff]米"), info2.remain)
		self.Slider2.value = (totalMeters2 - info2.remain) / totalMeters2
	else
		local totalSeconds = info2.usetime
		self.RightInfoLabel2.text = SafeStringFormat3(LocalString.GetString("用时[ffff00]%02d:%02d"), math.floor(totalSeconds / 60), totalSeconds % 60)
		self.Slider2.value = 1
	end
end

function LuaWuYi2023FYZXTopRightWnd:OnFenYongZhenXianReachScoreLimit(score)
	self.GoldTipView.gameObject:SetActive(true)
	local ani = self.GoldTipView.gameObject:GetComponent(typeof(Animation))
	ani:Stop()
	ani:Play()
end

--@region UIEvent

function LuaWuYi2023FYZXTopRightWnd:OnRightBtnClick()
	LuaUtils.SetLocalRotation(self.RightBtn.transform,0,0,0)
	CTopAndRightTipWnd.showPackage = false
	CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end

--@endregion UIEvent

