--该文件为流程图工具自动生成,编码为utf8-无BOM
local floor = math.floor
local random = math.random
local max = math.max
local min = math.min
Flowchart_ClientAINpc={}
( function() Flowchart_ClientAINpc["Test"] = {
    start={1},
    variables=0,
    nodes={
      [1]={ --wait(3)
        event=EFlowEvent.Tick,
        command=function(self)
          local __ret__ = fc_wait(3)
          return __ret__
        end,
        success={2},
        failure={2},
      },
      [2]={ --Say("flowchart_test", 5)
        command=function(self)
          local __ret__ = self:DoSay("flowchart_test",5,nil)
          return __ret__
        end,
        success={3},
        failure={3},
      },
      [3]={ --wait(3)
        event=EFlowEvent.Tick,
        command=function(self)
          local __ret__ = fc_wait(3)
          return __ret__
        end,
        success={4},
        failure={4},
      },
      [4]={ --MoveTo(171, 132, 5)
        condition=function(self)
          if self:AIMoveTo(171,132,5) then return 1 else return 0 end
        end,
        success={5},
        failure={5},
      },
      [5]={ --wait(2)
        event=EFlowEvent.Tick,
        command=function(self)
          local __ret__ = fc_wait(2)
          return __ret__
        end,
        success={6},
        failure={6},
      },
      [6]={ --StopMove()
        command=function(self)
          local __ret__ = self:StopMove()
          return __ret__
        end,
        success={7},
        failure={7},
      },
      [7]={ --Say("Reach destination",10)
        command=function(self)
          local __ret__ = self:DoSay("Reach destination",10,nil)
          return __ret__
        end,
        success={8},
        failure={8},
      },
      [8]={ --wait(11)
        event=EFlowEvent.Tick,
        command=function(self)
          local __ret__ = fc_wait(11)
          return __ret__
        end,
        success={7},
        failure={7},
      },
    },--nodes
  }  end)(); --Test