local CTopAndRightTipWnd=import "L10.UI.CTopAndRightTipWnd"

CLuaLianLianKanTopRightWnd=class()
RegistClassMember(CLuaLianLianKanTopRightWnd,"m_ExpandButton")
RegistClassMember(CLuaLianLianKanTopRightWnd,"m_NameLabel1")
RegistClassMember(CLuaLianLianKanTopRightWnd,"m_NameLabel2")
RegistClassMember(CLuaLianLianKanTopRightWnd,"m_ValueLabel1")
RegistClassMember(CLuaLianLianKanTopRightWnd,"m_ValueLabel2")

RegistClassMember(CLuaLianLianKanTopRightWnd,"m_PlayerId1")
RegistClassMember(CLuaLianLianKanTopRightWnd,"m_PlayerId2")

function CLuaLianLianKanTopRightWnd:Init()
    self.m_PlayerId1=0
    self.m_PlayerId2=0

    self.m_ExpandButton=FindChild(self.transform,"ExpandButton").gameObject
    self.m_NameLabel1=FindChild(self.transform,"NameLabel1"):GetComponent(typeof(UILabel))
    self.m_NameLabel1.text=nil
    self.m_NameLabel2=FindChild(self.transform,"NameLabel2"):GetComponent(typeof(UILabel))
    self.m_NameLabel2.text=nil
    self.m_ValueLabel1=FindChild(self.transform,"ValueLabel1"):GetComponent(typeof(UILabel))
    self.m_ValueLabel1.text=nil
    self.m_ValueLabel2=FindChild(self.transform,"ValueLabel2"):GetComponent(typeof(UILabel))
    self.m_ValueLabel2.text=nil

    if CLuaLianLianKanMgr.m_PlayerInfos then
        self.m_NameLabel1.text=CLuaLianLianKanMgr.m_PlayerInfos[1][3]
        self.m_ValueLabel1.text=tostring(CLuaLianLianKanMgr.m_PlayerInfos[1][2])
        self.m_NameLabel2.text=CLuaLianLianKanMgr.m_PlayerInfos[2][3]
        self.m_ValueLabel2.text=tostring(CLuaLianLianKanMgr.m_PlayerInfos[2][2])
    end

    UIEventListener.Get(self.m_ExpandButton).onClick=DelegateFactory.VoidDelegate(function(go)
        LuaUtils.SetLocalRotation(self.m_ExpandButton.transform,0,0,0)
        CTopAndRightTipWnd.showPackage = false;
        CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
    end)
end
function CLuaLianLianKanTopRightWnd:OnEnable()
    g_ScriptEvent:AddListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:AddListener("UpdateAllPlayerScoreToClient", self, "OnUpdateAllPlayerScoreToClient")
    
end
function CLuaLianLianKanTopRightWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HideTopAndRightTipWnd", self, "OnHideTopAndRightTipWnd")
    g_ScriptEvent:RemoveListener("UpdateAllPlayerScoreToClient", self, "OnUpdateAllPlayerScoreToClient")

    if CUIManager.IsLoaded(CUIResources.TopAndRightTipWnd) then
        CUIManager.CloseUI(CUIResources.TopAndRightTipWnd)
    end
end
function CLuaLianLianKanTopRightWnd:OnUpdateAllPlayerScoreToClient(playerId1,score1,name1,playerId2,score2,name2)
    self.m_NameLabel1.text=name1
    self.m_ValueLabel1.text=tostring(score1)
    self.m_NameLabel2.text=name2
    self.m_ValueLabel2.text=tostring(score2)
end


function CLuaLianLianKanTopRightWnd:OnHideTopAndRightTipWnd()
    LuaUtils.SetLocalRotation(self.m_ExpandButton.transform,0,0,180)
end
