local UILabel = import "UILabel"
local QnRadioBox = import "L10.UI.QnRadioBox"
local GameObject = import "UnityEngine.GameObject"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUILabelRef = import "L10.Engine.CUILabelRef"
local PlayerSettings = import "L10.Game.PlayerSettings"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"

LuaChooseLanguageWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChooseLanguageWnd, "CN", "CN", GameObject)
RegistChildComponent(LuaChooseLanguageWnd, "VN", "VN", GameObject)
RegistChildComponent(LuaChooseLanguageWnd, "EN", "EN", GameObject)
RegistChildComponent(LuaChooseLanguageWnd, "TH", "TH", GameObject)
RegistChildComponent(LuaChooseLanguageWnd, "INA", "INA", GameObject)
RegistChildComponent(LuaChooseLanguageWnd, "RestartLabel", "RestartLabel", UILabel)
RegistChildComponent(LuaChooseLanguageWnd, "CommitBtn", "CommitBtn", GameObject)
RegistChildComponent(LuaChooseLanguageWnd, "TypeRoot", "TypeRoot", QnRadioBox)
RegistChildComponent(LuaChooseLanguageWnd, "CommitLabel", "CommitLabel", UILabel)

--@endregion RegistChildComponent end

function LuaChooseLanguageWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.preSelectIndex = LocalString.languageId
    UIEventListener.Get(self.CommitBtn).onClick = DelegateFactory.VoidDelegate(function()
        self:OnCommitButtonClick()
    end)
end

function LuaChooseLanguageWnd:Init()
    self:InitRadioBox()
    self:InitLanguageButtonText()
    self.TypeRoot:ChangeTo(LocalString.languageId, true)
end

--@region UIEvent

--@endregion UIEvent

function LuaChooseLanguageWnd:InitLanguageButtonText()
    local languageItemList = {self.CN, self.VN, self.EN, self.TH, self.INA}
    local fontPostFixList = {"", "_vn", "_en", "_th", "_ina"}
    for i = 1, #languageItemList do
        local languageItem = languageItemList[i]
        local languageComp = languageItem.transform:Find("Medal"):GetComponent(typeof(UILabel))
        languageComp.text = LocalString.GetLanguageName(i-1, false)
        languageComp:LoadFont("Assets/Res" .. fontPostFixList[i] .. "/UI/Font/Default.TTF")
    end
end

function LuaChooseLanguageWnd:InitRadioBox()
    self.TypeRoot.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        self:PreSetLangeuage(index)
    end)
end

function LuaChooseLanguageWnd:PreSetLangeuage(index)
    self.preSelectIndex = index
    local fontPostFixList = {"", "_vn", "_en", "_th", "_ina"}
    self.RestartLabel:LoadFont("Assets/Res" .. fontPostFixList[index+1] .. "/UI/Font/Default.TTF")
    self.CommitLabel:LoadFont("Assets/Res" .. fontPostFixList[index+1] .. "/UI/Font/Default.TTF")
    if index == 0 then
        self.RestartLabel.text = "变更语言需要重启游戏"
        self.CommitLabel.text = "变更并重启游戏"
    elseif index == 1 then
        self.RestartLabel.text = "Thay đổi ngôn ngữ yêu cầu khởi động lại trò chơi"
        self.CommitLabel.text = "Thay đổi và khởi động lại trò chơi"
    elseif index == 2 then
        self.RestartLabel.text = "Changing language requires restarting the game"
        self.CommitLabel.text = "Change and restart game"
    elseif index == 3 then
        self.RestartLabel.text = "การเปลี่ยนภาษาต้องเริ่มเกมใหม่"
        self.CommitLabel.text = "เปลี่ยนและเริ่มเกมใหม่"
    elseif index == 4 then
        self.RestartLabel.text = "Mengubah bahasa memerlukan memulai ulang game"
        self.CommitLabel.text = "Ubah dan mulai ulang permainan"
    end
end

function LuaChooseLanguageWnd:OnCommitButtonClick()
    LocalString.language = LocalString.languages[self.preSelectIndex]
    --restart

    PlayerSettings.AgreeAccountList = ""
    PlayerSettings.PrivacyAgreeList = ""
    PlayerSettings.AgreeToUserAgreement = false
    PlayerSettings.UserAgreementVersion = 0

    PlayerPrefs.Save()
    if CommonDefs.IS_PUB_RELEASE then
        RegisterTickOnce(function()
            CommonDefs.TryRestartApplication()
        end, 100)
    else
        Application.Quit()
    end
end
