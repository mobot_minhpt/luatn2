
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnTableView = import "L10.UI.QnTableView"
local UILabel = import "UILabel"
local QnButton = import "L10.UI.QnButton"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local Profession = import "L10.Game.Profession"
local EnumClass = import "L10.Game.EnumClass"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local Constants = import "L10.Game.Constants"

LuaAntiProfessionMakeWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaAntiProfessionMakeWnd, "ScrollView", "ScrollView", QnTableView)
RegistChildComponent(LuaAntiProfessionMakeWnd, "AntiLvLab", "AntiLvLab", UILabel)
RegistChildComponent(LuaAntiProfessionMakeWnd, "OpBtn", "OpBtn", QnButton)
RegistChildComponent(LuaAntiProfessionMakeWnd, "TipBtn", "TipBtn", QnButton)
RegistChildComponent(LuaAntiProfessionMakeWnd, "OwnExp", "OwnExp", UILabel)
RegistChildComponent(LuaAntiProfessionMakeWnd, "CostExp", "CostExp", UILabel)
RegistChildComponent(LuaAntiProfessionMakeWnd, "OwnMoney", "OwnMoney", UILabel)
RegistChildComponent(LuaAntiProfessionMakeWnd, "CostMoney", "CostMoney", UILabel)
RegistChildComponent(LuaAntiProfessionMakeWnd, "OwnMana", "OwnMana", UILabel)
RegistChildComponent(LuaAntiProfessionMakeWnd, "CostMana", "CostMana", UILabel)
RegistChildComponent(LuaAntiProfessionMakeWnd, "CostExpRoot", "CostExpRoot", QnSelectableButton)
RegistChildComponent(LuaAntiProfessionMakeWnd, "CostMoneyRoot", "CostMoneyRoot", QnSelectableButton)
RegistChildComponent(LuaAntiProfessionMakeWnd, "CostManaRoot", "CostManaRoot", QnSelectableButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaAntiProfessionMakeWnd, "CostManaRoot")
RegistClassMember(LuaAntiProfessionMakeWnd, "m_NeedExp")
RegistClassMember(LuaAntiProfessionMakeWnd, "m_NeedMoney")
RegistClassMember(LuaAntiProfessionMakeWnd, "m_NeedMana")
RegistClassMember(LuaAntiProfessionMakeWnd, "m_CostType")
RegistClassMember(LuaAntiProfessionMakeWnd, "m_Btns")
RegistClassMember(LuaAntiProfessionMakeWnd, "m_EffectsInfos")
RegistClassMember(LuaAntiProfessionMakeWnd, "m_DefaultType")

function LuaAntiProfessionMakeWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.m_NeedExp = 0
    self.m_NeedMoney = 0
    self.m_NeedMana = 0
    self.m_CostType = 0
    --     eMana = 1,
    --     eExp = 2,
    --     eMoney = 3,
end

function LuaAntiProfessionMakeWnd:Init()
    self.OpBtn.OnClick = DelegateFactory.Action_QnButton(function (btn)
        if self.m_CostType ~= 0 then
            local message = self:GetMakeSureMessage(self.m_CostType)
            MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function () 
                Gac2Gas.AntiProfession_GenItem(self.m_CostType, 1)
            end), nil, nil, nil, false)
        else 
            g_MessageMgr:ShowMessage("ANTIPROFESSION_MAKEVIEW_NOSELECTED")
        end
    end)

    self.TipBtn.OnClick = DelegateFactory.Action_QnButton(function (btn)
        g_MessageMgr:ShowMessage("ANTIPROFESSION_MAKEVIEW_TIP")
    end)

    self.CostExpRoot.OnClick = DelegateFactory.Action_QnButton(function (btn)
        self:OnCostTypeClick(2)
    end)

    self.CostMoneyRoot.OnClick = DelegateFactory.Action_QnButton(function (btn)
        self:OnCostTypeClick(3)
    end)

    self.CostManaRoot.OnClick = DelegateFactory.Action_QnButton(function (btn)
        self:OnCostTypeClick(1)
    end)

    self.m_Btns = {self.CostManaRoot, self.CostExpRoot, self.CostMoneyRoot}

    self:InitInfo()
end

--@region UIEvent

--@endregion UIEvent

function LuaAntiProfessionMakeWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdateMainPlayerSoulCoreMana", self, "RefreshCostAndOwn")
    g_ScriptEvent:AddListener("MainPlayerPlayPropUpdate", self, "RefreshCostAndOwn")
    g_ScriptEvent:AddListener("MainPlayerUpdateMoney", self, "RefreshCostAndOwn")
end

function LuaAntiProfessionMakeWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateMainPlayerSoulCoreMana", self, "RefreshCostAndOwn")
    g_ScriptEvent:RemoveListener("MainPlayerPlayPropUpdate", self, "RefreshCostAndOwn")
    g_ScriptEvent:RemoveListener("MainPlayerUpdateMoney", self, "RefreshCostAndOwn")
end

--     eMana = 1,
--     eExp = 2,
--     eMoney = 3,
function LuaAntiProfessionMakeWnd:OnCostTypeClick(costType)
    local limitLevel = SoulCore_Settings.GetData().AntiProfessionMinSoulCoreLvUseMana
    if costType == 1 and CClientMainPlayer.Inst and CClientMainPlayer.Inst.SkillProp.SoulCore.Level < limitLevel then
        self.m_Btns[costType]:SetSelected(false, false)
        g_MessageMgr:ShowMessage("ANTIPROFESSION_CANT_USE_MANA", limitLevel)
        return
    end

    self.m_CostType = costType

    for k, v in ipairs(self.m_Btns) do
        v:SetSelected(k == costType, false)
    end
end

function LuaAntiProfessionMakeWnd:InitInfo()
    local adjustLevel, adjustId = LuaZongMenMgr:CalMainPlayerAdjustAntiProfessionLevel()
    local level, id = LuaZongMenMgr:CalMainPlayerAntiProfessionLevel()
    local data = SoulCore_AntiProfessionExp.GetData(adjustId)
    self.m_NeedExp = data.ItemInExp
    self.m_NeedMoney = data.ItemInMoney
    self.m_NeedMana = data.ItemInMana
    self:RefreshCostAndOwn()
    if self.m_DefaultType ~= nil then
        self:OnCostTypeClick(self.m_DefaultType)
    end
    self.AntiLvLab.text = SafeStringFormat3("%.1f", level)

    self:InitEffects()
end

function LuaAntiProfessionMakeWnd:InitEffects()
    self.ScrollView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_EffectsInfos
        end,
        function(item, index)
            self:InitEffect(item, self.m_EffectsInfos[index + 1])
        end
    )

    self:RefreshEffects()
end

function LuaAntiProfessionMakeWnd:RefreshEffects()
    self.m_EffectsInfos = {}

    CommonDefs.DictIterate(CClientMainPlayer.Inst.SkillProp.AntiProfessionSkill, DelegateFactory.Action_object_object(function(___key, ___value)
        local data = SoulCore_AntiProfessionLevel.GetData(___value.CurLevel)
        local oriMul =  data and data.AntiProfessionMul or 0
        
        table.insert(self.m_EffectsInfos, {key = ___key, curLevel = ___value.CurLevel, profession = ___value.Profession, oriMul = oriMul})
    end))

    table.sort(self.m_EffectsInfos, function(a , b)
        if a.oriMul > b.oriMul then
            return true
        elseif a.oriMul == b.oriMul then
            return a.profession < b.profession
        end
        return false
    end)
    
    self.ScrollView:ReloadData(true, false)
end

function LuaAntiProfessionMakeWnd:InitEffect(item, info)
    local lab = item.transform:Find("Lab"):GetComponent(typeof(UILabel))

    local profession = info.profession
    local proClass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), profession)

    local oriMul =  info.oriMul
    local effectStr
    
    if CClientMainPlayer.Inst.IsInXianShenStatus then
        local index = info.key
        local oriLv = info.curLevel
        local adjustLv = LuaZongMenMgr:GetAdjustAntiProfessionSkillLevel(index, oriLv)
        if oriLv ~= adjustLv then
            local adjustData = SoulCore_AntiProfessionLevel.GetData(adjustLv)
            local adjustMul =  adjustData and adjustData.AntiProfessionMul or 0
            effectStr = SafeStringFormat3("[c][%s]+%.1f%%[-][/c]", Constants.ColorOfFeiSheng, adjustMul * 100)
        else
            effectStr = SafeStringFormat3("+%.1f%%", oriMul * 100)
        end
    else
        effectStr = SafeStringFormat3("+%.1f%%", oriMul * 100)
    end

    lab.text = SafeStringFormat3(LocalString.GetString("对%s伤害 %s"), Profession.GetFullName(proClass) , effectStr)
end

function LuaAntiProfessionMakeWnd:RefreshCostAndOwn()
    local exp = CClientMainPlayer.Inst.PlayProp.Exp
    local silver = CClientMainPlayer.Inst.Silver
    local mana = CClientMainPlayer.Inst.SkillProp.SoulCore.Mana

    self.CostExp.text = self.m_NeedExp
    self.CostMoney.text = self.m_NeedMoney
    self.CostMana.text = self.m_NeedMana

    self.OwnExp.text = self.m_NeedExp > exp and SafeStringFormat3("[c][E74F55]%s[-][/c]", tostring(exp)) or tostring(exp)
    self.OwnMoney.text = self.m_NeedMoney > silver and SafeStringFormat3("[c][E74F55]%s[-][/c]", tostring(silver)) or tostring(silver)
    self.OwnMana.text = self.m_NeedMana > mana and SafeStringFormat3("[c][E74F55]%s[-][/c]", tostring(mana)) or tostring(mana)

    -- if self.m_NeedExp <= exp then
    --     self.m_DefaultType = 2
    -- elseif self.m_NeedMoney <= silver then
    --     self.m_DefaultType = 3
    -- elseif self.m_NeedMana <= mana then
    --     self.m_DefaultType = 1
    -- end
end

function LuaAntiProfessionMakeWnd:GetMakeSureMessage(costType)
    local costName, costNum
    if costType == 1 then
        costName = LocalString.GetString("灵力")
        costNum = self.m_NeedMana
    elseif costType == 2 then
        costName = LocalString.GetString("经验")
        costNum = self.m_NeedExp
    elseif costType == 3 then
        costName = LocalString.GetString("银两")
        costNum = self.m_NeedMoney
    end

    return g_MessageMgr:FormatMessage("AntiProfession_MakeCost_MakeSure", costNum, costName)
end