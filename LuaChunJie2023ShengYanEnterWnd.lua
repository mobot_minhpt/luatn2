local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CItemMgr = import "L10.Game.CItemMgr"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"

LuaChunJie2023ShengYanEnterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChunJie2023ShengYanEnterWnd, "RuleBtn", GameObject)
RegistChildComponent(LuaChunJie2023ShengYanEnterWnd, "MatchBtn", GameObject)
RegistChildComponent(LuaChunJie2023ShengYanEnterWnd, "CancelBtn", GameObject)

RegistChildComponent(LuaChunJie2023ShengYanEnterWnd, "TimeLabel", UILabel)
RegistChildComponent(LuaChunJie2023ShengYanEnterWnd, "FormLabel", UILabel)
RegistChildComponent(LuaChunJie2023ShengYanEnterWnd, "LevelLabel", UILabel)
RegistChildComponent(LuaChunJie2023ShengYanEnterWnd, "DescLabel", UILabel)

RegistChildComponent(LuaChunJie2023ShengYanEnterWnd, "ItemCell1", CQnReturnAwardTemplate)
RegistChildComponent(LuaChunJie2023ShengYanEnterWnd, "ItemCell2", CQnReturnAwardTemplate)
RegistChildComponent(LuaChunJie2023ShengYanEnterWnd, "ItemCell3", CQnReturnAwardTemplate)
RegistChildComponent(LuaChunJie2023ShengYanEnterWnd, "ItemCell4", CQnReturnAwardTemplate)

RegistClassMember(LuaChunJie2023ShengYanEnterWnd,"gameplayId")
--@endregion RegistChildComponent end

function LuaChunJie2023ShengYanEnterWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    UIEventListener.Get(self.RuleBtn).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleBtnClick()
	end)
    UIEventListener.Get(self.MatchBtn).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMatchBtnClick()
	end)
    UIEventListener.Get(self.CancelBtn).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCancelBtnClick()
	end)
end

function LuaChunJie2023ShengYanEnterWnd:Init()
    -- 活动时间，任务描述读取策划表
    local data = ChunJie_TTSYSetting.GetData()
    self.gameplayId = data.PlayDesignId
	self.TimeLabel.text = data.PlayTimeString
	self.FormLabel.text = data.PlayTaskForm
    self.DescLabel.text = data.PlayDescString
    self.LevelLabel.text = SafeStringFormat3(LocalString.GetString("%d级"), data.LevelLimit)
    -- 初始化活动奖励
    local firstWin = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eSpring2023TTSY_ChampionTimes)
    self.ItemCell1.transform:Find("mask").gameObject:SetActive(firstWin ~= 0)
    self.ItemCell1:Init(CItemMgr.Inst:GetItemTemplate(data.ChampionRewardItemId), 1)
    local reward1 = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eSpring2023TTSY_DailyRewardTimes_1)
    self.ItemCell2.transform:Find("mask").gameObject:SetActive(reward1 ~= 0)
    self.ItemCell2:Init(CItemMgr.Inst:GetItemTemplate(ChunJie_TTSYRank.GetData(1).RewardItem), 1)
    local reward2 = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eSpring2023TTSY_DailyRewardTimes_2)
    self.ItemCell3.transform:Find("mask").gameObject:SetActive(reward2 ~= 0)
    self.ItemCell3:Init(CItemMgr.Inst:GetItemTemplate(ChunJie_TTSYRank.GetData(2).RewardItem), 1)
    local reward3 = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eSpring2023TTSY_DailyRewardTimes_3)
    self.ItemCell4.transform:Find("mask").gameObject:SetActive(reward3 ~= 0)
    self.ItemCell4:Init(CItemMgr.Inst:GetItemTemplate(ChunJie_TTSYRank.GetData(3).RewardItem), 1)

    if self.gameplayId and CClientMainPlayer.Inst then
        Gac2Gas.GlobalMatch_RequestCheckSignUp(self.gameplayId, CClientMainPlayer.Inst.Id)
    end
end

function LuaChunJie2023ShengYanEnterWnd:UpdateMatchButton(isMatching)
    -- 改变下方报名按钮状态
    self.MatchBtn:SetActive(not isMatching)
    self.CancelBtn:SetActive(isMatching)
end

function LuaChunJie2023ShengYanEnterWnd:OnGlobalMatch_CheckInMatchingResultWithInfo(playerId, playId, isInMatching, resultStr, info_U)
    if playId == self.gameplayId then
        self:UpdateMatchButton(isInMatching)
    end
end

function LuaChunJie2023ShengYanEnterWnd:OnGlobalMatch_SignUpPlayResult(playId, success)
    if playId == self.gameplayId and success then
        self:UpdateMatchButton(true)
    end
end

function LuaChunJie2023ShengYanEnterWnd:OnGlobalMatch_CancelSignUpResult(playId, success)
    if playId == self.gameplayId and success then
        self:UpdateMatchButton(false)
    end
end

--@region UIEvent
function LuaChunJie2023ShengYanEnterWnd:OnRuleBtnClick()
    g_MessageMgr:ShowMessage("ChunJie2023_TaoTieShengYan_TaskDes")
end

function LuaChunJie2023ShengYanEnterWnd:OnMatchBtnClick()
    Gac2Gas.GlobalMatch_RequestSignUp(self.gameplayId)
end

function LuaChunJie2023ShengYanEnterWnd:OnCancelBtnClick()
	Gac2Gas.GlobalMatch_RequestCancelSignUp(self.gameplayId)
end
--@endregion UIEvent

function LuaChunJie2023ShengYanEnterWnd:OnEnable()
    g_ScriptEvent:AddListener("GlobalMatch_CheckInMatchingResultWithInfo", self, "OnGlobalMatch_CheckInMatchingResultWithInfo")
    g_ScriptEvent:AddListener("GlobalMatch_SignUpPlayResult", self, "OnGlobalMatch_SignUpPlayResult")
    g_ScriptEvent:AddListener("GlobalMatch_CancelSignUpResult", self, "OnGlobalMatch_CancelSignUpResult")
end

function LuaChunJie2023ShengYanEnterWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GlobalMatch_CheckInMatchingResultWithInfo", self, "OnGlobalMatch_CheckInMatchingResultWithInfo")
    g_ScriptEvent:RemoveListener("GlobalMatch_SignUpPlayResult", self, "OnGlobalMatch_SignUpPlayResult")
    g_ScriptEvent:RemoveListener("GlobalMatch_CancelSignUpResult", self, "OnGlobalMatch_CancelSignUpResult")
end