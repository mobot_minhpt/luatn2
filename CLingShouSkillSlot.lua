-- Auto Generated!!
local CLingShouSkillSlot = import "L10.UI.CLingShouSkillSlot"
local Collider = import "UnityEngine.Collider"
local CommonDefs = import "L10.Game.CommonDefs"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventDelegate = import "EventDelegate"
local EventManager = import "EventManager"
local Skill_AllSkills = import "L10.Game.Skill_AllSkills"
CLingShouSkillSlot.m_Init_CS2LuaHook = function (this, skillId) 
    this.skillId = skillId
    local template = Skill_AllSkills.GetData(skillId)
    if template ~= nil then
        --if (hasSkillTf!=null)
        this.hasSkillTf.gameObject:SetActive(true)
        this.icon:LoadSkillIcon(template.SkillIcon)
        this.levelLabel.text = "Lv." .. template.Level

        --GetComponent<Collider>().enabled = true;
        local col = CommonDefs.GetComponent_Component_Type(this, typeof(Collider))
        if col ~= nil then
            col.enabled = true
        end

        local noskill = this.transform:Find("NoSkill")
        if noskill ~= nil then
            noskill.gameObject:SetActive(false)
        end
    else
        this:InitNull()
    end

    if this.isSelected then
        --更新选中的条目
        --CLingShouMgr.Inst.selectedSkillId = skillId;
        EventManager.BroadcastInternalForLua(EnumEventType.LingShouSelectSkill, {skillId})
    end
end
CLingShouSkillSlot.m_Awake_CS2LuaHook = function (this) 
    if this.toggle ~= nil then
        CommonDefs.ListAdd(this.toggle.onChange, typeof(EventDelegate), CreateFromClass(EventDelegate, DelegateFactory.Callback(function () 
            if this.isSelected then
                --更新选中的条目
                --CLingShouMgr.Inst.selectedSkillId = skillId;
                EventManager.BroadcastInternalForLua(EnumEventType.LingShouSelectSkill, {this.skillId})
            end
        end)))
    end
end
CLingShouSkillSlot.m_OnClick_CS2LuaHook = function (this) 
    if this.skillId > 0 then
        CSkillInfoMgr.ShowSkillInfoWnd(this.skillId, true, 0, 0, nil)
    end
end
CLingShouSkillSlot.m_InitNull_CS2LuaHook = function (this) 
    local col = CommonDefs.GetComponent_Component_Type(this, typeof(Collider))
    if col ~= nil then
        col.enabled = false
    end
    this.hasSkillTf.gameObject:SetActive(false)
    this.icon.material = nil
    this.levelLabel.text = ""

    local noskill = this.transform:Find("NoSkill")
    if noskill ~= nil then
        noskill.gameObject:SetActive(true)
    end
end
