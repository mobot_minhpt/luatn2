require("common/common_include")

--require("design/GamePlay/WorldCup")
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

CLuaWorldCupMgr = {}
CLuaWorldCupMgr.m_JingCaiYinLiang = 0 or Lua_WorldCup_Setting.GetData().JingCaiYinLiang
CLuaWorldCupMgr.m_RenRenZhongCaiPiaoCode = nil
CLuaWorldCupMgr.m_MaxSetHomeTeamTimes = 0 or WorldCup_Setting.GetData().SetHomeTeamTimesLimit
CLuaWorldCupMgr.m_SetHomeTeamNeedSilver = 0 or WorldCup_Setting.GetData().SetHomeTeamNeedSilver

CLuaWorldCupMgr.m_GMTime = nil

CLuaWorldCupMgr.m_MaxTouZhuNum = 99

CLuaWorldCupMgr.m_TaoTaiSaiJingCaiBeginId = 15
CLuaWorldCupMgr.m_Detail_Pid = nil
CLuaWorldCupMgr.m_Detail_ResultData = nil
CLuaWorldCupMgr.m_Detail_Value = nil

CLuaWorldCupMgr.m_HomeTeamId = 0

CLuaWorldCupMgr.m_SelectDataTbl = {
    LocalString.GetString("主队胜"),
    LocalString.GetString("客队胜"),
    LocalString.GetString("平局")
}

CLuaWorldCupMgr.m_SelectDataTbl2 = {
    LocalString.GetString("主队胜"),
    LocalString.GetString("客队胜"),
}

CLuaWorldCupMgr.m_GroupPlayRoundTbl = {}

for i=1, 8 do
    CLuaWorldCupMgr.m_GroupPlayRoundTbl[i] = {}
end

local Group2Index = {
    A = 1,
    B = 2,
    C = 3,
    D = 4,
    E = 5,
    F = 6,
    G = 7,
    H = 8
}

-- assert(#Lua_WorldCup_PlayInfo == 64)
-- for round, designData in ipairs(Lua_WorldCup_PlayInfo) do
--     local group = string.sub(designData.RoundInfo, 1, 1)
--     if Group2Index[group] then
--         local index = Group2Index[group]
--         table.insert(CLuaWorldCupMgr.m_GroupPlayRoundTbl[index], round)
--     end
-- end
-- for i=1, 8 do
--     assert(#CLuaWorldCupMgr.m_GroupPlayRoundTbl[i] == 6)
-- end

-- 策划数据统一从这里获取
function CLuaWorldCupMgr.GetTeamInfoById(tid)
    return WorldCup_TeamInfo.GetData(tid)
end

function CLuaWorldCupMgr.GetPlayInfoById(rid)
    return WorldCup_PlayInfo.GetData(rid)
end

function CLuaWorldCupMgr.GetJingCaiInfoById(pid)
    return WorldCup_TodayJingCai.GetData(pid)
end

function CLuaWorldCupMgr.GetTimeStampForPlay(value)
    local year, month, day, hour, min = string.match(value, "(%d+)-(%d+)-(%d+) (%d+):(%d+)")        
    local timestamp = os.time({year=tonumber(year), month=tonumber(month), day=tonumber(day), hour=tonumber(hour), min=tonumber(min), sec=0, isdst=false})
    return timestamp
end

function CLuaWorldCupMgr.GetTimeStampForPlay_BeiJing(value)
    local year, month, day, hour, min = string.match(value, "(%d+)-(%d+)-(%d+) (%d+):(%d+)")        
    local timestamp = os.time({year=tonumber(year), month=tonumber(month), day=tonumber(day), hour=tonumber(hour), min=tonumber(min), sec=0, isdst=false})

    local now = CServerTimeMgr.Inst.timeStamp
    return timestamp + now - os.time(os.date("!*t", now)) - 8 * 60 * 60
end

function CLuaWorldCupMgr.GetJingCaiWinFx()
    if Lua_WorldCup_Setting["JingCaiWinFx"] then
        return Lua_WorldCup_Setting["JingCaiWinFx"].Value
    end

    return ""
end

function CLuaWorldCupMgr.HandleMyJingCaiTextColor(obj, score1, score2, myVote)
    -- 默认黄色
    obj.color = Color.yellow

    if score1 == -1 or score2 == -1 then
        return
    end

    if score1 > score2 then
        if myVote == 1 then
            obj.color = Color.green
        else
            obj.color = Color.red
        end
    elseif score1 < score2 then
        if myVote == 2 then
            obj.color = Color.green
        else
            obj.color = Color.red
        end
    else
        if myVote == 3 then
            obj.color = Color.green
        else
            obj.color = Color.red
        end
    end
end

function CLuaWorldCupMgr.GetProgressBarValueByMaxRound(maxRound)
    if not maxRound then
        return 0
    end

    if maxRound >= 1 and maxRound <= 48 then
        return 0
    elseif maxRound >= 49 and maxRound <= 56 then
        return 0.25
    elseif maxRound >= 57 and maxRound <= 60 then
        return 0.5
    elseif maxRound >= 61 and maxRound <= 62 then
        return 0.75
    elseif maxRound >= 63 and maxRound <= 63 then
        return 0.75
    elseif maxRound >= 64 and maxRound <= 64 then
        return 1
    else
        return 0
    end
end
