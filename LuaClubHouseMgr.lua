local CUIManager = import "L10.UI.CUIManager"
local MsgPackImpl=import "MsgPackImpl"
local Object=import "System.Object"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local ChatRoomSDK = import "ChatRoom.Unity3d.ChatRoomSDK"
local CCMiniAPIMgr = import "L10.Game.CCMiniAPIMgr"
local NativeTools = import "L10.Engine.NativeTools"
local Setting = import "L10.Engine.Setting"
local CChatMgr = import "L10.Game.CChatMgr"
local MessageWndManager=import "L10.UI.MessageWndManager"
local CSocialWndMgr = import "L10.UI.CSocialWndMgr"
local AudioSvcCsInterface = import "L10.Engine.AudioSvcCsInterface"
local EnumIOSAVAudioSessionCategory = import "L10.Engine.EnumIOSAVAudioSessionCategory"
local Time = import "UnityEngine.Time"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CIMMgr = import "L10.Game.CIMMgr"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local ChatChannelDef = import "L10.Game.ChatChannelDef"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"

if rawget(_G, "LuaClubHouseMgr") then
    g_ScriptEvent:RemoveListener("AppFocusChange", LuaClubHouseMgr, "OnApplicationFocus")
    g_ScriptEvent:RemoveListener("OnApplicationPause", LuaClubHouseMgr, "OnApplicationPause")
end

LuaClubHouseMgr = class()

g_ScriptEvent:AddListener("AppFocusChange", LuaClubHouseMgr, "OnApplicationFocus")
g_ScriptEvent:AddListener("OnApplicationPause", LuaClubHouseMgr, "OnApplicationPause")

------------------------
-- Definition
------------------------
EnumCHRoomType = { --和服务器端定义保持一致
    all = "all",
	everyone = "everyone",
    apponly = "apponly",
    private = "private",
}

EnumCHSortType = { --和服务器端定义保持一致
    heat = "heat",
    time = "time",
}

EnumCHCharacterType = {
    Owner = "Owner", --房主
    Speaker = "Speaker", --主播
    Listener = "Listener",--听众
}

EnumCHStatusType = {
    HandsUp = "HandsUp", --申请上麦
    Speaking = "Speaking", -- 开麦
    Forbid = "Forbid",--禁言
}

EnumCHAPPType = {
    qnmobile = "qnmobile",
    tymobile = "tymobile",
}

EnumClubHousePlatformType = {--和服务器端定义保持一致
	yunxin = 0,
	cc = 1,
}

EnumClubHouseRankType = {
	Give = 1,
	Recv = 2,
}

------------------------
-- Data
------------------------
LuaClubHouseMgr.m_CurRoomInfo = {}
LuaClubHouseMgr.m_ListenerAdded = false
LuaClubHouseMgr.m_CurAppName = "qnmobile"
LuaClubHouseMgr.m_LastSelectedTypeName = ""
LuaClubHouseMgr.m_RecoverIOSAudioSessionTick = nil
LuaClubHouseMgr.m_GenericStringObjectDict = MakeGenericClass(Dictionary, String, Object)
LuaClubHouseMgr.m_LocalMuted = false --本地静音开关，如果远程服务器出现问题无法正常响应，保底本地用户可以正常闭麦
LuaClubHouseMgr.m_LastCheckAndConfirmCCRoomTime = 0
LuaClubHouseMgr.m_RoomQueryType = EnumCHRoomType.apponly
LuaClubHouseMgr.m_RoomSortType = EnumCHSortType.heat
LuaClubHouseMgr.m_ChatRoomView = nil -- 记录当前打开的窗口

function LuaClubHouseMgr:GetRoomTypeNameList()
    if self:IsCrossAppRoomEnabled() then
        return InitializeList(CreateFromClass(MakeGenericClass(List, String)), String, 
                    LocalString.GetString("全部"), LocalString.GetString("开放"), LocalString.GetString("倩女"))
    else
        return InitializeList(CreateFromClass(MakeGenericClass(List, String)), String,
                    LocalString.GetString("倩女"))
    end
end

function LuaClubHouseMgr:GetRoomTypeByName(name)
    if name == LocalString.GetString("全部") then
        return "all"
    elseif name == LocalString.GetString("开放") then
        return "everyone"
    elseif name == LocalString.GetString("倩女") then
        return "apponly"
    else
        return ""
    end
end

function LuaClubHouseMgr:SetSelectedRoomTypeName(name)
    self.m_LastSelectedTypeName = name
end

--deprecated 目前不需要记录这个信息了，创建房间一律优先默认选中倩女房间
function LuaClubHouseMgr:LastSelectedRoomTypeIsAppOnly()
    return not self:IsCrossAppRoomEnabled() or LuaClubHouseMgr:GetRoomTypeByName(self.m_LastSelectedTypeName) == "apponly"
end

function LuaClubHouseMgr:GetBroadcasterStatusSprite(member, ignoreForbid)
    if self:IsSpeaking(member) then
        --开麦
        return "clubhouse_kaimai"
    elseif not ignoreForbid and member.Status[EnumCHStatusType.Forbid] then
        --禁言
        return "clubhouse_pingbi"
    else 
        -- 默认 闭麦
        return "clubhouse_bimai"
    end
end

function LuaClubHouseMgr:GetSpeakingSprite(member)
    if self:IsSpeaking(member) then
        return "clubhouse_kaimai"
    else
        return "clubhouse_bimai"
    end
end

function LuaClubHouseMgr:GetInverseHandsUpSprite(member)
    if member.Status[EnumCHStatusType.HandsUp] and not LuaClubHouseMgr:IsRoomForbiddenHandsUp() then
        return "clubhouse_quxiaojushou"
    else
        return "clubhouse_jushou"
    end
end

--C#下有用到
function LuaClubHouseMgr:IsInRoom()
    return self.m_CurRoomInfo and self.m_CurRoomInfo.RoomId~=nil
end

function LuaClubHouseMgr:IsMyGame(appName)
    return appName == self.m_CurAppName
end
--开关：是否允许跨APP访问房间
function LuaClubHouseMgr:IsCrossAppRoomEnabled()
    return false
end

function LuaClubHouseMgr:IsCCRoom()
    return self.m_CurRoomInfo and self.m_CurRoomInfo.Platform == EnumClubHousePlatformType.cc
end

function LuaClubHouseMgr:CheckAndConfirmCCRoom()
    --根据浩神和大家达成的规则，在CC的房间但是没有在语音流中，提示一下, 不做强制要求
    if Time.realtimeSinceStartup - self.m_LastCheckAndConfirmCCRoomTime < 0.5 then return end
    self.m_LastCheckAndConfirmCCRoomTime = Time.realtimeSinceStartup
    if LuaClubHouseMgr:IsCCRoom() and not CCMiniAPIMgr.Inst:IsInClubHouseStream() then
        self:TryReEnterRoomForCC()
    end
end

function LuaClubHouseMgr:GetAppDisplayName(appName)
    if appName == EnumCHAPPType.qnmobile then
        return LocalString.GetString("倩女")
    elseif appName == EnumCHAPPType.tymobile then
        return LocalString.GetString("天谕")
    end
    return appName
end

function LuaClubHouseMgr:CheckIfInCCMiniCapturingForCreateRoom()
    if CCMiniAPIMgr.Inst.IsCCMiniEnabled and CCMiniAPIMgr.Inst:IsCapturing() and not CCMiniAPIMgr.Inst:IsInClubHouseStream() then
        g_MessageMgr:ShowMessage("Voice_Mute_Can_Not_Create_Club_House_When_In_CC_Capture")
        return true
    else
        return false
    end
end

function LuaClubHouseMgr:CheckIfInCCMiniCapturing()
    if CCMiniAPIMgr.Inst.IsCCMiniEnabled and CCMiniAPIMgr.Inst:IsCapturing() and not CCMiniAPIMgr.Inst:IsInClubHouseStream() then
        g_MessageMgr:ShowMessage("Voice_Mute_Can_Not_Enter_Club_House_When_In_CC_Capture")
        return true
    else
        return false
    end
end

function LuaClubHouseMgr:GetRoomId()
    return self.m_CurRoomInfo and self.m_CurRoomInfo.RoomId or 0
end

function LuaClubHouseMgr:GetRoomName()
    return self.m_CurRoomInfo and self.m_CurRoomInfo.RoomName or ""
end

function LuaClubHouseMgr:GetRoomOwner()
    return self.m_CurRoomInfo and self.m_CurRoomInfo.Owner or nil
end

function LuaClubHouseMgr:GetRoomHeat()
    return self.m_CurRoomInfo and self.m_CurRoomInfo.Heat or nil
end

function LuaClubHouseMgr:GetRoomTag()
    return self.m_CurRoomInfo and self.m_CurRoomInfo.Tag or nil
end

function LuaClubHouseMgr:GetRoomTitle()
    return self.m_CurRoomInfo and self.m_CurRoomInfo.Title or nil
end

function LuaClubHouseMgr:IsRoomForbiddenHandsUp()
    return self.m_CurRoomInfo and self.m_CurRoomInfo.IsForbidHandsUp or false
end

function LuaClubHouseMgr:GetBroadcasters()
    local tbl = {}
    if self.m_CurRoomInfo and self.m_CurRoomInfo.MicTbl then
        table.insert(tbl, self.m_CurRoomInfo.Owner)
        for __,accid in pairs(self.m_CurRoomInfo.MicList) do
            table.insert(tbl, self.m_CurRoomInfo.Members[accid])
        end
    end
    return tbl
end

function LuaClubHouseMgr:GetAudiences()
    local tbl = {}
    if self.m_CurRoomInfo and self.m_CurRoomInfo.ListenerTbl then
        for __,accid in pairs(self.m_CurRoomInfo.ListenerList) do
            local member = self.m_CurRoomInfo.Members[accid]
            if member.IsMe then
                table.insert(tbl, 1, member) --我放在听众第一位
            else
                table.insert(tbl, member)
            end
        end
    end
    return tbl
end

function LuaClubHouseMgr:GetAudienceDisplayCount()
    if self.m_CurRoomInfo and self.m_CurRoomInfo.ListenerCount then
        return self.m_CurRoomInfo.ListenerCount
    else
        return 0
    end
end

function LuaClubHouseMgr:GetTotalMembersDisplayCount()
    if self.m_CurRoomInfo and self.m_CurRoomInfo.MicCount then
        return self.m_CurRoomInfo.MicCount + self.m_CurRoomInfo.ListenerCount
    else
        return 0
    end
end

function LuaClubHouseMgr:GetMyselfInfo()
    if self.m_CurRoomInfo and self.m_CurRoomInfo.Owner then
        if self.m_CurRoomInfo.Owner.IsMe then
            return self.m_CurRoomInfo.Owner
        else
            return self.m_CurRoomInfo.Members[self.m_CurRoomInfo.MyAccId]
        end
    end
    return nil
end

function LuaClubHouseMgr:GetMember(accid)
    if self.m_CurRoomInfo and self.m_CurRoomInfo.Owner then
        if self.m_CurRoomInfo.Owner.AccId == accid then
            return self.m_CurRoomInfo.Owner
        else
            return self.m_CurRoomInfo.Members[accid]
        end
    else
        return nil
    end
end

function LuaClubHouseMgr:GetAccIdByPlayerId(playerId)
    return playerId * 10 + 1
end

--当前是否开麦
function LuaClubHouseMgr:IsMyselfSpeaking()
    local myself = self:GetMyselfInfo()
    return self:IsSpeaking(myself)
end
--当前是否被禁言
function LuaClubHouseMgr:IsIsMyselfForbidden()
    local myself = self:GetMyselfInfo()
    return myself.Status[EnumCHStatusType.Forbid] and true or false
end
--当前是否正在申请上麦
function LuaClubHouseMgr:IsMyselfHandsUp()
    local myself = self:GetMyselfInfo()
    return myself.Status[EnumCHStatusType.HandsUp] and true or false
end

function LuaClubHouseMgr:IsBroadcaster(member)
    return member.Character == EnumCHCharacterType.Owner or member.Character == EnumCHCharacterType.Speaker
end

function LuaClubHouseMgr:IsAudience(member)
    return member.Character == EnumCHCharacterType.Listener
end

function LuaClubHouseMgr:IsForbidden(member)
    return member and member.Status[EnumCHStatusType.Forbid] and true or false
end

function LuaClubHouseMgr:IsSpeaking(member)
    if member and member.Status[EnumCHStatusType.Speaking] then
        if member.IsMe and self.m_LocalMuted then --这里加一个本地保底的处理
            return false
        else
            return true
        end
    else
        return false
    end
end

function LuaClubHouseMgr:GetHandsUpCount()
    local count = 0
    if self.m_CurRoomInfo and self.m_CurRoomInfo.Members then  
        for accid,member in pairs(self.m_CurRoomInfo.Members) do
           if member.Status[EnumCHStatusType.HandsUp] then
                count = count + 1
           end
        end
    end
    return count
end

function LuaClubHouseMgr:DoLeaveRoom()
    if self:IsCCRoom() then
        self.m_CurRoomInfo = {}
        return
    end
    self.m_CurRoomInfo = {}
    self:LeaveChannel()
    if CommonDefs.IsIOSPlatform() then
        --hack一下，参照 CUIManager OnKeyboardHide
        local FMOD_StudioSystem = import "FMOD_StudioSystem"
        FMOD_StudioSystem.instance:StopOrResumeFmod(true)
        FMOD_StudioSystem.instance:StopOrResumeFmod(false)
    end
end

function LuaClubHouseMgr:AddListener()
    if self.m_ListenerAdded then return end
    self.m_ListenerAdded = true
    g_ScriptEvent:AddListener("StartLoadScene", self, "StartLoadScene")
end

function LuaClubHouseMgr:StartLoadScene(args)
    local levelName = args[0]
    --退出到登录界面时，从房间状态中离开
    if levelName == Setting.sDengLu_Level and self:IsInRoom() then
        self:DoLeaveRoom()
    end
end
------------------------
-- NERtc 调用
------------------------
function LuaClubHouseMgr:SetupRTCEngine(appKey, isBoradcaster)
    if self:IsCCRoom() then return end
    local ret = ChatRoomSDK.Inst:NERTC_Init(appKey, 6) --loglevel这里固定填成6 后续看情况调整
    self:Log("NERTC_Init", ret)
    ChatRoomSDK.Inst:NERTC_SetAudioProfile(2,2) --暂时写死
    ChatRoomSDK.Inst:NERTC_SetClientRole(isBoradcaster and 0 or 1) --设置主播或观众角色
end

function LuaClubHouseMgr:JoinChannel(token, channelName, uid)
    if self:IsCCRoom() then return end
    ChatRoomSDK.Inst:NERTC_JoinChannel(token, channelName, uid)
    self:SetCCAutoDealHome(false)
end

function LuaClubHouseMgr:LeaveChannel()
    if self:IsCCRoom() then return end
    ChatRoomSDK.Inst:NERTC_LeaveChannel()
    self:SetCCAutoDealHome(true)
end

function LuaClubHouseMgr:MuteLocalAudio(muted)
    if self:IsCCRoom() then return end
    ChatRoomSDK.Inst:NERTC_MuteLocalAudioStream(muted)
end

function LuaClubHouseMgr:MuteLocalDevice(muted)
    if self:IsCCRoom() then return end
    ChatRoomSDK.Inst:NERTC_SetRecordDeviceMute(muted)
    ChatRoomSDK.Inst:NERTC_SetPlayoutDeviceMute(muted)
end

function LuaClubHouseMgr:SetClientRole(isBoradcaster)
    if self:IsCCRoom() then return end
    ChatRoomSDK.Inst:NERTC_SetClientRole(isBoradcaster and 0 or 1)
end

function LuaClubHouseMgr:EnableAudioVolumeIndication(enabled)
    if self:IsCCRoom() then return end
    ChatRoomSDK.Inst:NERTC_EnableAudioVolumeIndication(enabled, 1000)--开启音量提示(PC端在JoinChannel前调用测试无效,调用阶段改为Join成功之后)
end

function LuaClubHouseMgr:OnChatRoomJoinChannel(result, channelId, elapsed)
    if self:IsCCRoom() then return end
    self:EnableAudioVolumeIndication(true)
    if self:IsInRoom() then -- 如果进入房间瞬间退出，可能会导致房间数据消失但是OnChatRoomJoinChannel只会才回来，这里进行检查
        self:MuteLocalAudio(self:IsIsMyselfForbidden() or not self:IsMyselfSpeaking())
    end
end

function LuaClubHouseMgr:OnChatRoomLeaveChannel(result)
    if self:IsCCRoom() then return end
    self:EnableAudioVolumeIndication(false) --离开频道后还在接收消息，加个保底处理
end

function LuaClubHouseMgr:OnChatRoomReJoinChannel(result, channelId)

end

function LuaClubHouseMgr:OnChatRoomDisconnect(reason)

end

function LuaClubHouseMgr:OnChatRoomRemoteAudioVolumeIndication(speakers, totalVolume)
    local volumeTbl = {}
    CommonDefs.DictIterate(speakers, DelegateFactory.Action_object_object(function(key,val)
        local volume = tonumber(val)
        if volume>0 then
            volumeTbl[tonumber(key)] = volume
        end
    end))
    g_ScriptEvent:BroadcastInLua("ClubHouse_OnRemoteAudioVolumeIndication", volumeTbl)
end

function LuaClubHouseMgr:OnChatRoomLocalAudioVolumeIndication(volume)
    g_ScriptEvent:BroadcastInLua("ClubHouse_OnLocalAudioVolumeIndication", volume)
end

function LuaClubHouseMgr:OnChatRoomError(code)

end

function LuaClubHouseMgr:SetCCAutoDealHome(enabled)
    CCMiniAPIMgr.Inst:SetAutoDealHome(enabled)
end

function LuaClubHouseMgr:HasRecordPermission()
    if CommonDefs.IsAndroidPlatform() or CommonDefs.IsIOSPlatform() then
        if NativeTools.IsSupportRequestPermission() then
            return NativeTools.HasUserAuthorizedPermission(NativeTools.PermissionNames.RECORD_AUDIO)
        end
    end
    return true
end

function LuaClubHouseMgr:CheckAndRequestRecordPermission(callback)
    if CommonDefs.IsAndroidPlatform() or CommonDefs.IsIOSPlatform() then
        if NativeTools.IsSupportRequestPermission() then
            if not NativeTools.HasUserAuthorizedPermission(NativeTools.PermissionNames.RECORD_AUDIO) then
                NativeTools.RequestUserPermissionOrPromptIfDenied(NativeTools.PermissionNames.RECORD_AUDIO, 
                    DelegateFactory.Action_string_int(function(permissionName, result)
                        if result == 0 and callback then
                            callback()
                        end
                    end))
                return
            end
        end
    end
    if callback then
        callback()
    end
end
------------------------
-- 处理后台相关问题
------------------------
function LuaClubHouseMgr:OnApplicationFocus(args)
    if CommonDefs.IsIOSPlatform() and CChatMgr.Inst:IsChatRoomEnabled() then
        local focusStatus = args[0]
        if focusStatus then --ios端切后台或失去焦点的处理和CC有冲突，需要特殊处理一下
            if self:IsInRoom() and self:IsMyselfSpeaking() and not self:IsCCRoom() then
                self:StartRecoverIOSAudioSessionTick()
            end
        end
        if self:IsInRoom() and not self:IsCCRoom() then
            self:MuteLocalDevice(not focusStatus)
        end
    end
end

function LuaClubHouseMgr:StartRecoverIOSAudioSessionTick()
    self:StopRecoverIOSAudioSessionTick()
    self.m_RecoverIOSAudioSessionTick = RegisterTickOnce(function()
        if self:IsInRoom() and self:IsMyselfSpeaking() then
            if AudioSvcCsInterface.GetIOSAudioSessionCategory()~=EnumIOSAVAudioSessionCategory.PlayAndRecord then
                AudioSvcCsInterface.SetIOSAudioSessionCategory(true)
            end
        end
    end, 2000) --延迟2s来纠正audiosession，太早纠正会被cc给覆盖
end

function LuaClubHouseMgr:StopRecoverIOSAudioSessionTick()
    if self.m_RecoverIOSAudioSessionTick then
        UnRegisterTick(self.m_RecoverIOSAudioSessionTick)
        self.m_RecoverIOSAudioSessionTick = nil
    end
end

function LuaClubHouseMgr:OnApplicationPause(args)
    if CommonDefs.IsAndroidPlatform() and CChatMgr.Inst:IsChatRoomEnabled() then
        local pauseStatus = args[0]
        if self:IsInRoom() and not self:IsCCRoom() then
            self:MuteLocalDevice(pauseStatus)
        end
    end
end
------------------------
-- 一些便利方法
------------------------
function LuaClubHouseMgr:Log(...)
    --调试入口，在测试完毕后直接注销这里即可

end

function LuaClubHouseMgr:IsInWinSocialWnd()
    if CommonDefs.IsPCGameMode() then
        local CWinSocialWndMgr = import "L10.UI.CWinSocialWndMgr"
        return CWinSocialWndMgr.Inst.m_IsWinSocialWndOpened
    else
        return false
    end
end

function LuaClubHouseMgr:GetNumberValue(dict, key)
    return tonumber(CommonDefs.DictGetValue_LuaCall(dict, key))
end

function LuaClubHouseMgr:GetStringValue(dict, key)
    return CommonDefs.DecodeUnicode(tostring(CommonDefs.DictGetValue_LuaCall(dict, key)))
end

function LuaClubHouseMgr:GetRoomTypeName(roomType)
    if roomType == "everyone" then
        return LocalString.GetString("开放")
    elseif roomType == "apponly" then
        return LocalString.GetString("倩女")
    end
    return ""
end

function LuaClubHouseMgr:GetPortriatByExtraInfo(appName, extraInfoDict)
    if appName == "qnmobile" then
        return CUICommonDef.GetPortraitName(self:GetNumberValue(extraInfoDict, "Class"), self:GetNumberValue(extraInfoDict, "Gender"), -1)
    else
        return "hnpc_a" --其他游戏统一用这个图标
    end
end

function LuaClubHouseMgr:GetMemberInfoFromJsonDict(jsonDict)
    local member = {}
    member.AccId = self:GetNumberValue(jsonDict, "AccId")
    member.PlayerId = self:GetNumberValue(jsonDict, "PlayerId")
    member.PlayerName = self:GetStringValue(jsonDict, "PlayerName")
    member.Level = self:GetNumberValue(jsonDict, "Level")
    member.Character = self:GetStringValue(jsonDict, "Character")
    member.RoomId = self:GetNumberValue(jsonDict, "RoomId")
    member.AppName = self:GetStringValue(jsonDict, "AppName")
    local statusDict = CommonDefs.DictGetValue_LuaCall(jsonDict, "Status")
    member.Status = {}
    if TypeIs(statusDict, typeof(self.m_GenericStringObjectDict)) then 
        CommonDefs.DictIterate(statusDict, DelegateFactory.Action_object_object(function(key,val)
            member.Status[key] = true
        end))
    end
    local extraInfoDict = CommonDefs.DictGetValue_LuaCall(jsonDict, "ExtraInfo")
    member.Portrait = self:GetPortriatByExtraInfo(member.AppName, extraInfoDict)
    member.Server = self:GetStringValue(extraInfoDict, "Server")
    if member.AppName == self.m_CurAppName then --本游戏玩家
        member.IsInSameApp = true
        member.IsMe = CClientMainPlayer.Inst and member.PlayerId == CClientMainPlayer.Inst.Id or false
        member.QnLevel = self:GetNumberValue(extraInfoDict, "CurLv")
        member.QnFeiSheng = CommonDefs.DictGetValue_LuaCall(extraInfoDict, "FeiSheng") and true or false --bool
    else
        member.IsInSameApp = false
        member.IsMe = false
    end
    return member
end

function LuaClubHouseMgr:GetEnterRoomInfoFromJsonDict(jsonDict)
    self.m_RoomMessageDataList = {}     -- 清空聊天记录

    local enterRoomInfo = {}
    local token = tostring(CommonDefs.DictGetValue_LuaCall(jsonDict, "Token"))
    local appKey = tostring(CommonDefs.DictGetValue_LuaCall(jsonDict, "AppKey"))

    local roomDict = CommonDefs.DictGetValue_LuaCall(jsonDict, "Room")
    
    enterRoomInfo.RoomId = self:GetNumberValue(roomDict, "RoomId")
    enterRoomInfo.RoomType = self:GetStringValue(roomDict, "RoomType")
    enterRoomInfo.RoomName = self:GetStringValue(roomDict, "RoomName")
    enterRoomInfo.AppName = self:GetStringValue(roomDict, "AppName")
    enterRoomInfo.IsForbidHandsUp = CommonDefs.DictGetValue_LuaCall(roomDict, "IsForbidHandsUp") -- bool value
    enterRoomInfo.MicCount = self:GetNumberValue(roomDict, "MicCount") --根据浩神的设定这里包括了Owner， MicCount和ListenerCount仅仅用于显示之用，并不一定等于真是客户端的member数量
    enterRoomInfo.ListenerCount = self:GetNumberValue(roomDict, "ListenerCount")
    enterRoomInfo.Heat = self:GetNumberValue(roomDict, "Heat")
    enterRoomInfo.Tag = self:GetNumberValue(roomDict, "Tag")
    enterRoomInfo.Title = self:GetNumberValue(roomDict, "Title")
    local ownerDict = CommonDefs.DictGetValue_LuaCall(roomDict, "Owner")
    enterRoomInfo.Owner = self:GetMemberInfoFromJsonDict(ownerDict)
    enterRoomInfo.MyAccId = 0 --我的唯一标识
    if enterRoomInfo.Owner.IsMe then
        enterRoomInfo.MyAccId = enterRoomInfo.Owner.AccId
    end
    enterRoomInfo.Platform = self:GetNumberValue(roomDict, "Platform")

    enterRoomInfo.MicTbl = {}
    enterRoomInfo.MicList = {} -- 主要是为了维持麦序
    enterRoomInfo.ListenerTbl = {}
    enterRoomInfo.ListenerList = {} -- 主要是为了维持观众排序
    enterRoomInfo.Members = {}

    local micList = CommonDefs.DictGetValue_LuaCall(roomDict, "MicList")
    for i=0,micList.Count-1 do
        local member = self:GetMemberInfoFromJsonDict(micList[i])
        enterRoomInfo.MicTbl[member.AccId] = true
        table.insert(enterRoomInfo.MicList, member.AccId)
        enterRoomInfo.Members[member.AccId] = member
        if member.IsMe then
            enterRoomInfo.MyAccId = member.AccId
        end
    end

    local listenerList = CommonDefs.DictGetValue_LuaCall(roomDict, "ListenerList")
    for i=0,listenerList.Count-1 do
        local member = self:GetMemberInfoFromJsonDict(listenerList[i])
        enterRoomInfo.ListenerTbl[member.AccId] = true
        table.insert(enterRoomInfo.ListenerList, member.AccId)
        enterRoomInfo.Members[member.AccId] = member
        if member.IsMe then
            enterRoomInfo.MyAccId = member.AccId
        end
    end

    local presentInfo = CommonDefs.DictGetValue_LuaCall(roomDict, "PresentInfo")
    enterRoomInfo.PresentRankTbl = self:InitRoomPresentRankTbl(presentInfo)

    return token, appKey, enterRoomInfo
end

function LuaClubHouseMgr:RemoveValueFromTableList(tbl, keyVal)
    local tmp = {}
    for __,val in pairs(tbl) do
        if val~=keyVal then
            table.insert(tmp, val)
        end
    end
    return tmp
end

function LuaClubHouseMgr:AddOrUpdateMember(member)
    if self.m_CurRoomInfo and member and  member.RoomId == self.m_CurRoomInfo.RoomId then
        if self.m_CurRoomInfo.Owner.AccId == member.AccId then
            self.m_CurRoomInfo.Owner = member
        else
            if member.Character==EnumCHCharacterType.Listener then
                if self.m_CurRoomInfo.MicTbl[member.AccId] then
                    self.m_CurRoomInfo.MicTbl[member.AccId] = nil
                    self.m_CurRoomInfo.MicList = self:RemoveValueFromTableList(self.m_CurRoomInfo.MicList, member.AccId)
                end
                if not self.m_CurRoomInfo.ListenerTbl[member.AccId] then
                    self.m_CurRoomInfo.ListenerTbl[member.AccId] = true
                    table.insert(self.m_CurRoomInfo.ListenerList, member.AccId)
                end
            elseif member.Character==EnumCHCharacterType.Speaker then
                if self.m_CurRoomInfo.ListenerTbl[member.AccId] then
                    self.m_CurRoomInfo.ListenerTbl[member.AccId] = nil
                    self.m_CurRoomInfo.ListenerList = self:RemoveValueFromTableList(self.m_CurRoomInfo.ListenerList, member.AccId)
                end
                if not self.m_CurRoomInfo.MicTbl[member.AccId] then
                    self.m_CurRoomInfo.MicTbl[member.AccId] = true
                    table.insert(self.m_CurRoomInfo.MicList, member.AccId)
                end
            end
            self.m_CurRoomInfo.Members[member.AccId] = member
        end
        return true
    end
    return false
end
--根据规则，房主只能在主播中产生，会涉及Character变更
function LuaClubHouseMgr:UpdateOwnerInfo(newOwner, oldOwner)
    if self.m_CurRoomInfo and newOwner and oldOwner and newOwner.RoomId == self.m_CurRoomInfo.RoomId then
        self.m_CurRoomInfo.Owner = newOwner
        if self.m_CurRoomInfo.MicTbl[newOwner.AccId] then
            self.m_CurRoomInfo.MicTbl[newOwner.AccId] = nil
            self.m_CurRoomInfo.MicList = self:RemoveValueFromTableList(self.m_CurRoomInfo.MicList, newOwner.AccId)
        end
        if self.m_CurRoomInfo.ListenerTbl[newOwner.AccId] then
            self.m_CurRoomInfo.ListenerTbl[newOwner.AccId] = nil
            self.m_CurRoomInfo.ListenerList = self:RemoveValueFromTableList(self.m_CurRoomInfo.ListenerList, newOwner.AccId)
        end

        if oldOwner.Character==EnumCHCharacterType.Listener then
            if self.m_CurRoomInfo.MicTbl[oldOwner.AccId] then
                self.m_CurRoomInfo.MicTbl[oldOwner.AccId] = nil
                self.m_CurRoomInfo.MicList = self:RemoveValueFromTableList(self.m_CurRoomInfo.MicList, oldOwner.AccId)
            end
            if not self.m_CurRoomInfo.ListenerTbl[oldOwner.AccId] then
                self.m_CurRoomInfo.ListenerTbl[oldOwner.AccId] = true
                table.insert(self.m_CurRoomInfo.ListenerList, oldOwner.AccId)
            end
        elseif oldOwner.Character==EnumCHCharacterType.Speaker then
            if self.m_CurRoomInfo.ListenerTbl[oldOwner.AccId] then
                self.m_CurRoomInfo.ListenerTbl[oldOwner.AccId] = nil
                self.m_CurRoomInfo.ListenerList = self:RemoveValueFromTableList(self.m_CurRoomInfo.ListenerList, oldOwner.AccId)
            end
            if not self.m_CurRoomInfo.MicTbl[oldOwner.AccId] then
                self.m_CurRoomInfo.MicTbl[oldOwner.AccId] = true
                table.insert(self.m_CurRoomInfo.MicList, oldOwner.AccId)
            end
        end
        self.m_CurRoomInfo.Members[oldOwner.AccId] = oldOwner
    end
end

------------------------
-- Gac2Gas RPC Handle
------------------------
--查询是否存在已创建房间
function LuaClubHouseMgr:QueryMyRoom()
    if not CChatMgr.Inst:IsChatRoomEnabled() then
        g_MessageMgr:ShowMessage("FUNCTION_NOT_OPEN")
        return
    end
    Gac2Gas.ClubHouse_QueryMyRoomId("createroom")
end
--创建房间
function LuaClubHouseMgr:CreateRoom(roomName, roomType, roomTag, password)
    if not CChatMgr.Inst:IsChatRoomEnabled() then
        g_MessageMgr:ShowMessage("FUNCTION_NOT_OPEN")
        return
    end
    Gac2Gas.ClubHouse_CreateRoom(roomName, roomType, roomTag, password)
end
--进入房间
LuaClubHouseMgr.m_QueryEnterRoomId = nil
function LuaClubHouseMgr:EnterRoom(roomId, roomName, roomPassword, needOpenWnd, formLink)
    if not CChatMgr.Inst:IsChatRoomEnabled() then
        g_MessageMgr:ShowMessage("FUNCTION_NOT_OPEN")
        return
    end
    if self:CheckIfInCCMiniCapturing() then
        return
    end
    if needOpenWnd then
        CSocialWndMgr.ShowChatRoom()
    end

    LuaClubHouseMgr:CheckAndRequestRecordPermission(function()
        if self:IsInRoom() and self.m_CurRoomInfo.RoomId~=roomId and self:IsBroadcaster(self:GetMyselfInfo()) then
            local msg = g_MessageMgr:FormatMessage("CLUBHOUSE_BROADCASTER_ENTER_OTHER_ROOM_CONFIRM", roomName)
            MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
                LuaClubHouseMgr:QueryEnterRoom(roomId, roomPassword, formLink)
            end),nil,nil,nil,false)
        else 
            LuaClubHouseMgr:QueryEnterRoom(roomId, roomPassword, formLink)
        end
    end)
end

function LuaClubHouseMgr:QueryEnterRoom(roomId, password, fromLink)
    if password then
        Gac2Gas.ClubHouse_EnterRoom(roomId, password, fromLink or false)
    else
        self.m_QueryEnterRoomId = roomId
        CUIManager.ShowUI(CLuaUIResources.CHPrivatePasswordCheckWnd)
    end
end

--离开房间
function LuaClubHouseMgr:LeaveRoom()
    --房主离开需要做选择
    if self.m_CurRoomInfo.Owner and self.m_CurRoomInfo.Owner.IsMe then
        local broadcasters = self:GetBroadcasters()
        if #broadcasters>1 then --有其他主播
            local msg = g_MessageMgr:FormatMessage("CLUBHOUSE_LEAVE_ROOM_AND_CHANGE_OWNER_CONFIRM")
            MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
                Gac2Gas.ClubHouse_LeaveRoom(false)
            end),DelegateFactory.Action(function()
                Gac2Gas.ClubHouse_LeaveRoom(true)
            end),LocalString.GetString("转移并离开"),LocalString.GetString("直接解散"),true)
        else

            local msg = g_MessageMgr:FormatMessage("CLUBHOUSE_DISMISS_ROOM_CONFIRM")
            MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
                Gac2Gas.ClubHouse_LeaveRoom(true)
            end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
        end
    elseif self:IsBroadcaster(self:GetMyselfInfo()) then
        local msg = g_MessageMgr:FormatMessage("CLUBHOUSE_BROADCASTER_LEAVE_ROOM_CONFIRM")
        MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
            Gac2Gas.ClubHouse_LeaveRoom(false)
        end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
    else
        Gac2Gas.ClubHouse_LeaveRoom(false)
    end
end
--查询房间列表
function LuaClubHouseMgr:QueryRoomList(roomId, queryType, sortType, pageIndex, context)
    self:Log("LuaClubHouseMgr:QueryRoomList", roomId, queryType, sortType, pageIndex)
    Gac2Gas.ClubHouse_QueryRoomList(roomId or 0, queryType or "", sortType or "", pageIndex or 1, context)
end
--查询所有听众
function LuaClubHouseMgr:QueryAllListenersInRoom()
    Gac2Gas.ClubHouse_QueryAllListenersInRoom()
end
--申请/取消申请 上麦
function LuaClubHouseMgr:ChangeHandsUpStatus(newValue)
    Gac2Gas.ClubHouse_ChangeStatus(0, EnumCHStatusType.HandsUp, newValue)
end

--关闭/打开 本地麦克风
function LuaClubHouseMgr:ChangeSpeakingStatus(newValue)
    Gac2Gas.ClubHouse_ChangeStatus(0, EnumCHStatusType.Speaking, newValue)
end

--禁止/取消禁止 他人发言
function LuaClubHouseMgr:SetForbidStatus(accid, forbidden)
    Gac2Gas.ClubHouse_ChangeStatus(accid, EnumCHStatusType.Forbid, forbidden)
end
--处理上麦请求：同意或拒绝
function LuaClubHouseMgr:DealWithApplySpeaking(accid, approved)
    if approved then
        Gac2Gas.ClubHouse_SetMic(accid)
    else
        Gac2Gas.ClubHouse_ChangeStatus(accid, EnumCHStatusType.HandsUp, false)
    end
end
--清空所有的申请上麦状态
function LuaClubHouseMgr:ClearAllHandsUp()
    Gac2Gas.ClubHouse_ClearStatusAll(EnumCHStatusType.HandsUp)
end
--开启或关闭申请上麦功能
function LuaClubHouseMgr:ChangeForbidHandsUpStatus()
    Gac2Gas.ClubHouse_ForbidStatusAll(EnumCHStatusType.HandsUp, not self:IsRoomForbiddenHandsUp())
end
--下麦
function LuaClubHouseMgr:LeaveMic()
    local msg = g_MessageMgr:FormatMessage("CLUBHOUSE_LEAVE_MIC_CONFIRM")
    MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
        local selfInfo = self:GetMyselfInfo()
        if selfInfo then
            Gac2Gas.ClubHouse_UnsetMic(selfInfo.AccId)
        end
    end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
end
--邀请上麦或踢下麦位
function LuaClubHouseMgr:SetMicEnabled(accid, enabled)
    if enabled then
        Gac2Gas.ClubHouse_SetMic(accid)
    else
        Gac2Gas.ClubHouse_UnsetMic(accid)
    end
end
--请离房间
function LuaClubHouseMgr:KickoutRoom(accid)
    Gac2Gas.ClubHouse_Kick(accid)
end

--查询申请上麦列表
function LuaClubHouseMgr:QueryApplySpeakList()
    Gac2Gas.ClubHouse_QueryMemberByStatus(EnumCHStatusType.HandsUp,"applylist")
end
--变更房主
function LuaClubHouseMgr:ChangeOwner(accid)
    Gac2Gas.ClubHouse_ChangeOwner(accid)
end
--加入CC房间语音流
function LuaClubHouseMgr:TryReEnterRoomForCC()
    Gac2Gas.ClubHouse_TryReEnterRoom()
end
------------------------
-- Gas2Gac RPC Handle
------------------------

function LuaClubHouseMgr:ClubHouse_QueryRoom_Result(room_U)
    self:Log("ClubHouse_QueryRoom_Result", room_U)
end

function LuaClubHouseMgr:ClubHouse_QueryRoomList_Result(rooms_U, Page, context)
    self:Log("LuaClubHouseMgr:ClubHouse_QueryRoomList_Result", rooms_U, Page)
    local roomDataList = MsgPackImpl.unpack(rooms_U)
    local rooms = {}
    if roomDataList and roomDataList.Count>0 then
        for i=0, roomDataList.Count-1 do
            local roomData = roomDataList[i]
            local roomInfo = {}
            roomInfo.RoomId = self:GetNumberValue(roomData, "RoomId")
            roomInfo.RoomName = self:GetStringValue(roomData, "RoomName")
            roomInfo.RoomType = self:GetStringValue(roomData, "RoomType")
            roomInfo.MicCount = self:GetNumberValue(roomData, "MicCount")
            roomInfo.ListenerCount = self:GetNumberValue(roomData, "ListenerCount")
            roomInfo.AppName = self:GetStringValue(roomData, "AppName")
            local ownerDict = CommonDefs.DictGetValue_LuaCall(roomData, "Owner")
            roomInfo.Owner = self:GetMemberInfoFromJsonDict(ownerDict)
            roomInfo.Heat = self:GetNumberValue(roomData, "Heat")
            roomInfo.Tag = self:GetNumberValue(roomData, "Tag")
            roomInfo.Title = self:GetNumberValue(roomData, "Title")
            roomInfo.HasSpeaking = CommonDefs.DictGetValue_LuaCall(roomData, "HasSpeaking")
            table.insert(rooms, roomInfo)
        end
    end
    g_ScriptEvent:BroadcastInLua("ClubHouse_QueryRoomList_Result", rooms, Page, context)
end

function LuaClubHouseMgr:ClubHouse_CreateRoom_Result(playerId, bSuccess, data_U)
    self:Log("ClubHouse_CreateRoom_Result", playerId, bSuccess, data_U)
    if self:CheckIfInCCMiniCapturing() then
        return
    end
    if not CChatMgr.Inst:IsChatRoomEnabled() then
        return
    end
    if not self:HasRecordPermission() then
        return
    end
    if bSuccess then
        local dataDict = MsgPackImpl.unpack(data_U)
        local token = nil
        local appKey = nil
        token, appKey, self.m_CurRoomInfo = self:GetEnterRoomInfoFromJsonDict(dataDict)

        if not self:IsCCRoom() then
            self:SetupRTCEngine(appKey, true)
            self:JoinChannel(token, tostring(self.m_CurRoomInfo.RoomId), self.m_CurRoomInfo.MyAccId)
        end
    
        self:AddListener()
        g_ScriptEvent:BroadcastInLua("ClubHouse_CreateRoom_Result", playerId, bSuccess)
    end
    
end

function LuaClubHouseMgr:ClubHouse_LeaveRoom_Result(playerId, bSuccess, data_U)
    self:Log("ClubHouse_LeaveRoom_Result", playerId, bSuccess, data_U)
    --暂时不用处理，离开成功会有Notify_LeaveRoom
end

function LuaClubHouseMgr:ClubHouse_EnterRoom_Result(playerId, bSuccess, data_U, Reason)
    self:Log("ClubHouse_EnterRoom_Result", playerId, bSuccess, data_U, Reason)
    if not CChatMgr.Inst:IsChatRoomEnabled() then
        return
    end
    if not self:HasRecordPermission() then
        return
    end
    local dataDict = MsgPackImpl.unpack(data_U)
    if bSuccess then
        local token = nil
        local appKey = nil
        token, appKey, self.m_CurRoomInfo = self:GetEnterRoomInfoFromJsonDict(dataDict)
        local myself = self:GetMyselfInfo()

        if not self:IsCCRoom() then
            self:SetupRTCEngine(appKey, self:IsBroadcaster(myself))
            self:JoinChannel(token, tostring(self.m_CurRoomInfo.RoomId), myself.AccId)
        end

        self:AddListener()
        local isClientRequest = "ClientRequest" == Reason or "FromLink" == Reason

        local data = g_MessagePack.unpack(data_U)
        self:InitMsgCache(data and data.MsgCache)

        if CUIManager.IsLoaded(CLuaUIResources.CHPrivatePasswordCheckWnd) then
            CUIManager.CloseUI(CLuaUIResources.CHPrivatePasswordCheckWnd)
        end

        g_ScriptEvent:BroadcastInLua("ClubHouse_EnterRoom_Result", playerId, bSuccess, isClientRequest)
    else
        local msg = self:GetStringValue(dataDict, "msg")
        local roomId = self:GetNumberValue(dataDict, "RoomId")
        if msg == "ENTER_ROOM_LINK_NO_PASSWORD" then
            self.m_QueryEnterRoomId = roomId
            CUIManager.ShowUI(CLuaUIResources.CHPrivatePasswordCheckWnd)
        end
    end
end

function LuaClubHouseMgr:ClubHouse_SetMic_Result(playerId, bSuccess, data_U)
    self:Log("ClubHouse_SetMic_Result", playerId, bSuccess, data_U)
    --暂时不用处理，直接在ClubHouse_Notify_SetMic中进行处理
end

function LuaClubHouseMgr:ClubHouse_UnsetMic_Result(playerId, bSuccess, data_U)
    self:Log("ClubHouse_UnsetMic_Result", playerId, bSuccess, data_U)
    --暂时不用处理，直接在ClubHouse_Notify_UnsetMic中进行处理
end

function LuaClubHouseMgr:ClubHouse_Kick_Result(playerId, bSuccess, data_U)
    self:Log("ClubHouse_Kick_Result", playerId, bSuccess, data_U)
    --暂时不用处理，踢人成功会有Notify_LeaveRoom
end

function LuaClubHouseMgr:ClubHouse_DestroyRoom_Result(playerId, bSuccess, data_U)
    self:Log("ClubHouse_DestroyRoom_Result", playerId, bSuccess, data_U)
    --暂时不处理，会有Notify_DelRoom
end

function LuaClubHouseMgr:ClubHouse_ChangeStatus_Result(playerId, bSuccess, data_U)
    self:Log("ClubHouse_ChangeStatus_Result", playerId, bSuccess, data_U)
    --[[if self.m_CurRoomInfo and self.m_CurRoomInfo.Members then
        local data = MsgPackImpl.unpack(data_U)
        local Status = self:GetStringValue(data, "Status")
        local AccId = self:GetNumberValue(data, "AccId")
        local RoomId = self:GetNumberValue(data, "RoomId")
        local TimeStamp = self:GetNumberValue(Data, "TimeStamp")
        if self.m_CurRoomInfo.RoomId == RoomId and self.m_CurRoomInfo.Members[AccId] then
            local member = self.m_CurRoomInfo.Members[AccId]
            member.Status[Status] = (TimeStamp~=nil and true or nil)
            g_ScriptEvent:BroadcastInLua("ClubHouse_ChangeStatus_Result",playerId, bSuccess, member)
        end
    end]]
end
function LuaClubHouseMgr:ClubHouse_ChangeStatus_Reply(DstAccId, Status, bEnable)
    self:Log("ClubHouse_ChangeStatus_Reply", DstAccId, Status, bEnable)
    --服务器应答（此时服务器端尚未真正处理请求），据此来设定客户端临时状态，之所有走一圈服务器，是为了避免因rpc频率限制导致请求失效而状态不一致
    if Status == EnumCHStatusType.Speaking then
        self.m_LocalMuted = not bEnable
        if self.m_LocalMuted then --如果是闭麦，本地先进行处理，防止服务器端可能不响应
            self:MuteLocalAudio(self:IsIsMyselfForbidden() or not self:IsMyselfSpeaking())
        end
        local myself = self:GetMyselfInfo()
        if myself then
            g_ScriptEvent:BroadcastInLua("ClubHouse_Member_Update", myself)
        end
    end
end


function LuaClubHouseMgr:ClubHouse_QueryMemberByStatus_Result(playerId, bSuccess, data_U, status, context)
    local result = {}
    if bSuccess then
        local dataList = MsgPackImpl.unpack(data_U)
        for i=0, dataList.Count-1 do
            local member = self:GetMemberInfoFromJsonDict(dataList[i])
            table.insert(result, member)
        end
    end
    g_ScriptEvent:BroadcastInLua("ClubHouse_QueryMemberByStatus_Result",bSuccess, result, status, context)
end

function LuaClubHouseMgr:ClubHouse_ClearStatusAll_Result(playerId, bSuccess, data_U)
    self:Log("ClubHouse_ClearStatusAll_Result", playerId, bSuccess, data_U)
end

function LuaClubHouseMgr:ClubHouse_ForbidStatusAll_Result(playerId, bSuccess, data_U)
    self:Log("ClubHouse_ForbidStatusAll_Result", playerId, bSuccess, data_U)
    if bSuccess then
        local data = MsgPackImpl.unpack(data_U)
        if CommonDefs.DictGetValue_LuaCall(data, "Status") == EnumCHStatusType.HandsUp then
            if  CommonDefs.DictGetValue_LuaCall(data, "AddOrRm") then
                self:ClearAllHandsUp() --关闭申请上麦后，清一下申请状态
            end
        end
    end
end

function LuaClubHouseMgr:ClubHouse_ChangeOwner_Result(playerId, bSuccess, data_U)
    self:Log("ClubHouse_ChangeOwner_Result", playerId, bSuccess, data_U)
end

function LuaClubHouseMgr:ClubHouse_QueryMyRoomId_Result(RoomId, Context)
    self:Log("ClubHouse_QueryMyRoomId_Result", RoomId, Context)
    if Context == "createroom" then
        if RoomId>0 then
            g_MessageMgr:ShowMessage("CLUBHOUSE_ALREADY_HAS_ROOM")
        else
            if self.m_CurRoomInfo and self.m_CurRoomInfo.RoomId then
                local msg = g_MessageMgr:FormatMessage("CLUBHOUSE_CREATE_ROOM_WHEN_IN_OTHER_ROOM_CONFIRM")
                MessageWndManager.ShowOKCancelMessage(msg,DelegateFactory.Action(function()
                    CUIManager.ShowUI("CHCreateChatRoomWnd")
                end),nil,LocalString.GetString("确定"),LocalString.GetString("取消"),false)
            else 
                CUIManager.ShowUI("CHCreateChatRoomWnd")
            end
        end
    end
end

function LuaClubHouseMgr:ClubHouse_Notify_DelRoom(RoomId)
    self:Log("ClubHouse_Notify_DelRoom", RoomId)
    if self.m_CurRoomInfo and self.m_CurRoomInfo.RoomId == RoomId then
        self:DoLeaveRoom()
    end
    g_ScriptEvent:BroadcastInLua("ClubHouse_Notify_DelRoom", RoomId)
end

function LuaClubHouseMgr:ClubHouse_Notify_LeaveRoom(RoomId, AccId, MicCount, ListenerCount, Heat)
    self:Log("ClubHouse_Notify_LeaveRoom", RoomId, AccId, MicCount, ListenerCount)
    if self.m_CurRoomInfo and self.m_CurRoomInfo.RoomId == RoomId then

        if self.m_CurRoomInfo.MyAccId == AccId then
            self:DoLeaveRoom()
            g_ScriptEvent:BroadcastInLua("ClubHouse_Myself_Leave_Room") --主动离开或者被房主请离
            return
        end
        
        self.m_CurRoomInfo.MicCount = MicCount
        self.m_CurRoomInfo.ListenerCount = ListenerCount
        self.m_CurRoomInfo.Heat = Heat
        self.m_CurRoomInfo.Members[AccId] = nil
        if self.m_CurRoomInfo.MicTbl[AccId] then
            self.m_CurRoomInfo.MicTbl[AccId] = nil
            self.m_CurRoomInfo.MicList = self:RemoveValueFromTableList(self.m_CurRoomInfo.MicList, AccId)
            g_ScriptEvent:BroadcastInLua("ClubHouse_Broadcaster_Leave_Room")
        elseif self.m_CurRoomInfo.ListenerTbl[AccId] then
            self.m_CurRoomInfo.ListenerTbl[AccId] = nil
            self.m_CurRoomInfo.ListenerList = self:RemoveValueFromTableList(self.m_CurRoomInfo.ListenerList, AccId)
            g_ScriptEvent:BroadcastInLua("ClubHouse_Audience_Leave_Room")
        end
    end
end

function LuaClubHouseMgr:ClubHouse_Notify_EnterRoom(member_U, MicCount, ListenerCount, Heat)
    self:Log("ClubHouse_Notify_EnterRoom", member_U, MicCount, ListenerCount)
    local dict = MsgPackImpl.unpack(member_U)
    local member = self:GetMemberInfoFromJsonDict(dict)
    if member and self.m_CurRoomInfo and self.m_CurRoomInfo.RoomId == member.RoomId then
        self.m_CurRoomInfo.MicCount = MicCount
        self.m_CurRoomInfo.ListenerCount = ListenerCount
        self.m_CurRoomInfo.Heat = Heat
        if member.IsMe then return end
        if self:AddOrUpdateMember(member) then
            if self:IsBroadcaster(member) then
                g_ScriptEvent:BroadcastInLua("ClubHouse_Broadcaster_Enter_Room", member)
            elseif self:IsAudience(member) then
                g_ScriptEvent:BroadcastInLua("ClubHouse_Audience_Enter_Room", member)
            end
        end
    end
end

function LuaClubHouseMgr:ClubHouse_Notify_SetMic(member_U, MicCount, ListenerCount, Heat)
    self:Log("ClubHouse_Notify_SetMic", member_U, MicCount, ListenerCount)
    local dict = MsgPackImpl.unpack(member_U)
    local member = self:GetMemberInfoFromJsonDict(dict)
    if member and self.m_CurRoomInfo and self.m_CurRoomInfo.RoomId == member.RoomId then
        self.m_CurRoomInfo.MicCount = MicCount
        self.m_CurRoomInfo.ListenerCount = ListenerCount
        self.m_CurRoomInfo.Heat = Heat
        if member.IsMe then
            self:SetClientRole(self:IsBroadcaster(member))
        end
        if self:AddOrUpdateMember(member) then
            g_ScriptEvent:BroadcastInLua("ClubHouse_Member_Character_Changed",member)
        end
    end
end

function LuaClubHouseMgr:ClubHouse_Notify_UnsetMic(member_U, MicCount, ListenerCount, Heat)
    self:Log("ClubHouse_Notify_UnsetMic", member_U, MicCount, ListenerCount)
    local dict = MsgPackImpl.unpack(member_U)
    local member = self:GetMemberInfoFromJsonDict(dict)
    if member and self.m_CurRoomInfo and self.m_CurRoomInfo.RoomId == member.RoomId then
        self.m_CurRoomInfo.MicCount = MicCount
        self.m_CurRoomInfo.ListenerCount = ListenerCount
        self.m_CurRoomInfo.Heat = Heat
        if member.IsMe then
            self:SetClientRole(self:IsBroadcaster(member))
        end
        if self:AddOrUpdateMember(member) then
            g_ScriptEvent:BroadcastInLua("ClubHouse_Member_Character_Changed",member)
        end
    end
end

function LuaClubHouseMgr:ClubHouse_Notify_MemberChange(member_U)
    self:Log("ClubHouse_Notify_MemberChange", member_U)
    local dict = MsgPackImpl.unpack(member_U)
    local member = self:GetMemberInfoFromJsonDict(dict)
    if self:AddOrUpdateMember(member) then
        if member.IsMe then
            self.m_LocalMuted = false --清除本地状态，以服务器刷新为准
            --自己状态发生变化时，更新屏蔽麦克风状态
            self:MuteLocalAudio(self:IsIsMyselfForbidden() or not self:IsMyselfSpeaking())
        end
        g_ScriptEvent:BroadcastInLua("ClubHouse_Member_Update",member)
    end
end

function LuaClubHouseMgr:ClubHouse_Notify_Reset()
    self:Log("ClubHouse_Notify_Reset")
    if self:IsInRoom() then
        self:DoLeaveRoom()
        g_ScriptEvent:BroadcastInLua("ClubHouse_Notify_Reset")
    end
end

function LuaClubHouseMgr:ClubHouse_Notify_ClearStatusAll(RoomId, _status)
    self:Log("ClubHouse_Notify_ClearStatusAll", RoomId, _status)
    if self.m_CurRoomInfo and self.m_CurRoomInfo.RoomId == RoomId then
        local changed = false
        for accid, member in pairs(self.m_CurRoomInfo.Members) do
            if member.Status[_status] then
                member.Status[_status] = nil
                changed = true
            end
        end
        if changed then
            g_ScriptEvent:BroadcastInLua("ClubHouse_Notify_ClearStatusAll", _status)
        end
    end
end

function LuaClubHouseMgr:ClubHouse_Notify_ForbidHandsUpChange(RoomId, bForbid)
    self:Log("ClubHouse_Notify_ForbidHandsUpChange", RoomId, bForbid)
    if self.m_CurRoomInfo and self.m_CurRoomInfo.RoomId == RoomId then
        self.m_CurRoomInfo.IsForbidHandsUp = bForbid
        g_ScriptEvent:BroadcastInLua("ClubHouse_Notify_ForbidHandsUpChange", bForbid)
    end
end

function LuaClubHouseMgr:ClubHouse_QueryAllListenersInRoom_Result_Begin(playerId, RoomId)
    self:Log("ClubHouse_QueryAllListenersInRoom_Result_Begin")
end

function LuaClubHouseMgr:ClubHouse_QueryAllListenersInRoom_Result(playerId, RoomId, data_U)
    self:Log("ClubHouse_QueryAllListenersInRoom_Result",playerId, RoomId, data_U)
    if self.m_CurRoomInfo and self.m_CurRoomInfo.RoomId == tonumber(RoomId) then
        local dataList = MsgPackImpl.unpack(data_U)
        for i=0, dataList.Count-1 do
            local member = self:GetMemberInfoFromJsonDict(dataList[i])
            self:AddOrUpdateMember(member)
        end
    end
    g_ScriptEvent:BroadcastInLua("ClubHouse_RoomContentView_Broadcaster_Update")
end

function LuaClubHouseMgr:ClubHouse_QueryAllListenersInRoom_Result_End(playerId, RoomId)
    self:Log("ClubHouse_QueryAllListenersInRoom_Result_End")
    g_ScriptEvent:BroadcastInLua("ClubHouse_QueryAllListenersInRoom_Result_End")
end

function LuaClubHouseMgr:ClubHouse_Notify_ChangeOwner(owner_U, prevOwner_U)
    self:Log("ClubHouse_Notify_ChangeOwner", owner_U, prevOwner_U)
    local newOwner = self:GetMemberInfoFromJsonDict(MsgPackImpl.unpack(owner_U))
    local oldOwner = self:GetMemberInfoFromJsonDict(MsgPackImpl.unpack(prevOwner_U))
    self:UpdateOwnerInfo(newOwner, oldOwner)
    g_ScriptEvent:BroadcastInLua("ClubHouse_Notify_ChangeOwner")
end

function LuaClubHouseMgr:ClubHouse_Notify_UpdateHeat(RoomId, Heat)
    if self.m_CurRoomInfo == nil or self.m_CurRoomInfo.RoomId == RoomId then
        self.m_CurRoomInfo.Heat = Heat
        g_ScriptEvent:BroadcastInLua("ClubHouse_Notify_UpdateHeat")
    end
end

---------------------------
---排行榜
---------------------------
function LuaClubHouseMgr:ClubHouse_QueryRank_Result(RankType, RankData_U, MyValue)
    self:Log("ClubHouse_QueryRank_Result", RankType, RankData_U, MyValue)
    g_ScriptEvent:BroadcastInLua("ClubHouse_QueryRank_Result", RankType, RankData_U, MyValue)
end

--- 房间今日送礼排名
function LuaClubHouseMgr:InitRoomPresentRankTbl(presentInfo)
    local presentRankTbl = {}       -- 按排名存放AccId
    for i = 0, presentInfo.Count - 1 do
        local rankInfo = {}
        rankInfo.member = self:GetMemberInfoFromJsonDict(CommonDefs.DictGetValue_LuaCall(presentInfo[i], "Player"))
        rankInfo.value = self:GetNumberValue(presentInfo[i],"Value")
        rankInfo.updateTime = self:GetNumberValue(presentInfo[i],"UpdateTime")
        table.insert(presentRankTbl, rankInfo)
    end
    return presentRankTbl
end

function LuaClubHouseMgr:SetGivePresentTopThreeSprite(rankSprite, accId)
    local rank = nil
    for i = 1, 3 do
        local rankInfo = LuaClubHouseMgr.m_CurRoomInfo.PresentRankTbl[i]
        if rankInfo and rankInfo.member.AccId == accId then rank = i end
    end
    rankSprite.spriteName = self:GetGivePresentTopThreeSprite(rank)
end

function LuaClubHouseMgr:GetGivePresentTopThreeSprite(rank)
    local spriteList = {'clubhouse_paihang_01','clubhouse_paihang_02','clubhouse_paihang_03'}
    return rank and spriteList[rank] or ""
end


function LuaClubHouseMgr:ClubHouse_Notify_ResetRoomPresent()
    if self.m_CurRoomInfo == nil then return end
    self.m_CurRoomInfo.PresentRankTbl = {}
    g_ScriptEvent:BroadcastInLua("ClubHouse_Notify_ResetRoomPresent")
end

---------------------------
---送礼
---------------------------
LuaClubHouseMgr.m_GivePresentInfo = nil
function LuaClubHouseMgr:ClubHouse_Notify_GivePresent(SrcPlayer_U, PresentId, Count, Combo, Heat, DstAccId, EffectTimes, ToRemove_U)
    self:Log("ClubHouse_Notify_GivePresent", SrcPlayer_U, PresentId, Count, Combo, Heat, DstAccId, EffectTimes, ToRemove_U)
    -- 更新新的送礼数据
    local SrcPlayer = MsgPackImpl.unpack(SrcPlayer_U)
    local newRankInfo = {}
    newRankInfo.member = self:GetMemberInfoFromJsonDict(CommonDefs.DictGetValue_LuaCall(SrcPlayer, "Player"))
    newRankInfo.value = self:GetNumberValue(SrcPlayer,"Value")
    newRankInfo.updateTime = self:GetNumberValue(SrcPlayer,"UpdateTime")

    -- 房间送礼特效
    local srcPlayerName = newRankInfo.member.PlayerName
    self.m_CurRoomInfo.Heat = Heat
    self:UpdateRoomGivePresentRank(newRankInfo, ToRemove_U)

    -- 收礼玩家名
    local dstPlayerName = DstAccId == self.m_CurRoomInfo.Owner.AccId and self.m_CurRoomInfo.Owner.PlayerName or nil
    for _, member in pairs(self.m_CurRoomInfo.Members) do
        if DstAccId == member.AccId then
            dstPlayerName = member.PlayerName
            break
        end
    end
    g_ScriptEvent:BroadcastInLua("ClubHouse_Notify_GivePresent", srcPlayerName, dstPlayerName, PresentId, Combo, EffectTimes)

    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id == newRankInfo.member.PlayerId then
        -- 自身送礼数据另外存储一份，关系到送礼界面的显示
        local isNewCombo = true
        if self.m_GivePresentInfo then
            isNewCombo = self.m_GivePresentInfo.DstAccId ~= DstAccId or self.m_GivePresentInfo.PresentId ~= PresentId
        end
        self.m_GivePresentInfo = {}
        self.m_GivePresentInfo.isNewCombo = isNewCombo
        self.m_GivePresentInfo.DstAccId = DstAccId
        self.m_GivePresentInfo.PresentId = PresentId
        self.m_GivePresentInfo.Combo = Combo
        self.m_GivePresentInfo.UpdateTime = CServerTimeMgr.Inst.timeStamp
        g_ScriptEvent:BroadcastInLua("ClubHouse_Notify_GivePresent_Me")
    end
end

function LuaClubHouseMgr:UpdateRoomGivePresentRank(newRankInfo, ToRemove_U)
    if self.m_CurRoomInfo == nil or self.m_CurRoomInfo.PresentRankTbl == nil then return end
    -- 清除过期/排行榜外数据
    local addNew = true
    local ToRemove = g_MessagePack.unpack(ToRemove_U)
    local toRemoveList = {}
    toRemoveList[newRankInfo.member.AccId] = true
    for _, accId in pairs(ToRemove) do
        toRemoveList[accId] = true
        if accId == newRankInfo.member.AccId then
            addNew = false
        end
    end
    -- 获得新的排序
    local newRankTbl = {}
    for i = 1, #self.m_CurRoomInfo.PresentRankTbl do
        local info = self.m_CurRoomInfo.PresentRankTbl[i]
        if not toRemoveList[info.member.AccId] then
            if addNew and (info.value < newRankInfo.value or (info.value == newRankInfo.value and info.updateTime > newRankInfo.updateTime)) then
                addNew = false
                table.insert(newRankTbl, newRankInfo)
            end
            table.insert(newRankTbl, info)
        end
    end
    if addNew then
        table.insert(newRankTbl, newRankInfo)
    end
    self.m_CurRoomInfo.PresentRankTbl = newRankTbl
end

function LuaClubHouseMgr:ClubHouse_GivePresentFailed()
    self:Log("ClubHouse_GivePresentFailed")
end

---------------------------
---聊天
---------------------------
function LuaClubHouseMgr:DoSendChatMsg(msg)
    if CChatMgr.s_EnableFilterNewLine then
        msg = CChatMgr.Inst:FilterNewLine(msg, 5, " ")
    end
	--检查是否为空
	msg = CUICommonDef.Trim(msg)
	if msg == nil or msg == "" then return false end
	--检查是否有敏感词
	local ret = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(msg,"clubhouse", nil, false)
    if ret.msg == nil then
        g_MessageMgr:ShowMessage("Speech_Violation")
        return false
    end
    local lowLvIgnore = false
    if ret.shouldBeIgnore then
        lowLvIgnore = not CWordFilterMgr.Inst:CheckLowLvFilter(msg, "clubhouse")
    end
    local msgArray = Utility.BreakString(ret.msg, 2, 255, nil)
    Gac2Gas.ClubHouse_BroadcastMessageInRoom(ret.shouldBeIgnore, lowLvIgnore, msgArray[0], msgArray[1])
    return true
end

LuaClubHouseMgr.m_MaxMessageCount = 200
LuaClubHouseMgr.m_RoomMessageDataList = {}
function LuaClubHouseMgr:ClubHouse_BroadcastMessageInRoom(RoomId, SendId, AppName, SendName, content, RoomTop, Character)
    self:Log("ClubHouse_BroadcastMessageInRoom", RoomId, SendId, AppName, SendName, content, RoomTop, Character)
    if not CIMMgr.Inst:IsInBlackList(SendId) then
        local newMsg = {}
        newMsg.RoomId = RoomId
        newMsg.SendId = SendId
        newMsg.AppName = AppName
        newMsg.SendName = SendName
        newMsg.content = CChatMgr.Inst:FilterChatMessage(EnumToInt(ChatChannelDef.ClubHouse), content)
        newMsg.TagList = self:GetMsgTagList(RoomTop, Character)
        table.insert(self.m_RoomMessageDataList, newMsg)
        if #self.m_RoomMessageDataList > self.m_MaxMessageCount then
            table.remove(self.m_RoomMessageDataList, 1)
        end
        g_ScriptEvent:BroadcastInLua("ClubHouse_BroadcastMessageInRoom", SendId, content)
    end
end

function LuaClubHouseMgr:InitMsgCache(msgCache)
    if msgCache then
        for k, msg in pairs(msgCache) do
            local newMsg = {}
            newMsg.RoomId = self.m_CurRoomInfo.RoomId
            newMsg.SendId = msg[1]
            newMsg.AppName = msg[2]
            newMsg.SendName = msg[3]
            newMsg.content = CChatMgr.Inst:FilterChatMessage(EnumToInt(ChatChannelDef.ClubHouse), msg[4])
            newMsg.TagList = self:GetMsgTagList(msg[5], msg[6])
            table.insert(self.m_RoomMessageDataList, newMsg)
        end
    end
end

function LuaClubHouseMgr:SpeechRecovery(playerId)
    local newRoomMessageDataList = {}
    local withdrawPlayerMsg = false
    for _, msg in ipairs(self.m_RoomMessageDataList) do
        if msg.SendId ~= playerId then
            table.insert(newRoomMessageDataList, msg)
        else
            withdrawPlayerMsg = true
        end
    end
    if withdrawPlayerMsg then
        self.m_RoomMessageDataList = newRoomMessageDataList
        g_ScriptEvent:BroadcastInLua("ClubHouse_WithdrawPlayerMsg")
    end
end

function LuaClubHouseMgr:GetMsgTagList(roomTop, character)
    local tagList = {}
    local rankTagNameList = {"clubhouse_biaoshi_01", "clubhouse_biaoshi_02", "clubhouse_biaoshi_03"}
    -- 送礼排名标签
    if rankTagNameList[roomTop] then
        table.insert(tagList, rankTagNameList[roomTop])
    end
    -- 身份标签
    if character == EnumCHCharacterType.Owner then
        table.insert(tagList, "clubhouse_biaoshi_05")
    end
    if character == EnumCHCharacterType.Speaker then
        table.insert(tagList, "clubhouse_biaoshi_04")
    end
    return tagList
end

---房间改名
function LuaClubHouseMgr:ClubHouse_Notify_RenameRoom(RoomId, Name)
    self:Log("ClubHouse_Notify_RenameRoom", RoomId, Name)
    if RoomId == LuaClubHouseMgr:GetRoomId() and self.m_CurRoomInfo then
        self.m_CurRoomInfo.RoomName = Name
    end
    g_ScriptEvent:BroadcastInLua("ClubHouse_Notify_RenameRoom", RoomId, Name)
end

-- 房间排名更新
function LuaClubHouseMgr:ClubHouse_Notify_UpdateTitle(RoomId, Title)
    self:Log("ClubHouse_Notify_UpdateTitle", RoomId, Title)
    if RoomId == self.m_CurRoomInfo.RoomId then
        self.m_CurRoomInfo.Title = Title
    end
    g_ScriptEvent:BroadcastInLua("ClubHouse_Notify_UpdateTitle", RoomId, Title)
end

----------------------------- 
---举报
----------------------------- 
LuaClubHouseMgr.m_RoomViewIsOpen = false
function LuaClubHouseMgr:ShowClubHouseReportItem()
    -- 举报来源：茶室房间主播列表、听众界面、本日送礼排行榜、茶室聊天玩家链接
    if LuaClubHouseMgr.m_RoomViewIsOpen then
        local playerId = LuaPlayerReportMgr.m_PlayerReportInfo.playerId
        local member = self:GetMemberInfoByPlayerId(playerId)
        return member ~= nil
    end
    return false
end

function LuaClubHouseMgr:GetMemberInfoByPlayerId(playerId)
    -- 房主判断
    if playerId == self.m_CurRoomInfo.Owner.PlayerId then
        return self.m_CurRoomInfo.Owner
    end
    -- 主播听众判断
    for _, member in pairs(self.m_CurRoomInfo.Members) do
        if playerId == member.PlayerId then
            return member
        end
    end
end

----------------------------- 
-- 茶室使用梦岛头像
-- 茶室中所有梦岛头像查询汇总到一个接口，按照context区分查询来源
-- 目前分成两个缓存：房间界面和其他。避免在其他界面多次访问不同玩家信息，在删除缓存数据时，删除了房间界面的头像
-- 打开茶室频道开始tick按一定频率查询梦岛头像，关闭聊天界面时清空缓存
-- 加载梦岛头像有多个异步，每一步需要确定：头像是否仍存在，头像最新请求是否是对应玩家
----------------------------- 
LuaClubHouseMgr.m_UseMengDaoProfile = true
LuaClubHouseMgr.m_MengDaoProfileOnceQueryCount = 20
LuaClubHouseMgr.m_MengDaoProfilePortrait2PlayerId = {}  -- 存储头像对应的玩家ID
LuaClubHouseMgr.m_MengDaoProfileQueryList = {}          -- 存储查询列表（key:playerId）

LuaClubHouseMgr.m_MengDaoProfileCache = {}              -- 头像缓存池（key:playerId）
LuaClubHouseMgr.m_MengDaoProfileCachMaxCount = {20, 50}

function LuaClubHouseMgr:ClearMengDaoProfileQueryList()
    self.m_MengDaoProfileQueryList = {}
end

function LuaClubHouseMgr:SetMengDaoProfile(portrait, playerId, context)
    if not self.m_UseMengDaoProfile then return end
    self.m_MengDaoProfilePortrait2PlayerId[portrait] = {playerId = playerId, context = context}    -- 记录该头像对应玩家ID的最新情况
    local isCache = self:SetPlayerMengDaoProfileFromCache(playerId, portrait, context)   -- 尝试从缓存中获取
    if not isCache then
        if self.m_MengDaoProfileQueryList[playerId] then
            table.insert(self.m_MengDaoProfileQueryList[playerId].portraits, portrait)  -- 该玩家头像已在查询队列，将该头像加入对应头像队列
        else
            self.m_MengDaoProfileQueryList[playerId] = {portraits = {portrait}, isQuerying = false} -- 该玩家头像未在查询队列，新增
        end
    end
end

function LuaClubHouseMgr:GetUserProfile()
    local index = 1
    for playerId, _ in pairs(self.m_MengDaoProfileQueryList) do
        if self:GetPlayerMengDaoProfileQueryStatus(playerId) then
            index = index + 1
            self.m_MengDaoProfileQueryList[playerId].isQuerying = true
            CPersonalSpaceMgr.Inst:GetUserProfile(playerId, DelegateFactory.Action_CPersonalSpace_UserProfile_Ret(function (data)
                self:SetPlayerMengDaoProfile(playerId, data)
            end), nil)
        end
        if index > self.m_MengDaoProfileOnceQueryCount then break end
    end
end

-- 是否仍需要加载头像
function LuaClubHouseMgr:GetPlayerMengDaoProfileQueryStatus(playerId)
    local info = self.m_MengDaoProfileQueryList[playerId]
    if info then
        if info.isQuerying then return false end        -- 正在加载
        for _, portrait in ipairs(info.portraits) do
            local needQuery = not CommonDefs.IsNull(portrait) and self.m_MengDaoProfilePortrait2PlayerId[portrait]
                              and self.m_MengDaoProfilePortrait2PlayerId[portrait].playerId == playerId
            if needQuery then return true end           -- 至少有一个头像要加载
        end
        self.m_MengDaoProfileQueryList[playerId] = nil  -- 没有头像要加载
    end
    return false
end

-- 获得玩家头像数据后，准备下载头像替换
function LuaClubHouseMgr:SetPlayerMengDaoProfile(playerId, data)
    local info = self.m_MengDaoProfileQueryList[playerId]
    if info == nil then return end
    local portraitList = {}
    -- 找出该玩家所有需要替换的头像
    for _, portrait in ipairs(info.portraits) do
        if CommonDefs.IsNull(portrait) then  -- 头像不存在，清除记录
            self.m_MengDaoProfilePortrait2PlayerId[portrait] = nil
        end
        if self.m_MengDaoProfilePortrait2PlayerId[portrait] and 
           self.m_MengDaoProfilePortrait2PlayerId[portrait].playerId == playerId then
            table.insert(portraitList, portrait)
        end
    end
    local hasPhoto = playerId == data.data.roleid and data.code == 0 and not System.String.IsNullOrEmpty(data.data.photo)   -- 是否存在头像
    if hasPhoto and #portraitList > 0 then
        -- 有头像，且有需要替换的头像
        CPersonalSpaceMgr.Inst:DownLoadPortraitPic(data.data.photo, portraitList[1].texture, DelegateFactory.Action_Texture(function (texture)
            self:ShowAndRecodePlayerMengDaoProfile(portraitList, playerId, texture)
        end))
    end
    if not hasPhoto then self:ShowAndRecodePlayerMengDaoProfile(portraitList, playerId, nil) end     -- 没有头像记录空
    self.m_MengDaoProfileQueryList[playerId] = nil      -- 将该玩家从访问队列清除
end

function LuaClubHouseMgr:ShowAndRecodePlayerMengDaoProfile(portraitList, playerId, texture)
    if self.m_MengDaoProfilePortrait2PlayerId[portraitList[1]] then
        -- 如果下载完成后，该头像对应的玩家ID已经变更，则还原
        local info = self.m_MengDaoProfilePortrait2PlayerId[portraitList[1]]
        if info and info.playerId ~= playerId then
            local member = self:GetMemberInfoByPlayerId(info.playerId)
            portraitList[1]:LoadNPCPortrait(member.Portrait, false)
            self:SetMengDaoProfile(portraitList[1], playerId, info.context)
        end
    end
    for _, portrait in ipairs(portraitList) do
        local info = self.m_MengDaoProfilePortrait2PlayerId[portrait]
        if info and info.playerId == playerId then
            if texture then
                portrait:Clear()
                portrait.mainTexture = texture
            end
            local cacheIndex = self:GetCacheIndex(info.context)
            self:RecodePlayerMengDaoProfile(cacheIndex, playerId, texture)  -- 记录下载下来的texture
            self.m_MengDaoProfilePortrait2PlayerId[portrait] = nil
        end
    end
end

function LuaClubHouseMgr:GetCacheIndex(context)
    if context == "CHRoomBroadcaster" or context == "CHRoomGivePresentTops" then return 1 end
    return 2
end

function LuaClubHouseMgr:SetPlayerMengDaoProfileFromCache(playerId, portrait, context)
    local cacheIndex = self:GetCacheIndex(context)
    for _, t in pairs(self.m_MengDaoProfileCache) do
        if t.Data and t.Data[playerId] then
            local cache = t.Data[playerId]
            if cache.texture then
                portrait:Clear()
                portrait.mainTexture = cache.texture
                self.m_MengDaoProfilePortrait2PlayerId[portrait] = nil
            end
            self:RecodePlayerMengDaoProfile(cacheIndex, playerId, cache.texture)
            return true
        end
    end
    return false
end

function LuaClubHouseMgr:ClearPlayerMengDaoProfileCache()
    for _, table in pairs(self.m_MengDaoProfileCache) do
        for _, obj in pairs(table.Data) do
            if obj.texture then
                UnityEngine.Object.Destroy(obj.texture)
            end
        end
    end
    self.m_MengDaoProfileCache = {}
end

function LuaClubHouseMgr:RecodePlayerMengDaoProfile(index, playerId, texture)
    local table = self.m_MengDaoProfileCache[index] or {}
    self.m_MengDaoProfileCache[index] = table

    if table.Data == nil then
        table.Data = {}
        table.Size = 0
    end
    
    if table.Data[playerId] == nil then
        table.Data[playerId] = {playerId = playerId, texture = texture}
        table.Size = table.Size + 1
    end

    local obj = table.Data[playerId]
    if table.Header == nil then -- 链表为空
        table.Header = obj
        table.Trailer = obj
        return
    elseif obj == table.Trailer then -- 链表尾部
        return
    elseif obj == table.Header then -- 链表头部
        table.Header = obj.Next
    else -- 链表中间
        local prevObj = obj.Prev
        local nextObj = obj.Next
        if prevObj then prevObj.Next = nextObj end
        if nextObj then nextObj.Prev = prevObj end
    end
    obj.Next = nil
    obj.Prev = table.Trailer
    table.Trailer.Next = obj
    table.Trailer = obj
    if texture then obj.texture = texture end

    if table.Size > self.m_MengDaoProfileCachMaxCount[index] then
        local header = table.Header
        table.Header = header.Next
        if table.Header then table.Header.Prev = nil end
        if header.texture then
            local count = 0
            for _, t in pairs(self.m_MengDaoProfileCache) do
                if t.Data and t.Data[header.playerId] then
                    count = count + 1
                end
            end
            if count <= 1 then UnityEngine.Object.Destroy(header.texture) end
        end
        table.Data[header.playerId] = nil
        table.Size = table.Size - 1
    end
end