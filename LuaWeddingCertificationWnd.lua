local CommonDefs                = import "L10.Game.CommonDefs"
local DelegateFactory           = import "DelegateFactory"
local UILabel                   = import "UILabel"
local UIScrollView              = import "UIScrollView"
local UIEventListener           = import "UIEventListener"
local CWebBrowserMgr            = import "L10.Game.CWebBrowserMgr"
local CButton                   = import "L10.UI.CButton"
local Object                    = import "System.Object"
local CServerTimeMgr            = import "L10.Game.CServerTimeMgr"
local DateTime                  = import "System.DateTime"
local EnumClass                 = import "L10.Game.EnumClass"
local GameSetting_Common_Wapper = import "L10.Game.GameSetting_Common_Wapper"
local ShareBoxMgr               = import "L10.UI.ShareBoxMgr"
local EShareType                = import "L10.UI.EShareType"
local EnumGender                = import "L10.Game.EnumGender"
local Vector4                   = import "UnityEngine.Vector4"
local CUIFxPaths                = import "L10.UI.CUIFxPaths"

LuaWeddingCertificationWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaWeddingCertificationWnd, "groomNameLabel")
RegistClassMember(LuaWeddingCertificationWnd, "brideNameLabel")
RegistClassMember(LuaWeddingCertificationWnd, "marryTime")
RegistClassMember(LuaWeddingCertificationWnd, "groomPromise")
RegistClassMember(LuaWeddingCertificationWnd, "bridePromise")
RegistClassMember(LuaWeddingCertificationWnd, "serviceInfo")
RegistClassMember(LuaWeddingCertificationWnd, "weddingType")

RegistClassMember(LuaWeddingCertificationWnd, "marriageButton")
RegistClassMember(LuaWeddingCertificationWnd, "shareButton")
RegistClassMember(LuaWeddingCertificationWnd, "giftButton")
RegistClassMember(LuaWeddingCertificationWnd, "giftFx")

RegistClassMember(LuaWeddingCertificationWnd, "groomId")
RegistClassMember(LuaWeddingCertificationWnd, "brideId")
RegistClassMember(LuaWeddingCertificationWnd, "groomName")
RegistClassMember(LuaWeddingCertificationWnd, "brideName")
RegistClassMember(LuaWeddingCertificationWnd, "groomClass")
RegistClassMember(LuaWeddingCertificationWnd, "brideClass")
RegistClassMember(LuaWeddingCertificationWnd, "list")

function LuaWeddingCertificationWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
    self:InitEventListener()
end

-- 初始化UI组件
function LuaWeddingCertificationWnd:InitUIComponents()
    local anchor = self.transform:Find("Anchor")

    self.groomNameLabel = anchor:Find("Couple/MaleName"):GetComponent(typeof(UILabel))
    self.brideNameLabel = anchor:Find("Couple/FemaleName"):GetComponent(typeof(UILabel))
    self.marryTime = anchor:Find("MemorialDay"):GetComponent(typeof(UILabel))
    self.groomPromise = anchor:Find("Promise/Male/Label"):GetComponent(typeof(UILabel))
    self.bridePromise = anchor:Find("Promise/Female/Label"):GetComponent(typeof(UILabel))
    self.serviceInfo = anchor:Find("ServiceInfo"):GetComponent(typeof(UILabel))
    self.weddingType = anchor:Find("WeddingType"):GetComponent(typeof(UILabel))

    self.marriageButton = anchor:Find("MarriageButton")
    self.shareButton = anchor:Find("ShareButton"):GetComponent(typeof(CButton))
    self.giftButton = anchor:Find("GiftButton")
    self.giftFx = self.giftButton:Find("Fx"):GetComponent(typeof(CUIFx))
end

function LuaWeddingCertificationWnd:InitEventListener()
    UIEventListener.Get(self.marriageButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnMarriageButtonClick()
	end)

    UIEventListener.Get(self.shareButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnShareButtonClick()
	end)

    UIEventListener.Get(self.giftButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGiftButtonClick()
	end)
end

function LuaWeddingCertificationWnd:OnEnable()
    g_ScriptEvent:AddListener("MarriageCertInfoUpdate", self, "OnMarriageCertInfoUpdate") -- C# definination
    g_ScriptEvent:AddListener("OnSyncMarriageAnniversaryInfo", self, "OnSyncMarriageAnniInfo") -- C# definination
end

function LuaWeddingCertificationWnd:OnDisable()
    g_ScriptEvent:RemoveListener("MarriageCertInfoUpdate", self, "OnMarriageCertInfoUpdate") -- C# definination
    g_ScriptEvent:RemoveListener("OnSyncMarriageAnniversaryInfo", self, "OnSyncMarriageAnniInfo") -- C# definination
end

function LuaWeddingCertificationWnd:OnMarriageCertInfoUpdate(args)
    if not args then return end

    local groomName, brideName, groomId, brideId = args[0], args[1], args[2], args[3]
    if groomId == self.groomId and brideId == self.brideId then
        self.groomName = groomName
        self.brideName = brideName
        self:InitCert()
    end
end

function LuaWeddingCertificationWnd:OnSyncMarriageAnniInfo()
    if self.giftButton.gameObject.activeSelf then
        if LuaWeddingIterationMgr:IsGiftAvailable() then
            self.giftFx:LoadFx(CUIFxPaths.CommonYellowLineFxPath)
            local gap = 5
            local background = self.giftButton:GetComponent(typeof(UISprite))
            CUIFx.DoAni(Vector4(- background.width * 0.5 + gap, background.height * 0.5 - gap, background.width - gap * 2,
                background.height - gap * 2), false, self.giftFx)
        else
            self.giftFx:DestroyFx()
        end
    else
        self.giftFx:DestroyFx()
    end
end


function LuaWeddingCertificationWnd:Init()
    self:InitDefaultDisplay()
    self:QueryCertInfo()
    self:InitButtonsActive()
end

-- 默认显示
function LuaWeddingCertificationWnd:InitDefaultDisplay()
    self.brideNameLabel.text = ""
    self.groomNameLabel.text = ""
    self.marryTime.text = ""
    self.groomPromise.text = ""
    self.bridePromise.text = ""
    self.serviceInfo.text = ""
    self.weddingType.text = ""

    self.shareButton.Enabled = false
end

-- 请求结婚证信息
function LuaWeddingCertificationWnd:QueryCertInfo()
    local data = LuaWeddingIterationMgr.certItem.Item.ExtraVarData.Data
    if data == nil then return end

    self.list = TypeAs(MsgPackImpl.unpack(data), typeof(MakeGenericClass(List, Object)))
    if self.list == nil or self.list.Count < 9 then return end

    local default
    default, self.groomId = System.UInt64.TryParse(ToStringWrap(self.list[0]))
    local extern
    extern, self.brideId = System.UInt64.TryParse(ToStringWrap(self.list[1]))
    if default and extern then
        Gac2Gas.QueryWeddingCertNames(self.groomId, self.brideId)
    end
end

-- 初始化按键active
function LuaWeddingCertificationWnd:InitButtonsActive()
    local isGroomOrBride = CClientMainPlayer.Inst ~= nil and (CClientMainPlayer.Inst.Id == self.groomId or
        CClientMainPlayer.Inst.Id == self.brideId)

    if isGroomOrBride then
        if CommonDefs.IS_CN_CLIENT then
            self:SetButtonsActive(true, true, true)
        elseif CommonDefs.IS_HMT_CLIENT then
            self:SetButtonsActive(false, false, true)
        else
            self:SetButtonsActive(false, true, true)
        end
        Gac2Gas.QueryWeddingDayGiftInfo()
    else
        self:SetButtonsActive(false, false, false)
    end
end

-- 设置按键active
function LuaWeddingCertificationWnd:SetButtonsActive(marriageActive, shareActive, giftActive)
    self.marriageButton.gameObject:SetActive(marriageActive)
    self.shareButton.gameObject:SetActive(shareActive)
    self.giftButton.gameObject:SetActive(giftActive)
end

-- 初始化结婚证
function LuaWeddingCertificationWnd:InitCert()
    self.groomNameLabel.text = self.groomName
    self.brideNameLabel.text = self.brideName

    local default, timeStamp
    default, timeStamp = System.Double.TryParse(ToStringWrap(self.list[2]))
    if default then
        local time = CServerTimeMgr.ConvertTimeStampToZone8Time(timeStamp)
        self.marryTime.text = CommonDefs.String_Format_String_ArrayObject(LocalString.GetString("三界历：{0}年{1}月{2}日{3}时成婚"),
            {time.Year, time.Month, time.Day, time.Hour})
        local key = self:GetMarryYearCount(time)
        local anni = WeddingDay_Anniversary.GetData(key)
        if anni ~= nil then
            self.weddingType.text = anni.Name
        end
    end

    self.groomPromise.text = ToStringWrap(self.list[4])
    self.bridePromise.text = ToStringWrap(self.list[5])
    self.serviceInfo.text = System.String.Format(LocalString.GetString("[c][A65521][c][FF0000]{0}[-]服务器    第[c][FF0000]{1}[-]对结发夫妻"),
        ToStringWrap(self.list[8]), ToStringWrap(self.list[3]))

    local gClass, bClass
    local extern
    extern, gClass = System.UInt32.TryParse(ToStringWrap(self.list[6]))
    local ref
    ref, bClass = System.UInt32.TryParse(ToStringWrap(self.list[7]))
    if extern and ref then
        self.groomClass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), gClass)
        self.brideClass = CommonDefs.ConvertIntToEnum(typeof(EnumClass), bClass)
    end
    self:InitShareButton()
end

function LuaWeddingCertificationWnd:GetMarryYearCount(marry)
    local now = CServerTimeMgr.Inst:GetZone8Time()
    local marryDay = CreateFromClass(DateTime, marry.Year, marry.Month, marry.Day, 0, 0, 0)
    local nowDay = CreateFromClass(DateTime, marry.Year, now.Month, now.Day, 0, 0, 0)
    local key = now.Year - marry.Year - (CommonDefs.op_LessThan_DateTime_DateTime(nowDay, marryDay) and 1 or 0)
    return key
end

-- 初始化分享按键
function LuaWeddingCertificationWnd:InitShareButton()
    local isGroomOrBride = CClientMainPlayer.Inst ~= nil and (CClientMainPlayer.Inst.Id == self.groomId or
        CClientMainPlayer.Inst.Id == self.brideId)

    if CommonDefs.IS_HMT_CLIENT then
        self.shareButton.Enabled = false
    elseif CommonDefs.IS_KR_CLIENT then
        if isGroomOrBride then
            self.shareButton.Enabled = true
        else
            self.shareButton.Enabled = false
        end
    else
        if isGroomOrBride and GameSetting_Common_Wapper.Inst.TradeValidChannels then
            self.shareButton.Enabled = true
        else
            self.shareButton.Enabled = false
        end
    end
end


--@region UIEvent

function LuaWeddingCertificationWnd:OnMarriageButtonClick()
    CWebBrowserMgr.Inst:OpenActivityUrl(CWebBrowserMgr.s_WeddingAnniversaryIndex)
end

function LuaWeddingCertificationWnd:OnShareButtonClick()
    local playerId = CClientMainPlayer.Inst.Id

    if CommonDefs.IS_HMT_CLIENT then
    else
        if playerId == self.brideId then
            ShareBoxMgr.OpenShareBox(EShareType.Marry, 0, Table2ArrayWithCount({self.groomId, self.groomName, self.groomClass, EnumGender.Male}, 4, MakeArrayClass(System.Object)))
        elseif playerId == self.groomId then
            ShareBoxMgr.OpenShareBox(EShareType.Marry, 0, Table2ArrayWithCount({self.brideId, self.brideName, self.brideClass, EnumGender.Female}, 4, MakeArrayClass(System.Object)))
        end
    end
end

function LuaWeddingCertificationWnd:OnGiftButtonClick()
    local marryTime = 0
    local name = CClientMainPlayer.Inst.Id == self.brideId and self.groomName or self.brideName
    local default
    default, marryTime = System.Double.TryParse(ToStringWrap(self.list[2]))
    if default then
        LuaWeddingIterationMgr:OpenAnniversaryGiftWnd(marryTime, name)
    end
end

--@endregion UIEvent
