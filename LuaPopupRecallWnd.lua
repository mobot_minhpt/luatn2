require("common/common_include")


local UILabel = import "UILabel"
local UISprite = import "UISprite"
local CommonDefs = import "L10.Game.CommonDefs"
local Profession = import "L10.Game.Profession"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UIGrid = import "UIGrid"
local CUIGameObjectPool = import "L10.UI.CUIGameObjectPool"
local Object = import "UnityEngine.Object"
local EnumClass = import "L10.Game.EnumClass"

CLuaPopupRecallWnd=class()
RegistChildComponent(CLuaPopupRecallWnd, "Table", UIGrid)
RegistChildComponent(CLuaPopupRecallWnd, "Pool", CUIGameObjectPool)
RegistChildComponent(CLuaPopupRecallWnd, "ScrollView", CUIRestrictScrollView)


function CLuaPopupRecallWnd:Awake()
	
end

function CLuaPopupRecallWnd:Init()
	
end

function CLuaPopupRecallWnd:InitData(bindCode,count,list)
	--self:ClearList()
	for i = 0, list.Count - 1, 5 do
		local item = self.Pool:GetFromPool(0)
		if not item then 
			return
		end
		item.transform.parent = self.Table.transform
		item.transform.localScale = Vector3.one
		item.gameObject:SetActive(true)
		local anchor = CommonDefs.GetComponent_Component_Type(item.gameObject.transform:Find("Anchor"), typeof(UISprite))
		local nameLabel = CommonDefs.GetComponent_Component_Type(anchor.gameObject.transform:Find("NameLabel"), typeof(UILabel))--
		local pointsLabel = CommonDefs.GetComponent_Component_Type(anchor.gameObject.transform:Find("PointsLabel"), typeof(UILabel))--
		local jobSprite = CommonDefs.GetComponent_Component_Type(anchor.gameObject.transform:Find("Sprite"), typeof(UISprite))--

		local cls = list[i + 3]
		jobSprite.spriteName = Profession.GetIcon(CommonDefs.ConvertIntToEnum(typeof(EnumClass), (cls)))
		nameLabel.text=list[i + 1]
		pointsLabel.text=list[i + 2]
	end
	self.Table:Reposition()
	self.ScrollView:SetDragAmount(0, 0, false)
end

function CLuaPopupRecallWnd:OnEnable()
	g_ScriptEvent:AddListener("QueryMergeBattleInviteHuiLiuInfoResult", self, "InitData")

	Gac2Gas.QueryMergeBattleInviteHuiLiuInfo()
end

function CLuaPopupRecallWnd:OnDisable()
	g_ScriptEvent:RemoveListener("QueryMergeBattleInviteHuiLiuInfoResult", self, "InitData")
end


function CLuaPopupRecallWnd:ClearList()

	local childList = self.Table:GetChildList()
    do
        local i = 0
        while i < childList.Count do
            local trans = childList[i]
            trans.parent = nil
            Object.Destroy(trans.gameObject)
            i = i + 1
        end
    end
end

