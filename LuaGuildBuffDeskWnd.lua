local GameObject = import "UnityEngine.GameObject"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local UISprite = import "UISprite"
local LocalString = import "LocalString"
local CUITexture = import "L10.UI.CUITexture"
local Extensions = import "Extensions"
local NGUITools = import "NGUITools"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local Time = import "UnityEngine.Time"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CTooltipAlignType = import "L10.UI.CTooltip+AlignType"
local CItemAccessTipMgr = import "L10.UI.CItemAccessTipMgr"
local CButton = import "L10.UI.CButton"

LuaGuildBuffDeskWnd = class()
RegistChildComponent(LuaGuildBuffDeskWnd,"closeBtn", GameObject)
RegistChildComponent(LuaGuildBuffDeskWnd,"gridTable", UITable)
RegistChildComponent(LuaGuildBuffDeskWnd,"gridTemplate", GameObject)
RegistChildComponent(LuaGuildBuffDeskWnd,"restTime", UILabel)
RegistChildComponent(LuaGuildBuffDeskWnd,"itemCell", GameObject)
RegistChildComponent(LuaGuildBuffDeskWnd,"fillUpBtn", GameObject)
RegistChildComponent(LuaGuildBuffDeskWnd,"eatTip", GameObject)
RegistChildComponent(LuaGuildBuffDeskWnd,"eatBtn", GameObject)
RegistChildComponent(LuaGuildBuffDeskWnd,"tipBtn", GameObject)
RegistChildComponent(LuaGuildBuffDeskWnd,"rankTemplateNode", GameObject)
RegistChildComponent(LuaGuildBuffDeskWnd,"rankScrollView", UIScrollView)
RegistChildComponent(LuaGuildBuffDeskWnd,"rankTable", UITable)

RegistClassMember(LuaGuildBuffDeskWnd, "highNode")
RegistClassMember(LuaGuildBuffDeskWnd, "saveRequestBuyTime")
RegistClassMember(LuaGuildBuffDeskWnd, "saveBuyTemplateId")
RegistClassMember(LuaGuildBuffDeskWnd, "saveBuyGridIdx")
RegistClassMember(LuaGuildBuffDeskWnd, "nowChooseTemplateId")

RegistClassMember(LuaGuildBuffDeskWnd, "saveCoolDownTimeBegin")
RegistClassMember(LuaGuildBuffDeskWnd, "totalCoolDownTime")
RegistClassMember(LuaGuildBuffDeskWnd, "restCoolDownTime")
RegistClassMember(LuaGuildBuffDeskWnd, "coolDownLabel")

function LuaGuildBuffDeskWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.GuildBuffDeskWnd)
end

function LuaGuildBuffDeskWnd:OnEnable()
	g_ScriptEvent:AddListener("SendItem", self, "BuyItemBack")
	g_ScriptEvent:AddListener("SetItemAt", self, "BuyItemBack")
	g_ScriptEvent:AddListener("OnDiningOnGuildDiningTableSuccess", self, "ResetCoolDown")
	g_ScriptEvent:AddListener("OnRefreshGuildDiningWnd", self, "Init")
	g_ScriptEvent:AddListener("OnQuickBuyItemFromPlayerShopFail",self,"OnQuickBuyItemFromPlayerShopFail")
	--g_ScriptEvent:RemoveListener("OnFillGuildDiningTableGridSuccess", self, "BuyItemBack")
end

function LuaGuildBuffDeskWnd:OnDisable()
	g_ScriptEvent:RemoveListener("SendItem", self, "BuyItemBack")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "BuyItemBack")
	g_ScriptEvent:RemoveListener("OnDiningOnGuildDiningTableSuccess", self, "ResetCoolDown")
	g_ScriptEvent:RemoveListener("OnRefreshGuildDiningWnd", self, "Init")
	g_ScriptEvent:RemoveListener("OnQuickBuyItemFromPlayerShopFail",self,"OnQuickBuyItemFromPlayerShopFail")

	--g_ScriptEvent:RemoveListener("OnFillGuildDiningTableGridSuccess", self, "BuyItemBack")
end

function LuaGuildBuffDeskWnd:OnQuickBuyItemFromPlayerShopFail(args)
    local templateId, num = args[0],args[1]
    if templateId==self.saveBuyTemplateId then
        g_MessageMgr:ShowMessage("NpcHaoGanDuGift_BuyFail")
    end
end

function LuaGuildBuffDeskWnd:BuyItemBack()
	if self.saveBuyTemplateId and self.saveBuyGridIdx then
		local count = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, self.saveBuyTemplateId)
		local maxCount = GuildDining_Items.GetData(self.saveBuyTemplateId).Count
		if count >= maxCount then
			Gac2Gas.RequestFillGuildDiningTableGrid(CLuaGuildMgr.guildBuffId,self.saveBuyGridIdx,self.saveBuyTemplateId)
		end
	elseif self.nowChooseTemplateId then
		self:Init()
	end
end

function LuaGuildBuffDeskWnd:DoBuyItem(count,itemTemplateId,gridIdx)
	if not self.saveRequestBuyTime then
		self.saveRequestBuyTime = 0
	end

	if Time.realtimeSinceStartup - self.saveRequestBuyTime <= GuildFreight_GameSetting.GetData().BuyColdTime then
		g_MessageMgr:ShowMessage("RPC_TOO_FREQUENT")
		return
	end
	self.saveRequestBuyTime = Time.realtimeSinceStartup

	local itemGet = ItemGet_item.GetData(itemTemplateId)
	if itemGet ~= nil then
		self.saveBuyTemplateId = itemTemplateId
		self.saveBuyGridIdx = gridIdx
		if CommonDefs.Array_IndexOf_int(itemGet.GetPath, EnumItemAccessType_lua.PlayerShop) >= 0 then
			--玩家商店购买
			Gac2Gas.RequestQuickBuyItemFromPlayerShop(itemTemplateId, count, false)
		elseif CommonDefs.Array_IndexOf_int(itemGet.GetPath, EnumItemAccessType_lua.Yishi) >= 0 then
			--易市购买
			Gac2Gas.BuyMallItemWithReason(EShopMallRegion_lua.EYuanbaoMarket, itemTemplateId, count, "GuildDining")
		end
	end
end

function LuaGuildBuffDeskWnd:InitInfoNode(itemData,templateId,alfilled,haveNum,needFillNum,gridIdx)
	self.nowChooseTemplateId = templateId
	if CLuaGuildMgr.guildBuffBFull then
		CItemInfoMgr.ShowLinkItemTemplateInfo(templateId, false, nil, AlignType.Default, 0, 0, 0, 0)
		--self.itemCell:SetActive(false)
	else
		self.itemCell:SetActive(true)
		self.fillUpBtn:SetActive(true)
		local icon = self.itemCell.transform:Find('Icon'):GetComponent(typeof(CUITexture))
		icon:LoadMaterial(itemData.Icon)
		local quality = self.itemCell.transform:Find('Quality'):GetComponent(typeof(UISprite))
		quality.spriteName = CUICommonDef.GetItemCellBorder()
		local name = self.itemCell.transform:Find('Name'):GetComponent(typeof(UILabel))
		name.text = itemData.Name

		local acquire = self.itemCell.transform:Find('Acquire').gameObject

		if alfilled > 0 then
			acquire:SetActive(false)
			local onIconClick = function(go)
				CItemInfoMgr.ShowLinkItemTemplateInfo(templateId, false, nil, AlignType.Default, 0, 0, 0, 0)
			end
			CommonDefs.AddOnClickListener(self.itemCell,DelegateFactory.Action_GameObject(onIconClick),false)
			self.fillUpBtn.transform:Find('Label'):GetComponent(typeof(UILabel)).text = LocalString.GetString('已装填')
			self.fillUpBtn:GetComponent(typeof(CButton)).Enabled = false
		else
			self.fillUpBtn:GetComponent(typeof(CButton)).Enabled = true
			if haveNum >= needFillNum then
				acquire:SetActive(false)
				local onIconClick = function(go)
					CItemInfoMgr.ShowLinkItemTemplateInfo(templateId, false, nil, AlignType.Default, 0, 0, 0, 0)
				end
				CommonDefs.AddOnClickListener(self.itemCell,DelegateFactory.Action_GameObject(onIconClick),false)
				local onSendFillClick = function(go)
					Gac2Gas.RequestFillGuildDiningTableGrid(CLuaGuildMgr.guildBuffId,gridIdx,templateId)
				end
				self.fillUpBtn.transform:Find('Label'):GetComponent(typeof(UILabel)).text = LocalString.GetString('装填')
				CommonDefs.AddOnClickListener(self.fillUpBtn,DelegateFactory.Action_GameObject(onSendFillClick),false)
			else
				acquire:SetActive(true)

				local onIconClick = function(go)
					CItemAccessTipMgr.Inst:ShowAccessTip(nil, templateId, false, go.transform, CTooltipAlignType.Top)
				end
				CommonDefs.AddOnClickListener(self.itemCell,DelegateFactory.Action_GameObject(onIconClick),false)
				local onSendFillClick = function(go)
					self:DoBuyItem(needFillNum - haveNum,templateId,gridIdx)
				end
				self.fillUpBtn.transform:Find('Label'):GetComponent(typeof(UILabel)).text = LocalString.GetString('购买并装填')
				CommonDefs.AddOnClickListener(self.fillUpBtn,DelegateFactory.Action_GameObject(onSendFillClick),false)

			end
		end
	end
end

function LuaGuildBuffDeskWnd:ShowTimeText()
	local restTime = self.restCoolDownTime
	if restTime < 0 then
		restTime = 0
	end
	local min = math.floor(restTime / 60)
	local sec = math.floor(restTime - min * 60)
	if sec < 10 then
		sec = '0'..sec
	end

	if self.coolDownLabel then
		self.coolDownLabel.text = '[FF0000]'..min..':'..sec..LocalString.GetString('后[-]可再次食用')
	end
end

function LuaGuildBuffDeskWnd:UpdateCoolDownTime()
	if self.restCoolDownTime and self.restCoolDownTime >= 0 then
		local costTime = Time.realtimeSinceStartup - self.saveCoolDownTimeBegin
		if costTime > 0 then
			self.restCoolDownTime = self.totalCoolDownTime - costTime
			self:ShowTimeText()
		end
	else
		self:ResetCoolDown()
	end
end

function LuaGuildBuffDeskWnd:Update()
	self:UpdateCoolDownTime()
end

function LuaGuildBuffDeskWnd:ShowCoolDown(coolDownTime)
	self.saveCoolDownTimeBegin = Time.realtimeSinceStartup
	self.totalCoolDownTime = coolDownTime
	self.restCoolDownTime = self.totalCoolDownTime
	self.coolDownLabel = self.eatTip.transform:Find('coolDownTime'):GetComponent(typeof(UILabel))
	self.coolDownLabel.gameObject:SetActive(true)
	self:UpdateCoolDownTime()
end

function LuaGuildBuffDeskWnd:ResetCoolDown()
	local btnText = LocalString.GetString('食用') .. '(' .. CLuaGuildMgr.guildBuffEatTimes .. '/' .. GuildDining_Setting.GetData().MaxDiningTimes .. ')'
	self.eatBtn.transform:Find('Label'):GetComponent(typeof(UILabel)).text = btnText
	if CClientMainPlayer.Inst then
		local coolDownTime = CClientMainPlayer.Inst.CooldownProp:GetServerRemainTime(1500) / 1000
		if coolDownTime > 0 then
			self.eatBtn:GetComponent(typeof(CButton)).Enabled = false
			self:ShowCoolDown(coolDownTime)
		else
			local eatFunction = function(go)
				Gac2Gas.RequestDiningOnGuildDiningTable(CLuaGuildMgr.guildBuffId)
			end
			CommonDefs.AddOnClickListener(self.eatBtn,DelegateFactory.Action_GameObject(eatFunction),false)
			self.eatBtn:GetComponent(typeof(CButton)).Enabled = true
			if not self.coolDownLabel then
				self.coolDownLabel = self.eatTip.transform:Find('coolDownTime'):GetComponent(typeof(UILabel))
			end
			self.coolDownLabel.gameObject:SetActive(false)
		end
	end
end

function LuaGuildBuffDeskWnd:InitGrid()
	Extensions.RemoveAllChildren(self.gridTable.transform)
	self.itemCell:SetActive(false)
	self.fillUpBtn:SetActive(false)
	self.eatTip:SetActive(false)
	self.eatBtn:SetActive(false)
	if not CLuaGuildMgr.guildBuffGridList then
		return
	end
	if CLuaGuildMgr.guildBuffBFull then
		self.eatTip:SetActive(true)
		self.eatBtn:SetActive(true)
		self:ResetCoolDown()

		for i,v in ipairs(CLuaGuildMgr.guildBuffGridList) do
			local node = NGUITools.AddChild(self.gridTable.gameObject,self.gridTemplate)
			node:SetActive(true)
			local itemData = CItemMgr.Inst:GetItemTemplate(v[1])
			local icon = node.transform:Find('Icon'):GetComponent(typeof(CUITexture))
			icon:LoadMaterial(itemData.Icon)
			local quality = node.transform:Find('Quality'):GetComponent(typeof(UISprite))
			quality.spriteName = CUICommonDef.GetItemCellBorder()
			local mask = node.transform:Find('Mask').gameObject
			mask:SetActive(false)
			local alert = node.transform:Find('FillAvailable').gameObject
			alert:SetActive(false)
			local highlight = node.transform:Find('Highlight').gameObject
			highlight:SetActive(false)
			local filledNode = node.transform:Find('Filled').gameObject
			filledNode:SetActive(true)

			local amountLabel = node.transform:Find('Amount'):GetComponent(typeof(UILabel))
			amountLabel.text = LocalString.GetString('已装填')

			local clickFunction = function(go)
				CItemInfoMgr.ShowLinkItemTemplateInfo(v[1], false, nil, AlignType.Default, 0, 0, 0, 0)
			end
			CommonDefs.AddOnClickListener(node,DelegateFactory.Action_GameObject(clickFunction),false)
		end
		self.gridTable:Reposition()
	else
		local initSign = false
		local normalFirstFuc = nil
		local firstFuc = nil
		for i,v in ipairs(CLuaGuildMgr.guildBuffGridList) do
			local node = NGUITools.AddChild(self.gridTable.gameObject,self.gridTemplate)
			node:SetActive(true)
			local itemData = CItemMgr.Inst:GetItemTemplate(v[1])
			local icon = node.transform:Find('Icon'):GetComponent(typeof(CUITexture))
			icon:LoadMaterial(itemData.Icon)
			local quality = node.transform:Find('Quality'):GetComponent(typeof(UISprite))
			quality.spriteName = CUICommonDef.GetItemCellBorder()
			local mask = node.transform:Find('Mask').gameObject
			mask:SetActive(false)
			local alert = node.transform:Find('FillAvailable').gameObject
			alert:SetActive(false)
			local highlight = node.transform:Find('Highlight').gameObject
			highlight:SetActive(false)
			local filledNode = node.transform:Find('Filled').gameObject
			filledNode:SetActive(false)

			local count = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, v[1])
			local maxCount = GuildDining_Items.GetData(v[1]).Count
			local amountLabel = node.transform:Find('Amount'):GetComponent(typeof(UILabel))
			amountLabel.text = count .. '/' .. maxCount
			if count >= maxCount and v[2] <= 0 then
				alert:SetActive(true)
			end

			if v[2] > 0 then
				filledNode:SetActive(true)
				amountLabel.text = LocalString.GetString('已装填')
			end

			local clickFunction = function(go)
				if self.highNode then
					self.highNode:SetActive(false)
				end

				self.highNode = highlight
				highlight:SetActive(true)

				self:InitInfoNode(itemData,v[1],v[2],count,maxCount,v[3])
			end
			CommonDefs.AddOnClickListener(node,DelegateFactory.Action_GameObject(clickFunction),false)
			if not initSign then
				if self.nowChooseTemplateId and self.nowChooseTemplateId == v[1] then
					clickFunction()
					initSign = true
				elseif count >= maxCount and not firstFuc and v[2] <= 0 then
					firstFuc = clickFunction
				end
			end
			if not normalFirstFuc then
				normalFirstFuc = clickFunction
			end
		end
		if not initSign and firstFuc then
			firstFuc()
		elseif not initSign and normalFirstFuc then
			normalFirstFuc()
		end
		self.gridTable:Reposition()
	end
end

function LuaGuildBuffDeskWnd:InitRankInfo()
	Extensions.RemoveAllChildren(self.rankTable.transform)

	if CLuaGuildMgr.guildBuffTopList then --{pos, playerId, playerName, score}
		for i,v in ipairs(CLuaGuildMgr.guildBuffTopList) do
			local node = NGUITools.AddChild(self.rankTable.gameObject,self.rankTemplateNode)
			node:SetActive(true)
			local bg1 = node.transform:Find('bg1').gameObject
			local bg2 = node.transform:Find('bg2').gameObject
			if i%2 == 1 then
				bg1:SetActive(true)
				bg2:SetActive(false)
			else
				bg1:SetActive(false)
				bg2:SetActive(true)
			end
			node.transform:Find('name'):GetComponent(typeof(UILabel)).text = v[3]
			if v[1] <= 3 then
				for i=1,3 do
					if i == v[1] then
						node.transform:Find('sRank'..i).gameObject:SetActive(true)
					else
						node.transform:Find('sRank'..i).gameObject:SetActive(false)
					end
				end
				node.transform:Find('rank').gameObject:SetActive(false)
			else
				for i=1,3 do
					node.transform:Find('sRank'..i).gameObject:SetActive(false)
				end
				node.transform:Find('rank').gameObject:SetActive(true)
				node.transform:Find('rank'):GetComponent(typeof(UILabel)).text = i
			end
			node.transform:Find('num'):GetComponent(typeof(UILabel)).text = v[4]
		end
	end

	self.rankTable:Reposition()
	self.rankScrollView:ResetPosition()
end

function LuaGuildBuffDeskWnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	local onTipClick = function(go)
		g_MessageMgr:ShowMessage("GuildDining_Tips")
	end
	CommonDefs.AddOnClickListener(self.tipBtn,DelegateFactory.Action_GameObject(onTipClick),false)
	self.gridTemplate:SetActive(false)
	self.rankTemplateNode:SetActive(false)

	self:InitGrid()
	self:InitRankInfo()
end

return LuaGuildBuffDeskWnd
