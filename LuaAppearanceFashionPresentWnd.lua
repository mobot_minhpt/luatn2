local CCommonSelector = import "L10.UI.CCommonSelector"
local CPopupMenuInfoMgrAlignType = import "L10.UI.CPopupMenuInfoMgr+AlignType"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local AlignType = import "CPlayerInfoMgr+AlignType"
local CButton = import "L10.UI.CButton"

LuaAppearanceFashionPresentWnd = class()

RegistChildComponent(LuaAppearanceFashionPresentWnd, "m_SortSelector", "SortSelector", CCommonSelector)
RegistChildComponent(LuaAppearanceFashionPresentWnd, "m_ContentScrollView", "ContentScrollView", UIScrollView)
RegistChildComponent(LuaAppearanceFashionPresentWnd, "m_ContentGrid", "ContentGrid", UIGrid)
RegistChildComponent(LuaAppearanceFashionPresentWnd, "m_ItemTemplate", "ItemTemplate", GameObject)
RegistChildComponent(LuaAppearanceFashionPresentWnd, "m_InfoButton", "InfoButton", GameObject)
RegistChildComponent(LuaAppearanceFashionPresentWnd, "m_SelectedItemIcon", "ItemIcon", CUITexture)
RegistChildComponent(LuaAppearanceFashionPresentWnd, "m_SelectedItemNameLabel", "ItemNameLabel", UILabel)
RegistChildComponent(LuaAppearanceFashionPresentWnd, "m_SelectedItemRoot", "SelectedItemRoot", GameObject)
RegistChildComponent(LuaAppearanceFashionPresentWnd, "m_PlayersRoot", "PlayersRoot", GameObject)

RegistClassMember(LuaAppearanceFashionPresentWnd, "m_SortNames")
RegistClassMember(LuaAppearanceFashionPresentWnd, "m_AllFashions")
RegistClassMember(LuaAppearanceFashionPresentWnd, "m_SortIndex")
RegistClassMember(LuaAppearanceFashionPresentWnd, "m_SelectedDataId")
RegistClassMember(LuaAppearanceFashionPresentWnd, "m_CurFashionId")
RegistClassMember(LuaAppearanceFashionPresentWnd, "m_CurFashionPlayerInfoTbl")

function LuaAppearanceFashionPresentWnd:Awake()
    self.m_ItemTemplate:SetActive(false)
    UIEventListener.Get(self.m_InfoButton.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:OnInfoButtonClick() end)
    self.m_SortNames = LuaAppearancePreviewMgr:GetPresentFashionInfoSortNames()
    local names = Table2List(self.m_SortNames, MakeGenericClass(List,cs_string))
    self.m_SortSelector:Init(names, DelegateFactory.Action_int(function ( index )
        self.m_SortIndex = index + 1
		self.m_SortSelector:SetName(names[index])
        self:LoadData(index+1)
    end))
    self.m_SortSelector:SetPopupAlignType(CPopupMenuInfoMgrAlignType.Bottom)
    self.m_SelectedDataId = 0
end

function LuaAppearanceFashionPresentWnd:Init()
    self.m_SelectedDataId = LuaAppearancePreviewMgr.m_RequestPresentFasionId or 0
    self.m_SortIndex = 1
    self.m_SortSelector:SetName(self.m_SortNames[self.m_SortIndex])
    self:LoadData(self.m_SortIndex)
end

function LuaAppearanceFashionPresentWnd:LoadData(sortIndex)
    self.m_AllFashions = LuaAppearancePreviewMgr:GetAllPresentFashionInfo(sortIndex)
    Extensions.RemoveAllChildren(self.m_ContentGrid.transform)

    for i = 1, #self.m_AllFashions do
        local child = CUICommonDef.AddChild(self.m_ContentGrid.gameObject, self.m_ItemTemplate)
        child:SetActive(true)
        self:InitItem(child, self.m_AllFashions[i])
    end
   
    self.m_ContentGrid:Reposition()
    self.m_ContentScrollView:ResetPosition()
    self:InitSelection()
end

function LuaAppearanceFashionPresentWnd:InitSelection()
    if self.m_SelectedDataId == 0 then
        self:OnItemClick(nil)
        return
    end

    local childCount = self.m_ContentGrid.transform.childCount
    for i=0, childCount-1 do
        local childGo = self.m_ContentGrid.transform:GetChild(i).gameObject
        local fashionData = self.m_AllFashions[i+1]
        if fashionData and fashionData.id == self.m_SelectedDataId then
            self:OnItemClick(childGo)
            break
        end
    end
end

function LuaAppearanceFashionPresentWnd:InitItem(itemGo, fashionData)
    local iconTexture = itemGo.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local borderSprite = itemGo.transform:Find("Item/Border"):GetComponent(typeof(UISprite))
    local nameLabel = itemGo.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local positionLabel = itemGo.transform:Find("PositionLabel"):GetComponent(typeof(UILabel))
    local remainLabel = itemGo.transform:Find("RemainLabel"):GetComponent(typeof(UILabel))

    iconTexture:LoadMaterial(fashionData.icon)
    borderSprite.spriteName = LuaAppearancePreviewMgr:GetFashionQualityBorder(fashionData.quality)
    nameLabel.text = fashionData.name
    positionLabel.text = LuaAppearancePreviewMgr:GetFashionPositionName(fashionData.position)
    remainLabel.text = SafeStringFormat3(LocalString.GetString("剩余%d"), fashionData.remain)

    UIEventListener.Get(itemGo).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnItemClick(go)
    end)
end

function LuaAppearanceFashionPresentWnd:OnItemClick(go)
    local childCount = self.m_ContentGrid.transform.childCount
    local info = nil
    for i=0, childCount-1 do
        local childGo = self.m_ContentGrid.transform:GetChild(i).gameObject
        if childGo == go then
            childGo.transform:GetComponent(typeof(CButton)).Selected = true
            self.m_SelectedDataId = self.m_AllFashions[i+1].id
            info = self.m_AllFashions[i+1]
        else
            childGo.transform:GetComponent(typeof(CButton)).Selected = false
        end
    end
    self:UpdatePresentDetailView(info)
end

function LuaAppearanceFashionPresentWnd:UpdatePresentDetailView(fashionData)
    self.m_SelectedItemRoot:SetActive(fashionData~=nil)
    self.m_PlayersRoot:SetActive(fashionData~=nil)
    if fashionData then
        self.m_SelectedItemNameLabel.text = fashionData.name
        self.m_SelectedItemIcon:LoadMaterial(fashionData.icon)
        local playerInfoTbl = LuaAppearancePreviewMgr:GetPresentFahsionPlayerInfo(fashionData.id)
        local childCount = self.m_PlayersRoot.transform.childCount
        for i=0, childCount-1 do
            local child = self.m_PlayersRoot.transform:GetChild(i)
            local icon = child:Find("Icon"):GetComponent(typeof(CUITexture))
            local nameLabel = child:Find("Icon/NameLabel"):GetComponent(typeof(UILabel))
            local addGo = child:Find("Add").gameObject
            if i<#playerInfoTbl then
                icon.gameObject:SetActive(true)
                addGo:SetActive(false)
                icon:LoadNPCPortrait(playerInfoTbl[i+1].icon)
                nameLabel.text = playerInfoTbl[i+1].name
                local id =  playerInfoTbl[i+1].id
                UIEventListener.Get(child.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:ShowPlayerPopupMenu(id) end)
            else
                icon.gameObject:SetActive(false)
                addGo:SetActive(true)
                UIEventListener.Get(child.gameObject).onClick = DelegateFactory.VoidDelegate(function(go) self:GivePresent(fashionData.id, playerInfoTbl) end)
            end
        end
    end
end

function LuaAppearanceFashionPresentWnd:ShowPlayerPopupMenu(playerId)
    CPlayerInfoMgr.ShowPlayerPopupMenu(playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, Vector3.zero, AlignType.Default)
end

function LuaAppearanceFashionPresentWnd:GivePresent(fashionId, playerInfoTbl)
    self.m_CurFashionId = fashionId
    self.m_CurFashionPlayerInfoTbl = playerInfoTbl
    CLuaIMMgr:QueryOnlineReverseFriends(EnumQueryOnlineReverseFriendsContext_lua.AppearanceFashionPresent)
end

function LuaAppearanceFashionPresentWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSyncWardrobeProperty",self,"OnSyncWardrobeProperty")
    g_ScriptEvent:AddListener("QueryOnlineReverseFriendsFinished",self,"OnQueryOnlineReverseFriendsFinished")
end

function LuaAppearanceFashionPresentWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnSyncWardrobeProperty",self,"OnSyncWardrobeProperty")
    g_ScriptEvent:RemoveListener("QueryOnlineReverseFriendsFinished",self,"OnQueryOnlineReverseFriendsFinished")
end

function LuaAppearanceFashionPresentWnd:OnSyncWardrobeProperty(reason)
    self:LoadData(self.m_SortIndex)
end

function LuaAppearanceFashionPresentWnd:OnQueryOnlineReverseFriendsFinished(args)
    local playerIds = args[0]
    local context = args[1]
    if not self.m_CurFashionId or self.m_CurFashionId==0 then return end
    if context == EnumQueryOnlineReverseFriendsContext_lua.AppearanceFashionPresent then
        local playerInfoTbl = self.m_CurFashionPlayerInfoTbl or {}
        --基于服务器RPC QueryOnlineReverseFriends 的本服在线双向好友
        LuaCommonPlayerListMgr:ShowOnlineFriendsWithCheck(LocalString.GetString("好友"), LocalString.GetString("赠送"), function(playerId)
            LuaAppearancePreviewMgr:RequestPresentFashion_Permanent(self.m_CurFashionId, playerId)
        end, "Appearance_Fashion_Present_No_Available_Online_Friend", function(playerId)
            --跳过不在服务器返回结果里面的玩家
            if not CommonDefs.ListContains_LuaCall(playerIds, playerId) then
                return false
            end
            --跳过已经赠送的玩家
            for i=1, #playerInfoTbl do
                if playerInfoTbl[i].id == playerId then
                    return false
                end
            end
            return true
        end)
    end
end

function LuaAppearanceFashionPresentWnd:OnInfoButtonClick()
    g_MessageMgr:ShowMessage("Appearance_Fashion_Present_Description")
end 