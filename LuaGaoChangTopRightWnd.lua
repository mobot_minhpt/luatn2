local DelegateFactory = import "DelegateFactory"

LuaGaoChangTopRightWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaGaoChangTopRightWnd, "signalButton")

function LuaGaoChangTopRightWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
    self:InitEventListener()
end

function LuaGaoChangTopRightWnd:InitUIComponents()
    self.signalButton = self.transform:Find("Anchor/SignalButton").gameObject
end

function LuaGaoChangTopRightWnd:InitEventListener()
    UIEventListener.Get(self.signalButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSignalButtonClick()
	end)
end


function LuaGaoChangTopRightWnd:Init()

end

--@region UIEvent

function LuaGaoChangTopRightWnd:OnSignalButtonClick()
    CUIManager.ShowUI(CLuaUIResources.GaoChangSignalWnd)
end

--@endregion UIEvent

