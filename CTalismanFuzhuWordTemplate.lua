-- Auto Generated!!
local CTalismanFuzhuWordTemplate = import "L10.UI.CTalismanFuzhuWordTemplate"
local Extensions = import "Extensions"
local LocalString = import "LocalString"
local StringBuilder = import "System.Text.StringBuilder"
local Word_Word = import "L10.Game.Word_Word"
CTalismanFuzhuWordTemplate.m_Init_CS2LuaHook = function (this, oldWord, newWord, canWash, suitId) 
    this.suitId = suitId
    this.wordId = newWord
    this.canWash = canWash
    this.checkBox.gameObject:SetActive(canWash)
    this.cantWashLabel.gameObject:SetActive(not canWash)
    this.checkBox:SetSelected(false, false)
    local wordStr = ""
    local w_old = Word_Word.GetData(oldWord)
    local w_new = Word_Word.GetData(newWord)
    if w_old == nil or w_new == nil then
        return
    end
    local oldStr = System.String.Format(LocalString.GetString("{0}(评分+{1})"), w_old.Description, w_old.Mark)
    local newStr = System.String.Format(LocalString.GetString("{0}(评分+{1})"), w_new.Description, w_new.Mark)
    if oldWord == newWord then
        wordStr = oldStr
    else
        local builder = NewStringBuilderWraper(StringBuilder)
        builder:AppendLine(System.String.Format("[s][c][888888]{0}[-][/c][/s]", oldStr))
        builder:Append(newStr)
        wordStr = ToStringWrap(builder)
    end
    this.wordLabel.text = wordStr
    Extensions.SetLocalPositionY(this.background.transform, math.min(this.height, this.wordLabel.transform.localPosition.y - this.wordLabel.height - this.delta))
end
