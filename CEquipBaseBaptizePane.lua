-- Auto Generated!!
local CClientFormula = import "L10.Game.CClientFormula"
local CEquipBaptizeAttributeItem = import "L10.UI.CEquipBaptizeAttributeItem"
local CEquipBaseBaptizePane = import "L10.UI.CEquipBaseBaptizePane"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CGetMoneyMgr = import "L10.UI.CGetMoneyMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CMiddleNoticeMgr = import "L10.UI.CMiddleNoticeMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CTooltip = import "L10.UI.CTooltip"
local CUICommonDef = import "L10.UI.CUICommonDef"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local GameObject = import "UnityEngine.GameObject"
local LocalString = import "LocalString"
local Object = import "System.Object"
local String = import "System.String"
local StringBuilder = import "System.Text.StringBuilder"
local UIEventListener = import "UIEventListener"
local UIGrid = import "UIGrid"
local UInt32 = import "System.UInt32"
local Vector3 = import "UnityEngine.Vector3"
CEquipBaseBaptizePane.m_OnSelected_CS2LuaHook = function (this, itemId) 

    this:SetBaseInfo()
    local item = CItemMgr.Inst:GetById(itemId)
    this:UpdateEquipBaptizeDetail(item)
end
CEquipBaseBaptizePane.m_SetBaseInfo_CS2LuaHook = function (this) 
    this.nianzhuCost:SetBaseInfo()

    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    local equipItem = CEquipmentProcessMgr.Inst.BaptizingEquipment
    local cost = CEquipmentProcessMgr.Inst:GetBaptizeCost(equip)
end
CEquipBaseBaptizePane.m_Awake_CS2LuaHook = function (this) 
    this.itemTemplate:SetActive(false)
    UIEventListener.Get(this.baptizeBaseBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        --洗基础
        if CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize == nil then
            CMiddleNoticeMgr.ShowMiddleNotice(LocalString.GetString("至少选中一件装备"), 1)
        else
            if not this.nianzhuCost.yinliangEnough then
                g_MessageMgr:ShowMessage("SILVER_NOT_ENOUGH")
                return
            end

            local canBaptize = true

            if not this.nianzhuCost.nianzhuEnough then
                if not this.nianzhuCost.IsAutoCostJade then
                    canBaptize = false
                    g_MessageMgr:ShowMessage("BAPTIZE_EQUIP_WORD_ITEM_NOT_ENOUGH", List2Params(InitializeList(CreateFromClass(MakeGenericClass(List, Object)), Object, LocalString.GetString("念珠"))))
                else
                    if not this.nianzhuCost.lingyuEnough then
                        canBaptize = false
                        CGetMoneyMgr.Inst:GetLingYu(this.nianzhuCost.LingYuCost, this.nianzhuCost.TemplateId)
                    end
                end
            end


            if canBaptize then
                if not CEquipmentProcessMgr.Inst:CheckOperationConflict(CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize) then
                    return
                end
                local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
                Gac2Gas.RequestBaptizeEquipmentProperty(EnumToInt(equip.place), equip.pos, equip.itemId, false, this.nianzhuCost.IsAutoCostJade)
                CUICommonDef.SetActive(this.baptizeBaseBtn, false, true)
            end
        end
    end)
end
CEquipBaseBaptizePane.m_OnEnable_CS2LuaHook = function (this) 
    --刷新装备修饰词
    --数据变化
    --刷新装备修饰词
    EventManager.AddListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
    --数据变化
    EventManager.AddListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
    --数据有无变化
    EventManager.AddListenerInternal(EnumEventType.BaptizeEquipmentPropertySuccess, MakeDelegateFromCSFunction(this.OnBaptizeEquipmentPropertySuccess, MakeGenericClass(Action1, String), this))
    EventManager.AddListenerInternal(EnumEventType.BaptizeFail, MakeDelegateFromCSFunction(this.OnFail, MakeGenericClass(Action1, String), this))
end
CEquipBaseBaptizePane.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.BaptizeEquipmentPropertySuccess, MakeDelegateFromCSFunction(this.OnBaptizeEquipmentPropertySuccess, MakeGenericClass(Action1, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.BaptizeFail, MakeDelegateFromCSFunction(this.OnFail, MakeGenericClass(Action1, String), this))
end
CEquipBaseBaptizePane.m_OnBaptizeEquipmentPropertySuccess_CS2LuaHook = function (this, equipId) 

    CUICommonDef.SetActive(this.baptizeBaseBtn, true, true)

    local item = CItemMgr.Inst:GetById(equipId)
    if item ~= nil and item.IsEquip then
        local oldFeiShengStatus = item.Equip.IsFeiShengAdjusted
        this:TempSetEquipFeiShengStatus(item.Equip, 0)
        local recordList = item.Equip:GetStatistics(true)
        this:TempSetEquipFeiShengStatus(item.Equip, oldFeiShengStatus)
        if this:IsGood(recordList) then
            this:StartCoroutine(this:HideAndShow())
            g_MessageMgr:ShowMessage("EquipBaptize_JiChu_Better_Tip", item.ColoredName)
        end
    end
end
CEquipBaseBaptizePane.m_OnSendItem_CS2LuaHook = function (this, itemId) 

    local equip = CEquipmentProcessMgr.Inst.SelectEquipmentForBaptize
    if equip == nil then
        return
    end

    local item = CItemMgr.Inst:GetById(itemId)
    local equipment = CItemMgr.Inst:GetById(equip.itemId)
    if equipment == nil then
        return
    end
    --装备刷新
    if equip ~= nil and equip.itemId == itemId then
        this:UpdateEquipBaptizeDetail(item)
    end
end
CEquipBaseBaptizePane.m_OnSetItemAt_CS2LuaHook = function (this, place, position, oldItemId, newItemId) 

    if oldItemId == nil and newItemId == nil then
        return
    end
    local default
    if oldItemId ~= nil then
        default = oldItemId
    else
        default = newItemId
    end
    local itemId = default

    this:OnSendItem(itemId)
end
CEquipBaseBaptizePane.m_IsGood_CS2LuaHook = function (this, recordList) 
    if recordList == nil then
        return false
    end
    local e1 = recordList.Count
    local sum = 0
    local count = 0
    local maxVal = 0
    CommonDefs.ListIterate(recordList, DelegateFactory.Action_object(function (___value) 
        local item = ___value
        CommonDefs.EnumerableIterate(item.valuePairs, DelegateFactory.Action_object(function (___value) 
            local item2 = ___value
            sum = sum + item2.percent
            if item2.percent > maxVal then
                maxVal = item2.percent
            end
        end))
        count = count + item.valuePairs.Length
    end))
    local e2 = sum / count * 1.2
    local e3 = maxVal * 1.2
    --Debug.Log("count:"+count+ " e2:" + e2);
    local ret = CClientFormula.Inst:Formula_204(count, e2, e3)

    return ret > 0
end
CEquipBaseBaptizePane.m_UpdateEquipBaptizeDetail_CS2LuaHook = function (this, item) 
    if item == nil then
        return
    end
    --basePropertyLabel.text = item.Equip.GetDescription();
    Extensions.RemoveAllChildren(this.recordListNode)

    local oldFeiShengStatus = item.Equip.IsFeiShengAdjusted
    this:TempSetEquipFeiShengStatus(item.Equip, 0)
    local recordList = item.Equip:GetStatistics(false)
    this:TempSetEquipFeiShengStatus(item.Equip, oldFeiShengStatus)
    local builder = NewStringBuilderWraper(StringBuilder)
    do
        local i = 0
        while i < recordList.Count do
            local record = recordList[i]
            builder:AppendLine(ToStringWrap(record))
            local go = TypeAs(CommonDefs.Object_Instantiate(this.itemTemplate), typeof(GameObject))
            go:SetActive(true)
            CommonDefs.GetComponent_GameObject_Type(go, typeof(CEquipBaptizeAttributeItem)):Init(recordList[i])
            go.transform.parent = this.recordListNode
            go.transform.localScale = Vector3.one
            i = i + 1
        end
    end
    CommonDefs.GetComponent_Component_Type(this.recordListNode, typeof(UIGrid)):Reposition()

    local str = StringTrim(ToStringWrap(builder))
    UIEventListener.Get(this.equipRecordRegion).onClick = DelegateFactory.VoidDelegate(function (p) 
        if not System.String.IsNullOrEmpty(str) then
            CTooltip.Show(str, this.recordListNode, CTooltip.AlignType.Top)
        end
    end)
end
