local UIRoot = import "UIRoot"
local DelegateFactory  = import "DelegateFactory"
local UISlider = import "UISlider"
local UILabel = import "UILabel"
local CUIFx = import "L10.UI.CUIFx"
local UIProgressBar = import "UIProgressBar"
local UILongPressButton = import "L10.UI.UILongPressButton"
local UICamera = import "UICamera"
local Screen = import "UnityEngine.Screen"
local NGUIText = import "NGUIText"
local CMainCamera = import "L10.Engine.CMainCamera"
local Quaternion = import "UnityEngine.Quaternion"
local Vector2 = import "UnityEngine.Vector2"
local Physics = import "UnityEngine.Physics"

LuaCurlingOlympicWinterGamesPlayWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaCurlingOlympicWinterGamesPlayWnd, "Slider", "Slider", UISlider)
RegistChildComponent(LuaCurlingOlympicWinterGamesPlayWnd, "DistanceLabel", "DistanceLabel", UILabel)
RegistChildComponent(LuaCurlingOlympicWinterGamesPlayWnd, "StopFx", "StopFx", CUIFx)
RegistChildComponent(LuaCurlingOlympicWinterGamesPlayWnd, "ProgressBar", "ProgressBar", UIProgressBar)
RegistChildComponent(LuaCurlingOlympicWinterGamesPlayWnd, "Button", "Button", UILongPressButton)
RegistChildComponent(LuaCurlingOlympicWinterGamesPlayWnd, "CancelDot", "CancelDot", GameObject)
RegistChildComponent(LuaCurlingOlympicWinterGamesPlayWnd, "ControlCircle", "ControlCircle", UITexture)
RegistChildComponent(LuaCurlingOlympicWinterGamesPlayWnd, "Dot", "Dot", GameObject)
--@endregion RegistChildComponent end
RegistClassMember(LuaCurlingOlympicWinterGamesPlayWnd, "m_HideSliderTick")
RegistClassMember(LuaCurlingOlympicWinterGamesPlayWnd, "m_VirtualScreenWidth")
RegistClassMember(LuaCurlingOlympicWinterGamesPlayWnd, "m_VirtualScreenHeight")
RegistClassMember(LuaCurlingOlympicWinterGamesPlayWnd, "m_DoubleStickIsCancelCast")
RegistClassMember(LuaCurlingOlympicWinterGamesPlayWnd, "m_LastScreenPos")
RegistClassMember(LuaCurlingOlympicWinterGamesPlayWnd, "m_IsCancelCast")
RegistClassMember(LuaCurlingOlympicWinterGamesPlayWnd, "m_Radius") 
function LuaCurlingOlympicWinterGamesPlayWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaCurlingOlympicWinterGamesPlayWnd:Init()
    self.Slider.gameObject:SetActive(false)
    self.Slider.value = 0
    self.m_Radius = 140
    self.DistanceLabel.text = ""
    self.ProgressBar.gameObject:SetActive(false)
    self.Button.gameObject:SetActive(false)
    self.CancelDot.gameObject:SetActive(false)
    self.ControlCircle.gameObject:SetActive(false)
    UIEventListener.Get(self.Button.gameObject).onPress = DelegateFactory.BoolDelegate(function (go, isPressed)
        self:OnPress(go, isPressed)
    end)
    UIEventListener.Get(self.Button.gameObject).onDrag = DelegateFactory.VectorDelegate(function(go, delta)
        self:OnDrag(go, delta)
    end)
    local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    self.m_VirtualScreenWidth = Screen.width * UICamera.currentCamera.rect.width * scale
    self.m_VirtualScreenHeight = Screen.height * scale
    self.m_LastScreenPos = Vector3.zero
end

function LuaCurlingOlympicWinterGamesPlayWnd:OnEnable()
    g_ScriptEvent:AddListener("CurlingOlympicGamesRoundStop", self, "OnCurlingOlympicGamesRoundStop")
    g_ScriptEvent:AddListener("CurlingOlympicGamesThrowBall", self, "OnCurlingOlympicGamesThrowBall")
    g_ScriptEvent:AddListener("CurlingOlympicWinterGamesOnEnterEmbracing", self, "CurlingOlympicWinterGamesOnEnterEmbracing")
    g_ScriptEvent:AddListener("CurlingOlympicWinterGamesOnLeaveEmbracing", self, "CurlingOlympicWinterGamesOnLeaveEmbracing")
end

function LuaCurlingOlympicWinterGamesPlayWnd:OnDisable()
    g_ScriptEvent:RemoveListener("CurlingOlympicGamesRoundStop", self, "OnCurlingOlympicGamesRoundStop")
    g_ScriptEvent:RemoveListener("CurlingOlympicGamesThrowBall", self, "OnCurlingOlympicGamesThrowBall")
    g_ScriptEvent:RemoveListener("CurlingOlympicWinterGamesOnEnterEmbracing", self, "CurlingOlympicWinterGamesOnEnterEmbracing")
    g_ScriptEvent:RemoveListener("CurlingOlympicWinterGamesOnLeaveEmbracing", self, "CurlingOlympicWinterGamesOnLeaveEmbracing")
    LuaTweenUtils.DOKill(self.Slider.transform, false)
    LuaTweenUtils.DOKill(self.transform, false)
    self:CancelHideSliderTick()
    self.Slider.value = 0
    self.DistanceLabel.text = ""
    LuaOlympicGamesMgr.m_CurlingDistanceList = {}
end

function LuaCurlingOlympicWinterGamesPlayWnd:CurlingOlympicWinterGamesOnEnterEmbracing()
    self.Button.gameObject:SetActive(true)
end

function LuaCurlingOlympicWinterGamesPlayWnd:CurlingOlympicWinterGamesOnLeaveEmbracing()
    self.Button.gameObject:SetActive(false)
    self.ProgressBar.gameObject:SetActive(false)
    self.CancelDot.gameObject:SetActive(false)
end

function LuaCurlingOlympicWinterGamesPlayWnd:OnCurlingOlympicGamesThrowBall(dist)
    self.StopFx:DestroyFx()
    self.Slider.gameObject:SetActive(true)

    local val = dist / 64
    local range = WinterOlympic_CurlingSetting.GetData().ProgressDistRange
    local leftVal, rightVal = range[0], range[1]
    if rightVal == leftVal then return end
    local startVal = self.Slider.value * (rightVal - leftVal) + leftVal
    local endVal =  val
    LuaTweenUtils.DOKill(self.Slider.transform, true)
    local tweener = LuaTweenUtils.TweenFloat(startVal, endVal, 0.5, function ( value )
        self.DistanceLabel.text = value > 0 and SafeStringFormat3(LocalString.GetString("%.1f米"),value) or LocalString.GetString("无效")
		self.Slider.value = (value - leftVal) / (rightVal - leftVal)
	end)
    LuaTweenUtils.SetTarget(tweener,self.Slider.transform)
end

function LuaCurlingOlympicWinterGamesPlayWnd:OnCurlingOlympicGamesRoundStop(dist)
    self:OnCurlingOlympicGamesThrowBall(dist)
    self.StopFx:LoadFx("fx/ui/prefab/UI_levelup_xiru.prefab")
    self:CancelHideSliderTick()
    self.m_HideSliderTick = RegisterTickOnce(function ()
        self.Slider.gameObject:SetActive(false)
        self.Slider.value = 0
        self.DistanceLabel.text = ""
    end,3000)
end

function LuaCurlingOlympicWinterGamesPlayWnd:CancelHideSliderTick()
    if self.m_HideSliderTick then
        UnRegisterTick(self.m_HideSliderTick)
        self.m_HideSliderTick = nil
    end
end

function LuaCurlingOlympicWinterGamesPlayWnd:ShowProgressBar()
    LuaTweenUtils.DOKill(self.transform, false)
    local tweener = LuaTweenUtils.TweenInt(0, 200, 2, function (val)
		self.ProgressBar.value = (val <= 100) and (val/100) or (2 - val/100)
	end)
    LuaTweenUtils.SetLoops(tweener, -1)
    LuaTweenUtils.SetTarget(tweener, self.transform)
end

--@region UIEvent

function LuaCurlingOlympicWinterGamesPlayWnd:OnPress(go, isPressed)
    self.ProgressBar.gameObject:SetActive(isPressed)
    self.CancelDot.gameObject:SetActive(isPressed)
    self.ControlCircle.gameObject:SetActive(isPressed)
    self.Dot.gameObject:SetActive(isPressed)
    if isPressed then
        self:ShowProgressBar()
        self.ControlCircle.color = NGUIText.ParseColor24("5DCEFF", 0)
        self.m_IsCancelCast = false
        self.Dot.transform.localPosition = Vector3.zero
    else
        if not self.m_IsCancelCast then
            local direction = CClientMainPlayer.Inst.Dir 
            if direction < 0 then direction = direction + 360 end
            Gac2Gas.Curling_ThrowBall(self.ProgressBar.value * 100,direction)
            LuaTweenUtils.DOKill(self.transform, false)
        end
    end
end

function LuaCurlingOlympicWinterGamesPlayWnd:OnDrag(go, delta)
    local currentPos = UICamera.currentTouch.pos
    local worldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(Vector3(currentPos.x,currentPos.y,0))
    local pos = self.ControlCircle.transform:InverseTransformPoint(worldPos)
    pos = self:RestrainPos(pos)
    self.m_LastScreenPos = Vector3(currentPos.x, currentPos.y, currentPos.z)
    self.Dot.transform.localPosition = Vector3(pos.x, pos.y, 0)
    if CClientMainPlayer.Inst then
        CClientMainPlayer.Inst.Dir = self:ToRotation(self.Dot.transform.localPosition)
    end
    local ray = UICamera.currentCamera:ScreenPointToRay(self.m_LastScreenPos)
    local mask = UICamera.currentCamera.cullingMask
    local hits = Physics.RaycastAll(ray,  2147483647, mask)
    for i = 0,  hits.Length - 1 do
        if hits[i].collider.gameObject == self.CancelDot.gameObject then
            if not self.m_DoubleStickIsCancelCast then
                self.m_DoubleStickIsCancelCast = true
                self.ControlCircle.color = NGUIText.ParseColor24("FF3B3B", 0)
                self.m_IsCancelCast = true
            end
            return
        end
    end
    if self.m_DoubleStickIsCancelCast then
        self.m_DoubleStickIsCancelCast = false
        self.ControlCircle.color = NGUIText.ParseColor24("5DCEFF", 0)
        self.m_IsCancelCast = false
    end
end
--@endregion UIEvent
function LuaCurlingOlympicWinterGamesPlayWnd:RestrainPos(vect)
    local s = vect.x * vect.x + vect.y * vect.y
    local t = self.m_Radius * self.m_Radius
    if s > t and s ~= 0 then
        local scale = math.sqrt(t / s)
        return Vector3(vect.x * scale, vect.y * scale,0)
    end
    return vect
end

function LuaCurlingOlympicWinterGamesPlayWnd:CalcFinalVector(pos)
    local r = 0
    if CMainCamera.Inst then
        r = CMainCamera.Inst.transform.rotation.eulerAngles.y
    end
    local q = Quaternion.Euler(0, r - 70, 0)
    local v = Vector3(pos.x, 0, pos.y)
    v = CommonDefs.op_Multiply_Quaternion_Vector3(q,v)
    return Vector2(v.x / self.m_Radius, v.z / self.m_Radius)
end

function LuaCurlingOlympicWinterGamesPlayWnd:ToRotation(pos)
    local y = 0
    if pos.x > 0 then
        y = math.atan(pos.y / pos.x) * 57.3
    elseif pos.x < 0 then
        y = math.atan(pos.y / pos.x) * 57.3 + 180
    elseif pos.y > 0 then
        y = 90
    else
        y = -90
    end
    return y
end
