local EnumWarnFXType = import "L10.Game.EnumWarnFXType"
local CClientFurniture=import "L10.Game.CClientFurniture"
local CEffectMgr = import "L10.Game.CEffectMgr"
local Zhuangshiwu_Zhuangshiwu= import "L10.Game.Zhuangshiwu_Zhuangshiwu"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local CUICenterOnChild=import "L10.UI.CUICenterOnChild"
local LuaUtils=import "LuaUtils"
local CUITexture=import "L10.UI.CUITexture"
local QnSelectableButton=import "L10.UI.QnSelectableButton"

CLuaWinterHouseUnlockWnd=class()
RegistClassMember(CLuaWinterHouseUnlockWnd, "m_ModelTextureLoader")
RegistClassMember(CLuaWinterHouseUnlockWnd, "m_ModelTexture")
RegistClassMember(CLuaWinterHouseUnlockWnd, "m_ItemTemplate")
RegistClassMember(CLuaWinterHouseUnlockWnd, "m_SelectedIndex")
RegistClassMember(CLuaWinterHouseUnlockWnd, "m_TemplateIds")
RegistClassMember(CLuaWinterHouseUnlockWnd, "m_ItemLookup")
RegistClassMember(CLuaWinterHouseUnlockWnd, "m_TemplateId")
-- RegistClassMember(CLuaWinterHouseUnlockWnd, "m_TemplateId")
RegistClassMember(CLuaWinterHouseUnlockWnd, "m_PriceLabel")
RegistClassMember(CLuaWinterHouseUnlockWnd, "m_CostSprite")
-- 
RegistClassMember(CLuaWinterHouseUnlockWnd, "m_TatalCost")
RegistClassMember(CLuaWinterHouseUnlockWnd, "m_SwitchButton")
-- RegistClassMember(CLuaWinterHouseUnlockWnd, "m_StatusLookup")
RegistClassMember(CLuaWinterHouseUnlockWnd, "m_DescLabel")
RegistClassMember(CLuaWinterHouseUnlockWnd, "m_WinterHousePrice")

-- CLuaWinterHouseUnlockWnd.m_StatusLookup = {}

function CLuaWinterHouseUnlockWnd:Init()
    -- self.m_StatusLookup={}
    self.m_TemplateIds={}
    self.m_ItemLookup={}

    self.m_WinterHousePrice = WinterHouse_Setting.GetData().WinterHousePrice

    self.m_DescLabel = FindChild(self.transform,"DescLabel"):GetComponent(typeof(UILabel))


    self.m_ItemTemplate = FindChild(self.transform,"ItemTemplate").gameObject
    self.m_ItemTemplate:SetActive(false)
    self.m_PriceLabel = FindChild(self.transform,"PriceLabel"):GetComponent(typeof(UILabel))
    self.m_CostSprite = FindChild(self.transform,"CostSprite"):GetComponent(typeof(UISprite))
    self.m_SwitchButton = FindChild(self.transform,"SwitchButton"):GetComponent(typeof(QnSelectableButton))
    self.m_SwitchButton:SetSelected(true,false)
    self.m_SwitchButton.OnButtonSelected = DelegateFactory.Action_bool(function(b)
        if b then
            CLuaWinterHouseChooseWnd.m_StatusLookup[self.m_TemplateId] = nil
        else
            CLuaWinterHouseChooseWnd.m_StatusLookup[self.m_TemplateId] = true
        end
        --刷新一下消耗
        self:RefreshCost()
    end)

    self.m_ModelTexture = FindChild(self.transform,"Preview"):GetComponent(typeof(CUITexture))

    self.m_ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        self:LoadModel(ro)
    end)

    WinterHouse_Zhuangshiwu.ForeachKey(function(k)
        local wh = WinterHouse_Zhuangshiwu.GetData(k)
        if wh.Currency==1 then
            table.insert( self.m_TemplateIds,k )
        end
    end)

    local grid = FindChild(self.transform,"Grid").gameObject
    local centerOnChild = grid:GetComponent(typeof(CUICenterOnChild))
    centerOnChild.onCenter = LuaUtils.OnCenterCallback(function(go)
        for k,v in pairs(self.m_ItemLookup) do
            local nameLabel = k.transform:Find("Label"):GetComponent(typeof(UILabel))
            if k==go then
                nameLabel.color = Color(1,1,1,1)
            else
                nameLabel.color = Color(0,1,0,1)
            end
        end
        self:OnSelected(self.m_ItemLookup[go])
    end)
    local onclick = DelegateFactory.VoidDelegate(function(go)
        centerOnChild:CenterOnInstant(go.transform)
    end)
    for i,v in ipairs(self.m_TemplateIds) do
        local go = NGUITools.AddChild(grid,self.m_ItemTemplate)
        go:SetActive(true)

        local nameLabel = go.transform:Find("Label"):GetComponent(typeof(UILabel))
        local zsw = Zhuangshiwu_Zhuangshiwu.GetData(v)
        nameLabel.text = zsw.Name

        self.m_ItemLookup[go] = i
        UIEventListener.Get(go).onClick = onclick
    end
    centerOnChild:CenterOnInstant(grid.transform:GetChild(0))
    self:RefreshCost()
end

function CLuaWinterHouseUnlockWnd:OnSelected(index)
    local templateId = self.m_TemplateIds[index]
    if templateId~=self.m_TemplateId then
        self.m_TemplateId = templateId
        local wh = WinterHouse_Zhuangshiwu.GetData(templateId)
        self.m_PriceLabel.text=tostring(wh.Price)
        if wh.Currency==1 then
            self.m_CostSprite.spriteName = "packagewnd_yinliang"
        elseif wh.Currency==2 then
            self.m_CostSprite.spriteName = "packagewnd_lingyu"
        elseif wh.Currency==3 then
            self.m_CostSprite.spriteName = "packagewnd_yinpiao"
        else
            self.m_CostSprite.spriteName = nil
        end
        if CLuaWinterHouseChooseWnd.m_StatusLookup[self.m_TemplateId] then
            self.m_SwitchButton:SetSelected(false,false)
        else
            self.m_SwitchButton:SetSelected(true,false)
        end

        self.m_ModelTexture.mainTexture=CUIManager.CreateModelTexture("__FurniturePreview__", self.m_ModelTextureLoader,180,0.05,-1,4.66,false,true,1)

    end
end

function CLuaWinterHouseUnlockWnd:LoadModel(ro)
    local data = Zhuangshiwu_Zhuangshiwu.GetData(self.m_TemplateId)
    local resName = data.ResNameXue
    local resid = SafeStringFormat3("_%02d",data.ResIdXue)
    local prefabname = "Assets/Res/Character/Jiayuan/" .. data.ResNameXue .. "/Prefab/" .. data.ResNameXue .. resid .. ".prefab"
    if data.ProgramSnow>0 then
        resid = SafeStringFormat3("_%02d",data.ResId)
        prefabname = "Assets/Res/Character/Jiayuan/" .. data.ResName .. "/Prefab/" .. data.ResName .. resid .. ".prefab"
        ro:AddLoadAllFinishedListener(DelegateFactory.Action_CRenderObject(function(renderObject)
            CClientFurniture.SetSnowLevel(ro,true,self.m_TemplateId)
        end))
    end
    ro:LoadMain(prefabname)

    
    if data.FXXue>0 then
        local fx = CEffectMgr.Inst:AddObjectFX(data.FXXue, ro, 0,1,1,nil,false,EnumWarnFXType.None, Vector3.zero, Vector3.zero)
        ro:AddFX("SelfFx", fx)
    end

    local whdata = WinterHouse_Zhuangshiwu.GetData(self.m_TemplateId)
    ro.Scale = whdata.ModelScale
end

function CLuaWinterHouseUnlockWnd:OnDestroy()
    self.m_ModelTexture.mainTexture=nil
    CUIManager.DestroyModelTexture("__FurniturePreview__")
end

function CLuaWinterHouseUnlockWnd:RefreshCost()
    local total=0--self.m_WinterHousePrice
    for i,v in ipairs(self.m_TemplateIds) do
        local wh = WinterHouse_Zhuangshiwu.GetData(v)
        if wh.Currency==1 and not CLuaWinterHouseChooseWnd.m_StatusLookup[v] then
            total = total + wh.Price
        end
    end
    self.m_DescLabel.text = SafeStringFormat3(LocalString.GetString("可编辑开关来决定是否解锁，当前解锁装饰物雪景共花费%d#287"),total)
    g_ScriptEvent:BroadcastInLua("UpdateWinterHouseCost")
end
