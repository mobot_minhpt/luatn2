local UILabel = import "UILabel"
local QnButton = import "L10.UI.QnButton"
local CUITexture = import "L10.UI.CUITexture"
local DelegateFactory = import "DelegateFactory"

CLuaDiYuShiKongWnd = class()

RegistClassMember(CLuaDiYuShiKongWnd, "m_EnterBtn")
RegistClassMember(CLuaDiYuShiKongWnd, "m_DescBtn")

RegistClassMember(CLuaDiYuShiKongWnd, "m_DescLabel")
RegistClassMember(CLuaDiYuShiKongWnd, "m_Rule1TitleLabel")
RegistClassMember(CLuaDiYuShiKongWnd, "m_Rule1Texture")
RegistClassMember(CLuaDiYuShiKongWnd, "m_Rule1DescLabel")
RegistClassMember(CLuaDiYuShiKongWnd, "m_Rule2TitleLabel")
RegistClassMember(CLuaDiYuShiKongWnd, "m_Rule2Texture")
RegistClassMember(CLuaDiYuShiKongWnd, "m_Rule2DescLabel")
RegistClassMember(CLuaDiYuShiKongWnd, "m_Rule3TitleLabel")
RegistClassMember(CLuaDiYuShiKongWnd, "m_Rule3Texture")
RegistClassMember(CLuaDiYuShiKongWnd, "m_Rule3DescLabel")

function CLuaDiYuShiKongWnd:Awake()
    self:InitComponents()
end

function CLuaDiYuShiKongWnd:InitComponents()
    self.m_EnterBtn = self.transform:Find("Anchor/EnterBtn"):GetComponent(typeof(QnButton))
    self.m_DescBtn  = self.transform:Find("Anchor/RuleDescBtn"):GetComponent(typeof(QnButton))

    self.m_DescLabel = self.transform:Find("Anchor/Label"):GetComponent(typeof(UILabel))

    self.m_Rule1TitleLabel = self.transform:Find("Anchor/Rules/Rule1/Title1"):GetComponent(typeof(UILabel))
    self.m_Rule1Texture = self.transform:Find("Anchor/Rules/Rule1/Texture1"):GetComponent(typeof(CUITexture))
    self.m_Rule1DescLabel = self.transform:Find("Anchor/Rules/Rule1/Desc1"):GetComponent(typeof(UILabel))

    self.m_Rule2TitleLabel = self.transform:Find("Anchor/Rules/Rule2/Title2"):GetComponent(typeof(UILabel))
    self.m_Rule2Texture = self.transform:Find("Anchor/Rules/Rule2/Texture2"):GetComponent(typeof(CUITexture))
    self.m_Rule2DescLabel = self.transform:Find("Anchor/Rules/Rule2/Desc2"):GetComponent(typeof(UILabel))

    self.m_Rule3TitleLabel = self.transform:Find("Anchor/Rules/Rule3/Title3"):GetComponent(typeof(UILabel))
    self.m_Rule3Texture = self.transform:Find("Anchor/Rules/Rule3/Texture3"):GetComponent(typeof(CUITexture))
    self.m_Rule3DescLabel = self.transform:Find("Anchor/Rules/Rule3/Desc3"):GetComponent(typeof(UILabel))

end

function CLuaDiYuShiKongWnd:Init()
    self.m_DescLabel.text = g_MessageMgr:FormatMessage("WuYi2020_DYSK_zhujiemian")


    self.m_Rule1TitleLabel.text = g_MessageMgr:FormatMessage("WuYi2020_DYSK_diyilie")
    --self.m_Rule1Texture.LoadTexture()
    self.m_Rule1DescLabel.text = g_MessageMgr:FormatMessage("WuYi2020_DYSK_diyilie_1")

    self.m_Rule2TitleLabel.text = g_MessageMgr:FormatMessage("WuYi2020_DYSK_dierlie")
    --self.m_Rule2Texture.LoadTexture()
    self.m_Rule2DescLabel.text = g_MessageMgr:FormatMessage("WuYi2020_DYSK_dierlie_1")

    self.m_Rule3TitleLabel.text = g_MessageMgr:FormatMessage("WuYi2020_DYSK_disanlie")
    --self.m_Rule3DescLabelTexture.LoadTexture()
    self.m_Rule3DescLabel.text = g_MessageMgr:FormatMessage("WuYi2020_DYSK_disanlie_1")

    self.m_EnterBtn.OnClick = DelegateFactory.Action_QnButton(function (btn) self:OnEnterBtnClick() end)
    self.m_DescBtn.OnClick = DelegateFactory.Action_QnButton(function (btn) self:OnDescBtnClick() end)
end

function CLuaDiYuShiKongWnd:OnEnterBtnClick()
    Gac2Gas.EnterDiYuShiKongGameplay()
end

function CLuaDiYuShiKongWnd:OnDescBtnClick()
    g_MessageMgr:ShowMessage("WuYi2020_DYSK_Tips")
end
