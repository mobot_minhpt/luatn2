require("common/common_include")
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local CommonDefs = import "L10.Game.CommonDefs"
local LocalString = import "LocalString"
local UICommonDef = import "L10.UI.CUICommonDef"
local Extensions = import "Extensions"
local CUITexture = import "L10.UI.CUITexture"
local CChatHelper = import "L10.UI.CChatHelper"
local EChatPanel = import "L10.Game.EChatPanel"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"

LuaPreExistenceMarridgeAlbumWnd = class()
RegistClassMember(LuaPreExistenceMarridgeAlbumWnd, "m_CellTable")
RegistClassMember(LuaPreExistenceMarridgeAlbumWnd, "m_CellTemplate")
RegistClassMember(LuaPreExistenceMarridgeAlbumWnd, "m_ShareGroup")
RegistClassMember(LuaPreExistenceMarridgeAlbumWnd, "m_ShareBtn1")
RegistClassMember(LuaPreExistenceMarridgeAlbumWnd, "m_ShareBtn2")

function LuaPreExistenceMarridgeAlbumWnd:Awake()
    self.m_CellTable = self.transform:Find("Anchor/Table").gameObject
    self.m_CellTemplate = self.transform:Find("Anchor/PreExistenceTemplate").gameObject
    self.m_ShareGroup = self.transform:Find("Anchor/ShareGroup").gameObject
    self.m_ShareBtn1 = self.transform:Find("Anchor/ShareGroup/ShareBtn1").gameObject
    self.m_ShareBtn2 = self.transform:Find("Anchor/ShareGroup/ShareBtn2").gameObject
end

function LuaPreExistenceMarridgeAlbumWnd:Init()
    LuaValentine2019Mgr.m_AlbumWnd = self.gameObject

    self.m_CellTemplate:SetActive(false)
    self.m_ShareGroup:SetActive(false)

    if LuaValentine2019Mgr.m_PreExistenceInfo then --通过分享链接打开
        self:UpdateCells()
    else
        Gac2Gas.RequestValentineQSYYInfo() --发RPC获取数据
    end
end

function LuaPreExistenceMarridgeAlbumWnd:UpdateCells()
    Extensions.RemoveAllChildren(self.m_CellTable.transform)

    if
        not LuaValentine2019Mgr.m_PreExistenceInfo or
            #LuaValentine2019Mgr.m_PreExistenceInfo ~= LuaValentine2019Mgr.m_TotExistenceGeneration
     then
        return
    end

    for i = 1, LuaValentine2019Mgr.m_TotExistenceGeneration do
        local curCell = UICommonDef.AddChild(self.m_CellTable, self.m_CellTemplate)
        local curTemplateID = LuaValentine2019Mgr.m_PreExistenceInfo[i]
        self:InitOneCell(curCell, i, curTemplateID ~= 0, curTemplateID)
    end

    self.m_CellTable:GetComponent(typeof(UIGrid)):Reposition()

    local showSharebtn = LuaValentine2019Mgr.m_ShowPreExistenceShare
    self.m_ShareGroup:SetActive(showSharebtn)
    if showSharebtn then
        CommonDefs.AddOnClickListener(
            self.m_ShareBtn1,
            DelegateFactory.Action_GameObject(
                function(go)
                    self:OnClickShareToChatButton(go)
                end
            ),
            false
        )

        CommonDefs.AddOnClickListener(
            self.m_ShareBtn2,
            DelegateFactory.Action_GameObject(
                function(go)
                    self:OnClickNormalShareButton(go)
                end
            ),
            false
        )
    end
end

function LuaPreExistenceMarridgeAlbumWnd:InitOneCell(go, curNum, unlocked, templateID)
    local NumberLabel = go.transform:Find("NumberBG/NumberLabel"):GetComponent(typeof(UILabel))
    local NumberCornerLabel = go.transform:Find("NumberBG/NumberTexture/Label"):GetComponent(typeof(UILabel))

    local UnlockedGroup = go.transform:Find("DetailArea/AvatarCell/Unlocked").gameObject
    local LockedGroup = go.transform:Find("DetailArea/AvatarCell/Locked").gameObject
    local avatarTexture =
        go.transform:Find("DetailArea/AvatarCell/Unlocked/AvatarTexture"):GetComponent(typeof(CUITexture))
    local detailBtn = go.transform:Find("DetailArea/DetailBtn").gameObject
    local TipLabel = go.transform:Find("DetailArea/TipLabel").gameObject

    local ChineseNum = Extensions.ToChinese(curNum)
    NumberLabel.text = LocalString.GetString("第") .. ChineseNum .. LocalString.GetString("世")
    NumberCornerLabel.text = ChineseNum

    UnlockedGroup:SetActive(unlocked)
    LockedGroup:SetActive(not unlocked)
    detailBtn:SetActive(unlocked)
    TipLabel:SetActive(not unlocked)

    if unlocked then
        local data = Valentine2019_QSYYEnding.GetData(templateID)
        if data then
            avatarTexture:LoadNPCPortrait(data.Icon, false)

            CommonDefs.AddOnClickListener(
                detailBtn,
                DelegateFactory.Action_GameObject(
                    function(go)
                        self:OnClickDetailArea(go, curNum, templateID)
                    end
                ),
                false
            )
        end
    end

    go:SetActive(true)
end

function LuaPreExistenceMarridgeAlbumWnd:OnClickDetailArea(go, num, templateID)
    LuaValentine2019Mgr.ShowPreExistenceMarridgeDetailWnd(
        LuaValentine2019Mgr.m_ShowPreExistenceShare,
        num,
        templateID,
        LuaValentine2019Mgr.m_PreExistencePlayerID,
        LuaValentine2019Mgr.m_PreExistencePlayerName
    )
    self.gameObject:SetActive(false)
end

function LuaPreExistenceMarridgeAlbumWnd:OnClickShareToChatButton(go)
    local buttonName = g_MessageMgr:FormatMessage("SHARE_QSYY_TOCHAT", nil)

    local infoString = ""
    for i, v in ipairs(LuaValentine2019Mgr.m_PreExistenceInfo) do
        if i ~= 1 then
            infoString = infoString .. "+"
        end
        infoString = infoString .. tostring(v)
    end

    local msg =
        SafeStringFormat3(
        "<link button=%s,PreExistenceShareClick,%s,%s,%s>",
        buttonName,
        infoString,
        tostring(LuaValentine2019Mgr.m_PreExistencePlayerID),
        tostring(LuaValentine2019Mgr.m_PreExistencePlayerName)
    )
    CChatHelper.SendMsgWithFilterOption(EChatPanel.World, msg, 0, true)
end

function LuaPreExistenceMarridgeAlbumWnd:OnClickNormalShareButton(go)
    UICommonDef.CaptureScreen(
        "screenshot",
        true,
        false,
        self.m_ShareGroup,
        DelegateFactory.Action_string_bytes(
            function(fullPath, jpgBytes)
                ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
            end
        ),
        false
    )
end

function LuaPreExistenceMarridgeAlbumWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncValentineQSYYInfoFinish", self, "OnSyncValentineQSYYInfo")
end

function LuaPreExistenceMarridgeAlbumWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncValentineQSYYInfoFinish", self, "OnSyncValentineQSYYInfo")
end

function LuaPreExistenceMarridgeAlbumWnd:OnSyncValentineQSYYInfo()
    self:UpdateCells()
end

function LuaPreExistenceMarridgeAlbumWnd:OnDestroy()
    LuaValentine2019Mgr.m_AlbumWnd = nil
end
