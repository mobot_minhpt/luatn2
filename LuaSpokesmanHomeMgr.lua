--[[
    代言人家园模块，主要实现功能如下（如有增加请注明）
    1、实现在前3个阶段，已经后续阶段没有任何设计师装修的情况下，加载客户端装饰物的功能
    2、如果当前的设计师为该玩家，允许玩家进行装修
]]


local CClientFurnitureMgr = import "L10.Game.CClientFurnitureMgr"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local Vector3 = import "UnityEngine.Vector3"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"

CLuaSpokesmanHomeMgr = {}

--第四阶段，可以进行装修的玩家
CLuaSpokesmanHomeMgr.DesignPlayerID = 0

--？？
CLuaSpokesmanHomeMgr.FanPlayerID = 0

--[[
    @desc:获取代言人家园的进度
    author:{author}
    time:2020-06-23 10:05:50
    @return:
]]
function CLuaSpokesmanHomeMgr.GetProgress()
    return CClientHouseMgr.Inst.mCurExtraInfo.SpokesmanHouseProgress
end

function CLuaSpokesmanHomeMgr.AddFurniture(id, ftid, x, y, z, rot)
    return CClientFurnitureMgr.Inst:AddFurniture(id, ftid, x, y, z, rot,"",false,0,127,false,0,0,0);
end

function CLuaSpokesmanHomeMgr.AddDecoration(id,ftid,pid,slotIdx)
    CClientFurnitureMgr.Inst:AddDecoration(id,ftid,pid,slotIdx,false,0,0,0)
end

--[[
    @desc: 进入代言人家园,加载本地的家具配置
    author:{author}
    time:2020-06-22 16:02:42
    @return:
]]
function CLuaSpokesmanHomeMgr.OnEnterSpokemanHome()
    local sceneType = CClientHouseMgr.Inst:GetCurSceneType()
    if sceneType == 0 then return end -- 不在家园

    if CClientHouseMgr.Inst.mCurExtraInfo.IsSpokesmanHouse <= 0 then return end --不是代言人的家园

    local progress = CLuaSpokesmanHomeMgr.GetProgress()
    if progress <= 0 or progress > 3 then --只有1到3的进度才会有加载虚拟装饰物的功能
        return
    end
    
    local pinfo = Spokesman_Progress.GetData(progress)
    local str = pinfo.JiaJuInfo
    local strarray = g_LuaUtil:StrSplit(str, ";")
    

    for i = 1,#strarray do
        str = strarray[i]
        if sceneType == 1 then --正屋房间
            CLuaSpokesmanHomeMgr.ProcessRoomJiaJu(str)
        elseif sceneType == 2 then--庭院
            CLuaSpokesmanHomeMgr.ProcessYardJiaJu(str)
        elseif sceneType == 3 then--厢房 
            CLuaSpokesmanHomeMgr.ProcessXiangFangJiaJu(str)
        end
    end
end

function CLuaSpokesmanHomeMgr.ProcessRoomJiaJu(str)
    local format1 = "rf(%d+),([%d%.%-]+),([%d%.%-]+),([%d%.%-]+),(%d+),(%d+),(%d+);"
    local format2 = "rf(%d+),([%d%.%-]+),([%d%.%-]+),([%d%.%-]+),(%d+),(%d+),(%d+),(%d+),(%d+),(%d+);"
    for id, x, y, z, rot, ftid, scale in string.gmatch(str, format1) do
        id, x, y, z, rot, ftid, scale = tonumber(id), tonumber(x), tonumber(y), tonumber(z), tonumber(rot), tonumber(ftid), tonumber(scale)
        if id and x and y and z and rot and ftid and scale then
            CLuaSpokesmanHomeMgr.AddFurniture(id, ftid, x, y, z, rot)
        end
    end
    for id, x, y, z, rot, ftid, scale, rx,ry,rz in string.gmatch(str, format2) do
        id, x, y, z, rot, ftid, scale = tonumber(id), tonumber(x), tonumber(y), tonumber(z), tonumber(rot), tonumber(ftid), tonumber(scale)
        rx,ry,rz = tonumber(rx), tonumber(ry), tonumber(rz)
        if id and x and y and z and rot and ftid and scale then
            local fur = CLuaSpokesmanHomeMgr.AddFurniture(id, ftid, x, y, z, rot)
            if rx and ry and rz then
                fur.ServerRX = rx;
                fur.ServerRY = ry;
                fur.ServerRZ = rz;
                fur.RO.transform.localEulerAngles = Vector3(rx / 256 * 360, ry / 256 * 360, rz / 256 * 360);
            end
        end
    end

    for id, pid, slotIdx, ftid in string.gmatch(str, "rd(%d+),(%d+),(%d+),(%d+);") do
        id, pid, slotIdx, ftid = tonumber(id), tonumber(pid), tonumber(slotIdx), tonumber(ftid)
        if id and pid and slotIdx and ftid then
            CLuaSpokesmanHomeMgr.AddDecoration(id,ftid,pid,slotIdx)
        end
    end
end

function CLuaSpokesmanHomeMgr.ProcessXiangFangJiaJu(str)
    local format1 = "xf(%d+),([%d%.%-]+),([%d%.%-]+),([%d%.%-]+),(%d+),(%d+),(%d+);"
    local format2 = "xf(%d+),([%d%.%-]+),([%d%.%-]+),([%d%.%-]+),(%d+),(%d+),(%d+),(%d+),(%d+),(%d+);"
    for id, x, y, z, rot, ftid, scale in string.gmatch(str, format1) do
        id, x, y, z, rot, ftid, scale = tonumber(id), tonumber(x), tonumber(y), tonumber(z), tonumber(rot), tonumber(ftid), tonumber(scale)
        if id and x and y and z and rot and ftid and scale then
            CLuaSpokesmanHomeMgr.AddFurniture(id, ftid, x, y, z, rot)
        end
    end
    for id, x, y, z, rot, ftid, scale, rx,ry,rz in string.gmatch(str, format2) do
        id, x, y, z, rot, ftid, scale = tonumber(id), tonumber(x), tonumber(y), tonumber(z), tonumber(rot), tonumber(ftid), tonumber(scale)
        rx,ry,rz = tonumber(rx), tonumber(ry), tonumber(rz)
        if id and x and y and z and rot and ftid and scale then
            local fur = CLuaSpokesmanHomeMgr.AddFurniture(id, ftid, x, y, z, rot)
            if rx and ry and rz then
                fur.ServerRX = rx;
                fur.ServerRY = ry;
                fur.ServerRZ = rz;
                fur.RO.transform.localEulerAngles = Vector3(rx / 256 * 360, ry / 256 * 360, rz / 256 * 360);
            end
        end
    end

    for id, pid, slotIdx, ftid in string.gmatch(str, "xd(%d+),(%d+),(%d+),(%d+);") do
        id, pid, slotIdx, ftid = tonumber(id), tonumber(pid), tonumber(slotIdx), tonumber(ftid)
        if id and pid and slotIdx and ftid then
            CLuaSpokesmanHomeMgr.AddDecoration(id,ftid,pid,slotIdx)
        end
    end
end

function CLuaSpokesmanHomeMgr.ProcessYardJiaJu(str)
    local format1 = "yf(%d+),([%d%.%-]+),([%d%.%-]+),([%d%.%-]+),(%d+),(%d+),(%d+)"
    local format2 = "yf(%d+),([%d%.%-]+),([%d%.%-]+),([%d%.%-]+),(%d+),(%d+),(%d+),(%d+),(%d+),(%d+)"
        
    for id, x, y, z, rot, ftid, scale in string.gmatch(str, format1) do
        id, x, y, z, rot, ftid, scale = tonumber(id), tonumber(x), tonumber(y), tonumber(z), tonumber(rot), tonumber(ftid), tonumber(scale)
        if id and x and y and z and rot and ftid and scale then
            CLuaSpokesmanHomeMgr.AddFurniture(id, ftid, x, y, z, rot)
        end
    end
    for id, x, y, z, rot, ftid, scale, rx,ry,rz in string.gmatch(str, format2) do
        id, x, y, z, rot, ftid, scale = tonumber(id), tonumber(x), tonumber(y), tonumber(z), tonumber(rot), tonumber(ftid), tonumber(scale)
        rx,ry,rz = tonumber(rx), tonumber(ry), tonumber(rz)
        if id and x and y and z and rot and ftid and scale then
            local fur = CLuaSpokesmanHomeMgr.AddFurniture(id, ftid, x, y, z, rot)
            if rx and ry and rz then
                fur.ServerRX = rx;
                fur.ServerRY = ry;
                fur.ServerRZ = rz;
                fur.RO.transform.localEulerAngles = Vector3(rx / 256 * 360, ry / 256 * 360, rz / 256 * 360);
            end
        end
    end

    for id, pid, slotIdx, ftid in string.gmatch(str, "yd(%d+),(%d+),(%d+),(%d+)") do
        id, pid, slotIdx, ftid = tonumber(id), tonumber(pid), tonumber(slotIdx), tonumber(ftid)
        if id and pid and slotIdx and ftid then
            CLuaSpokesmanHomeMgr.AddDecoration(id,ftid,pid,slotIdx)
        end
    end
end


--designid,fansid是2个阶段的房客 都可以操作代言人家园
function CLuaSpokesmanHomeMgr.SyncSpokesmanOperatorId(designid,fansid,fake)--服务器可以保证这个时候，当前的家园信息客户端已经收到
    CLuaSpokesmanHomeMgr.DesignPlayerID = designid
    CLuaSpokesmanHomeMgr.FanPlayerID = fansid

    if CLuaSpokesmanHomeMgr.IsOperateSpokesmanHouse() then
        local inst = CClientHouseMgr.Inst
        if inst.mCurFurnitureProp then
            inst.mBasicProp = inst.mCurBasicProp
            inst.mConstructProp = inst.mCurConstruectProp
            inst.mProgressProp = inst.mCurProgressProp
            inst.mGardenProp = inst.mCurGardenProp
            inst.mFurnitureProp = inst.mCurFurnitureProp
            inst.mFurnitureProp2 = inst.mCurFurnitureProp2
            inst.mExtraProp = inst.mCurExtraInfo
        end
    end

    if fake > 0 then
        CLuaSpokesmanHomeMgr.OnEnterSpokemanHome()
    end
end


function CLuaSpokesmanHomeMgr.IsOperateSpokesmanHouse()
    local isSpokesmanHouse = false
    if CClientHouseMgr.Inst:IsPlayerInHouse() and CClientHouseMgr.Inst.mCurExtraInfo and CClientHouseMgr.Inst.mCurExtraInfo.IsSpokesmanHouse >0 then
        local playerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or nil
        if playerId==CLuaSpokesmanHomeMgr.DesignPlayerID or playerId==CLuaSpokesmanHomeMgr.FanPlayerID then
            return true
        end
    end
    return false
end

--如果玩家有权限装修，可以认为是在自己家里
CClientHouseMgr.m_hookIsInOwnHouse = function(this)
    if this:IsPlayerInHouse() then
        if this.mCurBasicProp and CClientMainPlayer.Inst.ItemProp.HouseId == this.mCurBasicProp.Id and this.mCurBasicProp.Id ~="" then
            return true
        end
        if CLuaSpokesmanHomeMgr.IsOperateSpokesmanHouse() then return true end
    end
    return false
end
