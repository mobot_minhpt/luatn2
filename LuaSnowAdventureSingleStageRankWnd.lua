require("common/common_include")
local QnTabButton = import "L10.UI.QnTabButton"
local UITabBar = import "L10.UI.UITabBar"
local CommonDefs = import "L10.Game.CommonDefs"
local Extensions = import "Extensions"
local CRankData = import "L10.UI.CRankData"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local EChatPanel = import "L10.Game.EChatPanel"
local AlignType = import "CPlayerInfoMgr+AlignType"
LuaSnowAdventureSingleStageRankWnd = class()
LuaSnowAdventureSingleStageRankWnd.InitStageType = nil
--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSnowAdventureSingleStageRankWnd, "Tabs", "Tabs", UITabBar)
RegistChildComponent(LuaSnowAdventureSingleStageRankWnd, "Tab1", "Tab1", QnTabButton)
RegistChildComponent(LuaSnowAdventureSingleStageRankWnd, "Tab2", "Tab2", QnTabButton)
RegistChildComponent(LuaSnowAdventureSingleStageRankWnd, "Tab3", "Tab3", QnTabButton)

--@endregion RegistChildComponent end

function LuaSnowAdventureSingleStageRankWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self:InitWndData()
    self:RefreshConstUI()
    self:RefreshVariableUI()
    self:InitUIEvent()
end

function LuaSnowAdventureSingleStageRankWnd:Init()

end

function LuaSnowAdventureSingleStageRankWnd:InitWndData()
    local rankId = HanJia2023_XuePoLiXianSetting.GetData().SingleModeRankIdList
    local rankIdList = g_LuaUtil:StrSplit(rankId,",")
    self.rankIdList = {}

    for i = 1, #rankIdList do
        table.insert(self.rankIdList, tonumber(rankIdList[i]))
    end
    
    self.tabButtonList = {self.Tab1, self.Tab2, self.Tab3}
    self.selectRankType = LuaSnowAdventureSingleStageRankWnd.InitStageType
    Gac2Gas.QueryRank(self.rankIdList[self.selectRankType])

    self.m_MyObj = self.transform:Find("Anchor/Rank/MyInfo").gameObject
end

function LuaSnowAdventureSingleStageRankWnd:RefreshConstUI()
    self.m_MyObj:SetActive(false)
end

function LuaSnowAdventureSingleStageRankWnd:RefreshVariableUI()
    for i = 1, #self.tabButtonList do
        self.tabButtonList[i].Selected = (self.selectRankType == i)
    end
end

function LuaSnowAdventureSingleStageRankWnd:InitUIEvent()
    self.Tabs.OnTabChange = DelegateFactory.Action_GameObject_int(function (_, index)
        local rankId = self.rankIdList[index+1]
        Gac2Gas.QueryRank(rankId)
    end)
end

function LuaSnowAdventureSingleStageRankWnd:OnEnable()
    g_ScriptEvent:AddListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaSnowAdventureSingleStageRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnRankDataReady", self, "OnRankDataReady")
end

function LuaSnowAdventureSingleStageRankWnd:OnRankDataReady()
    local templateObj = self.transform:Find("Anchor/Rank/AdvView/Pool/Template").gameObject
    templateObj:SetActive(false)
    local grid = self.transform:Find("Anchor/Rank/AdvView/Scroll View/Grid"):GetComponent(typeof(UIGrid))
    self.m_MyObj:SetActive(true)
    self:InitItem(self.m_MyObj.transform, CRankData.Inst.MainPlayerRankInfo, true, true)
    Extensions.RemoveAllChildren(grid.transform)
    for i = 0, CRankData.Inst.RankList.Count - 1 do
        local obj = NGUITools.AddChild(grid.gameObject, templateObj)
        obj:SetActive(true)
        self:InitItem(obj.transform, CRankData.Inst.RankList[i], false, i % 2 == 0)
        UIEventListener.Get(obj).onClick = DelegateFactory.VoidDelegate(function()
            local playerIndex = CRankData.Inst.RankList[i].PlayerIndex
            if playerIndex then
                CPlayerInfoMgr.ShowPlayerPopupMenu(playerIndex, EnumPlayerInfoContext.Undefined,EChatPanel.Undefined,"",nil,Vector3.zero,AlignType.Default)
            end
        end)
    end
    grid:Reposition()
end

function LuaSnowAdventureSingleStageRankWnd:InitItem(rootTrans, data, isMe, isOdd)
    if math.floor(data.Value/100000) == 0 and (not isMe) then
        rootTrans.gameObject:SetActive(false)
        return
    end
    if not isMe then
        rootTrans:Find("PlayerNameLabel"):GetComponent(typeof(UILabel)).text = data.Name
    else
        if CClientMainPlayer.Inst then
            rootTrans:Find("PlayerNameLabel"):GetComponent(typeof(UILabel)).text = CClientMainPlayer.Inst.Name
        end
    end
    local curData = data.Value
    rootTrans:Find("PassStageNumberLabel"):GetComponent(typeof(UILabel)).text = math.floor(curData/100000)
    local consumeSeconds = 100000 - (curData % 100000)

    if math.floor(curData/100000) == 0 then
        rootTrans:Find("ConsumeTimeLabel"):GetComponent(typeof(UILabel)).text = "-"
    else
        --rootTrans:Find("ConsumeTimeLabel"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("%d时%d分%d秒"), math.floor(consumeSeconds / 3600),
        --        math.floor(consumeSeconds % 3600 / 60), math.floor(consumeSeconds%60))
        
        local hour = math.floor(consumeSeconds / 3600)
        local minute = math.floor((consumeSeconds % 3600) / 60 )
        local second = math.floor(consumeSeconds % 60)
        local text = ""
        if hour ~= 0 then
            text = text .. SafeStringFormat3(LocalString.GetString("%d时"), hour)
        end
        if minute ~= 0 then
            text = text .. SafeStringFormat3(LocalString.GetString("%d分"), minute)
        end
        if second ~= 0 then
            text = text .. SafeStringFormat3(LocalString.GetString("%d秒"), second)
        end
        rootTrans:Find("ConsumeTimeLabel"):GetComponent(typeof(UILabel)).text = text
    end

    if not isMe then
        rootTrans:GetComponent(typeof(UISprite)).spriteName = isOdd and Constants.NewOddBgSprite or Constants.NewEvenBgSprite
    end

    local rankSprite = rootTrans:Find("RankSprite"):GetComponent(typeof(UISprite))
    rankSprite.gameObject:SetActive(data.Rank <= 3 and data.Rank >= 1)
    rootTrans:Find("RankLabel").gameObject:SetActive(data.Rank < 1 or data.Rank > 3)
    if data.Rank > 3 then
        rootTrans:Find("RankLabel"):GetComponent(typeof(UILabel)).text = data.Rank
    elseif data.Rank == 0 then
        rootTrans:Find("RankLabel"):GetComponent(typeof(UILabel)).text = LocalString.GetString("未上榜")
    else
        local spriteNames = {"Rank_No.1", "Rank_No.2png", "Rank_No.3png"}
        rankSprite.spriteName = spriteNames[data.Rank]
    end
end

--@region UIEvent

--@endregion UIEvent

