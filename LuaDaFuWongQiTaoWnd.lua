local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local CButton = import "L10.UI.CButton"
local GameObject = import "UnityEngine.GameObject"

LuaDaFuWongQiTaoWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaDaFuWongQiTaoWnd, "Dialog", "Dialog", UILabel)
RegistChildComponent(LuaDaFuWongQiTaoWnd, "ResultLabel", "ResultLabel", UILabel)
RegistChildComponent(LuaDaFuWongQiTaoWnd, "ConfirmButton", "ConfirmButton", CButton)
RegistChildComponent(LuaDaFuWongQiTaoWnd, "OKBtn", "OKBtn", GameObject)
RegistChildComponent(LuaDaFuWongQiTaoWnd, "NOBtn", "NOBtn", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongQiTaoWnd,"m_Select")
RegistClassMember(LuaDaFuWongQiTaoWnd, "m_PlayerHead")
function LuaDaFuWongQiTaoWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	
	UIEventListener.Get(self.ConfirmButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnConfirmButtonClick()
	end)


    --@endregion EventBind end
	self.m_Select = nil
	self.m_PlayerHead = self.transform:Find("Content/PlayerHead").gameObject
end

function LuaDaFuWongQiTaoWnd:Init()
	self.Dialog.text = g_MessageMgr:FormatMessage("DaFuWeng_Beggar_Dialog")
	UIEventListener.Get(self.OKBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    if not LuaDaFuWongMgr.IsMyRound then 
			local id = LuaDaFuWongMgr.CurRoundPlayer
			local name = LuaDaFuWongMgr.PlayerInfo[id].name
			g_MessageMgr:ShowMessage("DaFuWeng_OtherPlayerOperate",name)
			return
		end
		self:OnSelect(true)
	end)
	UIEventListener.Get(self.NOBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    if not LuaDaFuWongMgr.IsMyRound then 
			local id = LuaDaFuWongMgr.CurRoundPlayer
			local name = LuaDaFuWongMgr.PlayerInfo[id].name
			g_MessageMgr:ShowMessage("DaFuWeng_OtherPlayerOperate",name)
			return
		end
		self:OnSelect(false)
	end)

	self:OnSelect(false)
	local totalTime = DaFuWeng_Countdown.GetData(EnumDaFuWengStage.RandomEventBeggar).Time 
    local CountDown = totalTime - LuaDaFuWongMgr.GetCardTime
    local endFunc = function() self:OnConfirmResult() end
	local durationFunc = function(time) 
		if self.ConfirmButton then
			self.ConfirmButton.transform:Find("Label"):GetComponent(typeof(UILabel)).text = SafeStringFormat3(LocalString.GetString("确定(%d)"),time)
		end
	end
	LuaDaFuWongMgr:StartCountDownTick(EnumDaFuWengCountDownStage.RandomEvent,CountDown,durationFunc,endFunc)
	self:ShowPlayerHead()
end

function LuaDaFuWongQiTaoWnd:OnSelect(result)
	if self.m_Select == result then return end
	self.m_Select = result
	local colorcode = {"543524","803916"}
	self.OKBtn.transform:Find("Select").gameObject:SetActive(self.m_Select)
	self.NOBtn.transform:Find("Select").gameObject:SetActive(not self.m_Select)
	
	if not self.m_Select then
		self.ResultLabel.text = g_MessageMgr:FormatMessage("DaFuWeng_Beggar_ChooseNo")
		self.OKBtn.transform:Find("Label"):GetComponent(typeof(UILabel)).color =  NGUIText.ParseColor24(colorcode[1], 0)
		self.NOBtn.transform:Find("Label"):GetComponent(typeof(UILabel)).color =  NGUIText.ParseColor24(colorcode[2], 0)
	else
		self.ResultLabel.text = g_MessageMgr:FormatMessage("DaFuWeng_Beggar_ChooseOK",DaFuWeng_Setting.GetData().BeggarCostMoney)
		self.OKBtn.transform:Find("Label"):GetComponent(typeof(UILabel)).color =  NGUIText.ParseColor24(colorcode[2], 0)
		self.NOBtn.transform:Find("Label"):GetComponent(typeof(UILabel)).color =  NGUIText.ParseColor24(colorcode[1], 0)
	end
end

function LuaDaFuWongQiTaoWnd:OnConfirmResult()
	g_ScriptEvent:BroadcastInLua("OnDaFuWengFinishCurStage",EnumDaFuWengStage.RandomEventBeggar)
    CUIManager.CloseUI(CLuaUIResources.DaFuWongQiTaoWnd)
end
function LuaDaFuWongQiTaoWnd:ShowPlayerHead()
    if LuaDaFuWongMgr.IsMyRound then
        self.m_PlayerHead.gameObject:SetActive(false)
    else
        self.m_PlayerHead.gameObject:SetActive(true)
        local playerData = LuaDaFuWongMgr.PlayerInfo and LuaDaFuWongMgr.PlayerInfo[LuaDaFuWongMgr.CurPlayerRound]
        local Portrait = self.m_PlayerHead.transform:Find("Portrait"):GetComponent(typeof(CUITexture))
        local bg = self.m_PlayerHead.transform:Find("BG"):GetComponent(typeof(CUITexture))
		local Name = self.m_PlayerHead.transform:Find("Name"):GetComponent(typeof(UILabel))
        if playerData then
            Portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(playerData.class, playerData.gender, -1), false)
			bg:LoadMaterial(LuaDaFuWongMgr:GetHeadBgPath(playerData.round))
			Name.transform:Find("Texture"):GetComponent(typeof(CUITexture)):LoadMaterial(LuaDaFuWongMgr:GetNameBgPath(playerData.round))
			Name.text = playerData.name
        end
    end
end

--@region UIEvent

function LuaDaFuWongQiTaoWnd:OnConfirmButtonClick()
	if not LuaDaFuWongMgr.IsMyRound then 
		local id = LuaDaFuWongMgr.CurRoundPlayer
		local name = LuaDaFuWongMgr.PlayerInfo[id].name
		g_MessageMgr:ShowMessage("DaFuWeng_OtherPlayerOperate",name)
		return
	end
	local result = 0
	if self.m_Select then result = 1 end
	Gac2Gas.DaFuWengRandomEventBeggarSelect(result)
end

function LuaDaFuWongQiTaoWnd:OnTimeOut(curStage)
	if curStage ~= EnumDaFuWengStage.RandomEventBeggar then 
		CUIManager.CloseUI(CLuaUIResources.DaFuWongQiTaoWnd)
	end
end
--@endregion UIEvent

function LuaDaFuWongQiTaoWnd:OnEnable()
	g_ScriptEvent:AddListener("DaFuWengRandomEventBeggarSelect",self,"OnConfirmResult")
	g_ScriptEvent:AddListener("OnDaFuWengStageUpdate",self,"OnTimeOut")
end

function LuaDaFuWongQiTaoWnd:OnDisable()
	g_ScriptEvent:RemoveListener("DaFuWengRandomEventBeggarSelect",self,"OnConfirmResult")
	g_ScriptEvent:RemoveListener("OnDaFuWengStageUpdate",self,"OnTimeOut")
	LuaDaFuWongMgr:EndCountDownTick(EnumDaFuWengCountDownStage.RandomEvent)
end