local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaFSCInviteDlg = class()

RegistClassMember(LuaFSCInviteDlg,"m_Tick")
RegistClassMember(LuaFSCInviteDlg,"m_CountdownLabel")

LuaFSCInviteDlg.s_Info = {
    inviterId = 0, 
    inviterName = "", 
    currentInviteExpiredTime = 0,
}

function LuaFSCInviteDlg:Init()
    local info = LuaFSCInviteDlg.s_Info
    local acceptButton = self.transform:Find("Anchor/AcceptButton").gameObject
    UIEventListener.Get(acceptButton).onClick = DelegateFactory.VoidDelegate(function(go)
        Gac2Gas.Anniv2023FSC_AcceptInvitation(info.inviterId)
        CUIManager.CloseUI(CLuaUIResources.FSCInviteDlg)
    end)
    local refuseButton = self.transform:Find("Anchor/RefuseButton").gameObject
    UIEventListener.Get(refuseButton).onClick = DelegateFactory.VoidDelegate(function(go)
        Gac2Gas.Anniv2023FSC_RejectInvitation(info.inviterId)
        CUIManager.CloseUI(CLuaUIResources.FSCInviteDlg)
    end)
    local label = self.transform:Find("Anchor/InfoLabel"):GetComponent(typeof(UILabel))
    label.text = info.inviterName..LocalString.GetString("邀请你打一局四时戏")

    self.m_CountdownLabel = refuseButton.transform:Find("Label"):GetComponent(typeof(UILabel))

    local count = math.ceil(info.currentInviteExpiredTime - CServerTimeMgr.Inst.timeStamp)
    if count > 0 then
        self.m_CountdownLabel.text = SafeStringFormat3(LocalString.GetString("拒绝(%d)"), count)
        if self.m_Tick then UnRegisterTick(self.m_Tick) end
        self.m_Tick = RegisterTick(function()
            local count = math.ceil(info.currentInviteExpiredTime - CServerTimeMgr.Inst.timeStamp)
            if count <= 0 then 
                CUIManager.CloseUI(CLuaUIResources.FSCInviteDlg)
            else
                self.m_CountdownLabel.text = SafeStringFormat3(LocalString.GetString("拒绝(%d)"), count)
            end
        end,1000)
    end
end

function LuaFSCInviteDlg:OnDestroy()
    if self.m_Tick then
        UnRegisterTick(self.m_Tick)
        self.m_Tick = nil
    end
end