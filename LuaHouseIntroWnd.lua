local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local AlignType1 = import "CPlayerInfoMgr+AlignType"
local String = import "System.String"
local MessageWndManager = import "L10.UI.MessageWndManager"
local ETickType = import "L10.Engine.ETickType"
local EnumChineseDigits = import "L10.Game.EnumChineseDigits"
local EChuanjiabaoType = import "L10.Game.EChuanjiabaoType"
local CTickMgr = import "L10.Engine.CTickMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CPreciousMgr = import "L10.Game.CPreciousMgr"
local Color = import "UnityEngine.Color"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CCommunityMgr = import "L10.UI.CCommunityMgr"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CUITexture=import "L10.UI.CUITexture"
local CHouseIntroProgressBar=import "L10.UI.CHouseIntroProgressBar"
local CHouseIntroWnd=import "L10.UI.CHouseIntroWnd"
local PopupMenuItemData=import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr=import "L10.UI.CPopupMenuInfoMgr"
local AlignType2=import "L10.UI.CPopupMenuInfoMgr+AlignType"

CLuaHouseIntroWnd = class()
RegistClassMember(CLuaHouseIntroWnd,"addressLabel")
RegistClassMember(CLuaHouseIntroWnd,"goHasHouse")
RegistClassMember(CLuaHouseIntroWnd,"goNoHouse")
RegistClassMember(CLuaHouseIntroWnd,"goLeftBtn")
RegistClassMember(CLuaHouseIntroWnd,"goRightBtn")
RegistClassMember(CLuaHouseIntroWnd,"goCloseBtn")
RegistClassMember(CLuaHouseIntroWnd,"btnMoveIn")
RegistClassMember(CLuaHouseIntroWnd,"consumeFoodLabel")
RegistClassMember(CLuaHouseIntroWnd,"consumeWoodLabel")
RegistClassMember(CLuaHouseIntroWnd,"consumeStoneLabel")
RegistClassMember(CLuaHouseIntroWnd,"consumeSilverLabel")
RegistClassMember(CLuaHouseIntroWnd,"moveCDLabel")
RegistClassMember(CLuaHouseIntroWnd,"needLabel")
RegistClassMember(CLuaHouseIntroWnd,"OwnerGO")
RegistClassMember(CLuaHouseIntroWnd,"CoupleGO")
RegistClassMember(CLuaHouseIntroWnd,"ownerCJBIconTexture")
RegistClassMember(CLuaHouseIntroWnd,"coupleCJBIconTexture")
RegistClassMember(CLuaHouseIntroWnd,"labelHouse")
RegistClassMember(CLuaHouseIntroWnd,"labelZhengwu")
RegistClassMember(CLuaHouseIntroWnd,"labelCangku")
RegistClassMember(CLuaHouseIntroWnd,"labelXiangfang")
RegistClassMember(CLuaHouseIntroWnd,"labelQishu")
RegistClassMember(CLuaHouseIntroWnd,"labelZsw")
RegistClassMember(CLuaHouseIntroWnd,"labelFangke")
RegistClassMember(CLuaHouseIntroWnd,"labelMiaopu")
RegistClassMember(CLuaHouseIntroWnd,"labelOwner")
RegistClassMember(CLuaHouseIntroWnd,"labelCoupleOwner")
RegistClassMember(CLuaHouseIntroWnd,"btnVisit")
RegistClassMember(CLuaHouseIntroWnd,"labelShoucang")
RegistClassMember(CLuaHouseIntroWnd,"btnGuanzhuHasHouse")
RegistClassMember(CLuaHouseIntroWnd,"progressList")
RegistClassMember(CLuaHouseIntroWnd,"labelName")
RegistClassMember(CLuaHouseIntroWnd,"ownerOrigPos")
RegistClassMember(CLuaHouseIntroWnd,"coupleOrigPos")
RegistClassMember(CLuaHouseIntroWnd,"OwnerCJBPos2ItemId")
RegistClassMember(CLuaHouseIntroWnd,"CoupleCJBPos2ItemId")
RegistClassMember(CLuaHouseIntroWnd,"mProgressTick")
RegistClassMember(CLuaHouseIntroWnd,"labelHaimengze")

function CLuaHouseIntroWnd:Awake()
    self.addressLabel = self.transform:Find("Wnd_Bg_Secondary_1/TitleLabel"):GetComponent(typeof(UILabel))
    self.goHasHouse = self.transform:Find("Anchor/HasHouse").gameObject
    self.goNoHouse = self.transform:Find("Anchor/NoHouse").gameObject
    self.goLeftBtn = self.transform:Find("Anchor/LeftBtn").gameObject
    self.goRightBtn = self.transform:Find("Anchor/RightBtn").gameObject
    self.btnMoveIn = self.transform:Find("Anchor/NoHouse/QianruBtn").gameObject
    self.consumeFoodLabel = self.transform:Find("Anchor/NoHouse/MoveInConsume/ConsumeFood/FoodLabel"):GetComponent(typeof(UILabel))
    self.consumeWoodLabel = self.transform:Find("Anchor/NoHouse/MoveInConsume/ConsumeWood/WoodLabel"):GetComponent(typeof(UILabel))
    self.consumeStoneLabel = self.transform:Find("Anchor/NoHouse/MoveInConsume/ConsumeStone/StoneLabel"):GetComponent(typeof(UILabel))
    self.consumeSilverLabel = self.transform:Find("Anchor/NoHouse/MoveInConsume/ConsumeSilver/SilverLabel"):GetComponent(typeof(UILabel))
    self.moveCDLabel = self.transform:Find("Anchor/NoHouse/MoveCDLabel"):GetComponent(typeof(UILabel))
    self.needLabel = self.transform:Find("Anchor/NoHouse/MoveInNeed/Value"):GetComponent(typeof(UILabel))
    self.OwnerGO = self.transform:Find("Anchor/HasHouse/Left/Owner").gameObject
    self.CoupleGO = self.transform:Find("Anchor/HasHouse/Left/Couple").gameObject

    self.ownerCJBIconTexture = {}
    self.ownerCJBIconTexture[0] = self.OwnerGO.transform:Find("TypeTemplate/Icon/Texture"):GetComponent(typeof(CUITexture))
    self.ownerCJBIconTexture[1] = self.OwnerGO.transform:Find("TypeTemplate2/Icon/Texture"):GetComponent(typeof(CUITexture))
    self.ownerCJBIconTexture[2] = self.OwnerGO.transform:Find("TypeTemplate3/Icon/Texture"):GetComponent(typeof(CUITexture))

    self.coupleCJBIconTexture = {}
    self.coupleCJBIconTexture[0] = self.CoupleGO.transform:Find("TypeTemplate/Icon/Texture"):GetComponent(typeof(CUITexture))
    self.coupleCJBIconTexture[1] = self.CoupleGO.transform:Find("TypeTemplate2/Icon/Texture"):GetComponent(typeof(CUITexture))
    self.coupleCJBIconTexture[2] = self.CoupleGO.transform:Find("TypeTemplate3/Icon/Texture"):GetComponent(typeof(CUITexture))


    self.labelHouse = self.transform:Find("Anchor/HasHouse/Left/HouseLabel"):GetComponent(typeof(UILabel))
    self.labelZhengwu = self.transform:Find("Anchor/HasHouse/Left/ZhengwuLabel"):GetComponent(typeof(UILabel))
    self.labelCangku = self.transform:Find("Anchor/HasHouse/Left/CangkuLabel"):GetComponent(typeof(UILabel))
    self.labelXiangfang = self.transform:Find("Anchor/HasHouse/Left/XiangfangLabel"):GetComponent(typeof(UILabel))
    self.labelQishu = self.transform:Find("Anchor/HasHouse/Left/QishuLabel"):GetComponent(typeof(UILabel))
    self.labelZsw = self.transform:Find("Anchor/HasHouse/Left/ZswLabel"):GetComponent(typeof(UILabel))
    self.labelFangke = self.transform:Find("Anchor/HasHouse/Left/FangkeLabel"):GetComponent(typeof(UILabel))
    self.labelHaimengze = self.transform:Find("Anchor/HasHouse/Left/HaimengzeLabel"):GetComponent(typeof(UILabel))

    local tf = self.transform:Find("Anchor/HasHouse/Left")
    self.labelMiaopu = {}
    self.labelMiaopu[0] = tf:Find("Miaopu1Label"):GetComponent(typeof(UILabel))
    self.labelMiaopu[1] = tf:Find("Miaopu2Label"):GetComponent(typeof(UILabel))
    self.labelMiaopu[2] = tf:Find("Miaopu3Label"):GetComponent(typeof(UILabel))
    self.labelMiaopu[3] = tf:Find("Miaopu4Label"):GetComponent(typeof(UILabel))
    self.labelMiaopu[4] = tf:Find("Miaopu5Label"):GetComponent(typeof(UILabel))
    self.labelMiaopu[5] = tf:Find("Miaopu6Label"):GetComponent(typeof(UILabel))


    local leftNode = self.transform:Find("Anchor/HasHouse/Left/")
    self.labelOwner = leftNode:Find("Owner/OwnerLabel"):GetComponent(typeof(UILabel))
    UIEventListener.Get(self.labelOwner.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        local houseIntro = CLuaHouseMgr.currentHouseIntro
        if houseIntro == nil then
            return
        end
        if houseIntro.isSpokesmanHouse == 1 then --屏蔽掉巫先生的家园（原罗云熙家园）
            return
        end
        local pos = self.labelCoupleOwner.transform.position
        CPlayerInfoMgr.ShowPlayerPopupMenu(houseIntro.ownerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, pos, AlignType1.Default)
    end)

    self.labelCoupleOwner = leftNode:Find("Couple/CoupleOwnerLabel"):GetComponent(typeof(UILabel))
    UIEventListener.Get(self.labelCoupleOwner.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
        local houseIntro = CLuaHouseMgr.currentHouseIntro
        if houseIntro == nil then
            return
        end
        local pos = self.labelCoupleOwner.transform.position
        CPlayerInfoMgr.ShowPlayerPopupMenu(houseIntro.ownerId2, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, pos, AlignType1.Default)
    end)

    self.btnVisit = self.transform:Find("Anchor/HasHouse/Right/BaifangBtn").gameObject
    self.labelShoucang = self.transform:Find("Anchor/HasHouse/Right/GuanzhuBtn/Label"):GetComponent(typeof(UILabel))
    self.btnGuanzhuHasHouse = self.transform:Find("Anchor/HasHouse/Right/GuanzhuBtn").gameObject

    local listTf = self.transform:Find("Anchor/HasHouse/Right/ProgressList")
    self.progressList = {}
    self.progressList[0] = listTf:Find("Progress1"):GetComponent(typeof(CHouseIntroProgressBar))
    self.progressList[1] = listTf:Find("Progress2"):GetComponent(typeof(CHouseIntroProgressBar))
    self.progressList[2] = listTf:Find("Progress3"):GetComponent(typeof(CHouseIntroProgressBar))
    self.progressList[3] = listTf:Find("Progress4"):GetComponent(typeof(CHouseIntroProgressBar))
    self.progressList[4] = listTf:Find("Progress5"):GetComponent(typeof(CHouseIntroProgressBar))
    self.progressList[5] = listTf:Find("Progress6"):GetComponent(typeof(CHouseIntroProgressBar))

    self.labelName = self.transform:Find("Anchor/HasHouse/Left/HouseLabel/TitleLabel"):GetComponent(typeof(UILabel))
    self.ownerOrigPos = self.OwnerGO.transform.localPosition
    self.coupleOrigPos = self.CoupleGO.transform.localPosition 
    self.OwnerCJBPos2ItemId = {}
    self.CoupleCJBPos2ItemId = {}
    self.mProgressTick = nil

    UIEventListener.Get(self.btnVisit).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnClickVistBtn(go)
    end)

    UIEventListener.Get(self.btnGuanzhuHasHouse).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnGuanzhuBtnClicked(go)
    end)
    UIEventListener.Get(self.btnMoveIn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnMoveInBtnClicked(go)
    end)
    UIEventListener.Get(self.goLeftBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnLeftBtnClicked(go)
    end)
    UIEventListener.Get(self.goRightBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnRightBtnClicked(go)
    end)

    for i = 0, 2 do
        local go = self.coupleCJBIconTexture[i].transform.parent.parent.gameObject
        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function(p)
            self:OnCJBClicked(p)
        end)

        local go1 = self.ownerCJBIconTexture[i].transform.parent.parent.gameObject
        UIEventListener.Get(go1).onClick = DelegateFactory.VoidDelegate(function(p)
            self:OnCJBClicked(p)
        end)
    end
end

function CLuaHouseIntroWnd:OnClickVistBtn(go)
    if CLuaHouseMgr.currentHouseIntro then
        local houseId = CLuaHouseMgr.currentHouseIntro.houseId
        local hasHaiMengZe = CLuaHouseMgr.currentHouseIntro.constructProp.PoolGrade and CLuaHouseMgr.currentHouseIntro.constructProp.PoolGrade > 0
        local isInHaiMengZe = CClientHouseMgr.Inst:IsPlayerInHaiMengZe()
        if CUIManager.IsLoaded(CUIResources.CommunityWnd) then
            if hasHaiMengZe and isInHaiMengZe then
                local function SelectAction(index)             
                    if index==0 then--家园
                        Gac2Gas.RequestEnterHouseScene(CLuaHouseMgr.currentHouseIntro.houseId)     
                    elseif index==1 then--海梦泽
                        Gac2Gas.RequestEnterHousePoolScene(CLuaHouseMgr.currentHouseIntro.houseId)
                    end       
                end
                local selectShareItem=DelegateFactory.Action_int(SelectAction)
                local t={}
                local item=PopupMenuItemData(LocalString.GetString("家园"),selectShareItem,false,nil)
                table.insert(t, item)
                local item=PopupMenuItemData(LocalString.GetString("海梦泽底"),selectShareItem,false,nil)
                table.insert(t, item)
                local array=Table2Array(t,MakeArrayClass(PopupMenuItemData))
                CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, AlignType2.Top,1,nil,600,true,266)
            else
                Gac2Gas.RequestEnterHouseScene(CLuaHouseMgr.currentHouseIntro.houseId)--不需要吟唱
            end
        else
            Gac2Gas.RequestEnterHouseSceneFromRank(CLuaHouseMgr.currentHouseIntro.houseId)--需要吟唱
        end
    end
end


function CLuaHouseIntroWnd:Init( )
    local curHouseId = CCommunityMgr.Inst.CurHouseId
    if curHouseId==nil or curHouseId=="" then
        self:OnShowNoHouseWnd()
    else
        self:OnUpdateHouseIntro()
    end
end
function CLuaHouseIntroWnd:OnEnable( )
    g_ScriptEvent:AddListener("OnCollectHouseUpdate",self,"UpdateCollectLabel")
    g_ScriptEvent:AddListener("HouseIdChanged",self,"HouseIdChanged")


end
function CLuaHouseIntroWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("OnCollectHouseUpdate",self,"UpdateCollectLabel")
    g_ScriptEvent:RemoveListener("HouseIdChanged",self,"HouseIdChanged")
    self:UnRegisterTick()
end
function CLuaHouseIntroWnd:OnCJBClicked( go) 
    local itemid = nil
    for i = 0, 2 do
        if self.coupleCJBIconTexture[i].transform.parent.parent.gameObject == go and self.CoupleCJBPos2ItemId[i] then
            itemid = self.CoupleCJBPos2ItemId[i]
            break
        end

        if self.ownerCJBIconTexture[i].transform.parent.parent.gameObject == go and self.OwnerCJBPos2ItemId[i] then
            itemid = self.OwnerCJBPos2ItemId[i]
            break
        end
    end

    if itemid ~= nil then
        local item = CItemMgr.Inst:GetById(itemid)
        if item ~= nil then
            CItemInfoMgr.ShowLinkItemInfo(item, false, nil, AlignType.Default, 0, 0, 0, 0, 0)
        end
    end
end


function CLuaHouseIntroWnd:HouseIdChanged( args ) 
    local houseId = args[0]
    if CClientMainPlayer.Inst ~= nil and CLuaHouseMgr.currentHouseIntro ~= nil then
        local labelVisit = CommonDefs.GetComponentInChildren_GameObject_Type(self.btnVisit, typeof(UILabel))
        if labelVisit ~= nil then
            if CClientMainPlayer.Inst.ItemProp.HouseId == CLuaHouseMgr.currentHouseIntro.basicProp.Id then
                labelVisit.text = LocalString.GetString("回 家")
            else
                labelVisit.text = LocalString.GetString("拜 访")
            end
        end
    end
end
function CLuaHouseIntroWnd:OnLeftBtnClicked( go) 
    if CHouseIntroWnd.sCurCommunityId ~= - 1 and CHouseIntroWnd.sCurCommunityPos ~= - 1 then
        local sz = House_Setting.GetData().MaxHousePerCommunity
        local newPos = (CHouseIntroWnd.sCurCommunityPos - 1 + sz) % sz

        Gac2Gas.QueryHouseIntroByCommunityPos(CHouseIntroWnd.sCurCommunityId, newPos + 1)
    end
end
function CLuaHouseIntroWnd:OnRightBtnClicked( go) 
    if CHouseIntroWnd.sCurCommunityId ~= - 1 and CHouseIntroWnd.sCurCommunityPos ~= - 1 then
        local sz = House_Setting.GetData().MaxHousePerCommunity
        local newPos = (CHouseIntroWnd.sCurCommunityPos + 1) % sz

        Gac2Gas.QueryHouseIntroByCommunityPos(CHouseIntroWnd.sCurCommunityId, newPos + 1)
    end
end
function CLuaHouseIntroWnd:OnMoveInBtnClicked( go) 

    if CHouseIntroWnd.sCurCommunityId ~= - 1 and CHouseIntroWnd.sCurCommunityPos ~= - 1 then
        local text = g_MessageMgr:FormatMessage("RELOCATE_CONFIRM")
        MessageWndManager.ShowOKCancelMessage(text, DelegateFactory.Action(function () 
            Gac2Gas.RequestRelocateHouse(CHouseIntroWnd.sCurCommunityId, CHouseIntroWnd.sCurCommunityPos + 1)
            CUIManager.CloseUI(CUIResources.HouseIntroWnd)
        end), nil, nil, nil, false)
    end
end
function CLuaHouseIntroWnd:OnGuanzhuBtnClicked( go) 
    local houseIntro = CLuaHouseMgr.currentHouseIntro
    if houseIntro == nil then
        return
    end
    if CClientMainPlayer.Inst == nil then
        return
    end

    if CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.CollectedHouseId, typeof(String), houseIntro.houseId) then
        Gac2Gas.RequestRemoveCollectedHouse(houseIntro.houseId)
    else
        Gac2Gas.RequestCollectHouse(houseIntro.houseId)
    end
end
function CLuaHouseIntroWnd:UpdateCollectLabel( )
    local houseIntro = CLuaHouseMgr.currentHouseIntro
    if houseIntro == nil then
        return
    end
    if CClientMainPlayer.Inst == nil then
        return
    end

    if CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.CollectedHouseId, typeof(String), houseIntro.houseId) then
        self.labelShoucang.text = LocalString.GetString("取消收藏")
    else
        self.labelShoucang.text = LocalString.GetString("收  藏")
    end
end
function CLuaHouseIntroWnd:ShowTitleLabel( )
    if CHouseIntroWnd.sCurCommunityId ~= - 1 and CHouseIntroWnd.sCurCommunityPos ~= - 1 and CLuaHouseMgr.currentHouseIntro ~= nil then
        self.addressLabel.text = CLuaHouseMgr.currentHouseIntro.houseName
        self.labelName.text = CLuaHouseMgr.currentHouseIntro.houseName
    end
end
function CLuaHouseIntroWnd:ShowNoHouseTitleLabel( )
    if CHouseIntroWnd.sCurCommunityPos ~= - 1 then
        local communityText = CClientHouseMgr.GetStreetName(CHouseIntroWnd.sCurCommmunityStreetId)
        self.addressLabel.text = SafeStringFormat3(LocalString.GetString("%s %d号"), communityText, CHouseIntroWnd.sCurCommunityPos + 1)
    end
end
function CLuaHouseIntroWnd:OnShowNoHouseWnd( )
    self.goNoHouse:SetActive(true)
    self.goHasHouse:SetActive(false)

    self:ShowNoHouseTitleLabel()

    local cdTime = House_Setting.GetData().HouseMigrateCD
    self.moveCDLabel.text = SafeStringFormat3(LocalString.GetString("搬迁存在%d天的冷却时间，冷却时间内不能搬迁"), cdTime)

    local food = 0 local wood = 0 local stone = 0
    local grade = 0
    if CClientHouseMgr.Inst.mConstructProp ~= nil then
        food = CClientHouseMgr.Inst.mConstructProp.Food
        wood = CClientHouseMgr.Inst.mConstructProp.Wood
        stone = CClientHouseMgr.Inst.mConstructProp.Stone
        grade = CClientHouseMgr.Inst.mConstructProp.ZhengwuGrade
    end

    local needHouseGrade = House_Setting.GetData().HouseMigrateGrade
    self.needLabel.text = SafeStringFormat3(LocalString.GetString("家园等级高于%d级"), needHouseGrade)
    self.needLabel.color = (grade >= needHouseGrade) and Color.green or Color.red

    local need = House_Setting.GetData().HouseMigrateResource
    local needFood = need[0] local needWood = need[1] local needStone = need[2]
    local needSilver = House_Setting.GetData().HouseMigrateSilver
    local silver = CClientMainPlayer.Inst.Silver
    self.consumeFoodLabel.text = SafeStringFormat3(LocalString.GetString("%d/%d"), needFood, food)
    self.consumeWoodLabel.text = SafeStringFormat3(LocalString.GetString("%d/%d"), needWood, wood)
    self.consumeStoneLabel.text = SafeStringFormat3(LocalString.GetString("%d/%d"), needStone, stone)
    self.consumeSilverLabel.text = SafeStringFormat3(LocalString.GetString("%d"), needSilver)
    self.consumeFoodLabel.color = (food >= needFood) and Color.green or Color.red
    self.consumeWoodLabel.color = (wood >= needWood) and Color.green or Color.red
    self.consumeStoneLabel.color = (stone >= needStone) and Color.green or Color.red
    self.consumeSilverLabel.color = (silver >= needSilver) and Color.green or Color.red

    local bEnable = (food >= needFood) and (wood >= needWood) and (stone >= needStone) and (silver >= needSilver)
    CUICommonDef.SetActive(self.btnMoveIn, bEnable, true)
end
function CLuaHouseIntroWnd:OnUpdateHouseIntro( )
    self.goHasHouse:SetActive(true)
    self.goNoHouse:SetActive(false)

    self:ShowTitleLabel()

    local houseIntro = CLuaHouseMgr.currentHouseIntro
    if houseIntro == nil then
        return
    end

    self:UpdateCollectLabel()

    self.labelHouse.text = SafeStringFormat3(LocalString.GetString("%d级家园"), houseIntro.constructProp.ZhengwuGrade)
    self.labelZhengwu.text = SafeStringFormat3(LocalString.GetString("%d级"), houseIntro.constructProp.ZhengwuGrade)
    self.labelCangku.text = SafeStringFormat3(LocalString.GetString("%d级"), houseIntro.constructProp.CangkuGrade)
    self.labelXiangfang.text = SafeStringFormat3(LocalString.GetString("%d级"), houseIntro.constructProp.XiangfangGrade)
    self.labelQishu.text = tostring(houseIntro.qishu)
    self.labelZsw.text = SafeStringFormat3(LocalString.GetString("%d个"), houseIntro.furnitureCount)
    self.labelFangke.text = SafeStringFormat3(LocalString.GetString("%d个"), houseIntro.fangkeCount)
    self.labelHaimengze.text = SafeStringFormat3(LocalString.GetString("%d级"), houseIntro.constructProp.PoolGrade)

    local gardenLookup = {}
    CommonDefs.DictIterate(houseIntro.gardenProp.GardenList, DelegateFactory.Action_object_object(function (gardenIdx, garden) 
        gardenLookup[tonumber(gardenIdx)] = garden
    end))

    do
        local i = 0
        while i < 6 do
            local garden = gardenLookup[i]
            if garden then
                self.labelMiaopu[i].text = SafeStringFormat3(LocalString.GetString("%d级"), garden.Grade)
            else
                self.labelMiaopu[i].gameObject:SetActive(false)
            end
            i = i + 1
        end
    end
    self.labelOwner.text = SafeStringFormat3("[%s]",houseIntro.ownerName)
    self.labelCoupleOwner.text = SafeStringFormat3("[%s]",houseIntro.ownerName2)

    local labelVisit = CommonDefs.GetComponentInChildren_GameObject_Type(self.btnVisit, typeof(UILabel))
    if labelVisit ~= nil then
        if CClientMainPlayer.Inst.ItemProp.HouseId == houseIntro.basicProp.Id then
            labelVisit.text = LocalString.GetString("回 家")
        else
            labelVisit.text = LocalString.GetString("拜 访")
        end
    end

    self:ShowChuanjiabao()

    self:UpdateProgressList()
end
function CLuaHouseIntroWnd:ShowChuanjiabao( )
    local CJBType2BtnIdx = {}
    CJBType2BtnIdx[EnumToInt(EChuanjiabaoType.DiGe)] = 0
    CJBType2BtnIdx[EnumToInt(EChuanjiabaoType.TianGe)] = 1
    CJBType2BtnIdx[EnumToInt(EChuanjiabaoType.RenGe)] = 2
    self.OwnerCJBPos2ItemId = {}
    self.CoupleCJBPos2ItemId={}

    local houseIntro = CLuaHouseMgr.currentHouseIntro
    local coupleOwnerId = houseIntro.basicProp.OwnerId2
    if coupleOwnerId == 0 then
        self.CoupleGO:SetActive(false)
        self.OwnerGO.transform.localPosition = self.coupleOrigPos
    else
        self.CoupleGO:SetActive(true)
        self.OwnerGO.transform.localPosition = self.ownerOrigPos

        local inited = {}

        local coupleGroup = houseIntro.coupleCJBGroup
        if coupleGroup ~= nil then
            local activeIdx = coupleGroup.CurrentActiveSuite
            if coupleGroup ~= nil and CommonDefs.DictContains(coupleGroup.Chuanjiabaos, typeof(Byte), activeIdx) then
                local suit = CommonDefs.DictGetValue(coupleGroup.Chuanjiabaos, typeof(Byte), activeIdx)

                do
                    local i = 0
                    while i < suit.ChuanjiabaoIds.Length do
                        local citem = CItemMgr.Inst:GetById(suit.ChuanjiabaoIds[i])
                        if citem ~= nil then
                            local data = citem.Item.ChuanjiabaoItemInfo
                            if CJBType2BtnIdx[data.WordInfo.Type] then
                                local idx = CJBType2BtnIdx[data.WordInfo.Type]--CommonDefs.DictGetValue(CJBType2BtnIdx, typeof(UInt32), (data.WordInfo.Type))
                                local text = self.coupleCJBIconTexture[idx]
                                text:LoadMaterial(citem.Icon)
                                text.transform.parent:Find("QualitySprite").gameObject:SetActive(true)
                                CommonDefs.GetComponent_Component_Type(text.transform.parent:Find("QualitySprite"), typeof(UISprite)).spriteName = CUICommonDef.GetItemCellBorder(nil, citem, true)
                                local preciousSprite = text.transform.parent:Find("PreciousSprite").gameObject
                                if CPreciousMgr.Inst:IsPreciousChuanjiabaoInfo(data.WordInfo) then
                                    preciousSprite:SetActive(true)
                                else
                                    preciousSprite:SetActive(false)
                                end

                                inited[idx] = true
                                self.CoupleCJBPos2ItemId[idx] = suit.ChuanjiabaoIds[i]
                            end
                        end
                        i = i + 1
                    end
                end
            end

            for i = 0, 2 do
                if not inited[i] then
                    local text = self.coupleCJBIconTexture[i]
                    text.material = nil
                    local preciousSprite = text.transform.parent:Find("PreciousSprite").gameObject
                    preciousSprite:SetActive(false)
                    text.transform.parent:Find("QualitySprite").gameObject:SetActive(false)
                end
            end
        end
    end

    do
        local inited = {}

        local ownerGroup = houseIntro.ownerCJBGroup
        if ownerGroup ~= nil then
            local activeIdx = ownerGroup.CurrentActiveSuite
            if ownerGroup ~= nil and CommonDefs.DictContains(ownerGroup.Chuanjiabaos, typeof(Byte), activeIdx) then
                local suit = CommonDefs.DictGetValue(ownerGroup.Chuanjiabaos, typeof(Byte), activeIdx)

                do
                    local i = 0
                    while i < suit.ChuanjiabaoIds.Length do
                        local citem = CItemMgr.Inst:GetById(suit.ChuanjiabaoIds[i])
                        if citem ~= nil then
                            local data = citem.Item.ChuanjiabaoItemInfo
                            if CJBType2BtnIdx[data.WordInfo.Type] then
                                local idx = CJBType2BtnIdx[data.WordInfo.Type]
                                local text = self.ownerCJBIconTexture[idx]
                                text:LoadMaterial(citem.Icon)
                                text.transform.parent:Find("QualitySprite").gameObject:SetActive(true)
                                CommonDefs.GetComponent_Component_Type(text.transform.parent:Find("QualitySprite"), typeof(UISprite)).spriteName = CUICommonDef.GetItemCellBorder(nil, citem, true)
                                local preciousSprite = text.transform.parent:Find("PreciousSprite").gameObject
                                if CPreciousMgr.Inst:IsPreciousChuanjiabaoInfo(data.WordInfo) then
                                    preciousSprite:SetActive(true)
                                else
                                    preciousSprite:SetActive(false)
                                end
                                inited[idx] = true

                                self.OwnerCJBPos2ItemId[idx] = suit.ChuanjiabaoIds[i]
                            end
                        end
                        i = i + 1
                    end
                end
            end

            for i = 0, 2 do
                if not inited[i] then
                    local text = self.ownerCJBIconTexture[i]
                    text.material = nil
                    local preciousSprite = text.transform.parent:Find("PreciousSprite").gameObject
                    preciousSprite:SetActive(false)
                    text.transform.parent:Find("QualitySprite").gameObject:SetActive(false)
                end
            end
        end
    end
end
function CLuaHouseIntroWnd:RegisterTick( )
    self:UnRegisterTick()

    self.mProgressTick = CTickMgr.Register(DelegateFactory.Action(function () 
        self:OnProgressTick()
    end), 1000, ETickType.Loop)
end
function CLuaHouseIntroWnd:UnRegisterTick( )
    if self.mProgressTick ~= nil then
        invoke(self.mProgressTick)
        self.mProgressTick = nil
    end
end
function CLuaHouseIntroWnd:UpdateProgressList( )
    local houseIntro = CLuaHouseMgr.currentHouseIntro
    if houseIntro == nil then
        return
    end

    local curTime = math.floor(CServerTimeMgr.Inst.timeStamp)
    local bNeedTick = false
    local idx = 0

    local ProgressList={}
    CommonDefs.DictIterate(houseIntro.progressProp.ProgressList, DelegateFactory.Action_object_object(function (gardenIdx, garden) 
        ProgressList[tonumber(gardenIdx)] = garden
    end))

    CommonDefs.DictIterate(houseIntro.gardenProp.GardenList, DelegateFactory.Action_object_object(function (gardenIdx, garden) 
        self.progressList[idx].gameObject:SetActive(true)
        if garden.Grade > 0 then
            local cropId = garden.CropId
            local cropData = House_Crop.GetData(cropId)
            if cropData ~= nil then
                self.progressList[idx].NameLabel.text = cropData.Name
                if curTime > garden.KuweiTime then
                    self.progressList[idx].ReadySprite.gameObject:SetActive(false)
                    local bar = self.progressList[idx].gameObject.transform:Find("Progress")
                    if bar ~= nil then
                        CUICommonDef.SetActive(bar.gameObject, false, true)
                    end
                    self.progressList[idx]:UpdateValue("", 1)
                    self.progressList[idx].RestTimeLabel.text = LocalString.GetString("已枯萎")
                elseif curTime > garden.BeginTime + cropData.GrowTime then
                    self.progressList[idx].ReadySprite.gameObject:SetActive(true)
                    local bar = self.progressList[idx].gameObject.transform:Find("Progress")
                    if bar ~= nil then
                        CUICommonDef.SetActive(bar.gameObject, true, true)
                    end
                    self.progressList[idx]:UpdateValue("", 1)
                    self.progressList[idx].RestTimeLabel.text = LocalString.GetString("已成熟")
                else
                    local progressPos = (EHouseProgress_lua.eCropProgressBase + gardenIdx)
                    local p = ProgressList[progressPos]
                    if p then
                        local totalTime = 10
                        local default
                        default, totalTime = CClientHouseMgr.Inst:GetProgressEndTime(progressPos, p, houseIntro.gardenProp, houseIntro.constructProp)
                        local endTime = default
                        local restTime = math.floor(math.max(endTime - curTime, 0))

                        local percent = (curTime - p.BeginTime) / (endTime - p.BeginTime)
                        percent = math.max(math.min(1, percent), 0)

                        local txt = ""
                        if restTime > 0 then
                            bNeedTick = true
                            if (math.floor(restTime / 3600)) ~= 0 then
                                txt = txt .. SafeStringFormat3(LocalString.GetString("%d时"), (math.floor(restTime / 3600)))
                            end
                            if (math.floor((restTime % 3600) / 60)) ~= 0 then
                                txt = txt .. SafeStringFormat3(LocalString.GetString("%d分"), (math.floor((restTime % 3600) / 60)))
                            end
                            txt = txt .. SafeStringFormat3(LocalString.GetString("%d秒"), (restTime % 60))
                        end

                        self.progressList[idx]:Init(progressPos, cropData.Name)
                        self.progressList[idx]:UpdateValue(txt, percent)
                    end
                end
            else
                self.progressList[idx].NameLabel.text = SafeStringFormat3(LocalString.GetString("苗圃%s"), EnumChineseDigits.GetDigit(gardenIdx + 1))
                self.progressList[idx].ReadySprite.gameObject:SetActive(false)
                local bar = self.progressList[idx].gameObject.transform:Find("Progress")
                if bar ~= nil then
                    CUICommonDef.SetActive(bar.gameObject, false, true)
                end
                self.progressList[idx]:UpdateValue("", 1)
                self.progressList[idx].RestTimeLabel.text = ""
            end
        end
        idx = idx + 1
    end))

    for i = idx, 5 do
        self.progressList[i].gameObject:SetActive(false)
    end

    if bNeedTick and self.mProgressTick == nil then
        self:RegisterTick()
    end

    if not bNeedTick and self.mProgressTick ~= nil then
        self:UnRegisterTick()
    end
end

function CLuaHouseIntroWnd:OnProgressTick()
    self:UpdateProgressList()
end
