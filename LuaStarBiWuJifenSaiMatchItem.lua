local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local UISprite = import "UISprite"
local GameObject = import "UnityEngine.GameObject"

LuaStarBiWuJifenSaiMatchItem = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaStarBiWuJifenSaiMatchItem, "rank", "rank", UILabel)
RegistChildComponent(LuaStarBiWuJifenSaiMatchItem, "rankImage", "rankImage", UISprite)
RegistChildComponent(LuaStarBiWuJifenSaiMatchItem, "name1", "name1", UILabel)
RegistChildComponent(LuaStarBiWuJifenSaiMatchItem, "win1", "win1", GameObject)
RegistChildComponent(LuaStarBiWuJifenSaiMatchItem, "name2", "name2", UILabel)
RegistChildComponent(LuaStarBiWuJifenSaiMatchItem, "win2", "win2", GameObject)
RegistChildComponent(LuaStarBiWuJifenSaiMatchItem, "point", "point", UILabel)
RegistChildComponent(LuaStarBiWuJifenSaiMatchItem, "vs", "vs", GameObject)
RegistChildComponent(LuaStarBiWuJifenSaiMatchItem, "GuanZhanBtn", "GuanZhanBtn", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaStarBiWuJifenSaiMatchItem, "m_AttackId")
RegistClassMember(LuaStarBiWuJifenSaiMatchItem, "m_DefendId")
RegistClassMember(LuaStarBiWuJifenSaiMatchItem, "m_bg")
function LuaStarBiWuJifenSaiMatchItem:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.m_bg = self.gameObject:GetComponent(typeof(UISprite))
    self.m_AttackId = 0
    self.m_DefendId = 0
    UIEventListener.Get(self.GuanZhanBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGuanZhanBtnClick()
	end)
end

function LuaStarBiWuJifenSaiMatchItem:Init(data,selfZhanDuiId)
    if not data then self.gameObject:SetActive(false) return end
    self.gameObject:SetActive(true)
    self.m_AttackId = data.attackId
    self.m_DefendId = data.defendId
    local index = data.rank
    self.rank.text = index
    self.name1.text = data.attackName
	self.name2.text = data.defendName
	self.name1.color = data.attackId == selfZhanDuiId and Color.green or Color.white
	self.name2.color = data.defendId == selfZhanDuiId and Color.green or Color.white
    self.win1.gameObject:SetActive(data.winnerId == data.attackId and data.status == 2)
    self.win2.gameObject:SetActive(data.winnerId == data.defendId and data.status == 2)
    if data.status == 2 then
		self.point.text = SafeStringFormat3("%d-%d",data.attackScore,data.defendScore)
		self.vs.gameObject:SetActive(false)
	else
		self.point.text = ""
		self.vs.gameObject:SetActive(true)
	end
    self.GuanZhanBtn.gameObject:SetActive(data.status == 1)
    self.m_bg.spriteName = (index + math.floor((index - 1) / 2)) % 2 == 0 and "common_bg_mission_background_n" or "common_bg_mission_background_s"
end

function LuaStarBiWuJifenSaiMatchItem:OnGuanZhanBtnClick()
    g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("StarBiwu_Enter_Watch_Confirm"), function()
        -- 请求观战
        Gac2Gas.RequestWatchStarBiwuJiFenSai(7, self.m_AttackId, self.m_DefendId)
    end, nil, nil, nil, false)
end

--@region UIEvent

--@endregion UIEvent

