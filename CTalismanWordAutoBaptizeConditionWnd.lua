-- Auto Generated!!
local CAutoBaptizeConditionItem = import "L10.UI.CAutoBaptizeConditionItem"
local CLogMgr = import "L10.CLogMgr"
local CTalismanWordAutoBaptizeConditionWnd = import "L10.UI.CTalismanWordAutoBaptizeConditionWnd"
local CTalismanWordBaptizeMgr = import "L10.UI.CTalismanWordBaptizeMgr"
local DelegateFactory = import "DelegateFactory"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local Talisman_WordOption = import "L10.Game.Talisman_WordOption"
local Talisman_XianJia = import "L10.Game.Talisman_XianJia"
local UIEventListener = import "UIEventListener"
CTalismanWordAutoBaptizeConditionWnd.m_Init_CS2LuaHook = function (this) 
    this.autoBaptizeConditionItem:SetActive(false)
    this:InitList()

    UIEventListener.Get(this.confirmBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        this:Close()
    end)
end
CTalismanWordAutoBaptizeConditionWnd.m_InitList_CS2LuaHook = function (this) 
    --数据初始化
    --CEquipmentBaptizeMgr.Inst.InitCondition();
    local equipInfo = CTalismanWordBaptizeMgr.selectTalisman

    local talisman = EquipmentTemplate_Equip.GetData(equipInfo.templateId)
    if talisman ~= nil then
        local wordOption = Talisman_WordOption.GetData(talisman.Type)
        if wordOption ~= nil then
            local wordStr = CommonDefs.StringSplit_ArrayChar(wordOption.Word, ";")
            local xianjia = Talisman_XianJia.GetData(equipInfo.templateId)
            if xianjia == nil then
                wordStr = CommonDefs.StringSplit_ArrayChar(wordOption.NormalWord, ";")
            end
            if wordStr ~= nil then
                do
                    local i = 0
                    while i < wordStr.Length do
                        local condition = CommonDefs.StringSplit_ArrayChar(wordStr[i], ",")
                        if condition ~= nil and condition.Length >= 2 then
                            local wordId, amount
                            local default
                            default, wordId = System.UInt32.TryParse(condition[0])
                            local extern
                            extern, amount = System.UInt32.TryParse(condition[1])
                            if default and extern then
                                local go = NGUITools.AddChild(this.conditionTf.gameObject, this.autoBaptizeConditionItem)
                                go:SetActive(true)
                                local cmp = CommonDefs.GetComponent_GameObject_Type(go, typeof(CAutoBaptizeConditionItem))
                                if cmp ~= nil then
                                    cmp:InitTalismanWord(wordId, amount)
                                end
                            end
                        end
                        i = i + 1
                    end
                end
            end
        else
            CLogMgr.LogError(LocalString.GetString("找不到"))
        end
    end

    this:AddScoreConditionItem()
end
CTalismanWordAutoBaptizeConditionWnd.m_AddScoreConditionItem_CS2LuaHook = function (this) 
    --装备评分高于当前
    local go = NGUITools.AddChild(this.conditionTf.gameObject, this.autoBaptizeConditionItem)
    go:SetActive(true)
    local cmp = CommonDefs.GetComponent_GameObject_Type(go, typeof(CAutoBaptizeConditionItem))
    cmp:InitFaBaoCheckScoreItem()
end
