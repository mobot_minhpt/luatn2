local DelegateFactory  = import "DelegateFactory"
local UITable = import "UITable"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"
local CItemMgr = import "L10.Game.CItemMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local UITabBar = import "L10.UI.UITabBar"
local CGuideMgr = import "L10.Game.Guide.CGuideMgr"
local COpenEntryMgr=import "L10.Game.COpenEntryMgr"
local CAsyncLoadTexture = import "L10.UI.CAsyncLoadTexture"

LuaGuildLeagueCrossTrainBuffTaskSubview = class()

RegistClassMember(LuaGuildLeagueCrossTrainBuffTaskSubview, "m_RemainTimesLabel")
RegistClassMember(LuaGuildLeagueCrossTrainBuffTaskSubview, "m_InfoButton")
RegistClassMember(LuaGuildLeagueCrossTrainBuffTaskSubview, "m_BuffTaskGrid")
RegistClassMember(LuaGuildLeagueCrossTrainBuffTaskSubview, "m_BuffTaskItem")
RegistClassMember(LuaGuildLeagueCrossTrainBuffTaskSubview, "m_GuildRankGrid")
RegistClassMember(LuaGuildLeagueCrossTrainBuffTaskSubview, "m_GuildRankItem")
RegistClassMember(LuaGuildLeagueCrossTrainBuffTaskSubview, "m_EmptyLabel")

function LuaGuildLeagueCrossTrainBuffTaskSubview:Awake()
    self.m_RemainTimesLabel = self.transform:Find("RemainTimesLabel"):GetComponent(typeof(UILabel))
    self.m_InfoButton = self.transform:Find("InfoButton").gameObject
    self.m_BuffTaskGrid = self.transform:Find("BuffTaskGrid"):GetComponent(typeof(UIGrid))
    self.m_BuffTaskItem = self.transform:Find("BuffTaskItem").gameObject
    self.m_GuildRankGrid = self.transform:Find("GuildRankGrid"):GetComponent(typeof(UIGrid))
    self.m_GuildRankItem = self.transform:Find("GuildRankItem").gameObject
    self.m_EmptyLabel = self.transform:Find("EmptyLabel").gameObject
    self.m_BuffTaskItem:SetActive(false)
    self.m_GuildRankItem:SetActive(false)
    
    UIEventListener.Get(self.m_InfoButton).onClick = DelegateFactory.VoidDelegate(function() self:OnInfoButtonClick() end)
end

function LuaGuildLeagueCrossTrainBuffTaskSubview:Init()
    local trainInfo = LuaGuildLeagueCrossMgr.m_TrainInfo
    self.m_RemainTimesLabel.text = SafeStringFormat3(LocalString.GetString("今日剩余任务次数[00FF00]%d[-]"), trainInfo.bt_limitTaskNum - trainInfo.bt_finishTaskNum)
    self:InitGuildRankTable(trainInfo)
    self:InitBuffTaskTable(trainInfo)
end

function LuaGuildLeagueCrossTrainBuffTaskSubview:InitGuildRankTable(info)
    Extensions.RemoveAllChildren(self.m_GuildRankGrid.transform)

    for i=1,#info.guildBuffTaskRank do
        local child = CUICommonDef.AddChild(self.m_GuildRankGrid.gameObject, self.m_GuildRankItem)
        child:SetActive(true)
        local guildNameLabel = child.transform:Find("GuildNameLabel"):GetComponent(typeof(UILabel))
        local ownerNameLabel = child.transform:Find("OwnerNameLabel"):GetComponent(typeof(UILabel))
        local finishTimesLabel = child.transform:Find("FinishTimesLabel"):GetComponent(typeof(UILabel))
        local rankTexture = child.transform:GetComponent(typeof(CAsyncLoadTexture))
        local data = info.guildBuffTaskRank[i]
        guildNameLabel.text = data.guildName
        ownerNameLabel.text = data.leaderName
        finishTimesLabel.text = tostring(data.times)
        rankTexture:ShowTexture(i-1)
    end
    self.m_GuildRankGrid:Reposition()
    self.m_EmptyLabel:SetActive(#info.guildBuffTaskRank==0)
end

function LuaGuildLeagueCrossTrainBuffTaskSubview:GetBuffTaskUpgradeValue(designData, index)
    return index==1 and designData["UpgradeValue"] or designData["UpgradeValue2"]
end

function LuaGuildLeagueCrossTrainBuffTaskSubview:GetBuffTaskEffectValue(designData, index)
    return index==1 and designData["EffectValue"] or designData["AddPlayBuffNum"]
end

function LuaGuildLeagueCrossTrainBuffTaskSubview:InitBuffTaskTable(info)
    Extensions.RemoveAllChildren(self.m_BuffTaskGrid.transform)

    for i=1,#info.nameInfo do
        local child = CUICommonDef.AddChild(self.m_BuffTaskGrid.gameObject, self.m_BuffTaskItem)
        child:SetActive(true)
        local icon = child.transform:Find("Icon"):GetComponent(typeof(CUITexture))
        local outerCircle = child.transform:Find("OuterCircle"):GetComponent(typeof(UIWidget))
        local zhuangshi1 = child.transform:Find("ZhuangShiTexture"):GetComponent(typeof(UIWidget))
        local zhuangshi2 = child.transform:Find("ZhuangShiTexture2"):GetComponent(typeof(UIWidget))
        local nameLabel = child.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
        local levelLabel = child.transform:Find("LevelLabel"):GetComponent(typeof(UILabel))
        local effectLabel = child.transform:Find("EffectLabel"):GetComponent(typeof(UILabel))
        local progressLabel = child.transform:Find("ProgressLabel"):GetComponent(typeof(UILabel))
        local nextEffectLabel = child.transform:Find("NextEffectLabel"):GetComponent(typeof(UILabel))
        local button = child.transform:Find("Button"):GetComponent(typeof(CButton))
        local finishLabel = child.transform:Find("FinishLabel"):GetComponent(typeof(UILabel))  
        icon:LoadMaterial(info.portraitInfo[i], false)
        outerCircle.color = info.colorInfo[i]
        zhuangshi1.color = info.colorInfo[i]
        zhuangshi2.color = info.colorInfo[i]
        nameLabel.color = info.nameColorInfo[i]
        nameLabel.text = info.nameInfo[i]
         
        
        local curLevel = info.bt_levelInfo[i]
        levelLabel.text = SafeStringFormat3(LocalString.GetString("%d级"), curLevel)
        local designData = GuildLeagueCross_BuffTaskLevel.GetData(curLevel)
        local nextData = GuildLeagueCross_BuffTaskLevel.Exists(curLevel+1) and GuildLeagueCross_BuffTaskLevel.GetData(curLevel+1) or nil

        local effectTextFormat = (i==1 and LocalString.GetString("+%d点") or LocalString.GetString("各加%d次"))
        effectLabel.text = SafeStringFormat3(effectTextFormat, self:GetBuffTaskEffectValue(designData, i))
        progressLabel.text = SafeStringFormat3("%d/%d", info.bt_progressInfo[i], self:GetBuffTaskUpgradeValue(designData, i))
        outerCircle.fillAmount = info.bt_progressInfo[i]/self:GetBuffTaskUpgradeValue(designData, i)
        if nextData and self:GetBuffTaskUpgradeValue(designData, i)>0 then --下一级数据存在并且升级所需经验大于0
            nextEffectLabel.gameObject:SetActive(true)
            nextEffectLabel.text = SafeStringFormat3(effectTextFormat, self:GetBuffTaskEffectValue(nextData, i))
            button.gameObject:SetActive(info.bOpen)
            button.Text = LocalString.GetString("接取任务")
            finishLabel.text = ""
            UIEventListener.Get(button.gameObject).onClick = DelegateFactory.VoidDelegate(function() self:OnReceiveTaskButtonClick(button.gameObject, i) end)
            local alert = button.transform:Find("Alert").gameObject
            alert:SetActive(false)
        else
            local maxLevelData = GuildLeagueCross_BuffTaskLevel.GetData(curLevel - 1)
            progressLabel.text = SafeStringFormat3("%d/%d", self:GetBuffTaskUpgradeValue(maxLevelData, i), self:GetBuffTaskUpgradeValue(maxLevelData, i))
            nextEffectLabel.gameObject:SetActive(false)
            button.gameObject:SetActive(false)
            finishLabel.text = LocalString.GetString("已满级，任务结束")
        end
    end
    self.m_BuffTaskGrid:Reposition()
end

function LuaGuildLeagueCrossTrainBuffTaskSubview:OnInfoButtonClick()
    g_MessageMgr:ShowMessage("GLC_GUILD_LEAGUE_BUFF_TASK_DESC")
end

function LuaGuildLeagueCrossTrainBuffTaskSubview:OnReceiveTaskButtonClick(go, buffTaskType)
    LuaGuildLeagueCrossMgr:RequestAcceptGuildLeagueBuffTask(buffTaskType)
    CUIManager.CloseUI("GuildLeagueCrossTrainWnd")
    CUIManager.CloseUI("ScheduleWnd")
end

