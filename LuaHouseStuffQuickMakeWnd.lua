require("common/common_include")
local CLivingSkillMgr=import "L10.Game.CLivingSkillMgr"
local LifeSkill_Item=import "L10.Game.LifeSkill_Item"
local NGUITools = import "NGUITools"
local UIGrid=import "UIGrid"
local CItemCountUpdate=import "L10.UI.CItemCountUpdate"
local CUITexture=import "L10.UI.CUITexture"
local Item_Item=import "L10.Game.Item_Item"
local AlignType=import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local CHouseStuffMakeWnd=import "L10.UI.CHouseStuffMakeWnd"
local CLivingSkillQuickMakeMgr=import "L10.UI.CLivingSkillQuickMakeMgr"

local MessageMgr = import "L10.Game.MessageMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CTrackMgr = import "L10.Game.CTrackMgr"
local Constants = import "L10.Game.Constants"


CLuaHouseStuffQuickMakeWnd=class()
RegistClassMember(CLuaHouseStuffQuickMakeWnd,"m_ListItem")
RegistClassMember(CLuaHouseStuffQuickMakeWnd,"m_MatItem")
RegistClassMember(CLuaHouseStuffQuickMakeWnd,"m_MakeItemList")
RegistClassMember(CLuaHouseStuffQuickMakeWnd,"m_Grid")
RegistClassMember(CLuaHouseStuffQuickMakeWnd,"m_SkillLv")


function CLuaHouseStuffQuickMakeWnd:Init()
    self.m_ListItem=FindChild(self.transform,"ListItem").gameObject
    self.m_ListItem:SetActive(false)
    self.m_MatItem=FindChild(self.transform,"MatItem").gameObject
    self.m_MatItem:SetActive(false)
    self.m_Grid=FindChild(self.transform,"Grid"):GetComponent(typeof(UIGrid))

    self.m_MakeItemList={}


    local id=CLivingSkillMgr.Inst.useItemTemplateId
    local entrys=CLivingSkillMgr.Inst.entrys
    for i=1,entrys.Count do
        local skillEntry=entrys[i-1]
        local sections=skillEntry.sections
        for j=1,sections.Count do
            local itemIds=sections[j-1].itemIds
            for m=1,itemIds.Count do
                local itemId=itemIds[m-1]
                local data=LifeSkill_Item.GetData(itemId)
                if data and data.ConsumeItems then
                    local find=false
                    for k=1,data.ConsumeItems.Length do
                        if data.ConsumeItems[k-1].ID==id then
                            find=true
                            break
                        end
                    end
                    --该物品可以制作
                    if find then
                        table.insert( self.m_MakeItemList,itemId )
                    end
                end
            end
            
        end
    end

    self.m_SkillLv=CLivingSkillMgr.Inst:GetSkillLevel(CLivingSkillMgr.Inst.selectedSkill)


    local function compare( a, b ) 
        local dataA=LifeSkill_Item.GetData(a)
        local dataB=LifeSkill_Item.GetData(b)
        
        local bA=dataA.SkillLev<=self.m_SkillLv
        local bB=dataB.SkillLev<=self.m_SkillLv

        if bA and not bB then--能做的放前面
            return true
        elseif not bA and bB then
            return false
        else
            return a>b--id大的放前面
        end
    end
    table.sort( self.m_MakeItemList, compare )

    for i,v in ipairs(self.m_MakeItemList) do
        local go = NGUITools.AddChild(self.m_Grid.gameObject, self.m_ListItem)
        go:SetActive(true)
        self:InitListItem(go.transform,v)
    end
    self.m_Grid:Reposition()

    --一个都没有 关掉窗口
    if table.getn(self.m_MakeItemList)==0 then
        CUIManager.CloseUI(CUIResources.HouseStuffQuickMakeWnd)
    end
end



function CLuaHouseStuffQuickMakeWnd:InitListItem(tf,templateId)
    local data=LifeSkill_Item.GetData(templateId)
    local grid=FindChild(tf,"Grid"):GetComponent(typeof(UIGrid))
    local makeItem = Item_Item.GetData(templateId)
    if makeItem then
        local makeIcon=FindChild(tf,"MakeIcon"):GetComponent(typeof(CUITexture))
        makeIcon:LoadMaterial(makeItem.Icon)
    end

    local makeItemGo=FindChild(tf,"MakeItem").gameObject
    CommonDefs.AddOnClickListener(makeItemGo,DelegateFactory.Action_GameObject(function(go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(templateId,false,nil,AlignType.Default,0,0,0,0)
    end),false)

    local disableSprite=FindChild(tf,"DisableSprite").gameObject

    local lvLabel=FindChild(tf,"LvLabel"):GetComponent(typeof(UILabel))

    local btn=FindChild(tf,"Button").gameObject
    
    local lvenough=true
    if data.SkillLev>self.m_SkillLv then
        lvenough=false--等级不足 不用放前面
        disableSprite:SetActive(true)
        lvLabel.gameObject:SetActive(true)
        lvLabel.text=SafeStringFormat3( "lv.%d",data.SkillLev )
        btn:SetActive(false)
    else
        disableSprite:SetActive(false)
        btn:SetActive(true)
        CommonDefs.AddOnClickListener(btn,DelegateFactory.Action_GameObject(function(go)
            self:StartMake(templateId)
        end),false)
        lvLabel.gameObject:SetActive(false)
    end
    local enough=true

    if data and data.ConsumeItems then
        for k=1,data.ConsumeItems.Length do
            local id=data.ConsumeItems[k-1].ID
            local needCount=data.ConsumeItems[k-1].count
            local go = NGUITools.AddChild(grid.gameObject, self.m_MatItem)
            go:SetActive(true)
            local icon=FindChild(go.transform,"Icon"):GetComponent(typeof(CUITexture))
            local item = Item_Item.GetData(id)
            if item then
                icon:LoadMaterial(item.Icon)
            end
            local mask=FindChild(go.transform,"Mask").gameObject
            local countUpdate=go:GetComponent(typeof(CItemCountUpdate))
            countUpdate.templateId=id
            local function OnCountChange(val)
                if val>=needCount then
                    mask:SetActive(false)
                    countUpdate.format=SafeStringFormat3("{0}/%d",needCount)
                else
                    mask:SetActive(true)
                    countUpdate.format=SafeStringFormat3("[ff0000]{0}[-]/%d",needCount)
                    enough=false
                end
            end
            countUpdate.onChange=DelegateFactory.Action_int(OnCountChange)
            countUpdate:UpdateCount()
            CommonDefs.AddOnClickListener(go,DelegateFactory.Action_GameObject(function(go)
                if countUpdate.count>=needCount then
                    CItemInfoMgr.ShowLinkItemTemplateInfo(id,false,nil,AlignType.Default,0,0,0,0)
                else
                    CLivingSkillQuickMakeMgr.Inst:ShowMakeTipWnd(id)
                end
            end),false)
        end
        --检查材料够不够 如果够了 放在最前面
        if enough and lvenough then
            tf:SetSiblingIndex(0)
        end
    end
    grid:Reposition()

end

function CLuaHouseStuffQuickMakeWnd:StartMake(makeId)
    local isMakeZhuShaBi = CLivingSkillMgr.Inst:IsZhuShaBi(makeId)

    -- CHouseStuffMakeWnd.ZhuShaBiUpgradeMat = 0
    CHouseStuffMakeWnd.TemplateId=makeId

    if isMakeZhuShaBi and CClientMainPlayer.Inst and not CClientMainPlayer.Inst.ItemProp.HouseId then
        --没有家园
        local message = MessageMgr.Inst:FormatMessage("BUY_HOUSE_TIP",{})
        MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(
            function()
                CTrackMgr.Inst.FindNPC(Constants.JiayuanNpcId, Constants.HangZhouID, 89, 38,nil,nil)
            end), 
            nil, 
            LocalString.GetString("前往"), 
            LocalString.GetString("取消"),
            false)
    end
    CHouseStuffMakeWnd.IsMakeZhuShaBi = false
    CUIManager.ShowUI(CUIResources.HouseStuffMakeWnd)
end


return CLuaHouseStuffQuickMakeWnd
