local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local AlignType = import "CPlayerInfoMgr+AlignType"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local CItemMgr = import "L10.Game.CItemMgr"

LuaChunJie2023ShengYanRankWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChunJie2023ShengYanRankWnd, "ContentView", QnAdvanceGridView)
RegistChildComponent(LuaChunJie2023ShengYanRankWnd, "MainPlayerInfo", GameObject)
RegistChildComponent(LuaChunJie2023ShengYanRankWnd, "RewardInfo", GameObject)

RegistChildComponent(LuaChunJie2023ShengYanRankWnd, "ItemCell1", CQnReturnAwardTemplate)
RegistChildComponent(LuaChunJie2023ShengYanRankWnd, "ItemCell2", CQnReturnAwardTemplate)
RegistChildComponent(LuaChunJie2023ShengYanRankWnd, "ItemCell3", CQnReturnAwardTemplate)

RegistClassMember(LuaChunJie2023ShengYanRankWnd, "RankList")
RegistClassMember(LuaChunJie2023ShengYanRankWnd, "MyData")
--@endregion RegistChildComponent end

function LuaChunJie2023ShengYanRankWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
end

function LuaChunJie2023ShengYanRankWnd:Init()
    -- 显示排名
    self:InitRank()
    self:SyncTTSYPlayState()
    -- 显示奖励
    self.ItemCell1:Init(CItemMgr.Inst:GetItemTemplate(ChunJie_TTSYRank.GetData(1).RewardItem), 1)
    self.ItemCell2:Init(CItemMgr.Inst:GetItemTemplate(ChunJie_TTSYRank.GetData(2).RewardItem), 1)
    self.ItemCell3:Init(CItemMgr.Inst:GetItemTemplate(ChunJie_TTSYRank.GetData(3).RewardItem), 1)
end

function LuaChunJie2023ShengYanRankWnd:InitRank()
    -- 排名
    self.RankList = {}
    self.ContentView.m_DataSource = DefaultTableViewDataSource.Create(function()
        return #self.RankList
    end, function(item, index)
        item:SetBackgroundTexture(index % 2 == 1 and g_sprites.OddBgSpirite or g_sprites.EvenBgSprite)
        local data = self.RankList[index+1]
        self:InitRankItem(item, data)
    end)

    -- 设置选中状态
    self.ContentView.OnSelectAtRow = DelegateFactory.Action_int(function (row)
        -- 显示玩家信息
        local data = self.RankList[row+1]
        CPlayerInfoMgr.ShowPlayerPopupMenu(data.playerId, EnumPlayerInfoContext.Undefined, 
            EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
    end)
end

function LuaChunJie2023ShengYanRankWnd:InitRankItem(item, data)
	item.transform:Find("PlayerNameLabel"):GetComponent(typeof(UILabel)).text = data.Name
	local scoreLabel = item.transform:Find("Label3"):GetComponent(typeof(UILabel))
	local scoreLabel1 = item.transform:Find("Label5"):GetComponent(typeof(UILabel))
	local rankLabel1 = item.transform:Find("Label4"):GetComponent(typeof(UILabel))
	local scoreLabel2 = item.transform:Find("Label7"):GetComponent(typeof(UILabel))
	local rankLabel2 = item.transform:Find("Label6"):GetComponent(typeof(UILabel))
    scoreLabel.text = data.Score
    scoreLabel1.text = data.Score_1
    rankLabel1.text = data.Rank_1
    scoreLabel2.text = data.Score_2
    rankLabel2.text = data.Rank_2
    -- 设置透明度
    local color = scoreLabel.color
    local color_half_alpha = scoreLabel.color
    color_half_alpha.a = 0.5
    if LuaChunJie2023Mgr.TTSY_playState == EnumTTSYPlayState.Stage_1 then
        scoreLabel1.color = color
        rankLabel1.color = color
        scoreLabel2.color = color_half_alpha
        rankLabel2.color = color_half_alpha
    elseif LuaChunJie2023Mgr.TTSY_playState == EnumTTSYPlayState.Stage_2 then
        scoreLabel1.color = color_half_alpha
        rankLabel1.color = color_half_alpha
        scoreLabel2.color = color
        rankLabel2.color = color
    else
        scoreLabel1.color = color_half_alpha
        rankLabel1.color = color_half_alpha
        scoreLabel2.color = color_half_alpha
        rankLabel2.color = color_half_alpha
    end
    -- 显示排名
    local rankSp = item.transform:Find("RankImage"):GetComponent(typeof(UISprite))
	local rankLabel = item.transform:Find("RankLabel"):GetComponent(typeof(UILabel))
    local rankImgList = {g_sprites.RankFirstSpriteName, g_sprites.RankSecondSpriteName, g_sprites.RankThirdSpriteName}
    if data.Rank <= 3 and data.Rank >= 1 then
        rankLabel.gameObject:SetActive(false)
        rankSp.gameObject:SetActive(true)
        rankSp.spriteName = rankImgList[data.Rank]
    else
        rankLabel.gameObject:SetActive(true)
        rankSp.gameObject:SetActive(false)
        rankLabel.text = tostring(data.Rank)
    end
end

function LuaChunJie2023ShengYanRankWnd:SyncTTSYPlayState()
    self.MyData = self:TransformRankData(LuaChunJie2023Mgr.TTSY_MainPlayerRankInfo)
    self.RankList = {}
    for i, info in pairs(LuaChunJie2023Mgr.TTSY_RankTbl) do
        table.insert(self.RankList, self:TransformRankData(info))
    end

    if self.MyData.Score ~= nil then
        self.MyData.name = CClientMainPlayer.Inst.Name
        self.MyData.playerId = CClientMainPlayer.Inst.Id
    end
    self:InitRankItem(self.MainPlayerInfo, self.MyData)
    self.ContentView:ReloadData(true, false)
end

function LuaChunJie2023ShengYanRankWnd:TransformRankData(rankInfo)
    local data = {}
    data.playerId = rankInfo.PlayerId
    data.Name = rankInfo.Name
    data.Score = rankInfo.Score
    data.Score_1 = rankInfo.Score_1
    data.Score_2 = rankInfo.Score_2
    data.Rank = rankInfo.Rank
    -- 显示阶段排名
    if LuaChunJie2023Mgr.TTSY_playState == EnumTTSYPlayState.Idle then
        -- 准备阶段
        data.Rank_1 = LocalString.GetString("未开启")
        data.Rank_2 = LocalString.GetString("未开启")
    elseif LuaChunJie2023Mgr.TTSY_playState == EnumTTSYPlayState.Stage_1 then
        -- 第一轮
        if rankInfo.Failed_1 then
            data.Rank_1 = rankInfo.Rank_1
        else
            data.Rank_1 = LocalString.GetString("进行中")
        end
        data.Rank_2 = LocalString.GetString("未开启")
    elseif LuaChunJie2023Mgr.TTSY_playState == EnumTTSYPlayState.Stage_2 then
        -- 第二轮
        data.Rank_1 = rankInfo.Rank_1
        if rankInfo.Failed_2 then
            data.Rank_2 = rankInfo.Rank_2
        else
            data.Rank_2 = LocalString.GetString("进行中")
        end
    else
        -- 游戏结束
        data.Rank_1 = rankInfo.Rank_1
        data.Rank_2 = rankInfo.Rank_2
    end
    return data
end

--@region UIEvent

--@endregion UIEvent

function LuaChunJie2023ShengYanRankWnd:OnEnable()
    g_ScriptEvent:AddListener("SyncTTSYPlayState", self, "SyncTTSYPlayState")
end

function LuaChunJie2023ShengYanRankWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SyncTTSYPlayState", self, "SyncTTSYPlayState")
end