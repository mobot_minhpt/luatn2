-- Auto Generated!!
local CommonDefs = import "L10.Game.CommonDefs"
local CWorldEventVoiceItem = import "L10.UI.CWorldEventVoiceItem"
local CWorldEventVoiceWnd = import "L10.UI.CWorldEventVoiceWnd"
local Int32 = import "System.Int32"
--local ShiJieShiJian_Voice = import "L10.Game.ShiJieShiJian_Voice"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local WorldEventVoice = import "L10.UI.WorldEventVoice"
CWorldEventVoiceWnd.m_Init_CS2LuaHook = function (this) 
    this.m_MasterView.m_DataSource = this
    UIEventListener.Get(this.m_CloseButton).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)
    CommonDefs.DictClear(this.m_Data)
    CommonDefs.ListClear(this.m_Sections)
    CommonDefs.DictClear(this.m_SectionRows)
    ShiJieShiJian_Voice.Foreach(function (key, data) 
        local section = data.Type
        local subSection = data.SubType
        --确认语音版本号
        if data.Version > CWorldEventVoiceWnd.s_Version then
            return
        end
        if not CommonDefs.ListContains(this.m_Sections, typeof(String), section) then
            CommonDefs.ListAdd(this.m_Sections, typeof(String), section)
            if not CommonDefs.DictContains(this.m_SectionRows, typeof(String), section) then
                CommonDefs.DictAdd(this.m_SectionRows, typeof(String), section, typeof(MakeGenericClass(List, String)), CreateFromClass(MakeGenericClass(List, String)))
            end
        end
        if not CommonDefs.ListContains(CommonDefs.DictGetValue(this.m_SectionRows, typeof(String), section), typeof(String), subSection) then
            CommonDefs.ListAdd(CommonDefs.DictGetValue(this.m_SectionRows, typeof(String), section), typeof(String), subSection)
        end
        if not CommonDefs.DictContains(this.m_Data, typeof(String), section) then
            CommonDefs.DictAdd(this.m_Data, typeof(String), section, typeof(MakeGenericClass(Dictionary, String, MakeGenericClass(List, WorldEventVoice))), CreateFromClass(MakeGenericClass(Dictionary, String, MakeGenericClass(List, WorldEventVoice))))
        end
        if not CommonDefs.DictContains(CommonDefs.DictGetValue(this.m_Data, typeof(String), section), typeof(String), subSection) then
            CommonDefs.DictAdd(CommonDefs.DictGetValue(this.m_Data, typeof(String), section), typeof(String), subSection, typeof(MakeGenericClass(List, WorldEventVoice)), CreateFromClass(MakeGenericClass(List, WorldEventVoice)))
        end
        local voice = CreateFromClass(WorldEventVoice)
        voice.Section = section
        voice.Row = subSection
        voice.Text = data.VoiceWord
        voice.FmodEvent = data.VoicePath
        CommonDefs.ListAdd(CommonDefs.DictGetValue(CommonDefs.DictGetValue(this.m_Data, typeof(String), section), typeof(String), subSection), typeof(WorldEventVoice), voice)
    end)
    this.m_TitleView.OnSectionSelect = MakeDelegateFromCSFunction(this.OnTitleViewClick, MakeGenericClass(Action2, Int32, Int32), this)
    this.m_TitleView:Init(this.m_Sections, this.m_SectionRows)
end
CWorldEventVoiceWnd.m_OnTitleViewClick_CS2LuaHook = function (this, section, row) 
    if section >= 0 and section < this.m_Sections.Count then
        local sectionName = this.m_Sections[section]
        local subSectionName = CommonDefs.DictGetValue(this.m_SectionRows, typeof(String), sectionName)[row]
        if CommonDefs.DictContains(this.m_Data, typeof(String), sectionName) and CommonDefs.DictContains(CommonDefs.DictGetValue(this.m_Data, typeof(String), sectionName), typeof(String), subSectionName) then
            this.m_CurrentList = CommonDefs.DictGetValue(CommonDefs.DictGetValue(this.m_Data, typeof(String), sectionName), typeof(String), subSectionName)
            this.m_MasterView:ReloadData(true, false)
        end
    end
end
CWorldEventVoiceWnd.m_ItemAt_CS2LuaHook = function (this, view, row) 
    local data = this.m_CurrentList[row]
    local item = TypeAs(view:GetFromPool(0), typeof(CWorldEventVoiceItem))
    item:UpdateData(data.Text, data.FmodEvent, nil)
    item.OnPlayStart = MakeDelegateFromCSFunction(this.OnPlayStart, MakeGenericClass(Action1, Int32), this)
    item.OnPlayFinished = MakeDelegateFromCSFunction(this.OnPlayFinished, MakeGenericClass(Action1, Int32), this)
    return item
end
CWorldEventVoiceWnd.m_OnPlayStart_CS2LuaHook = function (this, row) 
    do
        local i = 0
        while i < this.m_CurrentList.Count do
            if i ~= row then
                local item = TypeAs(this.m_MasterView:GetItemAtRow(i), typeof(CWorldEventVoiceItem))
                item:StopPlay()
            end
            i = i + 1
        end
    end
end
CWorldEventVoiceWnd.m_OnPlayFinished_CS2LuaHook = function (this, row) 
    do
        local i = row + 1
        while i < this.m_CurrentList.Count do
            if not System.String.IsNullOrEmpty(this.m_CurrentList[i].FmodEvent) then
                this.m_MasterView:ScrollToRow(i)
                local item = TypeAs(this.m_MasterView:GetItemAtRow(i), typeof(CWorldEventVoiceItem))
                item:StartPlay()
                break
            end
            i = i + 1
        end
    end
end
