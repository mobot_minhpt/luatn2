local TweenHeight=import "TweenHeight"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CUIManager = import "L10.UI.CUIManager"

LuaWorldEventVerShowWnd = class()

RegistClassMember(LuaWorldEventVerShowWnd,"CloseBtn")
RegistClassMember(LuaWorldEventVerShowWnd,"ShowTexture")
RegistClassMember(LuaWorldEventVerShowWnd,"ShowBg")

function LuaWorldEventVerShowWnd:Init()
	local onCloseClick = function(go)
    CUIManager.CloseUI(CLuaUIResources.WorldEventVerShowWnd)
	end
	CommonDefs.AddOnClickListener(self.CloseBtn,DelegateFactory.Action_GameObject(onCloseClick),false)
	--self.CloseBtn:SetActive(false)
	--local path = ""
	--self.ShowTexture:GetComponent(typeof(CUITexture)):LoadTexture(path,false)

	RegisterTickWithDuration(function ()
		local showBgUIWidget = self.ShowBg:GetComponent(typeof(UIWidget))
		TweenHeight.Begin(showBgUIWidget, 3, 831)
	end,300,300)
end

return LuaWorldEventVerShowWnd
