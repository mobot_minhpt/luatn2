local Object = import "System.Object"
local CClientObjectMgr=import "L10.Game.CClientObjectMgr"
local CPos = import "L10.Engine.CPos"
local CTrackMgr=import "L10.Game.CTrackMgr"
local EnumObjectType=import "L10.Game.EnumObjectType"
local CScheduleMgr=import "L10.Game.CScheduleMgr"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
local Task_Task=import "L10.Game.Task_Task"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local CTaskMgr=import "L10.Game.CTaskMgr"
local CClientFormula=import "L10.Game.CClientFormula"
local CScene = import "L10.Game.CScene"
local CommonDefs = import "L10.Game.CommonDefs"
local CUIActionMgr = import "L10.UI.CUIActionMgr"
local CLoadingWnd = import "L10.UI.CLoadingWnd"
local CLoginMgr = import "L10.Game.CLoginMgr"

--[[
LuaEnumActivityTabType=
{
    None=0,
    Common=1,
    TuiJian=2,

    RiChang=3,--taskTabType+2
    JieRi=4,
    XiuXian=5,
    XianXia=6,
    ChengZhan=7,
    SaiShi=8,
    -- StarBiWu = 9,--跨服明星赛
    Spokesman = 10,--代言人,
    HaoYiXing = 13,--皓衣行
}

--这是task_schedule表里面填的类型
LuaEnumTaskTabType={
    RiChang=1,--日常
    JieRi=2,--节日
    XiuXian=3,--休闲
    XianXia=4,--线下
    ChengZhan=5,--城战
    SaiShi=6,--赛事
    StarBiWu = 7,--跨服明星赛
    Spokesman = 8,--代言人
    ChouJiang = 10,--抽奖
    HaoYiXing = 11,--皓衣行
}
]]
LuaEnumAlertState = {
    Show = 1,
    Hide = 2,
    Clicked = 3,
}


CLuaScheduleMgr={}

CLuaScheduleMgr.taskTabType = 0

--部分活动的红点提示比较特别
CLuaScheduleMgr.m_AlertSchedules = {}
function CLuaScheduleMgr.UpdateAlertStatus(scheduleId,actionType)
    if actionType==LuaEnumAlertState.Clicked then
        if CLuaScheduleMgr.m_AlertSchedules[scheduleId] then
            CLuaScheduleMgr.m_AlertSchedules[scheduleId] = LuaEnumAlertState.Clicked
            EventManager.Broadcast(EnumEventType.UpdateScheduleAlert)
        end
    elseif actionType==LuaEnumAlertState.Show then
        if CLuaScheduleMgr.m_AlertSchedules[scheduleId] ~= LuaEnumAlertState.Clicked then
            CLuaScheduleMgr.m_AlertSchedules[scheduleId] = LuaEnumAlertState.Show
            EventManager.Broadcast(EnumEventType.UpdateScheduleAlert)
        end
    elseif actionType==LuaEnumAlertState.Hide then
        CLuaScheduleMgr.m_AlertSchedules[scheduleId] = LuaEnumAlertState.Hide
        EventManager.Broadcast(EnumEventType.UpdateScheduleAlert)
    end
end

--请求某个活动的状态
function CLuaScheduleMgr.RequestAlertStatus()
    --跨服明星赛
    Gac2Gas.CheckStarBiwuInJingCai()
    -- 异世神武活动
    Gac2Gas.GetOffWorldRedDotInfo()

    --查询家园设计大赛奖励
    if CScheduleMgr.Inst:IsCanJoinSchedule(HouseCompetition_Setting.GetData().ScheduleId, true) then
        Gac2Gas.QueryTempPlayDataInUD(EnumTempPlayDataKey_lua.eHouseCompetitionFanPaiZiData)
    end

    --代言人契约
    Gac2Gas.CheckSpokesmanContractAward()

    Gac2Gas.RequestMySectNpcId()
    --本服开启跨服联赛期间，请求页面数据，主要是为了获取红点信息
    if CLuaScheduleMgr.taskTabType ~= 15 then
        LuaGuildLeagueCrossMgr:QueryForOpenGLCWnd() --打开页面也会发这个查询
    end
end
--该活动是否要显示
function CLuaScheduleMgr.IsScheduleNeedShowAlert(scheduleId)
    return CLuaScheduleMgr.m_AlertSchedules[scheduleId] and CLuaScheduleMgr.m_AlertSchedules[scheduleId]==LuaEnumAlertState.Show
end

--rawInfoNoTimeRestriction
--rawInfoTimeRestriction

CLuaScheduleMgr.selectedWeeklyScheduleInfo=nil
CLuaScheduleMgr.selectedScheduleInfo=nil

-- CLuaScheduleMgr.m_RecommendList={}
--[[
CLuaScheduleMgr.m_DailyList={}
CLuaScheduleMgr.m_JieRiList={}
CLuaScheduleMgr.m_CasualList={}
CLuaScheduleMgr.m_XianXiaList={}
CLuaScheduleMgr.m_ChengZhanList={}
CLuaScheduleMgr.m_SaiShiList={}--赛事
CLuaScheduleMgr.m_StarBiWuList={}--跨服明星
CLuaScheduleMgr.m_SpokesmanList = {}
CLuaScheduleMgr.m_ChouJiangList = {}--抽奖
CLuaScheduleMgr.m_HaoYiXingList = {}--皓衣行
]]
CLuaScheduleMgr.m_TableTypeList = {}   -- Tab日程List 

CLuaScheduleMgr.m_FilterLookup={}

CLuaScheduleMgr.m_PicList={}--带图片的
CLuaScheduleMgr.m_PicScheduleInfo=nil
CLuaScheduleMgr.m_PicScheduleIndex = 0

CLuaScheduleMgr.m_WuHuoQiQinCount=0
CLuaScheduleMgr.m_TaskCheck=true

CLuaScheduleMgr.m_SchoolTaskOpenStatus = true

--跨服特殊处理的活动
CLuaScheduleMgr.m_CrossServerActivityIds={
    [42000274]=true,--[跨服城战]三十六洞天
    [42000286]=true,--[城战]洞天守护灵
    [42000287]=true,--[城战]帮会押镖
    [42000274]=true,--跨服城战[开战期]
    [42000298]=true,--跨服城战[休战期]
    [42000284]=true,--[城战]真武任务
    -- [42030146]=true,--[跨服明星赛]竞猜 走表
}

--useid need CLuaScheduleMgr.BuildInfos()
function CLuaScheduleMgr:GetScheduleInfo(activityId)
    --local allList = {self.m_DailyList,self.m_JieRiList,self.m_CasualList,self.m_XianXiaList,self.m_ChengZhanList,self.m_SaiShiList
    --,self.m_StarBiWuList,self.m_SpokesmanList,self.m_ChouJiangList,self.m_HaoYiXingList}
    local allList = self.m_TableTypeList
    local scheduleInfo = nil
    for _,list in pairs(allList) do
        for _,info in pairs(list) do
            if info.activityId == activityId then
                scheduleInfo = info
                break
            end
        end
        if scheduleInfo then
            break
        end
    end
    return scheduleInfo
end

function CLuaScheduleMgr:ShowScheduleInfo(activityId)
    local scheduleInfo = self:GetScheduleInfo(activityId)
    if scheduleInfo then
        self.selectedScheduleInfo = scheduleInfo
        CUIManager.ShowUI(CLuaUIResources.ScheduleInfoWnd)
    else
        local scheduleData = Task_Schedule.GetData(activityId)
        local taskData = Task_Task.GetData(scheduleData.TaskID[0])
        local playerLv = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Level or 0

        if taskData and playerLv < taskData.Level then
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", SafeStringFormat3(LocalString.GetString("参加%s玩法需%s级"), scheduleData.TaskName, tostring(taskData.Level)))
        else
            g_MessageMgr:ShowMessage("PLAY_NOT_OPEN")
        end
    end
end

function CLuaScheduleMgr.ShowJieRiScheduleInfo(activityId, notopenmsg)
    local scheduleData = Task_Schedule.GetData(activityId)
    local fnd = false
    local jieRi = CLuaScheduleMgr:GetTodayFestivals()
    for i = 1, #jieRi do
        local data = jieRi[i]
        for j = 0, data.TaskIDs.Length - 1 do
            for k = 0, scheduleData.TaskID.Length - 1 do
                if data.TaskIDs[j] == scheduleData.TaskID[k] then
                    fnd = true
                end
                if fnd then break end
            end
            if fnd then break end
        end
    end
    if fnd then
        CLuaScheduleMgr.selectedScheduleInfo = CLuaScheduleMgr:GetScheduleInfo(activityId)
        if CLuaScheduleMgr.selectedScheduleInfo then
            CUIManager.ShowUI(CLuaUIResources.ScheduleInfoWnd)
            return
        end
    end
    
    local taskData = Task_Task.GetData(scheduleData.TaskID[0])
    local playerLv = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Level or 0

    if taskData and playerLv < taskData.Level then
        g_MessageMgr:ShowMessage("CUSTOM_STRING2", SafeStringFormat3(LocalString.GetString("参加%s玩法需%s级"), scheduleData.TaskName, tostring(taskData.Level)))
    else
        if notopenmsg == nil then
            notopenmsg = "PLAY_NOT_OPEN"
        end
        g_MessageMgr:ShowMessage(notopenmsg)
    end
end

function CLuaScheduleMgr.GetBannarEnable()
	if CommonDefs.IS_HMT_CLIENT then--HMT永久关闭Banner显示
		return false
	end
	return true
end

function CLuaScheduleMgr.GetHuoli(rawHuoli)
    if not rawHuoli then return 0 end
    local level = CClientMainPlayer.Inst and CClientMainPlayer.Inst.MaxLevel or 0
    local rtn = 0
    --从大到小
    for i=rawHuoli.Length,1,-1 do
        if rawHuoli[i-1][0]>=level then
            rtn = rawHuoli[i-1][1]
        else
            break
        end
    end

    return rtn
end

function CLuaScheduleMgr.IsLevelEnough(taskData)
    local player = CClientMainPlayer.Inst
    if player == nil or taskData == nil then
        return false
    end
    local level = player.Level
    if player.HasFeiSheng then
        level = player.XianShenLevel
        --飞升
        if taskData.GradeCheckFS1 ~= nil and taskData.GradeCheckFS1.Length > 0 then
            local gradeCheck = taskData.GradeCheckFS1[0]
            if level >= gradeCheck then
                return true
            end
        end
    else
        return level >= taskData.Level
    end
    return false
end
--暂不支持DailyTimesFormula
function CLuaScheduleMgr.GetDailyTimes(data)
    if data.DailyTimesFormula > 0 then
        return 99999
    else
        return data.DailyTimes
    end
end

--有这么一种特殊情况，策划填了个假任务做显示用，逻辑是做一系列任务，然后第一个任务接了，就显示已接，最后一个任务完成，则显示完成
function CLuaScheduleMgr.IsInProgress(taskId)
    if taskId == 22120222 then --有没有接系列任务来判断是否接任务了
        return CTaskMgr.Inst:IsInProgress(22120200) or CTaskMgr.Inst:IsInProgress(22120202)
    end
    return CTaskMgr.Inst:IsInProgress(taskId)
end

function CLuaScheduleMgr.CanShowButton(scheduleInfo)
    local ret=true
    local activityId=scheduleInfo.activityId
    local taskId=scheduleInfo.taskId
    if Task_Schedule.GetData(activityId).Type == "ShenYao" and LuaXinBaiLianDongMgr:IsShenYaoFinished() then
        ret=false
    elseif scheduleInfo.TotalTimes>0 and scheduleInfo.FinishedTimes >= scheduleInfo.RestrictTimes then
        ret=false--已完成次数
    else
        local template=Task_Task.GetData(taskId)
        local schedule=Task_Schedule.GetData(activityId)

        if CScheduleMgr.Inst:IsLevelEnough(template) then
            if CTaskMgr.Inst:IsInProgress(taskId) then
                -- print("in progress",schedule.TaskName)
                ret=false--进行中 不能参加
            else
                if schedule.NoJoinButton>0 then
                    ret=false
                else
                    if scheduleInfo.lastingTime and scheduleInfo.lastingTime>0 then--限时
                        local now=CServerTimeMgr.Inst:GetZone8Time()
                        local nowTime=(now.Hour*60+now.Minute)*60+now.Second
                        local startTime=(scheduleInfo.hour * 60 + scheduleInfo.minute) * 60 + CScheduleMgr.DelayTriggerSecond
                        local endTime=(scheduleInfo.hour * 60 + scheduleInfo.minute) * 60 + scheduleInfo.lastingTime*60
                        if nowTime> startTime and nowTime<=endTime then
                        else
                            ret=false--不在时间范围内
                        end
                    end
                end
            end
        else
            ret=false--等级不够
        end
    end
    return ret
end

function CLuaScheduleMgr.NeedShowHuoli(scheduleData)
    if scheduleData.HuoLi==nil or scheduleData.HuoLi.Length==0 or CLuaScheduleMgr.GetDailyTimes(scheduleData) == 0 then
        return false
    end
    return true
end

function CLuaScheduleMgr.GetMaxHuoLi()
    if CClientMainPlayer.Inst then
        return CClientFormula.Inst:Formula_198(CClientMainPlayer.Inst.MaxLevel)
    end
    return 0
end

function CLuaScheduleMgr.ScheduleInfoFilter(info, scheduleData)
	if info.activityId == 42000001 then
		return CLuaScheduleMgr.m_SchoolTaskOpenStatus
	elseif scheduleData.Type == "NewBieSchoolTask" then
		return not CLuaScheduleMgr.m_SchoolTaskOpenStatus
    elseif info.activityId == BeginnerGuide_Setting.GetData().ActivityId then
        return LuaQianYingChuWenMgr:IsQianYingChuWenOpen()
    end
	return true
end

-- 限时活动检查
function CLuaScheduleMgr.ScheduleInfoFilter2(info, scheduleData)
    if info.activityId == 42000429 then
        local serverID = 0
        if CClientMainPlayer.Inst then
            serverID = CClientMainPlayer.Inst:GetMyServerId()
        end
        if (not DouHunCross_GuanWangServer.Exists(serverID)) and (not DouHunCross_YingHeServer.Exists(serverID)) then
            return false
        end   
    elseif info.activityId == 42000018 then
        return not LuaGaoChangJiDouMgr:IsOpen()
    elseif info.taskId == GaoChangJiDou_Setting.GetData().ZhouLiTask then
        return false
	end
	return true
end

function CLuaScheduleMgr.BuildInfos()
    -- CLuaScheduleMgr.m_RecommendList={}
    --[[
    CLuaScheduleMgr.m_DailyList={}--1
    CLuaScheduleMgr.m_JieRiList={}--2
    CLuaScheduleMgr.m_CasualList={}--3
    CLuaScheduleMgr.m_XianXiaList={}--4
    CLuaScheduleMgr.m_ChengZhanList={}--5
    CLuaScheduleMgr.m_SaiShiList={} --6
    CLuaScheduleMgr.m_StarBiWuList={} --7
    CLuaScheduleMgr.m_SpokesmanList = {}
    CLuaScheduleMgr.m_ChouJiangList = {}
    CLuaScheduleMgr.m_HaoYiXingList = {}
]]
    CLuaScheduleMgr.m_TableTypeList = {}
    local mainPlayerLevel=0
    local hasFeiSheng=false
    local xianshenLevel=0
    local vipLevel = 0
    if CClientMainPlayer.Inst then
        mainPlayerLevel=CClientMainPlayer.Inst.Level
        hasFeiSheng=CClientMainPlayer.Inst.HasFeiSheng
        xianshenLevel=CClientMainPlayer.Inst.XianShenLevel
        vipLevel = CClientMainPlayer.Inst.ItemProp.Vip.Level
    end
    local function isLevelEnough(taskData)
        if hasFeiSheng then
            if taskData.GradeCheckFS1 and taskData.GradeCheckFS1.Length>0 then
                local gradeCheck=taskData.GradeCheckFS1[0]
                if xianshenLevel>=gradeCheck then
                    return true,0
                end
            end
        else
            return mainPlayerLevel>=taskData.Level, taskData.Level-mainPlayerLevel
        end
        return false,0
    end

    local now = CServerTimeMgr.Inst:GetZone8Time()

    local minute=now.Hour*60+now.Minute
    local lookup = {}

    local sortFunc=function(a,b)
        local task1=a:GetSchedleInfo()
        local task2=b:GetSchedleInfo()
        local startTime1=a.hour*60+a.minute
        local startTime2=b.hour*60+b.minute
        -- local endTime1=startTime1+a.lastingTime
        local taskFinished1=a:IsRegardAsFinished()
        local taskFinished2=b:IsRegardAsFinished()
        local isOver1=a.IsOver
        local isOver2=b.IsOver

        local id1= a.taskId
        local id2= b.taskId
        local taskTemplate1 = lookup[id1] and lookup[id1] or Task_Task.GetData(id1)
        lookup[id1] = taskTemplate1
        local taskTemplate2 = lookup[id2] and lookup[id2] or Task_Task.GetData(id2)
        lookup[id2] = taskTemplate2

        local levelEnough1,levelDiff1 = isLevelEnough(taskTemplate1)
        local levelEnough2,levelDiff2 = isLevelEnough(taskTemplate2)


        local timeDisable1,timeDiff1 = minute<startTime1,startTime1-minute
        local timeDisable2,timeDiff2 = minute<startTime2,startTime2-minute


        if taskFinished1 and not taskFinished2 then
            return false
        elseif not taskFinished1 and taskFinished2 then
            return true
        elseif taskFinished1 and taskFinished2 then
            return a.activityId<b.activityId
        elseif isOver1 and not isOver2 then
            return false
        elseif not isOver1 and isOver2 then
            return true
        elseif isOver1 and isOver2 then
            return a.activityId<b.activityId
        elseif levelEnough1 and not levelEnough2 then
            return true
        elseif not levelEnough1 and levelEnough2 then
            return false
        elseif not levelEnough1 and not levelEnough2 then
            return levelDiff1 < levelDiff2
        elseif timeDisable1 and not timeDisable2 then
            return false
        elseif not timeDisable1 and timeDisable2 then
            return true
        elseif timeDisable1 and timeDisable2 then
            return timeDiff1<timeDiff2
        else
            local pl1=task1.PriorityLevel
            local pl2=task2.PriorityLevel
            pl1=pl1>0 and pl1 or 999
            pl2=pl2>0 and pl2 or 999
            if pl1>pl2 then
                return false
            elseif pl1<pl2 then
                return true
            elseif startTime1< startTime2 then
                return true
            elseif startTime1>startTime2 then
                return false
            else
                return a.activityId<b.activityId
            end
        end

    end

    -- 未开放、已结束、已完成

    --看有没有需要显示banner的活动
    local nowTime=now.Hour*3600+now.Minute*60+now.Second

    local rawlist1=CScheduleMgr.Inst.rawInfoNoTimeRestriction--日常
    local rawlist2=CScheduleMgr.Inst.rawInfoTimeRestriction--限时

    --CLuaScheduleMgr.m_PicList扩展成包含两类结构
    --有一类是传统的, 另一类是只有gameplay_propagandaAlert里的idx的
    CLuaScheduleMgr.m_PicList={}
    CLuaScheduleMgr.m_FilterLookup={{},{},{}}
    table.insert(CLuaScheduleMgr.m_PicList,0)--代言人bannar置顶 暂时这么处理一下

    --检查vip等级
    if rawlist1 then
        for i=1,rawlist1.Count do
            local info=rawlist1[i-1]
            local scheduleData=Task_Schedule.GetData(info.activityId)
            if CScheduleMgr.ScheduleInfoFilter(info,10,true,false) and CLuaScheduleMgr.ScheduleInfoFilter(info, scheduleData) then
                if scheduleData.VIPGradeLimit>0 and scheduleData.VIPGradeLimit>vipLevel then
                    --vip限制
                else
                    if scheduleData.PicThub and scheduleData.PicThub~="" then
                        if string.find(scheduleData.PicThub, "banner_066", 1, true) ~= nil then
                            CLuaScheduleMgr.m_PicList[1] = info
                        else
                            table.insert( CLuaScheduleMgr.m_PicList,info )
                        end
                    end
    
                    for i = 0, scheduleData.TaskTabType.Length - 1 do 
                        local tableType = scheduleData.TaskTabType[i]
                        local tabTypeInfo = Task_TabType.GetData(tableType)
                        if not (tabTypeInfo.ShowOnlyAcceptable == 1) or CLuaScheduleMgr.CheckTaskState(info.taskId) then
                            if not CLuaScheduleMgr.m_TableTypeList[tableType] then
                                CLuaScheduleMgr.m_TableTypeList[tableType] = {}
                            end
                            table.insert( CLuaScheduleMgr.m_TableTypeList[tableType],info)
                            
                            if scheduleData.TaskType then
                                for i=1,scheduleData.TaskType.Length do
                                    CLuaScheduleMgr.m_FilterLookup[scheduleData.TaskType[i-1]][info.activityId]=true
                                end
                            end
                            
                        end
                    end
                end
            end
        end
    end

    if rawlist2 then
        for i=1,rawlist2.Count do
            local info=rawlist2[i-1]
            local scheduleData=Task_Schedule.GetData(info.activityId)
            if CScheduleMgr.ScheduleInfoFilter(info,10,true,false) and CLuaScheduleMgr.ScheduleInfoFilter2(info, scheduleData) then
                if scheduleData.VIPGradeLimit>0 and scheduleData.VIPGradeLimit>vipLevel then
                    --vip限制
                else
                    if scheduleData.PicThub and scheduleData.PicThub~="" then
                        if string.find(scheduleData.PicThub, "banner_066", 1, true) ~= nil then
                            CLuaScheduleMgr.m_PicList[1] = info
                        else
                            table.insert( CLuaScheduleMgr.m_PicList,info )
                        end
                    end

                    for i = 0, scheduleData.TaskTabType.Length - 1 do 
                        local tableType = scheduleData.TaskTabType[i]
                        local tabTypeInfo = Task_TabType.GetData(tableType)
                        if not (tabTypeInfo.ShowOnlyAcceptable == 1) or CLuaScheduleMgr.CheckTaskState(info.taskId) then
                            if not CLuaScheduleMgr.m_TableTypeList[tableType] then
                                CLuaScheduleMgr.m_TableTypeList[tableType] = {}
                            end
                            table.insert( CLuaScheduleMgr.m_TableTypeList[tableType],info)
        
                            if scheduleData.TaskType then
                                for i=1,scheduleData.TaskType.Length do
                                    CLuaScheduleMgr.m_FilterLookup[scheduleData.TaskType[i-1]][info.activityId]=true
                                end
                            end
                        end
                    end
                end
            end
        end
    end
    
    local segments=CScheduleMgr.Inst:GetSegmentedActivity()
    for i=1,segments.Count do
        local info=segments[i-1]
        local scheduleData=Task_Schedule.GetData(info.activityId)
        if scheduleData.VIPGradeLimit>0 and scheduleData.VIPGradeLimit>vipLevel then
            --vip限制
        else
            if scheduleData.PicThub and scheduleData.PicThub~="" then
                if string.find(scheduleData.PicThub, "banner_066", 1, true) ~= nil then
                    CLuaScheduleMgr.m_PicList[1] = info
                else
                    table.insert( CLuaScheduleMgr.m_PicList,info )
                end
            end

            for i = 0, scheduleData.TaskTabType.Length - 1 do 
                local tableType = scheduleData.TaskTabType[i]
                local tabTypeInfo = Task_TabType.GetData(tableType)
                if not (tabTypeInfo.ShowOnlyAcceptable == 1) or CLuaScheduleMgr.CheckTaskState(info.taskId) then
                    if not CLuaScheduleMgr.m_TableTypeList[tableType] then
                        CLuaScheduleMgr.m_TableTypeList[tableType] = {}
                    end
                    table.insert( CLuaScheduleMgr.m_TableTypeList[tableType],info)
    
                    if scheduleData.TaskType then
                        for i=1,scheduleData.TaskType.Length do
                            CLuaScheduleMgr.m_FilterLookup[scheduleData.TaskType[i-1]][info.activityId]=true
                        end
                    end
                end
            end
        end
    end
    for _,Tabletypeitem in pairs(CLuaScheduleMgr.m_TableTypeList) do
        table.sort(Tabletypeitem,sortFunc)
    end
    -- table.sort( CLuaScheduleMgr.m_RecommendList,sortFunc )
    --[[
    table.sort( CLuaScheduleMgr.m_DailyList,sortFunc )
    table.sort( CLuaScheduleMgr.m_JieRiList,sortFunc )
    table.sort( CLuaScheduleMgr.m_CasualList,sortFunc )
    table.sort( CLuaScheduleMgr.m_XianXiaList,sortFunc )
    table.sort( CLuaScheduleMgr.m_ChengZhanList,sortFunc )
    table.sort( CLuaScheduleMgr.m_SaiShiList,sortFunc )
    table.sort( CLuaScheduleMgr.m_SpokesmanList,sortFunc )
    table.sort( CLuaScheduleMgr.m_ChouJiangList,sortFunc )
    table.sort( CLuaScheduleMgr.m_HaoYiXingList,sortFunc )]]
    Gameplay_PropagandaAlert.Foreach(function (alertId, alertCfg)
        --开关为开
        if alertCfg.Status == 0 then
            --有bannerPath数据
            if alertCfg.BannerPicturePath and alertCfg.BannerPicturePath ~= "" then
                local needCheckOfficalChannel = (CommonDefs.IS_CN_CLIENT and (not CLoginMgr.Inst:IsNetEaseOfficialLogin()))
                local hiddenInChannel = CommonDefs.IS_CN_CLIENT
                        and alertCfg.HiddenInCHannels
                        and CommonDefs.HashSetContains(alertCfg.HiddenInCHannels, typeof(cs_string), CLoginMgr.Inst:GetLoginChannelConverted())
                if (((not needCheckOfficalChannel) or alertCfg.IsOfficial == 0) and (not hiddenInChannel)) then
                    table.insert( CLuaScheduleMgr.m_PicList, { alertId = alertId})
                end
            end
        end
    end)
    
    --代言人bannar置顶 暂时这么处理一下
    local picTop = CLuaScheduleMgr.m_PicList[1]
    if picTop and picTop==0 then
        table.remove(CLuaScheduleMgr.m_PicList,1)
    end

    table.sort(CLuaScheduleMgr.m_PicList,function (a,b)
        local priorityList = {0, 0}
        local infoList = {a, b}
        for i = 1, 2 do
            if infoList[i].activityId then
                local scheduleData = Task_Schedule.GetData(infoList[i].activityId)
                if scheduleData then
                    priorityList[i] = scheduleData.PicThubPriority
                end
            elseif infoList[i].alertId then
                local alertData = Gameplay_PropagandaAlert.GetData(infoList[i].alertId)
                if alertData then
                    priorityList[i] = alertData.BannerPriority
                end
            end
        end
        return priorityList[1] > priorityList[2]
    end)
    --读取JieRiGroup表
    --CLuaScheduleMgr.LoadJieRiList()
    LuaGaoChangJiDouMgr:Filter()
end

--很多行为都是自定义的
function CLuaScheduleMgr.TrackSchedule(info)
    if CClientMainPlayer.Inst==nil then return end

    if info.activityId == 42000252 then--全民pk每周战报
        CLuaShareMgr.ShareQMPKZhanBao()
        return
    elseif info.activityId == 42000286 then --洞天守护灵
        CLuaScheduleMgr.JoinShouHuLing()
        return
    elseif info.activityId == 42000287 then --城战帮会押镖
        CLuaScheduleMgr.JoinYaYun()
        return
    elseif info.activityId == 42000284 then  --城战真武任务
        Gac2Gas.RequestBackToOwnCity()
        return
    elseif info.activityId == 42000355 then -- 儿童节进游乐场
        if CScene.MainScene.SceneTemplateId ~= 16000197 then
            Gac2Gas.RequestTeleportToLiuYiPark()
            return
        end
    elseif info.activityId == 42000363 then
		CLuaScheduleMgr.JoinGanEnZhiYuTask(info.taskId)
		return
    elseif info.activityId == 42010125 then --水漫金山
        Gac2Gas.ShuiManJinShan_CheckPlayOpenMatch(tostring(info.taskId))
        return
	end
    if info.activityId == 42030287 then -- 2023年春节火树银花自动寻路时关闭界面
        if CUIManager.IsLoaded(CLuaUIResources.ChunJie2023MainWnd) then
            CUIManager.CloseUI(CLuaUIResources.ChunJie2023MainWnd)
        end
    end
    CLuaScheduleMgr.RealTrackSchedule(info)
end

function CLuaScheduleMgr.RealTrackSchedule(info)
    local data = Task_Schedule.GetData(info.activityId)

    --先匹配action
    if data.Action and data.Action~="" then
        local actionName,params = string.match(data.Action,"(%w+)%((.+)%)")
        if actionName=="RequestCrossMap" then
            local t = {}
            for p in string.gmatch(params, "([^,]+)") do
                table.insert(t,tonumber(p))
            end
            local sceneTemplateId,gridX,gridY,npcId = t[1],t[2],t[3],t[4]

            local dict = CreateFromClass(MakeGenericClass(Dictionary, String, Object))
            CommonDefs.DictAdd_LuaCall(dict, "scheduleId", info.activityId)
            local onSuccess = nil
            if npcId then
                onSuccess = DelegateFactory.Action(function()
                    local objs = {}
                    CClientObjectMgr.Inst:ForeachObject(DelegateFactory.Action_CClientObject(function(obj)
                        table.insert(objs,obj)
                    end))
                    for i,obj in ipairs(objs) do
                        if obj.ObjectType==EnumObjectType.NPC and obj.TemplateId==npcId and obj.Visible then
                            if CClientMainPlayer.Inst then
                                CClientMainPlayer.Inst.Target = obj
                                CClientMainPlayer.Inst:InteractWithNpc(obj.EngineId)
                            end
                            break
                        end
                    end
                end)
            end
            --如果之前有移动逻辑，需要先停止
            CTaskMgr.Inst:StopTrackForTask(true)
		    CTrackMgr.Inst:CrossMapTrack("",sceneTemplateId,CPos(gridX,gridY),32,onSuccess,nil,2,dict)
            return
        end
    end
    
    if not CScheduleMgr.Inst:DoScheduleTask(info.activityId) then
        if CTaskMgr.Inst:IsTrackingToDoTask(info.taskId) then
            return
        end
        CTaskMgr.Inst:TrackToDoTask(info.taskId)
    end
end

function CLuaScheduleMgr.JoinGanEnZhiYuTask(taskId)
	local taskProp = CClientMainPlayer.Inst.TaskProp
	if CommonDefs.DictContains(taskProp.CurrentTasks, typeof(UInt32), taskId) then
		return
	end
    
	if taskProp:IsMainPlayerTaskFinished(taskId) then
		return
	end

	local msg = g_MessageMgr:FormatMessage("GUIJIE2019_GANENZHIYU_TASK_CONFIRM")
	g_MessageMgr:ShowOkCancelMessage(msg, function() 
		Gac2Gas.RequestAcceptGanEnZhiYuTask()
	end, nil, LocalString.GetString("感到好奇"), LocalString.GetString("路过"), false)
end

function CLuaScheduleMgr.JoinShouHuLing()
    CLuaScheduleMgr.DoJoinYaYun()
end

function CLuaScheduleMgr.JoinYaYun()
    CLuaScheduleMgr.DoJoinYaYun()
end

function CLuaScheduleMgr.DoJoinYaYun()
    -- 根据策划的意见，洞天守护灵和帮会押镖的参与按钮行为保持一致
    local mainplayer = CClientMainPlayer.Inst
    if mainplayer and mainplayer.IsCrossServerPlayer then
        Gac2Gas.QueryMyCityData(EnumQueryCityDataAction.eJoinYaYun)
    else
        --本服，尝试传送到跨服
        Gac2Gas.RequestTeleportToTerritoryByBigMap()
    end
end

function Gas2Gac.QueryWuHuoQiQinCount(wuHuoQiQinCount)
    CLuaScheduleMgr.m_WuHuoQiQinCount=wuHuoQiQinCount
    g_ScriptEvent:BroadcastInLua("QueryWuHuoQiQinCount",wuHuoQiQinCount)

end

function Gas2Gac.SendWeeklyInfo( dataStr )
    local infos={}
    for activityId, minute, hour, day, taskId, lastingTime in string.gmatch(dataStr, "(%d+),(%d+),(%d+),(%d+),(%d+),(%d+);?") do
        local info={
            activityId = tonumber(activityId),
            minute = tonumber(minute),
            hour = tonumber(hour),
            day = tonumber(day),
            taskId = tonumber(taskId),
            lastingTime = tonumber(lastingTime),
        }
        local data = Task_Schedule.GetData(info.activityId)
        if data then
            info.name = data.TaskName
            if data.TureTaskID>0 then
                info.FinishedTimes = CScheduleMgr.GetFinishedTimes(info.activityId, data.TureTaskID)
            else
                info.FinishedTimes = CScheduleMgr.GetFinishedTimes(activityId, taskId)
            end
            info.TotalTimes = CLuaScheduleMgr.GetDailyTimes(data)
        end
        table.insert(infos,info)
    end

    LuaGaoChangJiDouMgr:Filter(infos)
    
    g_ScriptEvent:BroadcastInLua("SendWeeklyInfo",infos)

end

function CLuaScheduleMgr.IsShowDoubleScore(activityId,day)
    local data = Task_Schedule.GetData(activityId)
    if data then
        local array = data.DoubleDisplay
        if array then
            local cronday = day + 1
            if cronday==7 then
                cronday = 0
            end
            for i=1,array.Length do
                if array[i-1]==cronday then
                    return true
                end
            end
        end
    end
    return false
end

function Gas2Gac.SendSchoolTaskOpenStatus(openStatus)
	if CLuaScheduleMgr.m_SchoolTaskOpenStatus ~= openStatus then
		CLuaScheduleMgr.m_SchoolTaskOpenStatus = openStatus

		EventManager.Broadcast(EnumEventType.UpdateActivity)
	end
end

function CLuaScheduleMgr.QueryXinFuLotteryResult()
    Gac2Gas.QueryXinFuLotteryResult()
end

CLuaScheduleMgr.ChouJiangData = {}

function Gas2Gac.SendXinFuLotteryResult(resultUD,day)
    if day == nil then day = 1 end
    CLuaScheduleMgr.ChouJiangData.Day = day
    local data = MsgPackImpl.unpack(resultUD)
    local reward = {}
    for i=0,data.Count-1,3 do
        local rday = tonumber(data[i])
        if reward[rday] == nil then 
            reward[rday] = {}
        end
        local count = #reward[rday]
        reward[rday][count+1] = {pid = data[i+1], pname = data[i+2]}
    end
    CLuaScheduleMgr.ChouJiangData.Reward = reward
    g_ScriptEvent:BroadcastInLua("OnChouJiangDataChange")
end

function CLuaScheduleMgr.CheckTaskState(taskId)
    return CommonDefs.ListContains_LuaCall(CClientMainPlayer.Inst.TaskProp.AcceptableTaskList, taskId)
end



--@region JieRi

CLuaScheduleMgr.m_Activity2JieRiGroupId = {} -- Task2GroupId
CLuaScheduleMgr.s_IsInitActivity2JieRiGroupId = false
-- obsolete
function CLuaScheduleMgr.LoadJieRiList()
    if CLuaScheduleMgr.s_IsInitActivity2JieRiGroupId then
        return
    end
    Task_JieRiGroup.Foreach(function (groupId, data)
        CommonDefs.ListIterate(data.TaskIDs, DelegateFactory.Action_object(function (___value)
            local kv = ___value
            if kv ~= nil then
                CLuaScheduleMgr.m_Activity2JieRiGroupId[kv] = groupId
            end
        end))
    end)
    CLuaScheduleMgr.s_IsInitActivity2JieRiGroupId = true
end

function CLuaScheduleMgr.OnJieRiGroupScheduleClick(data)
    local reddotInfo = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp.FestivalReddotInfo
    if reddotInfo then
        if not CommonDefs.DictContains(reddotInfo, typeof(UInt32), data.ID) then
            CommonDefs.DictSet_LuaCall(reddotInfo, data.ID, CServerTimeMgr.Inst.timeStamp)
        end
    end
    Gac2Gas.RequestRemoveFestivalReddot(data.ID)

    LuaCommonJieRiMgr.jieRiId = data.ID
    if not String.IsNullOrEmpty(data.ActivityTabName) then
        CUIManager.ShowUI(CLuaUIResources.JieRiMainWnd)
    else
        local wndName = data.OpenWnd
        CUIActionMgr.Inst:Show(wndName,nil,nil,nil,nil,nil)
    end 

    LuaActivityRedDotMgr:OnRedDotClicked(data.ID, LuaEnumRedDotType.JieRi)

    EventManager.Broadcast(EnumEventType.SyncDynamicActivitySimpleInfo)
    --CLuaScheduleMgr.TrackSchedule(info)
    CUIManager.CloseUI(CLuaUIResources.JieRiGroupScheduleWnd)
end

CLuaScheduleMgr.m_IsShowJieRiButtonFx = false

function CLuaScheduleMgr:IsNeedShowJieRiButtonFx(data)
    if not CClientMainPlayer.Inst then return false end

    local reddotInfo = CClientMainPlayer.Inst.PlayProp.FestivalReddotInfo
    if reddotInfo then
        local now = CServerTimeMgr.Inst.timeStamp
        local lastStartTime
        local lastClickTime = CommonDefs.DictGetValue_LuaCall(reddotInfo,data.ID)
        if not lastClickTime then return true end
        for time in string.gmatch(data.StartTime, "[^;]+") do
            local startTime = CServerTimeMgr.Inst:GetTimeStampByStr(time)
            if now < startTime then
                return lastStartTime and lastClickTime < lastStartTime
            end
            lastStartTime = startTime
        end

        return lastClickTime < lastStartTime
    end

    --wyh 新节日机制和日程没有直接关联
    --[[
    local finishedTimes = scheduleInfo.FinishedTimes--每日完成次数
    if scheduleInfo.lastingTime > 0 then
        if CScheduleMgr.Inst:GetAlertState(scheduleInfo.activityId) == CScheduleMgr.EnumAlertState.Show 
            and not CTaskMgr.Inst:IsInProgress(scheduleInfo.taskId) and finishedTimes==0 then
            return true --原活动任务的红点机制
        end
    end
    --]]

    return false
end

CLuaScheduleMgr.m_TodayFestivals = {}
CLuaScheduleMgr.m_OpenFestivalTasks = {}

function CLuaScheduleMgr:GetTodayFestivals()
    return self.m_TodayFestivals
end

function CLuaScheduleMgr:IsFestivalTaskOpen(taskId, fesId)
    if fesId then
        local tasks = self.m_OpenFestivalTasks[fesId]
        if tasks then 
            for _, id in pairs(tasks) do
                if id == taskId then
                    return true
                end
            end
        end
    else
        for _, tasks in pairs(self.m_OpenFestivalTasks) do
            for __, id in pairs(tasks) do
                if id == taskId then
                    return true
                end
            end
        end
    end
    return false
end

function CLuaScheduleMgr:BuildDayFestivalInfo(todayFestival, time)
    self.m_TodayFestivals = {}
    self.m_OpenFestivalTasks = {}
    for str in string.gmatch(todayFestival, "([^;]+)") do
        -- 每条信息的第一项是节日id, 后面跟着当前处于开放状态的任务id: festivalId,taskId1,taskId2;
        local index = 0
        local jieRiId
        for id in string.gmatch(str, "([^,]+)") do
            id = tonumber(id)
            if index < 1 then
                jieRiId = id
                self.m_OpenFestivalTasks[jieRiId] = {}
                local data = Task_JieRiGroup.GetData(jieRiId)
                if data then
                    table.insert(self.m_TodayFestivals, data)
                end
            else
                table.insert(self.m_OpenFestivalTasks[jieRiId], id)
            end
            index = index + 1
        end
    end
    table.sort(self.m_TodayFestivals, function(a, b)
        return CServerTimeMgr.Inst:GetTimeStampByStr(a.StartTime) < CServerTimeMgr.Inst:GetTimeStampByStr(b.StartTime)
    end)
    --self.BuildInfos()
    EventManager.Broadcast(EnumEventType.SyncDynamicActivitySimpleInfo)
    LuaActivityRedDotMgr:StartCheck()

    g_ScriptEvent:BroadcastInLua("DayFestivalInfoUpdated")
end

function CLuaScheduleMgr:GetTaskCheckLevel(taskInfo)
    if taskInfo.ID == 22200206 or taskInfo.ID == 22200207 or  taskInfo.ID == 22200215 then 
        -- 采薇吉星守财等级检查
        local day = 0
        local newLevel = taskLevel
        if taskInfo.ID == 22200215 then  -- 守财
            day = QiFuShouCai_Setting.GetData().ExpAdjustSpan
            newLevel = QiFuShouCai_Setting.GetData().ExpAdjustLv
        else  -- 采薇吉星
            day = CaiweiJixing_CommonSetting.GetData().ExpAdjustSpan
            newLevel = CaiweiJixing_CommonSetting.GetData().ExpAdjustLv
        end
        if LuaLoginMgr.m_ServerOpenTime >=0 and LuaLoginMgr.m_ServerOpenTime <= day then
            return newLevel
        end
    end

    return taskInfo.Level
end

--@endregion

LuaCommonJieRiMgr = {}

LuaCommonJieRiMgr.jieRiId = 0

EnumCommonJieRiDisplayType = {
    ActivityButton = 1,
    ImportantActivity = 2,
    NonImportantActivity = 3,
}

function LuaCommonJieRiMgr:ShowMainWnd(wndName, jieRiId)
    LuaCommonJieRiMgr.jieRiId = tonumber(jieRiId)
    CUIManager.ShowUI(wndName)
end