local CUIManager = import "L10.UI.CUIManager"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local LocalString = import "LocalString"
local CAchievementMgr = import "L10.Game.CAchievementMgr"
local Vector3 = import "UnityEngine.Vector3"

LuaAchievementMgr = class()

LuaAchievementMgr.m_CurAchieveTab = nil
LuaAchievementMgr.m_CurAchieveSubTab = nil
LuaAchievementMgr.m_CurGroupId = 0

LuaAchievementMgr.m_CurAchievementId = 0 -- 成就奖励tip AchievementAwardsTipWnd需要用到的参数
LuaAchievementMgr.m_ShowViewButton = false
LuaAchievementMgr.m_AwardsTipTargetWorldPos = Vector3.zero
LuaAchievementMgr.m_AwardsTipTargetWidth = 0
LuaAchievementMgr.m_AwardsTipTargetHeight = 0

function LuaAchievementMgr:ShowDetailWndByAchievementId(achievementId)
	if achievementId and Achievement_Achievement.Exists(achievementId) then
		local achievement = Achievement_Achievement.GetData(achievementId)
		local group = Achievement_Group.GetData(achievement.Group)
		self.m_CurAchieveTab = group.TabName
		self.m_CurAchieveSubTab = group.SubTabName
		self.m_CurGroupId = achievement.Group
		CUIManager.ShowUI("AchievementDetailWnd")
	end
end

function LuaAchievementMgr:ShowDetailWndByAchievementByTabName(achieveTabName)
	if not self:ContainsTab(achieveTabName) then return end
	self.m_CurAchieveTab = achieveTabName
	self.m_CurAchieveSubTab = nil
	self.m_CurGroupId = 0
	CUIManager.ShowUI("AchievementDetailWnd")
end

function LuaAchievementMgr:ClearCurAchieveTabInfo()
	self.m_CurAchieveTab = nil
	self.m_CurAchieveSubTab = nil
	self.m_CurGroupId = 0
end

function LuaAchievementMgr:ShowAwardsTip(achievementId, showViewbutton, targetWorldPos, targetWidth, targetHeight)
	if achievementId and Achievement_Achievement.Exists(achievementId) then
		self.m_CurAchievementId = achievementId
		self.m_ShowViewButton = showViewbutton
		self.m_AwardsTipTargetWorldPos = targetWorldPos
		self.m_AwardsTipTargetWidth = targetWidth
		self.m_AwardsTipTargetHeight = targetHeight
		CUIManager.ShowUI("AchievementAwardsTipWnd")
	end
end

function LuaAchievementMgr:ContainsTab(achieveTabName)
	if not achieveTabName then return end
	return CommonDefs.DictContains_LuaCall(CAchievementMgr.Inst.m_TabNameList, achieveTabName)
end

function LuaAchievementMgr:ContainsGroup(groupId)
	if not groupId then return end
	return CommonDefs.DictContains_LuaCall(CAchievementMgr.Inst.m_GroupList, groupId)
end

function LuaAchievementMgr:GetGroupAchievements(groupId)
	if self:ContainsGroup(groupId) then
		return CommonDefs.DictGetValue_LuaCall(CAchievementMgr.Inst.m_GroupList, groupId)
	end
	return nil
end

--查找groupId对应的组下面当前尚未领取奖励的成就项(正在进行中或者尚未领取奖励)
function LuaAchievementMgr:GetCurrentNoneAwardedAchievement(groupId)
	local achievements = LuaAchievementMgr:GetGroupAchievements(groupId)
	local achievementId = 0
	local achievement = nil
	local mainplayer = CClientMainPlayer.Inst
	if not achievements or not mainplayer then return achievementId, achievement end
	local firstFinishButNotAwardedAchievement = nil --第一个已完成但未领取奖励的成就
	local firstNotFinishedAchievement = nil --第一个进行中的成就
	local lastFinishAndAwardedAchievement = nil --最后一个已完成并且已领取奖励的成就
    for i=0,achievements.Count-1 do
        achievement = Achievement_Achievement.GetData(achievements[i])
		if not mainplayer.PlayProp.Achievement:HasAwardFinished(achievements[i]) then
			if (mainplayer.PlayProp.Achievement:HasAchievement(achievements[i])) then
				firstFinishButNotAwardedAchievement = achievement
				break
			elseif firstNotFinishedAchievement==nil then
				firstNotFinishedAchievement = achievement
			end
        elseif i == achievements.Count - 1 then
			lastFinishAndAwardedAchievement = achievement
        end
	end
	if firstFinishButNotAwardedAchievement then
		return firstFinishButNotAwardedAchievement.ID, firstFinishButNotAwardedAchievement
	elseif firstNotFinishedAchievement then
		return firstNotFinishedAchievement.ID, firstNotFinishedAchievement
	elseif lastFinishAndAwardedAchievement then
		return lastFinishAndAwardedAchievement.ID, lastFinishAndAwardedAchievement
	end
	return 0, nil
end
--查找groupId对应的组下面当前已经完成的成就项
function LuaAchievementMgr:GetFinishedAchievementCount(groupId)
	local count = 0
    local achievements = LuaAchievementMgr:GetGroupAchievements(groupId)
    local mainplayer = CClientMainPlayer.Inst
	if not achievements or not mainplayer then return count end
    for i=0,achievements.Count-1 do
        local achievement = Achievement_Achievement.GetData(achievements[i])
        if achievement and mainplayer.PlayProp.Achievement:HasAchievement(achievements[i]) then
            count = count + 1
        end
    end
    return count
end

function LuaAchievementMgr:GetAchievementProgress(achievementId)
	return CAchievementMgr.Inst:GetAchievementProgress(achievementId)
end


function LuaAchievementMgr:GetSubTabsByCurAchievementTab()
	-- TabName大类->SubTabName小类->GroupId成就分组->AchievementId子成就ID
	-- SubTabName可以没有，从TabName直接指向GroupId
	local tbl = {}

	if not self:ContainsTab(self.m_CurAchieveTab) then return tbl end

	local tabInfo = CommonDefs.DictGetValue_LuaCall(CAchievementMgr.Inst.m_TabNameList, self.m_CurAchieveTab)

	local groupIdTbl = {}
	if tabInfo.subTabName.Count>0 then
	 	--有子Tab
	 	CommonDefs.DictIterate(tabInfo.subTabName, DelegateFactory.Action_object_object(function (subTabName, subTabInfo)
        	local subGroupIdTbl = {}
        	for i=0,subTabInfo.groupId.Count-1 do
        		table.insert(groupIdTbl, subTabInfo.groupId[i])
        		table.insert(subGroupIdTbl, subTabInfo.groupId[i])
        	
        	end
        	table.insert(tbl, {key=subTabName, value=subGroupIdTbl})
    	end))
	 else
	 	--无子Tab
	 	for i=0,tabInfo.groupId.Count-1 do
	 		table.insert(groupIdTbl, tabInfo.groupId[i])
	 	end
	 end
	 --第一行是个伪造的tab, 对于没有子tab的情况，只会有一个全部的tab
	 table.insert(tbl, 1, {key=LocalString.GetString("全部"), value = groupIdTbl})

	 return tbl
end

function LuaAchievementMgr:HideFinishedAchievement()
	return CAchievementMgr.Inst.HideFinishAchievement
end

-- 主角是否达成一组成就且奖励领取完毕
function LuaAchievementMgr:MainPlayerFinishAllAchievementsAndGetAllAwardsInGroup(groupId)
	local playProp = CClientMainPlayer.Inst and CClientMainPlayer.Inst.PlayProp
	if not playProp then return end

	local achieveList = CommonDefs.DictGetValue(CAchievementMgr.Inst.m_GroupList, typeof(UInt32), groupId)
    local allFinished = true
    for j=1,achieveList.Count do
        if not playProp.Achievement:HasAchievement(achieveList[j-1]) then --没有达成成就
            allFinished = false
            break
        elseif not  CAchievementMgr.Inst:MainPlayerHasAwardedAchievement(achieveList[j-1]) then --没有领取奖励
        	allFinished = false
        	break
        end
    end

    return allFinished
end

---
--获取某个Tab下对应的成信息，返回三个数值分别为 totalCount,finishCount,finishButNotAwardedCount
---
function LuaAchievementMgr:GetAchievementsInfoByTabName(tabName)
	
	-- TabName大类->SubTabName小类->GroupId成就分组->AchievementId子成就ID
	-- SubTabName可以没有，从TabName直接指向GroupId
	local totalCount = 0 --所有成就,统计group数量
	local finishCount = 0 --完成的成就，统计group数量
	local finishButNotAwardedCount = 0 --完成但尚未领奖的成就子项

	if not self:ContainsTab(tabName) then return totalCount,finishCount,finishButNotAwardedCount end

	local tabInfo = CommonDefs.DictGetValue_LuaCall(CAchievementMgr.Inst.m_TabNameList, tabName)

	if tabInfo.subTabName.Count>0 then
	 	--有子Tab
	 	CommonDefs.DictIterate(tabInfo.subTabName, DelegateFactory.Action_object_object(function (subTabName, subTabInfo)
        	local subGroupIdTbl = {}
        	for i=0,subTabInfo.groupId.Count-1 do
        		totalCount = totalCount + 1
        		local achievements = CommonDefs.DictGetValue_LuaCall(CAchievementMgr.Inst.m_GroupList, subTabInfo.groupId[i])
        		local allFinished = true
        		for j=0, achievements.Count-1 do
        			if CAchievementMgr.Inst:MainPlayerHasAchievement(achievements[j]) then
        				if not CAchievementMgr.Inst:MainPlayerHasAwardedAchievement(achievements[j]) then
        					finishButNotAwardedCount = finishButNotAwardedCount + 1
        				end
        			else
        				allFinished = false
        			end
        		end
				if allFinished then
					finishCount = finishCount + 1
				end
        	end
    	end))
	 else
	 	--无子Tab
	 	for i=0,tabInfo.groupId.Count-1 do
	 		totalCount = totalCount + 1
	 		local achievements = CommonDefs.DictGetValue_LuaCall(CAchievementMgr.Inst.m_GroupList, tabInfo.groupId[i])
    		local allFinished = true
    		for j=0, achievements.Count-1 do
    			if CAchievementMgr.Inst:MainPlayerHasAchievement(achievements[j]) then
    				if not CAchievementMgr.Inst:MainPlayerHasAwardedAchievement(achievements[j]) then
    					finishButNotAwardedCount = finishButNotAwardedCount + 1
    				end
    			else
    				allFinished = false
    			end
    		end
			if allFinished then
				finishCount = finishCount + 1
			end
	 	end
	 end
	 
	 return totalCount,finishCount,finishButNotAwardedCount
end

function LuaAchievementMgr.GetAllAchievementsBeforeProfessionTransfer()
    Achievement_Achievement.ForeachKey(function (key) 
        if CClientMainPlayer.Inst.PlayProp.Achievement:HasAchievement(key) and not CClientMainPlayer.Inst.PlayProp.Achievement:HasAwardFinished(key) then
			Gac2Gas.PlayerGetAchievementAward(key, "professiontransfer")
        end
    end)
end



