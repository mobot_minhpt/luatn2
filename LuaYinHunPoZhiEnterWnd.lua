local CommonDefs          = import "L10.Game.CommonDefs"
local DelegateFactory     = import "DelegateFactory"
local CMessageTipMgr      = import "L10.UI.CMessageTipMgr"
local CTipParagraphItem   = import "L10.UI.CTipParagraphItem"
local CCommonLuaScript    = import "L10.UI.CCommonLuaScript"
local CCarnivalLotteryMgr = import "L10.Game.CCarnivalLotteryMgr"

LuaYinHunPoZhiEnterWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaYinHunPoZhiEnterWnd, "awardTable")
RegistClassMember(LuaYinHunPoZhiEnterWnd, "awardTemplate")
RegistClassMember(LuaYinHunPoZhiEnterWnd, "ruleTable")
RegistClassMember(LuaYinHunPoZhiEnterWnd, "paragraphTemplate")
RegistClassMember(LuaYinHunPoZhiEnterWnd, "maxScore")
RegistClassMember(LuaYinHunPoZhiEnterWnd, "activityTime")
RegistClassMember(LuaYinHunPoZhiEnterWnd, "enterButton")
RegistClassMember(LuaYinHunPoZhiEnterWnd, "rankButton")

function LuaYinHunPoZhiEnterWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
    self:InitEventListener()
    self:InitActive()
end

-- 初始化UI组件
function LuaYinHunPoZhiEnterWnd:InitUIComponents()
    local anchor = self.transform:Find("Anchor")
    self.activityTime = anchor:Find("Time/Time"):GetComponent(typeof(UILabel))
    self.awardTable = anchor:Find("Award/Table"):GetComponent(typeof(UITable))
    self.awardTemplate = anchor:Find("Award/ItemTemplate").gameObject
    self.maxScore = anchor:Find("Award/MaxScore"):GetComponent(typeof(UILabel))
    self.ruleTable = anchor:Find("Rule/ScrollView/Table"):GetComponent(typeof(UITable))
    self.paragraphTemplate = anchor:Find("Rule/ParagraphTemplate").gameObject
    self.enterButton = anchor:Find("EnterButton").gameObject
    self.rankButton = anchor:Find("RankButton").gameObject
end

function LuaYinHunPoZhiEnterWnd:InitEventListener()
	UIEventListener.Get(self.enterButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnEnterButtonClick()
	end)

    UIEventListener.Get(self.rankButton).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRankButtonClick()
	end)
end

function LuaYinHunPoZhiEnterWnd:InitActive()
    self.awardTemplate:SetActive(false)
    self.paragraphTemplate:SetActive(false)
end


function LuaYinHunPoZhiEnterWnd:Init()
    self.activityTime.text = QingMing2022_PVESetting.GetData().ActivityTime
    self:ParseRuleText()
    self:InitAward()
end

-- 解析规则信息
function LuaYinHunPoZhiEnterWnd:ParseRuleText()
    Extensions.RemoveAllChildren(self.ruleTable.transform)
    local msg =	g_MessageMgr:FormatMessage("YINHUNPOZHI_RULE")
    if System.String.IsNullOrEmpty(msg) then return end

    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)
    if not info then return end

    do
		local i = 0
		while i < info.paragraphs.Count do							-- 根据信息创建并显示段落
			local paragraph = CUICommonDef.AddChild(self.ruleTable.gameObject, self.paragraphTemplate) -- 添加段落Go
			paragraph:SetActive(true)
			local tip = CommonDefs.GetComponent_GameObject_Type(paragraph, typeof(CTipParagraphItem))
			tip:Init(info.paragraphs[i], 4294967295)				-- 初始化段落，color = 4294967295（0xFFFFFFFF）
			i = i + 1
		end
	end
	self.ruleTable:Reposition()
end

-- 初始化奖励
function LuaYinHunPoZhiEnterWnd:InitAward()
    local maxScore = CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eQingMing2022PVEAwardScore)
    self.maxScore.text = maxScore

    Extensions.RemoveAllChildren(self.awardTable.transform)

    local setting = QingMing2022_PVESetting.GetData()
    local itemIdTbl = LuaQingMing2022Mgr:Array2Tbl(setting.AwardItemId)
    local scoreTbl = LuaQingMing2022Mgr:Array2Tbl(setting.AwardScore)
    if not itemIdTbl or not scoreTbl then return end

    for i = 1, #itemIdTbl do
        local child = CUICommonDef.AddChild(self.awardTable.gameObject, self.awardTemplate)
        child:SetActive(true)
        local awardItem = child.transform:Find("AwardItemCell"):GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
        awardItem:Init(itemIdTbl[i])
        awardItem:SetAwardComplete(true)

        local score = child.transform:Find("Score"):GetComponent(typeof(UILabel))
        score.text = SafeStringFormat3(LocalString.GetString("%d分"), scoreTbl[i])
    end
    self.awardTable:Reposition()
end

--@region UIEvent

function LuaYinHunPoZhiEnterWnd:OnEnterButtonClick()
    Gac2Gas.EnterQingMing2022PVEPlay()
end

function LuaYinHunPoZhiEnterWnd:OnRankButtonClick()
    LuaPlayerRankMgr:ShowPlayerRankWnd(QingMing2022_PVESetting.GetData().RankId, nil, nil, nil, LocalString.GetString("今日最高分数"),
        g_MessageMgr:FormatMessage("YINHUNPOZHI_RANK_TIP"))
end

--@endregion UIEvent
