local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local QnRadioBox = import "L10.UI.QnRadioBox"
local UILabel = import "UILabel"
local CUITexture = import "L10.UI.CUITexture"
local GameObject = import "UnityEngine.GameObject"
local UIGrid = import "UIGrid"
local CButton = import "L10.UI.CButton"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local TouchPhase = import "UnityEngine.TouchPhase"
local Ease = import "DG.Tweening.Ease"
local TweenScale = import "TweenScale"
local Object = import "System.Object"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local DefaultItemActionDataSource = import "L10.UI.DefaultItemActionDataSource"
local AlignType2 = import "L10.UI.CTooltip+AlignType"
local EnumTempPlayDataKey = import "L10.Game.EnumTempPlayDataKey"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local UIPanel = import "UIPanel"
local LayerDefine = import "L10.Engine.LayerDefine"

LuaNanDuFanHuaCookBookWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaNanDuFanHuaCookBookWnd, "QnRadioBox", "QnRadioBox", QnRadioBox)
RegistChildComponent(LuaNanDuFanHuaCookBookWnd, "NameLabel", "NameLabel", UILabel)
RegistChildComponent(LuaNanDuFanHuaCookBookWnd, "FoodItem", "FoodItem", GameObject)
RegistChildComponent(LuaNanDuFanHuaCookBookWnd, "FoodRoot", "FoodRoot", UIGrid)
RegistChildComponent(LuaNanDuFanHuaCookBookWnd, "DragItem", "DragItem", CUITexture)
RegistChildComponent(LuaNanDuFanHuaCookBookWnd, "CookButton", "CookButton", CButton)
RegistChildComponent(LuaNanDuFanHuaCookBookWnd, "TipButton", "TipButton", GameObject)
RegistChildComponent(LuaNanDuFanHuaCookBookWnd, "CookMethodRoot", "CookMethodRoot", UIGrid)
RegistChildComponent(LuaNanDuFanHuaCookBookWnd, "CookButtonFx", "CookButtonFx", CUIFx)
RegistChildComponent(LuaNanDuFanHuaCookBookWnd, "GuangFx", "GuangFx", CUIFx)
RegistChildComponent(LuaNanDuFanHuaCookBookWnd, "LockedRoot", "LockedRoot", GameObject)
RegistChildComponent(LuaNanDuFanHuaCookBookWnd, "UnlockedRoot", "UnlockedRoot", GameObject)
RegistChildComponent(LuaNanDuFanHuaCookBookWnd, "Description", "Description", UILabel)
RegistChildComponent(LuaNanDuFanHuaCookBookWnd, "IngredientsList", "IngredientsList", GameObject)
RegistChildComponent(LuaNanDuFanHuaCookBookWnd, "IngredientTemplate", "IngredientTemplate", GameObject)
RegistChildComponent(LuaNanDuFanHuaCookBookWnd, "SuccFx", "SuccFx", GameObject)
RegistChildComponent(LuaNanDuFanHuaCookBookWnd, "FailFx", "FailFx", GameObject)
RegistChildComponent(LuaNanDuFanHuaCookBookWnd, "FinishTag", "FinishTag", QnSelectableButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaNanDuFanHuaCookBookWnd,"m_CookBookData")
RegistClassMember(LuaNanDuFanHuaCookBookWnd,"m_FoodRootRadioBox")
RegistClassMember(LuaNanDuFanHuaCookBookWnd,"m_CookMethodRootRadioBox")
RegistClassMember(LuaNanDuFanHuaCookBookWnd,"m_FoodRootRadioBoxIndex")
RegistClassMember(LuaNanDuFanHuaCookBookWnd,"m_CookMethodRootRadioBoxIndex")
RegistClassMember(LuaNanDuFanHuaCookBookWnd,"m_SelectItemList")
RegistClassMember(LuaNanDuFanHuaCookBookWnd,"m_SelectMethodList")
RegistClassMember(LuaNanDuFanHuaCookBookWnd,"m_LastSelectTabIndex")
RegistClassMember(LuaNanDuFanHuaCookBookWnd,"m_IsDragging")
RegistClassMember(LuaNanDuFanHuaCookBookWnd,"m_LastDragPos")
RegistClassMember(LuaNanDuFanHuaCookBookWnd,"m_FingerIndex")
RegistClassMember(LuaNanDuFanHuaCookBookWnd,"m_DragItemIndex")
RegistClassMember(LuaNanDuFanHuaCookBookWnd,"m_DragItemIsMethod")
RegistClassMember(LuaNanDuFanHuaCookBookWnd,"m_LeftIngredientIndex")

LuaNanDuFanHuaCookBookWnd.DefaultOpenTab = nil
function LuaNanDuFanHuaCookBookWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    --create a empty gameObject
    local tempFxRoot = GameObject()
    --tempFxRoot.transform.parent = self.gameObject
    tempFxRoot.transform:SetParent(self.transform)
    tempFxRoot.layer = LayerDefine.UI
    tempFxRoot.name = "FxRoot"
    local fxPanelScript = tempFxRoot:AddComponent(typeof(UIPanel))
    fxPanelScript.depth = self.FoodRoot.transform:GetComponent(typeof(UIPanel)).depth + 1
    self.SuccFx.transform:SetParent(tempFxRoot.transform)
    self.FailFx.transform:SetParent(tempFxRoot.transform)
end

function LuaNanDuFanHuaCookBookWnd:Init()
    self.m_SelectItemList = {}
    self.m_SelectMethodList = {}
    self.FoodItem.gameObject:SetActive(false)
    self.FoodItem.transform:Find("Highlighted").gameObject:SetActive(false)
    self.DragItem.gameObject:SetActive(false)
    self.GuangFx:LoadFx("fx/ui/prefab/UI_caipu_guang.prefab")
    self.IngredientTemplate:SetActive(false)
    self.SuccFx.gameObject:SetActive(false)
    self.FailFx.gameObject:SetActive(false)
    self:InitTab()
end

function LuaNanDuFanHuaCookBookWnd:InitTab()
    if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, EnumToInt(EnumTempPlayDataKey.eLiYuZhangDaiFoodData)) then
        local playData = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, EnumToInt(EnumTempPlayDataKey.eLiYuZhangDaiFoodData))
        if playData.ExpiredTime > CServerTimeMgr.Inst.timeStamp and playData.Data and playData.Data.Data then
            local data = g_MessagePack.unpack(playData.Data.Data)
            self.foodData = data
        end
    end
    if self.foodData then
        self.prefixPath = ""

        self.m_CookBookData = {}

        -- 二进制食材编号分组
        self.m_IngredientInfos = {}
        self.m_IngredientInfos.maxId = 0
        self.m_IngredientInfos.maxGroup = 1
        self.m_GroupSizeLimit = 24
        LiYuZhangDaiFood_Ingredients.Foreach(function(ingredientId, ingredientData)
            local ingredientInfo = {}
            local groupOffset = (ingredientId - 1) % self.m_GroupSizeLimit
            ingredientInfo.group = math.floor((ingredientId - 1) / self.m_GroupSizeLimit + 1)
            ingredientInfo.bit = bit.lshift(1, groupOffset)
            self.m_IngredientInfos.maxId = math.max(self.m_IngredientInfos.maxId, ingredientId)
            self.m_IngredientInfos.maxGroup = math.max(self.m_IngredientInfos.maxGroup, ingredientInfo.group)
            self.m_IngredientInfos[ingredientId] = ingredientInfo
        end)

        LiYuZhangDaiFood_Cookbooks.ForeachKey(function(key)
            local data = LiYuZhangDaiFood_Cookbooks.GetData(key)
            self.m_CookBookData[key] = data
            self:ParseCookCfgData(key, data)
            local btn = self.QnRadioBox.m_RadioButtons[key - 1]
            btn.Text = data.TabName
            btn.transform:Find("Selected/SelectedLabel"):GetComponent(typeof(UILabel)).text = data.TabName
            local isUnlock = self.foodData[1][key] == 1
            btn.transform:Find("LockSprite").gameObject:SetActive(not isUnlock)
            btn.m_Label.gameObject:SetActive(isUnlock)
        end)

        self.QnRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn, index)
            self:OnSelectTab(btn,index)
        end)
        self.QnRadioBox:ChangeTo(self:GetDefaultSelectTabIndex(), true)

        UIEventListener.Get(self.CookButton.gameObject).onClick = DelegateFactory.VoidDelegate(function ()
            self:OnCookButtonClick()
        end)

        UIEventListener.Get(self.TipButton.gameObject).onClick = DelegateFactory.VoidDelegate(function ()
            self:OnTipButtonClick()
        end)
        
        self:RefreshTabCheckIcon()
    else
        print("tempPlayData中没有 liyuzhangdai数据???")
        CUIManager.CloseUI(CLuaUIResources.NanDuFanHuaCookBookWnd)
    end
end

function LuaNanDuFanHuaCookBookWnd:RefreshTabCheckIcon()
    if self.foodData and (not CommonDefs.IsNull(self.QnRadioBox)) then
        for i = 0, self.QnRadioBox.m_RadioButtons.Length - 1 do
            local btn = self.QnRadioBox.m_RadioButtons[i]
            local key = i + 1
            local isCookSucc = (self.foodData[2][key] ~= 0)
            btn.transform:Find("CheckIcon").gameObject:SetActive(isCookSucc)
        end
    end
end

function LuaNanDuFanHuaCookBookWnd:ParseCookCfgData(index, cfgData)
    local parsedIngredients = {}
    local parsedSkills = {}
    local parsedSequence = {}

    local ingredientSplitList = g_LuaUtil:StrSplit(cfgData.NeedIngredients, ";")
    for i = 1, #ingredientSplitList-1 do
        table.insert(parsedIngredients, tonumber(ingredientSplitList[i]))
    end
    local skillSplitList = g_LuaUtil:StrSplit(cfgData.NeedSkills, ";")
    for i = 1, #skillSplitList-1 do
        table.insert(parsedSkills, tonumber(skillSplitList[i]))
    end

    local sequenceNum = 0
    local sequenceSplitList = g_LuaUtil:StrSplit(cfgData.Sequences, ";")
    for i = 1, #sequenceSplitList-1 do
        local subSequence = sequenceSplitList[i]
        local subSequenceSplitList = g_LuaUtil:StrSplit(subSequence, ",")
        local tbl = {}
        for j = 1, #subSequenceSplitList do
            table.insert(tbl, tonumber(subSequenceSplitList[j]))
        end
        parsedSequence[i] = tbl
    end
    self.m_CookBookData[index].parsedIngredients = parsedIngredients
    self.m_CookBookData[index].parsedSkills = parsedSkills
    self.m_CookBookData[index].parsedSequence = parsedSequence
end

function LuaNanDuFanHuaCookBookWnd:GetDefaultSelectTabIndex()
    --return 0
    if LuaNanDuFanHuaCookBookWnd.DefaultOpenTab then
        return LuaNanDuFanHuaCookBookWnd.DefaultOpenTab
    end
    return 0
end

function LuaNanDuFanHuaCookBookWnd:OnSelectTab(btn, index)
    local key = index + 1
    local data = self.m_CookBookData[key]
    local isUnlock = self.foodData[1][key] == 1

    if isUnlock == false then
        g_MessageMgr:ShowMessage("NanDuFanHua_CookBookWnd_Select_LockedTab")
        if self.m_LastSelectTabIndex then
            self.QnRadioBox:ChangeTo(self.m_LastSelectTabIndex, false)
        end
        return
    end
    for i = 0,self.QnRadioBox.m_RadioButtons.Length - 1 do
        local t = self.foodData[1][i+1] == 1
        self.QnRadioBox.m_RadioButtons[i].m_Label.gameObject:SetActive(t and i ~= index)
        self.QnRadioBox.m_RadioButtons[i].transform:Find("NormalSprite").gameObject:SetActive(i ~= index)
    end
    self.m_LastSelectTabIndex = index

    self.NameLabel.text = data.Name
    self.Description.text = self:ForceFormateString(data.Description)

    self.m_SelectItemList = {}
    self.m_SelectMethodList = {}
    local isCookSucc = (self.foodData[2][key] ~= 0)
    if isCookSucc then
        --曾经制作过
        local cookMealData = self.foodData[2][key]
        self.m_SelectItemList = data.parsedSequence[cookMealData]
        self.m_SelectMethodList = data.parsedSkills
    end
    self.CookButton.Text = LocalString.GetString("烹饪")
    self:InitRightView(data, index)
    self:InitIngredientView(data, index)
    self:ShowCookButtonFx()
end

function LuaNanDuFanHuaCookBookWnd:ForceFormateString(str)
    return "[4E4135]" .. g_MessageMgr:Format(str) .. "[-]"
end

function LuaNanDuFanHuaCookBookWnd:InitIngredientView(data, index)
    Extensions.RemoveAllChildren(self.IngredientsList.transform)
    for i = 1, #data.parsedIngredients do
        local go = NGUITools.AddChild(self.IngredientsList, self.IngredientTemplate)
        go.gameObject:SetActive(true)
        local ingredientId = data.parsedIngredients[i]
        local ingredientData = self.foodData[3]
        local ingredientGroup = self.m_IngredientInfos[ingredientId].group
        local ingredientBit = self.m_IngredientInfos[ingredientId].bit
        self:InitIngredientItem(go, ingredientId, i, bit.band(ingredientData[ingredientGroup], ingredientBit) > 0, index+1)
    end
    self.IngredientsList.transform:GetComponent(typeof(UIGrid)):Reposition()
end

function LuaNanDuFanHuaCookBookWnd:InitIngredientItem(obj, ingredientId, indexInScrList, isGot, cookbookId)
    local cfgData = LiYuZhangDaiFood_Ingredients.GetData(ingredientId)
    obj.transform:Find("Label"):GetComponent(typeof(UILabel)).text = cfgData.Name
    obj.transform:Find("AddSprite").gameObject:SetActive(not isGot)
    obj.transform:Find("IngredientRes").gameObject:SetActive(isGot)
    
    local isCookSucc = (self.foodData[2][cookbookId] ~= 0)
    
    if isGot then
        --已获得食材
        obj.transform:Find("IngredientRes"):GetComponent(typeof(CUITexture)):LoadMaterial(self.prefixPath .. cfgData.Material)
        UIEventListener.Get(obj).onDragStart = DelegateFactory.VoidDelegate(function (go)
            if not isCookSucc then
                self:OnDragIconStart(obj, ingredientId, indexInScrList, false)
            end
        end)
    else
        --未获得食材
        UIEventListener.Get(obj).onClick = DelegateFactory.VoidDelegate(function (go)
            local msg = g_MessageMgr:FormatMessage("LIYUZHANGDAI_FIND_FOOD")
            MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(function()
                Gac2Gas.LiYuZhangDaiFoodEnterCopy(ingredientId)
                CUIManager.CloseUI(CLuaUIResources.NanDuFanHuaCookBookWnd)
            end),nil,nil,nil,false)
        end)
    end
end

function LuaNanDuFanHuaCookBookWnd:InitRightView(data, index)
    if not self.m_FoodRootRadioBox then
        self.m_FoodRootRadioBox = self.FoodRoot.gameObject:AddComponent(typeof(QnRadioBox))
    end
    Extensions.RemoveAllChildren(self.FoodRoot.transform)
    self.m_FoodRootRadioBox.m_RadioButtons = CreateFromClass(MakeArrayClass(QnSelectableButton), #data.parsedSequence[1])

    local isCookSucc = (self.foodData[2][index+1] ~= 0)
    self.transform:Find("BottomView").gameObject:SetActive(not isCookSucc)
    self.FinishTag.gameObject:SetActive(isCookSucc)

    for i = 1, #data.parsedSequence[1] do
        local go = NGUITools.AddChild(self.FoodRoot.gameObject, self.FoodItem.gameObject)
        go.transform:Find("Empty/Sprite").gameObject:SetActive(false)
        go.gameObject:SetActive(true)
        local curSelectItem = self.m_SelectItemList[i]
        self:InitFoodItem(go, true, isCookSucc, curSelectItem, i == 1)
        self.m_FoodRootRadioBox.m_RadioButtons[i - 1] = go:GetComponent(typeof(QnSelectableButton))
        UIEventListener.Get(go).onDragStart = DelegateFactory.VoidDelegate(function (go)
            if not isCookSucc then
                self:OnDragIconStart(go, nil, i, false)
            end
        end)

        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function (go)
            if not isCookSucc then
                g_MessageMgr:ShowMessage("NanDuFanHua_CookBookWnd_DragReminder")
            end
        end)
    end

    self.FoodRoot:Reposition()

    if not self.m_CookMethodRootRadioBox then
        self.m_CookMethodRootRadioBox = self.CookMethodRoot.gameObject:AddComponent(typeof(QnRadioBox))
    end
    Extensions.RemoveAllChildren(self.CookMethodRoot.transform)
    self.m_CookMethodRootRadioBox.m_RadioButtons = CreateFromClass(MakeArrayClass(QnSelectableButton), #data.parsedSkills)
    for i = 1, #data.parsedSkills do
        local go = NGUITools.AddChild(self.CookMethodRoot.gameObject, self.FoodItem.gameObject)
        go.gameObject:SetActive(true)
        local curSelectItem = self.m_SelectMethodList[i]
        self:InitFoodItem(go, false, isCookSucc, curSelectItem, i == 1)
        self.m_CookMethodRootRadioBox.m_RadioButtons[i - 1] = go:GetComponent(typeof(QnSelectableButton))
        self.m_CookMethodRootRadioBox.m_RadioButtons[i - 1].OnClick = MakeDelegateFromCSFunction(self.m_CookMethodRootRadioBox.On_Click, MakeGenericClass(Action1, QnButton), self.m_CookMethodRootRadioBox)
        UIEventListener.Get(go).onDragStart = DelegateFactory.VoidDelegate(function (go)
            if not isCookSucc then
                self:OnDragIconStart(go, nil, i,true)
            end
        end)
    end
    self.m_CookMethodRootRadioBox.OnSelect = DelegateFactory.Action_QnButton_int(function(btn,index)
        if not isCookSucc then
            self:OnCookMethodRadioBoxSelected(btn, index)
        end
    end)
    self.CookMethodRoot:Reposition()
end

function LuaNanDuFanHuaCookBookWnd:InitFoodItem(go, isFoodItem, isCookSucc, curSelectItem, isFirst)
    local item = go.transform:Find("Item"):GetComponent(typeof(CUITexture))
    local emptyGo = go.transform:Find("Empty").gameObject
    emptyGo.gameObject:SetActive(not isCookSucc and curSelectItem == nil)
    item.gameObject:SetActive(isCookSucc or curSelectItem)
    if curSelectItem then
        local cfgData = nil
        if isFoodItem then
            cfgData = LiYuZhangDaiFood_Ingredients.GetData(curSelectItem)
        else
            cfgData = LiYuZhangDaiFood_CookSkills.GetData(curSelectItem)
        end
        item:LoadMaterial(self.prefixPath .. cfgData.Material)
    end

    if isFirst == nil then
        --doNothing
    else
        go.transform:Find("Arrow").gameObject:SetActive(not isFirst)
    end
end

function LuaNanDuFanHuaCookBookWnd:OnCookMethodRadioBoxSelected(btn,index)
    self.m_CookMethodRootRadioBoxIndex = index
    local curSelectItem = self.m_SelectMethodList[index + 1]
    self:ShowItemSelecrWnd(2, curSelectItem)
end

function LuaNanDuFanHuaCookBookWnd:ShowItemSelecrWnd(type, curSelectItem)
    CUIManager.ShowUI(CLuaUIResources.NanDuFanHuaCookBookItemSelectWnd)
end

function LuaNanDuFanHuaCookBookWnd:ShowCookButtonFx()
    local key = self.m_LastSelectTabIndex + 1
    local data = self.m_CookBookData[key]
    local isCookSucc = (self.foodData[2][key] ~= 0)
    local checkSkills = true
    local checkIngredients = true
    if not isCookSucc then
        checkIngredients = (#self.m_SelectItemList == #data.parsedSequence[1])
        checkSkills = (#self.m_SelectMethodList == #data.parsedSkills)
    else
        checkSkills = false
        checkIngredients = false
    end

    if checkSkills and checkIngredients then
        self.CookButtonFx:DestroyFx()
        self.CookButtonFx.OnLoadFxFinish = DelegateFactory.Action(function()
            self.CookButtonFx.fx.transform.localPosition = Vector3(-5,-2,0)
            self.CookButtonFx.fx.transform.localScale = Vector3(0.7,1,1)
        end)
        self.CookButtonFx:LoadFx("fx/ui/prefab/UI_zbsjf_goumaianniu.prefab")
    else
        self.CookButtonFx:DestroyFx()
    end
end

function LuaNanDuFanHuaCookBookWnd:Update()
    if nil == self.m_LastDragPos then
        self.m_LastDragPos = Vector3.zero
    end
    if not self.m_IsDragging then
        return
    end

    if CommonDefs.IsInMobileDevice() then
        for i = 0,Input.touchCount - 1 do
            local touch = Input.GetTouch(i)
            if touch.fingerId == self.m_FingerIndex then
                if touch.phase ~= TouchPhase.Ended and touch.phase ~= TouchPhase.Canceled then
                    if touch.phase ~= TouchPhase.Stationary then
                        local pos = touch.position
                        self.m_LastDragPos = Vector3(pos.x, pos.y, 0)
                        self.DragItem.transform.position = CUIManager.UIMainCamera:ScreenToWorldPoint(self.m_LastDragPos)
                    end
                    return
                else
                    break
                end
            end
        end
    else
        if Input.GetMouseButton(0) then
            self.DragItem.transform.position = CUIManager.UIMainCamera:ScreenToWorldPoint(Input.mousePosition)
            self.m_LastDragPos = Input.mousePosition
            if not Input.GetMouseButtonUp(0) then
                return
            end
        end
    end
    self.DragItem.gameObject:SetActive(false)
    local hoveredObject = CUICommonDef.SelectedUIWithRacast
    if hoveredObject then
        self:OnDragEnd(hoveredObject)
    end
    self.m_IsDragging = false
    self.m_FingerIndex = -1
    self.m_DragItemIndex = -1
    self.m_LeftIngredientIndex = -1
end

function LuaNanDuFanHuaCookBookWnd:OnDragIconStart(go, itemId, indexInScrList, isMethod)
    local curSelectItem = nil
    if itemId then
        -- 是从左边食材库里拖出来的
        curSelectItem = LiYuZhangDaiFood_Ingredients.GetData(itemId)
    else    
        -- 是右边的食材选择/技法顺序中拖出来的
        if isMethod then
            local methodIndex = self.m_SelectMethodList[indexInScrList]
            curSelectItem = LiYuZhangDaiFood_CookSkills.GetData(methodIndex)
        else
            local gredientIndex = self.m_SelectItemList[indexInScrList]
            curSelectItem = LiYuZhangDaiFood_Ingredients.GetData(gredientIndex)
        end
    end

    if curSelectItem then
        self.DragItem.gameObject:SetActive(true)
        self.DragItem:LoadMaterial(self.prefixPath .. curSelectItem.Material)
        self.m_IsDragging = true
        self.m_LastDragPos = Input.mousePosition
        if CommonDefs.IsInMobileDevice() then
            self.m_FingerIndex = Input.GetTouch(0).fingerId
            self.m_LastDragPos = Input.GetTouch(0).position
        end
        self.m_DragItemIndex = indexInScrList
        self.m_DragItemIsMethod = isMethod
        self.m_LeftIngredientIndex = itemId
    end
end

function LuaNanDuFanHuaCookBookWnd:OnDragEnd(hoveredObject)
    local isMethod = self.m_DragItemIsMethod
    local newIndex = - 1
    if hoveredObject.transform.parent == self.FoodRoot.transform and self.m_FoodRootRadioBox then
        for i = 1, self.m_FoodRootRadioBox.m_RadioButtons.Length do
            local btn = self.m_FoodRootRadioBox.m_RadioButtons[i - 1]
            if btn.gameObject == hoveredObject.gameObject then
                --DragItem在 右上方foodList下松开
                if isMethod then
                    --类型不对
                    g_MessageMgr:ShowMessage("CookBookWnd_DragEnd_TypeError")
                else
                    if self.m_LeftIngredientIndex and  self.m_LeftIngredientIndex ~= -1 then
                        --是从左边食材库里拖出来的
                        self.m_SelectItemList[i] = self.m_LeftIngredientIndex
                        newIndex = i
                        self:InitFoodItem(btn.gameObject, true, false, self.m_LeftIngredientIndex, nil)
                    else
                        --是右边的食材选择
                        local temp = self.m_SelectItemList[i]
                        self.m_SelectItemList[i] = self.m_SelectItemList[self.m_DragItemIndex]
                        self.m_SelectItemList[self.m_DragItemIndex] = temp
                        newIndex = i
                        self:InitFoodItem(self.m_FoodRootRadioBox.m_RadioButtons[i-1].gameObject, true, false, self.m_SelectItemList[i], nil)
                        self:InitFoodItem(self.m_FoodRootRadioBox.m_RadioButtons[self.m_DragItemIndex-1].gameObject, true, false, self.m_SelectItemList[self.m_DragItemIndex], nil)
                    end
                end
                break
            end
        end
    elseif hoveredObject.transform.parent == self.CookMethodRoot.transform and self.m_CookMethodRootRadioBox then
        for i = 1,self.m_CookMethodRootRadioBox.m_RadioButtons.Length do
            local btn = self.m_CookMethodRootRadioBox.m_RadioButtons[i - 1]
            if btn.gameObject == hoveredObject.gameObject then
                --DragItem在 右上方methodList下松开
                if not isMethod then
                    --类型不对
                    g_MessageMgr:ShowMessage("CookBookWnd_DragEnd_TypeError")
                    break
                else
                    local temp = self.m_SelectMethodList[i]
                    self.m_SelectMethodList[i] = self.m_SelectMethodList[self.m_DragItemIndex]
                    self.m_SelectMethodList[self.m_DragItemIndex] = temp
                    newIndex = i
                    self:InitFoodItem(self.m_CookMethodRootRadioBox.m_RadioButtons[i-1].gameObject, false, false, self.m_SelectMethodList[i], nil)
                    self:InitFoodItem(self.m_CookMethodRootRadioBox.m_RadioButtons[self.m_DragItemIndex-1].gameObject, false, false, self.m_SelectMethodList[self.m_DragItemIndex], nil)
                end
                break
            end
        end
    end
end

function LuaNanDuFanHuaCookBookWnd:OnEnable()
    g_ScriptEvent:AddListener("OnNanDuFanHuaCookSelectCookSkillsId", self, "OnNanDuFanHuaCookSelectCookSkillsId")
    g_ScriptEvent:AddListener("SyncNanDuFanHuaCookResult", self, "OnSyncNanDuFanHuaCookResult")
end

function LuaNanDuFanHuaCookBookWnd:OnDisable()
    UnRegisterTick(self.m_TimeTick)
    g_ScriptEvent:RemoveListener("OnNanDuFanHuaCookSelectCookSkillsId", self, "OnNanDuFanHuaCookSelectCookSkillsId")
    g_ScriptEvent:RemoveListener("SyncNanDuFanHuaCookResult", self, "OnSyncNanDuFanHuaCookResult")
end

function LuaNanDuFanHuaCookBookWnd:OnNanDuFanHuaCookSelectCookSkillsId(cookId)
    --m_CookMethodRootRadioBoxIndex, cookId
    local btn = self.m_CookMethodRootRadioBox.m_RadioButtons[self.m_CookMethodRootRadioBoxIndex]
    self.m_SelectMethodList[self.m_CookMethodRootRadioBoxIndex+1] = cookId
    self:InitFoodItem(btn.gameObject, false, false, cookId, nil)
end

function LuaNanDuFanHuaCookBookWnd:OnSyncNanDuFanHuaCookResult(cookbookId, isSucc)
    if isSucc then
        if CClientMainPlayer.Inst and CommonDefs.DictContains_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, EnumToInt(EnumTempPlayDataKey.eLiYuZhangDaiFoodData)) then
            local playData = CommonDefs.DictGetValue_LuaCall(CClientMainPlayer.Inst.PlayProp.TempPlayData, EnumToInt(EnumTempPlayDataKey.eLiYuZhangDaiFoodData))
            if playData.ExpiredTime > CServerTimeMgr.Inst.timeStamp and playData.Data and playData.Data.Data then
                local data = g_MessagePack.unpack(playData.Data.Data)
                self.foodData = data
            end
        end
        
        self:RefreshTabCheckIcon()
        local data = self.m_CookBookData[cookbookId]
        self:InitRightView(data, cookbookId)
        self:InitIngredientView(data, cookbookId)
        if self.m_TimeTick then
            UnRegisterTick(self.m_TimeTick)
        end
        self.SuccFx.gameObject:SetActive(true)
        self.FailFx.gameObject:SetActive(false)
        self.m_TimeTick = RegisterTickOnce(function ()
            self.SuccFx.gameObject:SetActive(false)
            if not CommonDefs.IsNull(self.gameObject) then
                CUIManager.CloseUI(CLuaUIResources.NanDuFanHuaCookBookWnd)
            end
        end, 2 * 1000)
    else
        if self.m_TimeTick then
            UnRegisterTick(self.m_TimeTick)
        end
        self.SuccFx.gameObject:SetActive(false)
        self.FailFx.gameObject:SetActive(true)
        self.m_TimeTick = RegisterTickOnce(function ()
            self.FailFx.gameObject:SetActive(false)
        end, 2 * 1000)
    end
end


--@region UIEvent

function LuaNanDuFanHuaCookBookWnd:OnCookButtonClick()
    local key = self.m_LastSelectTabIndex + 1
    local data = self.m_CookBookData[key]

    --优先判断该菜谱对应的食材是否全获得, 如果没有全获得,就弹一个msg, 然后return了
    for i = 1, #data.parsedIngredients do
        local ingredientId = data.parsedIngredients[i]
        local ingredientData = self.foodData[3]
        local ingredientGroup = self.m_IngredientInfos[ingredientId].group
        local ingredientBit = self.m_IngredientInfos[ingredientId].bit
        if bit.band(ingredientData[ingredientGroup], ingredientBit) == 0 then
            g_MessageMgr:ShowMessage("COOKBOOK_FOOD_NOTGET")
            return
        end
    end
    
    local checkSkills = true
    local checkIngredients = true
    checkIngredients = (#self.m_SelectItemList == #data.parsedSequence[1])
    checkSkills = (#self.m_SelectMethodList == #data.parsedSkills)
    
    if not (checkSkills and checkIngredients) then
        g_MessageMgr:ShowMessage("NanDuFanHua_CookBookWnd_UnFinished")
        return
    end
    Gac2Gas.LiYuZhangDaiFoodStartCooking(key, g_MessagePack.pack(self.m_SelectItemList), g_MessagePack.pack(self.m_SelectMethodList))
end

function LuaNanDuFanHuaCookBookWnd:OnTipButtonClick()
    g_MessageMgr:ShowMessage("NanDuFanHua_CookBookWnd_ReadMe")
end

--@endregion UIEvent
