-- Auto Generated!!
local Constants = import "L10.Game.Constants"
local CXialvPKResultWenshi = import "L10.UI.CXialvPKResultWenshi"
local LocalString = import "LocalString"
CXialvPKResultWenshi.m_Init_CS2LuaHook = function (this, round, leftData, rightData, winTeamId, leftTeamId, rightTeamId, isRunning, notStarted) 
    --还没队伍获胜
    --还没队伍获胜
    if winTeamId < 0 then
        this.resultSprite1.enabled = false
        this.resultSprite2.enabled = false
    else
        this.resultSprite1.enabled = true
        this.resultSprite2.enabled = true
        if winTeamId == leftTeamId then
            this.resultSprite1.spriteName = Constants.WinSprite
            this.resultSprite2.spriteName = Constants.LoseSprite
        elseif winTeamId == rightTeamId then
            this.resultSprite1.spriteName = Constants.LoseSprite
            this.resultSprite2.spriteName = Constants.WinSprite
        end
    end

    if isRunning then
        this.roundLabel.text = System.String.Format(LocalString.GetString("[{0}]进行中[-]"), CXialvPKResultWenshi.yellowColor)
    else
        this.roundLabel.text = System.String.Format(LocalString.GetString("[{0}]第{1}场[-]"), CXialvPKResultWenshi.greyColor, round)
    end

    if leftData ~= nil then
        if not notStarted then
            this.totalLabel1.text = tostring(leftData.wenshiScore)
        else
            this.totalLabel1.text = "--"
        end
    else
        this.totalLabel1.text = "--"
    end

    if rightData ~= nil then
        if not notStarted then
            this.totalLabel2.text = tostring(rightData.wenshiScore)
        else
            this.totalLabel2.text = "--"
        end
    else
        this.totalLabel2.text = "--"
    end
end
