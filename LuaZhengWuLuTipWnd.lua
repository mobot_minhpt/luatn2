local UILabel = import "UILabel"

LuaZhengWuLuTipWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZhengWuLuTipWnd, "Title", "Title", UILabel)
RegistChildComponent(LuaZhengWuLuTipWnd, "Pic", "Pic", CUITexture)
RegistChildComponent(LuaZhengWuLuTipWnd, "Description", "Description", UILabel)

--@endregion RegistChildComponent end

function LuaZhengWuLuTipWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaZhengWuLuTipWnd:Init()
    self.Pic:LoadMaterial(LuaZhengWuLuMgr.selectedInfo.Icon)
    self.Title.text = LuaZhengWuLuMgr.selectedInfo.Name
    self.Description.text = LuaZhengWuLuMgr.selectedInfo.Description
end

--@region UIEvent

--@endregion UIEvent

