-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CHousePopupMgr = import "L10.UI.CHousePopupMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CWeekendFightMgr = import "L10.Game.CWeekendFightMgr"
local Debug = import "UnityEngine.Debug"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local ForceInfo = import "L10.Game.CWeekendFightMgr+ForceInfo"
local HousePopupMenuItemData = import "L10.UI.HousePopupMenuItemData"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local MsgPackImpl = import "MsgPackImpl"
local Object = import "System.Object"
local PlayerBasicInfo = import "L10.Game.CWeekendFightMgr+PlayerBasicInfo"
local PlayItemInfo = import "L10.Game.CWeekendFightMgr+PlayItemInfo"
local String = import "System.String"
local UILabel = import "UILabel"
local UInt32 = import "System.UInt32"
local UInt64 = import "System.UInt64"
local UISprite = import "UISprite"
local XianZongShan_Crystal = import "L10.Game.XianZongShan_Crystal"
local XianZongShan_Group = import "L10.Game.XianZongShan_Group"
local XianZongShan_Setting = import "L10.Game.XianZongShan_Setting"
CWeekendFightMgr.m_GetSelfForceInfo_CS2LuaHook = function (this) 
    if CClientMainPlayer.Inst == nil then
        return nil
    end
    if not CommonDefs.DictContains(this.PlayerInfoDic, typeof(UInt64), CClientMainPlayer.Inst.Id) then
        return nil
    end
    local pInfo = CommonDefs.DictGetValue(this.PlayerInfoDic, typeof(UInt64), CClientMainPlayer.Inst.Id)
    do
        local i = 0
        while i < this.ForceList.Count do
            local fInfo = this.ForceList[i]
            if fInfo.force == pInfo.force then
                return fInfo
            end
            i = i + 1
        end
    end
    return nil
end
CWeekendFightMgr.m_SetRankLabel_CS2LuaHook = function (this, labelNode, rank) 
    local label = CommonDefs.GetComponent_GameObject_Type(labelNode, typeof(UILabel))
    if label == nil then
        return
    end
    local speTr = labelNode.transform:Find("spe")
    speTr.gameObject:SetActive(false)
    if speTr ~= nil and rank <= 3 and rank > 0 then
        label.text = ""
        speTr.gameObject:SetActive(true)
        CommonDefs.GetComponent_Component_Type(speTr, typeof(UISprite)).spriteName = this.RankSpriteName[rank - 1]
    elseif rank > 3 then
        label.text = tostring(rank)
    else
        label.text = LocalString.GetString("未入榜")
    end
end
CWeekendFightMgr.m_SetForceIcon_CS2LuaHook = function (this, node, force) 
    local groupInfo = XianZongShan_Group.GetData(force)
    if groupInfo ~= nil then
        local ct = CommonDefs.GetComponent_GameObject_Type(node, typeof(UISprite))
        if ct ~= nil then
            ct.spriteName = groupInfo.Icon
            --这个方法被多处复用，暂时不调用MakePixelPerfect，最好的做法是头顶的显示和其他地方分开，头顶用于复用
        end
    end
end
CWeekendFightMgr.m_ShowPopupMenu_CS2LuaHook = function (this, engineId, curMode, remainCDTime) 
    if engineId ~= this.SaveSelectItemEngineId then
        return
    end

    local cb = CClientObjectMgr.Inst:GetObject(engineId)
    if cb == nil then
        return
    end
    this.NowChooseEngineID = engineId
    if CommonDefs.DictContains(CWeekendFightMgr.Inst.SaveWeekendFightItemTypeIDDic, typeof(UInt32), engineId) then
        local xc = XianZongShan_Crystal.GetData(CommonDefs.DictGetValue(CWeekendFightMgr.Inst.SaveWeekendFightItemTypeIDDic, typeof(UInt32), engineId))
        if xc ~= nil then
            local auraIconDic = CreateFromClass(MakeGenericClass(Dictionary, Int32, String))
            local auraIconString = XianZongShan_Setting.GetData().AuraIcon
            local auraIconStringArray = CommonDefs.StringSplit_ArrayChar(auraIconString, ";")
            do
                local i = 0
                while i < auraIconStringArray.Length do
                    local sarray = CommonDefs.StringSplit_ArrayChar(auraIconStringArray[i], ",")
                    if sarray ~= nil and sarray.Length == 2 then
                        local id = System.Int32.Parse(sarray[0])
                        CommonDefs.DictSet(auraIconDic, typeof(Int32), id, typeof(String), sarray[1])
                    end
                    i = i + 1
                end
            end

            local auraModeString = xc.AuraMode
            local auraList = CommonDefs.StringSplit_ArrayChar(auraModeString, ";")
            if auraList.Length > 0 then
                local itemActionPairs = CreateFromClass(MakeGenericClass(List, HousePopupMenuItemData))
                do
                    local i = 0
                    while i < auraList.Length do
                        local sarray = CommonDefs.StringSplit_ArrayChar(auraList[i], ",")
                        if sarray ~= nil and sarray.Length == 2 then
                            local setMode = System.UInt32.Parse(sarray[0])
                            local auraModeId = System.Int32.Parse(sarray[1])
                            if CommonDefs.DictContains(auraIconDic, typeof(Int32), auraModeId) then
                                if curMode == setMode then
                                    CommonDefs.ListAdd(itemActionPairs, typeof(HousePopupMenuItemData), HousePopupMenuItemData(LocalString.GetString(""), DelegateFactory.Action_int(function (index) 
                                        Gac2Gas.CrystalRequestChangeAuraMode(engineId, setMode)
                                    end), CommonDefs.DictGetValue(auraIconDic, typeof(Int32), auraModeId), true, true, -1))
                                else
                                    local default
                                    if remainCDTime > 0 then
                                        default = false
                                    else
                                        default = true
                                    end
                                    CommonDefs.ListAdd(itemActionPairs, typeof(HousePopupMenuItemData), HousePopupMenuItemData(LocalString.GetString(""), DelegateFactory.Action_int(function (index) 
                                        Gac2Gas.CrystalRequestChangeAuraMode(engineId, setMode)
                                    end), CommonDefs.DictGetValue(auraIconDic, typeof(Int32), auraModeId), true, default, remainCDTime))
                                end
                            end
                        end
                        i = i + 1
                    end
                end
                --CHousePopupMenu.s_EnableRealTimeFollow = false;
                CHousePopupMgr.ShowHousePopupMenu(CommonDefs.ListToArray(itemActionPairs), cb.RO:GetSlotTransform("TopAnchor", false))
            end
        end
    end
end

Gas2Gac.CrystalSyncForcePlayerInfo = function (data) 
    CommonDefs.DictClear(CWeekendFightMgr.Inst.SaveWeekendFightItemDic)
    CommonDefs.DictClear(CWeekendFightMgr.Inst.SaveWeekendFightItemTypeIDDic)

    local playerList = TypeAs(MsgPackImpl.unpack(data), typeof(MakeGenericClass(List, Object)))
    CommonDefs.DictClear(CWeekendFightMgr.Inst.PlayerInfoDic)

    do
        local i = 0
        while i < playerList.Count do
            local info = TypeAs(playerList[i], typeof(MakeGenericClass(List, Object)))
            local pInfo = CreateFromClass(PlayerBasicInfo)
            pInfo.playerid = math.floor(tonumber(info[0] or 0))
            pInfo.force = math.floor(tonumber(info[1] or 0))
            pInfo.clazz = math.floor(tonumber(info[2] or 0))
            pInfo.gender = math.floor(tonumber(info[3] or 0))
            pInfo.name = tostring(info[4])
            pInfo.level = math.floor(tonumber(info[5] or 0))
            pInfo.xianshen = CommonDefs.Convert_ToBoolean(info[6])
            pInfo.zhanli = math.floor(tonumber(info[7] or 0))
            CommonDefs.DictSet(CWeekendFightMgr.Inst.PlayerInfoDic, typeof(UInt64), pInfo.playerid, typeof(PlayerBasicInfo), pInfo)
            i = i + 1
        end
    end
end
Gas2Gac.CrystalSyncForcePlayInfo = function (data, crystalScoreData) 
    local infoList = TypeAs(MsgPackImpl.unpack(data), typeof(MakeGenericClass(List, Object)))
    CommonDefs.ListClear(CWeekendFightMgr.Inst.ForceList)
    CommonDefs.DictClear(CWeekendFightMgr.Inst.TowerScoreDic)
    CommonDefs.ListClear(CWeekendFightMgr.Inst.MiniMapShowItemList)

    do
        local i = 0
        while i < infoList.Count do
            local info = TypeAs(infoList[i], typeof(MakeGenericClass(List, Object)))
            local fInfo = CreateFromClass(ForceInfo)
            fInfo.force = math.floor(tonumber(info[0] or 0))
            fInfo.rank = math.floor(tonumber(info[1] or 0))
            fInfo.num1 = math.floor(tonumber(info[2] or 0))
            fInfo.num2 = math.floor(tonumber(info[3] or 0))
            fInfo.num3 = math.floor(tonumber(info[4] or 0))
            fInfo.killnum = math.floor(tonumber(info[5] or 0))
            fInfo.score = math.floor(tonumber(info[6] or 0))

            CommonDefs.ListAdd(CWeekendFightMgr.Inst.ForceList, typeof(ForceInfo), fInfo)
            i = i + 1
        end
    end
    CommonDefs.ListSort1(CWeekendFightMgr.Inst.ForceList, typeof(ForceInfo), DelegateFactory.Comparison_ForceInfo(function (data1, data2) 
        return NumberCompareTo(data1.rank, data2.rank)
    end))

    local scoreList = TypeAs(MsgPackImpl.unpack(crystalScoreData), typeof(MakeGenericClass(List, Object)))
    do
        local i = 0
        while i < scoreList.Count do
            local info = TypeAs(scoreList[i], typeof(MakeGenericClass(List, Object)))
            -- engineId, score
            local pInfo = CreateFromClass(PlayItemInfo)
            pInfo.engineId = math.floor(tonumber(info[0] or 0))
            pInfo.score = math.floor(tonumber(info[1] or 0))
            pInfo.force = math.floor(tonumber(info[2] or 0))
            pInfo.x = math.floor(tonumber(info[3] or 0))
            pInfo.y = math.floor(tonumber(info[4] or 0))
            -- info[2], info[3], info[4] = force, x, y
            --if(i < CWeekendFightMgr.MiniMapMustShowCount)
            --{
            --    CWeekendFightMgr.Inst.MiniMapShowItemList.Add(pInfo);
            --}
            --else
            --{
            --    CWeekendFightMgr.ForceInfo selfInfo = CWeekendFightMgr.Inst.GetSelfForceInfo();
            --    if(selfInfo != null)
            --    {
            --        if(selfInfo.force == pInfo.force)
            --        {
            --            CWeekendFightMgr.Inst.MiniMapShowItemList.Add(pInfo);
            --        }
            --    }
            --}
            CommonDefs.ListAdd(CWeekendFightMgr.Inst.MiniMapShowItemList, typeof(PlayItemInfo), pInfo)

            CommonDefs.DictSet(CWeekendFightMgr.Inst.TowerScoreDic, typeof(UInt32), pInfo.engineId, typeof(PlayItemInfo), pInfo)
            i = i + 1
        end
    end

    EventManager.Broadcast(EnumEventType.UpdateWeekendFightForceInfo)
    EventManager.BroadcastInternalForLua(EnumEventType.UpdatePlayerHeadInfoSepIcon, {true})
end
Gas2Gac.CrystalSyncTeammatePos = function (data) 
    local infoList = TypeAs(MsgPackImpl.unpack(data), typeof(MakeGenericClass(List, Object)))
    do
        local i = 0
        while i < infoList.Count do
            local info = TypeAs(infoList[i], typeof(MakeGenericClass(List, Object)))
            -- id, posx, posy
            Debug.Log((((("CrystalSyncTeammatePos :" .. info[0]:ToString()) .. ",") .. info[1]:ToString()) .. ",") .. info[2]:ToString())
            i = i + 1
        end
    end
end
