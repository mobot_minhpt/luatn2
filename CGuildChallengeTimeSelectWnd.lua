-- Auto Generated!!
local CButton = import "L10.UI.CButton"
local CGuildChallengeMgr = import "L10.Game.CGuildChallengeMgr"
local CGuildChallengeTimeSelectWnd = import "L10.UI.CGuildChallengeTimeSelectWnd"
local CommonDefs = import "L10.Game.CommonDefs"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CTooltip = import "L10.UI.CTooltip"
local CUIPickerWndMgr = import "L10.UI.CUIPickerWndMgr"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CGuildChallengeTimeSelectWnd.m_Init_CS2LuaHook = function (this) 
    this.dateLabel.text = LocalString.GetString("已约满")
    Extensions.SetLocalRotationZ(this.dateArrow.transform, 0)
    this.timeLabel.text = LocalString.GetString("已约满")
    Extensions.SetLocalRotationZ(this.timeArrow.transform, 0)
    this.costLabel.text = tostring(CGuildChallengeMgr.Inst.costGuildSilver)
    this.ownLabel.text = tostring(CGuildChallengeMgr.Inst.ownGuildSilver)
    this.selectDateIndex = 0
    this.selectTimeIndex = 0

    local dateBtn = CommonDefs.GetComponent_GameObject_Type(this.dateArrow, typeof(CButton))
    local timeBtn = CommonDefs.GetComponent_GameObject_Type(this.timeArrow, typeof(CButton))
    local timeListCount = CGuildChallengeMgr.Inst.availableTimeList.Count
    if dateBtn ~= nil then
        dateBtn.Enabled = timeListCount > 0
    end
    if timeBtn ~= nil then
        timeBtn.Enabled = timeListCount > 0
    end
    if timeListCount > 0 then
        local timeStamp = CommonDefs.DictGetValue(CGuildChallengeMgr.Inst.availableTimeList, typeof(Int32), CGuildChallengeMgr.Inst.availableDateList[0])[0]
        local time = CServerTimeMgr.ConvertTimeStampToZone8Time(timeStamp)
        this.dateLabel.text = System.String.Format(LocalString.GetString("{0}月{1}日"), time.Month, time.Day)
        this.timeLabel.text = this:FormatTimeRange(time)
    end
    local button = CommonDefs.GetComponent_GameObject_Type(this.declareButton, typeof(CButton))
    if button ~= nil then
        button.Enabled = timeListCount > 0
    end
end
CGuildChallengeTimeSelectWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.dateSelect).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.dateSelect).onClick, MakeDelegateFromCSFunction(this.OnDateSelect, VoidDelegate, this), true)
    UIEventListener.Get(this.timeSelect).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.timeSelect).onClick, MakeDelegateFromCSFunction(this.OnTimeSelect, VoidDelegate, this), true)
    UIEventListener.Get(this.declareButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.declareButton).onClick, MakeDelegateFromCSFunction(this.OnDeclareButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.tipsButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.tipsButton).onClick, MakeDelegateFromCSFunction(this.OnTipsButtonClick, VoidDelegate, this), true)
    EventManager.AddListenerInternal(EnumEventType.OnPickerItemSelected, MakeDelegateFromCSFunction(this.OnPickerWndSelect, MakeGenericClass(Action1, Int32), this))
end
CGuildChallengeTimeSelectWnd.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.dateSelect).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.dateSelect).onClick, MakeDelegateFromCSFunction(this.OnDateSelect, VoidDelegate, this), false)
    UIEventListener.Get(this.timeSelect).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.timeSelect).onClick, MakeDelegateFromCSFunction(this.OnTimeSelect, VoidDelegate, this), false)
    UIEventListener.Get(this.declareButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.declareButton).onClick, MakeDelegateFromCSFunction(this.OnDeclareButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.tipsButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.tipsButton).onClick, MakeDelegateFromCSFunction(this.OnTipsButtonClick, VoidDelegate, this), false)
    EventManager.RemoveListenerInternal(EnumEventType.OnPickerItemSelected, MakeDelegateFromCSFunction(this.OnPickerWndSelect, MakeGenericClass(Action1, Int32), this))
end
CGuildChallengeTimeSelectWnd.m_OnPickerWndSelect_CS2LuaHook = function (this, index) 
    if this.isSelectDate then
        local date = CGuildChallengeMgr.Inst.availableDateList[index]
        if this.selectDateIndex ~= index then
            this:UpdateTimeLabel(0)
        end
        this.selectDateIndex = index
        this.dateLabel.text = System.String.Format(LocalString.GetString("{0}月{1}日"), math.floor(date / 100), date % 100)
        Extensions.SetLocalRotationZ(this.dateArrow.transform, 0)
    else
        this:UpdateTimeLabel(index)
    end
end
CGuildChallengeTimeSelectWnd.m_UpdateTimeLabel_CS2LuaHook = function (this, index) 
    local timeStamp = CommonDefs.DictGetValue(CGuildChallengeMgr.Inst.availableTimeList, typeof(Int32), CGuildChallengeMgr.Inst.availableDateList[this.selectDateIndex])[index]
    local time = CServerTimeMgr.ConvertTimeStampToZone8Time(timeStamp)
    this.selectTimeIndex = index
    this.timeLabel.text = this:FormatTimeRange(time)
    Extensions.SetLocalRotationZ(this.timeArrow.transform, 0)
end
CGuildChallengeTimeSelectWnd.m_OnDateSelect_CS2LuaHook = function (this, go) 
    this.isSelectDate = true
    local contents = CreateFromClass(MakeGenericClass(List, String))
    do
        local i = 0
        while i < CGuildChallengeMgr.Inst.availableDateList.Count do
            local time = CGuildChallengeMgr.Inst.availableDateList[i]
            CommonDefs.ListAdd(contents, typeof(String), System.String.Format(LocalString.GetString("{0}月{1}日"), math.floor(time / 100), time % 100))
            i = i + 1
        end
    end
    local localPos = go.transform.localPosition
    local anchor = go.transform.parent:TransformPoint(localPos)
    CUIPickerWndMgr.ShowPickerWnd(contents, anchor, this.selectDateIndex, CTooltip.AlignType.Right)
    Extensions.SetLocalRotationZ(this.dateArrow.transform, 180)
end
CGuildChallengeTimeSelectWnd.m_OnTimeSelect_CS2LuaHook = function (this, go) 
    this.isSelectDate = false
    local contents = CreateFromClass(MakeGenericClass(List, String))
    local key = CGuildChallengeMgr.Inst.availableDateList[this.selectDateIndex]
    do
        local i = 0
        while i < CommonDefs.DictGetValue(CGuildChallengeMgr.Inst.availableTimeList, typeof(Int32), key).Count do
            local timeStamp = CommonDefs.DictGetValue(CGuildChallengeMgr.Inst.availableTimeList, typeof(Int32), key)[i]
            local time = CServerTimeMgr.ConvertTimeStampToZone8Time(timeStamp)
            CommonDefs.ListAdd(contents, typeof(String), this:FormatTimeRange(time))
            i = i + 1
        end
    end
    local localPos = go.transform.localPosition
    local anchor = go.transform.parent:TransformPoint(localPos)
    CUIPickerWndMgr.ShowPickerWnd(contents, anchor, this.selectTimeIndex, CTooltip.AlignType.Right)
    Extensions.SetLocalRotationZ(this.timeArrow.transform, 180)
end
CGuildChallengeTimeSelectWnd.m_OnDeclareButtonClick_CS2LuaHook = function (this, go) 
    MessageWndManager.ShowOKCancelMessage(LocalString.GetString("确定要进行宣战吗？"), DelegateFactory.Action(function () 
        Gac2Gas.GC_RequestChallenge(CGuildChallengeMgr.Inst.selectGuildId, math.floor(CommonDefs.DictGetValue(CGuildChallengeMgr.Inst.availableTimeList, typeof(Int32), CGuildChallengeMgr.Inst.availableDateList[this.selectDateIndex])[this.selectTimeIndex]))
    end), nil, nil, nil, false)
end

