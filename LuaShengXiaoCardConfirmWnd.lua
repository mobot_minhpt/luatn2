local GameObject = import "UnityEngine.GameObject"

local DelegateFactory  = import "DelegateFactory"

LuaShengXiaoCardConfirmWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaShengXiaoCardConfirmWnd, "Label", "Label", UILabel)
RegistChildComponent(LuaShengXiaoCardConfirmWnd, "Button1", "Button1", GameObject)
RegistChildComponent(LuaShengXiaoCardConfirmWnd, "Button2", "Button2", GameObject)
RegistChildComponent(LuaShengXiaoCardConfirmWnd, "Button3", "Button3", GameObject)

--@endregion RegistChildComponent end

function LuaShengXiaoCardConfirmWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.Button1.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButton1Click()
	end)

	UIEventListener.Get(self.Button2).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButton2Click()
	end)

	UIEventListener.Get(self.Button3.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnButton3Click()
	end)

    --@endregion EventBind end
	if LuaShengXiaoCardMgr.m_State==EnumShengXiaoCardState.PassGuess then
		self.Button2:SetActive(false)
	end
end

function LuaShengXiaoCardConfirmWnd:Init()
	if LuaShengXiaoCardMgr.m_ShowInfo then
		local showInfo = LuaShengXiaoCardMgr.m_ShowInfo
		local actor = showInfo.Actor
		local name = tostring(actor)
		if LuaShengXiaoCardMgr.m_PlayerProfiles[actor] then
			name = LuaShengXiaoCardMgr.m_PlayerProfiles[actor].Name
		end
		local displayId = showInfo.DisplayId
		local cardName = ShengXiaoCard_ShengXiao.GetData(displayId).Name
		self.Label.text = SafeStringFormat3(LocalString.GetString("%s说这是一张“%s”"),name,cardName)
	else
		self.Label.text = nil
	end
end

--@region UIEvent

function LuaShengXiaoCardConfirmWnd:OnButton1Click()
	if LuaShengXiaoCardMgr.m_State==EnumShengXiaoCardState.WaitGuess then
		Gac2Gas.ShengXiaoCard_Guess(0)
	elseif LuaShengXiaoCardMgr.m_State==EnumShengXiaoCardState.PassGuess then
		Gac2Gas.ShengXiaoCard_PassGuess(0)
	else
		Debug.LogError("error guess 0")
	end
	CUIManager.CloseUI(CLuaUIResources.ShengXiaoCardConfirmWnd)
end

function LuaShengXiaoCardConfirmWnd:OnButton2Click()
	if LuaShengXiaoCardMgr.m_State==EnumShengXiaoCardState.WaitGuess then
		LuaShengXiaoCardMgr.TryTriggerGuide(5)
		Gac2Gas.ShengXiaoCard_Pass()
	else
		Debug.LogError("error pass")
	end
	CUIManager.CloseUI(CLuaUIResources.ShengXiaoCardConfirmWnd)
end

function LuaShengXiaoCardConfirmWnd:OnButton3Click()
	if LuaShengXiaoCardMgr.m_State==EnumShengXiaoCardState.WaitGuess then
		Gac2Gas.ShengXiaoCard_Guess(1)
	elseif LuaShengXiaoCardMgr.m_State==EnumShengXiaoCardState.PassGuess then
		Gac2Gas.ShengXiaoCard_PassGuess(1)
	else
		Debug.LogError("error guess 1")
	end
	CUIManager.CloseUI(CLuaUIResources.ShengXiaoCardConfirmWnd)
end


--@endregion UIEvent

