local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local ShareBoxMgr = import "L10.UI.ShareBoxMgr"
local EShareType = import "L10.UI.EShareType"

LuaJiaoYueDuoSuYiResultWnd = class()
LuaJiaoYueDuoSuYiResultWnd.s_IsWin = false

--@region RegistChildComponent: Dont Modify Manually!


--@endregion RegistChildComponent end

function LuaJiaoYueDuoSuYiResultWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    local info = LuaZhongQiu2022Mgr:GetPlayerInfo(LuaZhongQiu2022Mgr.MyRole)
    local isWin = LuaJiaoYueDuoSuYiResultWnd.s_IsWin
    local succ = self.transform:Find("Anchor/Result/Success")
    local fail = self.transform:Find("Anchor/Result/Fail")
    local fx = self.transform:Find("Anchor/Fx"):GetComponent(typeof(CUIFx))
    if isWin then
        if LuaZhongQiu2022Mgr.MyRole == 0 then
            fx:LoadFx("Fx/UI/Prefab/UI_jiaoyue_shengli_tuzi.prefab")
        else
            fx:LoadFx("Fx/UI/Prefab/UI_jiaoyue_shengli_huli.prefab")
        end
        --succ.gameObject:SetActive(true)
        --fail.gameObject:SetActive(false)
        --succ:Find("Portrait/Fox").gameObject:SetActive(LuaZhongQiu2022Mgr.MyRole == 1)
        --succ:Find("Portrait/Rabbit").gameObject:SetActive(LuaZhongQiu2022Mgr.MyRole == 0)
    else        
        if LuaZhongQiu2022Mgr.MyRole == 0 then
            fx:LoadFx("Fx/UI/Prefab/UI_jiaoyue_shibai_tuzi.prefab")
        else
            fx:LoadFx("Fx/UI/Prefab/UI_jiaoyue_shibai_huli.prefab")
        end
        --fail.gameObject:SetActive(true)
        --succ.gameObject:SetActive(false)
        --fail:Find("Portrait/Fox").gameObject:SetActive(LuaZhongQiu2022Mgr.MyRole == 1)
        --fail:Find("Portrait/Rabbit").gameObject:SetActive(LuaZhongQiu2022Mgr.MyRole == 0)
    end

    local shareBtn = self.transform:Find("Anchor/Bottom/ShareButton").gameObject
    local closeBtn = self.transform:Find("WndBg/CloseButton").gameObject
    CommonDefs.AddOnClickListener(
        shareBtn, 
        DelegateFactory.Action_GameObject(function(go)
            shareBtn:SetActive(false)
            closeBtn:SetActive(false)
            CUICommonDef.CaptureScreen("screenshot", true, false, nil, DelegateFactory.Action_string_bytes(function(fullPath, jpgBytes)
                ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, fullPath)
                shareBtn:SetActive(true)
                closeBtn:SetActive(true)
            end))
        end), 
        false
    ) 

    local player = self.transform:Find("Anchor/Bottom/PlayerInfo")
    player:Find("Player/Avatar"):GetComponent(typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(info.cls, info.gender, -1))
    player:Find("Player/Lv"):GetComponent(typeof(UILabel)).text = info.lv
    player:Find("NameLabel/Name"):GetComponent(typeof(UILabel)).text = info.name
    player:Find("ServerLabel/Server"):GetComponent(typeof(UILabel)).text = CClientMainPlayer.Inst and CClientMainPlayer.Inst:GetMyServerName() or LocalString.GetString("未知服")
    player:Find("IDLabel/ID"):GetComponent(typeof(UILabel)).text = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or ""

    shareBtn:SetActive(not CommonDefs.IS_VN_CLIENT)
end

function LuaJiaoYueDuoSuYiResultWnd:Init()
    local shareSp = self.transform:Find("Anchor/Bottom/ShareButton/Sprite"):GetComponent(typeof(UISprite))
    shareSp.atlas = self.transform:Find("Anchor/Bottom/Bg"):GetComponent(typeof(UISprite)).atlas
    shareSp.spriteName = "common_fenxiang"
    shareSp.width = 64
    shareSp.height = 54
    LuaUtils.SetLocalPositionX(shareSp.transform, -79)
end

--@region UIEvent

--@endregion UIEvent

