local DelegateFactory = import "DelegateFactory"
local CScheduleMgr    = import "L10.Game.CScheduleMgr"
local ClientAction    = import "L10.UI.ClientAction"
local Item_Item       = import "L10.Game.Item_Item"
local CServerTimeMgr  = import "L10.Game.CServerTimeMgr"

LuaCommonJieRiMainWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaCommonJieRiMainWnd, "activityButtonItem")
RegistClassMember(LuaCommonJieRiMainWnd, "importantActivityItem")
RegistClassMember(LuaCommonJieRiMainWnd, "nonImportantActivityItem")

RegistClassMember(LuaCommonJieRiMainWnd, "activityButtonTable")
RegistClassMember(LuaCommonJieRiMainWnd, "specialActivityTable")
RegistClassMember(LuaCommonJieRiMainWnd, "displayType2ScheduleList") -- 显示类型 -> 活动列表
RegistClassMember(LuaCommonJieRiMainWnd, "jieRiId")

--RegistClassMember(LuaCommonJieRiMainWnd, "redDotId2Go") -- 红点id -> 红点Gameobject
RegistClassMember(LuaCommonJieRiMainWnd, "scheduleId2Go") -- 日程id -> 红点id
RegistClassMember(LuaCommonJieRiMainWnd, "scheduleId2RedDotId") -- 日程id -> 红点id

function LuaCommonJieRiMainWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end

    local anchor = self.transform:Find("Anchor")
    self.activityButtonItem = anchor:Find("Pool/ActivityButtonItem").gameObject
    self.importantActivityItem = anchor:Find("Pool/ImportantActivityItem").gameObject
    self.nonImportantActivityItem = anchor:Find("Pool/NonImportantActivityItem").gameObject

    self.activityButtonItem:SetActive(false)
    self.importantActivityItem:SetActive(false)
    self.nonImportantActivityItem:SetActive(false)

    self.activityButtonTable = anchor:Find("Info/ActivityButtonTable"):GetComponent(typeof(UITable))
    self.specialActivityTable = anchor:Find("Info/ScrollView/Table"):GetComponent(typeof(UITable))
    self.jieRiId = LuaCommonJieRiMgr.jieRiId

    local jieRiGroupData = Task_JieRiGroup.GetData(self.jieRiId)
    anchor:Find("Info/ActivityTime"):GetComponent(typeof(UILabel)).text = jieRiGroupData.ActivityTime
end

function LuaCommonJieRiMainWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdateActivity", self, "OnUpdateActivity")
    g_ScriptEvent:AddListener("UpdateActivityRedDot", self, "OnUpdateActivityRedDot")
    CScheduleMgr.Inst:RequestActivity()
end

function LuaCommonJieRiMainWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateActivity", self, "OnUpdateActivity")
    g_ScriptEvent:RemoveListener("UpdateActivityRedDot", self, "OnUpdateActivityRedDot")
end

function LuaCommonJieRiMainWnd:OnUpdateActivity()
    self:UpdateSchedule()
end

function LuaCommonJieRiMainWnd:OnUpdateActivityRedDot(hasChanged)
    if hasChanged then
        self:UpdateRedDot()
    end
end

function LuaCommonJieRiMainWnd:Init()
    self:InitScheduleId2RedDotId()
    self:UpdateSchedule()
    self:UpdateRedDot()
end

function LuaCommonJieRiMainWnd:UpdateSchedule()
    self:UpdateJieRiScheduleList()
    self.scheduleId2Go = {}
    self:UpdateActivityButtonTable()
    self:UpdateSpecialActivityTable()
end

-- 更新需要显示的活动列表
function LuaCommonJieRiMainWnd:UpdateJieRiScheduleList()
    local dict = {}
    local rawlist1 = CScheduleMgr.Inst.rawInfoNoTimeRestriction -- 日常
    local rawlist2 = CScheduleMgr.Inst.rawInfoTimeRestriction -- 限时
    if rawlist1 then
        for i = 1, rawlist1.Count do
            local info = rawlist1[i - 1]
            if not dict[info.activityId] then
                dict[info.activityId] = { info }
            else
                table.insert(dict[info.activityId], info)
            end
        end
    end

    if rawlist2 then
        for i = 1, rawlist2.Count do
            local info = rawlist2[i - 1]
            if not dict[info.activityId] then
                dict[info.activityId] = { info }
            else
                table.insert(dict[info.activityId], info)
            end
        end
    end

    local list = {}
    Task_JieRiSchedule.Foreach(function(scheduleId, data)
        if data.JieRiId == self.jieRiId then
            if dict[scheduleId] then
                table.insert(list, {id = scheduleId, infos = dict[scheduleId]})
            elseif data.OnlyShowWhenOpen == 0 then
                table.insert(list, {id = scheduleId})
            end
        end
    end)

    table.sort(list, function(a, b)
        local orderA = Task_JieRiSchedule.GetData(a.id).DisplayOrder
        local orderB = Task_JieRiSchedule.GetData(b.id).DisplayOrder
        if orderA ~= orderB then return orderA < orderB end
        return a.id < b.id
    end)

    self.displayType2ScheduleList = {}
    for _, data in ipairs(list) do
        local displayType = Task_JieRiSchedule.GetData(data.id).DisplayType
        if not self.displayType2ScheduleList[displayType] then
            self.displayType2ScheduleList[displayType] = {}
        end
        table.insert(self.displayType2ScheduleList[displayType], data)
    end
end

-- 初始化日程id -> 红点id
function LuaCommonJieRiMainWnd:InitScheduleId2RedDotId()
    self.scheduleId2RedDotId = {}
    Task_JieRiRedDot.Foreach(function(id, data)
        if data.Activity and data.Activity.Length > 0 then
            for i = 0, data.Activity.Length - 1 do
                if self.scheduleId2RedDotId[data.Activity[i]] then
                else
                    self.scheduleId2RedDotId[data.Activity[i]] = {}
                end
                table.insert(self.scheduleId2RedDotId[data.Activity[i]], id)
            end
        end
    end)
end

-- 保留活动按钮
function LuaCommonJieRiMainWnd:UpdateActivityButtonTable()
    Extensions.RemoveAllChildren(self.activityButtonTable.transform)
    local scheduleList = self.displayType2ScheduleList[EnumCommonJieRiDisplayType.ActivityButton]
    if not scheduleList or #scheduleList == 0 then return end

    for _, data in ipairs(scheduleList) do
        local scheduleId = data.id
        local jieRiScheduleData = Task_JieRiSchedule.GetData(scheduleId)
        local child = NGUITools.AddChild(self.activityButtonTable.gameObject, self.activityButtonItem)
        child:SetActive(true)

        if not System.String.IsNullOrEmpty(jieRiScheduleData.ButtonIcon) then
            child.transform:GetComponent(typeof(CUITexture)):LoadMaterial(jieRiScheduleData.ButtonIcon)
        end
        child.transform:Find("Name"):GetComponent(typeof(UILabel)).text = jieRiScheduleData.Name

        local redDotGo = child.transform:Find("Alert").gameObject
        redDotGo:SetActive(false)
        local redDotId = self.scheduleId2RedDotId[scheduleId]
        if redDotId then
            self.scheduleId2Go[scheduleId] = redDotGo
        end
        UIEventListener.Get(child).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnActivityButtonClick(scheduleId, data.infos, redDotId)
        end)
    end
    self.activityButtonTable:Reposition()
end

-- 特殊活动列表
function LuaCommonJieRiMainWnd:UpdateSpecialActivityTable()
    Extensions.RemoveAllChildren(self.specialActivityTable.transform)

    -- 重要活动
    local scheduleList = self.displayType2ScheduleList[EnumCommonJieRiDisplayType.ImportantActivity]
    if scheduleList and #scheduleList > 0 then
        for _, data in ipairs(scheduleList) do
            local scheduleId = data.id
            local jieRiScheduleData = Task_JieRiSchedule.GetData(scheduleId)
            local child = NGUITools.AddChild(self.specialActivityTable.gameObject, self.importantActivityItem)
            child:SetActive(true)

            child.transform:Find("Name"):GetComponent(typeof(UILabel)).text = jieRiScheduleData.Name
            local iconMatPath = String.IsNullOrEmpty(jieRiScheduleData.AwardIcon) and self:GetRewardIcon(scheduleId) or jieRiScheduleData.AwardIcon
            child.transform:Find("Icon"):GetComponent(typeof(CUITexture)):LoadMaterial(iconMatPath)
            child.transform:Find("Icon/Type"):GetComponent(typeof(UILabel)).text = jieRiScheduleData.ActivityType

            local redDotGo = child.transform:Find("Button/Alert").gameObject
            redDotGo:SetActive(false)
            local redDotId = self.scheduleId2RedDotId[scheduleId]
            if redDotId then
                self.scheduleId2Go[scheduleId] = redDotGo
            end
            local isOpenToday = data.infos ~= nil
            child.transform:Find("Lock").gameObject:SetActive(not isOpenToday)
            local time = child.transform:Find("Time"):GetComponent(typeof(UILabel))
            local canJiaButton = child.transform:Find("Button").gameObject
            local notOpenTrans = child.transform:Find("NotOpen")
            if notOpenTrans then notOpenTrans.gameObject:SetActive(false) end
            if isOpenToday then
                canJiaButton:SetActive(true)
                time.text = self:GetOpenTime(data.infos)
                UIEventListener.Get(canJiaButton).onClick = DelegateFactory.VoidDelegate(function (go)
                    self:OnCanJiaButtonClick(scheduleId, redDotId)
                end)
            else
                time.text = jieRiScheduleData.NotOpenText
                canJiaButton:SetActive(false)
            end

            UIEventListener.Get(child).onClick = DelegateFactory.VoidDelegate(function (go)
                self:OnImportantActivityClick(scheduleId, data.infos)
            end)
        end
    end

    -- 非重要活动
    scheduleList = self.displayType2ScheduleList[EnumCommonJieRiDisplayType.NonImportantActivity]
    if scheduleList and #scheduleList > 0 then
        for _, data in ipairs(scheduleList) do
            local scheduleId = data.id
            local jieRiScheduleData = Task_JieRiSchedule.GetData(scheduleId)
            local child = NGUITools.AddChild(self.specialActivityTable.gameObject, self.nonImportantActivityItem)
            child:SetActive(true)

            child.transform:Find("Name"):GetComponent(typeof(UILabel)).text = jieRiScheduleData.Name

            local redDotGo = child.transform:Find("Alert").gameObject
            redDotGo:SetActive(false)
            local redDotId = self.scheduleId2RedDotId[scheduleId]
            if redDotId then
                self.scheduleId2Go[scheduleId] = redDotGo
            end

            local isOpenToday = data.infos ~= nil
            child.transform:Find("Lock").gameObject:SetActive(not isOpenToday)
            local other = child.transform:Find("Other"):GetComponent(typeof(UILabel))
            other.gameObject:SetActive(isOpenToday)
            if isOpenToday then other.text = jieRiScheduleData.OtherText end

            UIEventListener.Get(child).onClick = DelegateFactory.VoidDelegate(function (go)
                self:OnNonImportantActivityClick(scheduleId, data.infos, redDotId)
            end)
        end
    end

    self.specialActivityTable:Reposition()
end

-- 获取奖励图标
function LuaCommonJieRiMainWnd:GetRewardIcon(scheduleId)
    local scheduleData = Task_Schedule.GetData(scheduleId)
    if not scheduleData.Reward or scheduleData.Reward.Length == 0 then return end

    local id = scheduleData.Reward[0]
    local itemData = Item_Item.GetData(id)
    if itemData then
        return itemData.Icon
    else
        local equipData = EquipmentTemplate_Equip.GetData(id)
        if equipData then return equipData.Icon end
    end
end

-- 获取最近开放时间段
function LuaCommonJieRiMainWnd:GetOpenTime(infos)
    if infos[1].lastingTime <= 0 then return LocalString.GetString("全天") end -- 非限时活动
    if infos[1].lastingTime == 1440 then return LocalString.GetString("全天") end   -- 持续时间为一天的活动
    local displayedInfo = infos[#infos]
    if #infos > 1 then
        table.sort(infos, function (info1, info2)
            local startTime1 = info1.hour * 60 + info1.minute
            local startTime2 = info2.hour * 60 + info2.minute
            if startTime1 ~= startTime2 then
                return startTime1 < startTime2
            else
                return false
            end
        end)

        local now = CServerTimeMgr.Inst:GetZone8Time()
        local nowTime = (now.Hour * 60 + now.Minute) * 60 + now.Second
        for _, info in ipairs(infos) do
            local endTime = (info.hour * 60 + info.minute + info.lastingTime) * 60
            if nowTime <= endTime then
                displayedInfo = info
                break
            end
        end
    end
    return self:GetOpenTimeStr(displayedInfo)
end

function LuaCommonJieRiMainWnd:GetOpenTimeStr(info)
    local startTime = info.hour * 60 + info.minute
    local endTime = math.min(1439, startTime + info.lastingTime)
    local endHour = math.floor(endTime / 60)
    local endMinute = endTime % 60
    return SafeStringFormat3("%02d:%02d-%02d:%02d", info.hour, info.minute, endHour, endMinute)
end


function LuaCommonJieRiMainWnd:UpdateRedDot()
    for id, go in pairs(self.scheduleId2Go) do
        local scheduleId = id
        local reddotList = self.scheduleId2RedDotId[scheduleId]
        local show = false
        for _, redDotId in pairs(reddotList) do
            show = show or LuaActivityRedDotMgr:IsRedDot(redDotId)
        end
        go:SetActive(show)
    end
end

-- 显示活动信息
function LuaCommonJieRiMainWnd:ShowScheduleInfo(infos)
    if #infos == 1 then
        CLuaScheduleMgr.selectedScheduleInfo = infos[1]
        CUIManager.ShowUI(CLuaUIResources.ScheduleInfoWnd)
    else
        local timeStr = ""
        for i = 1, #infos do
            timeStr = timeStr .. self:GetOpenTimeStr(infos[i])
            if i < #infos then
                timeStr = timeStr .. ", "
            end
        end

        CLuaScheduleMgr.selectedScheduleInfo = {
            activityId = infos[1].activityId,
            taskId = infos[1].taskId,
            TotalTimes = infos[1].TotalTimes,
            DailyTimes = 0,
            FinishedTimes = infos[1].FinishedTimes,
            ActivityTime = SafeStringFormat3(LocalString.GetString("%s, %s"), Task_Schedule.GetData(infos[1].activityId).ExternTip, timeStr),
            ShowJoinBtn = CLuaScheduleMgr.CanShowButton(infos[1]),
        }
        CLuaScheduleInfoWnd.s_UseCustomInfo = true
        CUIManager.ShowUI(CLuaUIResources.ScheduleInfoWnd)
    end
end

-- 如果填了DoTaskLevelJudge，会判断是否达到了任务等级，未达到给个提示，不执行action
function LuaCommonJieRiMainWnd:DoAction(action, scheduleId)
    if Task_JieRiSchedule.GetData(scheduleId).DoTaskLevelJudge > 0 then
        local player = CClientMainPlayer.Inst
        local taskData = Task_Task.GetData(Task_Schedule.GetData(scheduleId).TaskID[0])
        if player then
            local minGrade = player.HasFeiSheng and taskData.GradeCheckFS1[0] or taskData.GradeCheck[0]
            if player.Level < minGrade then
                g_MessageMgr:ShowMessage("COMMON_JIRRI_LEVEL_IS_NOT_OK", minGrade)
                return
            end
        end
    end
    ClientAction.DoAction(action)
end

--@region UIEvent

function LuaCommonJieRiMainWnd:OnActivityButtonClick(scheduleId, infos, redDotId)
    if redDotId then 
        for i = 1, #redDotId do
            LuaActivityRedDotMgr:OnRedDotClicked(redDotId[i])
        end
    end
    if infos then
        local action = Task_Schedule.GetData(scheduleId).Action
        if not System.String.IsNullOrEmpty(action) then
            self:DoAction(action, scheduleId)
        else
            self:ShowScheduleInfo(infos)
        end
    else
        g_MessageMgr:ShowMessage("COMMON_JIRRI_TODAY_IS_NOT_OPEN", Task_Schedule.GetData(scheduleId).ExternTip)
    end
end

function LuaCommonJieRiMainWnd:OnCanJiaButtonClick(scheduleId, redDotId)
    if redDotId then
        for i = 1, #redDotId do
            LuaActivityRedDotMgr:OnRedDotClicked(redDotId[i])
        end
    end
    local action = Task_Schedule.GetData(scheduleId).Action
    if not System.String.IsNullOrEmpty(action) then
        self:DoAction(action, scheduleId)
    end
end

function LuaCommonJieRiMainWnd:OnImportantActivityClick(scheduleId, infos)
    if infos then
        self:ShowScheduleInfo(infos)
    else
        g_MessageMgr:ShowMessage("COMMON_JIRRI_TODAY_IS_NOT_OPEN", Task_Schedule.GetData(scheduleId).ExternTip)
    end
end

function LuaCommonJieRiMainWnd:OnNonImportantActivityClick(scheduleId, infos, redDotId)
    if redDotId then
        for i = 1, #redDotId do
            LuaActivityRedDotMgr:OnRedDotClicked(redDotId[i])
        end
    end
    if infos then
        local action = Task_Schedule.GetData(scheduleId).Action
        if not System.String.IsNullOrEmpty(action) then
            self:DoAction(action, scheduleId)
        else
            self:ShowScheduleInfo(infos)
        end
    else
        g_MessageMgr:ShowMessage("COMMON_JIRRI_TODAY_IS_NOT_OPEN", Task_Schedule.GetData(scheduleId).ExternTip)
    end
end

--@endregion UIEvent
