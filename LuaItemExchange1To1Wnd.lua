local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local UITable = import "UITable"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local CUICommonDef = import "L10.UI.CUICommonDef"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local CButton = import "L10.UI.CButton"
local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local GameSetting_Common_Wapper = import "L10.Game.GameSetting_Common_Wapper"
local NGUIText = import "NGUIText"
local UIEventListener = import "UIEventListener"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local AlignType = import "L10.UI.CTooltip+AlignType"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local MessageWndManager=import "L10.UI.MessageWndManager"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local Constants = import "L10.Game.Constants"
local Item_Item = import "L10.Game.Item_Item"
local Zhuangshiwu_Setting = import "L10.Game.Zhuangshiwu_Setting"
local Zhuangshiwu_Zhuangshiwu = import "L10.Game.Zhuangshiwu_Zhuangshiwu"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local Exchange_Exchange = import "L10.Game.Exchange_Exchange"
LuaItemExchange1To1Wnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaItemExchange1To1Wnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaItemExchange1To1Wnd, "Table", "Table", UITable)
RegistChildComponent(LuaItemExchange1To1Wnd, "ItemSelectCell", "ItemSelectCell", GameObject)
RegistChildComponent(LuaItemExchange1To1Wnd, "ScrollViewIndicator", "ScrollViewIndicator", GameObject)
RegistChildComponent(LuaItemExchange1To1Wnd, "MateralHole", "MateralHole", GameObject)
RegistChildComponent(LuaItemExchange1To1Wnd, "ComposeHole", "ComposeHole", GameObject)
RegistChildComponent(LuaItemExchange1To1Wnd, "QnIncreseAndDecreaseButton", "QnIncreseAndDecreaseButton", QnAddSubAndInputButton)
RegistChildComponent(LuaItemExchange1To1Wnd, "ComposeBtn", "ComposeBtn", CButton)
RegistChildComponent(LuaItemExchange1To1Wnd, "SilverCost", "SilverCost", GameObject)
RegistChildComponent(LuaItemExchange1To1Wnd, "InstructionLabel", "InstructionLabel", GameObject)
RegistChildComponent(LuaItemExchange1To1Wnd, "ScrollView", "ScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaItemExchange1To1Wnd, "OneMaterialCompose", "OneMaterialCompose", GameObject)
RegistChildComponent(LuaItemExchange1To1Wnd, "QnCostAndOwnMoney", "QnCostAndOwnMoney", CCurentMoneyCtrl)

--@endregion RegistChildComponent end
RegistClassMember(LuaItemExchange1To1Wnd, "m_ExchangeIdList")
RegistClassMember(LuaItemExchange1To1Wnd, "m_TabList")
RegistClassMember(LuaItemExchange1To1Wnd, "m_ItemSelect")
RegistClassMember(LuaItemExchange1To1Wnd, "m_composeId")
RegistClassMember(LuaItemExchange1To1Wnd, "m_composeIdList")
RegistClassMember(LuaItemExchange1To1Wnd, "m_MaterialAvaliable")
RegistClassMember(LuaItemExchange1To1Wnd, "m_LeftTimes")

function LuaItemExchange1To1Wnd:Awake()
    self.InstructionLabel:SetActive(false)
    self.TitleLabel.text = Zhuangshiwu_Setting.GetData().ZhuangShiWuExchangeWndTitle

    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaItemExchange1To1Wnd:Init()
    local exchangeSetting = Zhuangshiwu_Setting.GetData().ZhuangShiWuExchangeGroupId
    if not exchangeSetting then return end
    self.m_ExchangeIdList = exchangeSetting
    self:UpdateLeftTimeInfo()
    self.InstructionLabel:SetActive(true)
    self:InitEmpty()
    self:InitLeftTab()
end

function LuaItemExchange1To1Wnd:InitEmpty()
    Extensions.RemoveAllChildren(self.Table.transform)
    self.ItemSelectCell:SetActive(false)
    self.m_TabList = {}
    self.m_composeIdList = {}
end

function LuaItemExchange1To1Wnd:InitLeftTab()
    local count = 0
    local len = self.m_ExchangeIdList.Length
    for i=0,len-1 do
        --local exchangeInfo = Exchange_Exchange.GetData(self.m_ExchangeIdList[i])
        local exchangeInfo = Zhuangshiwu_Exchange.GetData(self.m_ExchangeIdList[i])
        if exchangeInfo then
            local newItems = g_LuaUtil:StrSplit(exchangeInfo.NewItems,",")
            local newItemInfo = g_LuaUtil:StrSplit(newItems[1],":")
            self:AddItemWithTemplateId(newItemInfo[1],newItemInfo[2],self.m_ExchangeIdList[i])
            count = count + 1
            --print(exchangeInfo.NewItems[0][0])
            --self:AddItemWithTemplateId(exchangeInfo.NewItems[0][0],exchangeInfo.NewItems[0][1],self.m_ExchangeIdList[i])
            --count = count + 1
        end
    end
    self.Table:Reposition()
    self.ScrollView:ResetPosition()
    if count > 0 then
        self:OnSelectButtonClick(self.m_TabList[1].gameObject:GetComponent(typeof(QnSelectableButton)),self.m_composeIdList[1])
    else
        self:InitEmpty()
    end
end

function LuaItemExchange1To1Wnd:AddItemWithTemplateId(templateId,count,composeId)
    local SelectItemGo = CUICommonDef.AddChild(self.Table.gameObject,self.ItemSelectCell.gameObject)
    local ItemIcon = SelectItemGo.transform:Find("ItemCell/Icon"):GetComponent(typeof(CUITexture))
    local ItemName = SelectItemGo.transform:Find("Name"):GetComponent(typeof(UILabel))
    local ItemCount = SelectItemGo.transform:Find("Count"):GetComponent(typeof(UILabel))
    local ItemQuality = SelectItemGo.transform:Find("ItemCell/frame"):GetComponent(typeof(UISprite)) 
    SelectItemGo.transform:Find("BindCount").gameObject:SetActive(false)

    local item = Item_Item.GetData(templateId)
    ItemIcon:LoadMaterial(item.Icon)
    ItemQuality.spriteName = CUICommonDef.GetItemCellBorder(item, nil, false)
    ItemName.text = SafeStringFormat3("[c][%s]%s[-][/c]",NGUIText.EncodeColor24(GameSetting_Common_Wapper.Inst:GetColor(item.NameColor)),item.Name)
    ItemCount.text = tostring(count)
    local qnButton = SelectItemGo:GetComponent(typeof(QnSelectableButton))
    qnButton:SetSelected(false)
    UIEventListener.Get(SelectItemGo).onClick = DelegateFactory.VoidDelegate(function (go)
        self:OnSelectButtonClick(go:GetComponent(typeof(QnSelectableButton)),composeId)
    end)

    table.insert(self.m_TabList,SelectItemGo)
    table.insert(self.m_composeIdList,composeId)
    SelectItemGo:SetActive(true)
end

function LuaItemExchange1To1Wnd:OnSelectButtonClick(Button,composeId)
    if self.m_ItemSelect and self.m_ItemSelect ~= Button then 
        self.m_ItemSelect:SetSelected(false)
        self.m_ItemSelect = nil
    end
    Button:SetSelected(true)
    self.m_ItemSelect = Button
    self:InitComposeDetailWnd(composeId)
end

function LuaItemExchange1To1Wnd:InitComposeDetailWnd(composeId)
    self.m_composeId = composeId
    --local exchangeInfo = Exchange_Exchange.GetData(composeId)
    local exchangeInfo = Zhuangshiwu_Exchange.GetData(composeId)
    self:InitComposeDetailEmpty()
    if not exchangeInfo then return end
    local newItems = g_LuaUtil:StrSplit(exchangeInfo.NewItems,",")
    local newItemInfo = g_LuaUtil:StrSplit(newItems[1],":")
    local ConsumeItems = g_LuaUtil:StrSplit(exchangeInfo.ConsumeItems,",")
    local ConsumeItemsInfo = g_LuaUtil:StrSplit(ConsumeItems[1],":")

    self.QnIncreseAndDecreaseButton.gameObject:SetActive(true)
    self.QnIncreseAndDecreaseButton:SetValue(1,false)
    local needCount = ConsumeItemsInfo[2]
    local totalCount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag,ConsumeItemsInfo[1])
    local maxNum = math.max(1,math.floor(totalCount/needCount))
    --maxNum = math.min(maxNum,self.m_LeftTimes)
    self.QnIncreseAndDecreaseButton:SetMinMax(1, math.max(maxNum,1), 1)

    local cost = 0
    if exchangeInfo.ConsumeYinPiao > 0 then
        cost = exchangeInfo.ConsumeYinPiao
        self.QnCostAndOwnMoney:SetType(EnumMoneyType.YinPiao, EnumPlayScoreKey.NONE, true)
    elseif exchangeInfo.ConsumeSilver > 0 then
        cost = exchangeInfo.ConsumeSilver
        self.QnCostAndOwnMoney:SetType(EnumMoneyType.YinLiang, EnumPlayScoreKey.NONE, true)
    end

    local needSliver = cost > 0

    self.SilverCost:SetActive(needSliver)
    self.ComposeBtn.gameObject:SetActive(not needSliver)
    if needSliver then 
        self.QnCostAndOwnMoney:SetCost(self.QnIncreseAndDecreaseButton:GetValue() * cost) 
        local ComposeBtn = self.SilverCost.transform:Find("ComposeBtn").gameObject:GetComponent(typeof(CButton))
        UIEventListener.Get(ComposeBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnExchangeButtonClick(ConsumeItemsInfo[1],ConsumeItemsInfo[2],needSliver,newItemInfo[1])
        end)
    else
        UIEventListener.Get(self.ComposeBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            self:OnExchangeButtonClick(ConsumeItemsInfo[1],ConsumeItemsInfo[2],needSliver,newItemInfo[1])
        end)
    end

    self.OneMaterialCompose.gameObject:SetActive(true)
    self:InitMaterialCompose(ConsumeItemsInfo,self.QnIncreseAndDecreaseButton:GetValue(),newItemInfo)
    self.QnIncreseAndDecreaseButton.onValueChanged = DelegateFactory.Action_uint(function (v)
        self:InitMaterialCompose(ConsumeItemsInfo,v,newItemInfo)
        if needSliver then self.QnCostAndOwnMoney:SetCost(v * cost) end
    end)
end

function LuaItemExchange1To1Wnd:InitComposeDetailEmpty()
    self.OneMaterialCompose.gameObject:SetActive(false)
    self.QnIncreseAndDecreaseButton.gameObject:SetActive(false)
    self.ComposeHole.gameObject:SetActive(false)
    self.MateralHole.gameObject:SetActive(false)
end

function LuaItemExchange1To1Wnd:InitMaterialCompose(MaterialInfo,totalAmount,ComposeInfo)
    local MaterialItem = Item_Item.GetData(MaterialInfo[1])
    local ComposeItem = Item_Item.GetData(ComposeInfo[1])
    if not MaterialItem or not ComposeItem then return end
    self:InitItemHole(self.MateralHole,MaterialItem,MaterialInfo[2],totalAmount,true)
    self:InitItemHole(self.ComposeHole,ComposeItem,ComposeInfo[2],totalAmount,false)
    self.OneMaterialCompose.gameObject:SetActive(true)
end

function LuaItemExchange1To1Wnd:InitItemHole(go,Item,needCount,totalAmount,isMaterial)
    local notEnoughGo = go.transform:Find("NotEnough")
    local Icon = go.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local Count = go.transform:Find("Count"):GetComponent(typeof(UILabel))
    local Name = go.transform:Find("Name"):GetComponent(typeof(UILabel))
    local Quality = go.transform:Find("Quality"):GetComponent(typeof(UISprite)) 
    notEnoughGo.gameObject:SetActive(false)

    Icon:LoadMaterial(Item.Icon)
    Name.text = Item.Name
    Name.color = GameSetting_Common_Wapper.Inst:GetColor(Item.NameColor)
    Quality.spriteName = CUICommonDef.GetItemCellBorder(Item, nil, false)
    if not isMaterial then 
        Count.text = tostring(totalAmount * needCount)
    else
        local NeedCount = totalAmount * needCount 
        local TotalCount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, Item.ID)
        if NeedCount > TotalCount then
            self.m_MaterialAvaliable = false
            Count.text = SafeStringFormat3("[c][FF0000]%d[-]/%d",TotalCount,NeedCount)
            notEnoughGo.gameObject:SetActive(true)
        else
            self.m_MaterialAvaliable = true
            Count.text = SafeStringFormat3("[c][00FF00]%d[-]/%d",TotalCount,NeedCount)
            notEnoughGo.gameObject:SetActive(false)
        end
    end
    UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function (go)
        CItemInfoMgr.ShowLinkItemTemplateInfo(Item.ID)
    end)

    if isMaterial and not self.m_MaterialAvaliable then
        UIEventListener.Get(notEnoughGo.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            CItemAccessListMgr.Inst:ShowItemAccessInfo(Item.ID, false, self.MateralHole.transform, AlignType.Top)
        end)
    end
    go.gameObject:SetActive(true)
end

function LuaItemExchange1To1Wnd:OnExchangeButtonClick(materialId,needMaterialNum,needSliver,newItemsId)
    --if self.m_LeftTimes <= 0 then
    --    g_MessageMgr:ShowMessage("ZHUANGSHIWU_EXCHANGE_TIMES_LIMIT")
    --    return 
    --end
    local item = Item_Item.GetData(newItemsId)
    local ItemName = SafeStringFormat3("[c][%s]%s[-][/c]",NGUIText.EncodeColor24(GameSetting_Common_Wapper.Inst:GetColor(item.NameColor)),item.Name)
    if not self.m_MaterialAvaliable then
        CItemAccessListMgr.Inst:ShowItemAccessInfo(materialId, false, self.MateralHole.transform, AlignType.Top)
    elseif needSliver and not self.QnCostAndOwnMoney.moneyEnough then
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("NotEnough_Silver_QuickBuy", nil), DelegateFactory.Action(function () 
            CShopMallMgr.ShowLinyuShoppingMall(Constants.SilverItemId)
        end), nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
    else
        local Amount = self.QnIncreseAndDecreaseButton:GetValue()
        local needCount = needMaterialNum * Amount
        local needHouseGrade = Zhuangshiwu_Zhuangshiwu.GetDataBySubKey("ItemId",newItemsId).HouseGrade
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("ZhuangShiWu_Exchange_Confirm",tostring(needCount), ItemName, needHouseGrade), DelegateFactory.Action(function () 
        --MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("ZhuangShiWu_Exchange_Confirm", self.m_LeftTimes), DelegateFactory.Action(function () 
            Gac2Gas.PlayerRequestExchangeZhuangShiWu(self.m_composeId, self.QnIncreseAndDecreaseButton:GetValue())
            --print(self.m_composeId,self.QnIncreseAndDecreaseButton:GetValue())
        end), nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
    end
end

function LuaItemExchange1To1Wnd:UpdateLeftTimeInfo()
    --self.m_LeftTimes = Zhuangshiwu_Setting.GetData().ZhuangShiWuMaxExchangeTimes - CCarnivalLotteryMgr.Inst:GetGamePlayTempData(EnumPlayTimesKey_lua.eZhuangShiWuExchangeTimes)
    --self.m_LeftTimes = Zhuangshiwu_Setting.GetData().ZhuangShiWuMaxExchangeTimes
    --self.InstructionLabel:GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("ZhuangShiWu_Exchange_Tip",self.m_LeftTimes)
    self.InstructionLabel:GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("ZhuangShiWu_Exchange_Tip")
end

function LuaItemExchange1To1Wnd:OnPlayerItemChange()
    self.Table:Reposition()
    self.ScrollView:ResetPosition()
    local count = #self.m_TabList
    if count > 0 then
        self:OnSelectButtonClick(self.m_TabList[1].gameObject:GetComponent(typeof(QnSelectableButton)),self.m_composeIdList[1])
    else
        self:InitEmpty()
    end
end
--[[
function LuaItemExchange1To1Wnd:OnUpdateTempPlayTimesWithKey(args)
    local key = args[0]
    if key == EnumPlayTimesKey_lua.eZhuangShiWuExchangeTimes then
        self:UpdateLeftTimeInfo()
    end
end
]]
function LuaItemExchange1To1Wnd:OnEnable()
    g_ScriptEvent:AddListener("SendItem", self, "OnPlayerItemChange")
    --g_ScriptEvent:AddListener("UpdateTempPlayTimesWithKey", self, "OnUpdateTempPlayTimesWithKey")
end

function LuaItemExchange1To1Wnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendItem", self, "OnPlayerItemChange")
    --g_ScriptEvent:RemoveListener("UpdateTempPlayTimesWithKey", self, "OnUpdateTempPlayTimesWithKey")
end
--@region UIEvent

--@endregion UIEvent

