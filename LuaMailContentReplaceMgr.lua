local CMailMgr = import "L10.Game.CMailMgr"
local CChatLinkMgr = import "CChatLinkMgr"

CLuaMailContentReplaceMgr = {}
CLuaMailContentReplaceMgr.m_PatchOpen = false
CLuaMailContentReplaceMgr.m_IsReplace = false

function CLuaMailContentReplaceMgr:LoadAllReplaces()
    self.m_MailContentMapping = {}

    CityReplaceHelper_MailContentReplace.Foreach(function(id, data)
        if data.ReplaceStatus ~= 0 then return end

        if not data.MailTemplateIds then return end

        for i = 0, data.MailTemplateIds.Length - 1 do
            local templateId = data.MailTemplateIds[i]
            local from = data.Content
            local to = data.ReplaceContent
    
            self.m_MailContentMapping[templateId] = self.m_MailContentMapping[templateId] or {
                ReplaceMapping = {},
                RevertMapping = {},
            }
    
            self.m_MailContentMapping[templateId].ReplaceMapping[from] = to
            self.m_MailContentMapping[templateId].RevertMapping[to] = from
        end
    end)
end

function CLuaMailContentReplaceMgr:PatchOpen()
    self.m_PatchOpen = true
    self:LoadAllReplaces()
end

function CLuaMailContentReplaceMgr:StartReplace()
    if not self.m_PatchOpen then return end

    self.m_IsReplace = true
end

function CLuaMailContentReplaceMgr:StartRestore()
    if not self.m_PatchOpen then return end

    self.m_IsReplace = false
end

function CLuaMailContentReplaceMgr:GetReplacedContent(mailId, content)
    if not self.m_PatchOpen then return end

    local mail = CMailMgr.Inst:GetMail(mailId)
    local templateId = mail and mail.TemplateId
    if not self.m_MailContentMapping[templateId] then return end

    local mapping = self.m_IsReplace and self.m_MailContentMapping[templateId].ReplaceMapping or self.m_MailContentMapping[templateId].RevertMapping
    if not mapping then return end

    for from, to in pairs(mapping) do
        local fromNGUI = CChatLinkMgr.TranslateToNGUIText(from):gsub("(%W)","%%%1")
        local toNGUI = CChatLinkMgr.TranslateToNGUIText(to):gsub("(%W)","%%%1")
        content = string.gsub(content, fromNGUI, toNGUI)
    end

    return true, content
end
