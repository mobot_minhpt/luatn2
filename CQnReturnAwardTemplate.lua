-- Auto Generated!!
local CQnReturnAwardTemplate = import "L10.UI.CQnReturnAwardTemplate"
local CUICommonDef = import "L10.UI.CUICommonDef"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CQnReturnAwardTemplate.m_Init_CS2LuaHook = function (this, item, amount) 

    this.item = item
    this.iconTexture:LoadMaterial(item.Icon)
    if this.amountLabel ~= nil and amount > 1 then
        this.amountLabel.text = tostring(amount)
    elseif this.amountLabel ~= nil then
        this.amountLabel.text = ""
    end
    if this.qualitySprite ~= nil then
        this.qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(item, nil, true)
    end
    UIEventListener.Get(this.gameObject).onClick = MakeDelegateFromCSFunction(this.OnItemClick, VoidDelegate, this)
end
