local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local QnTableView = import "L10.UI.QnTableView"
local LuaGameObject=import "LuaGameObject"
local CItemMgr=import "L10.Game.CItemMgr"
local EnumItemPlace=import "L10.Game.EnumItemPlace"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local EquipmentTemplate_Equip=import "L10.Game.EquipmentTemplate_Equip"
local EquipmentTemplate_Type=import "L10.Game.EquipmentTemplate_Type"
local DefaultItemActionDataSource=import "L10.UI.DefaultItemActionDataSource"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local AlignType=import "L10.UI.CItemInfoMgr+AlignType"
local MessageMgr = import "L10.Game.MessageMgr"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CCommonItemSelectCellData = import "L10.UI.CCommonItemSelectCellData"
local CUICommonDef = import "L10.UI.CUICommonDef"

LuaBQPCommitEquipWnd=class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaBQPCommitEquipWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaBQPCommitEquipWnd, "ViewTitleLabel1", "ViewTitleLabel1", UILabel)
RegistChildComponent(LuaBQPCommitEquipWnd, "ViewTitleLabel2", "ViewTitleLabel2", UILabel)
RegistChildComponent(LuaBQPCommitEquipWnd, "RuleBtn", "RuleBtn", GameObject)
RegistChildComponent(LuaBQPCommitEquipWnd, "TableView1", "TableView1", QnTableView)
RegistChildComponent(LuaBQPCommitEquipWnd, "TableView2", "TableView2", QnTableView)
RegistChildComponent(LuaBQPCommitEquipWnd, "HintLabel1", "HintLabel1", UILabel)
RegistChildComponent(LuaBQPCommitEquipWnd, "HintLabel2", "HintLabel2", UILabel)
RegistChildComponent(LuaBQPCommitEquipWnd, "CommitBtn1", "CommitBtn1", GameObject)
RegistChildComponent(LuaBQPCommitEquipWnd, "CommitBtn2", "CommitBtn2", GameObject)

--@endregion RegistChildComponent end

RegistClassMember(LuaBQPCommitEquipWnd,"m_dataSource1")
RegistClassMember(LuaBQPCommitEquipWnd,"m_dataSource2")
RegistClassMember(LuaBQPCommitEquipWnd,"m_Equips1")
RegistClassMember(LuaBQPCommitEquipWnd,"m_Equips2")

RegistClassMember(LuaBQPCommitEquipWnd,"m_CommitType")
RegistClassMember(LuaBQPCommitEquipWnd,"m_EquipTypeNames")

function LuaBQPCommitEquipWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.RuleBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRuleBtnClick(go)
	end)

	UIEventListener.Get(self.CommitBtn1.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCommitBtn1Click(go)
	end)

	UIEventListener.Get(self.CommitBtn2.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCommitBtn2Click(go)
	end)

    --@endregion EventBind end
end

function LuaBQPCommitEquipWnd:Init()
	self.m_Equips1 = {}
	self.m_Equips2 = {}
	self.m_EquipTypeNames = {}
	self.m_EquipTypeNames[1] = LocalString.GetString("武器")
	BingQiPu_EquipType.Foreach(function (key,value)
		if value.FstTab ~= 1 then
			self.m_EquipTypeNames[value.FstTab] = value.SecTabName
		end
	end)

	self:InitBtns()

	self.HintLabel1.gameObject:SetActive(false)
	self.HintLabel2.gameObject:SetActive(false)

	local function InitItem1(item,index)
        self:InitItem1(item,index)
    end
    self.m_dataSource1=DefaultTableViewDataSource.CreateByCount(0,InitItem1)
    self.TableView1.m_DataSource=self.m_dataSource1
    self.TableView1.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow1(row)
    end)

	local function InitItem2(item,index)
    	self:InitItem2(item, index)
    end
    self.m_dataSource2=DefaultTableViewDataSource.CreateByCount(0,InitItem2)
    self.TableView2.m_DataSource=self.m_dataSource2
    self.TableView2.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        self:OnSelectAtRow2(row)
    end)
	LuaBingQiPuMgr.QueryBQPStage()
	LuaBingQiPuMgr.QueryBQPSubmitted()
end

function LuaBQPCommitEquipWnd:InitBtns()
	if LuaBingQiPuMgr.m_StageStatus == EnumBingQiPuStage.eSubmit then
		self.TitleLabel.text = LocalString.GetString("登记兵器")
		self.ViewTitleLabel1.text = LocalString.GetString("十大兵器榜已登记兵器")
		self.ViewTitleLabel2.text = LocalString.GetString("奇珍榜已登记兵器")
		self.CommitBtn1:SetActive(true)
		self.CommitBtn2:SetActive(true)
	else
		self.TitleLabel.text = LocalString.GetString("我的参选")
		self.ViewTitleLabel1.text = LocalString.GetString("十大兵器榜参选兵器")
		self.ViewTitleLabel2.text = LocalString.GetString("奇珍榜参选兵器")
		self.CommitBtn1:SetActive(false)
		self.CommitBtn2:SetActive(false)
	end
end

function LuaBQPCommitEquipWnd:InitBtn2()
	local btn2lb = LuaGameObject.GetChildNoGC(self.CommitBtn2.transform,"Label").label
	if self.m_Equips2 and #self.m_Equips2 > 0 then
		btn2lb.text = LocalString.GetString("已登记")
		CUICommonDef.SetActive(self.CommitBtn2,false,true)
	else
		btn2lb.text = LocalString.GetString("登记奇珍兵器")
		CUICommonDef.SetActive(self.CommitBtn2,true,true)
	end
end

-- 初始化已经提交的装备1
function LuaBQPCommitEquipWnd:InitItem1(go,index)
	local item = self.m_Equips1[index+1]
	self:InitItem(go, item)
end

-- 初始化已经提交的装备2
function LuaBQPCommitEquipWnd:InitItem2(go,index)
	local item = self.m_Equips2[index+1]
	self:InitItem(go, item)
end

function LuaBQPCommitEquipWnd:InitItem(go, data)
	local trans = go.transform
	local tf= LuaGameObject.GetChildNoGC(trans,"ItemCell").transform
	local iconTexture = LuaGameObject.GetChildNoGC(tf,"IconTexture").cTexture
	local qualitySprite = LuaGameObject.GetChildNoGC(tf,"QualitySprite").sprite
	local disableSprite = LuaGameObject.GetChildNoGC(tf,"DisableSprite").sprite
	local bindSprite = LuaGameObject.GetChildNoGC(tf,"BindSprite").sprite
	local amountLabel = LuaGameObject.GetChildNoGC(tf,"AmountLabel").label
	local textLabel = LuaGameObject.GetChildNoGC(tf,"TextLabel").label

	local lb1 = LuaGameObject.GetChildNoGC(trans,"Label1").label
	local lb2 = LuaGameObject.GetChildNoGC(trans,"Label2").label

	iconTexture:Clear()
	qualitySprite.spriteName = CUICommonDef.GetItemCellBorder()
	disableSprite.gameObject:SetActive(false)
	amountLabel.text = ""
	textLabel.text = ""

	if not data then return end
	local item = data.Item
	if not item then return end

	bindSprite.spriteName = item.BindOrEquipCornerMark
	iconTexture:LoadMaterial(item.Icon)
	qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(item.Equip.QualityType)
	textLabel.text = item.Equip.LostSoulOrLostDurationText
	disableSprite.enabled = (not item.MainPlayerIsFit)

	if data.Rank > 0 then
		local fmt = LocalString.GetString("%s榜 第[00FF00]%d[-]名")
		local cfg  = EquipmentTemplate_Equip.GetData(item.TemplateId)
		if data.Precious then
			lb1.text = SafeStringFormat3(fmt, LocalString.GetString("奇珍"), data.Rank)
		else
			local ename = self.m_EquipTypeNames[cfg.Type]
			lb1.text = SafeStringFormat3(fmt, ename, data.Rank)
		end
	else
		lb1.text = LocalString.GetString("该装备不在榜单")
	end
	lb2.text = SafeStringFormat3(LocalString.GetString("当前人气%d"),data.Score)
end

function LuaBQPCommitEquipWnd:OnSelectAtRow1(row)
	self:OnItemSelect(1,row)
end

function LuaBQPCommitEquipWnd:OnSelectAtRow2(row)
	self:OnItemSelect(2,row)
end

function LuaBQPCommitEquipWnd:OnItemSelect(etype,row)
	local info = nil
	local align = AlignType.ScreenLeft
	if etype == 1 then
		info = self.m_Equips1[row+1].Item
		align = AlignType.ScreenRight
	else
		info = self.m_Equips2[row+1].Item
	end
	
	local t_name={}
	t_name[1]=LocalString.GetString("详情") 
	t_name[2]=LocalString.GetString("推广") 
	t_name[3]=LocalString.GetString("赞助") 
	t_name[4]=LocalString.GetString("下榜")
	local t_action={}
	t_action[1] = function ()
		Gac2Gas.QueryBQPEquipDetails(info.Id)
	end
	t_action[2] = function ()
		local item = CItemMgr.Inst:GetById(info.Id)
		if not item then
			g_MessageMgr:ShowMessage("CUSTOM_STRING2",LocalString.GetString("请确保装备在背包中或者身上"))
			return
		end
		Gac2Gas.BQPQueryShareCost(info.Id)
	end
	t_action[3] = function ()
		local item = CItemMgr.Inst:GetById(info.Id)
		if not item then
			g_MessageMgr:ShowMessage("CUSTOM_STRING2",LocalString.GetString("请确保装备在背包中或者身上"))
			return
		end
		LuaBingQiPuMgr.m_SelectSponsorItem = {ID = info.Id,CfgID = info.TemplateId}
		CUIManager.ShowUI(CLuaUIResources.BQPMySponsorWnd)
	end
	t_action[4] = function ()
		local msg = MessageMgr.Inst:FormatMessage("BQP_XIABANG_CONFIRM", {})
		local xiajiaAction = DelegateFactory.Action(function() 
			Gac2Gas.BingQiPuOffShelf(info.Id) 
		end)
		MessageWndManager.ShowOKCancelMessage(msg, xiajiaAction, nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
	end
    local actionSource = DefaultItemActionDataSource.Create(4,t_action,t_name)
    CItemInfoMgr.ShowLinkItemInfo(info.Id, false, actionSource, align, 0, 0, 0, 0)
end

function LuaBQPCommitEquipWnd:OnEnable()
	g_ScriptEvent:AddListener("QueryBQPForSubmitResult", self, "UpdateEquipInfos")
	g_ScriptEvent:AddListener("UpdateBQPStatus",self, "UpdateBQPStatus")
end

function LuaBQPCommitEquipWnd:OnDisable()
	g_ScriptEvent:RemoveListener("QueryBQPForSubmitResult", self, "UpdateEquipInfos")
	g_ScriptEvent:RemoveListener("UpdateBQPStatus",self, "UpdateBQPStatus")
	CUIManager.CloseUI(CLuaUIResources.CommonItemSelectWnd)
end

function LuaBQPCommitEquipWnd:UpdateBQPStatus()
	self:InitBtns()
end

function LuaBQPCommitEquipWnd:UpdateEquipInfos()
    self:UpdateEquipsCommited()
	self:InitBtn2()
end

-- 更新已提交的装备列表
function LuaBQPCommitEquipWnd:UpdateEquipsCommited()
	self.m_Equips1 = {}
	self.m_Equips2 = {}
	for i=1,#LuaBingQiPuMgr.m_EquipsCommited do
		local data = LuaBingQiPuMgr.m_EquipsCommited[i]
		if data.Precious then 
			table.insert(self.m_Equips2,data)
		else
			table.insert(self.m_Equips1,data)
		end
	end

	self.HintLabel1.gameObject:SetActive(#self.m_Equips1 <= 0)
	self.m_dataSource1.count=#self.m_Equips1
	self.TableView1:ReloadData(true,false)

	self.HintLabel2.gameObject:SetActive(#self.m_Equips2 <= 0)
	self.m_dataSource2.count=#self.m_Equips2
	self.TableView2:ReloadData(true,false)
end

--@region 可提交装备界面

--获取可提交的装备列表
function LuaBQPCommitEquipWnd:InitData_ToCommit()
	local datas = {}
	local equipListOnBody = CItemMgr.Inst:GetPlayerEquipmentListAtPlace(EnumItemPlace.Body)
	local equipListInBag = CItemMgr.Inst:GetPlayerEquipmentListAtPlace(EnumItemPlace.Bag)
	for i=0, equipListOnBody.Count-1 do
		if self:CanCommit(equipListOnBody[i].itemId) then
			table.insert(datas, equipListOnBody[i])
		end
	end
	for i=0, equipListInBag.Count-1 do
		if self:CanCommit(equipListInBag[i].itemId) then
			table.insert(datas, equipListInBag[i])
		end
	end
	table.sort(datas,function(a,b)
		local item = CItemMgr.Inst:GetById(a.itemId)
		local item2 = CItemMgr.Inst:GetById(b.itemId)
		return item.Equip.Score > item2.Equip.Score
	end)
	return datas
end

-- 装备是否是可提交的类型
function LuaBQPCommitEquipWnd:IsNeededEquip(templateId)
	local equipTemplate = EquipmentTemplate_Equip.GetData(templateId)
	local equipType = EquipmentTemplate_Type.GetData(equipTemplate.Type)
	if equipTemplate.Type < 17 and equipType.EnableSearch > 0 then
		return true
	end
	return false
end

-- 装备或其类型是否已经提交
function LuaBQPCommitEquipWnd:IsCommittedEquipOrType(itemId,equipType)
	for k, v in ipairs(LuaBingQiPuMgr.m_EquipsCommited) do
		if v.Item.Id == itemId then
			return true
		end
	end

	for k, v in ipairs(LuaBingQiPuMgr.m_EquipTypesCommited) do
		if v == equipType then
			return true
		end
	end
	return false
end

function LuaBQPCommitEquipWnd:CanCommit(itemId)
	local item = CItemMgr.Inst:GetById(itemId)
	if not item.IsEquip then return false end
	if not item.IsBinded then return false end

	local qtype = EnumToInt(item.Equip.QualityType)
	if qtype <= 4 or qtype == 12 then 
		return false 
	end --过滤蓝色、蓝色品质以下、深蓝的装备
	
	local templateId = item.TemplateId
	local equipTemplate = EquipmentTemplate_Equip.GetData(templateId)
	local equipType = equipTemplate.Type
	if equipType >= 17 then return false end--过滤不能提交的东西

	local etypedata = EquipmentTemplate_Type.GetData(equipType)
	if etypedata.EnableSearch <= 0 then--过滤不能提交的东西
		return false
	end

	if self:IsCommittedEquipOrType(itemId,equipType) then--过滤已经提交的装备或装备类型
		return false
	end 

	if self.m_CommitType == 0 then--准备登记十大兵器
		local threshold = tonumber(LuaBingQiPuMgr.m_EquipCommitThreshold[tonumber(equipType)])
		if not threshold then return false end 
		return item.Equip.Score > threshold
	else--登记奇珍榜
		local threshold = LuaBingQiPuMgr.m_PreciousRankThreshold
		return self:CheckWord(itemId)
	end
	return false
end

function LuaBQPCommitEquipWnd:CheckWord(itemId)
	local item = CItemMgr.Inst:GetById(itemId)
	if not item or not item.Equip then return false end
	local itemWordInfo = item.Equip.WordInfo
	if not itemWordInfo then return false end
	local das = {}
	local ids = {251009,251012,251025,
				251120,251121,251123,251124,251125,251126,251127,251128,
				251185,251187,251189,251191,251196,251198}
	for i = 0, itemWordInfo.Count - 1, 1 do
		local base = math.floor(itemWordInfo[i].Id / 100)
		local level = math.floor(itemWordInfo[i].Id % 100)
		
		if base > 252100 then return true end --id 252100以上的词条，出现即可上榜

		for i=1,#ids do --id在ids集合中的词条，出现即可上榜
			if ids[i] == base then
				return true
			end
		end

		if das[base] == nil then 
			das[base] = 1
		else
			das[base] = das[base] + 1
			if das[base] > 1 then--任何词条出现2次即可上榜 
				return true
			end
		end
	end
	return false
end

--@endregion

function LuaBQPCommitEquipWnd:ShowSelectItems(etype)
	LuaCommonItemSelectMgr.CloseSelectWnd()
	self.m_CommitType = etype
	local equips = self:InitData_ToCommit()
	
	if equips==nil or #equips <= 0 then 
		g_MessageMgr:ShowMessage("BQP_COMMIT_NOEQUIP")
		return
	end

	local title = LocalString.GetString("选择要登记的兵器")
	local menu = LocalString.GetString("登记")
	local initfunc = function()
		local datas = CreateFromClass(MakeGenericClass(List, CCommonItemSelectCellData))
		for k, v in ipairs(equips) do
			local data = CreateFromClass(CCommonItemSelectCellData, v.itemId, v.templateId, true, 1)
			CommonDefs.ListAdd(datas,typeof(CCommonItemSelectCellData),data)
		end
		return datas
	end

	local selectfunc = function(itemId, templateId)
		self:OnSelectEquip(itemId, templateId)
	end
	LuaCommonItemSelectMgr.ShowSelectWnd(title,menu,initfunc,selectfunc)
end

function LuaBQPCommitEquipWnd:OnSelectEquip(itemId, templateId)
	LuaBingQiPuMgr.ShowCommitComfirmWnd(itemId,templateId,self.m_CommitType)
end

--@region UIEvent

function LuaBQPCommitEquipWnd:OnRuleBtnClick(go)
	MessageMgr.Inst:ShowMessage("BQP_COMMIT_DETAILS", {})
end

function LuaBQPCommitEquipWnd:OnCommitBtn1Click(go)
	self:ShowSelectItems(0)
end

function LuaBQPCommitEquipWnd:OnCommitBtn2Click(go)
	self:ShowSelectItems(1)
end

--@endregion UIEvent
