require("common/common_include")

local Gac2Gas = import "L10.Game.Gac2Gas"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CUIManager = import "L10.UI.CUIManager"
local MessageWndManager = import "L10.UI.MessageWndManager"

LuaWorldEventShareWnd = class()

RegistClassMember(LuaWorldEventShareWnd,"ShareBtn")
RegistClassMember(LuaWorldEventShareWnd,"ShareText1")
RegistClassMember(LuaWorldEventShareWnd,"ShareText2")
RegistClassMember(LuaWorldEventShareWnd,"CloseBtn")

function LuaWorldEventShareWnd:Init()
	local onShareClick = function(go)
    --MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("CUSTOM_STRING2",LocalString.GetString("确认退出吗?")), DelegateFactory.Action(function ()
    --  CUIManager.CloseUI(CUIRes.WorldEventDrawLineWnd)
    --end), nil, nil, nil, false)
		Gac2Gas.RequestShareLanRuoSiToWorld()
		--CUIManager.CloseUI(CLuaUIResources.WorldEventShareWnd)
	end
	CommonDefs.AddOnClickListener(self.ShareBtn,DelegateFactory.Action_GameObject(onShareClick),false)
  self.ShareText1.text = g_MessageMgr:FormatMessage("LanRuoShareAwardIntro")
  self.ShareText2.text = g_MessageMgr:FormatMessage("LanRuoShareIntro")

	local onCloseClick = function(go)
		MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("WorldEvent_Share_Close"), DelegateFactory.Action(function ()
			CUIManager.CloseUI(CLuaUIResources.WorldEventShareWnd)
		end), nil, nil, nil, false)
	end
	CommonDefs.AddOnClickListener(self.CloseBtn,DelegateFactory.Action_GameObject(onCloseClick),false)
end

return LuaWorldEventShareWnd
