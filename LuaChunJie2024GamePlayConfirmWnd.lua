local CGamePlayConfirmMemberItem = import "L10.UI.CGamePlayConfirmMemberItem"
local CTeamMgr                   = import "L10.Game.CTeamMgr"

LuaChunJie2024GamePlayConfirmWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChunJie2024GamePlayConfirmWnd, "memberItem", "MemberItem", CGamePlayConfirmMemberItem)
RegistChildComponent(LuaChunJie2024GamePlayConfirmWnd, "table", "Table", UIGrid)
RegistChildComponent(LuaChunJie2024GamePlayConfirmWnd, "confirmHintLabel", "ConfirmHintLabel", UILabel)
RegistChildComponent(LuaChunJie2024GamePlayConfirmWnd, "progressBar", "ProgressBar", UISlider)
RegistChildComponent(LuaChunJie2024GamePlayConfirmWnd, "cancelBtn", "CancelBtn", GameObject)
RegistChildComponent(LuaChunJie2024GamePlayConfirmWnd, "confirmBtn", "ConfirmBtn", GameObject)
RegistChildComponent(LuaChunJie2024GamePlayConfirmWnd, "waitForOthersLabel", "WaitForOthersLabel", GameObject)
RegistChildComponent(LuaChunJie2024GamePlayConfirmWnd, "noDoubleReward", "NoDoubleReward", GameObject)
RegistChildComponent(LuaChunJie2024GamePlayConfirmWnd, "titleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaChunJie2024GamePlayConfirmWnd, "foreground", "Foreground", UISprite)
RegistChildComponent(LuaChunJie2024GamePlayConfirmWnd, "leftTimeLabel", "LeftTimeLabel", UILabel)
--@endregion RegistChildComponent end

RegistClassMember(LuaChunJie2024GamePlayConfirmWnd, "tick")
RegistClassMember(LuaChunJie2024GamePlayConfirmWnd, "startTime")
RegistClassMember(LuaChunJie2024GamePlayConfirmWnd, "confirmDict")

function LuaChunJie2024GamePlayConfirmWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.cancelBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCancelBtnClick()
	end)

	UIEventListener.Get(self.confirmBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnConfirmBtnClick()
	end)
    --@endregion EventBind end

	self.memberItem.gameObject:SetActive(false)
end

function LuaChunJie2024GamePlayConfirmWnd:OnEnable()
	g_ScriptEvent:AddListener("TeamInfoChange", self, "OnTeamInfoChange")
	g_ScriptEvent:AddListener("SyncEnterGameplayConfirmInfo", self, "OnSyncEnterGameplayConfirmInfo")
	if not System.String.IsNullOrEmpty(LuaChunJie2024Mgr.gameplayInfo.gamePlayType) then
        Gac2Gas.QueryTeamConfirmWaitInfo()
    end
end

function LuaChunJie2024GamePlayConfirmWnd:OnDisable()
	g_ScriptEvent:RemoveListener("TeamInfoChange", self, "OnTeamInfoChange")
	g_ScriptEvent:RemoveListener("SyncEnterGameplayConfirmInfo", self, "OnSyncEnterGameplayConfirmInfo")
end

function LuaChunJie2024GamePlayConfirmWnd:OnTeamInfoChange()
	self:UpdateMembers()
end

function LuaChunJie2024GamePlayConfirmWnd:OnSyncEnterGameplayConfirmInfo(confirmInfo)
	self.confirmDict = {}
	CommonDefs.DictIterate(confirmInfo, DelegateFactory.Action_object_object(function (key, value)
		self.confirmDict[tonumber(key)] = value
	end))
	self:UpdateMembers()
end


function LuaChunJie2024GamePlayConfirmWnd:Init()
	self:ClearTick()

	local info = LuaChunJie2024Mgr.gameplayInfo
	local playId = info.playId
	local gameplayData = Gameplay_Gameplay.GetData(playId)
	self.titleLabel.text = gameplayData.Name

	self.waitForOthersLabel:SetActive(false)

	self.startTime = Time.realtimeSinceStartup
	local timeout = info.timeout
	self.progressBar.gameObject:SetActive(timeout > 0)
	if timeout > 0 then
		self.progressBar.value = 1
		self.leftTimeLabel.text = SafeStringFormat3(LocalString.GetString("[ffed5f]%d[-]秒"), math.floor(timeout))
		self.tick = RegisterTick(function()
			local timeout1 = LuaChunJie2024Mgr.gameplayInfo.timeout
			if timeout1 <= 0 then return end

			local leftTime = timeout1 - (Time.realtimeSinceStartup - self.startTime)
			if leftTime <= 0 then
				CUIManager.CloseUI(CLuaUIResources.ChunJie2024GamePlayConfirmWnd)
				return
			end
			self.progressBar.value = leftTime / timeout1
			local floorLeftTime = math.floor(leftTime)
			if floorLeftTime <= 3 then
				self.foreground.spriteName = "common_mainplayerwnd_red"
			end
			local color = floorLeftTime <= 3 and "ff5050" or "ffed5f"
			self.leftTimeLabel.text = SafeStringFormat3(LocalString.GetString("[%s]%d[-]秒"), color, floorLeftTime)
		end, 20)
	end

	self:InitMemberCells()
	self:UpdateMembers()
end

function LuaChunJie2024GamePlayConfirmWnd:InitMemberCells()
	Extensions.RemoveAllChildren(self.table.transform)
	for i = 1, 5 do
		local go = CUICommonDef.AddChild(self.table.gameObject, self.memberItem.gameObject)
		go.transform:Find("RewardIcon").gameObject:SetActive(false)
		go:SetActive(true)
		go.transform:GetComponent(typeof(CGamePlayConfirmMemberItem)):Init(nil, false, nil, nil, nil, nil, false)
	end
	self.table:Reposition()
end

function LuaChunJie2024GamePlayConfirmWnd:UpdateMembers()
	local rewardTimesDict = LuaChunJie2024Mgr.gameplayInfo.rewardTimesDict
    local hasRewardMemberCount = 0
	local memberCnt = 0
	local confirmMemberCnt = 0
    if CTeamMgr.Inst:TeamExists() then
        local members = CTeamMgr.Inst.Members
        memberCnt = members.Count
        for i = 1, 5 do
			local item = self.table.transform:GetChild(i - 1):GetComponent(typeof(CGamePlayConfirmMemberItem))
			if i <= memberCnt then
				local member = members[i - 1]
				local confirm = false
				if self.confirmDict and self.confirmDict[member.m_MemberId] then
					confirm = true
				end
				local portraitName = CUICommonDef.GetPortraitName(member.m_MemberClass, member.m_MemberGender, member.m_Expression)
				local expressionTxtPath = CUICommonDef.GetExpressionTxtPath(member.m_ExpressionTxt)
				local framePath = CUICommonDef.GetProfileFramePath(member.BasicProp.ProfileInfo.Frame)
				item:Init(member.m_MemberName, confirm, "", portraitName, expressionTxtPath, framePath, CTeamMgr.Inst:IsTeamLeader(member.m_MemberId))

				local rewardIcon = item.transform:Find("RewardIcon").gameObject
				local rewardTimes = rewardTimesDict[member.m_MemberId]
				if rewardTimes and rewardTimes > 0 then
					rewardIcon:SetActive(true)
					hasRewardMemberCount = hasRewardMemberCount + 1
				else
					rewardIcon:SetActive(false)
				end
			else
				item:Init(nil, false, nil, nil, nil, nil, false)
			end
        end
    end

	self.noDoubleReward:SetActive(hasRewardMemberCount < 3)
	self.confirmHintLabel.text = SafeStringFormat3(LocalString.GetString("已确认%d/%d"), confirmMemberCnt, memberCnt)
	local mainPlayer = CClientMainPlayer.Inst
	local mainplayerConfirm = false
	if self.confirmDict and mainPlayer and self.confirmDict[mainPlayer.Id] then
		mainplayerConfirm = true
	end
	self.confirmBtn:SetActive(not mainplayerConfirm)
	self.cancelBtn:SetActive(not mainplayerConfirm)
	self.waitForOthersLabel:SetActive(mainplayerConfirm)
end


function LuaChunJie2024GamePlayConfirmWnd:ClearTick()
	if self.tick then
		UnRegisterTick(self.tick)
		self.tick = nil
	end
end

function LuaChunJie2024GamePlayConfirmWnd:OnDestroy()
	self:ClearTick()
end

--@region UIEvent

function LuaChunJie2024GamePlayConfirmWnd:OnCancelBtnClick()
	Gac2Gas.SendGamePlayConfirmMessageResult(LuaChunJie2024Mgr.gameplayInfo.sessionId, 0, g_MessagePack.pack({}))
	CUIManager.CloseUI(CLuaUIResources.ChunJie2024GamePlayConfirmWnd)
end

function LuaChunJie2024GamePlayConfirmWnd:OnConfirmBtnClick()
	Gac2Gas.SendGamePlayConfirmMessageResult(LuaChunJie2024Mgr.gameplayInfo.sessionId, 1, g_MessagePack.pack({}))
end

--@endregion UIEvent
