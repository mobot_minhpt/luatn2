require("common/common_include")

local UIGrid = import "UIGrid"
local Vector3 = import "UnityEngine.Vector3"
local CUITexture = import "L10.UI.CUITexture"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local CMainCamera = import "L10.Engine.CMainCamera"
local CUIFx = import "L10.UI.CUIFx"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local Ease = import "DG.Tweening.Ease"

LuaWaKuangStateWnd = class()

RegistChildComponent(LuaWaKuangStateWnd, "ExpandButton", GameObject)
RegistChildComponent(LuaWaKuangStateWnd, "StoneGrid", UIGrid)
RegistChildComponent(LuaWaKuangStateWnd, "StoneTemplate", GameObject)
RegistChildComponent(LuaWaKuangStateWnd, "AddStoneFx", CUIFx)

-- 维护一份不同矿石对应的上限、数量
RegistClassMember(LuaWaKuangStateWnd, "PackageSize")
RegistClassMember(LuaWaKuangStateWnd, "GridCapacity")
RegistClassMember(LuaWaKuangStateWnd, "GridInfos")
RegistClassMember(LuaWaKuangStateWnd, 'StoneItems')

function LuaWaKuangStateWnd:Init()
	self.StoneTemplate:SetActive(false)
	self.AddStoneFx:DestroyFx()

	CommonDefs.AddOnClickListener(self.ExpandButton, DelegateFactory.Action_GameObject(function (go)
		self:OnExpandButtonClicked(go)
	end), false)

	self:Refresh()
	self.StoneItems = {}

	self:UpdateStones()
end


function LuaWaKuangStateWnd:Refresh()
	local setting = WuYi_WaKuangSetting.GetData()
	if LuaWuYiMgr.m_PackageSize == 0 then
		self.PackageSize = setting.InitPackageSize
	else
		self.PackageSize = LuaWuYiMgr.m_PackageSize
	end
	
	if LuaWuYiMgr.m_GridCapacity == 0 then
		self.GridCapacity = setting.InitGridCapacity
	else
		self.GridCapacity = LuaWuYiMgr.m_GridCapacity
	end
	
	if not LuaWuYiMgr.m_GridInfos then
		self.GridInfos = {}
		for i = 1, self.PackageSize do
			table.insert(self.GridInfos, {
				monsterId = 0,
				count = 0,
			})
		end
	else
		self.GridInfos = {}
		for i = 1, #LuaWuYiMgr.m_GridInfos do
			local monsterId = LuaWuYiMgr.m_GridInfos[i].monsterId
			local count =  LuaWuYiMgr.m_GridInfos[i].count
			table.insert(self.GridInfos, {
				monsterId = monsterId,
				count = count,
			})
		end
	end

	self:UpdateStones()
end

function LuaWaKuangStateWnd:UpdateStones()
	CUICommonDef.ClearTransform(self.StoneGrid.transform)
	self.StoneItems = {}

	for i = 1, self.PackageSize do
		local item = NGUITools.AddChild(self.StoneGrid.gameObject, self.StoneTemplate)
		self:InitStoneTemplate(item, self.GridInfos[i], i)
		table.insert(self.StoneItems, item)
		item:SetActive(true)
	end

	self.StoneGrid:Reposition()
end

function LuaWaKuangStateWnd:InitStoneTemplate(go, info, index)

	if not info then return end

	local BG = go.transform:Find("BG").gameObject
	local CapacityLabel = go.transform:Find("CapacityLabel"):GetComponent(typeof(UILabel))
	local StoneTexture = go.transform:Find("StoneTexture"):GetComponent(typeof(CUITexture))
	local CountLabel = go.transform:Find("CountLabel"):GetComponent(typeof(UILabel))

	CapacityLabel.text = tostring(self.GridCapacity)
	if info.monsterId == 0 then
		CountLabel.text = nil
		StoneTexture:Clear()

		CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(function (gameObject)
			
		end), false)
	else
		local kuangShi = WuYi_KuangShi.GetData(info.monsterId)
		CountLabel.text = tostring(info.count)
		StoneTexture:LoadMaterial(kuangShi.Icon)

		CommonDefs.AddOnClickListener(go, DelegateFactory.Action_GameObject(function (gameObject)
			self:OnItemClicked(gameObject, index, info.monsterId)
		end), false)
	end

	
end

function LuaWaKuangStateWnd:OnItemClicked(go, index, monsterId)
	local menuList = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
	CommonDefs.ListAdd(menuList, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("丢弃"), DelegateFactory.Action_int(function (idx) 
        Gac2Gas.RequestDropKuangShiFromPackage(index, monsterId)
    end), false, nil, EnumPopupMenuItemStyle.Orange))
	CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(menuList), go.transform, CPopupMenuInfoMgr.AlignType.Bottom, 1, nil, 350, true, 296)

end

function LuaWaKuangStateWnd:OnExpandButtonClicked(go)
	local rotation = self.ExpandButton.transform.localEulerAngles
    self.ExpandButton.transform.localEulerAngles = Vector3(rotation.x, rotation.y, 180)

    CUIManager.ShowUI(CUIResources.TopAndRightTipWnd)
end


function LuaWaKuangStateWnd:ShowAddStoneFx(engineId, fromX, fromY, idx, itemId, count)

	local screenPos = CMainCamera.Main:WorldToScreenPoint(Vector3(fromX, 0, fromY))
	screenPos.z = 0
	local fromNGUIworldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(screenPos)
	-- 相对于add特效的矿石位置
	local fromNGUIrelativePos = self.AddStoneFx.transform:InverseTransformPoint(fromNGUIworldPos)

	local viewPos = CMainCamera.Main:WorldToViewportPoint(Vector3(fromX, 0, fromY))
	local fromWorldPos = CUIManager.UIMainCamera:ScreenToWorldPoint(viewPos)

	local targetPos = self.StoneItems[idx].transform.localPosition
	local v1 = self.StoneItems[idx].transform:TransformPoint(fromWorldPos)
	local targetLocalPos = self.AddStoneFx.transform:InverseTransformPoint(v1)
	
	LuaTweenUtils.DOKill(self.AddStoneFx.FxRoot, false)
	local path = {fromNGUIrelativePos, targetLocalPos}
	local array = Table2Array(path, MakeArrayClass(Vector3))
	LuaUtils.SetLocalPosition(self.AddStoneFx.FxRoot,fromNGUIrelativePos.x, fromNGUIrelativePos.y, fromNGUIrelativePos.z)

    LuaTweenUtils.DOLocalPathOnce(self.AddStoneFx.FxRoot, array, 1, Ease.Linear)

	self.AddStoneFx:DestroyFx()
	self.AddStoneFx:LoadFx(CUIFxPaths.CommonYellowLineFxPath)

	-- 更新
	self.GridInfos[idx].monsterId = itemId
	self.GridInfos[idx].count = count
	self:InitStoneTemplate(self.StoneItems[idx], self.GridInfos[idx], idx)
end

function LuaWaKuangStateWnd:ShowRemovwStone(idx, itemId)
	self.GridInfos[idx].monsterId = 0
	self.GridInfos[idx].count = 0
	self:InitStoneTemplate(self.StoneItems[idx], self.GridInfos[idx], idx)
end


function LuaWaKuangStateWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateWaKuangState", self, "Refresh")
	g_ScriptEvent:AddListener("WaKuangAddStone", self, "ShowAddStoneFx")
	g_ScriptEvent:AddListener("WaKuangRemoveStone", self, "ShowRemovwStone")
end

function LuaWaKuangStateWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateWaKuangState", self, "Refresh")
	g_ScriptEvent:RemoveListener("WaKuangAddStone", self, "ShowAddStoneFx")
	g_ScriptEvent:RemoveListener("WaKuangRemoveStone", self, "ShowRemovwStone")
end

return LuaWaKuangStateWnd