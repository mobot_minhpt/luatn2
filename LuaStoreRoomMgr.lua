
LuaStoreRoomMgr = {}

function LuaStoreRoomMgr:GetTotalValidStoreRoomPosCount()
    if not CClientMainPlayer.Inst then
        return 0
    end

    local repoProp = CClientMainPlayer.Inst.RepertoryProp
    local total =  repoProp.ItemUnlockedRoomPosCount + 
        repoProp.SilverUnlockedRoomPosCount + 
        repoProp.JadeUnlockedRoomPosCount

    local setting = StoreRoom_Setting.GetData()
    --飞升解锁5格
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.HasFeiSheng then
        total = total + setting.FeiShengUnlock
    end
    --大月卡（特权月卡）解锁5格
    if LuaWelfareMgr.IsOpenBigMonthCard then
        local info = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.BigMonthCardInfo or nil
        local bigLeftDay = LuaWelfareMgr:MonthCardLeftDay(info)
        if bigLeftDay and bigLeftDay > 0 then
            total = total + setting.MonthCardUnlock
        end
    end
    local maxStoreRoomSize = setting.StoreRoomPageCount * setting.RoomPosCountPerPage
    return math.min(total, maxStoreRoomSize)
end

function LuaStoreRoomMgr:GetLockedButHasItemPosCount()
    local setting = StoreRoom_Setting.GetData()
    local unlockedCount = LuaStoreRoomMgr:GetTotalValidStoreRoomPosCount()
    local count =0
    local templateCount = setting.MonthCardUnlock--暂时只有大月卡解锁的格子可能是临时的
    if CClientMainPlayer.Inst then
        local repoProp = CClientMainPlayer.Inst.RepertoryProp
        for i=1,templateCount,1 do
            local pos = unlockedCount + i
            local itemId = repoProp.StoreRoom[pos]
            if itemId and itemId ~= "" then
                count = count + 1
            end
        end
    end
    return count
end

--府库里最后面一个放有东西的格子
function LuaStoreRoomMgr:GetLastUsedPos()
    local pos = 0
    if CClientMainPlayer.Inst then
        local repoProp = CClientMainPlayer.Inst.RepertoryProp
        for i=repoProp.StoreRoom.Length-1,1,-1 do
            local itemId = repoProp.StoreRoom[i]
            if itemId and itemId ~= "" then
                return i
            end
        end
    end
    return pos
end

function LuaStoreRoomMgr:GetUsedCount()
    local count = 0
    if CClientMainPlayer.Inst then
        local repoProp = CClientMainPlayer.Inst.RepertoryProp
        for i=1,repoProp.StoreRoom.Length-1,1 do
            if repoProp.StoreRoom[i] and repoProp.StoreRoom[i]~="" then
                count = count + 1
            end
        end
    end
    return count
end

function LuaStoreRoomMgr:OpenUnlockPosWnd()
    CUIManager.ShowUI(CLuaUIResources.StoreRoomPosUnlockWnd)
end

function LuaStoreRoomMgr:SyncStoreRoomPosCount(itemUnlockCount, silverUnlockCount, jadeUnlockCount)
    local repoProp = CClientMainPlayer.Inst.RepertoryProp

    repoProp.ItemUnlockedRoomPosCount = itemUnlockCount
    repoProp.SilverUnlockedRoomPosCount = silverUnlockCount
    repoProp.JadeUnlockedRoomPosCount = jadeUnlockCount

    g_ScriptEvent:BroadcastInLua("SyncStoreRoomPosCount", itemUnlockCount, silverUnlockCount, jadeUnlockCount)
end

function LuaStoreRoomMgr:OnOpenStoreRoomWnd(isValidZuoQi, zuoqiTemplateId)
    LuaStoreRoomMgr.mIsValidZuoQi = isValidZuoQi
    LuaStoreRoomMgr.mZuoqiTemplateId = zuoqiTemplateId
    CUIManager.CloseUI(CUIResources.WarehouseWnd)
    CUIManager.ShowUI(CLuaUIResources.StoreRoomWnd)
end

function LuaStoreRoomMgr:OpenConstructWnd()
    CUIManager.ShowUI(CLuaUIResources.StoreRoomConstructWnd)
end