local CItemMgr = import "L10.Game.CItemMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local QnMoneyCostMesssageMgr = import "L10.UI.QnMoneyCostMesssageMgr"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"

LuaChangeRuMenBiaoZhunWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChangeRuMenBiaoZhunWnd, "LeftGrid", "LeftGrid", UIGrid)
RegistChildComponent(LuaChangeRuMenBiaoZhunWnd, "RightGrid", "RightGrid", UIGrid)
RegistChildComponent(LuaChangeRuMenBiaoZhunWnd, "ConditionTemplate", "ConditionTemplate", GameObject)
RegistChildComponent(LuaChangeRuMenBiaoZhunWnd, "OkButton", "OkButton", GameObject)
RegistChildComponent(LuaChangeRuMenBiaoZhunWnd, "LastTimeLabel", "LastTimeLabel", UILabel)
RegistChildComponent(LuaChangeRuMenBiaoZhunWnd, "SelectXinWuBtn", "SelectXinWuBtn", GameObject)
RegistChildComponent(LuaChangeRuMenBiaoZhunWnd, "ChooseExpressionView", "ChooseExpressionView", GameObject)
RegistChildComponent(LuaChangeRuMenBiaoZhunWnd, "ExpressionViewIcon", "ExpressionViewIcon", CUITexture)
RegistChildComponent(LuaChangeRuMenBiaoZhunWnd, "ChooseExpressionSprite", "ChooseExpressionSprite", GameObject)
RegistChildComponent(LuaChangeRuMenBiaoZhunWnd, "BigFuxiExpressionNameLabel", "BigFuxiExpressionNameLabel", UILabel)
RegistChildComponent(LuaChangeRuMenBiaoZhunWnd, "RefreshButton", "RefreshButton", GameObject)
RegistChildComponent(LuaChangeRuMenBiaoZhunWnd, "TopLabel", "TopLabel", UILabel)

RegistClassMember(LuaChangeRuMenBiaoZhunWnd, "m_HasRight")
RegistClassMember(LuaChangeRuMenBiaoZhunWnd, "m_InCD")
--@endregion RegistChildComponent end

function LuaChangeRuMenBiaoZhunWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	UIEventListener.Get(self.OkButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnOkButtonClick()
	end)

	UIEventListener.Get(self.SelectXinWuBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnSelectXinWuBtnClick()
	end)

	UIEventListener.Get(self.ExpressionViewIcon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnExpressionViewIconClick()
	end)

	UIEventListener.Get(self.RefreshButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnRefreshButtonClick()
	end)

    --@endregion EventBind end
end

function LuaChangeRuMenBiaoZhunWnd:Init()
	self.TopLabel.text = ""
	self.m_IndexTextArray = {LocalString.GetString("一"),LocalString.GetString("二"),LocalString.GetString("三"),LocalString.GetString("四"),
    LocalString.GetString("五"),LocalString.GetString("六"),LocalString.GetString("七")}
	self.ConditionTemplate:SetActive(false)
	self.RefreshButton:SetActive(false)
	self.ChooseExpressionView:SetActive(false)
	self.BigFuxiExpressionNameLabel.text = ""
	self:InitJoinStandardList(LuaZongMenMgr.m_GuiZe)
	if CClientMainPlayer.Inst then
		Gac2Gas.QueryChangeSectJoinStdPreInfo(CClientMainPlayer.Inst.BasicProp.SectId)
	end
end

function LuaChangeRuMenBiaoZhunWnd:OnEnable()
    g_ScriptEvent:AddListener("OnSyncChangeSectJoinStdPreInfo", self, "OnSyncChangeSectJoinStdPreInfo")
	LuaZongMenMgr.m_XingWuItem = nil
	LuaZongMenMgr.m_CreateSectExpressionId = 0
	LuaZongMenMgr.m_CreateSectFuXiDanceMotionId = 0
end

function LuaChangeRuMenBiaoZhunWnd:OnDisable()
	g_ScriptEvent:RemoveListener("OnSyncChangeSectJoinStdPreInfo", self, "OnSyncChangeSectJoinStdPreInfo")
	CUIManager.CloseUI(CLuaUIResources.CommonItemChooseWnd)
	LuaZongMenMgr.m_XingWuItem = nil
end

function LuaChangeRuMenBiaoZhunWnd:OnSyncChangeSectJoinStdPreInfo(lastChangeTime, bHasRight)
	self.m_HasRight = bHasRight
	local now = CServerTimeMgr.Inst.timeStamp
	local subTime = (now - lastChangeTime) / 24 / 60 / 60
	local internal = Menpai_Setting.GetData("JoinStandardReplaceCD").Value

	local text = Menpai_Setting.GetData("JoinStandardReplaceCD").Value .. LocalString.GetString("天内可替换1次，距上次替换时间")
	local msg 
	if lastChangeTime == 0 then
		msg = g_MessageMgr:FormatMessage("MenpaiRulesReplaceWndTitle2",internal)
	elseif subTime >= tonumber(Menpai_Setting.GetData("JoinStandardReplaceCD").Value) then
		msg = g_MessageMgr:FormatMessage("MenpaiRulesReplaceWndTitle1",internal,SafeStringFormat3("[00ff60]%s",math.ceil(subTime)))
	else
		msg = g_MessageMgr:FormatMessage("MenpaiRulesReplaceWndTitle1",internal,SafeStringFormat3("[ff5050]%s",math.ceil(subTime)))
		self.m_InCD = true
	end

	self.TopLabel.text = CUICommonDef.TranslateToNGUIText(msg)
end
--@region UIEvent

function LuaChangeRuMenBiaoZhunWnd:OnOkButtonClick()
	if CClientMainPlayer.Inst then
		if not self.m_HasRight then
			g_MessageMgr:ShowMessage("Has_No_Jurisdiction_Change_RuMenBiaoZhun")
			return
		elseif not LuaZongMenMgr.m_XingWuItem then
			g_MessageMgr:ShowMessage("Select_XinWu_ChangeRuMenBiaoZhun_Confirm")
			return
		elseif self.ChooseExpressionView.activeSelf and LuaZongMenMgr.m_CreateSectExpressionId == 0 and LuaZongMenMgr.m_CreateSectFuXiDanceMotionId == 0 then
			g_MessageMgr:ShowMessage("COMMON_EXPRESSION_SELECTION_WND_NO_SELECTION")
			return
		elseif self.m_InCD then
			g_MessageMgr:ShowMessage("ChangeRuMenBiaoZhun_In_CD", Menpai_Setting.GetData("JoinStandardReplaceCD").Value)
			return
		end
		local default, itemPos, itemId,itemPlace = LuaZongMenMgr:GetXingWuItemInfo()
		local num = tonumber(Menpai_Setting.GetData("JoinStandardReplaceMoney").Value)
		QnMoneyCostMesssageMgr.ShowMoneyCostMessageBox(EnumMoneyType.YinLiang, g_MessageMgr:FormatMessage("Change_RuMenBiaoZhun_MoneyCost",num), num, DelegateFactory.Action(function ()
			Gac2Gas.RequestConfirmChangeSectJoinStd(CClientMainPlayer.Inst.BasicProp.SectId, itemPlace, itemPos, itemId, LuaZongMenMgr.m_CreateSectExpressionId, LuaZongMenMgr.m_CreateSectFuXiDanceMotionId)
			LuaZongMenMgr.m_XingWuItem = nil
			self:OnRefreshButtonClick()
			CUIManager.CloseUI(CLuaUIResources.ChangeRuMenBiaoZhunWnd)
        end), nil, false, true, EnumPlayScoreKey.NONE, false)	
	end
end

function LuaChangeRuMenBiaoZhunWnd:OnSelectXinWuBtnClick()
	if not self.m_HasRight then
		g_MessageMgr:ShowMessage("Has_No_Jurisdiction_Change_RuMenBiaoZhun")
		return
	end
	LuaCommonItemChooseMgr:ShowItemChooseWnd(LocalString.GetString("选择新入派标准"), LocalString.GetString("选择"),
	"", false, function (itemId)
		return self:SelectXinWu(itemId)
	end, nil, nil, function (items)
		self:OnSelectXinWu(items)
	end, nil, LocalString.GetString("你没有宗派信物"), {LuaZongMenMgr:GetXingWuTemplateID()})
end

function LuaChangeRuMenBiaoZhunWnd:SelectXinWu(itemId)
	if itemId ~= nil and not System.String.IsNullOrEmpty(itemId) then
		local item = CItemMgr.Inst:GetById(itemId)
		if item ~= nil then
			return item.TemplateId == LuaZongMenMgr:GetXingWuTemplateID()
		end
	end
	return false
end

function LuaChangeRuMenBiaoZhunWnd:OnSelectXinWu(items)
	self.ChooseExpressionView:SetActive(false)
	if #items > 0 and items[1] ~= nil and not System.String.IsNullOrEmpty(items[1]) then
		local item = CItemMgr.Inst:GetById(items[1])
		if item then
			local extraData = item.Item.ExtraVarData
			if extraData then
				local arr = MsgPackImpl.unpack(extraData.Data) 
				if arr then
					local ruMenGuiZe = arr
					Extensions.RemoveAllChildren(self.RightGrid.transform)
					for i = 0,ruMenGuiZe.Count - 1 do
						local id = ruMenGuiZe[i]
						local text = Menpai_JoinStandard.GetData(id).Description
						local go = NGUITools.AddChild(self.RightGrid.gameObject, self.ConditionTemplate)
						if id == tonumber(Menpai_Setting.GetData("CheckExpressionJoinStandard").Value) then
							self.ChooseExpressionView:SetActive(true)
						end
						go:SetActive(true)
						go.transform:Find("Label"):GetComponent(typeof(UILabel)).text = text
						go.transform:Find("IndexLabel"):GetComponent(typeof(UILabel)).text = self.m_IndexTextArray[i + 1]
					end
					self.RightGrid:Reposition()
					self.SelectXinWuBtn:SetActive(false)
					self.RefreshButton:SetActive(true)
					LuaZongMenMgr.m_XingWuItem = item
				end
			end
		end
	end
	CUIManager.CloseUI(CLuaUIResources.CommonItemChooseWnd)
end

function LuaChangeRuMenBiaoZhunWnd:OnExpressionViewIconClick()
	LuaExpressionMgr:ShowCommonExpressionSelectionWnd(LocalString.GetString( "请选择弟子入派动作"), true, false, function(data)
        LuaZongMenMgr.m_CreateSectExpressionId = 0
        LuaZongMenMgr.m_CreateSectFuXiDanceMotionId = 0
        local info = data.info
        self.ChooseExpressionSprite:SetActive(false)
        self.ExpressionViewIcon:LoadMaterial("")
        if data.isRegular then
            self.ExpressionViewIcon:LoadMaterial(info:GetIcon())	
            self.ExpressionViewIcon.gameObject:SetActive(true)
            LuaZongMenMgr.m_CreateSectExpressionId = info:GetID()
        else
            LuaZongMenMgr.m_CreateSectExpressionId = 47000714
            LuaZongMenMgr.m_CreateSectFuXiDanceMotionId = info.motionId
        end
        --self.m_ExpressionNameLabel.text = data.isRegular and info:GetName() or info.motionName
        self.BigFuxiExpressionNameLabel.text = data.isRegular and "" or CommonDefs.StringAt(info.motionName,0)
    end)
end

function LuaChangeRuMenBiaoZhunWnd:OnRefreshButtonClick()
	self.ChooseExpressionView:SetActive(false)
	self.BigFuxiExpressionNameLabel.text = ""
	self.SelectXinWuBtn:SetActive(true)
	self.ExpressionViewIcon:LoadMaterial("")
	LuaZongMenMgr.m_CreateSectExpressionId = 0
	LuaZongMenMgr.m_CreateSectFuXiDanceMotionId = 0
	Extensions.RemoveAllChildren(self.RightGrid.transform)
	self.RefreshButton:SetActive(false)
end

--@endregion UIEvent
function LuaChangeRuMenBiaoZhunWnd:InitJoinStandardList(data)
	Extensions.RemoveAllChildren(self.LeftGrid.transform)
	local joinStandardList = {}
	local joinStandard = data
	for i = 0, joinStandard.Length - 1 do     
        local id = tonumber(joinStandard[i])
        local data = Menpai_JoinStandard.GetData(id) 
        if data then
            table.insert(joinStandardList, data)
        end
    end
	for i, data in pairs(joinStandardList) do
        local go = CUICommonDef.AddChild(self.LeftGrid.gameObject, self.ConditionTemplate)
        go:SetActive(true)
        go.transform:Find("Label"):GetComponent(typeof(UILabel)).text = data.Description
		go.transform:Find("IndexLabel"):GetComponent(typeof(UILabel)).text = self.m_IndexTextArray[i]
    end
	self.LeftGrid:Reposition()
end
