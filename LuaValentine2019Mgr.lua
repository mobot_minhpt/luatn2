local CommonDefs = import "L10.Game.CommonDefs"

EnumQYSJTaskStatus = { --和服务器保持一致
    UNACCEPT = 1,
    ACCEPTED = 2,
    FINISHED = 3
}

LuaValentine2019Mgr = class()
---------------------------
---------Mgr Field---------
---------------------------
LuaValentine2019Mgr.ShouJiTbl = nil

LuaValentine2019Mgr.ShouJiPartnerId = nil
LuaValentine2019Mgr.ShouJiPartnerName = nil
LuaValentine2019Mgr.ShouJiTaskInfo = nil
LuaValentine2019Mgr.ShouJiPromisePartnerId = nil
LuaValentine2019Mgr.MemoirsItemId = nil

LuaValentine2019Mgr.m_TotExistenceGeneration = 4 --总共四个世代，写死在这里
LuaValentine2019Mgr.m_PreExistenceInfo = nil
LuaValentine2019Mgr.m_ShowPreExistenceShare = false
LuaValentine2019Mgr.m_AlbumWnd = nil

LuaValentine2019Mgr.m_PreExistenceDetailGeneration = 0
LuaValentine2019Mgr.m_PreExistenceDetailTemplateID = 0

LuaValentine2019Mgr.m_PreExistencePlayerID = 0
LuaValentine2019Mgr.m_PreExistencePlayerName = ""

LuaValentine2019Mgr.m_InTheNameOfLoveOthersInfo = nil
LuaValentine2019Mgr.m_InTheNameOfLoveSelfRank = -1
LuaValentine2019Mgr.m_InTheNameOfLoveSelfGuildName = ""
LuaValentine2019Mgr.m_InTheNameOfLoveSelfFlowerNum = 0

LuaValentine2019Mgr.m_BaoXianLiBaoTbl = {}

---------------------------
-------Mgr Function--------
---------------------------
function LuaValentine2019Mgr.ShowQingYuanShouJiListWnd(tbl)
	LuaValentine2019Mgr.ShouJiTbl = tbl or {}
	CUIManager.ShowUI("QingYuanShouJiListWnd")
end

function LuaValentine2019Mgr.GetWrapText(origin)
    if origin==nil or origin=="" then return origin end
     --lua不能正切分割中文，放到C#下
    local name = ""
    local n = CommonDefs.StringLength(origin)
    for i=0,n-1 do
        local split = i>0 and "\n" or ""
        name = name..split..CommonDefs.StringSubstring2(origin, i, 1)
    end
    return name
end

function LuaValentine2019Mgr.RequestValentineQYSJInfo(playerId)
    if playerId then
        LuaValentine2019Mgr.ShouJiPartnerId = playerId
        Gac2Gas.RequestValentineQYSJInfo(playerId)
    end
end

function LuaValentine2019Mgr.ShowQingYuanShouJiContentWnd(partnerId, partnerName, taskInfo)
    LuaValentine2019Mgr.ShouJiPartnerId = partnerId
    LuaValentine2019Mgr.ShouJiPartnerName = partnerName
    LuaValentine2019Mgr.ShouJiTaskInfo = taskInfo
    CUIManager.ShowUI("QingYuanShouJiContentWnd")
end

function LuaValentine2019Mgr.RequestAcceptValentineQYSJTask()
    Gac2Gas.RequestAcceptValentineQYSJTask(LuaValentine2019Mgr.ShouJiPartnerId or 0)
end

function LuaValentine2019Mgr.ShowQingYuanShouJiMemoirsWnd(itemId)
	LuaValentine2019Mgr.MemoirsItemId = itemId
	CUIManager.ShowUI("QingYuanShouJiMemoirsWnd")
end

function LuaValentine2019Mgr.ShowPromiseWnd(partnerId)
    if not partnerId then return end
    if CUIManager.IsLoaded("QingYuanShouJiPromiseWnd") then
        if LuaValentine2019Mgr.ShouJiPromisePartnerId == partnerId then
            return
        end
    end
    LuaValentine2019Mgr.ShouJiPromisePartnerId = partnerId
	CUIManager.ShowUI("QingYuanShouJiPromiseWnd")
end

function LuaValentine2019Mgr.SubmitPromise(promise)
    Gac2Gas.ValentineQYSJWritePromiseAndSubmit(LuaValentine2019Mgr.ShouJiPromisePartnerId or 0, promise)
end

function LuaValentine2019Mgr.AutoTakePhoto()
    CUICommonDef.CaptureScreen("screenshot", true, true, nil, DelegateFactory.Action_string_bytes(function(path,bytes)
            --复用拍照RPC
            Gac2Gas.PlayerCaptureScreenInGame()
        end), true)
end

function LuaValentine2019Mgr.ShowPreExistenceMarridgeAlbumWnd(showShare, infoString, playerId, playerName)
    LuaValentine2019Mgr.m_PreExistenceInfo = nil
    LuaValentine2019Mgr.m_ShowPreExistenceShare = showShare
    LuaValentine2019Mgr.m_PreExistencePlayerID = playerId
    LuaValentine2019Mgr.m_PreExistencePlayerName = playerName

    if infoString then
        LuaValentine2019Mgr.m_PreExistenceInfo = {}
        for templateID in string.gmatch(infoString,"(%d+)") do
            table.insert(LuaValentine2019Mgr.m_PreExistenceInfo, tonumber(templateID))
        end
    end

    CUIManager.ShowUI("PreExistenceMarridgeAlbumWnd")
end

function LuaValentine2019Mgr.ShowPreExistenceMarridgeDetailWnd(showShare, curNum, templateID, playerId, playerName)
    LuaValentine2019Mgr.m_ShowPreExistenceShare = showShare
    LuaValentine2019Mgr.m_PreExistenceDetailGeneration = curNum
    LuaValentine2019Mgr.m_PreExistenceDetailTemplateID = templateID
    LuaValentine2019Mgr.m_PreExistencePlayerID = playerId
    LuaValentine2019Mgr.m_PreExistencePlayerName = playerName

    CUIManager.ShowUI("PreExistenceMarridgeDetailWnd")
end

function LuaValentine2019Mgr.ShowInTheNameOfLoveRankWnd()
    CUIManager.ShowUI("InTheNameOfLoveRankWnd")
end

function LuaValentine2019Mgr.IsValentineBaoXianLiBao(itemTemplateId)
    if not itemTemplateId then return false end
    if LuaValentine2019Mgr.m_BaoXianLiBaoTbl == nil or #LuaValentine2019Mgr.m_BaoXianLiBaoTbl  == 0 then
        local data = Valentine2019_Setting.GetData().BaoXianLiBao
        local n = data and data.Length
        for i=0,n-1 do
            LuaValentine2019Mgr.m_BaoXianLiBaoTbl[data[i]] = true
        end
    end
    return LuaValentine2019Mgr.m_BaoXianLiBaoTbl[itemTemplateId] or false
end

