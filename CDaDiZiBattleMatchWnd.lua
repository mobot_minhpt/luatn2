-- Auto Generated!!
local AlignType1 = import "CPlayerInfoMgr+AlignType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CDaDiZiBattleMatchWnd = import "L10.UI.CDaDiZiBattleMatchWnd"
local CDaDiZiMgr = import "L10.Game.CDaDiZiMgr"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumClass = import "L10.Game.EnumClass"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local Int32 = import "System.Int32"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local Profession = import "L10.Game.Profession"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CDaDiZiBattleMatchWnd.m_SetStageDesc_CS2LuaHook = function (this, desc) 
    this.stageDescLabel.text = desc
    if System.String.IsNullOrEmpty(desc) then
        this.stageDescLabel.gameObject:SetActive(false)
    else
        this.stageDescLabel.gameObject:SetActive(true)
    end
end
CDaDiZiBattleMatchWnd.m_Awake_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)
    UIEventListener.Get(this.clsChangeBtn).onClick = MakeDelegateFromCSFunction(this.OnClickClassChangeButton, VoidDelegate, this)
    UIEventListener.Get(this.watchButton).onClick = MakeDelegateFromCSFunction(this.OnClickWatchButton, VoidDelegate, this)
    UIEventListener.Get(this.refreshBtn).onClick = MakeDelegateFromCSFunction(this.OnClickRefreshButton, VoidDelegate, this)

    this:SetStageDesc("")
    do
        local i = 0
        while i < this.stage1NodeTbl.Length do
            this.stage1NodeTbl[i].stage = 1
            i = i + 1
        end
    end
    do
        local i = 0
        while i < this.stage2NodeTbl.Length do
            this.stage2NodeTbl[i].stage = 2
            i = i + 1
        end
    end
    do
        local i = 0
        while i < this.stage3NodeTbl.Length do
            this.stage3NodeTbl[i].stage = 3
            i = i + 1
        end
    end
    this.stage4Node.stage = 4
end
CDaDiZiBattleMatchWnd.m_OnClickClassChangeButton_CS2LuaHook = function (this, go) 
    local items = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
    do
        local i = 0
        while i < this.clsList.Count do
            local item = PopupMenuItemData(Profession.GetFullName(CommonDefs.ConvertIntToEnum(typeof(EnumClass), this.clsList[i])), MakeDelegateFromCSFunction(this.OnChangeClass, MakeGenericClass(Action1, Int32), this), false, nil)
            CommonDefs.ListAdd(items, typeof(PopupMenuItemData), item)
            i = i + 1
        end
    end
    CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(items), go.transform, CPopupMenuInfoMgr.AlignType.Right)
end
CDaDiZiBattleMatchWnd.m_OnChangeClass_CS2LuaHook = function (this, index) 
    --Debug.Log("index " + index);
    this.curCls = CommonDefs.ConvertIntToEnum(typeof(EnumClass), (this.clsList[index]))
    this.curClsLabel.text = Profession.GetFullName(this.curCls)

    this:Clean()
    this.winnerTex:LoadMaterial(Profession.GetLargeIcon(this.curCls))
    Gac2Gas.QueryShouXiDaDiZiBiWuOverView(this.clsList[index])
end
CDaDiZiBattleMatchWnd.m_Clean_CS2LuaHook = function (this) 
    --stageDescLabel.text = "";
    this:SetStageDesc("")
    do
        local i = 0
        while i < this.nameLabelTbl.Length do
            this.nameLabelTbl[i].text = ""
            i = i + 1
        end
    end

    for i = 0, 7 do
        this.stage1NodeTbl[i]:Init(- 1)
    end
    for i = 0, 3 do
        this.stage2NodeTbl[i]:Init(- 1)
    end

    for i = 0, 1 do
        this.stage3NodeTbl[i]:Init(- 1)
    end
    this.stage4Node:Init(- 1)
end
CDaDiZiBattleMatchWnd.m_Init_CS2LuaHook = function (this) 
    do
        local i = 0
        while i < this.nameLabelTbl.Length do
            this.nameLabelTbl[i].text = ""
            i = i + 1
        end
    end

    this:InitClsList()
    if CClientMainPlayer.Inst ~= nil then
        this.curCls = CClientMainPlayer.Inst.Class
        this.winnerTex:LoadMaterial(Profession.GetLargeIcon(this.curCls))
        Gac2Gas.QueryShouXiDaDiZiBiWuOverView(EnumToInt(this.curCls))
        this.curClsLabel.text = Profession.GetFullName(this.curCls)
    end
end
CDaDiZiBattleMatchWnd.m_InitClsList_CS2LuaHook = function (this) 
    CommonDefs.ListClear(this.clsList)
    if CClientMainPlayer.Inst ~= nil then
        CommonDefs.ListAdd(this.clsList, typeof(UInt32), EnumToInt(CClientMainPlayer.Inst.Class))
    end
    local maxClass = EnumToInt(EnumClass.Undefined)-1
    for i = 1, maxClass do
        local cls = i
        if not CommonDefs.ListContains(this.clsList, typeof(UInt32), cls) then
            CommonDefs.ListAdd(this.clsList, typeof(UInt32), cls)
        end
    end
end
CDaDiZiBattleMatchWnd.m_OnClickName_CS2LuaHook = function (this, go) 
    do
        local i = 0
        while i < this.nameLabelTbl.Length do
            if this.nameLabelTbl[i].gameObject == go then
                if CDaDiZiMgr.Inst.stage1Data.Count > i then
                    local id = CDaDiZiMgr.Inst.stage1Data[i]
                    CPlayerInfoMgr.ShowPlayerPopupMenu(math.floor(id), EnumPlayerInfoContext.ChatLink, EChatPanel.Undefined, nil, nil, go.transform.position, AlignType1.Default)
                    break
                end
            end
            i = i + 1
        end
    end
end
CDaDiZiBattleMatchWnd.m_OnReplyShouXiDaDiZiBiWuOverView_CS2LuaHook = function (this) 
    this.winnerTex:LoadMaterial(Profession.GetLargeIcon(CDaDiZiMgr.Inst.cls))
    this:SetStageDesc(CDaDiZiMgr.Inst:GetStageDesc(CDaDiZiMgr.Inst.stage))
    local default
    if CClientMainPlayer.Inst ~= nil then
        default = CClientMainPlayer.Inst.Name
    else
        default = ""
    end
    local myName = default
    do
        local i = 0
        while i < this.nameLabelTbl.Length do
            this.nameLabelTbl[i].text = CDaDiZiMgr.Inst.nameList[i]
            if myName == CDaDiZiMgr.Inst.nameList[i] then
                this.nameLabelTbl[i].color = Color.green
            else
                this.nameLabelTbl[i].color = Color.white
            end
            UIEventListener.Get(this.nameLabelTbl[i].gameObject).onClick = MakeDelegateFromCSFunction(this.OnClickName, VoidDelegate, this)
            i = i + 1
        end
    end


    for i = 0, 7 do
        local index1 = MultiDimArrayGet(CDaDiZiBattleMatchWnd.IndexLookUp1, i, 0) - 1
        local index2 = MultiDimArrayGet(CDaDiZiBattleMatchWnd.IndexLookUp1, i, 1) - 1
        this.stage1NodeTbl[i].playerId1 = CDaDiZiMgr.Inst.stage1Data[index1]
        this.stage1NodeTbl[i].playerId2 = CDaDiZiMgr.Inst.stage1Data[index2]
        this.stage1NodeTbl[i]:Init(CDaDiZiMgr.Inst.stage2Data[i])
    end
    for i = 0, 3 do
        local index1 = MultiDimArrayGet(CDaDiZiBattleMatchWnd.IndexLookUp2, i, 0) - 1
        local index2 = MultiDimArrayGet(CDaDiZiBattleMatchWnd.IndexLookUp2, i, 1) - 1
        this.stage2NodeTbl[i].playerId1 = CDaDiZiMgr.Inst.stage2Data[index1]
        this.stage2NodeTbl[i].playerId2 = CDaDiZiMgr.Inst.stage2Data[index2]
        this.stage2NodeTbl[i]:Init(CDaDiZiMgr.Inst.stage3Data[i])
    end

    for i = 0, 1 do
        local index1 = MultiDimArrayGet(CDaDiZiBattleMatchWnd.IndexLookUp3, i, 0) - 1
        local index2 = MultiDimArrayGet(CDaDiZiBattleMatchWnd.IndexLookUp3, i, 1) - 1
        this.stage3NodeTbl[i].playerId1 = CDaDiZiMgr.Inst.stage3Data[index1]
        this.stage3NodeTbl[i].playerId2 = CDaDiZiMgr.Inst.stage3Data[index2]
        this.stage3NodeTbl[i]:Init(CDaDiZiMgr.Inst.stage4Data[i])
    end

    this.stage4Node.playerId1 = CDaDiZiMgr.Inst.stage4Data[0]
    this.stage4Node.playerId2 = CDaDiZiMgr.Inst.stage4Data[1]
    this.stage4Node:Init(CDaDiZiMgr.Inst.stage5Data[0])
end
