require("common/common_include")

local UITable = import "UITable"
local UIScrollView = import "UIScrollView"
local CHideAndSeekSkillItem = import "L10.UI.CHideAndSeekSkillItem"
local CButton = import "L10.UI.CButton"
local Extensions = import "Extensions"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"


LuaWaKuangStartWnd = class()

RegistChildComponent(LuaWaKuangStartWnd, "ShowTextLabel", UILabel)
RegistChildComponent(LuaWaKuangStartWnd, "Table", UITable)
RegistChildComponent(LuaWaKuangStartWnd, "ScrollView", UIScrollView)
RegistChildComponent(LuaWaKuangStartWnd, "SkillNode1", CHideAndSeekSkillItem)
RegistChildComponent(LuaWaKuangStartWnd, "SkillNode2", CHideAndSeekSkillItem)
RegistChildComponent(LuaWaKuangStartWnd, "TemplateNode", GameObject)
RegistChildComponent(LuaWaKuangStartWnd, "ApplyBtn", CButton)
RegistChildComponent(LuaWaKuangStartWnd, "AwardLabel", UILabel)

function LuaWaKuangStartWnd:Init()
	self.TemplateNode:SetActive(false)

	self:InitSkills()
	self:InitIntroduction()

	if LuaWuYiMgr.m_LeftRewardTimes == 0 then
		self.AwardLabel.text = SafeStringFormat3(LocalString.GetString("剩余奖励次数 [FF0000]%s[-]"), tostring(LuaWuYiMgr.m_LeftRewardTimes))
	else
		self.AwardLabel.text = SafeStringFormat3(LocalString.GetString("剩余奖励次数 [00FF00]%s[-]"), tostring(LuaWuYiMgr.m_LeftRewardTimes))
	end
	
end

function LuaWaKuangStartWnd:InitSkills()
	local setting = WuYi_WaKuangSetting.GetData()
	local waKuangSkillId = setting.InitWaKuangSkillId
	local jiaSuSkillId = setting.InitJiaSuSkillId

	self.SkillNode1:Init(waKuangSkillId)
	self.SkillNode2:Init(jiaSuSkillId)

	CommonDefs.AddOnClickListener(self.ApplyBtn.gameObject, DelegateFactory.Action_GameObject(function (go)
		self:OnApplyBtnClicked(go)
	end), false)
end

function LuaWaKuangStartWnd:InitIntroduction()
	Extensions.RemoveAllChildren(self.Table.transform)

	local msg = g_MessageMgr:FormatMessage("WaKuang_Introduction")
	if System.String.IsNullOrEmpty(msg) then
        return
    end

    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)

    if info == nil then
        return
    end

    do
        local i = 0
        while i < info.paragraphs.Count do
            local paragraphGo = CUICommonDef.AddChild(self.Table.gameObject, self.TemplateNode)
            paragraphGo:SetActive(true)
            CommonDefs.GetComponent_GameObject_Type(paragraphGo, typeof(CTipParagraphItem)):Init(info.paragraphs[i], 4294967295)
            i = i + 1
        end
    end

    self.ScrollView:ResetPosition()
    self.Table:Reposition()
end

function LuaWaKuangStartWnd:OnApplyBtnClicked(go)
	Gac2Gas.RequestEnterWaKuangPlay()
end

function LuaWaKuangStartWnd:OnEnable()
	
end

function LuaWaKuangStartWnd:OnDisable()
	
end
return LuaWaKuangStartWnd