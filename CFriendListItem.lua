-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CContactAssistantMgr = import "L10.Game.CContactAssistantMgr"
local CExpressionMgr = import "L10.Game.CExpressionMgr"
local CFriendListItem = import "L10.UI.CFriendListItem"
local CIMMgr = import "L10.Game.CIMMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPersonalSpaceMgr = import "L10.Game.CPersonalSpaceMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local ItemType = import "L10.UI.CFriendListItem+ItemType"
local L10 = import "L10"
local LocalString = import "LocalString"
local NGUIText = import "NGUIText"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local CJingLingMgr = import "L10.Game.CJingLingMgr"
CFriendListItem.m_Init_CS2LuaHook = function (this, data, showAlert, showOpButton) 

    this.FriendItemType = data.type
    this.PlayerId = data.playerId
    this.PlayerName = data.name
    this.IsInXianShenStatus = data.isInXianShenStatus

    local name = data.name
    local lianghaoIcon = CUICommonDef.GetLiangHaoIcon(data.profileInfo, false, true)
    if LuaLiangHaoMgr.LiangHaoEnabled() and lianghaoIcon~=nil then
        name = data.name..lianghaoIcon
    end
    this.nameLabel.text = name
    this.portrait:LoadNPCPortrait(data.portraitName, not data.isOnline)
    if this.profileFrame ~= nil and CExpressionMgr.EnableProfileFrame then
        local framePath = CUICommonDef.GetProfileFramePath(data.profileFrame)
        this.profileFrame:LoadMaterial(framePath)
    end
    this.expressionTxt:LoadMaterial(CUICommonDef.GetExpressionTxtPath(data.expressionTxt))
    local default
    if data.isInXianShenStatus then
        default = L10.Game.Constants.ColorOfFeiSheng
    else
        default = NGUIText.EncodeColor24(this.levelLabel.color)
    end
    this.levelLabel.text = System.String.Format(LocalString.GetString("[c][{0}]{1}[-][/c]"), default, data.level)
    this.levelLabel.enabled = (data.type == ItemType.Friend)
    this.AlertVisible = showAlert
    this.operationButton:SetActive(showOpButton)
    this.InfoText = ""
    if data.type == ItemType.JingLing and CContactAssistantMgr.Inst:ContactAssitantOpenVipCheck() then
        this.assistantBtn:SetActive(true)
    else
        this.assistantBtn:SetActive(false)
    end

    if CIMMgr.IM_GM_ID == data.playerId then
        if CPersonalSpaceMgr.EnableGMSpace and CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.ItemProp.Vip.Level >= CContactAssistantMgr.GMSpaceVipLevel then
            this.gmSpaceBtn:SetActive(true)
        else
            this.gmSpaceBtn:SetActive(false)
        end
    else
        this.gmSpaceBtn:SetActive(false)
    end
    CLuaFriendListItem:InitGMWeChatBtn(this, data, showAlert, showOpButton)
end
CFriendListItem.m_RefreshBasicInfo_CS2LuaHook = function (this, playerId, online, playerName, portraitName, level, isInXianShenStatus) 

    if this.PlayerId ~= playerId then
        return
    end
    this.PlayerName = this.PlayerName
    this.IsInXianShenStatus = isInXianShenStatus

    local name = playerName
    local basicInfo = CIMMgr.Inst:GetBasicInfo(playerId)
    local lianghaoIcon = basicInfo and CUICommonDef.GetLiangHaoIcon(basicInfo.ProfileInfo, false, true)
    if LuaLiangHaoMgr.LiangHaoEnabled() and lianghaoIcon~=nil then
        name = name..lianghaoIcon
    end
    this.nameLabel.text = name
    this.portrait:LoadNPCPortrait(portraitName, not online)
    local default
    if isInXianShenStatus then
        default = L10.Game.Constants.ColorOfFeiSheng
    else
        default = NGUIText.EncodeColor24(this.levelLabel.color)
    end
    this.levelLabel.text = System.String.Format(LocalString.GetString("[c][{0}]{1}[-][/c]"), default, level)
end
CFriendListItem.m_Start_CS2LuaHook = function (this) 
    UIEventListener.Get(this.operationButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.operationButton).onClick, MakeDelegateFromCSFunction(this.OnOpButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.assistantBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.assistantBtn).onClick, MakeDelegateFromCSFunction(this.OnAssistantButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.gmSpaceBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.gmSpaceBtn).onClick, MakeDelegateFromCSFunction(this.OnGMSpaceButtonClick, VoidDelegate, this), true)
end
CLuaFriendListItem = {}
CLuaFriendListItem.JingLingKey = nil
function CLuaFriendListItem:InitGMWeChatBtn(view, data, showAlert, showOpButton)

    local weChatBtn = view.transform:Find("GMWeChatBtn").gameObject
    weChatBtn:SetActive(false)
    if CClientMainPlayer.Inst == nil then
		return
	end
    if CIMMgr.IM_GM_ID == data.playerId and LuaJingLingMgr:CanOpenWeChatWelFare() then
        if CommonDefs.DictContains(CClientMainPlayer.Inst.PlayProp.PersistPlayData, typeof(UInt32), EnumPersistPlayDataKey_lua.eQiWeiFuLiGuanData) then
            local data = CClientMainPlayer.Inst.PlayProp.PersistPlayData[EnumPersistPlayDataKey_lua.eQiWeiFuLiGuanData].Data
            local info = data and g_MessagePack.unpack(data) or {}
            if #info >= 3 then
                CLuaFriendListItem.JingLingKey = {}
                CLuaFriendListItem.JingLingKey.key = info[1]
                CLuaFriendListItem.JingLingKey.time = info[2]
                CLuaFriendListItem.JingLingKey.duration = info[3]
                if info[3] == -1 or info[2] + info[3] > CServerTimeMgr.Inst.timeStamp then
                    weChatBtn:SetActive(true)
                end
            end
        end
    end
    UIEventListener.Get(weChatBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
        CLuaFriendListItem:OnGMWeChatBtnClick()
    end) 
end

function CLuaFriendListItem:OnGMWeChatBtnClick()
    if CLuaFriendListItem.JingLingKey then
        local key = CLuaFriendListItem.JingLingKey.key
        local time = CLuaFriendListItem.JingLingKey.time
        local duration = CLuaFriendListItem.JingLingKey.duration
        if duration == -1 or time + duration > CServerTimeMgr.Inst.timeStamp then
            CJingLingMgr.Inst:ShowJingLingWnd(key, "o_push", true, false, nil, false)
        end
    end
end


