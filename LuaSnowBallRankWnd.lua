local UIEventListener = import "UIEventListener"
local XueQiuDaZhan_Season = import "L10.Game.XueQiuDaZhan_Season"
local Extensions = import "Extensions"
local NGUITools = import "NGUITools"
local UILabel = import "UILabel"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

local AlignType = import "CPlayerInfoMgr+AlignType"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local DelegateFactory = import "DelegateFactory"
local EChatPanel = import "L10.Game.EChatPanel"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local GameObject = import "UnityEngine.GameObject"
local LocalString = import "LocalString"
local UISprite = import "UISprite"
local Vector3 = import "UnityEngine.Vector3"
local UITabBar = import "L10.UI.UITabBar"
local Object = import "System.Object"

LuaSnowBallRankWnd = class()
RegistChildComponent(LuaSnowBallRankWnd,"CloseButton", GameObject)
RegistChildComponent(LuaSnowBallRankWnd,"templateNode", GameObject)
RegistChildComponent(LuaSnowBallRankWnd,"selfInfoNode", GameObject)
RegistChildComponent(LuaSnowBallRankWnd,"scrollView", UIScrollView)
RegistChildComponent(LuaSnowBallRankWnd,"table", UITable)
RegistChildComponent(LuaSnowBallRankWnd,"Tabs", UITabBar)

RegistClassMember(LuaSnowBallRankWnd, "NodeList")

function LuaSnowBallRankWnd:GetSeasonId()
  local seasonCount = XueQiuDaZhan_Season.GetDataCount()
  local nowTime = CServerTimeMgr.Inst.timeStamp
  for i=1,seasonCount do
    local seasonInfo = XueQiuDaZhan_Season.GetData(1000+i)
    local index = i
    if seasonInfo then
      if nowTime <= seasonInfo.RankEndTime and nowTime >= seasonInfo.RankStartTime and seasonInfo.Status == 0 then
        return seasonInfo.ID
      end
    end
  end
  return 0
end

function LuaSnowBallRankWnd:SetNodeInfo(node,info, idx, selfSign)
	if info and CommonDefs.DictContains(info, typeof(String), 'IsEmpty') then
    if selfSign then
      node.transform:Find("Name/text"):GetComponent(typeof(UILabel)).text = CClientMainPlayer.Inst.Name
      node.transform:Find('score'):GetComponent(typeof(UILabel)).text = '0'
      node.transform:Find("rank"):GetComponent(typeof(UILabel)).text = LocalString.GetString('未上榜')
      node.transform:Find('totalnum'):GetComponent(typeof(UILabel)).text = '0'
      node.transform:Find('winrate'):GetComponent(typeof(UILabel)).text = '0%'
      node.transform:Find('kill'):GetComponent(typeof(UILabel)).text = '0'
    else
      node:SetActive(false)
    end
  elseif info then

    if info.playerId ~= CClientMainPlayer.Inst.Id then
      UIEventListener.Get(node).onClick = DelegateFactory.VoidDelegate(function (p)
        self:SetNodeSelected(idx)
        CPlayerInfoMgr.ShowPlayerPopupMenu(info.playerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), AlignType.Default)
      end)
    end
    node.transform:Find("Name/text"):GetComponent(typeof(UILabel)).text = info.name
    if CommonDefs.DictContains(info, typeof(String), 'rank') and info.rank > 0 then
      node.transform:Find("rank"):GetComponent(typeof(UILabel)).text = info.rank
    else
      node.transform:Find("rank"):GetComponent(typeof(UILabel)).text = LocalString.GetString('未上榜')
    end
    node.transform:Find('score'):GetComponent(typeof(UILabel)).text = info.matchScore
    node.transform:Find('totalnum'):GetComponent(typeof(UILabel)).text = info.totalNum
    if info.totalNum == 0 then
      node.transform:Find('winrate'):GetComponent(typeof(UILabel)).text = '0%'
      node.transform:Find('kill'):GetComponent(typeof(UILabel)).text = '0'
    else
      node.transform:Find('winrate'):GetComponent(typeof(UILabel)).text = SafeStringFormat3('%.2f',info.winNum / info.totalNum * 100) .. '%'
      node.transform:Find('kill'):GetComponent(typeof(UILabel)).text = SafeStringFormat3('%.2f',info.killNum / info.totalNum)
    end

    local sprite = (not selfSign) and node:GetComponent(typeof(UISprite)) --排除界面顶部自己的条目
    if sprite then
      if idx % 2 == 0 then
        sprite.spriteName = g_sprites.EvenBgSprite
      else
        sprite.spriteName = g_sprites.OddBgSpirite
      end
    end
  end
end

function LuaSnowBallRankWnd:SetNodeSelected(idx)
  if not self.NodeList then return end
  for i = 1, #self.NodeList do
    local node  = self.NodeList[i]
    self:SetOneNodeSelected(node, i == idx+1, idx)
  end
end

function LuaSnowBallRankWnd:SetOneNodeSelected(node, selected, idx)
  if not node then return end
  local sprite = node:GetComponent(typeof(UISprite))
  if selected then
    sprite.spriteName = g_sprites.HighlightBgSprite
  else
    if idx % 2 == 0 then
      sprite.spriteName = g_sprites.EvenBgSprite
    else
      sprite.spriteName = g_sprites.OddBgSpirite
    end
  end
end

function LuaSnowBallRankWnd:InitData()
  if LuaSnowBallMgr.TotalRankInfo then
    local selfRank = 0
    local count = LuaSnowBallMgr.TotalRankInfo.Count
    if not self.NodeList then self.NodeList = {} end
    for i = 0,count -1 do
      local data = LuaSnowBallMgr.TotalRankInfo[i]
      if data then
        local node = NGUITools.AddChild(self.table.gameObject,self.templateNode)
        node:SetActive(true)
        CommonDefs.DictAdd(data, typeof(String), 'rank', typeof(Object), i+1)
        if data.playerId == CClientMainPlayer.Inst.Id then
          selfRank = i + 1
        end
        self:SetNodeInfo(node, data, i)
        table.insert(self.NodeList, node)
      end
    end
    self.table:Reposition()
    self.scrollView:ResetPosition()

    if selfRank > 0 then
      CommonDefs.DictAdd(LuaSnowBallMgr.SelfRankInfo, typeof(String), 'rank', typeof(Object), selfRank)
    end
  end

  if LuaSnowBallMgr.SelfRankInfo then
    self.selfInfoNode:SetActive(true)
    self:SetNodeInfo(self.selfInfoNode,LuaSnowBallMgr.SelfRankInfo, 0, true)
  else
    self.selfInfoNode:SetActive(false)
  end
end

function LuaSnowBallRankWnd:OnTabChange(node,index)
  local seasonId = self:GetSeasonId()
  if seasonId == 0 then
    self.table.gameObject:SetActive(false)
    self.selfInfoNode:SetActive(false)
    return
  end
  if index == 0 then
    Gac2Gas.QueryXueQiuSeasonRank(1001,seasonId)
  elseif index == 1 then
    Gac2Gas.QueryXueQiuSeasonRank(1002,seasonId)
  end
end

function LuaSnowBallRankWnd:Init()
	self.Tabs.OnTabChange = DelegateFactory.Action_GameObject_int(function (go, index)
		self:OnTabChange(go, index)
	end)
  self.templateNode:SetActive(false)

	self.Tabs:ChangeTab(0)

  self:InitData()
end

function LuaSnowBallRankWnd:UpdateInfo()
  Extensions.RemoveAllChildren(self.table.transform)
  self.NodeList = {}
  self:InitData()
end

function LuaSnowBallRankWnd:OnEnable( ... )
	g_ScriptEvent:AddListener("UpdateSnowBallRankInfo", self, "UpdateInfo")
end

function LuaSnowBallRankWnd:OnDisable( ... )
	g_ScriptEvent:RemoveListener("UpdateSnowBallRankInfo", self, "UpdateInfo")
end

function LuaSnowBallRankWnd:OnDestroy( ... )
end

return LuaSnowBallRankWnd
