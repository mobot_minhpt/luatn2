local UISlider = import "UISlider"
local UILabel = import "UILabel"

LuaOlympicWinterGamesSnowBattleTaskView = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaOlympicWinterGamesSnowBattleTaskView, "Slider1", "Slider1", UISlider)
RegistChildComponent(LuaOlympicWinterGamesSnowBattleTaskView, "Slider2", "Slider2", UISlider)
RegistChildComponent(LuaOlympicWinterGamesSnowBattleTaskView, "ProgressLabel1", "ProgressLabel1", UILabel)
RegistChildComponent(LuaOlympicWinterGamesSnowBattleTaskView, "ProgressLabel2", "ProgressLabel2", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaOlympicWinterGamesSnowBattleTaskView, "m_IsInit")

function LuaOlympicWinterGamesSnowBattleTaskView:Awake()
    self.Slider1.value = 0
    self.Slider2.value = 0
    self.ProgressLabel1.text = ""
    self.ProgressLabel2.text = ""
end

function LuaOlympicWinterGamesSnowBattleTaskView:OnEnable()
    g_ScriptEvent:AddListener("OnSnowballFightSyncPlayState", self, "OnSnowballFightSyncPlayState")
    g_ScriptEvent:AddListener("OnCommonCountDownViewUpdate", self, "OnCommonCountDownViewUpdate")
    if self.m_IsInit then
        RegisterTickOnce(function()
            LuaOlympicGamesMgr:ShowSnowballFightTaskView()
        end,100)
    end
    if LuaOlympicGamesMgr.m_SnowballFightPlayProgress then
        self:OnSnowballFightSyncPlayState(LuaOlympicGamesMgr.m_SnowballFightPlayProgress[1], LuaOlympicGamesMgr.m_SnowballFightPlayProgress[2])
    end
end

function LuaOlympicWinterGamesSnowBattleTaskView:OnDisable()
    g_ScriptEvent:RemoveListener("OnSnowballFightSyncPlayState", self, "OnSnowballFightSyncPlayState")
    g_ScriptEvent:RemoveListener("OnCommonCountDownViewUpdate", self, "OnCommonCountDownViewUpdate")
end

function LuaOlympicWinterGamesSnowBattleTaskView:OnSnowballFightSyncPlayState(progress1, progress2)
    local maxProgress = WinterOlympic_SnowballFightSetting.GetData().MaxProgress
    self.Slider1.value = progress1 / maxProgress
    self.Slider2.value = progress2 / maxProgress
    self.ProgressLabel1.text = SafeStringFormat3("%d/%d",progress1,maxProgress)
    self.ProgressLabel2.text = SafeStringFormat3("%d/%d",progress2,maxProgress)
end

function LuaOlympicWinterGamesSnowBattleTaskView:OnCommonCountDownViewUpdate()
    self.m_IsInit = true
end
--@region UIEvent

--@endregion UIEvent
