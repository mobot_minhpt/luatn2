-- Auto Generated!!
local CHongbaoHistroyItem = import "L10.UI.CHongbaoHistroyItem"
local CHongBaoMgr = import "L10.UI.CHongBaoMgr"
local CItem = import "L10.Game.CItem"
local EnumHongbaoType = import "L10.UI.EnumHongbaoType"
local LocalString = import "LocalString"
CHongbaoHistroyItem.m_UpdateData_CS2LuaHook = function (this, data) 
    if data ~= nil then
        this.m_TimeLabel.text = data.TimeString
        if CHongBaoMgr.m_HongbaoType == EnumHongbaoType.Gift then
            this.m_MoneyLabel.text = (CItem.GetName(data.ItemId) .. LocalString.GetString("*")) .. tostring(data.ItemNum)
        else
            this.m_MoneyLabel.text = tostring(data.Silver)
        end
    else
        this.m_TimeLabel.text = ""
        this.m_MoneyLabel.text = "0"
    end
end
