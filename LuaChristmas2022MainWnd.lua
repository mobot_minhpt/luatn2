local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local QnButton = import "L10.UI.QnButton"
local Animation = import "UnityEngine.Animation"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local CWelfareMgr = import "L10.UI.CWelfareMgr"
local CSigninMgr = import "L10.Game.CSigninMgr"
local CScheduleMgr = import "L10.Game.CScheduleMgr"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

LuaChristmas2022MainWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaChristmas2022MainWnd, "TimeLab", "TimeLab", GameObject)
RegistChildComponent(LuaChristmas2022MainWnd, "TurkeyMatchBtn", "TurkeyMatchBtn", QnButton)
RegistChildComponent(LuaChristmas2022MainWnd, "WarmChristmasEveBtn", "WarmChristmasEveBtn", QnButton)
RegistChildComponent(LuaChristmas2022MainWnd, "ShoppingMallBtn", "ShoppingMallBtn", QnButton)
RegistChildComponent(LuaChristmas2022MainWnd, "ChasingElkBtn", "ChasingElkBtn", QnButton)
RegistChildComponent(LuaChristmas2022MainWnd, "SignInWelfareBtn", "SignInWelfareBtn", QnButton)

--@endregion RegistChildComponent end
RegistClassMember(LuaChristmas2022MainWnd, "m_Animator")
RegistClassMember(LuaChristmas2022MainWnd, "m_TimeLabel")
RegistClassMember(LuaChristmas2022MainWnd, "m_SignedGo")
RegistClassMember(LuaChristmas2022MainWnd, "m_ToSignGo")
RegistClassMember(LuaChristmas2022MainWnd, "m_WarmChristmasEveRedDot")
RegistClassMember(LuaChristmas2022MainWnd, "m_TurkeyMatchRedDot")
RegistClassMember(LuaChristmas2022MainWnd, "m_CreateSnowManRedDot")
RegistClassMember(LuaChristmas2022MainWnd, "m_ShoppingMallRedDot")

function LuaChristmas2022MainWnd:Awake()
    self.m_TimeLabel= self.TimeLab:GetComponent(typeof(UILabel))
    self.m_Animator = self.transform:GetComponent(typeof(Animation))
    self.m_SignedGo = self.SignInWelfareBtn.transform:Find("qipaokgd/qiandaokgd/christmas2022mainwnd_qiandao_yiqian").gameObject
    self.m_ToSignGo = self.SignInWelfareBtn.transform:Find("qipaokgd/qiandaokgd/vfx_christmas2022mainwnd_qiandao_qiandao").gameObject
    self.m_WarmChristmasEveRedDot   = self.WarmChristmasEveBtn.transform:Find("shengdanltkgd/alert").gameObject
    self.m_TurkeyMatchRedDot        = self.ChasingElkBtn.transform:Find("rongxuekgd/alert").gameObject
    self.m_CreateSnowManRedDot      = self.TurkeyMatchBtn.transform:Find("duixuerenkgd/alert").gameObject
    self.m_ShoppingMallRedDot       = self.ShoppingMallBtn.transform:Find("shengdanthkgd/alert").gameObject
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self.m_TimeLabel.text = g_MessageMgr:FormatMessage("Christmas2022_TimeText")

    --欢乐堆雪人
    self.TurkeyMatchBtn.OnClick = DelegateFactory.Action_QnButton(function(btn)
        if not self:IsChristmasStart() then
            g_MessageMgr:ShowMessage("QianDao_Not_Open")
			return
        end
        self:TryShowActivity(42030267, function ()
            CUIManager.ShowUI(CLuaUIResources.ChristmasCreateSnowmanSignWnd)
        end)
        LuaActivityRedDotMgr:OnRedDotClicked(20)
    end)

    --温暖平安夜
    self.WarmChristmasEveBtn.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:TryShowActivity(42030265)
        LuaActivityRedDotMgr:OnRedDotClicked(18)
    end)

    self.ShoppingMallBtn.OnClick = DelegateFactory.Action_QnButton(function(btn)
        if not self:IsChristmasStart() then
            g_MessageMgr:ShowMessage("ShengDanTeHu_Not_Open")
            return 
        end
        CShopMallMgr.ShowLingyuLiBaoShop()
        LuaActivityRedDotMgr:OnRedDotClicked(25)
    end)

    --融雪逐麋鹿
    self.ChasingElkBtn.OnClick = DelegateFactory.Action_QnButton(function(btn)
        self:TryShowActivity(42030266, function ()
            self.m_Animator:Play("christmas2022mainwnd_qiehuan")
        end)
        LuaActivityRedDotMgr:OnRedDotClicked(19)
    end)

    self.SignInWelfareBtn.OnClick = DelegateFactory.Action_QnButton(function(btn)
        if not self:IsChristmasStart() then
            g_MessageMgr:ShowMessage("QianDao_Not_Open")
            return 
        end
        CWelfareMgr.OpenWelfareWnd(LocalString.GetString("圣诞节签到"))
    end)
end

function LuaChristmas2022MainWnd:Init()
    if not LuaChristmas2022Mgr.m_MainWndOpened then
        self.m_Animator:Play("christmas2022mainwnd_zjmshow")
    else
        self.m_Animator:Play("christmas2022mainwnd_zjmshow_1")
    end
    LuaChristmas2022Mgr.m_MainWndOpened = true

    local signable = self:IsHolidayAlert()
    self.m_SignedGo:SetActive(not signable)
    self.m_ToSignGo:SetActive(signable)
    self:UpdateRedDot()
end

function LuaChristmas2022MainWnd:OnEnable()
    g_ScriptEvent:AddListener("UpdateActivityRedDot", self, "OnUpdateActivityRedDot")
end

function LuaChristmas2022MainWnd:OnDisable()
    g_ScriptEvent:RemoveListener("UpdateActivityRedDot", self, "OnUpdateActivityRedDot")
end

--@region UIEvent

--@endregion UIEvent

function LuaChristmas2022MainWnd:TryShowActivity(activityId, successFuc)
    if self:CheckScheduleOpen(activityId, true, false) and CScheduleMgr.Inst:IsCanJoinSchedule(activityId, true) then
        --在活动时间里
        if successFuc then
            successFuc()
        else
            CLuaScheduleMgr:ShowScheduleInfo(activityId)
        end
    else
        local scheduleData = Task_Schedule.GetData(activityId)
        CLuaScheduleMgr.selectedScheduleInfo = {
            activityId = activityId,
            taskId = scheduleData.TaskID[0],
            TotalTimes = 0,
            DailyTimes = 0,
            ActivityTime = scheduleData.ExternTip,
            ShowJoinBtn = false
        }
        CLuaScheduleInfoWnd.s_UseCustomInfo = true
        CUIManager.ShowUI(CLuaUIResources.ScheduleInfoWnd)
    end
end

function LuaChristmas2022MainWnd:CheckScheduleOpen(activityId, showLevelMsg, showTaskNotOpenMsg)
    local scheduleData = Task_Schedule.GetData(activityId)
    local taskData = Task_Task.GetData(scheduleData.TaskID[0])
    local playerLv = CClientMainPlayer.Inst and CClientMainPlayer.Inst.MaxLevel or 0
    local jieRiId = 25 -- 圣诞节

    if taskData and playerLv < taskData.Level then
        if showLevelMsg then
            g_MessageMgr:ShowMessage("CUSTOM_STRING2", SafeStringFormat3(LocalString.GetString("参加%s玩法需%s级"), scheduleData.TaskName, tostring(taskData.Level)))
        end
        return false
    elseif taskData and playerLv >= taskData.Level then
        --判断完等级之后, 再根据服务器数据判断一下这个玩法有没有开启吧..
        local openFestivalTasks = CLuaScheduleMgr.m_OpenFestivalTasks[jieRiId] or {}
        local taskId = scheduleData.TaskID[0]
        for i = 1, #openFestivalTasks do
            if openFestivalTasks[i] == taskId then
                return true
            end
        end
        --这个活动没开启
        if showTaskNotOpenMsg then
            g_MessageMgr:ShowMessage("PLAY_NOT_OPEN")
        end
        return false
    else
        --这个是保险起见, 一般不会跑到这里
        if showTaskNotOpenMsg then
            g_MessageMgr:ShowMessage("PLAY_NOT_OPEN")
        end
        return false
    end
end

function LuaChristmas2022MainWnd:IsHolidayAlert()
    if not self:IsChristmasStart() then
        return true
    end
    local holiday = CSigninMgr.Inst.holidayInfo
	if holiday and holiday.showSignin and not holiday.hasSignGift and holiday.todayInfo and holiday.todayInfo.Open == 1 then
		return true
	else
		return false
	end
end

function LuaChristmas2022MainWnd:IsChristmasStart()
    local now = CServerTimeMgr.Inst.timeStamp
    local begin = CServerTimeMgr.Inst:GetTimeStampByStr("2022-12-22 00:00")
    local beginSeconds = begin - now
    return beginSeconds < 0
end

function LuaChristmas2022MainWnd:OnUpdateActivityRedDot(hasChanged)
    if hasChanged then
        self:UpdateRedDot()
    end
end

function LuaChristmas2022MainWnd:UpdateRedDot()
    --Task_JieRiRedDot对应
    local holiday = CSigninMgr.Inst.holidayInfo
    self.m_WarmChristmasEveRedDot:SetActive(LuaActivityRedDotMgr:IsRedDot(18))
    self.m_TurkeyMatchRedDot:SetActive(LuaActivityRedDotMgr:IsRedDot(19))
    self.m_CreateSnowManRedDot:SetActive(LuaActivityRedDotMgr:IsRedDot(20))
    self.m_ShoppingMallRedDot:SetActive(holiday and LuaActivityRedDotMgr:IsRedDot(25))
end