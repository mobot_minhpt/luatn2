local DelegateFactory  = import "DelegateFactory"
local UITable = import "UITable"
local Extensions = import "Extensions"
local CButton = import "L10.UI.CButton"

LuaGuildLeagueCrossZhanLingAwardPoolWnd = class()

RegistChildComponent(LuaGuildLeagueCrossZhanLingAwardPoolWnd, "m_ContentLabel", "ContentLabel", UILabel)
RegistChildComponent(LuaGuildLeagueCrossZhanLingAwardPoolWnd, "m_ContentScrollView", "ContentScrollView", UIScrollView)
RegistChildComponent(LuaGuildLeagueCrossZhanLingAwardPoolWnd, "m_LingYuPoolLabel", "LingYuPool", UILabel)
RegistChildComponent(LuaGuildLeagueCrossZhanLingAwardPoolWnd, "m_YinLiangPoolLabel", "YinLiangPool", UILabel)
RegistChildComponent(LuaGuildLeagueCrossZhanLingAwardPoolWnd, "m_ZhanLingButton", "ZhanLingButton", GameObject)
RegistChildComponent(LuaGuildLeagueCrossZhanLingAwardPoolWnd, "m_GambleButton", "GambleButton", GameObject)

function LuaGuildLeagueCrossZhanLingAwardPoolWnd:Awake()
    UIEventListener.Get(self.m_ZhanLingButton).onClick = DelegateFactory.VoidDelegate(function() self:OnZhanLingButtonClick() end)
    UIEventListener.Get(self.m_GambleButton).onClick = DelegateFactory.VoidDelegate(function() self:OnGambleButtonClick() end)
end

function LuaGuildLeagueCrossZhanLingAwardPoolWnd:Init()
    local info = LuaGuildLeagueCrossMgr.m_ZhanLingAwardPoolInfo
    self.m_ContentLabel.text = info.desc
    self.m_LingYuPoolLabel.text = tostring(info.lingyuPool)
    self.m_YinLiangPoolLabel.text = tostring(info.yinliangPool)
    self.m_ContentScrollView:ResetPosition()
    if info.type == nil or info.type == EnumZhanLingAwardPoolWndOpenType.eDefault then
        self.m_ZhanLingButton:SetActive(true)
        self.m_GambleButton:SetActive(true)
    elseif info.type == EnumZhanLingAwardPoolWndOpenType.eGamble then
        self.m_ZhanLingButton:SetActive(true)
        self.m_GambleButton:SetActive(false)
    else
        self.m_ZhanLingButton:SetActive(false)
        self.m_GambleButton:SetActive(true)
    end
end

function LuaGuildLeagueCrossZhanLingAwardPoolWnd:OnZhanLingButtonClick()
    LuaGuildLeagueCrossMgr:QueryGuildLeagueCrossZhanLingExtInfo()
end

function LuaGuildLeagueCrossZhanLingAwardPoolWnd:OnGambleButtonClick()
    LuaGuildLeagueCrossMgr:QueryGuildLeagueCrossGambleInfo()
end

