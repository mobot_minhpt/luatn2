local QnTableView=import "L10.UI.QnTableView"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CUITexture=import "L10.UI.CUITexture"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local LuaDefaultModelTextureLoader = import "L10.UI.LuaDefaultModelTextureLoader"
local CRenderObject = import "L10.Engine.CRenderObject"
local LayerDefine = import "L10.Engine.LayerDefine"
local Quaternion = import "UnityEngine.Quaternion"
local Screen = import "UnityEngine.Screen"
local Vector3 = import "UnityEngine.Vector3"
local Time = import "UnityEngine.Time"
local Camera = import "UnityEngine.Camera"
local EnumMoneyType = import "L10.Game.EnumMoneyType"
local EnumPlayScoreKey = import "L10.Game.EnumPlayScoreKey"
local GestureRecognizer = import "L10.Engine.GestureRecognizer"
local GestureType = import "L10.Engine.GestureType"
local DelegateFactory = import "DelegateFactory"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local EasyTouch = import "EasyTouch"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType = import "L10.Game.EnumWarnFXType"

CLuaZswTemplatePreviewWnd = class()

RegistClassMember(CLuaZswTemplatePreviewWnd, "m_ModelTextureLoader")
RegistClassMember(CLuaZswTemplatePreviewWnd, "m_ModelTexture")
RegistChildComponent(CLuaZswTemplatePreviewWnd, "m_TableView","TableView", QnTableView)
RegistChildComponent(CLuaZswTemplatePreviewWnd, "m_BuyBtn","BuyBtn", GameObject)
RegistChildComponent(CLuaZswTemplatePreviewWnd, "m_ListBtn","ListBtn", GameObject)
RegistChildComponent(CLuaZswTemplatePreviewWnd, "m_Designer","Designer", UILabel)
RegistChildComponent(CLuaZswTemplatePreviewWnd, "m_HouseGrade","HouseGrade", UILabel)
RegistChildComponent(CLuaZswTemplatePreviewWnd, "m_ZswCount","ZswCount", UILabel)
RegistChildComponent(CLuaZswTemplatePreviewWnd, "m_TemplateNameLabel","TemplateNameLabel", UILabel)
RegistChildComponent(CLuaZswTemplatePreviewWnd,"m_OverLook","OverLook",GameObject)
RegistChildComponent(CLuaZswTemplatePreviewWnd,"m_MoneyCtrl", CCurentMoneyCtrl)

RegistChildComponent(CLuaZswTemplatePreviewWnd,"m_LeftBtn","LeftBtn",GameObject)
RegistChildComponent(CLuaZswTemplatePreviewWnd,"m_RightBtn","RightBtn",GameObject)
RegistChildComponent(CLuaZswTemplatePreviewWnd,"m_BestView","BestView",GameObject)
RegistChildComponent(CLuaZswTemplatePreviewWnd,"m_BestPreview","BestPreview",CUITexture)
RegistChildComponent(CLuaZswTemplatePreviewWnd,"m_HouseBtn","HouseBtn",GameObject)

RegistClassMember(CLuaZswTemplatePreviewWnd, "m_CurPreviewId")
RegistClassMember(CLuaZswTemplatePreviewWnd, "m_ZswTemplateTbl")
RegistClassMember(CLuaZswTemplatePreviewWnd, "m_Rotation")
RegistClassMember(CLuaZswTemplatePreviewWnd, "m_ZswComponentList")
RegistClassMember(CLuaZswTemplatePreviewWnd, "m_Offsets")
RegistClassMember(CLuaZswTemplatePreviewWnd, "m_Scales")
RegistClassMember(CLuaZswTemplatePreviewWnd, "m_Rotations")
RegistClassMember(CLuaZswTemplatePreviewWnd, "m_RoTbl")
RegistClassMember(CLuaZswTemplatePreviewWnd, "m_IsOverLook")
RegistClassMember(CLuaZswTemplatePreviewWnd, "m_ModelRoot")
RegistClassMember(CLuaZswTemplatePreviewWnd, "m_BestPreviewRotation")
RegistClassMember(CLuaZswTemplatePreviewWnd, "m_LeftPressed")
RegistClassMember(CLuaZswTemplatePreviewWnd, "m_RightPressed")
RegistClassMember(CLuaZswTemplatePreviewWnd, "m_StartPressTime")
RegistClassMember(CLuaZswTemplatePreviewWnd, "m_DeltaTime")
RegistClassMember(CLuaZswTemplatePreviewWnd, "m_DeltaRot")
RegistClassMember(CLuaZswTemplatePreviewWnd, "m_SelectIndex")
RegistClassMember(CLuaZswTemplatePreviewWnd, "m_SelectItemId")

function CLuaZswTemplatePreviewWnd:OnEnable()
    GestureRecognizer.Inst:AddNeedUIPinchWnd(CLuaUIResources.ZswTemplatePreviewWnd)
    self.onPinchInDelegate = DelegateFactory.Action_GenericGesture(function(gesture) self:OnPinchIn(gesture) end)
    self.onPinchOutDelegate = DelegateFactory.Action_GenericGesture(function(gesture) self:OnPinchOut(gesture) end)
    GestureRecognizer.AddGestureListener(GestureType.OnUIPinchIn, self.onPinchInDelegate)
    GestureRecognizer.AddGestureListener(GestureType.OnUIPinchOut, self.onPinchOutDelegate)
    g_ScriptEvent:AddListener("MouseScrollWheel",self,"OnMouseScrollWheel")
end

function CLuaZswTemplatePreviewWnd:OnDisable()
    GestureRecognizer.RemoveGestureListener(GestureType.OnUIPinchIn, self.onPinchInDelegate)
    GestureRecognizer.RemoveGestureListener(GestureType.OnUIPinchOut, self.onPinchOutDelegate)
    g_ScriptEvent:RemoveListener("MouseScrollWheel",self,"OnMouseScrollWheel")
end

function CLuaZswTemplatePreviewWnd:Awake()
    self.m_IsOverLook = false
    self.m_Identifier = "zsw_Template_preview"
    self.m_CurPreviewId = CLuaZswTemplateMgr.CurPreviewTemplateId
    self.m_ZswTemplateTbl = {}
    self.m_RoTbl = {}
    self.m_ModelTexture = self.transform:Find("Anchor/Preview"):GetComponent(typeof(CUITexture))
    self.m_ModelTextureLoader = LuaDefaultModelTextureLoader.Create(function (ro)
        self:LoadModel(ro)
        self.m_PreviewRootRo = ro
    end)
    CommonDefs.AddOnDragListener(self.m_ModelTexture.gameObject, DelegateFactory.Action_GameObject_Vector2(function (go, delta)
		self:OnScreenDrag(go, delta)
    end), false)
    self.m_Rotation = 0

    self.m_StartPressTime = 0
    self.m_DeltaTime = 0.1
    self.m_DeltaRot = -10
    Zhuangshiwu_ZswTemplate.Foreach(function (furnitureId, data)
        local zsw = Zhuangshiwu_Zhuangshiwu.GetData(furnitureId)
        if zsw then
            local data = Mall_LingYuMallLimit.GetData(zsw.ItemId)
            if data and data.Status == 0 then
                table.insert(self.m_ZswTemplateTbl,furnitureId)
                if self.m_CurPreviewId == furnitureId then
                    self.m_SelectIndex = #self.m_ZswTemplateTbl
                end
            end
        end
    end)
    ------

    self.m_TableView.m_DataSource = DefaultTableViewDataSource.Create(
        function()
            return #self.m_ZswTemplateTbl
        end,
        function(item,row)
            self:InitItem(item,row)
        end)

    self.m_TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
        self.m_CurPreviewId = self.m_ZswTemplateTbl[row + 1]
        CLuaZswTemplateMgr.CurPreviewTemplateId = self.m_CurPreviewId
        self:Refresh()
    end)
    
    UIEventListener.Get(self.m_ListBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.ShowUI(CLuaUIResources.ZswTemplateComponentListWnd)
    end)

    UIEventListener.Get(self.m_LeftBtn).onPress = DelegateFactory.BoolDelegate(function (go, isPressed)
        self.m_LeftPressed = isPressed
    end)
    UIEventListener.Get(self.m_RightBtn).onPress = DelegateFactory.BoolDelegate(function (go, isPressed)
        self.m_RightPressed = isPressed
    end)
    UIEventListener.Get(self.m_OverLook).onClick = DelegateFactory.VoidDelegate(function(go)
        self:ChangeToBestView()
    end)
    UIEventListener.Get(self.m_BuyBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnBuyButtonClick()
    end)
    UIEventListener.Get(self.m_HouseBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        local templateId = Zhuangshiwu_Zhuangshiwu.GetData(self.m_CurPreviewId).ItemId
        Gac2Gas.RequestEnterHouseCompetitionTemplateShow(templateId)
    end)

end

function CLuaZswTemplatePreviewWnd:Init()
    if self.m_SelectIndex then
        self.m_TableView:SetSelectRow(self.m_SelectIndex-1,true)
    end

    local tData =  Zhuangshiwu_ZswTemplate.GetData(self.m_CurPreviewId)
    local data = Zhuangshiwu_Zhuangshiwu.GetData(self.m_CurPreviewId)
    if not tData or not data then
        return
    end

    self.m_TemplateNameLabel.text = data.Name
    self.m_ZswComponentList = tData.DefaultComponents
    self.m_Offsets = tData.Offset
    self.m_Scales = tData.Scale
    self.m_Rotations = tData.Rotation
    self.m_Rotation = tData.PreviewDefaultRot
    self.m_Designer.text = tData.Designer
    self.m_ZswCount.text = tData.SlotNum
    self.m_HouseGrade.text = data.Grade
    local previewPos = tData.PreviewPos
    local previewScale = tData.PreviewScale
    if previewScale == 0 then previewScale = 1 end
    
    if previewPos then
        self.m_ModelTexture.mainTexture=CUIManager.CreateModelTexture(self.m_Identifier, self.m_ModelTextureLoader,self.m_Rotation,previewPos[0],previewPos[1],previewPos[2],false,true,previewScale)
    else
        self.m_ModelTexture.mainTexture=CUIManager.CreateModelTexture(self.m_Identifier, self.m_ModelTextureLoader,self.m_Rotation,-1.65,-3.05,14.98,false,true,previewScale)
    end
    --俯视视角
    local overLook = tData.OverLook
    self.m_IsOverLook = overLook == 1
    self.m_OverLook:SetActive(self.m_IsOverLook)

    self.m_TableView:ReloadData(false, false)
end

function CLuaZswTemplatePreviewWnd:Refresh()
    local tData =  Zhuangshiwu_ZswTemplate.GetData(self.m_CurPreviewId)
    local data = Zhuangshiwu_Zhuangshiwu.GetData(self.m_CurPreviewId)
    if not tData or not data then
        return
    end
    local cost = 0
    self.m_SelectItemId = data.ItemId
    local mallData = Mall_LingYuMallLimit.GetData(self.m_SelectItemId)
    if mallData then
        cost = mallData.Jade
    end

    self.m_MoneyCtrl:SetType(EnumMoneyType.LingYu, EnumPlayScoreKey.NONE, true)
    self.m_MoneyCtrl:SetCost(cost)

    self.m_TemplateNameLabel.text = data.Name
    self.m_ZswComponentList = tData.DefaultComponents
    self.m_Offsets = tData.Offset
    self.m_Scales = tData.Scale
    self.m_Rotations = tData.Rotation
    self.m_Rotation = tData.PreviewDefaultRot
    self.m_Designer.text = tData.Designer
    self.m_ZswCount.text = tData.SlotNum
    self.m_HouseGrade.text = tData.HouseGrade

    for i,ro in ipairs(self.m_RoTbl) do
        ro:Destroy()
    end
    self.m_RoTbl = {}

    local previewPos = tData.PreviewPos
    local previewScale = tData.PreviewScale
    local previewDeaultRot = tData.PreviewDefaultRot

    if previewScale == 0 then previewScale = 1 end

    self:LoadModel(self.m_PreviewRootRo)

    local modelRoot = self.m_PreviewRootRo.transform.parent
    if previewPos then
        modelRoot.localPosition = Vector3(previewPos[0],previewPos[1],previewPos[2])
        self.m_PreviewPos = modelRoot.localPosition
    end
    modelRoot.localScale = Vector3(previewScale,previewScale,previewScale)
    self.m_MinPreviewZ = tData.MinPosZ
    self.m_MaxPreviewZ = tData.MaxPosZ

    --俯视视角
    local overLook = tData.OverLook
    self.m_IsOverLook = overLook == 1
    self.m_OverLook:SetActive(self.m_IsOverLook)
    self.m_Rotation = previewDeaultRot
    self.m_ModelRoot.localRotation = Quaternion.Euler(0, self.m_Rotation, 0) 
end

function CLuaZswTemplatePreviewWnd:InitItem(item,row)
    local name = item.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local cost = item.transform:Find("CostLabel"):GetComponent(typeof(UILabel))
    local icon = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))

    local templateId = self.m_ZswTemplateTbl[row + 1]
    local tdata = Zhuangshiwu_ZswTemplate.GetData(templateId)
    local zdata = Zhuangshiwu_Zhuangshiwu.GetData(templateId)
    local itemId = zdata.ItemId
    local idata = Item_Item.GetData(itemId)
    local mdata = Mall_LingYuMallLimit.GetData(itemId)
    if mdata then
        cost.text = mdata.Jade
    end

    name.text = zdata.Name
    icon:LoadMaterial(idata.Icon)
    UIEventListener.Get(icon.gameObject).onClick = DelegateFactory.VoidDelegate(function (go) 
        CItemInfoMgr.ShowLinkItemTemplateInfo(itemId,false,nil,AlignType.Default, 0, 0, 0, 0)
    end)
    
end

function CLuaZswTemplatePreviewWnd:OnDestroy()
    self.m_ModelTexture.mainTexture=nil
    CUIManager.DestroyModelTexture(self.m_Identifier)
    for i,ro in ipairs(self.m_RoTbl) do
        ro:Destroy()
    end
    self.m_BestPreview.mainTexture=nil
    CUIManager.DestroyModelTexture("__zsw_templatebest__")

end

function CLuaZswTemplatePreviewWnd:OnScreenDrag(go, delta)   
    if EasyTouch.GetTouchCount() == 1 then 
        local rotx = -delta.x / Screen.width * 360
        self.m_Rotation = self.m_Rotation + rotx
        CUIManager.SetModelRotation(self.m_Identifier, self.m_Rotation)
    end
end

function CLuaZswTemplatePreviewWnd:LoadModel(ro) 
    self.m_ModelRoot = ro.transform.parent
    for i=0,self.m_ZswComponentList.Length-1,1 do
        local id = self.m_ZswComponentList[i]

        local data = Zhuangshiwu_Zhuangshiwu.GetData(id)
        local resName = data.ResName
        local resid = SafeStringFormat3("_%02d",data.ResId)
        local prefabname = "Assets/Res/Character/Jiayuan/" .. resName .. "/Prefab/" .. resName .. resid .. ".prefab"

        local ident = "templatepart_"..i
        local newRo = CRenderObject.CreateRenderObject(ro.gameObject, ident)
        newRo.Layer = LayerDefine.ModelForNGUI_3D
        newRo:LoadMain(prefabname)
        newRo.transform.localPosition = Vector3(self.m_Offsets[i*3]/100,self.m_Offsets[i*3+1]/100,self.m_Offsets[i*3+2]/100)
        newRo.Scale = self.m_Scales[i]
        newRo.transform.localRotation =Quaternion.Euler(Vector3(self.m_Rotations[i*3],self.m_Rotations[i*3+1],self.m_Rotations[i*3+2]))
        table.insert(self.m_RoTbl,newRo)
        if data.FX > 0 and id ~= 9336 then
            local selffx = CEffectMgr.Inst:AddObjectFX(data.FX, newRo, 0, 1, 1, nil, false, EnumWarnFXType.None, Vector3.zero, Vector3.zero, nil)
            newRo:AddFX("selfFx", selffx, -1)
        end
        
        newRo:AddLoadAllFinishedListener(
            DelegateFactory.Action_CRenderObject(
                function(_ro)
                    local rds = _ro.m_Renderers
                    if rds then
                        for i=0,rds.Count-1,1 do
                            local renderer = rds[i]
                            local mats = renderer.sharedMaterials
                            for k=0,mats.Length-1,1 do
                                local mat = mats[k]
                                if mat.shader.name == "L10/Jiayuan/JiayuanVegetation" then
                                    mat:SetFloat("_WindInfluence",0)
                                end
                            end
                        end
                    end
                end
            )
        )
    end
end

function CLuaZswTemplatePreviewWnd:ChangeToBestView()
    local identiTf = CUIManager.instance.transform:Find(self.m_Identifier)
    self.m_Camera = identiTf:GetComponent(typeof(Camera))
    self.m_Camera.enabled = false
    self.m_BestView:SetActive(true)

    local data = Zhuangshiwu_ZswTemplate.GetData(self.m_CurPreviewId)
    local previewPos = data.PreviewPos
    local previewScale = data.PreviewScale
    local rotation = data.PreviewDefaultRot
    if previewScale == 0 then previewScale = 1 end

    local loader = LuaDefaultModelTextureLoader.Create(function (ro)
    end)

    if previewPos then
        self.m_BestPreview.mainTexture=CUIManager.CreateModelTexture("__zsw_templatebest__", loader,0,previewPos[0],previewPos[1],previewPos[2],false,true,previewScale)
    else
        self.m_BestPreview.mainTexture=CUIManager.CreateModelTexture("__zsw_templatebest__", loader,0,-1.65,-3.05,14.98,false,true,previewScale)
    end
    CUIManager.instance.transform:Find("__zsw_templatebest__").localPosition = identiTf.localPosition
    --打开俯视视角
    self.m_BestPreviewRotation = data.BestPreviewRotation
    self.m_LastModleRot = self.m_ModelRoot.localRotation
    self.m_LastModlePosition = self.m_ModelRoot.localPosition
    self.m_ModelRoot.localRotation = Quaternion.Euler(self.m_BestPreviewRotation[0], self.m_BestPreviewRotation[1], self.m_BestPreviewRotation[2])
    if data.BestPreviewPos then
        self.m_ModelRoot.localPosition = Vector3(data.BestPreviewPos[0],data.BestPreviewPos[1],data.BestPreviewPos[2])
    end
end

function CLuaZswTemplatePreviewWnd:Update()
    if self.m_LeftPressed and Time.realtimeSinceStartup - self.m_StartPressTime > self.m_DeltaTime then
        self.m_Rotation = self.m_Rotation + self.m_DeltaRot
        self.m_StartPressTime = Time.realtimeSinceStartup
        CUIManager.SetModelRotation(self.m_Identifier, self.m_Rotation)
    elseif self.m_RightPressed and Time.realtimeSinceStartup - self.m_StartPressTime > self.m_DeltaTime then
        self.m_Rotation = self.m_Rotation - self.m_DeltaRot
        self.m_StartPressTime = Time.realtimeSinceStartup
        CUIManager.SetModelRotation(self.m_Identifier, self.m_Rotation)
    end

    self:ClickThroughToClose()
end
function CLuaZswTemplatePreviewWnd:ClickThroughToClose()
    if self.m_BestView.activeSelf and Input.GetMouseButtonDown(0) then    
        if (not CUICommonDef.IsOverUI or (UICamera.lastHit.collider and
           not CUICommonDef.IsChild(self.m_BestView.transform, UICamera.lastHit.collider.transform) and
            CUICommonDef.GetNearestPanelDepth(self.m_BestView.transform) > CUICommonDef.GetNearestPanelDepth(UICamera.lastHit.collider.transform))) then
            self.m_BestView:SetActive(false)
            
            self.m_BestPreview.mainTexture=nil
            CUIManager.DestroyModelTexture("__zsw_templatebest__")
            self.m_ModelRoot.localRotation = self.m_LastModleRot
            self.m_ModelRoot.localPosition = self.m_LastModlePosition
            self.m_Camera.enabled = true
            
        end
    end
end

function CLuaZswTemplatePreviewWnd:OnBuyButtonClick()
    if not self.m_SelectItemId then
        return
    end
    Gac2Gas.BuyMallItem(EShopMallRegion_lua.ELingyuMallLimit, self.m_SelectItemId, 1)
end
------
function CLuaZswTemplatePreviewWnd:OnPinchIn(gesture)
    local deltaPinch = gesture.deltaPinch
    local pinchScale = math.min(math.max(deltaPinch / 400, 0),1)
    self:OnPinch(-pinchScale)
end

function CLuaZswTemplatePreviewWnd:OnPinchOut(gesture)
    local deltaPinch = gesture.deltaPinch
    local pinchScale = math.min(math.max(deltaPinch / 400, 0),1)
    self:OnPinch(pinchScale)
end

function CLuaZswTemplatePreviewWnd:OnMouseScrollWheel()
    local v = Input.GetAxis("Mouse ScrollWheel")
    self:OnPinch(v)
end

function CLuaZswTemplatePreviewWnd:OnPinch(delta)
    local posZ = self.m_PreviewPos.z
    posZ = posZ - delta
    if (self.m_MaxPreviewZ and self.m_MinPreviewZ) and (posZ > self.m_MaxPreviewZ or posZ < self.m_MinPreviewZ) then
        return 
    end
    self.m_PreviewPos.z = posZ
    local pos = self.m_PreviewPos
    CUIManager.SetModelPosition(self.m_Identifier, pos)
end
function CLuaZswTemplatePreviewWnd:SetModelPosAndScale(pos, scale)
    self.localModelScale = scale
    pos = Vector3(pos.x / scale, pos.y / scale, pos.z /scale)
    CUIManager.SetModelPosition(self.m_Identifier, pos)
end
