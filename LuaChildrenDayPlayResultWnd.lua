local GameObject = import "UnityEngine.GameObject"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local UILabel = import "UILabel"
local DelegateFactory = import "DelegateFactory"
local CUIFx = import "L10.UI.CUIFx"
local CUITexture = import "L10.UI.CUITexture"
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CLoginMgr = import "L10.Game.CLoginMgr"

LuaChildrenDayPlayResultWnd = class()
RegistChildComponent(LuaChildrenDayPlayResultWnd,"closeBtn", GameObject)
RegistChildComponent(LuaChildrenDayPlayResultWnd,"shareBtn", GameObject)
RegistChildComponent(LuaChildrenDayPlayResultWnd,"checkBtn", GameObject)
RegistChildComponent(LuaChildrenDayPlayResultWnd,"playerInfoNode", GameObject)
RegistChildComponent(LuaChildrenDayPlayResultWnd,"scoreLabel", UILabel)
RegistChildComponent(LuaChildrenDayPlayResultWnd,"bonusLabel", UILabel)
RegistChildComponent(LuaChildrenDayPlayResultWnd,"draw", GameObject)
RegistChildComponent(LuaChildrenDayPlayResultWnd,"win", GameObject)
RegistChildComponent(LuaChildrenDayPlayResultWnd,"lose", GameObject)
RegistChildComponent(LuaChildrenDayPlayResultWnd,"fxNode", CUIFx)

RegistClassMember(LuaChildrenDayPlayResultWnd, "maxChooseNum")

function LuaChildrenDayPlayResultWnd:Close()
	CUIManager.CloseUI(CLuaUIResources.ChildrenDayPlayResultWnd)
end

function LuaChildrenDayPlayResultWnd:OnEnable()
	--g_ScriptEvent:AddListener("", self, "")
end

function LuaChildrenDayPlayResultWnd:OnDisable()
	--g_ScriptEvent:RemoveListener("", self, "")
end

function LuaChildrenDayPlayResultWnd:Init()
	local onCloseClick = function(go)
    self:Close()
	end
	CommonDefs.AddOnClickListener(self.closeBtn,DelegateFactory.Action_GameObject(onCloseClick),false)

	local onShareClick = function(go)
		CUICommonDef.CaptureScreenAndShare()
	end
	CommonDefs.AddOnClickListener(self.shareBtn,DelegateFactory.Action_GameObject(onShareClick),false)

	local onCheckClick = function(go)
		Gac2Gas.RequestPengPengCheBattleInfo()
	end
	CommonDefs.AddOnClickListener(self.checkBtn,DelegateFactory.Action_GameObject(onCheckClick),false)

	CommonDefs.GetComponent_Component_Type(self.playerInfoNode.transform:Find("name"), typeof(UILabel)).text = CClientMainPlayer.Inst.Name
	local myGameServer = CLoginMgr.Inst:GetSelectedGameServer()
	CommonDefs.GetComponent_Component_Type(self.playerInfoNode.transform:Find("server"), typeof(UILabel)).text = myGameServer.name
	CommonDefs.GetComponent_Component_Type(self.playerInfoNode.transform:Find("ID"), typeof(UILabel)).text = tostring(CClientMainPlayer.Inst.Id)
	CommonDefs.GetComponent_Component_Type(self.playerInfoNode.transform:Find("icon"), typeof(CUITexture)):LoadNPCPortrait(CUICommonDef.GetPortraitName(CClientMainPlayer.Inst.Class, CClientMainPlayer.Inst.Gender, -1), false)
	CommonDefs.GetComponent_Component_Type(self.playerInfoNode.transform:Find("icon/lv"), typeof(UILabel)).text = tostring(CClientMainPlayer.Inst.Level)

  -- result: 1 win 2 lose 3 dogfall
	--LuaChildrenDay2019Mgr.PengPengCheResultInfo = {result = result, playScore = playScore,addedLiuYiScore=addedLiuYiScore}
	self.scoreLabel.text = '0'
	self.bonusLabel.text = '0'
	local panelTable = {self.win,self.lose,self.draw}
	if LuaChildrenDay2019Mgr.PengPengCheResultInfo then
		self.scoreLabel.text = LuaChildrenDay2019Mgr.PengPengCheResultInfo.playScore
		self.bonusLabel.text = LuaChildrenDay2019Mgr.PengPengCheResultInfo.addedLiuYiScore
		for i,v in pairs(panelTable) do
			if i == LuaChildrenDay2019Mgr.PengPengCheResultInfo.result then
				v:SetActive(true)
			else
				v:SetActive(false)
			end
		end
	end
	if LuaChildrenDay2019Mgr.PengPengCheResultInfo.result == 1 then
		self.fxNode:LoadFx(("Fx/UI/Prefab/UI_pengpengche_shengli.prefab"))
	else
		self.fxNode:LoadFx(("Fx/UI/Prefab/UI_pengpengche_shibai.prefab"))
	end
end

function LuaChildrenDayPlayResultWnd:OnDestroy()

end

return LuaChildrenDayPlayResultWnd
