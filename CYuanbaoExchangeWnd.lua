local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local CCurentMoneyCtrl = import "L10.UI.CCurentMoneyCtrl"
local QnAddSubAndInputButton = import "L10.UI.QnAddSubAndInputButton"
local DelegateFactory = import "DelegateFactory"
local QnButton = import "L10.UI.QnButton"
local CMessageTipMgr = import "L10.UI.CMessageTipMgr"
local CTipParagraphItem = import "L10.UI.CTipParagraphItem"

CYuanbaoExchangeWnd = class()
RegistChildComponent(CYuanbaoExchangeWnd, "m_ParagraphTemplate", "ParagraphTemplate", GameObject)
RegistChildComponent(CYuanbaoExchangeWnd, "m_ScrollView", "ContentScrollView", UIScrollView)
RegistChildComponent(CYuanbaoExchangeWnd, "m_Table", "Table", UITable)
RegistChildComponent(CYuanbaoExchangeWnd, "m_MoneyCtrl", CCurentMoneyCtrl)
RegistChildComponent(CYuanbaoExchangeWnd, "m_ExchangeCountButton", QnAddSubAndInputButton)
RegistChildComponent(CYuanbaoExchangeWnd, "m_YuanBaoCountLabel", UILabel)
RegistChildComponent(CYuanbaoExchangeWnd, "m_OkButton", QnButton)


function CYuanbaoExchangeWnd:Init()
    if not CClientMainPlayer.Inst then return end
    local own = self.m_MoneyCtrl:GetOwn()
    if own <= 0 then
        self.m_ExchangeCountButton:SetMinMax(0, 0, 1)
        self.m_ExchangeCountButton:SetValue(0, true)
    else
        local maxValue = 999999
        if own < maxValue then
            maxValue = own
        end
        self.m_ExchangeCountButton:SetMinMax(1, maxValue, 1)
        self.m_ExchangeCountButton:SetValue(1, true)
    end
    self:OnExchangeCountChanged(self.m_ExchangeCountButton:GetValue())
    self.m_OkButton.OnClick = DelegateFactory.Action_QnButton(function(go) self:OnOkButtonClick(go) end)
    self.m_ExchangeCountButton.onValueChanged = DelegateFactory.Action_uint(function(v) self:OnExchangeCountChanged(v) end)
    self:ShowTip()
end
function CYuanbaoExchangeWnd:OnExchangeCountChanged(v)   
    self.m_YuanBaoCountLabel.text = tostring(v * 10)
end
function CYuanbaoExchangeWnd:OnOkButtonClick(button) 
    local count = self.m_ExchangeCountButton:GetValue()
    if CShopMallMgr.TryCheckEnoughJade(count, uint.MaxValue) then
        Gac2Gas.RequestExchangeJade2YuanBao(count)
    end
    self:CloseView()
    CUIManager.CloseUI("WelfareGiftWnd")
end
function CYuanbaoExchangeWnd:ShowTip()
    self.m_ParagraphTemplate:SetActive(false)
    Extensions.RemoveAllChildren(self.m_Table.transform)
    local msg =	g_MessageMgr:FormatMessage("Jade_YuanBao_Tip")
    if System.String.IsNullOrEmpty(msg) then return end

    local info = CMessageTipMgr.Inst:ExtractTitleAndParagraphs(msg)
    if not info then return end
    for i=0, info.paragraphs.Count-1 do
        local paragraph = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_ParagraphTemplate)
        paragraph:SetActive(true)
        local tip = CommonDefs.GetComponent_GameObject_Type(paragraph, typeof(CTipParagraphItem))
        tip:Init(info.paragraphs[i], 4294967295)
    end
    self.m_Table:Reposition()
    self.m_ScrollView:ResetPosition()
end

function CYuanbaoExchangeWnd:CloseView()
    CUIManager.CloseUI(CUIResources.YuanbaoExchangeWnd)
end

return CYuanbaoExchangeWnd
