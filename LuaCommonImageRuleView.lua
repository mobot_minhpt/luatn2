local CUITexture = import "L10.UI.CUITexture"
local UIWidget   = import "UIWidget"

LuaCommonImageRuleView = class()

RegistClassMember(LuaCommonImageRuleView, "m_ImageTemplate")
RegistClassMember(LuaCommonImageRuleView, "m_ImageRoot")
RegistClassMember(LuaCommonImageRuleView, "m_IndicatorTemplate")
RegistClassMember(LuaCommonImageRuleView, "m_IndicatorGrid")

RegistClassMember(LuaCommonImageRuleView, "m_PicList")
RegistClassMember(LuaCommonImageRuleView, "m_MessageList")
RegistClassMember(LuaCommonImageRuleView, "m_Angles")
RegistClassMember(LuaCommonImageRuleView, "m_DeltaDegree")
RegistClassMember(LuaCommonImageRuleView, "m_CurrentSelectedIndex")

function LuaCommonImageRuleView:Awake()
    self.m_ImageTemplate = self.transform:Find("Template").gameObject
    self.m_ImageRoot = self.transform:Find("ImageRoot")
    self.m_IndicatorTemplate = self.transform:Find("IndicatorTemplate").gameObject
    self.m_IndicatorGrid = self.transform:Find("IndicatorGrid"):GetComponent(typeof(UIGrid))
    self.m_ImageTemplate:SetActive(false)
    self.m_IndicatorTemplate:SetActive(false)
end

function LuaCommonImageRuleView:Init(imagePaths, msgs)
    self.m_PicList = imagePaths
    self.m_MessageList = msgs

    Extensions.RemoveAllChildren(self.m_ImageRoot.transform)
    for i, v in ipairs(self.m_PicList) do
        local go = CUICommonDef.AddChild(self.m_ImageRoot.gameObject, self.m_ImageTemplate)
        go:SetActive(true)
        local texture = go:GetComponent(typeof(CUITexture))
        texture:LoadMaterial(v)

        local ruleText = go.transform:Find("Border/Label"):GetComponent(typeof(UILabel))
        ruleText.text = self.m_MessageList[i]
        if System.String.IsNullOrEmpty(self.m_MessageList[i]) then
            ruleText.gameObject:SetActive(false)
        else
            ruleText.gameObject:SetActive(true)
        end

        UIEventListener.Get(go).onDrag = DelegateFactory.VectorDelegate(function(g, delta)
            self:OnDrag(g, delta)
        end)
        UIEventListener.Get(go).onDragEnd = DelegateFactory.VoidDelegate(function(g)
            self:OnDragEnd(g)
        end)
        UIEventListener.Get(go).onDragStart = DelegateFactory.VoidDelegate(function(g)
            self:OnDragStart(g)
        end)
    end
    self.m_Angles = {}
    self.m_DeltaDegree = math.floor(360 / #self.m_PicList)
    for i, v in ipairs(self.m_PicList) do
        self.m_Angles[i] = (i - 1) * self.m_DeltaDegree
    end

    self.m_CurrentSelectedIndex = 1
    self:UpdatePosition()

    Extensions.RemoveAllChildren(self.m_IndicatorGrid.transform)
    for i, v in ipairs(self.m_PicList) do
        local go = CUICommonDef.AddChild(self.m_IndicatorGrid.gameObject, self.m_IndicatorTemplate)
        go:SetActive(true)
    end
    self.m_IndicatorGrid:Reposition()

    self:UpdateIndicator(self.m_CurrentSelectedIndex)

    UIEventListener.Get(self.transform:Find("LastArrow").gameObject).onClick = DelegateFactory.VoidDelegate(function(g)
        self:UpdatePage( -1)
    end)
    UIEventListener.Get(self.transform:Find("NextArrow").gameObject).onClick = DelegateFactory.VoidDelegate(function(g)
        self:UpdatePage(1)
    end)
end

function LuaCommonImageRuleView:UpdateIndicator(index)
    for i = 1, self.m_IndicatorGrid.transform.childCount do
        local child = self.m_IndicatorGrid.transform:GetChild(i - 1)
        child:Find("Normal").gameObject:SetActive(i ~= index)
        child:Find("Highlight").gameObject:SetActive(i == index)
    end
end

function LuaCommonImageRuleView:OnDrag(go, delta)
    local moveDeg = 57.2958 * math.asin(math.max( -1, math.min(delta.x / 400, 1)))
    for i, v in ipairs(self.m_Angles) do
        local angle = v + moveDeg
        self.m_Angles[i] = angle - math.floor(angle / 360) * 360
    end

    self:UpdatePosition()
end

function LuaCommonImageRuleView:OnDragStart(go)
    for i, v in ipairs(self.m_Angles) do
        local child = self.m_ImageRoot:GetChild(i - 1)
        LuaTweenUtils.DOKill(child, false)
    end
end

function LuaCommonImageRuleView:UpdatePage(offset)
    self.m_CurrentSelectedIndex = self.m_CurrentSelectedIndex + offset

    if self.m_CurrentSelectedIndex < 1 then
        self.m_CurrentSelectedIndex = self.m_CurrentSelectedIndex + #self.m_Angles
    end
    if self.m_CurrentSelectedIndex > #self.m_Angles then
        self.m_CurrentSelectedIndex = self.m_CurrentSelectedIndex - #self.m_Angles
    end

    for i, v in ipairs(self.m_Angles) do
        local degree = (i - self.m_CurrentSelectedIndex) * self.m_DeltaDegree
        degree = degree - math.floor(degree / 360) * 360

        local moveTo = math.sin(0.0174533 * degree) * 250
        local scale = math.cos(0.0174533 * degree) * 0.2 + 0.8
        scale = math.max(0, math.min(1, scale))

        local child = self.m_ImageRoot:GetChild(i - 1)
        LuaTweenUtils.TweenPositionX(child, moveTo, 0.3)
        LuaTweenUtils.TweenScaleTo(child, Vector3(scale, scale, 1), 0.3)
        self:UpdateDepthAndAlpha(child, scale)
        self.m_Angles[i] = degree
    end

    self:UpdateIndicator(self.m_CurrentSelectedIndex)
end

function LuaCommonImageRuleView:OnDragEnd(go)
    local selectIndex = 0

    local index = go.transform:GetSiblingIndex() + 1
    local x = go.transform.localPosition.x
    local scale = go.transform.localScale.x
    --看划过的距离
    if x > -50 and x < 50 and scale > 0.7 then
        selectIndex = index
    elseif x < 0 then
        selectIndex = index + 1
    elseif x > 0 then
        selectIndex = index - 1
    end

    if selectIndex < 1 then selectIndex = selectIndex + #self.m_Angles end
    if selectIndex > #self.m_Angles then selectIndex = selectIndex - #self.m_Angles end

    for i, v in ipairs(self.m_Angles) do
        local degree = (i - selectIndex) * self.m_DeltaDegree
        degree = degree - math.floor(degree / 360) * 360

        local moveTo = math.sin(0.0174533 * degree) * 250
        local scale = math.cos(0.0174533 * degree) * 0.2 + 0.8
        scale = math.max(0, math.min(1, scale))

        local child = self.m_ImageRoot:GetChild(i - 1)
        LuaTweenUtils.TweenPositionX(child, moveTo, 0.3)
        LuaTweenUtils.TweenScaleTo(child, Vector3(scale, scale, 1), 0.3)
        self:UpdateDepthAndAlpha(child, scale)
        self.m_Angles[i] = degree
    end

    self:UpdateIndicator(selectIndex)
end

function LuaCommonImageRuleView:UpdatePosition()
    local childCount = self.m_ImageRoot.childCount
    for i = 0, childCount - 1 do
        local child = self.m_ImageRoot:GetChild(i)
        local angle = self.m_Angles[i + 1]
        angle = angle - math.floor(angle / 360) * 360

        local offset = math.sin(0.0174533 * angle) * 250
        LuaUtils.SetLocalPositionX(child, offset)

        local scale = math.cos(0.0174533 * angle) * 0.2 + 0.8
        LuaUtils.SetLocalScale(child, scale, scale, 1)

        self:UpdateDepthAndAlpha(child, scale)
    end
end

function LuaCommonImageRuleView:UpdateDepthAndAlpha(transRoot, scale)
    local _depth = math.floor(scale * 100) * 2
    local _alpha = scale * scale > 0.95 and 1 or scale * scale * scale * scale

    local uitexture = transRoot:GetComponent(typeof(UIWidget))
    local label = transRoot:Find("Border/Label"):GetComponent(typeof(UIWidget))
    local labelBg = transRoot:Find("Border/Bg"):GetComponent(typeof(UIWidget))
    local border = transRoot:Find("Border"):GetComponent(typeof(UIWidget))
    uitexture.depth = _depth
    label.depth = _depth + 2
    uitexture.alpha = _alpha
    label.alpha = _alpha
    border.depth = uitexture.depth - 1
    labelBg.depth = uitexture.depth + 1
end
