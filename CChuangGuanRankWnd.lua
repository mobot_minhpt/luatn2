-- Auto Generated!!
local CChuangGuanRankInfo = import "L10.Game.CChuangGuanRankInfo"
local CChuangGuanRankItem = import "L10.UI.CChuangGuanRankItem"
local CChuangGuanRankWnd = import "L10.UI.CChuangGuanRankWnd"
local ChuangGuan_Dispatch = import "L10.Game.ChuangGuan_Dispatch"
local CMenPaiChuangGuanMgr = import "L10.Game.CMenPaiChuangGuanMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CompletionTimeInfo = import "L10.Game.CompletionTimeInfo"
local CUICommonDef = import "L10.UI.CUICommonDef"
local DelegateFactory = import "DelegateFactory"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local UInt32 = import "System.UInt32"
CChuangGuanRankWnd.m_showRankItems_CS2LuaHook = function (this) 
    CommonDefs.ListClear(this.rankDataList)
    do
        local i = 0
        while i < this.TitleLabels.Length do
            local id = CMenPaiChuangGuanMgr.Inst.SelfRankInfo.singleCompletionTimes[i].menpaiId
            this.TitleLabels[i].text = ChuangGuan_Dispatch.GetData(id).desc
            i = i + 1
        end
    end

    if CMenPaiChuangGuanMgr.Inst.TargetPlayerRankInfo ~= nil then
        CommonDefs.ListAdd(this.rankDataList, typeof(CChuangGuanRankInfo), CMenPaiChuangGuanMgr.Inst.TargetPlayerRankInfo)
    end
    CommonDefs.ListAddRange(this.rankDataList, CMenPaiChuangGuanMgr.Inst.Ranklist)
    --  ButtonInfoArea.gameObject.SetActive(true);
    local bestTime = 0
    if CMenPaiChuangGuanMgr.Inst.CurMenPaiID > 0 then
        local data = CommonDefs.ListFind(CMenPaiChuangGuanMgr.Inst.SelfRankInfo.singleCompletionTimes, typeof(CompletionTimeInfo), DelegateFactory.Predicate_CompletionTimeInfo(function (x) 
            return x.menpaiId == CMenPaiChuangGuanMgr.Inst.CurMenPaiID
        end))
        if data ~= nil then
            bestTime = data.bestCompletionTime
        end
        --   bestTime = CMenPaiChuangGuanMgr.Inst.SelfRankInfo.singleCompletionTimes[(int)(CMenPaiChuangGuanMgr.Inst.CurMenPaiID - 1)].bestCompletionTime;
    else
        bestTime = CMenPaiChuangGuanMgr.Inst.SelfRankInfo.totalCompletionTime
    end

    local str = LocalString.GetString("暂无")
    if bestTime > 0 then
        str = CUICommonDef.SecondsToTimeString(bestTime, true)
    end
    this.BestTimeLabel.text = System.String.Format(LocalString.GetString("历史最佳：{0}"), str)


    str = LocalString.GetString("暂无")
    local targetTime = 0
    if CMenPaiChuangGuanMgr.Inst.TargetPlayerRankInfo ~= nil then
        if CMenPaiChuangGuanMgr.Inst.CurMenPaiID > 0 then
            local data = CommonDefs.ListFind(CMenPaiChuangGuanMgr.Inst.SelfRankInfo.singleCompletionTimes, typeof(CompletionTimeInfo), DelegateFactory.Predicate_CompletionTimeInfo(function (x) 
                return x.menpaiId == CMenPaiChuangGuanMgr.Inst.CurMenPaiID
            end))
            if data ~= nil then
                targetTime = data.bestCompletionTime
            end
            --    targetTime = CMenPaiChuangGuanMgr.Inst.TargetPlayerRankInfo.singleCompletionTimes[(int)(CMenPaiChuangGuanMgr.Inst.CurMenPaiID - 1)].bestCompletionTime;
        else
            targetTime = CMenPaiChuangGuanMgr.Inst.TargetPlayerRankInfo.totalCompletionTime
        end
    end
    if targetTime > 0 then
        str = CUICommonDef.SecondsToTimeString(targetTime, true)
    end
    this.TargetTimeLabel.text = System.String.Format(LocalString.GetString("争取超越的目标：{0}"), str)


    local info = CMenPaiChuangGuanMgr.Inst.SelfRankInfo
    local timeList = CreateFromClass(MakeGenericClass(List, Int32))
    local rankList = CreateFromClass(MakeGenericClass(List, UInt32))
    do
        local i = 0
        while i < info.singleCompletionTimes.Count do
            CommonDefs.ListAdd(timeList, typeof(Int32), info.singleCompletionTimes[i].bestCompletionTime)
            CommonDefs.ListAdd(rankList, typeof(UInt32), info.singleCompletionTimes[i].rank)
            i = i + 1
        end
    end
    this.SelfRankItem:SetRankInfo(info.playerRank, info.playerName, timeList, info.totalCompletionTime, true, rankList)
    this.advGridView:ReloadData(false, false)
end
CChuangGuanRankWnd.m_ItemAt_CS2LuaHook = function (this, view, row) 
    local cmp = TypeAs(view:GetFromPool(0), typeof(CChuangGuanRankItem))
    if this.rankDataList.Count > 0 then
        local info = this.rankDataList[row]
        local timeList = CreateFromClass(MakeGenericClass(List, Int32))
        do
            local i = 0
            while i < info.singleCompletionTimes.Count do
                CommonDefs.ListAdd(timeList, typeof(Int32), info.singleCompletionTimes[i].bestCompletionTime)
                i = i + 1
            end
        end
        cmp:SetRankInfo(info.playerRank, info.playerName, timeList, info.totalCompletionTime, false, nil)
        if CMenPaiChuangGuanMgr.Inst.TargetPlayerRankInfo ~= nil and row == 0 then
            cmp.MarkSprite:SetActive(true)
        else
            cmp.MarkSprite:SetActive(false)
        end
    end
    return cmp
end
