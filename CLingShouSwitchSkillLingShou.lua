-- Auto Generated!!
local CLingShouBaseMgr = import "L10.Game.CLingShouBaseMgr"
local CLingShouMgr = import "L10.Game.CLingShouMgr"
local CLingShouSwitchSkillLingShou = import "L10.UI.CLingShouSwitchSkillLingShou"
local CLingShouSwitchSkillMgr = import "L10.Game.CLingShouSwitchSkillMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local LocalString = import "LocalString"
local UIEventListener = import "UIEventListener"
local UILabel = import "UILabel"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CLingShouSwitchSkillLingShou.m_Init_CS2LuaHook = function (this, id) 
    this.lingshouId = id

    local data = CLingShouMgr.Inst:GetLingShouOverview(id)
    if data ~= nil then
        this.nameLabel.text = data.name
        this.icon:LoadNPCPortrait(CLingShouBaseMgr.GetLingShouIcon(data.templateId), false)
        this.natureLabel.text = LocalString.GetString("性格") .. data.Nature
    end
    this.btn:SetActive(false)


    if this.isFrom then
        CLingShouSwitchSkillMgr.Inst.LingShouSkills1 = nil
    else
        CLingShouSwitchSkillMgr.Inst.LingShouSkills2 = nil
    end
    CLingShouMgr.Inst:RequestLingShouDetails(this.lingshouId)
end
CLingShouSwitchSkillLingShou.m_InitBtn_CS2LuaHook = function (this) 
    if CLingShouSwitchSkillMgr.Inst.SwitchAdvanceSkill then
        if this.isFrom then
            this.btn:SetActive(true)
            CommonDefs.GetComponentInChildren_GameObject_Type(this.btn, typeof(UILabel)).text = LocalString.GetString("技能选择")
            UIEventListener.Get(this.btn).onClick = MakeDelegateFromCSFunction(this.OpenWnd, VoidDelegate, this)
        else
            this.btn:SetActive(false)
        end
    else
        if this.isFrom then
            this.btn:SetActive(false)
        else
            this.btn:SetActive(true)
            CommonDefs.GetComponentInChildren_GameObject_Type(this.btn, typeof(UILabel)).text = LocalString.GetString("技能锁定")
            UIEventListener.Get(this.btn).onClick = MakeDelegateFromCSFunction(this.OpenWnd, VoidDelegate, this)
        end
    end
end
CLingShouSwitchSkillLingShou.m_OpenWnd_CS2LuaHook = function (this, go) 
    --打开选择界面
    if this.lingshouId ~= nil then
        CUIManager.ShowUI(CUIResources.LingShouSkillSwitchSelectWnd)
    end
end
CLingShouSwitchSkillLingShou.m_UpdateLingShouSkillProp_CS2LuaHook = function (this, pLingshouId, skills) 
    if this.lingshouId == pLingshouId then
        this.slots:InitSlots(skills)
    end
end
CLingShouSwitchSkillLingShou.m_GetLingShouDetails_CS2LuaHook = function (this, lingShouId, details) 
    if lingShouId == this.lingshouId then
        --更新界面
        if details ~= nil then
            --加载技能
            this.slots:InitSlots(details.data.Props.SkillListForSave)
            if this.isFrom then
                CLingShouSwitchSkillMgr.Inst.LingShouSkills1 = details.data.Props.SkillListForSave
                if CLingShouSwitchSkillMgr.Inst.CanLockCount < CLingShouSwitchSkillMgr.Inst.LockedSkillCount then
                    --清空lock数量
                    CLingShouSwitchSkillMgr.Inst:CancleLock()
                end
            else
                CLingShouSwitchSkillMgr.Inst.LingShouSkills2 = details.data.Props.SkillListForSave
            end
        else
            this.slots:InitNull()
        end

        --消息过来之后再显示
        this:InitBtn()
    end
end
