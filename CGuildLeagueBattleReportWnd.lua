-- Auto Generated!!
local CGuildLeagueBattleReportItem = import "L10.UI.CGuildLeagueBattleReportItem"
local CGuildLeagueBattleReportWnd = import "L10.UI.CGuildLeagueBattleReportWnd"
local CGuildLeagueMgr = import "L10.Game.CGuildLeagueMgr"
local LocalString = import "LocalString"
CGuildLeagueBattleReportWnd.m_Init_CS2LuaHook = function (this) 
    local info = CGuildLeagueMgr.Inst.battleHistory
    local winRatio = 0
    if info.guildJoinCount > 0 then
        winRatio = info.guildWinCount / info.guildJoinCount * 100
    end
    this.guildInfoLabel.text = CommonDefs.String_Format_String_ArrayObject(LocalString.GetString("共进行联赛[00ff00]{0}[-]场\n总胜率[00ff00]{1:f2}[-]%（{2}胜{3}败）"), {info.guildJoinCount, winRatio, info.guildWinCount, info.guildJoinCount - info.guildWinCount})
    winRatio = 0
    if info.playerJoinCount > 0 then
        winRatio = info.playerWinCount / info.playerJoinCount * 100
    end
    this.personalInfoLabel.text = CommonDefs.String_Format_String_ArrayObject(LocalString.GetString("共进行联赛[00ff00]{0}[-]场\n总胜率[00ff00]{1:f2}[-]%（{2}胜{3}败）"), {info.playerJoinCount, winRatio, info.playerWinCount, info.playerJoinCount - info.playerWinCount})

    this.table.m_DataSource = this
    this.table:ReloadData(true, true)
end
CGuildLeagueBattleReportWnd.m_ItemAt_CS2LuaHook = function (this, view, row) 
    local cmp = this.table:GetFromPool(0)
    if cmp ~= nil then
        local item = CommonDefs.GetComponent_GameObject_Type(cmp.gameObject, typeof(CGuildLeagueBattleReportItem))
        item:Init(CGuildLeagueMgr.Inst.battleHistory.historyInfos[row])
    end
    return cmp
end
