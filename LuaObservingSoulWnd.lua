local Object = import "System.Object"
local ParticleSystem = import "UnityEngine.ParticleSystem"
local UIWidget = import "UIWidget"
local MsgPackImpl = import "MsgPackImpl"

LuaObservingSoulWnd = class()

RegistChildComponent(LuaObservingSoulWnd, "m_Fx1","Fx1", CUIFx)
RegistChildComponent(LuaObservingSoulWnd, "m_Fx2","Fx2", CUIFx)
RegistChildComponent(LuaObservingSoulWnd, "m_Fx3","Fx3", CUIFx)
RegistChildComponent(LuaObservingSoulWnd, "m_Fx4","Fx4", CUIFx)
RegistChildComponent(LuaObservingSoulWnd, "m_Fx5","Fx5", CUIFx)
RegistChildComponent(LuaObservingSoulWnd, "m_ScreenFx","ScreenFx", CUIFx)
RegistChildComponent(LuaObservingSoulWnd, "m_Texture1","Texture1", UITexture)
RegistChildComponent(LuaObservingSoulWnd, "m_Texture2","Texture2", UITexture)
RegistChildComponent(LuaObservingSoulWnd, "m_Character","Character", CUITexture)

RegistClassMember(LuaObservingSoulWnd, "m_FxList")
RegistClassMember(LuaObservingSoulWnd, "m_FxWidgetList")
RegistClassMember(LuaObservingSoulWnd, "m_RightDesire")
RegistClassMember(LuaObservingSoulWnd, "m_CloseWndTick")
RegistClassMember(LuaObservingSoulWnd, "m_IsFinished")
RegistClassMember(LuaObservingSoulWnd, "m_Symobol")
RegistClassMember(LuaObservingSoulWnd, "m_AimSymobol")

function LuaObservingSoulWnd:Init()
    self.m_FxList = {self.m_Fx1,self.m_Fx2,self.m_Fx3,self.m_Fx4,self.m_Fx5}
    self.m_Texture1.gameObject:SetActive(false)
    self.m_Texture2.gameObject:SetActive(false)
    local data = ZhuJueJuQing_SoulObserved.GetData(LuaObservingSoulMgr.m_TaskID)
    self.m_Character:LoadMaterial(data.CharacterPicture)
    LuaObservingSoulMgr:LoadObservingSoulData()
    if (not data) or (not LuaObservingSoulMgr.m_ObservingSoulData) then
        CUIManager.CloseUI(CLuaUIResources.ObservingSoulWnd)
    end
    self.m_RightDesire = tonumber(data.RightDesire)
    self.m_Symobol,self.m_AimSymobol = 0,0
    self.m_FxWidgetList = {}
    for index,fx in pairs(self.m_FxList) do
        table.insert(self.m_FxWidgetList, fx.transform.parent:GetComponent(typeof(UIWidget)))
        fx.OnLoadFxFinish = DelegateFactory.Action(function()
            self:OnLoadFxFinish(index, true)
        end)
        local scale = LuaObservingSoulMgr.m_ObservingSoulData[index].scale
        if index ~= self.m_RightDesire and scale ~= 0 then
            self.m_AimSymobol = bit.bor(bit.lshift(1,index), self.m_AimSymobol)
        end
        fx:LoadFx("fx/ui/prefab/UI_linghunguancha_huo03.prefab")
        UIEventListener.Get(fx.transform.parent.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            self:OnFxClick(index)
        end)
    end
end

function LuaObservingSoulWnd:OnEnable()
    g_ScriptEvent:AddListener("FinishObservingSoulTask",self,"OnFinishObservingSoulTask")
end

function LuaObservingSoulWnd:OnDisable()
    g_ScriptEvent:RemoveListener("FinishObservingSoulTask",self,"OnFinishObservingSoulTask")
    self:CancelCloseWndTip()
    CUIManager.CloseUI(CLuaUIResources.ObservingSoulDetailTipWnd)
end

function LuaObservingSoulWnd:OnFinishObservingSoulTask()
    self.m_IsFinished = true
    self.m_ScreenFx.OnLoadFxFinish = DelegateFactory.Action(function()
        self:OnLoadScreenFxFinish()
    end)
    self.m_ScreenFx:LoadFx("fx/ui/prefab/UI_linghunguancha_huo.prefab")
    self.m_CloseWndTick = RegisterTickOnce(function ()
        local empty = CreateFromClass(MakeGenericClass(List, Object))
        Gac2Gas.FinishClientTaskEvent(LuaObservingSoulMgr.m_TaskID,"ObservingSoul",MsgPackImpl.pack(empty))
        CUIManager.CloseUI(CLuaUIResources.ObservingSoulDetailTipWnd)
        CUIManager.CloseUI(CLuaUIResources.ObservingSoulWnd)
    end,3000)
end

function LuaObservingSoulWnd:CancelCloseWndTip()
    if self.m_CloseWndTick then
        UnRegisterTick(self.m_CloseWndTick)
        self.m_CloseWndTick = nil
    end
end

function LuaObservingSoulWnd:OnFxClick(index)
    if self.m_IsFinished then return end
    if(bit.band(self.m_Symobol, bit.bor(bit.lshift(1,index))) == 0) then
        local particleSystems = CommonDefs.GetComponentsInChildren_GameObject_Type_Boolean(self.m_FxList[index].gameObject, typeof(ParticleSystem),true)
        for i = 0, particleSystems.Length - 1 do
            local color = LuaObservingSoulMgr.m_ObservingSoulData[index].color
            color.a = 1
            particleSystems[i].startColor = color
            particleSystems[i]:Stop()
            particleSystems[i]:Play()
        end
    end
    self.m_Symobol = bit.bor(bit.lshift(1,index), self.m_Symobol)
    LuaObservingSoulMgr.m_SelectIndex = index
    LuaObservingSoulMgr.m_TipWndPos = self.m_FxWidgetList[index].transform.position
    LuaObservingSoulMgr.m_IsRightDesire = (self.m_RightDesire == index)
    CUIManager.ShowUI(CLuaUIResources.ObservingSoulDetailTipWnd)
    if self.m_RightDesire == index then return end
    if self.m_AimSymobol == self.m_Symobol then
        self.m_FxWidgetList[self.m_RightDesire].gameObject:SetActive(true)
    end
end

function LuaObservingSoulWnd:OnLoadFxFinish(index, isfixalpha)
    local pos = self.m_FxList[index].transform.localPosition
    local scale = LuaObservingSoulMgr.m_ObservingSoulData[index].scale
    local particleSystems = CommonDefs.GetComponentsInChildren_GameObject_Type_Boolean(self.m_FxList[index].gameObject, typeof(ParticleSystem),true)
    if scale ~= 0 then
        for i = 0, particleSystems.Length - 1 do
            CommonDefs.SetParticleSystemScale(particleSystems[i], CommonDefs.GetParticleSystemScale(particleSystems[i]) * scale)
            particleSystems[i].startSize = particleSystems[i].startSize * scale
            local color = LuaObservingSoulMgr.m_ObservingSoulData[index].color
            color.a = isfixalpha and ((self.m_RightDesire == index) and 1 or 0.5) or 1
            particleSystems[i].startColor = color
            particleSystems[i]:Stop()
            particleSystems[i]:Play()
        end
        self.m_FxList[index].transform.localPosition = Vector3(pos.x * scale, pos.y * scale, pos.z)
        self.m_FxWidgetList[index].width = self.m_FxWidgetList[index].width * scale
        self.m_FxWidgetList[index].height = self.m_FxWidgetList[index].height * scale
    end
    self.m_FxWidgetList[index].gameObject:SetActive(index ~= self.m_RightDesire and scale ~= 0)
end

function LuaObservingSoulWnd:OnLoadScreenFxFinish()
    local go = self.m_ScreenFx.FxGo
    go.transform:Find("Panel002").gameObject:SetActive(false)
    go.transform:Find("Panel003").gameObject:SetActive(false)
    local color =  LuaObservingSoulMgr.m_ObservingSoulData[self.m_RightDesire].color
    local particleSystems = { go.transform:Find("Particle004"):GetComponent(typeof(ParticleSystem)), go.transform:Find("inghunguancha_huodonghua/Particle006"):GetComponent(typeof(ParticleSystem))}
    for _,particleSystem in pairs((particleSystems)) do
        particleSystem.startColor = color
        particleSystem:Stop()
        particleSystem:Play()
    end
    self.m_Texture1.color = color
    self.m_Texture2.color = color
    self.m_Texture1.gameObject:SetActive(true)
    self.m_Texture2.gameObject:SetActive(true)
end

LuaObservingSoulMgr = class()
LuaObservingSoulMgr.m_TipWndPos = Vector3.zero
LuaObservingSoulMgr.m_SelectIndex = 0
LuaObservingSoulMgr.m_IsRightDesire = false
LuaObservingSoulMgr.m_TaskID = 0
LuaObservingSoulMgr.m_ObservingSoulData = {}

function LuaObservingSoulMgr:LoadObservingSoulData()
    local data = ZhuJueJuQing_SoulObserved.GetData(self.m_TaskID)
    if not data then return end
    local desires = g_LuaUtil:StrSplit(data.Desire,";")
    self.m_ObservingSoulData = {}
    for i = 1, 5 do
        local desire = desires[i]
        local info = g_LuaUtil:StrSplit(desire,",")
        local t = {name = info[2],msg = info[3],color = info[4], scale = info[5]}
        local isNull = cs_string.IsNullOrEmpty(info[3])
        t.color = isNull and Color(0,0,0,0) or NGUIText.ParseColor32(string.sub(t.color, 2,9),0)
        t.scale = isNull and 0 or tonumber(t.scale)
        table.insert(self.m_ObservingSoulData, t)
    end
end