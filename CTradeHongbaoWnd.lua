-- Auto Generated!!
local CChatMgr = import "L10.Game.CChatMgr"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CShopMallMgr = import "L10.UI.CShopMallMgr"
local CTradeHongbaoWnd = import "L10.UI.CTradeHongbaoWnd"
local CTradeMgr = import "L10.Game.CTradeMgr"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local Gac2Gas = import "L10.Game.Gac2Gas"
local HongBao_Setting = import "L10.Game.HongBao_Setting"
local LocalString = import "LocalString"
local MessageWndManager = import "L10.UI.MessageWndManager"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CTradeHongbaoWnd.m_Init_CS2LuaHook = function (this) 
    this.m_JadeButton:SetMinMax(HongBao_Setting.GetData().Value_Min_Personal, HongBao_Setting.GetData().Value_Max_Personal, 1)

    this.m_PresentTotal.text = tostring(CTradeMgr.Inst.presentTotalNum)
    if CTradeMgr.Inst.saveHongbaoNum > 0 then
        this.m_JadeButton:SetValue(CTradeMgr.Inst.saveHongbaoNum, true)
    else
        this.m_JadeButton:SetValue(CTradeHongbaoWnd.DefaultValue, true)
    end

    if not System.String.IsNullOrEmpty(CTradeMgr.Inst.saveHongbaoText) then
        this.m_WishInput.Text = CTradeMgr.Inst.saveHongbaoText
    else
        this.m_WishLabel.text = System.String.Format(LocalString.GetString("请输入红包留言（最多{0}个字）"), HongBao_Setting.GetData().PersonalHongBao_ContentLimit)
    end
end
CTradeHongbaoWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.m_CloseButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_CloseButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.m_SendHongbaoBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_SendHongbaoBtn).onClick, MakeDelegateFromCSFunction(this.OnSendHongbaoBtnClick, VoidDelegate, this), true)
    UIEventListener.Get(this.m_TipBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_TipBtn).onClick, MakeDelegateFromCSFunction(this.OnTipBtnClick, VoidDelegate, this), true)
    this.m_JadeButton.onValueChanged = CommonDefs.CombineListner_Action_uint(this.m_JadeButton.onValueChanged, MakeDelegateFromCSFunction(this.OnJadeValueChanged, MakeGenericClass(Action1, UInt32), this), true)
end
CTradeHongbaoWnd.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.m_CloseButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_CloseButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.m_SendHongbaoBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_SendHongbaoBtn).onClick, MakeDelegateFromCSFunction(this.OnSendHongbaoBtnClick, VoidDelegate, this), false)
    UIEventListener.Get(this.m_TipBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.m_TipBtn).onClick, MakeDelegateFromCSFunction(this.OnTipBtnClick, VoidDelegate, this), false)
    this.m_JadeButton.onValueChanged = CommonDefs.CombineListner_Action_uint(this.m_JadeButton.onValueChanged, MakeDelegateFromCSFunction(this.OnJadeValueChanged, MakeGenericClass(Action1, UInt32), this), false)
end
CTradeHongbaoWnd.m_OnOkButtonClick_CS2LuaHook = function (this) 
    if CShopMallMgr.TryCheckEnoughJade(this.m_CurentMoneyCtrl:GetCost(), 0) then
        local presentCost = System.UInt32.Parse(this.m_PresentCost.text)
        if presentCost <= CTradeMgr.Inst.presentTotalNum then
            local lingyuAmount = this.m_JadeButton:GetValue()
            local filteredContent = CChatMgr.Inst:FilterNewLine(this.m_WishContents, 0, " ")
            CTradeMgr.Inst.saveHongbaoText = filteredContent
            CTradeMgr.Inst.saveHongbaoNum = lingyuAmount
            Gac2Gas.RequestSendPersonalHongBao(CTradeMgr.Inst.targetPlayerId, lingyuAmount, filteredContent)
        else
            g_MessageMgr:ShowMessage("PersonalHongBao_JiFen_Not_Enough")
        end
    end
end
CTradeHongbaoWnd.m_OnSendHongbaoBtnClick_CS2LuaHook = function (this, button) 
    this.m_WishContents = this.m_WishInput.Text
    if CommonDefs.StringLength(this.m_WishContents) > HongBao_Setting.GetData().PersonalHongBao_ContentLimit then
        g_MessageMgr:ShowMessage("Hongbao_Input_Limit", HongBao_Setting.GetData().PersonalHongBao_ContentLimit)
        return
    end

    if CommonDefs.StringLength(this.m_WishContents) > 0 then
        local ret = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(this.m_WishContents, true)
        if ret.msg == nil then
            return
        else
            this.m_WishContents = ret.msg
        end
    else
        this.m_WishContents = LocalString.GetString("恭喜发财")
    end

    local obj = CClientObjectMgr.Inst:GetObject(CTradeMgr.Inst.targetEngineId)
    if obj ~= nil then
        local text = (((LocalString.GetString("是否花费") .. this.m_JadeButton:GetValue()) .. LocalString.GetString("灵玉给")) .. obj.Name) .. LocalString.GetString("发红包？")
        MessageWndManager.ShowOKCancelMessage(text, MakeDelegateFromCSFunction(this.OnOkButtonClick, Action0, this), nil, nil, nil, false)
    else
        g_MessageMgr:ShowMessage("PersonalHongBao_Target_Not_Same_Scene")
        this:Close()
    end
end
