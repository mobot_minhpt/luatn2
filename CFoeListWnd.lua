-- Auto Generated!!
local CFoeListMgr = import "L10.UI.CFoeListMgr"
local CFoeListWnd = import "L10.UI.CFoeListWnd"
local CIMMgr = import "L10.Game.CIMMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CRelationshipBasicPlayerInfo = import "L10.Game.CRelationshipBasicPlayerInfo"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local Int32 = import "System.Int32"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CFoeListWnd.m_Init_CS2LuaHook = function (this) 
    this.nameLabel.text = ""
    this.tips:SetActive(true)
    Extensions.SetLocalRotationZ(this.arrowTransform, 0)
    this.selectIndex = - 1
end
CFoeListWnd.m_OnEnable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this), true)
    UIEventListener.Get(this.okButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.okButton).onClick, MakeDelegateFromCSFunction(this.OnOkButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.cancelButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.cancelButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), true)
    UIEventListener.Get(this.selectButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.selectButton).onClick, MakeDelegateFromCSFunction(this.OnSelectButtonClick, VoidDelegate, this), true)
    EventManager.AddListenerInternal(EnumEventType.OnFoeSelect, MakeDelegateFromCSFunction(this.OnFoeSelect, MakeGenericClass(Action1, Int32), this))
end
CFoeListWnd.m_OnDisable_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this), false)
    UIEventListener.Get(this.okButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.okButton).onClick, MakeDelegateFromCSFunction(this.OnOkButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.cancelButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.cancelButton).onClick, MakeDelegateFromCSFunction(this.OnCloseButtonClick, VoidDelegate, this), false)
    UIEventListener.Get(this.selectButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.selectButton).onClick, MakeDelegateFromCSFunction(this.OnSelectButtonClick, VoidDelegate, this), false)
    EventManager.RemoveListenerInternal(EnumEventType.OnFoeSelect, MakeDelegateFromCSFunction(this.OnFoeSelect, MakeGenericClass(Action1, Int32), this))
end
CFoeListWnd.m_OnFoeSelect_CS2LuaHook = function (this, index) 
    Extensions.SetLocalRotationZ(this.arrowTransform, 0)
    if index >= 0 and index < CFoeListMgr.foeList.Count then
        this.selectIndex = index
        this.nameLabel.text = CFoeListMgr.foeList[index].Name
        this.tips:SetActive(false)
    end
end
CFoeListWnd.m_OnSelectButtonClick_CS2LuaHook = function (this, go) 
    if CFoeListMgr.foeList.Count > 0 then
        Extensions.SetLocalRotationZ(this.arrowTransform, 180)
        CUIManager.ShowUI(CUIResources.FoeSelectWnd)
    else
        g_MessageMgr:ShowMessage("HAVE_NO_CHOUDI")
    end
end
CFoeListWnd.m_OnOkButtonClick_CS2LuaHook = function (this, go) 
    if this.selectIndex < 0 then
        g_MessageMgr:ShowMessage("PLEASE_CHOOSE_A_CHOUDI")
    else
        Gac2Gas.SetDuoHunFan(EnumToInt(CFoeListMgr.duohunItem.place), CFoeListMgr.duohunItem.pos, CFoeListMgr.foeList[this.selectIndex].ID)
        this:Close()
    end
end
CFoeListMgr.m_OpenFoeListWnd_CS2LuaHook = function (item) 
    CFoeListMgr.duohunItem = item
    if CFoeListMgr.foeList == nil then
        CFoeListMgr.foeList = CreateFromClass(MakeGenericClass(List, CRelationshipBasicPlayerInfo))
    else
        CommonDefs.ListClear(CFoeListMgr.foeList)
    end
    local enemies = CIMMgr.Inst.Enemies
    CommonDefs.EnumerableIterate(enemies, DelegateFactory.Action_object(function (___value) 
        local playerId = ___value
        local player = CIMMgr.Inst:GetBasicInfo(playerId)
        CommonDefs.ListAdd(CFoeListMgr.foeList, typeof(CRelationshipBasicPlayerInfo), player)
    end))
    CUIManager.ShowUI(CUIResources.FoeListWnd)
end
