local CSocialWndMgr = import "L10.UI.CSocialWndMgr"
local EChatPanel = import "L10.Game.EChatPanel"

local PlayerSettings=import "L10.Game.PlayerSettings"
local CGuideMgr=import "L10.Game.Guide.CGuideMgr"

LuaCommonChatEntranceButton = class()

--@region RegistChildComponent: Dont Modify Manually!


--@endregion RegistChildComponent end

function LuaCommonChatEntranceButton:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    UIEventListener.Get(self.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)  
        --可能会打开竖屏聊天框，引导的时候强制打开横屏，简化引导逻辑
        local preSetting = nil
        if CGuideMgr.Inst:IsInPhase(EnumGuideKey.GamePlayConfirmChat) then
            preSetting = PlayerSettings.UsePortraitSocialWnd
            PlayerSettings.UsePortraitSocialWnd = false
        end
        --action
        if LuaCommonChatEntranceButtonMgr.ShowChatWndCallback then
            LuaCommonChatEntranceButtonMgr.ShowChatWndCallback()
        else
            CSocialWndMgr.ShowChatWnd(EChatPanel.Team)
        end
        --action end
        if preSetting then
            PlayerSettings.UsePortraitSocialWnd = preSetting
        end
	end)
    CLuaGuideMgr.TryTriggerGamePlayConfirmChatGuide()
end

function LuaCommonChatEntranceButton:OnDisable()
    LuaCommonChatEntranceButtonMgr.ShowChatWndCallback = nil
    CUIManager.CloseUI(CUIResources.SocialWnd)
end

LuaCommonChatEntranceButtonMgr = {}
LuaCommonChatEntranceButtonMgr.ShowChatWndCallback = nil
--@region UIEvent

--@endregion UIEvent

