local UISprite = import "UISprite"
local UILabel = import "UILabel"

local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CUITexture = import "L10.UI.CUITexture"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"

LuaZhanLingLevelRewardItem = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaZhanLingLevelRewardItem, "Progress", "Progress", UISprite)
RegistChildComponent(LuaZhanLingLevelRewardItem, "Level", "Level", UILabel)
RegistChildComponent(LuaZhanLingLevelRewardItem, "Reward1", "Reward1", GameObject)
RegistChildComponent(LuaZhanLingLevelRewardItem, "Reward2", "Reward2", GameObject)
RegistChildComponent(LuaZhanLingLevelRewardItem, "Reward3", "Reward3", GameObject)
RegistChildComponent(LuaZhanLingLevelRewardItem, "HighLight", "HighLight", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaZhanLingLevelRewardItem,"m_CurLevel")
function LuaZhanLingLevelRewardItem:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.Reward1.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnReward1Click()
	end)


	
	UIEventListener.Get(self.Reward2.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnReward2Click()
	end)


	
	UIEventListener.Get(self.Reward3.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnReward3Click()
	end)


    --@endregion EventBind end
end
function LuaZhanLingLevelRewardItem:OnEnable()
    g_ScriptEvent:AddListener("ReceiveLianDanLuRewardSuccess", self, "OnReceiveLianDanLuRewardSuccess")

end

function LuaZhanLingLevelRewardItem:OnDisable()
    g_ScriptEvent:RemoveListener("ReceiveLianDanLuRewardSuccess", self, "OnReceiveLianDanLuRewardSuccess")

end
function LuaZhanLingLevelRewardItem:Init(level,flag)
    local curLevel,totalProgress = LuaHuLuWa2022Mgr.GetCurZhanLingLevel()
    self.m_CurLevel = curLevel

    self.Level.text = level
    self:InitRewardItem(self.Reward1,1,level,flag)
    self:InitRewardItem(self.Reward2,2,level,flag)
    self:InitRewardItem(self.Reward3,3,level,flag)

    --progress
    local curLevelP = LuaHuLuWa2022Mgr.m_Level2NeedProgress[curLevel]
    local nextLevelP = LuaHuLuWa2022Mgr.m_Level2NeedProgress[curLevel+1]
    if not curLevelP then
        curLevelP = 0
    end
    if not nextLevelP then
        nextLevelP = curLevelP + (curLevelP -LuaHuLuWa2022Mgr.m_Level2NeedProgress[curLevel-1])
    end

    if level < curLevel then
        self.Progress.fillAmount = 1
    elseif level == curLevel then
        self.Progress.fillAmount = math.min(1,0.5 + (totalProgress - curLevelP) / (nextLevelP-curLevelP))
    elseif level == curLevel + 1 then
        self.Progress.fillAmount = math.min(0.5,(totalProgress - curLevelP) / (nextLevelP-curLevelP)-0.5)
    else
        self.Progress.fillAmount = 0
    end
    --进行中
    self.HighLight:SetActive(level == self.m_CurLevel)
end

function LuaZhanLingLevelRewardItem:InitRewardItem(item,idx,level,flag)
    local icon = item.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local countLabel = item.transform:Find("CountLabel"):GetComponent(typeof(UILabel))
    local lock = item.transform:Find("Lock").gameObject
    local hasGotTag = item.transform:Find("HasGotTag").gameObject
    local mask = item.transform:Find("Mask").gameObject

    local data = ZhanLing_Reward.GetData(level)
    local rewards
    local isVipUnLock = true
    if idx == 1 then
        rewards = data.ItemRewards1
    elseif idx == 2 then
        rewards = data.ItemRewards2
        isVipUnLock = LuaHuLuWa2022Mgr.m_IsVip1
    elseif idx == 3 then
        rewards = data.ItemRewards3
        isVipUnLock = LuaHuLuWa2022Mgr.m_IsVip2
    end
    local itemId
    countLabel.text = nil
    if rewards then
        itemId = rewards[0]
        local count = rewards[1]
        local itemdata = Item_Item.GetData(itemId)
        icon:LoadMaterial(itemdata.Icon)
        countLabel.text = count > 1 and count or nil
    end

    --丹炉是否被解锁
    lock:SetActive(not isVipUnLock)

    local isUnlock = false
    local isAwarded = false
    if isVipUnLock then
        if level <= self.m_CurLevel then
            isUnlock = true
        end 
    end
    mask:SetActive(not isVipUnLock or not isUnlock)

    local bitmask = 2^(idx-1)
    local isGot = false
    if bit.band(flag,bitmask) ~= 0 then
        isGot = true
    else
        isGot = false
    end
    hasGotTag:SetActive(isGot)
    --可领取   
    if isUnlock and isVipUnLock and not isGot then
        UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            Gac2Gas.RequestGetLianDanLuReward(level,idx)
        end)
    else
        UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
            CItemInfoMgr.ShowLinkItemTemplateInfo(itemId,false,nil,AlignType.Default, 0, 0, 0, 0)
        end)
    end
end

function LuaZhanLingLevelRewardItem:OnReceiveLianDanLuRewardSuccess(level,idex)
    --self:InitRewardItem(self.Reward1,1,level,flag)
end
--@region UIEvent

function LuaZhanLingLevelRewardItem:OnReward1Click()
end

function LuaZhanLingLevelRewardItem:OnReward2Click()
end

function LuaZhanLingLevelRewardItem:OnReward3Click()
end


--@endregion UIEvent

