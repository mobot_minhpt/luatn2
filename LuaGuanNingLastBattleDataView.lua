local LuaGameObject=import "LuaGameObject"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local EnumClass=import "L10.Game.EnumClass"
local CServerTimeMgr=import "L10.Game.CServerTimeMgr"
local CPopupMenuInfoMgr=import "L10.UI.CPopupMenuInfoMgr"
local PopupMenuItemData=import "L10.UI.PopupMenuItemData"
local AlignType=import "L10.UI.CPopupMenuInfoMgr+AlignType"
local CSwitchMgr=import "L10.Engine.CSwitchMgr"

--本场表现
CLuaGuanNingLastBattleDataView=class()
RegistClassMember(CLuaGuanNingLastBattleDataView,"m_ParamNameTable")
RegistClassMember(CLuaGuanNingLastBattleDataView,"m_ShareButton")--网站
RegistClassMember(CLuaGuanNingLastBattleDataView,"m_ItemTemplate")
RegistClassMember(CLuaGuanNingLastBattleDataView,"m_AchievementItemTemplate")

RegistClassMember(CLuaGuanNingLastBattleDataView,"m_ItemGrid")
RegistClassMember(CLuaGuanNingLastBattleDataView,"m_AchievementItemGrid")
RegistClassMember(CLuaGuanNingLastBattleDataView,"m_AchievementTabel")
RegistClassMember(CLuaGuanNingLastBattleDataView,"m_ShareType")

function CLuaGuanNingLastBattleDataView:Init(tf)
    if not tf then return end
    self.m_ShareButton=LuaGameObject.GetChildNoGC(tf,"ShareButton").gameObject
    UIEventListener.Get(self.m_ShareButton).onClick = DelegateFactory.VoidDelegate(function(go)
        self:Share2Web(go)
    end)
    if CLuaGuanNingMgr.IsOnlyShare2PersonalSpace() then
        LuaGameObject.GetChildNoGC(self.m_ShareButton.transform,"Label").label.text=LocalString.GetString("分享至梦岛")
    end

    if not CSwitchMgr.EnableGuanNingShare then
        LuaGameObject.GetChildNoGC(tf,"ShareButton").gameObject:SetActive(false)
    end
    
    self.m_ItemTemplate=LuaGameObject.GetChildNoGC(tf,"ItemTemplate").gameObject
    self.m_ItemTemplate:SetActive(false)
    self.m_AchievementItemTemplate=LuaGameObject.GetChildNoGC(tf,"AchievementItemTemplate").gameObject
    self.m_AchievementItemTemplate:SetActive(false)

    if not CLuaGuanNingMgr.lastBattleData then return end
    self.m_ParamNameTable={LocalString.GetString("击杀"),LocalString.GetString("阵亡"),LocalString.GetString("助攻"),LocalString.GetString("复活")}

    local lastBattleData=CLuaGuanNingMgr.lastBattleData
    self.m_AchievementTabel={}
    for i=1,#lastBattleData.achieveInfo do
        self.m_AchievementTabel[lastBattleData.achieveInfo[i]]=true
    end

    local scoreLabel=LuaGameObject.GetChildNoGC(tf,"ScoreLabel").label
    if scoreLabel then scoreLabel.text=SafeStringFormat3(lastBattleData.score) end
    local bestScoreLabel=LuaGameObject.GetChildNoGC(tf,"LastBestScoreLabel").label
    if bestScoreLabel then bestScoreLabel.text=SafeStringFormat3(LocalString.GetString("本周最佳:%d"),lastBattleData.lastMaxPlayScore) end
    local rankLabel=LuaGameObject.GetChildNoGC(tf,"RankLabel").label
    if rankLabel then rankLabel.text=SafeStringFormat3(LocalString.GetString("本职业排名:%d"),lastBattleData.rank) end

    if lastBattleData.isAnQiDaoData then
        LuaGameObject.GetChildNoGC(tf,"LongyinLabel").label.text = LocalString.GetString("大福舰")
        LuaGameObject.GetChildNoGC(tf,"HuxiaoLabel").label.text = LocalString.GetString("海沧舰")
    end

    self.m_ItemGrid=LuaGameObject.GetChildNoGC(tf,"ItemGrid").grid
    self.m_ShareButton=LuaGameObject.GetChildNoGC(tf,"ShareButton").gameObject

    self.m_AchievementItemGrid=LuaGameObject.GetChildNoGC(tf,"AchievementItemGrid").grid
    local len=3
    if CClientMainPlayer.Inst and CClientMainPlayer.Inst.Class==EnumClass.YiShi then
        len=4
    end

    CUICommonDef.ClearTransform(self.m_ItemGrid.transform)
    local parent=self.m_ItemGrid.gameObject
    for i=1,len do
        local go=NGUITools.AddChild(parent,self.m_ItemTemplate)
        go:SetActive(true)
        self:InitItem(go,i)
    end
    CUICommonDef.ClearTransform(self.m_AchievementItemGrid.transform)
    parent=self.m_AchievementItemGrid.gameObject
    local achievements={}
    GuanNing_Performance.ForeachKey(function(k)
        local v = GuanNing_Performance.GetData(k)
        table.insert( achievements,v )
    end)

    table.sort( achievements, function(a, b)
        local v1=self.m_AchievementTabel[a.ID]
        local v2=self.m_AchievementTabel[b.ID]
        if v1 and not v2 then
            return true
        elseif not v1 and v2 then
            return false
        else
            return a.ID<b.ID
        end
    end)
    for k,v in pairs(achievements) do
        -- print(k,v)
        local go=NGUITools.AddChild(parent,self.m_AchievementItemTemplate)
        go:SetActive(true)
        self:InitAchievementItem(go,v)
    end

    LuaGameObject.GetChildNoGC(tf,"MaxScore").gameObject:SetActive(lastBattleData.isMaxScore)

    self:InitTime(tf)
    self:InitResult(tf)
    
    if lastBattleData.score==0 then
        CUICommonDef.SetActive(self.m_ShareButton,false,true)
    else
        CUICommonDef.SetActive(self.m_ShareButton,true,true)
    end
end
function CLuaGuanNingLastBattleDataView:InitTime(tf)
    local timeLabel=LuaGameObject.GetChildNoGC(tf,"TimeLabel").label
    local time = CServerTimeMgr.Inst:GetZone8Time()
    if timeLabel then timeLabel.text=SafeStringFormat3( "%s/%02d/%02d",time.Year,time.Month,time.Day ) end

    if not CLuaGuanNingMgr.lastBattleData then return end
    local lastBattleData=CLuaGuanNingMgr.lastBattleData
    time = CServerTimeMgr.ConvertTimeStampToZone8Time(lastBattleData.timeStamp)
    if timeLabel then timeLabel.text=SafeStringFormat3( "%s/%02d/%02d",time.Year,time.Month,time.Day ) end
end
function CLuaGuanNingLastBattleDataView:InitResult(tf)
    local resultSprite=LuaGameObject.GetChildNoGC(tf,"ResultSprite").sprite
    resultSprite.gameObject:SetActive(false)

    local longyinGo=LuaGameObject.GetChildNoGC(tf,"LongyinLabel").gameObject
    longyinGo:SetActive(false)
    local huxiaoGo=LuaGameObject.GetChildNoGC(tf,"HuxiaoLabel").gameObject
    huxiaoGo:SetActive(false)

    if not CLuaGuanNingMgr.lastBattleData then return end
    local lastBattleData=CLuaGuanNingMgr.lastBattleData

    resultSprite.gameObject:SetActive(true)
    if lastBattleData.result==0 then
        resultSprite.spriteName="common_fight_result_win"
    elseif lastBattleData.result==1 then
        resultSprite.spriteName="common_fight_result_lose"
    elseif lastBattleData.result==2 then
        resultSprite.spriteName="common_fight_result_tie"
    end
    if lastBattleData.force==0 then
        huxiaoGo:SetActive(true)
    elseif lastBattleData.force==1 then
        longyinGo:SetActive(true)
    end

end


function CLuaGuanNingLastBattleDataView:InitAchievementItem(go,data)
    local tf=go.transform
    local nameLabel=LuaGameObject.GetLuaGameObjectNoGC(tf).label
    local achieve=false
    if self.m_AchievementTabel[data.ID] then
        achieve=true
    end
    if not achieve then
        nameLabel.text=SafeStringFormat3("[c][767676]%s[-][/c]",data.Name)
        LuaGameObject.GetChildNoGC(tf,"DescLabel").label.text=SafeStringFormat3("[c][767676]%s[-][/c]",data.Desc)
    else
        nameLabel.text=data.Name
        LuaGameObject.GetChildNoGC(tf,"DescLabel").label.text=data.Desc
    end
end
function CLuaGuanNingLastBattleDataView:InitItem(go,index)
    local tf=go.transform
    local scoreLabel=LuaGameObject.GetChildNoGC(tf,"ScoreLabel").label
    --如果是阵亡，显示本月最高
    local bestLabel=LuaGameObject.GetChildNoGC(tf,"BestLabel").label

    local nameLabel=LuaGameObject.GetChildNoGC(tf,"NameLabel").label
    nameLabel.text=self.m_ParamNameTable[index]
    if not CLuaGuanNingMgr.lastBattleData then return end
    local lastBattleData=CLuaGuanNingMgr.lastBattleData
    local formatA=LocalString.GetString("本周最佳:%d")
    local formatB=LocalString.GetString("本周最高:%d")
    local upSprite = tf:Find("Highlight/UpSprite").gameObject
    upSprite:SetActive(false)
    local refreshRecord=false
    if index==1 then
        local killNum=lastBattleData.killNum
        local lastMaxPlayKillNum=lastBattleData.lastMaxPlayKillNum
        refreshRecord=killNum>lastMaxPlayKillNum
        upSprite:SetActive(CLuaGuanNingMgr.m_BreakRecords.killNumBreakRecord)
        scoreLabel.text=tostring(killNum)
        bestLabel.text=SafeStringFormat3(formatA,lastMaxPlayKillNum)
    elseif index==2 then
        local dieNum=lastBattleData.dieNum
        local lastMaxPlayDieNum=lastBattleData.lastMaxPlayDieNum
        refreshRecord=dieNum>lastMaxPlayDieNum
        upSprite:SetActive(CLuaGuanNingMgr.m_BreakRecords.dieNumBreakRecord)
        scoreLabel.text=tostring(dieNum)
        bestLabel.text=SafeStringFormat3(formatB,lastMaxPlayDieNum)
    elseif index==3 then
        local helpNum=lastBattleData.helpNum
        local lastMaxPlayHelpNum=lastBattleData.lastMaxPlayHelpNum
        refreshRecord=helpNum>lastMaxPlayHelpNum
        upSprite:SetActive(CLuaGuanNingMgr.m_BreakRecords.helpNumBreakRecord)
        scoreLabel.text=tostring(lastBattleData.helpNum)
        bestLabel.text=SafeStringFormat3(formatA,lastBattleData.lastMaxPlayHelpNum)
    elseif index==4 then
        local reliveNum=lastBattleData.reliveNum
        local lastMaxPlayReliveNum=lastBattleData.lastMaxPlayReliveNum
        refreshRecord=reliveNum>lastMaxPlayReliveNum
        upSprite:SetActive(CLuaGuanNingMgr.m_BreakRecords.reliveNumBreakRecord)
        scoreLabel.text=tostring(lastBattleData.reliveNum)
        bestLabel.text=SafeStringFormat3(formatA,lastBattleData.lastMaxPlayReliveNum)
    end

    LuaGameObject.GetChildNoGC(tf,"Highlight").gameObject:SetActive(refreshRecord)
end

--https://ssl.hi.163.com/file_mg/public/qnm/guanningshare/data?gnid=13675863363&grade=129&rolename=玩家名称七个字&roleid=100100021&gander=1&server=西子湖&camp=0&professionid=7&ctime=1490927069&result=1&winstreaknum=12&allscore=500&gamebest=1&teambest=1&rank=1&kill=123&die=12&assist=12&reborn=0&achievement=1,4,6,8
function CLuaGuanNingLastBattleDataView:Share2Web(go)
    --分享到网站
    -- CGuanNingMgr.Inst:Share2Web()
    --渠道服
    if CLuaGuanNingMgr.IsOnlyShare2PersonalSpace() then
        -- self.m_ShareType=0
        -- CGuanNingMgr.Inst:Share2Web()
        -- CUICommonDef.SetActive(self.m_ShareButton,false,true)
        local isAnqidao = CLuaGuanNingMgr.lastBattleData.isAnQiDaoData 

        CLuaGuanNingMgr.Share2Web(1, isAnqidao)
    else
        self:ShowPopupMenu(go)
    end
end

function CLuaGuanNingLastBattleDataView:ShowPopupMenu(go)
    --如果没有搜索内容
    local function SelectAction(index)
        -- self.m_ShareType=index
        -- CGuanNingMgr.Inst:Share2Web()
        -- CUICommonDef.SetActive(self.m_ShareButton,false,true)
        -- CUICommonDef.SetActive(self.m_ShareButton,false,true)
        local isAnqidao = CLuaGuanNingMgr.lastBattleData.isAnQiDaoData 
        CLuaGuanNingMgr.Share2Web(index+1, isAnqidao)
    end
    local selectShareItem=DelegateFactory.Action_int(SelectAction)


    -- for i=1,#self.m_GroupTable do
    local t={}
        local item=PopupMenuItemData(LocalString.GetString("分享至梦岛"),selectShareItem,false,nil)
        table.insert(t, item)
        local item=PopupMenuItemData(CLuaGuanNingMgr.GetShareString(),selectShareItem,false,nil)
        table.insert(t, item)
    -- end
    local array=Table2Array(t,MakeArrayClass(PopupMenuItemData))
    CPopupMenuInfoMgr.ShowPopupMenu(array, go.transform, AlignType.Top,1,nil,600,true,266)

end


-- function CLuaGuanNingLastBattleDataView:OnQueryGuanNingShareImageUrl()
--     if self.m_ShareType==0 then
--         ShareMgr.ShareWebImage2PersonalSpace(ShareMgr.Share2PersonalSpaceFile, ShareMgr.Share2PersonalSpaceUrl, nil)
--     elseif self.m_ShareType==1 then
--         ShareBoxMgr.OpenShareBox(EShareType.ShareImage, 0, { ShareMgr.Share2PersonalSpaceFile })
--     end
--     CUICommonDef.SetActive(self.m_ShareButton,true,true)
-- end
return CLuaGuanNingLastBattleDataView
