require("3rdParty/ScriptEvent")
require("common/common_include")

local Gac2Gas = import "L10.Game.Gac2Gas"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local CUIManager = import "L10.UI.CUIManager"
local CUIRes = import "L10.UI.CUIResources"
local UICamera = import "UICamera"
local Physics = import "UnityEngine.Physics"
local Vector3 = import "UnityEngine.Vector3"
local CZhuJueJuQingMgr = import "L10.Game.CZhuJueJuQingMgr"
local CUIFx = import "L10.UI.CUIFx"
local UIRoot = import "UIRoot"

CLuaMiHanWnd=class()
RegistClassMember(CLuaMiHanWnd,"Shard1")
RegistClassMember(CLuaMiHanWnd,"Shard2")
RegistClassMember(CLuaMiHanWnd,"Shard3")
RegistClassMember(CLuaMiHanWnd,"Shard4")
RegistClassMember(CLuaMiHanWnd,"Shard5")
RegistClassMember(CLuaMiHanWnd,"Shard6")
RegistClassMember(CLuaMiHanWnd,"TargetParent")
RegistClassMember(CLuaMiHanWnd,"HintLabel")
RegistClassMember(CLuaMiHanWnd,"MarkLabel")
RegistClassMember(CLuaMiHanWnd,"FXRoot")
RegistClassMember(CLuaMiHanWnd,"CloseButton")
RegistClassMember(CLuaMiHanWnd,"BGArea")
RegistClassMember(CLuaMiHanWnd,"BGShadow")

RegistClassMember(CLuaMiHanWnd,"MarkColor")
RegistClassMember(CLuaMiHanWnd,"Offset1")
RegistClassMember(CLuaMiHanWnd,"Offset2")
RegistClassMember(CLuaMiHanWnd,"Offset3")
RegistClassMember(CLuaMiHanWnd,"Offset4")
RegistClassMember(CLuaMiHanWnd,"Offset5")
RegistClassMember(CLuaMiHanWnd,"Offset6")

RegistClassMember(CLuaMiHanWnd,"Finished")
RegistClassMember(CLuaMiHanWnd,"ori")
RegistClassMember(CLuaMiHanWnd,"oriScale")
RegistClassMember(CLuaMiHanWnd,"offset")

function CLuaMiHanWnd:Awake()
	self.Finished = false
	self.ori={}
	self.oriScale={}
	self.offset={}
end

function CLuaMiHanWnd:OnEnable()
	self.ori={}
	self.oriScale={}
	self.offset={}
	self.ori[self.Shard1] = self.Shard1.transform.parent
	self.ori[self.Shard2] = self.Shard2.transform.parent
	self.ori[self.Shard3] = self.Shard3.transform.parent
	self.ori[self.Shard4] = self.Shard4.transform.parent
	self.ori[self.Shard5] = self.Shard5.transform.parent
	self.ori[self.Shard6] = self.Shard6.transform.parent
	
	self.offset[self.Shard1] = self.Offset1
	self.offset[self.Shard2] = self.Offset2
	self.offset[self.Shard3] = self.Offset3
	self.offset[self.Shard4] = self.Offset4
	self.offset[self.Shard5] = self.Offset5
	self.offset[self.Shard6] = self.Offset6
	
	self.oriScale[self.Shard1] = self.Shard1.transform.localScale
	self.oriScale[self.Shard2] = self.Shard2.transform.localScale
	self.oriScale[self.Shard3] = self.Shard3.transform.localScale
	self.oriScale[self.Shard4] = self.Shard4.transform.localScale
	self.oriScale[self.Shard5] = self.Shard5.transform.localScale
	self.oriScale[self.Shard6] = self.Shard6.transform.localScale
	local onDrag = function(go,delta)
		if self.Finished then 
			return 
		end
		local scale = UIRoot.GetPixelSizeAdjustment(self.gameObject);
		local trans = go.transform
		local pos = trans.localPosition
		pos.x = pos.x + delta.x*scale
		pos.y = pos.y + delta.y*scale
		trans.localPosition = pos
	end
	local onPress = function(go,flag)
		if self.Finished then 
			return 
		end
		if not flag then
			local currentPos = UICamera.currentTouch.pos
			local ray = UICamera.currentCamera:ScreenPointToRay(Vector3(currentPos.x,currentPos.y,0))
			local hits = Physics.RaycastAll(ray, 99999,  UICamera.currentCamera.cullingMask)
			for i =0,hits.Length -1 do
				if hits[i].collider.gameObject.transform.parent == self.TargetParent then
					go.transform.parent = hits[i].collider.gameObject.transform
					local p = Vector3.zero
					local z = self.offset[go]
					p.x = p.x + z.x
					p.y = p.y + z.y
					go.transform.localPosition = p
					self:judgeFinish()
					self.HintLabel:SetActive(false)
					return
				end
			end
			go.transform.parent = self.ori[go]
			go.transform.localPosition = Vector3.zero
			go.transform.localScale = self.oriScale[go]
		else
			go.transform.localScale = Vector3.one
		end
	end
	local onCloseClick = function(go)
		CUIManager.CloseUI(CUIRes.MihanWnd)
	end
	CommonDefs.AddOnClickListener(self.BGShadow,DelegateFactory.Action_GameObject(onCloseClick),false)
	CommonDefs.AddOnClickListener(self.CloseButton,DelegateFactory.Action_GameObject(onCloseClick),false)
	CommonDefs.AddOnDragListener(self.Shard1,DelegateFactory.Action_GameObject_Vector2(onDrag),false)
	CommonDefs.AddOnDragListener(self.Shard2,DelegateFactory.Action_GameObject_Vector2(onDrag),false)
	CommonDefs.AddOnDragListener(self.Shard3,DelegateFactory.Action_GameObject_Vector2(onDrag),false)
	CommonDefs.AddOnDragListener(self.Shard4,DelegateFactory.Action_GameObject_Vector2(onDrag),false)
	CommonDefs.AddOnDragListener(self.Shard5,DelegateFactory.Action_GameObject_Vector2(onDrag),false)
	CommonDefs.AddOnDragListener(self.Shard6,DelegateFactory.Action_GameObject_Vector2(onDrag),false)
	CommonDefs.AddOnPressListener(self.Shard1,DelegateFactory.Action_GameObject_bool(onPress),false)
	CommonDefs.AddOnPressListener(self.Shard2,DelegateFactory.Action_GameObject_bool(onPress),false)
	CommonDefs.AddOnPressListener(self.Shard3,DelegateFactory.Action_GameObject_bool(onPress),false)
	CommonDefs.AddOnPressListener(self.Shard4,DelegateFactory.Action_GameObject_bool(onPress),false)
	CommonDefs.AddOnPressListener(self.Shard5,DelegateFactory.Action_GameObject_bool(onPress),false)
	CommonDefs.AddOnPressListener(self.Shard6,DelegateFactory.Action_GameObject_bool(onPress),false)
	
end
function CLuaMiHanWnd:OnDisable()
	
	CUICommonDef.KillTween(self.gameObject:GetHashCode())
end
function CLuaMiHanWnd:judgeFinish()
	for i = 0,self.TargetParent.childCount-1 do
		local trans = self.TargetParent:GetChild(i)
		if trans.childCount == 0 then
			return 
		end
		if trans:GetChild(0).name ~= trans.name then
			return 
		end
	end
	self.Finished=true
	self:showMarkAndAnimation()
	local fx = self.FXRoot:GetComponent(typeof(CUIFx))
	fx:LoadFx("fx/ui/prefab/ui_pintu.prefab")
	Gac2Gas.FinishTaskPuzzle(CZhuJueJuQingMgr.Instance.MihanTaskID)
end
function CLuaMiHanWnd:showMarkAndAnimation()
	CUICommonDef.KillTween(self.gameObject:GetHashCode())
	local targetTrans = self.TargetParent.transform
	local targetP = targetTrans.localPosition
	local onUpdate = function(p)
		local color = self.MarkColor
		color.a = p
		self.MarkLabel.color = color
		local z =targetP
		z.x = (1-p)*targetP.x
		z.y = (1-p)*targetP.y
		targetTrans.localPosition = z 
		local s =Vector3.one
		s.x = 1 + 0.2 * p
		s.y = 1 + 0.2 * p
		targetTrans.localScale = s
	end
	local onComplete = function()
		self.BGArea:SetActive(false)
		self.BGShadow:SetActive(true)
		CUICommonDef.Tween(1,DelegateFactory.Action_float(onUpdate),nil,1,self.gameObject:GetHashCode())
	end
	local pintuUpdate = function(p)
		for i = 0,self.TargetParent.childCount-1 do
			local go = self.TargetParent:GetChild(i):GetChild(0).gameObject
			local trans = go.transform
			local z = self.offset[go]
			z.x = z.x*(1-p)
			z.y = z.y*(1-p)
			trans.localPosition = z
		end
	end
	
	CUICommonDef.Tween(3,DelegateFactory.Action_float(pintuUpdate),DelegateFactory.Action(onComplete),1,self.gameObject:GetHashCode())
end

return CLuaMiHanWnd











