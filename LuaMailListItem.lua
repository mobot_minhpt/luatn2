local CMailMgr = import "L10.Game.CMailMgr"
local CButton = import "L10.UI.CButton"
local UILabel = import "UILabel"
local QnCheckBox = import "L10.UI.QnCheckBox"

LuaMailListItem = class()
RegistClassMember(LuaMailListItem,"m_NameLabel")    --邮件名称
RegistClassMember(LuaMailListItem,"m_DateLabel")    --过期时间
RegistClassMember(LuaMailListItem,"m_AlertIcon")    --新邮件提醒图标
RegistClassMember(LuaMailListItem,"m_ItemIcon")     --邮件图标
RegistClassMember(LuaMailListItem,"m_Button")       --选中状态按钮
RegistClassMember(LuaMailListItem,"m_ItemCell")     --邮件图标区域对象
RegistClassMember(LuaMailListItem,"m_AwardSprite")  --奖励图标 有奖励未领取、有奖励已领取、没有奖励三种情况
RegistClassMember(LuaMailListItem,"m_Checkbox")     -- 批量删除时的选中框

RegistClassMember(LuaMailListItem,"m_MailId")
RegistClassMember(LuaMailListItem,"m_MailTemplateId")

function LuaMailListItem:Awake()
    self.m_NameLabel = self.transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    self.m_DateLabel = self.transform:Find("TimeLabel"):GetComponent(typeof(UILabel))
    self.m_AlertIcon = self.transform:Find("ItemCell/AlertIcon").gameObject
    self.m_ItemIcon = self.transform:Find("ItemCell/IconTexture"):GetComponent(typeof(CUITexture))
    self.m_Button = self.transform:GetComponent(typeof(CButton))
    self.m_ItemCell = self.transform:Find("ItemCell").gameObject
    self.m_AwardSprite = self.transform:Find("AwardSprite"):GetComponent(typeof(UISprite))
    self.m_Checkbox = self.transform:Find("Checkbox"):GetComponent(typeof(QnCheckBox))
    self.m_MailId = nil
    self.m_MailTemplateId = 0
end


function LuaMailListItem:InitContent(mail)
    
    self.m_NameLabel.text = mail.Title
    self.m_DateLabel.text = SafeStringFormat3("[c][9d2c2c]%s[-][/c]", mail.OverdueStr)
    self.m_ItemCell:SetActive(true)
    self.m_Button.Selected = false
    self.m_MailId = mail.MailId
    self.m_MailTemplateId = mail.TemplateId
    self.m_ItemIcon:LoadMaterial(mail.Icon)
    self.m_AlertIcon:SetActive(not mail.bIsRead)
    self.m_Checkbox.gameObject:SetActive(false)

    self:ShowAwardSprite()
end

function LuaMailListItem:ShowAwardSprite()
    local mail = CMailMgr.Inst:GetMail(self.m_MailId)
    if not mail then return end

    local hasAwards = mail.CanAcceptAwards
    local notAwarded = mail.IsHandled == 0

    if hasAwards then
        self.m_AwardSprite.spriteName = notAwarded and g_sprites.MailNotAwardedGiftIcon or g_sprites.MailAwardedGiftIcon
    else
        self.m_AwardSprite.spriteName = ""
    end
end

function LuaMailListItem:SetSelected(selected)
    self.m_Button.Selected = selected
    if selected then
        self.m_AlertIcon:SetActive(false)
    end
end

function LuaMailListItem:IsSelected()
    return self.m_Button.Selected
end

function LuaMailListItem:HideCheckbox(visible)
    self.m_Checkbox.gameObject:SetActive(false)
    self.m_Checkbox.OnValueChanged = nil
    self.m_ItemCell:SetActive(true)
end

function LuaMailListItem:ShowCheckbox(defaultSelected, onChangeFunc)
    self.m_Checkbox.gameObject:SetActive(true)
    self.m_Checkbox:SetSelected(defaultSelected, true)
    self.m_ItemCell:SetActive(false)
    if onChangeFunc then
        self.m_Checkbox.OnValueChanged = DelegateFactory.Action_bool(function (checked) onChangeFunc(checked, self.m_MailId) end)
    end
end

function LuaMailListItem:OnEnable()
    --根据浩神的说法，MailAcceptAwardsDone和HandleMailNotDelSuccess只会返回其一，对于领取后不自动删除的返回第二个消息
    g_ScriptEvent:AddListener("MailAcceptAwardsDone", self, "OnAcceptAwardsDone")
    g_ScriptEvent:AddListener("HandleMailNotDelSuccess", self, "HandleMailNotDelSuccess")

    g_ScriptEvent:AddListener("OnReceiveNewMailTemplate", self, "OnReceiveNewMailTemplate")--这个消息之前金凡转lua的时候没有处理，补充一下
end

function LuaMailListItem:OnDisable()
    g_ScriptEvent:RemoveListener("MailAcceptAwardsDone", self, "OnAcceptAwardsDone")
    g_ScriptEvent:RemoveListener("HandleMailNotDelSuccess", self, "HandleMailNotDelSuccess")

    g_ScriptEvent:RemoveListener("OnReceiveNewMailTemplate", self, "OnReceiveNewMailTemplate")
end

function LuaMailListItem:OnAcceptAwardsDone(args)
    local success = args[0]
    local mailId = args[1]
    if self.m_MailId == mailId then
        self:ShowAwardSprite()
    end
end

function LuaMailListItem:HandleMailNotDelSuccess(mailId)
    if self.m_MailId == mailId then
        self:ShowAwardSprite()
    end
end

function LuaMailListItem:OnReceiveNewMailTemplate(args)
    local mailTemplateId = args[0]
    if mailTemplateId == self.m_MailTemplateId then
        local mail = CMailMgr.Inst:GetMail(self.m_MailId)
        if not mail then return end
        --初始化一些基础信息
        self.m_NameLabel.text = mail.Title
        self.m_DateLabel.text = SafeStringFormat3("[c][9d2c2c]%s[-][/c]", mail.OverdueStr)
        self.m_AlertIcon:SetActive(not mail.bIsRead)
        self:ShowAwardSprite()
    end
end
