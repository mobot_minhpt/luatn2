local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"
local UITable = import "UITable"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CScene = import "L10.Game.CScene"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"

LuaJuDianBattleSceneChooseWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaJuDianBattleSceneChooseWnd, "ScrollView", "ScrollView", CUIRestrictScrollView)
RegistChildComponent(LuaJuDianBattleSceneChooseWnd, "Table", "Table", UITable)
RegistChildComponent(LuaJuDianBattleSceneChooseWnd, "Template", "Template", GameObject)
RegistChildComponent(LuaJuDianBattleSceneChooseWnd, "BottomLabel", "BottomLabel", UILabel)

--@endregion RegistChildComponent end

function LuaJuDianBattleSceneChooseWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
end

function LuaJuDianBattleSceneChooseWnd:Init()
    self.Template:SetActive(false)
    self.BottomLabel.text = LocalString.GetString("[ACF9FF]仅[FFFE91]分线1[-]刷新分值较高的据点旗，两条分线均包含三个据点。")
end

function LuaJuDianBattleSceneChooseWnd:OnData(mapId, currentSceneIdx, serverDataList)
    Extensions.RemoveAllChildren(self.Table.transform)

    for i=1, #serverDataList do
        local data = serverDataList[i]
        local cur = CUICommonDef.AddChild(self.Table.gameObject, self.Template)
        cur:SetActive(true)
        self:InitSingleItem(cur, LocalString.GetString("分线") .. tostring(i), data.playerCount, currentSceneIdx == i, i)
    end

    self.Table:Reposition()
    self.ScrollView:ResetPosition()
end

-- 初始化单个分线信息
-- 据点旗帜固定在分线1
function LuaJuDianBattleSceneChooseWnd:InitSingleItem(go, branchName, playerNumber, isCurrent, index)
    local branchNameLabel = go.transform:Find("BranchNameLabel"):GetComponent(typeof(UILabel))
    local playerNumberLabel = go.transform:Find("PlayerNumberLabel"):GetComponent(typeof(UILabel))
    local statusIcon = go.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local canEnterLabel = go.transform:Find("CanEnterLabel"):GetComponent(typeof(UILabel))
    local currentFlag = go.transform:Find("CurrentFlag").gameObject
    local bg = go.transform:GetComponent(typeof(UISprite))

    local baomanIconSpriteName = "loginwnd_severstate_baoman"
    local busyIconSpriteName = "loginwnd_severstate_busy"
    local normalIconSpriteName = "loginwnd_severstate_smooth"

    branchNameLabel.text = branchName
    playerNumberLabel.text = tostring(playerNumber) .. LocalString.GetString("人")

    if index == 1 then
        statusIcon.gameObject:SetActive(true)
        statusIcon:LoadMaterial("UI/Texture/Transparent/Material/minimappopwnd_icon_qi.mat")
    elseif index <=2 then
        statusIcon.gameObject:SetActive(true)
        statusIcon:LoadMaterial("UI/Texture/Transparent/Material/minimappopwnd_icon_judian.mat")
    else
        bg.color = Color(1,1,1,0.5)
        statusIcon.gameObject:SetActive(false)
    end

    currentFlag:SetActive(isCurrent)

    UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function()
        local mapId = LuaJuDianBattleMgr:GetMapId()
        if mapId ~= 0 then
            -- 传送
            if isCurrent then
                g_MessageMgr:ShowMessage("Message_ChuanSong_Current_Scene_Tips")
            else
                if LuaJuDianBattleMgr:IsInGameplay() then
                    Gac2Gas.GuildJuDianRequestTeleportToPosition(mapId, index, 0, 0)
                elseif LuaJuDianBattleMgr:IsInDailyGameplay() then
                    Gac2Gas.GuildJuDianRequestTeleportToDailyScene(mapId, 0, 0)
                end
            end
        end
    end)
end

function LuaJuDianBattleSceneChooseWnd:OnEnable()
    local mapId = LuaJuDianBattleMgr:GetMapId()
    if mapId ~= 0 then
        print("GuildJuDianQueryPlayerCountByMapId", mapId)
        Gac2Gas.GuildJuDianQueryPlayerCountByMapId(mapId)
    end
    g_ScriptEvent:AddListener("GuildJuDianSyncPlayerCountByMapId", self, "OnData")
end

function LuaJuDianBattleSceneChooseWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GuildJuDianSyncPlayerCountByMapId", self, "OnData") 
end

--@region UIEvent

--@endregion UIEvent

