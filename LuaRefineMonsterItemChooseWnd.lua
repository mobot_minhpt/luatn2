
local DelegateFactory  = import "DelegateFactory"
local UILabel = import "UILabel"
local GameObject = import "UnityEngine.GameObject"
local QnTableView = import "L10.UI.QnTableView"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local CClientMainPlayer=import "L10.Game.CClientMainPlayer"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local CPackageItemCell = import "L10.UI.CPackageItemCell"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local AlignType2 = import "L10.UI.CItemInfoMgr+AlignType"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"

LuaRefineMonsterItemChooseWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaRefineMonsterItemChooseWnd, "TitleLabel", "TitleLabel", UILabel)
RegistChildComponent(LuaRefineMonsterItemChooseWnd, "VoidLabel", "VoidLabel", UILabel)
RegistChildComponent(LuaRefineMonsterItemChooseWnd, "CommitBtn", "CommitBtn", GameObject)
RegistChildComponent(LuaRefineMonsterItemChooseWnd, "CloseBtn", "CloseBtn", GameObject)
RegistChildComponent(LuaRefineMonsterItemChooseWnd, "DescLabel", "DescLabel", UILabel)
RegistChildComponent(LuaRefineMonsterItemChooseWnd, "TableView", "TableView", QnTableView)
RegistChildComponent(LuaRefineMonsterItemChooseWnd, "QuestBtn", "QuestBtn", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaRefineMonsterItemChooseWnd,"m_CurrentSelectData")
RegistClassMember(LuaRefineMonsterItemChooseWnd,"m_CurrentSelectIndex")
-- 1是物品 2是妖怪
RegistClassMember(LuaRefineMonsterItemChooseWnd,"m_ItemList")
RegistClassMember(LuaRefineMonsterItemChooseWnd,"m_LingHeLv")
RegistClassMember(LuaRefineMonsterItemChooseWnd,"m_IsAdvance")
RegistClassMember(LuaRefineMonsterItemChooseWnd,"m_MyTypeList")

function LuaRefineMonsterItemChooseWnd:Awake()
    --@region EventBind: Dont Modify Manually!

	
	UIEventListener.Get(self.CommitBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCommitBtnClick()
	end)


	
	UIEventListener.Get(self.QuestBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnQuestBtnClick()
	end)


    --@endregion EventBind end
end

function LuaRefineMonsterItemChooseWnd:Init()
	self.m_IsAdvance = LuaZongMenMgr.m_IsAdvanceCommit
	self.m_MyTypeList = LuaZongMenMgr.m_LingCaiTypeList

	self.TableView.OnSelectAtRow = DelegateFactory.Action_int(function(row)
		self:OnItemClick(row)
    end)
	self:InitStatus()

	if self.m_IsAdvance then
		self.TitleLabel.text = LocalString.GetString("选择高级炼化灵材")
	else
		self.TitleLabel.text = LocalString.GetString("选择普通炼化灵材")
	end
end

function LuaRefineMonsterItemChooseWnd:OnItemClick(row)
	local data = self.m_ItemList[row+1]
	local tbl = {}

	if data then
		if data.QualityLimit then
			table.insert(tbl, SafeStringFormat3(LocalString.GetString("[FF0000]评分 %d[-]"), data.Quality))
		else
			table.insert(tbl, SafeStringFormat3(LocalString.GetString("[ACF9FF]评分[-] %d"), data.Quality))
		end

		if data.LingHeLimit then
			table.insert(tbl, SafeStringFormat3(LocalString.GetString("[FF0000]需求灵核等级 %d[-]"), data.SoulCoreLvLimit))
		else
			table.insert(tbl, SafeStringFormat3(LocalString.GetString("[ACF9FF]需求灵核等级[-] %d"), data.SoulCoreLvLimit))
		end
		LuaZongMenMgr.m_RefineMonsterItemLabels = tbl

		if CZuoQiMgr.IsMaPi(data.TemplateId) and data.Type == 1 then
			local itemId = data.ItemId
			LuaZongMenMgr.m_RefineMonsterCommonItem = CItemMgr.Inst:GetById(itemId)
		end

		CItemInfoMgr.ShowLinkItemTemplateInfo(data.TemplateId, false, nil, AlignType2.Default, 0, 0, 0, 0)
		local tableItem = self.TableView:GetItemAtRow(row)
		local itemCell = tableItem.transform:GetComponent(typeof(CPackageItemCell))

		if self.m_CurrentSelectIndex ~= nil and self.m_CurrentSelectIndex ~= row+1 then
			-- 取消之前选中的物品
			local lastItem = self.TableView:GetItemAtRow(self.m_CurrentSelectIndex-1)
			if lastItem~= nil then
				local cell = lastItem.transform:GetComponent(typeof(CPackageItemCell))
				cell.Selected = false
			end
		end

		-- 点自己是取消选中
		if self.m_CurrentSelectIndex == row+1 then
			self.m_CurrentSelectIndex = nil
			self.m_CurrentSelectData = nil
			itemCell.Selected = false
		else
			self.m_CurrentSelectData = data
			self.m_CurrentSelectIndex = row+1
			itemCell.Selected = true
		end
		self:UpdateLingli()
	else
		-- 处理获取途径
		CUIManager.ShowUI(CLuaUIResources.RefineMonsterItemAccessWnd)
	end
end

-- 初始化物品
function LuaRefineMonsterItemChooseWnd:UpdateLingli()
	-- 更新灵力
	if self.m_CurrentSelectData then
		local data = LianHua_Item.GetData(self.m_CurrentSelectData.TemplateId)
		self.DescLabel.text = SafeStringFormat3(LocalString.GetString("[ACF8FF]炼化完成预计获得灵力[-]%0.1f"), math.floor((LuaZongMenMgr:GetLingCaiParam(data.LingLi) or 0)*10)/10)
	else
		self.DescLabel.text = ""
	end
end

-- 初始化物品
function LuaRefineMonsterItemChooseWnd:InitItem()
	local count = CClientMainPlayer.Inst.ItemProp:GetPlaceMaxSize(EnumItemPlace.Bag)
    -- 遍历包裹里的物品
    for i=0, count-1 do
        local itemId = CClientMainPlayer.Inst.ItemProp:GetItemAt(EnumItemPlace.Bag, i + 1)
		local commonItem = CItemMgr.Inst:GetById(itemId)
		if commonItem ~= nil then
			local templateId = 0
			if commonItem.IsItem then
				templateId = commonItem.Item.TemplateId
			elseif commonItem.IsEquip then
				templateId = commonItem.Equip.TemplateId
			end
			local itemData = LianHua_Item.GetData(templateId)
			if itemData ~= nil and self:CheckLianHuaItem(itemData) and not commonItem:IsItemExpire() then
				local t = {}
				t.ItemId = itemId
				t.TemplateId = templateId
				t.Type = 1
				t.Quality = itemData.Quality
				t.SoulCoreLvLimit = itemData.SoulCoreLvLimit
				table.insert(self.m_ItemList, t)
			end
		end
    end
end

-- 做检查
function LuaRefineMonsterItemChooseWnd:CheckLianHuaItem(data)
	local threshold = LianHua_Setting.GetData().LingCaiQualityThreshold
	if (data.Quality >= threshold and self.m_IsAdvance) or (data.Quality < threshold and not self.m_IsAdvance) then
		if #self.m_MyTypeList == 0 then
			return true
		else
			for i=1, #self.m_MyTypeList do
				if self.m_MyTypeList[i] == data.Type then
					return true
				end
			end
		end
	end
	return false
end

function LuaRefineMonsterItemChooseWnd:ItemSelect(itemId)
	local commonItem = CItemMgr.Inst:GetById(itemId)
	if commonItem == nil then
		return false
	end
	local templateId = 0

	if commonItem.IsEquip then
		templateId = commonItem.Equip.TemplateId
	elseif commonItem.IsItem then
		templateId = commonItem.Item.TemplateId
	end

	if LianHua_Item.GetData(templateId) ~= nil then
		return true
	else
		return false
	end
end

-- 初始化妖怪
function LuaRefineMonsterItemChooseWnd:InitYaoGuai()
	local record = {}
    for k,v in pairs(LuaZhuoYaoMgr.m_WarehouseYaoGuaiList) do
		local templateId = v.TemplateId
		if record[templateId] == nil then
			record[templateId] = 1
		else
			record[templateId] = record[templateId] + 1
		end
    end

	for k, v in pairs(record) do
		local data = ZhuoYao_TuJian.GetData(k)
		if data then
			local itemId = data.ItemID
			local itemData = LianHua_Item.GetData(itemId)
			if itemData and self:CheckLianHuaItem(itemData) then
				local t = {}
				t.YaoGuaiTemplateId = k
				t.TemplateId = itemId
				t.Count = v
				t.Type = 2
				t.Quality = itemData.Quality
				t.SoulCoreLvLimit = itemData.SoulCoreLvLimit
				table.insert(self.m_ItemList, t)
			end
		end
	end
end

function LuaRefineMonsterItemChooseWnd:InitStatus()
	-- 灵核等级不足
	self.m_LingHeLv = (((CClientMainPlayer.Inst or {}).SkillProp or {}).SoulCore or {}).Level or 1
    self.m_ItemList = {}

    self:InitItem()
	self:InitYaoGuai()

	-- 初始化条件限制
	for i=1, #self.m_ItemList do
		self:InitLimition(self.m_ItemList[i])
	end

	if #self.m_ItemList == 0 then
		self.VoidLabel.gameObject:SetActive(false)
	else
		self.VoidLabel.gameObject:SetActive(false)
	end

	-- 排序
	table.sort(self.m_ItemList, function(t1, t2)
		local data1 = LianHua_Item.GetData(t1.TemplateId)
		local data2 = LianHua_Item.GetData(t2.TemplateId)
		local limit1 = t1.QualityLimit or t1.LingHeLimit
		local limit2 = t2.QualityLimit or t2.LingHeLimit

		if limit1 ~= limit2 then
			-- 为false的在前面
			return limit2
		elseif data1.Quality == data2.Quality then
			if t1.Type == 1 and t2.Type==1 then
				return t1.ItemId > t2.ItemId
			elseif t1.Type == 2 and t2.Type == 2 then
				return false
			else
				return t1.Type < t2.Type
			end
		else
			return data1.Quality > data2.Quality
		end
	end)

    self.TableView.m_DataSource=DefaultTableViewDataSource.Create(
        function()
            return #self.m_ItemList+1
        end,
        
        function(item,index)
            self:InitItemCell(item.transform, index)
        end)

    self.TableView:ReloadData(true, false)

	-- 尝试获取上一次选中状态
	local hasFindLastSelect = false
	
	if self.m_CurrentSelectData then
		for i=1,#self.m_ItemList do
			local tableItem = self.TableView:GetItemAtRow(i-1)
			local itemCell = tableItem.transform:GetComponent(typeof(CPackageItemCell))
			local itemData = self.m_ItemList[i]

			if itemCell then
				itemCell.Selected = false
			end

			if itemData.Type == self.m_CurrentSelectData.Type then
				-- 物品
				if itemData.Type == 1 and itemData.ItemId == self.m_CurrentSelectData.ItemId then
					self.m_CurrentSelectIndex = i
					hasFindLastSelect = true
					if itemCell then
						itemCell.Selected = true
					end
				-- 妖怪
				elseif itemData.Type == 2 and itemData.TemplateId == self.m_CurrentSelectData.TemplateId then
					self.m_CurrentSelectIndex = i
					hasFindLastSelect = true
					if itemCell then
						itemCell.Selected = true
					end
				end
			end
		end
	end

	-- 没找到要清数据
	if not hasFindLastSelect then
		self.m_CurrentSelectIndex = nil
		self.m_CurrentSelectData = nil
	end
end

function LuaRefineMonsterItemChooseWnd:InitLimition(data)
	data.QualityLimit = false
	data.LingHeLimit = false
	-- 灵核等级不足
	if data.SoulCoreLvLimit > self.m_LingHeLv then
		data.LingHeLimit = true
	end

	-- 评分不足
    if not self.m_IsAdvance and LuaZongMenMgr.m_LingCaiCount >= SoulCore_SoulCore.GetData(self.m_LingHeLv).LingCaiLimitCount then
		if data.Quality <= LuaZongMenMgr.m_LingCaiLowest then
			data.QualityLimit = true
		end
	end

	if self.m_IsAdvance and LuaZongMenMgr.m_AdvanceLingCaiCount >= SoulCore_SoulCore.GetData(self.m_LingHeLv).AdvancedLingCaiLimitCount then
		if data.Quality <= LuaZongMenMgr.m_AdvanceLingCaiLowest then
			data.QualityLimit = true
		end
	end
end

function LuaRefineMonsterItemChooseWnd:InitItemCell(transform, index)
    local itemCell = transform:GetComponent(typeof(CPackageItemCell))
    local data = self.m_ItemList[index+1]
    if itemCell ~= nil then
		local getLabel = itemCell.transform:Find("GetLabel").gameObject
		local iconTexture = itemCell.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
		local amountLabel = itemCell.transform:Find("AmountLabel"):GetComponent(typeof(UILabel))
		local quailtySprite = itemCell.transform:Find("QualitySprite").gameObject
		local bindSprite = itemCell.transform:Find("BindSprite").gameObject
		local selectSprite = itemCell.transform:Find("SelectedSprite").gameObject
		local exSprite = itemCell.transform:Find("ExpiringNoticeSprite").gameObject
		exSprite:SetActive(false)
		if data ~= nil then
			selectSprite:SetActive(true)
			quailtySprite:SetActive(true)
			iconTexture.gameObject:SetActive(true)
			getLabel:SetActive(false)
			if data.Type == 1 then
				bindSprite:SetActive(true)
				itemCell:Init(data.ItemId, false, false, false, nil)
				itemCell.Selected = false
			elseif data.Type == 2 then
				bindSprite:SetActive(false)
				local itemData = Item_Item.GetData(data.TemplateId)
				if itemData ==nil then
					itemData = EquipmentTemplate_Equip.GetData(data.TemplateId)
				end
				if itemData then
					iconTexture:LoadMaterial(itemData.Icon)
					amountLabel.text = data.Count
				end
			end
				-- 蒙红
			itemCell.disableSprite.enabled = false
			if data.LingHeLimit or data.QualityLimit then
				itemCell.disableSprite.enabled = true
			end
		else
			selectSprite:SetActive(false)
			-- 最后一个获取按钮
			bindSprite:SetActive(false)
			quailtySprite:SetActive(true)
			local sp = quailtySprite.transform:GetComponent(typeof(UISprite))
			sp.spriteName = "common_itemcell_01_border"
			iconTexture.gameObject:SetActive(false)
			getLabel:SetActive(true)
			amountLabel.text = ""
			itemCell.disableSprite.enabled = false
		end
    end
end

function LuaRefineMonsterItemChooseWnd:SendItem(args)
    local itemId = args[0]
    if CItemMgr.Inst:GetById(itemId) == nil or self:ItemSelect(itemId) then
        self:InitStatus()
    end
end

function LuaRefineMonsterItemChooseWnd:SetItemAt(place, position, oldItemId, newItemId)
	if CItemMgr.Inst:GetById(oldItemId) == nil or CItemMgr.Inst:GetById(newItemId) or 
			self:ItemSelect(oldItemId) or self:ItemSelect(newItemId) then
		self:InitStatus()
	end
end

function LuaRefineMonsterItemChooseWnd:OnEnable()
    g_ScriptEvent:AddListener("SendItem", self, "SendItem")
	g_ScriptEvent:AddListener("SetItemAt", self, "SetItemAt")
    g_ScriptEvent:AddListener("OnWarehouseYaoGuaiListUpdate", self, "InitStatus")
    g_ScriptEvent:AddListener("RefineMonsterItemUpdatePersonalTab", self, "InitStatus")
    g_ScriptEvent:AddListener("RefineMonsterItemUpdateZongMenLingLi", self, "UpdateLingli")
end

function LuaRefineMonsterItemChooseWnd:OnDisable()
    g_ScriptEvent:RemoveListener("SendItem", self, "SendItem")
	g_ScriptEvent:RemoveListener("SetItemAt", self, "SetItemAt")
    g_ScriptEvent:RemoveListener("OnWarehouseYaoGuaiListUpdate", self, "InitStatus")
    g_ScriptEvent:RemoveListener("RefineMonsterItemUpdatePersonalTab", self, "InitStatus")
    g_ScriptEvent:RemoveListener("RefineMonsterItemUpdateZongMenLingLi", self, "UpdateLingli")
end

function LuaRefineMonsterItemChooseWnd:CommitFunction(data, itemData)
	if data.Type == 1 then
		-- 是物品
		local itemId = data.ItemId
		local pos = CClientMainPlayer.Inst.ItemProp:GetItemPos(EnumItemPlace.Bag, itemId)
		if data.LingHeLimit then
			-- 灵核等级不满足 不能投
			g_MessageMgr:ShowMessage("LIANHUA_ITEM_LINGHE_LEVEL_LOW")
		elseif (not self.m_IsAdvance) and LuaZongMenMgr.m_LingCaiCount >= SoulCore_SoulCore.GetData(self.m_LingHeLv).LingCaiLimitCount then
			-- 小于的时候不能投
			if itemData.Quality <= LuaZongMenMgr.m_LingCaiLowest then
				g_MessageMgr:ShowMessage("LIANHUA_ITEM_QUALITY_LOW")
			else
				-- 替换确认
				g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("LIANHUA_ITEM_PUTIN_CONFIRM"), function ()
					if self.m_IsAdvance then
						Gac2Gas.RequestPutAdvancedItemIntoFaZhen(EnumItemPlace.Bag, pos, itemId)
					else
						Gac2Gas.RequestPutItemIntoFaZhen(EnumItemPlace.Bag, pos, itemId)
					end
				end, nil, nil, nil, false)
			end
		elseif self.m_IsAdvance and LuaZongMenMgr.m_AdvanceLingCaiCount >= SoulCore_SoulCore.GetData(self.m_LingHeLv).AdvancedLingCaiLimitCount then
			-- 小于的时候不能投
			if itemData.Quality <= LuaZongMenMgr.m_AdvanceLingCaiLowest then
				g_MessageMgr:ShowMessage("LIANHUA_ITEM_QUALITY_LOW")
			else
				-- 替换确认
				g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("LIANHUA_ITEM_PUTIN_CONFIRM"), function ()
					if self.m_IsAdvance then
						Gac2Gas.RequestPutAdvancedItemIntoFaZhen(EnumItemPlace.Bag, pos, itemId)
					else
						Gac2Gas.RequestPutItemIntoFaZhen(EnumItemPlace.Bag, pos, itemId)
					end
				end, nil, nil, nil, false)
			end
		elseif self.m_IsAdvance then
			Gac2Gas.RequestPutAdvancedItemIntoFaZhen(EnumItemPlace.Bag, pos, itemId)
		else
			Gac2Gas.RequestPutItemIntoFaZhen(EnumItemPlace.Bag, pos, itemId)
		end
	elseif data.Type == 2 then
		-- 是妖怪
		local templateId = data.YaoGuaiTemplateId
		local lowest = 0
		local lowestID = 0
		for k,v in pairs(LuaZhuoYaoMgr.m_WarehouseYaoGuaiList) do
			if templateId == v.TemplateId then
				if v.Quality < lowest or lowest == 0 then
					lowest = v.Quality
					lowestID = v.Id
				end
			end
		end
		if lowestID~=0 then
			if data.LingHeLimit then
				-- 灵核等级不满足 不能投
				g_MessageMgr:ShowMessage("LIANHUA_ITEM_LINGHE_LEVEL_LOW")
			elseif (not self.m_IsAdvance) and LuaZongMenMgr.m_LingCaiCount >= SoulCore_SoulCore.GetData(self.m_LingHeLv).LingCaiLimitCount then
				if itemData.Quality <= LuaZongMenMgr.m_LingCaiLowest then
					g_MessageMgr:ShowMessage("LIANHUA_ITEM_QUALITY_LOW")
				else
					g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("LIANHUA_ITEM_PUTIN_CONFIRM"), function ()
						Gac2Gas.RequestPutMonsterAsItemIntoFaZhen(tostring(lowestID))
					end, nil, nil, nil, false)
				end
			elseif self.m_IsAdvance and LuaZongMenMgr.m_AdvanceLingCaiCount >= SoulCore_SoulCore.GetData(self.m_LingHeLv).AdvancedLingCaiLimitCount then
				if itemData.Quality <= LuaZongMenMgr.m_AdvanceLingCaiLowest then
					g_MessageMgr:ShowMessage("LIANHUA_ITEM_QUALITY_LOW")
				else
					g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("LIANHUA_ITEM_PUTIN_CONFIRM"), function ()
						if self.m_IsAdvance then
							Gac2Gas.RequestPutAdvancedMonsterAsItemIntoFaZhen(tostring(lowestID))
						end
					end, nil, nil, nil, false)
				end
			elseif self.m_IsAdvance then
				Gac2Gas.RequestPutAdvancedMonsterAsItemIntoFaZhen(tostring(lowestID))
			else
				Gac2Gas.RequestPutMonsterAsItemIntoFaZhen(tostring(lowestID))
			end
		end
	end
end

--@region UIEvent

function LuaRefineMonsterItemChooseWnd:OnCommitBtnClick()
	local data = self.m_CurrentSelectData
	if data then
		local itemData = LianHua_Item.GetData(data.TemplateId)
		-- 贵重物品增加确认
		if itemData.Precious >0 then
			local name = ""
			local templateData = Item_Item.GetData(data.TemplateId)
			if templateData == nil then
				templateData = EquipmentTemplate_Equip.GetData(data.TemplateId)
			end

			if templateData then
				name = templateData.Name
			end
			
			g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("LIANHUA_PRECIOUS_ITEM_PUTIN_CONFIRM", name), function ()
				self:CommitFunction(data, itemData)
			end, nil, nil, nil, false)
		else
			self:CommitFunction(data, itemData)
		end
	else
		g_MessageMgr:ShowMessage("Please_Choose_LianHau_LingCai")
	end
end

function LuaRefineMonsterItemChooseWnd:OnQuestBtnClick()
	local list = LianHua_Setting.GetData().LianHuaItemChooseDescMsg
	local msg = nil
	for i=0, list.Length-2, 2 do
		if self.m_LingHeLv >= tonumber(list[i]) or math.maxinteger then
			msg = list[i+1]
		end
	end

	if not self.m_IsAdvance then
		if msg then
			g_MessageMgr:ShowMessage(msg, tostring(self.m_LingHeLv))
		end
	else
		g_MessageMgr:ShowMessage("Advanced_LianHuaItem_SoulCoreLevel")
	end
end


--@endregion UIEvent
