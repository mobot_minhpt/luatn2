-- Auto Generated!!
local Constants = import "L10.Game.Constants"
local CXialvPKResultWushi = import "L10.UI.CXialvPKResultWushi"
local LocalString = import "LocalString"
CXialvPKResultWushi.m_Init_CS2LuaHook = function (this, round, leftData, rightData, winTeamId, leftTeamId, rightTeamId, isRunning, notStarted) 
    --还没队伍获胜
    --还没队伍获胜
    if winTeamId < 0 then
        this.resultSprite1.enabled = false
        this.resultSprite2.enabled = false
    else
        this.resultSprite1.enabled = true
        this.resultSprite2.enabled = true
        if winTeamId == leftTeamId then
            this.resultSprite1.spriteName = Constants.WinSprite
            this.resultSprite2.spriteName = Constants.LoseSprite
        elseif winTeamId == rightTeamId then
            this.resultSprite1.spriteName = Constants.LoseSprite
            this.resultSprite2.spriteName = Constants.WinSprite
        end
    end

    if isRunning then
        this.roundLabel.text = System.String.Format(LocalString.GetString("[{0}]进行中[-]"), CXialvPKResultWushi.yellowColor)
    else
        this.roundLabel.text = System.String.Format(LocalString.GetString("[{0}]第{1}场[-]"), CXialvPKResultWushi.greyColor, round)
    end

    if leftData ~= nil then
        if not notStarted then
            this.dpsLabel1.text = tostring(leftData.dpsNum)
            this.killNumLabel1.text = tostring(leftData.killNum)
        else
            this.dpsLabel1.text = "--"
            this.killNumLabel1.text = "--"
        end
    else
        this.dpsLabel1.text = "--"
        this.killNumLabel1.text = "--"
    end

    if rightData ~= nil then
        if not notStarted then
            this.dpsLabel2.text = tostring(rightData.dpsNum)
            this.killNumLabel2.text = tostring(rightData.killNum)
        else
            this.dpsLabel2.text = "--"
            this.killNumLabel2.text = "--"
        end
    else
        this.dpsLabel2.text = "--"
        this.killNumLabel2.text = "--"
    end
end
