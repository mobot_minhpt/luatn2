-- Auto Generated!!
local AlignType = import "L10.UI.CTooltip+AlignType"
local AlignType1 = import "L10.UI.CItemInfoMgr+AlignType"
local Boolean = import "System.Boolean"
local Callback = import "EventDelegate+Callback"
local CEquipBaptizeDescItem = import "L10.UI.CEquipBaptizeDescItem"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CLingfuBaptizeWnd = import "L10.UI.CLingfuBaptizeWnd"
local CLingfuQianghuaWnd = import "L10.UI.CLingfuQianghuaWnd"
local CLingfuView = import "L10.UI.CLingfuView"
local Color = import "UnityEngine.Color"
local CommonDefs = import "L10.Game.CommonDefs"
local CTickMgr = import "L10.Engine.CTickMgr"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIFx = import "L10.UI.CUIFx"
local CUIFxPaths = import "L10.UI.CUIFxPaths"
local CZuoQiMgr = import "L10.Game.CZuoQiMgr"
local DelegateFactory = import "DelegateFactory"
local EnumChineseDigits = import "L10.Game.EnumChineseDigits"
local EnumEventType = import "EnumEventType"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EnumMapiLingfuPos = import "L10.Game.EnumMapiLingfuPos"
local ETickType = import "L10.Engine.ETickType"
local EventManager = import "EventManager"
local Extensions = import "Extensions"
local Gac2Gas = import "L10.Game.Gac2Gas"
local Item_Item = import "L10.Game.Item_Item"
local LocalString = import "LocalString"
local String = import "System.String"
local Time = import "UnityEngine.Time"
local TweenPosition = import "TweenPosition"
local TweenScale = import "TweenScale"
local UIEventListener = import "UIEventListener"
local UInt32 = import "System.UInt32"
local Vector3 = import "UnityEngine.Vector3"
local VoidDelegate = import "UIEventListener+VoidDelegate"
local ZuoQi_LingfuQianghua = import "L10.Game.ZuoQi_LingfuQianghua"
local ZuoQi_Setting = import "L10.Game.ZuoQi_Setting"
CLingfuQianghuaWnd.m_OnTick_CS2LuaHook = function (this) 
    local percent = (Time.realtimeSinceStartup - this.mUpdateStartTime) / CLingfuQianghuaWnd.sUpdateTotalTime
    local lastPercent = (this.mLastUpdateTime - this.mUpdateStartTime) / CLingfuQianghuaWnd.sUpdateTotalTime

    local curStarCount = math.floor((percent * this.mTargetStarCount))
    local lastStarCount = math.floor((lastPercent * this.mTargetStarCount))
    if curStarCount > lastPercent then
        this:ShowStar(this.lingfuStars, this.mTargetStarCount, (curStarCount - 1))
        this:ShowStar(this.attrStars, this.mTargetStarCount, (curStarCount - 1))
    end

    this.lingfuRingSprite.fillAmount = (this.mTargetStarCount * math.max(0, math.min(1, percent))) / CZuoQiMgr.sMaxQianghuaXingji
    this:UpdateWordQianghua(percent)

    local hudu = (this.mTargetStarCount * math.max(0, math.min(1, percent)) * 2 * PI) / (CZuoQiMgr.sMaxQianghuaXingji)
    this.progressFX.transform.localPosition = Vector3(math.sin(hudu + PI) * 235, - math.cos(hudu + PI) * 235, 0)
    this.progressFX.gameObject:SetActive(true)

    this.mLastUpdateTime = Time.realtimeSinceStartup
    if percent > 1 then
        this:UnRegisterTick()
        return
    end
end
CLingfuQianghuaWnd.m_UpdateWordQianghua_CS2LuaHook = function (this, percent) 
    if CLingfuQianghuaWnd.sQianghuaLingfuId == nil then
        return
    end

    percent = math.max(0, math.min(1, percent))
    local item = CItemMgr.Inst:GetById(CLingfuQianghuaWnd.sQianghuaLingfuId)
    if not (item ~= nil and item.Item.LingfuItemInfo ~= nil and item.Item.LingfuItemInfo.QianghuaJieshu > 0 and item.Item.LingfuItemInfo.QianghuaXingji > 0) then
        return
    end

    local qianghuaData = ZuoQi_LingfuQianghua.GetData(item.Item.LingfuItemInfo.QianghuaJieshu)
    if qianghuaData == nil then
        return
    end

    do
        local i = 0
        while i < this.mWordsList.Count do
            local descItem = this.mDescItemList[i]
            descItem:UpdateExtraDescString(CZuoQiMgr.CalcWordAttrIncreaseStr(this.mWordsList[i], qianghuaData.WordAdd[item.Item.LingfuItemInfo.QianghuaXingji - 1], percent))
            i = i + 1
        end
    end
end
CLingfuQianghuaWnd.m_UnRegisterTick_CS2LuaHook = function (this) 
    if this.mUpdateTick ~= nil then
        invoke(this.mUpdateTick)
        this.mUpdateTick = nil
    end
end
CLingfuQianghuaWnd.m_InitWnd_CS2LuaHook = function (this, bSetPos) 
    if CLingfuQianghuaWnd.sQianghuaLingfuId ~= nil then
        local item = CItemMgr.Inst:GetById(CLingfuQianghuaWnd.sQianghuaLingfuId)
        if item ~= nil then
            local itemdata = Item_Item.GetData(item.TemplateId)
            if itemdata ~= nil then
                this.lingfuItemTex:LoadMaterial(itemdata.Icon)
                local data = item.Item.LingfuItemInfo
                if data ~= nil then
                    this.lingfuQualitySprite.spriteName = CZuoQiMgr.GetLingFuItemCellBorder(data.Quality)
                    this.lingfuItemLabel.text = ((itemdata.Name .. "(") .. CZuoQiMgr.GetLingFuPos(CommonDefs.ConvertIntToEnum(typeof(EnumMapiLingfuPos), data.Pos))) .. ")"
                    this.jieshuLabel.text = EnumChineseDigits.GetDigit(data.QianghuaJieshu) .. LocalString.GetString("阶")
                    this.jieshuLabel.gameObject:SetActive(data.QianghuaJieshu > 0)

                    this.attrQualDescLabel.text = System.String.Format(LocalString.GetString("{0}阶"), EnumChineseDigits.GetDigit(data.QianghuaJieshu))
                    this.attrQualIncrLabel.text = System.String.Format(LocalString.GetString("套装属性增加[c][00FF00] {0:N2}%[-][/c]"), CZuoQiMgr.CalcTaozhuangAttrIncrease(data, 0) * 100)

                    this:InitWords(data)

                    if bSetPos then
                        this.lingfuRingSprite.fillAmount = data.QianghuaXingji / CZuoQiMgr.sMaxQianghuaXingji
                        this:InitStars(this.lingfuStars, data.QianghuaXingji)
                        this:InitStars(this.attrStars, data.QianghuaXingji)
                        this:UpdateWordQianghua(1)

                        this.progressFX:LoadFx(CUIFxPaths.LingfuQianghuaRingFx)
                        local hudu = (data.QianghuaXingji * 2 * PI) / (CZuoQiMgr.sMaxQianghuaXingji)
                        this.progressFX.transform.localPosition = Vector3(math.sin(hudu + PI) * 235, - math.cos(hudu + PI) * 235, 0)

                        this.progressFX.gameObject:SetActive(data.QianghuaXingji > 0)
                        this.btnItemCountLabel.transform.parent.parent.gameObject:SetActive(not (data.QianghuaJieshu == CZuoQiMgr.sMaxQianghuaJieshu and data.QianghuaXingji == CZuoQiMgr.sMaxQianghuaXingji))
                    else
                        this.mTargetStarCount = data.QianghuaXingji
                        this.mUpdateStartTime = Time.realtimeSinceStartup
                        this.mLastUpdateTime = 0

                        do
                            local i = 0
                            while i < CZuoQiMgr.sMaxQianghuaXingji do
                                this:ShowStar(this.lingfuStars, 0, i)
                                this:ShowStar(this.attrStars, 0, i)
                                i = i + 1
                            end
                        end

                        this:UnRegisterTick()
                        this.mUpdateTick = CTickMgr.Register(DelegateFactory.Action(function () 
                            this:OnTick()
                        end), 33, ETickType.Loop)
                    end

                    local consumeItemTid = ZuoQi_Setting.GetData().LingfuQianghuaItemId
                    local itemHasCount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, consumeItemTid)
                    local needItemCount = 0
                    if data.QianghuaXingji == CZuoQiMgr.sMaxQianghuaXingji or data.QianghuaJieshu == 0 then
                        this.btnBtn.text = LocalString.GetString("升阶")
                        if data.QianghuaJieshu == CZuoQiMgr.sMaxQianghuaJieshu then
                            CUICommonDef.SetActive(this.btnBtn.gameObject, false, true)
                        else
                            local qianghuaData = ZuoQi_LingfuQianghua.GetData(data.QianghuaJieshu + 1)
                            needItemCount = qianghuaData.ShengjieConsume
                        end

                        this.btnItemCountLabel.transform.parent.parent.gameObject:SetActive(false)
                        this.mbNeedShengjieTween = true
                    else
                        this.btnBtn.text = LocalString.GetString("强化")
                        local qianghuaData = ZuoQi_LingfuQianghua.GetData(data.QianghuaJieshu)
                        needItemCount = qianghuaData.QianghuaConsume
                    end
                    this.btnItemCountLabel.text = System.String.Format(LocalString.GetString("{0}/{1}"), itemHasCount, needItemCount)
                    this.btnItemCountLabel.color = (itemHasCount >= needItemCount) and Color.white or Color.red
                    this.getConsumeItemGO:SetActive((itemHasCount < needItemCount))

                    local consumeItemData = Item_Item.GetData(consumeItemTid)
                    this.btnItemTex:LoadMaterial(consumeItemData.Icon)

                    return
                end
            end
        end
    end
end
CLingfuQianghuaWnd.m_InitWords_CS2LuaHook = function (this, data) 
    CommonDefs.ListClear(this.mDescItemList)
    CommonDefs.ListClear(this.mWordsList)

    CommonDefs.DictIterate(data.Words, DelegateFactory.Action_object_object(function (___key, ___value) 
        local p = {}
        p.Key = ___key
        p.Value = ___value
        CommonDefs.ListAdd(this.mWordsList, typeof(UInt32), p.Key)
    end))
    local c = CLingfuBaptizeWnd.GetWordColor(this.mWordsList.Count, data.Quality)
    Extensions.RemoveAllChildren(this.attrTbl.transform)
    do
        local i = 0
        while i < this.mWordsList.Count do
            local itemcell = CommonDefs.Object_Instantiate(this.wordTemplate)
            itemcell.transform.parent = this.attrTbl.transform
            itemcell.transform.localScale = Vector3.one
            itemcell.gameObject:SetActive(true)

            local simpleItem = CommonDefs.GetComponent_GameObject_Type(itemcell, typeof(CEquipBaptizeDescItem))
            simpleItem:Init(this.mWordsList[i])
            simpleItem.color = c

            CommonDefs.ListAdd(this.mDescItemList, typeof(CEquipBaptizeDescItem), simpleItem)
            i = i + 1
        end
    end
    this.attrTbl:Reposition()
    this.attrScrolView:ResetPosition()

    this.attrNoWordLabel.gameObject:SetActive(not CZuoQiMgr.LingFuHasCiTiao(CommonDefs.ConvertIntToEnum(typeof(EnumMapiLingfuPos), data.Pos)))
end
CLingfuQianghuaWnd.m_InitStars_CS2LuaHook = function (this, stars, xingji) 
    do
        local i = 0
        while i < CZuoQiMgr.sMaxQianghuaXingji do
            this:ShowStar(stars, xingji, i)
            i = i + 1
        end
    end
end
CLingfuQianghuaWnd.m_ShowStar_CS2LuaHook = function (this, stars, xingji, i) 
    local trans = stars.transform:Find(System.String.Format("Star{0}/FullStar", i + 1))
    if trans ~= nil then
        if xingji > i then
            trans.gameObject:SetActive(true)
        else
            trans.gameObject:SetActive(false)
        end
    end
    local fxTrans = stars.transform:Find(System.String.Format("Star{0}/Fx", i + 1))
    if fxTrans ~= nil then
        local fx = CommonDefs.GetComponent_GameObject_Type(fxTrans.gameObject, typeof(CUIFx))
        if xingji > i then
            fx:LoadFx(CUIFxPaths.LingfuQianghuaXingxingFx)
        end
        fx.gameObject:SetActive(xingji > i)
    end
end
CLingfuQianghuaWnd.m_OnEnable_CS2LuaHook = function (this) 
    EventManager.AddListenerInternal(EnumEventType.OnQianghuaLingfuSuccess, MakeDelegateFromCSFunction(this.OnQianghuaLingfuSuccess, MakeGenericClass(Action2, String, Boolean), this))
    EventManager.AddListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
    EventManager.AddListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))

    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.TryClose, VoidDelegate, this), true)
    UIEventListener.Get(this.btnBtn.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.btnBtn.gameObject).onClick, MakeDelegateFromCSFunction(this.OnBtnClick, VoidDelegate, this), true)
    UIEventListener.Get(this.btnItemTex.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.btnItemTex.gameObject).onClick, MakeDelegateFromCSFunction(this.OnConsumeItemClick, VoidDelegate, this), true)
    UIEventListener.Get(this.lingfuItemTex.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.lingfuItemTex.gameObject).onClick, MakeDelegateFromCSFunction(this.OnLingfuItemClick, VoidDelegate, this), true)
    UIEventListener.Get(this.questionBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.questionBtn).onClick, MakeDelegateFromCSFunction(this.OnQuestionClick, VoidDelegate, this), true)
end
CLingfuQianghuaWnd.m_OnDisable_CS2LuaHook = function (this) 
    EventManager.RemoveListenerInternal(EnumEventType.OnQianghuaLingfuSuccess, MakeDelegateFromCSFunction(this.OnQianghuaLingfuSuccess, MakeGenericClass(Action2, String, Boolean), this))
    EventManager.RemoveListenerInternal(EnumEventType.SetItemAt, MakeDelegateFromCSFunction(this.OnSetItemAt, MakeGenericClass(Action4, EnumItemPlace, UInt32, String, String), this))
    EventManager.RemoveListenerInternal(EnumEventType.SendItem, MakeDelegateFromCSFunction(this.OnSendItem, MakeGenericClass(Action1, String), this))

    UIEventListener.Get(this.closeButton).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.closeButton).onClick, MakeDelegateFromCSFunction(this.TryClose, VoidDelegate, this), false)
    UIEventListener.Get(this.btnBtn.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.btnBtn.gameObject).onClick, MakeDelegateFromCSFunction(this.OnBtnClick, VoidDelegate, this), false)
    UIEventListener.Get(this.btnItemTex.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.btnItemTex.gameObject).onClick, MakeDelegateFromCSFunction(this.OnConsumeItemClick, VoidDelegate, this), false)
    UIEventListener.Get(this.lingfuItemTex.gameObject).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.lingfuItemTex.gameObject).onClick, MakeDelegateFromCSFunction(this.OnLingfuItemClick, VoidDelegate, this), false)
    UIEventListener.Get(this.questionBtn).onClick = CommonDefs.CombineListner_VoidDelegate(UIEventListener.Get(this.questionBtn).onClick, MakeDelegateFromCSFunction(this.OnQuestionClick, VoidDelegate, this), false)

    CLingfuQianghuaWnd.sQianghuaLingfuId = nil
    this:UnRegisterTick()
end
CLingfuQianghuaWnd.m_OnConsumeItemClick_CS2LuaHook = function (this, go) 
    local item = CItemMgr.Inst:GetById(CLingfuQianghuaWnd.sQianghuaLingfuId)
    if item == nil or item.Item.LingfuItemInfo == nil then
        return
    end
    local data = item.Item.LingfuItemInfo

    local consumeItemTid = ZuoQi_Setting.GetData().LingfuQianghuaItemId
    local itemHasCount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, consumeItemTid)
    local needItemCount = 0
    if data.QianghuaXingji == CZuoQiMgr.sMaxQianghuaXingji or data.QianghuaJieshu == 0 then
        if data.QianghuaJieshu == CZuoQiMgr.sMaxQianghuaJieshu then
            return
        else
            local qianghuaData = ZuoQi_LingfuQianghua.GetData(data.QianghuaJieshu + 1)
            needItemCount = qianghuaData.ShengjieConsume
        end
    else
        local qianghuaData = ZuoQi_LingfuQianghua.GetData(data.QianghuaJieshu)
        needItemCount = qianghuaData.QianghuaConsume
    end
    if needItemCount > itemHasCount then
        CItemAccessListMgr.Inst:ShowItemAccessInfo(consumeItemTid, false, go.transform, AlignType.Right)
    else
        CItemInfoMgr.ShowLinkItemTemplateInfo(consumeItemTid, false, nil, AlignType1.Default, 0, 0, 0, 0)
    end
end
CLingfuQianghuaWnd.m_UpdateConsumeItem_CS2LuaHook = function (this) 
    if CLingfuQianghuaWnd.sQianghuaLingfuId == nil then
        return
    end

    local item = CItemMgr.Inst:GetById(CLingfuQianghuaWnd.sQianghuaLingfuId)
    if item == nil or item.Item.LingfuItemInfo == nil then
        return
    end
    local data = item.Item.LingfuItemInfo

    local consumeItemTid = ZuoQi_Setting.GetData().LingfuQianghuaItemId
    local itemHasCount = CItemMgr.Inst:GetItemAmountByTemplateId(EnumItemPlace.Bag, consumeItemTid)
    local needItemCount = 0
    if data.QianghuaXingji == CZuoQiMgr.sMaxQianghuaXingji or data.QianghuaJieshu == 0 then
        if data.QianghuaJieshu ~= CZuoQiMgr.sMaxQianghuaJieshu then
            local qianghuaData = ZuoQi_LingfuQianghua.GetData(data.QianghuaJieshu + 1)
            needItemCount = qianghuaData.ShengjieConsume
        end
    else
        local qianghuaData = ZuoQi_LingfuQianghua.GetData(data.QianghuaJieshu)
        needItemCount = qianghuaData.QianghuaConsume
    end
    this.btnItemCountLabel.text = System.String.Format(LocalString.GetString("{0}/{1}"), itemHasCount, needItemCount)
    this.btnItemCountLabel.color = (itemHasCount >= needItemCount) and Color.white or Color.red
    this.getConsumeItemGO:SetActive((itemHasCount < needItemCount))
end
CLingfuQianghuaWnd.m_OnQianghuaLingfuSuccess_CS2LuaHook = function (this, lingfuId, bIsShengjie) 
    if lingfuId == CLingfuQianghuaWnd.sQianghuaLingfuId and CLingfuQianghuaWnd.sQianghuaLingfuId ~= nil then
        local item = CItemMgr.Inst:GetById(CLingfuQianghuaWnd.sQianghuaLingfuId)
        if not (item ~= nil and item.Item.LingfuItemInfo ~= nil) then
            return
        end

        if bIsShengjie then
            local posTween = CommonDefs.GetComponent_GameObject_Type(this.lingfuGO, typeof(TweenPosition))
            if posTween ~= nil then
                posTween.enabled = true
                if item.Item.LingfuItemInfo.QianghuaXingji == CZuoQiMgr.sMaxQianghuaXingji then
                    this.attributeGO:SetActive(false)
                    CommonDefs.ListClear(posTween.onFinished)
                    posTween:PlayForward()
                else
                    posTween:PlayReverse()
                    posTween:AddOnFinished(MakeDelegateFromCSFunction(this.OnTweenFinished, Callback, this))
                end
            end

            local scaleTween = CommonDefs.GetComponent_GameObject_Type(this.jieshuLabel.gameObject, typeof(TweenScale))
            if scaleTween ~= nil then
                scaleTween.enabled = true
                scaleTween:PlayForward()
            end
        else
            if item.Item.LingfuItemInfo.QianghuaXingji == CZuoQiMgr.sMaxQianghuaXingji then
                this.btnItemCountLabel.transform.parent.parent.gameObject:SetActive(false)
                this.mbNeedShengjieTween = true
            end
        end
        this:InitWnd(false)
    end
end
CLingfuQianghuaWnd.m_OnBtnClick_CS2LuaHook = function (this, go) 
    if CLingfuQianghuaWnd.sQianghuaLingfuId == nil then
        return
    end

    if this.mbNeedShengjieTween then
        local posTween = CommonDefs.GetComponent_GameObject_Type(this.lingfuGO, typeof(TweenPosition))
        if posTween ~= nil then
            this.attributeGO:SetActive(false)
            posTween.enabled = true
            CommonDefs.ListClear(posTween.onFinished)
            posTween:PlayForward()
        end

        this.mbNeedShengjieTween = false
        this.btnItemCountLabel.transform.parent.parent.gameObject:SetActive(true)
        return
    end

    local item = CItemMgr.Inst:GetById(CLingfuQianghuaWnd.sQianghuaLingfuId)
    if item ~= nil and item.Item.LingfuItemInfo ~= nil then
        local bIsShengjie = (item.Item.LingfuItemInfo.QianghuaJieshu == 0 or item.Item.LingfuItemInfo.QianghuaXingji == CZuoQiMgr.sMaxQianghuaXingji)
        local zuoqiId = CLingfuView.sCurZuoQiId
        if zuoqiId == nil then
            zuoqiId = ""
        end

        Gac2Gas.RequestQianghuaLingfu(CLingfuQianghuaWnd.sQianghuaLingfuId, bIsShengjie, zuoqiId)
    end
end
