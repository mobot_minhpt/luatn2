local DelegateFactory      = import "DelegateFactory"
local TweenPosition        = import "TweenPosition"
local TweenAlpha           = import "TweenAlpha"
local TweenRotation        = import "TweenRotation"
local Animation            = import "UnityEngine.Animation"
local UIWidget             = import "UIWidget"
local UIVariableScrollView = import "L10.UI.UIVariableScrollView"
local LuaUtils             = import "LuaUtils"
local CConversationMgr     = import "L10.Game.CConversationMgr"
local LuaTweenUtils        = import "LuaTweenUtils"
local UIEventListener      = import "UIEventListener"
local UICamera             = import "UICamera"
local Vector3              = import "UnityEngine.Vector3"
local UIPanel              = import "UIPanel"
local CUIManager           = import "L10.UI.CUIManager"
local Ease                 = import "DG.Tweening.Ease"

LuaFanKanHuaCeWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
--@endregion RegistChildComponent end

RegistClassMember(LuaFanKanHuaCeWnd, "book")
RegistClassMember(LuaFanKanHuaCeWnd, "down")
RegistClassMember(LuaFanKanHuaCeWnd, "up")
RegistClassMember(LuaFanKanHuaCeWnd, "downFx")
RegistClassMember(LuaFanKanHuaCeWnd, "upFx")
RegistClassMember(LuaFanKanHuaCeWnd, "bgFx")
RegistClassMember(LuaFanKanHuaCeWnd, "scroll")
RegistClassMember(LuaFanKanHuaCeWnd, "brush")
RegistClassMember(LuaFanKanHuaCeWnd, "variableScrollView")
RegistClassMember(LuaFanKanHuaCeWnd, "portraits")
RegistClassMember(LuaFanKanHuaCeWnd, "anis")
RegistClassMember(LuaFanKanHuaCeWnd, "aniClips")

RegistClassMember(LuaFanKanHuaCeWnd, "tick")
RegistClassMember(LuaFanKanHuaCeWnd, "batDialogIds")
RegistClassMember(LuaFanKanHuaCeWnd, "taskDialogIds")
RegistClassMember(LuaFanKanHuaCeWnd, "centerId")
RegistClassMember(LuaFanKanHuaCeWnd, "dragStartX")
RegistClassMember(LuaFanKanHuaCeWnd, "needSwitch")

function LuaFanKanHuaCeWnd:Awake()
    --@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self:InitUIComponents()
    self:InitAni()
    self:InitDragListener()
    self:InitBrushActive()
end

-- 初始化UI组件
function LuaFanKanHuaCeWnd:InitUIComponents()
    self.book = self.transform:Find("Book").gameObject
    self.down = self.book.transform:Find("Down")
    self.up = self.down:Find("Up")
    self.downFx = self.book.transform:Find("DownFx"):GetComponent(typeof(CUIFx))
    self.upFx = self.book.transform:Find("UpFx"):GetComponent(typeof(CUIFx))
	self.bgFx = self.transform:Find("BgWnd/Panel/Fx"):GetComponent(typeof(CUIFx))
    self.scroll = self.transform:Find("Scroll"):GetComponent(typeof(UIWidget))
    self.variableScrollView = self.scroll.transform:Find("ScrollView"):GetComponent(typeof(UIVariableScrollView))

    self.portraits = {}
    local grid = self.scroll.transform:Find("ScrollView/Grid")
    for i = 0, grid.childCount - 1 do
        table.insert(self.portraits, grid:GetChild(i))
    end
end

-- 初始化拖拽响应
function LuaFanKanHuaCeWnd:InitDragListener()
    local dragGo = self.scroll.transform:Find("DragRegion").gameObject

    UIEventListener.Get(dragGo).onDragStart = DelegateFactory.VoidDelegate(function(go)
		self.dragStartX = UICamera.currentTouch.pos.x
    end)

    UIEventListener.Get(dragGo).onDragEnd = DelegateFactory.VoidDelegate(function(go)
		local diff = UICamera.currentTouch.pos.x - self.dragStartX
        if diff < -50 then
            if self.needSwitch then
                LuaTweenUtils.DOKill(self.portraits[self.centerId])
                self.centerId = self.centerId + 1
                self.variableScrollView:CenterOn(self.centerId - 1)
                self.needSwitch = false
            end
		end
    end)
end

-- 初始化动画
function LuaFanKanHuaCeWnd:InitAni()
    self.aniClips = {"FanKanHuaCe_Portrait1", "FanKanHuaCe_Portrait2", "FanKanHuaCe_Portrait3"}
    self.anis = {}
    for i = 1, #self.portraits do
        local ani = self.portraits[i]:GetComponent(typeof(Animation))
        self:SampleAni(ani, self.aniClips[i], 0)
        table.insert(self.anis, ani)
    end
end

-- 初始化画笔active
function LuaFanKanHuaCeWnd:InitBrushActive()
    for i = 1, #self.portraits do
        self.portraits[i]:Find("Brush").gameObject:SetActive(false)
    end
end


function LuaFanKanHuaCeWnd:Init()
    self:InitClassMembers()
    self:InitBook()
    self:InitScrollView()
end

function LuaFanKanHuaCeWnd:InitClassMembers()
    self.batDialogIds = {327, 328, 329}
    self.taskDialogIds = {2170, 2171, 2172}
end

-- 初始化书本
function LuaFanKanHuaCeWnd:InitBook()
    self.book:SetActive(true)
    self.scroll.alpha = 0
	local tweenPosition = self.up:GetComponent(typeof(TweenPosition))
	local tweenRotation = self.up:GetComponent(typeof(TweenRotation))
	tweenPosition:ResetToBeginning()
	tweenRotation:ResetToBeginning()
	tweenPosition:PlayForward()
	tweenRotation:PlayForward()

	tweenPosition:SetOnFinished(DelegateFactory.Callback(function ()
		self:FadeInAndOut()
	end))

	self.upFx:LoadFx(g_UIFxPaths.RuMengJiUpFxPath)
	self.downFx:LoadFx(g_UIFxPaths.RuMengJiDownFxPath)
    self.bgFx:LoadFx(g_UIFxPaths.RuMengJiBgFxPath)
end

-- 封面逐渐消失，书签逐渐显现
function LuaFanKanHuaCeWnd:FadeInAndOut()
	self.upFx:DestroyFx()
	self.downFx:DestroyFx()

	local tweenPosition = self.down:GetComponent(typeof(TweenPosition))
	local tweenAlpha = self.down:GetComponent(typeof(TweenAlpha))
	tweenPosition:ResetToBeginning()
	tweenAlpha:ResetToBeginning()
	tweenPosition:PlayForward()
	tweenAlpha:PlayForward()

    local duration = tweenPosition.duration
    LuaTweenUtils.TweenAlpha(self.scroll, 0, 1, duration, function()
		self.book:SetActive(false)
        self:PlayAni()
        self:PlayBatDialog()
	end)
end

-- 初始化scroll view
function LuaFanKanHuaCeWnd:InitScrollView()
    self.variableScrollView:InitItems(0)
    self.centerId = 1

	self.variableScrollView.onFinished = LuaUtils.OnFinished(function()
        if self.centerId > 1 then
            self:PlayAni()
            self:PlayBatDialog()
        end
	end)
end

-- 播放动画
function LuaFanKanHuaCeWnd:PlayAni()
    local id = self.centerId
    self.anis[id]:get_Item(self.aniClips[id]).time = 0
    self.anis[id]:Play()
end

-- 播放batDialog
function LuaFanKanHuaCeWnd:PlayBatDialog()
    local batDialogId = self.batDialogIds[self.centerId]

    CLuaBatDialogWnd.ShowWnd(batDialogId)
    local time = Dialog_BatDialog.GetData(batDialogId).Time
    self:StartTick(time)
end

-- 开始计时
function LuaFanKanHuaCeWnd:StartTick(time)
    self:ClearTick()

    self.tick = RegisterTickOnce(function()
        local taskDialogId = self.taskDialogIds[self.centerId]
        local dialogMsg = Dialog_TaskDialog.GetData(taskDialogId).Dialog
        CConversationMgr.Inst:OpenStoryDialog(dialogMsg, DelegateFactory.Action(function ()
            if self.centerId == 3 then
                local trans = self.variableScrollView.transform
                self:SetPositionX(trans, 0)

                local panel = trans:GetComponent(typeof(UIPanel))
                local co = panel.clipOffset
                co.x = 0
                panel.clipOffset = co

                self:SetPositionX(self.portraits[1], -540)
                self:SetPositionX(self.portraits[2], 0)
                self:SetPositionX(self.portraits[3], 540)
                self.portraits[1].localScale = Vector3.one
                self.portraits[2].localScale = Vector3.one
                self.portraits[3].localScale = Vector3.one

                self:FadeInAndOutReverse()
            else
                self:PlayTween()
                self.needSwitch = true
            end

            if self.centerId == 1 then
                g_MessageMgr:ShowMessage("FanKanHuaCe_TurnPage_Tip")
            end
        end))
    end, 1000 * time)
end

-- 设置x坐标
function LuaFanKanHuaCeWnd:SetPositionX(trans, x)
    local pos = trans.localPosition
    pos.x = x
    trans.localPosition = pos
end

-- 播放动效
function LuaFanKanHuaCeWnd:PlayTween()
    local trans = self.portraits[self.centerId]
    local tweenRotation = LuaTweenUtils.TweenRotationZ(trans, -8.0, 0.5)
    LuaTweenUtils.SetLoops(tweenRotation, -1, true)
    LuaTweenUtils.SetEase(tweenRotation, Ease.Linear)

    local tweenPosition = LuaTweenUtils.TweenPositionX(trans, trans.localPosition.x - 20, 0.5)
    LuaTweenUtils.SetLoops(tweenPosition, -1, true)
    LuaTweenUtils.SetEase(tweenPosition, Ease.Linear)
end

-- 封面逐渐出现，书签逐渐消失
function LuaFanKanHuaCeWnd:FadeInAndOutReverse()
    self.book:SetActive(true)
	local tweenPosition = self.down:GetComponent(typeof(TweenPosition))
	local tweenAlpha = self.down:GetComponent(typeof(TweenAlpha))
    tweenPosition.to, tweenPosition.from = tweenPosition.from, tweenPosition.to
    tweenAlpha.to, tweenAlpha.from = tweenAlpha.from, tweenAlpha.to

	tweenPosition:ResetToBeginning()
	tweenAlpha:ResetToBeginning()
	tweenPosition:PlayForward()
	tweenAlpha:PlayForward()

    local duration = tweenPosition.duration
    LuaTweenUtils.TweenAlpha(self.scroll, 1, 0, duration, function()
        self:InitBookReverse()
	end)
end

function LuaFanKanHuaCeWnd:InitBookReverse()
	local tweenPosition = self.up:GetComponent(typeof(TweenPosition))
	local tweenRotation = self.up:GetComponent(typeof(TweenRotation))
    tweenPosition.to, tweenPosition.from = tweenPosition.from, tweenPosition.to
    tweenRotation.to, tweenRotation.from = tweenRotation.from, tweenRotation.to

	tweenPosition:ResetToBeginning()
	tweenRotation:ResetToBeginning()
	tweenPosition:PlayForward()
	tweenRotation:PlayForward()

	tweenPosition:SetOnFinished(DelegateFactory.Callback(function ()
        LuaWuMenHuaShiMgr:SendFinishEvent(22111641, "LZHuiWeiWuQiong", false)
		CUIManager.CloseUI(CLuaUIResources.FanKanHuaCeWnd)
	end))

	self.upFx:LoadFx(g_UIFxPaths.RuMengJiUpFxPath)
	self.downFx:LoadFx(g_UIFxPaths.RuMengJiDownFxPath)
end

function LuaFanKanHuaCeWnd:SampleAni(anim, clipName, time)
	anim:get_Item(clipName).time = time
    anim:get_Item(clipName).enabled = true
    anim:get_Item(clipName).weight = 1
    anim:Sample()
    anim:get_Item(clipName).enabled = false
end

-- 清除计时器
function LuaFanKanHuaCeWnd:ClearTick()
    if self.tick then
		UnRegisterTick(self.tick)
		self.tick = nil
	end
end

function LuaFanKanHuaCeWnd:OnDestroy()
    self:ClearTick()

    LuaTweenUtils.DOKill(self.scroll.transform)
end

--@region UIEvent
--@endregion UIEvent
