local UILabel = import "UILabel"
local UISprite = import "UISprite"
local CUITexture = import "L10.UI.CUITexture"
local GameObject = import "UnityEngine.GameObject"
local CServerTimeMgr   = import "L10.Game.CServerTimeMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local Profession = import "L10.Game.Profession"
local CSkillItemCell = import "L10.UI.CSkillItemCell"
local CSkillInfoMgr = import "L10.UI.CSkillInfoMgr"
local CItemInfoMgrAlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemAccessListMgr = import "L10.UI.CItemAccessListMgr"
local CTooltip = import "L10.UI.CTooltip"
local Animation = import "UnityEngine.Animation"
local UILabelOverflow = import "UILabel+Overflow"

LuaSnowAdventureSnowManWnd = class()
LuaSnowAdventureSnowManWnd.ShowCountdownPattern = false

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaSnowAdventureSnowManWnd, "CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "TimeProgressBarBg", "TimeProgressBarBg", GameObject)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "RemainTimeLabel", "RemainTimeLabel", UILabel)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "TimeProgressBar", "TimeProgressBar", UISprite)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "SnowManTexture", "SnowManTexture", CUITexture)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "careerButton1", "careerButton1", GameObject)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "careerButton2", "careerButton2", GameObject)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "careerButton3", "careerButton3", GameObject)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "careerButton4", "careerButton4", GameObject)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "careerButton5", "careerButton5", GameObject)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "careerButton6", "careerButton6", GameObject)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "SnowManInfo", "SnowManInfo", GameObject)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "SnowmanName", "SnowmanName", UILabel)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "LockInfo", "LockInfo", GameObject)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "LockText", "LockText", UILabel)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "PlayerScore", "PlayerScore", UILabel)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "UnlockButton", "UnlockButton", GameObject)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "UnlockInfo", "UnlockInfo", GameObject)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "SkillContent", "SkillContent", GameObject)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "SkillItemTemplate", "SkillItemTemplate", GameObject)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "DetailButton", "DetailButton", GameObject)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "SnowmanLevel", "SnowmanLevel", UILabel)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "ImproveButton", "ImproveButton", GameObject)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "SetBattleButton", "SetBattleButton", GameObject)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "SetBattleButtonText", "SetBattleButtonText", UILabel)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "SetBattleButtonTips", "SetBattleButtonTips", UILabel)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "RuleButton", "RuleButton", GameObject)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "ImproveDetailInfo", "ImproveDetailInfo", GameObject)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "ImproveDetailPropertyContent", "ImproveDetailPropertyContent", GameObject)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "ImproveDetailPropertyPair", "ImproveDetailPropertyPair", GameObject)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "ImproveDetailSnowmanLevel", "ImproveDetailSnowmanLevel", UILabel)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "ImproveSendRPCButton", "ImproveSendRPCButton", GameObject)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "ImproveDetailConsumeItem", "ImproveDetailConsumeItem", GameObject)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "ResetButton", "ResetButton", GameObject)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "ImproveCloseButton", "ImproveCloseButton", GameObject)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "PropertyContent", "PropertyContent", GameObject)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "PropertyPair", "PropertyPair", GameObject)
RegistChildComponent(LuaSnowAdventureSnowManWnd, "MaxLevel", "MaxLevel", GameObject)

--@endregion RegistChildComponent end

function LuaSnowAdventureSnowManWnd:Awake()
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self:InitWndData()
    self:RefreshVariableUI()
    self:InitUIEvent()
end

function LuaSnowAdventureSnowManWnd:Init()

end

function LuaSnowAdventureSnowManWnd:InitWndData()
    self.prepareDuration = HanJia2023_XuePoLiXianSetting.GetData().PrepareDuration

    self.battleSnowmanType = LuaHanJia2023Mgr.snowAdventureData and LuaHanJia2023Mgr.snowAdventureData.lastSnowmanType or 0
    self.selectSnowmanType = self.battleSnowmanType

    self.skillList = {}
    self.attributeList = {}
    self.improveAttributeList = {}

    self.improveItemId = HanJia2023_XuePoLiXianSetting.GetData().SnowmanLvupItem

    --最后一级为满级
    self.snowmanMaxLevel = 0
    HanJia2023_XuePoLiXianSnowmanLevel.Foreach(function (id, _)
        if id > self.snowmanMaxLevel then
            self.snowmanMaxLevel = id
        end
    end)
    self.initEventOnce = false

    self.wndAnimation = self.transform:GetComponent(typeof(Animation))
end

function LuaSnowAdventureSnowManWnd:OnRefreshVariableUI()
    if LuaHanJia2023Mgr.snowAdventureData == nil then
        return
    end

    self.battleSnowmanType = LuaHanJia2023Mgr.snowAdventureData.lastSnowmanType
    self:RefreshVariableUI()
end

function LuaSnowAdventureSnowManWnd:RefreshVariableUI()
    if LuaHanJia2023Mgr.snowAdventureData == nil then
        return
    end

    self:RefreshCountdown(LuaSnowAdventureSnowManWnd.ShowCountdownPattern,
            LuaHanJia2023Mgr.multipleTeamInfo and LuaHanJia2023Mgr.multipleTeamInfo.startTs or {}, self.prepareDuration)
    self:RefreshWheel()
    self:RefreshSnowmanDetail(false)

    if self.initEventOnce == false then
        self:InitUIEvent()
    end
end

function LuaSnowAdventureSnowManWnd:InitUIEvent()
    if LuaHanJia2023Mgr.snowAdventureData == nil then
        return
    end
    self.initEventOnce = true
    local buttonList = {self.careerButton1, self.careerButton2, self.careerButton3, self.careerButton4,
                        self.careerButton5, self.careerButton6}
    for i = 1, #buttonList do
        UIEventListener.Get(buttonList[i]).onClick = DelegateFactory.VoidDelegate(function (_)
            if self.selectSnowmanType ~= i then
                self.selectSnowmanType = i
                self:RefreshWheel()
                self:RefreshSnowmanDetail(true, true)
                self.transform:Find("CUIFx").gameObject:SetActive(false)
                self.transform:Find("CUIFx").gameObject:SetActive(true)
            end
        end)
    end

    UIEventListener.Get(self.UnlockButton).onClick = DelegateFactory.VoidDelegate(function (_)
        local snowmanConfig = HanJia2023_XuePoLiXianSnowman.GetData(self.selectSnowmanType)
        local unlockCost = snowmanConfig.UnlockCost
        if LuaHanJia2023Mgr.snowAdventureData.score >= unlockCost then
            Gac2Gas.XuePoLiXianUnlock(self.selectSnowmanType)
        else
            g_MessageMgr:ShowMessage("SnowAdventure_JifenNotSatisfy")
        end
    end)

    UIEventListener.Get(self.ResetButton).onClick = DelegateFactory.VoidDelegate(function (_)
        local formulaId = HanJia2023_XuePoLiXianSetting.GetData().ResetSnowmanCost
        local formula = AllFormulas.Action_Formula[formulaId] and AllFormulas.Action_Formula[formulaId].Formula or nil
        if formula == nil then
            return
        end
        local consumeScore = 0
        local level = LuaHanJia2023Mgr.snowAdventureData.snowmanInfo[self.selectSnowmanType]
        for i = 1, level-1 do
            consumeScore = consumeScore + HanJia2023_XuePoLiXianSnowmanLevel.GetData(i).Cost
        end
        local price = formula(nil, nil, {consumeScore})
        local message = g_MessageMgr:FormatMessage("SnowAdventure_ResetSnowmanTips", tostring(price))
        MessageWndManager.ShowOKCancelMessage(message, DelegateFactory.Action(function()
            Gac2Gas.XuePoLiXianReset(self.selectSnowmanType)
        end), nil)
    end)

    UIEventListener.Get(self.DetailButton).onClick = DelegateFactory.VoidDelegate(function (_)
        LuaSnowAdventurePropertyDetailWnd.snowmanType = self.selectSnowmanType
        CUIManager.ShowUI(CLuaUIResources.SnowAdventurePropertyDetailWnd)
    end)

    self.DetailButton:SetActive(not LuaHanJia2023Mgr.IsInXuePoLiXianPlay())

    UIEventListener.Get(self.ImproveButton).onClick = DelegateFactory.VoidDelegate(function (_)
        self.ImproveDetailInfo:SetActive(true)
        self.UnlockInfo:SetActive(false)
        self.wndAnimation:Play("snowadventuresnowmanwnd_qiehuan")
    end)

    UIEventListener.Get(self.ImproveCloseButton).onClick = DelegateFactory.VoidDelegate(function (_)
        self.ImproveDetailInfo:SetActive(false)
        self.UnlockInfo:SetActive(true)
        self.wndAnimation:Play("snowadventuresnowmanwnd_qiehuanhui")
    end)

    UIEventListener.Get(self.SetBattleButton).onClick = DelegateFactory.VoidDelegate(function (_)
        if self.battleSnowmanType ~= self.selectSnowmanType then
            Gac2Gas.XuePoLiXianSetSnowmanType(self.selectSnowmanType)
        end
    end)

    UIEventListener.Get(self.RuleButton).onClick = DelegateFactory.VoidDelegate(function (_)
        local selectSnowmanType = self.selectSnowmanType
        local namesArr = {"She", "Yi", "Jia", "Fang", "Dao", "Hua"}

        g_MessageMgr:ShowMessage("SnowAdventure_SnowmanTrainningTips_" .. namesArr[selectSnowmanType])
    end)

    local aquireGo = self.ImproveDetailConsumeItem.transform:Find("GetNode").gameObject
    aquireGo.transform:Find("Sprite").gameObject:SetActive(false)
    UIEventListener.Get(aquireGo).onClick = DelegateFactory.VoidDelegate(function (_)
        CItemAccessListMgr.Inst:ShowItemAccessInfo(self.improveItemId, false, aquireGo.transform, CTooltip.AlignType.Right)
    end)

    UIEventListener.Get(self.ImproveSendRPCButton).onClick = DelegateFactory.VoidDelegate(function (_)
        Gac2Gas.XuePoLiXianLevelUp(self.selectSnowmanType)
    end)

    UIEventListener.Get(self.CloseButton).onClick = DelegateFactory.VoidDelegate(function (_)
        CUIManager.CloseUI(CLuaUIResources.SnowAdventureSnowManWnd)
        if LuaSnowAdventureSingleStageMainWnd.FirstOpenStageId then
            CUIManager.ShowUI(CLuaUIResources.SnowAdventureSingleStageMainWnd)
        elseif LuaSnowAdventureMultipleStageMainWnd.FirstOpenStageId then
            CUIManager.ShowUI(CLuaUIResources.SnowAdventureMultipleStageMainWnd)
        end
    end)
end

function LuaSnowAdventureSnowManWnd:OnEnable()
    g_ScriptEvent:AddListener("HanJia2023_SyncXuePoLiXianData", self, "OnRefreshVariableUI")
    g_ScriptEvent:AddListener("HanJia2023_XuePoLiXianStopPrepare", self, "OnStopPrepare")
end

function LuaSnowAdventureSnowManWnd:OnDisable()
    g_ScriptEvent:RemoveListener("HanJia2023_SyncXuePoLiXianData", self, "OnRefreshVariableUI")
    g_ScriptEvent:RemoveListener("HanJia2023_XuePoLiXianStopPrepare", self, "OnStopPrepare")

    if self.countdownTick then
        UnRegisterTick(self.countdownTick)
        self.countdownTick = nil
    end
end

function LuaSnowAdventureSnowManWnd:OnStopPrepare()
    CUIManager.CloseUI(CLuaUIResources.SnowAdventureSnowManWnd)
    if CUIManager.IsLoaded(CLuaUIResources.SnowAdventureMultipleTeamWnd) then
        CUIManager.CloseUI(CLuaUIResources.SnowAdventureMultipleTeamWnd)
    end
end

function LuaSnowAdventureSnowManWnd:RefreshWheel()
    local buttonList = {self.careerButton1, self.careerButton2, self.careerButton3, self.careerButton4,
                        self.careerButton5, self.careerButton6}
    HanJia2023_XuePoLiXianSnowman.Foreach(function(id, cfg)
        local buttonObj = buttonList[id]
        local snowmanLevel = LuaHanJia2023Mgr.snowAdventureData.snowmanInfo[id]
        if snowmanLevel then
            --已解锁的
            buttonObj.transform:Find("Level"):GetComponent(typeof(UILabel)).text = snowmanLevel
            buttonObj.transform:Find("Level").gameObject:SetActive(true)
            buttonObj.transform:Find("IsLocked").gameObject:SetActive(false)
        else
            --未解锁的
            buttonObj.transform:Find("Level").gameObject:SetActive(false)
            buttonObj.transform:Find("IsLocked").gameObject:SetActive(true)
        end
        buttonObj.transform:Find("IsSelected").gameObject:SetActive(id == self.selectSnowmanType)
        buttonObj.transform:Find("IsBattle").gameObject:SetActive(id == self.battleSnowmanType)
        buttonObj.transform:GetComponent(typeof(UISprite)).spriteName = Profession.GetIconByNumber(cfg.Career)
        if id == self.selectSnowmanType then
            self.SnowManTexture:LoadMaterial(cfg.Pic)
        end
    end)
end

function LuaSnowAdventureSnowManWnd:RefreshSnowmanDetail(changeState, needAnimation)
    local isSelectSnowmanLocked = (LuaHanJia2023Mgr.snowAdventureData.snowmanInfo[self.selectSnowmanType] == nil)
    local snowmanConfig = HanJia2023_XuePoLiXianSnowman.GetData(self.selectSnowmanType)
    needAnimation = needAnimation and needAnimation or false
    self.SnowmanName.text = snowmanConfig.Name
    self.SnowmanName.transform:Find("IsBattle").gameObject:SetActive(self.selectSnowmanType == self.battleSnowmanType)
    if isSelectSnowmanLocked then
        if needAnimation and (self.UnlockInfo.activeInHierarchy or self.ImproveDetailInfo.activeInHierarchy) then
            self.wndAnimation:Play("snowadventuresnowmanwnd_qiehuan")
        end

        self.LockInfo:SetActive(true)
        self.UnlockInfo:SetActive(false)
        self.ImproveDetailInfo:SetActive(false)
        self:RefreshLockInfo(self.selectSnowmanType)
    else
        if needAnimation and self.ImproveDetailInfo.activeInHierarchy then
            self.wndAnimation:Play("snowadventuresnowmanwnd_qiehuanhui")
            self.LockInfo:SetActive(false)
            if changeState then
                self.UnlockInfo:SetActive(true)
                self.ImproveDetailInfo:SetActive(false)
            end
        elseif self.LockInfo.activeInHierarchy then
            --刚刚解锁
            self.UnlockInfo:SetActive(true)
            self.LockInfo:SetActive(false)
            self.ImproveDetailInfo:SetActive(false)
            self.wndAnimation:Play("snowadventuresnowmanwnd_qiehuanhui")
        elseif self.ImproveDetailInfo.activeInHierarchy and LuaHanJia2023Mgr.snowAdventureData.snowmanInfo[self.selectSnowmanType]>= self.snowmanMaxLevel then
            self.UnlockInfo:SetActive(true)
            self.LockInfo:SetActive(false)
            self.ImproveDetailInfo:SetActive(false)
            self.wndAnimation:Play("snowadventuresnowmanwnd_qiehuanhui")
        end
        self:RefreshUnlockInfo(self.selectSnowmanType)
    end
end

function LuaSnowAdventureSnowManWnd:RefreshLockInfo(snowmanType)
    local snowmanConfig = HanJia2023_XuePoLiXianSnowman.GetData(snowmanType)
    local snowmanName = snowmanConfig.Name
    local unlockCost = snowmanConfig.UnlockCost
    self.LockText.color = NGUIText.ParseColor24("FFFFFF", 0)
    self.LockText.text = System.String.Format(LocalString.GetString("[c][274882]解锁{0}雪人需要消耗[-][/c][c][e5a408]{1}[-][/c][c][274882]霜雪（当前霜雪 [-][/c][c][19ae05]{2}[-][/c][c][274882]）[-][/c]"),
            snowmanName, unlockCost, LuaHanJia2023Mgr.snowAdventureData.score)
    self.LockText.overflowMethod = UILabelOverflow.ResizeFreely
    self.PlayerScore.gameObject:SetActive(false)
end

function LuaSnowAdventureSnowManWnd:RefreshUnlockInfo(snowmanType)
    local snowmanLevel = LuaHanJia2023Mgr.snowAdventureData.snowmanInfo[snowmanType]
    local snowmanConfig = HanJia2023_XuePoLiXianSnowman.GetData(snowmanType)


    self.SnowmanLevel.transform:Find("Label"):GetComponent(typeof(UILabel)).text = snowmanLevel
    self.ImproveButton:SetActive(snowmanLevel < self.snowmanMaxLevel and (not LuaHanJia2023Mgr.IsInXuePoLiXianPlay()))
    self.MaxLevel:SetActive(snowmanLevel >= self.snowmanMaxLevel)
    self.ResetButton:SetActive(snowmanLevel > 1)

    local propTbl = nil
    if LuaHanJia2023Mgr.IsInXuePoLiXianPlay() then
        --在雪魄玩法里时,已加上变形Buff, 再次计算容易套娃
        propTbl = LuaHanJia2023Mgr:GetSnowmanProp(snowmanType, 0)
    else
        propTbl = LuaHanJia2023Mgr:GetSnowmanProp(snowmanType, snowmanLevel)
    end
    local childCount = self.PropertyContent.transform.childCount
    if childCount < #propTbl then
        if childCount == 0 then
            table.insert(self.attributeList, self.PropertyPair)
        end
        childCount = childCount + 1
        for i = childCount+1, #propTbl do
            local attributeGo = CommonDefs.Object_Instantiate(self.PropertyPair)
            attributeGo.transform.parent = self.PropertyContent.transform
            attributeGo.transform.localScale = Vector3.one
            attributeGo:SetActive(true)
            table.insert(self.attributeList, attributeGo)
        end
    end
    for i = 1, #self.attributeList do
        local nameLabel = self.attributeList[i].transform:Find("PropertyName"):GetComponent(typeof(UILabel))
        local valueLabel = self.attributeList[i].transform:Find("PropertyValue"):GetComponent(typeof(UILabel))
        nameLabel.text = propTbl[i].name

        if #propTbl[i].value == 1 then
            --name: A
            valueLabel.text = propTbl[i].value[1]
        elseif #propTbl[i].value == 2 then
            --name: A~B
            valueLabel.text = propTbl[i].value[1] .. "-" .. propTbl[i].value[2]
        end
    end

    local skillIds = g_LuaUtil:StrSplit(snowmanConfig.TransformSkill,";")
    local childCount = self.SkillContent.transform.childCount
    if childCount < #skillIds then
        for i = childCount+1, #skillIds do
            local skillGo = CommonDefs.Object_Instantiate(self.SkillItemTemplate)
            local cell = skillGo:GetComponent(typeof(CSkillItemCell))
            skillGo.transform.parent = self.SkillContent.transform
            skillGo.transform.localScale = Vector3.one
            skillGo:SetActive(true)
            table.insert(self.skillList, cell)
        end
    elseif childCount > #skillIds then
        for i = #skillIds+1, childCount do
            local cell = self.skillList[i]
            cell.gameObject:SetActive(false)
        end
    end
    for i = 1, #skillIds do
        --Skill
        local cell = self.skillList[i]
        local skillLevel = snowmanLevel
        --skillLevel可能会超过设定的上限, 往回退一下
        for _skillLevel = snowmanLevel, 1, -1 do
            if (Skill_AllSkills.Exists(tonumber(skillIds[i])*100+ _skillLevel)) then
                skillLevel = _skillLevel
                break
            end
        end
        local skillData = Skill_AllSkills.GetData(tonumber(skillIds[i]) * 100 + skillLevel)
        cell.gameObject:SetActive(true)
        cell:Init(tonumber(skillIds[i]), 0, 0, false, skillData.SkillIcon, false, nil)
        UIEventListener.Get(cell.gameObject).onClick = DelegateFactory.VoidDelegate(function (_)
            CSkillInfoMgr.ShowSkillInfoWnd(tonumber(skillIds[i])*100+skillLevel, true, 0, 0, nil)
        end)
    end

    if self.battleSnowmanType == snowmanType then
        --当前雪人为出战雪人
        self.SetBattleButtonTips.gameObject:SetActive(false)
        self.SetBattleButtonText.text = LocalString.GetString("出战中")
        --self.SetBattleButton.transform:GetComponent(typeof(UISprite)).spriteName = "common_btn_01_yellow"
    else
        self.SetBattleButtonTips.gameObject:SetActive(true)
        local battleSnowmanConfig = HanJia2023_XuePoLiXianSnowman.GetData(self.battleSnowmanType)
        local battleSnowmanName = battleSnowmanConfig.Name
        local battleSnowmanLevel = LuaHanJia2023Mgr.snowAdventureData.snowmanInfo[self.battleSnowmanType]
        self.SetBattleButtonTips.text = g_MessageMgr:FormatMessage("SnowAdventure_CurrentBattleSnowman", battleSnowmanLevel, battleSnowmanName)
        self.SetBattleButtonText.text = LocalString.GetString("设为出战雪人")
        --self.SetBattleButton.transform:GetComponent(typeof(UISprite)).spriteName = "common_btn_01_blue"
    end
    self:RefreshImproveDetail(snowmanType)
end

function LuaSnowAdventureSnowManWnd:RefreshImproveDetail(snowmanType)
    local snowmanLevel = LuaHanJia2023Mgr.snowAdventureData.snowmanInfo[snowmanType]
    if snowmanLevel >= self.snowmanMaxLevel then
        self.ImproveDetailInfo:SetActive(false)
        self.UnlockInfo:SetActive(true)
        return
    end
    local improveCost = HanJia2023_XuePoLiXianSnowmanLevel.GetData(snowmanLevel).Cost
    local itemCfg = Item_Item.GetData(self.improveItemId)
    if itemCfg == nil then
        return
    end
    local iconTexture = self.ImproveDetailConsumeItem.transform:Find("Icon"):GetComponent(typeof(CUITexture))
    iconTexture:LoadMaterial(itemCfg.Icon)
    local score = LuaHanJia2023Mgr.snowAdventureData.score
    local itemAmountLabel = self.ImproveDetailConsumeItem.transform:Find("NumLabel"):GetComponent(typeof(UILabel))
    local aquireGo = self.ImproveDetailConsumeItem.transform:Find("GetNode").gameObject
    if score >= improveCost then
        itemAmountLabel.text = SafeStringFormat3("%d/%d", score, improveCost)
        aquireGo:SetActive(false)
    else
        itemAmountLabel.text = SafeStringFormat3("[c][FF0000]%d[-][c]/%d", score, improveCost)
        aquireGo:SetActive(true)
    end

    self.ImproveDetailSnowmanLevel.transform:Find("srcValue"):GetComponent(typeof(UILabel)).text = snowmanLevel
    self.ImproveDetailSnowmanLevel.transform:Find("tarValue"):GetComponent(typeof(UILabel)).text = snowmanLevel+1
    local sourceProp = LuaHanJia2023Mgr:GetSnowmanProp(snowmanType, snowmanLevel)
    local targetProp = LuaHanJia2023Mgr:GetSnowmanProp(snowmanType, snowmanLevel+1)
    local childCount = self.ImproveDetailPropertyContent.transform.childCount
    if childCount < #sourceProp then
        if childCount == 0 then
            table.insert(self.improveAttributeList, self.ImproveDetailPropertyPair)
        end
        childCount = childCount + 1

        for i = childCount+1, #sourceProp do
            local attributeGo = CommonDefs.Object_Instantiate(self.ImproveDetailPropertyPair)
            attributeGo.transform.parent = self.ImproveDetailPropertyContent.transform
            attributeGo.transform.localScale = Vector3.one
            attributeGo:SetActive(true)
            table.insert(self.improveAttributeList, attributeGo)
        end
    end
    for i = 1, #self.improveAttributeList do
        local nameLabel = self.improveAttributeList[i].transform:Find("PropertyName"):GetComponent(typeof(UILabel))
        local valueLabel = self.improveAttributeList[i].transform:Find("PropertyValue"):GetComponent(typeof(UILabel))
        local value2Label = self.improveAttributeList[i].transform:Find("PropertyValue2"):GetComponent(typeof(UILabel))
        nameLabel.text = sourceProp[i].name

        if #sourceProp[i].value == 1 then
            --name: A
            valueLabel.text = sourceProp[i].value[1]
            value2Label.text = targetProp[i].value[1]
        elseif #sourceProp[i].value == 2 then
            --name: A~B
            valueLabel.text = sourceProp[i].value[1] .. "-" .. sourceProp[i].value[2]
            value2Label.text = targetProp[i].value[1] .. "-" .. targetProp[i].value[2]
        end
    end
end

function LuaSnowAdventureSnowManWnd:RefreshCountdown(ShowCountdownPattern, startTs, duration)
    self.RemainTimeLabel.gameObject:SetActive(ShowCountdownPattern)
    self.TimeProgressBarBg.gameObject:SetActive(ShowCountdownPattern)
    if ShowCountdownPattern then
        if self.countdownTick then
            UnRegisterTick(self.countdownTick)
            self.countdownTick = nil
        end
        local nowTs = CServerTimeMgr.Inst.timeStamp
        local tickDuration = 0
        local remainValue = startTs+duration-nowTs
        if remainValue > 0 then
            tickDuration = (remainValue)*1000
        end
        local commonColor = NGUIText.ParseColor24("0E3254",0)
        local warningColor = NGUIText.ParseColor24("FF0B0B",0)
        self:ChangeCountdown(remainValue, duration, commonColor, warningColor)
        self.countdownTick = RegisterTickWithDuration(function ()
            local nowTs = CServerTimeMgr.Inst.timeStamp
            remainValue = startTs+duration-nowTs
            self:ChangeCountdown(remainValue, duration, commonColor, warningColor)
        end, 30, tickDuration)
    end
end

function LuaSnowAdventureSnowManWnd:ChangeCountdown(remainValue, duration, commonColor, warningColor)
    local changeColorTime = 10

    self.RemainTimeLabel.text = g_MessageMgr:FormatMessage("SnowAdventure_CountdownReminder", math.floor(remainValue))
    self.TimeProgressBar.fillAmount = remainValue / duration
    if remainValue <= changeColorTime then
        --变红
        self.RemainTimeLabel.color = warningColor
        self.TimeProgressBar.color = warningColor
    else
        --普通
        self.RemainTimeLabel.color = commonColor
        self.TimeProgressBar.color = commonColor
    end
end

function LuaSnowAdventureSnowManWnd:GetGuideGo(methodName)
    if methodName == "GetCloseButton" then
        return self.CloseButton
    end
    return nil
end


--@region UIEvent

--@endregion UIEvent

