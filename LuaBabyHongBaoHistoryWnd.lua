local CChatLinkMgr=import "CChatLinkMgr"

local CBaseWnd = import "L10.UI.CBaseWnd"
local DelegateFactory = import "DelegateFactory"
local CommonDefs = import "L10.Game.CommonDefs"
local CUICommonDef = import "L10.UI.CUICommonDef"
local UILabel = import "UILabel"
local UITable = import "UITable"
local UIScrollView = import "UIScrollView"
local Extensions = import "Extensions"
local CUITexture = import "L10.UI.CUITexture"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local ChatPlayerLink = import "CChatLinkMgr.ChatPlayerLink"
local UICamera = import "UICamera"
local UIWidget = import "UIWidget"
local NGUIText = import "NGUIText"

CLuaBabyHongBaoHistoryWnd = class()
RegistClassMember(CLuaBabyHongBaoHistoryWnd,"m_CloseBtn")
RegistClassMember(CLuaBabyHongBaoHistoryWnd,"m_Portrait")
RegistClassMember(CLuaBabyHongBaoHistoryWnd,"m_NameLabel")
RegistClassMember(CLuaBabyHongBaoHistoryWnd,"m_ScrollView")
RegistClassMember(CLuaBabyHongBaoHistoryWnd,"m_Table")
RegistClassMember(CLuaBabyHongBaoHistoryWnd,"m_Template")

function CLuaBabyHongBaoHistoryWnd:Awake()
    
end

function CLuaBabyHongBaoHistoryWnd:Close()
	self.gameObject:GetComponent(typeof(CBaseWnd)):Close()
end

function CLuaBabyHongBaoHistoryWnd:Init()
	self.m_CloseBtn = self.transform:Find("Wnd_Bg/CloseButton").gameObject
	CommonDefs.AddOnClickListener(self.m_CloseBtn, DelegateFactory.Action_GameObject(function(go) self:Close() end), false)

	self.m_Portrait = self.transform:Find("Head/Border/Portrait"):GetComponent(typeof(CUITexture))
	self.m_NameLabel = self.transform:Find("Head/NameLabel"):GetComponent(typeof(UILabel))
	self.m_ScrollView = self.transform:Find("ScrollView"):GetComponent(typeof(UIScrollView))
	self.m_Table = self.transform:Find("ScrollView/Table"):GetComponent(typeof(UITable))
	self.m_Template = self.transform:Find("ScrollView/Item").gameObject

	self.m_Template:SetActive(false)

	self.m_Portrait:Clear()
	self.m_NameLabel.text = nil

	self:LoadData({})

	LuaBabyMgr.QueryBabyLuckyMoneyHistory()
end

function CLuaBabyHongBaoHistoryWnd:OnEnable()
	g_ScriptEvent:AddListener("QueryBabyLuckyMoneyHistoryDone", self, "QueryBabyLuckyMoneyHistoryDone")
end

function CLuaBabyHongBaoHistoryWnd:OnDisable()
	g_ScriptEvent:RemoveListener("QueryBabyLuckyMoneyHistoryDone", self, "QueryBabyLuckyMoneyHistoryDone")
end

function CLuaBabyHongBaoHistoryWnd:QueryBabyLuckyMoneyHistoryDone(dataTbl)
	local portrait = LuaBabyMgr.BabyPortraitInHistoryWnd
	local name = LuaBabyMgr.BabyNameInHistoryWnd
	self.m_Portrait:LoadNPCPortrait(portrait, false)
	self.m_NameLabel.text = name
	self:LoadData(dataTbl)
end

function CLuaBabyHongBaoHistoryWnd:LoadData(dataTbl)
	Extensions.RemoveAllChildren(self.m_Table.transform)

	for _,data in pairs(dataTbl) do
		local instance = CUICommonDef.AddChild(self.m_Table.gameObject, self.m_Template)
		instance:SetActive(true)
		local bg = instance.transform:GetComponent(typeof(UIWidget))
		local descLabel = instance.transform:Find("DescLabel"):GetComponent(typeof(UILabel))
		local moneyLabel = instance.transform:Find("MoneyLabel"):GetComponent(typeof(UILabel))

		local dt = CServerTimeMgr.ConvertTimeStampToZone8Time(data.finishTime)
		local timeInfo = ToStringWrap(dt, "yyyy-MM-dd ")
		local playerInfo = ChatPlayerLink.GenerateLink(data.playerId, data.playerName).logicTag

		local text = nil
		if data.handOver == EnumBabyLuckMoneyHandOverType.eFather then
			text = g_MessageMgr:FormatMessage("YANGYU_LUCKY_MONEY_HANDOVER_TIP_DAD", timeInfo, playerInfo)
		elseif data.handOver == EnumBabyLuckMoneyHandOverType.eMother then
			text = g_MessageMgr:FormatMessage("YANGYU_LUCKY_MONEY_HANDOVER_TIP_MUM", timeInfo, playerInfo)
		else
			local designdata = Baby_LuckyMoney.GetData(data.messageId)
			local message = designdata and designdata.Message or ""
			text = g_MessageMgr:FormatMessage("YANGYU_LUCKY_MONEY_NO_HANDOVER_TIP", timeInfo, playerInfo, message)
		end

		descLabel.text = SafeStringFormat3("[c][%s]%s[-][/c]", NGUIText.EncodeColor24(descLabel.color), text)

		moneyLabel.text = tostring(data.silverNum)

		bg.height = descLabel.localSize.y + 20

		CommonDefs.AddOnClickListener(descLabel.gameObject, DelegateFactory.Action_GameObject(function(go) self:OnLabelClick(descLabel) end), false)
	end

	self.m_Table:Reposition()
	self.m_ScrollView:ResetPosition()
end

function CLuaBabyHongBaoHistoryWnd:OnLabelClick(label)
	local url = label:GetUrlAtPosition(UICamera.lastWorldPosition)
    if url ~= nil then
        CChatLinkMgr.ProcessLinkClick(url, nil)
    end
end
