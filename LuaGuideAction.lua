local PlayerSettings = import "L10.Game.PlayerSettings"
local OpenType=import "L10.Game.COpenEntryMgr.OpenType"
local COpenEntryMgr=import "L10.Game.COpenEntryMgr"
local CameraFollow = import "L10.Engine.CameraControl.CameraFollow"

LuaGuideAction = {}

function LuaGuideAction.ExecuteAction(stepId)
    local designData = Guide_Guide.GetData(stepId)
    if designData then
        local raw = designData.OnEnter
        if raw and raw~="" then
            LuaGuideAction.ExecuteInternal(raw)
        end
    end
end
function LuaGuideAction.ExecuteOnOldUserSkipAction(phaseId)
    local designData = Guide_Logic.GetData(phaseId)
    if designData then
        local raw = designData.OnOldUserSkip
        if raw and raw~="" then
            LuaGuideAction.ExecuteInternal(raw)
        end
    end
end
function LuaGuideAction.ExecuteOnSkipAction(phaseId)
    local designData = Guide_Logic.GetData(phaseId)
    if designData then
        local raw = designData.OnSkip
        if raw and raw~="" then
            LuaGuideAction.ExecuteInternal(raw)
        end
    end
end
function LuaGuideAction.ExecuteInternal(raw)
    for EventStr in string.gmatch(raw, "[^;]+") do
        local EventName, EventParam = EventStr, {}
        if string.find(EventStr,"%(") then
            local ParamsStr
            EventName, ParamsStr = string.match(EventStr, "([^%(]*)%(([^%)]*)%)")
            EventParam = { loadstring("return "..ParamsStr)() }
        end
        if EventName then
            local FuncName = LuaGuideAction[EventName]
            if FuncName then
                FuncName(unpack(EventParam))
            end
        end
    end
end

function LuaGuideAction.OpenEntry(entryType)
    COpenEntryMgr.PlayOpenEntry(entryType)
end

--锁视角
function LuaGuideAction.ForbiddDragOrZoom(active)
    CameraFollow.Inst.m_ForbiddDragOrZoom=active
end

--开启声音
function LuaGuideAction.EnableSound()
    if not PlayerSettings.SoundEnabled then
        PlayerSettings.SoundEnabled = true
    end
    if not PlayerSettings.AmbientSoundEnabled then
        PlayerSettings.AmbientSoundEnabled = true
    end
    if not PlayerSettings.MusicEnabled then
        PlayerSettings.MusicEnabled = true
    end
end

--包裹引导跳过的话，需要把药品打开
function LuaGuideAction.OpenDrug()
    COpenEntryMgr.Inst:SetOpenData(OpenType.OpenDrug)
    COpenEntryMgr.Inst:RefreshDrugButton()
end