require("3rdParty/ScriptEvent")
require("common/common_include")

local EquipmentTemplate_Equip=import "L10.Game.EquipmentTemplate_Equip"
local EquipmentTemplate_SubType=import "L10.Game.EquipmentTemplate_SubType"
local CItemInfoMgr=import "L10.UI.CItemInfoMgr"
local AlignType=import "L10.UI.CItemInfoMgr+AlignType"
local ShowType = import "L10.UI.CItemInfoMgr+ShowType"
local LinkItemInfo = import "L10.UI.CItemInfoMgr+LinkItemInfo"
local MessageMgr = import "L10.Game.MessageMgr"
local QnSelectableButton = import "L10.UI.QnSelectableButton"
local Vector2 = import "UnityEngine.Vector2"
local CUITexture=import "L10.UI.CUITexture"
local PopupMenuItemData = import "L10.UI.PopupMenuItemData"
local CPopupMenuInfoMgr = import "L10.UI.CPopupMenuInfoMgr"
local EnumPopupMenuItemStyle = import "L10.UI.EnumPopupMenuItemStyle"
local CPlayerInfoMgr = import "CPlayerInfoMgr"
local EnumPlayerInfoContext = import "L10.Game.EnumPlayerInfoContext"
local TweenPosition = import "TweenPosition"
local UIInput = import "UIInput"
local CButton = import "L10.UI.CButton"
local CChatHelper = import "L10.UI.CChatHelper"
local EChatPanel = import "L10.Game.EChatPanel"
local MessageWndManager = import "L10.UI.MessageWndManager"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local UIWidget = import "UIWidget"
local CUIGameObjectPool = import "L10.UI.CUIGameObjectPool"
local CChatLinkMgr = import "CChatLinkMgr"
local Color = import "UnityEngine.Color"
local Vector3 = import "UnityEngine.Vector3"
local Ease = import "DG.Tweening.Ease"
local Extensions = import "Extensions"
local CTeamMgr = import "L10.Game.CTeamMgr"
local Time = import "UnityEngine.Time"
local CCommonLuaScript = import "L10.UI.CCommonLuaScript"
local CCommonPlayerListMgr = import "L10.Game.CCommonPlayerListMgr"
local PlayerPrefs = import "UnityEngine.PlayerPrefs"

LuaBQPEquipInfoWnd=class()

RegistClassMember(LuaBQPEquipInfoWnd,"CloseButton")
RegistClassMember(LuaBQPEquipInfoWnd,"EquipInfoWnd")
RegistClassMember(LuaBQPEquipInfoWnd,"ItemNameLabel")
RegistClassMember(LuaBQPEquipInfoWnd,"AvailableLevelLabel")
RegistClassMember(LuaBQPEquipInfoWnd,"CategoryLabel")
RegistClassMember(LuaBQPEquipInfoWnd,"RankLabel")
RegistClassMember(LuaBQPEquipInfoWnd,"RankHintLabel")

RegistClassMember(LuaBQPEquipInfoWnd,"EquipIconTexture_WA")
RegistClassMember(LuaBQPEquipInfoWnd,"EquipIconTexture_Others")
RegistClassMember(LuaBQPEquipInfoWnd,"Tag1")

RegistClassMember(LuaBQPEquipInfoWnd,"EquipSocre")
RegistClassMember(LuaBQPEquipInfoWnd,"Fx")

RegistClassMember(LuaBQPEquipInfoWnd,"PlayerTexture")
RegistClassMember(LuaBQPEquipInfoWnd,"PlayerHeadGo")
RegistClassMember(LuaBQPEquipInfoWnd,"PlayerGrade")
RegistClassMember(LuaBQPEquipInfoWnd,"PlayerName")
RegistClassMember(LuaBQPEquipInfoWnd,"EquipDesc")
RegistClassMember(LuaBQPEquipInfoWnd,"ShareBtn")
RegistClassMember(LuaBQPEquipInfoWnd,"TryWearBtn")
RegistClassMember(LuaBQPEquipInfoWnd,"CommentBtn")
RegistClassMember(LuaBQPEquipInfoWnd,"DanMuSetting")
RegistClassMember(LuaBQPEquipInfoWnd,"RemainTimesLabel")
RegistClassMember(LuaBQPEquipInfoWnd,"DonateBtn")
RegistClassMember(LuaBQPEquipInfoWnd,"LikeBtn")
RegistClassMember(LuaBQPEquipInfoWnd,"TweenNode")

RegistClassMember(LuaBQPEquipInfoWnd,"BackButton")
RegistClassMember(LuaBQPEquipInfoWnd,"SubmitCommentButton")
RegistClassMember(LuaBQPEquipInfoWnd,"CommentInput")

RegistClassMember(LuaBQPEquipInfoWnd,"BulletScreen")
RegistClassMember(LuaBQPEquipInfoWnd,"DanMuScreen")
RegistClassMember(LuaBQPEquipInfoWnd,"BulletPool")


RegistClassMember(LuaBQPEquipInfoWnd,"DetailEquipInfo")
RegistClassMember(LuaBQPEquipInfoWnd,"EvaluateInterval")
RegistClassMember(LuaBQPEquipInfoWnd,"LastEvaluateTime")
RegistClassMember(LuaBQPEquipInfoWnd,"m_EnableDanmu")


function LuaBQPEquipInfoWnd:Init()
	self.m_EnableDanmu = self:GetEnableDanmu()==1
	self.EvaluateInterval = 2
	self.LastEvaluateTime = 0

	LuaBingQiPuMgr.QueryBQPPraiseCount()
	Gac2Gas.QueryBQPStage()
	Gac2Gas.ReqeustCheckBQPComment()

	self.EquipInfoWnd = self.transform:Find("DetailInfo/EquipInfoWnd").gameObject

	self.ItemNameLabel = self.transform:Find("Content/EquipInfo/ItemNameLabel"):GetComponent(typeof(UILabel))
	self.AvailableLevelLabel = self.transform:Find("Content/EquipInfo/AvailableLevelLabel"):GetComponent(typeof(UILabel))
	self.CategoryLabel = self.transform:Find("Content/EquipInfo/CategoryLabel"):GetComponent(typeof(UILabel))
	self.RankLabel = self.transform:Find("Content/Rank/RankLabel"):GetComponent(typeof(UILabel))
	self.RankHintLabel = self.transform:Find("Content/Rank/HintLabel"):GetComponent(typeof(UILabel))

	self.EquipIconTexture_WA = self.transform:Find("Content/BigIcon/IconTexture_WA"):GetComponent(typeof(CUITexture))
	self.EquipIconTexture_Others = self.transform:Find("Content/BigIcon/IconTexture_Others"):GetComponent(typeof(CUITexture))
	self.Tag1 = self.transform:Find("Content/Tag1/Label"):GetComponent(typeof(UILabel))
	self.EquipSocre = self.transform:Find("Content/Sprite/Label"):GetComponent(typeof(UILabel))
	self.Fx = self.transform:Find("Content/Sprite/Fx"):GetComponent(typeof(CUIFx))

	self.PlayerTexture = self.transform:Find("Content/OwnerInfo/Border/Portrait"):GetComponent(typeof(CUITexture))
	self.PlayerGrade = self.transform:Find("Content/OwnerInfo/Border/Grade"):GetComponent(typeof(UILabel))
	self.PlayerName = self.transform:Find("Content/OwnerInfo/OwnerName"):GetComponent(typeof(UILabel))
	self.EquipDesc = self.transform:Find("Content/OwnerInfo/EquipDesc"):GetComponent(typeof(UILabel))
	self.ShareBtn = self.transform:Find("Content/Share/ShareBtn").gameObject
	self.TryWearBtn = self.transform:Find("Content/Share/TryWearBtn").gameObject

	self.CommentBtn = self.transform:Find("Content/Bottom/Panel/Nodes/NormalNode/CommentBtn").gameObject
	self.DanMuSetting = self.transform:Find("Content/Bottom/Panel/Nodes/NormalNode/DanMuSetting"):GetComponent(typeof(QnSelectableButton))
	self.RemainTimesLabel = self.transform:Find("Content/Bottom/Panel/Nodes/NormalNode/RemainTimesLabel"):GetComponent(typeof(UILabel))
	
	self.DonateBtn = self.transform:Find("Content/Bottom/Panel/Nodes/NormalNode/DonateBtn"):GetComponent(typeof(CButton))
	self.LikeBtn = self.transform:Find("Content/Bottom/Panel/Nodes/NormalNode/LikeBtn"):GetComponent(typeof(CButton))

	self.TweenNode = self.transform:Find("Content/Bottom/Panel/Nodes"):GetComponent(typeof(TweenPosition))
	self.TweenNode.enabled = false
	self.BackButton = self.transform:Find("Content/Bottom/Panel/Nodes/SendNode/BackButton").gameObject
	self.SubmitCommentButton = self.transform:Find("Content/Bottom/Panel/Nodes/SendNode/SubmitButton").gameObject
	self.CommentInput = self.transform:Find("Content/Bottom/Panel/Nodes/SendNode/StatusText"):GetComponent(typeof(UIInput))

	self.BulletScreen = self.transform:Find("Content/BulletScreen/ScreenBound"):GetComponent(typeof(UIWidget))
	self.DanMuScreen = self.transform:Find("Content/BulletScreen").gameObject
	self.BulletPool = self.transform:Find("Content/BulletScreen/Pool"):GetComponent(typeof(CUIGameObjectPool))

	if not LuaBingQiPuMgr.m_DetailEquipInfo or #LuaBingQiPuMgr.m_DetailEquipInfo < 1 then return end

	self.DetailEquipInfo = LuaBingQiPuMgr.m_DetailEquipInfo[1]
	local equip = self.DetailEquipInfo.equip
	local template = EquipmentTemplate_Equip.GetData(equip.TemplateId)

	if not equip or not template then return end

	local previousAdjusted = equip.Equip.IsFeiShengAdjusted
	equip.Equip.IsFeiShengAdjusted = 0

	CItemInfoMgr.showType = ShowType.Link
	local linkItemInfo = LinkItemInfo(equip, false, nil)
	linkItemInfo.alignType = AlignType.Default
	linkItemInfo.targetSize = Vector2.zero
	linkItemInfo.targetCenterPos = Vector3.zero
	linkItemInfo.dragAmount = 0
	CItemInfoMgr.linkItemInfo = linkItemInfo
	CItemInfoMgr.NeedAdjustHeight = false
	CLuaEquipTip.s_EnableInfoBtn = false
	CLuaEquipTip.b_NeedShowFeiShengAdjusted = false
	self.EquipInfoWnd.gameObject:SetActive(true)
	local script = self.EquipInfoWnd.transform:GetComponent(typeof(CCommonLuaScript))
	script:Init({})
	--self.EquipInfoWnd:Init()

	-- 名称
	self.ItemNameLabel.text = equip.Equip.DisplayName
	self.ItemNameLabel.color = equip.Equip.DisplayColor
	

	-- 等级
    local level = 0
    if CClientMainPlayer.Inst ~= nil then
        level = CClientMainPlayer.Inst.FinalMaxLevelForEquip
    end
	local levelstr = ""
    if template.Grade == 0 then
        levelstr = ""
    elseif template.Grade > level then
		levelstr = System.String.Format(LocalString.GetString("[c][FF0000]{0}级[-][/c]  "), template.Grade)
     else
        levelstr = System.String.Format(LocalString.GetString("[c][FFFFFF]{0}级[-][/c]  "), template.Grade)
    end
	
    -- 类型
	local typestr = ""
    local subTypeTemplate = EquipmentTemplate_SubType.GetData(template.Type * 100 + template.SubType)
    local blankSpace = CommonDefs.IS_VN_CLIENT and " " or ""

    if subTypeTemplate ~= nil then
    	if template.Type == EnumBodyPosition_lua.Weapon then
            local default
            if template.BothHand == 1 then
                default = LocalString.GetString("双手")
            else
                default = LocalString.GetString("单手")
            end
            typestr = System.String.Format("{0}{1}({2})", subTypeTemplate.Name, blankSpace, default)
        else
            typestr = subTypeTemplate.Name
        end
    end
	self.AvailableLevelLabel.text = levelstr..typestr

    equip.Equip.IsFeiShengAdjusted = previousAdjusted

    -- 排名
    self.RankLabel.text = tostring(self.DetailEquipInfo.rankPos)

	local etype = self.DetailEquipInfo.Precious and 0 or template.Type
	local ename = LuaBingQiPuMgr.GetEquipTypeName(etype)
	self.RankHintLabel.text = SafeStringFormat3(LocalString.GetString("%s排名"),ename)

    -- 装备大图标
	local icon = template.BigIcon
	if equip.Equip.IsShenBing then
		local iconData = ShenBing_EquipIcon.GetData(template.ShenBingType)
        if iconData then
            icon = iconData.BigIcon
		end
	end
    if template.Type == EnumBodyPosition_lua.Weapon or template.Type == EnumBodyPosition_lua.Armour or template.Type == EnumBodyPosition_lua.Shield then
    	self.EquipIconTexture_WA:LoadMaterial(icon)
    	self.EquipIconTexture_WA.gameObject:SetActive(true)
    	self.EquipIconTexture_Others.gameObject:SetActive(false)
    else
    	self.EquipIconTexture_Others:LoadMaterial(icon)
    	self.EquipIconTexture_Others.gameObject:SetActive(true)
    	self.EquipIconTexture_WA.gameObject:SetActive(false)
    end

    -- 标签
    local tag1Id = self.DetailEquipInfo.tag1
	if not self.DetailEquipInfo.Precious then
    	local tag1Info = BingQiPu_EquipmentTag.GetData(tag1Id)
    	if tag1Info then
    		self.Tag1.text = tag1Info.VoteNum
		else
			self.Tag1.text = ""
    	end
	else
		local tag1Info = BingQiPu_TreasureTag.GetData(tag1Id)
		if tag1Info then
    		self.Tag1.text = tag1Info.TreasureVoteNum
		else
			self.Tag1.text = ""
    	end
    end

	-- 人气
	self.EquipSocre.text = SafeStringFormat3(LocalString.GetString("人气  %d"), self.DetailEquipInfo.Score)

    -- 拥有者信息
    self.PlayerTexture:LoadNPCPortrait(CUICommonDef.GetPortraitName(self.DetailEquipInfo.ownerClass,self.DetailEquipInfo.ownerGender, -1))
    self.PlayerName.text = SafeStringFormat3(LocalString.GetString("[ACF9FF]主人[-]  %s"), self.DetailEquipInfo.ownerName)
    self.PlayerGrade.text = self.DetailEquipInfo.ownerLevel
    self.EquipDesc.text = SafeStringFormat3(LocalString.GetString("“%s”"), self.DetailEquipInfo.discription)
    local onHeadGo = function (go)
    	CPlayerInfoMgr.ShowPlayerPopupMenu(self.DetailEquipInfo.ownerId, EnumPlayerInfoContext.Undefined, EChatPanel.Undefined, nil, nil, CommonDefs.GetDefaultValue(typeof(Vector3)), CPlayerInfoMgr.AlignType.Default)
    end
    CommonDefs.AddOnClickListener(self.PlayerTexture.gameObject,DelegateFactory.Action_GameObject(onHeadGo),false)

    -- 分享
    local onShare = function (go)
		self:OnShareClick(go)
	end
	CommonDefs.AddOnClickListener(self.ShareBtn,DelegateFactory.Action_GameObject(onShare),false)

	-- 试穿
	local onTry = function (go)
		self:OnTryWearClick(go)
	end
	CommonDefs.AddOnClickListener(self.TryWearBtn,DelegateFactory.Action_GameObject(onTry),false)

	-- 弹幕
	self.DanMuSetting.OnButtonSelected = DelegateFactory.Action_bool(function(tf)
		self:OnDanMuSelect(tf)
        end)
	self.DanMuSetting:SetSelected(self.m_EnableDanmu,false)

	-- 助力
	local onDonate = function (go)
		self:OnDonateClick(go)
	end

	CommonDefs.AddOnClickListener(self.DonateBtn.gameObject,DelegateFactory.Action_GameObject(onDonate),false)

	-- 点赞
	local onLike = function (go)
		self:OnLikeClick(go)
	end
	CommonDefs.AddOnClickListener(self.LikeBtn.gameObject,DelegateFactory.Action_GameObject(onLike),false)


	-- 评论
	local onComment = function (go)
		self:OnCommentClick(go)
	end
	CommonDefs.AddOnClickListener(self.CommentBtn,DelegateFactory.Action_GameObject(onComment),false)

	-- 返回
	local onBack = function (go)
		self.TweenNode:PlayReverse()
	end
	CommonDefs.AddOnClickListener(self.BackButton,DelegateFactory.Action_GameObject(onBack),false)

	-- 提交评论
	local onSubmit = function (go)
		self:OnSubmitClick(go)
	end
	CommonDefs.AddOnClickListener(self.SubmitCommentButton,DelegateFactory.Action_GameObject(onSubmit),false)

	self:UpdateButtonInfos()
end

function LuaBQPEquipInfoWnd:CanOperate()
	return LuaBingQiPuMgr.m_StageStatus == EnumBingQiPuStage.eSubmit or LuaBingQiPuMgr.m_StageStatus == EnumBingQiPuStage.eVote
end

function LuaBQPEquipInfoWnd:UpdateButtonInfos()

	if not self:CanOperate() then
		self.LikeBtn.gameObject:SetActive(false)
		self.DonateBtn.gameObject:SetActive(false)
		return
	end
	
	self.LikeBtn.gameObject:SetActive(true)
	self.DonateBtn.gameObject:SetActive(true)

	-- 剩余次数
	self.RemainTimesLabel.text = LuaBingQiPuMgr.GetLikeStr()

	if self.DetailEquipInfo.Like > 0 then
		self.LikeBtn.Enabled = false
		self.LikeBtn.Text = LocalString.GetString("已赞")
	else
		self.LikeBtn.Enabled = LuaBingQiPuMgr.m_LikesLeft > 0
		self.LikeBtn.Text = LocalString.GetString("点赞")
	end
end

function LuaBQPEquipInfoWnd:OnEnable()
	g_ScriptEvent:AddListener("UpdateBQPEquipInfo", self, "UpdateBQPEquipInfo")
	g_ScriptEvent:AddListener("UpdateBQPStatus", self, "UpdateButtonInfos")
	g_ScriptEvent:AddListener("BQPPraiseDone",self, "BQPPraiseDone")
	g_ScriptEvent:AddListener("BQPCommentListGot",self, "BQPCommentListGot")
	g_ScriptEvent:AddListener("BQPCommentCreated",self, "BQPCommentCreated")
	
end

function LuaBQPEquipInfoWnd:OnDisable()
	g_ScriptEvent:RemoveListener("UpdateBQPEquipInfo", self, "UpdateBQPEquipInfo")
	g_ScriptEvent:RemoveListener("UpdateBQPStatus", self, "UpdateButtonInfos")
	g_ScriptEvent:RemoveListener("BQPPraiseDone",self, "BQPPraiseDone")
	g_ScriptEvent:RemoveListener("BQPCommentListGot",self, "BQPCommentListGot")
	g_ScriptEvent:RemoveListener("BQPCommentCreated",self, "BQPCommentCreated")
	CLuaEquipTip.b_NeedShowFeiShengAdjusted = true
	CLuaEquipTip.s_EnableInfoBtn = true
end

function LuaBQPEquipInfoWnd:BQPPraiseDone(equipId)
	if self.DetailEquipInfo.equipId ~= equipId then return end
	self.DetailEquipInfo.Like = 1
	self.Fx:LoadFx("fx/ui/prefab/UI_bingqipu_dianzan.prefab")
	self:UpdateButtonInfos()
end

function LuaBQPEquipInfoWnd:UpdateBQPEquipInfo()
	self:UpdateButtonInfos()
end

-- 分享按钮
function LuaBQPEquipInfoWnd:OnShareClick(go)
	local menuList = CreateFromClass(MakeGenericClass(List, PopupMenuItemData))
	
	CommonDefs.ListAdd(menuList, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("至帮会频道"), DelegateFactory.Action_int(function (idx) 
                self:ShareTo(idx,0)
            end), false, nil, EnumPopupMenuItemStyle.Default))
	CommonDefs.ListAdd(menuList, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("至队伍频道"), DelegateFactory.Action_int(function (idx) 
                self:ShareTo(idx,0)
            end), false, nil, EnumPopupMenuItemStyle.Default))
	CommonDefs.ListAdd(menuList, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("至好友频道"), DelegateFactory.Action_int(function (idx) 
        		self:ShareToFriend(idx)
    		end), false, nil, EnumPopupMenuItemStyle.Default))
	CommonDefs.ListAdd(menuList, typeof(PopupMenuItemData), PopupMenuItemData(LocalString.GetString("至宗派频道"), DelegateFactory.Action_int(function (idx) 
        		self:ShareTo(idx, 0)
    		end), false, nil, EnumPopupMenuItemStyle.Default))
	CPopupMenuInfoMgr.ShowPopupMenu(CommonDefs.ListToArray(menuList), go.transform, CPopupMenuInfoMgr.AlignType.Bottom, 1, nil, 350, true, 330)
end

function LuaBQPEquipInfoWnd:ShareToFriend(idx)
	CCommonPlayerListMgr.Inst:ShowFriendsToShareJingLingAnswer(DelegateFactory.Action_ulong(function (playerId)
		self:ShareTo(idx, playerId)
	end))
end

-- 分享至不同的频道
function LuaBQPEquipInfoWnd:ShareTo(key,pid)
	local content = g_MessageMgr:FormatMessage("BQP_SHARE_TO_CHAT", self.DetailEquipInfo.equip.Name, self.DetailEquipInfo.equipId)
	if key == 2 then
		CChatHelper.SendMsgWithFilterOption(EChatPanel.Friend,content, pid ,true)
	elseif key == 0 then
		if CClientMainPlayer.Inst.BasicProp.GuildId > 0 then
			CChatHelper.SendMsgWithFilterOption(EChatPanel.Guild, content, 0 ,true)
		else
			MessageMgr.Inst:ShowMessage("NOT_IN_ANY_GUILD", {})
			return
		end
	elseif key == 1 then
		if CClientMainPlayer.Inst and not CTeamMgr.Inst:IsPlayerInTeam(CClientMainPlayer.Inst.Id) then
			MessageMgr.Inst:ShowMessage("NOT_IN_ANY_TEAM", {})
			return
		elseif CClientMainPlayer.Inst and CTeamMgr.Inst:IsPlayerInTeam(CClientMainPlayer.Inst.Id) then
			CChatHelper.SendMsgWithFilterOption(EChatPanel.Team, content, 0 ,true)
		end
	elseif key == 3 then
		if CClientMainPlayer.Inst and CClientMainPlayer.Inst.BasicProp.SectId > 0 then
			CChatHelper.SendMsgWithFilterOption(EChatPanel.Sect, content, 0, true)
		else
			MessageMgr.Inst:ShowMessage("NOT_IN_ANY_SECT",{})
			return
		end
	end
	MessageMgr.Inst:ShowMessage("BQP_SHARE_TO_CHANNEL_SUCCESS", {})
end

function LuaBQPEquipInfoWnd:OnTryWearClick(go)

	-- 添加二次确认
	local msg = LocalString.GetString("即将进入试穿副本，进入后你将穿上这件装备与之前的自己战斗，是否进入副本？")
    MessageWndManager.ShowOKCancelMessage(msg, DelegateFactory.Action(
    	function()
    		Gac2Gas.TryBingQiPuEquip(self.DetailEquipInfo.equipId)
    	end
    	), nil, LocalString.GetString("确定"), LocalString.GetString("取消"), false)
	
end

--点赞
function LuaBQPEquipInfoWnd:OnLikeClick(go)
	if Time.realtimeSinceStartup - self.LastEvaluateTime < self.EvaluateInterval then
		return
	end
	self.LastEvaluateTime = Time.realtimeSinceStartup
	LuaBingQiPuMgr.QueryBQPPraise(self.DetailEquipInfo.equipId,self.DetailEquipInfo.equip.TemplateId,false)
end

-- 助力
function LuaBQPEquipInfoWnd:OnDonateClick(go)
	LuaBingQiPuMgr.ShowDonateWnd(self.DetailEquipInfo.equipId)
end

-- 评论
function LuaBQPEquipInfoWnd:OnCommentClick(go)
	if LuaBingQiPuMgr.m_CanComment then
		self.TweenNode:PlayForward()
	end
end

function LuaBQPEquipInfoWnd:BQPCommentListGot(equipid,comments)
	if equipid ~= self.DetailEquipInfo.equipId then return end
	if self.DanMuScreen then
		self.DanMuScreen:SetActive(true)
		for i = 1, #comments do
			self:OnAddBullet(comments[i])
		end
	end
end

function LuaBQPEquipInfoWnd:BQPCommentCreated(equipid)
	if equipid ~= self.DetailEquipInfo.equipId then return end
	-- 去掉input内的所有数据
	if self.CommentInput then
		self.CommentInput.value = ""
	end
	if self.BulletScreen then
		-- 重新拉取数据
		Extensions.RemoveAllChildren(self.BulletScreen.transform) 
		self:GetCommentList()
	end
end

-- 提交评论
function LuaBQPEquipInfoWnd:OnSubmitClick(go)
	if Time.realtimeSinceStartup - self.LastEvaluateTime < self.EvaluateInterval then
		return
	end
	self.LastEvaluateTime = Time.realtimeSinceStartup

	local content = self.CommentInput.value
	if System.String.IsNullOrEmpty(content) then 
		return
	end
	-- 过滤
	local ret = CWordFilterMgr.Inst:DoFilterOnSend(content, nil, nil, true)
	local msg = ret.msg
	if not msg then return end

	if not CClientMainPlayer.Inst and not CClientMainPlayer.Inst.Name then return end
	msg = SafeStringFormat3("%s: %s", CClientMainPlayer.Inst.Name, msg)

	LuaBingQiPuMgr.CreateComment(self.DetailEquipInfo.equipId,msg)
end

function LuaBQPEquipInfoWnd:GetCommentList()
	LuaBingQiPuMgr.GetCommentList(self.DetailEquipInfo.equipId)
end

-- 添加弹幕
function LuaBQPEquipInfoWnd:OnAddBullet(content)
	if not content or content == "" then return end
	-- local label = NGUITools.AddChild(self.BulletScreen.gameObject, self.BulletPool:GetFromPool(0))
	local label = self.BulletPool:GetFromPool(0)
    label.transform.parent = self.BulletScreen.transform
	label.transform.localScale = Vector3.one
	
	label:SetActive(true)
	local bullet = label:GetComponent(typeof(UILabel))
	bullet.text = CChatLinkMgr.TranslateToNGUIText(content)
	bullet.color = Color(UnityEngine_Random(0.5, 1), UnityEngine_Random(0.5, 1), UnityEngine_Random(0.5, 1))
	bullet.fontSize = UnityEngine_Random(38, 52)
	bullet.transform.localPosition = Vector3(math.floor(self.BulletScreen.width / 2), UnityEngine_Random(math.floor(- self.BulletScreen.height / 2), math.floor(self.BulletScreen.height / 2)), 0)
    CommonDefs.OnComplete_Tweener(CommonDefs.SetEase_Tweener(LuaTweenUtils.DOLocalMoveX(bullet.transform, math.floor(- self.BulletScreen.width / 2) - bullet.width, UnityEngine_Random(6, 12), false), Ease.Linear), DelegateFactory.TweenCallback(function () 
        self.BulletPool:Recycle(bullet.gameObject)
    end))
end

-- 打开/关闭弹幕
function LuaBQPEquipInfoWnd:OnDanMuSelect(tf)
	self.m_EnableDanmu = tf
	if tf then
		self:SetEnableDanmu(1)
		Extensions.RemoveAllChildren(self.BulletScreen.transform) 
		self:GetCommentList()
	else
		self:SetEnableDanmu(0)
		Extensions.RemoveAllChildren(self.BulletScreen.transform) 
		self.DanMuScreen:SetActive(false)
	end
end

function LuaBQPEquipInfoWnd:GetEnableDanmu()
	return PlayerPrefs.GetInt("BingQiPuEnableDanmu",1)
end

function LuaBQPEquipInfoWnd:SetEnableDanmu(tf)
	PlayerPrefs.SetInt("BingQiPuEnableDanmu",tf)
end
