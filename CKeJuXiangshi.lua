-- Auto Generated!!
local CItemMgr = import "L10.Game.CItemMgr"
local CKeJuMgr = import "L10.Game.CKeJuMgr"
local CKeJuQuestionTemplate = import "L10.UI.CKeJuQuestionTemplate"
local CKeJuXiangshi = import "L10.UI.CKeJuXiangshi"
local CommonDefs = import "L10.Game.CommonDefs"
local Extensions = import "Extensions"
local KeJu_Setting = import "L10.Game.KeJu_Setting"
local LocalString = import "LocalString"
local NGUITools = import "NGUITools"
local Vector3 = import "UnityEngine.Vector3"
local CSocialWndMgr = import "L10.UI.CSocialWndMgr"
CKeJuXiangshi.m_Init_CS2LuaHook = function (this, questionTemplate) 

    this.descLabel.text = g_MessageMgr:FormatMessage("KEJU_XIANGSHI_TIP")
    this.bottomLabel.text = g_MessageMgr:FormatMessage("KEJU_XIANGSHI_GIFT_TIP")
    local awardItem = CItemMgr.Inst:GetItemTemplate(System.UInt32.Parse(KeJu_Setting.GetData().XiangShiItemId))
    this.awardTexture:LoadMaterial(awardItem.Icon)
    questionTemplate:SetActive(false)
    Extensions.RemoveAllChildren(this.questionAnchor)
    local question = NGUITools.AddChild(this.questionAnchor.gameObject, questionTemplate)
    question:SetActive(true)
    question.transform.localPosition = Vector3.zero
    this.questionBoard = CommonDefs.GetComponent_GameObject_Type(question, typeof(CKeJuQuestionTemplate))
    this.questionBoard:InitWithPlayOption(CKeJuMgr.Inst.bVoice, true)
    this.questionBoard.CorrectCountUpdate = MakeDelegateFromCSFunction(this.OnCorrectAnswerUpdate, Action0, this)

    this.leftTimeLabel.text = ""
    this:InitLeftTime()

    this.correctCountLabel.text = System.String.Format(LocalString.GetString("当前累计答对{0}题"), CKeJuMgr.Inst.xiangshiCorrectCount)
    
    local shareBtn = this.transform:Find("ShareButton").gameObject
    local chatBtn = this.transform:Find("ChatButton").gameObject
    UIEventListener.Get(shareBtn).onClick = DelegateFactory.VoidDelegate(function (go)
	    LuaKeJuMgr:ShowXiangshiPopupMenu(shareBtn)
	end)
    UIEventListener.Get(chatBtn).onClick = DelegateFactory.VoidDelegate(function (go)
	    CSocialWndMgr.ShowChatWnd();
	end)
end
CKeJuXiangshi.m_InitLeftTime_CS2LuaHook = function (this) 

    local leftTime = CKeJuMgr.Inst.xiangshiLeftTime
    local hour = math.floor(leftTime / 3600)
    local minute = math.floor((leftTime % 3600) / 60)
    local second = (leftTime % 3600) % 60
    this.leftTimeLabel.text = System.String.Format("{0}:{1}:{2}", tostring(hour), math.floor(minute / 10) > 0 and tostring(minute) or "0" .. tostring(minute), math.floor(second / 10) > 0 and tostring(second) or "0" .. tostring(second))
    if CKeJuMgr.Inst.xiangshiLeftTime > 0 then
        local default = CKeJuMgr.Inst
        default.xiangshiLeftTime = default.xiangshiLeftTime - 1
    else
        CKeJuMgr.Inst.Question = g_MessageMgr:FormatMessage("KEJU_XIANGSHI_OVER")
        CommonDefs.ListClear(CKeJuMgr.Inst.AnswerList)
        CKeJuMgr.Inst.bVoice = false
        this.questionBoard:Init(CKeJuMgr.Inst.bVoice)
        if this.cancel ~= nil then
            invoke(this.cancel)
        end
    end
end
