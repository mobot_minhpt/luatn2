-- Auto Generated!!
local CAutoBaptizeConditionItem = import "L10.UI.CAutoBaptizeConditionItem"
local CEquipmentBaptizeMgr = import "L10.Game.CEquipmentBaptizeMgr"
local CEquipmentProcessMgr = import "L10.Game.CEquipmentProcessMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CTalismanWordBaptizeMgr = import "L10.UI.CTalismanWordBaptizeMgr"
local DelegateFactory = import "DelegateFactory"
local EnumEventType = import "EnumEventType"
local EventDelegate = import "EventDelegate"
local EventManager = import "EventManager"
local LocalString = import "LocalString"
local UInt32 = import "System.UInt32"
local Word_Word = import "L10.Game.Word_Word"
CAutoBaptizeConditionItem.m_InitCount_CS2LuaHook = function (this, count) 
    this.count = count
    this.countOrWord = true
    this.descLabel.text = SafeStringFormat3(LocalString.GetString("%d条或以上"),count)

    CommonDefs.ListAdd(this.toggle.onChange, typeof(EventDelegate), CreateFromClass(EventDelegate, DelegateFactory.Callback(function () 
        if this.toggle.value then
            CEquipmentBaptizeMgr.Inst:AddCountOption(count)
        else
            CEquipmentBaptizeMgr.Inst:RemoveCountOption(count)
            if CEquipmentBaptizeMgr.Inst:GetAutoBaptizeParam4() == "" and CEquipmentBaptizeMgr.Inst:GetAutoBaptizeParam5() == "" and not CEquipmentBaptizeMgr.Inst:ContainSpecialWordOption(CEquipmentBaptizeMgr.Word_QingHong_8) and not CEquipmentBaptizeMgr.Inst.checkScore then
                this.toggle.value = true
                CEquipmentBaptizeMgr.Inst:AddCountOption(count)
                g_MessageMgr:ShowMessage("Auto_XiLian_NO_Choice")
            end
        end

        EventManager.Broadcast(EnumEventType.ChangeAutoBaptizeCondition)
    end)))

    if CEquipmentBaptizeMgr.Inst:ContainCountOption(count) then
        this.toggle.value = true
    else
        this.toggle.value = false
    end
end
CAutoBaptizeConditionItem.m_InitWord_CS2LuaHook = function (this, word) 
    this.word = word
    this.countOrWord = false
    this.descLabel.text = CEquipmentProcessMgr.Inst:GetBaseWordDescription(word)
    CommonDefs.ListAdd(this.toggle.onChange, typeof(EventDelegate), CreateFromClass(EventDelegate, DelegateFactory.Callback(function () 
        if this.toggle.value then
            CEquipmentBaptizeMgr.Inst:AddWordOption(word)
        else
            CEquipmentBaptizeMgr.Inst:RemoveWordOption(word)
            if CEquipmentBaptizeMgr.Inst:GetAutoBaptizeParam4() == "" and CEquipmentBaptizeMgr.Inst:GetAutoBaptizeParam5() == "" and not CEquipmentBaptizeMgr.Inst:ContainSpecialWordOption(CEquipmentBaptizeMgr.Word_QingHong_8) and not CEquipmentBaptizeMgr.Inst.checkScore then
                this.toggle.value = true
                CEquipmentBaptizeMgr.Inst:AddWordOption(word)
                g_MessageMgr:ShowMessage("Auto_XiLian_NO_Choice")
            end
        end

        EventManager.Broadcast(EnumEventType.ChangeAutoBaptizeCondition)
    end)))

    if CEquipmentBaptizeMgr.Inst:ContainWordOption(word) then
        this.toggle.value = true
    else
        this.toggle.value = false
    end
end
CAutoBaptizeConditionItem.m_InitSpecialWord_CS2LuaHook = function (this, word) 
    this.word = word
    this.countOrWord = false
    this.descLabel.text = Word_Word.GetData(word).Description
    CommonDefs.ListAdd(this.toggle.onChange, typeof(EventDelegate), CreateFromClass(EventDelegate, DelegateFactory.Callback(function () 
        if this.toggle.value then
            CEquipmentBaptizeMgr.Inst:AddSpecialWordOption(word)
        else
            CEquipmentBaptizeMgr.Inst:RemoveSpecialWordOption(word)
            if CEquipmentBaptizeMgr.Inst:GetAutoBaptizeParam4() == "" and CEquipmentBaptizeMgr.Inst:GetAutoBaptizeParam5() == "" and not CEquipmentBaptizeMgr.Inst:ContainSpecialWordOption(CEquipmentBaptizeMgr.Word_QingHong_8) and not CEquipmentBaptizeMgr.Inst.checkScore then
                this.toggle.value = true
                CEquipmentBaptizeMgr.Inst:AddSpecialWordOption(word)
                g_MessageMgr:ShowMessage("Auto_XiLian_NO_Choice")
            end
        end

        EventManager.Broadcast(EnumEventType.ChangeAutoBaptizeCondition)
    end)))

    if CEquipmentBaptizeMgr.Inst:ContainSpecialWordOption(word) then
        this.toggle.value = true
    else
        this.toggle.value = false
    end
end
CAutoBaptizeConditionItem.m_InitTalismanWord_CS2LuaHook = function (this, word, amount) 
    this.word = word
    --this.wordAmount = amount;
    this.descLabel.text = System.String.Format(LocalString.GetString("{0}条或以上{1}"), amount, CEquipmentProcessMgr.Inst:GetBaseWordDescription(word))
    CommonDefs.ListAdd(this.toggle.onChange, typeof(EventDelegate), CreateFromClass(EventDelegate, DelegateFactory.Callback(function () 
        if this.toggle.value then
            CTalismanWordBaptizeMgr.AddWord(word, amount)
        else
            CTalismanWordBaptizeMgr.RemoveWord(word)
        end
    end)))
    if CommonDefs.DictContains(CTalismanWordBaptizeMgr.autoBaptizeWordList, typeof(UInt32), word) then
        this.toggle.value = true
    else
        this.toggle.value = false
    end
end
CAutoBaptizeConditionItem.m_InitCheckScore_CS2LuaHook = function (this) 
    this.checkScore = true
    this.countOrWord = false
    this.descLabel.text = LocalString.GetString("装备评分高于当前")
    CommonDefs.ListAdd(this.toggle.onChange, typeof(EventDelegate), CreateFromClass(EventDelegate, DelegateFactory.Callback(function () 
        if this.toggle.value then
            CEquipmentBaptizeMgr.Inst.checkScore = true
        else
            CEquipmentBaptizeMgr.Inst.checkScore = false
            if CEquipmentBaptizeMgr.Inst:GetAutoBaptizeParam4() == "" and CEquipmentBaptizeMgr.Inst:GetAutoBaptizeParam5() == "" and not CEquipmentBaptizeMgr.Inst:ContainSpecialWordOption(CEquipmentBaptizeMgr.Word_QingHong_8) and not CEquipmentBaptizeMgr.Inst.checkScore then
                this.toggle.value = true
                CEquipmentBaptizeMgr.Inst.checkScore = true
                g_MessageMgr:ShowMessage("Auto_XiLian_NO_Choice")
            end
        end

        EventManager.Broadcast(EnumEventType.ChangeAutoBaptizeCondition)
    end)))

    if CEquipmentBaptizeMgr.Inst.checkScore then
        this.toggle.value = true
    else
        this.toggle.value = false
    end
end
CAutoBaptizeConditionItem.m_InitFaBaoCheckScoreItem_CS2LuaHook = function (this) 
    this.checkScore = true
    this.countOrWord = false
    this.descLabel.text = LocalString.GetString("装备评分高于当前")
    CommonDefs.ListAdd(this.toggle.onChange, typeof(EventDelegate), CreateFromClass(EventDelegate, DelegateFactory.Callback(function () 
        if this.toggle.value then
            CTalismanWordBaptizeMgr.checkScore = true
        else
            CTalismanWordBaptizeMgr.checkScore = false
        end

        --EventManager.Broadcast(EnumEventType.ChangeAutoBaptizeCondition);
    end)))

    if CTalismanWordBaptizeMgr.checkScore then
        this.toggle.value = true
    else
        this.toggle.value = false
    end
end
