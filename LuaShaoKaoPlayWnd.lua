local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local QnButton = import "L10.UI.QnButton"
local UISlider = import "UISlider"
local CButton = import "L10.UI.CButton"

LuaShaoKaoPlayWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaShaoKaoPlayWnd, "Anchor", "Anchor", GameObject)
RegistChildComponent(LuaShaoKaoPlayWnd, "GetRewardView", "GetRewardView", GameObject)
RegistChildComponent(LuaShaoKaoPlayWnd, "TimeLeftLabel", "TimeLeftLabel", UILabel)
RegistChildComponent(LuaShaoKaoPlayWnd, "FireBtn", "FireBtn", QnButton)
RegistChildComponent(LuaShaoKaoPlayWnd, "Common", "Common", GameObject)
RegistChildComponent(LuaShaoKaoPlayWnd, "Success", "Success", GameObject)
RegistChildComponent(LuaShaoKaoPlayWnd, "Faild", "Faild", GameObject)
RegistChildComponent(LuaShaoKaoPlayWnd, "Progress", "Progress", UISlider)
RegistChildComponent(LuaShaoKaoPlayWnd, "Foreground", "Foreground", GameObject)
RegistChildComponent(LuaShaoKaoPlayWnd, "rightfx", "rightfx", GameObject)
RegistChildComponent(LuaShaoKaoPlayWnd, "wrongfx", "wrongfx", GameObject)
RegistChildComponent(LuaShaoKaoPlayWnd, "CloseButton", "CloseButton", CButton)
RegistChildComponent(LuaShaoKaoPlayWnd, "Background", "Background", GameObject)

--@endregion RegistChildComponent end
RegistClassMember(LuaShaoKaoPlayWnd, "m_PlayId")
RegistClassMember(LuaShaoKaoPlayWnd, "m_TotalTime")
RegistClassMember(LuaShaoKaoPlayWnd, "m_InitProgress")
RegistClassMember(LuaShaoKaoPlayWnd, "m_TotalProgress")
RegistClassMember(LuaShaoKaoPlayWnd, "m_AddProgress")
RegistClassMember(LuaShaoKaoPlayWnd, "m_CurrentProgress")
RegistClassMember(LuaShaoKaoPlayWnd, "m_FadeProgress")
RegistClassMember(LuaShaoKaoPlayWnd, "m_ValidProgress")
RegistClassMember(LuaShaoKaoPlayWnd, "m_FailTime")
RegistClassMember(LuaShaoKaoPlayWnd, "m_CurFailProgress")
RegistClassMember(LuaShaoKaoPlayWnd, "m_DialogInfo")

RegistClassMember(LuaShaoKaoPlayWnd, "m_CurLeftTime")
RegistClassMember(LuaShaoKaoPlayWnd, "m_CountDownTick")
RegistClassMember(LuaShaoKaoPlayWnd, "m_ShowResultTick")
RegistClassMember(LuaShaoKaoPlayWnd, "m_IsInGame")
function LuaShaoKaoPlayWnd:Awake()
    --@region EventBind: Dont ModiPfy Manually!
	
	UIEventListener.Get(self.FireBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnFireBtnClick()
	end)


	
	UIEventListener.Get(self.CloseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCloseButtonClick()
	end)

	UIEventListener.Get(self.GetRewardView.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnGetRewardViewClick()
	end)


    --@endregion EventBind end
	self.m_CountDownTick = nil
	self.m_ShowResultTick = nil
	self.m_IsInGame = false
	self.m_PlayId = 0
	self.m_CurLeftTime = 0
	self.m_CurFailProgress = 0
	self.GetRewardView.gameObject:SetActive(false)
end

function LuaShaoKaoPlayWnd:Init()
	local id = LuaZhuJueJuQingMgr.m_ProgressPlayId
	local data = ZhuJueJuQing_ProgressPlay.GetData(id)
	if not data then return end
	self.m_PlayId = id
	self.m_TotalTime = data.PlayTime
	self.m_TotalProgress = data.TotalProgress
	self.m_InitProgress = data.InitProgress
	self.m_AddProgress = data.AddProgress
	self.m_FadeProgress = data.FadeProgress
	self.m_FailTime = data.FailTime
	self.m_ValidProgress = {}
	for i = 0, data.ValidProgress.Length-1 do
		table.insert(self.m_ValidProgress, data.ValidProgress[i])
	end
	self.m_DialogInfo = {}
	for i = 0, data.DoBatDialog.Length-1 do
		local dialogInfo = data.DoBatDialog[i]
		self.m_DialogInfo[tonumber(dialogInfo[0])] = tonumber(dialogInfo[1])
	end

	self:InitView()

end

function LuaShaoKaoPlayWnd:InitView()
	self.GetRewardView.gameObject:SetActive(false)
	local bgwidgt = self.Background:GetComponent(typeof(UIWidget))
	local forewidget = self.Foreground:GetComponent(typeof(UIWidget))
	Extensions.SetLocalPositionX(self.Foreground.transform, bgwidgt.width * (self.m_ValidProgress[1] / self.m_TotalProgress) - bgwidgt.width / 2)
	forewidget.width = bgwidgt.width * (self.m_ValidProgress[2] / self.m_TotalProgress) - bgwidgt.width * (self.m_ValidProgress[1] / self.m_TotalProgress)
	self.Progress.value = self.m_InitProgress / self.m_TotalProgress
	self.rightfx:SetActive(false)
	self.wrongfx:SetActive(false)
	self.Common.gameObject:SetActive(true)
	self.Success.gameObject:SetActive(false)
	self.Faild.gameObject:SetActive(false)
	self.TimeLeftLabel.text = nil
	self.m_IsInGame = false

	self:BeginGame()
end

function LuaShaoKaoPlayWnd:BeginGame()
	if self.m_CountDownTick then UnRegisterTick(self.m_CountDownTick) self.m_CountDownTick = nil end
	self.m_IsInGame = true
	self.m_CurLeftTime = self.m_TotalTime
	self.m_CurrentProgress = self.m_InitProgress
	self.TimeLeftLabel.text = self:GetRemainTimeText(self.m_CurLeftTime)

	self.m_CountDownTick = RegisterTickWithDuration(function()
		self.m_CurLeftTime = self.m_CurLeftTime - 1

		if self.m_CurLeftTime <= 0 then
			self.m_CurLeftTime = 0
			self:OnTimeOut()
		end
		if self.m_DialogInfo and self.m_DialogInfo[self.m_CurLeftTime] then
			CLuaBatDialogWnd.ShowWnd(self.m_DialogInfo[self.m_CurLeftTime])
		end
		self.TimeLeftLabel.text = self:GetRemainTimeText(self.m_CurLeftTime)
	end,1000, (self.m_TotalTime + 1) * 1000)

end


function LuaShaoKaoPlayWnd:Update()
	if not self.m_IsInGame then return end

	self.m_CurrentProgress = self.m_CurrentProgress - Time.deltaTime * self.m_FadeProgress
	self.m_CurrentProgress = math.max(self.m_CurrentProgress, 0)
	self.Progress.value = self.m_CurrentProgress / self.m_TotalProgress

	self:CheckFireTime()
end

function LuaShaoKaoPlayWnd:CheckFireTime()
	if self.m_CurrentProgress < self.m_ValidProgress[1] or self.m_CurrentProgress > self.m_ValidProgress[2] then
		self.m_CurFailProgress = self.m_CurFailProgress + Time.deltaTime
		self.rightfx:SetActive(false)
		self.wrongfx:SetActive(true)
	else
		self.wrongfx:SetActive(false)
		self.rightfx:SetActive(true)
	end
	if self.m_CurFailProgress >= self.m_FailTime then
		self:PlayFailed()
	end
end

function LuaShaoKaoPlayWnd:PlayFailed()
	self.m_IsInGame = false
	-- 播放失败动效
	EventManager.BroadcastInternalForLua(EnumEventType.OnLoadCommonUIFx, {"Fx/UI/Prefab/UI_shibai.prefab"})
	self.Common.gameObject:SetActive(false)
	self.Success.gameObject:SetActive(false)
	self.Faild.gameObject:SetActive(true)

	if self.m_ShowResultTick then UnRegisterTick(self.m_ShowResultTick) self.m_ShowResultTick = nil end
	if self.m_CountDownTick then UnRegisterTick(self.m_CountDownTick) self.m_CountDownTick = nil end
	g_MessageMgr:ShowMessage("ShaoKao_Failed")
	self.m_ShowResultTick = RegisterTickOnce(function()
		CUIManager.CloseUI(CLuaUIResources.ShaoKaoPlayWnd)
	end, 1000)
end

function LuaShaoKaoPlayWnd:OnTimeOut()
	self.m_IsInGame = false
	-- 播成功动效
	EventManager.BroadcastInternalForLua(EnumEventType.OnLoadCommonUIFx, {"Fx/UI/Prefab/UI_wancheng.prefab"})
	self.Common.gameObject:SetActive(false)
	self.Success.gameObject:SetActive(true)
	self.Faild.gameObject:SetActive(false)

	-- 发成功RPC
	Gac2Gas.FinishEventTask(self.m_PlayId,"shanzhenmeiwei")

	if self.m_ShowResultTick then UnRegisterTick(self.m_ShowResultTick) self.m_ShowResultTick = nil end
	if self.m_CountDownTick then UnRegisterTick(self.m_CountDownTick) self.m_CountDownTick = nil end
	self.m_ShowResultTick = RegisterTickOnce(function()
		self.Anchor.gameObject:SetActive(false)
		self.GetRewardView.gameObject:SetActive(true)
	end, 1000)
end

function LuaShaoKaoPlayWnd:GetRemainTimeText(totalSeconds)
	if totalSeconds < 0 then
        totalSeconds = 0
    end
    if totalSeconds >= 3600 then
        return SafeStringFormat3("%02d:%02d:%02d", math.floor(totalSeconds / 3600), math.floor(totalSeconds % 3600 / 60), totalSeconds % 60)
    else
        return SafeStringFormat3("%02d:%02d", math.floor(totalSeconds / 60), totalSeconds % 60)
    end
end

--@region UIEvent

function LuaShaoKaoPlayWnd:OnFireBtnClick()
	if not self.m_IsInGame then return end
	self.m_CurrentProgress = self.m_CurrentProgress + self.m_AddProgress
end

function LuaShaoKaoPlayWnd:OnCloseButtonClick()
	g_MessageMgr:ShowOkCancelMessage(g_MessageMgr:FormatMessage("LEAVE_COPY_CONFIRM"), function ()
		Gac2Gas.RequestLeavePlay()
		CUIManager.CloseUI(CLuaUIResources.ShaoKaoPlayWnd)
	end,nil, nil, nil, false)
end

function LuaShaoKaoPlayWnd:OnGetRewardViewClick()
	CUIManager.CloseUI(CLuaUIResources.ShaoKaoPlayWnd)
end

--@endregion UIEvent

function LuaShaoKaoPlayWnd:OnDisable()
	if self.m_CountDownTick then UnRegisterTick(self.m_CountDownTick) self.m_CountDownTick = nil end
	if self.m_ShowResultTick then UnRegisterTick(self.m_ShowResultTick) self.m_ShowResultTick = nil end
end