local SpecialGameLauncher          = import "L10.Engine.SpecialGameLauncher"
local SpicalLaunchType             = import "L10.Engine.SpicalLaunchType"
local Application                  = import "Application"
local CFacialMgr                   = import "L10.Game.CFacialMgr"
local CPlayerDataMgr               = import "L10.Game.CPlayerDataMgr"
local NativeTools                  = import "L10.Engine.NativeTools"
local CFileTools                   = import "CFileTools"
local EnumClass                    = import "L10.Game.EnumClass"
local EnumGender                   = import "L10.Game.EnumGender"
local CPinchFaceHubMgr             = import "L10.Game.CPinchFaceHubMgr"
local CPinchFaceCinemachineCtrlMgr = import "L10.Game.CPinchFaceCinemachineCtrlMgr"
local Screen                       = import "UnityEngine.Screen"
local RenderTexture                = import "UnityEngine.RenderTexture"
local RenderTextureFormat          = import "UnityEngine.RenderTextureFormat"
local RenderTextureReadWrite       = import "UnityEngine.RenderTextureReadWrite"
local Texture2D                    = import "UnityEngine.Texture2D"
local TextureFormat                = import "UnityEngine.TextureFormat"
local Rect                         = import "UnityEngine.Rect"
local CResourceMgr                 = import "L10.Engine.CResourceMgr"
local Setting                      = import "L10.Engine.Setting"
local PlayerSettings               = import "L10.Game.PlayerSettings"
local JsonUtility                  = import "UnityEngine.JsonUtility"
local CLoadingWnd                  = import "L10.UI.CLoadingWnd"
local File                         = import "System.IO.File"
local CFuxiFaceDNAMgr              = import "L10.Game.CFuxiFaceDNAMgr"
local FacialSaveData               = import "L10.Game.CFacialMgr.FacialSaveData"
local CRenderObject                = import "L10.Engine.CRenderObject"

LuaCloudGameFaceMgr = {}

LuaCloudGameFaceMgr.checkTick = nil
LuaCloudGameFaceMgr.isDaShen = false

EnumCloudGamePresetFashionId = {
    [1] = 1,
    [2] = 12,
    [3] = 10,
    [4] = 1,
    [5] = 3,
    [6] = 3,
    [7] = 1,
    [8] = 3,
    [9] = 5,
    [10] = 5,
    [11] = 1
}

if CommonDefs.IsAndroidPlatform() then
    LuaCloudGameFaceMgr.initDataPath = "/mnt/sdcard/Documents/InitFacialData.txt"
    LuaCloudGameFaceMgr.resultPhotoPath = "/mnt/sdcard/Pictures/Result.jpg"
else
    LuaCloudGameFaceMgr.initDataPath = Application.dataPath .. "/../InitFacialData.txt"
    LuaCloudGameFaceMgr.resultPhotoPath = Application.dataPath .. "/../Result.jpg"
end

SpecialGameLauncher.s_hookShowFacialWnd = function(this)
    CFuxiFaceDNAMgr.m_IsOpenFuxiFacialDNA = false
    CFuxiFaceDNAMgr.m_IsOpenFuxiRandomFacialDNA = false

    LuaCloudGameFaceMgr:PatchCloseCloth()

    if CommonDefs.IsAndroidPlatform() then
        CPlayerDataMgr.CLOBAL_DATA_PATH = "/mnt/sdcard/Documents/"

        Setting.eDefaultFPS = 60
        Setting.eLowFPS = 30
        Setting.eTickInterval = 16
        Application.targetFrameRate = 60
        PlayerSettings.HighFrameRateEnabled = true
    end

    CFacialMgr.m_hookClearFacialDataCache = function(path)
        if File.Exists(path) then
            File.Delete(path)
        end
    end

    if LuaCloudGameFaceMgr:IsDaShenCloudGameFace() then
        LuaCloudGameFaceMgr:ShowLoginUI()
        CPinchFaceHubMgr.s_CloudGameAccount = "ds:" .. math.random(100000, 999999) .. math.random(100000, 999999)
        return
    end

    -- 打开捏脸后等一秒再关闭Loading界面
    if not LuaCloudGameFaceMgr:CheckInitData() then
        LuaCloudGameFaceMgr.checkTick = RegisterTick(function()
            LuaCloudGameFaceMgr:CheckInitData()
        end, 100)
    end
end

function LuaCloudGameFaceMgr:PatchCloseCloth()
    PinchFace_PreviewFashion.PatchField(7, "GenderLimit", 3)
    PinchFace_PreviewFashion.PatchField(9, "GenderLimit", 3)
    PinchFace_PreviewFashion.PatchField(8, "Number", LocalString.GetString("陆"))
    PinchFace_PreviewFashion.PatchField(10, "Number", LocalString.GetString("柒"))
    PinchFace_PreviewFashion.PatchField(11, "Number", LocalString.GetString("捌"))
    PinchFace_PreviewFashion.PatchField(12, "Number", LocalString.GetString("捌"))

    PinchFace_PreselectionCustomStyle.PatchField(4, "GenderLimit", 3)
    PinchFace_PreselectionCustomStyle.PatchField(12, "GenderLimit", 3)
end

function LuaCloudGameFaceMgr:ShowLoginUI()
    LuaCharacterCreation2023Mgr:ShowLoginUI()
    RegisterTickOnce(function()
        if CLoadingWnd.Inst then
            CLoadingWnd.Inst:FinishLoadCloudGameFace()
        end
    end, 500)
end

-- 检查初始数据
function LuaCloudGameFaceMgr:CheckInitData()
    local initDataStr = CFileTools.LoadFile(self.initDataPath)
    if initDataStr then
        local tbl = luaJson.json2table(initDataStr)
        if tbl and tbl.account then
            CPinchFaceHubMgr.s_CloudGameAccount = tbl.account
            local gender = tbl.gender and CommonDefs.ConvertIntToEnum(typeof(EnumGender), tbl.gender)
            local class = tbl.careerId and CommonDefs.ConvertIntToEnum(typeof(EnumClass), tbl.careerId)
            local faceId = tbl.faceId

            local cacheData = LuaPinchFaceMgr:LoadOfflineDataCache()
            local needSetFashionData = false
            if not cacheData and faceId then
                local filename = "face_" .. faceId .. ".json"
                local path = CResourceMgr.Inst:GetSyncLoadPath("assets/res/syncload/cloudgame_presetface/" .. filename)
                cacheData = CFacialMgr.LoadFacialDataCache(path)
                cacheData.Gender = gender
                cacheData.Job = class
                local json = JsonUtility.ToJson(cacheData)
                CFileTools.SaveFile(CPlayerDataMgr.CLOBAL_DATA_PATH .. "FacialDataCache.txt", json)
                needSetFashionData = true
            end

            if cacheData then
                LuaPinchFaceMgr.m_IsUseIKLookAt = false
                LuaPinchFaceMgr:OnDestroy()
                LuaPinchFaceMgr.m_IsInCreateMode = true
                LuaPinchFaceMgr.m_IsInModifyMode = false
                LuaPinchFaceMgr.m_CurEnumClass = cacheData.Job
                LuaPinchFaceMgr.m_CurEnumGender =  cacheData.Gender
                LuaPinchFaceMgr.m_HeadId = cacheData.HairColorIndex
                LuaPinchFaceMgr.m_HairId = cacheData.HairId
                local customFacialData = CFacialMgr.GetCustomFacialData(cacheData.FaceDnaData, cacheData.PartDatas)
                LuaPinchFaceMgr.m_CacheCustomFacialData = customFacialData
                LuaPinchFaceMgr:CacheWndData()
                if needSetFashionData then
                    LuaPinchFaceMgr:SetFashionData(EnumCloudGamePresetFashionId[faceId])
                end
                if CLoadingWnd.Inst then
                    CLoadingWnd.Inst:SetTipLabel(LocalString.GetString("正在加载角色资源..."))
                end
                CUIManager.ShowUI(CLuaUIResources.PinchFaceWnd)
            else
                LuaCloudGameFaceMgr:ShowLoginUI()
            end
            self:ClearCheckTick()
            return true
        end
    end
    return false
end

function LuaCloudGameFaceMgr:CacheFacialData()
    if not self:IsDaShenCloudGameFace() then
        LuaPinchFaceMgr:CacheOfflineData()
    end
end

function LuaCloudGameFaceMgr:CacheOfflineData(facialData)
    local facialSaveData = CreateFromClass(FacialSaveData)
    facialSaveData.Gender = LuaPinchFaceMgr.m_CurEnumGender
    facialSaveData.Job = LuaPinchFaceMgr.m_CurEnumClass
    facialSaveData.FaceDnaData = CFacialMgr.GetFaceDnaDataByCustomFacialData(facialData, CRenderObject.s_FACE_DNA_LEN)
    facialSaveData.PartDatas = CFacialMgr.GetPartDatasByCustomFacialData(facialData, CRenderObject.s_FACE_DNA_LEN)
    facialSaveData.HairId = LuaPinchFaceMgr.m_HairId
    facialSaveData.HairColorIndex = LuaPinchFaceMgr.m_HeadId
    facialSaveData.FaceId = LuaPinchFaceMgr.m_PreselectionIndex + 1
    local json = JsonUtility.ToJson(facialSaveData)
    CFileTools.SaveFile(CPlayerDataMgr.CLOBAL_DATA_PATH .. "FacialDataCache.txt", json)
    LuaPinchFaceMgr.m_CanCacheOfflineData = false
end

-- 清除Tick
function LuaCloudGameFaceMgr:ClearCheckTick()
    if self.checkTick then
        UnRegisterTick(self.checkTick)
        self.checkTick = nil
    end
end

-- 是否是云游戏捏脸
function LuaCloudGameFaceMgr:IsCloudGameFace()
    return SpecialGameLauncher.s_LaunchType and (SpecialGameLauncher.s_LaunchType == SpicalLaunchType.CloudGameFace)
end

-- 是否是大神云游戏捏脸
function LuaCloudGameFaceMgr:IsDaShenCloudGameFace()
    return self.isDaShen
end

-- 点击捏脸站按钮
function LuaCloudGameFaceMgr:OnLibraryClick()
    g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("敬请期待"))
end

-- 点击只能按钮
function LuaCloudGameFaceMgr:OnPhotoClick()
    g_MessageMgr:ShowMessage("CUSTOM_STRING2", LocalString.GetString("敬请期待"))
end

-- 完成捏脸
function LuaCloudGameFaceMgr:FinishCloudFace()
    RegisterTickOnce(function()
        local jpgBytes = self:GetScreenShoot()
        System.IO.File.WriteAllBytes(self.resultPhotoPath, jpgBytes)
        if CommonDefs.IsAndroidPlatform() then
            NativeTools.RefreshPhotoLib(self.resultPhotoPath)
        end

        local careerId = EnumToInt(LuaPinchFaceMgr.m_CurEnumClass)
        local gender = EnumToInt(LuaPinchFaceMgr.m_CurEnumGender)
        Application.OpenURL("cg://page_forward/?careerId=" .. careerId .. "&gender=" .. gender)
    end, 100)
end

-- 屏幕截图，因为渲染不是用的主相机，CUICommonDef.CaptureScreen不能用，所以单独写一个截图逻辑
function LuaCloudGameFaceMgr:GetScreenShoot()
    if not CPinchFaceCinemachineCtrlMgr.Inst then return end
    local camera = CPinchFaceCinemachineCtrlMgr.Inst.m_Cam

    local screenWidth = Screen.width
    local screenHeight = Screen.height

    -- 渲染一个rt
    local rt = RenderTexture.GetTemporary(screenWidth, screenHeight, 16, RenderTextureFormat.ARGB32, RenderTextureReadWrite.sRGB)
    camera.targetTexture = rt
    camera:Render()
    CUIManager.UIMainCamera.targetTexture = rt
    CUIManager.UIMainCamera:Render()

    -- 从rt读一个texture
    local tex = Texture2D(screenWidth, screenHeight, TextureFormat.RGB24, false)
    local old = RenderTexture.active
    RenderTexture.active = rt

    local readRect = Rect(0, 0, screenWidth, screenHeight)
    tex:ReadPixels(readRect, 0, 0)
    tex:Apply()
    RenderTexture.active = old

    camera.targetTexture = nil
    CUIManager.UIMainCamera.targetTexture = nil
    RenderTexture.ReleaseTemporary(rt)
    local bytes = CommonDefs.EncodeToJPG(tex)
    GameObject.Destroy(tex)
    return bytes
end

-- 点击了返回按钮
function LuaCloudGameFaceMgr:OnReturnClick()
    Application.OpenURL("cg://page_backward/")
end
