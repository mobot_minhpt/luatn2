-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CellIndex = import "L10.UI.UITableView+CellIndex"
local CellIndexType = import "L10.UI.UITableView+CellIndexType"
local CFreightMgr = import "L10.Game.CFreightMgr"
local CommonDefs = import "L10.Game.CommonDefs"
local CPlayerShopBuyTableView = import "L10.UI.CPlayerShopBuyTableView"
local CPlayerShopMgr = import "L10.UI.CPlayerShopMgr"
local CYuanbaoBuySectionTemplate = import "L10.UI.CYuanbaoBuySectionTemplate"
local EnumGuildFreightStatus = import "L10.Game.EnumGuildFreightStatus"
local GuildFreight_Equipments = import "L10.Game.GuildFreight_Equipments"
local Item_Item = import "L10.Game.Item_Item"
local NeedTag = import "L10.UI.CPlayerShopBuyTableView+NeedTag"
local Section = import "L10.UI.CPlayerShopBuyTableView+Section"
local String = import "System.String"
local UInt32 = import "System.UInt32"
local EnumRedDotNotifierType = import "L10.Game.EnumRedDotNotifierType"
CPlayerShopBuyTableView.m_Init_CS2LuaHook = function (this, sections, subsections, isPublicity,  typeList) 
    if typeList then
        this.m_SectionNotifierTypeList = typeList
    end
    CommonDefs.ListClear(this.sectionsData)
    do
        local i = 0
        while i < sections.Count do
            local section = CreateFromClass(Section)
            section.Name = sections[i]
            section.RowList = CommonDefs.DictGetValue(subsections, typeof(String), section.Name)
            CommonDefs.ListAdd(this.sectionsData, typeof(Section), section)
            i = i + 1
        end
    end
    if not isPublicity then
        this:UpdateNeedTagList()
    end
    this:LoadData(CreateFromClass(CellIndex, 0, 0, CellIndexType.Section), true)
end
CPlayerShopBuyTableView.m_SelectRowIndex_CS2LuaHook = function (this, section, row) 
    local type = CellIndexType.Section
    if row ~= 0 then
        type = CellIndexType.Row
    end
    local cellIndex = CreateFromClass(CellIndex, section, row, type)
    if type == CellIndexType.Section then
        this:SetSectionSelected(cellIndex)
    else
        this:SetRowSelected(cellIndex)
    end
    --只滚动到对应的section
    this:SetContentOffset(CreateFromClass(CellIndex, cellIndex.section, 0, CellIndexType.Section))
end
CPlayerShopBuyTableView.m_RefreshCellForRowAtIndex_CS2LuaHook = function (this, cell, index) 
    local notifierType = EnumRedDotNotifierType.None;
    if this.m_SectionNotifierTypeList.Count > index.section then
        notifierType = this.m_SectionNotifierTypeList[index.section]
    end
    if index.type == CellIndexType.Section then
        do
            local i = 0
            while i < this.m_NeedTagList.Count do
                if this.m_NeedTagList[i].Category == index.section then
                    CommonDefs.GetComponentInChildren_GameObject_Type(cell, typeof(CYuanbaoBuySectionTemplate)):Init(this.sectionsData[index.section].Name, true, notifierType)
                    return
                end
                i = i + 1
            end
        end
        CommonDefs.GetComponentInChildren_GameObject_Type(cell, typeof(CYuanbaoBuySectionTemplate)):Init(this.sectionsData[index.section].Name, false, notifierType)
    else
        do
            local i = 0
            while i < this.m_NeedTagList.Count do
                if this.m_NeedTagList[i].Category == index.section and this.m_NeedTagList[i].subCategory == index.row then
                    CommonDefs.GetComponentInChildren_GameObject_Type(cell, typeof(CYuanbaoBuySectionTemplate)):Init(this.sectionsData[index.section].RowList[index.row], true, notifierType)
                    return
                end
                i = i + 1
            end
        end
        CommonDefs.GetComponentInChildren_GameObject_Type(cell, typeof(CYuanbaoBuySectionTemplate)):Init(this.sectionsData[index.section].RowList[index.row], false, notifierType)
    end
end
CPlayerShopBuyTableView.m_UpdateNeedTagList_CS2LuaHook = function (this)
    CommonDefs.ListClear(this.m_NeedTagList)
    if CClientMainPlayer.Inst == nil then
        return
    end
    --货运任务购买物品
    if CommonDefs.ListContains(CClientMainPlayer.Inst.TaskProp.CurrentTaskList, typeof(UInt32), CFreightMgr.Inst.taskId) and not CommonDefs.DictGetValue(CClientMainPlayer.Inst.TaskProp.CurrentTasks, typeof(UInt32), CFreightMgr.Inst.taskId).IsFailed then
        do
            local i = 0
            while i < CFreightMgr.Inst.cargoList.Count do
                if (CFreightMgr.Inst.cargoList[i].status == EnumGuildFreightStatus.eNotFilled or CFreightMgr.Inst.cargoList[i].status == EnumGuildFreightStatus.eNotFilledNeedHelp) then
                    local templateId = CFreightMgr.Inst.cargoList[i].templateId
                    local data = Item_Item.GetData(templateId)
                    if data ~= nil then
                        local group = CPlayerShopMgr.Inst:GetItemGroupName(data.Type)
                        local subgroup = CPlayerShopMgr.Inst:GetItemTypeName(data.Type)
                        if not System.String.IsNullOrEmpty(group) and not System.String.IsNullOrEmpty(subgroup) then
                            --只有玩家商店group包含的TemplateId才显示角标
                            local templateIds = CPlayerShopMgr.Inst:GetItemTemplatesInGroup(group, subgroup)
                            if CommonDefs.ListContains(templateIds, typeof(UInt32), templateId) then
                                local tag = this:GetNeedTag(group, subgroup)
                                CommonDefs.ListAdd(this.m_NeedTagList, typeof(NeedTag), tag)
                            end
                        end
                    else
                        local equipData = GuildFreight_Equipments.GetData(templateId)
                        if equipData ~= nil then
                            local tag = CreateFromClass(NeedTag)
                            tag.Category = 1
                            tag.subCategory = 0
                            CommonDefs.ListAdd(this.m_NeedTagList, typeof(NeedTag), tag)
                        end
                    end
                end
                i = i + 1
            end
        end
    end
end
CPlayerShopBuyTableView.m_GetNeedTag_CS2LuaHook = function (this, groupName, typeName) 
    local tag = CreateFromClass(NeedTag)
    do
        local i = 0
        while i < this.sectionsData.Count do
            if this.sectionsData[i].Name == groupName then
                tag.Category = i
                do
                    local j = 0
                    while j < this.sectionsData[i].RowList.Count do
                        if this.sectionsData[i].RowList[j] == typeName then
                            tag.subCategory = j
                            break
                        end
                        j = j + 1
                    end
                end
                break
            end
            i = i + 1
        end
    end
    return tag
end
