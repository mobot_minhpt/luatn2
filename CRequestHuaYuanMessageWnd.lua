-- Auto Generated!!
local ChujiaMgr = import "L10.Game.ChujiaMgr"
local CInputBoxMgr = import "L10.UI.CInputBoxMgr"
local CRequestHuaYuanMessageWnd = import "L10.UI.CRequestHuaYuanMessageWnd"
local CUICommonDef = import "L10.UI.CUICommonDef"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local CWordFilterMgr = import "L10.Game.CWordFilterMgr"
local Int32 = import "System.Int32"
local LocalString = import "LocalString"
local QnButton = import "L10.UI.QnButton"
local String = import "System.String"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CRequestHuaYuanMessageWnd.m_Init_CS2LuaHook = function (this) 

    this.sayingLabel.text = System.String.Format(LocalString.GetString("对[c][ffed5f]{0}[-][/c]说:"), ChujiaMgr.HuaYuanRecieverName)
    this.labels[0].text = g_MessageMgr:FormatMessage("HuaYuan_Message01")
    this.labels[1].text = g_MessageMgr:FormatMessage("HuaYuan_Message02")
    this.labels[2].text = g_MessageMgr:FormatMessage("HuaYuan_Message03")

    this.mMessage = g_MessageMgr:FormatMessage("HuaYuan_Message01")
end
CRequestHuaYuanMessageWnd.m_Awake_CS2LuaHook = function (this) 
    UIEventListener.Get(this.closeButton).onClick = MakeDelegateFromCSFunction(this.OnClickCloseButton, VoidDelegate, this)
    UIEventListener.Get(this.message4Button).onClick = MakeDelegateFromCSFunction(this.OnClickCustomMsgBtn, VoidDelegate, this)
    UIEventListener.Get(this.sendButton).onClick = MakeDelegateFromCSFunction(this.OnClickSendButton, VoidDelegate, this)
    this.radioBox.OnSelect = MakeDelegateFromCSFunction(this.OnClickMessageButton, MakeGenericClass(Action2, QnButton, Int32), this)
end
CRequestHuaYuanMessageWnd.m_OnClickCustomMsgBtn_CS2LuaHook = function (this, go) 
    CInputBoxMgr.ShowInputBox(LocalString.GetString("输入想对施主说的话"), MakeDelegateFromCSFunction(this.OnConfirmMessage, MakeGenericClass(Action1, String), this), ChujiaMgr.MaxApplyMsgLen * 2, true, nil, LocalString.GetString("点击此处输入(15字以内)"))
end
CRequestHuaYuanMessageWnd.m_OnConfirmMessage_CS2LuaHook = function (this, msg) 
    msg = StringTrim(msg)
    if not System.String.IsNullOrEmpty(msg) then
        if this:CheckMsg(msg) then
            Gac2Gas.ConfirmChuJiaHuaYuan(msg)
            CUIManager.CloseUI(CUIResources.RequestHuaYuanMessageWnd)
        end
    end
end
CRequestHuaYuanMessageWnd.m_CheckMsg_CS2LuaHook = function (this, msg) 
    local length = CUICommonDef.GetStrByteLength(msg)
    local ret = CWordFilterMgr.Inst:DoFilterOnSendWithoutVoiceCheck(msg, true)
    if ret.msg == nil or ret.shouldBeIgnore then
        g_MessageMgr:ShowMessage("Speech_Violation")
        return false
    end
    return true
end
CRequestHuaYuanMessageWnd.m_OnClickSendButton_CS2LuaHook = function (this, go) 
    if this.mMessage == nil or System.String.IsNullOrEmpty(this.mMessage) then
        --g_MessageMgr:ShowMessage("HUAYUAN_MESSAGE_NULL")
    elseif this:CheckMsg(this.mMessage) then
        Gac2Gas.ConfirmChuJiaHuaYuan(this.mMessage)
        CUIManager.CloseUI(CUIResources.RequestHuaYuanMessageWnd)
    end
end
CRequestHuaYuanMessageWnd.m_OnClickMessageButton_CS2LuaHook = function (this, btn, index) 
    if index == 0 then
        this.mMessage = g_MessageMgr:FormatMessage("HuaYuan_Message01")
    elseif index == 1 then
        this.mMessage = g_MessageMgr:FormatMessage("HuaYuan_Message02")
    elseif index == 2 then
        this.mMessage = g_MessageMgr:FormatMessage("HuaYuan_Message03")
    end
end
