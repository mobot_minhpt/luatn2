-- Auto Generated!!
local CJieBaiRankPlayerTemplate = import "L10.UI.CJieBaiRankPlayerTemplate"
local CUICommonDef = import "L10.UI.CUICommonDef"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CJieBaiRankPlayerTemplate.m_Init_CS2LuaHook = function (this, info) 
    this.info = info
    local ret = CUICommonDef.GetPortraitName(info.playerCls, info.gender, -1)
    if not System.String.IsNullOrEmpty(ret) then
        this.icon:LoadNPCPortrait(ret, false)
    end
    this.grade.text = tostring(info.grade)
    this.personalTitle.text = info.personalTitle
    this.playerName.text = info.playerName

    UIEventListener.Get(this.gameObject).onClick = MakeDelegateFromCSFunction(this.OnPlayerClick, VoidDelegate, this)
end
