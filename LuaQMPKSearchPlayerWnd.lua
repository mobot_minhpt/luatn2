local CQuanMinPKMgr = import "L10.Game.CQuanMinPKMgr"
local QnTableView=import "L10.UI.QnTableView"
local CUITexture=import "L10.UI.CUITexture"
local DefaultTableViewDataSource=import "L10.UI.DefaultTableViewDataSource"
local Profession=import "L10.Game.Profession"

CLuaQMPKSearchPlayerWnd = class()
RegistClassMember(CLuaQMPKSearchPlayerWnd,"m_PlayerTable")
RegistClassMember(CLuaQMPKSearchPlayerWnd,"m_RefreshBtn")
RegistClassMember(CLuaQMPKSearchPlayerWnd,"m_CloseBtn")
RegistClassMember(CLuaQMPKSearchPlayerWnd,"m_PlayerList")

function CLuaQMPKSearchPlayerWnd:Awake()
    self.m_PlayerTable = self.transform:Find("Anchor/ResultTable"):GetComponent(typeof(QnTableView))
    self.m_RefreshBtn = self.transform:Find("Anchor/RefreshBtn").gameObject
    self.m_CloseBtn = self.transform:Find("Wnd_Bg_Secondary_1/CloseButton").gameObject
    self.m_PlayerList = {}

end

function CLuaQMPKSearchPlayerWnd:OnEnable( )
    UIEventListener.Get(self.m_RefreshBtn).onClick = DelegateFactory.VoidDelegate(function(go) self:OnRefreshBtnClicked(go) end)
    g_ScriptEvent:AddListener("ReplyQmpkPlayerListQueryResult", self, "OnQueryPlayerResult")

end
function CLuaQMPKSearchPlayerWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("ReplyQmpkPlayerListQueryResult", self, "OnQueryPlayerResult")
end
function CLuaQMPKSearchPlayerWnd:Init( )
    local getNumFunc=function() return #self.m_PlayerList end
    local initItemFunc=function(item,index) 
        self:InitItem(item,index,self.m_PlayerList[index+1])
    end
    self.m_PlayerTable.m_DataSource=DefaultTableViewDataSource.Create(getNumFunc,initItemFunc)
    self.m_PlayerTable.OnSelectAtRow=DelegateFactory.Action_int(function (row)
        self:OnPlayerItemSelected(row)
    end)
    self:OnRefreshBtnClicked(nil)

end
function CLuaQMPKSearchPlayerWnd:OnQueryPlayerResult( result) 
    self.m_PlayerList = result
    self.m_PlayerTable:ReloadData(true, false)
end

function CLuaQMPKSearchPlayerWnd:OnRefreshBtnClicked( go) 
    Gac2Gas.RequestQueryQmpkPlayerList(
        CQuanMinPKMgr.Inst.m_SearchClass, 
        CQuanMinPKMgr.Inst.m_SearchLocation, 
        CQuanMinPKMgr.Inst.m_SearchTime, 
        CQuanMinPKMgr.Inst.m_SearchCommandIndex, 
        CQuanMinPKMgr.Inst.m_SearchTeamIndex)
end
function CLuaQMPKSearchPlayerWnd:OnPlayerItemSelected( row) 
    Gac2Gas.RequestQueryQmpkPlayerDetails(self.m_PlayerList[row+1].m_Id)
end

function CLuaQMPKSearchPlayerWnd:InitItem(item,row,player)
    local transform=item.transform
    local m_PortraitTex = transform:Find("Portrait/Icon"):GetComponent(typeof(CUITexture))
    local m_LevelLabel = transform:Find("Portrait/Label"):GetComponent(typeof(UILabel))
    local m_NameLabel = transform:Find("Name"):GetComponent(typeof(UILabel))
    local m_RemarkLabel = transform:Find("Remark"):GetComponent(typeof(UILabel))
    local m_GoodAtClassSprite = {}
    m_GoodAtClassSprite[0]=transform:Find("GoodAtSprite1"):GetComponent(typeof(UISprite))
    m_GoodAtClassSprite[1]=transform:Find("GoodAtSprite2"):GetComponent(typeof(UISprite))
    m_GoodAtClassSprite[2]=transform:Find("GoodAtSprite3"):GetComponent(typeof(UISprite))

    local m_CommonderObj = transform:Find("Commander").gameObject

    m_PortraitTex:LoadNPCPortrait(CUICommonDef.GetPortraitName(player.m_Clazz, player.m_Gender, -1), false)
    m_NameLabel.text = player.m_Name
    m_RemarkLabel.text = player.m_Remark

    local default, str = m_RemarkLabel:Wrap(player.m_Remark)
    if not default then
        m_RemarkLabel.text = CommonDefs.StringSubstring2(str, 0, CommonDefs.StringLength(str) - 1) .. "..."
    end

    m_LevelLabel.text = tostring(player.m_Grade)

    if player.m_CanCommand > 0 then
        m_CommonderObj:SetActive(true)
    else
        m_CommonderObj:SetActive(false)
    end
    local index = 0
    if player.m_GoodAtClass > 0 then
        for i = 0, 14 do
            if (bit.band(player.m_GoodAtClass, (bit.lshift(1, i)))) > 0 then
                m_GoodAtClassSprite[index].gameObject:SetActive(true)
                local extern = index
                index = extern + 1
                m_GoodAtClassSprite[extern].spriteName = Profession.GetIconByNumber(i + 1)
            end
        end
    end
    do
        local i = index local cnt = 3
        while i < cnt do
            m_GoodAtClassSprite[i].gameObject:SetActive(false)
            i = i + 1
        end
    end
end

return CLuaQMPKSearchPlayerWnd
