-- Auto Generated!!
local CClientMainPlayer = import "L10.Game.CClientMainPlayer"
local CYuanDanMgr = import "L10.Game.CYuanDanMgr"
local CYuanDanQiangDanRankItem = import "L10.UI.CYuanDanQiangDanRankItem"
local CYuanDanQiangDanSituationWnd = import "L10.UI.CYuanDanQiangDanSituationWnd"
CYuanDanQiangDanSituationWnd.m_CellForRowAtIndex_CS2LuaHook = function (this, index) 
    local cellIdentifier = "RowCell"

    local cell = this.tableView:DequeueReusableCellWithIdentifier(cellIdentifier)
    if cell == nil then
        cell = this.tableView:AllocNewCellWithIdentifier(this.template, cellIdentifier)
    end
    if index >= 0 and index < CYuanDanMgr.Inst.QiangDanPlayerInfoList.Count then
        local data = CYuanDanMgr.Inst.QiangDanPlayerInfoList[index]
        CommonDefs.GetComponentInChildren_GameObject_Type(cell, typeof(CYuanDanQiangDanRankItem)):Init(data, CClientMainPlayer.Inst ~= nil and CClientMainPlayer.Inst.Id == data.playerId)
    end

    return cell
end
