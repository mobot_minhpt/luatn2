-- Auto Generated!!
local CEquipIntensifyBaseItem = import "L10.UI.CEquipIntensifyBaseItem"
local Color = import "UnityEngine.Color"
local EquipIntensify_Intensify = import "L10.Game.EquipIntensify_Intensify"
local L10 = import "L10"
local NGUIMath = import "NGUIMath"
CEquipIntensifyBaseItem.m_UpdateAppearance_CS2LuaHook = function (this) 
    if this.m_Info.NeedPatch then
        if this.m_Info.addValue == this.m_Info.maxAddValue then
            --bgSprite.enabled = false;
            this.bgSprite.depth = 10
            this.bgSprite.color = NGUIMath.HexToColor(3285113855)
            --lineSprite.enabled = true;
        else
            --bgSprite.enabled = true;
            this.bgSprite.depth = 1
            this.bgSprite.color = Color.white
            --lineSprite.enabled = false;
        end
    else
        --bgSprite.enabled = true;
        this.bgSprite.depth = 1
        this.bgSprite.color = Color.white
        --lineSprite.enabled = false;
    end
end
CEquipIntensifyBaseItem.m_UpdateColor_CS2LuaHook = function (this) 
    if this.m_Info.NeedPatch then
        local config = EquipIntensify_Intensify.GetData(this.m_Info.Level)
        local colorIndex = - 1
        do
            local i = 0
            while i < config.Range.Length do
                if config.Range[i] == this.m_Info.addValue then
                    colorIndex = i
                    break
                end
                i = i + 1
            end
        end
        if colorIndex < 0 then
            L10.CLogMgr.LogError("get intensify color index error")
            return
        end

        local color = config.DisplayColor[colorIndex]
        if color == "A" then
            this.LevelLabel.color = NGUIMath.HexToColor(2178588415)
        elseif color == "B" then
            this.LevelLabel.color = NGUIMath.HexToColor(4202695423)
        elseif color == "C" then
            this.LevelLabel.color = NGUIMath.HexToColor(4283453695)
        end
    else
        this.LevelLabel.color = NGUIMath.HexToColor(4294967295)
    end
    this.percentLabel.color = this.LevelLabel.color
end
