local GameObject                = import "UnityEngine.GameObject"
local Vector4                   = import "UnityEngine.Vector4"
local CUIManager                = import "L10.UI.CUIManager"
local CUICommonDef              = import "L10.UI.CUICommonDef"
local CItemInfoMgr				= import "L10.UI.CItemInfoMgr"
local AlignType					= import "L10.UI.CItemInfoMgr+AlignType"
local QnTableView               = import "L10.UI.QnTableView"
local QnButton                  = import "L10.UI.QnButton"
local DefaultTableViewDataSource= import "L10.UI.DefaultTableViewDataSource"
local UILabel                   = import "UILabel"
local DelegateFactory		    = import "DelegateFactory"
local UIEventListener		    = import "UIEventListener"
local LocalString               = import "LocalString"
local LuaGameObject             = import "LuaGameObject"
local LuaTweenUtils             = import "LuaTweenUtils"
local NGUIMath                  = import "NGUIMath"
local PathMode                  = import "DG.Tweening.PathMode"
local PathType                  = import "DG.Tweening.PathType"
local Ease                      = import "DG.Tweening.Ease"
local QnAdvanceGridView = import "L10.UI.QnAdvanceGridView"
local ZuoQi_ZuoQi = import "L10.Game.ZuoQi_ZuoQi"
local CCPlayerCtrl = import "L10.Game.CCPlayerCtrl"
local CWinCGMgr = import "L10.Game.CWinCGMgr"
local COpenEntryMgr     = import "L10.Game.COpenEntryMgr"

--寒假2023通行证主界面奖励页签

LuaHanJia2023PassMainRewardTab = class()

RegistChildComponent(LuaHanJia2023PassMainRewardTab, "NextLabel",		    UILabel)
RegistChildComponent(LuaHanJia2023PassMainRewardTab, "TimeLb",		    UILabel)
RegistChildComponent(LuaHanJia2023PassMainRewardTab, "ItemCell1",		    GameObject)
RegistChildComponent(LuaHanJia2023PassMainRewardTab, "ItemCell2",		    GameObject)
RegistChildComponent(LuaHanJia2023PassMainRewardTab, "ItemCell3",		    GameObject)
RegistChildComponent(LuaHanJia2023PassMainRewardTab, "AdvPassImg2",		GameObject)
RegistChildComponent(LuaHanJia2023PassMainRewardTab, "AdvPassImg3",		GameObject)
RegistChildComponent(LuaHanJia2023PassMainRewardTab, "OneKeyBtn",		    GameObject)
RegistChildComponent(LuaHanJia2023PassMainRewardTab, "ShopBtn",		    GameObject)
RegistChildComponent(LuaHanJia2023PassMainRewardTab, "QnTableView1",		QnAdvanceGridView)
RegistChildComponent(LuaHanJia2023PassMainRewardTab, "Sprite2",           GameObject)
RegistChildComponent(LuaHanJia2023PassMainRewardTab, "Sprite3",           GameObject)
RegistChildComponent(LuaHanJia2023PassMainRewardTab, "RideItem",          GameObject)
RegistChildComponent(LuaHanJia2023PassMainRewardTab, "LastPageBtn",          GameObject)
RegistChildComponent(LuaHanJia2023PassMainRewardTab, "NextPageBtn",          GameObject)
RegistChildComponent(LuaHanJia2023PassMainRewardTab, "RideLabel",          UILabel)
RegistChildComponent(LuaHanJia2023PassMainRewardTab, "Ride",          GameObject)
RegistChildComponent(LuaHanJia2023PassMainRewardTab, "Flower",          GameObject)
RegistChildComponent(LuaHanJia2023PassMainRewardTab, "Sprite1",           GameObject)

RegistClassMember(LuaHanJia2023PassMainRewardTab,  "CurImpLv")
RegistClassMember(LuaHanJia2023PassMainRewardTab,  "SelectedItem")
RegistClassMember(LuaHanJia2023PassMainRewardTab,  "Isdirty")

function LuaHanJia2023PassMainRewardTab:Awake()
    self.CurImpLv = -1
    self.SelectedItem = nil
    self.SelectRideIndex = 1
    UIEventListener.Get(self.OneKeyBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnOneKeyBtnClick()
    end)

    UIEventListener.Get(self.ShopBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self:OnShopBtnClick()
    end)

    UIEventListener.Get(self.Sprite2).onClick = DelegateFactory.VoidDelegate(function(go)
        local passtype = LuaHanJia2023Mgr.XKLData.PassType
        if passtype == 0 or passtype == 2 then
            CUIManager.ShowUI(CLuaUIResources.HanJia2023PassDetailWnd)
        end
    end)

    UIEventListener.Get(self.Sprite3).onClick = DelegateFactory.VoidDelegate(function(go)
        local passtype = LuaHanJia2023Mgr.XKLData.PassType
        if passtype == 0 or passtype == 1 then
            CUIManager.ShowUI(CLuaUIResources.HanJia2023PassDetailWnd)
        end
    end)

    UIEventListener.Get(self.NextPageBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self.SelectRideIndex = self.SelectRideIndex + 1
        if self.SelectRideIndex == 4 then
            self.SelectRideIndex = 1
        end
        self:RefreshBear()
    end)

    UIEventListener.Get(self.LastPageBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        self.SelectRideIndex = (self.SelectRideIndex - 1)
        if self.SelectRideIndex == 0 then
            self.SelectRideIndex = 3
        end
        self:RefreshBear()
    end)

    self.spotTemplateContent = self.transform:Find("RideName")
    self.spotTemplate = self.spotTemplateContent:Find("SpriteTemplate")
    self.spotList = {}
    table.insert(self.spotList, self.spotTemplate)
    local spotPositionX = {-44.4, -22.2, 0}
    local spotPositionY = -480.4
    for i = 1, 2 do
        local obj = NGUITools.AddChild(self.spotTemplateContent.gameObject, self.spotTemplate.gameObject)
        table.insert(self.spotList, obj)
    end
    for i = 1, 3 do
        local go = self.spotList[i]
        go.transform.localPosition = Vector3(spotPositionX[i], spotPositionY, 0)
    end

    local setting = HanJia2023_XianKeLaiSetting.GetData()
    self.transform:Find("RideDesLabel"):GetComponent(typeof(UILabel)).text = g_MessageMgr:FormatMessage("HanJia2023_BattlePass_MainWnd_Tips")
    UIEventListener.Get(self.RideItem).onClick = DelegateFactory.VoidDelegate(function(go)
        local vehicleId = setting.VehicleID[self.SelectRideIndex-1]
        local zuoqiCfg = ZuoQi_ZuoQi.GetData(vehicleId)
        if zuoqiCfg then
            local previewUrl = zuoqiCfg.PreviewVideoUrl
            if not CCPlayerCtrl.IsSupportted() then
                g_MessageMgr:ShowMessage("CUSTOM_STRING1", LocalString.GetString("客户端引擎版本过旧，不能播放此视频，请更新版本后再试"))
                return
            end
            local url = CommonDefs.IS_PUB_RELEASE and "http://l10.gph.netease.com/fashionpreviewvideo/" or "http://l10-patch.leihuo.netease.com/fashionpreviewvideo/"
            CWinCGMgr.Inst.m_CGUrl = url .. previewUrl
            CUIManager.ShowUI(CUIResources.WinCGWnd)
        end
    end)

    self.QnTableView1.m_ScrollView.horizontalScrollBar.OnChangeValue = DelegateFactory.Action_float(function(value)
        self:OnScroll(value)
    end)


    local spriteList = {self.Sprite1, self.Sprite2, self.Sprite3}
    local names = g_LuaUtil:StrSplit(HanJia2023_XianKeLaiSetting.GetData().BattlePassNames, ";")
    for i = 1, #spriteList do
        spriteList[i].transform:Find("Label"):GetComponent(typeof(UILabel)).text = names[i] or ""
    end

    g_ScriptEvent:AddListener("HanJia2023_SyncXianKeLianData", self, "OnXianKeLaiDataChange")
    g_ScriptEvent:AddListener("HanJia2023_XianKeLaiPassTypeChange", self, "OnXianKeLaiPassTypeChange")
    g_ScriptEvent:AddListener("ShowUIPreDraw", self, "OnShowUIPreDraw")
    Gac2Gas.QueryXianKeLaiPlayData()

    self:TryPlayUnlockVipEffect()

    self.firstOpenWnd = true
end

function LuaHanJia2023PassMainRewardTab:RefreshBear()
    local flowerPositionX = {101, 50, -101}
    local flowerPositionY = -109
    for i = 1, 3 do
        self.Ride.transform:Find("hanji2023pass_bear_" .. tostring(i)).gameObject:SetActive(i == self.SelectRideIndex)
        if self.spotList[i] then
            self.spotList[i].transform:GetComponent(typeof(UISprite)).alpha = i == self.SelectRideIndex and 0.78 or 0.38
        end
    end
    self.Flower.transform.localPosition = Vector3(flowerPositionX[self.SelectRideIndex], flowerPositionY, 0)
    local setting = HanJia2023_XianKeLaiSetting.GetData()
    local showNames = g_LuaUtil:StrSplit(setting.ZuoQiIDName,";")
    self.RideLabel.text = showNames[self.SelectRideIndex]
end

function LuaHanJia2023PassMainRewardTab:OnDestroy()
    g_ScriptEvent:RemoveListener("HanJia2023_SyncXianKeLianData", self, "OnXianKeLaiDataChange")
    g_ScriptEvent:RemoveListener("HanJia2023_XianKeLaiPassTypeChange", self, "OnXianKeLaiPassTypeChange")
    g_ScriptEvent:RemoveListener("ShowUIPreDraw", self, "OnShowUIPreDraw")

    if self.showVip2UnlockAnimation then
        COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.HanJia2023BattlePassUnlockVip2Guide)
        UnRegisterTick(self.showVip2UnlockAnimation)
        self.showVip2UnlockAnimation = nil
    end
    if self.showVip1UnlockAnimation then
        COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.HanJia2023BattlePassUnlockVip1Guide)
        UnRegisterTick(self.showVip1UnlockAnimation)
        self.showVip1UnlockAnimation = nil
    end
    if self.showVip1Delay then
        UnRegisterTick(self.showVip1Delay)
        self.showVip1Delay = nil
    end
    if self.showVip2Delay then
        UnRegisterTick(self.showVip2Delay)
        self.showVip2Delay = nil
    end
end

function LuaHanJia2023PassMainRewardTab:OnXianKeLaiPassTypeChange(type)
    if type == 1 or type == 3 then
        local go = LuaGameObject.GetChildNoGC(self.Sprite2.transform,"Fxs").gameObject
        go:SetActive(true)
    end
    if type == 2 or type == 3 then
        local go = LuaGameObject.GetChildNoGC(self.Sprite3.transform,"Fxs").gameObject
        go:SetActive(true)
    end
end

function LuaHanJia2023PassMainRewardTab:OnScroll(value)--ok
    local vdelta = 5.2
    local impcount = 10
    local index = math.ceil((LuaHanJia2023Mgr.XKLData.CfgMaxLv - vdelta) * value+vdelta)
    local implv = (math.floor((index - 1) / impcount) + 1) * impcount 

    if implv == self.CurImpLv then return end
    self.CurImpLv = implv

    self:RefreshImpItems()
end

function LuaHanJia2023PassMainRewardTab:HaveImpReward(implv, data)--ok
    --判断impLv是否可以领取
    if data.RewardInfo[implv] then
        if data.PassType == 0 then
            --没买
            return (data.RewardInfo[implv] ~= 1)
        elseif data.PassType == 1 then
            --只买普通
            return (data.RewardInfo[implv] ~= 3)
        elseif data.PassType == 2 then
            --只买高级
            return (data.RewardInfo[implv] ~= 5)
        elseif data.PassType == 3 then    
            --普通,高级都买了
            return (data.RewardInfo[implv] ~= 7)
        end
    else
        return true
    end
    return false
end

function LuaHanJia2023PassMainRewardTab:RefreshImpItems()--ok
    local implv = self.CurImpLv
    local data = LuaHanJia2023Mgr.XKLData
    local nmlitems,advitems,advitems2 = self:GetLvItems(implv)

    local cell1 = self.ItemCell1.transform
    local cell2 = self.ItemCell2.transform
    local cell3 = self.ItemCell3.transform

    local haveimpreward = self:HaveImpReward(implv, data)
    if haveimpreward then
        self.NextLabel.text = SafeStringFormat3(LocalString.GetString("%d级可领"),implv)
    else
        self.NextLabel.text = LocalString.GetString("已领取")
    end
    
    if nmlitems == nil or implv > data.CfgMaxLv then
        self:FillItemCell(cell1,nil, implv, 1,false)
    else
        self:FillItemCell(cell1, nmlitems[1], implv, 1,false)
    end
    
    self:FillItemCell(cell2, advitems and advitems[1] or nil, implv, 2, false)
    self:FillItemCell(cell3, advitems2 and advitems2[1] or nil, implv, 3, false)
end

function LuaHanJia2023PassMainRewardTab:OnEnable()--ok
    if self.Isdirty then
        self:OnXianKeLaiDataChange()
    end
end

function LuaHanJia2023PassMainRewardTab:OnDisable()--ok
    if self.m_DelayTick ~= nil then
        invoke(self.m_DelayTick)
    end
end

function LuaHanJia2023PassMainRewardTab:OnXianKeLaiDataChange()
    if not self.gameObject.activeSelf then
        self.Isdirty = true
        return 
    end
    
    self.Isdirty = false
    local data = LuaHanJia2023Mgr.XKLData

    local lv = data.Lv
    local lvmax = data.CfgMaxLv
    
    local passtype = data.PassType --0 免费通行证, 1 高级通行证, 2 高级通行证豪华版
    CUICommonDef.SetGrey(self.AdvPassImg2,passtype ~=1 and passtype ~=3)
    CUICommonDef.SetGrey(self.AdvPassImg3,passtype ~=2 and passtype ~=3)
    local lockgo2 = LuaGameObject.GetChildNoGC(self.Sprite2.transform,"LockedSprite").gameObject
    local lockgo3 = LuaGameObject.GetChildNoGC(self.Sprite3.transform,"LockedSprite").gameObject
    lockgo2:SetActive(passtype ~= 1 and passtype ~= 3)
    lockgo3:SetActive(passtype ~= 2 and passtype ~= 3)

    local setting = HanJia2023_XianKeLaiSetting.GetData()
    self.TimeLb.text = LocalString.GetString("活动时间：") .. setting.BattlePassTime

    local initfunc = function(item,index)
        self:FillContentItem(item,index)
    end
    self.QnTableView1.m_DataSource = DefaultTableViewDataSource.CreateByCount(lvmax,initfunc)
    self.QnTableView1:ReloadData(true, false)
    self.QnTableView1.m_ScrollView:InvalidateBounds()

    local adjustTableview = true
    if self.firstOpenWnd then
        --打开面板时,期望滚动到1.可领奖的最低级, 2.未解锁的最低级 3.停留在最高级,(有重置次数的时候)重置增加光效?
        local minRewardLv = LuaHanJia2023Mgr:CheckMinRewardLv(1, LuaHanJia2023Mgr.XKLData.Lv)
        if minRewardLv == -1 then
            --滚动到当前级, 
            if lv == lvmax then
                --这个时候期望展现出重置按钮上的光效?
                --doNothing
            end
        else
            lv = minRewardLv
        end

        self.firstOpenWnd = false
    else
        --后续数据更新时, 当前领奖的?前三级和后三级判断有没有领奖, 有领奖就不滚动 2.待领奖的最低级 3.未解锁的最低级 4.停留在最高级,(有重置次数的时候)重置增加光效?
        if (self.requestItemLevel and LuaHanJia2023Mgr:CheckHaveRewardRange(self.requestItemLevel, 3)) or false then
            lv = self.requestItemLevel
        elseif LuaHanJia2023Mgr:CheckHaveReward() then
            --还有地方可以领奖
            lv = LuaHanJia2023Mgr:CheckMinRewardLv(1, LuaHanJia2023Mgr.XKLData.Lv)
        else
            --没有奖可领取, 滚动到当前级
            if lv == lvmax then
                --这个时候期望展现出重置按钮上的光效?
                --doNothing
            end
        end


    end

    if self.m_DelayTick ~= nil then
        invoke(self.m_DelayTick)
    end
    self.m_DelayTick = CTickMgr.RegisterUpdate(DelegateFactory.Action_uint(function (delta)
        self.QnTableView1:ScrollToRow(lv)
    end), ETickType.Once)
    
    self:RefreshImpItems()
    
    self:RefreshBear()
end

function LuaHanJia2023PassMainRewardTab:FillContentItem(item,index)--ok
    local lv = index + 1

    local curlv = LuaHanJia2023Mgr.XKLData.Lv

    local lvlbstr = "Lv1/Label"
    if lv > curlv then 
        lvlbstr = "Lv2/Label"
    end
    local lvlb = item.transform:Find(lvlbstr):GetComponent(typeof(UILabel))
    lvlb.text = SafeStringFormat3(LocalString.GetString("%d级"), lv)
    
    local selectgo = LuaGameObject.GetChildNoGC(item.transform,"SelectImg").gameObject
    selectgo:SetActive(curlv == lv)

    local lvbg1 = LuaGameObject.GetChildNoGC(item.transform,"Lv1").gameObject
    local lvbg2 = LuaGameObject.GetChildNoGC(item.transform,"Lv2").gameObject
    lvbg1:SetActive(lv <= curlv)
    lvbg2:SetActive(lv > curlv)
    
    local nmlitems,advitems,advitems2 = self:GetLvItems(lv)

    self:FillItems(item,nmlitems,lv,1)
    self:FillItems(item,advitems,lv,2)
    self:FillItems(item,advitems2,lv,3)
end

function LuaHanJia2023PassMainRewardTab:FillItems(item,datas,lv,index)--ok
    local cell = item.transform:Find("Item"..index.."/ItemCell")
    if datas == nil then 
        self:FillItemCell(cell,nil,lv,index,true)
    else
        self:FillItemCell(cell,datas[1],lv,index,true)
    end    
end

function LuaHanJia2023PassMainRewardTab:FillItemCell(cell,data,lv,index,inscroll)--ok
    --index range: [1,3]
    if data == nil then 
        cell.gameObject:SetActive(false)
    else
        local icon = LuaGameObject.GetChildNoGC(cell,"IconTexture").cTexture
        local bindgo = LuaGameObject.GetChildNoGC(cell,"BindSprite").gameObject
        local countlb = LuaGameObject.GetChildNoGC(cell,"AmountLabel").label
        local checkgo = LuaGameObject.GetChildNoGC(cell,"ClearupCheckbox").gameObject
        local lockgo = LuaGameObject.GetChildNoGC(cell,"LockedSprite").gameObject
        local fx = LuaGameObject.GetChildNoGC(cell,"Fx").uiFx

        local itemid = data.ItemID
        local count = data.Count
        local isBind = data.IsBind
        local curlv = LuaHanJia2023Mgr.XKLData.Lv
        local rewardInfo = LuaHanJia2023Mgr.XKLData.RewardInfo[lv]
        local passType = LuaHanJia2023Mgr.XKLData.PassType
        local showVip1FakeLock = not COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.HanJia2023BattlePassUnlockVip1Guide)
        local showVip2FakeLock = not COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.HanJia2023BattlePassUnlockVip2Guide)

        local itemcfg = Item_Item.GetData(itemid)

        if itemcfg then
            icon:LoadMaterial(itemcfg.Icon)
        end

        bindgo:SetActive(tonumber(isBind) == 1)
        
        --index类lv级通行证是否已领取
        local showCheck = true
        local showLock = true
        local canreward = true
        if curlv >= lv then
            --等级上已经到达
            if index == 1 then
                --普通项, 必解锁
                if rewardInfo then
                    showCheck = rewardInfo % 2 == 1
                    showLock = false
                    canreward = not (rewardInfo % 2 == 1)
                else
                    showCheck = false
                    showLock = false
                    canreward = true
                end
            elseif index == 2 then
                --通行证1, 检查有没有买
                if passType ~= 1 and passType ~= 3 then
                    --没买
                    showCheck = false
                    showLock = true
                    canreward = false
                else    
                    --买了
                    if rewardInfo then
                        showCheck = (rewardInfo % 4 > 1)
                        showLock = false
                        canreward = not (rewardInfo % 4 > 1)
                    else
                        showCheck = false
                        showLock = false
                        canreward = true
                    end
                end
            elseif index == 3 then
                --通行证2, 检查有没有买
                if passType ~= 2 and passType ~= 3 then
                    --没买
                    showCheck = false
                    showLock = true
                    canreward = false
                else
                    --买了
                    if rewardInfo then
                        showCheck = rewardInfo >= 4
                        showLock = false
                        canreward = not (rewardInfo >= 4)
                    else
                        showCheck = false
                        showLock = false
                        canreward = true
                    end
                end
            end
        else
            --等级上没有到达
            showCheck = false
            showLock = true
            canreward = false
        end

        --究极后处理, 当判断需要播动效的时候, 先把原状态设置为lock
        if (index == 2 and showVip1FakeLock) or (index == 3 and showVip2FakeLock) then
            showCheck = false
            showLock = true
            canreward = false            
        end
        
        checkgo:SetActive(showCheck)
        lockgo:SetActive(showLock)
        if inscroll then
            fx.ScrollView = self.QnTableView1.m_ScrollView
        end

        if canreward then 
            local b = NGUIMath.CalculateRelativeWidgetBounds(cell)
            local waypoints = CUICommonDef.GetWayPoints(Vector4(b.min.x, b.max.y, b.size.x, b.size.y), 1, false)
            fx.transform.localPosition = waypoints[0]
            fx:LoadFx("fx/ui/prefab/UI_kuang_yellow02.prefab")
            LuaTweenUtils.DOLuaLocalPath(fx.transform, waypoints, 2, PathType.Linear, PathMode.Ignore, 2, - 1, Ease.Linear)
        else
            fx:DestroyFx()
        end
        
        UIEventListener.Get(cell.gameObject).onClick = DelegateFactory.VoidDelegate(function(go)
            if canreward then
                self.requestItemLevel = lv
                Gac2Gas.RequestGetXianKeLaiReward(lv, index)
            else
                CItemInfoMgr.ShowLinkItemTemplateInfo(itemid, false, nil, AlignType.Default, 0, 0, 0, 0) 
            end
            if self.SelectedItem ~= go then
                if self.SelectedItem then
                    self:SelectItem(self.SelectedItem,false)
                end
                self.SelectedItem = go
                self:SelectItem(self.SelectedItem,true)
            end
        end)

        if tonumber(count) <= 1 then
            countlb.text = ""
        else
            countlb.text = count
        end

        cell.gameObject:SetActive(true)
    end
end

function LuaHanJia2023PassMainRewardTab:SelectItem(itemgo,selected)--ok
    local selectgo = LuaGameObject.GetChildNoGC(itemgo.transform,"SelectedSprite").gameObject
    selectgo:SetActive(selected)
end

function LuaHanJia2023PassMainRewardTab:GetLvItems(lv)--ok
    local data = LuaHanJia2023Mgr.XKLData
    local curcfg = data.Cfg[lv]
    if curcfg == nil then 
        return nil,nil
    else
        return curcfg.NmlItems,curcfg.AdvItems,curcfg.UltraItems
    end
end

--@region UIEvent

function LuaHanJia2023PassMainRewardTab:OnOneKeyBtnClick()
    local havereward = LuaHanJia2023Mgr:CheckHaveReward()
    if havereward then
        self.requestItemLevel = nil
        Gac2Gas.RequestGetXianKeLaiReward(0, 0)
    else
        g_MessageMgr:ShowMessage("XIAN_KE_LAI_NO_REWARD_RECEIVE")
    end
end

function LuaHanJia2023PassMainRewardTab:OnShopBtnClick()
    local setting = HanJia2023_XianKeLaiSetting.GetData()
    local shopid = setting.ShopID
    CLuaNPCShopInfoMgr.ShowScoreShopById(shopid)
end

function LuaHanJia2023PassMainRewardTab:OnShowUIPreDraw(args)
    local show, name = args[0], args[1]
    if name == "HanJia2023PassDetailWnd" and show == false and CUIManager.instance then
        self:TryPlayUnlockVipEffect()
    end
end

function LuaHanJia2023PassMainRewardTab:TryPlayUnlockVipEffect()
    local passtype = LuaHanJia2023Mgr.XKLData.PassType
    self.transform:Find("Line2/vfx").gameObject:SetActive(false)
    self.transform:Find("Line3/vfx").gameObject:SetActive(false)

    local showVip1FakeLock = not COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.HanJia2023BattlePassUnlockVip1Guide)
    local showVip2FakeLock = not COpenEntryMgr.Inst:GetGuideRecord(EnumGuideKey.HanJia2023BattlePassUnlockVip2Guide)
    if showVip1FakeLock and (passtype == 1 or passtype == 3) then
        --做一个动效
        if self.showVip1UnlockAnimation then

        else
            self.showVip1Delay = RegisterTickOnce(function()
                self.transform:Find("Line2/vfx").gameObject:SetActive(true)
            end, 200)

            self.showVip1UnlockAnimation = RegisterTickOnce(function ()
                COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.HanJia2023BattlePassUnlockVip1Guide)
                self.transform:Find("Line2/vfx").gameObject:SetActive(false)
                self.QnTableView1:ReloadData()
            end, 2000 + 200)
        end
    end

    if showVip2FakeLock and (passtype == 2 or passtype == 3) then
        if self.showVip2UnlockAnimation then

        else
            self.showVip2Delay = RegisterTickOnce(function()
                self.transform:Find("Line3/vfx").gameObject:SetActive(true)
            end, 200)
            self.showVip2UnlockAnimation = RegisterTickOnce(function ()
                COpenEntryMgr.Inst:SetGuideRecord(EnumGuideKey.HanJia2023BattlePassUnlockVip2Guide)
                self.transform:Find("Line3/vfx").gameObject:SetActive(false)
                self.QnTableView1:ReloadData()
            end, 2000 + 200)
        end
    end
end

--@endregion
