
local Application = import "UnityEngine.Application"
local NativeTools = import "L10.Engine.NativeTools"
local Object = import "System.Object"
local Main = import "L10.Engine.Main"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"

PreventPluginMgr = {}

PreventPluginMgr.m_ReportHistory = {}
PreventPluginMgr.m_ReportTimeThreshold = 10 * 60

function PreventPluginMgr:CheckActivate(actionName, argsTbl)
    if CommonDefs.IS_CN_CLIENT then
        if Application.platform == RuntimePlatform.WindowsPlayer or Application.platform == RuntimePlatform.WindowsEditor then
            if Main.Inst.FocusStatus then return end

            local playerId = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Id or nil
            if playerId == nil then return end
            local curTimestamp = CServerTimeMgr.Inst.timeStamp
            if not self.m_ReportHistory[playerId] then
                self.m_ReportHistory[playerId] = {}
            end
            if not self.m_ReportHistory[playerId][actionName] then
                self.m_ReportHistory[playerId][actionName] = {time = 0}
            end
            if self.m_ReportHistory[playerId][actionName].time + self.m_ReportTimeThreshold > curTimestamp then
                return --指定时间段内相同ID相同action名只记录一次
            end

            self.m_ReportHistory[playerId][actionName].time = curTimestamp

            local foregroundWndName = NativeTools.GetForegroundWndName()
            local backgroundWndName = NativeTools.GetNonForegroundWndNames()

            local objectList = CreateFromClass(MakeGenericClass(List, Object))
            for k,v in pairs(argsTbl) do
                CommonDefs.ListAdd_LuaCall(objectList,v)
            end
            local argsTblUd = MsgPackImpl.pack(objectList)
            Gac2Gas.SendNoneForegroundWndInfo(actionName, argsTblUd, foregroundWndName, backgroundWndName)
        end
    end
end