local UIGrid = import "UIGrid"
local CCommonLuaWnd = import "L10.UI.CCommonLuaWnd"
local CUITexture = import "L10.UI.CUITexture"
local Item_Item = import "L10.Game.Item_Item"
local CItemMgr = import "L10.Game.CItemMgr"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local IdPartition = import "L10.Game.IdPartition"
local EnumQualityType = import "L10.Game.EnumQualityType"
local DateTime=import "System.DateTime"
local DateTimeKind = import "System.DateTimeKind"
local CServerTimeMgr = import "L10.Game.CServerTimeMgr"
local RegexOptions = import "System.Text.RegularExpressions.RegexOptions"
local Regex = import "System.Text.RegularExpressions.Regex"
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"

LuaCommonGetRewardWnd = class()
LuaCommonGetRewardWnd.m_materialName = "gongxihuode"
LuaCommonGetRewardWnd.m_Reward1List = {{ItemID=21006495, Count=1}, {ItemID=21006495, Count=1}}
LuaCommonGetRewardWnd.m_Reward2Label = ""
LuaCommonGetRewardWnd.m_Reward2List = {}
LuaCommonGetRewardWnd.m_hint = ""
LuaCommonGetRewardWnd.m_button = {
    {spriteName="blue", buttonLabel = LocalString.GetString("测试1"), clickCB = function() print("click blue") end},
    {spriteName="yellow", buttonLabel = LocalString.GetString("测试2"), clickCB = function() print("click yellow") end}
}   
LuaCommonGetRewardWnd.m_hideCloseBtn = false


RegistChildComponent(LuaCommonGetRewardWnd, "RewardGrid", UIGrid)
RegistChildComponent(LuaCommonGetRewardWnd, "RewardItem", GameObject)
RegistChildComponent(LuaCommonGetRewardWnd, "HintLabel", UILabel)
RegistChildComponent(LuaCommonGetRewardWnd, "Mat", CUITexture)
RegistChildComponent(LuaCommonGetRewardWnd, "Reward2Label", UILabel)
RegistChildComponent(LuaCommonGetRewardWnd, "Reward2Grid", UIGrid)
RegistChildComponent(LuaCommonGetRewardWnd, "ButtonGrid", UIGrid)
RegistChildComponent(LuaCommonGetRewardWnd, "ButtonItem", GameObject)
RegistChildComponent(LuaCommonGetRewardWnd, "TextureBg", UITexture)
RegistChildComponent(LuaCommonGetRewardWnd, "CenterLable", GameObject)
RegistChildComponent(LuaCommonGetRewardWnd, "grid2Root", GameObject)
RegistChildComponent(LuaCommonGetRewardWnd, "grid1Root", GameObject)
RegistChildComponent(LuaCommonGetRewardWnd, "Reward", UIGrid)


RegistClassMember(LuaCommonGetRewardWnd, "m_Wnd")

function LuaCommonGetRewardWnd:Init()
    self.m_Wnd = self.gameObject:GetComponent(typeof(CCommonLuaWnd))
    
    self.transform:Find("CloseButton").gameObject:SetActive(not LuaCommonGetRewardWnd.m_hideCloseBtn)

    --DEFINE WND CONST
    self.aboveItemScorllItemLimit = 11
    self.belowItemScale = 0.8
    self.belowItemScrollItemLimit = 14
    self.blueButtonTextColor = "0e3254"
    self.yellowButtonTextColor = "351b01"
    self.oneLineHeight = 380
    self.threeLineHeight = 500
    
    --m_Reward1List
    local tbl = {}
    local realIdTbl = {}
    for i = 1, #LuaCommonGetRewardWnd.m_Reward1List do
        if LuaCommonGetRewardWnd.m_Reward1List[i].RealItemId then
            table.insert(realIdTbl, LuaCommonGetRewardWnd.m_Reward1List[i])
        else
            tbl[LuaCommonGetRewardWnd.m_Reward1List[i].ItemID] = (tbl[LuaCommonGetRewardWnd.m_Reward1List[i].ItemID] and tbl[LuaCommonGetRewardWnd.m_Reward1List[i].ItemID] or 0) + LuaCommonGetRewardWnd.m_Reward1List[i].Count
        end
    end
    LuaCommonGetRewardWnd.m_Reward1List = {}
    for k, v in pairs(tbl) do
        local isEquip = IdPartition.IdIsEquip(k)
        if isEquip then
            -- 装备不叠加显示
            for i = 1, v do
                table.insert(LuaCommonGetRewardWnd.m_Reward1List, {ItemID = k, Count = 1})
            end
        else
            table.insert(LuaCommonGetRewardWnd.m_Reward1List, {ItemID = k, Count = v})
        end
    end

    -- 用来显示实际下发的物品
    for k, v in ipairs(realIdTbl) do
        table.insert(LuaCommonGetRewardWnd.m_Reward1List, v)
    end

    --m_Reward2List

    tbl = {}
    for i = 1, #LuaCommonGetRewardWnd.m_Reward2List do
        tbl[LuaCommonGetRewardWnd.m_Reward2List[i].ItemID] = (tbl[LuaCommonGetRewardWnd.m_Reward2List[i].ItemID] and tbl[LuaCommonGetRewardWnd.m_Reward2List[i].ItemID] or 0) + LuaCommonGetRewardWnd.m_Reward2List[i].Count
    end
    LuaCommonGetRewardWnd.m_Reward2List = {}
    for k, v in pairs(tbl) do
        local isEquip = IdPartition.IdIsEquip(k)
        if isEquip then
            -- 装备不叠加显示
            for i = 1, v do
                table.insert(LuaCommonGetRewardWnd.m_Reward2List, {ItemID = k, Count = 1})
            end
        else
            table.insert(LuaCommonGetRewardWnd.m_Reward2List, {ItemID = k, Count = v})
        end
    end
    
    self:UpdateRewards()
    self:UpdateButtons()
    self:UpdateOthers()
    self.Reward:Reposition()
end

function LuaCommonGetRewardWnd:OnDestroy()
    LuaCommonGetRewardWnd.m_hideCloseBtn = false
end

function LuaCommonGetRewardWnd:UpdateRewards()
    self.RewardItem:SetActive(false)
    CUICommonDef.ClearTransform(self.RewardGrid.transform)
    CUICommonDef.ClearTransform(self.Reward2Grid.transform)
    
    for i = 1, #LuaCommonGetRewardWnd.m_Reward1List do
        local go = NGUITools.AddChild(self.RewardGrid.gameObject, self.RewardItem)
        self:InitRewardItem(go, LuaCommonGetRewardWnd.m_Reward1List[i])
        go:SetActive(true)
    end
    self.RewardGrid.transform:GetComponent(typeof(UIScrollView)).enabled = (#LuaCommonGetRewardWnd.m_Reward1List >= self.aboveItemScorllItemLimit)
    self.RewardGrid:Reposition()

    if #LuaCommonGetRewardWnd.m_Reward2List > 0 then
        --self.TextureBg
        self.TextureBg.height = self.threeLineHeight
    else    
        self.TextureBg.height = self.oneLineHeight
    end
    
    for i = 1, #LuaCommonGetRewardWnd.m_Reward2List do
        local go = NGUITools.AddChild(self.Reward2Grid.gameObject, self.RewardItem)
        go.transform.localScale = Vector3(self.belowItemScale, self.belowItemScale, self.belowItemScale)
        self:InitRewardItem(go, LuaCommonGetRewardWnd.m_Reward2List[i])
        go:SetActive(true)
    end
    self.Reward2Grid.transform:GetComponent(typeof(UIScrollView)).enabled = (#LuaCommonGetRewardWnd.m_Reward2List >= self.belowItemScrollItemLimit)
    self.Reward2Grid:Reposition()
end

function LuaCommonGetRewardWnd:InitRewardItem(item, info)
    if not item or not info then return end

    local iconTexture = item.transform:Find("IconTexture"):GetComponent(typeof(CUITexture))
    iconTexture:Clear()

    local qualitySprite = item.transform:Find("QualitySprite"):GetComponent(typeof(UISprite))
    qualitySprite.spriteName = nil

    local amountLabel = item.transform:Find("AmountLabel"):GetComponent(typeof(UILabel))
    amountLabel.text = nil

    local isEquip = IdPartition.IdIsEquip(info.ItemID)

    local template = nil
    if not isEquip then
        template = Item_Item.GetData(info.ItemID)
    else
        template = EquipmentTemplate_Equip.GetData(info.ItemID)
    end

    if not template then return end

    local icon = template.Icon
    if (not isEquip) and self:CheckPeriod(template) then
        icon = "UI/Texture/Item_Other/Material/other_444.mat"
    end

    iconTexture:LoadMaterial(icon)

    -- 一件装备不显示数量
    if isEquip and info.Count == 1 then
        amountLabel.text = ""
    else
        amountLabel.text = tostring(info.Count)
    end

    if isEquip then
        qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(CommonDefs.ConvertIntToEnum(typeof(EnumQualityType), template.Color))
    else
        qualitySprite.spriteName = CUICommonDef.GetItemCellBorder(template.NameColor)
    end

    CommonDefs.AddOnClickListener(item, DelegateFactory.Action_GameObject(function (go)
        if info.RealItemId then
            local commonItem = CItemMgr.Inst:GetById(info.RealItemId)
            if commonItem then
                CItemInfoMgr.ShowLinkItemInfo(commonItem, true, nil, AlignType.Default, 0, 0, 0, 0, 0)
                return
            end
        end
        CItemInfoMgr.ShowLinkItemTemplateInfo(info.ItemID, false, nil, CItemInfoMgr.AlignType.Default, 0, 0, 0, 0)
    end), false)
end

function LuaCommonGetRewardWnd:UpdateButtons()
    self.ButtonItem:SetActive(false)
    CUICommonDef.ClearTransform(self.ButtonGrid.transform)
    for i = 1, #LuaCommonGetRewardWnd.m_button do
        local go = NGUITools.AddChild(self.ButtonGrid.gameObject, self.ButtonItem)
        local buttonInfo = LuaCommonGetRewardWnd.m_button[i]
        go.transform:GetComponent(typeof(UISprite)).spriteName = self:ConvertButtonSpritePath(buttonInfo.spriteName)
        go.transform:Find("Label"):GetComponent(typeof(UILabel)).color = self:ConvertTextColor(buttonInfo.spriteName)
        go.transform:Find("Label"):GetComponent(typeof(UILabel)).text = buttonInfo.buttonLabel
        UIEventListener.Get(go).onClick = DelegateFactory.VoidDelegate(function (_)
            if buttonInfo.clickCB then
                buttonInfo.clickCB()
            end
        end)
        go:SetActive(true)
    end
    self.ButtonGrid:Reposition()
end

function LuaCommonGetRewardWnd:UpdateOthers()
    self.Mat:LoadMaterial(self:ConvertMatPath(LuaCommonGetRewardWnd.m_materialName))
    self.Mat.transform:Find("Mat (1)"):GetComponent(typeof(CUITexture)):LoadMaterial(self:ConvertMatPath(LuaCommonGetRewardWnd.m_materialName))

    self.grid1Root:SetActive(#LuaCommonGetRewardWnd.m_Reward1List ~= 0)
    if LuaCommonGetRewardWnd.m_Reward2Label and LuaCommonGetRewardWnd.m_Reward2Label ~= "" then
        self.Reward2Label.text = LuaCommonGetRewardWnd.m_Reward2Label
        self.CenterLable:SetActive(true)
    else
        self.CenterLable:SetActive(false)
    end
    self.grid2Root:SetActive(#LuaCommonGetRewardWnd.m_Reward2List ~= 0)

    if LuaCommonGetRewardWnd.m_hint and LuaCommonGetRewardWnd.m_hint ~= "" then
        self.HintLabel.text = LuaCommonGetRewardWnd.m_hint
        self.HintLabel.gameObject:SetActive(true)
    else
        self.HintLabel.gameObject:SetActive(false)
    end
    self.ButtonGrid.gameObject:SetActive(#LuaCommonGetRewardWnd.m_button ~= 0)
end

function LuaCommonGetRewardWnd:ConvertMatPath(matSimpleName)
    return "UI/Texture/Transparent/Material/common_" .. matSimpleName .. ".mat"
end

function LuaCommonGetRewardWnd:ConvertButtonSpritePath(spriteSimpleName)
    return "common_btn_01_" .. spriteSimpleName
end

function LuaCommonGetRewardWnd:ConvertTextColor(spriteSimpleName)
    if spriteSimpleName == "yellow" then
        return NGUIText.ParseColor24(self.yellowButtonTextColor, 0)
    elseif spriteSimpleName == "blue" then
        return NGUIText.ParseColor24(self.blueButtonTextColor, 0)
    else    
        return NGUIText.ParseColor24("FFFFFF", 0)
    end
end 

function LuaCommonGetRewardWnd:CheckPeriod(itemTemplate)
    --客户端仅根据Period字段来确定显示方式，具体显示值需要读取CItem.ExpiredTime
    if not System.String.IsNullOrEmpty(itemTemplate.Period) then
        local _, _, year, month, day, hour, minute = string.find(itemTemplate.Period, "(%d+)-(%d+)-(%d+)-(%d+)-(%d+)")
        if year and month and day and hour and minute then
            local expiredTime = CreateFromClass(DateTime, year, month, day, hour, minute, 0, DateTimeKind.Utc)
            if not CommonDefs.op_GreaterThan_DateTime_DateTime(expiredTime, CServerTimeMgr.Inst:GetZone8Time()) then
                return true
            end
        end
    end
    return false
end