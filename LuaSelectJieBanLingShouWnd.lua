local CUITexture=import "L10.UI.CUITexture"
local LingShou_LingShou=import "L10.Game.LingShou_LingShou"
local CLingShouMgr=import "L10.Game.CLingShouMgr"
local UIGrid=import "UIGrid"
local UIToggle=import "UIToggle"

CLuaSelectJieBanLingShouWnd = class()
RegistClassMember(CLuaSelectJieBanLingShouWnd, "m_ItemTemplate")
RegistClassMember(CLuaSelectJieBanLingShouWnd, "m_SelectedLingShouId")

-- RequestBindPartner_LingShou

function CLuaSelectJieBanLingShouWnd:Init()
    self.m_SelectedLingShouId=nil
    self.m_ItemTemplate=FindChild(self.transform,"ItemTemplate").gameObject
    self.m_ItemTemplate:SetActive(false)
    CLingShouMgr.Inst:RequestLingShouList()

    UIEventListener.Get(FindChild(self.transform,"Button").gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
        if self.m_SelectedLingShouId then
            Gac2Gas.RequestBindPartner_LingShou(self.m_SelectedLingShouId)
            CUIManager.CloseUI(CLuaUIResources.SelectJieBanLingShouWnd)
        end
    end)
end

function CLuaSelectJieBanLingShouWnd:OnEnable()
    g_ScriptEvent:AddListener("GetAllLingShouOverview", self, "GetAllLingShouOverview")
end

function CLuaSelectJieBanLingShouWnd:OnDisable()
    g_ScriptEvent:RemoveListener("GetAllLingShouOverview", self, "GetAllLingShouOverview")
end

function CLuaSelectJieBanLingShouWnd:GetAllLingShouOverview( )
    local list = CLingShouMgr.Inst:GetLingShouOverviewList()
    --初始化列表
    -- self.lingShouTable:ReloadData(false, false)
    local grid = FindChild(self.transform,"Grid").gameObject
    for i=1,list.Count do
        local go = NGUITools.AddChild(grid,self.m_ItemTemplate)
        go:SetActive(true)
        self:InitItem(go.transform,list[i-1])
    end
    grid:GetComponent(typeof(UIGrid)):Reposition()
end

function CLuaSelectJieBanLingShouWnd:InitItem(transform,overviewData)
    local icon = transform:Find("Icon"):GetComponent(typeof(CUITexture))
    local nameLabel = transform:Find("NameLabel"):GetComponent(typeof(UILabel))
    local descLabel = transform:Find("DescLabel"):GetComponent(typeof(UILabel))

    local templateId = overviewData.templateId
    local data = LingShou_LingShou.GetData(templateId)
    if data then
        icon:LoadNPCPortrait(data.Portrait)
    end
    nameLabel.text = overviewData.name

    descLabel.text=CLuaLingShouMgr.GetEvolveGradeDesc(overviewData.evolveGrade)

    local current = transform:Find("Current").gameObject
    current:SetActive(false)

    local id = CClientMainPlayer.Inst and CClientMainPlayer.Inst.ItemProp.BindedPartenerLingShouId or nil
    local currentId = overviewData.id
    if id==currentId then
        current:SetActive(true)
    end
    UIEventListener.Get(transform.gameObject).onClick=DelegateFactory.VoidDelegate(function(go)
        go:GetComponent(typeof(UIToggle)).value=true
        self.m_SelectedLingShouId=currentId
    end)
end