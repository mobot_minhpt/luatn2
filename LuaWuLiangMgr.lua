local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local EPlayerFightProp = import "L10.Game.EPlayerFightProp"
local CScene = import "L10.Game.CScene"
local CClientObjectMgr = import "L10.Game.CClientObjectMgr"
local CClientMonster = import "L10.Game.CClientMonster"
local CRenderScene=import "L10.Engine.Scene.CRenderScene"
local CEffectMgr = import "L10.Game.CEffectMgr"
local EnumWarnFXType =import "L10.Game.EnumWarnFXType"
local CMonsterPropertyFight = import "L10.Game.CMonsterPropertyFight"
local EMonsterFightProp = import "L10.Game.EMonsterFightProp"

LuaWuLiangMgr = {}
-- 选关界面信息
LuaWuLiangMgr.passPlayIdTbl = {}
-- 按钮模式
LuaWuLiangMgr.isEnterPrepare = true
-- 进入挑战数据
LuaWuLiangMgr.enterData = {}
-- 近期通关界面 选中关卡
LuaWuLiangMgr.passedTeamInfoLevelId = 0
-- 结算界面数据
LuaWuLiangMgr.resultData = nil
-- 伙伴信息
LuaWuLiangMgr.huobanTipData = nil
-- 伙伴界面的数据
LuaWuLiangMgr.huobanData = nil
LuaWuLiangMgr.huobanSelectId = 0
-- 战斗时同步的伙伴数据
LuaWuLiangMgr.huobanSyncData = nil


LuaWuLiangMgr.seasonRankType = 2100

-- 打开选关界面
function LuaWuLiangMgr:OpenLevelWnd(playIdTbl)
	self.passPlayIdTbl = playIdTbl
	CUIManager.ShowUI(CLuaUIResources.WuLiangLevelWnd)
end

-- 打开历史通关界面
function LuaWuLiangMgr:OpenPassedTeamInfoWnd(levelId)
	self.passedTeamInfoLevelId = levelId
	CUIManager.ShowUI(CLuaUIResources.WuLiangPassedTeamInfoWnd)
end

-- 打开通关界面
function LuaWuLiangMgr:OpenResultWnd(data)
	-- list {EngineId, Heal, Dps, Hp, UnderDamage, Level, Tid, InheritLevel, Attr, SkillLevelTbl}
	local huobanInfo = data["HuoBanInfo"]

	-- Level, Heal, Name, DPS, Id, Class, UnderDamage, Gender
	local playerInfo = data["PlayerInfo"]

	local playId = data["PlayId"]

	self.resultData = data
	CUIManager.ShowUI(CLuaUIResources.WuLiangResultWnd)
end

-- 显示伙伴信息
-- 注意这里面要自己塞一个monsterId
function LuaWuLiangMgr:OpenHuoBanTipWnd(data, notFeishengCheck)
	-- { MonsterId, Id, Heal, Dps, Hp, UnderDamage, Level, Tid, InheritLevel, Attr, SkillLevelTbl}
	self.huobanTipData = data
	self.notFeishengCheck = notFeishengCheck
	CUIManager.ShowUI(CLuaUIResources.WuLiangCompanionTipWnd)
end

-- 显示进入按钮
-- 进入准备场景 或者正式挑战场景
function LuaWuLiangMgr:OpenEnterBtn(isEnterPrepare)
	self.isEnterPrepare = isEnterPrepare
	CUIManager.ShowUI(CLuaUIResources.WuLiangEnterButtonWnd)
end

-- 设置准备场景的显示
function LuaWuLiangMgr:SetPrepareScene(gateData)
	local setting = XinBaiLianDong_Setting.GetData()
	-- 可挑战大门特效（顺序为苦集灭）
	local doorFxList = setting.LeiFengTaDoorFxList
	-- 前五层的集灭门的雾气遮挡特效
	local fogFx = setting.LeiFengTaFogFx
	-- 中心区域特效（常驻、踏入高亮）
	local centerFxList = setting.LeiFengTaCenterFxList
	-- 该难度未开启时开门黑漩涡
	local lockFx = setting.LeiFengTaLockFx

	if CRenderScene.Inst then
        local openNode = CRenderScene.Inst.transform:Find("Models/Trees/door_open")
        local closeNode = CRenderScene.Inst.transform:Find("Models/Trees/door_closed")

		local openList = {}
		local closeList = {}

		local closeDoorList = {3,1,2}
		local openDoorList = {2,3,1}
		for i=1,3 do
			local closeIndex = closeDoorList[i]
			local openIndex = openDoorList[i]
			table.insert(openList, openNode:GetChild(openIndex-1))
			table.insert(closeList, closeNode:GetChild(closeIndex-1))
		end

		for i=1,3 do
			closeList[i].gameObject:SetActive(true)
			openList[i].gameObject:SetActive(false)
		end

		local posList = {Vector3(87, 13.11, 62.99), Vector3(70.8, 13.11, 78.3), Vector3(71, 13.11, 47) }
		local rotateList = {270, 330, 210}

		local alreadySet = {}
		for k, data in pairs(gateData) do

			local playId = data.PlayId
			local isPass = data.IsPass
			local isUnlock = data.IsUnlock

			-- 用playId来判断对应哪个门
			local playData = XinBaiLianDong_WuLiangPlay.GetDataBySubKey("GameplayId", playId)
			local index = playData.Id%10

			alreadySet[index] = true

			-- 设置这个门的状态
			closeList[index].gameObject:SetActive(isPass)
			openList[index].gameObject:SetActive(not isPass)

			local pos = posList[index]
			local rot = rotateList[index]

			if not isUnlock then
				CEffectMgr.Inst:AddWorldPositionFX(lockFx, pos, rot,0,1,-1,EnumWarnFXType.None,nil,0,0, nil)
			elseif not isPass then
				-- 设置特效
				local fxId = doorFxList[index-1]
				CEffectMgr.Inst:AddWorldPositionFX(fxId, pos, rot,0,1,-1,EnumWarnFXType.None,nil,0,0, nil)
			end
		end

		local fogPosList = {Vector3(87, 13.11, 62.99), Vector3(70, 13.11, 76), Vector3(69, 13.11, 49) }
		local fogRotateList = {270, 330, 210}
		-- 没设置的表明是前五层的
		for i=1,3 do
			if not alreadySet[i] then
				local pos = fogPosList[i]
				local rot = fogRotateList[i]
				CEffectMgr.Inst:AddWorldPositionFX(fogFx, pos, rot,0,1,-1,EnumWarnFXType.None,nil,0,0, nil)
			end
		end

		local centerPos = Vector3(61.5, 13.6, 62.9)
		-- 设置常驻特效
		CEffectMgr.Inst:AddWorldPositionFX(centerFxList[0], centerPos, 0,0,1,-1,EnumWarnFXType.None,nil,0,0, nil)
    end
end

-- 显示属性的方法
-- 一些UI会复用这个逻辑 抽出来放在Mgr里面
function LuaWuLiangMgr:ShowAttr(root, level, huoBanId)

    local attrList = {}
	-- 气血
	table.insert(attrList, {"AdjHpFull"})
	-- 攻击
	table.insert(attrList, {"AdjpAttMin","AdjpAttMax"})
	table.insert(attrList, {"AdjmAttMin", "AdjmAttMax"})
	
	-- 防御
	table.insert(attrList, {"AdjpDef"})
	table.insert(attrList, {"AdjmDef"})
	-- 致命
	table.insert(attrList, {"AdjpFatal"})
	table.insert(attrList, {"AdjmFatal"})
	-- 眩晕/混乱
	table.insert(attrList, {"AdjEnhanceDizzy"})
	table.insert(attrList, {"AdjEnhanceChaos"})

	local levelData = XinBaiLianDong_WuLiangInherit.GetData(level)
	local huobanData = XinBaiLianDong_WuLiangHuoBan.GetData(huoBanId)
	-- 加成比例
	local effect = levelData.PropertyCoefficient + huobanData.PropertyCoefficient

	local playerLv = CClientMainPlayer.Inst and CClientMainPlayer.Inst.Level or 1

	local huobanClient = nil
	CClientObjectMgr.Inst:ForeachObject(DelegateFactory.Action_CClientObject(function(obj)
		if TypeIs(obj, typeof(CClientMonster)) then
			if obj and obj.TemplateId == huoBanId then
				huobanClient = obj
			end
		end
	end))

	for i=1, #attrList do
		local label = root.transform:Find(tostring(i)):GetComponent(typeof(UILabel))

		local attrNameList = attrList[i]
		local attrValueList = {}

		-- 遍历每个属性名字
		-- 可能有两个
		for i, name in ipairs(attrNameList) do
			local propData = XinBaiLianDong_WuLiangBasicProp.GetData(name)
			if propData then
				local propNameList = propData.OwnerPropName
				local max = 0

				if huobanClient and name == "AdjHpFull" and huobanClient.HpFull > 0 then
					max = huobanClient.HpFull
				else
					-- 多个值取最大的
					for i=0, propNameList.Length-1 do
						local prop = propNameList[i]
						local val = CClientMainPlayer.Inst.FightProp:GetParam(EPlayerFightProp[prop])
						val = val * effect
						if val > max then
							max = val
						end
					end

					-- 基础属性用公式计算
					if propData.BasicPropFormulaId >0 then
						max = max + GetFormula(propData.BasicPropFormulaId)(nil, nil, {huoBanId, playerLv})
					end
				end
				
				max = math.floor(max)
				table.insert(attrValueList, max)
			end
			
		end

		if #attrValueList == 1 then
			label.text = tostring(attrValueList[1])
		else
			label.text = SafeStringFormat3("%s-%s", tostring(attrValueList[1]), tostring(attrValueList[2]))
		end
	end
end

-- 显示属性的新方法
-- 依赖服务器传值
function LuaWuLiangMgr:ShowAttrNew(root, huoBanId, huobanData)

    local attrList = {}
	-- 气血
	table.insert(attrList, {"HpFull"})
	-- 攻击
	table.insert(attrList, {"pAttMin","pAttMax"})
	table.insert(attrList, {"mAttMin", "mAttMax"})
	
	-- 防御
	table.insert(attrList, {"pDef"})
	table.insert(attrList, {"mDef"})
	-- 致命
	table.insert(attrList, {"pFatal"})
	table.insert(attrList, {"mFatal"})
	-- 命中
	table.insert(attrList, {"pHit"})
	table.insert(attrList, {"mHit"})

	for i=1, #attrList do
		local label = root.transform:Find(tostring(i)):GetComponent(typeof(UILabel))
		local attrNameList = attrList[i]

		if #attrNameList == 1 then
			label.text = tostring(math.floor(huobanData[attrNameList[1]]))
		else
			label.text = SafeStringFormat3("%s-%s", tostring(math.floor(huobanData[attrNameList[1]])), 
			tostring(math.floor(huobanData[attrNameList[2]])))
		end
	end
end

-- 显示技能的方法
-- 一些UI会复用这个逻辑 抽出来放在Mgr里面
function LuaWuLiangMgr:ShowSkill(root, monsterId, skillLevelTbl, callBack, notCheck)
	local huoBanData = XinBaiLianDong_WuLiangHuoBan.GetData(monsterId)

	for i=0, huoBanData.SkillList.Length -1 do
		local skillId = huoBanData.SkillList[i]
		local skillLevel = 1

		-- 等级加成
		if skillLevelTbl and skillLevelTbl[skillId] then
			skillLevel = skillLevelTbl[skillId] + 1
		end

		-- 设置显示 图标和等级
		local item = root.transform:Find(tostring(i+1))
        local skillIcon = item:GetComponent(typeof(CUITexture))
        local levelLabel = item:Find("LevelLabel"):GetComponent(typeof(UILabel))

		local skillData = Skill_AllSkills.GetData(skillId *100 + skillLevel)

		local checked, realLv =  LuaWuLiangMgr:CheckSkillModify(skillId, skillLevel)

		if not notCheck and checked and realLv ~= skillLevel then
			levelLabel.text =  "[ff7900]" .. realLv
		else
			levelLabel.text = realLv
		end
        
		skillIcon:LoadSkillIcon(skillData.SkillIcon)

		-- 有可能还需要设置技能名
		local nameRoot = item:Find("NameLabel")
		if nameRoot then
			local nameLabel = nameRoot:GetComponent(typeof(UILabel))
			nameLabel.text = skillData.Name
		end

		if callBack then
			UIEventListener.Get(item.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
				callBack(i+1, skillId)
			end)
		end
	end
end

function LuaWuLiangMgr:IsInWuLiangPrepareScene()
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        local data = XinBaiLianDong_WuLiangPrepare.GetDataBySubKey("GameplayId", gamePlayId)
		return data ~= nil
    end
    return false
end

function LuaWuLiangMgr:IsInWuLiangBattleScene()
    if CScene.MainScene then
        local gamePlayId = CScene.MainScene.GamePlayDesignId
        local data = XinBaiLianDong_WuLiangPlay.GetDataBySubKey("GameplayId", gamePlayId)
		return data ~= nil
    end
    return false
end

function LuaWuLiangMgr:ProcessTask(task)
	local listMap = XinBaiLianDong_Setting.GetData().LeiFengTaDailyTaskID
	
    if CommonDefs.HashSetContains(listMap, typeof(UInt32), task.TemplateId) then
		if task.CanSubmit == 1 then
			g_MessageMgr:ShowMessage("WuLiang_Task_Submit_Tip")
			return true
		elseif self:IsInWuLiangPrepareScene() then
			g_MessageMgr:ShowMessage("WuLiang_Task_FindPath_In_Gameplay_Tip")
			return true
		end
	end
	return false
end

function LuaWuLiangMgr:CheckSkillModify(skillId, level)
	local skillData = XinBaiLianDong_WuLiangSkill.GetData(skillId)

	if skillData and CClientMainPlayer.Inst and CClientMainPlayer.Inst.IsInXianShenStatus then
		local cost = skillData.Cost
		local costDataList = {XinBaiLianDong_WuLiangSkillCost_1, XinBaiLianDong_WuLiangSkillCost_2, XinBaiLianDong_WuLiangSkillCost_3, 
			XinBaiLianDong_WuLiangSkillCost_4, XinBaiLianDong_WuLiangSkillCost_5}
		
		local costData = costDataList[cost]
		local maxLevel = 1
		local playerLv =  CClientMainPlayer.Inst.Level

		costData.Foreach(function(lv, data)
			if playerLv >= data.NeedPlayerLevel and lv+1 > maxLevel then
				maxLevel = lv+1
			end
		end)

		if maxLevel > level then
			maxLevel = level
		end

		return true, maxLevel
	end

	return false, level
end
