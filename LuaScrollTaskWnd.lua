local UIWidget = import "UIWidget"
local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UITexture = import "UITexture"
local LuaTweenUtils = import "LuaTweenUtils"
local Ease = import "DG.Tweening.Ease"
local CChatLinkMgr = import "CChatLinkMgr"
local NGUITools = import "NGUITools"
local Vector4 = import "UnityEngine.Vector4"
local SoundManager = import "SoundManager"
local CUITexture = import "L10.UI.CUITexture"
local CImageSpreadEffectHelper = import "CImageSpreadEffectHelper"
local CLoadingWnd = import "L10.UI.CLoadingWnd"
local CMiddleNoticeMgr = import "L10.UI.CMiddleNoticeMgr"
local Type = import "UIBasicSprite+Type"
local Rect = import "UnityEngine.Rect"
local CMainCamera = import "L10.Engine.CMainCamera"

LuaScrollTaskWnd = class()

--@region RegistChildComponent: Dont Modify Manually!
RegistChildComponent(LuaScrollTaskWnd, "m_Formats", "Formats", GameObject)
RegistChildComponent(LuaScrollTaskWnd, "m_Loading", "Loading", GameObject)
RegistChildComponent(LuaScrollTaskWnd, "m_Bg", "Bg", UITexture)
RegistChildComponent(LuaScrollTaskWnd, "m_LeftBorder", "LeftBorder", GameObject)
RegistChildComponent(LuaScrollTaskWnd, "m_RightBorder", "RightBorder", GameObject)
RegistChildComponent(LuaScrollTaskWnd, "m_CloseButton", "CloseButton", GameObject)
RegistChildComponent(LuaScrollTaskWnd, "m_Content", "Content", UIWidget)

--@endregion RegistChildComponent end
RegistClassMember(LuaScrollTaskWnd, "m_Panel")

RegistClassMember(LuaScrollTaskWnd, "m_OpenAnimTweeners")
RegistClassMember(LuaScrollTaskWnd, "m_CommonAnimTweeners")
RegistClassMember(LuaScrollTaskWnd, "m_Texture")
RegistClassMember(LuaScrollTaskWnd, "m_CUITex")
RegistClassMember(LuaScrollTaskWnd, "m_MaskPanel")
RegistClassMember(LuaScrollTaskWnd, "m_BigTex")
RegistClassMember(LuaScrollTaskWnd, "m_IsInLoadingScene")
RegistClassMember(LuaScrollTaskWnd, "m_WaitForLoadingCallback")
RegistClassMember(LuaScrollTaskWnd, "m_IsInScorllAnim")
RegistClassMember(LuaScrollTaskWnd, "m_IsPreLoad")
RegistClassMember(LuaScrollTaskWnd, "m_CurrentGird")
RegistClassMember(LuaScrollTaskWnd, "m_CurrentTex")
RegistClassMember(LuaScrollTaskWnd, "m_CurrentSubtitleId")
RegistClassMember(LuaScrollTaskWnd, "m_CurrentPictureKey")
RegistClassMember(LuaScrollTaskWnd, "m_CurrentLayout")

RegistClassMember(LuaScrollTaskWnd, "m_FxXingXing")
RegistClassMember(LuaScrollTaskWnd, "m_FxSaoGuang")
RegistClassMember(LuaScrollTaskWnd, "m_FxZhongXin")
RegistClassMember(LuaScrollTaskWnd, "m_FxLeft")
RegistClassMember(LuaScrollTaskWnd, "m_FxRight")

RegistClassMember(LuaScrollTaskWnd, "m_Games")
RegistClassMember(LuaScrollTaskWnd, "m_GamePanels")
RegistClassMember(LuaScrollTaskWnd, "m_CurrentGame")
RegistClassMember(LuaScrollTaskWnd, "m_Helper")
RegistClassMember(LuaScrollTaskWnd, "m_BorderPanel")
RegistClassMember(LuaScrollTaskWnd, "m_DecPanel")
RegistClassMember(LuaScrollTaskWnd, "m_CacheScrollInfo")

function LuaScrollTaskWnd:Awake()
    self.m_OpenAnimTweeners = {}
    self.m_CommonAnimTweeners = {}

    self.m_IsInLoadingScene = false
    --@region EventBind: Dont Modify Manually!

    --@endregion EventBind end
    self:InitComponents()
    self.m_Texture.material.mainTexture = nil
    self.m_MaskPanel:Refresh()

    self.m_GamePanels = {}
    for i = 0, self.m_Games.transform.childCount-1 do
        local panel = self.m_Games.transform:GetChild(i):GetComponent(typeof(UIPanel))
        table.insert(self.m_GamePanels, panel)
    end

    CMiddleNoticeMgr.ShowNoticeDisable = true
    LuaScrollTaskMgr:ShowOrHideHeadInfo(false)
    self.m_Bg.type = Type.Simple
end

function LuaScrollTaskWnd:InitComponents()
    self.m_Panel    = self.transform:GetComponent(typeof(UIPanel))
    self.m_MaskPanel= self.transform:Find("Anchor/MaskPanel"):GetComponent(typeof(UIPanel))
    self.m_Helper   = self.transform:Find("Anchor/MaskPanel/Texture"):GetComponent(typeof(CImageSpreadEffectHelper))
    self.m_Texture  = self.transform:Find("Anchor/MaskPanel/Texture"):GetComponent(typeof(UITexture))
    self.m_CUITex   = self.transform:Find("Anchor/MaskPanel/Texture"):GetComponent(typeof(CUITexture))
    self.m_BigTex   = self.transform:Find("Anchor/Content/Formats/Format4/Texture"):GetComponent(typeof(UITexture))

    self.m_FxXingXing   = self.transform:Find("Anchor/Bg/BorderPanel/Fx_xingxing"):GetComponent(typeof(CUIFx))
    self.m_FxSaoGuang   = self.transform:Find("Anchor/Bg/BorderPanel/Fx_saoguang"):GetComponent(typeof(CUIFx))
    --self.m_FxZhongXin   = self.transform:Find("Anchor/Bg/BorderPanel/RightBorder/Fx_zhongxin"):GetComponent(typeof(CUIFx))
    self.m_FxLeft       = self.transform:Find("Anchor/Bg/BorderPanel/LeftBorder/Fx"):GetComponent(typeof(CUIFx))
    self.m_FxRight      = self.transform:Find("Anchor/Bg/BorderPanel/RightBorder/Fx"):GetComponent(typeof(CUIFx))
    self.m_BorderPanel  = self.transform:Find("Anchor/Bg/BorderPanel"):GetComponent(typeof(UIPanel))
    self.m_DecPanel     = self.transform:Find("Anchor/Content/DecPanel"):GetComponent(typeof(UIPanel))

    self.m_Games        = self.transform:Find("Anchor/Content/Games").gameObject
end

function LuaScrollTaskWnd:Init()
    self:ChangeWndPanelDepth(CUIManager.MAX_DEPTH-3)

    self.m_FxXingXing.gameObject:SetActive(LuaScrollTaskMgr.startFromScene)
    self.m_FxSaoGuang.gameObject:SetActive(false)
    --self.m_FxZhongXin.gameObject:SetActive(false)
    self.m_FxLeft.gameObject:SetActive(true)
    self.m_FxRight.gameObject:SetActive(true)

    CommonDefs.AddOnClickListener(self.m_CloseButton.gameObject, DelegateFactory.Action_GameObject(function(go)
        self:OnCloseBtnClicked(go)
    end), false)

    if LuaScrollTaskMgr.startFromScene then
        self:CheckNextScroll()
    else
        self:Open()
    end

    self.m_CloseButton.gameObject:SetActive(true)
    if CLoadingWnd.Inst and CLoadingWnd.Inst.gameObject.activeSelf then
        self:OnStartLoadScene()
    end
end

function LuaScrollTaskWnd:OnEnable()
    g_ScriptEvent:AddListener("ScrollTaskDoNextScroll", self, "CheckNextScroll")
    g_ScriptEvent:AddListener("StartLoadScene", self, "OnStartLoadScene")
    g_ScriptEvent:AddListener("LoadingSceneFinished", self, "OnLoadingSceneFinished")
    g_ScriptEvent:AddListener("WaitForShowScrollTaskSubTitle", self, "OnWaitForShowScrollTaskSubTitle")
    g_ScriptEvent:AddListener("CloseScrollTask", self, "OnCloseScrollTask")
    g_ScriptEvent:AddListener("ScrollTaskPreLoadPictureFinished", self, "OnScrollTaskPreLoadPictureFinished")
    g_ScriptEvent:AddListener("ShowUIPreDraw", self, "ShowUIPreDraw")
    local scaler = CMainCamera.Inst.m_ResolutionScaler
    if scaler then
        scaler.IsCaptureing = true
    end
end

function LuaScrollTaskWnd:OnDisable()
    g_ScriptEvent:RemoveListener("ScrollTaskDoNextScroll", self, "CheckNextScroll")
    g_ScriptEvent:RemoveListener("StartLoadScene", self, "OnStartLoadScene")
    g_ScriptEvent:RemoveListener("LoadingSceneFinished", self, "OnLoadingSceneFinished")
    g_ScriptEvent:RemoveListener("WaitForShowScrollTaskSubTitle", self, "OnWaitForShowScrollTaskSubTitle")
    g_ScriptEvent:RemoveListener("CloseScrollTask", self, "OnCloseScrollTask")
    g_ScriptEvent:RemoveListener("ScrollTaskPreLoadPictureFinished", self, "OnScrollTaskPreLoadPictureFinished")
    g_ScriptEvent:RemoveListener("ShowUIPreDraw", self, "ShowUIPreDraw")
    local scaler = CMainCamera.Inst.m_ResolutionScaler
    if scaler then
        scaler.IsCaptureing = false
    end
end

--@region UIEvent

--@endregion UIEvent

function LuaScrollTaskWnd:StartFromSceneInit(formatId, subtitleId, pictureKey)
    self.m_CurrentLayout = formatId
    self.m_CurrentSubtitleId = subtitleId
    self.m_CurrentPictureKey = pictureKey

    local formatGo = self.m_Formats.transform:GetChild(formatId - 1)
    local labelGridTf = formatGo.transform:Find("LabelGrid")
    local textureTf = formatGo.transform:Find("Texture")
    if textureTf ~= nil then
        self.m_IsInScorllAnim = true

        local checkNextScrollcallback = function()
            --self.m_FxSaoGuang.gameObject:SetActive(true)
            self.m_IsInScorllAnim = false
            self:CheckNextScroll()
        end
        self:StartFromScene(textureTf:GetComponent(typeof(UITexture)), pictureKey, function()
            if labelGridTf then
                self:ShowSubtitleAnim(labelGridTf:GetComponent(typeof(UIGrid)), subtitleId, checkNextScrollcallback)
            else 
                checkNextScrollcallback()
            end
        end)
    end
end

function LuaScrollTaskWnd:OnCloseBtnClicked(go)
    local msg = g_MessageMgr:FormatMessage("TaskScroll_Close")
    g_MessageMgr:ShowOkCancelMessage(msg, function()
        Gac2Gas.RequestLeaveScrollTaskMode(1)
        end, nil, nil, nil, false)
end

function LuaScrollTaskWnd:OpenAnim(callback)
    -- 状态初始化
    local width = self.m_Bg.width
    local leftBorderX = self.m_LeftBorder.transform.localPosition.x
    local rightBorderX = self.m_RightBorder.transform.localPosition.x

    self.m_Helper.m_Alpha = 0
    self.m_MaskPanel:Refresh()
    self.m_Bg.width = 0
    self.m_Bg.mRect = CreateFromClass(Rect, 0.5, 0, 0, 1)
    LuaUtils.SetLocalPositionX(self.m_LeftBorder.transform, 0)
    LuaUtils.SetLocalPositionX(self.m_RightBorder.transform, 0)

    -- 隐藏中心飘带特效
    --self.m_FxZhongXin.gameObject:SetActive(true)

    -- 旋转背景 rotation z 从 -22 到 0
    local tweener = LuaTweenUtils.TweenFloat(-22, 0, 0.5, function ( val )
        Extensions.SetLocalRotationZ(self.m_Bg.transform, val)
	end)
    LuaTweenUtils.SetEase(tweener, Ease.InOutQuad)
    LuaTweenUtils.SetDelay(tweener, 0.5)
    table.insert(self.m_OpenAnimTweeners, tweener)

    -- 卷轴展开
    tweener = LuaTweenUtils.TweenFloat(0, 1, 3, function ( val )
        self.m_Bg.width = width * val
        self.m_Bg.mRect = CreateFromClass(Rect, 0.5-(0.5*val), 0, val, 1)
        LuaUtils.SetLocalPositionX(self.m_LeftBorder.transform, leftBorderX * val)
        LuaUtils.SetLocalPositionX(self.m_RightBorder.transform, rightBorderX * val)
    end)
    LuaTweenUtils.SetEase(tweener, Ease.InOutQuad)
    LuaTweenUtils.SetDelay(tweener, 0.7)
    table.insert(self.m_OpenAnimTweeners, tweener)

    -- 回调
    if callback then
        CommonDefs.OnComplete_Tweener(tweener, DelegateFactory.TweenCallback(callback))
    end
end

function LuaScrollTaskWnd:CloseAnim(callback)
    -- 状态初始化
    local width = self.m_Bg.width
    local leftBorderX = self.m_LeftBorder.transform.localPosition.x
    local rightBorderX = self.m_RightBorder.transform.localPosition.x

    self.m_Helper.m_Alpha = 0
    self.m_Helper:RefreshOffsetAndSize()
    self.m_Content.alpha = 0
    self.m_Bg.width = 1
    self.m_Bg.mRect = CreateFromClass(Rect, 0, 0, 1, 1)
    -- LuaUtils.SetLocalPositionX(self.m_LeftBorder.transform, 0)
    -- LuaUtils.SetLocalPositionX(self.m_RightBorder.transform, 0)

    -- 卷轴收缩
    local tweener = LuaTweenUtils.TweenFloat(1, 0, 3, function ( val )
        self.m_Bg.width = width * val
        self.m_Bg.mRect = CreateFromClass(Rect, 0.5-(0.5*val), 0, val, 1)
        LuaUtils.SetLocalPositionX(self.m_LeftBorder.transform, leftBorderX * val)
        LuaUtils.SetLocalPositionX(self.m_RightBorder.transform, rightBorderX * val)
    end)
    LuaTweenUtils.SetEase(tweener, Ease.InOutQuad)
    --LuaTweenUtils.SetDelay(tweener, 0.7)
    table.insert(self.m_OpenAnimTweeners, tweener)
    CommonDefs.OnComplete_Tweener(tweener, DelegateFactory.TweenCallback(function()
        --self.m_FxZhongXin.gameObject:SetActive(true)
        self.m_FxXingXing.gameObject:SetActive(false)
        self.m_FxXingXing.gameObject:SetActive(false)
    end))
    
    -- 旋转背景 rotation z 从 0 到 -22
    tweener = LuaTweenUtils.TweenFloat(0, -22, 0.5, function ( val )
        Extensions.SetLocalRotationZ(self.m_Bg.transform, val)
	end)
    LuaTweenUtils.SetEase(tweener, Ease.InOutQuad)
    LuaTweenUtils.SetDelay(tweener, 3.2)
    table.insert(self.m_OpenAnimTweeners, tweener)

    -- 回调
    if callback then
        CommonDefs.OnComplete_Tweener(tweener, DelegateFactory.TweenCallback(callback))
    end
end

function LuaScrollTaskWnd:Open()
    self.m_IsInScorllAnim = true
    -- LuaUtils.SetLocalPositionX(self.m_LeftBorder.transform, 0)
    -- LuaUtils.SetLocalPositionX(self.m_RightBorder.transform, 0)
    Extensions.SetLocalRotationZ(self.m_Bg.transform, -22)
    -- self.m_Bg.width = 0
    local callback = function()
        self.m_IsInScorllAnim = false

        self.m_FxXingXing.gameObject:SetActive(true)
        --self.m_FxSaoGuang.gameObject:SetActive(true)
        --self.m_FxZhongXin.gameObject:SetActive(false)
        self:CheckNextScroll()
    end
    self:OpenAnim(callback)
end

function LuaScrollTaskWnd:ChangeLayout(layoutId, subtitleId, pirctureKey)
    if self.m_IsInScorllAnim then
        return
    end
    
    local formatGo = self.m_Formats.transform:GetChild(layoutId - 1)
    local labelGridTf = formatGo.transform:Find("LabelGrid")
    local textureTf = formatGo.transform:Find("Texture")
    local labelGrid = labelGridTf and labelGridTf:GetComponent(typeof(UIGrid)) or nil
    local texture = textureTf and textureTf:GetComponent(typeof(UITexture)) or nil

    self.m_IsInScorllAnim = true
    local checkNextScrollcallback = function()
        self.m_IsInScorllAnim = false
        self:CheckNextScroll()
    end

    self.m_CurrentLayout = layoutId
    self:HideSubtitleAnim(self.m_CurrentGird, function() 
        self:SwitchToTextureMask(texture, 3, Ease.InQuint, function()
            self:ShowSubtitleAnim(labelGrid, subtitleId, checkNextScrollcallback)
        end)
    end)
end

function LuaScrollTaskWnd:SwitchToTextureMask(targetTexture, duration, ease, callback, fromRate, toRate)
    if targetTexture == nil then
        if callback then
            callback()
        end
        return 
    end

    self.m_CurrentTex = targetTexture
    fromRate = fromRate or 1
    toRate = toRate or 1
    local maskPosX  = self.m_Helper.baseClipRegion.x
    local maskPosY  = self.m_Helper.baseClipRegion.y
    local maskWidth = self.m_Helper.baseClipRegion.z * fromRate
    local maskHeight= self.m_Helper.baseClipRegion.w * fromRate

    local posX  = targetTexture.transform.localPosition.x
    local posY  = targetTexture.transform.localPosition.y
    local width = targetTexture.width * toRate
    local height= targetTexture.height * toRate

    local noRateMaskHeight = self.m_Helper.baseClipRegion.w
    local noRateHeight = targetTexture.height
    local tweener = LuaTweenUtils.TweenFloat(0, 1, duration, function(val)
        self.m_Helper.baseClipRegion = Vector4(
            math.lerp(maskPosX, posX, val),
            math.lerp(maskPosY, posY, val),
            math.lerp(maskWidth, width, val),
            math.lerp(maskHeight, height, val))

            self.m_Helper.m_MaxHeight = math.lerp(noRateMaskHeight, noRateHeight, val)
            self.m_Helper.m_MaxClipAmount = 0.001 * math.lerp(maskWidth, width, val) + 0.2
            self.m_Helper:RefreshOffsetAndSize()
    end)
    LuaTweenUtils.SetEase(tweener, ease)
    table.insert(self.m_CommonAnimTweeners, tweener)

    if callback then
        CommonDefs.OnComplete_Tweener(tweener, DelegateFactory.TweenCallback(callback))
    end
end

function LuaScrollTaskWnd:SwitchToTextureMaskImmediately(targetTexture, rate)
    self.m_CurrentTex = targetTexture
    rate = rate or 1

    local posX  = targetTexture.transform.localPosition.x
    local posY  = targetTexture.transform.localPosition.y
    local width = targetTexture.width * rate
    local height= targetTexture.height * rate

    self.m_Helper.baseClipRegion = Vector4(
        posX,
        posY,
        width,
        height)
    self.m_Helper.m_MaxHeight = height
    self.m_Helper.m_MaxClipAmount = 0.001 * width + 0.2
    self.m_Helper:RefreshOffsetAndSize()
end

function LuaScrollTaskWnd:CloseWithEndToScene()
    if self.m_IsInScorllAnim then
        return
    end

    self.m_IsInScorllAnim = true
    self:EndToScene(function()
        self.m_IsInScorllAnim = false
        CUIManager.CloseUI(CLuaUIResources.ScrollTaskWnd)
    end)
end

function LuaScrollTaskWnd:EndToScene(callback)
    self:SwitchToTextureMask(self.m_CurrentTex, 4, Ease.InQuart, callback, 1, 4)
    if self.m_CurrentPictureKey ~= "GameScreen" then
        self:HideUI()
    end

    local from = self.m_Bg.transform.localScale
    local to = CommonDefs.op_Multiply_Vector3_Single(self.m_Bg.transform.localScale, 4)
    local tweener = LuaTweenUtils.TweenScale(self.m_Bg.transform, from, to, 4)
    LuaTweenUtils.SetEase(tweener, Ease.InQuart)
    table.insert(self.m_CommonAnimTweeners, tweener)
end

function LuaScrollTaskWnd:StartFromScene(targetTexture, pictureKey, callback)
    local adjustment = 1
    local texture, isLoadPic = LuaScrollTaskMgr:GetScreenshot(pictureKey)
    if not isLoadPic then
        adjustment = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    end
    local width = isLoadPic and 1920 or (texture.width * adjustment)
    local height = isLoadPic and 1080 or (texture.height * adjustment)

    self.m_Texture.material.mainTexture = texture
    self.m_Texture.width = width
    self.m_Texture.height = height
    self.m_MaskPanel:Refresh()
    self:SwitchToTextureMaskImmediately(targetTexture, 4)
    self:SwitchToTextureMask(targetTexture, 4, Ease.OutQuart, callback, 1, 1)
    self:ShowUI()

    local from = CommonDefs.op_Multiply_Vector3_Single(self.m_Bg.transform.localScale, 4)
    local to = self.m_Bg.transform.localScale
    local tweener = LuaTweenUtils.TweenScale(self.m_Bg.transform, from, to, 4)
    LuaTweenUtils.SetEase(tweener, Ease.OutQuart)
    table.insert(self.m_CommonAnimTweeners, tweener)
end

function LuaScrollTaskWnd:ShowScrollAnim(formatId, subtitleId, pictureKey, callback)
    local formatGo = self.m_Formats.transform:GetChild(formatId - 1)
    local labelGridTf = formatGo.transform:Find("LabelGrid")
    local textureTf = formatGo.transform:Find("Texture")

    local labelGrid = labelGridTf and labelGridTf:GetComponent(typeof(UIGrid)) or nil
    local texture = textureTf and textureTf:GetComponent(typeof(UITexture)) or nil

    local isProcessSubtitle =  not (self.m_CurrentLayout == formatId and self.m_CurrentSubtitleId == subtitleId)
    local isPorcessTexture = not (self.m_CurrentLayout == formatId and self.m_CurrentPictureKey == pictureKey)
    local taskCount = (isProcessSubtitle and 1 or 0) + (isPorcessTexture and 1 or 0)
    
    self.m_CurrentLayout = formatId
    if taskCount == 0 then
        callback()
        return
    end
    local count = 0
    local callBackAfterAllDone = function(TaskCount)
        return function()
            count = count + 1
            if count >= TaskCount then
                callback()
            end
        end
    end
    if isProcessSubtitle then
        self:HideSubtitleAnim(self.m_CurrentGird, function()
            self:ShowSubtitleAnim(labelGrid, subtitleId, callBackAfterAllDone(taskCount))
        end)
    end
    if isPorcessTexture then
        self:HideTextureAnim(self.m_CurrentTex, function()
            self:ShowTextureAnim(texture, pictureKey, callBackAfterAllDone(taskCount))
        end)
    end
end

function LuaScrollTaskWnd:ShowSubtitleAnim(labelGrid, subtitleId, callback)
    if labelGrid == nil then
        if callback then
            callback()
        end
        return
    end

    self.m_CurrentGird = labelGrid
    self.m_CurrentSubtitleId = subtitleId
    local template = labelGrid.transform.parent:Find("LabelTemplate").gameObject
    local subtitle = Subtitle_Subtitle.GetData(subtitleId)
    if subtitle == nil then
        if callback then
            callback()
        end
        return
    end
    local content = CChatLinkMgr.TranslateToNGUIText(subtitle.Content, false)
    local lines = CommonDefs.StringSplit_ArrayChar(content, "\n")
    local labelsWidget = labelGrid.gameObject:GetComponent(typeof(UIWidget))
    labelsWidget.alpha = 1

    -- 实例化或销毁labels
    local labelCount = labelGrid.transform.childCount
    if lines.Length > labelCount then
        for i = labelCount, lines.Length - 1 do
            NGUITools.AddChild(labelGrid.gameObject, template)
        end
    else
        local needDestroyGo = {}
        for i = lines.Length , labelCount - 1 do
            table.insert(needDestroyGo, labelGrid.transform:GetChild(i).gameObject)
        end

        for i = 1, #needDestroyGo do
            needDestroyGo[i]:SetActive(false)
            GameObject.Destroy(needDestroyGo[i])
        end
    end

    -- 初始化label
    local labels = {}
    for i = 0, lines.Length - 1 do
        local label = labelGrid.transform:GetChild(i):GetComponent(typeof(UILabel))
        label.text = lines[i]
        label.alpha = 0
        label.gameObject:SetActive(true)
        table.insert(labels, label)
    end
    
    labelGrid:Reposition()
    
    -- 逐行淡入
    local nextSubtitleId = subtitle.Next
    local aliveDuration = subtitle.AliveDuration
    local fadeInTime = subtitle.FadeInTime
    local fadeOutTime = subtitle.FadeOutTime

    local duration = fadeInTime
    local timeDelayTemp = 0
    for i = 1, #labels do
        local label = labels[i]
        local tweener = LuaTweenUtils.TweenAlpha(label, 0, 1, 1)
        --LuaTweenUtils.SetEase(tweener, Ease.InQuint)
        LuaTweenUtils.SetDelay(tweener, timeDelayTemp)
        table.insert(self.m_CommonAnimTweeners, tweener)

        -- 最终走传入的callback回调
        if i == #labels and callback then
            CommonDefs.OnComplete_Tweener(tweener, DelegateFactory.TweenCallback(callback))
        end
        timeDelayTemp = timeDelayTemp + duration
    end
    -- Sound 
    if not System.String.IsNullOrEmpty(subtitle.Sound) then
        SoundManager.Inst:StartDialogSound(subtitle.Sound)
    end
end

function LuaScrollTaskWnd:HideSubtitleAnim(labelGrid, callback)
    if labelGrid == nil then
        if callback then
            callback()
        end
        return
    end

    local labelsWidget = labelGrid.gameObject:GetComponent(typeof(UIWidget))
    if labelsWidget.alpha == 0 then
        if callback then
            callback()
        end
        return
    end
    local tweener = LuaTweenUtils.TweenAlpha(labelsWidget, 1, 0, 2)
    LuaTweenUtils.SetEase(tweener, Ease.InQuint)
    table.insert(self.m_CommonAnimTweeners, tweener)

    if callback then
        CommonDefs.OnComplete_Tweener(tweener, DelegateFactory.TweenCallback(callback))
    end
end

function LuaScrollTaskWnd:ShowTextureAnim(texture, pictureKey, callback)
    local adjustment = 1
    local picture, isLoadPic = LuaScrollTaskMgr:GetScreenshot(pictureKey)
    if not isLoadPic then
        adjustment = UIRoot.GetPixelSizeAdjustment(self.gameObject)
    end
    if texture == nil or picture == nil then
        if callback then
            callback()
        end
        return
    end

    self.m_CurrentTex = texture
    self.m_CurrentPictureKey = pictureKey
    local posX  = texture.transform.localPosition.x
    local posY  = texture.transform.localPosition.y
    local width = texture.width
    local height= texture.height

    self.m_Helper.m_Alpha = 0
    self.m_Texture.material.mainTexture = picture
    self.m_Texture.width = isLoadPic and 1920 or (picture.width * adjustment)
    self.m_Texture.height = isLoadPic and 1080 or (picture.height * adjustment)
    self.m_MaskPanel:Refresh()
    self.m_Texture:MarkAsChanged()
    self.m_Texture.gameObject:SetActive(true)
    self.m_Helper.baseClipRegion = Vector4(posX, posY, 0, 0)
    self.m_Helper.m_MaxHeight = height
    self.m_Helper.m_MaxClipAmount = 0.001 * width + 0.2
    self.m_Helper:RefreshOffsetAndSize()
    local tweener = LuaTweenUtils.TweenFloat(0, 1, 3, function(val)
        self.m_Helper.m_Alpha = val
        self.m_Helper.baseClipRegion = Vector4(posX, posY, width * val, height * val)
        self.m_Helper.m_MaxClipAmount = 0.001 * width * val + 0.2
        self.m_Helper:RefreshOffsetAndSize()
        self.m_Texture:MarkAsChanged()
    end)
    LuaTweenUtils.SetEase(tweener, Ease.OutQuint)
    table.insert(self.m_CommonAnimTweeners, tweener)
    if callback then
        CommonDefs.OnComplete_Tweener(tweener, DelegateFactory.TweenCallback(callback))
    end
end

function LuaScrollTaskWnd:HideTextureAnim(texture, callback)
    if texture == nil or self.m_Helper.m_Alpha == 0 then
        if callback then
            callback()
        end
        return
    end

    local maskPosX  = self.m_Helper.baseClipRegion.x
    local maskPosY  = self.m_Helper.baseClipRegion.y
    local maskWidth = self.m_Helper.baseClipRegion.z
    local maskHeight= self.m_Helper.baseClipRegion.w

    local tweener = LuaTweenUtils.TweenFloat(1, 0, 2, function(val)
        self.m_Helper.m_Alpha = val
        local sizeRate = 0.9 + val * 0.1
        self.m_Helper.m_MaxClipAmount = 0.001 * maskWidth * sizeRate + 0.2
        self.m_Helper.baseClipRegion = Vector4(maskPosX, maskPosY, maskWidth * sizeRate, maskHeight * sizeRate)
        self.m_Helper:RefreshOffsetAndSize()
    end)
    LuaTweenUtils.SetEase(tweener, Ease.InQuint)
    table.insert(self.m_CommonAnimTweeners, tweener)

    if callback then
        CommonDefs.OnComplete_Tweener(tweener, DelegateFactory.TweenCallback(callback))
    end
end

function LuaScrollTaskWnd:ShowScroll(formatId, subtitleId, pictureKey)
    -- 已经在动画中则返回
    if self.m_IsInScorllAnim then
        return
    end

    self.m_IsInScorllAnim = true
    local callback = function ()
        self.m_IsInScorllAnim = false
        self:CheckNextScroll()
    end

    self:ShowScrollAnim(formatId, subtitleId, pictureKey, callback)
end

function LuaScrollTaskWnd:HideScroll(subtitleId, pictureKey)
    -- 已经在动画中则返回
    if self.m_IsInScorllAnim then
        return
    end

    self.m_IsInScorllAnim = true
    local callback = function ()
        self.m_IsInScorllAnim = false
        self:CheckNextScroll()
    end

    self:HideScrollTaskSubTitle(subtitleId, pictureKey, callback)
end

function LuaScrollTaskWnd:CheckNextScroll()
    -- 已经在动画中则返回
    if self.m_IsInScorllAnim or self.m_IsPreLoad then
        return
    end

    if LuaScrollTaskMgr.waitForSubtitleId and self.m_CurrentSubtitleId == LuaScrollTaskMgr.waitForSubtitleId then
        Gac2Gas.ShowScrollTaskSubTitleDone(LuaScrollTaskMgr.waitForSubtitleId)
        LuaScrollTaskMgr.waitForSubtitleId = nil
    end

    local scrollInfo = LuaScrollTaskMgr:DequeueScorllInfo()
    if scrollInfo then
        self:DoScroll(scrollInfo)
    elseif self.m_IsInLoadingScene then
        self.m_Loading:SetActive(true)
    end
end
-- op formatId subtitleId pictureKey
function LuaScrollTaskWnd:DoScroll(scrollInfo)
    if scrollInfo.op ~= "HideScroll" and scrollInfo.pictureKey and scrollInfo.pictureKey ~= "" and scrollInfo.pictureKey ~= "0" and LuaScrollTaskMgr:GetScreenshot(scrollInfo.pictureKey) == nil and not scrollInfo.IsPreLoaded then
        scrollInfo.IsPreLoaded = true
        self.m_IsPreLoad = true
        self.m_CacheScrollInfo = scrollInfo
        LuaScrollTaskMgr:PreLoadPicture(scrollInfo.pictureKey)
        return
    end
    if scrollInfo.op == "ShowScroll" then
        self:ShowScroll(scrollInfo.formatId, scrollInfo.subtitleId, scrollInfo.pictureKey)
    elseif scrollInfo.op == "HideScroll" then
        self:HideScroll(scrollInfo.subtitleId, scrollInfo.pictureKey)
    elseif scrollInfo.op == "ChangeLayout" then
        self:ChangeLayout(scrollInfo.formatId, scrollInfo.subtitleId, scrollInfo.pictureKey)
    elseif scrollInfo.op == "ShowScrollWithStartFromScene" then
        self:StartFromSceneInit(scrollInfo.formatId, scrollInfo.subtitleId, scrollInfo.pictureKey)
    elseif scrollInfo.op == "CloseWithEndToScene" then
        self:CloseWithEndToScene()
    elseif scrollInfo.op == "SwitchToGameView" then
        local e = g_LuaUtil:StrSplit(scrollInfo.eventName,"_")
        local gameType = "ScrollTask" .. e[1] .. "TaskView"
        local gameId = tonumber(e[2]) or 1
        self:SwitchToGameView(scrollInfo.formatId, scrollInfo.pictureKey, gameType, gameId, scrollInfo.taskId, scrollInfo.eventName)
    elseif scrollInfo.op == "HideGameView" then
        self:HideGameView()
    end
end

function LuaScrollTaskWnd:OnScrollTaskPreLoadPictureFinished()
    if self.m_CacheScrollInfo then
        local scrollInfo = self.m_CacheScrollInfo
        self.m_CacheScrollInfo = nil
        self.m_IsPreLoad = false

        self:DoScroll(scrollInfo)
    end
end

function LuaScrollTaskWnd:HideScrollTaskSubTitle(subtitleId, pictureKey, callback)
    local isHideSubtitle = self.m_CurrentSubtitleId == subtitleId
    local isHidePicture = self.m_CurrentPictureKey == pictureKey

    local taskCount = (isHideSubtitle and 1 or 0) + (isHidePicture and 1 or 0)
    if taskCount == 0 then
        callback()
        return
    end
    local count = 0
    local callBackAfterAllDone = function(TaskCount)
        return function()
            count = count + 1
            if count >= TaskCount then
                callback()
            end
        end
    end
    if isHideSubtitle then
        self:HideSubtitleAnim(self.m_CurrentGird, callBackAfterAllDone(taskCount))
        self.m_CurrentSubtitleId = nil
    end
    if isHidePicture then
        self:HideTextureAnim(self.m_CurrentTex, callBackAfterAllDone(taskCount))
        self.m_CurrentPictureKey = nil
    end
end

function LuaScrollTaskWnd:ClearCommonTweeners()
    if self.m_CommonAnimTweeners then
        for i = 1, #self.m_CommonAnimTweeners do
            LuaTweenUtils.Kill(self.m_CommonAnimTweeners[i], false)
        end
    end
    self.m_CommonAnimTweeners = {}
end

function LuaScrollTaskWnd:ClearOpenTweeners()
    if self.m_OpenAnimTweeners then
        for i = 1, #self.m_OpenAnimTweeners do
            LuaTweenUtils.Kill(self.m_OpenAnimTweeners[i], false)
        end
    end
    self.m_OpenAnimTweeners = {}
end

function LuaScrollTaskWnd:OnDestroy()
    self:ClearOpenTweeners()
    self:ClearCommonTweeners()

    LuaScrollTaskMgr.startFromScene = false
    LuaScrollTaskMgr.endToScene = false
    LuaScrollTaskMgr.nextLoadingPicToScroll = false

    LuaScrollTaskMgr:ClearScrollQueue()
    LuaScrollTaskMgr:ClearScreenshot()
    LuaScrollTaskMgr:ShowOrHideHeadInfo(false)

    CMiddleNoticeMgr.ShowNoticeDisable = false
end

function LuaScrollTaskWnd:ChangeWndPanelDepth(depth)
    self.m_Panel.depth = depth - 1
    self.m_MaskPanel.depth = depth
    for i = 1, #self.m_GamePanels do
        self.m_GamePanels[i].depth = depth + 2
    end
    self.m_BorderPanel.depth = depth + 4
    self.m_DecPanel.depth = depth + 5

    LuaScrollTaskMgr.m_HeadInfoPanelShowDepth = depth + 6

    g_ScriptEvent:BroadcastInLua("ScrollTaskGamePanelChange", depth + 1)
end

function LuaScrollTaskWnd:OnStartLoadScene()
    self.m_IsInLoadingScene = true
    self.m_CloseButton.gameObject:SetActive(false)
    if not self.m_IsInScorllAnim then
        self.m_Loading:SetActive(true)
    end
    if LuaScrollTaskMgr.nextLoadingPicToScroll then
        self:ChangeWndPanelDepth(CUIManager.LOADING_DEPTH + 2)
    else 
        CUIManager.CloseUI(CLuaUIResources.ScrollTaskWnd)
    end
end

function LuaScrollTaskWnd:OnLoadingSceneFinished()
    self.m_IsInLoadingScene = false
    self.m_CloseButton.gameObject:SetActive(true)
    self.m_Loading:SetActive(false)
    LuaScrollTaskMgr.nextLoadingPicToScroll = false
    self:ChangeWndPanelDepth(CUIManager.MAX_DEPTH-3)
end

function LuaScrollTaskWnd:OnWaitForShowScrollTaskSubTitle()
	if not LuaScrollTaskMgr.waitForSubtitleId then
		return
	end
    if LuaScrollTaskMgr:FindScorllInfoBySubtitleId(LuaScrollTaskMgr.waitForSubtitleId) or self.m_CurrentSubtitleId == LuaScrollTaskMgr.waitForSubtitleId then
        return
    end

    -- 如果当前动画和后续动画都没有此subtitleId则直接响应
	Gac2Gas.ShowScrollTaskSubTitleDone(LuaScrollTaskMgr.waitForSubtitleId)
    LuaScrollTaskMgr.waitForSubtitleId = nil
end

function LuaScrollTaskWnd:SwitchToGameView(formatId, pictureKey, gameType, gameId, taskId, eventName)
    self.m_IsInScorllAnim = true
    --self:ShowScrollAnim(formatId, nil, pictureKey, function()
    local transform = self.m_Games.transform:Find(gameType)
    if transform == nil then
        self.m_IsInScorllAnim = false
        self:CheckNextScroll()
        return
    end

    transform:GetComponent(typeof(CCommonLuaScript)):Awake()
    local script = transform:GetComponent(typeof(CCommonLuaScript)).m_LuaSelf
    self:HideCurrentGame(function()
        local panel = transform:GetComponent(typeof(UIPanel))
        panel.alpha = 0
        panel.gameObject:SetActive(true) 

        script:InitGame(gameId, taskId, function()
            Gac2Gas.ScrollTaskGameEventDone(taskId, eventName)
            self:HideCurrentGame(function()
                self.m_IsInScorllAnim = false
                self:CheckNextScroll()
            end)
        end)
        self:ShowGame(transform)
    end)

    self.m_CurrentGame = transform
    --end)
end

function LuaScrollTaskWnd:HideCurrentGame(callback)
    if self.m_CurrentGame then
        local panel = self.m_CurrentGame:GetComponent(typeof(UIPanel))
        local tweener = LuaTweenUtils.TweenFloat(1, 0, 1, function ( val )
            panel.alpha = val
        end)
        LuaTweenUtils.SetEase(tweener, Ease.InQuint)
        table.insert(self.m_CommonAnimTweeners, tweener)
    
        -- 回调
        if callback then
            CommonDefs.OnComplete_Tweener(tweener, DelegateFactory.TweenCallback(function()
                panel.gameObject:SetActive(false)
                callback()
            end))
        end

        self.m_CurrentGame = nil
    else 
        if callback then
            callback()
        end
    end
end

function LuaScrollTaskWnd:HideGameView()
    self.m_IsInScorllAnim = true
    self:HideCurrentGame(function()
        self.m_IsInScorllAnim = false
        self:CheckNextScroll()
    end)
end

function LuaScrollTaskWnd:ShowGame(gameTransform, callback)
    local panel = gameTransform:GetComponent(typeof(UIPanel))
    local tweener = LuaTweenUtils.TweenFloat(0, 1, 1, function ( val )
        panel.alpha = val
    end)
    LuaTweenUtils.SetEase(tweener, Ease.OutQuint)
    table.insert(self.m_CommonAnimTweeners, tweener)
    -- 回调
    if callback then
        CommonDefs.OnComplete_Tweener(tweener, DelegateFactory.TweenCallback(callback))
    end
end

function LuaScrollTaskWnd:ShowUI()
    self.m_Panel.alpha = 0
    self.m_Helper.m_Alpha = 0
    self.m_MaskPanel:Refresh()
    local tweener = LuaTweenUtils.TweenFloat(0, 1, 1, function ( val )
        self.m_Panel.alpha = val
        self.m_Helper.m_Alpha = val
        self.m_MaskPanel:Refresh()
    end)
    table.insert(self.m_CommonAnimTweeners, tweener)
end

function LuaScrollTaskWnd:HideUI()
    self.m_Panel.alpha = 1
    self.m_Helper.m_Alpha = 1
    self.m_MaskPanel:Refresh()
    local tweener = LuaTweenUtils.TweenFloat(1, 0, 1, function ( val )
        self.m_Panel.alpha = val
        self.m_Helper.m_Alpha = val
        self.m_MaskPanel:Refresh()
    end)
    LuaTweenUtils.SetDelay(tweener, 3)
    table.insert(self.m_CommonAnimTweeners, tweener)
end

function LuaScrollTaskWnd:OnCloseScrollTask()
    self:ClearCommonTweeners()
    LuaScrollTaskMgr:ClearScreenshot()
    local callback = function()
        CUIManager.CloseUI(CLuaUIResources.ScrollTaskWnd)
    end
    self:CloseAnim(callback)
end

function LuaScrollTaskWnd:ShowUIPreDraw(args)
    local show,name=args[0],args[1]
    if name == "WinCGWnd" and CUIManager.instance then
        local winGo = CommonDefs.DictGetValue_LuaCall(CUIManager.instance.loadedUIs, CUIResources.WinCGWnd)
        if winGo then
            local winPanel = winGo:GetComponent(typeof(UIPanel))
            winPanel.depth = LuaScrollTaskMgr.m_HeadInfoPanelShowDepth
        end
    end
end
