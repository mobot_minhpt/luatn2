local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"

LuaOlympicWinterGamesSnowBattleSkillView = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaOlympicWinterGamesSnowBattleSkillView, "Root", "Root", GameObject)
RegistChildComponent(LuaOlympicWinterGamesSnowBattleSkillView, "SnowballFightBulletNumLabel", "SnowballFightBulletNumLabel", UILabel)
RegistChildComponent(LuaOlympicWinterGamesSnowBattleSkillView, "Icon", "Icon", CUITexture)
--@endregion RegistChildComponent end

function LuaOlympicWinterGamesSnowBattleSkillView:Awake()
    self.Root.gameObject:SetActive(false)
end

function LuaOlympicWinterGamesSnowBattleSkillView:OnEnable()
    g_ScriptEvent:AddListener("OnSnowballFightSyncPlayerInfo", self, "OnSnowballFightSyncPlayerInfo")
    g_ScriptEvent:AddListener("MainPlayerCreated",self,"OnMainPlayerCreated")
end

function LuaOlympicWinterGamesSnowBattleSkillView:OnDisable()
    g_ScriptEvent:RemoveListener("OnSnowballFightSyncPlayerInfo", self, "OnSnowballFightSyncPlayerInfo")
    g_ScriptEvent:RemoveListener("MainPlayerCreated",self,"OnMainPlayerCreated")
end

function LuaOlympicWinterGamesSnowBattleSkillView:OnSnowballFightSyncPlayerInfo()
    self.Root.gameObject:SetActive(true)
    local bulletId = LuaOlympicGamesMgr.m_SnowballBulletId
    self.SnowballFightBulletNumLabel.text = bulletId == WinterOlympic_SnowballFightSetting.GetData().DefaultBulletId and 
        LuaOlympicGamesMgr.m_SnowballFightBulletNum or LuaOlympicGamesMgr.m_SnowballLeftTimes
    local data = WinterOlympic_SnowballGun.GetData(bulletId)
    if data then
        self.Icon:LoadSkillIcon(data.Icon)
    end
    local isNoneBall = (bulletId == WinterOlympic_SnowballFightSetting.GetData().DefaultBulletId) and (LuaOlympicGamesMgr.m_SnowballFightBulletNum == 0)
    Extensions.SetLocalPositionZ(self.Icon.transform, isNoneBall and -1 or 0)
    self.SnowballFightBulletNumLabel.color = isNoneBall and Color.red or Color.white
end

function LuaOlympicWinterGamesSnowBattleSkillView:OnMainPlayerCreated()
    self.Root.gameObject:SetActive(false)
end

--@region UIEvent

--@endregion UIEvent

