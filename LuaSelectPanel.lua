local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local CUIRestrictScrollView = import "L10.UI.CUIRestrictScrollView"

LuaSelectPanel = class()

--@region RegistChildComponent: Dont Modify Manually!


--@endregion RegistChildComponent end

function LuaSelectPanel:Init(obj, panelCfg, firstSelectTabIndex)
    self.prefixPath = "UI/Texture/Transparent/Material/"
    self.tabScrollNumber = 6
    self.subScrollNumber = 12
    
    self.transform = obj.transform
    self.TabBtnsWidget = self.transform:Find("TabBtnsWidget").gameObject
    self.TabContent = self.TabBtnsWidget.transform:Find("TabContent").gameObject
    self.TabButtonTemplate = self.TabBtnsWidget.transform:Find("TabButtonTemplate").gameObject
    self.SubBtnsWidget = self.transform:Find("SubBtnsWidget").gameObject
    self.SubContent = self.SubBtnsWidget.transform:Find("SubContent").gameObject
    self.SubButtonTemplate = self.SubBtnsWidget.transform:Find("SubButtonTemplate").gameObject
    self.CommitBtn = self.transform:Find("CommitBtn").gameObject
    self.Tabtable = self.TabContent.transform:Find("scrollView/Tabtable"):GetComponent(typeof(UIGrid))
    self.Subtable = self.SubContent.transform:Find("scrollView/Subtable"):GetComponent(typeof(UIGrid))
    self.tabScroll = self.TabContent.transform:Find("scrollView"):GetComponent(typeof(CUIRestrictScrollView))
    self.subScroll = self.SubContent.transform:Find("scrollView"):GetComponent(typeof(CUIRestrictScrollView))

    self.curSelectTabIndex = -1
    self.tabButtonList = {}
    self.subButtonList = {}
    --拿来显示角标用
    self.repeatTimeData = {{}, {}, {}, {}}
    
    self.panelCfg = panelCfg
    for i = 1, #panelCfg do
        local cfg = panelCfg[i]
        --cfg = {element = {}, title = LocalString.GetString("铺色"), RepeatMaxNum = 1, UniqueRange = {1, 1}}
        local trans = CommonDefs.Object_Instantiate(self.TabButtonTemplate.transform)
        trans.gameObject:SetActive(true)
        trans:SetParent(self.Tabtable.transform)
        trans.localScale = Vector3.one
        trans.localPosition = Vector3.zero
        trans:Find("Label"):GetComponent(typeof(UILabel)).text = cfg.title
        trans:Find("AlertSprite").gameObject:SetActive(cfg.InitRedAlert)
        trans:Find("highlight").gameObject:SetActive(false)

        UIEventListener.Get(trans.gameObject).onClick = DelegateFactory.VoidDelegate(function ()
            if i ~= self.curSelectTabIndex then
                self:ClickTabButton(i)
            end
        end)
        table.insert(self.tabButtonList, trans.gameObject)
    end
    self.Tabtable:Reposition()
    self.tabScroll.enabled = #self.tabButtonList >= self.tabScrollNumber
    if firstSelectTabIndex then
        self:ClickTabButton(firstSelectTabIndex)
    end

    UIEventListener.Get(self.CommitBtn).onClick = DelegateFactory.VoidDelegate(function ()
        g_ScriptEvent:BroadcastInLua("SelectPanel_TryCommit")
    end)
end

function LuaSelectPanel:ClickTabButton(idx)
    if self.curSelectTabIndex ~= -1 then
        self.tabButtonList[self.curSelectTabIndex].transform:Find("highlight").gameObject:SetActive(false)
        self.tabButtonList[self.curSelectTabIndex].transform:Find("Label"):GetComponent(typeof(UILabel)).color = NGUIText.ParseColor24("F7D4A3", 0)
    end
    g_ScriptEvent:BroadcastInLua("SelectPanel_ClickTabButton", idx)
    self.tabButtonList[idx].transform:Find("highlight").gameObject:SetActive(true)
    self.tabButtonList[idx].transform:Find("Label"):GetComponent(typeof(UILabel)).color = NGUIText.ParseColor24("7F4E2C", 0)

    self.curSelectTabIndex = idx

    local cfg = self.panelCfg[idx]
    Extensions.RemoveAllChildren(self.Subtable.transform)
    self.subButtonList = {}
    for subId = 1, #cfg.element do
        local subTrans = CommonDefs.Object_Instantiate(self.SubButtonTemplate.transform)
        --Load Material
        subTrans:GetComponent(typeof(CUITexture)):LoadMaterial(self.prefixPath .. cfg.element[subId].IconResource .. ".mat")
        subTrans:GetComponent(typeof(UITexture)).color = NGUIText.ParseColor24(cfg.element[subId].IconRGB, 0)
        
        subTrans.gameObject:SetActive(true)
        subTrans:SetParent(self.Subtable.transform)
        subTrans.localScale = Vector3.one
        subTrans.localPosition = Vector3.zero
        UIEventListener.Get(subTrans.gameObject).onClick = DelegateFactory.VoidDelegate(function ()
            --这个地方不检查, 交由父节点Wnd来检查是否添加成功的逻辑
            self:OnChangeSelectBg(idx, subId, true)
            g_ScriptEvent:BroadcastInLua("SelectPanel_TryAddSubItem", idx, subId)
        end)
        table.insert(self.subButtonList, subTrans.gameObject)

        if self.repeatTimeData[idx][subId] then
            local repeatTime = self.repeatTimeData[idx][subId]
            subTrans:Find("ApplyBg").gameObject:SetActive(repeatTime == 1)
            subTrans:Find("MultipleApplyBg").gameObject:SetActive(repeatTime > 1)
            subTrans:Find("MultipleApplyBg/Number"):GetComponent(typeof(UILabel)).text = LocalString.GetString("X") .. repeatTime
        else
            subTrans:Find("ApplyBg").gameObject:SetActive(false)
            subTrans:Find("MultipleApplyBg").gameObject:SetActive(false)
        end
    end
    self.Subtable:Reposition()
    self.Tabtable:Reposition()
    self.subScroll.enabled = #self.subButtonList >= self.subScrollNumber

end

--@region UIEvent

--@endregion UIEvent

function LuaSelectPanel:Ctor()
    g_ScriptEvent:AddListener("SelectPanel_ChangeAlertState", self, "OnChangeAlertState")
    g_ScriptEvent:AddListener("SelectPanel_AddSubItemSuccess", self, "OnAddSubItemSuccess")
    g_ScriptEvent:AddListener("SelectPanel_AddSubItemAndClearOtherSuccess", self, "OnAddSubItemAndClearOtherSuccess")
    g_ScriptEvent:AddListener("SelectPanel_RemoveSubItemSuccess", self, "OnRemoveSubItemSuccess")
    g_ScriptEvent:AddListener("SelectPanel_ChangeSelectBg", self, "OnChangeSelectBg")
    g_ScriptEvent:AddListener("SelectPanel_ChangeSelectExceptBg", self, "OnChangeExceptSelectBg")
end

function LuaSelectPanel:OnDestroy()
    g_ScriptEvent:RemoveListener("SelectPanel_ChangeAlertState", self, "OnChangeAlertState")
    g_ScriptEvent:RemoveListener("SelectPanel_AddSubItemSuccess", self, "OnAddSubItemSuccess")
    g_ScriptEvent:RemoveListener("SelectPanel_AddSubItemAndClearOtherSuccess", self, "OnAddSubItemAndClearOtherSuccess")
    g_ScriptEvent:RemoveListener("SelectPanel_RemoveSubItemSuccess", self, "OnRemoveSubItemSuccess")
    g_ScriptEvent:RemoveListener("SelectPanel_ChangeSelectBg", self, "OnChangeSelectBg")
    g_ScriptEvent:RemoveListener("SelectPanel_ChangeSelectExceptBg", self, "OnChangeExceptSelectBg")
end

function LuaSelectPanel:OnChangeExceptSelectBg(tabIndex, subIndex, isOn)
    --同tabIndex下, 所有非subIndex的修改状态
    if self.curSelectTabIndex and self.curSelectTabIndex == tabIndex then
        if self.subButtonList and (not CommonDefs.IsNull(self.transform)) then
            for k, v in pairs(self.subButtonList) do
                if k ~= subIndex and v.transform then
                    local subTrans = v.transform
                    subTrans:Find("SelectBg").gameObject:SetActive(isOn)
                end
            end
        end
    end
end

function LuaSelectPanel:OnChangeSelectBg(tabIndex, subIndex, isOn)
    if self.curSelectTabIndex and self.curSelectTabIndex == tabIndex then
        if self.subButtonList and (not CommonDefs.IsNull(self.transform)) then
            if (subIndex <= #self.subButtonList) and (not CommonDefs.IsNull(self.subButtonList[subIndex])) then
                local subTrans = self.subButtonList[subIndex].transform
                subTrans:Find("SelectBg").gameObject:SetActive(isOn)
            end
        end
    end
end

function LuaSelectPanel:OnChangeAlertState(tabIndex, isOn)
    if self.tabButtonList and (not CommonDefs.IsNull(self.transform)) then
        if (tabIndex <= #self.tabButtonList) and (not CommonDefs.IsNull(self.tabButtonList[tabIndex])) then
            self.tabButtonList[tabIndex].transform:Find("AlertSprite").gameObject:SetActive(isOn)
        end
    end
end

function LuaSelectPanel:OnAddSubItemAndClearOtherSuccess(tabIndex, subIndex, repeatTime)
    if self.subButtonList and (not CommonDefs.IsNull(self.transform)) then
        if (subIndex <= #self.subButtonList) and (not CommonDefs.IsNull(self.subButtonList[subIndex])) then
            for k, v in pairs(self.repeatTimeData[tabIndex]) do
                self.repeatTimeData[tabIndex][k] = 0
            end
            self.repeatTimeData[tabIndex][subIndex] = repeatTime

            if self.curSelectTabIndex and self.curSelectTabIndex == tabIndex then
                for k, v in pairs(self.subButtonList) do
                    local subTrans = v.transform
                    subTrans:Find("SelectBg").gameObject:SetActive(subIndex == k)
                    subTrans:Find("ApplyBg").gameObject:SetActive(repeatTime == 1 and subIndex == k)
                    subTrans:Find("MultipleApplyBg").gameObject:SetActive(repeatTime > 1 and subIndex == k)
                    subTrans:Find("MultipleApplyBg/Number"):GetComponent(typeof(UILabel)).text = LocalString.GetString("X") .. repeatTime
                end
            end
        end
    end
end

function LuaSelectPanel:OnAddSubItemSuccess(tabIndex, subIndex, repeatTime)
    if self.subButtonList and (not CommonDefs.IsNull(self.transform)) then
        if self.curSelectTabIndex and self.curSelectTabIndex == tabIndex then
            if (subIndex <= #self.subButtonList) and (not CommonDefs.IsNull(self.subButtonList[subIndex])) then
                local subTrans = self.subButtonList[subIndex].transform
                subTrans:Find("ApplyBg").gameObject:SetActive(repeatTime == 1)
                subTrans:Find("MultipleApplyBg").gameObject:SetActive(repeatTime > 1)
                subTrans:Find("MultipleApplyBg/Number"):GetComponent(typeof(UILabel)).text = LocalString.GetString("X") .. repeatTime
            end
        end
        self.repeatTimeData[tabIndex][subIndex] = repeatTime
    end
end

function LuaSelectPanel:OnRemoveSubItemSuccess(tabIndex, subIndex, repeatTime)
    if self.subButtonList and (not CommonDefs.IsNull(self.transform)) then
        if self.curSelectTabIndex and self.curSelectTabIndex == tabIndex then
            if (subIndex <= #self.subButtonList) and (not CommonDefs.IsNull(self.subButtonList[subIndex])) then
                local subTrans = self.subButtonList[subIndex].transform
                subTrans:Find("ApplyBg").gameObject:SetActive(repeatTime == 1)
                subTrans:Find("MultipleApplyBg").gameObject:SetActive(repeatTime > 1)
                subTrans:Find("MultipleApplyBg/Number"):GetComponent(typeof(UILabel)).text = LocalString.GetString("X") .. repeatTime
            end
        end
        self.repeatTimeData[tabIndex][subIndex] = repeatTime
    end
end