local CommonDefs = import "L10.Game.CommonDefs"
local DelegateFactory  = import "DelegateFactory"
local CUITexture = import "L10.UI.CUITexture"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"

LuaDaFuWongShuiLaoWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaDaFuWongShuiLaoWnd, "HeadIcon", "HeadIcon", CUITexture)
RegistChildComponent(LuaDaFuWongShuiLaoWnd, "bg", "bg", GameObject)
RegistChildComponent(LuaDaFuWongShuiLaoWnd, "CloseLabel", "CloseLabel", UILabel)
RegistChildComponent(LuaDaFuWongShuiLaoWnd, "Name", "Name", UILabel)
RegistChildComponent(LuaDaFuWongShuiLaoWnd, "DesLabel", "DesLabel", UILabel)

--@endregion RegistChildComponent end
RegistClassMember(LuaDaFuWongShuiLaoWnd,"nameColor")
RegistClassMember(LuaDaFuWongShuiLaoWnd,"headIconBg")
RegistClassMember(LuaDaFuWongShuiLaoWnd, "m_PlayerHead")
function LuaDaFuWongShuiLaoWnd:Awake()
--@region EventBind: Dont Modify Manually!
    --@endregion EventBind end
    self.nameColor = {"FFEEBC","FAC4BA","e6caff","BDFAD9"}
    self.headIconBg = {"dafuwongplaylandeopwnd_huang","dafuwongplaylandeopwnd_hong","dafuwongplaylandeopwnd_zi","dafuwongplaylandeopwnd_lv"}
    self.m_PlayerHead = self.transform:Find("Anchor/Content/PlayerHead").gameObject
end

function LuaDaFuWongShuiLaoWnd:Init()
    if not LuaDaFuWongMgr.CurAniInfo then return end
    self:ShowPlayerHead()
    local AnimInfo = LuaDaFuWongMgr.CurAniInfo
    local playerId = 0
    if LuaDaFuWongMgr.CurStage == EnumDaFuWengStage.RoundCheckAnimation then
        playerId = AnimInfo.playerId
        self.DesLabel.gameObject:SetActive(false)
        self.m_PlayerHead.gameObject:SetActive(false)
    elseif LuaDaFuWongMgr.CurStage == EnumDaFuWengStage.RoundCardAnimation then
        playerId = AnimInfo.selectPlayerId
        self.DesLabel.gameObject:SetActive(true)
    end
    local playerInfo = LuaDaFuWongMgr.PlayerInfo[playerId]
    if not playerInfo then return end
    local id = DaFuWeng_Setting.GetData().ShuiLaoBuffId
    local Times = DaFuWeng_Buff.GetData(id).Times
    local headIndex = DaFuWeng_Setting.GetData().ShuiLaoHeadIndex

    self.DesLabel.text = nil
    local name = playerInfo.name
    local nameColor = self.nameColor[playerInfo.round]

    --local text = LocalString.GetString("[%s]%s[-]在%d回合内无法行动并无法收取过路费。[bcbcbc](%d秒后关闭)[-]")

    self.HeadIcon:LoadNPCPortrait(CUICommonDef.GetPortraitName(playerInfo.class, playerInfo.gender, headIndex), false)
    local headIconBg = self.bg.gameObject:GetComponent(typeof(CUITexture))
    headIconBg:LoadMaterial(self:GetTexturePath(playerInfo.round))
    local countDown = AnimInfo.time
	local endFunc = function() CUIManager.CloseUI(CLuaUIResources.DaFuWongShuiLaoWnd) end
	local durationFunc = function(time)
        self.DesLabel.text = g_MessageMgr:FormatMessage("DaFuWeng_CannotMove_Desc",nameColor,name,Times,time)
        --self.DesLabel.text = SafeStringFormat3(text,nameColor,name,Times,time)
    end
    SoundManager.Inst:PlayOneShot("event:/L10/L10_UI/DaFuWeng/UI_DaFuWeng_ShuiLao", Vector3.zero, nil, 0)
	LuaDaFuWongMgr:StartCountDownTick(EnumDaFuWengCountDownStage.RandomEvent,countDown,durationFunc,endFunc)
end
function LuaDaFuWongShuiLaoWnd:ShowPlayerHead()
    if LuaDaFuWongMgr.IsMyRound then
        self.m_PlayerHead.gameObject:SetActive(false)
    else
        self.m_PlayerHead.gameObject:SetActive(true)
        local playerData = LuaDaFuWongMgr.PlayerInfo and LuaDaFuWongMgr.PlayerInfo[LuaDaFuWongMgr.CurPlayerRound]
        local Portrait = self.m_PlayerHead.transform:Find("Portrait"):GetComponent(typeof(CUITexture))
        local bg = self.m_PlayerHead.transform:Find("BG"):GetComponent(typeof(CUITexture))
        local Name = self.m_PlayerHead.transform:Find("Name"):GetComponent(typeof(UILabel))
        if playerData then
            Portrait:LoadNPCPortrait(CUICommonDef.GetPortraitName(playerData.class, playerData.gender, -1), false)
			bg:LoadMaterial(LuaDaFuWongMgr:GetHeadBgPath(playerData.round))
			Name.transform:Find("Texture"):GetComponent(typeof(CUITexture)):LoadMaterial(LuaDaFuWongMgr:GetNameBgPath(playerData.round))
            Name.text = playerData.name
        end
    end
end
function LuaDaFuWongShuiLaoWnd:GetTexturePath(index)
    local name = self.headIconBg[index]
    return SafeStringFormat3("UI/Texture/FestivalActivity/Festival_DaFuWong/Material/%s.mat",name)
end

function LuaDaFuWongShuiLaoWnd:OnStageUpdate(CurStage)
    if CurStage ~= EnumDaFuWengStage.RoundCheckAnimation and  CurStage ~= EnumDaFuWengStage.RoundCardAnimation then
        CUIManager.CloseUI(CLuaUIResources.DaFuWongShuiLaoWnd)
    end
end

function LuaDaFuWongShuiLaoWnd:OnDisable()
    g_ScriptEvent:RemoveListener("OnDaFuWengStageUpdate",self,"OnStageUpdate")
    LuaDaFuWongMgr:EndCountDownTick(EnumDaFuWengCountDownStage.RandomEvent)
end
function LuaDaFuWongShuiLaoWnd:OnEnable()
    g_ScriptEvent:AddListener("OnDaFuWengStageUpdate",self,"OnStageUpdate")
end
--@region UIEvent

--@endregion UIEvent

