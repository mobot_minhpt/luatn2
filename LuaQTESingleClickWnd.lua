local DelegateFactory = import "DelegateFactory"
local CommonDefs = import "L10.Game.CommonDefs"
local UITexture = import "UITexture"
local UnityEngine = import "UnityEngine"
local CUIFx = import "L10.UI.CUIFx"

LuaQTESingleClickWnd = class()
RegistClassMember(LuaQTESingleClickWnd, "Button")
RegistClassMember(LuaQTESingleClickWnd, "CountDownBar")
RegistClassMember(LuaQTESingleClickWnd, "curTime")
RegistClassMember(LuaQTESingleClickWnd, "totTime")
RegistClassMember(LuaQTESingleClickWnd, "UIFx")
RegistClassMember(LuaQTESingleClickWnd, "Circle")
RegistClassMember(LuaQTESingleClickWnd, "HintFx")

function LuaQTESingleClickWnd:Awake()
    self.Button = self.transform:Find("Button").gameObject
    self.CountDownBar = self.transform:Find("Button/CountDownBar"):GetComponent(typeof(UITexture))
    self.UIFx = self.transform:Find("Fx"):GetComponent(typeof(CUIFx))
    self.Circle = self.transform:Find("Button/Circle").gameObject
    self.HintFx = self.transform:Find("Button/HintFx"):GetComponent(typeof(CUIFx))

    self.curTime, self.totTime = 0, 0
    self.gameObject:SetActive(false)
end

function LuaQTESingleClickWnd:Start()
    self.UIFx:LoadFx(CLuaUIFxPaths.QTESingleClickFx)
end

function LuaQTESingleClickWnd:Init()
    if LuaQTEMgr.CurrentQTEType ~= EnumQTEType.SingleClick then
        return
    end

    CommonDefs.AddOnClickListener(
        self.Button,
        DelegateFactory.Action_GameObject(
            function(go)
                self:OnButtonClicked(go)
            end
        ),
        false
    )
    self.curTime = tonumber(LuaQTEMgr.CurrentQTEInfo.qteTimeOffset)
    self.totTime = tonumber(LuaQTEMgr.CurrentQTEInfo.qteDuration)

    self.transform.localPosition =
        LuaQTEMgr.ParsePosition(self.gameObject, LuaQTEMgr.CurrentQTEInfo.xPos, LuaQTEMgr.CurrentQTEInfo.yPos)

    self.HintFx:DestroyFx()
    self.gameObject:SetActive(true)
    
    if LuaQTEMgr.CurrentQTEInfo.HintFx == nil or LuaQTEMgr.CurrentQTEInfo.HintFx == "" then
        self.Circle:SetActive(true)
    else
        self.Circle:SetActive(false)
        self.HintFx:LoadFx(SafeStringFormat3("Fx/UI/Prefab/%s.prefab", LuaQTEMgr.CurrentQTEInfo.HintFx))
    end
    
    
end

function LuaQTESingleClickWnd:Update()
    self.curTime = self.curTime + UnityEngine.Time.deltaTime
    local timeNow = math.min(self.totTime, math.max(self.curTime, 0.0))

    local ratio = 0.0
    if self.totTime ~= 0 then
        ratio = 1 - timeNow / self.totTime
    end
    self.CountDownBar.fillAmount = ratio

    if ratio <=0 then
        self:QTEFail()
    end
end

function LuaQTESingleClickWnd:OnButtonClicked(go)
    LuaQTEMgr.TriggerQTEFinishFx(true, self.transform.localPosition)
    LuaQTEMgr.FinishCurrentQTE(true)
    self:Reset()
end

function LuaQTESingleClickWnd:QTEFail()
    LuaQTEMgr.TriggerQTEFinishFx(false, self.transform.localPosition)
    LuaQTEMgr.FinishCurrentQTE(false)
    self:Reset()
end

function LuaQTESingleClickWnd:OnTimeLimitExceeded()
    LuaQTEMgr.TriggerQTEFinishFx(false, self.transform.localPosition)
    self:Reset()
end

function LuaQTESingleClickWnd:Reset()
    self.curTime, self.totTime = 0, 0
    self.gameObject:SetActive(false)
    g_ScriptEvent:BroadcastInLua("OnQTESubWndClose")
end
