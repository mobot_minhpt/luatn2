-- Auto Generated!!
local CBWZQMgr = import "L10.Game.CBWZQMgr"
local CBWZQSuccWnd = import "L10.UI.CBWZQSuccWnd"
local DelegateFactory = import "DelegateFactory"
local MessageWndManager = import "L10.UI.MessageWndManager"
local UIEventListener = import "UIEventListener"
CBWZQSuccWnd.m_Init_CS2LuaHook = function (this) 
    this.textLabel.text = System.String.Format(this.textLabel.text, CBWZQMgr.Inst.savePlayerName)
    UIEventListener.Get(this.closeBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("SURE_TO_GIVE_UP", nil), DelegateFactory.Action(function () 
            Gac2Gas.RequestUseItem(CBWZQMgr.Inst.saveItemPlace, CBWZQMgr.Inst.saveItemPos, CBWZQMgr.Inst.saveItemName, "1")
            this:Close()
        end), nil, nil, nil, false)
    end)

    UIEventListener.Get(this.submitBtn).onClick = DelegateFactory.VoidDelegate(function (p) 
        MessageWndManager.ShowOKCancelMessage(g_MessageMgr:FormatMessage("SURE_TO_GIVE_XIUQIU", nil), DelegateFactory.Action(function () 
            Gac2Gas.RequestUseItem(CBWZQMgr.Inst.saveItemPlace, CBWZQMgr.Inst.saveItemPos, CBWZQMgr.Inst.saveItemName, "2")
            this:Close()
        end), nil, nil, nil, false)
    end)
end
