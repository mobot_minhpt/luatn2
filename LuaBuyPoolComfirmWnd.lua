local DelegateFactory  = import "DelegateFactory"
local GameObject = import "UnityEngine.GameObject"
local UILabel = import "UILabel"
local CClientHouseMgr = import "L10.Game.CClientHouseMgr"
local CShopMallMgr = import "L10.UI.CShopMallMgr"

LuaBuyPoolComfirmWnd = class()

--@region RegistChildComponent: Dont Modify Manually!

RegistChildComponent(LuaBuyPoolComfirmWnd, "CalcelBtn", "CalcelBtn", GameObject)
RegistChildComponent(LuaBuyPoolComfirmWnd, "ComfirmBtn", "ComfirmBtn", GameObject)
RegistChildComponent(LuaBuyPoolComfirmWnd, "IncreaseButton", "IncreaseButton", GameObject)
RegistChildComponent(LuaBuyPoolComfirmWnd, "CountLabel", "CountLabel", UILabel)
RegistChildComponent(LuaBuyPoolComfirmWnd, "MsgLabel", "MsgLabel", UILabel)

--@endregion RegistChildComponent end

function LuaBuyPoolComfirmWnd:Awake()
    --@region EventBind: Dont Modify Manually!
	UIEventListener.Get(self.CalcelBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnCalcelBtnClick()
	end)

	UIEventListener.Get(self.ComfirmBtn.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnComfirmBtnClick()
	end)

	UIEventListener.Get(self.IncreaseButton.gameObject).onClick = DelegateFactory.VoidDelegate(function (go)
	    self:OnIncreaseButtonClick()
	end)

    --@endregion EventBind end
end

function LuaBuyPoolComfirmWnd:Init()
	self.MsgLabel.text = SafeStringFormat3(LocalString.GetString("是否确认消耗石料[c][00ffff]%d[-][/c]将水域面积上限增加至[c][00ffff]%d[-][/c]?"),LuaPoolMgr.NeedStoneCount,LuaPoolMgr.NewMaxCount)
	self.CountLabel.text = CClientHouseMgr.Inst.mConstructProp.Stone
end

--@region UIEvent

function LuaBuyPoolComfirmWnd:OnCalcelBtnClick()
	LuaPoolMgr.m_BuyPoolNeedSave = false
	CUIManager.CloseUI(CLuaUIResources.BuyPoolComfirmWnd)
end

function LuaBuyPoolComfirmWnd:OnComfirmBtnClick()
	Gac2Gas.RequestAddHousePoolGrid(LuaPoolMgr.AddCount)
end

function LuaBuyPoolComfirmWnd:OnIncreaseButtonClick()
	-- 选中五石仓
	CShopMallMgr.ShowLinyuShoppingMall(21003558)
end

--@endregion UIEvent

function LuaBuyPoolComfirmWnd:OnEnable()
    g_ScriptEvent:AddListener("OnUpdateHouseResource", self, "Init")
    g_ScriptEvent:AddListener("OnSendHouseData", self, "Init")
end

function LuaBuyPoolComfirmWnd:OnDisable( )
    g_ScriptEvent:RemoveListener("OnUpdateHouseResource", self, "Init")
    g_ScriptEvent:RemoveListener("OnSendHouseData", self, "Init")
end
