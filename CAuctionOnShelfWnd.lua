-- Auto Generated!!
local AlignType = import "L10.UI.CItemInfoMgr+AlignType"
local CAuctionMgr = import "L10.Game.CAuctionMgr"
local CAuctionOnShelfWnd = import "L10.UI.CAuctionOnShelfWnd"
local CItemInfoMgr = import "L10.UI.CItemInfoMgr"
local CItemMgr = import "L10.Game.CItemMgr"
local CUIManager = import "L10.UI.CUIManager"
local CUIResources = import "L10.UI.CUIResources"
local EnumItemPlace = import "L10.Game.EnumItemPlace"
local EquipmentTemplate_Equip = import "L10.Game.EquipmentTemplate_Equip"
local Gac2Gas = import "L10.Game.Gac2Gas"
local Item_Item = import "L10.Game.Item_Item"
local LocalString = import "LocalString"
local QnButton = import "L10.UI.QnButton"
local UIEventListener = import "UIEventListener"
local VoidDelegate = import "UIEventListener+VoidDelegate"
CAuctionOnShelfWnd.m_Init_CS2LuaHook = function (this) 
    local item = CAuctionMgr.Inst.itemToShelf
    if item ~= nil then
        local minPreciousPrice = 0
        
        local FixpriceLabel = this.transform:Find("Content/ContentRight/Grid/FixpriceLabel").gameObject
        local FixButton = this.transform:Find("Content/ContentRight/Grid/QnIncreseAndDecreaseButtonFix").gameObject
        -- 不同情况的layout
        if LuaAuctionMgr.NeedShowStartPrice(item.TemplateId) then
            this.m_StartingpriceLabel.gameObject:SetActive(true)
            this.m_SingleItemStartPriceInput.gameObject:SetActive(true)
            LuaUtils.SetLocalPosition(this.m_StartingpriceLabel.transform, 149, -49, 0)
            LuaUtils.SetLocalPosition(this.m_SingleItemStartPriceInput.transform, -20, -100, 0)
            LuaUtils.SetLocalPosition(FixpriceLabel.transform, 149, 62, 0)
            LuaUtils.SetLocalPosition(FixButton.transform, -20, 6, 0)
        else
            this.m_StartingpriceLabel.gameObject:SetActive(false)
            this.m_SingleItemStartPriceInput.gameObject:SetActive(false)
            LuaUtils.SetLocalPosition(FixpriceLabel.transform, 149, 32, 0)
            LuaUtils.SetLocalPosition(FixButton.transform, -20, -24, 0)
        end

        
        this.m_SingleItemStartPriceInput:SetValue(LuaAuctionMgr:GetItemStartPrice(item), false)
        this.m_SingleItemStartPriceInput:EnableButtons(false)
        this.m_SingleItemStartPriceInput:SetInputEnabled(false)
        
        if item.IsEquip then
            local equip = EquipmentTemplate_Equip.GetData(item.TemplateId)
            if equip ~= nil then
                minPreciousPrice = equip.ValuablesFloorPrice
            end
        elseif item.IsItem then
            local it = Item_Item.GetData(item.TemplateId)
            if it ~= nil then
                minPreciousPrice = it.ValuablesFloorPrice
            end
        end
        --m_SingleItemPriceInput.onValueChanged = onPriceChange;
        minPreciousPrice = math.ceil((minPreciousPrice / Paimai_Setting.GetData().LingyuExchangeSilver))
        this.recommendStartPrice = minPreciousPrice
        --maxAuctionStartPrice = (uint)Mathf.CeilToInt(recommendStartPrice * 1.2f);
        --minAuctionStartPrice = (uint)Mathf.FloorToInt(recommendStartPrice * 0.8f);
        this.m_NameLabel.text = item.Name
        if item.IsItem then
            this.m_NameLabel.color = item.Item.Color
        elseif item.IsEquip then
            this.m_NameLabel.color = item.Equip.DisplayColor
        end
        this.m_LevelLabel.text = System.String.Format(LocalString.GetString("{0}级"), item.Grade)
        --m_SingleItemPriceInput.SetMinMax(minAuctionStartPrice, maxAuctionStartPrice, 1);
        --m_SingleItemPriceInput.SetValue(recommendStartPrice);
        local setting = Paimai_Setting.GetData()
        this.m_SingleItemFixPriceInput:SetMinMax(math.ceil(this.recommendStartPrice * setting.OneShootPriceFloorFactor), math.min(math.ceil(this.recommendStartPrice * setting.OneShootPriceCeilFactor), CAuctionOnShelfWnd.maxAuctionPrice), 1)
        if LuaAuctionMgr.NeedShowStartPrice(item.TemplateId) then
            this.m_SingleItemFixPriceInput:SetValue(this.m_SingleItemFixPriceInput:GetMaxValue(), true)
        else
            this.m_SingleItemFixPriceInput:SetValue(this.m_SingleItemFixPriceInput:GetValue(), true)
        end
        
        --设置一下才能显示出来
        this.m_SpentMoneyLabel.text = tostring(Paimai_Setting.GetData().PaiMaiPoundage)

        this.m_ItemTexture:LoadMaterial(item.Icon)

        if item.IsItem then
            --   m_NameLabel.color = CItem.GetColor(item.TemplateId);
            this.m_ItemInfoLabel:SetContent(item.Item.Description)
        else
            --    m_NameLabel.color = CEquipment.GetColor(item.TemplateId);
            this.m_ItemInfoLabel:SetContent(item.Equip:GetDescription())
        end
        local leftCount = CAuctionMgr.MaxShelfCount - CAuctionMgr.Inst.SelfShelfItemCount
        this.m_LeftCountLabel.text = System.String.Format(LocalString.GetString("剩余{0}格"), leftCount)
        if leftCount <= 0 then
            CommonDefs.GetComponent_GameObject_Type(this.m_SubmitButton, typeof(QnButton)).Enabled = false
        else
            CommonDefs.GetComponent_GameObject_Type(this.m_SubmitButton, typeof(QnButton)).Enabled = true
        end
    end
end
CAuctionOnShelfWnd.m_Start_CS2LuaHook = function (this) 
    UIEventListener.Get(this.m_SubmitButton).onClick = MakeDelegateFromCSFunction(this.onRequestOnShelfClick, VoidDelegate, this)
    UIEventListener.Get(this.m_CloseButton).onClick = MakeDelegateFromCSFunction(this.onCloseButtonClick, VoidDelegate, this)
    UIEventListener.Get(this.m_ItemTexture.gameObject).onClick = MakeDelegateFromCSFunction(this.onIconClick, VoidDelegate, this)
end
CAuctionOnShelfWnd.m_onRequestOnShelfClick_CS2LuaHook = function (this, go) 

    local item = CAuctionMgr.Inst.itemToShelf
    if item ~= nil then
        local pos = CItemMgr.Inst:FindItemPosition(EnumItemPlace.Bag, item.Id)
        if pos > 0 then
            local startPrice = LuaAuctionMgr.NeedShowStartPrice(item.TemplateId) and LuaAuctionMgr:GetItemStartPrice(item) or 0
            Gac2Gas.RequestPaimaiOnShelf(pos, 1, startPrice, this.m_SingleItemFixPriceInput:GetValue())
            CUIManager.CloseUI(CUIResources.AuctionOnShelfWnd)
        end
    end
end
CAuctionOnShelfWnd.m_onIconClick_CS2LuaHook = function (this, go) 
    local item = CAuctionMgr.Inst.itemToShelf
    if item == nil then
        return
    end
    CItemInfoMgr.ShowLinkItemInfo(item, true, nil, AlignType.Default, 0, 0, 0, 0, 0)
end
