local MsgPackImpl=import "MsgPackImpl"
local CLingShou=import "L10.Game.CLingShou"
local CLingShouMgr=import "L10.Game.CLingShouMgr"
local LingShouDetails=import "L10.Game.CLingShouBaseMgr+LingShouDetails"

CLuaLingShouOtherMgr = {}

LuaEnumOtherLingShouRequestType = {
    None = 0,
    SingleLingShou = 1,
    PlayerLingShou = 2,
    LingShouBaby = 3,
    PlayerLingShouForProperySwitch = 4,
}
CLuaLingShouOtherMgr.requestType = LuaEnumOtherLingShouRequestType.None

CLuaLingShouOtherMgr.playerId = 0
CLuaLingShouOtherMgr.lingShouId = nil

CLuaLingShouOtherMgr.playerName = nil

CLuaLingShouOtherMgr.lingShouDetailsList = {}

CLuaLingShouOtherMgr.lingShouOverviewList = {}

CLuaLingShouOtherMgr.lastLingShouId = nil
CLuaLingShouOtherMgr.assistLingShouId = nil
CLuaLingShouOtherMgr.partnerLingShouId = nil
CLuaLingShouOtherMgr.futiLingShouId = nil
CLuaLingShouOtherMgr.jiebanQiChangId = 0

function CLuaLingShouOtherMgr.QueryMainPlayerLingShouForProperySwitch()
    if not CClientMainPlayer.Inst then return end

    CLuaLingShouOtherMgr.lingShouId = ""
    CLuaLingShouOtherMgr.playerId = CClientMainPlayer.Inst.Id
    CLuaLingShouOtherMgr.playerName = CClientMainPlayer.Inst.Name
    CLuaLingShouOtherMgr.requestType = LuaEnumOtherLingShouRequestType.PlayerLingShouForProperySwitch
    CLuaLingShouOtherMgr.ClearLingShouData()
    Gac2Gas.QueryAllPlayerLingShou(CLuaLingShouOtherMgr.playerId, 1)
end

function CLuaLingShouOtherMgr.QueryLingShouInfo(playerId,lingshouId, context)
    if CLuaLingShouOtherMgr.lingShouDetailsList[lingshouId] then
        g_ScriptEvent:BroadcastInLua("SendQueryLingShouInfoResult",lingshouId, CLuaLingShouOtherMgr.lingShouDetailsList[lingshouId])
    else
        -- print(playerId,lingshouId)
        Gac2Gas.QueryLingShouInfo(playerId, lingshouId, context)
    end
end
function CLuaLingShouOtherMgr.ClearLingShouData()
    CLuaLingShouOtherMgr.lastLingShouId = nil
    CLuaLingShouOtherMgr.assistLingShouId = nil
    CLuaLingShouOtherMgr.partnerLingShouId = nil
    CLuaLingShouOtherMgr.futiLingShouId = nil
    CLuaLingShouOtherMgr.jiebanQiChangId = 0
    
    CLuaLingShouOtherMgr.lingShouDetailsList = {}
    CLuaLingShouOtherMgr.lingShouOverviewList = {}
end
function CLuaLingShouOtherMgr.GetLingShouDetails(lingshouId)
    return CLuaLingShouOtherMgr.lingShouDetailsList[lingshouId]
end

function CLuaLingShouOtherMgr.AddLingShouDetails(lingshouId,pData,pFightProperty)
    local details = CLuaLingShouOtherMgr.lingShouDetailsList[lingshouId]
    if details then
        details.data = pData
        details.fightProperty = pFightProperty
    else
        details = LingShouDetails()
        details.id = lingshouId
        details.data = pData
        details.fightProperty = pFightProperty
        CLuaLingShouOtherMgr.lingShouDetailsList[lingshouId] = details
    end
    return details
end


function Gas2Gac.SendQueryLingShouOverviewResult(playerId,count,lingshouOverview)
    if count <0 then
        g_MessageMgr:ShowMessage("LingShou_Query_NotExist")
    end
    local overviewInfo = MsgPackImpl.unpack(lingshouOverview)


    CLuaLingShouOtherMgr.lastLingShouId = overviewInfo[0]
    CLuaLingShouOtherMgr.assistLingShouId = overviewInfo[1]
    CLuaLingShouOtherMgr.partnerLingShouId = overviewInfo[2]
    CLuaLingShouOtherMgr.futiLingShouId = overviewInfo[3]
    CLuaLingShouOtherMgr.jiebanQiChangId = overviewInfo[4]
    local start = 5
    local fieldNum = 6
    local t = {}
    for i=1,count do
        table.insert( t,{
            lingshouId = overviewInfo[(i-1)*fieldNum + start],
            lingshouName = overviewInfo[(i-1) * fieldNum + 1 + start],
            level = overviewInfo[(i-1) * fieldNum + 2 + start],
            evolveGrade = overviewInfo[(i-1) * fieldNum + 3 + start],
            templateId = overviewInfo[(i-1) * fieldNum + 4 + start],
            gender = overviewInfo[(i-1) * fieldNum + 5 + start]
        } )
    end
    CLuaLingShouOtherMgr.lingShouOverviewList = t
    if #t>0 then
        if CLuaLingShouOtherMgr.requestType == LuaEnumOtherLingShouRequestType.PlayerLingShou 
            or CLuaLingShouOtherMgr.requestType == LuaEnumOtherLingShouRequestType.PlayerLingShouForProperySwitch then
            CUIManager.ShowUI(CUIResources.LingShouLookUpWnd)
        end
    else
        if CLuaLingShouOtherMgr.requestType == LuaEnumOtherLingShouRequestType.PlayerLingShouForProperySwitch then
            g_MessageMgr:ShowMessage("LINGSHOU_DONT_HAVE")
        end
    end
end


function Gas2Gac.SendQueryLingShouInfoResult(lingshouId, lingshouData)
    local lingshou = CLingShou()
    lingshou:LoadFromString(lingshouData)

    local fightProp = CLingShouMgr.Inst:GetLingShouFightPropByLingShou(lingshou)
    local d = CLuaLingShouOtherMgr.AddLingShouDetails(lingshouId,lingshou,fightProp)
    g_ScriptEvent:BroadcastInLua("SendQueryLingShouInfoResult",lingshouId, d)

    if CLuaLingShouOtherMgr.requestType == LuaEnumOtherLingShouRequestType.SingleLingShou then
        CUIManager.ShowUI(CUIResources.LingShouLookUpWnd)
    elseif CLuaLingShouOtherMgr.requestType == LuaEnumOtherLingShouRequestType.LingShouBaby then
        CLuaLingShouMgr.m_TempLingShouInfo = d
        CUIManager.ShowUI(CUIResources.LingShouOtherBabyWnd)
    end
end

function Gas2Gac.SendQueryLingShouOverview_Offline(playerId)
    g_MessageMgr:ShowMessage("LingShou_Query_Offline")
end

function Gas2Gac.SendQueryLingShouOverview_NotExist()
    g_MessageMgr:ShowMessage("LingShou_Query_NotExist")

end
function Gas2Gac.SendQueryLingShouInfoResult_Offline(lingshouId)
    g_MessageMgr:ShowMessage("LingShou_Query_Offline")

end
function Gas2Gac.SendQueryLingShouInfoResult_NotExist(lingshouId)
    g_MessageMgr:ShowMessage("Speak_LingShou_Query_NotExist")
end
