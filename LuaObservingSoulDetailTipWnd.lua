local ParticleSystem = import "UnityEngine.ParticleSystem"

LuaObservingSoulDetailTipWnd = class()

RegistChildComponent(LuaObservingSoulDetailTipWnd, "m_Fx","Fx", CUIFx)
RegistChildComponent(LuaObservingSoulDetailTipWnd, "m_ObserveBtn","ObserveBtn", GameObject)
RegistChildComponent(LuaObservingSoulDetailTipWnd, "m_ConfirmBtn","ConfirmBtn", GameObject)
RegistChildComponent(LuaObservingSoulDetailTipWnd, "m_Text","Text", UILabel)
RegistChildComponent(LuaObservingSoulDetailTipWnd, "m_NameLabel","NameLabel", UILabel)

function LuaObservingSoulDetailTipWnd:Init()
    self.transform.position = LuaObservingSoulMgr.m_TipWndPos
    self.m_ObserveBtn:SetActive(not LuaObservingSoulMgr.m_IsRightDesire)
    self.m_ConfirmBtn:SetActive(LuaObservingSoulMgr.m_IsRightDesire)
    self.m_Fx.OnLoadFxFinish = DelegateFactory.Action(function()
        self:OnLoadFxFinish()
    end)
    self.m_Fx:LoadFx("fx/ui/prefab/UI_linghunguancha_huo02.prefab")
    self.m_NameLabel.text = LuaObservingSoulMgr.m_ObservingSoulData[LuaObservingSoulMgr.m_SelectIndex].name
    self.m_Text.text = LuaObservingSoulMgr.m_ObservingSoulData[LuaObservingSoulMgr.m_SelectIndex].msg
    UIEventListener.Get(self.m_ObserveBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.CloseUI(CLuaUIResources.ObservingSoulDetailTipWnd)
    end)
    UIEventListener.Get(self.m_ConfirmBtn).onClick = DelegateFactory.VoidDelegate(function(go)
        CUIManager.CloseUI(CLuaUIResources.ObservingSoulDetailTipWnd)
        g_ScriptEvent:BroadcastInLua("FinishObservingSoulTask")
    end)
end

function LuaObservingSoulDetailTipWnd:OnLoadFxFinish()
    local particleSystems = CommonDefs.GetComponentsInChildren_GameObject_Type_Boolean(self.m_Fx.gameObject, typeof(ParticleSystem),true)
    for i = 0, particleSystems.Length - 1 do
        particleSystems[i].startColor = LuaObservingSoulMgr.m_ObservingSoulData[LuaObservingSoulMgr.m_SelectIndex].color
        particleSystems[i]:Stop()
        particleSystems[i]:Play()
    end
end